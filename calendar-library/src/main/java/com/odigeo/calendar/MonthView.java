/*
Copyright 2012 Square, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.odigeo.calendar;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MonthView extends LinearLayout {
  TextView title;
  CalendarGridView grid;
  TextView tvCountryCaption;
  TextView tvRegionCaption;
  LinearLayout linearLayoutCaptions;
  private Listener listener;
  private List<CalendarCellDecorator> decorators;
  private boolean isRtl;
  private Locale locale;

  public MonthView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public static MonthView create(ViewGroup parent, LayoutInflater inflater,
      DateFormat weekdayNameFormat, Listener listener, Calendar today, int dividerColor,
      int dayBackgroundResId, int dayTextColorResId, int titleTextColor, boolean displayHeader,
      int headerTextColor, Locale locale, DayViewAdapter adapter, String countryCaption,
      String regionCaption) {
    return create(parent, inflater, weekdayNameFormat, listener, today, dividerColor,
        dayBackgroundResId, dayTextColorResId, titleTextColor, displayHeader, headerTextColor, null,
        locale, adapter, countryCaption, regionCaption);
  }

  public static MonthView create(ViewGroup parent, LayoutInflater inflater,
      DateFormat weekdayNameFormat, Listener listener, Calendar today, int dividerColor,
      int dayBackgroundResId, int dayTextColorResId, int titleTextColor, boolean displayHeader,
      int headerTextColor, List<CalendarCellDecorator> decorators, Locale locale,
      DayViewAdapter adapter, String countryCaption, String regionCaption) {
    final MonthView view = (MonthView) inflater.inflate(R.layout.month, parent, false);
    view.setDayViewAdapter(adapter);
    view.setDividerColor(dividerColor);
    view.setDayTextColor(dayTextColorResId);
    view.setTitleTextColor(titleTextColor);
    view.setDisplayHeader(displayHeader);
    view.setHeaderTextColor(headerTextColor);

    if (dayBackgroundResId != 0) {
      view.setDayBackground(dayBackgroundResId);
    }

    final int originalDayOfWeek = today.get(Calendar.DAY_OF_WEEK);

    view.isRtl = isRtl(locale);
    view.locale = locale;
    int firstDayOfWeek = today.getFirstDayOfWeek();
    final CalendarRowView headerRow = (CalendarRowView) view.grid.getChildAt(0);
    for (int offset = 0; offset < 7; offset++) {
      today.set(Calendar.DAY_OF_WEEK, getDayOfWeek(firstDayOfWeek, offset, view.isRtl));
      final TextView textView = (TextView) headerRow.getChildAt(offset);
      textView.setText(weekdayNameFormat.format(today.getTime()));
      if (today.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
          || today.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          textView.setTextAppearance(R.style.CalendarCellText_DayHeaderBold);
        } else {
          textView.setTextAppearance(view.getContext(), R.style.CalendarCellText_DayHeaderBold);
        }
      } else {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          textView.setTextAppearance(R.style.CalendarCellText_DayHeader);
        } else {
          textView.setTextAppearance(view.getContext(), R.style.CalendarCellText_DayHeader);
        }
      }
    }
    today.set(Calendar.DAY_OF_WEEK, originalDayOfWeek);
    view.listener = listener;
    view.decorators = decorators;

    view.linearLayoutCaptions.setVisibility(View.GONE);

    if ((countryCaption != null && countryCaption.length() > 0)) {
      view.tvCountryCaption.setText(countryCaption);
      view.linearLayoutCaptions.setVisibility(View.VISIBLE);
    } else {
      view.tvCountryCaption.setVisibility(View.GONE);
    }

    if ((regionCaption != null && regionCaption.length() > 0)) {
      view.tvRegionCaption.setText(regionCaption);
      view.linearLayoutCaptions.setVisibility(View.VISIBLE);
    } else {
      view.tvRegionCaption.setVisibility(View.GONE);
    }

    return view;
  }

  private static int getDayOfWeek(int firstDayOfWeek, int offset, boolean isRtl) {
    int dayOfWeek = firstDayOfWeek + offset;
    if (isRtl) {
      return 8 - dayOfWeek;
    }
    return dayOfWeek;
  }

  private static boolean isRtl(Locale locale) {
    // TODO convert the build to gradle and use getLayoutDirection instead of this (on 17+)?
    final int directionality = Character.getDirectionality(locale.getDisplayName(locale).charAt(0));
    return directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT
        || directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC;
  }

  public List<CalendarCellDecorator> getDecorators() {
    return decorators;
  }

  public void setDecorators(List<CalendarCellDecorator> decorators) {
    this.decorators = decorators;
  }

  @Override protected void onFinishInflate() {
    super.onFinishInflate();
    title = (TextView) findViewById(R.id.title);
    grid = (CalendarGridView) findViewById(R.id.calendar_grid);

    tvCountryCaption = (TextView) findViewById(R.id.tvCountryCaption);
    tvRegionCaption = (TextView) findViewById(R.id.tvRegionCaption);
    linearLayoutCaptions = (LinearLayout) findViewById(R.id.footer);
  }

  public void init(MonthDescriptor month, List<List<MonthCellDescriptor>> cells,
      boolean displayOnly, Typeface titleTypeface, Typeface dateTypeface) {
    Logr.d("Initializing MonthView (%d) for %s", System.identityHashCode(this), month);
    long start = System.currentTimeMillis();
    title.setText(month.getLabel());
    NumberFormat numberFormatter = NumberFormat.getInstance(locale);

    final int numRows = cells.size();
    grid.setNumRows(numRows);
    Calendar calendar = Calendar.getInstance();
    for (int i = 0; i < 6; i++) {
      CalendarRowView weekRow = (CalendarRowView) grid.getChildAt(i + 1);
      weekRow.setListener(listener);
      if (i < numRows) {
        weekRow.setVisibility(VISIBLE);
        List<MonthCellDescriptor> week = cells.get(i);
        for (int c = 0; c < week.size(); c++) {
          MonthCellDescriptor cell = week.get(isRtl ? 6 - c : c);
          CalendarCellView cellView = (CalendarCellView) weekRow.getChildAt(c);

          String cellDate = numberFormatter.format(cell.getValue());
          if (!cellView.getDayOfMonthTextView().getText().equals(cellDate)
              && cell.isCurrentMonth()) {
            cellView.getDayOfMonthTextView().setText(cellDate);
          }

          calendar.setTime(cell.getDate());
          if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
              || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            cellView.getDayOfMonthTextView().setTypeface(null, Typeface.BOLD);
          } else {
            cellView.getDayOfMonthTextView().setTypeface(null, Typeface.NORMAL);
          }

          cellView.setEnabled(cell.isCurrentMonth());
          cellView.setClickable(!displayOnly);

          cellView.setSelectable(cell.isSelectable());
          cellView.setSelected(cell.isSelected());
          cellView.setCurrentMonth(cell.isCurrentMonth());
          cellView.setToday(cell.isToday());
          cellView.setRangeState(cell.getRangeState());
          cellView.setHighlighted(cell.isHighlighted());
          cellView.setTag(cell);

          if (null != decorators) {
            for (CalendarCellDecorator decorator : decorators) {
              decorator.decorate(cellView, cell.getDate());
            }
          }
        }
      } else {
        weekRow.setVisibility(GONE);
      }
    }

    if (titleTypeface != null) {
      title.setTypeface(titleTypeface);
    }
    if (dateTypeface != null) {
      grid.setTypeface(dateTypeface);
    }

    Logr.d("MonthView.init took %d ms", System.currentTimeMillis() - start);
  }

  public void setDividerColor(int color) {
    grid.setDividerColor(color);
  }

  public void setDayBackground(int resId) {
    grid.setDayBackground(resId);
  }

  public void setDayTextColor(int resId) {
    grid.setDayTextColor(resId);
  }

  public void setDayViewAdapter(DayViewAdapter adapter) {
    grid.setDayViewAdapter(adapter);
  }

  public void setTitleTextColor(int color) {
    title.setTextColor(color);
  }

  public void setDisplayHeader(boolean displayHeader) {
    grid.setDisplayHeader(displayHeader);
  }

  public void setHeaderTextColor(int color) {
    grid.setHeaderTextColor(color);
  }

  public interface Listener {
    void handleClick(MonthCellDescriptor cell);
  }
}
