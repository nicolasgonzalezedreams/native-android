/*
Copyright 2012 Square, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.odigeo.calendar;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.calendar.MonthCellDescriptor.RangeState;

public class CalendarCellView extends FrameLayout {
  private static final int[] STATE_SELECTABLE = {
      R.attr.tsquare_state_selectable
  };
  private static final int[] STATE_CURRENT_MONTH = {
      R.attr.tsquare_state_current_month
  };
  private static final int[] STATE_TODAY = {
      R.attr.tsquare_state_today
  };
  private static final int[] STATE_HIGHLIGHTED = {
      R.attr.tsquare_state_highlighted
  };
  private static final int[] STATE_RANGE_FIRST = {
      R.attr.tsquare_state_range_first
  };
  private static final int[] STATE_RANGE_MIDDLE = {
      R.attr.tsquare_state_range_middle
  };
  private static final int[] STATE_RANGE_MIDDLE_AND_SELECTED = {
      R.attr.tsquare_state_range_middle_and_selected
  };
  private static final int[] STATE_RANGE_LAST = {
      R.attr.tsquare_state_range_last
  };
  private static final int[] STATE_RANGE_SAME_DAY = {
      R.attr.tsquare_state_range_same_day
  };
  private static final int[] STATE_RANGE_FIRST_SAME_DAY = {
      R.attr.tsquare_state_range_first_same_day
  };
  private static final int[] STATE_RANGE_LAST_SAME_DAY = {
      R.attr.tsquare_state_range_last_same_day
  };
  private static final int[] STATE_RANGE_MIDDLE_AND_SELECTED_SAME_DAY = {
      R.attr.tsquare_state_range_middle_and_selected_same_day
  };
  private static final int[] STATE_RANGE_MULTI_TO_CHANGE_FIRST = {
      R.attr.tsquare_state_range_multi_to_change_first
  };
  private static final int[] STATE_RANGE_MULTI_TO_CHANGE_MIDDLE = {
      R.attr.tsquare_state_range_multi_to_change_middle
  };
  private static final int[] STATE_RANGE_MULTI_TO_CHANGE_LAST = {
      R.attr.tsquare_state_range_multi_to_change_last
  };

  private boolean isSelectable = false;
  private boolean isCurrentMonth = false;
  private boolean isToday = false;
  private boolean isHighlighted = false;
  private RangeState rangeState = RangeState.NONE;
  private TextView dayOfMonthTextView;
  private ImageView holidayIndicatorImageView;

  @SuppressWarnings("UnusedDeclaration") //
  public CalendarCellView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public boolean isCurrentMonth() {
    return isCurrentMonth;
  }

  public void setCurrentMonth(boolean isCurrentMonth) {
    if (this.isCurrentMonth != isCurrentMonth) {
      this.isCurrentMonth = isCurrentMonth;
      refreshDrawableState();
    }
  }

  public boolean isToday() {
    return isToday;
  }

  public void setToday(boolean isToday) {
    if (this.isToday != isToday) {
      this.isToday = isToday;
      refreshDrawableState();
    }
  }

  public boolean isSelectable() {
    return isSelectable;
  }

  public void setSelectable(boolean isSelectable) {
    if (this.isSelectable != isSelectable) {
      this.isSelectable = isSelectable;
      refreshDrawableState();
    }
  }

  public boolean isHighlighted() {
    return isHighlighted;
  }

  public void setHighlighted(boolean isHighlighted) {
    if (this.isHighlighted != isHighlighted) {
      this.isHighlighted = isHighlighted;
      refreshDrawableState();
    }
  }

  public RangeState getRangeState() {
    return rangeState;
  }

  public void setRangeState(MonthCellDescriptor.RangeState rangeState) {
    if (this.rangeState != rangeState) {
      this.rangeState = rangeState;
      refreshDrawableState();
    }
  }

  @Override protected int[] onCreateDrawableState(int extraSpace) {
    final int[] drawableState = super.onCreateDrawableState(extraSpace + 5);

    if (isSelectable) {
      mergeDrawableStates(drawableState, STATE_SELECTABLE);
    }

    if (isCurrentMonth) {
      mergeDrawableStates(drawableState, STATE_CURRENT_MONTH);
    }

    if (isToday) {
      mergeDrawableStates(drawableState, STATE_TODAY);
    }

    if (isHighlighted) {
      mergeDrawableStates(drawableState, STATE_HIGHLIGHTED);
    }

    switch (rangeState) {
      case FIRST:
        mergeDrawableStates(drawableState, STATE_RANGE_FIRST);
        break;

      case MIDDLE:
        mergeDrawableStates(drawableState, STATE_RANGE_MIDDLE);
        break;

      case MIDDLE_AND_SELECTED:
        mergeDrawableStates(drawableState, STATE_RANGE_MIDDLE_AND_SELECTED);
        break;

      case LAST:
        mergeDrawableStates(drawableState, STATE_RANGE_LAST);
        break;

      case SAME_DAY:
        mergeDrawableStates(drawableState, STATE_RANGE_SAME_DAY);
        break;

      case FIRST_SAME_DAY:
        mergeDrawableStates(drawableState, STATE_RANGE_FIRST_SAME_DAY);
        break;

      case LAST_SAME_DAY:
        mergeDrawableStates(drawableState, STATE_RANGE_LAST_SAME_DAY);
        break;

      case MIDDLE_AND_SELECTED_SAME_DAY:
        mergeDrawableStates(drawableState, STATE_RANGE_MIDDLE_AND_SELECTED_SAME_DAY);
        break;

      case MULTI_TO_CHANGE_FIRST:
        mergeDrawableStates(drawableState, STATE_RANGE_MULTI_TO_CHANGE_FIRST);
        break;

      case MULTI_TO_CHANGE_MIDDLE:
        mergeDrawableStates(drawableState, STATE_RANGE_MULTI_TO_CHANGE_MIDDLE);
        break;

      case MULTI_TO_CHANGE_LAST:
        mergeDrawableStates(drawableState, STATE_RANGE_MULTI_TO_CHANGE_LAST);
        break;
    }

    return drawableState;
  }

  public TextView getDayOfMonthTextView() {
    if (dayOfMonthTextView == null) {
      throw new IllegalStateException(
          "You have to setDayOfMonthCell in your custom DayViewAdapter.");
    }
    return dayOfMonthTextView;
  }

  public void setDayOfMonthTextView(TextView textView) {
    this.dayOfMonthTextView = textView;
  }

  public ImageView getHolidayIndicator() {
    if (holidayIndicatorImageView == null) {
      throw new IllegalStateException(
          "You have to setDayOfMonthCell in your custom DayViewAdapter.");
    }
    return holidayIndicatorImageView;
  }

  public void setHolidayIndicator(ImageView imageView) {
    this.holidayIndicatorImageView = imageView;
  }
}
