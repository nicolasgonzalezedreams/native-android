package com.odigeo.calendar;

import android.view.ContextThemeWrapper;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DefaultDayViewAdapter implements DayViewAdapter {
  @Override public void makeCellView(CalendarCellView parent) {
    RelativeLayout relativeLayout =
        new RelativeLayout(new ContextThemeWrapper(parent.getContext(), R.style.CalendarCell));
    TextView textView = new TextView(
        new ContextThemeWrapper(parent.getContext(), R.style.CalendarCellText_CalendarDate));
    ImageView imageView =
        new ImageView(new ContextThemeWrapper(parent.getContext(), R.style.CalendarCell_Indicator));

    textView.setId(R.id.holidays_marker_id);
    imageView.setContentDescription("Description");

    textView.setDuplicateParentStateEnabled(true);
    relativeLayout.setDuplicateParentStateEnabled(true);

    RelativeLayout.LayoutParams paramsTextView =
        new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT);
    paramsTextView.addRule(RelativeLayout.CENTER_IN_PARENT);

    RelativeLayout.LayoutParams paramsImageView =
        new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT);
    paramsImageView.addRule(RelativeLayout.CENTER_HORIZONTAL);
    paramsImageView.addRule(RelativeLayout.BELOW, textView.getId());

    relativeLayout.addView(textView, paramsTextView);
    relativeLayout.addView(imageView, paramsImageView);

    parent.setDayOfMonthTextView(textView);
    parent.setHolidayIndicator(imageView);
    parent.addView(relativeLayout);
  }
}
