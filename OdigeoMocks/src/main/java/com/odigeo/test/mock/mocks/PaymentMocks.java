package com.odigeo.test.mock.mocks;

import com.odigeo.data.entity.CreditCardBinDetails;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.contract.CreditCardType;
import com.odigeo.data.entity.shoppingCart.AirlineGroup;
import com.odigeo.data.entity.shoppingCart.BaggageApplicationLevel;
import com.odigeo.data.entity.shoppingCart.BaggageConditions;
import com.odigeo.data.entity.shoppingCart.BaggageDescriptor;
import com.odigeo.data.entity.shoppingCart.BaggageSelectionResponse;
import com.odigeo.data.entity.shoppingCart.BookingBasicInfo;
import com.odigeo.data.entity.shoppingCart.BookingDetail;
import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.data.entity.shoppingCart.BookingResponseStatus;
import com.odigeo.data.entity.shoppingCart.BookingStatus;
import com.odigeo.data.entity.shoppingCart.Buyer;
import com.odigeo.data.entity.shoppingCart.BuyerIdentificationType;
import com.odigeo.data.entity.shoppingCart.BuyerRequiredFields;
import com.odigeo.data.entity.shoppingCart.Carrier;
import com.odigeo.data.entity.shoppingCart.CollectionMethod;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.FeeDetails;
import com.odigeo.data.entity.shoppingCart.FeeInfo;
import com.odigeo.data.entity.shoppingCart.Insurance;
import com.odigeo.data.entity.shoppingCart.InsuranceShoppingItem;
import com.odigeo.data.entity.shoppingCart.ItinerariesLegend;
import com.odigeo.data.entity.shoppingCart.Itinerary;
import com.odigeo.data.entity.shoppingCart.ItineraryProviderBooking;
import com.odigeo.data.entity.shoppingCart.ItineraryProviderBookings;
import com.odigeo.data.entity.shoppingCart.ItineraryShoppingItem;
import com.odigeo.data.entity.shoppingCart.ItinerarySortCriteria;
import com.odigeo.data.entity.shoppingCart.Location;
import com.odigeo.data.entity.shoppingCart.MarketingRevenueDataAdapter;
import com.odigeo.data.entity.shoppingCart.MessageResponse;
import com.odigeo.data.entity.shoppingCart.Money;
import com.odigeo.data.entity.shoppingCart.OtherProductsShoppingItems;
import com.odigeo.data.entity.shoppingCart.PricingBreakdown;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItem;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItemType;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownStep;
import com.odigeo.data.entity.shoppingCart.PromotionalCodeUsageResponse;
import com.odigeo.data.entity.shoppingCart.SectionResult;
import com.odigeo.data.entity.shoppingCart.Segment;
import com.odigeo.data.entity.shoppingCart.SegmentResult;
import com.odigeo.data.entity.shoppingCart.SegmentTypeIndex;
import com.odigeo.data.entity.shoppingCart.SelectableBaggageDescriptor;
import com.odigeo.data.entity.shoppingCart.ShoppingCart;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.shoppingCart.ShoppingCartPriceCalculator;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.data.entity.shoppingCart.TravellerIdentificationType;
import com.odigeo.data.entity.shoppingCart.TravellerInformationFieldConditionRule;
import com.odigeo.data.entity.shoppingCart.TravellerRequiredFields;
import com.odigeo.data.entity.shoppingCart.TravellerType;
import com.odigeo.data.net.error.DAPIError;
import com.odigeo.test.mock.MocksProvider;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.odigeo.data.entity.shoppingCart.RequiredField.MANDATORY;

public class PaymentMocks {
  private static final String INVALID_BOOKING_ID = "ERR-003";
  private static final String BOOKING_FAKE_ARRIVAL_CITY_NAME = "fake_arrival_city_name";
  private static final String BOOKING_FAKE_TRIP_TYPE = Booking.TRIP_TYPE_ONE_WAY;
  private static final String BOOKING_FAKE_AIRPORT_CODE = "MAD";
  private static double COLLECTION_OPTION_FEE = 3.0;

  public List<PricingBreakdownItem> provideListPricingBreakdownItems() {
    List<PricingBreakdownItem> pricingBreakdownItems = new ArrayList<>();

    PricingBreakdownItem pricingBreakdownItem = new PricingBreakdownItem();
    pricingBreakdownItem.setPriceItemType(PricingBreakdownItemType.PROMOCODE_DISCOUNT);
    pricingBreakdownItem.setPriceItemAmount(new BigDecimal(2));

    pricingBreakdownItems.add(pricingBreakdownItem);

    return pricingBreakdownItems;
  }

  public CreateShoppingCartResponse provideCreateShoppingCartResponse() {
    CreateShoppingCartResponse createShoppingCartResponse = new CreateShoppingCartResponse();
    createShoppingCartResponse.setPricingBreakdown(providePricingBreakdown());
    createShoppingCartResponse.setSortCriteria(ItinerarySortCriteria.MINIMUM_PURCHASABLE_PRICE);
    createShoppingCartResponse.setShoppingCart(provideShoppingCart());
    return createShoppingCartResponse;
  }

  public CreateShoppingCartResponse provideCreateShoppingCartResponseWithPromoCode() {
    CreateShoppingCartResponse createShoppingCartResponse = new CreateShoppingCartResponse();
    createShoppingCartResponse.setPricingBreakdown(providePricingBreakdown());
    createShoppingCartResponse.setSortCriteria(ItinerarySortCriteria.MINIMUM_PURCHASABLE_PRICE);
    createShoppingCartResponse.setShoppingCart(provideShoppingCartWithPromoCodeAdded());
    return createShoppingCartResponse;
  }

  public CreateShoppingCartResponse provideCreateShoppingCartResponseWithOutdatedBooking() {
    CreateShoppingCartResponse createShoppingCartResponse = new CreateShoppingCartResponse();
    createShoppingCartResponse.setPricingBreakdown(providePricingBreakdown());
    createShoppingCartResponse.setSortCriteria(ItinerarySortCriteria.MINIMUM_PURCHASABLE_PRICE);
    createShoppingCartResponse.setShoppingCart(provideShoppingCartWithPromoCodeAdded());

    MessageResponse messageResponse = new MessageResponse();
    messageResponse.setCode(INVALID_BOOKING_ID);
    List<MessageResponse> messageResponses = new ArrayList<>();
    messageResponses.add(messageResponse);

    createShoppingCartResponse.setMessages(messageResponses);
    return createShoppingCartResponse;
  }

  public CreateShoppingCartResponse provideCreateShoppingCartResponseWithPromoCodeErrors() {
    CreateShoppingCartResponse createShoppingCartResponse = new CreateShoppingCartResponse();
    createShoppingCartResponse.setPricingBreakdown(providePricingBreakdown());
    createShoppingCartResponse.setSortCriteria(ItinerarySortCriteria.MINIMUM_PURCHASABLE_PRICE);
    createShoppingCartResponse.setShoppingCart(provideShoppingCartWithPromoCodeAdded());

    MessageResponse messageResponse = new MessageResponse();
    messageResponse.setCode(DAPIError.WNG_007.getDapiError());
    List<MessageResponse> messageResponses = new ArrayList<>();
    messageResponses.add(messageResponse);

    createShoppingCartResponse.setMessages(messageResponses);
    return createShoppingCartResponse;
  }

  public CreateShoppingCartResponse provideCreateShoppingCartResponseWithoutInsurance() {
    CreateShoppingCartResponse createShoppingCartResponse = new CreateShoppingCartResponse();
    createShoppingCartResponse.setPricingBreakdown(providePricingBreakdown());
    createShoppingCartResponse.setSortCriteria(ItinerarySortCriteria.MINIMUM_PURCHASABLE_PRICE);
    createShoppingCartResponse.setShoppingCart(provideShoppingCartWithoutInsurances());
    return createShoppingCartResponse;
  }

  public PricingBreakdown providePricingBreakdown() {
    PricingBreakdown pricingBreakdown = new PricingBreakdown();
    pricingBreakdown.setPricingBreakdownSteps(providePricingBreakdownStepsWithoutMembership());

    return pricingBreakdown;
  }

  public List<PricingBreakdownItem> providePricingBreakdownItems() {
    List<PricingBreakdownItem> pricingBreakdownItems = new ArrayList<>();
    pricingBreakdownItems.add(
        providePriceBreakdownItem(123, PricingBreakdownItemType.ADULTS_PRICE));
    pricingBreakdownItems.add(
        providePriceBreakdownItem(123, PricingBreakdownItemType.SERVICE_CHARGES));
    return pricingBreakdownItems;
  }

  public List<PricingBreakdownStep> providePricingBreakdownStepsWithoutMembership() {
    List<PricingBreakdownItem> pricingBreakdownItems = providePricingBreakdownItems();

    PricingBreakdownStep pricingBreakdownDefaultStep = new PricingBreakdownStep();
    pricingBreakdownDefaultStep.setStep(Step.DEFAULT);
    pricingBreakdownDefaultStep.setTotalPrice(new BigDecimal(123));
    pricingBreakdownDefaultStep.setPricingBreakdownItems(pricingBreakdownItems);

    PricingBreakdownStep pricingBreakdownInsuranceStep = new PricingBreakdownStep();
    pricingBreakdownInsuranceStep.setStep(Step.INSURANCE);
    pricingBreakdownInsuranceStep.setTotalPrice(new BigDecimal(123));
    pricingBreakdownInsuranceStep.setPricingBreakdownItems(pricingBreakdownItems);

    List<PricingBreakdownStep> pricingBreakdownSteps = new ArrayList<>();
    pricingBreakdownSteps.add(pricingBreakdownDefaultStep);
    pricingBreakdownSteps.add(pricingBreakdownInsuranceStep);
    return pricingBreakdownSteps;
  }

  private PricingBreakdownItem providePriceBreakdownItem(double amount,
      PricingBreakdownItemType type) {
    PricingBreakdownItem pricingBreakdownItem = new PricingBreakdownItem();
    pricingBreakdownItem.setPriceItemAmount(new BigDecimal(amount));
    pricingBreakdownItem.setPriceItemType(type);
    return pricingBreakdownItem;
  }

  private List<BaggageConditions> provideBaggageConditions() {
    SelectableBaggageDescriptor noBaggageSelectableBaggageDescriptor =
        new SelectableBaggageDescriptor();
    noBaggageSelectableBaggageDescriptor.setPrice(new BigDecimal(0));
    noBaggageSelectableBaggageDescriptor.setBaggageDescriptor(new BaggageDescriptor());

    SelectableBaggageDescriptor oneSelectableBaggageDescriptor = new SelectableBaggageDescriptor();
    oneSelectableBaggageDescriptor.setPrice(new BigDecimal(17.50));
    oneSelectableBaggageDescriptor.setBaggageDescriptor(new BaggageDescriptor(1, 20));

    List<SelectableBaggageDescriptor> selectableBaggageDescriptors = new ArrayList<>();
    selectableBaggageDescriptors.add(noBaggageSelectableBaggageDescriptor);
    selectableBaggageDescriptors.add(oneSelectableBaggageDescriptor);

    BaggageConditions baggageCondition = new BaggageConditions();
    baggageCondition.setSegmentTypeIndex(SegmentTypeIndex.FIRST);
    baggageCondition.setSelectableBaggageDescriptor(selectableBaggageDescriptors);
    baggageCondition.setBaggageApplicationLevel(BaggageApplicationLevel.SEGMENT);

    List<BaggageConditions> baggageConditionsList = new ArrayList<>();
    baggageConditionsList.add(baggageCondition);
    return baggageConditionsList;
  }

  public List<BaggageConditions> provideBaggageConditionsIncluded() {
    BaggageDescriptor baggageDescriptor = new BaggageDescriptor(1, 15);

    BaggageConditions baggageConditionOutbound = new BaggageConditions();
    baggageConditionOutbound.setSegmentTypeIndex(SegmentTypeIndex.FIRST);
    baggageConditionOutbound.setBaggageApplicationLevel(BaggageApplicationLevel.SEGMENT);
    baggageConditionOutbound.setBaggageDescriptorIncludedInPrice(baggageDescriptor);
    baggageConditionOutbound.setSelectableBaggageDescriptor(
        new ArrayList<SelectableBaggageDescriptor>());

    BaggageConditions baggageConditionInbound = new BaggageConditions();
    baggageConditionInbound.setSegmentTypeIndex(SegmentTypeIndex.SECOND);
    baggageConditionInbound.setBaggageApplicationLevel(BaggageApplicationLevel.SEGMENT);
    baggageConditionInbound.setBaggageDescriptorIncludedInPrice(baggageDescriptor);
    baggageConditionInbound.setSelectableBaggageDescriptor(
        new ArrayList<SelectableBaggageDescriptor>());

    List<BaggageConditions> baggageConditionsList = new ArrayList<>();
    baggageConditionsList.add(baggageConditionOutbound);
    baggageConditionsList.add(baggageConditionInbound);
    return baggageConditionsList;
  }

  private List<TravellerRequiredFields> provideTravellerFieldsList() {
    List<TravellerRequiredFields> requiredFields = new ArrayList<>();
    requiredFields.add(provideTravellerRequiredFields());
    return requiredFields;
  }

  public TravellerRequiredFields provideTravellerRequiredFields() {
    TravellerRequiredFields fields = new TravellerRequiredFields();
    fields.setConditionallyNeededFields(new ArrayList<TravellerInformationFieldConditionRule>());
    fields.setIdentificationTypes(new ArrayList<TravellerIdentificationType>());
    fields.setBaggageConditions(provideBaggageConditions());
    fields.setFrequentFlyerAirlineGroups(provideAirlineGroupField());
    fields.setNeedsTitle(MANDATORY);
    fields.setNeedsName(MANDATORY);
    fields.setNeedsGender(MANDATORY);
    fields.setTravellerType(TravellerType.ADULT);
    return fields;
  }

  private List<AirlineGroup> provideAirlineGroupField() {
    ArrayList<AirlineGroup> airlineGroupList = new ArrayList<>();
    AirlineGroup airlineGroup = new AirlineGroup();
    airlineGroup.setCarriers(provideCarriersList());
    airlineGroupList.add(airlineGroup);
    return airlineGroupList;
  }

  private List<Carrier> provideCarriersList() {
    List<Carrier> carriers = new ArrayList<>();
    Carrier carrier = new Carrier("LH", "Lufthansa");
    Carrier secondCarrier = new Carrier("TP", "TAP Portugal");
    carriers.add(carrier);
    carriers.add(secondCarrier);
    return carriers;
  }

  private BuyerRequiredFields provideBuyerFields() {
    BuyerRequiredFields buyerRequiredFields = new BuyerRequiredFields();
    buyerRequiredFields.setNeedsAddress(MANDATORY);
    buyerRequiredFields.setNeedsCityName(MANDATORY);
    buyerRequiredFields.setNeedsZipCode(MANDATORY);
    buyerRequiredFields.setNeedsCountryCode(MANDATORY);
    buyerRequiredFields.setNeedsMail(MANDATORY);
    buyerRequiredFields.setNeedsPhoneNumber(MANDATORY);
    buyerRequiredFields.setNeedsMail(MANDATORY);
    buyerRequiredFields.setIdentificationTypes(new ArrayList<BuyerIdentificationType>());
    return buyerRequiredFields;
  }

  private List<SegmentResult> provideSegmentResults() {
    List<SegmentResult> segmentResults = new ArrayList<>();
    SegmentResult result = new SegmentResult();

    Segment segment = new Segment();
    List<Integer> sections = new ArrayList<>();
    sections.add(Integer.valueOf(1));
    segment.setSections(sections);

    result.setSegment(segment);
    segmentResults.add(result);
    return segmentResults;
  }

  private List<SectionResult> provideSectionResults() {
    com.odigeo.data.entity.shoppingCart.Section section =
        new com.odigeo.data.entity.shoppingCart.Section();
    section.setCarrier(0);
    section.setFrom(0);
    section.setTo(1);

    SectionResult sectionResult = new SectionResult();
    sectionResult.setId(1);
    sectionResult.setSection(section);

    List<SectionResult> sectionResults = new ArrayList<>();
    sectionResults.add(sectionResult);
    return sectionResults;
  }

  private ItineraryShoppingItem provideItineraryShoppingItem() {
    ItineraryShoppingItem itineraryShoppingItem = new ItineraryShoppingItem();
    Itinerary itinerary = new Itinerary();
    ItinerariesLegend itinerariesLegend = new ItinerariesLegend();
    itinerariesLegend.setSegmentResults(provideSegmentResults());
    itinerariesLegend.setSectionResults(provideSectionResults());
    itinerariesLegend.setCarriers(provideCarriersList());
    itinerariesLegend.setLocations(provideLocations());
    itinerary.setLegend(itinerariesLegend);
    itineraryShoppingItem.setItinerary(itinerary);
    return itineraryShoppingItem;
  }

  private List<Location> provideLocations() {
    Location barcelona = new Location();
    barcelona.setCityIataCode("BCN");
    barcelona.setCityName("Barcelona");
    barcelona.setGeoNodeId(0);

    Location madrid = new Location();
    madrid.setCityIataCode("MAD");
    madrid.setCityName("Madrid");
    madrid.setGeoNodeId(1);

    List<Location> locations = new ArrayList<>();
    locations.add(barcelona);
    locations.add(madrid);
    return locations;
  }

  public ShoppingCart provideShoppingCart() {
    ShoppingCart shoppingCart = new ShoppingCart();
    List<Traveller> travellers = new ArrayList<>();
    travellers.add(provideTraveller());
    shoppingCart.setBookingId(123456789);
    shoppingCart.setTravellers(travellers);
    shoppingCart.setOtherProductsShoppingItems(provideOtherProductsShoppingItem());
    shoppingCart.setCollectionOptions(provideCollectionOptions());
    shoppingCart.setBuyer(provideBuyer());
    shoppingCart.setTotalPrice(new BigDecimal(123));
    shoppingCart.setPromotionalCodes(providePromotionalCodes());
    shoppingCart.setRequiredTravellerInformation(provideTravellerFieldsList());
    shoppingCart.setRequiredBuyerInformation(provideBuyerFields());
    shoppingCart.setItineraryShoppingItem(provideItineraryShoppingItem());
    shoppingCart.setPrice(provideShoppingCartPriceCalculator());
    shoppingCart.setCollectionTotalFee(new BigDecimal(0.0));
    shoppingCart.setCyberSourceMerchantId("eDreams");
    return shoppingCart;
  }

  private ShoppingCartPriceCalculator provideShoppingCartPriceCalculator() {
    FeeDetails feeDetails = new FeeDetails();
    feeDetails.setDiscount(new BigDecimal(0.0));
    feeDetails.setTax(new BigDecimal(0.0));

    FeeInfo feeInfo = new FeeInfo();
    feeInfo.setPaymentFee(feeDetails);
    feeInfo.setSearchFee(feeDetails);

    ShoppingCartPriceCalculator shoppingCartPriceCalculator = new ShoppingCartPriceCalculator();
    shoppingCartPriceCalculator.setFeeInfo(feeInfo);
    return shoppingCartPriceCalculator;
  }

  private ShoppingCart provideShoppingCartWithoutInsurances() {
    ShoppingCart shoppingCart = new ShoppingCart();
    List<Traveller> travellers = new ArrayList<>();
    travellers.add(provideTraveller());
    shoppingCart.setBookingId(123456789);
    shoppingCart.setTravellers(travellers);
    shoppingCart.setOtherProductsShoppingItems(provideEmptyOtherProductsShoppingItems());
    shoppingCart.setCollectionOptions(provideCollectionOptions());
    shoppingCart.setBuyer(provideBuyer());
    shoppingCart.setTotalPrice(new BigDecimal(123));
    shoppingCart.setPromotionalCodes(providePromotionalCodes());
    shoppingCart.setRequiredTravellerInformation(provideTravellerFieldsList());
    shoppingCart.setRequiredBuyerInformation(provideBuyerFields());
    shoppingCart.setItineraryShoppingItem(provideItineraryShoppingItem());
    return shoppingCart;
  }

  public Traveller provideTraveller() {
    Traveller traveller = new Traveller();
    List<BaggageSelectionResponse> baggageSelectionResponseList = new ArrayList<>();
    baggageSelectionResponseList.add(provideBaggageSelectionResponse());
    traveller.setBaggageSelections(baggageSelectionResponseList);
    return traveller;
  }

  private BaggageSelectionResponse provideBaggageSelectionResponse() {
    BaggageSelectionResponse baggageSelectionResponse = new BaggageSelectionResponse();
    baggageSelectionResponse.setAllowanceChanged(false);
    baggageSelectionResponse.setBaggageApplicationLevel(BaggageApplicationLevel.SEGMENT);
    baggageSelectionResponse.setBaggageDescriptor(new BaggageDescriptor(1, 25));
    baggageSelectionResponse.setBaggageDescriptorIncludedInPrice(new BaggageDescriptor(1, 25));
    baggageSelectionResponse.setPrice(new BigDecimal(0));
    baggageSelectionResponse.setSegmentTypeIndex(SegmentTypeIndex.FIRST);
    return baggageSelectionResponse;
  }

  private ShoppingCart provideShoppingCartWithPromoCodeAdded() {
    ShoppingCart shoppingCart = new ShoppingCart();
    List<Traveller> travellers = new ArrayList<>();
    travellers.add(new Traveller());
    shoppingCart.setBookingId(123456789);
    shoppingCart.setTravellers(travellers);
    shoppingCart.setOtherProductsShoppingItems(provideOtherProductsShoppingItem());
    shoppingCart.setCollectionOptions(provideCollectionOptions());
    shoppingCart.setPromotionalCodes(providePromotionalCodes());
    shoppingCart.setBuyer(provideBuyer());
    return shoppingCart;
  }

  private Buyer provideBuyer() {
    Buyer buyer = new Buyer();
    buyer.setMail("buyer@odigeo.com");
    return buyer;
  }

  private List<PromotionalCodeUsageResponse> providePromotionalCodes() {
    List<PromotionalCodeUsageResponse> promotionalCodeUsageResponses = new ArrayList<>();

    PromotionalCodeUsageResponse promotionalCodeUsageResponse = new PromotionalCodeUsageResponse();
    promotionalCodeUsageResponse.setCode("Code");
    promotionalCodeUsageResponse.setAmount(new BigDecimal(2));
    promotionalCodeUsageResponses.add(promotionalCodeUsageResponse);

    return promotionalCodeUsageResponses;
  }

  public List<ShoppingCartCollectionOption> provideCollectionOptions() {
    List<ShoppingCartCollectionOption> shoppingCartCollectionOptions = new ArrayList<>();
    shoppingCartCollectionOptions.add(provideCreditCardShoppingCartCollectionOption("AE"));
    shoppingCartCollectionOptions.add(provideCreditCardShoppingCartCollectionOption("VI"));
    shoppingCartCollectionOptions.add(provideCreditCardShoppingCartCollectionOption("CA"));
    shoppingCartCollectionOptions.add(provideCreditCardShoppingCartCollectionOption("AX"));
    shoppingCartCollectionOptions.add(provideCreditCardShoppingCartCollectionOption("MD"));
    shoppingCartCollectionOptions.add(provideCreditCardShoppingCartCollectionOption("JC"));
    shoppingCartCollectionOptions.add(provideCreditCardShoppingCartCollectionOption("DC"));
    shoppingCartCollectionOptions.add(provideBankTransferShoppingCartCollectionOption());
    shoppingCartCollectionOptions.add(provideTrustlyShoppingCartCollectionOption());
    shoppingCartCollectionOptions.add(provideKlarnaShoppingCartCollectionOption());
    shoppingCartCollectionOptions.add(providePayPalShoppingCartCollectionOption());
    return shoppingCartCollectionOptions;
  }

  public ShoppingCartCollectionOption provideCreditCardShoppingCartCollectionOption(
      String cardType) {
    ShoppingCartCollectionOption shoppingCartCollectionOption = new ShoppingCartCollectionOption();

    CreditCardType creditCardType = new CreditCardType();
    creditCardType.setCode(cardType);
    creditCardType.setName("Test Card");

    CollectionMethod collectionMethod = new CollectionMethod();
    collectionMethod.setCreditCardType(creditCardType);
    collectionMethod.setType(CollectionMethodType.CREDITCARD);

    shoppingCartCollectionOption.setMethod(collectionMethod);
    shoppingCartCollectionOption.setFee(new BigDecimal(COLLECTION_OPTION_FEE));

    return shoppingCartCollectionOption;
  }

  private ShoppingCartCollectionOption provideBankTransferShoppingCartCollectionOption() {
    ShoppingCartCollectionOption shoppingCartCollectionOption = new ShoppingCartCollectionOption();

    CollectionMethod collectionMethod = new CollectionMethod();
    collectionMethod.setType(CollectionMethodType.BANKTRANSFER);

    shoppingCartCollectionOption.setMethod(collectionMethod);
    shoppingCartCollectionOption.setFee(new BigDecimal(COLLECTION_OPTION_FEE));

    return shoppingCartCollectionOption;
  }

  private ShoppingCartCollectionOption provideKlarnaShoppingCartCollectionOption() {
    ShoppingCartCollectionOption shoppingCartCollectionOption = new ShoppingCartCollectionOption();

    CollectionMethod collectionMethod = new CollectionMethod();
    collectionMethod.setType(CollectionMethodType.KLARNA);

    shoppingCartCollectionOption.setMethod(collectionMethod);
    shoppingCartCollectionOption.setFee(new BigDecimal(COLLECTION_OPTION_FEE));

    return shoppingCartCollectionOption;
  }

  private ShoppingCartCollectionOption provideTrustlyShoppingCartCollectionOption() {
    ShoppingCartCollectionOption shoppingCartCollectionOption = new ShoppingCartCollectionOption();

    CollectionMethod collectionMethod = new CollectionMethod();
    collectionMethod.setType(CollectionMethodType.TRUSTLY);

    shoppingCartCollectionOption.setMethod(collectionMethod);
    shoppingCartCollectionOption.setFee(new BigDecimal(COLLECTION_OPTION_FEE));

    return shoppingCartCollectionOption;
  }

  private ShoppingCartCollectionOption providePayPalShoppingCartCollectionOption() {
    ShoppingCartCollectionOption paypalCollectionOption = new ShoppingCartCollectionOption();
    paypalCollectionOption.setFee(new BigDecimal(COLLECTION_OPTION_FEE));
    CollectionMethod paypalCollectionMethod = new CollectionMethod();
    paypalCollectionMethod.setType(CollectionMethodType.PAYPAL);
    paypalCollectionOption.setMethod(paypalCollectionMethod);
    return paypalCollectionOption;
  }

  public ShoppingCartCollectionOption provideShoppingCartCollectionOptionWithCreditCard(
      String cardType) {
    ShoppingCartCollectionOption shoppingCartCollectionOption = new ShoppingCartCollectionOption();

    CreditCardType creditCardType = new CreditCardType();
    creditCardType.setCode(cardType);

    CollectionMethod collectionMethod = new CollectionMethod();
    collectionMethod.setType(CollectionMethodType.CREDITCARD);
    collectionMethod.setCreditCardType(creditCardType);

    shoppingCartCollectionOption.setMethod(collectionMethod);
    shoppingCartCollectionOption.setFee(new BigDecimal(10));

    return shoppingCartCollectionOption;
  }

  private OtherProductsShoppingItems provideOtherProductsShoppingItem() {
    OtherProductsShoppingItems otherProductsShoppingItems = new OtherProductsShoppingItems();
    otherProductsShoppingItems.setInsuranceShoppingItems(provideListInsuranceShoppingItem());
    return otherProductsShoppingItems;
  }

  private OtherProductsShoppingItems provideEmptyOtherProductsShoppingItems() {
    OtherProductsShoppingItems otherProductsShoppingItems = new OtherProductsShoppingItems();
    otherProductsShoppingItems.setInsuranceShoppingItems(new ArrayList<InsuranceShoppingItem>());
    return otherProductsShoppingItems;
  }

  private List<InsuranceShoppingItem> provideListInsuranceShoppingItem() {
    InsuranceShoppingItem insuranceShoppingItem = new InsuranceShoppingItem();
    insuranceShoppingItem.setInsurance(provideInsurance());
    insuranceShoppingItem.setTotalPrice(new BigDecimal(123));
    insuranceShoppingItem.setId("1");
    insuranceShoppingItem.setSelectable(true);
    List<InsuranceShoppingItem> insuranceShoppingItems = new ArrayList<>();
    insuranceShoppingItems.add(insuranceShoppingItem);
    return insuranceShoppingItems;
  }

  private Insurance provideInsurance() {
    Insurance insurance = new Insurance();
    insurance.setType("C");
    insurance.setPolicy("cnxast152");
    return insurance;
  }

  public BookingResponse provideBookingResponseContract() {
    BookingResponse bookingResponse = new BookingResponse();
    bookingResponse.setStatus(BookingResponseStatus.BOOKING_CONFIRMED);
    bookingResponse.setMarketingRevenue(new ArrayList<MarketingRevenueDataAdapter>());

    BookingDetail bookingDetail = new BookingDetail();
    bookingDetail.setBookingStatus(BookingStatus.CONTRACT);

    Money money = new Money();
    money.setCurrency("EUR");
    bookingDetail.setPrice(money);

    ItineraryProviderBookings itineraryProviderBookings = new ItineraryProviderBookings();
    itineraryProviderBookings.setBookings(new ArrayList<ItineraryProviderBooking>());
    ItinerariesLegend itinerariesLegend = new ItinerariesLegend();
    itineraryProviderBookings.setLegend(itinerariesLegend);
    bookingDetail.setItineraryBookings(itineraryProviderBookings);

    BookingBasicInfo bookingBasicInfo = new BookingBasicInfo();
    bookingBasicInfo.setId(1);
    bookingDetail.setBookingBasicInfo(bookingBasicInfo);

    bookingResponse.setBookingDetail(bookingDetail);

    return bookingResponse;
  }

  public BookingResponse provideBookingResponseRetry() {
    BookingResponse bookingResponse = new BookingResponse();
    bookingResponse.setStatus(BookingResponseStatus.BOOKING_PAYMENT_RETRY);
    bookingResponse.setProcessed(true);
    bookingResponse.setCollectionOptions(provideCollectionOptions());
    return bookingResponse;
  }

  public BookingResponse provideBookingResponseStop() {
    BookingResponse bookingResponse = new BookingResponse();
    bookingResponse.setStatus(BookingResponseStatus.BOOKING_STOP);
    bookingResponse.setProcessed(true);
    return bookingResponse;
  }

  public Booking provideBooking() {
    return MocksProvider.providesBookingMocks().provideBooking();
  }

  public CreditCardBinDetails provideCreditCardBinDetails() {
    CreditCardBinDetails creditCardDetails = new CreditCardBinDetails();
    creditCardDetails.setCreditCardBin("430152");
    creditCardDetails.setCreditCardTypeCode("VI");
    creditCardDetails.setCreditCardNumCountryCode(380);
    creditCardDetails.setCreditCardCountryCode("IT");

    HashMap<String, String> subtypes = new HashMap<>();
    subtypes.put("430152888", "EA");
    creditCardDetails.setCreditCardBinSubtypes(subtypes);

    return creditCardDetails;
  }

  public List<PricingBreakdownStep> providePricingBreakdownStepsWithMembership() {
    List<PricingBreakdownStep> pricingBreakdownSteps =
        providePricingBreakdownStepsWithoutMembership();

    PricingBreakdownItem pricingBreakdownItemMembership =
        provideMembershipPerksPricingBreakdownItem();

    for (PricingBreakdownStep pricingBreakdownStep : pricingBreakdownSteps) {

      pricingBreakdownStep.setTotalPrice(
          BigDecimal.valueOf(123 + pricingBreakdownItemMembership.getPriceItemAmount()
              .doubleValue()));
      pricingBreakdownStep.getPricingBreakdownItems().add(pricingBreakdownItemMembership);
    }

    return pricingBreakdownSteps;
  }

  public PricingBreakdownItem provideMembershipPerksPricingBreakdownItem() {
    PricingBreakdownItem pricingBreakdownItem =
        providePriceBreakdownItem(-13, PricingBreakdownItemType.MEMBERSHIP_PERKS);
    return pricingBreakdownItem;
  }
}