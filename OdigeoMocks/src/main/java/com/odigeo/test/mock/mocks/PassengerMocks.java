package com.odigeo.test.mock.mocks;

import com.odigeo.data.entity.shoppingCart.BuyerIdentificationType;
import com.odigeo.data.entity.shoppingCart.BuyerRequiredFields;
import com.odigeo.data.entity.shoppingCart.RequiredField;
import com.odigeo.data.entity.shoppingCart.TravellerRequiredFields;
import com.odigeo.data.entity.shoppingCart.TravellerType;
import com.odigeo.data.entity.userData.UserAddress;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.ArrayList;
import java.util.List;

public class PassengerMocks {

  public UserTraveller provideUserTravellerWithDataNoProfile() {
    return new UserTraveller((long) 1, (long) 12, false, "javier.rebollo@odigeo.com",
        UserTraveller.TypeOfTraveller.ADULT, "MealType1", (long) 13);
  }

  public List<UserTraveller> provideEmptyUserTravellerListWithDefaultMock() {
    List<UserTraveller> localContactsEmpty = new ArrayList<>();
    localContactsEmpty.add(provideEmptyContactUserTraveller("EMPTY NAME", 0));
    return localContactsEmpty;
  }

  private UserTraveller provideEmptyContactUserTraveller(String nameEmptyContactDetail,
      int contactId) {
    UserProfile userProfile =
        new UserProfile(-1, -1, null, null, nameEmptyContactDetail, null, null, null, 0, null, null,
            null, null, null, null, null, false, null, null, null, -1);
    return new UserTraveller(0, contactId, false, null, null, null, null, userProfile, 0);
  }

  public List<UserTraveller> provideNonEmptyUserTravellerListMock() {
    List<UserTraveller> localContacts = new ArrayList<>();
    localContacts.add(provideEmptyContactUserTraveller("EMPTY NAME", 0));
    localContacts.add(provideMainUserTraveller());
    return localContacts;
  }

  public BuyerRequiredFields getBuyerRequireFieldsMock() {
    BuyerRequiredFields buyerRequiredFields = new BuyerRequiredFields();

    List<BuyerIdentificationType> identificationTypes = new ArrayList<>();
    BuyerIdentificationType firstBuyerIdentificationType = BuyerIdentificationType.NIE;
    identificationTypes.add(firstBuyerIdentificationType);
    BuyerIdentificationType secondBuyerIdentificationType = BuyerIdentificationType.PASSPORT;
    identificationTypes.add(secondBuyerIdentificationType);
    BuyerIdentificationType thirdBuyerIdentificationType = BuyerIdentificationType.NIF;
    identificationTypes.add(thirdBuyerIdentificationType);
    buyerRequiredFields.setIdentificationTypes(identificationTypes);

    buyerRequiredFields.setNeedsAddress(RequiredField.MANDATORY);
    buyerRequiredFields.setNeedsAlternativePhoneNumber(RequiredField.MANDATORY);
    buyerRequiredFields.setNeedsCityName(RequiredField.MANDATORY);
    buyerRequiredFields.setNeedsCountryCode(RequiredField.MANDATORY);
    buyerRequiredFields.setNeedsCpf(RequiredField.NOT_REQUIRED);
    buyerRequiredFields.setNeedsDateOfBirth(RequiredField.NOT_REQUIRED);
    buyerRequiredFields.setNeedsIdentification(RequiredField.OPTIONAL);
    buyerRequiredFields.setNeedsLastNames(RequiredField.MANDATORY);
    buyerRequiredFields.setNeedsMail(RequiredField.MANDATORY);
    buyerRequiredFields.setNeedsName(RequiredField.MANDATORY);
    buyerRequiredFields.setNeedsPhoneNumber(RequiredField.MANDATORY);
    buyerRequiredFields.setNeedsStateName(RequiredField.MANDATORY);
    buyerRequiredFields.setNeedsZipCode(RequiredField.MANDATORY);
    return buyerRequiredFields;
  }

  public List<TravellerRequiredFields> getRequiredTravellerInformationOneAdult() {
    List<TravellerRequiredFields> travellerRequiredFieldsList = new ArrayList<>();
    travellerRequiredFieldsList.add(provideTravellerRequiredFieldsAdult());
    return travellerRequiredFieldsList;
  }

  public List<TravellerRequiredFields> getRequiredTravellerInformationTwoAdults() {
    List<TravellerRequiredFields> travellerRequiredFieldsList = new ArrayList<>();
    travellerRequiredFieldsList.add(provideTravellerRequiredFieldsAdult());
    travellerRequiredFieldsList.add(provideTravellerRequiredFieldsAdult());
    return travellerRequiredFieldsList;
  }

  public List<TravellerRequiredFields> getRequiredTravellerInformationOneAdultAndOneChild() {
    List<TravellerRequiredFields> travellerRequiredFieldsList = new ArrayList<>();
    travellerRequiredFieldsList.add(provideTravellerRequiredFieldsAdult());
    travellerRequiredFieldsList.add(provideTravellerRequiredFieldsChild());
    return travellerRequiredFieldsList;
  }

  private TravellerRequiredFields provideTravellerRequiredFieldsAdult() {
    TravellerRequiredFields travellerRequiredFields = new TravellerRequiredFields();
    travellerRequiredFields.setNeedsTitle(RequiredField.MANDATORY);
    travellerRequiredFields.setNeedsName(RequiredField.MANDATORY);
    travellerRequiredFields.setNeedsNationality(RequiredField.MANDATORY);
    travellerRequiredFields.setTravellerType(TravellerType.ADULT);
    return travellerRequiredFields;
  }

  private TravellerRequiredFields provideTravellerRequiredFieldsChild() {
    TravellerRequiredFields travellerRequiredFields = new TravellerRequiredFields();
    travellerRequiredFields.setNeedsTitle(RequiredField.MANDATORY);
    travellerRequiredFields.setNeedsName(RequiredField.MANDATORY);
    travellerRequiredFields.setNeedsNationality(RequiredField.MANDATORY);
    travellerRequiredFields.setTravellerType(TravellerType.INFANT);
    return travellerRequiredFields;
  }
  
  public UserTraveller provideMainUserTraveller() {
    UserAddress userAddress = provideUserAddress();
    UserProfile userProfile =
        new UserProfile(0, 1, "MALE", UserProfile.Title.MR, "James", null, "Toca", "Munoz", 0, "ES",
            "9566311212", null, null, null, null, null, true, null, userAddress, null, -1);
    return new UserTraveller(0, 1, false, "test@test.com", UserTraveller.TypeOfTraveller.ADULT,
        null, null, userProfile, 0);
  }

  public UserTraveller provideUserTraveller() {
    UserAddress userAddress = provideUserAddress();
    UserProfile userProfile =
        new UserProfile(1, 2, "MALE", UserProfile.Title.MR, "Carlos", null, "Cordero", "Munoz", 0,
            "ES",
            "9566311212", null, null, null, null, null, false, null, userAddress, null, -1);
    return new UserTraveller(1, 2, false, "test@test.com", UserTraveller.TypeOfTraveller.ADULT,
        null, null, userProfile, 1);
  }

  private UserAddress provideUserAddress() {
    return new UserAddress(0, 0, "Avenida virgen del carmen", "ST.", "Madrid", "ES", "Madrid", "28013",
        false, null, 1);
  }
}
