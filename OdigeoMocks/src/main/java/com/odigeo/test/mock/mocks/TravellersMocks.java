package com.odigeo.test.mock.mocks;

import com.odigeo.data.entity.shoppingCart.Traveller;
import java.util.ArrayList;
import java.util.List;

public class TravellersMocks {

  public List<Traveller> provideMockedTravellers(String name, String firstLastName, String
      secondLastName) {
    List<Traveller> travellerList = new ArrayList<>();
    Traveller traveller = new Traveller();
    traveller.setName(name);
    traveller.setFirstLastName(firstLastName);
    traveller.setSecondLastName(secondLastName);
    travellerList.add(traveller);
    return travellerList;
  }
}
