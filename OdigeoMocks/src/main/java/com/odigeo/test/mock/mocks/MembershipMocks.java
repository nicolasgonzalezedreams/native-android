package com.odigeo.test.mock.mocks;

import com.odigeo.data.entity.userData.Membership;

public class MembershipMocks {

  public Membership provideMockedMembership(String name, String lastnames) {
    Membership membership = new Membership();
    membership.setWebsite("ES");
    membership.setFirstName(name);
    membership.setLastNames(lastnames);

    return membership;
  }
}
