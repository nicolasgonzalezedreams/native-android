package com.odigeo.test.mock.mocks;

import com.odigeo.data.entity.shoppingCart.CollectionMethod;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CollectionOptionsMocks {

  private static double COLLECTION_OPTION_FEE = 3.0;
  private static ArrayList<ShoppingCartCollectionOption> mShoppingCartCollectionOptions;

  public static List<ShoppingCartCollectionOption> provideCollectionOptions() {
    mShoppingCartCollectionOptions = new ArrayList<>();

    ShoppingCartCollectionOption paypalCollectionOption = new ShoppingCartCollectionOption();
    paypalCollectionOption.setFee(new BigDecimal(COLLECTION_OPTION_FEE));
    CollectionMethod paypalCollectionMethod = new CollectionMethod();
    paypalCollectionMethod.setType(CollectionMethodType.PAYPAL);
    paypalCollectionOption.setMethod(paypalCollectionMethod);
    mShoppingCartCollectionOptions.add(paypalCollectionOption);

    ShoppingCartCollectionOption bankTransferCollectionOption = new ShoppingCartCollectionOption();
    CollectionMethod banktransferCollectionMethod = new CollectionMethod();
    banktransferCollectionMethod.setType(CollectionMethodType.BANKTRANSFER);
    bankTransferCollectionOption.setMethod(banktransferCollectionMethod);
    bankTransferCollectionOption.setFee(new BigDecimal(COLLECTION_OPTION_FEE));
    mShoppingCartCollectionOptions.add(bankTransferCollectionOption);
    return mShoppingCartCollectionOptions;
  }
}
