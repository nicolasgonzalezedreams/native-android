package com.odigeo.test.mock.mocks;

import com.odigeo.data.entity.booking.SearchSegment;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.entity.userData.CreditCard;
import com.odigeo.data.entity.userData.Membership;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.entity.userData.UserAddress;
import com.odigeo.data.entity.userData.UserAirlinePreferences;
import com.odigeo.data.entity.userData.UserAirportPreferences;
import com.odigeo.data.entity.userData.UserDestinationPreferences;
import com.odigeo.data.entity.userData.UserDevice;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.data.entity.userData.UserLogin;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserTraveller;

import java.util.ArrayList;
import java.util.List;

public class UserMocks {

  public static long GENERIC_ID = 12314l;
  public static long USER_ID = 8574l;
  public static String EMAIL = "email@email.com";
  public static String WEBSITE = "ES_es";
  public static boolean ACCEPTS_NEWSLETTER = true;
  public static boolean ACCEPTS_PARTNERS_INFO = true;
  public static long CREATION_DATE = 2432l;
  public static long LAST_MODIFIED = 12342l;
  public static String LOCALE = "ES";
  public static String MARKETING_PORTAL = "portal";
  public static User.Source SOURCE = User.Source.REGISTERED;
  public static User.Status STATUS = User.Status.ACTIVE;
  public static String NAME = "Name";
  public static String SURNAME = "Surname";
  public static long MEMBER_ID = 54345l;
  public static String WEBSITE_MEMBERSHIP = "ES_es";
  public static String PASSWORD = "123456a";
  public static String ACCESS_TOKEN = "gfdar83425tfsgfgfdt43";
  public static long EXPIRATION_TOKEN_DATE = 3254385435l;
  public static String HASHCODE = "45frew42343fer4r";
  public static String ACTIVATION_CODE = "ACTIVATIONCODE";
  public static long ACTIVATION_CODE_DATE = 143254352l;
  public static long LOGIN_ATTEMPT_FAILED = 12432545543l;
  public static long LAST_LOGIN_DATE = 145435455l;
  public static String REFRESH_TOKEN = "4t34fw214543t";
  public static String MEAL_TYPE = "type";
  public static long BIRTHDATE = 1432424l;
  public static String PREFIX_PHONENUMBER = "+34";
  public static String MOBILE_PHONENUMBER = "4324235432";
  public static String NATIONALITY_COUNTRY_CODE = "ES";
  public static String CPF = "432344";
  public static boolean IS_DEFAULT_TRAVELLER = true;
  public static String GENDER = "male";
  public static long USER_TRAVELLER_ID = 4322434l;
  public static long USER_PROFILE_ID = 21312321l;
  public static long USER_ADDRESS_ID = 4324324l;
  public static long USER_IDENTIFICATION_ID = 3432432l;
  public static long STORED_SEARCH_ID = 1231432l;
  public static String ADDRESS = "Address";
  public static String ADDRESS_TYPE = "Calle";
  public static String CITY = "Barcelona";
  public static String COUNTRY = "Spain";
  public static String STATE = "";
  public static String POSTAL_CODE = "4324523";
  public static boolean IS_PRIMARY = true;
  public static String ALIAS = "Alias";
  public static String IDENTIFICATION_ID = "3242342343J";
  public static long IDENTIFICATION_EXPIRATION = 432542323l;
  public static String IATA_DEPARTURE = "BCN";
  public static String IATA_RETURN = "MAD";
  public static String PROFILE_URL = "http://url.com";
  public static long USER_LOGIN_ID = 2;
  public static int NUM_ADULTS = 1;
  public static int NUM_CHILDREN = 0;
  public static int NUM_INFANTS = 0;
  public static long SEARCH_SEGMENT_ID_1 = 1;
  public static long SEARCH_SEGMENT_ID_2 = 2;
  public static long DEPARTURE_DATE = 3432432432l;
  public static String MIDDLE_NAME = "";
  public static String SECOND_LAST_NAME = "";
  public static String PHONE_NUMBER = "";
  public static String PREFIX_ALTERNATE_PHONE_NUMBER = "";
  public static String ALTERNATE_PHONE_NUMBER = "";
  public static String PHOTO_URI = "";
  public static boolean IS_BUYER = true;
  public static boolean STORED_SEARCH_IS_DIRECT = true;
  public static boolean STORED_SEARCH_IS_SYNC = true;
  public static String IATA_1 = "MAD";
  public static String IATA_2 = "BCN";
  public static String AIRLINE_CODE_1 = "IBI";
  public static String AIRLINE_CODE_2 = "RYA";

  private static int RANDOM_INT = 2;

  public static List<UserAirlinePreferences> provideUserAirlinePreferences() {
    List<UserAirlinePreferences> preferencesList = new ArrayList<>();
    UserAirlinePreferences preferences;
    long id, airlinePreferenceId;

    for (int i = 1; i < 3; ++i) {
      id = i;
      airlinePreferenceId = i * RANDOM_INT;
      preferences =
          new UserAirlinePreferences(id, airlinePreferenceId, provideAirlinesCode(i), USER_ID);
      preferencesList.add(preferences);
    }

    return preferencesList;
  }

  public static List<UserAirportPreferences> provideUserAirportPreferences() {
    List<UserAirportPreferences> preferencesList = new ArrayList<>();
    UserAirportPreferences preferences;
    long id, airportPreferenceId;

    for (int i = 1; i < 3; ++i) {
      id = i;
      airportPreferenceId = i * RANDOM_INT;
      preferences =
          new UserAirportPreferences(id, airportPreferenceId, provideCitiesIata(i), USER_ID);
      preferencesList.add(preferences);
    }

    return preferencesList;
  }

  public static List<UserDestinationPreferences> provideUserDestinationPreferences() {
    List<UserDestinationPreferences> preferencesList = new ArrayList<>();
    UserDestinationPreferences preferences;
    long id, destinationPreferenceId;

    for (int i = 1; i < 3; ++i) {
      id = i;
      destinationPreferenceId = i * RANDOM_INT;
      preferences =
          new UserDestinationPreferences(id, destinationPreferenceId, provideCitiesIata(i),
              USER_ID);
      preferencesList.add(preferences);
    }

    return preferencesList;
  }

  public static List<UserDevice> provideUserDevices() {
    List<UserDevice> deviceList = new ArrayList<>();
    UserDevice userDevice;
    long id, userDeviceId;
    String deviceId, deviceName, alias;
    boolean isPrimary;

    for (int i = 1; i < 3; ++i) {
      id = i;
      userDeviceId = i * RANDOM_INT;
      deviceId = i * RANDOM_INT + "id";
      deviceName = "DEVICE" + i;
      alias = "ALIAS" + i;
      isPrimary = i % RANDOM_INT == 0;
      userDevice =
          new UserDevice(id, userDeviceId, deviceName, deviceId, isPrimary, alias, USER_ID);
      deviceList.add(userDevice);
    }

    return deviceList;
  }

  public static UserLogin provideDefaultUserLogin() {
    UserLogin userLogin =
        new UserLogin(GENERIC_ID, USER_LOGIN_ID, PASSWORD, ACCESS_TOKEN, EXPIRATION_TOKEN_DATE,
            HASHCODE, ACTIVATION_CODE, ACTIVATION_CODE_DATE, LOGIN_ATTEMPT_FAILED, LAST_LOGIN_DATE,
            REFRESH_TOKEN, USER_ID);

    return userLogin;
  }

  public static List<UserTraveller> provideDefaultUserTravellers() {
    List<UserTraveller> userTravellerList = new ArrayList<>();
    UserTraveller userTraveller = new UserTraveller(GENERIC_ID, USER_TRAVELLER_ID, IS_BUYER, EMAIL,
        UserTraveller.TypeOfTraveller.ADULT, MEAL_TYPE, new ArrayList<UserFrequentFlyer>(),
        provideDefaultUserProfile(), USER_ID);
    userTravellerList.add(userTraveller);
    return userTravellerList;
  }

  public static UserProfile provideDefaultUserProfile() {

    UserProfile userProfile =
        new UserProfile(GENERIC_ID, USER_PROFILE_ID, GENDER, UserProfile.Title.MR, NAME,
            MIDDLE_NAME, SURNAME, SECOND_LAST_NAME, BIRTHDATE, PREFIX_PHONENUMBER, PHONE_NUMBER,
            PREFIX_ALTERNATE_PHONE_NUMBER, ALTERNATE_PHONE_NUMBER, MOBILE_PHONENUMBER,
            NATIONALITY_COUNTRY_CODE, CPF, IS_DEFAULT_TRAVELLER, PHOTO_URI,
            provideDefaultUserAddress(), provideDefaultUserIdentifications(), USER_TRAVELLER_ID);

    return userProfile;
  }

  public static UserAddress provideDefaultUserAddress() {
    UserAddress userAddress =
        new UserAddress(GENERIC_ID, USER_ADDRESS_ID, ADDRESS, ADDRESS_TYPE, CITY, COUNTRY, STATE,
            POSTAL_CODE, IS_PRIMARY, ALIAS, USER_PROFILE_ID);
    return userAddress;
  }

  public static List<UserIdentification> provideDefaultUserIdentifications() {
    List<UserIdentification> userIdentificationList = new ArrayList<>();
    UserIdentification userIdentification =
        new UserIdentification(GENERIC_ID, USER_IDENTIFICATION_ID, IDENTIFICATION_ID, COUNTRY,
            IDENTIFICATION_EXPIRATION, UserIdentification.IdentificationType.NIF, USER_PROFILE_ID);
    userIdentificationList.add(userIdentification);
    return userIdentificationList;
  }

  public static List<StoredSearch> provideDefaultStoredSearches() {
    List<StoredSearch> storedSearchList = new ArrayList<>();
    StoredSearch storedSearch =
        new StoredSearch(GENERIC_ID, STORED_SEARCH_ID, NUM_ADULTS, NUM_CHILDREN, NUM_INFANTS,
            StoredSearch.CabinClass.TOURIST, STORED_SEARCH_IS_DIRECT, StoredSearch.TripType.R,
            STORED_SEARCH_IS_SYNC, provideDefaultSearchSegments(), CREATION_DATE, USER_ID);

    storedSearchList.add(storedSearch);
    return storedSearchList;
  }

  public static List<SearchSegment> provideDefaultSearchSegments() {
    List<SearchSegment> searchSegmentList = new ArrayList<>();
    long searchSegmentOrder = 1;

    SearchSegment searchSegmentDep =
        new SearchSegment(GENERIC_ID, SEARCH_SEGMENT_ID_1, IATA_DEPARTURE, IATA_RETURN,
            DEPARTURE_DATE, searchSegmentOrder);

    ++searchSegmentOrder;

    SearchSegment searchSegmentRet =
        new SearchSegment(GENERIC_ID, SEARCH_SEGMENT_ID_2, IATA_RETURN, IATA_DEPARTURE,
            DEPARTURE_DATE, searchSegmentOrder);

    searchSegmentList.add(searchSegmentDep);
    searchSegmentList.add(searchSegmentRet);

    return searchSegmentList;
  }

  public static List<CreditCard> provideDefaultCreditCards() {
    return new ArrayList<>();
  }

  public static List<Membership> provideDefaultMemberships() {
    List<Membership> membershipList = new ArrayList<>();
    Membership membership = new Membership();
    membership.setFirstName(NAME);
    membership.setLastNames(SURNAME);
    membership.setMemberId(MEMBER_ID);
    membership.setWebsite(WEBSITE_MEMBERSHIP);
    membershipList.add(membership);

    return membershipList;
  }

  private static String provideCitiesIata(int seed) {
    if (seed % 2 == 0) return IATA_1;
    return IATA_2;
  }

  private static String provideAirlinesCode(int seed) {
    if (seed % 2 == 0) return AIRLINE_CODE_1;
    return AIRLINE_CODE_2;
  }

  public static class Builder {
    private List<UserAirlinePreferences> airlinePreferences;
    private List<UserAirportPreferences> airportPreferences;
    private List<UserDestinationPreferences> userDestinationPreferences;
    private List<UserDevice> userDevices;
    private UserLogin userLogin;
    private List<UserTraveller> userTravellers;
    private List<StoredSearch> storedSearches;
    private List<CreditCard> creditCards;
    private List<Membership> memberships;

    public Builder() {
      airlinePreferences = provideUserAirlinePreferences();
      airportPreferences = provideUserAirportPreferences();
      userDestinationPreferences = provideUserDestinationPreferences();
      userDevices = provideUserDevices();
      userLogin = provideDefaultUserLogin();
      userTravellers = provideDefaultUserTravellers();
      storedSearches = provideDefaultStoredSearches();
      creditCards = provideDefaultCreditCards();
      memberships = provideDefaultMemberships();
    }

    public Builder setAirlinePreferences(List<UserAirlinePreferences> airlinePreferences) {
      this.airlinePreferences = airlinePreferences;
      return this;
    }

    public Builder setUserAirportPreferences(List<UserAirportPreferences> airportPreferences) {
      this.airportPreferences = airportPreferences;
      return this;
    }

    public Builder setUserDestinationPreferences(
        List<UserDestinationPreferences> destinationPreferences) {
      this.userDestinationPreferences = destinationPreferences;
      return this;
    }

    public Builder setUserDevices(List<UserDevice> userDevices) {
      this.userDevices = userDevices;
      return this;
    }

    public Builder setUserLogin(UserLogin userLogin) {
      this.userLogin = userLogin;
      return this;
    }

    public Builder setUserTravellers(List<UserTraveller> userTravellers) {
      this.userTravellers = userTravellers;
      return this;
    }

    public Builder setStoredSearches(List<StoredSearch> storedSearches) {
      this.storedSearches = storedSearches;
      return this;
    }

    public Builder setCreditCards(List<CreditCard> creditCards) {
      this.creditCards = creditCards;
      return this;
    }

    public Builder setMembershipInfo(List<Membership> memberships) {
      this.memberships = memberships;
      return this;
    }

    public User build() {
      return new User(GENERIC_ID, USER_ID, EMAIL, WEBSITE, ACCEPTS_NEWSLETTER,
          ACCEPTS_PARTNERS_INFO, CREATION_DATE, LAST_MODIFIED, LOCALE, MARKETING_PORTAL, SOURCE,
          STATUS, airlinePreferences, airportPreferences, userDestinationPreferences, userDevices,
          userLogin, userTravellers, storedSearches, creditCards, memberships);
    }
  }
}
