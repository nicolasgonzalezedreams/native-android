package com.odigeo.test.mock.mocks;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartRequestModel;
import com.odigeo.data.entity.shoppingCart.ExtensionRequest;
import com.odigeo.data.entity.shoppingCart.ItinerarySelectionRequest;
import com.odigeo.data.entity.shoppingCart.ItinerarySortCriteria;
import java.util.ArrayList;
import java.util.List;

public class CreateShoppingCartRequestModelMocks {

  public CreateShoppingCartRequestModel provideCreateShoppingCartRequestModelMock(
      boolean isMember) {
    CreateShoppingCartRequestModel createShoppingCartRequestModel =
        new CreateShoppingCartRequestModel();

    createShoppingCartRequestModel.setSearchId(33424323);
    createShoppingCartRequestModel.setExtensions(proviceExtensionRequests());
    createShoppingCartRequestModel.setItinerarySelectionRequest(
        proviceItinerarySelectionRequest(isMember));

    return createShoppingCartRequestModel;
  }

  private ItinerarySelectionRequest proviceItinerarySelectionRequest(boolean isMember) {
    ItinerarySelectionRequest itinerarySelectionRequest = new ItinerarySelectionRequest();

    itinerarySelectionRequest.setExtensions(proviceExtensionRequests());
    itinerarySelectionRequest.setSortCriteria(
        isMember ? ItinerarySortCriteria.MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE
            : ItinerarySortCriteria.MINIMUM_PURCHASABLE_PRICE);
    itinerarySelectionRequest.setFareItineraryKey("fareitinerarykey");
    itinerarySelectionRequest.setSegmentKeys(provideSegmentKeys());

    return itinerarySelectionRequest;
  }

  private List<String> provideSegmentKeys() {
    List<String> segmentKeys = new ArrayList<>();
    segmentKeys.add("SegmentKey1");
    segmentKeys.add("SegmentKey2");
    return segmentKeys;
  }

  private List<ExtensionRequest> proviceExtensionRequests() {
    List<ExtensionRequest> extensionRequests = new ArrayList<>();

    ExtensionRequest extensionRequest1 = new ExtensionRequest("Name1", "Value1");
    ExtensionRequest extensionRequest2 = new ExtensionRequest("Name2", "Name2");

    extensionRequests.add(extensionRequest1);
    extensionRequests.add(extensionRequest2);

    return extensionRequests;
  }
}
