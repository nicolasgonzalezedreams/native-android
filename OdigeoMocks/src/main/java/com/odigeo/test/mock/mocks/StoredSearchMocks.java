package com.odigeo.test.mock.mocks;

import com.odigeo.data.entity.booking.SearchSegment;
import com.odigeo.data.entity.booking.StoredSearch;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Javier Marsicano on 25/01/17.
 */

public class StoredSearchMocks {
  private static final int NUM_INFANTS = 1;
  private static final int NUM_CHILDREN = 2;
  private static final int NUM_ADULTS = 3;
  private static final int USER_ID = 0;
  private static final long CREATION_DATE = 147000000L;

  public StoredSearch provideStoredSearchWithSegment() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());
    calendar.add(Calendar.MONTH, 1);

    SearchSegment searchSegment =
        new SearchSegment(1, 1, "MAD", "PAR", calendar.getTimeInMillis(), 0, 1);
    List<SearchSegment> segments = new ArrayList<>();
    segments.add(searchSegment);

    StoredSearch storedSearch =
        new StoredSearch(1, 1, 1, 0, 0, StoredSearch.CabinClass.BUSINESS, false,
            StoredSearch.TripType.O, false, segments, 1483228800000l, 0);
    return storedSearch;
  }

  public StoredSearch provideStoredSearchNotSync() {
    return new StoredSearch(1, 1, NUM_ADULTS, NUM_CHILDREN, NUM_INFANTS, true,
        StoredSearch.CabinClass.BUSINESS, StoredSearch.TripType.M, false, CREATION_DATE, USER_ID);
  }

  public StoredSearch provideSyncStoredSearch() {
    return new StoredSearch(2, 5555, NUM_ADULTS, NUM_CHILDREN, NUM_INFANTS, true,
        StoredSearch.CabinClass.TOURIST, StoredSearch.TripType.O, true, CREATION_DATE, USER_ID);
  }

  public StoredSearch provideStoredSearchPastDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());
    calendar.add(Calendar.MONTH, -1);

    SearchSegment searchSegment =
        new SearchSegment(1, 1, "MAD", "PAR", calendar.getTimeInMillis(), 0, 1);
    List<SearchSegment> segments = new ArrayList<>();
    segments.add(searchSegment);

    StoredSearch storedSearch =
        new StoredSearch(1, 1, 1, 0, 0, StoredSearch.CabinClass.BUSINESS, false,
            StoredSearch.TripType.O, false, segments, 1483228800000l, 0);
    return storedSearch;
  }
}
