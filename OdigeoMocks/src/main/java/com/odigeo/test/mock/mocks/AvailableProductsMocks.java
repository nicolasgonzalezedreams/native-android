package com.odigeo.test.mock.mocks;

import com.odigeo.data.entity.shoppingCart.AvailableProductsResponse;
import com.odigeo.data.entity.shoppingCart.Insurance;
import com.odigeo.data.entity.shoppingCart.InsuranceOffer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Javier Marsicano on 21/02/17.
 */

public class AvailableProductsMocks {
  public AvailableProductsResponse provideAvailableProducts() {
    AvailableProductsResponse availableProductsResponse = new AvailableProductsResponse();
    List<InsuranceOffer> insuranceOffers = new ArrayList<>();
    InsuranceOffer insuranceOffer = new InsuranceOffer();
    insuranceOffer.setTotalPrice(new BigDecimal(777));
    insuranceOffer.setId("ID mock");
    Insurance insurance = new Insurance();
    insurance.setPolicy("Policy mock");
    insurance.setConditionAcceptance("Conditions acceptance mock text lorem ipsum");
    List<Insurance> insuranceList = new ArrayList<>();
    insuranceList.add(insurance);
    insuranceOffer.setInsurances(insuranceList);
    insuranceOffers.add(insuranceOffer);
    availableProductsResponse.setInsuranceOffers(insuranceOffers);
    return availableProductsResponse;
  }
}
