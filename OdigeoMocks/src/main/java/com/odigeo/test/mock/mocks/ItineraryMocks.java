package com.odigeo.test.mock.mocks;

import com.odigeo.data.entity.shoppingCart.Carrier;
import com.odigeo.data.entity.shoppingCart.ItinerariesLegend;
import com.odigeo.data.entity.shoppingCart.Itinerary;
import com.odigeo.data.entity.shoppingCart.Location;
import com.odigeo.data.entity.shoppingCart.Section;
import com.odigeo.data.entity.shoppingCart.SectionResult;
import com.odigeo.data.entity.shoppingCart.Segment;
import com.odigeo.data.entity.shoppingCart.SegmentResult;
import java.util.ArrayList;
import java.util.List;

public class ItineraryMocks {
  private Itinerary mItinerary;

  public ItineraryMocks() {
    initMock();
  }

  private void initMock() {
    mItinerary = new Itinerary();

    ItinerariesLegend itinerariesLegend = new ItinerariesLegend();
    itinerariesLegend.setLocations(buildLocationList());
    itinerariesLegend.setSegmentResults(buildSegmentResultList());
    itinerariesLegend.setSectionResults(buildSectionResultList());
    itinerariesLegend.setCarriers(buildCarriersResultList());

    mItinerary.setLegend(itinerariesLegend);
  }

  private List<Carrier> buildCarriersResultList() {
    Carrier carrier = new Carrier("RY", "Ryanair");
    List<Carrier> carriers = new ArrayList<>();
    carriers.add(carrier);
    return carriers;
  }

  private List<Location> buildLocationList() {
    Location arrivalLocation = new Location();
    arrivalLocation.setGeoNodeId(0);
    arrivalLocation.setCityName("Sevilla");

    Location departureLocation = new Location();
    departureLocation.setGeoNodeId(1);
    departureLocation.setCityName("Madrid");

    List<Location> locationArrayList = new ArrayList<>();
    locationArrayList.add(arrivalLocation);
    locationArrayList.add(departureLocation);

    return locationArrayList;
  }

  private List<SegmentResult> buildSegmentResultList() {
    List<Integer> sectionIdArrayList = new ArrayList<>();
    sectionIdArrayList.add(0);

    Segment segment = new Segment();
    segment.setSections(sectionIdArrayList);

    SegmentResult segmentResult = new SegmentResult();
    segmentResult.setSegment(segment);

    List<SegmentResult> segmentResultArrayList = new ArrayList<>();
    segmentResultArrayList.add(segmentResult);
    return segmentResultArrayList;
  }

  private List<SectionResult> buildSectionResultList() {
    Section section = new Section();
    section.setFrom(0);
    section.setTo(1);
    section.setCarrier(0);

    SectionResult sectionResult = new SectionResult();
    sectionResult.setId(0);
    sectionResult.setSection(section);

    List<SectionResult> sectionResultArrayList = new ArrayList<>();
    sectionResultArrayList.add(sectionResult);
    return sectionResultArrayList;
  }

  public Itinerary getItinerary() {
    return mItinerary;
  }
}
