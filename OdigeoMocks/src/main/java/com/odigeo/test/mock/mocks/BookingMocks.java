package com.odigeo.test.mock.mocks;

import com.odigeo.data.entity.booking.Baggage;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Buyer;
import com.odigeo.data.entity.booking.Carrier;
import com.odigeo.data.entity.booking.FlightStats;
import com.odigeo.data.entity.booking.Insurance;
import com.odigeo.data.entity.booking.ItineraryBooking;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.entity.booking.Traveller;
import java.util.ArrayList;
import java.util.List;

public class BookingMocks {
  private static final long BOOKING_ID = 1000;
  private static final long TEN_MINUTES_IN_MILLIS = 1000 * 60 * 10;

  public Booking provideBooking() {
    Booking booking = new Booking();
    booking.setBookingId(BOOKING_ID);
    booking.setBookingStatus(Booking.BOOKING_STATUS_INIT);
    booking.setTripType(Booking.TRIP_TYPE_ONE_WAY);
    booking.setBuyer(provideBuyer());
    booking.setSegments(new ArrayList<Segment>());
    booking.setInsurances(new ArrayList<Insurance>());
    ArrayList<Traveller> travellers = new ArrayList<>();
    travellers.add(provideTraveller());
    booking.setTravellers(travellers);
    ArrayList<Segment> segments = new ArrayList<>();
    segments.add(0, provideSegment());
    booking.setSegments(segments);
    booking.setArrivalAirportCode("MNL");
    booking.setArrivalLastLeg(System.currentTimeMillis() + TEN_MINUTES_IN_MILLIS);
    return booking;
  }

  public Booking provideRoundBooking() {
    Booking booking = provideBooking();

    List<Segment> segments = booking.getSegments();
    Segment segment = provideSegment();

    segments.add(segment);
    booking.setSegments(segments);
    booking.setTripType(Booking.TRIP_TYPE_ROUND_TRIP);
    return booking;
  }

  public Booking providePastBooking() {
    Booking booking = provideBooking();
    booking.setArrivalLastLeg(System.currentTimeMillis() - TEN_MINUTES_IN_MILLIS);
    return booking;
  }

  public Booking provideRoundPastBooking(){
    Booking booking = providePastBooking();

    List<Segment> segments = booking.getSegments();
    Segment segment = provideSegment();

    segments.add(segment);
    booking.setSegments(segments);
    booking.setTripType(Booking.TRIP_TYPE_ROUND_TRIP);
    return booking;
  }

  public Booking provideMultitripPastBooking() {
    Booking booking = provideBooking();
    booking.setArrivalLastLeg(System.currentTimeMillis() - TEN_MINUTES_IN_MILLIS);

    List<Segment> segments = booking.getSegments();
    Segment segment = provideSegment();
    Segment otherSegment = provideSegment();
    List<Section> sectionsList = otherSegment.getSectionsList();
    Section section = sectionsList.get(0);
    LocationBooking locationFrom =
        new LocationBooking(1339, "TYO", "Tokio", "Japón", "", "JP", "Asia/Tokyo", "Narita",
            "Airport", "NRT");
    LocationBooking locationTo =
        new LocationBooking(1240, "MAD", "Madrid", "España", "", "ES", "Europe/Madrid",
            "Aeopuerto Madrid barajas", "Airport", "MAD");

    section.setFrom(locationFrom);
    section.setTo(locationTo);
    sectionsList.add(0,section);
    otherSegment.setSectionsList(sectionsList);

    segments.add(segment);
    segments.add(otherSegment);
    booking.setSegments(segments);

    booking.setTripType(Booking.TRIP_TYPE_MULTI_SEGMENT);

    return booking;
  }

  public Section provideSection() {
    Section section = new Section();
    section.setId(1);
    section.setCarrier(new Carrier(1, "GK", "Jetstar Japan"));
    section.setCabinClass("TOURIST");
    section.setArrivalDate(1480140300000L);
    section.setDepartureDate(1480121400000L);
    section.setDuration(255);
    section.setFlightStats(
        new FlightStats(0, null, 0, null, null, null, null, 0, null, null, null, null, null, null,
            null, null, null, null, null, null, null));
    LocationBooking locationFrom =
        new LocationBooking(1240, "MNL", "Manila", "Filipinas", "", "PH", "Asia/Manila",
            "Ninoy Aquino (Manila) International Airport", "Airport", "MNL");
    LocationBooking locationTo =
        new LocationBooking(1339, "TYO", "Tokio", "Japón", "", "JP", "Asia/Tokyo", "Narita",
            "Airport", "NRT");
    section.setFrom(locationFrom);
    section.setTo(locationTo);
    ArrayList<Section> sections = new ArrayList<>();
    sections.add(0, section);
    ItineraryBooking itineraryBooking =
        new ItineraryBooking(1, Booking.BOOKING_STATUS_INIT, null, 0, sections);
    section.setItineraryBooking(itineraryBooking);
    return section;
  }

  public Segment provideSegment() {
    Segment segment = new Segment(1, 255, 0, 1, BOOKING_ID);
    ArrayList<Section> sections = new ArrayList<>();
    Section section = provideSection();
    section.setSegment(segment);
    sections.add(0, section);
    segment.setSectionsList(sections);
    return segment;
  }

  public Traveller provideTraveller() {
    Traveller traveller =
        new Traveller(0, "", "", "Marsicano", "Javier", 1, "", 0, "", "", "", "", "", "", "",
            BOOKING_ID);
    traveller.setId(1);
    traveller.setBaggageList(new ArrayList<Baggage>());
    return traveller;
  }

  public Buyer provideBuyer() {
    Buyer buyer = new Buyer();
    buyer.setCityName("City");
    buyer.setId(1);
    buyer.setBookingId(BOOKING_ID);
    buyer.setName("Javier");
    buyer.setLastname("Marsicano");
    return buyer;
  }
}
