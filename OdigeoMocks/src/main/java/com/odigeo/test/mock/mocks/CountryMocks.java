package com.odigeo.test.mock.mocks;

import com.odigeo.data.entity.booking.Country;

public class CountryMocks {

  public Country provideCountryMock() {
    Country country = new Country();
    country.setName("Anguila");
    country.setGeoNodeId(11660);
    country.setPhonePrefix("+1 264");
    country.setCountryCode("AI");
    return country;
  }
}
