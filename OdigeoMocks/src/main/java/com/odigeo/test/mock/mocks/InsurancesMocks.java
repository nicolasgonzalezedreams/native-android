package com.odigeo.test.mock.mocks;

import com.odigeo.data.entity.shoppingCart.AvailableProductsResponse;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.FlowConfigurationResponse;
import com.odigeo.data.entity.shoppingCart.Insurance;
import com.odigeo.data.entity.shoppingCart.InsuranceOffer;
import com.odigeo.data.entity.shoppingCart.InsuranceShoppingItem;
import com.odigeo.data.entity.shoppingCart.ItinerarySortCriteria;
import com.odigeo.data.entity.shoppingCart.OtherProductsShoppingItems;
import com.odigeo.data.entity.shoppingCart.PricingBreakdown;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItem;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItemType;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownStep;
import com.odigeo.data.entity.shoppingCart.ShoppingCart;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.shoppingCart.Traveller;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class InsurancesMocks {

  public AvailableProductsResponse provideAvailableProductResponse() {
    AvailableProductsResponse availableProductsResponse = new AvailableProductsResponse();
    availableProductsResponse.setInsuranceOffers(provideListInsuranceOffer());
    return availableProductsResponse;
  }

  public List<InsuranceOffer> provideListInsuranceOffer() {
    List<InsuranceOffer> insuranceOffers = new ArrayList<>();
    insuranceOffers.add(provideCancellationInsuranceOffer());
    insuranceOffers.add(provideCancelationAndAssistanceInsuranceOffer());
    return insuranceOffers;
  }

  private InsuranceOffer provideCancellationInsuranceOffer() {
    InsuranceOffer insuranceOffer = new InsuranceOffer();
    insuranceOffer.setId("1");
    insuranceOffer.setTotalPrice(new BigDecimal(100));
    insuranceOffer.setProviderPrice(new BigDecimal(100));

    List<Insurance> insurances = new ArrayList<>();
    insurances.add(provideCancelationInsurance());

    insuranceOffer.setInsurances(insurances);

    return insuranceOffer;
  }

  private InsuranceOffer provideCancelationAndAssistanceInsuranceOffer() {
    InsuranceOffer insuranceOffer = new InsuranceOffer();
    insuranceOffer.setId("2");
    insuranceOffer.setTotalPrice(new BigDecimal(150));
    insuranceOffer.setProviderPrice(new BigDecimal(150));

    List<Insurance> insurances = new ArrayList<>();
    insurances.add(provideCancelationAndAssistanceInsurance());

    insuranceOffer.setInsurances(insurances);

    return insuranceOffer;
  }

  public Insurance provideCancelationInsurance() {
    Insurance insurance = new Insurance();
    insurance.setType("C");
    insurance.setPolicy("cnxflt151");
    return insurance;
  }

  public Insurance provideCancelationAndAssistanceInsurance() {
    Insurance insurance = new Insurance();
    insurance.setType("CA");
    insurance.setPolicy("cnxast152");
    return insurance;
  }

  public FlowConfigurationResponse provideFlowConfigurationResponse() {
    FlowConfigurationResponse flowConfigurationResponse = new FlowConfigurationResponse();
    flowConfigurationResponse.setInsuranceOptout("C");
    flowConfigurationResponse.setInsuranceRecomended("C");
    return flowConfigurationResponse;
  }

  public FlowConfigurationResponse provideEmptyFLowConfigurationResponse() {
    return new FlowConfigurationResponse();
  }

  public FlowConfigurationResponse provideNonOutputFLowConfigurationResponse() {
    FlowConfigurationResponse flowConfigurationResponse = new FlowConfigurationResponse();
    flowConfigurationResponse.setInsuranceRecomended("C");
    return flowConfigurationResponse;
  }

  public CreateShoppingCartResponse provideCreateShoppingCartResponse() {
    CreateShoppingCartResponse createShoppingCartResponse = new CreateShoppingCartResponse();
    createShoppingCartResponse.setPricingBreakdown(providePricingBreakdown());
    createShoppingCartResponse.setSortCriteria(ItinerarySortCriteria.MINIMUM_PURCHASABLE_PRICE);
    createShoppingCartResponse.setShoppingCart(provideShoppingCart());
    return createShoppingCartResponse;
  }

  public CreateShoppingCartResponse provideCreateShoppingCartResponseWithoutInsurancesShoppinItem() {
    CreateShoppingCartResponse createShoppingCartResponse = new CreateShoppingCartResponse();
    createShoppingCartResponse.setPricingBreakdown(providePricingBreakdown());
    createShoppingCartResponse.setSortCriteria(ItinerarySortCriteria.MINIMUM_PURCHASABLE_PRICE);
    createShoppingCartResponse.setShoppingCart(provideShoppingCartWithoutInsurancesShoppingItems());
    return createShoppingCartResponse;
  }

  private ShoppingCart provideShoppingCart() {
    ShoppingCart shoppingCart = new ShoppingCart();
    List<Traveller> travellers = new ArrayList<>();
    travellers.add(new Traveller());
    shoppingCart.setTravellers(travellers);
    shoppingCart.setOtherProductsShoppingItems(provideOtherProductsShoppingItem());
    return shoppingCart;
  }

  private ShoppingCart provideShoppingCartWithoutInsurancesShoppingItems() {
    ShoppingCart shoppingCart = new ShoppingCart();
    List<Traveller> travellers = new ArrayList<>();
    travellers.add(new Traveller());
    shoppingCart.setTravellers(travellers);
    shoppingCart.setOtherProductsShoppingItems(provideOtherProductsShoppingItemWithOutInsurances());
    return shoppingCart;
  }

  private PricingBreakdown providePricingBreakdown() {
    PricingBreakdownItem pricingBreakdownItem = new PricingBreakdownItem();
    pricingBreakdownItem.setPriceItemAmount(new BigDecimal(123));
    pricingBreakdownItem.setPriceItemType(PricingBreakdownItemType.ADULTS_PRICE);

    List<PricingBreakdownItem> pricingBreakdownItems = new ArrayList<>();
    pricingBreakdownItems.add(pricingBreakdownItem);

    PricingBreakdownStep pricingBreakdownStep = new PricingBreakdownStep();
    pricingBreakdownStep.setStep(Step.INSURANCE);
    pricingBreakdownStep.setTotalPrice(new BigDecimal(123));
    pricingBreakdownStep.setPricingBreakdownItems(pricingBreakdownItems);

    List<PricingBreakdownStep> pricingBreakdownSteps = new ArrayList<>();
    pricingBreakdownSteps.add(pricingBreakdownStep);

    PricingBreakdown pricingBreakdown = new PricingBreakdown();
    pricingBreakdown.setPricingBreakdownSteps(pricingBreakdownSteps);

    return pricingBreakdown;
  }

  private OtherProductsShoppingItems provideOtherProductsShoppingItem() {
    OtherProductsShoppingItems otherProductsShoppingItems = new OtherProductsShoppingItems();
    otherProductsShoppingItems.setInsuranceShoppingItems(provideListInsuranceShoppingItem());
    return otherProductsShoppingItems;
  }

  private OtherProductsShoppingItems provideOtherProductsShoppingItemWithOutInsurances() {
    OtherProductsShoppingItems otherProductsShoppingItems = new OtherProductsShoppingItems();
    List<InsuranceShoppingItem> insuranceShoppingItems = new ArrayList<>();
    otherProductsShoppingItems.setInsuranceShoppingItems(insuranceShoppingItems);
    return otherProductsShoppingItems;
  }

  private List<InsuranceShoppingItem> provideListInsuranceShoppingItem() {

    List<InsuranceShoppingItem> insuranceShoppingItems = new ArrayList<>();
    insuranceShoppingItems.add(provideInsuranceShoppingItem());
    return insuranceShoppingItems;
  }

  public InsuranceShoppingItem provideInsuranceShoppingItem() {
    InsuranceShoppingItem insuranceShoppingItem = new InsuranceShoppingItem();
    insuranceShoppingItem.setInsurance(provideCancelationInsurance());
    insuranceShoppingItem.setTotalPrice(new BigDecimal(123));
    insuranceShoppingItem.setId("1");
    return insuranceShoppingItem;
  }
}