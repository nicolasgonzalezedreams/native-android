package com.odigeo.test.mock;

import com.odigeo.data.entity.userData.Membership;
import com.odigeo.test.mock.mocks.AvailableProductsMocks;
import com.odigeo.test.mock.mocks.BookingMocks;
import com.odigeo.test.mock.mocks.CountryMocks;
import com.odigeo.test.mock.mocks.CreateShoppingCartRequestModelMocks;
import com.odigeo.test.mock.mocks.InsurancesMocks;
import com.odigeo.test.mock.mocks.ItineraryMocks;
import com.odigeo.test.mock.mocks.MembershipMocks;
import com.odigeo.test.mock.mocks.PassengerMocks;
import com.odigeo.test.mock.mocks.PaymentMocks;
import com.odigeo.test.mock.mocks.StoredSearchMocks;
import com.odigeo.test.mock.mocks.TravellersMocks;

public class MocksProvider {
  private static PaymentMocks paymentMocksInstance;
  private static CountryMocks countryMocksInstance;
  private static InsurancesMocks insurancesMocksInstance;
  private static ItineraryMocks itineraryMocksInstance;
  private static PassengerMocks passengerMocksInstance;
  private static StoredSearchMocks storedSearchMocks;
  private static AvailableProductsMocks availableProductsMocks;
  private static BookingMocks bookingMocks;
  private static CreateShoppingCartRequestModelMocks createShoppingCartRequestModelMocks;
  private static MembershipMocks membershipMocks;
  private static TravellersMocks travellersMocks;

  private MocksProvider() {
    // Nothing
  }

  public static PaymentMocks providesPaymentMocks() {
    if (paymentMocksInstance == null) {
      paymentMocksInstance = new PaymentMocks();
    }
    return paymentMocksInstance;
  }

  public static CountryMocks providesCountryMocks() {
    if (countryMocksInstance == null) {
      countryMocksInstance = new CountryMocks();
    }
    return countryMocksInstance;
  }

  public static InsurancesMocks providesInsurancesMocks() {
    if (insurancesMocksInstance == null) {
      insurancesMocksInstance = new InsurancesMocks();
    }
    return insurancesMocksInstance;
  }

  public static ItineraryMocks providesItineraryMocks() {
    if (itineraryMocksInstance == null) {
      itineraryMocksInstance = new ItineraryMocks();
    }
    return itineraryMocksInstance;
  }

  public static PassengerMocks providesPassengerMocks() {
    if (passengerMocksInstance == null) {
      passengerMocksInstance = new PassengerMocks();
    }
    return passengerMocksInstance;
  }

  public static StoredSearchMocks providesStoredSearchMocks() {
    if (storedSearchMocks == null) {
      storedSearchMocks = new StoredSearchMocks();
    }
    return storedSearchMocks;
  }

  public static AvailableProductsMocks providesAvailableProductsMocks() {
    if (availableProductsMocks == null) {
      availableProductsMocks = new AvailableProductsMocks();
    }
    return availableProductsMocks;
  }

  public static BookingMocks providesBookingMocks() {
    if (bookingMocks == null) {
      bookingMocks = new BookingMocks();
    }
    return bookingMocks;
  }

  public static CreateShoppingCartRequestModelMocks provideCreateShoppingCartRequestModelMocks() {
    if (createShoppingCartRequestModelMocks == null) {
      createShoppingCartRequestModelMocks = new CreateShoppingCartRequestModelMocks();
    }
    return createShoppingCartRequestModelMocks;
  }

  public static MembershipMocks provideMembershipMocks() {
    if(membershipMocks == null) {
      membershipMocks = new MembershipMocks();
    }
    return membershipMocks;
  }

  public static TravellersMocks provideTravellersMocks() {
    if(travellersMocks == null) {
      travellersMocks = new TravellersMocks();
    }
    return travellersMocks;
  }
}
