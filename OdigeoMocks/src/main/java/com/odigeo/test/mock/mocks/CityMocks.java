package com.odigeo.test.mock.mocks;

import com.odigeo.data.entity.geo.City;
import com.odigeo.data.entity.geo.LocationDescriptionType;

/**
 * Created by daniel.morales on 11/5/17.
 */

public class CityMocks {

  public static City provideCityWithIATA(String iata) {
    City city = new City();
    city.setCityName(iata + " name");
    city.setDistanceToCurrentLocation(200f);
    city.setType(LocationDescriptionType.CITY);
    city.setCountryName(iata + " country");
    city.setCountryCode(iata);
    city.setGeoNodeId(312454);
    city.setGroupParent(false);
    city.setIataCode(iata);
    city.setName(iata);
    return city;
  }
}
