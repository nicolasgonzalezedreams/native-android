Feature: As a user I want to change the country So that I can change correctly my country

  Scenario Outline: Change country in settings page
    Given Settings page
    When user select <country> country
    Then country selected is <country>

    Examples:
      | country |
      | France  |

#  For this Gherkin has been assumed that the default selected distance units are Miles,
#  so the change is done to Km. It can be done in both ways.
  Scenario: Change distance units
    Given Settings page
    When User change "Distance Settings"
    Then distance units have changed

  Scenario Outline: Change passenger settings
    Given Settings page
    When User select "Passenger Settings"
    And user select <adults> adults, <children> children and <infants> infants
    Then selected users are <adults> adults, <children> children and <infants> infants.

    Examples:
      | adults | children | infants |
      | 2      | 1        | 1       |

  Scenario: Push notifications switch
    Given Settings page
    When User changes Push Notification status
    Then Push notification status has changed