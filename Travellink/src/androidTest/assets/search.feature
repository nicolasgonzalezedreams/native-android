Feature: As a user I want to perform a flight search, so that I can select a flight.

  Scenario Outline: Search flights
    Given Search page
    When User searches <type> flight
    Then Flights appear

    Examples:
      | type      |
      | OW        |
      | Return    |
      | Multistop |

  Scenario Outline: Search flights error
    Given Search page
    When User searches <type> flight
    Then Error page appears

    Examples:
      | type         |
      | Search_error |

  Scenario Outline: Search Return direct flight
    Given Search page
    When User searches <type> flight
    And Select origin and destiny with no direct flights
    And User select only_direct
    Then ReSearch Page appears

    Examples:
      | type          |
      | Return Direct |

  Scenario Outline: Search flight with different cabin classes
    Given Search page
    When User select <class> class
    And User searches <type> flight
    Then Flights appear
    And Flight class is <class>

    Examples:
      | class    | type        |
      | all      | OW          |
      | economy  | OW economy  |
      | premium  | OW premium  |
      | business | OW business |
      | first    | OW first    |

  Scenario Outline: Select a recent origin
    Given Search page
    And the user have searched origin <place>
    When user select recent origin <place>
    Then origin selected is <place>

    Examples:
      | place |
      | PAR   |

  Scenario Outline: Select a recent destination
    Given Search page
    And the user have searched destination <place>
    When user select recent destination <place>
    Then destination selected is <place>

    Examples:
      | place |
      | MAD   |

