Feature: As a user that is in payment page, I can filled my credit card information and pay

  Scenario Outline: I Can not pay without all creditcard fields filled
    Given We are in the Payment page
    When User Adds <creditcard>
    And Adds <cc_name>
    And Adds <cc_date_month>
    And Adds <cc_date_year>
    And Adds <cvv>
    Then Continue button is not clickable

    Examples:
      | cc_date_month | cc_date_year | cvv | creditcard       | cc_name |
      | 04            |              | 111 | 4916310479268680 |         |
      | 04            | 23           |     | 4916310479268680 | test    |
      |               |              | 111 | 4916310479268680 |         |
      | 04            | 23           | 111 |                  | test    |

  Scenario Outline: I Can pay with all creditcard fields filled
    Given We are in the Payment page
    When User Adds <creditcard>
    And Adds <cc_name>
    And Adds <cc_date_month>
    And Adds <cc_date_year>
    And Adds <cvv>
    Then Continue button is clickable

    Examples:
      | cc_date_month | cc_date_year | cvv  | creditcard       | cc_name |
      | 12            | 23           | 4444 | 370000000000002  | test    |
      | 12            | 23           | 444  | 4940100183883571 | test    |
      | 12            | 23           | 444  | 36006666333344   | test    |
      | 12            | 23           | 444  | 3530111333300000 | test    |


  Scenario Outline: I can pay with a selected payment method
    Given We are in the Payment page
    When User Selects <payment_method>
    Then Continue button is clickable

    Examples:
      | payment_method |
      | paypal         |
      | banktransfer   |

  Scenario Outline: I Can pay with all cofinoga fields filled
    Given We are in the Payment page
    When User Selects <payment_method>
    And User Adds <cofinogacard>
    And  Adds <cofinogaName>
    And  Adds valid birthdate
    Then Continue button is clickable

    Examples:
      | payment_method  | cofinogacard                  | cofinogaName |
      | cofinoga        | 30600556924283783             | test         |