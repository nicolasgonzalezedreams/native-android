Feature: Specific walkthrough is only showed on specific countries, default one is showed instead.

  Scenario Outline: Paypal walkthrough
    Given The <market> market is selected
    When Walkthrough page appears
    Then Specific walkthrough <isShowed>

    Examples:
      | market | isShowed |
      | AR     | false    |
      | AU     | true     |
      | BR     | false    |
      | CA:en  | true     |
      | CA:fr  | true     |
      | CL     | false    |
      | CO     | false    |
      | DE     | true     |
      | ES     | true     |
      | US:es  | false    |
      | FR     | true     |
      | HK     | false    |
      | IT     | true     |
      | ID     | false    |
      | NL     | true     |
      | MX     | true     |
      | PH     | false    |
      | NZ     | false    |
      | PT     | true     |
      | CH:fr  | false    |
      | CH:it  | false    |
      | CH:de  | false    |
      | TR     | false    |
      | TH     | true     |
      | AE     | false    |
      | UK     | true     |
      | VE     | false    |
      | US:en  | false    |
      | RU     | false    |
      | JP     | false    |

