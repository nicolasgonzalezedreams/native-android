Feature: As a user seeing a marketing card, I want to have two CTA that I can interact withSo that I can choose the better option for me

  Scenario Outline: Cards with 2 CTA can not have more than 12 characters
    Given A card with 2 CTA
    When Each CTA have <maxSize> characters
    Then All the text is displayed

    Examples:
      | maxSize |
      | 12      |

  Scenario: Cards with 2 CTA can not show more than 12 characters per CTA
    Given A card with 2 CTA
    When Each CTA have an extra large text
    Then The text is ellipsized

  Scenario Outline: Cards with 1 CTA can have more than 12 characters
    Given A card with 1 CTA
    Then CTA can have more than <maxSize> characters
    Examples:
      | maxSize |
      | 12      |
