Feature: As a user I want to logout properly

  Scenario: User is logged out after logout
    Given The user is logged in
    When User log out
    Then User is not logged in
