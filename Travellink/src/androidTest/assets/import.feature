Feature: As a user I want import a trip

  #TODO review why is failing
#  Scenario: Import a trip
#    Given Import Trip Page
#    When User import a valid trip
#    Then Trip is imported

  Scenario: Rate the app title is correct
    Given the page My Trips with one imported trip
    Then the title of the widget is the proper one

  Scenario: Rate the app "I like it"
    Given the page My Trips with one imported trip
    When user select the button I like it
    Then we reached the page leave your feedback

  Scenario: As a user I want to open a trip in MyTrips
    Given the page My Trips with one imported trip
    When user select the trip
    Then trip information is displayed