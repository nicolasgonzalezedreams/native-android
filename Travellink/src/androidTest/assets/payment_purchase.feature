Feature: I am on Payment Screen and I want to switch on TAC acceptance

  Scenario: Accept TAC
    Given We are in the Payment page
    When User Tap in Terms And Conditions Checkbox
    Then The Terms And Conditions checkbox is checked

  Scenario: Tap in new T&C link
    Given We are in the Payment page
    When User taps on T&C
    And Selects airline T&C
    Then Airline T&C document opens