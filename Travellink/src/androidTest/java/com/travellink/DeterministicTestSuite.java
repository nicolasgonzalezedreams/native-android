package com.travellink;

import com.travellink.tests.calendar.CalendarHolidaysTest;
import com.travellink.tests.calendar.CalendarSelectionTest;
import com.travellink.tests.carousel.CarouselCTATest;
import com.travellink.tests.confirmation.ConfirmationTest;
import com.travellink.tests.insurances.InsurancesTest;
import com.travellink.tests.logout.LogoutTest;
import com.travellink.tests.passengers.BaggageTest;
import com.travellink.tests.passengers.PassengerTest;
import com.travellink.tests.payment.PaymentFormTest;
import com.travellink.tests.payment.PaymentPurchaseTest;
import com.travellink.tests.register.RegisterTest;
import com.travellink.tests.results.ResultsTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    CalendarHolidaysTest.class, CarouselCTATest.class, ConfirmationTest.class, InsurancesTest.class,
    LogoutTest.class, BaggageTest.class, PassengerTest.class, PaymentFormTest.class,
    PaymentPurchaseTest.class, RegisterTest.class, ResultsTest.class, CalendarSelectionTest.class
}) public class DeterministicTestSuite {
}
