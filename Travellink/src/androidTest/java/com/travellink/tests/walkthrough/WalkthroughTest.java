package com.travellink.tests.walkthrough;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.walkthrough.OdigeoWalkthroughTest;
import com.pepino.annotations.FeatureOptions;
import com.travellink.activities.WalkthroughActivity;

/**
 * Created by gastonmira on 17/2/17.
 */
@FeatureOptions(feature = "walkthrough.feature") public class WalkthroughTest
    extends OdigeoWalkthroughTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(WalkthroughActivity.class, false, false);
  }
}
