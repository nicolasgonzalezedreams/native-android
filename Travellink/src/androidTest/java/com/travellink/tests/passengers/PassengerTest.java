package com.travellink.tests.passengers;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.app.android.navigator.PassengerNavigator;
import com.odigeo.test.tests.passengers.OdigeoPassengerTest;
import com.pepino.annotations.FeatureOptions;

/**
 * Created by gastonmira on 17/2/17.
 */

@FeatureOptions(feature = "passenger.feature") public class PassengerTest
    extends OdigeoPassengerTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(PassengerNavigator.class, true, false);
  }
}
