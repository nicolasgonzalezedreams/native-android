package com.travellink.tests.importTrip;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.app.android.navigator.MyTripsNavigator;
import com.odigeo.test.tests.importTrip.OdigeoImportTripTest;
import com.pepino.annotations.FeatureOptions;

@FeatureOptions(feature = "import.feature") public class ImportTripTest
    extends OdigeoImportTripTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(MyTripsNavigator.class, false, false);
  }
}
