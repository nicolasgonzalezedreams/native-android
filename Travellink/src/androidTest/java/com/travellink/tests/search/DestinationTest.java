package com.travellink.tests.search;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.search.OdigeoDestinationTest;
import com.pepino.annotations.FeatureOptions;
import com.travellink.activities.DestinationActivity;

/**
 * Created by gastonmira on 17/2/17.
 */
@FeatureOptions(feature = "destination.feature") public class DestinationTest
    extends OdigeoDestinationTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(DestinationActivity.class, true, false);
  }
}
