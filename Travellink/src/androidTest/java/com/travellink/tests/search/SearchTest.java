package com.travellink.tests.search;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.search.OdigeoSearchTest;
import com.pepino.annotations.FeatureOptions;
import com.travellink.activities.SearchActivity;

/**
 * Created by gaston.mira on 1/30/17.
 */
@FeatureOptions(feature = "search.feature") public class SearchTest extends OdigeoSearchTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(SearchActivity.class);
  }
}
