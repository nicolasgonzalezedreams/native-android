package com.travellink.tests.settings;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.settings.OdigeoSettingsTest;
import com.pepino.annotations.FeatureOptions;
import com.travellink.activities.SettingsActivity;

/**
 * Created by gaston.mira on 1/30/17.
 */

@FeatureOptions(feature = "settings.feature") public class SettingsTest extends OdigeoSettingsTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(SettingsActivity.class);
  }
}
