package com.travellink.tests.calendar;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.search.OdigeoCalendarSelectionTest;
import com.pepino.annotations.FeatureOptions;
import com.travellink.activities.SearchActivity;

@FeatureOptions(feature = "calendar_selection.feature") public class CalendarSelectionTest
    extends OdigeoCalendarSelectionTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(SearchActivity.class, false, false);
  }
}