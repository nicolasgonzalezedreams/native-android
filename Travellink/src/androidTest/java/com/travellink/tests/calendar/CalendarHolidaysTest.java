package com.travellink.tests.calendar;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.search.OdigeoCalendarHolidaysTest;
import com.pepino.annotations.FeatureOptions;
import com.travellink.activities.SearchActivity;

/**
 * Created by gaston.mira on 1/30/17.
 */

@FeatureOptions(feature = "calendar_holidays.feature") public class CalendarHolidaysTest
    extends OdigeoCalendarHolidaysTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(SearchActivity.class);
  }
}
