package com.travellink.tests.results;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.results.OdigeoResultsTest;
import com.pepino.annotations.FeatureOptions;
import com.travellink.activities.SearchResultsActivity;

/**
 * Created by gastonmira on 31/3/17.
 */
@FeatureOptions(feature = "results.feature") public class ResultsTest extends OdigeoResultsTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(SearchResultsActivity.class, false, false);
  }
}
