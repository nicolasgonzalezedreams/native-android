package com.travellink.tests.logout;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.logout.OdigeoLogoutTest;
import com.pepino.annotations.FeatureOptions;
import com.travellink.navigator.NavigationDrawerNavigator;

/**
 * Created by gastonmira on 17/2/17.
 */
@FeatureOptions(feature = "logout.feature") public class LogoutTest extends OdigeoLogoutTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(NavigationDrawerNavigator.class);
  }
}
