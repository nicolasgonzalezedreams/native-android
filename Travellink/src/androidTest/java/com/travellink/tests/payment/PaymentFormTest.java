package com.travellink.tests.payment;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.app.android.navigator.PaymentNavigator;
import com.odigeo.test.tests.payment.OdigeoPaymentFormTest;
import com.pepino.annotations.FeatureOptions;

/**
 * Created by gastonmira on 17/2/17.
 */

@FeatureOptions(feature = "paymentform.feature") public class PaymentFormTest
    extends OdigeoPaymentFormTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(PaymentNavigator.class, false, false);
  }
}
