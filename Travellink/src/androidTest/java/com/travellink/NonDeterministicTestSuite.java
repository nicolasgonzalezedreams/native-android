package com.travellink;

import com.travellink.tests.carousel.CarouselDropOffTest;
import com.travellink.tests.importTrip.ImportTripTest;
import com.travellink.tests.payment.PromoCodeTest;
import com.travellink.tests.search.DestinationTest;
import com.travellink.tests.search.SearchTest;
import com.travellink.tests.settings.SettingsTest;
import com.travellink.tests.walkthrough.WalkthroughTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    CarouselDropOffTest.class, ImportTripTest.class, PromoCodeTest.class, DestinationTest.class,
    SearchTest.class, SettingsTest.class, WalkthroughTest.class
}) public class NonDeterministicTestSuite {
}
