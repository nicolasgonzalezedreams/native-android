package com.travellink;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by gaston.mira on 1/30/17.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ DeterministicTestSuite.class, NonDeterministicTestSuite.class })
public class AllTestSuite {
}
