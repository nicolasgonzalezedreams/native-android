package com.travellink;

import com.facebook.stetho.Stetho;

/**
 * Created by gaston.mira on 1/31/17.
 */

public class ApplicationTravellinkDebug extends ApplicationTravellink {
    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
