package com.travellink.activities;

import com.odigeo.app.android.lib.activities.OdigeoDestinationActivity;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;

/**
 * Created by gastonmira on 26/1/17.
 */

public class DestinationActivity extends OdigeoDestinationActivity {
  @Override public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getApplication());
  }

  @Override public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {

  }
}
