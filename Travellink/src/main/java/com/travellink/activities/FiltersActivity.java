package com.travellink.activities;

import com.odigeo.app.android.lib.activities.OdigeoFiltersActivity;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;

/**
 * Created by gastonmira on 26/1/17.
 */

public class FiltersActivity extends OdigeoFiltersActivity {
  @Override public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getApplication());
  }

  @Override public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getApplication());
  }
}
