package com.travellink.activities;

import com.odigeo.app.android.lib.activities.OdigeoSearchResultsActivity;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.fragments.OdigeoNoResultsRoundFragment;
import com.odigeo.app.android.lib.fragments.OdigeoNoResultsSimpleFragment;
import com.odigeo.app.android.lib.fragments.base.OdigeoNoResultsFragment;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;

/**
 * Created by gastonmira on 26/1/17.
 */

public class SearchResultsActivity extends OdigeoSearchResultsActivity {

  //TODO ask if method addWaitingFragment() goes here.

  @Override public OdigeoNoResultsFragment getNoResultsSimpleFragment() {
    return OdigeoNoResultsSimpleFragment.newInstance(getSearchOptions());
  }

  @Override public OdigeoNoResultsFragment getNoResultsRoundFragment() {
    return OdigeoNoResultsRoundFragment.newInstance(getSearchOptions());
  }

  @Override public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getApplication());
  }

  @Override public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getApplication());
  }

  @Override public void onClickHistorySearch(SearchOptions searchOptions) {

  }
}
