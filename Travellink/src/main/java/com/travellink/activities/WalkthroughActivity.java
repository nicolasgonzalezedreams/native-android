package com.travellink.activities;

import com.odigeo.app.android.lib.activities.OdigeoWalkthroughActivity;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;

/**
 * Created by gastonmira on 26/1/17.
 */

public class WalkthroughActivity extends OdigeoWalkthroughActivity {
  @Override public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getApplication());
  }

  @Override public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), this.getApplication());
  }
}
