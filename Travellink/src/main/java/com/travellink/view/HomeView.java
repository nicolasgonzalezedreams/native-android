package com.travellink.view;

import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.squareup.otto.Subscribe;

/**
 * Created by gastonmira on 25/1/17.
 */

public class HomeView extends com.odigeo.app.android.view.HomeView {

  public static HomeView newInstance() {
    return new HomeView();
  }

  @Subscribe public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getActivity().getApplication());
  }

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getActivity().getApplication());
  }

  @Override public void setCampaignBackgroundImage() {
  }

  @Override public void setCityBackgroundImage(String imageUrl) {
  }

  @Override public void setDefaultBackgroundImage() {
  }

  @Override public void translateBackgroundPosition(float position) {
  }

  @Override protected boolean shouldShowPromotionText() {
    return false;
  }
}
