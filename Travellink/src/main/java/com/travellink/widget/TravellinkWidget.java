package com.travellink.widget;

import android.widget.RemoteViews;
import com.odigeo.app.android.lib.widget.OdigeoWidget;
import com.odigeo.data.entity.booking.Segment;
import com.travellink.navigator.NavigationDrawerNavigator;

/**
 * Created by gastonmira on 25/1/17.
 */

public class TravellinkWidget extends OdigeoWidget {
  @Override public Class getHomeActivity() {
    return NavigationDrawerNavigator.class;
  }

  @Override public Class getWidgetServiceClass() {
    return null;
  }

  @Override protected void setBookingScales(Segment segment, RemoteViews remoteViews) {

  }
}
