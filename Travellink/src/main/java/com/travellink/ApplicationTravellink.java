package com.travellink;

import com.crashlytics.android.Crashlytics;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;
import com.odigeo.app.android.navigator.InsurancesNavigator;
import com.odigeo.app.android.navigator.MyTripDetailsNavigator;
import com.odigeo.app.android.navigator.MyTripsNavigator;
import com.odigeo.app.android.navigator.PassengerNavigator;
import com.odigeo.app.android.navigator.PaymentNavigator;
import com.odigeo.app.android.navigator.TravellersNavigator;
import com.travellink.activities.CalendarActivity;
import com.travellink.activities.ConditionsInsurances;
import com.travellink.activities.ConfirmationActivity;
import com.travellink.activities.DestinationActivity;
import com.travellink.activities.DuplicatedBookingActivity;
import com.travellink.activities.FiltersActivity;
import com.travellink.activities.FrequentFlyerActivity;
import com.travellink.activities.NoConnectionActivity;
import com.travellink.activities.PixelWebViewActivity;
import com.travellink.activities.QAModeActivity;
import com.travellink.activities.SearchActivity;
import com.travellink.activities.SearchResultsActivity;
import com.travellink.activities.SettingsActivity;
import com.travellink.activities.SummaryActivity;
import com.travellink.activities.WalkthroughActivity;
import com.travellink.activities.WebViewActivity;
import com.travellink.navigator.NavigationDrawerNavigator;
import com.travellink.widget.TravellinkWidget;
import io.fabric.sdk.android.Fabric;

public class ApplicationTravellink extends OdigeoApp {

  @Override public void onCreate() {
    configureBuildTypes(BuildConfig.MSL_URL, BuildConfig.DEBUG);
    super.onCreate();
  }

  @Override protected void setFonts() {
    Fonts fonts = Configuration.getInstance().getFonts();
    fonts.setBlack(getFontAsset(R.string.fontBlack));
    fonts.setBlackItalic(getFontAsset(R.string.fontBlackItalic));
    fonts.setBold(getFontAsset(R.string.fontBold));
    fonts.setBoldItalic(getFontAsset(R.string.fontBoldItalic));
    fonts.setExtraLight(getFontAsset(R.string.fontExtraLight));
    fonts.setExtraLightItalic(getFontAsset(R.string.fontExtraLightItalic));
    fonts.setItalic(getFontAsset(R.string.fontItalic));
    fonts.setLight(getFontAsset(R.string.fontLight));
    fonts.setLightItalic(getFontAsset(R.string.fontLightItalic));
    fonts.setRegular(getFontAsset(R.string.fontRegular));
    fonts.setSemiBold(getFontAsset(R.string.fontSemiBold));
    fonts.setSemiBoldItalic(getFontAsset(R.string.fontSemiBoldItalic));
    fonts.pack();
  }

  @Override public Class getHomeActivity() {
    return NavigationDrawerNavigator.class;
  }

  @Override public Class getSearchFlightsActivityClass() {
    return SearchActivity.class;
  }

  @Override public Class getCalendarActivityClass() {
    return CalendarActivity.class;
  }

  @Override public Class getDestinationActivityClass() {
    return DestinationActivity.class;
  }

  @Override public Class getSearchResultsActivityClass() {
    return SearchResultsActivity.class;
  }

  @Override public Class getFiltersActivityClass() {
    return FiltersActivity.class;
  }

  @Override public Class getInsurancesActivityClass() {
    return InsurancesNavigator.class;
  }

  @Override public Class getInsurancesConditionsActivityClass() {
    return ConditionsInsurances.class;
  }

  @Override public Class getSummaryActivityClass() {
    return SummaryActivity.class;
  }

  @Override public Class getMyInfoActivityClass() {
    return TravellersNavigator.class;
  }

  @Override public Class getMyTripDetailsActivityClass() {
    return MyTripDetailsNavigator.class;
  }

  @Override public Class getMyTripsActivityClass() {
    return MyTripsNavigator.class;
  }

  @Override public Class getSettingsActivityClass() {
    return SettingsActivity.class;
  }

  @Override public Class getPassengersActivityClass() {
    return PassengerNavigator.class;
  }

  @Override public Class getPaymentActivityClass() {
    return PaymentNavigator.class;
  }

  @Override public Class getPixelWebViewActivityClass() {
    return PixelWebViewActivity.class;
  }

  @Override public Class getConfirmationActivityClass() {
    return ConfirmationActivity.class;
  }

  @Override public Class getNoConnectionClass() {
    return NoConnectionActivity.class;
  }

  @Override public Class getWebViewActivityClass() {
    return WebViewActivity.class;
  }

  @Override public Class getBookingDuplicatedActivityClass() {
    return DuplicatedBookingActivity.class;
  }

  @Override public Class getFrequentFlyer() {
    return FrequentFlyerActivity.class;
  }

  @Override public Class getQAModeActivityClass() {
    return QAModeActivity.class;
  }

  @Override public Class getWalkthroughActivity() {
    return WalkthroughActivity.class;
  }

  @Override public Class getWidgetClass() {
    return TravellinkWidget.class;
  }

  @Override public void setCrashlyticValue(String key, String value) {
    Crashlytics.setString(key, value);
  }

  @Override public void initializeCrashlytics() {
    if (!Fabric.isInitialized()) {
      Fabric.with(this, new Crashlytics());
    }
  }

  @Override public boolean hasAnimatedBackground() {
    return false;
  }

  @Override public Class getNavigationDrawerNavigator() {
    return NavigationDrawerNavigator.class;
  }

  @Override public void trackNonFatal(Throwable throwable) {
    Crashlytics.logException(throwable);
  }

  @Override public void setBool(String key, boolean value) {
    Crashlytics.setBool(key, value);
  }

  @Override public void setDouble(String key, double value) {
    Crashlytics.setDouble(key, value);
  }

  @Override public void setFloat(String key, float value) {
    Crashlytics.setFloat(key, value);
  }

  @Override public void setInt(String key, int value) {
    Crashlytics.setInt(key, value);
  }

  @Override public void setString(String key, String value) {
    Crashlytics.setString(key, value);
  }

  @Override public void setLong(String key, long value) {
    Crashlytics.setLong(key, value);
  }
}
