package com.odigeo.app.android.lib.utils;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class UtilTest {

  @Test public void checksRightDecimalSeparatorIndexWithComma() {
    int index = Util.getIndexDecimalSeparator("900,20");

    assertEquals(3, index);
  }

  @Test public void checksRightDecimalSeparatorIndexWithDot() {
    int index = Util.getIndexDecimalSeparator("900.20");

    assertEquals(3, index);
  }

  @Test public void checksRightDecimalSeparatorIndexWithCommaAndDot() {
    int index = Util.getIndexDecimalSeparator("1,000.20");

    assertEquals(5, index);
  }

  @Test public void checksRightDecimalSeparatorIndexWithCommaAndDotInverted() {
    int index = Util.getIndexDecimalSeparator("1.000,20");

    assertEquals(5, index);
  }

  @Test public void checksNoDecimalSeparatorIndex() {
    int index = Util.getIndexDecimalSeparator("800");

    assertEquals(-1, index);
  }

  @Test public void checksNoDecimalSeparatorIndexWithDot() {
    int index = Util.getIndexDecimalSeparator("1.000");

    assertEquals(-1, index);
  }

  @Test public void checksNoDecimalSeparatorIndexWithComma() {
    int index = Util.getIndexDecimalSeparator("1,000");

    assertEquals(-1, index);
  }
}