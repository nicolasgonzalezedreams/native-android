package com.odigeo.app.android.view;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.BaseTest;
import com.odigeo.app.android.lib.R;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.presenter.PromoCodeWidgetPresenter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;

public class PromoCodeWidgetViewTest extends BaseTest {

  @Mock CreateShoppingCartResponse createShoppingCartResponse;

  private PromoCodeWidgetPresenter promoCodeWidgetPresenter;

  private PromoCodeWidgetView promoCodeWidget;

  @Before public void setUp() {
    promoCodeWidget = new PromoCodeWidgetView(application(), createShoppingCartResponse);
    promoCodeWidgetPresenter = dependencyInjector.providePromoCodeWidgetPresenter(promoCodeWidget);
  }

  @Test public void shouldExpandWidget() {
    Button btnExpandPromoCode = (Button) promoCodeWidget.findViewById(R.id.btnExpandPromoCode);
    btnExpandPromoCode.performClick();
    assertThat("Hide Expand button", btnExpandPromoCode.getVisibility(), is(View.INVISIBLE));
  }

  @Test public void shouldAttemptToValidate() {
    Button btnValidatePromoCode = (Button) promoCodeWidget.findViewById(R.id.btnValidatePromoCode);
    btnValidatePromoCode.performClick();
    verify(promoCodeWidgetPresenter).onValidatePromoCodeButtonClick();
  }

  @Test public void shouldOpenTAC() {
    TextView tvTAC = (TextView) promoCodeWidget.findViewById(R.id.tvTAC);
    tvTAC.performClick();
    verify(promoCodeWidgetPresenter).onTACClick();
  }
}