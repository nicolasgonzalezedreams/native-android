package com.odigeo.app.android.view.validators.creditcards;

import com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators.MasterCardValidator;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class MasterCardValidatorTest {
  @Test public void shouldValidateAsMastercard_16_digits() {
    String mastercard = "5140787971898152";
    assertTrue(mastercard.matches(MasterCardValidator.REGEX_MASTERCARD));
  }

  @Test public void shouldNotValidateAsMastercard_les_than_16_digits() {
    String mastercard = "514078797189815";
    assertFalse(mastercard.matches(MasterCardValidator.REGEX_MASTERCARD));
  }

  @Test public void shouldNotValidateAsMastercard_more_than_16_digits() {
    String mastercard = "51407879718981571";
    assertFalse(mastercard.matches(MasterCardValidator.REGEX_MASTERCARD));
  }

  @Test public void shouldNotValidateAsMastercard_non_5_at_start() {
    String mastercard = "4140787971898152";
    assertFalse(mastercard.matches(MasterCardValidator.REGEX_MASTERCARD));
  }
}
