package com.odigeo.app.android.utils;

import com.odigeo.app.android.BaseTest;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BookingUtilsTest extends BaseTest {

  private static final String DELIMITER = "#";

  @Mock Booking booking;

  @Test public void testGetArrivalCityForMultiSegmentTrip() {
    when(booking.isMultiSegment()).thenReturn(true);

    List<Segment> segments = new ArrayList<Segment>() {{
      final LocationBooking locationBooking1 = mock(LocationBooking.class);
      when(locationBooking1.getCityName()).thenReturn("Barcelona");
      final LocationBooking locationBooking2 = mock(LocationBooking.class);
      when(locationBooking2.getCityName()).thenReturn("Madrid");
      final LocationBooking locationBooking3 = mock(LocationBooking.class);
      when(locationBooking3.getCityName()).thenReturn("London");
      final Segment segment1 = mock(Segment.class);
      when(segment1.getArrivalCity()).thenReturn(locationBooking1);
      final Segment segment2 = mock(Segment.class);
      when(segment2.getArrivalCity()).thenReturn(locationBooking2);
      final Segment segment3 = mock(Segment.class);
      when(segment3.getArrivalCity()).thenReturn(locationBooking3);

      add(segment1);
      add(segment2);
      add(segment3);
    }};
    when(booking.getSegments()).thenReturn(segments);

    final String arrivalCityName = BookingUtils.getArrivalCityName(booking, DELIMITER);

    assertThat(arrivalCityName, is(equalTo("Barcelona#Madrid#London")));
  }

  @Test public void testGetArrivalCityForNonMultiSegmentTrip() {
    when(booking.isMultiSegment()).thenReturn(false);

    final LocationBooking locationBooking = mock(LocationBooking.class);
    when(locationBooking.getCityName()).thenReturn("Barcelona");
    final Section section = mock(Section.class);
    when(section.getTo()).thenReturn(locationBooking);
    final Segment segment = mock(Segment.class);
    when(segment.getLastSection()).thenReturn(section);
    when(booking.getFirstSegment()).thenReturn(segment);

    final String arrivalCityName = BookingUtils.getArrivalCityName(booking, DELIMITER);

    assertThat(arrivalCityName, is(equalTo("Barcelona")));
  }
}
