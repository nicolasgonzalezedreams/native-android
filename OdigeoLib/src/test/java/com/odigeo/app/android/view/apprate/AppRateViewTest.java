package com.odigeo.app.android.view.apprate;

import android.content.Intent;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.BaseTest;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.navigator.AppRateFeedBackNavigator;
import com.odigeo.app.android.navigator.AppRateNavigator;
import com.odigeo.app.android.view.AppRateView;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.interactors.provider.MarketProviderInterface;
import org.junit.Before;
import org.junit.Test;
import org.robolectric.Robolectric;

import static com.odigeo.app.android.lib.utils.ViewUtils.findById;
import static com.odigeo.app.android.view.constants.OneCMSKeys.ABOUTOPTIONSMODULE_ABOUT_OPTION_LEAVEFEEDBACK;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_BUTTON_BAD;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_BUTTON_GOOD;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_SUBTITLE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_TITLE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

public class AppRateViewTest extends BaseTest {

  private static final String MOCK_FEEDBACK = "MOCK_FEEDBACK";
  private static final String MOCK_TITLE = "MOCK_TITLE";
  private static final String MOCK_SUBTITLE = "MOCK_SUBTITLE";
  private static final String MOCK_BUTTON_GOOD = "MOCK_BUTTON_GOOD";
  private static final String MOCK_BUTTON_BAD = "MOCK_BUTTON_BAD";
  private static final String MOCK_BRAND = "MOCK_BRAND";

  private AppRateView appRateView;

  private TrackerControllerInterface trackerController;
  private MarketProviderInterface marketProvider;

  @Before public void setup() {
    super.setup();
    marketProvider = dependencyInjector.provideMarketProvider();
    trackerController = dependencyInjector.provideTrackerController();

    setupAppRateView();
  }

  @Test public void testAppRateViewCMSKeys() {

    TextView appRateTitleTV = findById(appRateView.getView(), R.id.tv_apprate_title);
    TextView appRateSubtitleTV = findById(appRateView.getView(), R.id.tv_apprate_subtitle);
    Button likeItBtn = findById(appRateView.getView(), R.id.btnLikeIt);
    Button couldBeBetterBtn = findById(appRateView.getView(), R.id.btnCouldBeBetter);

    assertThat(appRateTitleTV.getText().toString(), is(equalTo(MOCK_FEEDBACK)));
    assertThat(appRateSubtitleTV.getText().toString(),
        is(equalTo(MOCK_TITLE + ". " + MOCK_SUBTITLE)));
    assertThat(likeItBtn.getText().toString(), is(equalTo(MOCK_BUTTON_GOOD)));
    assertThat(couldBeBetterBtn.getText().toString(), is(equalTo(MOCK_BUTTON_BAD)));
  }

  @Test public void testLikeButtonTracksEvent() {

    findById(appRateView.getView(), R.id.btnLikeIt).performClick();

    verify(trackerController, times(1)).trackYourAppExperienceLikeItButton();

    verifyNoMoreInteractions(trackerController);
  }

  @Test public void testDontLikeButtonTracksEvent() {

    findById(appRateView.getView(), R.id.btnCouldBeBetter).performClick();

    verify(trackerController, times(1)).trackYourAppExperienceNotLikeItButton();

    verifyNoMoreInteractions(trackerController);
  }

  @Test public void testLikeButtonNavigatesToThanksView() {
    findById(appRateView.getView(), R.id.btnLikeIt).performClick();

    Intent intent = shadowOf(application()).getNextStartedActivity();

    assertThat(intent.getComponent().getClassName(),
        is(equalTo(AppRateFeedBackNavigator.class.getName())));
    assertThat(intent.getStringExtra(AppRateNavigator.DESTINATION),
        is(equalTo(AppRateNavigator.DESTINATION_THANKS)));
  }

  @Test public void testDontLikeButtonNavigatesToHelpImproveView() {
    findById(appRateView.getView(), R.id.btnCouldBeBetter).performClick();

    Intent intent = shadowOf(application()).getNextStartedActivity();

    assertThat(intent.getComponent().getClassName(),
        is(equalTo(AppRateFeedBackNavigator.class.getName())));
    assertThat(intent.getStringExtra(AppRateNavigator.DESTINATION),
        is(equalTo(AppRateNavigator.DESTINATION_HELP_IMPROVE)));
  }

  private void setupAppRateView() {

    when(marketProvider.getBrand()).thenReturn(MOCK_BRAND);

    when(localizableProvider.getString(ABOUTOPTIONSMODULE_ABOUT_OPTION_LEAVEFEEDBACK,
        MOCK_BRAND)).thenReturn(MOCK_FEEDBACK);
    when(localizableProvider.getString(MYTRIPSVIEWCONTROLLER_RATE_TITLE)).thenReturn(MOCK_TITLE);
    when(localizableProvider.getString(MYTRIPSVIEWCONTROLLER_RATE_SUBTITLE)).thenReturn(
        MOCK_SUBTITLE);
    when(localizableProvider.getString(MYTRIPSVIEWCONTROLLER_RATE_BUTTON_GOOD)).thenReturn(
        MOCK_BUTTON_GOOD);
    when(localizableProvider.getString(MYTRIPSVIEWCONTROLLER_RATE_BUTTON_BAD)).thenReturn(
        MOCK_BUTTON_BAD);

    AppRateNavigator appRateNavigator = Robolectric.setupActivity(AppRateNavigator.class);

    appRateView = (AppRateView) appRateNavigator.getSupportFragmentManager()
        .findFragmentById(R.id.fl_container);
  }
}
