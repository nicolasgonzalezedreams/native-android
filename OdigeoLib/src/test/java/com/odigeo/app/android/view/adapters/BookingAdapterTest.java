package com.odigeo.app.android.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.BaseTest;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.utils.BookingUtils;
import com.odigeo.app.android.view.adapters.holders.BookingCardHolder;
import com.odigeo.app.android.view.adapters.holders.HeaderHolder;
import com.odigeo.app.android.view.adapters.holders.PastBookingCardHolder;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.interactors.provider.MyTripsProvider;
import com.odigeo.presenter.MyTripsPresenter;
import com.odigeo.test.mock.MocksProvider;
import com.odigeo.tools.DateHelperInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.robolectric.RuntimeEnvironment;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class BookingAdapterTest extends BaseTest {

  private static final String EXPECTED_DATE_FORMAT_EEE_D_MMM_YY = "EEE, d MMM ''yy";

  @Mock private DateHelperInterface dateHelper;
  @Mock private BookingAdapter.OnBookingClickListener onBookingClickListener;
  @Mock private MyTripsPresenter myTripsPresenter;

  private MyTripsProvider myTripsProvider;
  private Booking upcomingBooking;
  private Booking upcomingRoundedBooking;
  private Booking pastBooking;
  private Booking roundedPastBooking;

  private BookingAdapter bookingAdapter;

  @Before public void setUp() {
    pastBooking = MocksProvider.providesBookingMocks().providePastBooking();
    roundedPastBooking = MocksProvider.providesBookingMocks().provideRoundPastBooking();
    upcomingBooking = MocksProvider.providesBookingMocks().provideBooking();
    upcomingRoundedBooking = MocksProvider.providesBookingMocks().provideRoundBooking();

    when(localizableProvider.getString(anyString())).thenReturn("MOCKED_LOCALIZED_STRING");
    when(localizableProvider.getString(OneCMSKeys.TEMPLATE_DATELONG1)).thenReturn(
        EXPECTED_DATE_FORMAT_EEE_D_MMM_YY);

    myTripsProvider = createEmptyMyTrips();

    bookingAdapter = new BookingAdapter(myTripsProvider, dateHelper, myTripsPresenter);
  }

  @Test public void getItem() {
    setBookingsToAdapter(pastBooking);
    assertEquals(pastBooking.getBookingId(), bookingAdapter.getItem(2).getBookingId());
  }

  @Test public void getItemViewTypePastTrip() {
    setBookingsToAdapter(pastBooking);
    assertEquals(BookingAdapter.TYPE_PAST_TRIP, bookingAdapter.getItemViewType(2));
  }

  @Test public void getItemViewTypeHeader() {
    setBookingsToAdapter(pastBooking);
    assertEquals(BookingAdapter.TYPE_HEADER, bookingAdapter.getItemViewType(1));
  }

  @Test public void testGetItemViewTypeFooter() {
    setBookingsToAdapter(pastBooking);
    assertEquals(BookingAdapter.TYPE_FOOTER,
        bookingAdapter.getItemViewType(myTripsProvider.size() - 1));
  }

  @Test public void pastTrip_shouldShowHeader() {
    setBookingsToAdapter(pastBooking);
    LayoutInflater inflater = (LayoutInflater) RuntimeEnvironment.application.getSystemService(
        Context.LAYOUT_INFLATER_SERVICE);

    ViewGroup listItemView = (ViewGroup) inflater.inflate(R.layout.my_booking_header, null, false);
    HeaderHolder holder = HeaderHolder.newInstance(listItemView);
    bookingAdapter.onBindViewHolder(holder, 0);

    assertTrue(holder.header.getText() == "MOCKED_LOCALIZED_STRING");
  }

  @Test public void pastOneWayTrip_shouldShowDate() {
    setBookingsToAdapter(pastBooking);
    SimpleDateFormat simpleDateFormat =
        new SimpleDateFormat(EXPECTED_DATE_FORMAT_EEE_D_MMM_YY, new Locale("ES"));
    String date = simpleDateFormat.format(pastBooking.getLastSegment().getDepartureDate());
    when(
        dateHelper.millisecondsToDateGMT(pastBooking.getLastSegment().checkAndAddSegmentDelayDate(),
            EXPECTED_DATE_FORMAT_EEE_D_MMM_YY)).thenReturn(date);
    LayoutInflater inflater = LayoutInflater.from(RuntimeEnvironment.application);
    ViewGroup listItemView =
        (ViewGroup) inflater.inflate(R.layout.my_past_booking_item, null, false);
    PastBookingCardHolder holder =
        PastBookingCardHolder.newInstance(listItemView, dateHelper, onBookingClickListener);

    bookingAdapter.onBindViewHolder(holder, 2);
    TextView tvDate = (TextView) holder.getCard().findViewById(R.id.tvDate);

    assertTrue(tvDate.getText().toString().equalsIgnoreCase(date));
  }

  @Test public void pastRoundTrip_shouldShowDate() {
    setBookingsToAdapter(roundedPastBooking);
    SimpleDateFormat simpleDateFormat =
        new SimpleDateFormat(EXPECTED_DATE_FORMAT_EEE_D_MMM_YY, new Locale("ES"));
    when(dateHelper.millisecondsToDateGMT(roundedPastBooking.getFirstSegment().getDepartureDate(),
        EXPECTED_DATE_FORMAT_EEE_D_MMM_YY)).thenReturn(
        simpleDateFormat.format(roundedPastBooking.getFirstSegment().getDepartureDate()));
    when(dateHelper.millisecondsToDateGMT(roundedPastBooking.getLastSegment().getArrivalDate(),
        EXPECTED_DATE_FORMAT_EEE_D_MMM_YY)).thenReturn(
        simpleDateFormat.format(roundedPastBooking.getLastSegment().getArrivalDate()));
    LayoutInflater inflater = (LayoutInflater) RuntimeEnvironment.application.getSystemService(
        Context.LAYOUT_INFLATER_SERVICE);
    ViewGroup listItemView =
        (ViewGroup) inflater.inflate(R.layout.my_past_booking_item, null, false);
    PastBookingCardHolder holder =
        PastBookingCardHolder.newInstance(listItemView, dateHelper, onBookingClickListener);
    bookingAdapter.onBindViewHolder(holder, 2);
    TextView tvDate = (TextView) holder.getCard().findViewById(R.id.tvDate);

    String date = BookingUtils.formatSegmentDate(roundedPastBooking.getFirstSegment(), dateHelper,
        localizableProvider) + " - " + BookingUtils.formatSegmentDate(
        roundedPastBooking.getLastSegment(), dateHelper, localizableProvider);

    assertTrue(tvDate.getText().toString().equalsIgnoreCase(date));
  }

  @Test public void upcomingRoundTrip_shouldShowDateTo() {
    setBookingsToAdapter(upcomingRoundedBooking);
    LayoutInflater inflater = (LayoutInflater) RuntimeEnvironment.application.getSystemService(
        Context.LAYOUT_INFLATER_SERVICE);
    ViewGroup listItemView = (ViewGroup) inflater.inflate(R.layout.my_booking_item, null, false);
    BookingCardHolder holder =
        BookingCardHolder.newInstance(listItemView, dateHelper, 1, onBookingClickListener);
    bookingAdapter.onBindViewHolder(holder, 1);
    LinearLayout dateContainer =
        (LinearLayout) holder.getCard().findViewById(R.id.booking_to_date_container);

    assertThat("To-date is display when is a round trip", dateContainer.getVisibility(),
        is(equalTo(VISIBLE)));
  }

  @Test public void upcomingOneWayTrip_shouldHideDateTo() {
    setBookingsToAdapter(upcomingBooking);
    LayoutInflater inflater = (LayoutInflater) RuntimeEnvironment.application.getSystemService(
        Context.LAYOUT_INFLATER_SERVICE);
    ViewGroup listItemView = (ViewGroup) inflater.inflate(R.layout.my_booking_item, null, false);
    BookingCardHolder holder =
        BookingCardHolder.newInstance(listItemView, dateHelper, 1, onBookingClickListener);
    bookingAdapter.onBindViewHolder(holder, 1);
    LinearLayout dateContainer =
        (LinearLayout) holder.getCard().findViewById(R.id.booking_to_date_container);

    assertThat("To-date is not display when is a one way trip", dateContainer.getVisibility(),
        is(equalTo(GONE)));
  }

  private void setBookingsToAdapter(Booking booking) {
    List<Booking> bookings = new ArrayList<>();
    bookings.add(booking);
    myTripsProvider.clear();
    myTripsProvider.addAllBookings(bookings);
  }

  @NonNull private MyTripsProvider createEmptyMyTrips() {
    return new MyTripsProvider();
  }
}