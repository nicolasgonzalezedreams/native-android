package com.odigeo.app.android.view.apprate;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v4.app.Fragment;
import com.odigeo.app.android.BaseTest;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.navigator.AppRateFeedBackNavigator;
import com.odigeo.app.android.navigator.NavigationDrawerNavigator;
import com.odigeo.app.android.view.AppRateHelpImproveView;
import com.odigeo.app.android.view.AppRateThanksView;
import org.junit.Before;
import org.junit.Test;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowPackageManager;

import static android.content.Intent.ACTION_VIEW;
import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static com.odigeo.app.android.navigator.AppRateFeedBackNavigator.MARKET_URI;
import static com.odigeo.app.android.navigator.AppRateNavigator.DESTINATION;
import static com.odigeo.app.android.navigator.AppRateNavigator.DESTINATION_HELP_IMPROVE;
import static com.odigeo.app.android.navigator.AppRateNavigator.DESTINATION_THANKS;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_HEADER;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

public class AppRateFeedbackNavigatorTest extends BaseTest {

  private static final String MOCK_FEEDBACK_HEADER = "MOCK_HEADER";

  private AppRateFeedBackNavigator appRateFeedBackNavigator;

  @Before public void setup() {
    super.setup();
  }

  @Test public void testAppRateFeedbackNavigatorCMSKeys() {
    setupAppRateFeedbackNavigator(DESTINATION_THANKS);

    assertThat(appRateFeedBackNavigator.getTitle().toString(), is(equalTo(MOCK_FEEDBACK_HEADER)));
  }

  @Test public void testNavigatorShowsThanksView() {
    setupAppRateFeedbackNavigator(DESTINATION_THANKS);

    Fragment fragment =
        appRateFeedBackNavigator.getSupportFragmentManager().findFragmentById(R.id.fl_container);

    assertThat(fragment, is(instanceOf(AppRateThanksView.class)));
  }

  @Test public void testNavigatorShowsHelpImproveView() {
    setupAppRateFeedbackNavigator(DESTINATION_HELP_IMPROVE);

    Fragment fragment =
        appRateFeedBackNavigator.getSupportFragmentManager().findFragmentById(R.id.fl_container);

    assertThat(fragment, is(instanceOf(AppRateHelpImproveView.class)));
  }

  @Test public void testNavigateToPlayStore() {
    setupAppRateFeedbackNavigator(DESTINATION_THANKS);

    ShadowPackageManager shadowPackageManager = shadowOf(application().getPackageManager());
    shadowPackageManager.addResolveInfoForIntent(
        new Intent(ACTION_VIEW, Uri.parse(MARKET_URI + application().getPackageName())),
        new ResolveInfo());

    appRateFeedBackNavigator.navigateToPlayStore();

    Intent intent = shadowOf(application()).getNextStartedActivity();

    assertThat(intent.getAction(), is(equalTo(ACTION_VIEW)));
    assertThat(intent.getData().toString(),
        is(equalTo(MARKET_URI + application().getPackageName())));
    assertThat(intent.getFlags(), is(equalTo(FLAG_ACTIVITY_CLEAR_TOP)));
  }

  @Test public void testNavigateToHome() {
    setupAppRateFeedbackNavigator(DESTINATION_THANKS);

    appRateFeedBackNavigator.navigateToHome();

    Intent intent = shadowOf(application()).getNextStartedActivity();

    assertThat(intent.getComponent().getClassName(),
        is(equalTo(NavigationDrawerNavigator.class.getName())));
    assertThat(intent.getFlags(), is(equalTo(FLAG_ACTIVITY_CLEAR_TOP)));
  }

  private void setupAppRateFeedbackNavigator(String extra) {
    when(localizableProvider.getString(LEAVEFEEDBACK_HEADER)).thenReturn(MOCK_FEEDBACK_HEADER);

    Intent intent = new Intent();
    intent.putExtra(DESTINATION, extra);
    appRateFeedBackNavigator =
        Robolectric.buildActivity(AppRateFeedBackNavigator.class, intent).setup().get();
  }
}
