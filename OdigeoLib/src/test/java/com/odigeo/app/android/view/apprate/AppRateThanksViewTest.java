package com.odigeo.app.android.view.apprate;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.BaseTest;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.navigator.AppRateFeedBackNavigator;
import com.odigeo.app.android.view.AppRateThanksView;
import com.odigeo.data.tracker.TrackerControllerInterface;
import org.junit.Before;
import org.junit.Test;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowPackageManager;

import static android.content.Intent.ACTION_VIEW;
import static com.odigeo.app.android.lib.utils.ViewUtils.findById;
import static com.odigeo.app.android.navigator.AppRateFeedBackNavigator.MARKET_URI;
import static com.odigeo.app.android.navigator.AppRateNavigator.DESTINATION;
import static com.odigeo.app.android.navigator.AppRateNavigator.DESTINATION_THANKS;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_THANKS_BUTTON;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_THANKS_SUBTITLE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_THANKS_TITLE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

public class AppRateThanksViewTest extends BaseTest {

  private static final String MOCK_TITLE = "MOCK_TITLE";
  private static final String MOCK_SUBTITLE = "MOCK_SUBTITLE";
  private static final String MOCK_BUTTON = "MOCK_BUTTON";

  private AppRateThanksView appRateThanksView;
  private TrackerControllerInterface trackerController;

  @Before public void setup() {
    super.setup();
    trackerController = dependencyInjector.provideTrackerController();

    setupAppRateThanksView();
  }

  @Test public void testAppRateThanksUsesRightCMSKeys() {

    TextView appRateThanksTitleTV =
        findById(appRateThanksView.getView(), R.id.tv_apprate_thanks_title);
    TextView appRateThanksTV = findById(appRateThanksView.getView(), R.id.tv_apprate_thanks_text);
    Button thanksBtn = findById(appRateThanksView.getView(), R.id.btnThanks);

    assertThat(appRateThanksTitleTV.getText().toString(), is(equalTo(MOCK_TITLE)));
    assertThat(appRateThanksTV.getText().toString(), is(equalTo(MOCK_SUBTITLE)));
    assertThat(thanksBtn.getText().toString(), is(equalTo(MOCK_BUTTON)));
  }

  @Test public void testThanksButtonNavigatesToPlayStore() {

    ShadowPackageManager shadowPackageManager = shadowOf(application().getPackageManager());
    shadowPackageManager.addResolveInfoForIntent(
        new Intent(ACTION_VIEW, Uri.parse(MARKET_URI + application().getPackageName())),
        new ResolveInfo());

    findById(appRateThanksView.getView(), R.id.btnThanks).performClick();

    Intent intent = shadowOf(application()).getNextStartedActivity();

    assertThat(intent.getAction(), is(equalTo(ACTION_VIEW)));
    assertThat(intent.getData().toString(),
        is(equalTo(MARKET_URI + application().getPackageName())));
  }

  @Test public void testThanksButtonTracksEvent() {

    findById(appRateThanksView.getView(), R.id.btnThanks).performClick();

    verify(trackerController, times(1)).trackAppRateThanksView();

    verifyNoMoreInteractions(trackerController);
  }

  private void setupAppRateThanksView() {

    when(localizableProvider.getString(LEAVEFEEDBACK_THANKS_TITLE)).thenReturn(MOCK_TITLE);
    when(localizableProvider.getString(LEAVEFEEDBACK_THANKS_SUBTITLE)).thenReturn(MOCK_SUBTITLE);
    when(localizableProvider.getString(LEAVEFEEDBACK_THANKS_BUTTON)).thenReturn(MOCK_BUTTON);

    Intent intent = new Intent();
    intent.putExtra(DESTINATION, DESTINATION_THANKS);
    AppRateFeedBackNavigator appRateFeedBackNavigator =
        Robolectric.buildActivity(AppRateFeedBackNavigator.class, intent).setup().get();

    appRateThanksView = (AppRateThanksView) appRateFeedBackNavigator.getSupportFragmentManager()
        .findFragmentById(R.id.fl_container);
  }
}
