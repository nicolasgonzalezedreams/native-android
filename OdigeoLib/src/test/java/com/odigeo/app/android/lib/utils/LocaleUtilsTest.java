package com.odigeo.app.android.lib.utils;

import org.junit.Test;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class LocaleUtilsTest {
  @Test public void getLocalizedSweedenCurrencyBigValue() throws Exception {
    String localeUnderTest = "sv_SE";
    double initialValueUnderTest = 2044;
    String expectedValue = "2 044,00 SEK";
    testLocaleData(initialValueUnderTest, localeUnderTest, expectedValue);
  }

  @Test public void getLocalizedSweedenCurrencySmallWithDecimalsValue() throws Exception {
    String localeUnderTest = "sv_SE";
    double initialValueUnderTest = 34.42;
    String expectedValue = "34,42 SEK";
    testLocaleData(initialValueUnderTest, localeUnderTest, expectedValue);
  }

  @Test public void getLocalizedDenmarkCurrencyBigValue() throws Exception {
    String localeUnderTest = "da_DK";
    double initialValueUnderTest = 2044;
    String expectedValue = "2.044,00 DKK";
    testLocaleData(initialValueUnderTest, localeUnderTest, expectedValue);
  }

  @Test public void getLocalizedDenmarkCurrencySmallWithDecimalsValue() throws Exception {
    String localeUnderTest = "da_DK";
    double initialValueUnderTest = 34.42;
    String expectedValue = "34,42 DKK";
    testLocaleData(initialValueUnderTest, localeUnderTest, expectedValue);
  }

  @Test public void getLocalizedIcelandCurrencyBigValue() throws Exception {
    String localeUnderTest = "is_IS";
    double initialValueUnderTest = 2044;
    String expectedValue = "2.044,00 kr.";
    testLocaleData(initialValueUnderTest, localeUnderTest, expectedValue);
  }

  @Test public void getLocalizedIcelandCurrencySmallWithDecimalsValue() throws Exception {
    String localeUnderTest = "is_IS";
    double initialValueUnderTest = 34.42;
    String expectedValue = "34,42 kr.";
    testLocaleData(initialValueUnderTest, localeUnderTest, expectedValue);
  }

  @Test public void getLocalizedPolandCurrencyBigValue() throws Exception {
    String localeUnderTest = "pl_PL";
    double initialValueUnderTest = 2044;
    String expectedValue = "2 044,00 PLN";
    testLocaleData(initialValueUnderTest, localeUnderTest, expectedValue);
  }

  @Test public void getLocalizedPolandCurrencySmallWithDecimalsValue() throws Exception {
    String localeUnderTest = "pl_PL";
    double initialValueUnderTest = 34.42;
    String expectedValue = "34,42 PLN";
    testLocaleData(initialValueUnderTest, localeUnderTest, expectedValue);
  }

  @Test public void getLocalizedFindlandCurrencyBigValue() throws Exception {
    String localeUnderTest = "fi_FI";
    double initialValueUnderTest = 2044;
    String expectedValue = "2 044,00 €";
    testLocaleData(initialValueUnderTest, localeUnderTest, expectedValue);
  }

  @Test public void getLocalizedFindlandCurrencySmallWithDecimalsValue() throws Exception {
    String localeUnderTest = "fi_FI";
    double initialValueUnderTest = 34.42;
    String expectedValue = "34,42 €";
    testLocaleData(initialValueUnderTest, localeUnderTest, expectedValue);
  }

  @Test public void getLocalizedNorwegianCurrencyBigValue() throws Exception {
    String localeUnderTest = "no_NO";
    double initialValueUnderTest = 2044;
    String expectedValue = "2 044,00 NOK";
    testLocaleData(initialValueUnderTest, localeUnderTest, expectedValue);
  }

  @Test public void getLocalizedNorwegianCurrencySmallWithDecimalsValue() throws Exception {
    String localeUnderTest = "no_NO";
    double initialValueUnderTest = 34.42;
    String expectedValue = "34,42 NOK";
    testLocaleData(initialValueUnderTest, localeUnderTest, expectedValue);
  }

  private void testLocaleData(double valueUnderTest, String localeUnderTest, String expectedValue) {
    String returnedValue = LocaleUtils.getLocalizedCurrencyValue(valueUnderTest, localeUnderTest);
    assertThat("format of " + expectedValue + " does not match", returnedValue,
        equalTo(expectedValue));
  }
}