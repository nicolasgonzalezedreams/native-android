package com.odigeo.app.android.view.validators.creditcards;

import com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators.DinersClubValidator;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class DinnersClubValidatorTest {

  @Test public void shouldValidateAsDinnersClub_15_digits_with_second_digit_0() {
    String dinnersClub = "30305919425170";
    assertTrue(dinnersClub.matches(DinersClubValidator.REGEX_DINNERS));
  }

  @Test public void shouldValidateAsDinnersClub_15_digits_with_second_digit_6() {
    String dinnersClub = "36305919425170";
    assertTrue(dinnersClub.matches(DinersClubValidator.REGEX_DINNERS));
  }

  @Test public void shouldValidateAsDinnersClub_15_digits_with_second_digit_8() {
    String dinnersClub = "38305919425170";
    assertTrue(dinnersClub.matches(DinersClubValidator.REGEX_DINNERS));
  }

  @Test public void shouldValidateAsDinnersClub_15_digits_with_second_digit_9() {
    String dinnersClub = "39305919425170";
    assertTrue(dinnersClub.matches(DinersClubValidator.REGEX_DINNERS));
  }

  @Test public void shouldNotValidateAsDinnersClub_less_than_15_digits() {
    String dinnersClub = "3930591942517";
    assertFalse(dinnersClub.matches(DinersClubValidator.REGEX_DINNERS));
  }

  @Test public void shouldNotValidateAsDinnersClub_more_than_16_digits() {
    String dinnersClub = "393059194251702";
    assertFalse(dinnersClub.matches(DinersClubValidator.REGEX_DINNERS));
  }

  @Test public void shouldNotValidateAsDinnersClub_15_with_non_3_at_start() {
    String dinnersClub = "58305919425170";
    assertFalse(dinnersClub.matches(DinersClubValidator.REGEX_DINNERS));
  }

  @Test public void shouldNotValidateAsDinnersClub_15_with_second_digit_different_than_0_6_8_9() {
    String dinnersClub = "32283454965191";
    assertFalse(dinnersClub.matches(DinersClubValidator.REGEX_DINNERS));
  }
}
