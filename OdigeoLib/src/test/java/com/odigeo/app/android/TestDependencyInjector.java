package com.odigeo.app.android;

import android.graphics.Typeface;
import com.odigeo.app.android.lib.utils.LocaleHelper;
import com.odigeo.app.android.providers.FontProvider;
import com.odigeo.app.android.view.helpers.DateHelper;
import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.db.dao.GuideDBDAOInterface;
import com.odigeo.data.db.dao.UserDBDAOInterface;
import com.odigeo.data.download.DownloadController;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.data.tracker.TuneTrackerInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.helper.ABTestHelper;
import com.odigeo.interactors.CheckUserCredentialsInteractor;
import com.odigeo.interactors.MyTripsInteractor;
import com.odigeo.interactors.UpdateBookingStatusInteractor;
import com.odigeo.interactors.UpdateBookingsFromNetworkInteractor;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.presenter.BaggageCollectionPresenter;
import com.odigeo.presenter.MyTripsPresenter;
import com.odigeo.presenter.PassengerWidgetPresenter;
import com.odigeo.presenter.PaymentPresenter;
import com.odigeo.presenter.PriceBreakdownWidgetPresenter;
import com.odigeo.presenter.PromoCodeWidgetPresenter;
import com.odigeo.presenter.contracts.navigators.MyTripsNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.PaymentNavigatorInterface;
import com.odigeo.presenter.contracts.views.BaggageCollection;
import com.odigeo.presenter.contracts.views.MyTripsViewInterface;
import com.odigeo.presenter.contracts.views.PassengerWidget;
import com.odigeo.presenter.contracts.views.PaymentViewInterface;
import com.odigeo.presenter.contracts.views.PriceBreakdownWidgetInterface;
import com.odigeo.presenter.contracts.views.PromoCodeWidget;
import com.odigeo.tools.DurationFormatter;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TestDependencyInjector extends AndroidDependencyInjector {

  private MarketProviderInterface marketProvider;
  private TuneTrackerInterface tuneTracker;
  private PaymentPresenter paymentPresenter;
  private LocalizableProvider localizables;
  private BaggageCollectionPresenter baggageCollectionPresenter;
  private PromoCodeWidgetPresenter promoCodeWidgetPresenter;
  private PassengerWidgetPresenter passengerWidgetPresenter;
  private MyTripsInteractor myTripsInteractor;
  private PreferencesControllerInterface preferencesController;
  private BookingDBDAOInterface bookingDBDAOInterface;
  private SessionController sessionController;
  private CheckUserCredentialsInteractor checkUserCredentialsInteractor;
  private UpdateBookingStatusInteractor updateBookingStatusInteractor;
  private UpdateBookingsFromNetworkInteractor updateBookingsFromNetworkInteractor;
  private TrackerControllerInterface trackerController;
  private PriceBreakdownWidgetPresenter priceBreakdownWidgetPresenter;
  private ABTestHelper abTestHelper;
  private DateHelper dateHelper;
  private LocaleHelper localeHelper;
  private GuideDBDAOInterface guideDBDao;
  private MyTripsPresenter myTripsPresenter;

  @Override public SessionController provideSessionController() {
    if (sessionController == null) {
      sessionController = mock(SessionController.class);
    }
    return sessionController;
  }

  @Override public PreferencesControllerInterface providePreferencesController() {
    if (preferencesController == null) {
      preferencesController = mock(PreferencesControllerInterface.class);
    }
    return preferencesController;
  }

  @Override public UserDBDAOInterface provideUserDBDAO() {
    return null;
  }

  @Override public DownloadController provideDownloadController() {
    return null;
  }

  @Override public BookingDBDAOInterface provideBookingDBDAO() {
    if (bookingDBDAOInterface == null) {
      bookingDBDAOInterface = mock(BookingDBDAOInterface.class);
    }
    return bookingDBDAOInterface;
  }

  @Override public TrackerControllerInterface provideTrackerController() {
    if (trackerController == null) {
      trackerController = mock(TrackerControllerInterface.class);
    }
    return trackerController;
  }

  @Override public DurationFormatter provideDurationFormatter() {
    return mock(DurationFormatter.class);
  }

  @Override public MarketProviderInterface provideMarketProvider() {
    if (marketProvider == null) {
      marketProvider = mock(MarketProviderInterface.class);
    }
    return marketProvider;
  }

  @Override public TuneTrackerInterface provideTuneTracker() {
    if (tuneTracker == null) {
      tuneTracker = mock(TuneTrackerInterface.class);
    }
    return tuneTracker;
  }

  @Override public LocalizableProvider provideLocalizables() {
    if (localizables == null) {
      localizables = mock(LocalizableProvider.class);
    }
    return localizables;
  }

  @Override public BaggageCollectionPresenter provideBaggageCollectionPresenter(
      BaggageCollection baggageCollection) {
    if (baggageCollectionPresenter == null) {
      baggageCollectionPresenter = mock(BaggageCollectionPresenter.class);
    }
    return baggageCollectionPresenter;
  }

  @Override
  public PromoCodeWidgetPresenter providePromoCodeWidgetPresenter(PromoCodeWidget promoCodeWidget) {
    if (promoCodeWidgetPresenter == null) {
      promoCodeWidgetPresenter = mock(PromoCodeWidgetPresenter.class);
    }
    return promoCodeWidgetPresenter;
  }

  @Override public PassengerWidgetPresenter providePassengerWidgetPresenter(PassengerWidget view) {
    if (passengerWidgetPresenter == null) {
      passengerWidgetPresenter = mock(PassengerWidgetPresenter.class);
    }
    return passengerWidgetPresenter;
  }

  @Override public MyTripsPresenter provideMyTripsPresenter(MyTripsViewInterface view,
      MyTripsNavigatorInterface navigator) {
    if (myTripsPresenter == null) {
      myTripsPresenter = mock(MyTripsPresenter.class);
    }
    return myTripsPresenter;
  }

  @Override public PaymentPresenter providePaymentPresenter(PaymentViewInterface view,
      PaymentNavigatorInterface navigator) {
    if (paymentPresenter == null) {
      paymentPresenter = mock(PaymentPresenter.class);
    }
    return paymentPresenter;
  }

  @Override public MyTripsInteractor provideMyTripsInteractor() {
    if (myTripsInteractor == null) {
      myTripsInteractor = mock(MyTripsInteractor.class);
    }
    return myTripsInteractor;
  }

  @Override public CheckUserCredentialsInteractor provideCheckUserCredentialsInteractor() {
    if (checkUserCredentialsInteractor == null) {
      checkUserCredentialsInteractor = mock(CheckUserCredentialsInteractor.class);
    }
    return checkUserCredentialsInteractor;
  }

  @Override public UpdateBookingStatusInteractor provideUpdateBookingStatusInteractor() {
    if (updateBookingStatusInteractor == null) {
      updateBookingStatusInteractor = mock(UpdateBookingStatusInteractor.class);
    }
    return updateBookingStatusInteractor;
  }

  @Override public UpdateBookingsFromNetworkInteractor provideUpdateBookingFromNetworkInteractor() {
    if (updateBookingsFromNetworkInteractor == null) {
      updateBookingsFromNetworkInteractor = mock(UpdateBookingsFromNetworkInteractor.class);
    }
    return updateBookingsFromNetworkInteractor;
  }

  @Override public PriceBreakdownWidgetPresenter providePriceBreakdownWidgetPresenter(
      PriceBreakdownWidgetInterface priceBreakdownWidget) {

    if (priceBreakdownWidgetPresenter == null) {
      priceBreakdownWidgetPresenter = mock(PriceBreakdownWidgetPresenter.class);
    }

    return priceBreakdownWidgetPresenter;
  }

  @Override public ABTestHelper provideABTestHelper() {
    if (abTestHelper == null) {
      abTestHelper = mock(ABTestHelper.class);
    }
    return abTestHelper;
  }

  @Override public LocaleHelper provideLocaleHelper() {
    if (abTestHelper == null) {
      localeHelper = mock(LocaleHelper.class);
    }
    return localeHelper;
  }

  @Override public DateHelper provideDateHelper() {
    if (dateHelper == null) {
      dateHelper = mock(DateHelper.class);
    }
    return dateHelper;
  }

  @Override public GuideDBDAOInterface provideGuideDBDAO() {
    if (guideDBDao == null) {
      guideDBDao = mock(GuideDBDAOInterface.class);
    }
    return guideDBDao;
  }

  @Override public FontProvider provideFontProvider() {
    final FontProvider fontProvider = mock(FontProvider.class);
    when(fontProvider.loadTypeFaceFromFontAsset(anyString())).thenReturn(Typeface.DEFAULT);
    return fontProvider;
  }
}
