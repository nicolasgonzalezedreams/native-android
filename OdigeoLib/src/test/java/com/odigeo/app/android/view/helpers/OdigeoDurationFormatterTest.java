package com.odigeo.app.android.view.helpers;

import com.odigeo.app.android.BaseTest;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.localizable.LocalizableProvider;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

public class OdigeoDurationFormatterTest extends BaseTest {

  @Mock private LocalizableProvider localizableProvider;

  private OdigeoDurationFormatter odigeoDurationFormatter;

  @Before public void init() {

    odigeoDurationFormatter = new OdigeoDurationFormatter(localizableProvider);
  }

  @Test public void shouldFormatSwedenDuration() {
    doReturn("%02dh%02dmin").when(localizableProvider).getString(OneCMSKeys.TEMPLATE_TIME_DURATION);

    String format = odigeoDurationFormatter.format(90);

    assertEquals(format, "01h30min");
  }

  @Test public void shouldFormatNorwayDuration() {
    doReturn("%02dt%02dmin").when(localizableProvider).getString(OneCMSKeys.TEMPLATE_TIME_DURATION);

    String format = odigeoDurationFormatter.format(90);

    assertEquals(format, "01t30min");
  }

  @Test public void shouldFormatDenmarkDuration() {
    doReturn("%02dt%02dmin").when(localizableProvider).getString(OneCMSKeys.TEMPLATE_TIME_DURATION);

    String format = odigeoDurationFormatter.format(90);

    assertEquals(format, "01t30min");
  }

  @Test public void shouldFormatFinlandDuration() {
    doReturn("%02dt%02dmin").when(localizableProvider).getString(OneCMSKeys.TEMPLATE_TIME_DURATION);

    String format = odigeoDurationFormatter.format(90);

    assertEquals(format, "01t30min");
  }

  @Test public void shouldFormatIslandDuration() {
    doReturn("%02dklst%02dm").when(localizableProvider)
        .getString(OneCMSKeys.TEMPLATE_TIME_DURATION);

    String format = odigeoDurationFormatter.format(90);

    assertEquals(format, "01klst30m");
  }

  @Test public void shouldFormatPolandDuration() {
    doReturn("%02dh%02d\'").when(localizableProvider).getString(OneCMSKeys.TEMPLATE_TIME_DURATION);

    String format = odigeoDurationFormatter.format(90);

    assertEquals(format, "01h30'");
  }

  @Test public void shouldFormatDefaultDuration() {
    doReturn("%02dh%02d\'").when(localizableProvider).getString(OneCMSKeys.TEMPLATE_TIME_DURATION);

    String format = odigeoDurationFormatter.format(90);

    assertEquals(format, "01h30'");
  }
}