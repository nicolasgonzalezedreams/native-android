package com.odigeo.app.android.view.validators.creditcards;

import com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators.AmericanExpressValidator;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class AmericanExpressValidatorTest {

  @Test public void shouldValidateAsAmex_15_digits_with_second_digit_7() {
    String amex = "375269848501449";
    assertTrue(amex.matches(AmericanExpressValidator.REGEX_AMEX));
  }

  @Test public void shouldValidateAsAmex_15_digits_with_second_digit_4() {
    String amex = "341766846445710";
    assertTrue(amex.matches(AmericanExpressValidator.REGEX_AMEX));
  }

  @Test public void shouldNotValidateAsAmex_more_than_15_digits() {
    String amex = "3495869257188581";
    assertFalse(amex.matches(AmericanExpressValidator.REGEX_AMEX));
  }

  @Test public void shouldNotValidateAsAmex_less_than_15_digits() {
    String amex = "34958692571885";
    assertFalse(amex.matches(AmericanExpressValidator.REGEX_AMEX));
  }

  @Test public void shouldNotValidateAsAmex_15_digits_with_second_digit_different_than_4_and_7() {
    String amex = "30958692571885";
    assertFalse(amex.matches(AmericanExpressValidator.REGEX_AMEX));
  }

  @Test public void shouldNotValidateAsAmex_15_digits_with_non_3_at_start() {
    String amex = "40958692571885";
    assertFalse(amex.matches(AmericanExpressValidator.REGEX_AMEX));
  }
}
