package com.odigeo.app.android.view.mytrips;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import com.odigeo.app.android.BaseTest;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.navigator.AppRateFeedBackNavigator;
import com.odigeo.app.android.navigator.AppRateNavigator;
import com.odigeo.app.android.navigator.LoginNavigator;
import com.odigeo.app.android.navigator.MyTripsNavigator;
import com.odigeo.app.android.view.MyTripsView;
import com.odigeo.app.android.view.adapters.BookingAdapter;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.FlightStats;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.interactors.provider.MyTripsProvider;
import com.odigeo.presenter.MyTripsPresenter;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.robolectric.Robolectric;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.odigeo.app.android.lib.utils.ViewUtils.findById;
import static com.odigeo.app.android.navigator.AppRateNavigator.DESTINATION_HELP_IMPROVE;
import static com.odigeo.app.android.navigator.AppRateNavigator.DESTINATION_THANKS;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_BUTTON_BAD;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_BUTTON_GOOD;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_SUBTITLE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_TITLE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LIST_CARD_TITLE_TRIP;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LIST_STATUS_CANCEL;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LIST_STATUS_CONFIRM;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LIST_STATUS_PENDING;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LOG_IN_BUTTON;
import static com.odigeo.app.android.view.constants.OneCMSKeys.SSO_ND_TRIPS;
import static com.odigeo.app.android.view.constants.OneCMSKeys.TEMPLATE_DATELONG1;
import static com.odigeo.data.entity.booking.Booking.BOOKING_STATUS_CONTRACT;
import static com.odigeo.data.entity.booking.Booking.BOOKING_STATUS_DIDNOTBUY;
import static com.odigeo.data.entity.booking.Booking.BOOKING_STATUS_REJECTED;
import static com.odigeo.data.entity.booking.Booking.TRIP_TYPE_ONE_WAY;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

public class MyTripsViewTest extends BaseTest {

  private static final String MY_TRIPS_LOG_IN_BUTTON_TEXT = "Localized Login Text";
  private static final String MOCK_TITLE = "My trips title";

  private MyTripsNavigator myTripsNavigator;
  private MyTripsView myTripsView;
  private MyTripsPresenter presenter;

  @Captor private ArgumentCaptor<OnRequestDataListener<MyTripsProvider>> getBookingsListener;

  @Captor private ArgumentCaptor<OnRequestDataListener<Void>> voidOnRequestDataListener;

  @Override public void setup() {
    super.setup();
    setupMyTripsView();
  }

  @Test public void testWhenUserIsLoggedOutAndNoTripsShouldShowLoginButton() {
    myTripsView.showEmptyTrips(true);
    myTripsView.showLoginViews();

    Button loginBtn = findById(myTripsView.getView(), R.id.btnLogin);
    assertThat("Login button is not displayed when user is logged out", loginBtn.getVisibility(),
        is(equalTo(VISIBLE)));
  }

  @Test public void testWhenUserIsLoggedInAndHasNoTripsShouldNotShowLoginButton() {
    myTripsView.showEmptyTrips(true);
    myTripsView.hideLoginViews();

    Button loginBtn = findById(myTripsView.getView(), R.id.btnLogin);
    assertThat("Login button is not displayed when user is logged out", loginBtn.getVisibility(),
        is(equalTo(GONE)));
  }

  @Test public void testWhenUserIsLoggedOutAndHasNoTripsClickOnLoginShouldRedirectToLogin() {
    myTripsView.showEmptyTrips(true);
    myTripsView.showLoginViews();

    Button loginBtn = findById(myTripsView.getView(), R.id.btnLogin);
    loginBtn.performClick();

    verify(presenter).openLogin();
    myTripsNavigator.navigateToLogin();

    Intent intent = shadowOf(application()).getNextStartedActivity();
    assertThat("Intent to LoginNavigator is not triggered", intent.getComponent().getClassName(),
        is(equalTo(LoginNavigator.class.getName())));
  }

  @Test public void testWhenUserLogsInFromLoginShouldHideLoginButton() {
    myTripsView.showEmptyTrips(true);
    myTripsView.showLoginViews();

    myTripsNavigator.navigateToLogin();

    shadowOf(myTripsView.getActivity()).receiveResult(
        new Intent(myTripsNavigator, LoginNavigator.class), RESULT_OK, null);

    Button loginBtn = findById(myTripsView.getView(), R.id.btnLogin);
    assertThat("After user logged in the login button should be hidden", loginBtn.getVisibility(),
        is(equalTo(GONE)));
  }

  @Test public void testWhenUserLogsInFromLoginShouldShowItsBookings() {
    myTripsNavigator.navigateToLogin();

    when(localizableProvider.getString(TEMPLATE_DATELONG1)).thenReturn("EEE, d MMM ''yy");

    shadowOf(myTripsView.getActivity()).receiveResult(
        new Intent(myTripsNavigator, LoginNavigator.class), RESULT_OK, null);

    MyTripsProvider myTripsProvider = new MyTripsProvider();
    myTripsProvider.addAllBookings(new ArrayList<Booking>() {{
      add(new Booking());
    }});

    myTripsView.notifyBookingsHasChanged(myTripsProvider, true);

    RecyclerView myTripsRV = findById(myTripsView.getView(), R.id.bookingsListRecyclerView);

    assertThat("User bookings list is not displayed", myTripsRV.getVisibility(),
        is(equalTo(VISIBLE)));
    assertThat("User bookings list is displayed but it's empty",
        myTripsRV.getAdapter().getItemCount(), is(greaterThan(0)));
  }

  @Test public void testWhenUserCancelLogInFromLoginShouldStillShowLoginButton() {
    myTripsView.showEmptyTrips(true);
    myTripsView.showLoginViews();

    myTripsNavigator.navigateToLogin();

    MyTripsNavigator myTripsNavigator = (MyTripsNavigator) myTripsView.getActivity();

    shadowOf(myTripsView.getActivity()).receiveResult(
        new Intent(myTripsNavigator, LoginNavigator.class), RESULT_CANCELED, null);

    Button loginBtn = findById(myTripsView.getView(), R.id.btnLogin);
    assertThat("After user cancels login the login button is not visible", loginBtn.getVisibility(),
        is(equalTo(VISIBLE)));
  }

  @Test public void testWhenUserIsLoggedOutAndHasNoTripsLoginButtonShouldShowCMSKey() {
    when(localizableProvider.getString(MY_TRIPS_LOG_IN_BUTTON)).thenReturn(
        MY_TRIPS_LOG_IN_BUTTON_TEXT);

    myTripsView.showEmptyTrips(true);
    myTripsView.showLoginViews();

    Button loginBtn = findById(myTripsView.getView(), R.id.btnLogin);
    String loginBtnText = loginBtn.getText().toString();

    assertThat("Login button is not using the copy defined in MY_TRIPS_LOG_IN_BUTTON CMS key",
        loginBtnText, is(equalTo(MY_TRIPS_LOG_IN_BUTTON_TEXT.toUpperCase())));
  }

  @Test public void testUpcomingBookingCardUsesRightCMSKeys() {
    List<Booking> bookings = new ArrayList<Booking>() {{
      add(buildBooking(BOOKING_STATUS_CONTRACT, false));
      add(buildBooking(Booking.BOOKING_STATUS_PENDING, false));
      add(buildBooking(BOOKING_STATUS_REJECTED, false));
    }};
    MyTripsProvider myTripsProvider = new MyTripsProvider();
    myTripsProvider.addAllBookings(bookings, false);

    when(presenter.checkShouldShowAppRate()).thenReturn(false);
    myTripsView.notifyBookingsHasChanged(myTripsProvider, false);

    verify(localizableProvider, times(3)).getString(MY_TRIPS_LIST_CARD_TITLE_TRIP);
    verify(localizableProvider, atLeast(3)).getString(TEMPLATE_DATELONG1);
    verify(localizableProvider).getString(MY_TRIPS_LIST_STATUS_CONFIRM);
    verify(localizableProvider).getString(MY_TRIPS_LIST_STATUS_PENDING);
    verify(localizableProvider).getString(MY_TRIPS_LIST_STATUS_CANCEL);
  }

  @Test public void testAppRateCardIsDisplayed() {
    List<Booking> bookings = new ArrayList<Booking>() {{
      add(buildBooking(BOOKING_STATUS_CONTRACT, false));
      add(buildBooking(Booking.BOOKING_STATUS_PENDING, false));
      add(buildBooking(BOOKING_STATUS_REJECTED, false));
    }};
    MyTripsProvider myTripsProvider = new MyTripsProvider();
    myTripsProvider.addAllBookings(bookings);

    when(presenter.checkShouldShowAppRate()).thenReturn(true);
    myTripsView.notifyBookingsHasChanged(myTripsProvider, true);

    RecyclerView myTripsRV = findById(myTripsView.getView(), R.id.bookingsListRecyclerView);
    assertThat("First item of the list is not AppRate card",
        myTripsRV.getAdapter().getItemViewType(0), is(equalTo(BookingAdapter.TYPE_RATE_APP)));
  }

  @Test public void testAppRateCardIsNotDisplayed() {
    List<Booking> bookings = new ArrayList<Booking>() {{
      add(buildBooking(BOOKING_STATUS_CONTRACT, false));
      add(buildBooking(Booking.BOOKING_STATUS_PENDING, false));
      add(buildBooking(BOOKING_STATUS_REJECTED, false));
    }};
    MyTripsProvider myTripsProvider = new MyTripsProvider();
    myTripsProvider.addAllBookings(bookings);

    when(presenter.checkShouldShowAppRate()).thenReturn(false);
    myTripsView.notifyBookingsHasChanged(myTripsProvider, false);

    RecyclerView myTripsRV = findById(myTripsView.getView(), R.id.bookingsListRecyclerView);

    assertThat("First item of the list is AppRate card", myTripsRV.getAdapter().getItemViewType(0),
        is(not(equalTo(BookingAdapter.TYPE_RATE_APP))));
  }

  @Test public void testAppRateCardUsesRightCMSKeys() {
    List<Booking> bookings = new ArrayList<Booking>() {{
      add(buildBooking(BOOKING_STATUS_CONTRACT, false));
      add(buildBooking(Booking.BOOKING_STATUS_PENDING, false));
      add(buildBooking(BOOKING_STATUS_REJECTED, false));
    }};
    MyTripsProvider myTripsProvider = new MyTripsProvider();
    myTripsProvider.addAllBookings(bookings);

    when(presenter.checkShouldShowAppRate()).thenReturn(true);
    myTripsView.notifyBookingsHasChanged(myTripsProvider, true);

    verify(localizableProvider).getString(MYTRIPSVIEWCONTROLLER_RATE_TITLE);
    verify(localizableProvider).getString(MYTRIPSVIEWCONTROLLER_RATE_SUBTITLE);
    verify(localizableProvider).getString(MYTRIPSVIEWCONTROLLER_RATE_BUTTON_GOOD);
    verify(localizableProvider).getString(MYTRIPSVIEWCONTROLLER_RATE_BUTTON_BAD);
  }

  @Test public void testAppRateCardCouldBeBetterOpensFeedbackDidntLike() {
    List<Booking> bookings = new ArrayList<Booking>() {{
      add(buildBooking(BOOKING_STATUS_CONTRACT, false));
      add(buildBooking(Booking.BOOKING_STATUS_PENDING, false));
      add(buildBooking(BOOKING_STATUS_REJECTED, false));
    }};
    MyTripsProvider myTripsProvider = new MyTripsProvider();
    myTripsProvider.addAllBookings(bookings);

    when(presenter.checkShouldShowAppRate()).thenReturn(true);
    myTripsView.notifyBookingsHasChanged(myTripsProvider, true);

    RecyclerView myTripsRV = findById(myTripsView.getView(), R.id.bookingsListRecyclerView);

    View appRateCard = myTripsRV.getChildAt(0);
    Button didntLike = findById(appRateCard, R.id.btn_not_like);
    didntLike.performClick();

    Intent intent = shadowOf(application()).getNextStartedActivity();
    String extra = intent.getExtras().getString(AppRateNavigator.DESTINATION);

    assertThat("AppRateFeedBackNavigator activity should be started",
        intent.getComponent().getClassName(),
        is(equalTo(AppRateFeedBackNavigator.class.getName())));
    assertThat("Intent should contain extra " + DESTINATION_HELP_IMPROVE, extra,
        is(equalTo(DESTINATION_HELP_IMPROVE)));
  }

  @Test public void testAppRateCardCouldBeBetterOpensFeedbackILikeIt() {
    List<Booking> bookings = new ArrayList<Booking>() {{
      add(buildBooking(BOOKING_STATUS_CONTRACT, false));
      add(buildBooking(Booking.BOOKING_STATUS_PENDING, false));
      add(buildBooking(BOOKING_STATUS_REJECTED, false));
    }};
    MyTripsProvider myTripsProvider = new MyTripsProvider();
    myTripsProvider.addAllBookings(bookings);

    when(presenter.checkShouldShowAppRate()).thenReturn(true);
    myTripsView.notifyBookingsHasChanged(myTripsProvider, true);

    RecyclerView myTripsRV = findById(myTripsView.getView(), R.id.bookingsListRecyclerView);

    View appRateCard = myTripsRV.getChildAt(0);
    Button likeIt = findById(appRateCard, R.id.btn_like);
    likeIt.performClick();

    Intent intent = shadowOf(application()).getNextStartedActivity();
    String extra = intent.getExtras().getString(AppRateNavigator.DESTINATION);

    assertThat("AppRateFeedBackNavigator activity should be started",
        intent.getComponent().getClassName(),
        is(equalTo(AppRateFeedBackNavigator.class.getName())));
    assertThat("Intent should contain extra " + DESTINATION_THANKS, extra,
        is(equalTo(DESTINATION_THANKS)));
  }

  @Test public void testMyTripsViewUsesRightCMSKeys() {
    assertThat("Screen title should be " + MOCK_TITLE, getActionBar().getTitle().toString(),
        is(equalTo(MOCK_TITLE)));
  }

  @Nullable private ActionBar getActionBar() {
    return ((AppCompatActivity) myTripsView.getActivity()).getSupportActionBar();
  }

  private Booking buildBooking(String bookingStatus, boolean isPastBooking) {
    Booking booking = mock(Booking.class);
    LocationBooking locationBooking = mock(LocationBooking.class);
    Section section = mock(Section.class);
    Segment firstSegment = mock(Segment.class);
    Segment lastSegment = mock(Segment.class);
    FlightStats flightStats = mock(FlightStats.class);

    when(locationBooking.getCityName()).thenReturn("A city");
    when(section.getTo()).thenReturn(locationBooking);
    when(section.getFlightStats()).thenReturn(flightStats);
    when(section.getFrom()).thenReturn(locationBooking);
    when(firstSegment.getFirstSection()).thenReturn(section);
    when(firstSegment.getLastSection()).thenReturn(section);
    when(lastSegment.getFirstSection()).thenReturn(section);
    when(lastSegment.getLastSection()).thenReturn(section);
    when(booking.getFirstSegment()).thenReturn(firstSegment);
    when(booking.getLastSegment()).thenReturn(lastSegment);
    when(booking.getBookingStatus()).thenReturn(bookingStatus);
    when(booking.isPastBooking()).thenReturn(isPastBooking);
    when(booking.getTripType()).thenReturn(TRIP_TYPE_ONE_WAY);

    final boolean isConfirmed = bookingStatus.equals(BOOKING_STATUS_CONTRACT);
    final boolean isCancelled =
        bookingStatus.equals(BOOKING_STATUS_REJECTED) || bookingStatus.equals(
            BOOKING_STATUS_DIDNOTBUY);
    final boolean isPending = !isConfirmed && !isCancelled;
    when(booking.isConfirmed()).thenReturn(isConfirmed);
    when(booking.isCancelled()).thenReturn(isCancelled);
    when(booking.isPending()).thenReturn(isPending);

    return booking;
  }

  private void setupMyTripsView() {
    when(localizableProvider.getString(eq(SSO_ND_TRIPS))).thenReturn(MOCK_TITLE);

    myTripsNavigator = Robolectric.setupActivity(MyTripsNavigator.class);
    myTripsView = (MyTripsView) myTripsNavigator.getSupportFragmentManager()
        .findFragmentById(R.id.fl_container);

    presenter = dependencyInjector.provideMyTripsPresenter(myTripsView, myTripsNavigator);

    verify(localizableProvider, atLeastOnce()).getString(SSO_ND_TRIPS);

    // workaround applied only on test to avoid adapter notification issues using robolectric
    // see https://github.com/robolectric/robolectric/issues/3299 for further details
    RecyclerView recyclerView =
        (RecyclerView) myTripsView.getView().findViewById(R.id.bookingsListRecyclerView);
    recyclerView.setHasFixedSize(false);
  }
}
