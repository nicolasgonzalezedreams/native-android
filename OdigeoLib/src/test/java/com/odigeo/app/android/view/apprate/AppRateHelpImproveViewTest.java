package com.odigeo.app.android.view.apprate;

import android.Manifest;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.odigeo.app.android.BaseTest;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.navigator.AppRateFeedBackNavigator;
import com.odigeo.app.android.view.AppRateHelpImproveView;
import com.odigeo.data.tracker.TrackerControllerInterface;
import org.junit.Before;
import org.junit.Test;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowActivity;

import static android.content.Intent.ACTION_CHOOSER;
import static android.content.Intent.ACTION_GET_CONTENT;
import static android.content.Intent.EXTRA_INTENT;
import static com.odigeo.app.android.lib.utils.ViewUtils.findById;
import static com.odigeo.app.android.navigator.AppRateNavigator.DESTINATION;
import static com.odigeo.app.android.navigator.AppRateNavigator.DESTINATION_HELP_IMPROVE;
import static com.odigeo.app.android.view.AppRateHelpImproveView.CHARACTERS_MAX;
import static com.odigeo.app.android.view.AppRateHelpImproveView.DEFAULT_CHARACTER_COUNT;
import static com.odigeo.app.android.view.constants.OneCMSKeys.HELPCENTER_UPLOAD_SCREENSHOT_BUTTON;
import static com.odigeo.app.android.view.constants.OneCMSKeys.HINT_EMAIL;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_GIVE_FEEDBACK_SUBTITLE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_GIVE_FEEDBACK_TITLE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_HELP_PLACEHOLDER;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_HELP_TITLE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_SEND_FEEDBACK_BUTTON;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

public class AppRateHelpImproveViewTest extends BaseTest {

  private static final String MOCK_FEEDBACK_TITLE = "MOCK_FEEDBACK_TITLE";
  private static final String MOCK_FEEDBACK_SUBTITLE = "MOCK_FEEDBACK_SUBTITLE";
  private static final String MOCK_FEEDBACK_BUTTON = "MOCK_FEEDBACK_BUTTON";
  private static final String MOCK_HELP_PLACEHOLDER = "MOCK_HELP_PLACEHOLDER";
  private static final String MOCK_HELP_TITLE = "MOCK_HELP_TITLE";
  private static final String MOCK_UPLOAD_SCREENSHOT_BUTTON = "MOCK_UPLOAD_SCREENSHOT_BUTTON";
  private static final String MOCK_HINT_EMAIL = "MOCK_HINT_EMAIL";
  private static final String MOCK_POSITIVE_BUTTON = "MOCK_POSITIVE_BUTTON";
  private static final String MOCK_NEGATIVE_BUTTON = "MOCK_NEGATIVE_BUTTON";
  private static final String MOCK_UPLOAD_QUESTION = "MOCK_UPLOAD_QUESTION";

  private AppRateHelpImproveView appRateHelpImproveView;
  private TrackerControllerInterface trackerController;

  @Before public void setup() {
    super.setup();
    trackerController = dependencyInjector.provideTrackerController();

    setupAppRateThanksView();
  }

  @Test public void testAppRateThanksUsesRightCMSKeys() {

    TextView appRateHelpImproveTitle =
        findById(appRateHelpImproveView.getView(), R.id.tv_apprate_helpimprove_title);
    TextView appRateHelpImproveText =
        findById(appRateHelpImproveView.getView(), R.id.tv_apprate_helpimprove_text);
    TextView appRateHelpImproveSubtitle =
        findById(appRateHelpImproveView.getView(), R.id.tv_apprate_helpimprove_subtitle);
    EditText appRateHelpImproveComment =
        findById(appRateHelpImproveView.getView(), R.id.et_apprate_helpimprove_comment);

    TextView appRateHelpImproveCharacters =
        findById(appRateHelpImproveView.getView(), R.id.tv_apprate_helpimprove_characters);
    Button screenshotBtn = findById(appRateHelpImproveView.getView(), R.id.btn_screenshot);
    Button helpImproveBtn = findById(appRateHelpImproveView.getView(), R.id.btn_help_improve);

    assertThat(appRateHelpImproveTitle.getText().toString(), is(equalTo(MOCK_FEEDBACK_TITLE)));
    assertThat(appRateHelpImproveText.getText().toString(), is(equalTo(MOCK_FEEDBACK_SUBTITLE)));
    assertThat(appRateHelpImproveSubtitle.getText().toString(), is(equalTo(MOCK_HELP_TITLE)));
    assertThat(appRateHelpImproveComment.getHint().toString(), is(equalTo(MOCK_HELP_PLACEHOLDER)));
    assertThat(screenshotBtn.getText().toString(), is(equalTo(MOCK_UPLOAD_SCREENSHOT_BUTTON)));
    assertThat(helpImproveBtn.getText().toString(), is(equalTo(MOCK_FEEDBACK_BUTTON)));
    assertThat(appRateHelpImproveCharacters.getText().toString(),
        is(equalTo(DEFAULT_CHARACTER_COUNT + CHARACTERS_MAX)));
  }

  @Test public void testHelpImproveButtonTracksEvent() {

    findById(appRateHelpImproveView.getView(), R.id.btn_help_improve).performClick();

    verify(trackerController, times(1)).trackAppRateHelpUsImproveView();

    verifyNoMoreInteractions(trackerController);
  }

  @Test public void testScreenshotButtonTriggersGalleryChooserIntent() {

    shadowOf(appRateHelpImproveView.getActivity()).grantPermissions(
        Manifest.permission.READ_EXTERNAL_STORAGE);

    findById(appRateHelpImproveView.getView(), R.id.btn_screenshot).performClick();

    ShadowActivity.IntentForResult chooserIntent =
        shadowOf(appRateHelpImproveView.getActivity()).getNextStartedActivityForResult();

    Intent targetIntent = chooserIntent.intent.getParcelableExtra(EXTRA_INTENT);

    assertThat(chooserIntent.intent.getAction(), is(equalTo(ACTION_CHOOSER)));
    assertThat(targetIntent.getType(), is(equalTo("image/*")));
    assertThat(targetIntent.getAction(), is(equalTo(ACTION_GET_CONTENT)));
  }

  private void setupAppRateThanksView() {

    when(localizableProvider.getString(LEAVEFEEDBACK_GIVE_FEEDBACK_TITLE)).thenReturn(
        MOCK_FEEDBACK_TITLE);
    when(localizableProvider.getString(LEAVEFEEDBACK_GIVE_FEEDBACK_SUBTITLE)).thenReturn(
        MOCK_FEEDBACK_SUBTITLE);
    when(localizableProvider.getString(LEAVEFEEDBACK_SEND_FEEDBACK_BUTTON)).thenReturn(
        MOCK_FEEDBACK_BUTTON);
    when(localizableProvider.getString(LEAVEFEEDBACK_HELP_PLACEHOLDER)).thenReturn(
        MOCK_HELP_PLACEHOLDER);
    when(localizableProvider.getString(LEAVEFEEDBACK_HELP_TITLE)).thenReturn(MOCK_HELP_TITLE);
    when(localizableProvider.getString(HELPCENTER_UPLOAD_SCREENSHOT_BUTTON)).thenReturn(
        MOCK_UPLOAD_SCREENSHOT_BUTTON);
    when(localizableProvider.getString(HINT_EMAIL)).thenReturn(MOCK_HINT_EMAIL);

    Intent intent = new Intent();
    intent.putExtra(DESTINATION, DESTINATION_HELP_IMPROVE);
    AppRateFeedBackNavigator appRateFeedBackNavigator =
        Robolectric.buildActivity(AppRateFeedBackNavigator.class, intent).setup().get();

    appRateHelpImproveView =
        (AppRateHelpImproveView) appRateFeedBackNavigator.getSupportFragmentManager()
            .findFragmentById(R.id.fl_container);
  }
}
