package com.odigeo.app.android;

import android.support.annotation.CallSuper;
import com.odigeo.app.android.lib.BuildConfig;
import com.odigeo.app.android.lib.utils.LocaleHelper;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.interactors.provider.MarketProviderInterface;
import java.util.Locale;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, application = TestOdigeoApp.class)
public abstract class BaseTest {

  protected AndroidDependencyInjector dependencyInjector;
  protected LocalizableProvider localizableProvider;
  protected LocaleHelper localeHelper;
  protected MarketProviderInterface marketProvider;

  @CallSuper @Before public void setup() {
    MockitoAnnotations.initMocks(this);
    dependencyInjector = (AndroidDependencyInjector) application().getDependencyInjector();
    localizableProvider = dependencyInjector.provideLocalizables();
    localeHelper = dependencyInjector.provideLocaleHelper();
    marketProvider = dependencyInjector.provideMarketProvider();

    when(localeHelper.getLocalizedCurrencyValue(anyDouble(), any(Locale.class),
        anyBoolean())).thenReturn("123456.78");
    when(localizableProvider.getString(anyString())).thenReturn("MOCKED_LOCALIZED_STRING");
    when(localizableProvider.getString(anyString(), Mockito.<String>anyVararg())).thenReturn(
        "MOCKED_LOCALIZED_STRING");
    when(localizableProvider.getRawString(anyString())).thenReturn("MOCKED_LOCALIZED_STRING");
  }

  protected TestOdigeoApp application() {
    return (TestOdigeoApp) RuntimeEnvironment.application.getApplicationContext();
  }
}
