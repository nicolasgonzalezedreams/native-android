package com.odigeo.app.android.view.validators;

import org.junit.Test;
import static com.odigeo.app.android.view.textwatchers.fieldsvalidation.AddressValidator.REGEX_ADDRESS;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class AddressValidatorTest {

  private String address;

  @Test public void shouldValidateAddress() {
    address = "Avenida random, 123, 5-B";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "C/Random de random, 4 izq, 3B";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "132, My Street,Bigtown BG23 4YZ";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "My Street,132 ,Bigtown BG23 4YZ";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "p. Milos Kuric Jindišská 909/14";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "ABB Lackieranlagen GmbH Schorbachstrasse 9";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "18, Heracleous St.Kifissia 145 64 - Athens, ";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "01311-300 Sâo Paulo - SP";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "Str. Florilor 8, Bl. I19, Sc. 2, Et. 2, Ap. 25";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "Károlyi Mihály utca 11-15., 1053 Budapest";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "RandÖm de random, 4 izq. 3B";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "RÀndÔm Dê RÄnDom de españÁ. 4-izq. 3B";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "RåndÓÍm Dê RÄnDòm dë Èspaña. 4-izq. 3B";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "RåndÓÍm Dé RÄnDØm de plaçaŠ ÈspÅñÃ.,.3B ";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "RåndÕÎm Dê RÄnDßm ý plaçaŞ ÈspaÑa de la lûz.,.3B";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "RåndÖÏm Dè RÄnDÆm ÿ plaçŒŞ ÈspaÑa de la Ğúz.,.3B";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "RåndÓÍm Dé RÄnDÔm Ý plaçaŠ ÈspÅñÃ Ğ";
    assertTrue(address.matches(REGEX_ADDRESS));

    address = "RåndÓÍm Dé RÄnDØm Ÿ plaçaŠ ÈspÅñÃ.,.3B ";
    assertTrue(address.matches(REGEX_ADDRESS));
  }

  @Test public void shouldNotValidateAddress() {
    address = "Avenida random, 123, 5ºB";
    assertFalse(address.matches(REGEX_ADDRESS));

    address = "Avenida random, 123, 5&B";
    assertFalse(address.matches(REGEX_ADDRESS));

    address = "C\\Random de random, 4 izq, 3B";
    assertFalse(address.matches(REGEX_ADDRESS));

    address = "";
    assertFalse(address.matches(REGEX_ADDRESS));

    address = "Random de (random), 4 izq, 3B";
    assertFalse(address.matches(REGEX_ADDRESS));

    address = "CRandom {de} random, 4 izq, 3B";
    assertFalse(address.matches(REGEX_ADDRESS));

    address = "Avenida random!, 123, 5&B";
    assertFalse(address.matches(REGEX_ADDRESS));

    address = "Avenida random?, 123, 5&B";
    assertFalse(address.matches(REGEX_ADDRESS));

    address = "Avenida random=, 123, 5&B";
    assertFalse(address.matches(REGEX_ADDRESS));

    address =
        "Avenida randomrandomrandomrandomrandomrandomrandomrandomrandomrandomrandomrandomrandom";
    assertFalse(address.matches(REGEX_ADDRESS));

    address = "Avenida random%, 123, 5B";
    assertFalse(address.matches(REGEX_ADDRESS));

    address = "Avenida #random, 123, 5B";
    assertFalse(address.matches(REGEX_ADDRESS));

    address = "@venida random, 123, 5B";
    assertFalse(address.matches(REGEX_ADDRESS));

    address = "Avenida r;andom, 123, 5B";
    assertFalse(address.matches(REGEX_ADDRESS));

    address = "Avenida] random, 123, 5B";
    assertFalse(address.matches(REGEX_ADDRESS));

  }
}
