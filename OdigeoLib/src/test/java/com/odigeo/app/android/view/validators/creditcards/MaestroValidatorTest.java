package com.odigeo.app.android.view.validators.creditcards;

import com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators.MaestroValidator;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class MaestroValidatorTest {

  @Test public void shouldValidateAsMaestro_16_digits_with_6759_at_start() {
    String maestro = "6759649826438453";
    assertTrue(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }

  @Test public void shouldValidateAsMaestro_16_digits_with_5018_at_start() {
    String maestro = "5018649826438453";
    assertTrue(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }

  @Test public void shouldValidateAsMaestro_16_digits_with_5020_at_start() {
    String maestro = "5020649826438453";
    assertTrue(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }

  @Test public void shouldValidateAsMaestro_16_digits_with_5038_at_start() {
    String maestro = "5038649826438453";
    assertTrue(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }

  @Test public void shouldValidateAsMaestro_16_digits_with_6304_at_start() {
    String maestro = "6304649826438453";
    assertTrue(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }

  @Test public void shouldValidateAsMaestro_16_digits_with_6761_at_start() {
    String maestro = "6761649826438453";
    assertTrue(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }

  @Test public void shouldValidateAsMaestro_16_digits_with_6763_at_start() {
    String maestro = "6763649826438453";
    assertTrue(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }

  @Test public void shouldNotValidateAsMaestro_with_more_than_20_digits() {
    String maestro = "676364982643845312341";
    assertFalse(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }

  @Test public void shouldNotValidateAsMaestro_with_less_than_12_digits() {
    String maestro = "67596498264";
    assertFalse(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }

  @Test public void shouldValidateAsMaestro_12_digits() {
    String maestro = "675964982642";
    assertTrue(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }

  @Test public void shouldValidateAsMaestro_13_digits() {
    String maestro = "6759649826423";
    assertTrue(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }

  @Test public void shouldValidateAsMaestro_14_digits() {
    String maestro = "67596498264234";
    assertTrue(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }

  @Test public void shouldValidateAsMaestro_15_digits() {
    String maestro = "675964982642345";
    assertTrue(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }

  @Test public void shouldValidateAsMaestro_17_digits() {
    String maestro = "6759649826423458";
    assertTrue(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }

  @Test public void shouldValidateAsMaestro_18_digits() {
    String maestro = "67596498264234581";
    assertTrue(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }

  @Test public void shouldValidateAsMaestro_19_digits() {
    String maestro = "675964982642345812";
    assertTrue(maestro.matches(MaestroValidator.REGEX_MAESTRO));
  }
}
