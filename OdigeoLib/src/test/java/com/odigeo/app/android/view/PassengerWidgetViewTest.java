package com.odigeo.app.android.view;

import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Switch;
import com.odigeo.app.android.BaseTest;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.view.interfaces.ListenerPassengerNavigator;
import com.odigeo.app.android.view.interfaces.ListenerPassengerWidget;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.shoppingCart.Itinerary;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.data.entity.shoppingCart.TravellerRequiredFields;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.presenter.PassengerWidgetPresenter;
import com.odigeo.tools.DateHelperInterface;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class PassengerWidgetViewTest extends BaseTest {

  @Mock FragmentManager fragmentManager;
  @Mock TravellerRequiredFields travellerRequiredFields;
  @Mock DateHelperInterface dateHelperInterface;
  @Mock ListenerPassengerNavigator listenerPassengerNavigator;
  @Mock List<UserTraveller> travellersListForWidget;
  @Mock ListenerPassengerWidget listenerPassengerWidget;
  @Mock Itinerary itinerary;
  @Mock List<Traveller> travellers;

  private PassengerWidgetView passengerWidgetView;
  private PassengerWidgetPresenter passengerWidgetPresenter;

  @Before public void setUp() {
    passengerWidgetView =
        new PassengerWidgetView(application(), fragmentManager, 0, travellerRequiredFields,
            dateHelperInterface, listenerPassengerNavigator, travellersListForWidget,
            listenerPassengerWidget, itinerary, travellers, TravelType.SIMPLE);
    passengerWidgetPresenter =
        dependencyInjector.providePassengerWidgetPresenter(passengerWidgetView);
  }

  @Test public void shouldDisableApplyBaggageSelection() {
    Switch applyBaggageSelectionSwitch =
        (Switch) passengerWidgetView.findViewById(R.id.applyBaggageSelectionSwitch);
    applyBaggageSelectionSwitch.setChecked(true);
    passengerWidgetView.disableApplyBaggageSelection();
    assertThat("Disable apply selection switch", applyBaggageSelectionSwitch.isChecked(),
        is(false));
  }

  @Test public void shouldApplyBaggageSelectionWidget() {
    LinearLayout applyBaggageSelection =
        (LinearLayout) passengerWidgetView.findViewById(R.id.applyBaggageSelection);

    passengerWidgetView.setPassengerAsDefaultPassenger(true);
    passengerWidgetView.onBaggageSelectionChange(false);
    assertThat("apply selection widget is visible", applyBaggageSelection.getVisibility(),
        is(View.VISIBLE));
  }
}
