package com.odigeo.app.android.view;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import com.odigeo.app.android.BaseTest;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import com.odigeo.app.android.model.BookingInfoViewModel;
import com.odigeo.app.android.navigator.PaymentNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.robolectric.Robolectric;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class PaymentViewTest extends BaseTest {

  private final boolean IS_FULL_TRANSPARENCY = true;
  private final double LAST_TICKET_PRICE = 2.0;
  private final double LAST_INSURANCE_PRICE = 1.0;
  @Mock private CreateShoppingCartResponse createShoppingCartResponse;
  @Mock private SearchOptions searchOptions;
  @Mock private CollectionMethodWithPrice collectionMethodWithPrice;
  @Mock private BookingInfoViewModel bookingInfoViewModel;
  private PaymentNavigator paymentNavigator;
  private PaymentView paymentView;

  @Before public void setUp() {

    createPaymentNavigator();

    createPaymentViewWithFullTransparency();
  }

  @Test public void shouldShowCallButtonIfMarketHasPhoneNumber() {
    when(localizableProvider.getString(
        OneCMSKeys.PAYMENTVIEWCONTROLLER_PAYMENTRETRY_PHONE_NUMBER)).thenReturn("655443322");

    paymentView.showPaymentRetry();

    Button callButton = (Button) paymentView.alertDialog.findViewById(R.id.btn_call);
    assertThat(callButton.getVisibility(), is(View.VISIBLE));
  }

  @Test public void
  shouldNotShowCallButtonIfMarketHasNoPhoneNumber() {
    when(localizableProvider.getString(
        OneCMSKeys.PAYMENTVIEWCONTROLLER_PAYMENTRETRY_PHONE_NUMBER)).thenReturn("");

    paymentView.showPaymentRetry();

    Button callButton = (Button) paymentView.alertDialog.findViewById(R.id.btn_call);
    assertThat(callButton.getVisibility(), is(View.GONE));
  }

  private void createPaymentViewWithFullTransparency() {
    paymentView =
        PaymentView.newInstance(createShoppingCartResponse, IS_FULL_TRANSPARENCY, LAST_TICKET_PRICE,
            LAST_INSURANCE_PRICE, searchOptions, collectionMethodWithPrice, bookingInfoViewModel);
    paymentNavigator.getSupportFragmentManager()
        .beginTransaction()
        .replace(R.id.flContainer, paymentView)
        .commit();
  }

  private void createPaymentNavigator() {

    Intent intent = new Intent();
    intent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE, createShoppingCartResponse);
    intent.putExtra(Constants.EXTRA_FULL_TRANSPARENCY, IS_FULL_TRANSPARENCY);
    intent.putExtra(Constants.EXTRA_REPRICING_TICKETS_PRICES, LAST_TICKET_PRICE);
    intent.putExtra(Constants.EXTRA_REPRICING_INSURANCES, LAST_INSURANCE_PRICE);
    intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, searchOptions);
    intent.putExtra(Constants.COLLECTION_METHOD_WITH_PRICE, collectionMethodWithPrice);

    paymentNavigator = Robolectric.buildActivity(PaymentNavigator.class, intent).create().get();
  }
}