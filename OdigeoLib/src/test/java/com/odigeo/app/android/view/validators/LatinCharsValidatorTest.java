package com.odigeo.app.android.view.validators;

import org.junit.Test;
import static com.odigeo.app.android.view.textwatchers.fieldsvalidation.NameValidator.REGEX_LATIN_CHARS;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class LatinCharsValidatorTest {

  private String word;

  @Test public void shouldValidateWord(){
    word = "randomName";
    assertTrue(word.matches(REGEX_LATIN_CHARS));

    word = "Random Name";
    assertTrue(word.matches(REGEX_LATIN_CHARS));

    word = "Random Random Random";
    assertTrue(word.matches(REGEX_LATIN_CHARS));

    word = "randomName";
    assertTrue(word.matches(REGEX_LATIN_CHARS));

    word = "RANDOMNAME";
    assertTrue(word.matches(REGEX_LATIN_CHARS));

    word = "RÀndÔm NÁmË Šan";
    assertTrue(word.matches(REGEX_LATIN_CHARS));

    word = "Asbjörn";
    assertTrue(word.matches(REGEX_LATIN_CHARS));

    word = "Arnvir";
    assertTrue(word.matches(REGEX_LATIN_CHARS));

    word = "aa";
    assertTrue(word.matches(REGEX_LATIN_CHARS));

    word = "random-Name";
    assertTrue(word.matches(REGEX_LATIN_CHARS));

    word = "-Name";
    assertTrue(word.matches(REGEX_LATIN_CHARS));

    word = "random-";
    assertTrue(word.matches(REGEX_LATIN_CHARS));
  }

  @Test public void shouldNotValidateWord(){
    word = "randomName1";
    assertFalse(word.matches(REGEX_LATIN_CHARS));

    word = "";
    assertFalse(word.matches(REGEX_LATIN_CHARS));

    word = "RandomRandomRandomRandomRandomRandom";
    assertFalse(word.matches(REGEX_LATIN_CHARS));

    word = "randomName.";
    assertFalse(word.matches(REGEX_LATIN_CHARS));

    word = "randomN@me.";
    assertFalse(word.matches(REGEX_LATIN_CHARS));

    word = "random123456789_-";
    assertFalse(word.matches(REGEX_LATIN_CHARS));

    word = "randomName.";
    assertFalse(word.matches(REGEX_LATIN_CHARS));

    word = "a";
    assertFalse(word.matches(REGEX_LATIN_CHARS));

    word = "-";
    assertFalse(word.matches(REGEX_LATIN_CHARS));
  }


}
