package com.odigeo.app.android.view.validators.creditcards;

import com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators.VisaValidator;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class VisaValidatorTest {

  @Test public void shouldValidateAsVisa_16_digits() {
    String visa = "4485259394636797";
    assertTrue(visa.matches(VisaValidator.REGEX_VISA));
  }

  @Test public void shouldValidateAsVisa_13_digits() {
    String visa = "4485259394636";
    assertTrue(visa.matches(VisaValidator.REGEX_VISA));
  }

  @Test public void shouldNotValidateAsVisa_with_non_4_at_start() {
    String visa = "3485259394636797";
    assertFalse(visa.matches(VisaValidator.REGEX_VISA));
  }

  @Test public void shouldNotValidateAsVisa_less_than_13_digits() {
    String visa = "448525939463";
    assertFalse(visa.matches(VisaValidator.REGEX_VISA));
  }

  @Test public void shouldNotValidateAsVisa_more_than_13_and_less_than_16_digits() {
    String visa = "44852593946312";
    assertFalse(visa.matches(VisaValidator.REGEX_VISA));
  }

  @Test public void shouldNotValidateAsVisa_more_than_16_digits() {
    String visa = "44852593946367975";
    assertFalse(visa.matches(VisaValidator.REGEX_VISA));
  }
}
