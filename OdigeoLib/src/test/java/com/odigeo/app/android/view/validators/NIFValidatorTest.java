package com.odigeo.app.android.view.validators;

import org.junit.Test;
import static com.odigeo.app.android.view.textwatchers.fieldsvalidation.NIFValidator.REGEX_NIF;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class NIFValidatorTest {

  private String nif;

  @Test public void shouldValidateNif(){
    nif = "79799178-B";
    assertTrue(nif.matches(REGEX_NIF));

    nif = "79799178B";
    assertTrue(nif.matches(REGEX_NIF));

    nif ="24351473Q";
    assertTrue(nif.matches(REGEX_NIF));

    nif = "79799178-b";
    assertTrue(nif.matches(REGEX_NIF));

    nif = "79799178h";
    assertTrue(nif.matches(REGEX_NIF));
  }

  @Test public void shouldNotValidateNif(){
    nif ="X4351473Q";
    assertFalse(nif.matches(REGEX_NIF));

    nif ="";
    assertFalse(nif.matches(REGEX_NIF));

    nif ="435147334";
    assertFalse(nif.matches(REGEX_NIF));

    nif ="43514733-4";
    assertFalse(nif.matches(REGEX_NIF));

    nif ="79799178-BB";
    assertFalse(nif.matches(REGEX_NIF));

    nif ="'9799178-BB";
    assertFalse(nif.matches(REGEX_NIF));

    nif ="Y0583093B";
    assertFalse(nif.matches(REGEX_NIF));

    nif ="H72769011";
    assertFalse(nif.matches(REGEX_NIF));

    nif = "797991788B";
    assertFalse(nif.matches(REGEX_NIF));
  }
}
