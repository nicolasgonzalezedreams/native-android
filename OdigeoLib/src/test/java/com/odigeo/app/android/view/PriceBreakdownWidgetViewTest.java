package com.odigeo.app.android.view;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.odigeo.app.android.BaseTest;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Market;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.shoppingCart.AdditionalProductShoppingItem;
import com.odigeo.data.entity.shoppingCart.InsuranceShoppingItem;
import com.odigeo.data.entity.shoppingCart.OtherProductsShoppingItems;
import com.odigeo.data.entity.shoppingCart.PricingBreakdown;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItem;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItemType;
import com.odigeo.data.entity.shoppingCart.ShoppingCart;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.presenter.PriceBreakdownWidgetPresenter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PriceBreakdownWidgetViewTest extends BaseTest {

  @Mock private ScrollView scrollView;
  @Mock private SearchOptions searchOptions;
  @Mock private PricingBreakdown pricingBreakdown;
  @Mock private CollectionMethodWithPrice collectionMethodWithPrice;
  @Mock private ShoppingCart shoppingCart;

  private PriceBreakdownWidgetView priceBreakdownWidget;
  private PriceBreakdownWidgetPresenter presenter;

  @Before public void setUp() {
    priceBreakdownWidget = new PriceBreakdownWidgetView(application());
    presenter = dependencyInjector.providePriceBreakdownWidgetPresenter(priceBreakdownWidget);
  }

  @Test public void shouldShowTaxRefundTest() {
    priceBreakdownWidget = new PriceBreakdownWidgetView(application());

    TextView taxRefundableInfo =
        (TextView) priceBreakdownWidget.findViewById(R.id.taxrefundableinfo);


    when(pricingBreakdown.getTaxRefund()).thenReturn(5.6);
    when(dependencyInjector.provideMarketProvider()
        .getLocalizedCurrencyValue(anyDouble())).thenReturn("5.6€");
    when(
        localizableProvider.getString(OneCMSKeys.FULLPRICE_TAX_REFUNDABLE_INFO, "5.6€")).thenReturn(
        "Refundable Taxes 5.6€");

    priceBreakdownWidget.addData(scrollView, searchOptions, pricingBreakdown, Step.PAYMENT,
        collectionMethodWithPrice, shoppingCart);

    priceBreakdownWidget.showTaxRefundableInfo(5.6);
    assertThat("Show Tax Button", taxRefundableInfo.getVisibility(), is(View.VISIBLE));
  }

  @Test public void shouldNotShowTaxRefundTest() {
    priceBreakdownWidget = new PriceBreakdownWidgetView(application());

    TextView taxRefundableInfo =
        (TextView) priceBreakdownWidget.findViewById(R.id.taxrefundableinfo);

    priceBreakdownWidget.addData(scrollView, searchOptions, pricingBreakdown, Step.PAYMENT,
        collectionMethodWithPrice, shoppingCart);
    when(pricingBreakdown.getTaxRefund()).thenReturn(0.0);
    priceBreakdownWidget.checkTaxRefundableInfo();
    assertThat("Not show Tax Button", taxRefundableInfo.getVisibility(), is(View.GONE));
  }

  @Test public void shouldShowMoreInformationWhenWidgetIsExpand() {
    givenPriceBreakDownWithContent();

    TextView seeDetails = (TextView) priceBreakdownWidget.findViewById(R.id.tvSeeDetails);
    LinearLayout priceBreakdownExpandableContent =
        (LinearLayout) priceBreakdownWidget.findViewById(R.id.llPriceBreakdownExpandableContent);

    priceBreakdownWidget.addData(scrollView, searchOptions, pricingBreakdown, Step.PAYMENT,
        collectionMethodWithPrice, shoppingCart);

    seeDetails.performClick();

    assertEquals(priceBreakdownExpandableContent.getVisibility(), View.VISIBLE);
  }

  @Test public void shouldShowLessInformationWhenWidgetIsCollapse() {
    givenPriceBreakDownWithContent();

    TextView seeDetails = (TextView) priceBreakdownWidget.findViewById(R.id.tvSeeDetails);
    LinearLayout priceBreakdownExpandableContent =
        (LinearLayout) priceBreakdownWidget.findViewById(R.id.llPriceBreakdownExpandableContent);

    priceBreakdownWidget.addData(scrollView, searchOptions, pricingBreakdown, Step.PAYMENT,
        collectionMethodWithPrice, shoppingCart);

    seeDetails.performClick();
    seeDetails.performClick();

    assertEquals(priceBreakdownExpandableContent.getVisibility(), View.GONE);
  }

  @Test public void shouldNotShowAnyMembershipLabel() {
    givenPriceBreakDownWithContent();

    priceBreakdownWidget.addData(scrollView, searchOptions, pricingBreakdown, Step.PAYMENT,
        collectionMethodWithPrice, shoppingCart);

    when(presenter.shouldShowMembershipAppliedLabel()).thenReturn(false);
    initializeWidgetPresenter();

    priceBreakdownWidget.showPricesBreakdown();

    TextView tvPriceBreakdownMembershipApplied =
        (TextView) priceBreakdownWidget.findViewById(R.id.tvPriceBreakdownMembershipApplied);
    assertEquals(tvPriceBreakdownMembershipApplied.getVisibility(), View.GONE);
  }

  @Test public void shouldShowMembershipAppliedLabel() {
    givenPriceBreakDownWithContent();

    priceBreakdownWidget.addData(scrollView, searchOptions, pricingBreakdown, Step.PAYMENT,
        collectionMethodWithPrice, shoppingCart);
    TextView tvPriceBreakdownMembershipApplied =
        (TextView) priceBreakdownWidget.findViewById(R.id.tvPriceBreakdownMembershipApplied);

    when(presenter.shouldShowMembershipAppliedLabel()).thenReturn(true);
    initializeWidgetPresenter();
    when(presenter.membershipIsApplied()).thenReturn(true);

    priceBreakdownWidget.showPricesBreakdown();

    assertEquals(tvPriceBreakdownMembershipApplied.getVisibility(), View.VISIBLE);
    assertEquals(tvPriceBreakdownMembershipApplied.getText(),
        localizableProvider.getString(OneCMSKeys.PRICING_BREAKDOWN_PREMIUM_DISCOUNT_APPLIED));
    assertEquals(tvPriceBreakdownMembershipApplied.getCurrentTextColor(), ContextCompat.getColor
        (application(), R.color.semantic_positive));
  }

  @Test public void shouldShowMembershipNotAppliedLabel() {
    givenPriceBreakDownWithContent();

    priceBreakdownWidget.addData(scrollView, searchOptions, pricingBreakdown, Step.PAYMENT,
        collectionMethodWithPrice, shoppingCart);
    TextView tvPriceBreakdownMembershipApplied =
        (TextView) priceBreakdownWidget.findViewById(R.id.tvPriceBreakdownMembershipApplied);

    when(presenter.shouldShowMembershipAppliedLabel()).thenReturn(true);
    initializeWidgetPresenter();
    when(presenter.membershipIsApplied()).thenReturn(false);

    priceBreakdownWidget.showPricesBreakdown();

    assertEquals(tvPriceBreakdownMembershipApplied.getVisibility(), View.VISIBLE);
    assertEquals(tvPriceBreakdownMembershipApplied.getText(),
        localizableProvider.getString(OneCMSKeys.PRICING_BREAKDOWN_PREMIUM_DISCOUNT_NOT_APPLIED));
    assertEquals(tvPriceBreakdownMembershipApplied.getCurrentTextColor(), ContextCompat.getColor
        (application(), R.color.secondary_brand_dark));
  }

  private void initializeWidgetPresenter() {
    when(presenter.getTotalPrice()).thenReturn(120d);
    when(presenter.getPricingBreakdownItems()).thenReturn(
        provideMockedPricingBreakdownWithMembershipItems());
    when(presenter.getShoppingCart()).thenReturn(
        provideMockedShoppingCart());
    when(presenter.getPriceItemAmountWithType(any(PricingBreakdownItemType.class), anyInt(), any
        (InsuranceShoppingItem.class))).thenReturn(BigDecimal.valueOf(120));
    when(dependencyInjector.provideMarketProvider().getLocalizedCurrencyValue(anyDouble()))
        .thenReturn("120€");
    Configuration.getInstance().setCurrentMarket(provideCurrentMarket());
  }

  private Market provideCurrentMarket() {
    Market market = new Market();
    market.setLocale("es_ES");
    return market;
  }

  private ShoppingCart provideMockedShoppingCart() {
    ShoppingCart shoppingCart = new ShoppingCart();
    shoppingCart.setOtherProductsShoppingItems(provideOtherProductsShoppingItems());
    return shoppingCart;
  }

  private OtherProductsShoppingItems provideOtherProductsShoppingItems() {
    OtherProductsShoppingItems otherProductsShoppingItems = new OtherProductsShoppingItems();
    otherProductsShoppingItems.setInsuranceShoppingItems(new ArrayList<InsuranceShoppingItem>());
    otherProductsShoppingItems.setAdditionalProductShoppingItems(
        new ArrayList<AdditionalProductShoppingItem>());
    return otherProductsShoppingItems;
  }

  private List<PricingBreakdownItem> provideMockedPricingBreakdownWithMembershipItems() {
    List<PricingBreakdownItem> pricingBreakdownItems = new ArrayList<>();

    PricingBreakdownItem pricingBreakdownItem = new PricingBreakdownItem();
    pricingBreakdownItem.setPriceItemAmount(BigDecimal.valueOf(10));
    pricingBreakdownItem.setPriceItemType(PricingBreakdownItemType.MEMBERSHIP_PERKS);
    pricingBreakdownItems.add(pricingBreakdownItem);

    return pricingBreakdownItems;
  }

  private void givenPriceBreakDownWithContent() {
    when(localizableProvider.getString(OneCMSKeys.COMMON_SHOW_MORE).toUpperCase()).thenReturn(
        "Show More");
    when(localizableProvider.getString(OneCMSKeys.COMMON_SHOW_LESS).toUpperCase()).thenReturn(
        "Show Less");
    priceBreakdownWidget = new PriceBreakdownWidgetView(application());
  }
}

