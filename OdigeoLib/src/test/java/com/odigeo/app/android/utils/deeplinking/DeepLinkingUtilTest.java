package com.odigeo.app.android.utils.deeplinking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class DeepLinkingUtilTest {

  @Test public void testDeepLinkUriBuild() {
    String uri = DeepLinkingUtil.build("http", "host", new ArrayList<String>() {{
      add("path");
    }}, new HashMap<String, String>() {{
      put("param", "value");
    }});
    final String result = "http://host/path?param=value";

    assertThat("Uri's don't match", uri, equalTo(result));
  }

  @Test public void testDeepLinkUriBuildWithMultiplePaths() {
    String uri = DeepLinkingUtil.build("http", "host", new ArrayList<String>() {{
      add("path1");
      add("path2");
      add("path3");
    }}, new LinkedHashMap<String, String>() {{
      put("param", "value");
    }});

    final String result = "http://host/path1/path2/path3?param=value";

    assertThat("Uri's don't match", uri, equalTo(result));
  }

  @Test public void testDeepLinkUriBuildWithMultipleParams() {
    String uri = DeepLinkingUtil.build("http", "host", new ArrayList<String>() {{
      add("path");
    }}, new LinkedHashMap<String, String>() {{
      put("param1", "value1");
      put("param2", "value2");
      put("param3", "value3");
    }});

    final String result = "http://host/path?param1=value1&param2=value2&param3=value3";

    assertThat("Uri's don't match", uri, equalTo(result));
  }

  @Test public void testDeepLinkReturnsNullWithEmptyScheme() {
    String uri = DeepLinkingUtil.build(null, "host", new ArrayList<String>() {{
      add("path");
    }}, new HashMap<String, String>() {{
      put("param", "value");
    }});

    assertThat("Uri's is not null", uri, is(nullValue()));
  }

  @Test public void testDeepLinkReturnsNullWithEmptyAuthority() {
    String uri = DeepLinkingUtil.build("http", null, new ArrayList<String>() {{
      add("path");
    }}, new HashMap<String, String>() {{
      put("param", "value");
    }});

    assertThat("Uri's is not null", uri, is(nullValue()));
  }
}