package com.odigeo.app.android.view.mytrips;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import com.odigeo.app.android.BaseTest;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.navigator.MyTripDetailsNavigator;
import com.odigeo.data.db.dao.GuideDBDAOInterface;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Guide;
import com.odigeo.test.mock.MocksProvider;
import com.odigeo.tools.DateHelperInterface;
import org.hamcrest.text.IsEqualIgnoringCase;
import org.junit.Before;
import org.junit.Test;
import org.robolectric.Robolectric;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_BOOKING_STATUS_PENDING_MESSAGE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LIST_STATUS_CANCEL;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LIST_STATUS_CONFIRM;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LIST_STATUS_PENDING;
import static com.odigeo.data.entity.booking.Booking.TRIP_TYPE_ONE_WAY;
import static com.odigeo.data.entity.booking.Booking.TRIP_TYPE_ROUND_TRIP;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class MyTripDetailsViewTest extends BaseTest {

  private static final String TRIP_DATE = "Wed, 30 Aug '17";
  private static final String CONFIRMED_STATUS_LABEL = "Confirmed";
  private static final String PENDING_STATUS_LABEL = "Pending";
  private static final String PENDING_STATUS_MESSAGE = "Your booking is pending";
  private static final String CANCELLED_STATUS_LABEL = "Cancelled";
  private static final String BRAND_VISUAL_NAME = "eDreams";

  private MyTripDetailsNavigator navigator;
  private Booking booking;

  @Before public void setup() {
    super.setup();
    setupMocks(TRIP_TYPE_ROUND_TRIP);
  }

  @Test public void testHeaderShowsBookingArrivalCityName() {
    setupNavigator();

    TextView arrivalCityNameTV = (TextView) navigator.findViewById(R.id.booking_destination_title);
    assertThat(arrivalCityNameTV.getText().toString(),
        is(IsEqualIgnoringCase.equalToIgnoringCase(booking.getArrivalCityName(0))));
  }

  @Test public void testHeaderShowsRightDatesIfRoundTrip() {
    setupNavigator();

    TextView inboundDateTV = (TextView) navigator.findViewById(R.id.booking_inbound_date);
    TextView outboundDateTV = (TextView) navigator.findViewById(R.id.booking_outbound_date);

    assertThat(inboundDateTV.getText().toString(),
        is(IsEqualIgnoringCase.equalToIgnoringCase(TRIP_DATE)));
    assertThat(outboundDateTV.getText().toString(),
        is(IsEqualIgnoringCase.equalToIgnoringCase(TRIP_DATE)));
  }

  @Test public void testHeaderShowsRightDatesIfOneWay() {
    setupMocks(TRIP_TYPE_ONE_WAY);
    setupNavigator();

    TextView outboundDateTV = (TextView) navigator.findViewById(R.id.booking_outbound_date);
    View inboundDateContainer = navigator.findViewById(R.id.booking_inbound_date_container);

    assertThat(outboundDateTV.getText().toString(),
        is(IsEqualIgnoringCase.equalToIgnoringCase(TRIP_DATE)));
    assertThat(inboundDateContainer.getVisibility(), is(equalTo(GONE)));
  }

  @Test public void testHeaderShowsConfirmedStatus() {
    booking = spy(booking);
    when(booking.isConfirmed()).thenReturn(true);
    setupNavigator();

    View bookingStatusContainer = navigator.findViewById(R.id.booking_status_container);
    TextView bookingStatusLabelTV = (TextView) navigator.findViewById(R.id.booking_status_label);

    assertThat(bookingStatusContainer.getVisibility(), is(equalTo(VISIBLE)));
    assertThat(bookingStatusLabelTV.getText().toString(), is(equalTo(CONFIRMED_STATUS_LABEL)));
  }

  @Test public void testHeaderShowsPendingStatus() {
    setupNavigator();

    View bookingStatusContainer = navigator.findViewById(R.id.booking_status_container);
    TextView bookingStatusLabelTV = (TextView) navigator.findViewById(R.id.booking_status_label);
    TextView bookingStatusDescriptionTV =
        (TextView) navigator.findViewById(R.id.booking_status_pending_message);

    assertThat(bookingStatusContainer.getVisibility(), is(equalTo(VISIBLE)));
    assertThat(bookingStatusLabelTV.getText().toString(), is(equalTo(PENDING_STATUS_LABEL)));
    assertThat(bookingStatusDescriptionTV.getText().toString(),
        is(equalTo(PENDING_STATUS_MESSAGE)));
  }

  @Test public void testHeaderShowsCancelledStatus() {
    booking = spy(booking);
    when(booking.isCancelled()).thenReturn(true);
    setupNavigator();

    View bookingStatusContainer = navigator.findViewById(R.id.booking_status_container);
    TextView bookingStatusLabelTV = (TextView) navigator.findViewById(R.id.booking_status_label);

    assertThat(bookingStatusContainer.getVisibility(), is(equalTo(VISIBLE)));
    assertThat(bookingStatusLabelTV.getText().toString(), is(equalTo(CANCELLED_STATUS_LABEL)));
  }

  @Test public void testHeaderShowsConfirmedDescriptionWithBrandAndBookingId() {
    booking = spy(booking);
    when(booking.isConfirmed()).thenReturn(true);
    setupNavigator();

    View bookingStatusContainer = navigator.findViewById(R.id.booking_status_container);
    TextView bookingStatusLabelTV = (TextView) navigator.findViewById(R.id.booking_status_label);
    TextView bookingStatusDescriptionTV =
        (TextView) navigator.findViewById(R.id.booking_status_description);
    TextView bookingIdLabelTV = (TextView) navigator.findViewById(R.id.booking_id_label);

    assertThat(bookingStatusContainer.getVisibility(), is(equalTo(VISIBLE)));
    assertThat(bookingStatusLabelTV.getText().toString(), is(equalTo(CONFIRMED_STATUS_LABEL)));
    assertThat(bookingStatusDescriptionTV.getText().toString(), is(equalTo(BRAND_VISUAL_NAME)));
    assertThat(bookingIdLabelTV.getText().toString(),
        is(equalTo(String.format("#%s", booking.getBookingId()))));
  }

  @Test public void testHeaderShowsCancelledDescriptionWithBrandAndBookingId() {
    booking = spy(booking);
    when(booking.isCancelled()).thenReturn(true);
    setupNavigator();

    View bookingStatusContainer = navigator.findViewById(R.id.booking_status_container);
    TextView bookingStatusLabelTV = (TextView) navigator.findViewById(R.id.booking_status_label);
    TextView bookingStatusDescriptionTV =
        (TextView) navigator.findViewById(R.id.booking_status_description);
    TextView bookingIdLabelTV = (TextView) navigator.findViewById(R.id.booking_id_label);

    assertThat(bookingStatusContainer.getVisibility(), is(equalTo(VISIBLE)));
    assertThat(bookingStatusLabelTV.getText().toString(), is(equalTo(CANCELLED_STATUS_LABEL)));
    assertThat(bookingStatusDescriptionTV.getText().toString(), is(equalTo(BRAND_VISUAL_NAME)));
    assertThat(bookingIdLabelTV.getText().toString(),
        is(equalTo(String.format("#%s", booking.getBookingId()))));
  }

  @Test public void testHeaderShowsPendingDescriptionWithPendingMessage() {
    booking = spy(booking);
    when(booking.isPending()).thenReturn(true);
    setupNavigator();

    View bookingStatusContainer = navigator.findViewById(R.id.booking_status_container);
    TextView bookingStatusLabelTV = (TextView) navigator.findViewById(R.id.booking_status_label);
    TextView bookingStatusDescriptionTV =
        (TextView) navigator.findViewById(R.id.booking_status_pending_message);
    TextView bookingIdLabelTV = (TextView) navigator.findViewById(R.id.booking_id_label);

    assertThat(bookingStatusContainer.getVisibility(), is(equalTo(VISIBLE)));
    assertThat(bookingStatusLabelTV.getText().toString(), is(equalTo(PENDING_STATUS_LABEL)));
    assertThat(bookingStatusDescriptionTV.getText().toString(),
        is(equalTo(PENDING_STATUS_MESSAGE)));
    assertThat(bookingIdLabelTV.getVisibility(), is(equalTo(GONE)));
  }

  private void setupNavigator() {
    Intent intent = MyTripDetailsNavigator.startIntent(application(), booking);
    navigator = Robolectric.buildActivity(MyTripDetailsNavigator.class, intent).setup().get();
  }

  private void setupMocks(String tripType) {
    DateHelperInterface dateHelper = dependencyInjector.provideDateHelper();
    when(dateHelper.dateToMiliseconds(anyString(), anyString())).thenReturn(1L);
    when(dateHelper.millisecondsToDateGMT(anyLong(), anyString())).thenReturn(TRIP_DATE);
    when(localizableProvider.getString(MY_TRIPS_LIST_STATUS_CONFIRM)).thenReturn(
        CONFIRMED_STATUS_LABEL);
    when(localizableProvider.getString(MY_TRIPS_LIST_STATUS_PENDING)).thenReturn(
        PENDING_STATUS_LABEL);
    when(localizableProvider.getString(MY_TRIPS_BOOKING_STATUS_PENDING_MESSAGE)).thenReturn(
        PENDING_STATUS_MESSAGE);
    when(localizableProvider.getString(MY_TRIPS_LIST_STATUS_CANCEL)).thenReturn(
        CANCELLED_STATUS_LABEL);
    when(marketProvider.getBrandVisualName()).thenReturn(BRAND_VISUAL_NAME);

    GuideDBDAOInterface guideDBDAO = dependencyInjector.provideGuideDBDAO();
    when(guideDBDAO.getGuideByGeoNodeId(anyLong())).thenReturn(new Guide());

    booking = MocksProvider.providesBookingMocks().provideBooking();
    booking.setTripType(tripType);
  }
}
