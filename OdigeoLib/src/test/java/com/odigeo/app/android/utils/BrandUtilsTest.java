package com.odigeo.app.android.utils;

import com.odigeo.app.android.lib.consts.Constants;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class BrandUtilsTest {

  @Test public void testIsEdreams() {
    boolean isEdreams = BrandUtils.isEdreamsBrand(Constants.BRAND_EDREAMS);

    assertThat("Is not eDreams brand", isEdreams, is(true));
  }

  @Test public void testIsOpodo() {
    boolean isOpodo = BrandUtils.isOpodoBrand(Constants.BRAND_OPODO);

    assertThat("Is not Opodo brand", isOpodo, is(true));
  }

  @Test public void testSwedenIsOpodoNordics() {
    boolean isOpodo = BrandUtils.isOpodoBrand(Constants.BRAND_OPODO);
    boolean isNordic =
        BrandUtils.isOpodoNordicsBrand(Constants.BRAND_OPODO, BrandUtils.MARKET_KEY_SE);

    assertThat(BrandUtils.MARKET_KEY_SE + " is not Opodo Nordics brand", isOpodo,
        is(equalTo(isNordic)));
  }

  @Test public void testDenmarkIsOpodoNordics() {
    boolean isOpodo = BrandUtils.isOpodoBrand(Constants.BRAND_OPODO);
    boolean isNordic =
        BrandUtils.isOpodoNordicsBrand(Constants.BRAND_OPODO, BrandUtils.MARKET_KEY_DK);

    assertThat(BrandUtils.MARKET_KEY_SE + " is not Opodo Nordics brand", isOpodo,
        is(equalTo(isNordic)));
  }

  @Test public void testNorwayIsOpodoNordics() {
    boolean isOpodo = BrandUtils.isOpodoBrand(Constants.BRAND_OPODO);
    boolean isNordic =
        BrandUtils.isOpodoNordicsBrand(Constants.BRAND_OPODO, BrandUtils.MARKET_KEY_NO);

    assertThat(BrandUtils.MARKET_KEY_SE + " is not Opodo Nordics brand", isOpodo,
        is(equalTo(isNordic)));
  }

  @Test public void testFinlandIsOpodoNordics() {
    boolean isOpodo = BrandUtils.isOpodoBrand(Constants.BRAND_OPODO);
    boolean isNordic =
        BrandUtils.isOpodoNordicsBrand(Constants.BRAND_OPODO, BrandUtils.MARKET_KEY_FI);

    assertThat(BrandUtils.MARKET_KEY_SE + " is not Opodo Nordics brand", isOpodo,
        is(equalTo(isNordic)));
  }

  @Test public void testPolandIsOpodoNordics() {
    boolean isOpodo = BrandUtils.isOpodoBrand(Constants.BRAND_OPODO);
    boolean isNordic =
        BrandUtils.isOpodoNordicsBrand(Constants.BRAND_OPODO, BrandUtils.MARKET_KEY_PL);

    assertThat(BrandUtils.MARKET_KEY_SE + " is not Opodo Nordics brand", isOpodo,
        is(equalTo(isNordic)));
  }

  @Test public void testRestOfWorldIsNotOpodoNordics() {
    boolean isOpodo = BrandUtils.isOpodoBrand(Constants.BRAND_OPODO);
    boolean isNordic =
        BrandUtils.isOpodoNordicsBrand(Constants.BRAND_OPODO, BrandUtils.MARKET_KEY_COM);

    assertThat(BrandUtils.MARKET_KEY_COM + " is Opodo Nordics brand", isOpodo,
        is(not(equalTo(isNordic))));
  }

  @Test public void testIsGoVoyages() {
    boolean isGoVoyages = BrandUtils.isGoVoyagesBrand(Constants.BRAND_GOVOYAGES);

    assertThat("Is not Go Voyages brand", isGoVoyages, is(true));
  }

  @Test public void testIsTravellink() {
    boolean isTravellink = BrandUtils.isTravellinkBrand(Constants.BRAND_TRAVELLINK);

    assertThat("Is not Travellink brand", isTravellink, is(true));
  }

  @Test public void testSwedenMarketIsNordic() {
    boolean isNordic = BrandUtils.isNordicCountryByMarket(BrandUtils.MARKET_KEY_SE);

    assertThat(BrandUtils.MARKET_KEY_SE + " is not nordic", isNordic, is(true));
  }

  @Test public void testNorwayMarketIsNordic() {
    boolean isNordic = BrandUtils.isNordicCountryByMarket(BrandUtils.MARKET_KEY_NO);

    assertThat(BrandUtils.MARKET_KEY_NO + " is not nordic", isNordic, is(true));
  }

  @Test public void testDenmarkMarketIsNordic() {
    boolean isNordic = BrandUtils.isNordicCountryByMarket(BrandUtils.MARKET_KEY_DK);

    assertThat(BrandUtils.MARKET_KEY_DK + " is not nordic", isNordic, is(true));
  }

  @Test public void testIcelandMarketIsNordic() {
    boolean isNordic = BrandUtils.isNordicCountryByMarket(BrandUtils.MARKET_KEY_IS);

    assertThat(BrandUtils.MARKET_KEY_IS + " is not nordic", isNordic, is(true));
  }

  @Test public void testPolandMarketIsNordic() {
    boolean isNordic = BrandUtils.isNordicCountryByMarket(BrandUtils.MARKET_KEY_PL);

    assertThat(BrandUtils.MARKET_KEY_PL + " is not nordic", isNordic, is(true));
  }

  @Test public void testFinlandMarketIsNordic() {
    boolean isNordic = BrandUtils.isNordicCountryByMarket(BrandUtils.MARKET_KEY_FI);

    assertThat(BrandUtils.MARKET_KEY_FI + " is not nordic", isNordic, is(true));
  }

  @Test public void testSwedenLocaleIsNordic() {
    boolean isNordic = BrandUtils.isNordicCountryByLocale(BrandUtils.LOCALE_KEY_SE);

    assertThat(BrandUtils.LOCALE_KEY_SE + " is not nordic", isNordic, is(true));
  }

  @Test public void testNorwayLocaleIsNordic() {
    boolean isNordic = BrandUtils.isNordicCountryByLocale(BrandUtils.LOCALE_KEY_NO);

    assertThat(BrandUtils.LOCALE_KEY_NO + " is not nordic", isNordic, is(true));
  }

  @Test public void testDenmarkLocaleIsNordic() {
    boolean isNordic = BrandUtils.isNordicCountryByLocale(BrandUtils.LOCALE_KEY_DK);

    assertThat(BrandUtils.LOCALE_KEY_DK + " is not nordic", isNordic, is(true));
  }

  @Test public void testIcelandLocaleIsNordic() {
    boolean isNordic = BrandUtils.isNordicCountryByLocale(BrandUtils.LOCALE_KEY_IS);

    assertThat(BrandUtils.LOCALE_KEY_IS + " is not nordic", isNordic, is(true));
  }

  @Test public void testPolandLocaleIsNordic() {
    boolean isNordic = BrandUtils.isNordicCountryByLocale(BrandUtils.LOCALE_KEY_PL);

    assertThat(BrandUtils.LOCALE_KEY_PL + " is not nordic", isNordic, is(true));
  }

  @Test public void testFinlandLocaleIsNordic() {
    boolean isNordic = BrandUtils.isNordicCountryByLocale(BrandUtils.LOCALE_KEY_FI);

    assertThat(BrandUtils.LOCALE_KEY_FI + " is not nordic", isNordic, is(true));
  }
}
