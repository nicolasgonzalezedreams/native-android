package com.odigeo.app.android.utils;

import com.odigeo.app.android.lib.utils.LocaleUtils;
import org.junit.Test;

import static com.odigeo.app.android.lib.utils.LocaleUtils.strToLocale;
import static com.odigeo.app.android.utils.BrandUtils.LOCALE_KEY_DK;
import static com.odigeo.app.android.utils.BrandUtils.LOCALE_KEY_FI;
import static com.odigeo.app.android.utils.BrandUtils.LOCALE_KEY_IS;
import static com.odigeo.app.android.utils.BrandUtils.LOCALE_KEY_NO;
import static com.odigeo.app.android.utils.BrandUtils.LOCALE_KEY_PL;
import static com.odigeo.app.android.utils.BrandUtils.LOCALE_KEY_SE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.IsNot.not;

public class LocaleUtilsTest {

  private static final long PRICE = 12345L;
  private static final String DECIMAL_SEPARATOR = ",";

  @Test public void testIcelandShouldShowPriceWithoutDecimals() {

    String localizedPrice =
        LocaleUtils.getLocalizedCurrencyValue(PRICE, strToLocale(LOCALE_KEY_IS), true);

    assertThat("Localized price is showing decimals", localizedPrice,
        not(containsString(DECIMAL_SEPARATOR)));
  }

  @Test public void testSwedenShouldShowPriceWithoutDecimals() {

    String localizedPrice =
        LocaleUtils.getLocalizedCurrencyValue(PRICE, strToLocale(LOCALE_KEY_SE), true);

    assertThat("Localized price is showing decimals", localizedPrice,
        not(containsString(DECIMAL_SEPARATOR)));
  }

  @Test public void testDenmarkShouldShowPriceWithoutDecimals() {

    String localizedPrice =
        LocaleUtils.getLocalizedCurrencyValue(PRICE, strToLocale(LOCALE_KEY_DK), true);

    assertThat("Localized price is showing decimals", localizedPrice,
        not(containsString(DECIMAL_SEPARATOR)));
  }

  @Test public void testNorwayShouldShowPriceWithoutDecimals() {

    String localizedPrice =
        LocaleUtils.getLocalizedCurrencyValue(PRICE, strToLocale(LOCALE_KEY_NO), true);

    assertThat("Localized price is showing decimals", localizedPrice,
        not(containsString(DECIMAL_SEPARATOR)));
  }

  @Test public void testPolandShouldShowPriceWithoutDecimals() {

    String localizedPrice =
        LocaleUtils.getLocalizedCurrencyValue(PRICE, strToLocale(LOCALE_KEY_PL), true);

    assertThat("Localized price is showing decimals", localizedPrice,
        not(containsString(DECIMAL_SEPARATOR)));
  }

  @Test public void testFinlandShouldShowPriceWithDecimals() {

    String localizedPrice =
        LocaleUtils.getLocalizedCurrencyValue(PRICE, strToLocale(LOCALE_KEY_FI), true);

    assertThat("Localized price is not showing decimals", localizedPrice,
        containsString(DECIMAL_SEPARATOR));
  }
}
