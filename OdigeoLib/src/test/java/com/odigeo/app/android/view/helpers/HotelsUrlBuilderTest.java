package com.odigeo.app.android.view.helpers;

import com.odigeo.app.android.lib.config.HotelsUrlBuilder;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.Section;
import com.odigeo.app.android.lib.models.dto.LocationDTO;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.test.utils.UrlParser;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.odigeo.app.android.lib.config.HotelsUrlBuilder.OPODO_NORDICS_AID;
import static com.odigeo.app.android.lib.consts.Constants.BRAND_OPODO;
import static com.odigeo.app.android.lib.consts.Constants.BRAND_TRAVELLINK;
import static com.odigeo.app.android.utils.BrandUtils.MARKET_KEY_SE;
import static com.odigeo.test.matchers.Matchers.matchesPattern;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.hamcrest.collection.IsMapContaining.hasKey;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class HotelsUrlBuilderTest {

  private static final String MOCK_SCREEN_NAME = "mytrips";
  private static final String MOCK_LABEL = "mytrips";
  private static final String MOCK_AID = "1234567890";
  private static final String MOCK_TRAVELLINK_SEARCH_URL =
      "m.hotels.travellink.se/searchresults.html";
  private static final String MOCK_TRAVELLINK_FILTER_URL = "m.hotels.travellink.se/";
  private static final String MOCK_OPODO_NORDICS_SEARCH_URL =
      "mobile-hotels.opodo.se/searchresults.html";
  private static final String MOCK_OPODO_NORDICS_FILTER_URL = "mobile-hotels.opodo.se/";
  private static final String MOCK_LANG = "SV";
  private static final String MOCK_CURRENCY = "SEK";
  private static final String MOCK_IATA = "AIRPORT";
  private static final long MOCK_DATE_MILLIS = 1494934113000L; // 5/16/2017, 1:28:33 PM
  private static final String MOCK_MONTHDAY = "16";
  private static final String MOCK_YEAR_MONTH = "2017-5";
  private static final int MOCK_NUM_ADULTS = 1;
  private static final int MOCK_NUM_KIDS = 1;

  @Mock private MarketProviderInterface marketProvider;

  @Mock private SearchOptions searchOptions;

  @Mock private FlightSegment firstSegment;

  @Mock private FlightSegment lastSegment;

  @Mock private Section section;

  @Mock private LocationDTO locationDTO;

  @Mock private Booking booking;

  @Before public void setup() {
    when(marketProvider.
        getMarketKey()).thenReturn(MARKET_KEY_SE);
    when(marketProvider.getHotelsLabel()).thenReturn(MOCK_LABEL);
    when(marketProvider.getAid()).thenReturn(MOCK_AID);
    when(marketProvider.getLanguage()).thenReturn(MOCK_LANG);
    when(marketProvider.getCurrencyKey()).thenReturn(MOCK_CURRENCY);

    when(searchOptions.getFirstSegment()).thenReturn(firstSegment);
    when(searchOptions.getLastSegment()).thenReturn(lastSegment);
    when(searchOptions.getNumberOfAdults()).thenReturn(MOCK_NUM_ADULTS);
    when(searchOptions.getNumberOfKids()).thenReturn(MOCK_NUM_KIDS);

    when(booking.getArrivalAirportCode()).thenReturn(MOCK_IATA);
    when(booking.getArrivalDate(anyInt())).thenReturn(MOCK_DATE_MILLIS);
    when(booking.getArrivalLastLeg()).thenReturn(MOCK_DATE_MILLIS);
    when(booking.getNumberOfAdults()).thenReturn(MOCK_NUM_ADULTS);
    when(booking.getNumberOfKids()).thenReturn(MOCK_NUM_KIDS);

    when(firstSegment.getLastSection()).thenReturn(section);
    when(firstSegment.getArrivalDate()).thenReturn(MOCK_DATE_MILLIS);
    when(lastSegment.getDate()).thenReturn(MOCK_DATE_MILLIS);
    when(locationDTO.getIataCode()).thenReturn(MOCK_IATA);
    when(section.getLocationTo()).thenReturn(locationDTO);
  }

  @Test public void testMyTripsTravelinkBrandHotelUrl() {

    when(marketProvider.getBrand()).thenReturn(BRAND_TRAVELLINK);
    when(marketProvider.getxSellingSearchResultsHotelsUrl()).thenReturn(MOCK_TRAVELLINK_SEARCH_URL);

    HotelsUrlBuilder hotelsUrlBuilder =
        new HotelsUrlBuilder(booking, MOCK_SCREEN_NAME, marketProvider);

    String url = hotelsUrlBuilder.getUrl();

    UrlParser parser = new UrlParser(url);
    String path = parser.getUrlParts()[0];

    assertThat("Invalid host: " + path, path, is(equalTo(MOCK_TRAVELLINK_SEARCH_URL)));
    assertThat("Invalid params", parser.getQueryStringParams(),
        allOf(hasEntry("aid", MOCK_AID), hasEntry("label", MOCK_LABEL), hasEntry("lang", MOCK_LANG),
            hasEntry("selected_currency", MOCK_CURRENCY), hasEntry("iata", MOCK_IATA),
            hasEntry("iata_orr", "1"), hasEntry("checkin_monthday", MOCK_MONTHDAY),
            hasEntry("checkin_year_month", MOCK_YEAR_MONTH),
            hasEntry("checkout_monthday", MOCK_MONTHDAY),
            hasEntry("checkout_year_month", MOCK_YEAR_MONTH)));
  }

  @Test public void testConfirmationTravelinkBrandHotelUrl() {

    when(marketProvider.getBrand()).thenReturn(BRAND_TRAVELLINK);
    when(marketProvider.getxSellingSearchResultsHotelsUrl()).thenReturn(MOCK_TRAVELLINK_SEARCH_URL);

    HotelsUrlBuilder hotelsUrlBuilder =
        new HotelsUrlBuilder(searchOptions, MOCK_SCREEN_NAME, marketProvider);

    String url = hotelsUrlBuilder.getUrl();

    UrlParser parser = new UrlParser(url);
    String path = parser.getUrlParts()[0];

    assertThat("Invalid host: " + path, path, is(equalTo(MOCK_TRAVELLINK_SEARCH_URL)));
    assertThat("Invalid params", parser.getQueryStringParams(),
        allOf(hasEntry("aid", MOCK_AID), hasEntry("label", MOCK_LABEL), hasEntry("lang", MOCK_LANG),
            hasEntry("selected_currency", MOCK_CURRENCY), hasEntry("iata", MOCK_IATA),
            hasEntry("iata_orr", "1"), hasEntry("checkin_monthday", MOCK_MONTHDAY),
            hasEntry("checkin_year_month", MOCK_YEAR_MONTH),
            hasEntry("checkout_monthday", MOCK_MONTHDAY),
            hasEntry("checkout_year_month", MOCK_YEAR_MONTH)));
  }

  @Test public void testHomeTravelinkBrandHotelUrl() {

    when(marketProvider.getBrand()).thenReturn(BRAND_TRAVELLINK);
    when(marketProvider.getxSellingFilterHotelsUrl()).thenReturn(MOCK_TRAVELLINK_FILTER_URL);

    HotelsUrlBuilder hotelsUrlBuilder =
        new HotelsUrlBuilder(false, MOCK_SCREEN_NAME, marketProvider);

    String url = hotelsUrlBuilder.getUrl();

    UrlParser parser = new UrlParser(url);
    String path = parser.getUrlParts()[0];

    assertThat("Invalid host: " + path, path, is(equalTo(MOCK_TRAVELLINK_FILTER_URL)));
    assertThat("Invalid params", parser.getQueryStringParams(),
        allOf(hasEntry("aid", MOCK_AID), hasEntry("label", MOCK_LABEL), hasEntry("lang", MOCK_LANG),
            hasEntry("selected_currency", MOCK_CURRENCY)));
  }

  @Test public void testMyTripsOpodoNordicsBrandHotelUrl() {

    when(marketProvider.getBrand()).thenReturn(BRAND_OPODO);
    when(marketProvider.getxSellingSearchResultsHotelsUrl()).thenReturn(
        MOCK_OPODO_NORDICS_SEARCH_URL);

    HotelsUrlBuilder hotelsUrlBuilder =
        new HotelsUrlBuilder(booking, MOCK_SCREEN_NAME, marketProvider);

    String url = hotelsUrlBuilder.getUrl();

    UrlParser parser = new UrlParser(url);
    String path = parser.getUrlParts()[0];

    Map<String, String> params = parser.getQueryStringParams();

    assertThat("Invalid host: " + path, path, is(equalTo(MOCK_OPODO_NORDICS_SEARCH_URL)));
    assertThat("Invalid params", parser.getQueryStringParams(),
        allOf(hasEntry("aid", OPODO_NORDICS_AID), hasKey("label"), hasEntry("lang", MOCK_LANG),
            hasEntry("selected_currency", MOCK_CURRENCY), hasEntry("iata", MOCK_IATA),
            hasEntry("iata_orr", "1"), hasEntry("checkin_monthday", MOCK_MONTHDAY),
            hasEntry("checkin_year_month", MOCK_YEAR_MONTH),
            hasEntry("checkout_monthday", MOCK_MONTHDAY),
            hasEntry("checkout_year_month", MOCK_YEAR_MONTH)));
    assertThat("Invalid label", params.get("label"),
        matchesPattern("opo-link-" + MARKET_KEY_SE + "-" + MOCK_LABEL + "-conf-appand-of"));
  }

  @Test public void testConfirmationOpodoNordicsBrandHotelUrl() {

    when(marketProvider.getBrand()).thenReturn(BRAND_OPODO);
    when(marketProvider.getxSellingSearchResultsHotelsUrl()).thenReturn(
        MOCK_OPODO_NORDICS_SEARCH_URL);

    HotelsUrlBuilder hotelsUrlBuilder =
        new HotelsUrlBuilder(booking, MOCK_SCREEN_NAME, marketProvider);

    String url = hotelsUrlBuilder.getUrl();

    UrlParser parser = new UrlParser(url);
    String path = parser.getUrlParts()[0];

    Map<String, String> params = parser.getQueryStringParams();

    assertThat("Invalid host: " + path, path, is(equalTo(MOCK_OPODO_NORDICS_SEARCH_URL)));
    assertThat("Invalid params", parser.getQueryStringParams(),
        allOf(hasEntry("aid", OPODO_NORDICS_AID), hasKey("label"), hasEntry("lang", MOCK_LANG),
            hasEntry("selected_currency", MOCK_CURRENCY), hasEntry("iata", MOCK_IATA),
            hasEntry("iata_orr", "1"), hasEntry("checkin_monthday", MOCK_MONTHDAY),
            hasEntry("checkin_year_month", MOCK_YEAR_MONTH),
            hasEntry("checkout_monthday", MOCK_MONTHDAY),
            hasEntry("checkout_year_month", MOCK_YEAR_MONTH)));
    assertThat("Invalid label", params.get("label"),
        matchesPattern("opo-link-" + MARKET_KEY_SE + "-" + MOCK_LABEL + "-conf-appand-of"));
  }

  @Test public void testHomeOpodoNordicsBrandHotelUrl() {

    when(marketProvider.getBrand()).thenReturn(BRAND_OPODO);
    when(marketProvider.getxSellingFilterHotelsUrl()).thenReturn(MOCK_OPODO_NORDICS_FILTER_URL);

    HotelsUrlBuilder hotelsUrlBuilder =
        new HotelsUrlBuilder(false, MOCK_SCREEN_NAME, marketProvider);

    String url = hotelsUrlBuilder.getUrl();

    UrlParser parser = new UrlParser(url);
    String path = parser.getUrlParts()[0];

    Map<String, String> params = parser.getQueryStringParams();

    assertThat("Invalid host: " + path, path, is(equalTo(MOCK_OPODO_NORDICS_FILTER_URL)));
    assertThat("Invalid params", parser.getQueryStringParams(),
        allOf(hasEntry("aid", OPODO_NORDICS_AID), hasKey("label"), hasEntry("lang", MOCK_LANG),
            hasEntry("selected_currency", MOCK_CURRENCY)));
    assertThat("Invalid label", params.get("label"),
        matchesPattern("opo-link-" + MARKET_KEY_SE + "-" + MOCK_LABEL + "-conf-appand-of"));
  }

  @Test public void testShouldAddGroupsParams() {
    when(marketProvider.getBrand()).thenReturn(BRAND_OPODO);
    when(marketProvider.getxSellingSearchResultsHotelsUrl()).thenReturn(
        MOCK_OPODO_NORDICS_SEARCH_URL);

    when(searchOptions.getNumberOfAdults()).thenReturn(MOCK_NUM_ADULTS);
    when(searchOptions.getNumberOfAdults()).thenReturn(MOCK_NUM_KIDS);

    HotelsUrlBuilder hotelsUrlBuilder =
        new HotelsUrlBuilder(searchOptions, MOCK_SCREEN_NAME, marketProvider);

    String url = hotelsUrlBuilder.getUrl();

    UrlParser parser = new UrlParser(url);
    Map<String, String> params = parser.getQueryStringParams();

    assertThat("Invalid adults param", params,
        allOf(hasEntry("group_adults", "1"), hasEntry("group_children", "1")));
  }

  @Test public void testRoomNumberAccordingToAdultsNumber() {
    when(marketProvider.getBrand()).thenReturn(BRAND_OPODO);
    when(marketProvider.getxSellingSearchResultsHotelsUrl()).thenReturn(
        MOCK_OPODO_NORDICS_SEARCH_URL);

    when(searchOptions.getNumberOfAdults()).thenReturn(MOCK_NUM_ADULTS);

    HotelsUrlBuilder hotelsUrlBuilder =
        new HotelsUrlBuilder(searchOptions, MOCK_SCREEN_NAME, marketProvider);

    hotelsUrlBuilder.setRoomsForAdultsNumber(1);
    String url = hotelsUrlBuilder.getUrl();
    UrlParser parser = new UrlParser(url);
    Map<String, String> params = parser.getQueryStringParams();
    assertThat("1-4 adults should be one room", params, hasEntry("no_rooms", "1"));

    hotelsUrlBuilder.setRoomsForAdultsNumber(2);
    url = hotelsUrlBuilder.getUrl();
    parser = new UrlParser(url);
    params = parser.getQueryStringParams();
    assertThat("1-4 adults should be one room", params, hasEntry("no_rooms", "1"));

    hotelsUrlBuilder.setRoomsForAdultsNumber(3);
    url = hotelsUrlBuilder.getUrl();
    parser = new UrlParser(url);
    params = parser.getQueryStringParams();
    assertThat("1-4 adults should be one room", params, hasEntry("no_rooms", "1"));

    hotelsUrlBuilder.setRoomsForAdultsNumber(4);
    url = hotelsUrlBuilder.getUrl();
    parser = new UrlParser(url);
    params = parser.getQueryStringParams();
    assertThat("1-4 adults should be one room", params, hasEntry("no_rooms", "1"));

    hotelsUrlBuilder.setRoomsForAdultsNumber(5);
    url = hotelsUrlBuilder.getUrl();
    parser = new UrlParser(url);
    params = parser.getQueryStringParams();
    assertThat("5 adults should be two rooms", params, hasEntry("no_rooms", "2"));

    hotelsUrlBuilder.setRoomsForAdultsNumber(6);
    url = hotelsUrlBuilder.getUrl();
    parser = new UrlParser(url);
    params = parser.getQueryStringParams();
    assertThat("6-7 adults should be three rooms", params, hasEntry("no_rooms", "3"));

    hotelsUrlBuilder.setRoomsForAdultsNumber(7);
    url = hotelsUrlBuilder.getUrl();
    parser = new UrlParser(url);
    params = parser.getQueryStringParams();
    assertThat("6-7 adults should be three rooms", params, hasEntry("no_rooms", "3"));

    hotelsUrlBuilder.setRoomsForAdultsNumber(8);
    url = hotelsUrlBuilder.getUrl();
    parser = new UrlParser(url);
    params = parser.getQueryStringParams();
    assertThat("8-9 adults should be four rooms", params, hasEntry("no_rooms", "4"));

    hotelsUrlBuilder.setRoomsForAdultsNumber(9);
    url = hotelsUrlBuilder.getUrl();
    parser = new UrlParser(url);
    params = parser.getQueryStringParams();
    assertThat("8-9 adults should be four rooms", params, hasEntry("no_rooms", "4"));

    hotelsUrlBuilder.setRoomsForAdultsNumber(10);
    url = hotelsUrlBuilder.getUrl();
    parser = new UrlParser(url);
    params = parser.getQueryStringParams();
    assertThat("Anything else should default to 1", params, hasEntry("no_rooms", "1"));

    hotelsUrlBuilder.setRoomsForAdultsNumber(0);
    url = hotelsUrlBuilder.getUrl();
    parser = new UrlParser(url);
    params = parser.getQueryStringParams();
    assertThat("Anything else should default to 1", params, hasEntry("no_rooms", "1"));
  }
}
