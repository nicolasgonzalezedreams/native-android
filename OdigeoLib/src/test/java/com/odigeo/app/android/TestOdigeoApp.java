package com.odigeo.app.android;

import android.content.Context;
import com.odigeo.DependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.navigator.NavigationDrawerNavigator;

public class TestOdigeoApp extends OdigeoApp {

  private DependencyInjector dependencyInjector;

  @Override public void onCreate() {
    super.onCreate();
  }

  @Override protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
  }

  @Override protected void initConfiguration() {

  }

  @Override public DependencyInjector getDependencyInjector() {
    if (dependencyInjector == null) {
      dependencyInjector = new TestDependencyInjector();
    }

    return dependencyInjector;
  }

  @Override public void trackNonFatal(Throwable throwable) {

  }

  @Override public void setBool(String key, boolean value) {

  }

  @Override public void setDouble(String key, double value) {

  }

  @Override public void setFloat(String key, float value) {

  }

  @Override public void setInt(String key, int value) {

  }

  @Override public void setString(String key, String value) {

  }

  @Override public void setLong(String key, long value) {

  }

  @Override protected void setFonts() {

  }

  @Override public Class getHomeActivity() {
    return NavigationDrawerNavigator.class;
  }

  @Override public Class getSearchFlightsActivityClass() {
    return null;
  }

  @Override public Class getCalendarActivityClass() {
    return null;
  }

  @Override public Class getDestinationActivityClass() {
    return null;
  }

  @Override public Class getSearchResultsActivityClass() {
    return null;
  }

  @Override public Class getFiltersActivityClass() {
    return null;
  }

  @Override public Class getInsurancesActivityClass() {
    return null;
  }

  @Override public Class getInsurancesConditionsActivityClass() {
    return null;
  }

  @Override public Class getSummaryActivityClass() {
    return null;
  }

  @Override public Class getMyInfoActivityClass() {
    return null;
  }

  @Override public Class getMyTripDetailsActivityClass() {
    return null;
  }

  @Override public Class getMyTripsActivityClass() {
    return null;
  }

  @Override public Class getSettingsActivityClass() {
    return null;
  }

  @Override public Class getPassengersActivityClass() {
    return null;
  }

  @Override public Class getPaymentActivityClass() {
    return null;
  }

  @Override public Class getPixelWebViewActivityClass() {
    return null;
  }

  @Override public Class getConfirmationActivityClass() {
    return null;
  }

  @Override public Class getNoConnectionClass() {
    return null;
  }

  @Override public Class getWebViewActivityClass() {
    return null;
  }

  @Override public Class getBookingDuplicatedActivityClass() {
    return null;
  }

  @Override public Class getFrequentFlyer() {
    return null;
  }

  @Override public Class getQAModeActivityClass() {
    return null;
  }

  @Override public Class getWalkthroughActivity() {
    return null;
  }

  @Override public Class getWidgetClass() {
    return null;
  }

  @Override public void setCrashlyticValue(String key, String value) {

  }

  @Override public void initializeCrashlytics() {

  }

  @Override public boolean hasAnimatedBackground() {
    return false;
  }

  @Override public Class getNavigationDrawerNavigator() {
    return null;
  }
}
