package com.odigeo.app.android.view.helpers;

import com.odigeo.app.android.helper.CrossCarUrlHandler;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.helper.CrossCarUrlHandlerInterface;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.tools.DateHelperInterface;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.odigeo.app.android.lib.consts.Constants.BRAND_TRAVELLINK;
import static com.odigeo.app.android.utils.BrandUtils.MARKET_KEY_DK;
import static com.odigeo.app.android.utils.BrandUtils.MARKET_KEY_FI;
import static com.odigeo.app.android.utils.BrandUtils.MARKET_KEY_UK;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class CrossCarUrlHandlerTest {

  private static final String MOCK_ARRIVAL = "arrival";
  private static final long MOCK_ARRIVAL_DATE = 123456;
  private static final long MOCK_BIRTHDATE = 18051990;
  private static final long MOCK_DEPARTURE_DATE = 7890;

  @Mock private MarketProviderInterface market;

  @Mock private DateHelperInterface dateHelper;

  private CrossCarUrlHandler crossCarUrlHandler;

  @Before public void setup() {
    crossCarUrlHandler = new CrossCarUrlHandler(market, dateHelper);
  }

  @Test public void testGetCarUrlAddsCommonBasicParams() {

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(Constants.BRAND_EDREAMS);

    crossCarUrlHandler.getCarUrl(CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    Map<String, String> map = crossCarUrlHandler.getUrlParams();

    assertThat("Missing prefLang param", true,
        allOf(is(map.containsKey(CrossCarUrlHandler.PREF_LANG)),
            is(map.containsValue(market.getLanguage()))));

    assertThat("Missing cor param", true, allOf(is(map.containsKey(CrossCarUrlHandler.COR)),
        is(map.containsValue(market.getCrossSellingMarketKey()))));

    assertThat("Missing prefcurrency param", true,
        allOf(is(map.containsKey(CrossCarUrlHandler.PREF_CURRENCY)),
            is(map.containsValue(market.getCurrencyKey()))));

    assertThat("Missing adcamp param", true, allOf(is(map.containsKey(CrossCarUrlHandler.AD_CAMP)),
        is(map.containsValue(CrossCarUrlHandlerInterface.CAMPAIGN_HEADER))));
  }

  @Test public void testGetCarUrlAddsBasicParamsTravellinkBrand() {

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(BRAND_TRAVELLINK);

    crossCarUrlHandler.getCarUrl(CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    Map<String, String> map = crossCarUrlHandler.getUrlParams();

    assertThat("Missing adplat param", true, allOf(is(map.containsKey(CrossCarUrlHandler.AD_PLAT)),
        is(map.containsValue(market.getLanguage()))));

    assertThat("Missing affiliateCode param", true,
        allOf(is(map.containsKey(CrossCarUrlHandler.AFFILIATE_CODE)),
            is(map.containsValue(market.getCrossSellingMarketKey()))));
  }

  @Test public void testGetCarUrlForTLPlatformAddsRightParams() {

    when(market.getLanguage()).thenReturn("de");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_DK);
    when(market.getCurrencyKey()).thenReturn("de");
    when(market.getBrand()).thenReturn(BRAND_TRAVELLINK);

    crossCarUrlHandler.getCarUrl(CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    Map<String, String> map = crossCarUrlHandler.getUrlParams();

    assertThat("Missing Travelink params", true,
        allOf(is(map.containsKey(CrossCarUrlHandler.AD_PLAT)),
            is(map.containsValue(CrossCarUrlHandler.PLAT_CONNECTAPP)),
            is(map.containsKey(CrossCarUrlHandler.AFFILIATE_CODE)),
            is(map.containsValue(BRAND_TRAVELLINK.toLowerCase()))));
  }

  @Test public void testGetCarUrlForOpodoMarketAddsRightParams() {
    when(market.getLanguage()).thenReturn("en");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("en");
    when(market.getBrand()).thenReturn(Constants.BRAND_OPODO);

    crossCarUrlHandler.getCarUrl(CrossCarUrlHandlerInterface.CAMPAIGN_MY_TRIPS);

    Map<String, String> map = crossCarUrlHandler.getUrlParams();

    assertThat("Missing Opodo market params", true,
        allOf(is(map.containsKey(CrossCarUrlHandler.AD_PLAT)),
            is(map.containsValue(CrossCarUrlHandler.PLAT_CONNECTAPP))));
  }

  @Test public void testGetCarUrlForOpodoNordicsAndNotHomeCompaingMarketAddsRightParams() {
    when(market.getLanguage()).thenReturn("fi");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_FI);
    when(market.getCurrencyKey()).thenReturn("fi");
    when(market.getBrand()).thenReturn(Constants.BRAND_OPODO);

    crossCarUrlHandler.getCarUrl(CrossCarUrlHandlerInterface.CAMPAIGN_MY_TRIPS);

    Map<String, String> map = crossCarUrlHandler.getUrlParams();

    assertThat("Missing params for Opodo market when not tracking home campaign", true,
        allOf(is(map.containsKey(CrossCarUrlHandler.AD_PLAT)),
            is(map.containsValue(CrossCarUrlHandler.PLAT_CONNECTAPP)),
            is(map.containsKey(CrossCarUrlHandler.AFFILIATE_CODE)),
            is(map.containsValue(CrossCarUrlHandler.OPODO_AFFILIATE_CODE))));
  }

  @Test public void testGetCarUrlForOpodoNordicsHomeCampaignMarketAddsRightParams() {
    when(market.getLanguage()).thenReturn("fi");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_FI);
    when(market.getCurrencyKey()).thenReturn("fi");
    when(market.getBrand()).thenReturn(Constants.BRAND_OPODO);

    crossCarUrlHandler.getCarUrl(CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    Map<String, String> map = crossCarUrlHandler.getUrlParams();

    assertThat("Missing params for Opodo market when tracking home campaign", true,
        allOf(is(map.containsKey(CrossCarUrlHandler.AD_PLAT)),
            is(map.containsValue(CrossCarUrlHandler.PLAT_CONNECTAPP)),
            is(not(map.containsKey(CrossCarUrlHandler.AFFILIATE_CODE))),
            is(not(map.containsValue(CrossCarUrlHandler.OPODO_AFFILIATE_CODE)))));
  }

  @Test public void testGetCarUrlForUkMarketAddsRightParams() {
    when(market.getLanguage()).thenReturn("en");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("en");
    when(market.getBrand()).thenReturn(Constants.BRAND_EDREAMS);

    crossCarUrlHandler.getCarUrl(CrossCarUrlHandlerInterface.CAMPAIGN_MY_TRIPS);

    Map<String, String> map = crossCarUrlHandler.getUrlParams();

    assertThat("Missing UK market params", true,
        allOf(is(map.containsKey(CrossCarUrlHandler.AD_PLAT)),
            is(map.containsValue(CrossCarUrlHandler.PLAT_CONNECTAPP)),
            is(map.containsKey(CrossCarUrlHandler.ENABLER)),
            is(map.containsValue(CrossCarUrlHandler.ENABLER_PROMOAPP))));
  }

  @Test public void testGetCarUrlWithSearchLoaderSetsPickupDateParams() {

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(Constants.BRAND_EDREAMS);

    crossCarUrlHandler.getCarUrlWithSearchLoader(MOCK_ARRIVAL, MOCK_ARRIVAL_DATE,
        MOCK_DEPARTURE_DATE, MOCK_BIRTHDATE, CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    Map<String, String> map = crossCarUrlHandler.getUrlParams();

    assertThat("Missing a PickUp param", true,
        allOf(is(map.containsKey(CrossCarUrlHandler.PICKUP_YEAR)),
            is(map.containsKey(CrossCarUrlHandler.PICKUP_MONTH)),
            is(map.containsKey(CrossCarUrlHandler.PICKUP_DAY)),
            is(map.containsKey(CrossCarUrlHandler.PICKUP_HOUR)),
            is(map.containsKey(CrossCarUrlHandler.PICKUP_MINUTE))));
  }

  @Test public void testGetCarUrlWithSearchLoaderSetsAgeParams() {

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(Constants.BRAND_EDREAMS);

    crossCarUrlHandler.getCarUrlWithSearchLoader(MOCK_ARRIVAL, MOCK_ARRIVAL_DATE,
        MOCK_DEPARTURE_DATE, MOCK_BIRTHDATE, CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    Map<String, String> map = crossCarUrlHandler.getUrlParams();

    assertThat("Missing Driver Age param", true,
        is(map.containsKey(CrossCarUrlHandler.DRIVER_AGE)));
  }

  @Test public void testGetCarUrlWithSearchLoaderSetsFromParams() {

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(Constants.BRAND_EDREAMS);

    crossCarUrlHandler.getCarUrlWithSearchLoader(MOCK_ARRIVAL, MOCK_ARRIVAL_DATE,
        MOCK_DEPARTURE_DATE, MOCK_BIRTHDATE, CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    Map<String, String> map = crossCarUrlHandler.getUrlParams();

    assertThat("Missing From param", true, is(map.containsKey(CrossCarUrlHandler.FROM)));
  }

  @Test public void testGetCarUrlWithSearchLoaderSetsToParams() {

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(Constants.BRAND_EDREAMS);

    crossCarUrlHandler.getCarUrlWithSearchLoader(MOCK_ARRIVAL, 1, 2, MOCK_BIRTHDATE,
        CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    Map<String, String> map = crossCarUrlHandler.getUrlParams();

    assertThat("Missing To param", true, is(map.containsKey(CrossCarUrlHandler.TO)));
  }

  @Test public void testGetCarUrlWithSearchLoaderSetsForceMobileParams() {

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(Constants.BRAND_EDREAMS);

    crossCarUrlHandler.getCarUrlWithSearchLoader(MOCK_ARRIVAL, MOCK_ARRIVAL_DATE,
        MOCK_DEPARTURE_DATE, MOCK_BIRTHDATE, CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    Map<String, String> map = crossCarUrlHandler.getUrlParams();

    assertThat("Missing ForceMobile param", true,
        is(map.containsKey(CrossCarUrlHandler.FORCE_MOBILE)));
  }

  @Test public void testGetCarUrlWithSearchLoaderSetsDropoffDateParams() {

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(Constants.BRAND_EDREAMS);

    crossCarUrlHandler.getCarUrlWithSearchLoader(MOCK_ARRIVAL, MOCK_ARRIVAL_DATE,
        MOCK_DEPARTURE_DATE, MOCK_BIRTHDATE, CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    Map<String, String> map = crossCarUrlHandler.getUrlParams();

    assertThat("Missing a DropoffDate param", true,
        allOf(is(map.containsKey(CrossCarUrlHandler.DROPOFF_YEAR)),
            is(map.containsKey(CrossCarUrlHandler.DROPOFF_MONTH)),
            is(map.containsKey(CrossCarUrlHandler.DROPOFF_DAY)),
            is(map.containsKey(CrossCarUrlHandler.DROPOFF_HOUR)),
            is(map.containsKey(CrossCarUrlHandler.DROPOFF_MINUTE))));
  }

  @Test public void testGetCarUrlWithSearchLoaderSetsDropoffDateWithDepartureDateParams() {

    final long arrivalDate = 1;
    final long departureDate = 2;

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(Constants.BRAND_EDREAMS);

    crossCarUrlHandler.getCarUrlWithSearchLoader(MOCK_ARRIVAL, arrivalDate, departureDate,
        MOCK_BIRTHDATE, CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    verify(dateHelper).millisecondsLeastOffset(departureDate);
  }

  @Test
  public void testGetCarUrlWithSearchLoaderSetsDropoffDateWithDepartureDatePlusOneWayWithCarsParams() {

    final long arrivalDate = 2;
    final long departureDate = 1;
    final long departureDatePlusOneWayWithCars =
        arrivalDate + CrossCarUrlHandler.ONE_WAY_TIME_WITH_CARS;

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(Constants.BRAND_EDREAMS);

    crossCarUrlHandler.getCarUrlWithSearchLoader(MOCK_ARRIVAL, arrivalDate, departureDate,
        MOCK_BIRTHDATE, CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    verify(dateHelper).millisecondsLeastOffset(departureDatePlusOneWayWithCars);
  }

  @Test public void testGetCarUrlGetsRightBaseUrlForEDreamsBrand() {

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(Constants.BRAND_EDREAMS);

    String url = crossCarUrlHandler.getCarUrl(CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    assertThat("Wrong eDreams cars base url", url.startsWith(CrossCarUrlHandler.HOST_URL_EDREAMS),
        is(true));
  }

  @Test public void testGetCarUrlGetsRightBaseUrlForTravellinkBrand() {

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(BRAND_TRAVELLINK);

    String url = crossCarUrlHandler.getCarUrl(CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    assertThat("Wrong Travellink cars base url",
        url.startsWith(CrossCarUrlHandler.HOST_URL_TRAVELLINK), is(true));
  }

  @Test public void testGetCarUrlGetsRightBaseUrlForGoVoyagesBrand() {

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(Constants.BRAND_GOVOYAGES);

    String url = crossCarUrlHandler.getCarUrl(CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    assertThat("Wrong GoVoyages cars base url",
        url.startsWith(CrossCarUrlHandler.HOST_URL_GOVOYAGE), is(true));
  }

  @Test public void testGetCarUrlGetsRightBaseUrlForOpodoBrand() {

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(Constants.BRAND_OPODO);

    String url = crossCarUrlHandler.getCarUrl(CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    assertThat("Wrong Opodo cars base url", url.startsWith(CrossCarUrlHandler.HOST_URL_OPODO_UK),
        is(true));
  }

  @Test public void testGetCarUrlGetsRightBaseUrlForOpodoNordicBrand() {

    when(market.getLanguage()).thenReturn("fi");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_FI);
    when(market.getCurrencyKey()).thenReturn("fi");
    when(market.getBrand()).thenReturn(Constants.BRAND_OPODO);

    String url = crossCarUrlHandler.getCarUrl(CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    assertThat("Wrong OpodoNordic cars base url",
        url.startsWith(CrossCarUrlHandler.HOST_URL_OPODO_NORDICS), is(true));
  }

  @Test public void testGetCarUrlWithSearchLoaderGetsRightBaseUrlForEDreamsBrand() {

    String searchUrlPrefix = CrossCarUrlHandler.HOST_URL_EDREAMS + CrossCarUrlHandler.SEARCH_LOADER;

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(Constants.BRAND_EDREAMS);

    String url = crossCarUrlHandler.getCarUrlWithSearchLoader(MOCK_ARRIVAL, MOCK_ARRIVAL_DATE,
        MOCK_DEPARTURE_DATE, MOCK_BIRTHDATE, CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    assertThat("Wrong eDreams cars base url", url.startsWith(searchUrlPrefix), is(true));
  }

  @Test public void testGetCarUrlWithSearchLoaderGetsRightBaseUrlForTravellinkBrand() {

    String searchUrlPrefix =
        CrossCarUrlHandler.HOST_URL_TRAVELLINK + CrossCarUrlHandler.SEARCH_LOADER;

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(BRAND_TRAVELLINK);

    String url = crossCarUrlHandler.getCarUrlWithSearchLoader(MOCK_ARRIVAL, MOCK_ARRIVAL_DATE,
        MOCK_DEPARTURE_DATE, MOCK_BIRTHDATE, CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    assertThat("Wrong Travellink cars base url", url.startsWith(searchUrlPrefix), is(true));
  }

  @Test public void testGetCarUrlWithSearchLoaderGetsGetsRightBaseUrlForGoVoyagesBrand() {

    String searchUrlPrefix =
        CrossCarUrlHandler.HOST_URL_GOVOYAGE + CrossCarUrlHandler.SEARCH_LOADER;

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(Constants.BRAND_GOVOYAGES);

    String url = crossCarUrlHandler.getCarUrlWithSearchLoader(MOCK_ARRIVAL, MOCK_ARRIVAL_DATE,
        MOCK_DEPARTURE_DATE, MOCK_BIRTHDATE, CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    assertThat("Wrong GoVoyages cars base url", url.startsWith(searchUrlPrefix), is(true));
  }

  @Test public void testGetCarUrlWithSearchLoaderGetsRightBaseUrlForOpodoBrand() {

    String searchUrlPrefix =
        CrossCarUrlHandler.HOST_URL_OPODO_UK + CrossCarUrlHandler.SEARCH_LOADER;

    when(market.getLanguage()).thenReturn("uk");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_UK);
    when(market.getCurrencyKey()).thenReturn("uk");
    when(market.getBrand()).thenReturn(Constants.BRAND_OPODO);

    String url = crossCarUrlHandler.getCarUrlWithSearchLoader(MOCK_ARRIVAL, MOCK_ARRIVAL_DATE,
        MOCK_DEPARTURE_DATE, MOCK_BIRTHDATE, CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    assertThat("Wrong Opodo cars base url", url.startsWith(searchUrlPrefix), is(true));
  }

  @Test public void testGetCarUrlWithSearchLoaderGetsRightBaseUrlForOpodoNordicBrand() {

    String searchUrlPrefix =
        CrossCarUrlHandler.HOST_URL_OPODO_NORDICS + CrossCarUrlHandler.SEARCH_LOADER;

    when(market.getLanguage()).thenReturn("fi");
    when(market.getCrossSellingMarketKey()).thenReturn(MARKET_KEY_FI);
    when(market.getCurrencyKey()).thenReturn("fi");
    when(market.getBrand()).thenReturn(Constants.BRAND_OPODO);

    String url = crossCarUrlHandler.getCarUrlWithSearchLoader(MOCK_ARRIVAL, MOCK_ARRIVAL_DATE,
        MOCK_DEPARTURE_DATE, MOCK_BIRTHDATE, CrossCarUrlHandlerInterface.CAMPAIGN_HEADER);

    assertThat("Wrong OpodoNordic cars base url", url.startsWith(searchUrlPrefix), is(true));
  }
}
