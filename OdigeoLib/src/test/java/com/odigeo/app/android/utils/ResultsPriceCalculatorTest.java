package com.odigeo.app.android.utils;

import com.odigeo.app.android.lib.models.CollectionEstimationFees;
import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import com.odigeo.app.android.lib.models.dto.CollectionMethodDTO;
import com.odigeo.app.android.lib.models.dto.CollectionMethodTypeDTO;
import com.odigeo.app.android.lib.models.dto.CreditCardTypeDTO;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;
import com.odigeo.app.android.lib.models.dto.FareItineraryPriceCalculatorDTO;
import com.odigeo.app.android.lib.models.dto.FeeDetailsDTO;
import com.odigeo.app.android.lib.models.dto.FeeInfoDTO;
import com.odigeo.app.android.lib.models.dto.MembershipPerksDTO;
import com.odigeo.app.android.lib.utils.ResultsPriceCalculator;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class ResultsPriceCalculatorTest {

  private FareItineraryDTO fareItineraryDTO;
  private FareItineraryDTOMocks fareItineraryDTOMocks;
  private CollectionMethodWithPrice collectionMethodWithPrice;

  private static String CREDIT_CARD_CODE = "342432";
  private static String CREDIT_CARD_NAME = "VISO";
  private static BigDecimal CREDIT_CARD_FEE = BigDecimal.valueOf(5);
  private static BigDecimal SORT_PRICE = BigDecimal.valueOf(100);
  private static BigDecimal MEMBERSHIP_PERKS = BigDecimal.valueOf(-12);
  private static String LOCALE_ES = "es_ES";
  private static String LOCALE_US = "en_US";

  @Before public void setUp() {
    fareItineraryDTOMocks = new FareItineraryDTOMocks();
    fareItineraryDTO = fareItineraryDTOMocks.provideFareItineraryDTO();
    collectionMethodWithPrice = fareItineraryDTOMocks.provideCollectionMethodWithPrice();
  }

  @Test public void calculatePriceNoSelectedMethodTest() {
    double result = ResultsPriceCalculator.calculatePricePerPassenger(fareItineraryDTO, null, 1);
    String resultText = ResultsPriceCalculator.calculatePrice(fareItineraryDTO, null, 1, LOCALE_ES);
    assertEquals(result, 95d);
    assertEquals(resultText, "95,00 €");
  }

  @Test public void calculatePriceWithSelectedMethodTest() {
    double result = ResultsPriceCalculator.calculatePricePerPassenger(fareItineraryDTO,
        collectionMethodWithPrice, 1);
    String resultText =
        ResultsPriceCalculator.calculatePrice(fareItineraryDTO, collectionMethodWithPrice, 1,
            LOCALE_ES);
    assertEquals(result, 100d);
    assertEquals(resultText, "100,00 €");
  }

  @Test public void calculatePriceNoSelectedMethodMoreThanOnePassengerTest() {
    double result = ResultsPriceCalculator.calculatePricePerPassenger(fareItineraryDTO, null, 2);
    String resultText = ResultsPriceCalculator.calculatePrice(fareItineraryDTO, null, 2, LOCALE_ES);

    assertEquals(result, 47.5d);
    assertEquals(resultText, "47,50 €");
  }

  @Test public void calculatePriceWithSelectedMethodMoreThanOnePassengerTest() {
    double result = ResultsPriceCalculator.calculatePricePerPassenger(fareItineraryDTO,
        collectionMethodWithPrice, 2);
    String resultText =
        ResultsPriceCalculator.calculatePrice(fareItineraryDTO, collectionMethodWithPrice, 2,
            LOCALE_ES);

    assertEquals(result, 50d);
    assertEquals(resultText, "50,00 €");
  }

  @Test public void calculatePriceEN_USLocaleTest() {
    double result = ResultsPriceCalculator.calculatePricePerPassenger(fareItineraryDTO,
        collectionMethodWithPrice, 2);
    String resultText =
        ResultsPriceCalculator.calculatePrice(fareItineraryDTO, collectionMethodWithPrice, 2,
            LOCALE_US);

    assertEquals(result, 50d);
    assertEquals(resultText, "$ 50.00");
  }

  @Test public void calculatePriceWithMembershipPerks() {
    fareItineraryDTO = fareItineraryDTOMocks.provideFareItineraryDTOWithMembershipPerks();
    double result = ResultsPriceCalculator.calculatePricePerPassenger(fareItineraryDTO,
        collectionMethodWithPrice, 2);

    String resultText = ResultsPriceCalculator.calculatePrice(fareItineraryDTO,
        collectionMethodWithPrice, 2, LOCALE_ES);

    String resultTextSlashed = ResultsPriceCalculator.calculateSlashedPrice(fareItineraryDTO,
        collectionMethodWithPrice, 2, LOCALE_ES);

    assertEquals(result, 50d);
    assertEquals(resultText, "50,00 €");
    assertEquals(resultTextSlashed, "56,00 €");
  }

  public class FareItineraryDTOMocks {

    public FareItineraryDTO provideFareItineraryDTO() {
      FareItineraryDTO fareItineraryDTO = new FareItineraryDTO();
      fareItineraryDTO.setKey("Key");
      fareItineraryDTO.setCollectionMethodFees(0);
      fareItineraryDTO.setBaggageFees(0);
      fareItineraryDTO.setResident(false);
      fareItineraryDTO.setItinerary(0);
      fareItineraryDTO.setPrice(provideFareItineraryPriceCalculatorDTO());
      fareItineraryDTO.setCollectionMethodFeesObject(provideCollectionEstimationFees());

      return fareItineraryDTO;
    }

    public FareItineraryDTO provideFareItineraryDTOWithMembershipPerks() {
      FareItineraryDTO fareItineraryDTO = provideFareItineraryDTO();
      fareItineraryDTO.setMembershipPerks(provideMembershipPerks());

      return fareItineraryDTO;
    }

    private MembershipPerksDTO provideMembershipPerks() {
      MembershipPerksDTO membershipPerksDTO = new MembershipPerksDTO();
      membershipPerksDTO.setFee(MEMBERSHIP_PERKS);
      return membershipPerksDTO;
    }

    private CollectionEstimationFees provideCollectionEstimationFees() {
      CollectionEstimationFees collectionEstimationFees = new CollectionEstimationFees();
      collectionEstimationFees.setId(1234);
      collectionEstimationFees.setCheapest(provideCollectionMethodWithPrice());
      collectionEstimationFees.setCollectionMethodFees(provideCollectionMethodFees());
      return collectionEstimationFees;
    }

    private Map<String, CollectionMethodWithPrice> provideCollectionMethodFees() {
      Map<String, CollectionMethodWithPrice> collectionMethods = new HashMap<>();
      collectionMethods.put(CREDIT_CARD_CODE, provideCollectionMethodWithPrice());
      return collectionMethods;
    }

    private CollectionMethodWithPrice provideCollectionMethodWithPrice() {
      CollectionMethodWithPrice collectionMethodWithPrice = new CollectionMethodWithPrice();
      collectionMethodWithPrice.setCheapest(true);
      collectionMethodWithPrice.setImageId(0);
      collectionMethodWithPrice.setPrice(CREDIT_CARD_FEE);
      collectionMethodWithPrice.setCollectionMethodKey(0);
      collectionMethodWithPrice.setCollectionMethod(provideCollectionMethod());

      return collectionMethodWithPrice;
    }

    private CollectionMethodDTO provideCollectionMethod() {
      CollectionMethodDTO collectionMethodDTO = new CollectionMethodDTO();
      collectionMethodDTO.setCreditCardType(provideCreditCardType());
      collectionMethodDTO.setType(CollectionMethodTypeDTO.CREDITCARD);

      return collectionMethodDTO;
    }

    private CreditCardTypeDTO provideCreditCardType() {
      CreditCardTypeDTO creditCardTypeDTO = new CreditCardTypeDTO();
      creditCardTypeDTO.setCode(CREDIT_CARD_CODE);
      creditCardTypeDTO.setName(CREDIT_CARD_NAME);

      return creditCardTypeDTO;
    }

    private FareItineraryPriceCalculatorDTO provideFareItineraryPriceCalculatorDTO() {
      FareItineraryPriceCalculatorDTO fareItineraryPriceCalculatorDTO =
          new FareItineraryPriceCalculatorDTO();
      fareItineraryPriceCalculatorDTO.setMarkup(BigDecimal.valueOf(10));
      fareItineraryPriceCalculatorDTO.setSortPrice(SORT_PRICE);
      fareItineraryPriceCalculatorDTO.setFeeInfo(provideFeeInfo());

      return fareItineraryPriceCalculatorDTO;
    }

    private FeeInfoDTO provideFeeInfo() {
      FeeInfoDTO feeInfoDTO = new FeeInfoDTO();
      feeInfoDTO.setPaymentFee(provideFeeDetails());
      feeInfoDTO.setSearchFee(provideFeeDetails());

      return feeInfoDTO;
    }

    private FeeDetailsDTO provideFeeDetails() {
      FeeDetailsDTO feeDetailsDTO = new FeeDetailsDTO();
      feeDetailsDTO.setTax(BigDecimal.valueOf(0));
      feeDetailsDTO.setDiscount(BigDecimal.valueOf(0));

      return feeDetailsDTO;
    }
  }
}
