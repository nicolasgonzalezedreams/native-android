package com.odigeo.app.android.view.validators.creditcards;

import com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators.JCBValidator;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class JCBValidatorTest {

  @Test public void shouldValidateAsJCB_16_digits() {
    String jcb = "3528139789613588";
    assertTrue(jcb.matches(JCBValidator.REGEX_JCB));
  }

  @Test public void shouldValidateAsJCB_15_digits_with_21_at_start() {
    String jcb = "212813978961358";
    assertTrue(jcb.matches(JCBValidator.REGEX_JCB));
  }

  @Test public void shouldValidateAsJCB_15_digits_with_18_at_start() {
    String jcb = "182813978961358";
    assertTrue(jcb.matches(JCBValidator.REGEX_JCB));
  }

  @Test public void shouldNotValidateAsJCB_15_digits_with_no_21_18_at_start() {
    String jcb = "252813978961358";
    assertFalse(jcb.matches(JCBValidator.REGEX_JCB));
  }

  @Test public void shouldNotValidateAsJCB_more_than_16_digits() {
    String jcb = "35281397896135881";
    assertFalse(jcb.matches(JCBValidator.REGEX_JCB));
  }

  @Test public void shouldNotValidateAsJCB_less_than_15_digits() {
    String jcb = "35281397896135";
    assertFalse(jcb.matches(JCBValidator.REGEX_JCB));
  }
}
