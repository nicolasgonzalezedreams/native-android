package com.odigeo.app.android.view.validators;

import org.junit.Test;
import static com.odigeo.app.android.view.textwatchers.fieldsvalidation.MailValidator.REGEX_EMAIL;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class MailValidatorTest {

  private String email;

  @Test public void shouldValidateMail(){
    email = "random@gmail.com";
    assertTrue(email.matches(REGEX_EMAIL));

    email = "random-100@gmail.com";
    assertTrue(email.matches(REGEX_EMAIL));

    email = "random.100@gmail.es";
    assertTrue(email.matches(REGEX_EMAIL));

    email = "random100@gmail.net";
    assertTrue(email.matches(REGEX_EMAIL));

    email = "random@gmail12.es";
    assertTrue(email.matches(REGEX_EMAIL));

    email = "random@12.es";
    assertTrue(email.matches(REGEX_EMAIL));

    email = "random@gmail.co.uk";
    assertTrue(email.matches(REGEX_EMAIL));
  }

  @Test public void shouldNotValidateMail(){

    email = "";
    assertFalse(email.matches(REGEX_EMAIL));

    email = "random";
    assertFalse(email.matches(REGEX_EMAIL));

    email = "random@.com.es";
    assertFalse(email.matches(REGEX_EMAIL));

    email = "random@gmail.a";
    assertFalse(email.matches(REGEX_EMAIL));

    email = "random@.es";
    assertFalse(email.matches(REGEX_EMAIL));

    email = "random@.es.es";
    assertFalse(email.matches(REGEX_EMAIL));

    email = "random@%*.es";
    assertFalse(email.matches(REGEX_EMAIL));

    email = "random()*@gmail.es";
    assertFalse(email.matches(REGEX_EMAIL));

    email = "random..1900@gmail.es";
    assertFalse(email.matches(REGEX_EMAIL));

    email = "random.@gmail.es";
    assertFalse(email.matches(REGEX_EMAIL));

    email = "random@jimy@gmail.es";
    assertFalse(email.matches(REGEX_EMAIL));

    email = "random@gmail.es.1a";
    assertFalse(email.matches(REGEX_EMAIL));
  }
}
