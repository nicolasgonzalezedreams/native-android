package com.odigeo.test.matchers;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class Matchers {

  public static Matcher<String> matchesPattern(String pattern) {
    return new MatchesPattern(pattern);
  }

  private static final class MatchesPattern extends TypeSafeMatcher<String> {

    private final String pattern;

    private MatchesPattern(String pattern) {
      this.pattern = pattern;
    }

    @Override protected boolean matchesSafely(String item) {
      return item.matches(pattern);
    }

    @Override public void describeTo(Description description) {
      description.appendText("a string matching pattern " + pattern);
    }
  }
}
