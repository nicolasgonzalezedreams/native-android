package com.odigeo.test.utils;

import java.util.LinkedHashMap;
import java.util.Map;

public final class UrlParser {

  private final String url;

  public UrlParser(String url) {
    this.url = url;
  }

  public Map<String, String> getQueryStringParams() {

    String[] urlParts = getUrlParts();

    String queryString = urlParts[1];

    String[] queryParams = queryString.split("&");
    Map<String, String> params = new LinkedHashMap<>();
    for (String queryParam : queryParams) {
      String[] param = queryParam.split("=");
      params.put(param[0], param[1]);
    }

    return params;
  }

  public String[] getUrlParts() {
    return url.split("\\?");
  }

  public String getHost() {
    return getPathAt(0);
  }

  public String getPathAt(int index) {
    String[] urlParts = getUrlParts();

    String fullPath = urlParts[0];

    String[] paths = fullPath.split("/");

    if (index >= paths.length) {
      return null;
    }

    return paths[index];
  }
}
