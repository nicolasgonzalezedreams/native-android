package com.globant.roboneck.common;

import android.util.Log;
import com.globant.roboneck.requests.BaseNeckHttpRequest;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.request.listener.PendingRequestListener;
import com.octo.android.robospice.request.listener.RequestListener;
import roboguice.util.temp.Ln;

/**
 * {@link SpiceManager} that uses {@link BaseNeckHttpRequest} and simplifies
 * cache.
 *
 * @author federico.perez
 */
public class NeckSpiceManager extends SpiceManager {

  public NeckSpiceManager() {
    super(NeckService.class);
    Ln.getConfig().setLoggingLevel(Log.ERROR);
  }

  /**
   * Executes a cached request.
   *
   * @param request The request type.
   * @param requestListener Callback for result.
   */
  public final <T, R> void executeCacheRequest(BaseNeckHttpRequest<T, R> request,
      PendingRequestListener<T> requestListener) {
    super.execute(request, request.getCachekey(), request.getCacheExpirationTime(),
        requestListener);
  }

  /**
   * Gets result from cache for a certain request.
   *
   * @param request The request to search in the cache.
   * @param requestListener Callback for result.
   */
  public final <T, R> void getResultFromCache(BaseNeckHttpRequest<T, R> request,
      RequestListener<T> requestListener) {
    super.getFromCache(request.getResultType(), request.getCachekey(),
        request.getCacheExpirationTime(), requestListener);
  }

  /**
   * Adds listener for pending requests due to orientation changes or leaving
   * the application.
   *
   * @param request The request to listen.
   * @param requestListener Callback for result.
   */
  public final <T, R> void addListenerIfPending(BaseNeckHttpRequest<T, R> request,
      PendingRequestListener<T> requestListener) {
    super.addListenerIfPending(request.getResultType(), request.getCachekey(), requestListener);
  }
}
