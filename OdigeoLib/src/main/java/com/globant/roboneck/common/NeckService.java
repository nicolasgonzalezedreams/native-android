package com.globant.roboneck.common;

import android.app.Application;
import com.octo.android.robospice.okhttp.OkHttpSpiceService;
import com.octo.android.robospice.persistence.CacheManager;
import com.odigeo.app.android.lib.OdigeoApp;

public class NeckService extends OkHttpSpiceService {

  @Override public final CacheManager createCacheManager(Application application) {
    CacheManager manager = new CacheManager();
    manager.addPersister(((OdigeoApp) application).getPersister());
    return manager;
  }
}
