package com.globant.roboneck.common;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.consts.Constants;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.CookieHandler;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class Utils {
  private Utils() {
    // No instances.
  }

  static void copyStream(InputStream in, OutputStream out, byte[] buffer) throws IOException {
    int count = in.read(buffer);
    while (count != -1) {
      out.write(buffer, 0, count);
      count = in.read(buffer);
    }
  }

  static void isNotNull(Object obj, String message) {
    if (obj == null) {
      throw new IllegalStateException(message);
    }
  }

  static void isNull(Object obj, String message) {
    if (obj != null) {
      throw new IllegalStateException(message);
    }
  }

  static void isNotEmpty(String thing, String message) {
    isNotNull(thing, message);
    if ("".equals(thing.trim())) {
      throw new IllegalStateException(message);
    }
  }

  static void isNotZero(int value, String message) {
    if (value != 0) {
      throw new IllegalStateException(message);
    }
  }

  public static List<NeckCookie> getCookies(HttpURLConnection connection) {
    List<NeckCookie> lstCookies = new ArrayList<>();
    Map<String, List<String>> headerFields = connection.getHeaderFields();
    try {
      CookieHandler.getDefault().put(connection.getURL().toURI(), headerFields);
    } catch (URISyntaxException | IOException e) {
      Log.e(Constants.TAG_LOG, e.getMessage(), e);
    }
    Set<String> headerFieldsSet = headerFields.keySet();
    Iterator<String> hearerFieldsIter = headerFieldsSet.iterator();
    while (hearerFieldsIter.hasNext()) {
      String headerFieldKey = hearerFieldsIter.next();
      if ("Set-Cookie".equalsIgnoreCase(headerFieldKey)) {
        List<String> headerFieldValue = headerFields.get(headerFieldKey);
        setData(headerFieldValue, lstCookies);
      }
    }

    if (lstCookies.isEmpty()) {
      return null;
    }

    return lstCookies;
  }

  /**
   * @param headerFieldValue These are the header fields read in the getCookies method
   * @param lstCookies The list where the cookies will be stored and returned on the getCookies
   * method
   */
  private static void setData(List<String> headerFieldValue, List<NeckCookie> lstCookies) {
    for (String headerValue : headerFieldValue) {
      String[] fields = headerValue.split(";\\s*");
      String cookieValue = fields[0];
      String expires = null;
      String path = null;
      String domain = null;
      boolean secure = false;
      // Parse each field
      for (int j = 1; j < fields.length; j++) {
        if ("secure".equalsIgnoreCase(fields[j])) {
          secure = true;
        } else if (fields[j].indexOf('=') > 0) {
          String[] f = fields[j].split("=");
          if ("expires".equalsIgnoreCase(f[0])) {
            expires = f[1];
          } else if ("domain".equalsIgnoreCase(f[0])) {
            domain = f[1];
          } else if ("path".equalsIgnoreCase(f[0])) {
            path = f[1];
          }
        }
      }
      NeckCookie nuevaCookie = new NeckCookie();

      nuevaCookie.setValue(cookieValue);
      nuevaCookie.setDomain(domain);
      nuevaCookie.setPath(path);
      nuevaCookie.setSecure(secure);
      nuevaCookie.setExpires(expires);

      lstCookies.add(nuevaCookie);
    }
  }

  /**
   * Redirects to home activity
   */
  public static void sendToHome(Activity activity, OdigeoApp odigeoApp) {
    Intent intent = new Intent(activity, odigeoApp.getHomeActivity());
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    activity.startActivity(intent);
    activity.finish();
  }
}
