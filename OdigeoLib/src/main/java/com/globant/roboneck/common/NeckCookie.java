package com.globant.roboneck.common;

import java.io.Serializable;

/**
 * Created by ManuelOrtiz on 13/09/2014.
 */
public class NeckCookie implements Serializable {
  private String value;
  private String expires;
  private String path;
  private String domain;
  private boolean secure;

  public final boolean isSecure() {
    return secure;
  }

  public final void setSecure(boolean secure) {
    this.secure = secure;
  }

  public final String getValue() {
    return value;
  }

  public final void setValue(String value) {
    this.value = value;
  }

  public final String getExpires() {
    return expires;
  }

  public final void setExpires(String expires) {
    this.expires = expires;
  }

  public final String getPath() {
    return path;
  }

  public final void setPath(String path) {
    this.path = path;
  }

  public final String getDomain() {
    return domain;
  }

  public final void setDomain(String domain) {
    this.domain = domain;
  }

  public final String extractTheValuePart() {
    String valuePart = null;

    if (value != null && value.indexOf('=') > 0) {
      String[] valueSections = value.split("=");
      valuePart = valueSections[1];
    }

    return valuePart;
  }

  public final String extractTheNamePart() {
    String namePart = null;

    if (value != null && value.indexOf('=') > 0) {
      String[] valueSections = value.split("=");
      namePart = valueSections[0];
    }

    return namePart;
  }
}
