package com.globant.roboneck.requests;

import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Pair;
import com.globant.roboneck.common.NeckCookie;
import com.globant.roboneck.common.Utils;
import com.octo.android.robospice.request.okhttp.OkHttpSpiceRequest;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.neck.errors.NeckNotInternetError;
import com.odigeo.app.android.lib.utils.Util;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.IOUtils;

public abstract class BaseNeckHttpRequest<T, R> extends OkHttpSpiceRequest<T> {
  protected static final String ENCODING = "UTF-8";
  private static final int RESPONSE_CODE_MIN_LIMIT = 400;
  private static final int RESPONSE_CODE_MAX_LIMIT = 600;
  private static final int RESPONSE_CODE = 200;
  private static final int RESPONSE_CODE2 = 201;
  private List<NeckCookie> cookiesReceived;
  private int timeoutInSeconds;

  public BaseNeckHttpRequest(Class<T> clazz) {
    super(clazz);
  }

  public abstract Object getCachekey();

  public abstract long getCacheExpirationTime();

  protected abstract String getBody();

  protected abstract Map<String, String> getHeaders();

  protected abstract String getUrl();

  protected abstract Method getMethod();

  protected abstract R processContent(String responseBody);

  protected abstract boolean isLogicError(R response);

  protected abstract T getRequestModel(R response);

  protected abstract BaseNeckRequestException.Error processError(int httpStatus, R response,
      String responseBody);

  @Nullable protected abstract List<Pair<String, String>> getQueryParameters();

  @Nullable protected abstract List<NeckCookie> getCookies();

  public final List<NeckCookie> getCookiesReceived() {
    return cookiesReceived;
  }

  public final void setCookiesReceived(List<NeckCookie> cookiesReceived) {
    this.cookiesReceived = cookiesReceived;
  }

  public final int getTimeoutInSeconds() {
    return timeoutInSeconds;
  }

  public final void setTimeoutInSeconds(int timeoutInSeconds) {
    this.timeoutInSeconds = timeoutInSeconds;
  }

  // Override this method if more configuration is needed for the Connection
  protected final HttpURLConnection getConnection(URI uri) throws MalformedURLException {
    HttpURLConnection conn = getOkHttpClient().open(uri.toURL());
    Map<String, String> headers = getHeaders();
    if (headers != null) {
      for (String header : headers.keySet()) {
        conn.addRequestProperty(header, headers.get(header));
        Log.i(Constants.TAG_LOG, header + " -> " + headers.get(header));
      }
    }

    List<NeckCookie> cookies = getCookies();
    if (cookies != null) {
      for (NeckCookie cookie : cookies) {
        conn.addRequestProperty("Cookie", cookie.getValue());
        Log.i(Constants.TAG_LOG, "Cookie, " + cookie.getValue());
      }
    }

    return conn;
  }

  /**
   * Creates the URI used to loadWidgetImage the data from network
   *
   * @return URI used to loadWidgetImage the data from network
   * @throws UnsupportedEncodingException
   */
  private Uri.Builder getUriBuilder() throws UnsupportedEncodingException {
    Uri.Builder uriBuilder = Uri.parse(getUrl()).buildUpon();

    //Set timeout
    if (timeoutInSeconds > 0) {
      getOkHttpClient().setConnectTimeout(timeoutInSeconds, TimeUnit.SECONDS);
    }

    Log.i(Constants.TAG_LOG, "Calling " + getUrl());

    List<Pair<String, String>> params = getQueryParameters();
    if (params != null) {
      for (Pair<String, String> pair : params) {

        uriBuilder.appendQueryParameter(URLEncoder.encode(pair.first, ENCODING),
            URLEncoder.encode(pair.second, ENCODING));
      }
    }

    return uriBuilder;
  }

  /**
   * Sets the values to the connection depending on the type of request it is.
   *
   * @throws IOException
   */
  private void setConnectionData(HttpURLConnection connection) throws IOException {
    switch (getMethod()) {
      case GET:
        connection.setRequestMethod("GET");
        break;
      case POST:
        connection.setRequestMethod("POST");
        writeBody(connection.getOutputStream());
        break;
      case PUT:
        connection.setRequestMethod("PUT");
        writeBody(connection.getOutputStream());
        break;
      case DELETE:
        connection.setRequestMethod("DELETE");
      default:
        break;
    }
  }

  /**
   * Returns the body with the content needed to loadWidgetImage the data from network
   *
   * @param in input stream to loadWidgetImage data from network
   * @param connection connection used to loadWidgetImage data from network
   * @return body with the content needed to loadWidgetImage the data from network
   * @throws IOException
   */
  private String getBody(InputStream in, HttpURLConnection connection) throws IOException {
    byte[] bytes = null;
    String body = null;
    String contentEncoding = connection.getHeaderField("Content-Encoding");

    if (contentEncoding == null) {
      body = IOUtils.toString(in, ENCODING);
    } else { //If the response is compressed, make a decompression
      if (contentEncoding.toLowerCase(Locale.ENGLISH).contains("gzip")) {
        bytes = IOUtils.toByteArray(in);

        try {
          body = Util.decompress(bytes, Charset.forName(ENCODING));
        } catch (Exception e3) {
          Log.i(Constants.TAG_LOG, e3.getMessage());
        }
      } else {
        throw new UnsupportedOperationException();
      }
    }

    return body;
  }

  @SuppressWarnings("unchecked") @Override public final T loadDataFromNetwork()
      throws BaseNeckRequestException {
    try {
      Uri.Builder uriBuilder = getUriBuilder();

      URI uri;

      uri = new URI(String.valueOf(uriBuilder.build()));

      HttpURLConnection connection = getConnection(uri);

      setConnectionData(connection);

      return assignValues(connection);
    } catch (SocketTimeoutException | URISyntaxException | MalformedURLException | UnsupportedEncodingException | ConnectException e2) {
      Log.e(Constants.TAG_LOG, "RequestEx: " + e2, e2);
      return (T) new NeckNotInternetError("Rest Service timeout",
          Constants.ERROR_CODE_SPICE_TIMEOUT, 1);
    } catch (IOException e2) {
      Log.e(Constants.TAG_LOG, "RequestEx: " + e2, e2);
      throw new BaseNeckRequestException(e2);
    }
  }

  /**
   * Assigns values needed to loadWidgetImage data from network
   *
   * @param connection connection used to loadWidgetImage data from network
   * @return a object of the parametrized class
   * @throws IOException
   */
  private T assignValues(HttpURLConnection connection) throws IOException {
    BaseNeckRequestException.Error e = null;
    R r = null;
    T t = null;

    InputStream in = null;

    int responseCode = connection.getResponseCode();
    if (responseCode >= RESPONSE_CODE_MIN_LIMIT) {
      in = connection.getErrorStream();
    } else {
      in = connection.getInputStream();
    }

    setCookiesReceived(Utils.getCookies(connection));
    String body = getBody(in, connection);

    setRetryPolicy(null);

    if (responseCode == RESPONSE_CODE || responseCode == RESPONSE_CODE2) {
      r = processContent(body);
      if (isLogicError(r)) {
        e = processError(responseCode, r, body);
      } else {
        t = getRequestModel(r);
      }
    } else if (responseCode >= RESPONSE_CODE_MIN_LIMIT && responseCode <= RESPONSE_CODE_MAX_LIMIT) {
      setRetryPolicy(null);
      e = processError(responseCode, null, body);
    } else {
      e = processError(responseCode, null, body);
    }

    if (in != null) {
      in.close();
    }

    if (e != null) {
      return (T) e;
    }

    return t;
  }

  private void writeBody(OutputStream stream) throws UnsupportedEncodingException {
    String body = getBody();
    if (body != null && !body.isEmpty()) {
      try {
        stream.write(body.getBytes(ENCODING));
        stream.close();
      } catch (IOException e) {
        Log.d("Exception: ", e.toString());
      }
    }
  }

  protected enum Method {
    GET, POST, PUT, DELETE
  }
}
