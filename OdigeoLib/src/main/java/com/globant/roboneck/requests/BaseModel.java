package com.globant.roboneck.requests;

public class BaseModel {

  Error error;

  final Error getError() {
    return this.error;
  }

  final void setError(Error error) {
    this.error = error;
  }
}
