package com.globant.roboneck.base;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import com.globant.roboneck.common.NeckSpiceManager;

public class RoboNeckActivity extends ActionBarActivity {
  protected NeckSpiceManager spiceManager = new NeckSpiceManager();

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    spiceManager = new NeckSpiceManager();
  }

  @Override public void onStart() {
    spiceManager.start(this);
    super.onStart();
  }

  @Override public void onStop() {
    spiceManager.shouldStop();
    super.onStop();
  }

  @Override public void onResume() {
    super.onResume();

    if (getActivityTitle() != null) {
      getSupportActionBar().setTitle(getActivityTitle());
    }
  }

  public String getActivityTitle() {
    return null;
  }
}
