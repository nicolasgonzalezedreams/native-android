package com.globant.roboneck.base;

import android.os.Bundle;
import com.globant.roboneck.common.NeckSpiceManager;

public class RoboNeckFragment extends android.support.v4.app.Fragment {
  protected NeckSpiceManager spiceManager = new NeckSpiceManager();

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    spiceManager = new NeckSpiceManager();
  }

  @Override public void onStart() {
    spiceManager.start(this.getActivity());
    super.onStart();
  }

  @Override public void onStop() {
    spiceManager.shouldStop();
    super.onStop();
  }

  protected RoboNeckActivity getBaseActivity() {
    return (RoboNeckActivity) getActivity();
  }
}
