package com.odigeo.app.android.lib.models;

/**
 * Created by luis.bejarano on 10/2/14.
 */
public class RowItemListFilters {

  private final String title;
  private int idAir;
  private boolean isSelected = false;

  public RowItemListFilters(int idAir, String title) {

    this.idAir = idAir;
    this.title = title;
  }

  public RowItemListFilters(String title) {
    this.title = title;
  }

  public int getIdAir() {
    return idAir;
  }

  public void setIdAir(int idAir) {
    this.idAir = idAir;
  }

  public String getTitle() {
    return title;
  }

  public boolean isSelected() {
    return isSelected;
  }

  public void setSelected(boolean isSelected) {
    this.isSelected = isSelected;
  }

  @Override public String toString() {
    return title;
  }
}
