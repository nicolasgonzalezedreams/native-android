package com.odigeo.app.android.lib.models;

import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.LocationDTO;
import java.io.Serializable;
import java.util.List;

/**
 * Created by manuel on 21/10/14.
 */
public class FiltersSelected implements Serializable {
  private List<CarrierDTO> carriers;
  private List<FilterTimeModel> times;
  private List<FilterStopModel> stops;
  private List<LocationDTO> airports;

  public final List<CarrierDTO> getCarriers() {
    return carriers;
  }

  public final void setCarriers(List<CarrierDTO> carriers) {
    this.carriers = carriers;
  }

  public final List<FilterTimeModel> getTimes() {
    return times;
  }

  public final void setTimes(List<FilterTimeModel> times) {
    this.times = times;
  }

  public final List<FilterStopModel> getStops() {
    return stops;
  }

  public final void setStops(List<FilterStopModel> stops) {
    this.stops = stops;
  }

  public final List<LocationDTO> getAirports() {
    return airports;
  }

  public final void setAirports(List<LocationDTO> airports) {
    this.airports = airports;
  }
}
