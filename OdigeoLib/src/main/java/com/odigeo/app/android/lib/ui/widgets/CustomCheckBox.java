package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;

/**
 * This class should be used in replace of any button in order to add it a text that comes from the
 * database.
 *
 * @author Cristian Fanjul
 * @since 02/06/2015
 */
@Deprecated public class CustomCheckBox extends AppCompatCheckBox {
  private static final int REFERENCE_NOT_FOUND = -1;

  public CustomCheckBox(Context context, AttributeSet attrs) {
    super(context, attrs);

    applyAttributes(context, attrs);
  }

  private void applyAttributes(Context context, AttributeSet attrs) {
    TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.TextViewWithFont);

    String odigeoText = values.getString(R.styleable.OdigeoTextAttrs_odigeoText);

    if (odigeoText != null) {
      setText(LocalizablesFacade.getString(context, odigeoText));
    }
  }
}
