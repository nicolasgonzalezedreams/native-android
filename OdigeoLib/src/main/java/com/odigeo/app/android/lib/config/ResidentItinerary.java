package com.odigeo.app.android.lib.config;

import java.io.Serializable;

/**
 * Created by emiliano.gudino on 20/11/2014.
 */
public class ResidentItinerary implements Serializable {

  private ResidentItineraryPlace destination;
  private ResidentItineraryPlace origin;
  private String residentPlace;

  public ResidentItinerary() {

  }

  public ResidentItinerary(ResidentItineraryPlace origin, ResidentItineraryPlace destination,
      String residentPlace) {
    this.origin = origin;
    this.destination = destination;
    this.residentPlace = residentPlace;
  }

  public final ResidentItineraryPlace getOrigin() {
    return origin;
  }

  public final void setOrigin(ResidentItineraryPlace origin) {
    this.origin = origin;
  }

  public final ResidentItineraryPlace getDestination() {
    return destination;
  }

  public final void setDestination(ResidentItineraryPlace destination) {
    this.destination = destination;
  }

  public final String getResidentPlace() {
    return residentPlace;
  }

  public final void setResidentPlace(String residentPlace) {
    this.residentPlace = residentPlace;
  }
}
