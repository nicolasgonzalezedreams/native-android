package com.odigeo.app.android.lib.models.dto;

import java.util.List;

/**
 * @author miguel
 */
public class ItinerarySearchRequestDTO {

  protected List<ItinerarySegmentRequestDTO> segmentRequests;
  protected List<String> includedCarriers;
  protected List<String> excludedCarriers;
  protected CabinClassDTO cabinClass;
  protected int numAdults;
  protected int numChildren;
  protected int numInfants;
  protected Boolean resident;
  protected SearchProductTypeDTO searchMainProductType;
  protected Boolean directFlightsOnly;
  protected Boolean dynpackSearch;
  protected ItinerarySortCriteriaDTO sortCriteria;
  protected Boolean mainAirportsOnly;
  protected Boolean isMember;

  /**
   * Gets the value of the cabinClass property.
   *
   * @return possible object is {@link com.odigeo.app.android.lib.models.CabinClass }
   */
  public CabinClassDTO getCabinClass() {
    return cabinClass;
  }

  /**
   * Sets the value of the cabinClass property.
   *
   * @param value allowed object is {@link com.odigeo.app.android.lib.models.CabinClass }
   */
  public void setCabinClass(CabinClassDTO value) {
    this.cabinClass = value;
  }

  /**
   * Gets the value of the numAdults property.
   */
  public int getNumAdults() {
    return numAdults;
  }

  /**
   * Sets the value of the numAdults property.
   */
  public void setNumAdults(int value) {
    this.numAdults = value;
  }

  /**
   * Gets the value of the numChildren property.
   */
  public int getNumChildren() {
    return numChildren;
  }

  /**
   * Sets the value of the numChildren property.
   */
  public void setNumChildren(int value) {
    this.numChildren = value;
  }

  /**
   * Gets the value of the numInfants property.
   */
  public int getNumInfants() {
    return numInfants;
  }

  /**
   * Sets the value of the numInfants property.
   */
  public void setNumInfants(int value) {
    this.numInfants = value;
  }

  /**
   * Gets the value of the resident property.
   *
   * @return possible object is {@link Boolean }
   */
  public Boolean isResident() {
    return resident;
  }

  /**
   * Sets the value of the resident property.
   *
   * @param value allowed object is {@link Boolean }
   */
  public void setResident(Boolean value) {
    this.resident = value;
  }

  /**
   * Gets the value of the searchMainProductType property.
   *
   * @return possible object is {@link SearchProductType }
   */
  public SearchProductTypeDTO getSearchMainProductType() {
    return searchMainProductType;
  }

  /**
   * Sets the value of the searchMainProductType property.
   *
   * @param value allowed object is {@link SearchProductType }
   */
  public void setSearchMainProductType(SearchProductTypeDTO value) {
    this.searchMainProductType = value;
  }

  /**
   * Gets the value of the directFlightsOnly property.
   *
   * @return possible object is {@link Boolean }
   */
  public Boolean isDirectFlightsOnly() {
    return directFlightsOnly;
  }

  /**
   * Sets the value of the directFlightsOnly property.
   *
   * @param value allowed object is {@link Boolean }
   */
  public void setDirectFlightsOnly(Boolean value) {
    this.directFlightsOnly = value;
  }

  /**
   * Gets the value of the dynpackSearch property.
   *
   * @return possible object is {@link Boolean }
   */
  public Boolean isDynpackSearch() {
    return dynpackSearch;
  }

  /**
   * Sets the value of the dynpackSearch property.
   *
   * @param value allowed object is {@link Boolean }
   */
  public void setDynpackSearch(Boolean value) {
    this.dynpackSearch = value;
  }

  /**
   * Gets the value of the sortCriteria property.
   *
   * @return possible object is {@link ItinerarySortCriteria }
   */
  public ItinerarySortCriteriaDTO getSortCriteria() {
    return sortCriteria;
  }

  /**
   * Sets the value of the sortCriteria property.
   *
   * @param value allowed object is {@link ItinerarySortCriteria }
   */
  public void setSortCriteria(ItinerarySortCriteriaDTO value) {
    this.sortCriteria = value;
  }

  /**
   * @return the segmentRequests
   */
  public List<ItinerarySegmentRequestDTO> getSegmentRequests() {
    return segmentRequests;
  }

  /**
   * @param segmentRequests the segmentRequests to set
   */
  public void setSegmentRequests(List<ItinerarySegmentRequestDTO> segmentRequests) {
    this.segmentRequests = segmentRequests;
  }

  /**
   * @return the includedCarriers
   */
  public List<String> getIncludedCarriers() {
    return includedCarriers;
  }

  /**
   * @param includedCarriers the includedCarriers to set
   */
  public void setIncludedCarriers(List<String> includedCarriers) {
    this.includedCarriers = includedCarriers;
  }

  /**
   * @return the excludedCarriers
   */
  public List<String> getExcludedCarriers() {
    return excludedCarriers;
  }

  /**
   * @param excludedCarriers the excludedCarriers to set
   */
  public void setExcludedCarriers(List<String> excludedCarriers) {
    this.excludedCarriers = excludedCarriers;
  }

  /**
   * Gets the value of the mainAirportsOnly property.
   *
   * @return possible object is {@link Boolean }
   */
  public Boolean isMainAirportsOnly() {
    return mainAirportsOnly;
  }

  /**
   * Sets the value of the mainAirportsOnly property.
   *
   * @param value allowed object is {@link Boolean }
   */
  public void setMainAirportsOnly(Boolean mainAirportsOnly) {
    this.mainAirportsOnly = mainAirportsOnly;
  }

  public Boolean getIsMember() {
    return isMember;
  }

  public void setIsMember(Boolean isMember) {
    this.isMember = isMember;
  }
}
