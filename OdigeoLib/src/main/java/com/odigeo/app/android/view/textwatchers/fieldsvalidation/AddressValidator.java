package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

public class AddressValidator extends BaseValidator implements FieldValidator {

  public static final String REGEX_ADDRESS =
      "^[-/ ,\\.'A-Za-zÀÁÂÃÄÅàáâãäåÆæßÈÉÊËèéêëÌÍÎÏIİìíîïıiŒœÒÓÔÕÖØøòóôõöøÙÚÛÜùúûüŸÝýÿÇçÑñÐþÞµŠšSsŞşGĞgğƒ\\d]{2,50}$";

  public AddressValidator() {
    super();
  }

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_ADDRESS);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
