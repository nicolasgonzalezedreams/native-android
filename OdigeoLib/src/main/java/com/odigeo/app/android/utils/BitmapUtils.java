package com.odigeo.app.android.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.Base64;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eleazarspak on 3/5/16.
 */
public class BitmapUtils {

  public static String getEncodedBitmapByteArray(Bitmap source) {

    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    Bitmap bitmap = source.copy(source.getConfig(), true);
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
    bitmap.recycle();
    return Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);
  }

  public static List<String> getEncodedBitmapByteArrayList(List<Bitmap> bitmaps) {
    List<String> returnList = new ArrayList<>();
    for (Bitmap bitmap : bitmaps) {
      returnList.add(getEncodedBitmapByteArray(bitmap));
    }
    return returnList;
  }

  public static Bitmap roundCorners(Bitmap bitmap) {
    Bitmap imageRounded =
        Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
    Canvas canvas = new Canvas(imageRounded);
    canvas.drawARGB(0, 0, 0, 0);
    Paint mpaint = new Paint();
    mpaint.setAntiAlias(true);
    mpaint.setShader(new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
    canvas.drawRoundRect((new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight())), 100, 100,
        mpaint);// Round Image Corner 100 100 100 100
    // create Xfer mode
    mpaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
    // draw source image to canvas
    canvas.drawBitmap(bitmap, rect, rect, mpaint);
    return imageRounded;
  }
}
