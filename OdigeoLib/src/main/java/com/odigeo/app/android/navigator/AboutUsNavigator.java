package com.odigeo.app.android.navigator;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoWebViewActivity;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.PdfDownloader;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.view.AboutContactUsView;
import com.odigeo.app.android.view.AboutFaqView;
import com.odigeo.app.android.view.AboutUsView;
import com.odigeo.app.android.view.TermsAndConditionsView;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.PermissionsHelper;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.contracts.navigators.AboutUsNavigatorInterface;

public class AboutUsNavigator extends BaseNavigator implements AboutUsNavigatorInterface {

  private static final String PDF_EXTENSION = ".pdf";
  private String mPhoneNumber;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view);

    if (savedInstanceState == null) {
      replaceFragment(R.id.fl_container, new AboutUsView());
    }

    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    setNavigationTitle(
        LocalizablesFacade.getString(this, OneCMSKeys.ABOUTVIEWCONTROLLER_TITLE).toString());
  }

  @Override public void onResume() {
    super.onResume();
    BusProvider.getInstance().post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_HELP_SECTION));
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      postGAEventOnBackPressed();
      onBackPressed();
    }
    return super.onOptionsItemSelected(item);
  }

  private void postGAEventOnBackPressed() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_ABOUT_US,
            GAnalyticsNames.ACTION_NAVIGATION, GAnalyticsNames.LABEL_GO_BACK));
  }

  @Override public void onRequestPermissionsResult(int requestCode, String[] permissions,
      int[] grantResults) {
    switch (requestCode) {
      case PermissionsHelper.PHONE_REQUEST_CODE:
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT, TrackerConstants.LABEL_PHONE_ACCESS_ALLOW);
          Intent callIntent = new Intent(Intent.ACTION_CALL);
          callIntent.setData(Uri.parse("tel:" + mPhoneNumber));
          startActivity(callIntent);
        } else {
          tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT, TrackerConstants.LABEL_PHONE_ACCESS_DENY);
        }
    }
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }

  public void setPhoneCallNumber(String number) {
    mPhoneNumber = number;
  }

  @Override public void openAboutFaq() {
    replaceFragmentWithBackStack(R.id.fl_container, new AboutFaqView());
  }

  @Override public void openAboutContactUs() {
    replaceFragmentWithBackStack(R.id.fl_container, new AboutContactUsView());
  }

  @Override public void openTermsAndConditions() {
    replaceFragmentWithBackStack(R.id.fl_container, new TermsAndConditionsView());
  }

  @Override public void openAppRate() {
    Intent i = new Intent(getApplicationContext(), AppRateNavigator.class);
    startActivity(i);
  }

  @Override public void openHelpCenter() {
    openUrl(LocalizablesFacade.getString(getApplicationContext(),
        OneCMSKeys.ABOUTOPTIONSMODULE_HELPCENTER_URL).toString());
  }

  @Override public void openTCBrand() {
    openUrl(LocalizablesFacade.getString(getApplicationContext(),
        OneCMSKeys.ABOUTOPTIONSMODULE_ABOUT_OPTION_TERMS_URL).toString());
  }

  @Override public void openTCAirline() {
    openUrl(LocalizablesFacade.getString(getApplicationContext(), OneCMSKeys.ABOUT_TC_AIRLINE_URL)
        .toString());
  }

  @Override public void openPrivacyPolicy() {
    openUrl(LocalizablesFacade.getString(getApplicationContext(), OneCMSKeys.PRIVACY_POLICY_URL)
        .toString());
  }

  @Override public void openWebView(String key, String defaultUrl) {

    String urlOneCms = LocalizablesFacade.getString(getApplicationContext(), key).toString();

    if (!urlOneCms.contains(key) && !urlOneCms.isEmpty()) {

      openUrl(urlOneCms);
    } else if (defaultUrl != null && !defaultUrl.isEmpty()) {

      openUrl(defaultUrl);
    }
  }

  private void openUrl(String url) {

    if (url.contains(PDF_EXTENSION)) {
      PdfDownloader task = new PdfDownloader(this);
      task.execute(url);
    } else {
      Intent intent = new Intent(getApplicationContext(), OdigeoWebViewActivity.class);
      intent.putExtra(Constants.EXTRA_URL_WEBVIEW, url);
      startActivity(intent);
    }
  }
}
