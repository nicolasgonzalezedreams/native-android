package com.odigeo.app.android.lib.pojos.banktransfer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 27/04/15
 */
public abstract class IncompleteBookingBean {

  /**
   * This method is for indicate the behavior to inflate, link, and set up the view.
   *
   * @param context Context where be inflated the view.
   * @param parent Parent where be placed the inflated view.
   * @return View inflated.
   */
  public abstract View inflate(@NonNull Context context, @NonNull ViewGroup parent);
}
