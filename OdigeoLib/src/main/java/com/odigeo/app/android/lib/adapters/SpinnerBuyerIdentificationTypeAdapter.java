package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.models.SpinnerItemModel;
import com.odigeo.app.android.lib.models.dto.BuyerIdentificationTypeDTO;
import java.util.List;

/**
 * Created by manuel on 06/10/14.
 *
 * @deprecated Use {@link OdigeoSpinnerAdapter} instead
 */
@Deprecated public class SpinnerBuyerIdentificationTypeAdapter
    extends ArrayAdapter<SpinnerItemModel<BuyerIdentificationTypeDTO>> {
  public SpinnerBuyerIdentificationTypeAdapter(Context context,
      List<SpinnerItemModel<BuyerIdentificationTypeDTO>> objects) {
    super(context, R.layout.layout_spinner_simple_text, objects);
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {
    View view =
        LayoutInflater.from(getContext()).inflate(R.layout.layout_spinner_simple_text, null);
    TextView textView = (TextView) view.findViewById(R.id.textView_card_name_and_icon);
    textView.setTypeface(Configuration.getInstance().getFonts().getRegular());

    textView.setText(getItem(position).getText());
    return view;
  }

  @Override public View getDropDownView(int position, View convertView, ViewGroup parent) {
    View view =
        LayoutInflater.from(getContext()).inflate(R.layout.layout_spinner_drop_simple_text, null);
    TextView textView = (TextView) view.findViewById(R.id.textView_card_name_and_icon);
    textView.setTypeface(Configuration.getInstance().getFonts().getRegular());
    textView.setText(getItem(position).getText());
    return view;
  }
}
