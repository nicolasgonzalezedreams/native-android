package com.odigeo.app.android.lib.models.dto;

import java.util.ArrayList;
import java.util.List;

public class PassengersConflictDetailsDTO {

  protected long bookingId;
  protected List<String> pnrs;
  protected long depTime;
  protected String depAirport;
  protected String arrAirport;
  protected BookingStatusDTO bookingStatus;

  /**
   * Gets the value of the bookingId property.
   */
  public long getBookingId() {
    return bookingId;
  }

  /**
   * Sets the value of the bookingId property.
   */
  public void setBookingId(long value) {
    this.bookingId = value;
  }

  /**
   * Gets the value of the pnrs property.
   */
  public List<String> getPnrs() {
    if (pnrs == null) {
      pnrs = new ArrayList<String>();
    }
    return this.pnrs;
  }

  /**
   * Sets the value of the pnrs property.
   */
  public void setPnrs(List<String> value) {
    this.pnrs = value;
  }

  /**
   * Gets the value of the depTime property.
   */
  public long getDepTime() {
    return depTime;
  }

  /**
   * Sets the value of the depTime property.
   */
  public void setDepTime(long value) {
    this.depTime = value;
  }

  /**
   * Gets the value of the depAirport property.
   */
  public String getDepAirport() {
    return depAirport;
  }

  /**
   * Sets the value of the depAirport property.
   */
  public void setDepAirport(String value) {
    this.depAirport = value;
  }

  /**
   * Gets the value of the arrAirport property.
   */
  public String getArrAirport() {
    return arrAirport;
  }

  /**
   * Sets the value of the arrAirport property.
   */
  public void setArrAirport(String value) {
    this.arrAirport = value;
  }

  /**
   * Gets the value of the bookingStatus property.
   */
  public BookingStatusDTO getBookingStatus() {
    return bookingStatus;
  }

  /**
   * Sets the value of the bookingStatus property.
   */
  public void setBookingStatus(BookingStatusDTO value) {
    this.bookingStatus = value;
  }
}
