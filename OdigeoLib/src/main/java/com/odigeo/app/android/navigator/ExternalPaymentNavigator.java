package com.odigeo.app.android.navigator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.view.ExternalPaymentView;
import com.odigeo.app.android.view.KlarnaExternalPaymentView;
import com.odigeo.app.android.view.PaypalExternalPaymentView;
import com.odigeo.app.android.view.TrustlyExternalPaymentView;
import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.ResumeDataRequest;
import com.odigeo.presenter.ExternalPaymentPresenter;
import com.odigeo.presenter.contracts.ExternalPaymentNavigatorInterface;

public class ExternalPaymentNavigator extends BaseNavigator
    implements ExternalPaymentNavigatorInterface {

  protected ExternalPaymentView externalPaymentView;
  protected BookingResponse bookingResponse;
  protected CollectionMethodType collectionMethodType;

  protected Fragment getInstanceExternalPaymentView() {
    if (externalPaymentView == null && collectionMethodType != null) {

      switch (collectionMethodType) {
        case PAYPAL:
          externalPaymentView = new PaypalExternalPaymentView();
          break;
        case TRUSTLY:
          externalPaymentView = new TrustlyExternalPaymentView();
          break;
        case KLARNA:
          externalPaymentView = new KlarnaExternalPaymentView();
          break;
        default:
          externalPaymentView = new ExternalPaymentView();
      }

      externalPaymentView = externalPaymentView.setInstance(bookingResponse);
      externalPaymentView.setArguments(getIntent().getExtras());
    }
    return externalPaymentView;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_external_payment);
    bookingResponse =
        (BookingResponse) getIntent().getSerializableExtra(Constants.EXTRA_BOOKING_RESPONSE);
    collectionMethodType =
        (CollectionMethodType) getIntent().getSerializableExtra(Constants.EXTRA_COLLECTION_METHOD);
    replaceFragment(R.id.fl_container_external_payment, getInstanceExternalPaymentView());
  }

  @Override public void onBackPressed() {
    externalPaymentView.onBackPressed();
    externalPaymentView.trackGoBack();

    if (externalPaymentView.canGoBack()) {
      externalPaymentView.goBack();
    } else {
      super.onBackPressed();
    }
  }

  @Override public void finishExternalPaymentNavigator(int externalPaymentCode,
      ResumeDataRequest resumeDataRequest) {
    if (externalPaymentCode == ExternalPaymentPresenter.EXTERNAL_PAYMENT_CODE_OK) {
      Intent resultIntent = new Intent();
      resultIntent.putExtra(Constants.EXTRA_RESUME_DATA_REQUEST, resumeDataRequest);
      setResult(externalPaymentCode, resultIntent);
    }
    finish();
  }
}
