package com.odigeo.app.android.lib.models.dto;

import com.odigeo.data.entity.shoppingCart.Step;
import java.util.List;

public class PersonalInfoRequestDTO extends BaseRequestDTO {
  protected BuyerRequestDTO buyer;
  protected List<TravellerRequestDTO> travellerRequests;
  protected long bookingId;
  protected ItinerarySortCriteriaDTO sortCriteria;
  protected Step step;

  /**
   * @return the travellerRequests
   */
  public List<TravellerRequestDTO> getTravellerRequests() {
    return travellerRequests;
  }

  /**
   * @param travellerRequests the travellerRequests to set
   */
  public void setTravellerRequests(List<TravellerRequestDTO> travellerRequests) {
    this.travellerRequests = travellerRequests;
  }

  /**
   * Gets the value of the buyer property.
   *
   * @return possible object is
   * {@link BuyerRequestDTO }
   */
  public BuyerRequestDTO getBuyer() {
    return buyer;
  }

  /**
   * Sets the value of the buyer property.
   *
   * @param value allowed object is
   * {@link BuyerRequestDTO }
   */
  public void setBuyer(BuyerRequestDTO value) {
    this.buyer = value;
  }

  /**
   * Gets the value of the bookingId property.
   */
  public long getBookingId() {
    return bookingId;
  }

  /**
   * Sets the value of the bookingId property.
   */
  public void setBookingId(long value) {
    this.bookingId = value;
  }

  /**
   * Gets the value of the sortCriteria property.
   */
  public ItinerarySortCriteriaDTO getSortCriteria() {
    return sortCriteria;
  }

  /**
   * Sets the value of the sortCriteria property.
   */
  public void setSortCriteria(ItinerarySortCriteriaDTO sortCriteria) {
    this.sortCriteria = sortCriteria;
  }

  /**
   * Gets the value of the step property.
   */
  public Step getStep() {
    return step;
  }

  /**
   * Sets the value of the step property.
   */
  public void setStep(Step step) {
    this.step = step;
  }
}
