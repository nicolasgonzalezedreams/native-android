package com.odigeo.app.android.lib.models.dto;

public class ImageInformationDTO {

  protected String name;
  protected String url;
  protected String thumbnailUrl;
  protected Integer width;
  protected Integer height;
  protected Integer byteSize;
  protected String caption;

  /**
   * Gets the value of the name property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the value of the name property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setName(String value) {
    this.name = value;
  }

  /**
   * Gets the value of the url property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getUrl() {
    return url;
  }

  /**
   * Sets the value of the url property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setUrl(String value) {
    this.url = value;
  }

  /**
   * Gets the value of the thumbnailUrl property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

  /**
   * Sets the value of the thumbnailUrl property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setThumbnailUrl(String value) {
    this.thumbnailUrl = value;
  }

  /**
   * Gets the value of the width property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getWidth() {
    return width;
  }

  /**
   * Sets the value of the width property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setWidth(Integer value) {
    this.width = value;
  }

  /**
   * Gets the value of the height property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getHeight() {
    return height;
  }

  /**
   * Sets the value of the height property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setHeight(Integer value) {
    this.height = value;
  }

  /**
   * Gets the value of the byteSize property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getByteSize() {
    return byteSize;
  }

  /**
   * Sets the value of the byteSize property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setByteSize(Integer value) {
    this.byteSize = value;
  }

  /**
   * Gets the value of the caption property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCaption() {
    return caption;
  }

  /**
   * Sets the value of the caption property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCaption(String value) {
    this.caption = value;
  }
}
