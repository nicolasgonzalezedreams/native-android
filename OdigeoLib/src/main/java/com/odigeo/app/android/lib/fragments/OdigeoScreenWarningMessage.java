package com.odigeo.app.android.lib.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;

/**
 * Created by Irving on 17/02/2015.
 * This class is used for show a general message in top screen
 */
public class OdigeoScreenWarningMessage extends DialogFragment {
  public static final String ID_ICON_RESOURCE = "idIcon";
  public static final String MESSAGE = "msgWM";
  private static final int TIME_TO_WIDENNING = 5001;

  public static OdigeoScreenWarningMessage newInstance(String message) {
    return newInstance(-1, message);
  }

  public static OdigeoScreenWarningMessage newInstance(int iconResource, String message) {
    OdigeoScreenWarningMessage messageFragment = new OdigeoScreenWarningMessage();
    Bundle bundle = new Bundle();
    bundle.putInt(ID_ICON_RESOURCE, iconResource);
    bundle.putString(MESSAGE, message);
    messageFragment.setArguments(bundle);
    return messageFragment;
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.layout_widening_widget, container, false);
    TextView textView = (TextView) view.findViewById(R.id.tvWideningLeyend);

    Bundle bundle = getArguments();
    String message = bundle.getString(MESSAGE);
    int idResourceIcon = bundle.getInt(ID_ICON_RESOURCE, -1);
    if (idResourceIcon != -1) {
      ImageView imageView = (ImageView) view.findViewById(R.id.icon_widening);
      imageView.setImageResource(idResourceIcon);
    }
    textView.setText(message);
    view.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        getFragmentManager().popBackStack();
      }
    });

    new Handler().postDelayed(new Runnable() {
      @Override public void run() {
        if (OdigeoScreenWarningMessage.this.isVisible()) {
          getFragmentManager().popBackStack();
        }
      }
    }, TIME_TO_WIDENNING);
    return view;
  }

  /**
   * The system calls this only when creating the layout in a dialog.
   */
  @Override public Dialog onCreateDialog(Bundle savedInstanceState) {
    // The only reason you might override this method when using onCreateView() is
    // to modify any dialog characteristics. For example, the dialog includes a
    // title by default, but your custom layout might not need it. So here you can
    // remove the dialog title, but you must call the superclass to get the Dialog.
    Dialog dialog = super.onCreateDialog(savedInstanceState);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    return dialog;
  }
}
