package com.odigeo.app.android.lib.models.dto;

import android.support.annotation.Nullable;
import java.io.Serializable;

/**
 * Created by ximena.perez on 27/01/2015.
 */
public enum SegmentTypeIndexDTO implements Serializable {
  FIRST(0), SECOND(1), THIRD(2), FOURTH(3), FIFTH(4), SIXTH(5);

  /**
   * Index to search on the array of segments
   */
  int index;

  SegmentTypeIndexDTO(int index) {
    this.index = index;
  }

  public static SegmentTypeIndexDTO fromValue(String v) {
    return valueOf(v);
  }

  /**
   * Gets the corresponding {@link com.odigeo.app.android.lib.models.dto.SegmentTypeIndexDTO}
   *
   * @param index index to search in enum options
   * @return The {@link com.odigeo.app.android.lib.models.dto.SegmentTypeIndexDTO} with the
   * desired index, or null if the index is not found
   */
  @Nullable public static SegmentTypeIndexDTO fromIndex(int index) {
    for (SegmentTypeIndexDTO segmentTypeIndexDTO : SegmentTypeIndexDTO.values()) {
      if (segmentTypeIndexDTO.getIndex() == index) {
        return segmentTypeIndexDTO;
      }
    }
    return null;
  }

  public int getIndex() {
    return index;
  }

  public String value() {
    return name();
  }

}
