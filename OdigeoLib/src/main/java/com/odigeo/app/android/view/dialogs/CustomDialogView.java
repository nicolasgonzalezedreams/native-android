package com.odigeo.app.android.view.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

public class CustomDialogView extends AlertDialog.Builder {

  public CustomDialogView(Context context, String title, String message, String positiveText,
      DialogInterface.OnClickListener positiveListener) {
    super(context);
    this.setTitle(title);
    this.setMessage(message);
    this.setPositiveButton(positiveText, positiveListener);
  }

  public CustomDialogView(Context context, String title, String message, String positiveText,
      DialogInterface.OnClickListener positiveListener, String negativeText,
      DialogInterface.OnClickListener negativeListener) {
    super(context);
    this.setTitle(title);
    this.setMessage(message);
    this.setPositiveButton(positiveText, positiveListener);
    this.setNegativeButton(negativeText, negativeListener);
  }
}
