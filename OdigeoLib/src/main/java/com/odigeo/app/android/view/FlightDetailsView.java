package com.odigeo.app.android.view;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.ui.widgets.SummaryTravelWidgetNew;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.navigator.FlightDetailsNavigator;
import com.odigeo.app.android.view.helpers.DateHelper;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.presenter.FlightDetailsPresenter;
import com.odigeo.presenter.contracts.navigators.FlightDetailsNavigatorInterface;
import com.odigeo.presenter.contracts.views.FlightDetailsViewInterface;
import com.odigeo.tools.DurationFormatter;
import java.util.List;

public class FlightDetailsView extends BaseView<FlightDetailsPresenter>
    implements FlightDetailsViewInterface {

  private Booking mBooking;
  private LinearLayout mLayoutForFlights;

  public static FlightDetailsView newInstance(Booking booking) {
    FlightDetailsView myFlightDetailsView = new FlightDetailsView();
    Bundle args = new Bundle();
    args.putSerializable(EXTRA_BOOKING, booking);
    myFlightDetailsView.setArguments(args);
    return myFlightDetailsView;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mBooking = (Booking) getArguments().getSerializable(FlightDetailsNavigator.EXTRA_BOOKING);
  }

  @Override protected FlightDetailsPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideFlightDetailsPresenter(this, (FlightDetailsNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.flight_details_view;
  }

  @Override protected void initComponent(View view) {
    mLayoutForFlights = (LinearLayout) view.findViewById(R.id.layout_for_flights_summary);
    drawFlightsWidgets();
  }

  @Override protected void setListeners() {
    //Nothing
  }

  @Override protected void initOneCMSText(View view) {
    //Nothing
  }

  private DateHelper getDateHelper() {
    return AndroidDependencyInjector.getInstance().provideDateHelper();
  }

  private DurationFormatter getDurationFormatter() {
    return dependencyInjector.provideDurationFormatter();
  }

  private void drawFlightsWidgets() {
    List<Segment> segments = mBooking.getSegments();
    if (segments != null && !segments.isEmpty()) {
      int size = segments.size();
      for (int index = 0; index < size; index++) {
        Segment segment = segments.get(index);
        TravelType travelType;
        if (mBooking.getTripType().equalsIgnoreCase(Booking.TRIP_TYPE_ONE_WAY)) {
          travelType = TravelType.SIMPLE;
        } else if (mBooking.getTripType().equalsIgnoreCase(Booking.TRIP_TYPE_ROUND_TRIP)) {
          travelType = TravelType.ROUND;
        } else {
          travelType = TravelType.MULTIDESTINATION;
        }
        SummaryTravelWidgetNew widget =
            new SummaryTravelWidgetNew(getContext(), segment, travelType, index,
                getDurationFormatter());
        ViewUtils.setSeparator(widget, getContext());
        mLayoutForFlights.addView(widget);
      }
    }
  }
}
