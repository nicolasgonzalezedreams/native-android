package com.odigeo.app.android.lib.models;

import android.support.annotation.DrawableRes;

/**
 * Model for pages that are downloaded form the WS, this page contains an URL for getting the page
 * image
 *
 * @author M.Sc. Javier Silva Perez
 * @since 06/04/15.
 */
public class WalkthroughPageWS extends WalkthroughPageStatic {

  private String imageUrl;

  /**
   * Creates a new page item using all its parameters
   *
   * @param imageUrl Drawable reference to be shown
   * @param imageRes Drawable resource to show as placeholder while the final image is downloaded
   * @param title Page title
   * @param description description of the page
   */
  public WalkthroughPageWS(String imageUrl, @DrawableRes int imageRes, String title,
      String description) {
    super(imageRes, title, description);
    this.imageUrl = imageUrl;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }
}
