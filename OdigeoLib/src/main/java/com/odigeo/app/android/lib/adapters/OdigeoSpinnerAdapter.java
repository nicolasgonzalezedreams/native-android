package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.holders.BaseHolder;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.data.entity.BaseSpinnerItem;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import java.util.List;

/**
 * Parametrized adapter to be used with the {@link com.odigeo.app.android.lib.ui.widgets.base.OdigeoSpinner}.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/05/15
 */
public class OdigeoSpinnerAdapter<T extends BaseSpinnerItem> extends BaseOdigeoAdapter<T> {

  private OdigeoImageLoader mImageLoader;

  /**
   * Constructor
   *
   * @param context Context where be called the adapter.
   * @param objects List of the object to be displayed.
   */
  public OdigeoSpinnerAdapter(Context context, List<T> objects) {
    super(context, objects);

    mImageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();
  }

  @Override protected View buildViewAndHolder(View convertView, ViewGroup parent, int layout) {
    if (convertView == null) {
      convertView = LayoutInflater.from(getContext()).inflate(layout, parent, false);
      ViewHolder holder = new ViewHolder(convertView);
      convertView.setTag(holder);
    }
    return convertView;
  }

  @Override protected void drawItem(BaseHolder baseHolder, BaseSpinnerItem item) {
    ViewHolder holder = (ViewHolder) baseHolder;
    //If has a String to show.
    if (!TextUtils.isEmpty(item.getShownText())) {
      holder.textView.setText(item.getShownText());
    } else if (!item.getShownTextKey().equals(BaseSpinnerItem.EMPTY_STRING)) {
      //If has a valid resource to show.
      holder.textView.setText(LocalizablesFacade.getString(getContext(), item.getShownTextKey()));
    } else {
      //If the item, has no text string or text resource, clean the text view
      holder.textView.setText(null);
    }
    //If has a valid image resource.
    if (item.getImageId() != BaseSpinnerItem.EMPTY_RESOURCE) {
      holder.imageView.setVisibility(View.VISIBLE);
      holder.imageView.setImageResource(item.getImageId());
    } else if (item.getImageUrl() != null) {
      // If the item has an url to download the image
      holder.imageView.setVisibility(View.VISIBLE);

      if (item.getPlaceHolder() != BaseSpinnerItem.EMPTY_RESOURCE) {
        mImageLoader.load(holder.imageView, item.getImageUrl(), item.getPlaceHolder());
      } else {
        mImageLoader.load(holder.imageView, item.getImageUrl());
      }
    } else {
      holder.imageView.setVisibility(View.GONE);
    }
  }

  /**
   * Class to retain the instances of the views in a item.
   */
  private static class ViewHolder extends BaseHolder {
    final TextView textView;
    final ImageView imageView;

    ViewHolder(View view) {
      super(view);
      textView = ((TextView) view.findViewById(android.R.id.text1));
      imageView = ((ImageView) view.findViewById(R.id.text_image));
    }
  }
}
