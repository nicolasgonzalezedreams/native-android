package com.odigeo.app.android.lib.models.transform;

import android.support.annotation.NonNull;
import android.util.SparseArray;
import com.odigeo.app.android.lib.models.CollectionEstimationFees;
import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import com.odigeo.app.android.lib.models.SegmentGroup;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.BaggageIncludedDTO;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.CollectionEstimationFeesDTO;
import com.odigeo.app.android.lib.models.dto.CollectionMethodDTO;
import com.odigeo.app.android.lib.models.dto.CollectionMethodKeyPriceDTO;
import com.odigeo.app.android.lib.models.dto.CollectionMethodLegendDTO;
import com.odigeo.app.android.lib.models.dto.CollectionMethodTypeDTO;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;
import com.odigeo.app.android.lib.models.dto.ItinerarySortCriteriaDTO;
import com.odigeo.app.android.lib.models.dto.LocationDTO;
import com.odigeo.app.android.lib.models.dto.SectionDTO;
import com.odigeo.app.android.lib.models.dto.SectionResultDTO;
import com.odigeo.app.android.lib.models.dto.SegmentDTO;
import com.odigeo.app.android.lib.models.dto.SegmentResultDTO;
import com.odigeo.app.android.lib.models.dto.responses.FlightSearchResponse;
import com.odigeo.app.android.lib.utils.CollectionMethodComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Parse the flight search response using a wrapper in order to simplify its access
 *
 * @author Manuel Ortiz
 * @author Javier Silva
 * @version 1.0
 * @since 10/09/2014
 */
public class FlightSearchTransform {

  private static final int MIN_NO_SEGMENTS_1 = 3;
  private static final int MIN_NO_SEGMENTS_2 = 4;
  private static final int MIN_NO_SEGMENTS_3 = 5;

  public final void makeFlightSearchResponseFriendly(FlightSearchResponse searchResponse,
      int noSegments, ItinerarySortCriteriaDTO sortCriteria) {
    //Set all the Segments Result into a Hashmap
    SparseArray<SegmentDTO> mapSegmentsResult = getSegmentDTOSparseArray(searchResponse);

    //Set all the Sections Result into a hashmap
    SparseArray<SectionDTO> mapSections = getSectionDTOSparseArray(searchResponse);

    SparseArray<LocationDTO> mapLocations = getLocationDTOSparseArray(searchResponse);

    List<CarrierDTO> mapCarriers =
        searchResponse.getItineraryResultsPage().getLegend().getCarriers();

    SparseArray<CollectionEstimationFees> collectionEstimationFees = null;

    collectionEstimationFees = new SparseArray<CollectionEstimationFees>();
    //Full price enabled?
    if (sortCriteria == ItinerarySortCriteriaDTO.MINIMUM_PURCHASABLE_PRICE) {

      SparseArray<CollectionMethodDTO> collectionMethods =
          getCollectionMethodDTOSparseArray(searchResponse);

      for (CollectionEstimationFeesDTO collectionEstimationFee : searchResponse.getItineraryResultsPage()
          .getLegend()
          .getCollectionEstimationFees()) {

        CollectionEstimationFees fees = new CollectionEstimationFees();
        fees.setId(collectionEstimationFee.getId());
        fees.setCollectionMethodFees(new HashMap<String, CollectionMethodWithPrice>());

        collectionEstimationFees.put(fees.getId(), fees);

        List<CollectionMethodWithPrice> newListCollectionMethodFees =
            getCollectionMethodWithPrices(collectionMethods, collectionEstimationFee);

        //Order the list, from the cheapest to the most expensive
        orderFees(fees, newListCollectionMethodFees);
      }
    }

    //Translate the segments of each Itinerary result in a friendlier way
    translateSegments(searchResponse, noSegments, mapSegmentsResult, collectionEstimationFees);

    for (FareItineraryDTO itineraryResult : searchResponse.getItineraryResultsPage()
        .getItineraryResults()) {
      int indexSegmentGroup = 0;

      for (SegmentGroup segmentGroup : itineraryResult.getSegmentGroups()) {
        segmentGroup.setSegmentGroupIndex(indexSegmentGroup++);

        //Set the itineraryResult parent
        segmentGroup.setItineraryResultParent(itineraryResult);

        //Add the sections objects to each segment
        for (SegmentWrapper segmentWrapper : segmentGroup.getSegmentWrappers()) {
          //Add the Carrier object to the segment and to the itinerary
          segmentWrapper.setCarrier(mapCarriers.get(segmentWrapper.getSegment().getCarrier()));

          itineraryResult.getCarriers().add(segmentWrapper.getCarrier());

          if (segmentGroup.getCarrier() == null) {
            segmentGroup.setCarrier(segmentWrapper.getCarrier());
          }

          fillSections(mapSections, mapLocations, mapCarriers, segmentWrapper);
        }
      }
    }
  }

  /**
   * Using the loaded sections add them to the segment wrapper
   *
   * @param mapSections Sections of the response
   * @param mapLocations Locations of the response
   * @param mapCarriers Carriers of the response
   * @param segmentWrapper Section wrapper to add the sections
   */
  private void fillSections(@NonNull SparseArray<SectionDTO> mapSections,
      @NonNull SparseArray<LocationDTO> mapLocations, @NonNull List<CarrierDTO> mapCarriers,
      @NonNull SegmentWrapper segmentWrapper) {
    for (Integer indexSection : segmentWrapper.getSegment().getSections()) {
      SectionDTO section = mapSections.get(indexSection);

      if (section != null) {
        section.setLocationFrom(mapLocations.get(section.getFrom()));
        section.setLocationTo(mapLocations.get(section.getTo()));

        if (section.getOperatingCarrier() != null) {
          section.setOperatingCarrierObject(mapCarriers.get(section.getOperatingCarrier()));
        }
      }

      segmentWrapper.getSectionsObjects().add(section);
    }
  }

  /**
   * Translate the segments of each Itinerary result in a friendlier way
   *
   * @param searchResponse Current search response
   * @param noSegments total number of segments
   * @param mapSegmentsResult Map of segments of the flight
   * @param collectionEstimationFees Estimation fees to add
   */
  private void translateSegments(FlightSearchResponse searchResponse, int noSegments,
      SparseArray<SegmentDTO> mapSegmentsResult,
      SparseArray<CollectionEstimationFees> collectionEstimationFees) {
    for (FareItineraryDTO itineraryResult : searchResponse.getItineraryResultsPage()
        .getItineraryResults()) {
      if (noSegments > 0) {
        itineraryResult.getSegmentGroups()
            .add(makeSegmentsFriendly(itineraryResult.getFirstSegments(),
                itineraryResult.getFirstSegmentsKeys(), mapSegmentsResult,
                itineraryResult.getFirstSegmentBaggageIncluded()));
      }

      if (noSegments > 1) {
        itineraryResult.getSegmentGroups()
            .add(makeSegmentsFriendly(itineraryResult.getSecondSegments(),
                itineraryResult.getSecondSegmentsKeys(), mapSegmentsResult,
                itineraryResult.getSecondSegmentBaggageIncluded()));
      }

      if (noSegments > 2) {
        itineraryResult.getSegmentGroups()
            .add(makeSegmentsFriendly(itineraryResult.getThirdSegments(),
                itineraryResult.getThirdSegmentsKeys(), mapSegmentsResult,
                itineraryResult.getThirdSegmentBaggageIncluded()));
      }

      if (noSegments > MIN_NO_SEGMENTS_1) {
        itineraryResult.getSegmentGroups()
            .add(makeSegmentsFriendly(itineraryResult.getFourthSegments(),
                itineraryResult.getFourthSegmentsKeys(), mapSegmentsResult,
                itineraryResult.getFourthSegmentBaggageIncluded()));
      }

      if (noSegments > MIN_NO_SEGMENTS_2) {
        itineraryResult.getSegmentGroups()
            .add(makeSegmentsFriendly(itineraryResult.getFifthSegments(),
                itineraryResult.getFifthSegmentsKeys(), mapSegmentsResult,
                itineraryResult.getFifthSegmentBaggageIncluded()));
      }

      if (noSegments > MIN_NO_SEGMENTS_3) {
        itineraryResult.getSegmentGroups()
            .add(makeSegmentsFriendly(itineraryResult.getSixthSegments(),
                itineraryResult.getSixthSegmentsKeys(), mapSegmentsResult,
                itineraryResult.getSixthSegmentBaggageIncluded()));
      }

      //Set the collectionMethodEstimationFees
      itineraryResult.setCollectionMethodFeesObject(
          collectionEstimationFees.get(itineraryResult.getCollectionMethodFees()));
    }
  }

  private void orderFees(CollectionEstimationFees fees,
      List<CollectionMethodWithPrice> newListCollectionMethodFees) {
    Collections.sort(newListCollectionMethodFees, new CollectionMethodComparator());

    if (!newListCollectionMethodFees.isEmpty()) {
      fees.setCheapest(newListCollectionMethodFees.get(0));
      fees.getCheapest().setCheapest(true);

      for (CollectionMethodWithPrice methodWithPrice : newListCollectionMethodFees) {
        fees.getCollectionMethodFees()
            .put(methodWithPrice.getCollectionMethod().getCreditCardType().getCode(),
                methodWithPrice);
      }
    }
  }

  /**
   * Gets a list of the collections methods with price from the service collection method
   *
   * @param collectionMethods Collection methods to check
   * @param collectionEstimationFee Estimation fee
   * @return The list of collections methods with price
   */
  private List<CollectionMethodWithPrice> getCollectionMethodWithPrices(
      SparseArray<CollectionMethodDTO> collectionMethods,
      CollectionEstimationFeesDTO collectionEstimationFee) {
    List<CollectionMethodWithPrice> newListCollectionMethodFees =
        new ArrayList<CollectionMethodWithPrice>();

    for (CollectionMethodKeyPriceDTO methodKeyPriceDTO : collectionEstimationFee.getCollectionMethodFees()) {

      CollectionMethodDTO collectionMethodDTO =
          collectionMethods.get(methodKeyPriceDTO.getCollectionMethodKey());

      //Only Credit Card payment method
      if (collectionMethodDTO != null
          && collectionMethodDTO.getType() == CollectionMethodTypeDTO.CREDITCARD) {

        CollectionMethodWithPrice methodWithPrice = new CollectionMethodWithPrice();

        methodWithPrice.setCollectionMethod(collectionMethodDTO);

        //This information is not used
        methodWithPrice.setCollectionMethodKey(methodKeyPriceDTO.getCollectionMethodKey());
        methodWithPrice.setPrice(methodKeyPriceDTO.getPrice());

        newListCollectionMethodFees.add(methodWithPrice);
      }
    }
    return newListCollectionMethodFees;
  }

  /**
   * Creates a Sparse array with all the collection methods included in the response
   *
   * @param searchResponse {@link FlightSearchResponse} to get the collections methods list
   * @return A sparse array with the response collections using {@link
   * CollectionMethodLegendDTO#getId()} as key
   */
  private SparseArray<CollectionMethodDTO> getCollectionMethodDTOSparseArray(
      FlightSearchResponse searchResponse) {
    SparseArray<CollectionMethodDTO> collectionMethods = new SparseArray<CollectionMethodDTO>();
    for (CollectionMethodLegendDTO collectionMethodGroup : searchResponse.getItineraryResultsPage()
        .getLegend()
        .getCollectionMethods()) {
      collectionMethods.put(collectionMethodGroup.getId(),
          collectionMethodGroup.getCollectionMethod());
    }
    return collectionMethods;
  }

  /**
   * Get a map of carriers form the search response
   *
   * @param searchResponse {@link FlightSearchResponse} to get the carriers list
   * @return A map with all the carriers from the response, the key will be de carried code
   */
  private Map<String, CarrierDTO> getStringCarrierDTOMap(FlightSearchResponse searchResponse) {
    Map<String, CarrierDTO> mapCarriers = new HashMap<String, CarrierDTO>();
    for (CarrierDTO carrier : searchResponse.getItineraryResultsPage().getLegend().getCarriers()) {
      mapCarriers.put(carrier.getCode(), carrier);
    }
    return mapCarriers;
  }

  /**
   * Creates a Sparse array with all the locations included in the response
   *
   * @param searchResponse {@link FlightSearchResponse} to get the locations list
   * @return A sparse array with the response locations using {@link LocationDTO#getGeoNodeId()}
   * as key
   */
  private SparseArray<LocationDTO> getLocationDTOSparseArray(FlightSearchResponse searchResponse) {
    SparseArray<LocationDTO> mapLocations = new SparseArray<LocationDTO>();
    for (LocationDTO location : searchResponse.getItineraryResultsPage()
        .getLegend()
        .getLocations()) {
      mapLocations.put(location.getGeoNodeId(), location);
    }
    return mapLocations;
  }

  /**
   * Creates a Sparse array with all the sections included in the response
   *
   * @param searchResponse {@link FlightSearchResponse} to get the sections list
   * @return A sparse array with the response sections using {@link SectionDTO#getId()} as key
   */
  private SparseArray<SectionDTO> getSectionDTOSparseArray(FlightSearchResponse searchResponse) {
    SparseArray<SectionDTO> mapSections = new SparseArray<SectionDTO>();
    for (SectionResultDTO sectionResult : searchResponse.getItineraryResultsPage()
        .getLegend()
        .getSectionResults()) {
      mapSections.put(sectionResult.getId(), sectionResult.getSection());
    }
    return mapSections;
  }

  /**
   * Creates a Sparse array with all the segments included in the response
   *
   * @param searchResponse {@link FlightSearchResponse} to get the locations list
   * @return A sparse array with the response segments using {@link SegmentResultDTO#getId()} as
   * key
   */
  private SparseArray<SegmentDTO> getSegmentDTOSparseArray(FlightSearchResponse searchResponse) {
    SparseArray<SegmentDTO> mapSegmentsResult = new SparseArray<SegmentDTO>();

    for (SegmentResultDTO segmentResult : searchResponse.getItineraryResultsPage()
        .getLegend()
        .getSegmentResults()) {
      mapSegmentsResult.put(segmentResult.getId(), segmentResult.getSegment());
    }
    return mapSegmentsResult;
  }

  public final SegmentGroup makeSegmentsFriendly(List<Integer> segmentsResultIndexes,
      List<String> segmentsKeys, SparseArray<SegmentDTO> mapSegments,
      BaggageIncludedDTO baggageIncluded) {
    SegmentGroup segmentGroup = new SegmentGroup();

    for (int i = 0; i < segmentsResultIndexes.size(); i++) {
      int segmentResultIndex = segmentsResultIndexes.get(i);
      String segmentKey = null;

      if (segmentsKeys.size() > i) {
        segmentKey = segmentsKeys.get(i);
      }

      SegmentDTO segment = mapSegments.get(segmentResultIndex);

      //Create the Wrapper for the Segment
      SegmentWrapper segmentWrapper = new SegmentWrapper(segmentKey, segment);
      segmentGroup.getSegmentWrappers().add(segmentWrapper);
      segmentGroup.setBaggageIncluded(baggageIncluded);
    }

    return segmentGroup;
  }
}
