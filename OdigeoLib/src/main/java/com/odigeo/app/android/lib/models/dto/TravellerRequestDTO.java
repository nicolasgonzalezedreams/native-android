package com.odigeo.app.android.lib.models.dto;

import java.util.ArrayList;
import java.util.List;

public class TravellerRequestDTO {

  protected List<FrequentFlyerCardCodeDTO> frequentFlyerAirlineCodes;
  protected String travellerTypeName;
  protected String titleName;
  protected String name;
  protected String firstLastName;
  protected String secondLastName;
  protected String middleName;
  protected String gender;
  protected int dayOfBirth;
  protected int monthOfBirth;
  protected int yearOfBirth;
  protected String nationalityCountryCode;
  protected String countryCodeOfResidence;
  protected String identification;
  protected String identificationTypeName;
  protected int identificationExpirationDay;
  protected int identificationExpirationMonth;
  protected int identificationExpirationYear;
  protected String identificationIssueContryCode;
  protected String localityCodeOfResidence;
  protected List<BaggageSelectionDTO> baggageSelections;
  protected String mealName;

  /**
   * Gets the value of the frequentFlyerAirlineCodes property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any
   * modification you make to the returned list will be present inside the JAXB object. This is
   * why there is not a <CODE>set</CODE> method for the frequentFlyerAirlineCodes property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getFrequentFlyerAirlineCodes().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list {@link FrequentFlyerCardCodeDTO }
   */
  public List<FrequentFlyerCardCodeDTO> getFrequentFlyerAirlineCodes() {
    if (frequentFlyerAirlineCodes == null) {
      frequentFlyerAirlineCodes = new ArrayList<FrequentFlyerCardCodeDTO>();
    }
    return this.frequentFlyerAirlineCodes;
  }

  /**
   * Sets the list of the frequentFlyerAirlineCodes property.
   *
   * @param list allowed object is {@link List<FrequentFlyerCardCodeDTO> }
   */
  public void setFrequentFlyerAirlineCodes(List<FrequentFlyerCardCodeDTO> list) {
    this.frequentFlyerAirlineCodes = list;
  }

  /**
   * Gets the value of the travellerTypeName property.
   *
   * @return possible object is {@link String }
   */
  public String getTravellerTypeName() {
    return travellerTypeName;
  }

  /**
   * Sets the value of the travellerTypeName property.
   *
   * @param value allowed object is {@link String }
   */
  public void setTravellerTypeName(String value) {
    this.travellerTypeName = value;
  }

  /**
   * Gets the value of the titleName property.
   *
   * @return possible object is {@link String }
   */
  public String getTitleName() {
    return titleName;
  }

  /**
   * Sets the value of the titleName property.
   *
   * @param value allowed object is {@link String }
   */
  public void setTitleName(String value) {
    this.titleName = value;
  }

  /**
   * Gets the value of the name property.
   *
   * @return possible object is {@link String }
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the value of the name property.
   *
   * @param value allowed object is {@link String }
   */
  public void setName(String value) {
    this.name = value;
  }

  /**
   * Gets the value of the firstLastName property.
   *
   * @return possible object is {@link String }
   */
  public String getFirstLastName() {
    return firstLastName;
  }

  /**
   * Sets the value of the firstLastName property.
   *
   * @param value allowed object is {@link String }
   */
  public void setFirstLastName(String value) {
    this.firstLastName = value;
  }

  /**
   * Gets the value of the secondLastName property.
   *
   * @return possible object is {@link String }
   */
  public String getSecondLastName() {
    return secondLastName;
  }

  /**
   * Sets the value of the secondLastName property.
   *
   * @param value allowed object is {@link String }
   */
  public void setSecondLastName(String value) {
    this.secondLastName = value;
  }

  /**
   * Gets the value of the middleName property.
   *
   * @return possible object is {@link String }
   */
  public String getMiddleName() {
    return middleName;
  }

  /**
   * Sets the value of the middleName property.
   *
   * @param value allowed object is {@link String }
   */
  public void setMiddleName(String value) {
    this.middleName = value;
  }

  /**
   * Gets the value of the gender property.
   *
   * @return possible object is {@link String }
   */
  public String getGender() {
    return gender;
  }

  /**
   * Sets the value of the gender property.
   *
   * @param value allowed object is {@link String }
   */
  public void setGender(String value) {
    this.gender = value;
  }

  /**
   * Gets the value of the dayOfBirth property.
   */
  public int getDayOfBirth() {
    return dayOfBirth;
  }

  /**
   * Sets the value of the dayOfBirth property.
   */
  public void setDayOfBirth(int value) {
    this.dayOfBirth = value;
  }

  /**
   * Gets the value of the monthOfBirth property.
   */
  public int getMonthOfBirth() {
    return monthOfBirth;
  }

  /**
   * Sets the value of the monthOfBirth property.
   */
  public void setMonthOfBirth(int value) {
    this.monthOfBirth = value;
  }

  /**
   * Gets the value of the yearOfBirth property.
   */
  public int getYearOfBirth() {
    return yearOfBirth;
  }

  /**
   * Sets the value of the yearOfBirth property.
   */
  public void setYearOfBirth(int value) {
    this.yearOfBirth = value;
  }

  /**
   * Gets the value of the nationalityCountryCode property.
   *
   * @return possible object is {@link String }
   */
  public String getNationalityCountryCode() {
    return nationalityCountryCode;
  }

  /**
   * Sets the value of the nationalityCountryCode property.
   *
   * @param value allowed object is {@link String }
   */
  public void setNationalityCountryCode(String value) {
    this.nationalityCountryCode = value;
  }

  /**
   * Gets the value of the countryCodeOfResidence property.
   *
   * @return possible object is {@link String }
   */
  public String getCountryCodeOfResidence() {
    return countryCodeOfResidence;
  }

  /**
   * Sets the value of the countryCodeOfResidence property.
   *
   * @param value allowed object is {@link String }
   */
  public void setCountryCodeOfResidence(String value) {
    this.countryCodeOfResidence = value;
  }

  /**
   * Gets the value of the identification property.
   *
   * @return possible object is {@link String }
   */
  public String getIdentification() {
    return identification;
  }

  /**
   * Sets the value of the identification property.
   *
   * @param value allowed object is {@link String }
   */
  public void setIdentification(String value) {
    this.identification = value;
  }

  /**
   * Gets the value of the identificationTypeName property.
   *
   * @return possible object is {@link String }
   */
  public String getIdentificationTypeName() {
    return identificationTypeName;
  }

  /**
   * Sets the value of the identificationTypeName property.
   *
   * @param value allowed object is {@link String }
   */
  public void setIdentificationTypeName(String value) {
    this.identificationTypeName = value;
  }

  /**
   * Gets the value of the identificationExpirationDay property.
   */
  public int getIdentificationExpirationDay() {
    return identificationExpirationDay;
  }

  /**
   * Sets the value of the identificationExpirationDay property.
   */
  public void setIdentificationExpirationDay(int value) {
    this.identificationExpirationDay = value;
  }

  /**
   * Gets the value of the identificationExpirationMonth property.
   */
  public int getIdentificationExpirationMonth() {
    return identificationExpirationMonth;
  }

  /**
   * Sets the value of the identificationExpirationMonth property.
   */
  public void setIdentificationExpirationMonth(int value) {
    this.identificationExpirationMonth = value;
  }

  /**
   * Gets the value of the identificationExpirationYear property.
   */
  public int getIdentificationExpirationYear() {
    return identificationExpirationYear;
  }

  /**
   * Sets the value of the identificationExpirationYear property.
   */
  public void setIdentificationExpirationYear(int value) {
    this.identificationExpirationYear = value;
  }

  /**
   * Gets the value of the identificationIssueContryCode property.
   *
   * @return possible object is {@link String }
   */
  public String getIdentificationIssueContryCode() {
    return identificationIssueContryCode;
  }

  /**
   * Sets the value of the identificationIssueContryCode property.
   *
   * @param value allowed object is {@link String }
   */
  public void setIdentificationIssueContryCode(String value) {
    this.identificationIssueContryCode = value;
  }

  /**
   * Gets the value of the localityCodeOfResidence property.
   *
   * @return possible object is {@link String }
   */
  public String getLocalityCodeOfResidence() {
    return localityCodeOfResidence;
  }

  /**
   * Sets the value of the localityCodeOfResidence property.
   *
   * @param value allowed object is {@link String }
   */
  public void setLocalityCodeOfResidence(String value) {
    this.localityCodeOfResidence = value;
  }

  /**
   * Gets the value of the mealName property.
   *
   * @return possible object is {@link String }
   */
  public String getMealName() {
    return mealName;
  }

  /**
   * Sets the value of the mealName property.
   *
   * @param value allowed object is {@link String }
   */
  public void setMealName(String value) {
    this.mealName = value;
  }

  /**
   * Gets the value of the baggageSelections property.
   *
   * @return possible object is {@link BaggageSelectionDTO }
   */
  public List<BaggageSelectionDTO> getBaggageSelections() {
    if (this.baggageSelections == null) {
      this.baggageSelections = new ArrayList<BaggageSelectionDTO>();
    }

    return this.baggageSelections;
  }

  /**
   * Sets the list of the additional baggage selected by the user
   *
   * @param baggageSelectionRequest List of additional baggage selected by the user
   */
  public void setBaggageSelections(List<BaggageSelectionDTO> baggageSelectionRequest) {
    this.baggageSelections = baggageSelectionRequest;
  }
}

