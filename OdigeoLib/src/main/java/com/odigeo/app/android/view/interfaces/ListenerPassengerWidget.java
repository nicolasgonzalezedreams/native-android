package com.odigeo.app.android.view.interfaces;

import android.widget.ScrollView;
import com.odigeo.data.entity.shoppingCart.ItinerarySortCriteria;
import com.odigeo.data.entity.shoppingCart.TravellerType;
import com.odigeo.data.entity.shoppingCart.request.BaggageSelectionRequest;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.List;

public interface ListenerPassengerWidget {

  boolean IsPassengerSelectedInAnotherWidgetSpinner(int widgetPosition, String travellerName,
      TravellerType passengerSpinnerType);

  void onSetDefaultPassengerInformation(UserTraveller passenger,
      int newMainPassengerPositionSpinner);

  void setNewDefaultPassengerSpinner(int newDefaultPassengerPositionSpinner);

  ScrollView getScrollView();

  void applyDefaultBaggageSelection(
      List<BaggageSelectionRequest> baggageSelections);

  void disableApplyDefaultBaggageSelection();

  void propagateItinerarySortCriteria(ItinerarySortCriteria itinerarySortCriteria);

  void showMembershipPrices();

  void showPricesWithoutMembershipPerks();
}