package com.odigeo.app.android.lib.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.GeneralConstants;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.DistanceUnits;
import com.odigeo.app.android.lib.models.AppSettings;
import com.odigeo.app.android.lib.models.OldMyShoppingCart;
import com.odigeo.app.android.lib.models.Passenger;
import com.odigeo.app.android.lib.models.dto.BaggageDescriptorDTO;
import com.odigeo.app.android.lib.models.dto.BaggageSelectionDTO;
import com.odigeo.app.android.lib.models.dto.SegmentTypeIndexDTO;
import com.odigeo.app.android.lib.models.dto.TravellerDTO;
import com.odigeo.app.android.lib.models.dto.TravellerTypeDTO;
import com.odigeo.presenter.model.QAModeUrlModel;
import com.odigeo.data.entity.carrousel.CampaignCard;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/**
 * Created by manuel on 12/10/14.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 2.1
 * @since 28/01/15
 */
@Deprecated
/* USE preferencesController.class instead */ public final class PreferencesManager {

  private static final int FLIGHTS_LIMIT = 10;
  private static final String STRING_EMPTY = "";
  private static final String ERROR_TAG = "JSON_ERROR_TAG";
  private static final Gson GSON =
      new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
  private static PreferencesManager instance;

  private PreferencesManager() {
  }

  /**
   * Return a singleton instance of {@link com.odigeo.app.android.lib.utils.PreferencesManager}
   *
   * @return A valid instance of the singleton
   */
  public static synchronized PreferencesManager getInstance() {
    //Only make a new instance of the object when its required for first time
    if (instance == null) {
      instance = new PreferencesManager();
    }
    return instance;
  }

  /**
   * Remove all searchOptions and shopping cart
   */
  public static void clearSearchOptionsAndShoppingCart(Context context) {

    List<SearchOptions> searchOptionsList = readSearchOptions(context);

    //Clear all shopping cart
    for (SearchOptions searchOptions : searchOptionsList) {
      SharedPreferences.Editor editor = context.getSharedPreferences(
          Constants.SHARED_PREFERENCE_FILE_SHOPPING_CART + searchOptions.getId(),
          Context.MODE_PRIVATE).edit();
      editor.clear();
      editor.apply();
    }

    //Clear seach options
    SharedPreferences.Editor editor =
        context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_SEARCH_OPTIONS,
            Context.MODE_PRIVATE).edit();
    editor.clear();
    editor.apply();
  }

  public static List<Passenger> readPassengers(Context context) {
    List<Passenger> passengers = new ArrayList<Passenger>();

    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_PASSENGERS,
            Context.MODE_PRIVATE);

    if (preferences != null && preferences.contains(Constants.PREFERENCE_PASSENGERS)) {
      String strPassengers = preferences.getString(Constants.PREFERENCE_PASSENGERS, null);

      TypeToken t = new TypeToken<ArrayList<Passenger>>() {
      };
      Type typeList = t.getType();

      passengers = new Gson().fromJson(strPassengers, typeList);
    }

    return passengers;
  }

  public static List<Passenger> readPassengers(Context context, TravellerTypeDTO travellerType) {
    List<Passenger> passengers = new ArrayList<>();

    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_PASSENGERS,
            Context.MODE_PRIVATE);

    if (preferences != null && preferences.contains(Constants.PREFERENCE_PASSENGERS)) {
      String strPassengers = preferences.getString(Constants.PREFERENCE_PASSENGERS, null);

      TypeToken t = new TypeToken<ArrayList<Passenger>>() {
      };
      Type typeList = t.getType();

      passengers = new Gson().fromJson(strPassengers, typeList);
    }

    //Filter the list, by the given type
    List<Passenger> passengersByType = new ArrayList<Passenger>();
    for (Passenger passenger : passengers) {
      if (passenger.getTravellerType() == travellerType) {
        passengersByType.add(passenger);
      }
    }

    return passengersByType;
  }

  /**
   * Save the list of passengers in shared preferences
   *
   * @param context Context to save the shared preferences file
   * @param passengers List of passengers to be saved
   */
  public static void savePassengers(Context context, List<Passenger> passengers) {
    //Set the passenger default
    Passenger adultPassenger = null;
    int noDefaultPassengers = 0;

    for (Passenger passenger : passengers) {
      //Check if come the second last name in the first last name

      //TODO Solve what this code was solving, this solution is weird
      /**
       String lastName = passenger.getFirstLastName();
       String secondLastName = passenger.getSecondLastName();
       if (secondLastName != null
       && !TextUtils.isEmpty(secondLastName)
       && lastName.contains(secondLastName)) {
       passenger.setFirstLastName(lastName.replace(secondLastName, "").trim());
       }
       **/
      if (passenger.getTravellerType() == TravellerTypeDTO.ADULT) {
        if (adultPassenger == null) {
          adultPassenger = passenger;
        }

        if (passenger.isPassengerDefault()) {
          noDefaultPassengers++;
        }
      } else {
        passenger.setPassengerDefault(false);
      }
    }

    //If there isn't a  adult Passenger, set the first one as the default
    if (adultPassenger != null && noDefaultPassengers == 0) {
      adultPassenger.setPassengerDefault(true);
    }

    //Save the passengers list
    String strPassengers = new Gson().toJson(passengers);

    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_PASSENGERS,
            Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putString(Constants.PREFERENCE_PASSENGERS, strPassengers);
    editor.apply();
  }

  /**
   * This method returns the list of URLs for the QA Mode created by the user, stored in Shared
   * Preferences. If there are no URLs saved,
   * it returns the known URLs for dev environment and prod environment, which will be saved there
   * from now on.
   *
   * @param context application context
   * @return URLs created previously by the user
   */
  public static List<QAModeUrlModel> readQAModeURLs(Context context) {
    List<QAModeUrlModel> urls = new ArrayList<>();

    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_QA_MODE_URLS,
            Context.MODE_PRIVATE);

    if (preferences != null) {
      String strUrls = preferences.getString(Constants.PREFERENCE_QA_MODE_URLS, null);
      if (strUrls == null) {
        urls.add(new QAModeUrlModel(
            AndroidDependencyInjector.getInstance().provideDomainHelper().getUrl(), true));
        urls.add(new QAModeUrlModel(Configuration.getInstance().getProdDomain(), false));
      } else {
        TypeToken t = new TypeToken<ArrayList<QAModeUrlModel>>() {
        };
        Type typeList = t.getType();
        urls = new Gson().fromJson(strUrls, typeList);
      }
    }

    return urls;
  }

  /**
   * This method saves the URLs created for the QA Mode
   *
   * @param context application context
   * @param qaModeUrl list of URLs created by the user
   */
  public static void saveQAModeURLs(Context context, List<QAModeUrlModel> qaModeUrl) {
    //Save the URLs list
    TypeToken t = new TypeToken<ArrayList<QAModeUrlModel>>() {
    };
    Type typeList = t.getType();
    String strURLs = new Gson().toJson(qaModeUrl, typeList);

    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_QA_MODE_URLS,
            Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putString(Constants.PREFERENCE_QA_MODE_URLS, strURLs);
    editor.apply();
  }

  /**
   * This method returns the URL selected by the user, and if the user didn't select any, it will
   * return the DOMAIN from Constants
   *
   * @param context application context
   * @return url selected previously by the user
   */
  public static String getQAModeURL(Context context) {

    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_QA_MODE_URLS,
            Context.MODE_PRIVATE);
    String strUrl = STRING_EMPTY;
    if (preferences != null) {
      strUrl = preferences.getString(Constants.PREFERENCE_QA_MODE_URL, null);
      if (strUrl == null) {
        return AndroidDependencyInjector.getInstance().provideDomainHelper().getUrl();
      }
    }
    return strUrl;
  }

  public static void saveFlights(Context context, List<SearchOptions> searchOptionsList,
      String typeHistorySearch) {
    //Save the list
    String historySearch = new Gson().toJson(searchOptionsList);

    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_HISTORY_SEARCH,
            Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putString(typeHistorySearch, historySearch);
    editor.apply();
  }

  public static void savedFlight(Context context, SearchOptions searchFlight,
      String typeHistorySearch) {
    List<SearchOptions> searchesList = readFlights(context, typeHistorySearch);

    searchesList.remove(searchFlight);
    searchesList.add(0, searchFlight);

    //Remove the others after the 10th
    if (searchesList.size() > FLIGHTS_LIMIT) {
      searchesList.remove(searchesList.size() - 1);
    }

    saveFlights(context, searchesList, typeHistorySearch);
  }

  public static List<SearchOptions> readFlights(Context context, String typeHistorySearch) {
    List<SearchOptions> searchesFlights = new ArrayList<>();
    try {
      SharedPreferences preferences =
          context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_HISTORY_SEARCH,
              Context.MODE_PRIVATE);
      if (preferences != null && preferences.contains(typeHistorySearch)) {
        String historyString = preferences.getString(typeHistorySearch, null);
        if (historyString != null) {
          TypeToken t = new TypeToken<ArrayList<SearchOptions>>() {
          };
          Type typeList = t.getType();
          Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
          searchesFlights = gson.fromJson(historyString, typeList);
          JSONArray jsonArray = new JSONArray(historyString);
          for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject search = jsonArray.getJSONObject(i);
            String stringValue = search.getString("searchDate");
            SimpleDateFormat f = new SimpleDateFormat("MMM d, yyyy h:mm:ss a", Locale.US);
            Date d = f.parse(stringValue);
            final long milliseconds = d.getTime();
            searchesFlights.get(i).setSearchDate(milliseconds);
          }

          for (SearchOptions searchOptions : searchesFlights) {
            searchOptions.updateCabinClass();
          }
        }
      }
    } catch (Exception e) {
      Log.e(ERROR_TAG, e.toString());
    }
    return searchesFlights;
  }

  @NonNull public static List<SearchOptions> readSearchOptions(Context context) {
    List<SearchOptions> searchOptions = new ArrayList<>();
    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_SEARCH_OPTIONS,
            Context.MODE_PRIVATE);
    if (preferences != null && preferences.contains(Constants.PREFERENCE_SEARCH_OPTIONS)) {
      String strSearchOptions = preferences.getString(Constants.PREFERENCE_SEARCH_OPTIONS, null);
      try {
        TypeToken t = new TypeToken<ArrayList<SearchOptions>>() {
        };
        Type typeList = t.getType();
        searchOptions = new Gson().fromJson(strSearchOptions, typeList);
        for (SearchOptions searchOption : searchOptions) {
          searchOption.updateCabinClass();
        }
      } catch (JsonSyntaxException | NumberFormatException e) {
        Log.e("PreferencesManager", e.getMessage());
      }
    }
    return searchOptions;
  }

  /**
   * Update the preference file where is stored the SearchOptions object.
   *
   * @param context The context of the application.
   */
  public static void updateSearchOptions(Context context) {
    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_SEARCH_OPTIONS,
            Context.MODE_PRIVATE);

    if (preferences != null && preferences.contains(Constants.PREFERENCE_SEARCH_OPTIONS)) {
      String strSearchOptions = preferences.getString(Constants.PREFERENCE_SEARCH_OPTIONS, null);

      strSearchOptions = Util.updateTripAtPreferences(strSearchOptions);

      SharedPreferences.Editor editor = preferences.edit();
      editor.putString(Constants.PREFERENCE_SEARCH_OPTIONS, strSearchOptions);
      editor.apply();
    }
  }

  /**
   * Update the preference files where are stored the ShoppingCart objects .
   *
   * @param context The context of the application.
   */
  public static void updateShoppingCart(Context context) {
    List<SearchOptions> searchOptionsList = PreferencesManager.readSearchOptions(context);
    for (SearchOptions searchOptions : searchOptionsList) {
      long idSearchOptions = searchOptions.getId();
      SharedPreferences preferences = context.getSharedPreferences(
          Constants.SHARED_PREFERENCE_FILE_SHOPPING_CART + idSearchOptions, Context.MODE_PRIVATE);
      if (preferences != null && preferences.contains(Constants.PREFERENCE_SHOPPING_CART)) {
        String strShoppingCart = preferences.getString(Constants.PREFERENCE_SHOPPING_CART, null);
        strShoppingCart = Util.updateShoppingCart(strShoppingCart);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.PREFERENCE_SHOPPING_CART, strShoppingCart);
        editor.apply();
        Log.e(Constants.TAG_LOG, strShoppingCart);
      }
    }
  }

  /**
   * Update the preference files where the Search History are stored .
   *
   * @param context The context of the application.
   */
  public static void updateHistorySearch(Context context, String typeHistorySearch) {
    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_HISTORY_SEARCH,
            Context.MODE_PRIVATE);
    String historyString = "";
    if (preferences != null && preferences.contains(typeHistorySearch)) {
      historyString = preferences.getString(typeHistorySearch, null);
      if (historyString != null) {
        historyString = Util.updateHistoryJSON(historyString);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(typeHistorySearch, historyString);
        editor.apply();
      }
    }
  }

  public static void setNewVersion(Context context) {
    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putBoolean(Constants.PREFERENCE_NEW_VERSION, true);
    editor.apply();
  }

  public static boolean isNewVersion(Context context) {
    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, context.MODE_PRIVATE);
    //Check if need check the version.
    if (preferences.getBoolean(Constants.PREFERENCE_NEW_VERSION, true)) {
      //Change the value to prevent the repeated show of the dialog.
      SharedPreferences.Editor editor = preferences.edit();
      editor.putBoolean(Constants.PREFERENCE_NEW_VERSION, false);
      editor.apply();
      return true;
    }
    return false;
  }

  public static void saveMyShoppingCart(Context context, OldMyShoppingCart oldMyShoppingCart,
      SearchOptions searchOptions) {
    Gson gson = new GsonBuilder().setVersion(Constants.DTOVersion.MSL_V2).create();
    String strMyShoppingCart = gson.toJson(oldMyShoppingCart);

    SharedPreferences preferences = context.getSharedPreferences(
        Constants.SHARED_PREFERENCE_FILE_SHOPPING_CART + searchOptions.getId(),
        Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putString(Constants.PREFERENCE_SHOPPING_CART, strMyShoppingCart);
    editor.apply();
  }

  public static OldMyShoppingCart readShoppingCart(Context context, SearchOptions searchOptions) {
    SharedPreferences preferences = context.getSharedPreferences(
        Constants.SHARED_PREFERENCE_FILE_SHOPPING_CART + searchOptions.getId(),
        Context.MODE_PRIVATE);

    OldMyShoppingCart oldMyShoppingCart = null;

    if (preferences != null && preferences.contains(Constants.PREFERENCE_SHOPPING_CART)) {
      String strShoppingCart = preferences.getString(Constants.PREFERENCE_SHOPPING_CART, null);

      oldMyShoppingCart = new Gson().fromJson(strShoppingCart, OldMyShoppingCart.class);
      boolean updated = updateShoppingCarVersion(oldMyShoppingCart, searchOptions);
      if (updated) {
        saveMyShoppingCart(context, oldMyShoppingCart, searchOptions);
      }
    }
    return oldMyShoppingCart;
  }

  /**
   * Updates the shopping car data structure version using the current shopping cart version vs
   * {@link
   * com.odigeo.app.android.lib.consts.Constants.DTOVersion#CURRENT}
   *
   * @param shoppingCart The shopping car that contains all the information to be updated
   * @return TRUE if the shopping cart was updated, false if not
   */
  public static boolean updateShoppingCarVersion(OldMyShoppingCart shoppingCart,
      SearchOptions searchOptions) {
    boolean updated = false;

    // If the shopping car doesnt have a version set the default
    if (shoppingCart.getDtoVersion() == null) {
      shoppingCart.setDtoVersion(Constants.DTOVersion.DEFAULT);
    }

    if (shoppingCart.getDtoVersion() < Constants.DTOVersion.MSL_V2) {
      //Migrate baggage conditions
      updateBaggageFromDefault(shoppingCart);
      shoppingCart.setDtoVersion(Constants.DTOVersion.MSL_V2);
      updated = true;
    }

    //If the shopping car version needs to be updated to V3 (Baggage details) version
    if (shoppingCart.getDtoVersion() < Constants.DTOVersion.BAGGAGE_DETAILS) {
      updateBaggageToBaggageDetails(shoppingCart, searchOptions);
      shoppingCart.setDtoVersion(Constants.DTOVersion.BAGGAGE_DETAILS);
      updated = true;
    }

    if (shoppingCart.getDtoVersion() < Constants.DTOVersion.SPINNER_UPDATE) {
      searchOptions.updateCabinClass();
      shoppingCart.setDtoVersion(Constants.DTOVersion.SPINNER_UPDATE);
      updated = true;
    }
    return updated;
  }

  /**
   * Updates the baggage conditions information using a shopping cart of version {@link
   * com.odigeo.app.android.lib.consts.Constants
   * .DTOVersion#DEFAULT}, in default version the baggage number is stored on each traveller and it
   * must be set as a baggage selection, so
   * a new baggage selection option is added with the number of baggages included in the last
   * version
   *
   * @param shoppingCart Shopping cart to update
   */
  private static void updateBaggageFromDefault(@NonNull OldMyShoppingCart shoppingCart) {
    for (int i = 0; i < shoppingCart.getTravellers().size(); i++) {
      TravellerDTO oldTraveler = shoppingCart.getTravellers().get(i);
      List<BaggageSelectionDTO> baggageSelectionDTOs = new LinkedList<>();
      //Get the old number of baggages from the traveller object
      int oldBags = oldTraveler.getNumBags();
      // If the traveller has at least one bag, create a new baggage selection and add it
      // to the list of the list of baggage selections
      BaggageSelectionDTO baggageSelectionDTO = new BaggageSelectionDTO();
      if (oldBags > 0) {
        // Save the baggage included in price pieces
        BaggageDescriptorDTO included = new BaggageDescriptorDTO();
        included.setPieces(oldBags);
        baggageSelectionDTO.setBaggageDescriptor(included);
        baggageSelectionDTOs.add(baggageSelectionDTO);
      }
      shoppingCart.getTravellers().get(i).setBaggageSelections(baggageSelectionDTOs);
    }
  }

  /**
   * Updates the baggage conditions information using a shopping cart of version {@link
   * com.odigeo.app.android.lib.consts.Constants
   * .DTOVersion#MSL_V2}, in this case we must consider two cases: <ul> <ol>The baggage selection
   * was migrated from {@link
   * com.odigeo.app.android.lib.consts.Constants.DTOVersion#DEFAULT} so the segment index will be
   * null, in this case, we must repeat the
   * baggage selections for each segment</ol> <ol>The baggage selection was added using {@link
   * com.odigeo.app.android.lib.consts.Constants.DTOVersion#MSL_V2}, in this case there is nothing
   * to do</ol> </ul>
   *
   * @param shoppingCart Shopping cart to update
   */
  private static void updateBaggageToBaggageDetails(@NonNull OldMyShoppingCart shoppingCart,
      SearchOptions searchOptions) {

    for (int i = 0; i < shoppingCart.getTravellers().size(); i++) {
      TravellerDTO oldTraveler = shoppingCart.getTravellers().get(i);
      List<BaggageSelectionDTO> baggageSelectionDTOs = new LinkedList<>();
      List<BaggageSelectionDTO> v2BaggageSelections = oldTraveler.getBaggageSelections();

      if (needsToUpdateBaggageDetails(oldTraveler)) {
        int oldBags = v2BaggageSelections.get(0).getBaggageDescriptor().getPieces();
        for (int j = 0; j < searchOptions.getFlightSegments().size(); j++) {

          // Add baggage selections per segment
          BaggageSelectionDTO baggageSelectionDTO = new BaggageSelectionDTO();
          baggageSelectionDTO.setSegmentTypeIndex(SegmentTypeIndexDTO.fromIndex(j));
          //Save the baggage included in price pieces
          BaggageDescriptorDTO included = new BaggageDescriptorDTO();
          included.setPieces(oldBags);
          baggageSelectionDTO.setBaggageDescriptor(included);
          baggageSelectionDTOs.add(baggageSelectionDTO);
        }
        shoppingCart.getTravellers().get(i).setBaggageSelections(baggageSelectionDTOs);
      }
    }
  }

  /**
   * Check if the baggage selection must be updated, a baggage selection list must be updated if it
   * contains a single element which was
   * migrated from the first version of the app, which only includes the total baggage number, to
   * check this the list must have only one
   * element and the segment index of this element should be null and if the traveller does not
   * include baggage
   *
   * @param traveller List to check
   * @return TRUE if the list was created from the first app version, FALSE otherwise
   */
  private static boolean needsToUpdateBaggageDetails(TravellerDTO traveller) {
    List<BaggageSelectionDTO> v2BaggageSelections = traveller.getBaggageSelections();
    //If the baggage list has exactly one element, and this element has a null segment index
    return traveller.getIncludedBaggage().isEmpty()
        && v2BaggageSelections.size() == 1
        && v2BaggageSelections.get(0).getSegmentTypeIndex() == null
        && v2BaggageSelections.get(0).getBaggageDescriptor().getPieces() > 0;
  }

  public static void saveSettings(Context context, AppSettings settings) {
    String settingsString = new Gson().toJson(settings);
    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_SETTINGS,
            Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putString(Constants.PREFERENCE_SETTINGS, settingsString);
    editor.apply();
  }

  public static AppSettings readSettings(Context context) {

    AppSettings defaultSettings = null;

    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_SETTINGS,
            Context.MODE_PRIVATE);
    if (preferences != null && preferences.contains(Constants.PREFERENCE_SETTINGS)) {
      String settingsString = preferences.getString(Constants.PREFERENCE_SETTINGS, null);
      if (settingsString != null) {
        defaultSettings = new Gson().fromJson(settingsString, AppSettings.class);
      }
    }

    if (defaultSettings == null) {
      defaultSettings = new AppSettings();
      GeneralConstants generalConstants = Configuration.getInstance().getGeneralConstants();
      defaultSettings.setDefaultAdults(generalConstants.getDefaultNumberOfAdults());
      defaultSettings.setDefaultKids(generalConstants.getDefaultNumberOfKids());
      defaultSettings.setDefaultBabies(generalConstants.getDefaultNumberOfInfants());
      defaultSettings.setDistanceUnit(DistanceUnits.KILOMETERS);
    }

    return defaultSettings;
  }

  public static void setLastAppVersionCode(Context context, int versionCode) {
    SharedPreferences.Editor editor =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE)
            .edit();

    editor.putInt(Constants.PREFERENCE_APP_VERSION_CODE, versionCode);
    editor.apply();
  }

  public static int readLastAppVersionCode(Context context) {
    SharedPreferences sharedPreferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE);

    return sharedPreferences.getInt(Constants.PREFERENCE_APP_VERSION_CODE,
        Constants.NOT_PREVIOUS_VERSION_FOUND);
  }

  public static String readUpdateVersionToIgnore(Context context) {
    SharedPreferences sharedPreferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE);

    return sharedPreferences.getString(Constants.PREFERENCE_APP_VERSION_UPDATE_TO_IGNORE,
        STRING_EMPTY);
  }

  public static void setUpdateVersionToIgnore(String versionName, Context context) {
    SharedPreferences.Editor editor =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE)
            .edit();

    editor.putString(Constants.PREFERENCE_APP_VERSION_UPDATE_TO_IGNORE, versionName);
    editor.apply();
  }

  //Set value to status notifications
  public static void setStatusNotifications(Context context, boolean status) {
    SharedPreferences.Editor editor =
        context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_SETTINGS,
            Context.MODE_PRIVATE).edit();
    editor.putBoolean(Constants.PREFERENCE_STATUS_NOTIFICATIONS, status);
    editor.apply();
  }

  public static void setFirstTimeInHome(Context context, boolean isFirstTime) {
    SharedPreferences.Editor editor =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE)
            .edit();
    editor.putBoolean(Constants.PREFERENCE_IS_FIRST_IN_HOME, isFirstTime);
    editor.apply();
  }

  //Get value status notifications
  public static boolean readStatusNotifications(Context context) {
    SharedPreferences sharedPreferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_SETTINGS,
            Context.MODE_PRIVATE);
    return sharedPreferences.getBoolean(Constants.PREFERENCE_STATUS_NOTIFICATIONS, false);
  }

  /**
   * Save time of the last booking
   *
   * @param context Context to save the shared preferences file
   * @param timeMillis time on milliseconds
   */
  public static void saveTimeMostRecentBooking(Context context, long timeMillis) {

    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putLong(Constants.SHARED_PREFERENCES_MOST_RECENT_BOOKING, timeMillis);
    editor.apply();
  }

  public static void saveTimes(Context context, String key, long dateMillis) {
    SharedPreferences.Editor editor =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE)
            .edit();

    editor.putLong(key, dateMillis);
    editor.apply();
  }

  public static long readSaveTimes(Context context, String key) {
    SharedPreferences sharedPreferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE);

    return sharedPreferences.getLong(key, 0);
  }

  public static boolean wasAppInstalled(Context context) {
    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE);
    return preferences.getBoolean(Constants.WAS_APP_INSTALLED, false);
  }

  public static void setWasAppInstalled(Context context, boolean value) {
    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putBoolean(Constants.WAS_APP_INSTALLED, value);
    editor.apply();
  }

  public static boolean hasDatesMigrated(Context context) {
    SharedPreferences sharedPreferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE);
    return sharedPreferences.getBoolean(Constants.PREFERENCE_DATES_HAS_BEEN_MIGRATED, false);
  }

  public static void setHasDatesMigrated(Context context, boolean value) {
    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putBoolean(Constants.PREFERENCE_DATES_HAS_BEEN_MIGRATED, value);
    editor.apply();
  }

  public static void setCampaignCardKey(Context context, String key) {
    SharedPreferences.Editor editor =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE)
            .edit();
    editor.putString(CampaignCard.LOCALYTICS_CAMPAIGN_CARD_KEY, key);
    editor.apply();
  }

  @Deprecated public static String getCampaignCardKey(Context context) {
    if (context != null) {
      SharedPreferences preferences =
          context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE);
      return preferences.getString(CampaignCard.LOCALYTICS_CAMPAIGN_CARD_KEY, "");
    } else {
      return STRING_EMPTY;
    }
  }

  @Deprecated public static void clearCampaignCardKey(Context context) {
    SharedPreferences.Editor editor =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE)
            .edit();
    editor.remove(CampaignCard.LOCALYTICS_CAMPAIGN_CARD_KEY);
    editor.apply();
  }

  @Deprecated public static boolean hasCampaignCardKey(Context context) {
    if (context != null) {
      SharedPreferences preferences =
          context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE);
      return preferences.contains(CampaignCard.LOCALYTICS_CAMPAIGN_CARD_KEY);
    } else {
      return false;
    }
  }
}
