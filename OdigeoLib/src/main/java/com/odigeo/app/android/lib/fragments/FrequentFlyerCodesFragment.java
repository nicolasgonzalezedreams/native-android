package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.FrequentFlyerCardCodeDTO;
import com.odigeo.app.android.lib.ui.widgets.FrequentFlyerWidget;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Irving on 17/11/2014.
 */
@Deprecated public class FrequentFlyerCodesFragment extends Fragment {

  private List<FrequentFlyerWidget> frequentFlyerWidgets;

  private List<FrequentFlyerCardCodeDTO> flyerCardCodeDTOs;

  private int indexPassenger;

  public static FrequentFlyerCodesFragment newInstance(int indexPassenger,
      List<List<CarrierDTO>> groupsCarriers, List<FrequentFlyerCardCodeDTO> flyerCodes) {
    FrequentFlyerCodesFragment fragment = new FrequentFlyerCodesFragment();
    Bundle bundle = new Bundle();
    bundle.putSerializable(Constants.EXTRA_FREQUENT_FLYER_CARRIERS, (ArrayList) groupsCarriers);
    bundle.putSerializable(Constants.EXTRA_FREQUENT_FLYER_LIST, (ArrayList) flyerCodes);
    bundle.putInt(Constants.EXTRA_PASSENGER_INDEX, indexPassenger);
    fragment.setArguments(bundle);
    return fragment;
  }

  @Override public final void onAttach(Activity activity) {
    super.onAttach(activity);
    setHasOptionsMenu(true);
  }

  @Override public final View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    frequentFlyerWidgets = new ArrayList<>();

    View view = inflater.inflate(R.layout.layout_frequent_flyer_codes_fragment, container, false);
    LinearLayout containerLayout =
        (LinearLayout) view.findViewById(R.id.layout_frequent_flyer_container);

    flyerCardCodeDTOs = (ArrayList<FrequentFlyerCardCodeDTO>) getArguments().getSerializable(
        Constants.EXTRA_FREQUENT_FLYER_LIST);
    indexPassenger = getArguments().getInt(Constants.EXTRA_PASSENGER_INDEX);
    List<List<CarrierDTO>> groupsCarrier = (List<List<CarrierDTO>>) getArguments().getSerializable(
        Constants.EXTRA_FREQUENT_FLYER_CARRIERS);

    if (groupsCarrier != null) {
      for (List<CarrierDTO> carrierList : groupsCarrier) {
        addFlyerWidgets(containerLayout, carrierList);
      }
    }

    return view;
  }

  private void addFlyerWidgets(LinearLayout containerLayout, List<CarrierDTO> carrierList) {
    FrequentFlyerWidget flyerWidget = new FrequentFlyerWidget(getActivity());
    flyerWidget.setCarrierList(carrierList);
    if (flyerCardCodeDTOs != null) {
      for (FrequentFlyerCardCodeDTO flyerCode : flyerCardCodeDTOs) {
        CarrierDTO carrierDTO = new CarrierDTO();
        carrierDTO.setCode(flyerCode.getCarrierCode());
        if (carrierList.indexOf(carrierDTO) >= 0) {
          flyerWidget.setFlyerCardCodeDTO(flyerCode);
        }
      }
    }
    flyerWidget.hideDelete();
    frequentFlyerWidgets.add(flyerWidget);
    containerLayout.addView(flyerWidget);
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {
    int itemId = item.getItemId();
    if (itemId == android.R.id.home) {

      getActivity().onBackPressed();
    }
    return super.onOptionsItemSelected(item);
  }

  public final int getIndexPassenger() {
    return indexPassenger;
  }

  public final List<FrequentFlyerCardCodeDTO> getCodes() {

    for (FrequentFlyerWidget widget : frequentFlyerWidgets) {

      if (widget.isValid()) {
        if (flyerCardCodeDTOs == null) {
          flyerCardCodeDTOs = new ArrayList<>();
        }
        if (flyerCardCodeDTOs.indexOf(widget.getFlyerCardCodeDTO()) < 0) {
          flyerCardCodeDTOs.add(widget.getFlyerCardCodeDTO());
        }
      }
    }
    return flyerCardCodeDTOs;
  }
}
