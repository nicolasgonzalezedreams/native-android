package com.odigeo.app.android.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.data.db.dao.UserDBDAOInterface;
import com.odigeo.data.db.helper.MembershipHandlerInterface;
import com.odigeo.data.db.helper.TravellersHandlerInterface;
import com.odigeo.data.db.helper.UserCreateOrUpdateHandlerInterface;
import com.odigeo.data.entity.userData.Membership;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.data.net.NetToolInterface;
import com.odigeo.data.net.controllers.UserNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.CarouselManagerInterface;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.UpdateBookingStatusInteractor;
import com.odigeo.interactors.UpdateBookingsFromLocalInteractor;
import java.util.List;

public class SyncService extends Service {

  private final IBinder mIBinder = new SyncServiceBinder();

  private UserNetControllerInterface mUserNetController;
  private SessionController mSessionController;
  private UserDBDAOInterface mUserDBDAO;
  private TravellersHandlerInterface mUserTravellers;
  private UserCreateOrUpdateHandlerInterface mUserCreateOrUpdateHandler;
  private UpdateBookingsFromLocalInteractor updateBookingsInteractor;
  private UpdateBookingStatusInteractor updateBookingStatusInteractor;
  private NetToolInterface netTool;
  private CarouselManagerInterface mCarouselManager;
  private MembershipHandlerInterface membershipHandlerInterface;
  private CrashlyticsController crashlyticsController;

  public SyncService() {
  }

  @Override public void onCreate() {
    super.onCreate();
    AndroidDependencyInjector androidDependencyInjector = AndroidDependencyInjector.getInstance();
    mUserNetController = androidDependencyInjector.provideUserNetController();
    mSessionController = androidDependencyInjector.provideSessionController();
    mUserDBDAO = androidDependencyInjector.provideUserDBDAO();
    mUserTravellers = androidDependencyInjector.provideTravellersHandler();
    mUserCreateOrUpdateHandler = androidDependencyInjector.provideUserCreateOrUpdateDBHandler();
    updateBookingsInteractor = androidDependencyInjector.provideUpdateBookingFromLocalInteractor();
    updateBookingStatusInteractor =
        androidDependencyInjector.provideUpdateBookingStatusInteractor();
    netTool = androidDependencyInjector.provideNetTool();
    mCarouselManager = androidDependencyInjector.provideCarouselManager();
    membershipHandlerInterface = androidDependencyInjector.provideMembershipHandler();
    crashlyticsController = androidDependencyInjector.provideCrashlyticsController();
  }

  @Override public IBinder onBind(Intent intent) {
    return mIBinder;
  }

  public void synchronizeData(OnAuthRequestDataListener<Void> listener) {
    Log.i(Constants.TAG_SYNC, "Sync Service Started");
    if (netTool.isThereInternetConnection()) {
      if (mSessionController.getCredentials() != null) {
        getDataUser(listener);
        Log.i(Constants.TAG_SYNC, "Sync Service Successful");
      } else {
        updateBookingStatus();
      }
    } else {
      stopSelf();
      Log.i(Constants.TAG_SYNC, "Sync Service Failed");
    }
    Log.i(Constants.TAG_SYNC, "Sync Service Stopped");
  }

  private void getDataUser(final OnAuthRequestDataListener<Void> listener) {
    mUserNetController.getUser(new OnAuthRequestDataListener<User>() {

      @Override public void onAuthError() {
        listener.onAuthError();
      }

      @Override public void onResponse(User user) {
        mSessionController.attachCreditCardsToSession(user.getCreditCards());
        checkUpdate(user);
      }

      @Override public void onError(MslError error, String message) {
        stopSelf();
      }
    });
  }

  private void checkUpdate(User user) {
    mSessionController.saveUserId(user.getUserId());
    if (mUserDBDAO.getCurrentUser() != null) {
      long databaseLastUpdate = mUserDBDAO.getCurrentUser().getLastModified();
      if (databaseLastUpdate > user.getLastModified()) {
        updateServerData(user);
      } else if (databaseLastUpdate == user.getLastModified()) {
        updateMyTrips(user.getBookingsId());
      } else {
        updateDatabaseData(user);
        updateMyTrips(user.getBookingsId());
      }
    }
  }

  private void updateDatabaseData(User user) {
    Log.d(getClass().getName(), "UpdateDatabaseData");
    mUserCreateOrUpdateHandler.addOrCreateUserAndAllComponents(user);
  }

  private void updateServerData(final User user) {
    Log.d(getClass().getName(), "UpdateServerData");
    user.setUserTravellerList(mUserTravellers.getFullUserTravellerList());
    mUserNetController.updateUser(new OnRequestDataListener<User>() {
      @Override public void onResponse(User newUser) {
        List<UserTraveller> userTravellersFromServer = newUser.getUserTravellerList();
        addNewUserTravellers(userTravellersFromServer);
        deleteOldUserTravellers(userTravellersFromServer);
        updateMyTrips(newUser.getBookingsId());

        try {
          updateMembershipInfo(newUser.getMemberships());
          deleteOldMembershipInfo(newUser.getMemberships());
        } catch(Exception e) {
          crashlyticsController.trackNonFatal(e);
          membershipHandlerInterface.createTableIfNotExists();
          updateMembershipInfo(newUser.getMemberships());
          deleteOldMembershipInfo(newUser.getMemberships());
        }
      }

      @Override public void onError(MslError error, String message) {
        stopSelf();
      }
    }, user);
  }

  private void addNewUserTravellers(List<UserTraveller> userTravellersFromServer) {
    List<UserTraveller> userTravellerFromDB = mUserTravellers.getFullUserTravellerList();
    for (UserTraveller userTraveller : userTravellersFromServer) {
      if (!userTravellerFromDB.contains(userTraveller)) {
        mUserCreateOrUpdateHandler.updateOrCreateAUserTravellerAndAllComponentsInDB(userTraveller);
      }
    }
  }

  private void deleteOldUserTravellers(List<UserTraveller> userTravellersFromServer) {
    List<UserTraveller> userTravellerFromDB = mUserTravellers.getFullUserTravellerList();
    for (UserTraveller userTraveller : userTravellerFromDB) {
      if (!userTravellersFromServer.contains(userTraveller)) {
        mUserCreateOrUpdateHandler.deleteUserTravellerCompletely(userTraveller);
      }
    }
  }

  private void deleteOldMembershipInfo(List<Membership> membershipsFromServer) {
    List<Membership> membershipsFromDB = membershipHandlerInterface.getAllMembershipsOfUser();
    for (Membership membership : membershipsFromDB) {
      if (!membershipsFromServer.contains(membership)) {
        membershipHandlerInterface.deleteMembership(membership);
      }
    }
  }

  private void updateMembershipInfo(List<Membership> membershipsFromServer) {
    List<Membership> membershipsFromDB = membershipHandlerInterface.getAllMembershipsOfUser();
    for (Membership membership : membershipsFromServer) {
      if (!membershipsFromDB.contains(membership)) {
        membershipHandlerInterface.addMembership(membership);
      }
    }
  }

  private void updateMyTrips(List<Long> bookingsId) {
    updateBookingsInteractor.updateMyTrips(bookingsId, new OnRequestDataListener<Void>() {
      @Override public void onResponse(Void object) {
        stopSelf();
      }

      @Override public void onError(MslError error, String message) {
        stopSelf();
      }
    });
  }

  //##########################
  //######## MY TRIPS ########
  //##########################

  private void updateBookingStatus() {
    updateBookingStatusInteractor.updateBookingStatus(false);
  }

  public class SyncServiceBinder extends Binder {
    public SyncService getService() {
      return SyncService.this;
    }
  }
}
