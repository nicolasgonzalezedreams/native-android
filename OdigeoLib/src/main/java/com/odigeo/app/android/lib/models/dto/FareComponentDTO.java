package com.odigeo.app.android.lib.models.dto;

import java.util.List;

public class FareComponentDTO {

  protected List<String> fareRules;
  protected String passengerType;
  protected String origin;
  protected String destination;

  public List<String> getFareRules() {
    return fareRules;
  }

  public void setFareRules(List<String> fareRules) {
    this.fareRules = fareRules;
  }

  /**
   * Gets the value of the passengerType property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getPassengerType() {
    return passengerType;
  }

  /**
   * Sets the value of the passengerType property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setPassengerType(String value) {
    this.passengerType = value;
  }

  /**
   * Gets the value of the origin property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getOrigin() {
    return origin;
  }

  /**
   * Sets the value of the origin property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setOrigin(String value) {
    this.origin = value;
  }

  /**
   * Gets the value of the destination property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Sets the value of the destination property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setDestination(String value) {
    this.destination = value;
  }
}
