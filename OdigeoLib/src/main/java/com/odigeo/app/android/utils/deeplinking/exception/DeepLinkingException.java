package com.odigeo.app.android.utils.deeplinking.exception;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 8/06/16
 */

public class DeepLinkingException extends Exception {

  public DeepLinkingException(String detailMessage, Throwable throwable) {
    super(detailMessage, throwable);
  }
}