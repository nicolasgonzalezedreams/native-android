package com.odigeo.app.android.helper;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.DeviceUtils;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.view.constants.OneCMSKeys;

public class EmailSender {

  public static void send(String to, String onCmsSubjectKey, Context context) {
    final String SUBJECT_PATERN = "%s - %s";
    final String FOOTER_PATERN =
        "<br/><br/><br/><font size=\"6\" color=\"lightgray\">%s%s v%s - %s<br/>%s</font>";

    String subject = String.format(SUBJECT_PATERN, Configuration.getInstance().getBrandVisualName(),
        LocalizablesFacade.getString(context, onCmsSubjectKey).toString());

    String footerText =
        String.format(FOOTER_PATERN, context.getResources().getString(R.string.copyright),
            Configuration.getInstance().getBrandVisualName(),
            DeviceUtils.getApplicationVersionName(context),
            Configuration.getInstance().getCurrentMarket().getKey(), Build.MODEL);

    Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", to, null));
    sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
    sendIntent.putExtra(Intent.EXTRA_TEXT, HtmlUtils.formatHtml(footerText));
    context.startActivity(Intent.createChooser(sendIntent,
        LocalizablesFacade.getRawString(context, OneCMSKeys.ABOUTOPTIONSMODULE_SEND_MAIL)));
  }
}
