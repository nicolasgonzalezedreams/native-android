package com.odigeo.app.android.lib.models.transform;

import com.odigeo.app.android.lib.models.dto.InsuranceDTO;
import com.odigeo.app.android.lib.models.dto.InsuranceOfferDTO;
import com.odigeo.app.android.lib.models.dto.InsuranceShoppingItemDTO;
import java.util.ArrayList;
import java.util.List;

public final class InsuranceOfferTransform {

  private InsuranceOfferTransform() {

  }

  public static InsuranceOfferDTO transformInsuranceShoppingItemDTO2InsuranceOffer(
      InsuranceShoppingItemDTO insuranceDTO) {
    InsuranceOfferDTO insuranceOffer = new InsuranceOfferDTO();

    if (insuranceDTO != null) {
      List<InsuranceDTO> newListInsurances = new ArrayList<InsuranceDTO>();

      insuranceOffer.setTotalPrice(insuranceDTO.getTotalPrice());
      insuranceOffer.setId(insuranceDTO.getId());
      insuranceOffer.setProviderPrice(insuranceDTO.getTotalPrice());
      insuranceOffer.setInsurances(newListInsurances);

      if (insuranceDTO.getInsurance() != null) {
        newListInsurances.add(insuranceDTO.getInsurance());
        insuranceDTO.getInsurance().parseDescriptionsItems();
      }
    }

    return insuranceOffer;
  }
}
