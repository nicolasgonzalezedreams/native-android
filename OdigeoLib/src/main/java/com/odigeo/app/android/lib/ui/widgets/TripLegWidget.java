package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.interfaces.ITripLegWidgetListener;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.data.entity.geo.City;
import java.util.Date;

/**
 * Created by emiliano.desantis on 23/09/2014.
 */
public class TripLegWidget extends LinearLayout implements View.OnClickListener {
  private ButtonSearchDestination btnDestinationFrom;
  private ButtonSearchDestination btnDestinationTo;
  private ButtonSearchDate btnDepartureDate;
  private int noSegment;
  private FlightSegment flightSegment;
  private ITripLegWidgetListener listener;
  private TextView txtLegNumber;
  private ImageView btnRemoveLegWidget;

  public TripLegWidget(Context context, AttributeSet attrs) {
    super(context, attrs);

    initialize();
  }

  public TripLegWidget(Context context, int noSegment, FlightSegment flightSegment) {
    super(context);

    this.noSegment = noSegment;
    this.setFlightSegment(flightSegment);

    initialize();
  }

  public void setListener(ITripLegWidgetListener listener) {
    this.listener = listener;
  }

  private void initialize() {
    inflate(getContext(), R.layout.widget_trip_leg, this);

    btnDestinationFrom = (ButtonSearchDestination) findViewById(R.id.panelDeparture);
    btnDestinationTo = (ButtonSearchDestination) findViewById(R.id.panelDestination);
    btnDepartureDate = (ButtonSearchDate) findViewById(R.id.panelDepartureDate);
    txtLegNumber = (TextView) findViewById(R.id.txtLegNumber);
    btnRemoveLegWidget = (ImageView) findViewById(R.id.btnRemoveLegWidget);

    txtLegNumber.setText(
        CommonLocalizables.getInstance().getCommonLeg(getContext()) + " " + (noSegment + 1));

    btnDestinationFrom.setOnClickListener(this);
    btnDestinationTo.setOnClickListener(this);
    btnDepartureDate.setOnClickListener(this);
    btnRemoveLegWidget.setOnClickListener(this);
  }

  public void setNoSegment(int noSegment) {
    this.noSegment = noSegment;

    txtLegNumber.setText(
        CommonLocalizables.getInstance().getCommonLeg(getContext()) + " " + (noSegment + 1));
  }

  @Override public void onClick(View v) {
    if (listener != null) {
      if (v.getId() == btnDestinationFrom.getId()) {
        listener.onClickDestinationFrom(noSegment);
      } else if (v.getId() == btnDestinationTo.getId()) {
        listener.onClickDestinationTo(noSegment);
      } else if (v.getId() == btnDepartureDate.getId()) {
        listener.onClickDepartureDate(noSegment);
      } else if (v.getId() == btnRemoveLegWidget.getId()) {
        listener.onClickRemoveWidget(noSegment);
      }
    }
  }

  public void showRemoveButton() {
    btnRemoveLegWidget.setVisibility(VISIBLE);
  }

  public void hideRemoveButton() {
    btnRemoveLegWidget.setVisibility(GONE);
  }

  public FlightSegment getFlightSegment() {
    return flightSegment;
  }

  public final void setFlightSegment(FlightSegment flightSegment) {
    this.flightSegment = flightSegment;
  }

  public void setCityFrom(City city) {
    btnDestinationFrom.setCity(city);
  }

  public void setCityTo(City city) {
    btnDestinationTo.setCity(city);
  }

  public void setDepartureDate(Date dateGo) {
    btnDepartureDate.setDate(dateGo);
  }

  public void updateData() {
    setCityFrom(flightSegment.getDepartureCity());
    setCityTo(flightSegment.getArrivalCity());
    setDepartureDate(OdigeoDateUtils.createDate(flightSegment.getDate()));
  }
}
