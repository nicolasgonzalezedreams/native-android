package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;

/**
 * Created by emiliano.desantis on 09/09/2014.
 * <p/>
 * A HUGE thank you to Ignacio Belderrain and Pablo Catullo for their help.
 */
public class OdometerWidget extends LinearLayout {

  private static final int NUMBERS = 6;
  private static final int POS_I = 0;
  private static final int POS_X = 1;
  private static final int POS_C = 2;
  private static final int POS_1_K = 3;
  private static final int POS_10_K = 4;
  private static final int POS_100_K = 5;
  private static final int FINAL_NUMBER_COLUMN = 5;
  private static final int TARGET_NUMBER = 84;
  private static final int TEN = 10;
  private final TextView[] numbersAbove = new TextView[NUMBERS];
  private final TextView[] numbersBelow = new TextView[NUMBERS];
  private final TextView header;
  private final TextView footer;
  boolean[] finishedDigits = new boolean[NUMBERS];
  private int digitsShowing = 0;
  private long targetNumber;
  private long targetInterval;

  public OdometerWidget(Context context, AttributeSet attrs) {

    super(context, attrs);

    inflate(getContext(), R.layout.widget_casino_slots, this);

    numbersAbove[POS_100_K] = (TextView) findViewById(R.id.txt100kAbove);
    numbersAbove[POS_10_K] = (TextView) findViewById(R.id.txt10kAbove);
    numbersAbove[POS_1_K] = (TextView) findViewById(R.id.txt1kAbove);
    numbersAbove[POS_C] = (TextView) findViewById(R.id.txtCAbove);
    numbersAbove[POS_X] = (TextView) findViewById(R.id.txtXAbove);
    numbersAbove[POS_I] = (TextView) findViewById(R.id.txtIAbove);

    numbersBelow[POS_100_K] = (TextView) findViewById(R.id.txt100kBelow);
    numbersBelow[POS_10_K] = (TextView) findViewById(R.id.txt10kBelow);
    numbersBelow[POS_1_K] = (TextView) findViewById(R.id.txt1kBelow);
    numbersBelow[POS_C] = (TextView) findViewById(R.id.txtCBelow);
    numbersBelow[POS_X] = (TextView) findViewById(R.id.txtXBelow);
    numbersBelow[POS_I] = (TextView) findViewById(R.id.txtIBelow);

    header = (TextView) findViewById(R.id.txtBasicPassengerInfoHeader);
    footer = (TextView) findViewById(R.id.txtFooter);

    Fonts fonts = Configuration.getInstance().getFonts();

    for (int i = 0; i < NUMBERS; i++) {
      numbersAbove[i].setTypeface(fonts.getRegular());
      numbersBelow[i].setTypeface(fonts.getRegular());
    }

    setTargetNumber(TARGET_NUMBER);
    setInitialNumber(0);
  }

  public final void setInitialNumber(int initialNumber) {
    //Reset all NUMBERS
    for (int i = 0; i < NUMBERS; i++) {
      //currentDigits[x] = 0;
      numbersAbove[i].setText("");
      numbersBelow[i].setText("1");

      numbersAbove[i].setVisibility(GONE);
      numbersBelow[i].setVisibility(GONE);
    }

    //Get the number in reverse, in string
    String strNumber = new StringBuilder(String.valueOf(initialNumber)).reverse().toString();

    //Set new number
    for (int i = 0; i < strNumber.length(); i++) {
      int currentDigit = Integer.parseInt(String.valueOf(strNumber.charAt(i)));
      int nextDigit = (currentDigit + 1) % TEN; //Units only

      String strCurrentDigit = String.valueOf(strNumber.charAt(i));
      String strNextDigit = String.valueOf(nextDigit);

      numbersAbove[i].setText(strCurrentDigit);
      numbersBelow[i].setText(strNextDigit);

      numbersAbove[i].setVisibility(VISIBLE);
    }

    numbersBelow[0].setVisibility(VISIBLE);
  }

  public final void setTargetNumber(int n) {
    targetNumber = n;
    this.digitsShowing = String.valueOf(n).length();
  }

  public final void setTargetInterval(int lapTime) {
    targetInterval = lapTime;
  }

  public final void startAnimation() {
    startAnimation(0);
  }

  private void startAnimation(final int i) {
    if (digitsShowing == 0) {
      digitsShowing = 1;
    }

    Animation sliderAbove = AnimationUtils.loadAnimation(getContext(), R.anim.slider);
    Animation sliderBelow = AnimationUtils.loadAnimation(getContext(), R.anim.slider);

    sliderAbove.setDuration(targetInterval);
    sliderBelow.setDuration(targetInterval);

    sliderAbove.setAnimationListener(new AnimationListener() {

      @Override public void onAnimationStart(Animation animation) {
        firstAnimationStart(i);
      }

      @Override public void onAnimationRepeat(Animation animation) {
        // Empty.
      }

      @Override public void onAnimationEnd(Animation animation) {
        firstAnimationEnd(i);
      }
    });

    numbersAbove[i].startAnimation(sliderAbove);

    numbersBelow[i].startAnimation(sliderBelow);
  }

  /**
   * Wraps the logic of onAnimationStart method. Changes the text in numbersBelow array.
   *
   * @param i position of number to change.
   */
  private void firstAnimationStart(int i) {
    int cur = 0;
    try {
      cur = Integer.parseInt(numbersAbove[i].getText().toString());
    } catch (Exception e) {
      Log.e(this.getClass().toString(), e.getMessage());
    }

    if (cur == FINAL_NUMBER_COLUMN) {
      finishedDigits[i] = true;
      numbersBelow[i].setText("0");
    } else {
      numbersBelow[i].setText(String.valueOf(cur + 1));
    }
  }

  /**
   * Wraps the logic of onAnimationEnd method. Animates the number in the position of numbersBelow
   * array if the final number has reached
   *
   * @param i position of number to change.
   */
  private void firstAnimationEnd(int i) {
    numbersAbove[i].setText(numbersBelow[i].getText());

    if (Integer.parseInt(numbersAbove[i].getText().toString()) == FINAL_NUMBER_COLUMN) {
      finishedDigits[i] = false;
      animateOnce(i + 1);
    }

    if (!targetNumberReached()) {
      startAnimation(i);
    }
  }

  private void animateOnce(final int i) {
    if (digitsShowing < (i + 1)) {
      digitsShowing = i + 1;
    }

    if (numbersAbove[i].getVisibility() == GONE) {
      numbersAbove[i].setVisibility(VISIBLE);
    }

    if (numbersBelow[i].getVisibility() == GONE) {
      numbersBelow[i].setVisibility(VISIBLE);
    }

    Animation sliderAbove = AnimationUtils.loadAnimation(getContext(), R.anim.slider);
    Animation sliderBelow = AnimationUtils.loadAnimation(getContext(), R.anim.slider);

    sliderAbove.setDuration(targetInterval);
    sliderBelow.setDuration(targetInterval);

    sliderAbove.setAnimationListener(getAnimationListener(i));

    numbersAbove[i].startAnimation(sliderAbove);
    numbersBelow[i].startAnimation(sliderBelow);
  }

  private AnimationListener getAnimationListener(final int i) {
    return new AnimationListener() {
      @Override public void onAnimationStart(Animation animation) {
        firstAnimationStart(i);
      }

      @Override public void onAnimationRepeat(Animation animation) {
        // Empty.
      }

      @Override public void onAnimationEnd(Animation animation) {
        numbersAbove[i].setText(numbersBelow[i].getText());
        if (finishedDigits[i]) {
          finishedDigits[i] = false;
          animateOnce(i + 1);
        }
        numbersBelow[i].setVisibility(GONE);
      }
    };
  }

  /**
   * If the number showing is the target number
   */
  private boolean targetNumberReached() {
    StringBuffer currentNumber = new StringBuffer();

    for (int i = 0; i < digitsShowing; i++) {
      currentNumber.append(String.valueOf(numbersAbove[i].getText()));
    }

    String reverseCurrentNumber =
        new StringBuilder(String.valueOf(currentNumber)).reverse().toString();

    return Integer.parseInt(reverseCurrentNumber) >= targetNumber;
  }

  public final void setTextHeader(String text) {
    header.setText(text);
  }

  public final void setTextFooter(String text) {
    footer.setText(text);
  }
}
