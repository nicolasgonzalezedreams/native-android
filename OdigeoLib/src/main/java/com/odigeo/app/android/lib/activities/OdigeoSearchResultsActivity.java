package com.odigeo.app.android.lib.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import com.globant.roboneck.common.NeckCookie;
import com.google.gson.Gson;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.fragments.MSLErrorUtilsDialogFragment;
import com.odigeo.app.android.lib.fragments.OdigeoNoResultsFiltersFragment;
import com.odigeo.app.android.lib.fragments.OdigeoNoResultsMultipleFragment;
import com.odigeo.app.android.lib.fragments.OdigeoResultsFragment;
import com.odigeo.app.android.lib.fragments.OdigeoSearchWaitingResultsFragment;
import com.odigeo.app.android.lib.fragments.base.OdigeoNoResultsFragment;
import com.odigeo.app.android.lib.fragments.base.TripFragmentBase;
import com.odigeo.app.android.lib.interfaces.OdigeoInterfaces;
import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import com.odigeo.app.android.lib.models.FiltersSelected;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.OldMyShoppingCart;
import com.odigeo.app.android.lib.models.Section;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;
import com.odigeo.app.android.lib.models.dto.InsuranceDTO;
import com.odigeo.app.android.lib.models.dto.InsuranceOfferDTO;
import com.odigeo.app.android.lib.models.dto.ItinerarySegmentRequestDTO;
import com.odigeo.app.android.lib.models.dto.ItinerarySortCriteriaDTO;
import com.odigeo.app.android.lib.models.dto.LocationRequestDTO;
import com.odigeo.app.android.lib.models.dto.MessageResponseDTO;
import com.odigeo.app.android.lib.models.dto.MoneyDTO;
import com.odigeo.app.android.lib.models.dto.SearchProductTypeDTO;
import com.odigeo.app.android.lib.models.dto.SectionDTO;
import com.odigeo.app.android.lib.models.dto.ShoppingCartDTO;
import com.odigeo.app.android.lib.models.dto.requests.FlightsRequestModel;
import com.odigeo.app.android.lib.models.dto.responses.AvailableProductsResponse;
import com.odigeo.app.android.lib.models.dto.responses.CreateShoppingCartResponse;
import com.odigeo.app.android.lib.models.dto.responses.FlightSearchResponse;
import com.odigeo.app.android.lib.models.transform.CollectionMethodTransform;
import com.odigeo.app.android.lib.models.transform.FlightSearchTransform;
import com.odigeo.app.android.lib.ui.widgets.SearchTripWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.model.BookingInfoViewModel;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.FlightsDirection;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.entity.shoppingCart.FlowConfigurationResponse;
import com.odigeo.data.entity.shoppingCart.PricingMode;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.shoppingCart.StepsConfiguration;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.MembershipInteractor;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.odigeo.data.preferences.PreferencesControllerInterface.JSESSION_COOKIE;
import static com.odigeo.data.tracker.CrashlyticsController.DAPI_JSESSION;

public abstract class OdigeoSearchResultsActivity extends OdigeoBackgroundAnimatedActivity
    implements OdigeoInterfaces.SearchListener {

  protected OdigeoApp odigeoApp;
  protected SearchOptions mSearchOptions;
  private OdigeoSession odigeoSession;
  private FlightsRequestModel requestModel;
  private Fragment currentFragment;
  private FlightSearchResponse searchResponse;
  private FiltersSelected filtersSelected;
  private CollectionMethodWithPrice cardSelected;

  private List<StepsConfiguration> stepsFlowConfiguration;
  private List<CollectionMethodWithPrice> collectionMethodWithPrices;

  private com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse
      mCreateShoppingCartResponse;
  private com.odigeo.data.entity.shoppingCart.AvailableProductsResponse mAvailableProductsResponse;
  private FlowConfigurationResponse mFlowConfigurationResponse;
  private boolean mIsFullTransparency = false;
  private BookingInfoViewModel bookingInfo;
  private MembershipInteractor membershipInteractor;
  private CrashlyticsController crashlyticsController;
  private SessionController sessionController;

  public abstract OdigeoNoResultsFragment getNoResultsSimpleFragment();

  public abstract OdigeoNoResultsFragment getNoResultsRoundFragment();

  @Override public final String getActivityTitle() {
    return LocalizablesFacade.getString(this, OneCMSKeys.RESULTSVIEWCONTROLLER_TITLE_RESULTS)
        .toString();
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Log.d(Constants.TAG_LOG, "Creando Activity OdigeoSearchResultsActivity");
    setContentView(R.layout.activity_results);

    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    sessionController = ((OdigeoApp) getApplication()).getDependencyInjector()
        .provideSessionController();
    crashlyticsController =
        ((OdigeoApp) getApplication()).getDependencyInjector().provideCrashlyticsController();
    membershipInteractor =
        ((OdigeoApp) getApplication()).getDependencyInjector().provideMembershipInteractor();

    resetPromocode();

    odigeoApp = (OdigeoApp) getApplication();
    odigeoSession = odigeoApp.getOdigeoSession();
    collectionMethodWithPrices = new ArrayList<>();

    mSearchOptions =
        ((SearchOptions) getIntent().getExtras().getSerializable(Constants.EXTRA_SEARCH_OPTIONS));
    requestModel = createFlightsSearchRequestModel();

    if (savedInstanceState == null) {
      addWaitingFragment();
    } else {
      if (savedInstanceState.getSerializable(Constants.EXTRA_SEARCH_OPTIONS) != null) {
        mSearchOptions =
            (SearchOptions) savedInstanceState.getSerializable(Constants.EXTRA_SEARCH_OPTIONS);
      }

      if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {
        searchResponse = odigeoSession.getSearchResponse();
        if (searchResponse == null) {
          searchResponse = new FlightSearchResponse();
        }
      }
    }
  }

  @Override public void onResume() {
    super.onResume();
    BusProvider.getInstance().post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_RESULTS));
    BusProvider.getInstance()
        .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_WAITING_PAGE_ONE));
  }

  public final void searchFlights() {
    requestModel = createFlightsSearchRequestModel();
    addWaitingFragment();
  }

  public final SearchOptions getSearchOptions() {
    return mSearchOptions;
  }

  public final void addFragment(Fragment fragment) {
    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

    if (currentFragment != null) {
      transaction.remove(currentFragment);
    }
    Log.d("FRAGMENT TO REPLACE", fragment.toString());
    currentFragment = fragment;
    transaction.replace(R.id.frame_fragment_results, fragment);
    transaction.commitAllowingStateLoss();
  }

  public final FlightsRequestModel getRequestModel() {
    return requestModel;
  }

  public final void setRequestModel(FlightsRequestModel requestModel) {
    this.requestModel = requestModel;
  }

  public final FlightSearchResponse getSearchResponse() {
    return searchResponse;
  }

  public final PricingMode getPricingMode() {
    return Util.getPriceTotalMode(Step.RESULTS, stepsFlowConfiguration);
  }

  public final List<ItinerarySegmentRequestDTO> getSegmentsFlightsRequest() {

    List<ItinerarySegmentRequestDTO> segmentRequests = new ArrayList<ItinerarySegmentRequestDTO>();

    for (FlightSegment flightSegment : mSearchOptions.getFlightSegments()) {

      //TODO: MSL_V2 Change the object: LocationRequestDTO
      LocationRequestDTO destinationFrom =
          new LocationRequestDTO(flightSegment.getDepartureCity().getIataCode(),
              flightSegment.getDepartureCity().getCityName());

      //TODO: MSL_V2 Change the object: LocationRequestDTO
      LocationRequestDTO arrivalDestination =
          new LocationRequestDTO(flightSegment.getArrivalCity().getIataCode(),
              flightSegment.getArrivalCity().getCityName());

      String date = null;
      SimpleDateFormat dateFormat =
          OdigeoDateUtils.getGmtDateFormat(getResources().getString(R.string.templates__date_dapi));

      if (dateFormat != null && flightSegment.getDate() != 0) {
        date = dateFormat.format(OdigeoDateUtils.createDate(flightSegment.getDate()));
      }

      segmentRequests.add(
          new ItinerarySegmentRequestDTO(date, arrivalDestination, destinationFrom));
    }

    return segmentRequests;
  }

  protected final FlightsRequestModel createFlightsSearchRequestModel() {
    FlightsRequestModel requestModel = new FlightsRequestModel();
    requestModel.getItinerarySearchRequest().setSearchMainProductType(SearchProductTypeDTO.FLIGHT);
    requestModel.getItinerarySearchRequest().setNumAdults(mSearchOptions.getNumberOfAdults());
    requestModel.getItinerarySearchRequest().setNumChildren(mSearchOptions.getNumberOfKids());
    requestModel.getItinerarySearchRequest().setNumInfants(mSearchOptions.getNumberOfBabies());

    if (mSearchOptions.getCabinClass() != CabinClassDTO.DEFAULT) {
      requestModel.getItinerarySearchRequest().setCabinClass(mSearchOptions.getCabinClass());
    } else {
      requestModel.getItinerarySearchRequest().setCabinClass(null);
    }

    requestModel.getItinerarySearchRequest().setDirectFlightsOnly(mSearchOptions.isDirectFlight());

    requestModel.getItinerarySearchRequest().setSegmentRequests(getSegmentsFlightsRequest());

    requestModel.getItinerarySearchRequest().setResident(mSearchOptions.getResident() != null);

    boolean isMember = false;
    if(sessionController.getCredentials() != null) {
      try {
        isMember = membershipInteractor.isMemberForCurrentMarket();
      } catch (Exception e) {
        crashlyticsController.trackNonFatal(e);
        isMember = false;
      }
    }

    requestModel.getItinerarySearchRequest().setIsMember(isMember);

    return requestModel;
  }

  @Override protected final void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK) {
      //If it is the Flight Return answer in the calendar Activity
      if (requestCode == Constants.SELECTING_DESTINATION) {
        selectDestination(data);
      } else if (requestCode == Constants.SELECTING_DATE) {
        selectDate(data);
      } else if (requestCode == OdigeoResultsFragment.RESULT_FROM_FILTER) {
        filterResult(data);
      }
    } else if (resultCode == AppCompatActivity.RESULT_CANCELED
        && requestCode == Constants.REQUEST_CODE_FLIGHTLISTENER) {
      // remove NoRsultsFragment
      onBackPressed();
    }
  }

  public final boolean onShoppingCartRequestSuccess(
      CreateShoppingCartResponse createShoppingCartResponse,
      List<SegmentWrapper> segmentsWrapperSelected,
      CollectionMethodWithPrice methodWithPriceSelected) {

    MoneyDTO moneyDTO = new MoneyDTO();
    moneyDTO.setCurrency(Configuration.getInstance().getCurrentMarket().getCurrencyKey());

    OldMyShoppingCart myCart = new OldMyShoppingCart();
    myCart.setTotalPrice(createShoppingCartResponse.getShoppingCart().getTotalPrice());
    myCart.setDtoVersion(Constants.DTOVersion.CURRENT);
    myCart.setPrice(moneyDTO);
    myCart.setLocale(LocaleUtils.localeToString(Configuration.getCurrentLocale()));
    ShoppingCartDTO shoppingCart = createShoppingCartResponse.getShoppingCart();

    if (shoppingCart != null) {
      myCart.setBookingId(shoppingCart.getBookingId());
      myCart.setRequiredTravellerInformations(shoppingCart.getRequiredTravellerInformation());
      myCart.setRequiredBuyerInformation(shoppingCart.getRequiredBuyerInformation());
      myCart.setShoppingCartPrice(createShoppingCartResponse.getShoppingCart().getPrice());
    }
    bookingInfo = new BookingInfoViewModel();
    bookingInfo.setCollectionMethodWithPrice(methodWithPriceSelected);
    bookingInfo.setSegmentsWrappers(segmentsWrapperSelected);
    myCart.setStepsFlowConfiguration(stepsFlowConfiguration);
    myCart.setFullPriceMethodWithPriceSelected(methodWithPriceSelected);

    //Set the time to the search options flight Segments
    for (int i = 0; i < bookingInfo.getSegmentsWrappers().size(); i++) {
      List<SectionDTO> sectionList = bookingInfo.getSegmentsWrappers().get(i).getSectionsObjects();

      //Get the first and last section
      SectionDTO departureSection = sectionList.get(0);
      SectionDTO arrivalSection = sectionList.get(sectionList.size() - 1);

      //Set the times to the segment
      FlightSegment flightSegment = mSearchOptions.getFlightSegments().get(i);
      flightSegment.setDate(departureSection.getDepartureDate());
      flightSegment.setArrivalDate(arrivalSection.getArrivalDate());

      //Add the sections information
      flightSegment.setSections(new ArrayList<Section>());
      for (SectionDTO sectionDTO : sectionList) {
        Section section = new Section();
        section.setId(sectionDTO.getId());
        section.setDepartureDate(sectionDTO.getDepartureDate());
        section.setArrivalDate(sectionDTO.getArrivalDate());
        section.setCarrier(String.valueOf(sectionDTO.getCarrier()));
        section.setOperatingCarrier(String.valueOf(sectionDTO.getOperatingCarrier()));
        section.setLocationFrom(sectionDTO.getLocationFrom());
        section.setLocationTo(sectionDTO.getLocationTo());

        flightSegment.getSections().add(section);
      }
    }

    odigeoSession.setOldMyShoppingCart(myCart);

    return true;
  }

  public final void onProductsRequestSuccess(AvailableProductsResponse availableProductsResponse) {

    if (availableProductsResponse != null) {

      if (availableProductsResponse.getError() != null) {
        List<MessageResponseDTO> messageDTO = availableProductsResponse.getMessages();
        if (messageDTO != null && !messageDTO.isEmpty()) {
          MessageResponseDTO error = messageDTO.get(0);
          MSLErrorUtilsDialogFragment fragment =
              MSLErrorUtilsDialogFragment.newInstance(messageDTO, searchResponse.getError());
          fragment.setParentScreen(Step.RESULTS.toString());
          fragment.show(getSupportFragmentManager(), error.getCode());
        }
      } else { //Create elements from the description
        if (availableProductsResponse.getInsuranceOffers() != null) {
          for (InsuranceOfferDTO insuranceOffer : availableProductsResponse.getInsuranceOffers()) {
            for (InsuranceDTO insurance : insuranceOffer.getInsurances()) {
              insurance.parseDescriptionsItems();
            }
          }
        }

        odigeoSession.setAvailableProductsResponse(availableProductsResponse);

        showNextActivity(availableProductsResponse);
      }
    }
  }

  /**
   * This method starts the next activity, and shows the insurance screen if necessary
   */
  private void showNextActivity(AvailableProductsResponse availableProductsResponse) {
    //Set if the insurances screen should be skipped
    if (availableProductsResponse.getInsuranceOffers() == null
        || availableProductsResponse.getInsuranceOffers().isEmpty()) {
      odigeoSession.setSkipInsurance(true);
    } else {
      odigeoSession.setSkipInsurance(false);
    }

    //Start the next activity
    Intent intent = new Intent(this, odigeoApp.getNextActivityClass(Step.RESULTS));

    intent.putExtra(Constants.EXTRA_STEP_TO_SUMMARY, Step.RESULTS);
    intent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE, mCreateShoppingCartResponse);
    intent.putExtra(Constants.EXTRA_AVAILABLE_PRODUCTS_RESPONSE, mAvailableProductsResponse);
    intent.putExtra(Constants.EXTRA_FLOW_CONFIGURATION_RESPONSE, mFlowConfigurationResponse);
    intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, mSearchOptions);
    intent.putExtra(Constants.EXTRA_FULL_TRANSPARENCY, mIsFullTransparency);
    intent.putExtra(Constants.EXTRA_BOOKING_INFO, bookingInfo);
    startActivity(intent);
  }

  /**
   * Process flight search response Process flight search response
   *
   * @param flightSearchResponse Service response
   */
  public final void onFlightsRequestSuccess(FlightSearchResponse flightSearchResponse) {
    //TODO: Add @NonNull annotation to flightSearchResponse
    //Check if there is an error
    if (flightSearchResponse.getItineraryResultsPage() != null
        && flightSearchResponse.getItineraryResultsPage().getItineraryResults() != null
        && !flightSearchResponse.getItineraryResultsPage().getItineraryResults().isEmpty()) {
      parseRequestResponse(flightSearchResponse);
    } else {
      showNoResultsFragment();
    }
  }

  public void showNoResultsFragment() {
    if (getSearchOptions().getTravelType() == TravelType.SIMPLE) {
      OdigeoNoResultsFragment newFragment = getNoResultsSimpleFragment();
      newFragment.setListener(this);
      addFragment(newFragment);
    } else if (getSearchOptions().getTravelType() == TravelType.ROUND) {
      OdigeoNoResultsFragment newFragment = getNoResultsRoundFragment();
      newFragment.setListener(this);
      addFragment(newFragment);
    } else {
      OdigeoNoResultsMultipleFragment newFragment =
          OdigeoNoResultsMultipleFragment.newInstance(getSearchOptions());
      addFragment(newFragment);
    }
  }

  @Override public final void onClickSearch(SearchTripWidget searchTripWidget, boolean isResident) {
    searchFlights();
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == android.R.id.home) {
      postGAEventTrackingGoBack();
      onBackPressed();
    }

    return super.onOptionsItemSelected(item);
  }

  public final CollectionMethodWithPrice getCardSelected() {
    return cardSelected;
  }

  public final void setCardSelected(CollectionMethodWithPrice cardSelected) {
    this.cardSelected = cardSelected;
  }

  public final FiltersSelected getFiltersSelected() {
    return filtersSelected;
  }

  public final void setFiltersSelected(FiltersSelected filtersSelected) {
    this.filtersSelected = filtersSelected;
  }

  private void generatedCheapestMethod() {
    //Set the cheapest method
    if (collectionMethodWithPrices != null && !collectionMethodWithPrices.isEmpty()) {
      //collectionMethodWithPrices.get(0).setCheapest(true);
      CollectionMethodWithPrice cheapestPrice =
          CollectionMethodTransform.clone(collectionMethodWithPrices.get(0));
      cheapestPrice.getCollectionMethod().getCreditCardType().setCode("CHEAPEST");
      cheapestPrice.getCollectionMethod()
          .getCreditCardType()
          .setName(LocalizablesFacade.getString(this, "fullprice_picker_cheapest").toString());
      cheapestPrice.setCheapest(true);

      collectionMethodWithPrices.add(0, cheapestPrice);
      cardSelected = cheapestPrice;
    }
  }

  public final void recreateFragmentWithCollectionMethods() {
    OdigeoResultsFragment resultsFragment = OdigeoResultsFragment.newInstance(mSearchOptions);
    resultsFragment.setCollectionMethodWithPrices(collectionMethodWithPrices);
    resultsFragment.setFullTransparency(mIsFullTransparency);
    addFragment(resultsFragment);
  }

  public final void onDeleteHistorySearchW(TripFragmentBase fragment) {
    BusProvider.getInstance()
        .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_NO_RESULTS_MULTI));
  }

  private void postGAScreenTrackingNoResultsWithFilters() {
    // GAnalytics screen tracking - No results with filters
    BusProvider.getInstance()
        .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_NO_RESULTS_FILTERS));
  }

  private void postGAEventTrackingGoBack() {
    // GATracker - Flight search results page - event 1
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_RESULTS,
            GAnalyticsNames.ACTION_NAVIGATION, GAnalyticsNames.LABEL_GO_BACK));
  }

  private void selectDestination(Intent data) {

    //Get Data from Destinations activity
    int noSegment = data.getIntExtra(Constants.EXTRA_FLIGHT_SEGMENT_NUMBER, 0);
    FlightsDirection flightDirection =
        (FlightsDirection) data.getSerializableExtra(Constants.EXTRA_FLIGHT_DIRECTION);
    City destinationSelected = (City) data.getSerializableExtra(Constants.EXTRA_DESTINATION);

    //Set the destination selected
    if (flightDirection == FlightsDirection.RETURN) {
      getSearchOptions().getFlightSegments().get(noSegment).setArrivalCity(destinationSelected);
    } else {
      getSearchOptions().getFlightSegments().get(noSegment).setDepartureCity(destinationSelected);
    }

    //Set the second segment to the round trip
    if ((noSegment == 0 || noSegment == 1) && getSearchOptions().getFlightSegments().size() > 1) {
      getSearchOptions().getFlightSegments()
          .get(1)
          .setDepartureCity(getSearchOptions().getFlightSegments().get(0).getArrivalCity());
      getSearchOptions().getFlightSegments()
          .get(1)
          .setArrivalCity(getSearchOptions().getFlightSegments().get(0).getDepartureCity());
    }
  }

  private void selectDate(Intent data) {
    //Get Data from Calendar activity
    int noSegment = data.getIntExtra(Constants.EXTRA_FLIGHT_SEGMENT_NUMBER, 0);

    List<Date> dates = (ArrayList<Date>) data.getSerializableExtra(Constants.EXTRA_SELECTED_DATES);

    Date departureDate = dates.get(0);
    Date returnDate = dates.get(1);

    if (noSegment == 0 || noSegment == 1) {
      getSearchOptions().getFlightSegments().get(0).setDate(departureDate.getTime());

      if (getSearchOptions().getFlightSegments().size() > 1) {
        if (returnDate != null) {
          getSearchOptions().getFlightSegments().get(1).setDate(returnDate.getTime());
        } else {
          getSearchOptions().getFlightSegments().get(1).setDate(0);
        }
      }
    }
  }

  private void filterResult(Intent data) {
    setFiltersSelected(
        (FiltersSelected) data.getSerializableExtra(Constants.EXTRA_FILTERS_SELECTED));

    if (!odigeoSession.getItineraryResultsFiltered().isEmpty()) {
      recreateFragmentWithCollectionMethods();
    } else {
      OdigeoNoResultsFiltersFragment fragmentFilters = new OdigeoNoResultsFiltersFragment();

      postGAScreenTrackingNoResultsWithFilters();

      fragmentFilters.setListener(this);
      addFragment(fragmentFilters);
    }
  }

  private void parseRequestResponse(FlightSearchResponse flightSearchResponse) {
    searchResponse = flightSearchResponse;
    odigeoSession.setSearchResponse(flightSearchResponse);

    //Sort criteria default
    if (flightSearchResponse.getItineraryResultsPage().getSortCriteria() == null) {
      flightSearchResponse.getItineraryResultsPage()
          .setSortCriteria(ItinerarySortCriteriaDTO.APPARENT_PROVIDER_PRICE_WITH_DISCOUNTS);
    }
    Log.i(Constants.TAG_LOG_FULLPRICE,
        flightSearchResponse.getItineraryResultsPage().getSortCriteria().name());

    odigeoSession.setSortCriteria(flightSearchResponse.getItineraryResultsPage().getSortCriteria());

    odigeoSession.setSessionBookingCookies(flightSearchResponse.getCookies());

    //Add The JSession Cookie to Crashlytics
    NeckCookie jsessionIdCookie =
        Util.getCookieById(flightSearchResponse.getCookies(), JSESSION_COOKIE);

    if (jsessionIdCookie != null) {
      CrashlyticsController crashlyticsController =
          AndroidDependencyInjector.getInstance().provideCrashlyticsController();
      crashlyticsController.setString(DAPI_JSESSION, jsessionIdCookie.extractTheValuePart());

      PreferencesControllerInterface preferencesController =
          AndroidDependencyInjector.getInstance().providePreferencesController();
      preferencesController.saveStringValue(JSESSION_COOKIE,
          jsessionIdCookie.extractTheValuePart());
    }

    //Set the default Insurance selected
    if (flightSearchResponse.getFlowConfiguration() != null) {
      Log.i(Constants.TAG_LOG,
          "FlowConfiguration= " + new Gson().toJson(flightSearchResponse.getFlowConfiguration()));
      odigeoSession.setInsuranceOptOut(
          flightSearchResponse.getFlowConfiguration().getInsuranceOptout());
    }

    //Make friendly the list
    new FlightSearchTransform().makeFlightSearchResponseFriendly(flightSearchResponse,
        mSearchOptions.getFlightSegments().size(), odigeoSession.getSortCriteria());

    //The list filtered is the list complete
    odigeoSession.setItineraryResults(
        flightSearchResponse.getItineraryResultsPage().getItineraryResults());
    odigeoSession.setItineraryResultsFiltered(new ArrayList<FareItineraryDTO>());
    if (flightSearchResponse.getFlowConfiguration() != null) {
      stepsFlowConfiguration = flightSearchResponse.getFlowConfiguration().getStepsConfiguration();
    }
    Log.i(Constants.TAG_LOG, "Adding Results Fragment");

    OdigeoResultsFragment resultsFragment = OdigeoResultsFragment.newInstance(mSearchOptions);

    //Show full Price widget
    if (odigeoSession.getSortCriteria() == ItinerarySortCriteriaDTO.MINIMUM_PURCHASABLE_PRICE) {
      collectionMethodWithPrices = resultsFragment.getCollectionMethodFees(flightSearchResponse);
      generatedCheapestMethod();
      resultsFragment.setCollectionMethodWithPrices(collectionMethodWithPrices);
    }
    setFlowConfigurationResponse(flightSearchResponse.getFlowConfiguration());

    mIsFullTransparency = flightSearchResponse.getItineraryResultsPage().isFullTransparency();
    resultsFragment.setFullTransparency(mIsFullTransparency);
    addFragment(resultsFragment);
  }

  private void resetPromocode() {
    SharedPreferences sp =
        getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sp.edit();
    editor.remove(Constants.PROMOCODE_KEY);
    editor.apply();
  }

  @Override public void onBackPressed() {
    Intent data = new Intent();
    data.putExtra(Constants.EXTRA_SEARCH_OPTIONS, mSearchOptions);
    setResult(RESULT_OK, data);
    super.onBackPressed();
  }

  public abstract void onGAEventTriggered(GATrackingEvent event);

  protected void addWaitingFragment() {
    addFragment(new OdigeoSearchWaitingResultsFragment());
  }

  public void setCreateShoppingCartResponse(
      com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse createShoppingCartResponse) {
    mCreateShoppingCartResponse = createShoppingCartResponse;
  }

  public void setAvailableProductsResponse(
      com.odigeo.data.entity.shoppingCart.AvailableProductsResponse availableProductsResponse) {
    mAvailableProductsResponse = availableProductsResponse;
  }

  public void setFlowConfigurationResponse(FlowConfigurationResponse flowConfigurationResponse) {
    mFlowConfigurationResponse = flowConfigurationResponse;
  }

  public boolean userIsMember() {
    return membershipInteractor.isMemberForCurrentMarket();
  }
}
