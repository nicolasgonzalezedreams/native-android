package com.odigeo.app.android.lib.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.globant.roboneck.base.RoboNeckFragment;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.utils.BusProvider;

public class MenuHomeEmptyWidgetFragment extends RoboNeckFragment {

  public MenuHomeEmptyWidgetFragment() {
    // Required empty public constructor
  }

  @Override public final void onResume() {
    super.onResume();
    BusProvider.getInstance().register(this);
  }

  @Override public final void onPause() {
    super.onPause();
    BusProvider.getInstance().unregister(this);
  }

  @Override public final View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    return inflater.inflate(R.layout.layout_mytrip_home_empty_widget, container, false);
  }
}
