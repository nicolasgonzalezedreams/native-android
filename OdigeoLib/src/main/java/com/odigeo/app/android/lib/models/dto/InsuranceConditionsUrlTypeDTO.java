package com.odigeo.app.android.lib.models.dto;

public enum InsuranceConditionsUrlTypeDTO {
  BASIC, EXTENDED;
}
