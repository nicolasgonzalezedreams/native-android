package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.helpers.DrawableUtils;

public class FilterButtonWidget extends LinearLayout {
  private int idIndex;
  private boolean isSelected;
  private int idResourceSelectedImage;
  private int idResourceUnselectedImage;
  private String textButton;

  private TextView buttonTextView;
  private ImageView buttonImageView;

  public FilterButtonWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
    TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.FilterButtonWidget);
    idIndex = typedArray.getInt(R.styleable.FilterButtonWidget_idIndex, 0);
    isSelected = typedArray.getBoolean(R.styleable.FilterButtonWidget_isSelected, false);
    idResourceSelectedImage =
        typedArray.getResourceId(R.styleable.FilterButtonWidget_idImageSelected,
            R.mipmap.search_filters);
    idResourceUnselectedImage =
        typedArray.getResourceId(R.styleable.FilterButtonWidget_idImageUnselected,
            R.mipmap.search_filters);
    textButton = typedArray.getString(R.styleable.FilterButtonWidget_odigeoText);
    typedArray.recycle();
    inflateLayout();
    init();
  }

  public FilterButtonWidget(Context context) {
    super(context);
    inflateLayout();
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.layout_filter_button, this);
    buttonTextView = (TextView) findViewById(R.id.text_filter_button);
    buttonImageView = (ImageView) findViewById(R.id.icon_filters_button);
  }

  private void init() {
    if (textButton != null) {
      buttonTextView.setText(LocalizablesFacade.getString(getContext(), textButton));
    }
    if (isSelected) {
      buttonImageView.setImageDrawable(
          DrawableUtils.getTintedResource(idResourceSelectedImage, R.color.selected_filter_icon,
              getContext()));
      buttonTextView.setTextColor(getResources().getColor(R.color.selected_filter_icon));
    } else {
      buttonImageView.setImageResource(idResourceUnselectedImage);
      buttonTextView.setTextColor(getResources().getColor(R.color.basic_middle));
    }
  }

  public final void click() {
    isSelected = !isSelected;
    setSelected(isSelected);
    if (isSelected) {
      buttonImageView.setImageDrawable(
          DrawableUtils.getTintedResource(idResourceSelectedImage, R.color.selected_filter_icon,
              getContext()));
      buttonTextView.setTextColor(getResources().getColor(R.color.selected_filter_icon));
    } else {
      buttonImageView.setImageResource(idResourceUnselectedImage);
      buttonTextView.setTextColor(getResources().getColor(R.color.basic_middle));
    }
  }

  public final int getIdIndex() {
    return idIndex;
  }

  public final void setIdIndex(int idIndex) {
    this.idIndex = idIndex;
  }
}

