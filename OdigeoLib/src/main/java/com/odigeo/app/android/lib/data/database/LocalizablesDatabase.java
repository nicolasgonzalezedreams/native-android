package com.odigeo.app.android.lib.data.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import java.io.IOException;

/**
 * @author Cristian Fanjul
 * @since 27/06/2015.
 */
public class LocalizablesDatabase {
  private static LocalizablesDatabase instance = new LocalizablesDatabase();
  private LocalizablesDBManager dbManager;

  public static LocalizablesDatabase getInstance() {
    return instance;
  }

  public SQLiteDatabase getDatabase(Context context) throws IOException, SQLException {
    if (dbManager == null) {
      dbManager = new LocalizablesDBManager(context);
      dbManager.createDataBase();
    }
    return dbManager.getWritableDatabase();
  }
}
