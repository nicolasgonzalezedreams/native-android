package com.odigeo.app.android.lib.models.dto;

import java.util.ArrayList;
import java.util.List;

public class AdditionalProductBookingDTO {

  protected List<AdditionalProductProviderBookingDTO> bookings;

  public List<AdditionalProductProviderBookingDTO> getBookings() {
    if (bookings == null) {
      bookings = new ArrayList<AdditionalProductProviderBookingDTO>();
    }
    return this.bookings;
  }
}
