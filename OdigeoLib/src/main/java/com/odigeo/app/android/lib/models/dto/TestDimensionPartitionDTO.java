package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for testDimensionPartition complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="testDimensionPartition">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="partitionNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}int"
 * />
 *       &lt;attribute name="weight" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class TestDimensionPartitionDTO {

  protected int partitionNumber;
  protected Integer weight;

  /**
   * Gets the value of the partitionNumber property.
   */
  public int getPartitionNumber() {
    return partitionNumber;
  }

  /**
   * Sets the value of the partitionNumber property.
   */
  public void setPartitionNumber(int value) {
    this.partitionNumber = value;
  }

  /**
   * Gets the value of the weight property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getWeight() {
    return weight;
  }

  /**
   * Sets the value of the weight property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setWeight(Integer value) {
    this.weight = value;
  }
}
