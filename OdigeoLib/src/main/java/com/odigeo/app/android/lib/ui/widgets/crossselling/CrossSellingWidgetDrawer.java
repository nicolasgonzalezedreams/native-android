package com.odigeo.app.android.lib.ui.widgets.crossselling;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoConfirmationActivity;
import com.odigeo.app.android.lib.config.HotelsUrlBuilder;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.models.OldMyShoppingCart;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.shoppingCart.ShoppingCart;
import com.odigeo.helper.CrossCarUrlHandlerInterface;
import com.odigeo.interactors.provider.MarketProviderInterface;
import java.lang.ref.WeakReference;

/**
 * Draws cross selling widget when instantiated. It needs to have the xml part set in the layout.
 *
 * @author cristian.fanjul
 * @since 24/02/2015
 */
@Deprecated public class CrossSellingWidgetDrawer {

  private static final int DAYS_TO_ADD_SIMPLE_FLIGHT = 3;
  private final Activity activity;
  private final SearchOptions searchOptions;
  private final OldMyShoppingCart mOldMyShoppingCart;
  private final ShoppingCart mShoppingCart;
  private final MarketProviderInterface marketProvier;
  private HotelsUrlBuilder hotelsUrlBuilder;
  private String screenLabel;
  private String screenAdCamp;

  /**
   * The Widget is drawn.
   *
   * @param wrefActivity Gets a weak reference instead of the whole activity in order to optimize
   * memory usage.
   */
  public CrossSellingWidgetDrawer(WeakReference<Activity> wrefActivity,
      OldMyShoppingCart oldMyShoppingCart, SearchOptions searchOptions, String screenLabel,
      String screenAdCamp, ShoppingCart shoppingCart, MarketProviderInterface marketProvider) {
    this.activity = wrefActivity.get();
    this.searchOptions = searchOptions;
    this.screenLabel = screenLabel;
    this.screenAdCamp = screenAdCamp;
    this.marketProvier = marketProvider;
    mOldMyShoppingCart = oldMyShoppingCart;
    mShoppingCart = shoppingCart;
    drawCrossSellingWidget();
  }

  /**
   * Cross selling widget is displayed with the information about the trip.
   */
  private void drawCrossSellingWidget() {
    CrossSellingWidget crossSellingWidget =
        (CrossSellingWidget) activity.findViewById(R.id.cross_selling_widget);
    crossSellingWidget.setSearchOptions(searchOptions);
    crossSellingWidget.setVisibility(View.VISIBLE);

    crossSellingWidget.getBookHotelButton().setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        postGAEventBookHotel();
        setHotelButtonClickListener();
      }
    });

    crossSellingWidget.getRentCarButton().setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        postGAEventRentCar();
        setRentCarButtonClickListener();
      }
    });
  }

  /**
   * Listener for hotel button.
   */
  private void setHotelButtonClickListener() {
    postGAEventOpenHotelsWebView();

    if (searchOptions.getNumberOfKids() > 0) {
      // Redirects to home or filter page.
      hotelsUrlBuilder = new HotelsUrlBuilder(false, screenLabel, marketProvier);
      hotelsUrlBuilder.setParametersForFilters(searchOptions);
    } else {
      // Redirects to search results.
      hotelsUrlBuilder = new HotelsUrlBuilder(searchOptions, screenLabel, marketProvier);
    }

    if (searchOptions.getTravelType() == TravelType.SIMPLE) {
      addExtraDaysToSimpleTravel();
    }

    logCatUrl(hotelsUrlBuilder.getUrl());

    startWebViewActivity(hotelsUrlBuilder.getUrl(), Constants.WEBVIEW_HOTELS);
  }

  /**
   * A number of days are added to the arrival date for simple travels. These days are used to
   * find a hotel.
   */
  private void addExtraDaysToSimpleTravel() {

    hotelsUrlBuilder.setCheckOutDateValues(OdigeoDateUtils.addDaysToDate(
        OdigeoDateUtils.createDate(searchOptions.getFirstSegment().getArrivalDate()),
        DAYS_TO_ADD_SIMPLE_FLIGHT));
  }

  /**
   * Listener for car button.
   */
  private void setRentCarButtonClickListener() {
    postGAEventOpenCarsWebView();

    long travellerBirthDate;
    if (mOldMyShoppingCart.getTravellers() != null) {
      travellerBirthDate = mOldMyShoppingCart.getTravellers().get(0).getDateOfBirth();
    } else {
      travellerBirthDate = mShoppingCart.getTravellers().
          get(0).getDateOfBirth();
    }
    long arrivalDate = mOldMyShoppingCart.getArrivalDate();
    long lastDepartureDate = mOldMyShoppingCart.getLastDepartureDate();

    String arrivalAirportCode = searchOptions.getFirstSegment().getArrivalCity().getIataCode();
    CrossCarUrlHandlerInterface crossCarUrlHandler =
        AndroidDependencyInjector.getInstance().provideCrossCarUrlHandler();
    String url = crossCarUrlHandler.getCarUrlWithSearchLoader(arrivalAirportCode, arrivalDate,
        lastDepartureDate, travellerBirthDate, screenAdCamp);

    startWebViewActivity(url, Constants.WEBVIEW_CARS);
  }

  /**
   * The webview activity indicated is initialized.
   */
  private void startWebViewActivity(String url, String webViewValue) {
    OdigeoApp odigeoApp = (OdigeoApp) activity.getApplication();
    Intent intent = new Intent(activity, odigeoApp.getWebViewActivityClass());
    intent.putExtra(Constants.EXTRA_URL_WEBVIEW, url);
    intent.putExtra(Constants.WEBVIEW_TYPE, webViewValue);
    activity.startActivity(intent);
  }

  private void postGAEventOpenCarsWebView() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_HOME,
            GAnalyticsNames.ACTION_PRODUCT_SELECTOR, GAnalyticsNames.LABEL_PRODUCT_CARS_CLICKS));
  }

  private void postGAEventOpenHotelsWebView() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_HOME,
            GAnalyticsNames.ACTION_PRODUCT_SELECTOR, GAnalyticsNames.LABEL_PRODUCT_HOTELS_CLICKS));
  }

  /**
   * Logs url called in logcat.
   */
  private void logCatUrl(String url) {
    Log.i(Constants.TAG_LOG, "Calling URL: " + url);
  }

  /**
   * tracking of tap on "Book Hotel" button check if the parent activity is Confirmation or
   * Summary
   */
  private void postGAEventBookHotel() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(getCategoryByActivity(), GAnalyticsNames.ACTION_CROSS_SELL_WIDGET,
            GAnalyticsNames.LABEL_CROSS_SELL_HOTEL_CLICKS));
  }

  /**
   * tracking of tap on "Rent a car" button check if the parent activity is Confirmation or
   * Summary
   */
  private void postGAEventRentCar() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(getCategoryByActivity(), GAnalyticsNames.ACTION_CROSS_SELL_WIDGET,
            GAnalyticsNames.LABEL_CROSS_SELL_CAR_CLICKS));
  }

  /**
   * check if the parent activity is confirmation or summary
   *
   * @return category for the tracking of event
   */
  private String getCategoryByActivity() {
    if (this.activity instanceof OdigeoConfirmationActivity) {
      return GAnalyticsNames.CATEGORY_FLIGHTS_CONFIRMATION_PAGE_CONFIRMED;
    } else {
      return GAnalyticsNames.CATEGORY_MY_TRIPS;
    }
  }
}
