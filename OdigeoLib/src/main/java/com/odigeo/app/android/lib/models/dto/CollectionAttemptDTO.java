package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class CollectionAttemptDTO {

  protected CollectionMethodDTO collectionMethod;
  protected BigDecimal amount;
  protected CollectionMethodTypeDTO type;

  public CollectionMethodDTO getCollectionMethod() {
    return collectionMethod;
  }

  /**
   * Gets the value of the amount property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getAmount() {
    return amount;
  }

  /**
   * Sets the value of the amount property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */

  public void setType(CollectionMethodTypeDTO value) {
    this.type = value;
  }
}
