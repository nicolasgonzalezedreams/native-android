package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoFiltersActivity;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.ui.widgets.FilterListWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Irving Lóp on 14/10/2014.
 */
public class OdigeoFilterAirlinesFragment extends Fragment {

  private OdigeoFiltersActivity activity;
  private OdigeoApp odigeoApp;
  private OdigeoSession odigeoSession;

  private List<CarrierDTO> carriers;
  private FilterListWidget listWidget;

  @Override public final void onAttach(Activity activity) {
    super.onAttach(activity);
    this.activity = (OdigeoFiltersActivity) activity;

    postGAScreenTrackingFiltersAirline();
  }

  @Override public final void onCreate(Bundle savedInstaceState) {
    super.onCreate(savedInstaceState);
    odigeoApp = (OdigeoApp) activity.getApplication();
    odigeoSession = odigeoApp.getOdigeoSession();

    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {

      carriers = activity.getCarriers();
    }
  }

  @Override public final View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.layout_filter_compagnie_fragment, container, false);
    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {

      listWidget = (FilterListWidget) view.findViewById(R.id.list_filter_compagnie_filterlist);
      drawValues();

      //Set the carriers that were selected previously
      if (this.activity.getFiltersSelected() != null
          && this.activity.getFiltersSelected().getCarriers() != null) {

        List<String> carriersNames = new ArrayList<String>();

        for (CarrierDTO carrier : this.activity.getFiltersSelected().getCarriers()) {
          carriersNames.add(carrier.getName());
        }

        listWidget.setSelectedNames(carriersNames);
      }
    }
    return view;
  }

  @Override public final void onViewCreated(View view, Bundle savedInstanceState) {

    super.onViewCreated(view, savedInstanceState);
    if (odigeoSession == null || !odigeoSession.isDataHasBeenLoaded()) {
      Util.sendToHome(activity, odigeoApp);
    }
  }

  private void drawValues() {
    if (carriers != null) {
      List<String> stringNames = new ArrayList<String>();
      for (CarrierDTO carrier : carriers) {
        stringNames.add(carrier.getName());
      }
      //            listWidget.setValues(stringNames);
      listWidget.setCarriers(carriers);
    }
  }

  public final boolean hasNewValues() {
    return carriers.size() != listWidget.getSelectedNames().size();
  }

  public final List<CarrierDTO> getSelectedCarriers() {
    List<CarrierDTO> filterList = new ArrayList<CarrierDTO>();
    List<String> selectedCarriers = listWidget.getSelectedNames();

    for (CarrierDTO carrier : carriers) {
      if (selectedCarriers.contains(carrier.getName())) {
        filterList.add(carrier);
      }
    }

    return filterList;
  }

  private void postGAScreenTrackingFiltersAirline() {
    BusProvider.getInstance()
        .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_FILTERS_AIRLINE
            //, getActivity().getApplication()
        ));
  }
}
