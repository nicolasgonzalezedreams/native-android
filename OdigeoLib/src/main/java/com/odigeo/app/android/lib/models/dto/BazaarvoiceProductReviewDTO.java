package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BazaarvoiceProductReviewDTO {

  protected String title;
  protected String reviewer;
  protected BigDecimal rating;
  protected String text;
  protected List<BazaarvoiceRatingValueReviewDTO> ratingValueReviews;
  protected long reviewDate;

  /**
   * Gets the value of the textTitle property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getTitle() {
    return title;
  }

  /**
   * Sets the value of the textTitle property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setTitle(String value) {
    this.title = value;
  }

  /**
   * Gets the value of the reviewer property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getReviewer() {
    return reviewer;
  }

  /**
   * Sets the value of the reviewer property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setReviewer(String value) {
    this.reviewer = value;
  }

  /**
   * Gets the value of the rating property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getRating() {
    return rating;
  }

  /**
   * Sets the value of the rating property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setRating(BigDecimal value) {
    this.rating = value;
  }

  /**
   * Gets the value of the text property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getText() {
    return text;
  }

  /**
   * Sets the value of the text property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setText(String value) {
    this.text = value;
  }

  /**
   * Gets the value of the ratingValueReviews property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the ratingValueReviews property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getRatingValueReviews().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list
   * {@link BazaarvoiceRatingValueReviewDTO }
   */
  public List<BazaarvoiceRatingValueReviewDTO> getRatingValueReviews() {
    if (ratingValueReviews == null) {
      ratingValueReviews = new ArrayList<BazaarvoiceRatingValueReviewDTO>();
    }
    return this.ratingValueReviews;
  }

  /**
   * Gets the value of the reviewDate property.
   *
   * @return possible object is
   * {@link java.util.Calendar }
   */
  public long getReviewDate() {
    return reviewDate;
  }

  /**
   * Sets the value of the reviewDate property.
   *
   * @param value allowed object is
   * {@link long }
   */
  public void setReviewDate(long value) {
    this.reviewDate = value;
  }
}
