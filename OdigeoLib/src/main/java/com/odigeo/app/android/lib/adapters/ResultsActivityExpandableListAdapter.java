package com.odigeo.app.android.lib.adapters;

import android.app.Activity;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.interfaces.AdapterExpandableListListener;
import com.odigeo.app.android.lib.interfaces.ISegmentGroupWidget;
import com.odigeo.app.android.lib.models.CollectionEstimationFees;
import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import com.odigeo.app.android.lib.models.SegmentGroup;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;
import com.odigeo.app.android.lib.ui.widgets.SegmentGroupWidget;
import com.odigeo.app.android.lib.utils.ResultsPriceCalculator;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.data.entity.TravelType;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public abstract class ResultsActivityExpandableListAdapter extends BaseExpandableListAdapter
    implements ISegmentGroupWidget {
  private static final int RIGHT_VALUE = 6;
  protected final Activity context;
  protected LayoutInflater inflater;
  private Fonts fonts;
  private int noSegments;
  protected SegmentGroupWidget[][] segmentGroupWidgets;
  protected final List<FareItineraryDTO> itineraryResults;
  private final TravelType travelType;
  private AdapterExpandableListListener adapterExpandableListListener;
  protected int divider;
  protected View[] itineraryContainers;
  private int indexItineraryResultSelected = -1;
  protected CollectionMethodWithPrice selectedMethod;
  protected String mFullPriceSentence;
  protected String locale;
  private OdigeoImageLoader mImageLoader;

  protected ResultsActivityExpandableListAdapter(Activity context,
      List<FareItineraryDTO> itineraryResults, TravelType travelType, int divider, int numSegments,
      String locale, AdapterExpandableListListener adapterExpandableListListener) {
    super();
    this.context = context;
    this.travelType = travelType;
    this.itineraryResults = itineraryResults;
    this.noSegments = numSegments;
    this.divider = divider;
    this.locale = locale;
    this.adapterExpandableListListener = adapterExpandableListListener;

    fonts = Configuration.getInstance().getFonts();
    segmentGroupWidgets = new SegmentGroupWidget[itineraryResults.size()][noSegments];
    inflater = LayoutInflater.from(context);
    mImageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();
  }

  public void setFullPriceSentence(String sentence) {
    mFullPriceSentence = sentence;
  }

  public void setItineraryContainers() {
    itineraryContainers = new View[getGroupCount()];
  }

  @Override public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
      ViewGroup parent) {
    return inflateHeader(groupPosition, isExpanded, convertView, parent);
  }

  protected boolean isFullTransparency() {
    return (mFullPriceSentence != null) && !TextUtils.isEmpty(mFullPriceSentence);
  }

  protected void setItineraryAirlinesImages(int groupPosition) {
    View convertView = itineraryContainers[groupPosition];

    Set<CarrierDTO> setCarriers = ((FareItineraryDTO) getGroup(groupPosition)).getCarriers();
    if (setCarriers != null) {
      LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
          context.getResources().getDimensionPixelOffset(R.dimen.size_airline_icon),
          context.getResources().getDimensionPixelOffset(R.dimen.size_airline_icon));
      params.setMargins(0, 0, RIGHT_VALUE, 0);
      LinearLayout layoutIcons =
          (LinearLayout) convertView.findViewById(R.id.layout_airlines_icons);
      for (CarrierDTO carrier : setCarriers) {
        ImageView imageViewIcon = new ImageView(context);
        imageViewIcon.setLayoutParams(params);
        String url = ViewUtils.getCarrierLogo(carrier.getCode());
        mImageLoader.load(imageViewIcon, url);
        layoutIcons.addView(imageViewIcon);
      }
    }
  }

  protected void setItineraryPrice(int groupPosition) {
    View convertView = itineraryContainers[groupPosition];

    if (convertView != null) {

      String headerPrice =
          ResultsPriceCalculator.calculatePrice((FareItineraryDTO) getGroup(groupPosition),
              selectedMethod, divider, locale);

      TextView textHeaderPrice = (TextView) convertView.findViewById(R.id.flights_price);
      textHeaderPrice.setText(Util.getTextStylePrice(headerPrice));
      textHeaderPrice.setTypeface(fonts.getBold());
    }
  }

  @Override public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
      View convertView, ViewGroup parent) {
    View view;

    if (segmentGroupWidgets[groupPosition][childPosition] != null) {
      view = segmentGroupWidgets[groupPosition][childPosition];
    } else {
      SegmentGroup segmentGroup = getChild(groupPosition, childPosition);

      SegmentGroupWidget widget =
          new SegmentGroupWidget(context, segmentGroup, travelType, groupPosition);
      widget.setListener(this);
      segmentGroupWidgets[groupPosition][childPosition] = widget;

      //Add a divider between each segment group
      if (childPosition == (getChildrenCount(groupPosition) - 1)) {
        widget.setLastInForm(true);
        widget.drawSeparatorGroup();
      }

      widget.drawSegments();
      view = widget;
    }

    //Is the last child?
    if (childPosition == getChildrenCount(groupPosition) - 1) {
      CollectionEstimationFees collectionEstimationFees =
          ((FareItineraryDTO) getGroup(groupPosition)).getCollectionMethodFeesObject();

      CollectionMethodWithPrice cheapestPrice =
          ResultsPriceCalculator.getCheapestCollectionMethodFee(collectionEstimationFees);

      CollectionMethodWithPrice selectedPrice =
          ResultsPriceCalculator.getSelectedCollectionMethodFee(collectionEstimationFees,
              selectedMethod);

      String creditCardMessage = null;

      if (cheapestPrice != null && selectedPrice != null) {
        if (selectedPrice.isCheapest() && context != null) {
          creditCardMessage = String.format(CommonLocalizables.getInstance().
                  getFullpriceResultsDescription(context).toString(),
              Configuration.getInstance().getBrandVisualName(),
              cheapestPrice.getCollectionMethod().getCreditCardType().getName());
        } else {
          creditCardMessage =
              CommonLocalizables.getInstance().getFullpriceResultsSelected(context).toString();
        }
      }

      segmentGroupWidgets[groupPosition][childPosition].setCardInformation(creditCardMessage,
          isFullTransparency());
    }

    return view;
  }

  @Override public int getGroupCount() {
    //Log.d(Constants.TAG_LOG, "getGroupCount() = " + itineraryResults.size());
    return itineraryResults.size();
  }

  @Override public int getChildrenCount(int groupPosition) {

    return itineraryResults.get(groupPosition).getSegmentGroups().size();
  }

  @Override public Object getGroup(int groupPosition) {
    if (groupPosition >= getGroupCount()) {
      Log.d(Constants.TAG_LOG, "Results, getGroup("
          + groupPosition
          + ") but there are only "
          + getGroupCount()
          + " items");
    }

    return itineraryResults.get(groupPosition);
  }

  @Override public SegmentGroup getChild(int groupPosition, int childPosition) {

    return itineraryResults.get(groupPosition).getSegmentGroups().get(childPosition);
  }

  @Override public long getGroupId(int groupPosition) {
    return groupPosition;
  }

  @Override public long getChildId(int groupPosition, int childPosition) {
    return childPosition;
  }

  @Override public boolean hasStableIds() {
    return false;
  }

  @Override public boolean isChildSelectable(int groupPosition, int childPosition) {
    return true;
  }

  @Override public void onSegmentSelected(SegmentGroupWidget segmentGroupWidget) {
    checkSelectedItinerary(segmentGroupWidget);
    //The index of the new itinerary result selected
    indexItineraryResultSelected = segmentGroupWidget.getItineraryResultParentIndex();
    int segmentGroupIndex = getSegmentGroupIndex(segmentGroupWidget);
    //Count how many segments are selected in the itinerary result
    int noSelected = 0;
    for (SegmentGroupWidget segmentGroupWidgetTemp : segmentGroupWidgets[indexItineraryResultSelected]) {
      if (segmentGroupWidgetTemp != null) {
        if (segmentGroupWidgetTemp.isSelected()) {
          noSelected++;
        }
      }
    }

    //The segments selected are the number of the segments in the trip?
    if (noSelected == noSegments) {
      Log.i(Constants.TAG_LOG, "Mostrando botón de continuar");

      //Show the continue button
      if (segmentGroupWidgets[indexItineraryResultSelected][noSegments - 1] != null) {
        segmentGroupWidgets[indexItineraryResultSelected][noSegments - 1].showButton();
        adapterExpandableListListener.onWidgetSelected(true);
      }
    }

    adapterExpandableListListener.onSegmentSelected(segmentGroupWidget, segmentGroupIndex);
  }

  /**
   * Get the segment group of the widget.
   *
   * @param segmentGroupWidget Segment where be performed the operations.
   * @return Index where be performed the next operations.
   */
  private int getSegmentGroupIndex(SegmentGroupWidget segmentGroupWidget) {
    //Select all the segments that are only one in its segment group
    int segmentGroupIndex = 0;
    int index = 0;
    for (SegmentGroupWidget segmentGroupWidgetTemp : segmentGroupWidgets[indexItineraryResultSelected]) {
      if (segmentGroupWidgetTemp != null) {
        if (!segmentGroupWidget.equals(segmentGroupWidgetTemp)) {
          //Is only one segment?
          if (segmentGroupWidgetTemp.getSegmentWidgets().size() == 1) {
            segmentGroupWidgetTemp.setSegmentWidgetSelected(
                segmentGroupWidgetTemp.getSegmentWidgets().get(0));
          }
        } else {
          segmentGroupIndex = index;
        }
      }

      index++;
    }
    return segmentGroupIndex;
  }

  /**
   * Check if it's selected the same itinerary.
   *
   * @param segmentGroupWidget Segment to be checked.
   */
  private void checkSelectedItinerary(SegmentGroupWidget segmentGroupWidget) {
    //Is the user selecting a segment in a different itinerary result?
    if (indexItineraryResultSelected != -1
        && indexItineraryResultSelected != segmentGroupWidget.getItineraryResultParentIndex()) {
      //Deselect all the segments in the previos FareItineraryDTO
      for (SegmentGroupWidget segmentGroupWidgetTemp : segmentGroupWidgets[indexItineraryResultSelected]) {
        if (segmentGroupWidgetTemp != null) {
          segmentGroupWidgetTemp.deselect();
        }
      }

      adapterExpandableListListener.onSelectDifferentItinerary(
          itineraryResults.get(segmentGroupWidget.getItineraryResultParentIndex()),
          segmentGroupWidget.getItineraryResultParentIndex());
    }
  }

  @Override public void onSegmentGroupUnselected(SegmentGroupWidget segmentGroupWidget) {
    //Hide the button
    if (segmentGroupWidgets[segmentGroupWidget.getItineraryResultParentIndex()][noSegments - 1]
        != null) {
      segmentGroupWidgets[segmentGroupWidget.getItineraryResultParentIndex()][noSegments
          - 1].hideButton();
      adapterExpandableListListener.onWidgetSelected(false);
    }
  }

  @Override public void onItinerarySelect(int itineraryIndex) {

    List<SegmentWrapper> segmentsSelected = new ArrayList<>();
    for (SegmentGroupWidget segmentGroupWidget : segmentGroupWidgets[itineraryIndex]) {
      if (segmentGroupWidget != null) {
        segmentsSelected.add(segmentGroupWidget.getSegmentWidgetSelected().getSegmentWrapper());
      }
    }

    adapterExpandableListListener.onItinerarySelected(itineraryResults.get(itineraryIndex),
        segmentsSelected);
  }

  public void setSelectedMethod(CollectionMethodWithPrice selectedMethod) {
    this.selectedMethod = selectedMethod;
  }

  @VisibleForTesting public List<FareItineraryDTO> getFareItineraries() {
    return itineraryResults;
  }

  @VisibleForTesting public boolean isSegmentGroupWidgetSelected() {
    return indexItineraryResultSelected != -1;
  }

  public abstract View inflateHeader(int groupPosition, boolean isExpanded, View convertView,
      ViewGroup parent);
}
