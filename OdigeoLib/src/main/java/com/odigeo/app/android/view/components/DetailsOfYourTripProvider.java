package com.odigeo.app.android.view.components;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.utils.LocaleHelper;
import com.odigeo.app.android.providers.FontProvider;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.FontHelper;
import com.odigeo.data.entity.booking.Baggage;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.entity.booking.Traveller;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import java.util.ArrayList;

import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_BILLING_INFORMATION;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_BOOKING_REFERENCE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_BTN_CONDITIONS;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_FLIGHT_CANCELATION;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_INSURANCE_DETAILS;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_PASSENGERS_INFORMATION;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_TITLE;

public class DetailsOfYourTripProvider {

  private final TrackerControllerInterface tracker;
  private final Activity activity;
  private final Booking booking;
  private final LocalizableProvider localizableProvider;
  private final LocaleHelper localeHelper;
  private final FontProvider fontProvider;
  private View mView;
  private TextView mTvImgInsuranceDetails;
  private TextView mTvImgBillingInformation;
  private TextView mTvImgBookingReference;
  private TextView mTvImgPassengersInformation;
  private TextView mTvDetailsTitle;
  private TextView mTvInsuranceDetails;
  private TextView mTvBillingInformation;
  private TextView mTvBookingReference;
  private TextView mTvPassengersInformation;
  private TextView mTvFoldUnfoldBooking;
  private TextView mTvFoldUnfoldPassengers;
  private TextView mTvFoldUnfoldInsurance;
  private TextView mTvFlightCancelationTitle;
  private TextView mTvFoldUnfoldBilling;
  private LinearLayout mLlContainerBooking;
  private LinearLayout mLlContainerPassengers;
  private LinearLayout mLlContainerInsurance;
  private LinearLayout mLlContainerBilling;
  private LinearLayout mLlInsuranceDetails;
  private LinearLayout mLlBillingInformation;
  private LinearLayout mLlBookingReference;
  private LinearLayout mLlPassengersInformation;
  private LinearLayout mLlInsuranceDescription;
  private TextView mTvInsurancePrice;
  private TextView mTvInsurancePricePerPassenger;
  private Button mBtnSeeConditions;
  private Typeface mTypeface;

  public DetailsOfYourTripProvider(Activity activity, Booking booking,
      TrackerControllerInterface tracker, LocalizableProvider localizableProvider,
      LocaleHelper localeHelper, FontProvider fontProvider) {
    this.activity = activity;
    this.booking = booking;
    this.tracker = tracker;
    this.localizableProvider = localizableProvider;
    this.localeHelper = localeHelper;
    this.fontProvider = fontProvider;
  }

  private void initView(ViewGroup parent) {
    mView = activity.getLayoutInflater().inflate(R.layout.details_of_your_trip, parent, false);

    mTvImgInsuranceDetails = (TextView) mView.findViewById(R.id.tvImgInsuranceDetails);
    mTvImgBillingInformation = (TextView) mView.findViewById(R.id.tvImgBillingInformation);
    mTvImgBookingReference = (TextView) mView.findViewById(R.id.tvImgBookingReference);
    mTvImgPassengersInformation = (TextView) mView.findViewById(R.id.tvImgPassengersInformation);

    mTvDetailsTitle = (TextView) mView.findViewById(R.id.tvDetailsTitle);
    mTvInsuranceDetails = (TextView) mView.findViewById(R.id.tvInsuranceDetails);
    mTvFlightCancelationTitle = (TextView) mView.findViewById(R.id.tvFlightCancelationTitle);
    mTvBillingInformation = (TextView) mView.findViewById(R.id.tvBillingInformation);
    mTvBookingReference = (TextView) mView.findViewById(R.id.tvBookingReference);
    mTvPassengersInformation = (TextView) mView.findViewById(R.id.tvPassengersInformation);

    mTvFoldUnfoldBooking = (TextView) mView.findViewById(R.id.tvFoldUnfoldBooking);
    mTvFoldUnfoldPassengers = (TextView) mView.findViewById(R.id.tvFoldUnfoldPassengers);
    mTvFoldUnfoldInsurance = (TextView) mView.findViewById(R.id.tvFoldUnfoldInsurances);
    mTvFoldUnfoldBilling = (TextView) mView.findViewById(R.id.tvFoldUnfoldBilling);

    mLlContainerBooking = (LinearLayout) mView.findViewById(R.id.llContainerBooking);
    mLlContainerPassengers = (LinearLayout) mView.findViewById(R.id.llContainerPassengers);
    mLlContainerInsurance = (LinearLayout) mView.findViewById(R.id.llContainerInsurance);
    mLlContainerBilling = (LinearLayout) mView.findViewById(R.id.llContainerBilling);

    mLlInsuranceDetails = (LinearLayout) mView.findViewById(R.id.llInsuranceDetails);
    mLlBillingInformation = (LinearLayout) mView.findViewById(R.id.llBillingInformation);
    mLlBookingReference = (LinearLayout) mView.findViewById(R.id.llBookingReference);
    mLlPassengersInformation = (LinearLayout) mView.findViewById(R.id.llPassengersInformation);
    mLlInsuranceDescription = (LinearLayout) mView.findViewById(R.id.llInsuranceDescription);

    mTvInsurancePrice = (TextView) mView.findViewById(R.id.tvInsurancePrice);
    mTvInsurancePricePerPassenger =
        (TextView) mView.findViewById(R.id.tvInsurancePricePerPassenger);

    mBtnSeeConditions = (Button) mView.findViewById(R.id.btnSeeConditions);
    mTypeface = fontProvider.loadTypeFaceFromFontAsset("fonts/trip_details_font.ttf");

    initOneCMSText();
  }

  /**
   * Get a card details of your trips built
   *
   * @return Card of details of your trips
   */
  public View getDetailsOfYourTrips(ViewGroup parent) {
    initView(parent);
    setFontAndImage();
    setListeners();
    setDetailsOfYourTrips();
    return mView;
  }

  /**
   * Hide all containers
   */
  private void hideAllContainers() {
    mLlContainerBilling.setVisibility(View.GONE);
    mLlContainerPassengers.setVisibility(View.GONE);
    mLlContainerBooking.setVisibility(View.GONE);
    mLlContainerInsurance.setVisibility(View.GONE);

    mTvFoldUnfoldBooking.setText(FontHelper.ICON_MY_TRIP_DETAILS_UNFOLD);
    mTvFoldUnfoldPassengers.setText(FontHelper.ICON_MY_TRIP_DETAILS_UNFOLD);
    mTvFoldUnfoldInsurance.setText(FontHelper.ICON_MY_TRIP_DETAILS_UNFOLD);
    mTvFoldUnfoldBilling.setText(FontHelper.ICON_MY_TRIP_DETAILS_UNFOLD);

    //Change Color
    mTvImgBookingReference.setTextColor(activity.getResources().getColor(R.color.basic_dark));
    mTvBookingReference.setTextColor(activity.getResources().getColor(R.color.basic_dark));

    mTvImgInsuranceDetails.setTextColor(activity.getResources().getColor(R.color.basic_dark));
    mTvInsuranceDetails.setTextColor(activity.getResources().getColor(R.color.basic_dark));

    mTvImgPassengersInformation.setTextColor(activity.getResources().getColor(R.color.basic_dark));
    mTvPassengersInformation.setTextColor(activity.getResources().getColor(R.color.basic_dark));

    mTvImgBillingInformation.setTextColor(activity.getResources().getColor(R.color.basic_dark));
    mTvBillingInformation.setTextColor(activity.getResources().getColor(R.color.basic_dark));
  }

  /**
   * Set icon font
   */
  private void setFontAndImage() {
    mTvImgBookingReference.setTypeface(mTypeface);
    mTvImgBookingReference.setText(FontHelper.ICON_MY_TRIP_DETAILS_BOOKING_REFERENCE);

    mTvImgBillingInformation.setTypeface(mTypeface);
    mTvImgBillingInformation.setText(FontHelper.ICON_MY_TRIP_DETAILS_BILLING);

    mTvImgInsuranceDetails.setTypeface(mTypeface);
    mTvImgInsuranceDetails.setText(FontHelper.ICON_MY_TRIP_DETAILS_INSURANCES);

    mTvImgPassengersInformation.setTypeface(mTypeface);
    mTvImgPassengersInformation.setText(FontHelper.ICON_MY_TRIP_DETAILS_PASSENGERS);

    mTvFoldUnfoldBooking.setTypeface(mTypeface);
    mTvFoldUnfoldBooking.setText(FontHelper.ICON_MY_TRIP_DETAILS_UNFOLD);

    mTvFoldUnfoldPassengers.setTypeface(mTypeface);
    mTvFoldUnfoldPassengers.setText(FontHelper.ICON_MY_TRIP_DETAILS_UNFOLD);

    mTvFoldUnfoldInsurance.setTypeface(mTypeface);
    mTvFoldUnfoldInsurance.setText(FontHelper.ICON_MY_TRIP_DETAILS_UNFOLD);

    mTvFoldUnfoldBilling.setTypeface(mTypeface);
    mTvFoldUnfoldBilling.setText(FontHelper.ICON_MY_TRIP_DETAILS_UNFOLD);
  }

  /**
   * Set all listeners of the collapse list
   */
  private void setListeners() {

    mLlInsuranceDetails.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {

        tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS_TRIP_INFO,
            TrackerConstants.ACTION_TRIP_DETAILS, TrackerConstants.LABEL_OPEN_INSURANCE_INFO);

        if (!booking.getInsurances().isEmpty()) {

          if (mLlContainerInsurance.getVisibility() == View.VISIBLE) {
            hideAllContainers();
          } else {
            hideAllContainers();
            mLlContainerInsurance.setVisibility(View.VISIBLE);
            mTvFoldUnfoldInsurance.setText(FontHelper.ICON_MY_TRIP_DETAILS_FOLD);

            //Change Color
            mTvImgInsuranceDetails.setTextColor(
                activity.getResources().getColor(R.color.semantic_information));
            mTvInsuranceDetails.setTextColor(
                activity.getResources().getColor(R.color.semantic_information));
          }
        }
      }
    });

    mLlBookingReference.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {

        tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS_TRIP_INFO,
            TrackerConstants.ACTION_TRIP_DETAILS, TrackerConstants.LABEL_OPEN_BOOKING_INFO);

        if (mLlContainerBooking.getChildCount() > 0) {

          if (mLlContainerBooking.getVisibility() == View.VISIBLE) {
            hideAllContainers();
          } else {
            hideAllContainers();
            mLlContainerBooking.setVisibility(View.VISIBLE);
            mTvFoldUnfoldBooking.setText(FontHelper.ICON_MY_TRIP_DETAILS_FOLD);

            //Change Color
            mTvImgBookingReference.setTextColor(
                activity.getResources().getColor(R.color.semantic_information));
            mTvBookingReference.setTextColor(
                activity.getResources().getColor(R.color.semantic_information));
          }
        }
      }
    });

    mLlPassengersInformation.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {

        tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS_TRIP_INFO,
            TrackerConstants.ACTION_TRIP_DETAILS, TrackerConstants.LABEL_OPEN_PASSENGER_INFO);

        if (mLlContainerPassengers.getChildCount() > 0) {

          if (mLlContainerPassengers.getVisibility() == View.VISIBLE) {
            hideAllContainers();
          } else {
            hideAllContainers();
            mLlContainerPassengers.setVisibility(View.VISIBLE);
            mTvFoldUnfoldPassengers.setText(FontHelper.ICON_MY_TRIP_DETAILS_FOLD);

            //Change Color
            mTvImgPassengersInformation.setTextColor(
                activity.getResources().getColor(R.color.semantic_information));
            mTvPassengersInformation.setTextColor(
                activity.getResources().getColor(R.color.semantic_information));
          }
        }
      }
    });

    mLlBillingInformation.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {

        tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS_TRIP_INFO,
            TrackerConstants.ACTION_TRIP_DETAILS, TrackerConstants.LABEL_OPEN_BILLING_INFO);

        if (mLlContainerBilling.getChildCount() > 0) {

          if (mLlContainerBilling.getVisibility() == View.VISIBLE) {
            hideAllContainers();
          } else {
            hideAllContainers();
            mLlContainerBilling.setVisibility(View.VISIBLE);
            mTvFoldUnfoldBilling.setText(FontHelper.ICON_MY_TRIP_DETAILS_FOLD);

            //Change Color
            mTvImgBillingInformation.setTextColor(
                activity.getResources().getColor(R.color.semantic_information));
            mTvBillingInformation.setTextColor(
                activity.getResources().getColor(R.color.semantic_information));
          }
        }
      }
    });
  }

  /**
   * Set all components of details of your trips
   */
  private void setDetailsOfYourTrips() {
    setBookingReferences();
    setBillingDetails();
    setPassengerInformation();
    setInsuranceDetails();
  }

  /**
   * Set insurace details item
   */
  private void setInsuranceDetails() {
    ArrayList<String> descriptionItems;
    String conditionsUrl;

    //TODO CHANGE THIS BECAUSE I DON'T KNOW IF THERE MAY BE MORE THAN INSURANCE
    if (!booking.getInsurances().isEmpty()) {

      if (booking.getInsurances().get(0).getConditionURLSecundary() != null) {
        conditionsUrl = booking.getInsurances().get(0).getConditionURLSecundary();
      } else {
        conditionsUrl = booking.getInsurances().get(0).getConditionURLPrimary();
      }

      descriptionItems =
          parseDescriptionsItems(booking.getInsurances().get(0).getInsuranceDescription());

      double price = booking.getInsurances().get(0).getTotal();

      double pricePerPassenger =
          booking.getInsurances().get(0).getTotal() / booking.getTravellers().size();

      addInsurance(price, pricePerPassenger, descriptionItems, conditionsUrl);
    } else {
      mLlInsuranceDetails.setVisibility(View.GONE);
    }
  }

  /**
   * Set passenger information item
   */
  private void setPassengerInformation() {
    ArrayList<Baggage> baggageListSimple = new ArrayList<>();
    ArrayList<Baggage> baggageListRoundTrip = new ArrayList<>();

    for (int i = 0; i < booking.getTravellers().size(); i++) {
      addPassenger(booking.getTravellers().get(i).getTravellerType(),
          booking.getTravellers().get(i).getName() + " " + booking.getTravellers()
              .get(i)
              .getLastname());

      for (int x = 0; x < booking.getTravellers().get(i).getBaggageList().size(); x++) {

        //Get if baggage is a go and return
        int index = isBaggageRoundTrip(baggageListSimple,
            booking.getTravellers().get(i).getBaggageList().get(x));

        //If not exist add in baggage list, else remove of the simple list and add to the go and return list.
        if (index == -1) {
          baggageListSimple.add(booking.getTravellers().get(i).getBaggageList().get(x));
        } else {
          baggageListRoundTrip.add(baggageListSimple.get(index));
          baggageListSimple.remove(index);
        }
      }

      String bags;
      //Add the baggage to the view
      for (Baggage baggage : baggageListRoundTrip) {
        if (baggage.getPieces() > 0) {
          String[] cities = getCityNames(baggage.getSegment());
          bags = localizableProvider.getString(
              OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_BAGGAGE_INCLUDED_EACH_WAY,
              baggage.getPieces() + "");
          addBaggage(cities[0], cities[1], bags, true);
        }
      }

      for (Baggage baggage : baggageListSimple) {
        if (baggage.getPieces() > 0) {
          String[] cities = getCityNames(baggage.getSegment());
          bags =
              localizableProvider.getString(OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_BAGGAGE_INCLUDED,
                  baggage.getPieces() + "");
          addBaggage(cities[0], cities[1], bags, false);
        }
      }

      baggageListSimple.clear();
      baggageListRoundTrip.clear();
    }
  }

  /**
   * Set billing details item
   */
  private void setBillingDetails() {
    addBillingBuyer(booking.getBuyer().getName() + " " + booking.getBuyer().getLastname());
    addBillingEmail(booking.getBuyer().getEmail());

    addBillingPrice(localeHelper.getLocalizedCurrencyValue(booking.getPrice()));
  }

  /**
   * Set booking references item
   */
  private void setBookingReferences() {
    String bookingId = String.valueOf(booking.getBookingId());
    String pnrList = "";

    for (int i = 0; i < booking.getSegments().size(); i++) {
      for (int x = 0; x < booking.getSegments().get(i).getSectionsList().size(); x++) {
        String pnr =
            booking.getSegments().get(i).getSectionsList().get(x).getItineraryBooking().getPnr();

        if (pnr != null) {
          if (pnrList.isEmpty()) {
            pnrList += pnr;
          } else if (!pnrList.contains(pnr)) {
            pnrList += " | " + pnr;
          }
        }
      }
    }

    addBooking(bookingId, pnrList);
  }

  //#############
  //  INSURANCE
  //#############

  /**
   * Add insurance into Insurance details item
   *
   * @param price Price
   * @param pricePerPassenger Price per passenger
   * @param descriptionItems Description items list
   * @param conditionsUrl Insurance conditions url
   */
  private void addInsurance(double price, double pricePerPassenger,
      ArrayList<String> descriptionItems, final String conditionsUrl) {

    mTvInsurancePrice.setText(localeHelper.getLocalizedCurrencyValue(price));
    mTvInsurancePricePerPassenger.setText(
        String.format("%s/%s", localeHelper.getLocalizedCurrencyValue(pricePerPassenger),
            localizableProvider.getString(OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_PASSENGERS)));

    for (int i = 0; i < descriptionItems.size(); i++) {
      mLlInsuranceDescription.addView(provideInsuranceDescriptionItem(descriptionItems.get(i)));
    }

    mBtnSeeConditions.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(conditionsUrl));
        activity.startActivity(browserIntent);
      }
    });
  }

  //###########
  //  BILLING
  //###########

  /**
   * Add buyer name into billing item
   *
   * @param buyer Buyer name
   */
  private void addBillingBuyer(String buyer) {
    mLlContainerBilling.addView(provideDetailItem(
        localizableProvider.getString(OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_BUYER), buyer,
        FontHelper.ICON_MY_TRIP_DETAILS_ADULT));
  }

  /**
   * Add buyer email into billing item
   *
   * @param email email
   */
  private void addBillingEmail(String email) {
    mLlContainerBilling.addView(provideDetailItem(
        localizableProvider.getString(OneCMSKeys.MY_TRIPS_DETAILS_TRIP_EMAIL_ADDRESS), email,
        FontHelper.ICON_MY_TRIP_DETAILS_MAIL));
  }

  /**
   * Add paymet method into billing item
   *
   * @param method method
   */
  private void addBillingPayment(String method) {
    mLlContainerBilling.addView(provideDetailItem(
        localizableProvider.getString(OneCMSKeys.MY_TRIPS_DETAILS_TRIP_PAYMENT_METHOD), method,
        FontHelper.ICON_MY_TRIP_DETAILS_CREDIT_CARD));
  }

  /**
   * Add price into billing item
   *
   * @param price price
   */
  private void addBillingPrice(String price) {
    mLlContainerBilling.addView(provideDetailItem(
        localizableProvider.getString(OneCMSKeys.MY_TRIPS_DETAILS_TRIP_TOTAL_PRICE), price,
        FontHelper.ICON_MY_TRIP_DETAILS_TOTAL_PRICE));
  }

  //#############
  //  PASSEGNER
  //#############

  /**
   * Add passenger info into passenger details
   *
   * @param type Passenger type
   * @param name Passenger name
   */
  private void addPassenger(String type, String name) {
    String passengerType = "";
    if (type != null) {
      if (type.equalsIgnoreCase(Traveller.TRAVELLER_TYPE_ADULT)) {
        passengerType = localizableProvider.getString(OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_ADULT);
      } else if (type.equalsIgnoreCase(Traveller.TRAVELLER_TYPE_CHILD)) {
        passengerType = localizableProvider.getString(OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_CHILD);
      } else if (type.equalsIgnoreCase(Traveller.TRAVELLER_TYPE_INFANT)) {
        passengerType = localizableProvider.getString(OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_BABY);
      }
    }
    mLlContainerPassengers.addView(
        provideDetailItem(passengerType, name, FontHelper.ICON_MY_TRIP_DETAILS_PASSENGERS));
  }

  /**
   * Add passenger baggage into passenger details
   *
   * @param from City name from which the baggage
   * @param to City name where to go the baggage
   * @param baggage Baggage
   * @param isRoundTrip If the travel is a roundtrip type
   */
  private void addBaggage(String from, String to, String baggage, boolean isRoundTrip) {
    mLlContainerPassengers.addView(provideDetailItem(String.format("%s %s %s", from,
        (isRoundTrip) ? FontHelper.ICON_MY_TRIP_DETAILS_ARROW_DOUBLE
            : FontHelper.ICON_MY_TRIP_DETAILS_ARROW_SIMPLE, to), baggage,
        FontHelper.ICON_MY_TRIP_DETAILS_BAGGAGE_OK));
  }

  //############
  //  BOOKING
  //############

  /**
   * Add booking information
   *
   * @param bookingId Booking id
   * @param pnrList Flight's pnr
   */
  private void addBooking(String bookingId, String pnrList) {
    mLlContainerBooking.addView(provideDetailItem(
        localizableProvider.getString(OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_BRAND_BOOKING_REFERENCE,
            Configuration.getInstance().getBrandVisualName()), bookingId,
        FontHelper.ICON_MY_TRIP_DETAILS_BRAND_BOOKING_REFERENCE));
    mLlContainerBooking.addView(provideDetailItem(
        localizableProvider.getString(OneCMSKeys.MY_TRIPS_DETAILS_TRIP_CARD_AIRLINE_REFERENTE),
        pnrList, FontHelper.ICON_MY_TRIP_DETAILS_BOOKING_REFERENCE));
  }

  //####################
  //  INFLATE PROVIDER
  //####################

  /**
   * Get detail item view built
   *
   * @param title Title of the item
   * @param subtitle Subtitle of the item
   * @param icon Icon of the item
   * @return View built
   */
  private View provideDetailItem(String title, String subtitle, String icon) {
    View view = activity.getLayoutInflater().inflate(R.layout.details_of_your_trips_item, null);

    TextView tvIcon = (TextView) view.findViewById(R.id.tvImgIcon);
    TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
    TextView tvSubtitle = (TextView) view.findViewById(R.id.tvSubtitle);

    tvIcon.setTypeface(mTypeface);

    tvTitle.setText(title);
    tvSubtitle.setText(subtitle);
    tvIcon.setText(icon);

    return view;
  }

  /**
   * Get insurance description item view built
   *
   * @param descriptionItem Description item
   * @return View built
   */
  private View provideInsuranceDescriptionItem(String descriptionItem) {
    View view = activity.getLayoutInflater()
        .inflate(R.layout.my_trip_details_view_insurance_description_item, null);

    TextView tvMark = (TextView) view.findViewById(R.id.tvMark);
    TextView tvDescriptionItem = (TextView) view.findViewById(R.id.tvDescriptionItem);

    tvMark.setTypeface(mTypeface);
    tvMark.setText(FontHelper.ICON_MY_TRIP_DETAILS_TICK);

    tvDescriptionItem.setText(descriptionItem);

    return view;
  }

  //#########
  //  TOOLS
  //#########

  /**
   * Check if the baggage is a round trip type
   *
   * @param baggageList Baggage list
   * @param baggage Baggage
   * @return If the baggage is a round trip type or not
   */
  private int isBaggageRoundTrip(ArrayList<Baggage> baggageList, Baggage baggage) {

    for (int i = 0; i < baggageList.size(); i++) {

      //GeoNode of the baggage item
      long fromGeoNodeId =
          baggageList.get(i).getSegment().getSectionsList().get(0).getFrom().getGeoNodeId();
      long toGeoNodeId = baggageList.get(i)
          .getSegment()
          .getSectionsList()
          .get(baggageList.get(i).getSegment().getSectionsList().size() - 1)
          .getTo()
          .getGeoNodeId();

      //GeoNode of the baggage
      long fromGeoNodeIdOfBaggage =
          baggage.getSegment().getSectionsList().get(0).getFrom().getGeoNodeId();
      long toGeoNodeIdOfBaggage;
      if (baggage.getSegment().getSectionsList().size() >= baggageList.get(i)
          .getSegment()
          .getSectionsList()
          .size()) {
        toGeoNodeIdOfBaggage = baggage.getSegment()
            .getSectionsList()
            .get(baggageList.get(i).getSegment().getSectionsList().size() - 1)
            .getTo()
            .getGeoNodeId();
      } else {
        toGeoNodeIdOfBaggage = baggage.getSegment().getSectionsList().get(0).getTo().getGeoNodeId();
      }

      //Check if exist
      if (fromGeoNodeId == toGeoNodeIdOfBaggage
          && fromGeoNodeIdOfBaggage == toGeoNodeId
          && baggage.getWeight() == baggageList.get(i).getWeight()) {

        return i;
      }
    }

    return -1;
  }

  /**
   * Return a city names of the segment
   *
   * @param segment Segment to check city names
   * @return city names
   */
  private String[] getCityNames(Segment segment) {
    String cityFrom = segment.getSectionsList().get(0).getFrom().getCityName();
    String cityTo =
        segment.getSectionsList().get(segment.getSectionsList().size() - 1).getTo().getCityName();

    String[] cities = { cityFrom, cityTo };
    return cities;
  }

  /**
   * Parse description items
   *
   * @param description Description
   * @return ArraList with all description items
   */
  private ArrayList<String> parseDescriptionsItems(String description) {
    int subStringDescriptionStart = 3;

    ArrayList<String> insuranceDescriptionItems = new ArrayList<String>();
    //        //If the textTitle is null then show the policy as the textTitle
    //        if (title == null || title.isEmpty()) {
    //            title = policy;
    //        }

    if (description != null && !description.isEmpty()) {
      String[] rowsDescription = description.split("\n");

      for (String itemDescription : rowsDescription) {
        String newDescription = itemDescription.trim();

        if (newDescription.startsWith("{b ")) {
          newDescription =
              newDescription.substring(subStringDescriptionStart, itemDescription.length() - 1);
          if (newDescription.contains("}")) {
            newDescription = newDescription.replace("}", "");
          }
          newDescription =
              newDescription.substring(0, 1).toUpperCase() + newDescription.substring(1);

          //Remove break lines and spaces
          newDescription = newDescription.replaceAll("\n", "");
          newDescription = newDescription.trim();

          if (!("").equals(newDescription)) {
            insuranceDescriptionItems.add(newDescription);
          }
        } else {
          //Remove break lines and spaces
          newDescription = newDescription.replaceAll("\n", "");
          newDescription = newDescription.trim();

          if (!("").equals(newDescription)) {
            insuranceDescriptionItems.add(newDescription);
          }
        }
      }
    }

    return insuranceDescriptionItems;
  }

  private void initOneCMSText() {
    mTvDetailsTitle.setText(localizableProvider.getString(MY_TRIPS_DETAILS_TRIP_CARD_TITLE));
    mTvBookingReference.setText(
        localizableProvider.getString(MY_TRIPS_DETAILS_TRIP_CARD_BOOKING_REFERENCE));
    mTvPassengersInformation.setText(
        localizableProvider.getString(MY_TRIPS_DETAILS_TRIP_CARD_PASSENGERS_INFORMATION));
    mTvInsuranceDetails.setText(
        localizableProvider.getString(MY_TRIPS_DETAILS_TRIP_CARD_INSURANCE_DETAILS));
    mTvBillingInformation.setText(
        localizableProvider.getString(MY_TRIPS_DETAILS_TRIP_CARD_BILLING_INFORMATION));
    mTvFlightCancelationTitle.setText(
        localizableProvider.getString(MY_TRIPS_DETAILS_TRIP_CARD_FLIGHT_CANCELATION));
    mBtnSeeConditions.setText(
        localizableProvider.getString(MY_TRIPS_DETAILS_TRIP_CARD_BTN_CONDITIONS));
  }
}
