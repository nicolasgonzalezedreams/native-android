package com.odigeo.app.android.lib.models;

import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import com.odigeo.app.android.lib.models.dto.CollectionMethodDTO;
import com.odigeo.app.android.lib.models.dto.CollectionMethodTypeDTO;
import com.odigeo.data.entity.BaseSpinnerItem;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by ManuelOrtiz on 16/10/2014.
 */
public class CollectionMethodWithPrice implements Serializable, BaseSpinnerItem {
  protected Integer collectionMethodKey;
  protected CollectionMethodDTO collectionMethod;
  private BigDecimal price;
  private boolean cheapest;

  @DrawableRes private int imageId;

  public final Integer getCollectionMethodKey() {
    return collectionMethodKey;
  }

  public final void setCollectionMethodKey(Integer collectionMethodKey) {
    this.collectionMethodKey = collectionMethodKey;
  }

  public final CollectionMethodDTO getCollectionMethod() {
    return collectionMethod;
  }

  public final void setCollectionMethod(CollectionMethodDTO collectionMethod) {
    this.collectionMethod = collectionMethod;
  }

  public final BigDecimal getPrice() {
    return price;
  }

  public final void setPrice(BigDecimal price) {
    this.price = price;
  }

  public final boolean isCheapest() {
    return cheapest;
  }

  public final void setCheapest(boolean cheapest) {
    this.cheapest = cheapest;
  }

  @Override public final boolean equals(Object object) {
    if (!(object instanceof CollectionMethodWithPrice)) {
      return false;
    }

    return getCollectionMethod().equals(((CollectionMethodWithPrice) object).getCollectionMethod());
  }

  @Override public String getShownTextKey() {
    return EMPTY_STRING;
  }

  @Override public int getImageId() {
    return imageId;
  }

  public void setImageId(@DrawableRes int imageId) {
    this.imageId = imageId;
  }

  @Nullable @Override public String getShownText() {
    if (collectionMethod.getType() == CollectionMethodTypeDTO.CREDITCARD) {
      return collectionMethod.getCreditCardType().getName();
    } else {
      return collectionMethod.getType().value();
    }
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public Object getData() {
    return null;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }
}
