package com.odigeo.app.android.lib.ui.widgets.base;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.Util;

/**
 * Created by Irving Lóp on 19/09/2014.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 2.1
 * @since 12/02/2015
 */
public class TwoTextsDetailsBreakdown extends RelativeLayout {
  private static final int DEFAULT_TEXT_SIZE = R.dimen.size_font_L;
  private static final int INCREMENT_TEXT_SIZE = 2;
  private static final int TYPE_FONT = 9;
  private final String locale;
  private final int typeFont;
  private String description;
  private String keyDescription;
  private double price;
  private int textSizeResource;
  private TextView textViewPrice;
  private TextView textViewDescription;

  public TwoTextsDetailsBreakdown(Context context, AttributeSet attrs) {
    super(context, attrs);

    // get design time params
    TypedArray typedArray =
        context.obtainStyledAttributes(attrs, R.styleable.TwoTextsDetailsBreakdown, 0, 0);
    keyDescription = typedArray.getString(R.styleable.TwoTextsDetailsBreakdown_keyDescription);
    price = typedArray.getFloat(R.styleable.TwoTextsDetailsBreakdown_price, 0);
    locale = LocaleUtils.localeToString(Configuration.getCurrentLocale());
    textSizeResource =
        typedArray.getResourceId(R.styleable.TwoTextsDetailsBreakdown_textSizeResourceDes,
            DEFAULT_TEXT_SIZE);
    typeFont = typedArray.getInt(R.styleable.TwoTextsDetailsBreakdown_typeface, 0);
    typedArray.recycle();
    inflateLayout();
    setResourceTextSize(textSizeResource);
    textViewPrice.setTextColor(getResources().getColor(R.color.primary_brand));
  }

  public TwoTextsDetailsBreakdown(Context context, String description, double price,
      String locale) {
    super(context);
    this.description = description;
    this.price = price;
    this.locale = locale;
    typeFont = TYPE_FONT;

    inflateLayoutWithText();
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.breakdown_details_two_texts, this);
    textViewDescription = (TextView) findViewById(R.id.text_description_detailsTwoTexts);
    textViewPrice = (TextView) findViewById(R.id.text_price_detailsTwoTexts);
    setPrice(price, locale);
    if (keyDescription != null) {
      textViewDescription.setText(LocalizablesFacade.getString(getContext(), keyDescription));
    }
    textViewDescription.setTypeface(Configuration.getInstance().getFonts().get(typeFont));
    textViewPrice.setTypeface(Configuration.getInstance().getFonts().get(typeFont));
  }

  private void inflateLayoutWithText() {
    inflateLayout();
    textViewDescription.setText(description);
  }

  public final void setResourceTextSize(int sizeResource) {
    textSizeResource = sizeResource;
    textViewPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX,
        getResources().getDimensionPixelOffset(textSizeResource) * INCREMENT_TEXT_SIZE);
  }

  /**
   * Set the price value with the currency for locale passed in format "en_US".
   *
   * @param priceD Price to be displayed.
   */
  public final void setPrice(double priceD) {
    this.price = priceD;
    textViewPrice.setText(LocaleUtils.getLocalizedCurrencyValue(price, locale));
  }

  /**
   * Set the price value with the currency for locale passed in format "en_US".
   *
   * @param priceD Price to be displayed.
   * @param locale Locale where be extracted the currency.
   */
  public final void setPrice(double priceD, String locale) {
    this.price = priceD;
    textViewPrice.setText(LocaleUtils.getLocalizedCurrencyValue(price, locale));
  }

  /**
   * Set the price value with the currency for locale passed in format "en_US", and with special
   * style.
   *
   * @param priceD Price to be displayed.
   * @param locale Locale where be extracted the currency.
   */
  public final void setPricePrincipal(double priceD, String locale) {
    this.price = priceD;
    String priceFormatted = LocaleUtils.getLocalizedCurrencyValue(price, locale);
    textViewPrice.setText(Util.getTextStylePrice(priceFormatted));
  }
}
