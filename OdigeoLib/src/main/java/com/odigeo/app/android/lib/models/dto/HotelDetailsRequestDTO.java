package com.odigeo.app.android.lib.models.dto;

/**
 * @author miguel
 */
public class HotelDetailsRequestDTO {

  protected long searchId;
  protected String accommodationDealKey;

  /**
   * @return the searchId
   */
  public long getSearchId() {
    return searchId;
  }

  /**
   * @param searchId the searchId to set
   */
  public void setSearchId(long searchId) {
    this.searchId = searchId;
  }

  /**
   * @return the accommodationDealKey
   */
  public String getAccommodationDealKey() {
    return accommodationDealKey;
  }

  /**
   * @param accommodationDealKey the accommodationDealKey to set
   */
  public void setAccommodationDealKey(String accommodationDealKey) {
    this.accommodationDealKey = accommodationDealKey;
  }
}
