package com.odigeo.app.android.lib.models;

import com.odigeo.app.android.lib.models.dto.BaggageIncludedDTO;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;
import com.odigeo.app.android.lib.models.dto.FareItineraryPriceCalculatorDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by manuel on 09/10/14.
 */
public class SegmentGroup implements Serializable {
  private List<SegmentWrapper> segmentWrappers;
  private FareItineraryPriceCalculatorDTO price;
  private transient FareItineraryDTO itineraryResultParent;
  private int segmentGroupIndex;
  private CarrierDTO carrier;
  private BaggageIncludedDTO baggageIncluded;

  public SegmentGroup() {
    setSegmentWrappers(new ArrayList<SegmentWrapper>());
  }

  public List<SegmentWrapper> getSegmentWrappers() {
    return segmentWrappers;
  }

  public final void setSegmentWrappers(List<SegmentWrapper> segmentWrappers) {
    this.segmentWrappers = segmentWrappers;
  }

  public FareItineraryPriceCalculatorDTO getPrice() {
    return price;
  }

  public void setPrice(FareItineraryPriceCalculatorDTO price) {
    this.price = price;
  }

  public FareItineraryDTO getItineraryResultParent() {
    return itineraryResultParent;
  }

  public void setItineraryResultParent(FareItineraryDTO itineraryResultParent) {
    this.itineraryResultParent = itineraryResultParent;
  }

  public int getSegmentGroupIndex() {
    return segmentGroupIndex;
  }

  public void setSegmentGroupIndex(int segmentGroupIndex) {
    this.segmentGroupIndex = segmentGroupIndex;
  }

  public CarrierDTO getCarrier() {
    return carrier;
  }

  public void setCarrier(CarrierDTO carrierObject) {
    this.carrier = carrierObject;
  }

  public BaggageIncludedDTO getBaggageIncluded() {
    return baggageIncluded;
  }

  public void setBaggageIncluded(BaggageIncludedDTO baggageIncluded) {
    this.baggageIncluded = baggageIncluded;
  }
}
