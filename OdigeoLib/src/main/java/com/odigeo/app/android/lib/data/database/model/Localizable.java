package com.odigeo.app.android.lib.data.database.model;

/**
 * Localizable object.
 *
 * @author Cristian Fanjul
 * @since 11/05/2015
 */
public class Localizable {
  private String key;
  private String value;

  public Localizable() {
  }

  public Localizable(String key, String value) {
    this.key = key;
    this.value = value;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
