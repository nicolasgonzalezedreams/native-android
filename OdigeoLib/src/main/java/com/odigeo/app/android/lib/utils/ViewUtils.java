package com.odigeo.app.android.lib.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.Html;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageRequest;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ViewUtils {

  public static final String ELEMENT_TO_REPLACE = "%s";
  private static final float ROUNDED_CORNERS_RADIUS = 0.04f;

  private ViewUtils() {

  }

  /**
   * Method to obtain the Resource Id from a Resource, by the resources name
   *
   * @param context The context of the activity
   * @param pVariableName The name of the Resource
   * @param pResourceType The resource name
   * @param pPackageName The package of the resource
   * @return The Resource Id
   */
  public static int getResourceIdByName(Context context, String pVariableName, String pResourceType,
      String pPackageName) {
    try {
      return context.getResources().getIdentifier(pVariableName, pResourceType, pPackageName);
    } catch (Exception e) {
      Logger.getLogger(ViewUtils.class.getName()).log(Level.SEVERE, Constants.TAG_LOG, e);
      return -1;
    }
  }

  /**
   * Method to obtain the Resource Text from a Resource, given its name
   *
   * @param context The context of the activity
   * @param pVariableName The name of the Resource
   * @param pPackageName The package of the resource
   * @return The Resource drawable
   */
  public static Drawable getDrawableResourceByName(Context context, String pVariableName,
      String pPackageName) {
    int idResource = getResourceIdByName(context, pVariableName, "drawable", pPackageName);

    if (idResource == 0) {
      Log.i("ODIGEO APP", "The Resource id is "
          + idResource
          + " NOT FOUND for "
          + pVariableName
          + ", "
          + pPackageName);
      return null;
    }

    return context.getResources().getDrawable(idResource);
  }

  /**
   * This method adds padding on top of the view
   *
   * @param view View where the padding is added
   * @param context Application context
   */
  public static void setSeparator(View view, Context context) {
    // add Divider between each widget
    int paddingTop =
        context.getResources().getDimensionPixelSize(R.dimen.separator_general_widgets);
    view.setPadding(0, paddingTop, 0, 0);
  }

  /**
   * This method rounds the corners of a bitmap
   *
   * @param sourceBitmap Bitmap to be edited
   */
  public static Bitmap setRoundedTopCornersToBitMap(Bitmap sourceBitmap, View view) {
    if (view != null && sourceBitmap != null) {
      return setRoundedTopCornersToBitMap(sourceBitmap, view, ROUNDED_CORNERS_RADIUS);
    }
    return null;
  }

  /**
   * This method rounds the corners of a bitmap with a specific radius
   *
   * @param sourceBitmap Bitmap to be edited
   * @param view ImageView where the bitmap will be
   */
  public static Bitmap setRoundedTopCornersToBitMap(Bitmap sourceBitmap, View view, float radius) {
    float roundedCorners = RoundedCornersDrawable.getDpFromDpi(radius);

    // Resize the image to center crop in the imageView
    Bitmap resizedProfileBitmap =
        RoundedCornersDrawable.resizeImageToImageView(sourceBitmap, view.getWidth(),
            view.getHeight());

    // Add rounded top corners to the image
    return RoundedCornersDrawable.getRoundedCornerBitmap(resizedProfileBitmap, roundedCorners, true,
        true, false, false);
  }

  /**
   * This method sets a bitmap to grayscale
   *
   * @param bmpOriginal Bitmap to be edited
   * @return Edited bitmap
   */
  public static Bitmap toGrayscale(Bitmap bmpOriginal) {
    int width, height;
    if (bmpOriginal == null) {
      return null;
    }
    height = bmpOriginal.getHeight();
    width = bmpOriginal.getWidth();

    Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
    Canvas c = new Canvas(bmpGrayscale);
    Paint paint = new Paint();
    ColorMatrix cm = new ColorMatrix();
    cm.setSaturation(0);
    ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
    paint.setColorFilter(f);
    c.drawBitmap(bmpOriginal, 0, 0, paint);
    return bmpGrayscale;
  }

  /**
   * This method launches an animation
   *
   * @param view View to be animated
   * @param idAnimation Animation to be used
   */
  public static void loadAnimate(View view, int idAnimation) {

    if (view != null) {
      view.setVisibility(View.VISIBLE);
      view.startAnimation(AnimationUtils.loadAnimation(view.getContext(), idAnimation));
    }
  }

  /**
   * Uses the {@link android.view.inputmethod.InputMethodManager} system service to hide the soft
   * keyboard from the screen
   *
   * @param activity The current activity
   */
  public static void hideKeyboard(Activity activity) {
    if (activity == null) {
      return;
    }
    InputMethodManager imm =
        (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
    imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
  }

  /**
   * Uses the {@link android.view.inputmethod.InputMethodManager} system service to hide the soft
   * keyboard from the screen
   *
   * @param context The current context
   * @param view The main context view to extract the window token from as of: ({@link
   * android.view.View#getWindowToken()}
   */
  public static void hideKeyboard(Context context, View view) {
    if (context == null || view == null) {
      return;
    }
    InputMethodManager imm =
        (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
  }

  public static String getSearchImage(Context context, String iata) {
    return context.getString(R.string.booking_image_path) + String.format(
        context.getString(R.string.booking_image_filename), iata);
  }

  public static void setBackground(Context context, View view, Bitmap bitmap, int color,
      boolean roundCorners) {
    RoundedBitmapDrawable roundedBitmapDrawable =
        RoundedBitmapDrawableFactory.create(context.getResources(), bitmap);
    if (roundCorners) {
      float roundPx = (float) bitmap.getWidth() * ROUNDED_CORNERS_RADIUS;
      roundedBitmapDrawable.setCornerRadius(roundPx);
    }
    roundedBitmapDrawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
    view.setBackgroundDrawable(roundedBitmapDrawable);
  }

  public static ImageRequest filterRemoteImage(final Context context, final View view, String url,
      final int color, final boolean roundCorners) {
    return new ImageRequest(url, new Response.Listener<Bitmap>() {

      @Override public void onResponse(Bitmap bitmap) {
        setBackground(context, view, bitmap, color, roundCorners);
      }
    }, view.getWidth(), view.getHeight(), null, null);
  }

  public static Spannable paintStringTag(@NonNull String text, @NonNull int colorId,
      @NonNull Context context, @Nullable String... params) {

    int color = ContextCompat.getColor(context, colorId);

    Pattern pattern = Pattern.compile(">(.*?)</");
    Matcher matcher = pattern.matcher(text);

    String textExtracted = "";
    Spannable formattedText;
    int start = 0;
    int end = 0;

    if ((text.contains(ELEMENT_TO_REPLACE)) && (params != null)) {
      text = String.format(text, (Object[]) params);
    }

    if (matcher.find()) {
      textExtracted = matcher.group(1);

      formattedText = (Spannable) Html.fromHtml(text);

      Pattern patternTextExtracted = Pattern.compile(textExtracted);
      Matcher matcherTextExtracted = patternTextExtracted.matcher(formattedText);

      if (matcherTextExtracted.find()) {
        start = matcherTextExtracted.start(0);
        end = matcherTextExtracted.end(0);
      }
    } else {
      formattedText = (Spannable) Html.fromHtml(text);
    }

    formattedText.setSpan(new ForegroundColorSpan(color), start, end,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

    return formattedText;
  }

  public static void tintBackground(View view, int colorRes) {
    view.getBackground()
        .setColorFilter(ContextCompat.getColor(view.getContext(), colorRes),
            PorterDuff.Mode.SRC_ATOP);
  }

  public static void tintImageViewSrc(ImageView imageView, int colorRes) {
    Drawable drawable = DrawableCompat.wrap(imageView.getDrawable());
    DrawableCompat.setTint(drawable.mutate(),
        ContextCompat.getColor(imageView.getContext(), colorRes));
  }

  public static String getCarrierLogo(String carrierCode) {
    return String.format("%s%s.png",
        Configuration.getInstance().getImagesSources().getUrlAirlineLogos(), carrierCode);
  }

  @SuppressWarnings("unchecked")
  public static <T extends View> T findById(@NonNull View view, @IdRes int idRes) {
    return (T) view.findViewById(idRes);
  }

  @SuppressWarnings("unchecked")
  public static <T extends View> T findById(@NonNull Activity activity, @IdRes int idRes) {
    return (T) activity.findViewById(idRes);
  }
}
