package com.odigeo.app.android.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.FacebookAlreadyRegisteredPresenter;
import com.odigeo.presenter.contracts.navigators.SocialLoginNavigatorInterface;
import com.odigeo.presenter.contracts.views.FacebookAlreadyRegisteredViewInterface;

public class FacebookAlreadyRegisteredView extends SocialLoginHelperView
    implements FacebookAlreadyRegisteredViewInterface {

  private static final String USER_EMAIL = "USER_EMAIL";
  private static final String PARENT_CLASS_NAME_STRING_KEY = "parentClass";
  private static final String PARENT_CLASS_REGISTER = "Register";
  private static final String PARENT_CLASS_LOGIN = "Login";
  private Button mButtonFacebookAlreadyRegistered;

  public FacebookAlreadyRegisteredView() {
  }

  public static FacebookAlreadyRegisteredView newInstance(String email) {
    FacebookAlreadyRegisteredView view = new FacebookAlreadyRegisteredView();
    Bundle args = new Bundle();
    args.putString(USER_EMAIL, email);
    view.setArguments(args);
    return view;
  }

  private String getEmail() {
    Bundle args = this.getArguments();
    String email = "";
    if (args != null) {
      email = args.getString(USER_EMAIL);
    }
    return email;
  }

  @Override public void onResume() {
    super.onResume();
    if (getArguments().getString(PARENT_CLASS_NAME_STRING_KEY, "").equals(PARENT_CLASS_REGISTER)) {
      mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_REGISTER_EMAIL_ALREADY_USED);
    } else if (getArguments().getString(PARENT_CLASS_NAME_STRING_KEY, "")
        .equals(PARENT_CLASS_LOGIN)) {
      mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_LOGIN_WRONG_TYPE);
    }
  }

  @Override protected FacebookAlreadyRegisteredPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideFacebookRegisteredPresenter(this, (SocialLoginNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_facebook_already_registered_view;
  }

  @Override protected void initComponent(View view) {
    mButtonFacebookAlreadyRegistered =
        (Button) view.findViewById(R.id.btnFacebookAlreadyRegistered);
    TextView tvUsername = (TextView) view.findViewById(R.id.txt_vw_restore_pass_paragraph1);
    tvUsername.setText(getEmail());
  }

  @Override protected void setListeners() {
    mButtonFacebookAlreadyRegistered.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        loginFacebook();
        if (getArguments().getString(PARENT_CLASS_NAME_STRING_KEY, "")
            .equals(PARENT_CLASS_REGISTER)) {
          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_SSO_REGISTER,
              TrackerConstants.ACTION_REGISTER_FORM,
              TrackerConstants.LABEL_SSO_EMAIL_ALREADY_USED_FACEBOOK_LOGIN);
        } else if (getArguments().getString(PARENT_CLASS_NAME_STRING_KEY, "")
            .equals(PARENT_CLASS_LOGIN)) {
          mTracker.trackAnalyticsEvent(getSSOTrackingCategory(), TrackerConstants.ACTION_SIGN_IN,
              TrackerConstants.LABEL_SSO_EMAIL_ALREADY_USED_FACEBOOK_LOGIN);
        }
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    TextView tvAlreadyRegistered =
        (TextView) view.findViewById(R.id.txt_vw_restore_pass_paragraph2);
    tvAlreadyRegistered.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SOO_REGISTERINFO_ALREADY_REGISTERED,
            FACEBOOK_SOURCE));
    mButtonFacebookAlreadyRegistered.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.SSO_REGISTERINFO_SIGNIN_FACEBOOK)
            .toString());
  }
}
