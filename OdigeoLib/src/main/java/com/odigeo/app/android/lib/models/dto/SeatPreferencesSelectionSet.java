package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.Set;

public class SeatPreferencesSelectionSet implements Serializable {
  private Set<AmbienceSeatPreferencesSelection> ambienceSeatPreferencesSelections;
  private Set<SeatZonePreferencesSelection> seatZonePreferencesSelections;

  public Set<AmbienceSeatPreferencesSelection> getAmbienceSeatPreferencesSelections() {
    return ambienceSeatPreferencesSelections;
  }

  public void setAmbienceSeatPreferencesSelections(
      Set<AmbienceSeatPreferencesSelection> ambienceSeatPreferencesSelections) {
    this.ambienceSeatPreferencesSelections = ambienceSeatPreferencesSelections;
  }

  public Set<SeatZonePreferencesSelection> getSeatZonePreferencesSelections() {
    return seatZonePreferencesSelections;
  }

  public void setSeatZonePreferencesSelections(
      Set<SeatZonePreferencesSelection> seatZonePreferencesSelections) {
    this.seatZonePreferencesSelections = seatZonePreferencesSelections;
  }
}
