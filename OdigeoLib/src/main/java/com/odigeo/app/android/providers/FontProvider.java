package com.odigeo.app.android.providers;

import android.content.Context;
import android.graphics.Typeface;

public class FontProvider {

  private final Context context;

  public FontProvider(Context context) {
    this.context = context;
  }

  public Typeface loadTypeFaceFromFontAsset(String path) {
    return Typeface.createFromAsset(context.getAssets(), path);
  }
}
