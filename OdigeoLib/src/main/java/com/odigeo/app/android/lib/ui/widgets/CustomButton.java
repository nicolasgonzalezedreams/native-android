package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Button;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.LocalizableBackground;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.data.TextForViewListener;

/**
 * This class should be used in replace of any button in order to add it a text that comes from the
 * database.
 *
 * @author Cristian Fanjul
 * @since 15/05/2015
 */
@Deprecated public class CustomButton extends Button implements TextForViewListener {

  private static final int REFERENCE_NOT_FOUND = -1;

  public CustomButton(Context context, AttributeSet attrs) {
    super(context, attrs);

    applyAttributes(context, attrs);
  }

  private void applyAttributes(Context context, AttributeSet attrs) {
    TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.TextViewWithFont);

    if (!isInEditMode()) {
      String odigeoDbKey = values.getString(R.styleable.OdigeoTextAttrs_odigeoText);
      int typefaceValue = values.getInt(R.styleable.TextViewWithFont_typeface, REFERENCE_NOT_FOUND);

      boolean isAsync = values.getBoolean(R.styleable.OdigeoTextAttrs_isAsync, false);
      if (odigeoDbKey != null) {
        if (isAsync) {
          new LocalizableBackground(getContext(), this,
              odigeoDbKey); // Complete text Asynchronously.
        } else {
          setText(LocalizablesFacade.getString(context, odigeoDbKey));
        }
      }
      if (typefaceValue != REFERENCE_NOT_FOUND) {
        setTypeface(Configuration.getInstance().getFonts().get(typefaceValue));
      }
    }
    values.recycle();
  }

  @Override public void setViewText(CharSequence text) {
    setText(text);
  }
}
