package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class SmsNotification implements Serializable {
  private String provider;
  private String description;

  public String getProvider() {
    return provider;
  }

  public void setProvider(String provider) {
    this.provider = provider;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
