package com.odigeo.app.android.lib.utils.events;

import android.location.Location;

/**
 * Created by manuel on 31/08/14.
 */
public class LocationChangedEvent {

  private Location location;

  public LocationChangedEvent(Location location) {
    this.location = location;
  }

  public final Location getLocation() {
    return location;
  }

  public final void setLocation(Location location) {
    this.location = location;
  }
}
