package com.odigeo.app.android.view;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.custom.AboutInfoItem;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.TermsAndConditionsPresenter;
import com.odigeo.presenter.contracts.navigators.AboutUsNavigatorInterface;
import com.odigeo.presenter.contracts.views.TermsAndConditionsViewInterface;

public class TermsAndConditionsView extends BaseView<TermsAndConditionsPresenter>
    implements TermsAndConditionsViewInterface {

  private AboutInfoItem mWdgtAirlinesBrandTC;
  private AboutInfoItem mWdgtBrandTC;
  private AboutInfoItem mWdgtPrivacyPolicy;
  private TextView mTvAboutTitle;

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mPresenter.setActionBarTitle(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.ABOUT_BUTTONS_TERMS).toString());
  }

  @Override protected TermsAndConditionsPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideTermsAndConditionsPresenter(this, (AboutUsNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.terms_and_conditions_view;
  }

  @Override protected void initComponent(View view) {
    mTvAboutTitle = (TextView) view.findViewById(R.id.tvAboutTitle);
    mWdgtAirlinesBrandTC = (AboutInfoItem) view.findViewById(R.id.wdgtAirlinesBrandTC);
    mWdgtBrandTC = (AboutInfoItem) view.findViewById(R.id.wdgtBrandTC);
    mWdgtPrivacyPolicy = (AboutInfoItem) view.findViewById(R.id.wdgtPrivacyPolicy);
  }

  @Override protected void setListeners() {
    mWdgtBrandTC.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.openTCBrand();
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_ABOUT_US,
            TrackerConstants.ACTION_ABOUT_US,
            TrackerConstants.LABEL_ABOUT_TERMS_AND_CONDITIONS_OPEN);
      }
    });

    mWdgtAirlinesBrandTC.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.openTCAirline();
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_ABOUT_US,
            TrackerConstants.ACTION_ABOUT_US,
            TrackerConstants.LABEL_ABOUT_TERMS_AND_CONDITIONS_AIRLINES_OPEN);
      }
    });

    mWdgtPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.openPrivacyPolicy();
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_ABOUT_US,
            TrackerConstants.ACTION_ABOUT_US, TrackerConstants.LABEL_ABOUT_PRIVACY_POLICY_OPEN);
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    mTvAboutTitle.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.ABOUT_BUTTONS_TERMS));
    mWdgtAirlinesBrandTC.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.ABOUT_BUTTONS_TC_AIRLINE).toString());
    mWdgtBrandTC.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.ABOUT_BUTTONS_TC_BRAND,
            Configuration.getInstance().getBrand()).toString());
    mWdgtPrivacyPolicy.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.SSO_PRIVACY).toString());
  }
}
