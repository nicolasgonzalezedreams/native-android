package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;
import java.util.List;

public class ItineraryProviderDTO {

  protected PassengerTypeFareDTO adultFares;
  protected PassengerTypeFareDTO childFares;
  protected PassengerTypeFareDTO infantFares;
  protected int id;
  protected String provider;
  protected List<Integer> sections;
  protected BigDecimal price;
  protected BigDecimal providerPrice;
  protected String providerCurrency;
  protected String flightType;

  /**
   * Gets the value of the adultFares property.
   *
   * @return possible object is
   * {@link PassengerTypeFare }
   */
  public PassengerTypeFareDTO getAdultFares() {
    return adultFares;
  }

  /**
   * Sets the value of the adultFares property.
   *
   * @param value allowed object is
   * {@link PassengerTypeFare }
   */
  public void setAdultFares(PassengerTypeFareDTO value) {
    this.adultFares = value;
  }

  /**
   * Gets the value of the childFares property.
   *
   * @return possible object is
   * {@link PassengerTypeFare }
   */
  public PassengerTypeFareDTO getChildFares() {
    return childFares;
  }

  /**
   * Sets the value of the childFares property.
   *
   * @param value allowed object is
   * {@link PassengerTypeFare }
   */
  public void setChildFares(PassengerTypeFareDTO value) {
    this.childFares = value;
  }

  /**
   * Gets the value of the infantFares property.
   *
   * @return possible object is
   * {@link PassengerTypeFare }
   */
  public PassengerTypeFareDTO getInfantFares() {
    return infantFares;
  }

  /**
   * Sets the value of the infantFares property.
   *
   * @param value allowed object is
   * {@link PassengerTypeFare }
   */
  public void setInfantFares(PassengerTypeFareDTO value) {
    this.infantFares = value;
  }

  /**
   * Gets the value of the id property.
   */
  public int getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   */
  public void setId(int value) {
    this.id = value;
  }

  /**
   * Gets the value of the provider property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getProvider() {
    return provider;
  }

  /**
   * Sets the value of the provider property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setProvider(String value) {
    this.provider = value;
  }

  public List<Integer> getSections() {
    return sections;
  }

  public void setSections(List<Integer> sections) {
    this.sections = sections;
  }

  /**
   * Gets the value of the price property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getPrice() {
    return price;
  }

  /**
   * Sets the value of the price property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setPrice(BigDecimal value) {
    this.price = value;
  }

  /**
   * Gets the value of the providerPrice property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  /**
   * Sets the value of the providerPrice property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setProviderPrice(BigDecimal value) {
    this.providerPrice = value;
  }

  /**
   * Gets the value of the providerCurrency property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getProviderCurrency() {
    return providerCurrency;
  }

  /**
   * Sets the value of the providerCurrency property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setProviderCurrency(String value) {
    this.providerCurrency = value;
  }

  /**
   * Gets the value of the flightType property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getFlightType() {
    return flightType;
  }

  /**
   * Sets the value of the flightType property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setFlightType(String value) {
    this.flightType = value;
  }
}
