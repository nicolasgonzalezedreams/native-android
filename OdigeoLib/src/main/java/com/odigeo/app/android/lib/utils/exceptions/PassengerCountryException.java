package com.odigeo.app.android.lib.utils.exceptions;

import com.odigeo.data.tracker.CrashlyticsController;

public class PassengerCountryException extends Exception {

  private PassengerCountryException(String message) {

    super(message);
  }

  public static PassengerCountryException newInstance(String countryCode, String laguageIso) {

    String message = CrashlyticsController.PASSENGER_COUNTRY_EXCEPTION
        + " "
        + "from "
        + countryCode
        + "."
        + laguageIso;
    return new PassengerCountryException(message);
  }
}
