package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import java.util.List;

/**
 * Created by yamil.marques on 10/09/2014.
 */
public class ListCountriesActivityAdapter extends ArrayAdapter<String> {

  private final List<String> countriesList;
  private final LayoutInflater layoutInflater;

  public ListCountriesActivityAdapter(Context context, List<String> countriesList) {
    super(context, 0, countriesList);
    this.countriesList = countriesList;
    this.layoutInflater =
        (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Override public final View getView(int position, View convertView, ViewGroup parent) {
    View rowView = convertView;
    rowView = layoutInflater.inflate(R.layout.row_list_countries, null);

    String item = countriesList.get(position);

    TextView txtCountryName = (TextView) rowView.findViewById(R.id.textViewCountryName);
    txtCountryName.setText(item);

    return rowView;
  }

  public final List<String> getList() {
    return countriesList;
  }
}
