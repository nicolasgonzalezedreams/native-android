package com.odigeo.app.android.lib.neck.requests;

import android.util.Log;
import com.globant.roboneck.common.NeckCookie;
import com.google.gson.Gson;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.dto.requests.AddOrRemoveProductRequestModel;
import com.odigeo.app.android.lib.models.dto.responses.CreateShoppingCartResponse;
import com.odigeo.app.android.lib.neck.requests.base.BaseOdigeoRequests;
import com.odigeo.data.net.helper.DomainHelperInterface;
import java.util.List;

/**
 * Created by manuel on 26/09/14.
 */
@Deprecated public class AddProductRequest extends
    BaseOdigeoRequests<CreateShoppingCartResponse, CreateShoppingCartResponse, AddOrRemoveProductRequestModel> {

  private static final String PATH = "addProduct";
  private DomainHelperInterface mDomainHelper;

  public AddProductRequest(AddOrRemoveProductRequestModel requestModel, List<NeckCookie> cookies,
      OdigeoSession odigeoSession, String deviceId, DomainHelperInterface domainHelper) {
    super(CreateShoppingCartResponse.class, requestModel, cookies, odigeoSession, deviceId);
    mDomainHelper = domainHelper;
  }

  @Override protected final String getUrl() {
    String url = String.format("%s%s", mDomainHelper.getUrl(), PATH);

    return url;
  }

  @Override protected CreateShoppingCartResponse processContent(String responseBody) {

    CreateShoppingCartResponse response = super.processContent(responseBody);
    Log.i(Constants.TAG_LOG, "CreateShoppingCart Rest Services returned " + responseBody);

    if (response != null) {
      response.setCookies(getCookiesReceived());
      Log.d(Constants.TAG_LOG, "[ADD PRODUCT] Pricing break down : " + new Gson().toJson(
          response.getPricingBreakdown()));
    }

    return response;
  }
}
