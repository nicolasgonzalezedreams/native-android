package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.List;

public class PostsellServiceOption implements Serializable {

  private String provider;
  private PostsellServiceOptionType postsellServiceOptionType;
  private List<PostsellServiceOptionProduct> productList;

  public final String getProvider() {
    return provider;
  }

  public final void setProvider(String provider) {
    this.provider = provider;
  }

  public final PostsellServiceOptionType getPostsellServiceOptionType() {
    return postsellServiceOptionType;
  }

  public final void setPostsellServiceOptionType(
      PostsellServiceOptionType postsellServiceOptionType) {
    this.postsellServiceOptionType = postsellServiceOptionType;
  }

  public final List<PostsellServiceOptionProduct> getProductList() {
    return productList;
  }

  public final void setProductList(List<PostsellServiceOptionProduct> productList) {
    this.productList = productList;
  }
}
