package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

/**
 * <p>Java class for Phone complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="Phone">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="countryCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="number" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class PhoneDTO implements Serializable {

  protected String countryCode;
  protected String number;

  /**
   * Gets the value of the countryCode property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCountryCode() {
    return countryCode;
  }

  /**
   * Sets the value of the countryCode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCountryCode(String value) {
    this.countryCode = value;
  }

  /**
   * Gets the value of the number property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getNumber() {
    return number;
  }

  /**
   * Sets the value of the number property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setNumber(String value) {
    this.number = value;
  }
}
