package com.odigeo.app.android.navigator;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.MenuItem;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.data.tracker.TuneTrackerInterface;
import com.odigeo.helper.ABTestHelper;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.presenter.contracts.navigators.BaseNavigatorInterface;
import com.tune.Tune;

import static com.odigeo.dataodigeo.tracker.TrackerConstants.BACK_EVENT;

public class BaseNavigator extends AppCompatActivity implements BaseNavigatorInterface {

  static {
    AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
  }

  public static final String BACK_STACK_NAVIGATOR = "back_stack_navigator";

  protected AndroidDependencyInjector dependencyInjector;
  //TODO HOME REFACTOR: ¿Deberíamos tener esto aquí? ¿Debería ser el navigator el encargado de trackear o el presenter? Bajo mi punto de vista lo segundo
  protected TrackerControllerInterface tracker;
  protected TuneTrackerInterface mTuneTracker;
  protected LocalizableProvider localizables;
  protected MarketProviderInterface marketProvider;
  protected ABTestHelper abTestHelper;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    dependencyInjector =
        (AndroidDependencyInjector) ((OdigeoApp) getApplicationContext()).getDependencyInjector();
    localizables = dependencyInjector.provideLocalizables();
    tracker = dependencyInjector.provideTrackerController();
    mTuneTracker = dependencyInjector.provideTuneTracker();
    boolean wasAppInstalled = PreferencesManager.wasAppInstalled(this);
    Log.d(Constants.TAG_LOG, "wasAppInstalled=" + wasAppInstalled);
    PreferencesManager.setWasAppInstalled(this, true);
    mTuneTracker.setExistingUser(wasAppInstalled);
    marketProvider = dependencyInjector.provideMarketProvider();
    abTestHelper = dependencyInjector.provideABTestHelper();
  }

  /**
   * Replace a {@link Fragment} to this activity's layout.
   *
   * @param containerViewId The container view to where add the fragment.
   * @param fragment The fragment to be added.
   */
  protected void replaceFragment(int containerViewId, Fragment fragment) {
    FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
        android.R.anim.fade_in, android.R.anim.fade_out);
    fragmentTransaction.replace(containerViewId, fragment);
    fragmentTransaction.commit();
  }

  /**
   * Replace a {@link Fragment} to this activity's layout.
   *
   * @param containerViewId The container view to where add the fragment.
   * @param fragment The fragment to be added.
   * @param tagFragment The Tag of the fragment that indentifies it and will be replaced and
   * inserted into the back stack
   */
  protected void replaceFragment(int containerViewId, Fragment fragment, String tagFragment) {
    FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
        android.R.anim.fade_in, android.R.anim.fade_out);
    fragmentTransaction.replace(containerViewId, fragment, tagFragment);
    fragmentTransaction.commit();
  }

  /**
   * Replace a {@link Fragment} to this activity's layout. This works with only one level of
   * fragments
   *
   * @param containerViewId The container view to where add the fragment.
   * @param fragment The fragment to be added.
   */
  protected void replaceFragmentWithBackStack(int containerViewId, Fragment fragment) {
    FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
        android.R.anim.fade_in, android.R.anim.fade_out);
    fragmentTransaction.replace(containerViewId, fragment);
    fragmentTransaction.addToBackStack(BACK_STACK_NAVIGATOR);
    fragmentTransaction.commit();
  }

  /**
   * Replace a {@link Fragment} to this activity's layout.
   *
   * @param containerViewId The container view to where add the fragment.
   * @param fragment The fragment to be added.
   * @param tagFragment The Tag of the fragment that indentifies it and will be replaced and
   * inserted into the back stack
   */
  protected void replaceFragmentWithBackStack(int containerViewId, Fragment fragment,
      String tagFragment) {
    FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
        android.R.anim.fade_in, android.R.anim.fade_out);
    fragmentTransaction.addToBackStack(tagFragment);
    fragmentTransaction.replace(containerViewId, fragment);
    fragmentTransaction.commit();
  }

  /**
   * Sets the title in the actionbar and also configure the back button.
   *
   * @param title Title to be set on the ActionBar.
   */
  @Override public void setNavigationTitle(String title) {
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setTitle(title);
      actionBar.setDisplayHomeAsUpEnabled(true);
    }
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      onHomeUpButtonPressed();
    }
    return super.onOptionsItemSelected(item);
  }

  /**
   * Triggers the event when the HomeUp button is pressed.
   */
  protected void onHomeUpButtonPressed() {
    onBackPressed();
  }

  @Override public void onBackPressed() {
    ViewUtils.hideKeyboard(this);
    tracker.trackAppseeEvent(BACK_EVENT);

    int count = getSupportFragmentManager().getBackStackEntryCount();

    if (count == 0) {
      super.onBackPressed();
    } else {
      getSupportFragmentManager().popBackStack();
    }
  }

  @Override public void onResume() {
    super.onResume();
        /* This lines weren't encapsulated by the TuneTracker, because they need an Activity
         * reference and we can't put Android SDK elements in the domain module.*/
    Tune tune = Tune.getInstance();
    if (tune != null) {
      tune.setReferralSources(this);
      tune.measureSession();
    }
  }

  /**
   * Set the Home button as Up and set the resource instead of the normal up icon.
   *
   * @param resourceId Resource of the Id to be set. Put 0 for normal Up button.
   */
  public void setUpToolbarButton(int resourceId) {
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      if (resourceId != 0) {
        actionBar.setHomeAsUpIndicator(resourceId);
      } else {
        actionBar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_material);
      }
      actionBar.setDisplayHomeAsUpEnabled(true);
    }
  }

  /**
   * Set the Home button as Up and set the resource instead of the normal up icon.
   *
   * @param resource Resource to be set. Put null for normal button.
   */
  public void setUpToolbarButton(Drawable resource) {
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      if (resource != null) {
        actionBar.setHomeAsUpIndicator(resource);
      } else {
        actionBar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_material);
      }
      actionBar.setDisplayHomeAsUpEnabled(true);
    }
  }

  protected void startAppseeSession() {
    tracker.startSession();
  }

  @Override public void onAuthProblem() {
    startActivity(new Intent(getApplicationContext(), JoinUsNavigator.class));
  }
}
