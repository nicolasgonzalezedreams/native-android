package com.odigeo.app.android.receivers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.interactors.managers.BookingStatusAlarmManagerInterface;

public class BootReceiver extends WakefulBroadcastReceiver {

  @Override public void onReceive(Context context, Intent intent) {
    if (intent.getAction() != null && intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
      BookingStatusAlarmManagerInterface bookingStatusAlarmManager =
          AndroidDependencyInjector.getInstance().provideBookingStatusAlarmManager();
      bookingStatusAlarmManager.initializeAlarm();
      completeWakefulIntent(intent);
    }
  }
}
