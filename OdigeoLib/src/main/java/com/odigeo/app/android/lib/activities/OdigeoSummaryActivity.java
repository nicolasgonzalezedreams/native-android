package com.odigeo.app.android.lib.activities;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.helper.CrossCarUrlHandler;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Market;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.fragments.MSLErrorUtilsDialogFragment;
import com.odigeo.app.android.lib.interfaces.ITimeoutWidget;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.OldMyShoppingCart;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.BuyerDTO;
import com.odigeo.app.android.lib.models.dto.LocationDTO;
import com.odigeo.app.android.lib.models.dto.SectionDTO;
import com.odigeo.app.android.lib.models.dto.custom.Code;
import com.odigeo.app.android.lib.models.dto.custom.MslError;
import com.odigeo.app.android.lib.ui.widgets.ConfirmationBuyerInfoWidget;
import com.odigeo.app.android.lib.ui.widgets.ConfirmationInfoItem2Texts;
import com.odigeo.app.android.lib.ui.widgets.PassengersResumeWidget;
import com.odigeo.app.android.lib.ui.widgets.SummaryTravelWidget;
import com.odigeo.app.android.lib.ui.widgets.TimeoutCounterWidget;
import com.odigeo.app.android.lib.ui.widgets.TopBriefWidget;
import com.odigeo.app.android.lib.ui.widgets.UnfoldingBankTransferInfo;
import com.odigeo.app.android.lib.ui.widgets.WhatsNextWidget;
import com.odigeo.app.android.lib.ui.widgets.crossselling.CrossSellingWidgetDrawer;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.app.android.lib.utils.TimeoutCounterWidgetInitializer;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.model.BookingInfoViewModel;
import com.odigeo.app.android.view.InsuranceWidget;
import com.odigeo.app.android.view.PriceBreakdownWidgetView;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.app.android.view.snackbars.PersuasionMessageCountDown;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.shoppingCart.AvailableProductsResponse;
import com.odigeo.data.entity.shoppingCart.BankTransferData;
import com.odigeo.data.entity.shoppingCart.BankTransferResponse;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.FlowConfigurationResponse;
import com.odigeo.data.entity.shoppingCart.InsuranceShoppingItem;
import com.odigeo.data.entity.shoppingCart.PricingBreakdown;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItem;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItemType;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownStep;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackHelper;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackerFlowSession;
import com.odigeo.dataodigeo.tracker.tune.TuneHelper;
import com.odigeo.interactors.MembershipInteractor;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by manuel on 15/09/14.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 2.1
 * @since 13/02/2015
 */
public abstract class OdigeoSummaryActivity extends OdigeoBackgroundAnimatedActivity
    implements ITimeoutWidget {

  public static final String XSELL_SCREEN_LABEL_NAME = "mytrips";
  public static final String XSELL_AD_CAMP_SCREEN = "andXsellmytrips";
  private static final int PASSENGER_LAUNCH_CODE = 1;
  private OdigeoApp odigeoApp;
  private OdigeoSession odigeoSession;
  private OldMyShoppingCart oldMyShoppingCart;

  private Step stepBefore;

  private TimeoutCounterWidget timeoutWidget;
  private ScrollView mScrollView;
  private TimeoutCounterWidgetInitializer timeoutCounterWidgetInitializer;
  private boolean isVisible = false;
  private boolean mustShowConnectionErrorDialog = false;
  private AvailableProductsResponse mAvailableProductsResponse;
  private CreateShoppingCartResponse mCreateShoppingCartResponse;
  private FlowConfigurationResponse mFlowConfigurationResponse;
  private SearchOptions mSearchOptions;
  private boolean mIsFullTransparency;
  private BookingInfoViewModel bookingInfo;
  private BankTransferResponse mBankTransferResponse;
  private List<BankTransferData> mBankTransferDataList;

  private TopBriefWidget mTopBriefWidget;
  private PriceBreakdownWidgetView mPriceBreakdownWidget;
  private SearchTrackHelper mSearchTrackHelper;
  private MembershipInteractor membershipInteractor;

  @Override public void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    odigeoApp = (OdigeoApp) getApplication();
    odigeoSession = odigeoApp.getOdigeoSession();
    timeoutCounterWidgetInitializer = TimeoutCounterWidgetInitializer.getInstance();

    mCreateShoppingCartResponse = (CreateShoppingCartResponse) getIntent().getSerializableExtra(
        Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE);
    mAvailableProductsResponse = (AvailableProductsResponse) getIntent().getSerializableExtra(
        Constants.EXTRA_AVAILABLE_PRODUCTS_RESPONSE);
    mFlowConfigurationResponse = (FlowConfigurationResponse) getIntent().getSerializableExtra(
        Constants.EXTRA_FLOW_CONFIGURATION_RESPONSE);
    mSearchOptions =
        (SearchOptions) getIntent().getSerializableExtra(Constants.EXTRA_SEARCH_OPTIONS);
    mIsFullTransparency = getIntent().getBooleanExtra(Constants.EXTRA_FULL_TRANSPARENCY, false);
    bookingInfo =
        (BookingInfoViewModel) getIntent().getSerializableExtra(Constants.EXTRA_BOOKING_INFO);
    mBankTransferResponse = (BankTransferResponse) getIntent().getSerializableExtra(
        Constants.EXTRA_BANK_TRANSFER_RESPONSE);
    mBankTransferDataList = (List<BankTransferData>) getIntent().getExtras()
        .getSerializable(Constants.EXTRA_BANK_TRANSFER_DATA);

    membershipInteractor =
        ((OdigeoApp) getApplication()).getDependencyInjector().provideMembershipInteractor();

    mSearchTrackHelper = SearchTrackerFlowSession.getInstance().getSearchTrackHelper();

    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {
      setContentView(R.layout.activity_summary);

      ActionBar actionBar = getSupportActionBar();
      if (actionBar != null) {
        actionBar.setDisplayHomeAsUpEnabled(true);
      }

      timeoutWidget = (TimeoutCounterWidget) findViewById(R.id.timeout_counter_widget);
      mScrollView = (ScrollView) findViewById(R.id.scroll_summary);

      initStepBefore();

      mTopBriefWidget = ((TopBriefWidget) findViewById(R.id.topbrief_summary));
      mTopBriefWidget.addData(mSearchOptions);

      mPriceBreakdownWidget = (PriceBreakdownWidgetView) findViewById(R.id.priceBreakDown_summary);

      TextView tvSummaryTopLabel = (TextView) findViewById(R.id.tvSummaryTopLabel);
      tvSummaryTopLabel.setText(LocalizablesFacade.getString(getApplicationContext(),
          OneCMSKeys.DETAILSVIEWCONTROLLER_INFORMATIVEMODULE_TEXT));

      TextView txtCO2TermsAndConditions = (TextView) findViewById(R.id.txtCO2TermsAndConditions);
      txtCO2TermsAndConditions.setText(LocalizablesFacade.getString(getApplicationContext(),
          OneCMSKeys.CO2TERMSANDCONDITIONS_TEXT));

      TextView txtCompleteTripAddingHotelCarSummary =
          (TextView) findViewById(R.id.txtCompleteTripAddingHotelCarSummary);
      txtCompleteTripAddingHotelCarSummary.setText(
          LocalizablesFacade.getString(getApplicationContext(),
              OneCMSKeys.CROSSSELLINGMODULE_INFORMATIVETITLE));
      initTextView(txtCO2TermsAndConditions);
      if (oldMyShoppingCart != null) {
        drawFlightsWidgets();
        drawPassengersWidget();
        drawBuyerWidget();
        drawPricingBreakdownWidget();
        drawInsurancesWidget();
        if (mBankTransferDataList != null && mBankTransferResponse != null) {

          drawIncompleteBooking();
        }
        trackSummaryReached();
      } else {
        Util.sendToHome(this, odigeoApp);
      }
    } else {
      Util.sendToHome(this, odigeoApp);
    }
  }

  @Override public void onStart() {

    super.onStart();
    this.isVisible = true;
    if (mustShowConnectionErrorDialog) {
      displayErrorDialog();
      mustShowConnectionErrorDialog = false;
    }
  }

  @Override public void onResume() {

    super.onResume();

    performGATracking();

    if (stepBefore == Step.RESULTS
        && timeoutCounterWidgetInitializer.isWidgetClosed()
        && timeoutWidget.getVisibility() == View.VISIBLE) {
      timeoutWidget.setVisibility(View.GONE);
      timeoutCounterWidgetInitializer.setWidgetClosed(false);
    }
  }

  @Override public void onPause() {

    super.onPause();
    this.isVisible = false;
  }

  @Override public void onStop() {

    super.onStop();
    this.isVisible = false;
  }

  @Override public final void onDestroy() {

    if (stepBefore == Step.RESULTS) {
      if (timeoutCounterWidgetInitializer.getHandler() != null) {
        timeoutCounterWidgetInitializer.getHandler().removeCallbacksAndMessages(null);
      }
      if (timeoutCounterWidgetInitializer != null) {
        timeoutCounterWidgetInitializer.removeSubscriber(this);
      }
      if (timeoutWidget != null) {
        timeoutWidget.cancelTimer();
      }
    }
    super.onDestroy();
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == Activity.RESULT_OK && requestCode == PASSENGER_LAUNCH_CODE) {
      mCreateShoppingCartResponse = (CreateShoppingCartResponse) data.getSerializableExtra(
          Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE);
      mSearchOptions = (SearchOptions) data.getSerializableExtra(Constants.EXTRA_SEARCH_OPTIONS);

      if ((mIsFullTransparency || membershipInteractor.isMemberForCurrentMarket())
          && stepBefore.equals(Step.RESULTS)) {
        mTopBriefWidget.setMembershipApplied(true);

        mTopBriefWidget.updatePrice(
            mPriceBreakdownWidget.getTotalPrice());
      }

      drawPassengersWidget();
      drawInsurancesWidget();
      drawPricingBreakdownWidget();
    }
  }

  /**
   * Method to show What's Next widget in the summary screen
   */
  private void drawNextWidget() {

    WhatsNextWidget whatsNextWidget = (WhatsNextWidget) findViewById(R.id.whatsnext_summary);
    whatsNextWidget.setStatus(oldMyShoppingCart.getBookingSummaryStatus());
    whatsNextWidget.setEmailConfirmation(oldMyShoppingCart.getBuyer().getMail());
  }

  public void setWidgetClosed() {
    //Nothing to do
  }

  @Override public final String getActivityTitle() {

    return LocalizablesFacade.getString(this, "summaryviewcontroller_title").toString();
  }

  private void trackSummaryReached() {

    if (mSearchTrackHelper == null) return;

    mSearchTrackHelper.setPrice(secureGetPrice());
    for (SegmentWrapper segmentWrapper : bookingInfo.getSegmentsWrappers()) {
      String airline = segmentWrapper.getCarrier().getCode();
      mSearchTrackHelper.addAirline(airline);
    }
    mTracker.trackSummaryReached(oldMyShoppingCart.getPrice().getCurrency(),
        mSearchTrackHelper.getFlightType(), mSearchTrackHelper.getAdults(),
        mSearchTrackHelper.getKids(), mSearchTrackHelper.getInfants(),
        mSearchTrackHelper.getPrice(), mSearchTrackHelper.getOrigins(),
        mSearchTrackHelper.getDestinations(), mSearchTrackHelper.getDates(),
        mSearchTrackHelper.getAirlines(), mSearchTrackHelper.getDepartureCountries(),
        mSearchTrackHelper.getArrivalCountries());

    double unitPrice = getUnitPrice();
    TuneHelper.setUnitPrice(unitPrice);
    TuneHelper.setCurrencyCode(oldMyShoppingCart.getPrice().getCurrency());
    mTuneTracker.trackViewProduct();

    SearchTrackerFlowSession.getInstance().setSearchTrackHelper(mSearchTrackHelper);
  }

  private double getUnitPrice() {

    double unitPrice = 0.0;
    for (PricingBreakdownStep step : mCreateShoppingCartResponse.getPricingBreakdown()
        .getPricingBreakdownSteps()) {
      for (PricingBreakdownItem item : step.getPricingBreakdownItems()) {
        if (item.getPriceItemType().equals(PricingBreakdownItemType.ADULTS_PRICE)) {
          unitPrice =
              item.getPriceItemAmount().doubleValue() / (double) mSearchOptions.getNumberOfAdults();
          break;
        }
      }
    }
    return unitPrice;
  }

  private double secureGetPrice() {

    double price = 0.0;
    if (oldMyShoppingCart.getTotalPrice() != null) {
      price = oldMyShoppingCart.getTotalPrice().doubleValue();
    } else if (oldMyShoppingCart.getPrice().getAmount() != null) {
      price = oldMyShoppingCart.getPrice().getAmount().doubleValue();
    } else {
      List<PricingBreakdownStep> steps =
          mCreateShoppingCartResponse.getPricingBreakdown().getPricingBreakdownSteps();
      if (steps != null) {
        for (PricingBreakdownStep step : steps) {
          if (step.getStep().equals(Step.DEFAULT) && step.getTotalPrice() != null) {
            price = step.getTotalPrice().doubleValue();
            break;
          }
        }
      }
    }
    return price;
  }

  public void displayErrorDialog() {

    MslError error = new MslError();
    error.setCode(Code.SESSION_TIMEOUT.getErrorCode());
    error.setCodeString(Code.SESSION_TIMEOUT);
    MSLErrorUtilsDialogFragment fragment = MSLErrorUtilsDialogFragment.newInstance(null, error);
    fragment.setParentScreen(Step.SUMMARY.toString());
    fragment.setCancelable(false);
    fragment.show(this.getSupportFragmentManager(), "");
  }

  public void showTimeoutWidget() {

    timeoutWidget.setVisibility(View.VISIBLE);
    timeoutWidget.setMinutes(Constants.TIMEOUT_COUNTER_WIDGET_SUMMARY * 60,
        Constants.TIMEOUT_COUNTER_WIDGET_SUMMARY);
  }

  public void setScrollParameters() {

    RelativeLayout.LayoutParams p =
        new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT);
    p.addRule(RelativeLayout.ABOVE, R.id.timeout_counter_widget);
    p.addRule(RelativeLayout.BELOW, R.id.topbrief_summary);

    mScrollView.setLayoutParams(p);
  }

  public void showErrorDialog() {

    if (isVisible) {
      displayErrorDialog();
    } else {
      mustShowConnectionErrorDialog = true;
    }
  }

  private void initTextView(TextView txtCO2TermsAndConditions) {

    txtCO2TermsAndConditions.setVisibility(View.GONE);
    txtCO2TermsAndConditions.setTypeface(Configuration.getInstance().getFonts().getRegular());

    if (Configuration.getInstance().getCurrentMarket().getHasCo2()) {
      txtCO2TermsAndConditions.setVisibility(View.VISIBLE);
      txtCO2TermsAndConditions.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {

          Market currentMarket = Configuration.getInstance().getCurrentMarket();
          String co2Link = currentMarket.getCo2Link();
          launchNewWebView(co2Link);
        }
      });
    }
  }

  private void initStepBefore() {

    stepBefore = (Step) getIntent().getSerializableExtra(Constants.EXTRA_STEP_TO_SUMMARY);
    if (stepBefore == null) {
      stepBefore = Step.RESULTS;
    }

    //TODO THIS CONDITION NEVER HAPPEND
    if (stepBefore == Step.MYTRIPS) {
      int searchOptionId = getIntent().getIntExtra(Constants.EXTRA_SEARCH_OPTION_ID, 0);
      mSearchOptions = new SearchOptions();
      mSearchOptions.setId(searchOptionId);
      oldMyShoppingCart = PreferencesManager.readShoppingCart(this, mSearchOptions);
      if (oldMyShoppingCart != null) {
        drawClientBookingIds(oldMyShoppingCart.getClientBookingReferenceIds());

        Button button = (Button) findViewById(R.id.button_summary_addcalendar);
        button.setVisibility(View.VISIBLE);
        button.setOnClickListener(new View.OnClickListener() {
          @Override public void onClick(View v) {

            addToCalendar();
          }
        });
        button.setText(
            LocalizablesFacade.getString(this, "confirmationviewcontroller_addcalendar_button")
                .toString());

        TextView txtCompleteTripAddingHotelCar =
            (TextView) findViewById(R.id.txtCompleteTripAddingHotelCarSummary);

        txtCompleteTripAddingHotelCar.setText(

            LocalizablesFacade.getString(this, "crosssellingmodule_informativetitle",
                oldMyShoppingCart.getBuyer().getName()));

        txtCompleteTripAddingHotelCar.setVisibility(View.VISIBLE);

        if (!OdigeoDateUtils.isOneDayTrip(mSearchOptions)
            && mSearchOptions.getTravelType() != TravelType.MULTIDESTINATION) {
          txtCompleteTripAddingHotelCar.setVisibility(View.VISIBLE);
          new CrossSellingWidgetDrawer(new WeakReference<Activity>(this), oldMyShoppingCart,
              mSearchOptions, XSELL_SCREEN_LABEL_NAME, CrossCarUrlHandler.CAMPAIGN_MY_TRIPS,
              mCreateShoppingCartResponse.getShoppingCart(),
              AndroidDependencyInjector.getInstance().provideMarketProvider());
        }
        drawNextWidget();
      }
    } else {
      oldMyShoppingCart = odigeoSession.getOldMyShoppingCart();
      if (stepBefore == Step.RESULTS) {
        showContinueButton();
        timeoutCounterWidgetInitializer.addSubscriber(this);
        timeoutCounterWidgetInitializer.initializeTimeoutRunnable();

        if (Configuration.getInstance().getShowPersuasive()) {
          PersuasionMessageCountDown.getInstance().start();
        }

        //this is done in order to make the animation flow smoothly and reach the bottom of the content by scrolling
        timeoutCounterWidgetInitializer.initializeScrollRunnable();
        timeoutCounterWidgetInitializer.getHandler()
            .postDelayed(timeoutCounterWidgetInitializer.getTimeoutRunnable(),
                Constants.TIMEOUT_COUNTER_WIDGET_DELAY);
        timeoutCounterWidgetInitializer.getHandler()
            .postDelayed(timeoutCounterWidgetInitializer.getScrollFixRunnable(),
                Constants.TIMEOUT_COUNTER_WIDGET_DELAY
                    + Constants.TIMEOUT_COUNTER_SCROLL_PARAMS_REFORMAT);
      }
    }
  }

  private void drawFlightsWidgets() {

    List<SegmentWrapper> segments = bookingInfo.getSegmentsWrappers();

    if (segments != null && !segments.isEmpty()) {
      /// ** STAR: draw cards for travels
      LinearLayout layoutForFlights = (LinearLayout) findViewById(R.id.layout_for_flights_summary);
      int index = 0;
      for (SegmentWrapper segmentWrapper : segments) {
        SummaryTravelWidget widget =
            new SummaryTravelWidget(this, segmentWrapper, mSearchOptions.getTravelType(), index++);
        ViewUtils.setSeparator(widget, getApplicationContext());
        layoutForFlights.addView(widget);
      }
      /// ** END
    }
  }

  private void drawClientBookingIds(List<String> referenceIds) {

    if (referenceIds != null) {
      LinearLayout container =
          (LinearLayout) findViewById(R.id.view_summary_clientBookingId_layout);
      container.setVisibility(View.VISIBLE);
      for (int i = 0; i < referenceIds.size(); i++) {
        ConfirmationInfoItem2Texts item = new ConfirmationInfoItem2Texts(getApplicationContext());
        item.setText(referenceIds.get(i));
        item.setIdImage(R.drawable.confirmation_update);
        CharSequence secondText =
            LocalizablesFacade.getString(this, "confirmation_whatsnext_bookingid_placeholder");
        item.setSecondText(secondText);
        container.addView(item);
        if (i == referenceIds.size() - 1) {
          item.setLastInForm(true);
        }
      }
    }
  }

  private void drawBuyerWidget() {

    if (stepBefore == Step.MYTRIPS) {
      BuyerDTO buyerLegacyDTO = oldMyShoppingCart.getBuyer();
      if (buyerLegacyDTO != null) {
        ConfirmationBuyerInfoWidget buyerWidget =
            (ConfirmationBuyerInfoWidget) findViewById(R.id.custom_summary_buyer_widget);
        buyerWidget.setVisibility(View.VISIBLE);
        buyerWidget.setBuyer(buyerLegacyDTO);
        buyerWidget.showInfoCard(false);
      }
    } else {
      ConfirmationBuyerInfoWidget buyerWidget =
          (ConfirmationBuyerInfoWidget) findViewById(R.id.custom_summary_buyer_widget);
      buyerWidget.setVisibility(View.GONE);
    }
  }

  private void drawPassengersWidget() {

    List<Traveller> travellers = mCreateShoppingCartResponse.getShoppingCart().getTravellers();
    List<FlightSegment> flightSegments = mSearchOptions.getFlightSegments();

    PassengersResumeWidget widget =
        (PassengersResumeWidget) findViewById(R.id.passengers_widget_summary);
    if (travellers != null && !travellers.isEmpty()) {
      widget.setPassengers(travellers, flightSegments);
      widget.setVisibility(View.VISIBLE);
    } else {
      widget.setVisibility(View.GONE);
    }
  }

  private void drawPricingBreakdownWidget() {

    mPriceBreakdownWidget.addData(mScrollView, mSearchOptions,
        mCreateShoppingCartResponse.getPricingBreakdown(), stepBefore,
        bookingInfo.getCollectionMethodWithPrice(), mCreateShoppingCartResponse.getShoppingCart());

    mPriceBreakdownWidget.showPricesBreakdown();

    if ((mIsFullTransparency || membershipInteractor.isMemberForCurrentMarket())
        && stepBefore.equals(Step.RESULTS)) {
      mTopBriefWidget.setMembershipApplied(true);
      mTopBriefWidget.updatePrice(mPriceBreakdownWidget.getTotalPrice());
    }
  }

  private void drawInsurancesWidget() {

    LinearLayout lLInsurancesContent =
        ((LinearLayout) findViewById(R.id.layout_for_insurances_summary));
    List<InsuranceShoppingItem> insurances = mCreateShoppingCartResponse.getShoppingCart()
        .getOtherProductsShoppingItems()
        .getInsuranceShoppingItems();
    if (insurances != null && !insurances.isEmpty()) {
      lLInsurancesContent.removeAllViews();
      for (InsuranceShoppingItem insurance : insurances) {
        if (insurance.isSelectable()) {
          InsuranceWidget insuranceWidget = new InsuranceWidget(this, mScrollView, insurance,
              mCreateShoppingCartResponse.getShoppingCart().getTravellers().size());
          ViewUtils.setSeparator(insuranceWidget, getApplicationContext());
          lLInsurancesContent.addView(insuranceWidget);
          lLInsurancesContent.setVisibility(View.VISIBLE);
        }
      }
    } else {
      lLInsurancesContent.setVisibility(View.GONE);
    }
  }

  private void showContinueButton() {

    Button button = (Button) findViewById(R.id.btnContinuarSummary);
    button.setVisibility(View.VISIBLE);
    button.setTypeface(Configuration.getInstance().getFonts().getBold());
    button.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {

        postGAEventSummaryContinue();

        if (mSearchTrackHelper != null && mTracker != null) {
          mTracker.trackSummaryContinue(oldMyShoppingCart.getPrice().getCurrency(),
              mSearchTrackHelper.getFlightType(), mSearchTrackHelper.getAdults(),
              mSearchTrackHelper.getKids(), mSearchTrackHelper.getInfants(),
              mSearchTrackHelper.getPrice(), mSearchTrackHelper.getOrigins(),
              mSearchTrackHelper.getDestinations(), mSearchTrackHelper.getDates(),
              mSearchTrackHelper.getAirlines(), mSearchTrackHelper.getDepartureCountries(),
              mSearchTrackHelper.getArrivalCountries());
        }

        Intent intent =
            new Intent(OdigeoSummaryActivity.this, odigeoApp.getNextActivityClass(Step.SUMMARY));
        intent.putExtra(Constants.EXTRA_REPRICING_TICKETS_PRICES,
            getTicketsTotal(mCreateShoppingCartResponse.getPricingBreakdown()));
        intent.putExtra(Constants.TIMEOUT_WIDGET_PREVIOUS_TIME, timeoutWidget.getTime());
        intent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE, mCreateShoppingCartResponse);
        intent.putExtra(Constants.EXTRA_AVAILABLE_PRODUCTS_RESPONSE, mAvailableProductsResponse);
        intent.putExtra(Constants.EXTRA_FLOW_CONFIGURATION_RESPONSE, mFlowConfigurationResponse);
        intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, mSearchOptions);
        intent.putExtra(Constants.EXTRA_FULL_TRANSPARENCY, mIsFullTransparency);
        intent.putExtra(Constants.EXTRA_BOOKING_INFO, bookingInfo);

        if (timeoutWidget.getVisibility() == View.VISIBLE) {
          intent.putExtra(Constants.TIMEOUT_WIDGET_SHOWN, true);
        } else {
          intent.putExtra(Constants.TIMEOUT_WIDGET_SHOWN, false);
        }
        startActivityForResult(intent, PASSENGER_LAUNCH_CODE);
      }
    });
    button.setText(
        LocalizablesFacade.getString(getApplicationContext(), "common_buttoncontinue").toString());
  }

  private double getTicketsTotal(PricingBreakdown pricingBreakdown) {

    double total = 0;
    int lastStep = pricingBreakdown.getPricingBreakdownSteps().size() - 1;
    PricingBreakdownStep step = pricingBreakdown.getPricingBreakdownSteps().get(lastStep);
    for (PricingBreakdownItem item : step.getPricingBreakdownItems()) {
      switch (item.getPriceItemType()) {
        case ADULTS_PRICE:
        case CHILDREN_PRICE:
        case INFANTS_PRICE:
          total += item.getPriceItemAmount().doubleValue();
          break;
      }
    }
    return total;
  }

  private void addToCalendar() {

    List<SegmentWrapper> flightSegments = bookingInfo.getSegmentsWrappers();
    if (flightSegments != null && !flightSegments.isEmpty()) {
      SimpleDateFormat simpleHourFormat = OdigeoDateUtils.getGmtDateFormat("hh:mm");

      SegmentWrapper segmentWrapper = flightSegments.get(0);
      List<SectionDTO> sectionDTOs = segmentWrapper.getSectionsObjects();
      SectionDTO departureFlight = sectionDTOs.get(0);
      SectionDTO arrivalFlight = sectionDTOs.get(sectionDTOs.size() - 1);

      LocationDTO locationFrom = departureFlight.getLocationFrom();
      LocationDTO locationTo = arrivalFlight.getLocationTo();
      Calendar departureDate = OdigeoDateUtils.createCalendar(departureFlight.getDepartureDate());
      Calendar arrivalDate = OdigeoDateUtils.createCalendar(arrivalFlight.getArrivalDate());
      String description = CommonLocalizables.getInstance().getCommonLeg(this)
          + "\n"
          + locationFrom.getName()
          + "\n"
          + locationFrom.getCityName()
          + "\n"
          + locationFrom.getCountryName()
          + "\n"
          + simpleHourFormat.format(departureDate.getTime())
          + "\n"
          + CommonLocalizables.getInstance().getCommonInbound(this)
          + "\n"
          + locationTo.getName()
          + "\n"
          + locationTo.getCityName()
          + "\n"
          + locationTo.getCountryName()
          + "\n"
          + simpleHourFormat.format(arrivalDate.getTime());

      Intent intent = new Intent(Intent.ACTION_INSERT).setData(CalendarContract.Events.CONTENT_URI)
          .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, departureDate.getTimeInMillis())
          .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, arrivalDate.getTimeInMillis())
          .putExtra(CalendarContract.Events.TITLE, Configuration.getInstance().getBrandVisualName())
          .putExtra(CalendarContract.Events.DESCRIPTION, description)
          .putExtra(CalendarContract.Events.EVENT_LOCATION, locationFrom.getName());
      startActivity(intent);
    }
  }

  private void shareTrip() {

    SimpleDateFormat simpleDateFormat = OdigeoDateUtils.getGmtDateFormat("EEE. dd MMM");
    SimpleDateFormat simpleHourFormat = OdigeoDateUtils.getGmtDateFormat("HH:mm");

    String departureCity = "";
    String arrivalCity = "";

    String urlPlayStore = Configuration.getInstance().getAppStoreURL();
    String appName = Configuration.getInstance().getBrandVisualName();

    String subject =
        LocalizablesFacade.getString(this, "sharekit_tripdetails_email_subject", appName)
            .toString();
    String title =
        LocalizablesFacade.getRawString(this, "sharekit_tripdetails_email_header", urlPlayStore,
            appName);

    StringBuilder body = new StringBuilder(title);
    body.append("\n");

    List<SegmentWrapper> segments = bookingInfo.getSegmentsWrappers();

    String totalPrice = LocaleUtils.getLocalizedCurrencyValue(mPriceBreakdownWidget.getTotalPrice(),
        oldMyShoppingCart.getLocale());

    if (segments != null && !segments.isEmpty()) {

      int index = 0;

      for (SegmentWrapper segmentWrapper : segments) {
        List<SectionDTO> sectionDTOs = segmentWrapper.getSectionsObjects();
        SectionDTO sectionDepartureDTO = sectionDTOs.get(0);
        SectionDTO sectionArrivalDto = sectionDTOs.get(sectionDTOs.size() - 1);
        Calendar date = OdigeoDateUtils.createCalendar(sectionDepartureDTO.getDepartureDate());

        String details =
            LocalizablesFacade.getString(this, "sharekit_tripdetails_email_body_itinerarysegment",
                Util.getTitleHeader(this, mSearchOptions.getTravelType(), index),
                String.valueOf(mSearchOptions.getTotalPassengers()),
                simpleDateFormat.format(date.getTime()),
                sectionDepartureDTO.getLocationFrom().getCityName(),
                sectionArrivalDto.getLocationTo().getCityName(),
                simpleHourFormat.format(date.getTime()), simpleHourFormat.format(
                    OdigeoDateUtils.createDate(sectionArrivalDto.getArrivalDate())),
                segmentWrapper.getCarrier().getName()).toString();

        body.append(details);
        body.append("\n");
        index++;

        //TODO: Ask why is one?
        if (index == 1) {
          departureCity = sectionDepartureDTO.getLocationFrom().getCityName();
          arrivalCity = sectionArrivalDto.getLocationTo().getCityName();
        }
      }

      body.append("\n");
      body.append(LocalizablesFacade.getString(this, "sharekit_tripdetails_email_body_totalprice",
          totalPrice).toString());
    }

    String bodyTwitter =
        LocalizablesFacade.getString(this, "sharing_flightdetailsmytrips_twitter_body_new",
            departureCity, arrivalCity, totalPrice, urlPlayStore).toString();

    String bodyPlain =
        LocalizablesFacade.getString(this, "sharing_flightdetailsmytrips_whatsapp_description",
            departureCity, arrivalCity, totalPrice, urlPlayStore).toString();

    loadIntent(bodyTwitter, subject, body.toString(), bodyPlain);
  }

  private void loadIntent(String bodyTwitter, String subject, String body, String bodyPlain) {

    PackageManager pm = getPackageManager();
    Intent shareIntent = new Intent(Intent.ACTION_SEND);
    shareIntent.setType("text/plain");
    shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
    shareIntent.putExtra(Intent.EXTRA_TEXT, bodyPlain);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
      shareIntent.putExtra(Intent.EXTRA_HTML_TEXT, Html.fromHtml(body));
    }

    List<ResolveInfo> resolveInfos = pm.queryIntentActivities(shareIntent, 0);
    List<LabeledIntent> intentList = new ArrayList<>();

    Intent chooserIntent;

    for (ResolveInfo info : resolveInfos) {
      String packageName = info.activityInfo.packageName;
      Intent intent = new Intent(Intent.ACTION_SEND);
      intent.setComponent(new ComponentName(packageName, info.activityInfo.name));
      intent.setType("text/plain");
      intent.putExtra(Intent.EXTRA_SUBJECT, subject);

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
        intent.putExtra(Intent.EXTRA_HTML_TEXT, Html.fromHtml(body));
      }

      if (packageName.contains("twitter")) {
        intent.putExtra(Intent.EXTRA_TEXT, bodyTwitter);
      } else {
        intent.putExtra(Intent.EXTRA_TEXT, bodyPlain);
      }

      intentList.add(new LabeledIntent(intent, packageName, info.loadLabel(pm), info.icon));
    }

    if (!intentList.isEmpty()) {
      chooserIntent = Intent.createChooser(intentList.remove(0),
          LocalizablesFacade.getString(this, "aboutoptionsmodule_about_option_shareapp")
              .toString());
    } else {
      chooserIntent = shareIntent;
    }

    if (chooserIntent != null) {
      chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
          intentList.toArray(new LabeledIntent[intentList.size()]));
      startActivity(chooserIntent);
    }
  }

  @Override public final boolean onCreateOptionsMenu(Menu menu) {

    getMenuInflater().inflate(R.menu.menu_share_trip, menu);
    MenuItem item = menu.findItem(R.id.menu_item_share_trip);
    item.setTitle(LocalizablesFacade.getString(this, "common_share").toString());
    item.setIcon(
        DrawableUtils.getTintedResource(R.drawable.actionbar_share, R.color.semantic_icon, this));
    return super.onCreateOptionsMenu(menu);
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {

    int i = item.getItemId();
    if (i == android.R.id.home) {

      if (stepBefore.equals(Step.MYTRIPS)) {
        postGAEventGoBackToMyTrips();
      } else if (stepBefore.equals(Step.INSURANCE)) {
        postGAEventGoBackToInsurances();
      } else if (stepBefore.equals(Step.PASSENGER)) {
        postGAEventGoBackToPassengers();
      } else if (stepBefore.equals(Step.PAYMENT)) {
        postGAEventGoBackToPayment();
      } else {
        postGAEventGoBackFromSummary();
      }

      finish();
      return true;
    } else if (i == R.id.menu_item_share_trip) {
      postGAEventShare();
      shareTrip();
      return true;
    } else {
      return super.onOptionsItemSelected(item);
    }
  }

  private void launchNewWebView(String url) {

    Intent intent = new Intent(this, odigeoApp.getWebViewActivityClass());
    Bundle extras = new Bundle();
    extras.putString(Constants.EXTRA_URL_WEBVIEW, url);
    intent.putExtras(extras);
    startActivity(intent);
  }

  public final Step getStepBefore() {

    return stepBefore;
  }

  private void postGAEventShare() {

    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_SUMMARY,
            GAnalyticsNames.ACTION_TRIP_DETAILS, GAnalyticsNames.LABEL_PARTIAL_MYTRIPS_SHARE));
  }

  // GATracking - Flight summary - event 4
  private void postGAEventSummaryContinue() {

    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_SUMMARY,
            GAnalyticsNames.ACTION_NAVIGATION, GAnalyticsNames.LABEL_CONTINUE_CLICKS_BOTTOM));
  }

  private void postGAEventGoBackToMyTrips() {
    // GATracking - Flight summary - event 1
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_MY_TRIPS_INFO,
            GAnalyticsNames.ACTION_NAVIGATION, GAnalyticsNames.LABEL_GO_BACK));
  }

  private void postGAEventGoBackFromSummary() {
    // GATracking - Flight summary - event 1
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_SUMMARY,
            GAnalyticsNames.ACTION_NAVIGATION, GAnalyticsNames.LABEL_GO_BACK));
  }

  private void postGAEventGoBackToInsurances() {
    // GATracking - Flight summary - event 1
    BusProvider.getInstance()
        .post(new GATrackingEvent(
            GAnalyticsNames.CATEGORY_PARTIAL_FLIGHTS_TRIP_DETAILS + "insurances",
            GAnalyticsNames.ACTION_NAVIGATION, GAnalyticsNames.LABEL_GO_BACK));
  }

  private void postGAEventGoBackToPassengers() {
    // GATracking - Flight summary - event 1
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_PARTIAL_FLIGHTS_TRIP_DETAILS + "pax",
            GAnalyticsNames.ACTION_NAVIGATION, GAnalyticsNames.LABEL_GO_BACK));
  }

  private void postGAEventGoBackToPayment() {
    // GATracking - Flight summary - event 1
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_PARTIAL_FLIGHTS_TRIP_DETAILS + "pay",
            GAnalyticsNames.ACTION_NAVIGATION, GAnalyticsNames.LABEL_GO_BACK));
  }

  /**
   * Here we link and setup the information for the incomplete booking
   */
  private void drawIncompleteBooking() {

    BankTransferData bankTransferData = mBankTransferDataList.get(0);
    BankTransferResponse bankTransferResponse = mBankTransferResponse;

    if (bankTransferData == null || bankTransferResponse == null) {
      findViewById(R.id.form_incomplete_booking).setVisibility(View.GONE);
    } else {
      findViewById(R.id.form_incomplete_booking).setVisibility(View.VISIBLE);
      UnfoldingBankTransferInfo bankTransferInfo =
          ((UnfoldingBankTransferInfo) findViewById(R.id.banktransfer_wrapper));
      bankTransferInfo.setData(bankTransferData, bankTransferResponse,
          mCreateShoppingCartResponse.getShoppingCart().getTotalPrice());
    }
  }

  public void performGATracking() {

    if (getStepBefore() == Step.MYTRIPS) {
      // GAnalytics screen tracking - MyTrip details
      mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_MY_TRIPS_DETAILS);
    } else {
      if (getStepBefore() == Step.RESULTS) {
        // GAnalytics screen tracking - Show flight summary
        BusProvider.getInstance()
            .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_FLIGHT_SUMMARY));
      } else {
        // GAnalytics screen tracking - Trip details
        BusProvider.getInstance()
            .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_TRIP_DETAILS));
      }
    }
  }

  public abstract void onGAEventTriggered(GATrackingEvent event);
}
