package com.odigeo.app.android.lib.models.dto.responses;

import com.odigeo.app.android.lib.models.dto.InsuranceOfferDTO;
import com.odigeo.app.android.lib.models.dto.responses.base.BaseResponseDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Deprecated public class AvailableProductsResponse extends BaseResponseDTO implements Serializable {

  private List<InsuranceOfferDTO> insuranceOffers = new ArrayList<InsuranceOfferDTO>();

  public final List<InsuranceOfferDTO> getInsuranceOffers() {
    return insuranceOffers;
  }

  public final void setInsuranceOffers(List<InsuranceOfferDTO> insuranceOffers) {
    this.insuranceOffers = insuranceOffers;
  }
}
