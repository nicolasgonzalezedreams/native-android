package com.odigeo.app.android.lib.ui.widgets;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 29/02/16
 */
public class MonthYearPicker extends DialogFragment
    implements View.OnClickListener, NumberPicker.OnValueChangeListener {

  private static final String TAG_DIALOG = "month_year_picker";
  private static final int MAX_MONTH = 12;
  private static final int MIN_MONTH = 0;
  private static final int YEAR_ADDITIONAL = 25;
  private static final String MONTH_FORMAT = "%02d";
  private static final String PICKER_FORMAT = "MMMM, yyyy";
  private final SimpleDateFormat mSimpleDateFormat =
      new SimpleDateFormat(PICKER_FORMAT, LocaleUtils.getCurrentLocale());
  private OnDateSelectedListener mOnDateSelectedListener;
  private NumberPicker mMonth;
  private NumberPicker mYear;
  private Button mOkButton;
  private Dialog mDialog;
  private int mMinMonth;
  private int mMinYear;
  private long mSelectedDate;
  private String[] mMonthValues;

  private CharSequence getPositiveButtonText(Context context) {
    return LocalizablesFacade.getString(context, OneCMSKeys.PASSENGER_PICKER_OK_BTN);
  }

  private long getSelectedDateOnMillis() {
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.DAY_OF_MONTH, 1);
    calendar.set(Calendar.MONTH, Integer.parseInt(mMonthValues[mMonth.getValue()]) - 1);
    calendar.set(Calendar.YEAR, mYear.getValue());
    return calendar.getTimeInMillis();
  }

  private void setOnDateSelectedListener(OnDateSelectedListener onDateSelectedListener) {
    this.mOnDateSelectedListener = onDateSelectedListener;
  }

  private void setSelectedDate(long selectedDate) {
    mSelectedDate = selectedDate;
  }

  private void setInitialDate(long mInitialDate) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(mInitialDate);
    mMinMonth = calendar.get(Calendar.MONTH);
    mMinYear = calendar.get(Calendar.YEAR);
  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View root = inflater.inflate(R.layout.month_year_picker, container, false);
    linkViews(root);
    initializeViews();
    mDialog.setTitle(mSimpleDateFormat.format(new Date(getSelectedDateOnMillis())));
    return root;
  }

  @NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState) {
    mDialog = super.onCreateDialog(savedInstanceState);
    return mDialog;
  }

  private void buildMonthValues() {
    List<String> values = new LinkedList<>();
    for (int index = mMinMonth; index < MAX_MONTH; index++) {
      values.add(String.format(Locale.US, MONTH_FORMAT, index + 1));
    }
    for (int index = MIN_MONTH; index < mMinMonth; index++) {
      values.add(String.format(Locale.US, MONTH_FORMAT, index + 1));
    }
    mMonthValues = new String[values.size()];
    values.toArray(mMonthValues);
  }

  private void initializeViews() {
    initYear();
    buildMonthValues();
    mMonth.setMinValue(0);
    mMonth.setMaxValue(mMonthValues.length - 1);
    mMonth.setDisplayedValues(mMonthValues);
    mYear.setMinValue(mMinYear);
    mYear.setMaxValue(mMinYear + YEAR_ADDITIONAL);
    mMonth.setWrapSelectorWheel(false);
    mYear.setWrapSelectorWheel(false);
    mMonth.setOnValueChangedListener(this);
    mYear.setOnValueChangedListener(this);
    mOkButton.setText(getPositiveButtonText(getActivity()));
    mOkButton.setOnClickListener(this);
    selectDate();
  }

  private void selectDate() {
    if (mSelectedDate > 0) {
      Calendar calendar = Calendar.getInstance();
      calendar.setTimeInMillis(mSelectedDate);
      int month = 0;
      int monthSelected = calendar.get(Calendar.MONTH) + 1;
      for (String value : mMonthValues) {
        if (Integer.parseInt(value) == monthSelected) {
          break;
        }
        month++;
      }
      mMonth.setValue(month);
      mYear.setValue(calendar.get(Calendar.YEAR));
    }
  }

  private void initYear() {
    if (mMinYear == 0) {
      Calendar calendar = Calendar.getInstance();
      mMinYear = calendar.get(Calendar.YEAR);
    }
  }

  private void linkViews(View root) {
    mMonth = ((NumberPicker) root.findViewById(R.id.picker_month));
    mYear = ((NumberPicker) root.findViewById(R.id.picker_year));
    mOkButton = ((Button) root.findViewById(R.id.ok_button));
  }

  @Override public void onClick(View v) {
    if (mOnDateSelectedListener != null) {
      mOnDateSelectedListener.onDateSelected(getSelectedDateOnMillis());
    }
    dismiss();
  }

  @Override public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
    mDialog.setTitle(mSimpleDateFormat.format(new Date(getSelectedDateOnMillis())));
    validateDateSelected(picker);
  }

  private void validateDateSelected(NumberPicker picker) {
    int minMonthValue = Integer.parseInt(mMonthValues[0]);
    int newMonthValue = Integer.parseInt(mMonthValues[mMonth.getValue()]);
    int newYearValue = mYear.getValue();
    if (picker == mMonth) {
      if (mMinYear == newYearValue && newMonthValue < minMonthValue) {
        mYear.setValue(mYear.getValue() + 1);
      }
    } else if (picker == mYear) {
      if (mMinYear == newYearValue && newMonthValue < minMonthValue) {
        mMonth.setValue(0);
      }
    }
  }

  public interface OnDateSelectedListener {

    void onDateSelected(long selectedDate);
  }

  public static class Builder {

    private final FragmentManager mFragmentManager;
    private Fragment mFragment;
    private boolean isAlreadyAdded;

    public Builder(FragmentManager fragmentManager) {
      mFragmentManager = fragmentManager;
      mFragment = mFragmentManager.findFragmentByTag(TAG_DIALOG);
      if (mFragment == null) {
        mFragment = new MonthYearPicker();
      } else {
        isAlreadyAdded = true;
      }
    }

    public Builder setInitialDate(long initialDate) {
      ((MonthYearPicker) mFragment).setInitialDate(initialDate);
      return this;
    }

    public Builder setSelectedDate(long selectedDate) {
      ((MonthYearPicker) mFragment).setSelectedDate(selectedDate);
      return this;
    }

    public Builder setOnDateSelectedListener(OnDateSelectedListener onDateSelectedListener) {
      ((MonthYearPicker) mFragment).setOnDateSelectedListener(onDateSelectedListener);
      return this;
    }

    public void show() {
      if (!isAlreadyAdded) {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(mFragment, TAG_DIALOG);
        fragmentTransaction.commit();
      }
    }
  }
}
