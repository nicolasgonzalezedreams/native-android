package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.ui.widgets.base.ButtonWithHint;

/**
 * Created by manuel on 12/10/14.
 */
public class ButtonSwitchForm extends ButtonWithHint {
  private final SwitchCompat switchDefaultPassenger;

  public ButtonSwitchForm(Context context, AttributeSet attrs) {
    super(context, attrs);

    switchDefaultPassenger = (SwitchCompat) findViewById(R.id.switchDefaultPassenger);
  }

  @Override public final int getLayout() {
    return R.layout.layout_form_switch_widget;
  }

  public final boolean isChecked() {
    return switchDefaultPassenger.isChecked();
  }

  public final void setChecked(boolean checked) {
    switchDefaultPassenger.setChecked(checked);
  }
}
