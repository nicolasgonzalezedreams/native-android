package com.odigeo.app.android.lib.models.dto;

public enum ProviderPaymentMethodTypeDTO {

  //    CASH("CASH"),
  //    CREDITCARD("CREDITCARD"),
  //    SECURE_3_D("SECURE3D"),
  //    INSTALLMENT("INSTALLMENT"),
  //    DELAYED_PAYMENT("DELAYED_PAYMENT"),
  //    NO_PAYMENT_NEEDED("NO_PAYMENT_NEEDED");

  // NOTE: In the DTO's for Import Trip has this values changed
  CASH("B"), CREDITCARD("C"), SECURE3D("S"), INSTALLMENT("T"), DELAYED_PAYMENT(
      "N"), NO_PAYMENT_NEEDED("O"), TRAVEL_ACCOUNT("A");

  private final String value;

  ProviderPaymentMethodTypeDTO(String v) {
    value = v;
  }

  public static ProviderPaymentMethodTypeDTO fromValue(String v) {
    for (ProviderPaymentMethodTypeDTO c : ProviderPaymentMethodTypeDTO.values()) {
      if (c.value.equals(v)) {
        return c;
      }
    }
    throw new IllegalArgumentException(v);
  }

  public String value() {
    return value;
  }

}
