package com.odigeo.app.android.view;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.HotelsUrlBuilder;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.navigator.BookingGuideViewerNavigator;
import com.odigeo.app.android.ui.recyclerview.OffsetItemDecoration;
import com.odigeo.app.android.utils.GroundTransportationUrlBuilder;
import com.odigeo.app.android.view.adapters.CrossSellingCardsAdapter;
import com.odigeo.app.android.view.components.ArrivalGuidesProvider;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.custom.GroundTransportationViewBase;
import com.odigeo.app.android.view.custom.TripDetailsGroundTransportationView;
import com.odigeo.app.android.view.helpers.PermissionsHelper;
import com.odigeo.app.android.view.imageutils.BookingImageUtil;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.helper.ABTestHelper;
import com.odigeo.helper.CrossCarUrlHandlerInterface;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.presenter.CrossSellingCardsPresenter;
import com.odigeo.presenter.contracts.navigators.MyTripDetailsNavigatorInterface;
import com.odigeo.presenter.contracts.views.CrossSellingCardsView;
import com.odigeo.presenter.model.CrossSellingCard;
import com.odigeo.tools.DateHelperInterface;
import java.util.Collection;

import static android.support.v7.widget.LinearLayoutManager.VERTICAL;
import static com.odigeo.app.android.lib.consts.Constants.EXTRA_URL_WEBVIEW;
import static com.odigeo.app.android.lib.consts.Constants.WEBVIEW_CARS;
import static com.odigeo.app.android.lib.consts.Constants.WEBVIEW_HOTELS;
import static com.odigeo.app.android.lib.consts.Constants.WEBVIEW_TYPE;
import static com.odigeo.app.android.lib.consts.GAnalyticsNames.LABEL_CROSS_SELL_CAR_CLICKS;
import static com.odigeo.app.android.lib.consts.GAnalyticsNames.LABEL_CROSS_SELL_HOTEL_CLICKS;
import static com.odigeo.app.android.lib.utils.ViewUtils.findById;
import static com.odigeo.app.android.view.constants.OneCMSKeys.TRIP_DETAILS_GROUND_TRANSPORTATION_URL;
import static com.odigeo.app.android.view.constants.OneCMSKeys.WEBVIEWCONTROLLER_GROUND_TRANSPORTATION_TITLE;
import static com.odigeo.app.android.view.helpers.PermissionsHelper.EXTERNAL_STORAGE_REQUEST_CODE;
import static com.odigeo.configuration.ABAlias.TRA_TRIPATT_548;
import static com.odigeo.configuration.ABPartition.TWO;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_XSELL_WIDGET;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_MY_TRIPS_TRIP_INFO;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CLICKED_CROSS_SELL_CARS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CLICKED_CROSS_SELL_HOTELS;
import static com.odigeo.helper.CrossCarUrlHandlerInterface.CAMPAIGN_MY_TRIPS;

public class CrossSellingWidget
    implements CrossSellingCardsView, CrossSellingCardsAdapter.OnCrossSellItemSelected,
    GroundTransportationViewBase.GroundTransportationWidgetListener {

  private static final int DEFAULT_COLUMN_SPAN = 1;
  private static final int EXPANDED_COLUMN_SPAN = 2;
  private static final String XSELL_SCREEN_LABEL_NAME = "mytrips";
  private static final int DAYS_TO_ADD_SIMPLE_FLIGHT = 3;
  private static final String CROSS_DATE_FORMAT = "%s - %s";
  private static final String CROSS_TITLE_FORMAT = "%s %s";

  private final CrossCarUrlHandlerInterface crossCarUrlHandler;
  private final MarketProviderInterface marketProvider;
  private final Activity activity;
  private final ABTestHelper abTestHelper;
  private final Booking booking;
  private final TrackerControllerInterface tracker;
  private final CrossSellingCardsPresenter presenter;
  private final LocalizableProvider localizableProvider;
  private final OdigeoImageLoader<ImageView> imageLoader;
  private final BookingImageUtil bookingImageUtil;

  private final DateHelperInterface dateHelper;
  private final ArrivalGuidesProvider arrivalGuideProvider;

  private RecyclerView xSellCardsRV;
  private CrossSellingCardsAdapter crossSellingCardsAdapter;
  private CrossSellingCard selectedCrossSellingCard;
  private Button mBtnHotel;
  private Button mBtnCar;
  private TextView mTvCrossDate;
  private TextView mTvCrossDescription;
  private TextView mTvCrossTitle;
  private ImageView mIvImgCross;

  public CrossSellingWidget(Activity activity, Booking booking,
      CrossCarUrlHandlerInterface crossCarUrlHandler, MarketProviderInterface marketProvider,
      TrackerControllerInterface trackerController, DateHelperInterface dateHelper,
      ABTestHelper abTestHelper, MyTripDetailsNavigatorInterface navigator) {
    this.activity = activity;
    this.booking = booking;
    this.tracker = trackerController;
    this.crossCarUrlHandler = crossCarUrlHandler;
    this.marketProvider = marketProvider;
    this.localizableProvider = getDependencyInjector().provideLocalizables();
    this.imageLoader = getDependencyInjector().provideImageLoader();
    this.bookingImageUtil = getDependencyInjector().provideMyTripsImageUtil();
    this.abTestHelper = abTestHelper;
    this.dateHelper = dateHelper;
    this.presenter = getDependencyInjector().provideCrossSellingCardsPresenter(this, navigator);
    this.arrivalGuideProvider = new ArrivalGuidesProvider(activity, booking);
  }

  private AndroidDependencyInjector getDependencyInjector() {
    return (AndroidDependencyInjector) application().getDependencyInjector();
  }

  private OdigeoApp application() {
    return ((OdigeoApp) activity.getApplicationContext());
  }

  private View setupOldXSellView() {
    ViewGroup view =
        (ViewGroup) LayoutInflater.from(application()).inflate(R.layout.cross_selling, null, false);

    mBtnHotel = findById(view, R.id.btnHotel);
    mBtnCar = findById(view, R.id.btnCar);
    mTvCrossDate = findById(view, R.id.tvCrossDate);
    mTvCrossDescription = findById(view, R.id.tvCrossDescription);
    mTvCrossTitle = findById(view, R.id.tvCrossTitle);
    mIvImgCross = findById(view, R.id.ivImgCross);

    int primaryColor = ContextCompat.getColor(application(), R.color.primary_brand);
    Drawable hotelIcon = ContextCompat.getDrawable(application(), R.mipmap.icon_xselling_hotel);
    Drawable carIcon = ContextCompat.getDrawable(application(), R.mipmap.icon_xselling_car);
    if (hotelIcon != null) {
      hotelIcon.setColorFilter(primaryColor, PorterDuff.Mode.SRC_IN);
      mBtnHotel.setCompoundDrawablesWithIntrinsicBounds(hotelIcon, null, null, null);
    }
    if (carIcon != null) {
      carIcon.setColorFilter(primaryColor, PorterDuff.Mode.SRC_IN);
      mBtnCar.setCompoundDrawablesWithIntrinsicBounds(carIcon, null, null, null);
    }

    setImage();
    setText();
    setListeners();
    initOneCMSText();

    setupArrivalGuide(view);
    setupGroundTransportationView(view);

    return view;
  }

  private void setupGroundTransportationView(ViewGroup view) {
    String active =
        localizableProvider.getString(OneCMSKeys.TRIP_DETAILS_GROUND_TRANSPORTATION_ACTIVE);
    boolean shouldShowGroundTransportation = Boolean.parseBoolean(active);

    if (shouldShowGroundTransportation) {
      TripDetailsGroundTransportationView tripDetailsGroundTransportationWidget =
          new TripDetailsGroundTransportationView(application(), booking, this);
      tripDetailsGroundTransportationWidget.setId(R.id.ground_transportation_widget);
      view.addView(tripDetailsGroundTransportationWidget);
      tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS_TRIP_INFO,
          TrackerConstants.ACTION_XSELL_WIDGET, TrackerConstants.LABEL_GT_APPEARANCES);
    }
  }

  private void setupArrivalGuide(ViewGroup view) {
    if (!booking.getTripType().equals(Booking.TRIP_TYPE_MULTI_SEGMENT) && presenter.hasArrivalGuide(
        booking.getArrivalGeoNodeId())) {
      view.addView(arrivalGuideProvider.getArrivalGuides());
      arrivalGuideProvider.onStart();
    }
  }

  private void initOneCMSText() {
    mTvCrossTitle.setText(String.format(CROSS_TITLE_FORMAT,
        localizableProvider.getString(OneCMSKeys.MY_TRIPS_CROSS_TITLE),
        booking.getArrivalCityName(0)));

    mTvCrossDescription.setText(Html.fromHtml(
        localizableProvider.getString(OneCMSKeys.MY_TRIPS_CROSS_DESCRIPTION,
            booking.getBuyer().getName())));

    mBtnHotel.setText(localizableProvider.getString(OneCMSKeys.MY_TRIPS_CROSS_BTN_HOTEL));

    mBtnCar.setText(localizableProvider.getString(OneCMSKeys.MY_TRIPS_CROSS_BTN_CAR));
  }

  private void setImage() {
    bookingImageUtil.loadBookingImage(application(), imageLoader, mIvImgCross, booking, false);
  }

  private void setText() {

    mTvCrossDate.setText(String.format(CROSS_DATE_FORMAT,
        dateHelper.millisecondsToDate(booking.getDepartureDate(), "d MMM"),
        dateHelper.millisecondsToDate(booking.getLastDepartureDate(), "d MMM")));
  }

  private void setListeners() {
    mBtnCar.setOnClickListener(new View.OnClickListener() {

      @Override public void onClick(View v) {
        openCars(booking);
      }
    });

    mBtnHotel.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        openHotels(booking);
      }
    });
  }

  private View setupNewXsellView() {
    xSellCardsRV = (RecyclerView) LayoutInflater.from(activity)
        .inflate(R.layout.cross_selling_cards, null, false);
    xSellCardsRV.setNestedScrollingEnabled(false);
    final Collection<CrossSellingCard> cards = presenter.buildXsellCards(booking);
    GridLayoutManager gridLayoutManager =
        new GridLayoutManager(activity, EXPANDED_COLUMN_SPAN, VERTICAL, false);
    gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
      @Override public int getSpanSize(int position) {
        final int count = cards.size();
        if (position == count - 1 && cards.size() % 2 > 0) {
          return EXPANDED_COLUMN_SPAN;
        } else {
          return DEFAULT_COLUMN_SPAN;
        }
      }
    });

    int spacingInPixels =
        activity.getResources().getDimensionPixelSize(R.dimen.mytrip_details_grid_spacing);

    xSellCardsRV.addItemDecoration(new OffsetItemDecoration(spacingInPixels));
    xSellCardsRV.setLayoutManager(gridLayoutManager);
    xSellCardsRV.setHasFixedSize(true);
    crossSellingCardsAdapter = new CrossSellingCardsAdapter(cards, this);
    xSellCardsRV.setAdapter(crossSellingCardsAdapter);

    return xSellCardsRV;
  }

  public View getCrossSelling() {
    return shouldShowNewXSellView() ? setupNewXsellView() : setupOldXSellView();
  }

  private boolean shouldShowNewXSellView() {
    return abTestHelper.getPartition(TRA_TRIPATT_548, 2) == TWO;
  }

  @Override public void onCrossSellingItemSelected(CrossSellingCard card) {
    this.selectedCrossSellingCard = card;
    presenter.onCrossSellingCardSelected(selectedCrossSellingCard);
  }

  @Override public boolean isActive() {
    return xSellCardsRV.isAttachedToWindow();
  }

  @Override public void openCityGuide(Booking booking) {
    Intent intent = new Intent(activity, BookingGuideViewerNavigator.class);
    intent.putExtra(BookingGuideViewerNavigator.EXTRA_BOOKING, booking);
    activity.startActivity(intent);
  }

  @Override public void openCars(Booking booking) {
    tracker.trackAnalyticsEvent(CATEGORY_MY_TRIPS_TRIP_INFO, ACTION_XSELL_WIDGET,
        LABEL_CROSS_SELL_CAR_CLICKS);

    String url = crossCarUrlHandler.getCarUrlWithSearchLoader(booking.getArrivalAirportCode(),
        booking.getArrivalDate(0), booking.getLastDepartureDate(),
        booking.getTravellers().get(0).getDateOfBirth(), CAMPAIGN_MY_TRIPS);

    Bundle extras = new Bundle();
    extras.putString(WEBVIEW_TYPE, WEBVIEW_CARS);
    openWebView(url, extras);

    tracker.trackLocalyticsEvent(CLICKED_CROSS_SELL_CARS);
  }

  private void openWebView(String url, Bundle extras) {
    OdigeoApp odigeoApp = (OdigeoApp) activity.getApplicationContext();
    Intent intent = new Intent(activity, odigeoApp.getWebViewActivityClass());
    intent.putExtra(EXTRA_URL_WEBVIEW, url);
    intent.putExtras(extras);
    activity.startActivity(intent);
  }

  @Override public void openGroundTransportation(Booking booking) {

    String rawUrl = localizableProvider.getString(TRIP_DETAILS_GROUND_TRANSPORTATION_URL);

    Bundle extras = new Bundle();
    extras.putString(Constants.TITLE_WEB_ACTIVITY,
        localizableProvider.getString(WEBVIEWCONTROLLER_GROUND_TRANSPORTATION_TITLE));
    extras.putBoolean(Constants.SHOW_HOME_ICON, true);

    openWebView(new GroundTransportationUrlBuilder(booking).build(rawUrl), extras);

    tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS_TRIP_INFO,
        TrackerConstants.ACTION_XSELL_WIDGET, TrackerConstants.LABEL_GT_ON_CLICK);
  }

  @Override public void openHotels(Booking booking) {

    tracker.trackAnalyticsEvent(CATEGORY_MY_TRIPS_TRIP_INFO, ACTION_XSELL_WIDGET,
        LABEL_CROSS_SELL_HOTEL_CLICKS);

    HotelsUrlBuilder hotelsUrlBuilder;
    if (booking.getNumberOfKids() > 0) {
      // Redirects to home or filter page.
      hotelsUrlBuilder = new HotelsUrlBuilder(false, XSELL_SCREEN_LABEL_NAME, marketProvider);
      hotelsUrlBuilder.setParametersForFilters(booking);
    } else {
      // Redirects to search results.
      hotelsUrlBuilder = new HotelsUrlBuilder(booking, XSELL_SCREEN_LABEL_NAME, marketProvider);
    }

    if (booking.getTripType().equalsIgnoreCase(Booking.TRIP_TYPE_ONE_WAY)) {
      hotelsUrlBuilder.setCheckOutDateValues(
          OdigeoDateUtils.addDaysToDate(OdigeoDateUtils.createDate(booking.getArrivalDate(0)),
              DAYS_TO_ADD_SIMPLE_FLIGHT));
    }

    Bundle extras = new Bundle();
    extras.putString(WEBVIEW_TYPE, WEBVIEW_HOTELS);
    openWebView(hotelsUrlBuilder.getUrl(), extras);

    tracker.trackLocalyticsEvent(CLICKED_CROSS_SELL_HOTELS);
  }

  @Override public void trackGroundTransportationCardAppearance() {
    tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS_TRIP_INFO,
        TrackerConstants.ACTION_XSELL_WIDGET, TrackerConstants.LABEL_GT_APPEARANCES);
  }

  // TODO: remove this once AB test is finished and if the new design should stay
  @Override public void showDownloadProgress(double progress, double totalDownloadSize) {
  }

  @Override public void showNoConnectionDialog() {

  }

  @Override public void showDownloadFailedDialog() {

  }

  @Override public void showInsufficientSpaceDialog() {

  }

  @Override public void showGuideDownloading() {
    crossSellingCardsAdapter.showGuideDownloading();
  }

  @Override public void hideGuideDownloading() {
    crossSellingCardsAdapter.hideGuideDownloading();
  }

  @Override public boolean checkStoragePermission() {
    return PermissionsHelper.askForPermissionIfNeeded(Manifest.permission.WRITE_EXTERNAL_STORAGE,
        activity, OneCMSKeys.PERMISSION_STORAGE_TRAVELGUIDE_MESSAGE, EXTERNAL_STORAGE_REQUEST_CODE);
  }

  @Override public void onStoragePermissionAccepted() {
    if(shouldShowNewXSellView()) {
      onCrossSellingItemSelected(selectedCrossSellingCard);
    }else{
      arrivalGuideProvider.downloadGuide();
    }
  }

  public void onDetachView() {
    if (!shouldShowNewXSellView()) {
      arrivalGuideProvider.onStop();
    }
  }

  public void deleteGuide() {
    arrivalGuideProvider.deleteGuide();
  }

  @Override public void navigateToGroundTransportation(String url) {
    presenter.navigateToGroundTransportation(url);
  }

  public void onAttachView() {
    if (!shouldShowNewXSellView()) {
      arrivalGuideProvider.onStart();
    }
  }
}