package com.odigeo.app.android.lib.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.models.RowItemListFilters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by luis.bejarano on 10/2/14.
 */
@Deprecated public class ListViewFiltersAdapter extends ArrayAdapter<RowItemListFilters> {

  private final Context context;
  private final Map<Integer, RowItemListFilters> selectedRows;

  private boolean isAllSelected = false;

  public ListViewFiltersAdapter(Context context, int resourceId, List<RowItemListFilters> items) {
    super(context, resourceId, items);
    this.context = context;
    selectedRows = new HashMap<Integer, RowItemListFilters>();
  }

  public final View getView(int position, View convertView, ViewGroup parent) {

    ViewHolder holder = null;
    View view = convertView;
    final RowItemListFilters rowItem = getItem(position);

    LayoutInflater mInflater =
        (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

    if (view == null) {

      view = mInflater.inflate(R.layout.list_item_filters, parent, false);

      holder = new ViewHolder();

      holder.txtTitle = (TextView) view.findViewById(R.id.txtTitleFiltersAir);
      holder.checkBox = (CheckBox) view.findViewById(R.id.checkBoxFiltersAir);
      holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

        @Override public void onCheckedChanged(CompoundButton arg0, boolean selected) {

          rowItem.setSelected(selected);

          if (selected) {
            selectedRows.put(rowItem.getIdAir(), rowItem);
          } else {
            selectedRows.remove(rowItem.getIdAir());
          }
        }
      });

      holder.checkBox.setChecked(rowItem.isSelected());

      view.setTag(holder);
    } else {
      holder = (ViewHolder) view.getTag();
    }

    holder.txtTitle.setText(rowItem.getTitle());
    holder.checkBox.setChecked(isAllSelected);

    return view;
  }

  public final void isAllSelected(Boolean value) {

    int count = getCount();

    if (count >= 1) {

      isAllSelected = value;

      for (int i = 0; i < count; i++) {
        getItem(i).setSelected(isAllSelected);
      }

      notifyDataSetChanged();
    }
  }

  public final boolean isAllSelected() {
    return isAllSelected;
  }

  public final void setAllSelected(boolean isAllSelected) {
    this.isAllSelected = isAllSelected;
  }

  public final List<RowItemListFilters> getSelectedItems() {
    return new ArrayList<RowItemListFilters>(selectedRows.values());
  }

  public final Integer[] getSelectedAirIds() {

    Set<Integer> keys = selectedRows.keySet();
    Integer[] intKeys = new Integer[keys.size()];
    keys.toArray(intKeys);

    return intKeys;
  }

  private class ViewHolder {

    TextView txtTitle;
    CheckBox checkBox;
  }
}
