package com.odigeo.app.android.view.custom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageRequest;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.LinkedHashMap;
import java.util.Map;

public class HomeBackground extends FrameLayout {

  private ImageView mIVBackgroundImage;
  private Map<String, Bitmap> mImages;

  public HomeBackground(Context context) {
    super(context);
    initialize(context);
  }

  public HomeBackground(Context context, AttributeSet attrs) {
    super(context, attrs);
    initialize(context);
  }

  public HomeBackground(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    initialize(context);
  }

  private void initialize(Context context) {
    mIVBackgroundImage = new ImageView(context);
    mIVBackgroundImage.setAdjustViewBounds(true);
    HorizontalScrollView scrollView = new HorizontalScrollView(context);
    scrollView.addView(mIVBackgroundImage);

    scrollView.setOnTouchListener(new OnTouchListener() {
      @Override public boolean onTouch(View v, MotionEvent event) {
        return true;
      }
    });

    scrollView.setFillViewport(false);
    scrollView.setFadingEdgeLength(0);
    addView(scrollView);

    mImages = new LinkedHashMap<>();
    setDefaultBackground();
  }

  private void setDefaultBackground() {
    setImageAsBackground(R.drawable.default_empty_trip_home_widget, false);
  }

  public void setCampaignBackground() {
    setImageAsBackground(R.drawable.background_campaign_cards, true);
  }

  private void setImageAsBackground(int imageId, boolean isTile) {
    mIVBackgroundImage.setImageDrawable(null);
    BitmapDrawable bitmapDrawable =
        new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), imageId));
    if (isTile) {
      bitmapDrawable.setTileModeX(Shader.TileMode.REPEAT);
    }

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
      setBackground(bitmapDrawable);
    } else {
      setBackgroundDrawable(bitmapDrawable);
    }
  }

  public void setCityImageBackground(String imageUrl) {

    if (imageUrl.isEmpty()) {
      setDefaultImage();
    } else if (mImages.get(imageUrl) != null) {
      mIVBackgroundImage.setImageBitmap(mImages.get(imageUrl));
    } else {
      ImageRequest imageRequest = loadRemoteImage(imageUrl);
      RequestHelper requestHelper = AndroidDependencyInjector.getInstance().provideRequestHelper();
      requestHelper.addImageRequest(imageRequest);
    }
  }

  private ImageRequest loadRemoteImage(final String url) {
    return new ImageRequest(url, new Response.Listener<Bitmap>() {

      @Override public void onResponse(Bitmap bitmap) {
        mImages.put(url, bitmap);
        mIVBackgroundImage.setImageBitmap(bitmap);
      }
    }, 0, 0, Bitmap.Config.ARGB_8888, null);
  }

  private void setXValue(float position) {
    mIVBackgroundImage.setX(position);
  }

  public void translatePosition(float positionOffset) {
    int transitionOffsetMultiplier = 100;
    setXValue(positionOffset * transitionOffsetMultiplier);
  }

  public void setDefaultImage() {
    mIVBackgroundImage.setImageDrawable(null);
  }
}
