package com.odigeo.app.android.lib.models;

import android.support.annotation.Nullable;
import com.odigeo.app.android.lib.models.dto.FrequentFlyerCardCodeDTO;
import com.odigeo.app.android.lib.models.dto.TravellerGenderDTO;
import com.odigeo.app.android.lib.models.dto.TravellerTitleDTO;
import com.odigeo.app.android.lib.models.dto.TravellerTypeDTO;
import com.odigeo.data.entity.BaseSpinnerItem;
import com.odigeo.data.entity.booking.Country;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Crux on 01/10/2014.
 */
public class Passenger implements Serializable, BaseSpinnerItem {

  private Integer id;
  private TravellerTypeDTO travellerType;
  private TravellerTitleDTO titleName;
  private String name;
  private String middleName;
  private String firstLastName;
  private String secondLastName;
  private TravellerGenderDTO gender;
  private Country nationality;
  private Country residenceCountry;
  private String mealName;
  private String localityCodeOfResidence;
  private Date birthDate;
  private List<FrequentFlyerCardCodeDTO> frequentFlyerAirlineCodes;
  private String cityName;
  private String phoneNumber;
  private String zipCode;
  private String cpf;
  private String stateName;
  private String address;
  private Country countryPhoneNumber;
  private String mail;
  private boolean passengerDefault;
  private List<PassengerIdentification> identifications;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public String getFirstLastName() {
    return firstLastName;
  }

  public void setFirstLastName(String firstLastName) {
    this.firstLastName = firstLastName;
  }

  public String getSecondLastName() {
    return secondLastName;
  }

  public void setSecondLastName(String secondLastName) {
    this.secondLastName = secondLastName;
  }

  public String getMealName() {
    return mealName;
  }

  public void setMealName(String mealName) {
    this.mealName = mealName;
  }

  public String getLocalityCodeOfResidence() {
    return localityCodeOfResidence;
  }

  public void setLocalityCodeOfResidence(String localityCodeOfResidence) {
    this.localityCodeOfResidence = localityCodeOfResidence;
  }

  public List<FrequentFlyerCardCodeDTO> getFrequentFlyerAirlineCodes() {
    return frequentFlyerAirlineCodes;
  }

  public void setFrequentFlyerAirlineCodes(
      List<FrequentFlyerCardCodeDTO> frequentFlyerAirlineCodes) {
    this.frequentFlyerAirlineCodes = frequentFlyerAirlineCodes;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getCpf() {
    return cpf;
  }

  public void setCpf(String cpf) {
    this.cpf = cpf;
  }

  public String getStateName() {
    return stateName;
  }

  public void setStateName(String stateName) {
    this.stateName = stateName;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public boolean isPassengerDefault() {
    return passengerDefault;
  }

  public void setPassengerDefault(boolean passengerDefault) {
    this.passengerDefault = passengerDefault;
  }

  public Country getNationality() {
    return nationality;
  }

  public void setNationality(Country nationality) {
    this.nationality = nationality;
  }

  public Country getResidenceCountry() {
    return residenceCountry;
  }

  public void setResidenceCountry(Country residenceCountry) {
    this.residenceCountry = residenceCountry;
  }

  public Date getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  public Country getCountryPhoneNumber() {
    return countryPhoneNumber;
  }

  public void setCountryPhoneNumber(Country countryPhoneNumber) {
    this.countryPhoneNumber = countryPhoneNumber;
  }

  public List<PassengerIdentification> getIdentifications() {
    return identifications;
  }

  public void setIdentifications(List<PassengerIdentification> identifications) {
    this.identifications = identifications;
  }

  public TravellerGenderDTO getGender() {
    return gender;
  }

  public void setGender(TravellerGenderDTO gender) {
    this.gender = gender;
  }

  @Override public boolean equals(Object passenger) {
    boolean areEquals = true;

    if (/*passenger == null ||*/ !(passenger instanceof Passenger)) {
      areEquals = false;
    } else if (areEquals) {
      Passenger passengerP = (Passenger) passenger;

      if (!(this.getId() != null
          && passengerP.getId() != null
          && this.getId().intValue() != 0
          && passengerP.getId().intValue() != 0
          && this.getId().intValue() == passengerP.getId().intValue())) {
        //Are equals by the id
        //            } else {
        //                They have different id's
        areEquals = equalsDifferentIDs(passengerP);
      }
    }
    return areEquals;
  }

  /**
   * This method analizes if the passengers are equal but have different ids.
   *
   * @param passengerP passanger compared
   * @return true if they're equal
   */
  private boolean equalsDifferentIDs(Passenger passengerP) {
    boolean areEquals = true;
    if (!areObjectEquals(this.getName(), passengerP.getName())) {
      areEquals = false;
    } else if (!areObjectEquals(this.getFirstLastName(), passengerP.getFirstLastName())) {
      areEquals = false;
    }
    if (this.getTitleName() != passengerP.getTitleName()) {
      areEquals = false;
    }
    return areEquals;
  }

  private boolean areObjectEquals(Object object1, Object object2) {
    boolean areEquals = areObjectsNull(object1, object2);

    if (areEquals) {
      //The two objects are not null
      if (object1 != null && object2 != null) {
        if (!object1.equals(object2)) {
          areEquals = false;
        }
      } else if (object1 != null || object2 != null) {
        //Almost one object is null, but the other is not null
        areEquals = false;
      }
    }

    return areEquals;
  }

  /**
   * This method checks if the objects compared are null
   *
   * @param object1 object being compared
   * @param object2 object being compared
   * @return if they are null or not
   */
  private boolean areObjectsNull(Object object1, Object object2) {
    if (object1 instanceof Integer && object2 instanceof Integer) {
      if (((Integer) object2 == 0)) {
        return false;
      } else if (((Integer) object1 == 0)) {
        return false;
      }
    }
    return true;
  }

  public TravellerTypeDTO getTravellerType() {
    return travellerType;
  }

  public void setTravellerType(TravellerTypeDTO travellerType) {
    this.travellerType = travellerType;
  }

  public TravellerTitleDTO getTitleName() {
    return titleName;
  }

  public void setTitleName(TravellerTitleDTO titleName) {
    this.titleName = titleName;
  }

  @Override public String getShownTextKey() {
    return EMPTY_STRING;
  }

  @Override public int getImageId() {
    return EMPTY_RESOURCE;
  }

  @Nullable @Override public String getShownText() {
    return getName();
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public Object getData() {
    return null;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }
}
