package com.odigeo.app.android.lib.models.dto.mappers;

import com.odigeo.app.android.lib.models.Passenger;
import com.odigeo.app.android.lib.models.PassengerIdentification;
import com.odigeo.app.android.lib.models.dto.FrequentFlyerCardCodeDTO;
import com.odigeo.app.android.lib.models.dto.TravellerTitleDTO;
import com.odigeo.app.android.lib.models.dto.TravellerTypeDTO;
import com.odigeo.data.entity.userData.UserAddress;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserProfile.Title;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.ArrayList;
import java.util.List;

public class TravellerFromOldVersionMapper {

  public UserTraveller mapperFromPassenger(Passenger passenger) {

    UserTraveller.TypeOfTraveller typeOfTraveller =
        getTypeOfTraveller(passenger.getTravellerType());

    List<UserFrequentFlyer> userFrequentFlyers =
        getUserFrequentFlyers(passenger.getFrequentFlyerAirlineCodes());

        /* When migratin old travellers, some times identification list is null */
    List<UserIdentification> userIdentificationList = new ArrayList<>();
    if (passenger.getIdentifications() != null) {
      userIdentificationList = getUserIdentifications(passenger.getIdentifications());
    }

    Title title = getTitle(passenger.getTitleName());

    UserAddress userAddress = getUserAddress(passenger);

    String gender = passenger.getGender() != null ? passenger.getGender().getShownText() : "";

    Long birthDate = passenger.getBirthDate() != null ? passenger.getBirthDate().getTime() : 0;

    String phoneNumberCountryCode =
        passenger.getCountryPhoneNumber() != null ? passenger.getCountryPhoneNumber()
            .getCountryCode() : "";

    String nationalityCountryCode =
        passenger.getNationality() != null ? passenger.getNationality().getCountryCode() : "";

    UserProfile userProfile =
        new UserProfile(-1, -1, gender, title, passenger.getName(), passenger.getMiddleName(),
            passenger.getFirstLastName(), passenger.getSecondLastName(), birthDate,
            phoneNumberCountryCode, passenger.getPhoneNumber(), "", "", "", nationalityCountryCode,
            passenger.getCpf(), passenger.isPassengerDefault(), passenger.getImageUrl(),
            userAddress, userIdentificationList, -1);

    return new UserTraveller(-1, -1, false, passenger.getMail(), typeOfTraveller, null,
        userFrequentFlyers, userProfile, -1);
  }

  private UserAddress getUserAddress(Passenger passenger) {

    String residenceCountry;
    if (passenger.getResidenceCountry() != null) {
      residenceCountry = passenger.getResidenceCountry().getName();
    } else {
      residenceCountry = "";
    }

    return new UserAddress(-1, -1, passenger.getAddress(), "", passenger.getCityName(),
        residenceCountry, passenger.getStateName(), passenger.getZipCode(), false, "", -1);
  }

  private Title getTitle(TravellerTitleDTO titleName) {
    Title title;
    try {
      switch (titleName) {
        case MR:
          title = Title.MR;
          break;
        case MRS:
          title = Title.MRS;
          break;
        case MS:
          title = Title.MS;
          break;
        default:
          title = Title.UNKNOWN;
          break;
      }
    } catch (NullPointerException e) {
            /* Sometimes the title of the traveller during migration is null. In this cases, we need
            to use the Unknown state */
      title = Title.UNKNOWN;
    }

    return title;
  }

  private UserTraveller.TypeOfTraveller getTypeOfTraveller(TravellerTypeDTO travellerType) {

    UserTraveller.TypeOfTraveller typeOfTraveller;
    try {
      switch (travellerType) {
        case ADULT:
          typeOfTraveller = UserTraveller.TypeOfTraveller.ADULT;
          break;
        case CHILD:
          typeOfTraveller = UserTraveller.TypeOfTraveller.CHILD;
          break;
        case INFANT:
          typeOfTraveller = UserTraveller.TypeOfTraveller.INFANT;
          break;
        default:
          typeOfTraveller = UserTraveller.TypeOfTraveller.UNKNOWN;
          break;
      }
    } catch (NullPointerException e) {
      typeOfTraveller = UserTraveller.TypeOfTraveller.UNKNOWN;
    }
    return typeOfTraveller;
  }

  private List<UserFrequentFlyer> getUserFrequentFlyers(
      List<FrequentFlyerCardCodeDTO> frequentFlyerAirlineCodes) {

    List<UserFrequentFlyer> userFrequentFlyers = new ArrayList<>();
    if (frequentFlyerAirlineCodes != null) {
      for (FrequentFlyerCardCodeDTO frequentFlyerCardCodeDTO : frequentFlyerAirlineCodes) {

        UserFrequentFlyer userFrequentFlyer =
            new UserFrequentFlyer(0, 0, frequentFlyerCardCodeDTO.getCarrierCode(),
                frequentFlyerCardCodeDTO.getPassengerCardNumber(), 0);

        userFrequentFlyers.add(userFrequentFlyer);
      }
    }
    return userFrequentFlyers;
  }

  private List<UserIdentification> getUserIdentifications(
      List<PassengerIdentification> identifications) {

    List<UserIdentification> userIdentificationList = new ArrayList<>();
    for (PassengerIdentification passengerIdentification : identifications) {

      UserIdentification.IdentificationType identificationType;
      try {
        switch (passengerIdentification.getIdentificationType()) {
          case BIRTH_DATE:
            identificationType = UserIdentification.IdentificationType.BIRTH_DATE;
            break;
          case CIF:
            identificationType = UserIdentification.IdentificationType.CIF;
            break;
          case EMPTY:
            identificationType = UserIdentification.IdentificationType.UNKNOWN;
            break;
          case NATIONAL_ID_CARD:
            identificationType = UserIdentification.IdentificationType.NATIONAL_ID_CARD;
            break;
          case NIE:
            identificationType = UserIdentification.IdentificationType.NIE;
            break;
          case NIF:
            identificationType = UserIdentification.IdentificationType.NIF;
            break;
          case PASSPORT:
            identificationType = UserIdentification.IdentificationType.PASSPORT;
            break;
          default:
            identificationType = UserIdentification.IdentificationType.UNKNOWN;
            break;
        }
      } catch (NullPointerException e) {
        identificationType = UserIdentification.IdentificationType.UNKNOWN;
      }

      String countryCode =
          passengerIdentification.getCountry() != null ? passengerIdentification.getCountry()
              .getCountryCode() : "";

      Long expirationDate = passengerIdentification.getExpirationDate() != null
          ? passengerIdentification.getExpirationDate().getTime() : 0;

      UserIdentification userIdentification =
          new UserIdentification(-1, -1, passengerIdentification.getNumber(), countryCode,
              expirationDate, identificationType, -1);

      userIdentificationList.add(userIdentification);
    }
    return userIdentificationList;
  }
}