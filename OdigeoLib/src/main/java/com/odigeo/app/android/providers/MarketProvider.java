package com.odigeo.app.android.providers;

import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.interactors.provider.MarketProviderInterface;

public class MarketProvider implements MarketProviderInterface {

  private final Configuration configuration;
  private final EmailProvider emailProvider;

  public MarketProvider(Configuration configuration, EmailProvider emailProvider) {
    this.configuration = configuration;
    this.emailProvider = emailProvider;
  }

  @Override public String getWebsite() {
    return configuration.getCurrentMarket().getWebsite();
  }

  @Override public String getLocale() {
    return configuration.getCurrentMarket().getLocale();
  }

  @Override public String getFeedbackEmail() {
    return emailProvider.getFeedbackEmail();
  }

  @Override public String getBrand() {
    return configuration.getBrand();
  }

  @Override public String getBrandVisualName() {
    return configuration.getBrandVisualName();
  }

  @Override public String getBrandKey() {
    return configuration.getBrandKey();
  }

  @Override public int getAdultsByDefault() {
    return configuration.getGeneralConstants().getDefaultNumberOfAdults();
  }

  @Override public int getKidsByDefault() {
    return configuration.getGeneralConstants().getDefaultNumberOfKids();
  }

  @Override public int getInfantsByDefault() {
    return configuration.getGeneralConstants().getDefaultNumberOfInfants();
  }

  @Override public String getLanguage() {
    return configuration.getCurrentMarket().getLanguage();
  }

  @Override public String getMarketKey() {
    return configuration.getCurrentMarket().getKey().toLowerCase();
  }

  @Override public String getCrossSellingMarketKey() {
    return configuration.getCurrentMarket().getCrossSellingMarketKey();
  }

  @Override public String getCurrencyKey() {
    return configuration.getCurrentMarket().getCurrencyKey();
  }

  @Override public String getLocalizedCurrencyValue(double value) {
    return LocaleUtils.getLocalizedCurrencyValue(value, getLocale());
  }

  @Override public String getName() {
    return configuration.getCurrentMarket().getName();
  }

  @Override public String getAid() {
    return configuration.getAid();
  }

  @Override public String getHotelsLabel() {
    return configuration.getHotelsLabel();
  }

  @Override public String getxSellingSearchResultsHotelsUrl() {
    return configuration.getCurrentMarket().getxSellingSearchResultsHotelsUrl();
  }

  @Override public String getxSellingFilterHotelsUrl() {
    return configuration.getCurrentMarket().getxSellingFilterHotelsUrl();
  }

  @Override public String getWebViewHotelsURL() {
    return configuration.getWebViewHotelsURL();
  }

  @Override public String getOneCMSTableForCurrentMarket() {
    return configuration.getCurrentMarket().getOneCMSTable();
  }

  @Override public String getAppStoreUrl() {
    return configuration.getAppStoreURL();
  }

  @Override public String getProductionUrl() {
    return configuration.getProdDomain();
  }
}
