package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.ui.widgets.base.ButtonWithAndWithoutHint;

/**
 * Created by manuel on 12/10/14.
 */
public class ButtonForm extends ButtonWithAndWithoutHint {
  public ButtonForm(Context context, AttributeSet attrs) {
    super(context, attrs);

    setClickable(true);
  }

  @Override public final int getEmptyLayout() {
    return R.layout.layout_form_text_field_empty;
  }

  @Override public final int getLayout() {
    return R.layout.layout_form_text_field;
  }

  public final void setTextEmpty(String textEmpty) {
    super.inflateEmptyLayout();
    getTxtVwText().setText(textEmpty);
  }
}
