package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.ui.widgets.base.TwoTextWithImage;

/**
 * Created by Irving Lóp on 11/10/2014.
 */
public class ConfirmationInfoItem2Texts extends TwoTextWithImage {
  public ConfirmationInfoItem2Texts(Context context) {
    super(context);
  }

  public ConfirmationInfoItem2Texts(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected final int getLayout() {
    return R.layout.layout_confirmation_item_row;
  }

  @Override protected final int getIdTextView() {
    return R.id.txtButtonWithHint_text;
  }

  @Override protected final int getIdSecondTextView() {
    return R.id.txtButtonWithHint_hint;
  }

  @Override protected final int getIdImageView() {
    return R.id.imgTravellerType;
  }
}
