package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

import android.util.Log;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;

public class BaseValidator {

  private static final String TAG = "BaseValidator";

  public BaseValidator() {
  }

  public boolean checkField(String text, String regex) {
    return text != null
        && text.replace(" ", "").length() > 0
        && validateLatinCharset(text)
        && text.matches(regex);
  }

  public boolean validateLatinCharset(String value) {
    String[] codingAccepted = { "ISO_8859_1", "ISO_8859_2", "ISO_8859_9", "ISO_8859_15" };
    boolean isAccepted = false;
    for (String charset : codingAccepted) {
      try {
        if (Charset.forName(charset).newEncoder().canEncode(value)) {
          isAccepted = true;
        }
      } catch (UnsupportedCharsetException e) {
        Log.e(TAG, "Couldn't validate in base validator");
      }
    }
    return isAccepted;
  }
}
