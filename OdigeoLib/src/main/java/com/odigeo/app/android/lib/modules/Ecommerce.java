package com.odigeo.app.android.lib.modules;

import android.util.Log;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.Section;
import com.odigeo.app.android.lib.models.dto.InsuranceTypeDTO;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.shoppingCart.CollectionMethod;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.FeeDetails;
import com.odigeo.data.entity.shoppingCart.InsuranceShoppingItem;
import com.odigeo.data.entity.shoppingCart.ShoppingCart;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.tracker.Product;
import com.odigeo.data.entity.tracker.Transaction;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

/**
 * Log ecommerce operations to GA
 *
 * @author Manuel Ortiz
 * @author Javier Silva
 * @since 11/20/2014
 */
public final class Ecommerce {

  private static final SimpleDateFormat DATE_FORMAT =
      new SimpleDateFormat(Constants.OdigeoDateFormat.GA_FORMAT, new Locale("en", "US"));

  static {
    DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("GMT"));
  }

  private Ecommerce() {

  }

  private static String productCode(List<InsuranceShoppingItem> insuranceShoppingItems,
      SearchOptions searchOption) {
    String code = "";

    if (searchOption != null
        && searchOption.getFlightSegments() != null
        && !searchOption.getFlightSegments().isEmpty()) {

      boolean areSelectableInsurances = false;

      if (insuranceShoppingItems != null) {
        for (InsuranceShoppingItem insurance : insuranceShoppingItems) {
          if (insurance.isSelectable()) {
            areSelectableInsurances = true;
          }
        }
      }

      if (areSelectableInsurances) {
        code = "FI+IN";
      } else {
        code = "FI";
      }
    }

    return code;
  }

  private static String flightType(SearchOptions searchOption) {
    String type = "";

    if (searchOption.getTravelType() == TravelType.SIMPLE) {
      type = "O";
    } else if (searchOption.getTravelType() == TravelType.ROUND) {
      type = "R";
    } else if (searchOption.getTravelType() == TravelType.MULTIDESTINATION) {
      type = "C";
    }

    return type;
  }

  private static String insuranceType(InsuranceShoppingItem insuranceShoppingItem) {
    String type;

    if (insuranceShoppingItem.getInsurance().getType().equals(InsuranceTypeDTO.C.name())) {
      type = "Cancellation";
    } else if (insuranceShoppingItem.getInsurance().getType().equals(InsuranceTypeDTO.CA.name())) {
      type = "Assistance";
    } else {
      type = "Multirisk";
    }

    return type;
  }

  private static String paymentMethod(CollectionMethod paymentMethod) {
    String paymentType = "";

    if (paymentMethod.getType() == CollectionMethodType.CREDITCARD) {
      paymentType = "CREDIT_CARD";
    } else if (paymentMethod.getType() == CollectionMethodType.BANKTRANSFER) {
      paymentType = "BANK_TRANSFER";
    }

    return paymentType;
  }

  public static String[] carriers(SearchOptions searchOptions) {
    Set<String> airlines = new HashSet<String>();
    for (FlightSegment segment : searchOptions.getFlightSegments()) {
      for (Section section : segment.getSections()) {
        airlines.add(section.getCarrier());
      }
    }

    return airlines.toArray(new String[] {});
  }

  /**
   * Creates the string to be sent to google analytics
   *
   * @param originSegment Origin segment to send to analytics, this will used to get departure
   * section city name
   * and date
   * @param destinationSegment Arrival segment to send to analytics, this will used to get segment
   * the city name and
   * date
   * @param paymentMethod Payment method used in the transaction
   * @param searchOptions Search options used in the transaction
   * @return The string formatted for GA as: ORIGIN-DESTINATION-DEPARTURE_DATE-RETURN_DATE-FLIGHT_TYPE-#PAX-#ADULTS-#CHILDREN-#INFANTS-PAYMENT_METHOD
   */
  public static String categoryForSegment(FlightSegment originSegment,
      FlightSegment destinationSegment, CollectionMethod paymentMethod,
      SearchOptions searchOptions) {

    String dash = "-";
    String departureDate = "";
    String destinationCity;
    Section origin = originSegment.getFirstSection();
    Section destination;
    //Get destination values for round trips
    if (searchOptions.getTravelType() == TravelType.ROUND) {
      //For round trips get the departure date and city name form first section of the last segment (return segment)
      destination = destinationSegment.getFirstSection();
      departureDate = dash + formatSyncDate(destination.getDepartureDate());

      destinationCity = destination.getLocationFrom().getCityIataCode();
    } else {
      //For simple and multidestination trips, get the last section and the final location city name
      destination = destinationSegment.getLastSection();
      destinationCity = destination.getLocationTo().getCityIataCode();
    }

    StringBuilder segmentBuilder = new StringBuilder(origin.getLocationFrom().getCityIataCode());
    segmentBuilder.append(dash).append(destinationCity);
    segmentBuilder.append(dash).append(formatSyncDate(origin.getDepartureDate()));
    segmentBuilder.append(departureDate);

    segmentBuilder.append(dash).append(flightType(searchOptions));
    segmentBuilder.append(dash).append(searchOptions.getTotalPassengers());
    segmentBuilder.append(dash).append(searchOptions.getNumberOfAdults());
    segmentBuilder.append(dash).append(searchOptions.getNumberOfKids());
    segmentBuilder.append(dash).append(searchOptions.getNumberOfBabies());
    segmentBuilder.append(dash).append(paymentMethod(paymentMethod));

    Log.d(Constants.TAG_LOG, "Analytics Segment: " + segmentBuilder.toString());
    return segmentBuilder.toString();
  }

  private static synchronized String formatSyncDate(long time) {
    return DATE_FORMAT.format(OdigeoDateUtils.createDate(time));
  }

  private static String categoryForInsurance(InsuranceShoppingItem insuranceShoppingItem,
      SearchOptions searchOptions) {
    return String.format("%s-%d", insuranceType(insuranceShoppingItem),
        searchOptions.getTotalPassengers());
  }

  /**
   * This method logs the different status of the shopping cart
   *
   * @param oldMyShoppingCart shopping cart to log
   */
  private static void logStatus(ShoppingCart oldMyShoppingCart) {
    if (oldMyShoppingCart == null) {
      Log.d(Constants.TAG_LOG, "shoppingCartDTO is null [createGATransaction()]");
    } else if (oldMyShoppingCart.getPrice() == null) {
      Log.d(Constants.TAG_LOG, "shoppingCartDTO.getUnitPrice() is null [createGATransaction()]");
    } else if (oldMyShoppingCart.getPrice().getFeeInfo() == null) {
      Log.d(Constants.TAG_LOG,
          "shoppingCartDTO.getUnitPrice().getFeeInfo() is null [createGATransaction()]");
    } else if (oldMyShoppingCart.getPrice().getFeeInfo().getPaymentFee() == null) {
      Log.d(Constants.TAG_LOG,
          "shoppingCartDTO.getUnitPrice().getFeeInfo().getPaymentFee() is null [createGATransaction()]");
    }
  }

  public static Transaction createGATransaction(SearchOptions searchOptions,
      ShoppingCart shoppingCart, List<InsuranceShoppingItem> insuranceShoppingItems,
      BigDecimal marketingRevenue, String currency,
      ShoppingCartCollectionOption shoppingCartCollectionOption) {
    BigDecimal revenue = marketingRevenue.divide(BigDecimal.TEN);   //10%

    //Creates the transaction
    Transaction transaction = new Transaction(String.valueOf(shoppingCart.getBookingId()));
    transaction.setProductList(new ArrayList<Product>());
    transaction.setAffiliation(affiliation(searchOptions));
    transaction.setRevenue(revenue.doubleValue());
    transaction.setCurrencyCode(currency);

    logStatus(shoppingCart);

    FeeDetails feeInfo = shoppingCart.getPrice().getFeeInfo().getPaymentFee();
    double paymentFeeTotal = feeInfo.getTax().doubleValue() - feeInfo.getDiscount().doubleValue();
    transaction.setShipping(paymentFeeTotal);

    //Where the taxes are taken?
    transaction.setTax(shoppingCart.getCollectionTotalFee().doubleValue());

    if (shoppingCartCollectionOption == null) {
      Log.d(Constants.TAG_LOG,
          "ECOMMERCE oldMyShoppingCart.getPaymentMethodMethodWithPriceSelected() is null");
    } else if (shoppingCartCollectionOption.getMethod() == null) {
      Log.d(Constants.TAG_LOG,
          "ECOMMERCE oldMyShoppingCart.getPaymentMethodMethodWithPriceSelected().getCollectionMethod() is null");
    } else {
      Log.d(Constants.TAG_LOG, "ECOMMERCE sin valores NULL");
    }

    //If it is multidestination, create a product for each segment
    List<FlightSegment> flightSegments = searchOptions.getFlightSegments();
    if (searchOptions.getTravelType() == TravelType.MULTIDESTINATION) {

      int flightIndex = 1;

      for (FlightSegment segment : flightSegments) {
        BigDecimal price = BigDecimal.ZERO;

        //The price is set only for the first segment
        if (flightIndex == 1) {
          price = shoppingCartCollectionOption.getFee();
        }

        String category =
            categoryForSegment(segment, segment, shoppingCartCollectionOption.getMethod(),
                searchOptions);
        Product product = createProduct(shoppingCart, insuranceShoppingItems, searchOptions,
            String.format("FLIGHT-%d", flightIndex), currency, price.doubleValue(), category);

        transaction.getProductList().add(product);

        flightIndex++;
      }
    } else if (searchOptions.getTravelType() == TravelType.SIMPLE
        || searchOptions.getTravelType() == TravelType.ROUND) {

      //Creates a single product
      FlightSegment departureSegment = flightSegments.get(0);
      FlightSegment lastSegment = flightSegments.get(flightSegments.size() - 1);

      CollectionMethod collectionMethod = null;

      if (shoppingCartCollectionOption != null) {
        collectionMethod = shoppingCartCollectionOption.getMethod();
      }

      //fullPrice, without update card in spinner
      String category =
          categoryForSegment(departureSegment, lastSegment, collectionMethod, searchOptions);
      Product product =
          createProduct(shoppingCart, insuranceShoppingItems, searchOptions, "FLIGHT", currency,
              shoppingCart.getTotalPrice().doubleValue(), category);

      transaction.getProductList().add(product);
    }

    createProducts(shoppingCart, insuranceShoppingItems, searchOptions, currency, transaction);

    return transaction;
  }

  private static void createProducts(ShoppingCart shoppingCart,
      List<InsuranceShoppingItem> insuranceShoppingItems, SearchOptions searchOptions,
      String currency, Transaction transaction) {
    if (insuranceShoppingItems != null) {

      int insuranceIndex = 1;

      for (InsuranceShoppingItem insuranceShoppingItem : insuranceShoppingItems) {
        if (insuranceShoppingItem.isSelectable()) {
          String insuranceSku = "INSURANCE";
          if (insuranceShoppingItems.size() > 1) {
            insuranceSku = String.format("INSURANCE-%d", insuranceIndex);
          }

          Product product =
              createProduct(shoppingCart, insuranceShoppingItems, searchOptions, insuranceSku,
                  currency, insuranceShoppingItem.getTotalPrice().doubleValue(),
                  categoryForInsurance(insuranceShoppingItem, searchOptions));
          transaction.getProductList().add(product);

          insuranceIndex++;
        }
      }
    }
  }

  private static Product createProduct(ShoppingCart shoppingCart,
      List<InsuranceShoppingItem> insuranceShoppingItems, SearchOptions searchOptions,
      String insuranceSku, String currency, double price, String category) {
    Product product = new Product(String.valueOf(shoppingCart.getBookingId()),
        productCode(insuranceShoppingItems, searchOptions));
    product.setItemSku(insuranceSku);
    product.setItemCategory(category);
    product.setItemPrice(price);
    product.setItemQuantity(1);
    product.setCurrencyCode(currency);

    return product;
  }

  public static String affiliation(SearchOptions searchOptions) {
    StringBuilder airlinesString = new StringBuilder();

    String[] aCarriers = carriers(searchOptions);

    int i = 0;
    for (String carrier : aCarriers) {
      airlinesString.append(carrier);

      if (i < aCarriers.length - 1) {
        airlinesString.append('-');
      }

      i++;
    }

    return airlinesString.toString();
  }
}