package com.odigeo.app.android.navigator.helpers;

import android.content.Context;
import android.net.Uri;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by javier.rebollo on 1/9/15.
 */
public class GoogleIndexingAPIHelper {

  public static final String SEARCH = "search";
  public static final String TRIPS = "trips";
  private Uri mAppUri;
  private Uri mWebUrl;
  private GoogleApiClient mClient;
  private Action mViewAction;

  public GoogleIndexingAPIHelper(Context context, String type) {

    String packageName = context.getPackageName();
    String brand;

    //TODO THIS IS A CRAP CODE, SEARCH OTHER METHOD TO DO THIS
    if (packageName.contains("edreams")) {
      brand = "ed";
    } else if (packageName.contains("opodo")) {
      brand = "op";
    } else {
      brand = "go";
    }

    mAppUri = Uri.parse("android-app://" + packageName + "/" + brand + "-" + type + "/");
    mWebUrl = Uri.parse(brand + "-" + type + "://");

    mClient = new GoogleApiClient.Builder(context).addApi(AppIndex.API).build();
  }

  public void onStart(String title) {
    // Connect your client
    mClient.connect();

    // Construct the Action performed by the user
    mViewAction = Action.newAction(Action.TYPE_VIEW, title, mWebUrl, mAppUri);

    // Call the App Indexing API start method after the view has completely rendered
    AppIndex.AppIndexApi.start(mClient, mViewAction);
  }

  public void onStop() {
    AppIndex.AppIndexApi.end(mClient, mViewAction);
    mClient.disconnect();
  }
}
