package com.odigeo.app.android.lib.modules;

import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.ResidentItinerary;
import com.odigeo.data.entity.geo.City;
import java.util.List;

/**
 * Created by ManuelOrtiz on 08/12/2014.
 */
public class Residents {

  public static ResidentItinerary applyResidentDiscount(City origin, City destination) {
    ResidentItinerary residentItineraryFound = null;

    List<ResidentItinerary> residentItineraries =
        Configuration.getInstance().getCurrentMarket().getResidentItineraries();

    if (origin != null && destination != null) {

      for (ResidentItinerary residentItinerary : residentItineraries) {

        String originCode = setOriginCode(origin, residentItinerary);

        String destinationCode = setDestinationCode(destination, residentItinerary);

        if (originCode != null && destinationCode != null && originCode.equals(
            residentItinerary.getOrigin().getCode()) && destinationCode.equals(
            residentItinerary.getDestination().getCode())) {
          residentItineraryFound = residentItinerary;
          break;
        }
      }
    }

    return residentItineraryFound;
  }

  /**
   * This method gets the origin code from the origin city
   *
   * @param origin origin city
   * @param residentItinerary resident itinerary
   * @return origin code
   */
  private static String setOriginCode(City origin, ResidentItinerary residentItinerary) {
    String originCode = "";
    if (residentItinerary.getOrigin().isCountry()) {
      originCode = origin.getCountryCode();
    } else {
      originCode = origin.getIataCode();
    }
    return originCode;
  }

  /**
   * This method gets the destination code from the destination city
   *
   * @param destination destination city
   * @param residentItinerary resident itinerary
   * @return destination code
   */
  private static String setDestinationCode(City destination, ResidentItinerary residentItinerary) {
    String destinationCode = "";
    if (residentItinerary.getDestination().isCountry()) {
      destinationCode = destination.getCountryCode();
    } else {
      destinationCode = destination.getIataCode();
    }
    return destinationCode;
  }
}
