package com.odigeo.app.android.view;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DateHelper;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.DelayedSectionCard;
import com.odigeo.data.entity.extensions.UIDelayedCard;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.tools.DurationFormatter;

/**
 * @author Javier Marsicano
 * @version 1.0
 * @since 27/01/2016.
 */
public class NextTripDelayedView extends NextTripView {

  private static final String KEY_DELAY_CARD = "KEY_DELAY_CARD";

  private TextView mTvTripStatus;
  private TextView mTvFirstArrivalTime;
  private TextView mTvFirstDepartureTime;

  private String stringIsDelayed;
  private long mBookingId;
  private DateHelper mDateHelper;
  private DurationFormatter durationFormatter;

  public static NextTripDelayedView newInstance(DelayedSectionCard card, long lastUpdate) {
    Bundle args = new Bundle();
    args.putParcelable(KEY_DELAY_CARD, new UIDelayedCard(card));
    args.putLong(Card.KEY_LAST_UPDATE, lastUpdate);
    NextTripDelayedView fragment = new NextTripDelayedView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle args = getArguments();
    mDateHelper = getDateHelper();
    durationFormatter = getDurationFormatter();
    if (args.containsKey(KEY_DELAY_CARD)
        && args.containsKey(Card.KEY_LAST_UPDATE)
        && args.containsKey(Card.KEY_CARD_POSITION)) {
      mCard = (UIDelayedCard) args.getParcelable(KEY_DELAY_CARD);
      mLastUpdate = args.getLong(Card.KEY_LAST_UPDATE);
      mCardPosition = args.getInt(Card.KEY_CARD_POSITION);
    }
    mBookingId = mCard.getId();
  }

  @Override protected void initComponent(View view) {
    super.initComponent(view);
    mTvFirstArrivalTime = (TextView) view.findViewById(R.id.TvFirstArrivalTime);
    mTvFirstDepartureTime = (TextView) view.findViewById(R.id.TvFirstDepartureTime);
    mTvTripStatus = (TextView) view.findViewById(R.id.TvTripStatus);
  }

  private void initDataNextTripDelayed() {
    int orangeColor = ContextCompat.getColor(getActivity(), R.color.carousel_card_font_delay);

    String departureTimeOriginal = mDateHelper.getTimeGMT(mCard.getFrom().getDate(), getActivity());
    String arrivalTimeOriginal = mDateHelper.getTimeGMT(mCard.getTo().getDate(), getActivity());

    mTvFirstArrivalTime.setVisibility(View.VISIBLE);
    mTvFirstArrivalTime.setPaintFlags(
        mTvFirstArrivalTime.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    mTvFirstArrivalTime.setText(arrivalTimeOriginal);

    mTvFirstDepartureTime.setVisibility(View.VISIBLE);
    mTvFirstDepartureTime.setPaintFlags(
        mTvFirstDepartureTime.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    mTvFirstDepartureTime.setText(departureTimeOriginal);

    long delay =
        mDateHelper.minutesToMiliseconds(Long.parseLong(((UIDelayedCard) mCard).getArrivalDelay()));
    setDateTexts(mCard.getTo().getDate(), delay, mTvScheduleArrivalCity, orangeColor);

    delay = mDateHelper.minutesToMiliseconds(
        Long.parseLong(((UIDelayedCard) mCard).getDepartureDelay()));
    setDateTexts(mCard.getFrom().getDate(), delay, mTvScheduleDepartureCity, orangeColor);

    mTvSubtitleCard.setText(mCard.getTripToHeader());
    String lastUpdateText = stringLastUpdate
        + " "
        + "<b>"
        + mDateHelper.getTime(mLastUpdate, getActivity())
        + "</b> "
        + mDateHelper.getDayAndMonth(mLastUpdate, getActivity());
    mTvDateLastUpdate.setText(Html.fromHtml(lastUpdateText));

    //append word delayed with the delay time in minutes
    stringIsDelayed = LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.CARDS_DELAYED);

    int delayToShow = Integer.parseInt(((UIDelayedCard) mCard).getDepartureDelay());

    stringIsDelayed =
        String.format(stringIsDelayed, (String) durationFormatter.format(delayToShow));
    mTvTripStatus.setText(
        ViewUtils.paintStringTag(stringIsDelayed, R.color.carousel_card_font_delay, getActivity()));
  }

  private void setDateTexts(long date, long delay, TextView tv, int color) {
    long delayedTime = date + delay;
    String dateText = mDateHelper.getDayAndMonthGMT(delayedTime, getActivity());
    String timeText = mDateHelper.getTimeGMT(delayedTime, getActivity());
    String fullScheduleText = timeText + " " + dateText;

    Spannable wordToSpan = new SpannableString(fullScheduleText);
    wordToSpan.setSpan(new ForegroundColorSpan(color), 0, timeText.length(), 0);
    wordToSpan.setSpan(new StyleSpan(Typeface.BOLD), 0, timeText.length(), 0);
    tv.setText(wordToSpan);
  }

  private DateHelper getDateHelper() {
    return AndroidDependencyInjector.getInstance().provideDateHelper();
  }

  private DurationFormatter getDurationFormatter() {
    return dependencyInjector.provideDurationFormatter();
  }

  @Override protected void setListeners() {
    super.setListeners();
    initDataNextTripDelayed();
  }

  protected void setListeners(View view) {
    view.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.showBookingDetail(mBookingId);
      }
    });
  }

  @Override protected void trackSeeInfo() {
    String trackingLabel =
        TrackerConstants.LABEL_CARD_SEE_INFO_FORMAT.replace(TrackerConstants.CARD_TYPE_IDENTIFIER,
            TrackerConstants.LABEL_CARD_DELAYED)
            .replace(TrackerConstants.CARD_POSITION_IDENTIFIER, String.valueOf(mCardPosition));
    mTracker.trackMTTCardEvent(trackingLabel);
  }

  @Override protected void initOneCMSText(View view) {
    super.initOneCMSText(view);
  }

  @Override public void onDestroy() {
    super.onDestroy();
  }
}
