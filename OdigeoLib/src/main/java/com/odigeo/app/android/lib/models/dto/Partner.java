package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class Partner implements Serializable {
  private long id;
  private String name;
  private String type;

  public final long getId() {
    return id;
  }

  public final void setId(long id) {
    this.id = id;
  }

  public final String getName() {
    return name;
  }

  public final void setName(String name) {
    this.name = name;
  }

  public final String getType() {
    return type;
  }

  public final void setType(String type) {
    this.type = type;
  }
}
