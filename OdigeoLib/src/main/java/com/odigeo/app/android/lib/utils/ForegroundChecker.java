package com.odigeo.app.android.lib.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * This class uses a SharedPreferences file to store the application state: if it is in foreground
 * (the user is interacting with it) or not.
 *
 * @author Julio Kun
 * @since 29/06/2015
 */
public class ForegroundChecker {
  private static final String SHARED_PREF_NAME = "odigeo_foreground";
  private static final String FOREGROUND_KEY = "onForeground";
  private static ForegroundChecker instance = new ForegroundChecker();

  public static ForegroundChecker getInstance() {
    return instance;
  }

  /**
   * Return the SharedPreferences for this class.
   */
  private SharedPreferences getPreferences(Context context) {
    SharedPreferences sharedPref =
        context.getSharedPreferences(SHARED_PREF_NAME, context.MODE_PRIVATE);
    return sharedPref;
  }

  /**
   * Sets if the applications is in foreground.
   */
  public void setOnForeground(boolean onForeground, Context context) {
    SharedPreferences.Editor editor = getPreferences(context).edit();
    editor.putBoolean(FOREGROUND_KEY, onForeground);
    editor.commit();
  }

  /**
   * Returns if the application is in foreground.
   */
  public boolean isAppOnForeground(Context context) {
    return getPreferences(context).getBoolean(FOREGROUND_KEY, false);
  }
}
