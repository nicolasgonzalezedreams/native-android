package com.odigeo.app.android.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.LegacyUserPresenter;
import com.odigeo.presenter.contracts.navigators.LoginNavigatorInterface;
import com.odigeo.presenter.contracts.views.LegacyUserViewInterface;

/**
 * Created by ximena.perez on 21/08/2015.
 */
public class LegacyUserView extends BaseView<LegacyUserPresenter>
    implements LegacyUserViewInterface {

  private static final String USER_EMAIL = "USER_EMAIL";
  private Button mBtnGoToLogin;
  private TextView mTvMessage;
  private String mEmail;

  public static LegacyUserView newInstance(String email) {
    LegacyUserView view = new LegacyUserView();
    Bundle args = new Bundle();
    args.putString(USER_EMAIL, email);
    view.setArguments(args);

    return view;
  }

  @Override public void onResume() {
    super.onResume();
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_LOGIN_ACCOUNT_BLOCKED_LEGACY);
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mEmail = getArguments().getString(USER_EMAIL, "");
  }

  @Override protected LegacyUserPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideLegacyUserPresenter(this, (LoginNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_reset_password;
  }

  @Override protected void initComponent(View view) {
    mBtnGoToLogin = (Button) view.findViewById(R.id.btnLogIn);
    mTvMessage = (TextView) view.findViewById(R.id.txt_vw_reset_pass_paragraph2);
  }

  @Override protected void setListeners() {
    mBtnGoToLogin.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.goToLogin();
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_SSO_BLOCKED_LEGACY,
            TrackerConstants.ACTION_SSO_BLOCKED, TrackerConstants.LABEL_GO_TO_LOGIN);
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    mTvMessage.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_LEGACY_USER_SENT_MAIL, mEmail)
            .toString());
    mBtnGoToLogin.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_LOGIN_BUTTON).toString());
  }
}
