package com.odigeo.app.android.view.components;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.DeviceUtils;
import com.odigeo.app.android.navigator.MyTripDetailsNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.PermissionsHelper;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.presenter.ArrivalGuidesCardPresenter;
import com.odigeo.presenter.contracts.views.ArrivalGuidesProviderViewInterface;
import java.text.DecimalFormat;

import static com.odigeo.app.android.view.helpers.PermissionsHelper.EXTERNAL_STORAGE_REQUEST_CODE;
import static com.odigeo.app.android.view.helpers.PermissionsHelper.EXTERNAL_STORAGE_REQUEST_CODE_FOR_DELETING;

public class ArrivalGuidesProvider implements ArrivalGuidesProviderViewInterface {

  private static final String MB = "MB";
  private static final int ONE_MB = 1048576;
  private static final int MAX_DESCRIPTION_LINES = 3;
  private static final int MAX_TITLE_LINES = 2;
  private Activity mActivity;
  private Booking mBooking;
  private View mView;
  private Button mButtonDownload;
  private Button mButtonOpen;
  private Button mButtonDelete;
  private LinearLayout mViewDownloadedButtons;
  private ProgressBar mProgressBar;
  private LinearLayout mViewProgress;
  private TextView mTextViewProgress;
  private TextView mTextViewLanguage;
  private TextView mTextViewTitle;
  private TextView mTextViewDownloading;
  private String mTextProgressWithMax;
  private ArrivalGuidesCardPresenter mPresenter;
  private Boolean mFirstProgress = true;
  private ImageView mIconCancelDownload;

  public ArrivalGuidesProvider(Activity activity, Booking booking) {
    this.mActivity = activity;
    this.mBooking = booking;
    mPresenter = getPresenter();
  }

  protected ArrivalGuidesCardPresenter getPresenter() {
    return ((OdigeoApp) mActivity.getApplicationContext()).getDependencyInjector()
        .provideArrivalGuidesCardsPresenter(this, (MyTripDetailsNavigator) mActivity);
  }

  /**
   * Get a card details of your trips built
   *
   * @return Card of details of your trips
   */
  public View getArrivalGuides() {
    initView();
    setListeners();
    setText();
    mPresenter.initCard(mBooking, mActivity.getResources().getConfiguration().locale.getLanguage());
    return mView;
  }

  public void downloadGuide() {
    mPresenter.downloadGuide(DeviceUtils.isGoodNetworkConnectivityAvailable(mView.getContext()));
  }

  public void deleteGuide() {
    mPresenter.deleteGuide();
  }

  private void setText() {
    mTextViewTitle.setText(LocalizablesFacade.getString(mActivity.getApplicationContext(),
        OneCMSKeys.TRAVELGUIDE_PDF_DOWNLOAD_TITLE));
    mTextViewLanguage.setText(LocalizablesFacade.getString(mActivity.getApplicationContext(),
        OneCMSKeys.TRAVELGUIDE_PDF_EN_ONLY));
    mButtonDownload.setText(LocalizablesFacade.getString(mActivity.getApplicationContext(),
        OneCMSKeys.TRAVELGUIDE_PDF_DOWNLOAD_BUTTON));
    mButtonDelete.setText(LocalizablesFacade.getString(mActivity.getApplicationContext(),
        OneCMSKeys.TRAVELGUIDE_PDF_DOWNLOAD_DELETE_BUTTON));
    mButtonOpen.setText(LocalizablesFacade.getString(mActivity.getApplicationContext(),
        OneCMSKeys.TRAVELGUIDE_PDF_DOWNLOAD_OPEN_BUTTON));
    mTextViewDownloading.setText(LocalizablesFacade.getString(mActivity.getApplicationContext(),
        OneCMSKeys.TRAVELGUIDE_PDF_DOWNLOAD_IN_PROGRESS));

    mButtonDelete.setAllCaps(true);
    mButtonOpen.setAllCaps(true);
    mButtonDownload.setAllCaps(true);

    mTextViewLanguage.post(new Runnable() {
      @Override public void run() {
        if (mTextViewLanguage.getLineCount() > MAX_DESCRIPTION_LINES) {
          mTextViewLanguage.setTextSize(TypedValue.COMPLEX_UNIT_PX,
              mActivity.getResources().getDimension(R.dimen.size_font_XS));
        }
      }
    });

    mTextViewTitle.post(new Runnable() {
      @Override public void run() {
        if (mTextViewTitle.getLineCount() > MAX_TITLE_LINES) {
          mTextViewTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,
              mActivity.getResources().getDimension(R.dimen.size_font_L));
        }
      }
    });
  }

  private void setListeners() {
    mButtonDownload.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          if (PermissionsHelper.askForPermissionIfNeeded(
              Manifest.permission.WRITE_EXTERNAL_STORAGE, mActivity,
              OneCMSKeys.PERMISSION_STORAGE_TRAVELGUIDE_MESSAGE, EXTERNAL_STORAGE_REQUEST_CODE)) {
            downloadGuide();
          }
        } else {
          downloadGuide();
        }
      }
    });

    mButtonOpen.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.openGuide();
      }
    });

    mIconCancelDownload.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        mPresenter.cancelDownload();
      }
    });

    mButtonDelete.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          if (PermissionsHelper.askForPermissionIfNeeded(
              Manifest.permission.WRITE_EXTERNAL_STORAGE, mActivity,
              OneCMSKeys.PERMISSION_STORAGE_TRAVELGUIDE_MESSAGE,
              EXTERNAL_STORAGE_REQUEST_CODE_FOR_DELETING)) {
            deleteGuide();
          }
        } else {
          deleteGuide();
        }
      }
    });
  }

  private void initView() {
    mView = mActivity.getLayoutInflater().inflate(R.layout.arrival_guides, null);
    mViewDownloadedButtons =
        (LinearLayout) mView.findViewById(R.id.viewDownloadedButtonsArrivalGuides);
    mButtonDownload = (Button) mView.findViewById(R.id.btnDownloadArrivalGuides);
    mButtonOpen = (Button) mViewDownloadedButtons.findViewById(R.id.btnOpenArrivalGuides);
    mButtonDelete = (Button) mView.findViewById(R.id.btnDeleteArrivalGuides);
    mProgressBar = (ProgressBar) mView.findViewById(R.id.progressBarArrivalGuides);
    mViewProgress = (LinearLayout) mView.findViewById(R.id.linearLayoutProgressArrivalGuides);
    mTextViewProgress = (TextView) mView.findViewById(R.id.tvProgressArrivalGuides);
    mTextViewLanguage = (TextView) mView.findViewById(R.id.tvDescriptionArrivalGuides);
    mTextViewLanguage.setVisibility(View.GONE);
    mTextViewTitle = (TextView) mView.findViewById(R.id.tvTitleArrivalGuides);
    mTextViewDownloading = (TextView) mView.findViewById(R.id.tvDownloadingArrivalGuides);
    mIconCancelDownload = (ImageView) mView.findViewById(R.id.iconCancelDownloadArrivalGuides);
  }

  @Override public void showDownloadGuideState() {
    mViewProgress.setVisibility(View.INVISIBLE);
    mProgressBar.setIndeterminate(true);
    mTextViewProgress.setText("");
    mFirstProgress = true;
    mButtonDownload.setVisibility(View.VISIBLE);
    mViewDownloadedButtons.setVisibility(View.INVISIBLE);
  }

  @Override public void showGuideInDifferentLanguageMessage() {
    mTextViewLanguage.setVisibility(View.VISIBLE);
  }

  @Override public void showDownloadingGuideState() {
    mButtonDownload.setVisibility(View.INVISIBLE);
    mProgressBar.setVisibility(View.VISIBLE);
    mViewProgress.setVisibility(View.VISIBLE);
  }

  @Override public void showDownloadProgress(double progress, double totalDownloadSize) {
    String progressText;
    if (mFirstProgress && (progress > 0)) {
      mProgressBar.setIndeterminate(false);
      mProgressBar.setMax((int) totalDownloadSize);
      mFirstProgress = false;
      String max =
          new DecimalFormat("#.##").format((totalDownloadSize / ONE_MB)); //gets the max in MB
      progressText = new DecimalFormat("#.##").format((progress / ONE_MB));
      mTextProgressWithMax = "(%s/" + max + MB + ")";
      mTextViewProgress.setText(String.format(mTextProgressWithMax, progressText));
    }
    if (progress > 0) {
      mProgressBar.setProgress((int) progress);
      progressText = new DecimalFormat("#.##").format((progress / ONE_MB));
      mTextViewProgress.setText(String.format(mTextProgressWithMax, progressText));
    }
  }

  @Override public void showGuideDownloadedState() {
    mViewDownloadedButtons.setVisibility(View.VISIBLE);
    mViewProgress.setVisibility(View.INVISIBLE);
    mButtonDownload.setVisibility(View.INVISIBLE);
  }

  @Override public void showDownloadFailedDialog() {
    AlertDialog.Builder downloadFailedDialog = new AlertDialog.Builder(mView.getContext());
    downloadFailedDialog.setTitle(LocalizablesFacade.getString(mView.getContext(),
        OneCMSKeys.TRAVEL_GUIDE_DOWNLOAD_FAIL_TITLE));
    downloadFailedDialog.setMessage(
        LocalizablesFacade.getString(mView.getContext(), OneCMSKeys.TRAVEL_GUIDE_DOWNLOAD_FAIL_TEXT)
            .toString());
    downloadFailedDialog.setPositiveButton(LocalizablesFacade.getString(mView.getContext(),
        OneCMSKeys.TRAVEL_GUIDE_DOWNLOAD_FAIL_TRY_AGAIN), new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        mPresenter.downloadGuide(
            DeviceUtils.isGoodNetworkConnectivityAvailable(mView.getContext()));
      }
    });
    downloadFailedDialog.setNegativeButton(android.R.string.cancel,
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
            mPresenter.cancelDownload();
          }
        });
    downloadFailedDialog.show();
  }

  @Override public void showNoConnectionDialog() {
    AlertDialog.Builder noConnectionDialog = new AlertDialog.Builder(mView.getContext());
    noConnectionDialog.setTitle(
        LocalizablesFacade.getString(mView.getContext(), OneCMSKeys.ERROR_NO_CONNECTION_TITLE));
    noConnectionDialog.setMessage(
        LocalizablesFacade.getString(mView.getContext(), OneCMSKeys.ERROR_NO_CONNECTION_SUBTITLE)
            .toString());
    noConnectionDialog.setPositiveButton(
        LocalizablesFacade.getString(mView.getContext(), OneCMSKeys.ERROR_NO_CONNECTION_TRY_AGAIN),
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            mPresenter.downloadGuide(
                DeviceUtils.isGoodNetworkConnectivityAvailable(mView.getContext()));
          }
        });
    noConnectionDialog.setNegativeButton(android.R.string.cancel,
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
            mPresenter.cancelDownload();
          }
        });
    noConnectionDialog.show();
  }

  @Override public void showInsufficientSpaceDialog() {
    AlertDialog.Builder insufficientSpaceDialog = new AlertDialog.Builder(mView.getContext());
    insufficientSpaceDialog.setTitle(LocalizablesFacade.getString(mView.getContext(),
        OneCMSKeys.TRAVEL_GUIDE_INSUFFICIENT_SPACE_TITLE));
    insufficientSpaceDialog.setMessage(LocalizablesFacade.getString(mView.getContext(),
        OneCMSKeys.TRAVEL_GUIDE_INSUFFICIENT_SPACE_TEXT).toString());
    insufficientSpaceDialog.setPositiveButton(LocalizablesFacade.getString(mView.getContext(),
        OneCMSKeys.TRAVEL_GUIDE_INSUFFICIENT_SPACE_STORAGE), new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        mView.getContext()
            .startActivity(new Intent(android.provider.Settings.ACTION_INTERNAL_STORAGE_SETTINGS));
      }
    });
    insufficientSpaceDialog.setNegativeButton(android.R.string.cancel,
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
            mPresenter.cancelDownload();
          }
        });
    insufficientSpaceDialog.show();
  }

  @Override public void showDeleteGuideConfirmationDialog(String arrivalCityName) {
    AlertDialog.Builder deleteConfirmDialog = new AlertDialog.Builder(mView.getContext());
    deleteConfirmDialog.setTitle(
        LocalizablesFacade.getString(mView.getContext(), OneCMSKeys.TRAVELGUIDE_DELETE_WARNING,
            arrivalCityName).toString());
    deleteConfirmDialog.setNegativeButton(
        LocalizablesFacade.getString(mView.getContext(), OneCMSKeys.COMMON_CANCEL).toString(),
        null);
    deleteConfirmDialog.setPositiveButton(
        LocalizablesFacade.getString(mView.getContext(), OneCMSKeys.COMMON_DELETE).toString(),
        new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialog, int which) {
            mPresenter.confirmedDeleteGuide();
          }
        });
    deleteConfirmDialog.show();
  }

  public void onStart() {
    mPresenter.subscribeDownloadManager();
  }

  public void onStop() {
    mPresenter.unsubscribeDownloadManager();
  }

  @Override public boolean isActive() {
    return ViewCompat.isAttachedToWindow(mView);
  }
}
