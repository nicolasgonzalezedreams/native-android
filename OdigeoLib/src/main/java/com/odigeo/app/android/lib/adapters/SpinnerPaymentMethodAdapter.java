package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.models.dto.CollectionMethodDTO;
import com.odigeo.app.android.lib.models.dto.ShoppingCartCollectionOptionDTO;
import com.odigeo.app.android.lib.utils.ViewUtils;
import java.util.List;

/**
 * Created by ManuelOrtiz on 08/10/2014.
 *
 * @deprecated Use {@link OdigeoSpinnerAdapter} instead
 */
@Deprecated public class SpinnerPaymentMethodAdapter
    extends ArrayAdapter<ShoppingCartCollectionOptionDTO> {

  public SpinnerPaymentMethodAdapter(Context context,
      List<ShoppingCartCollectionOptionDTO> creditCardTypes) {
    super(context, R.layout.layout_spinner_simple_text, creditCardTypes);
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {
    View view = LayoutInflater.from(getContext())
        .inflate(R.layout.layout_spinner_simple_text_and_pic, parent, false);
    TextView textView = (TextView) view.findViewById(R.id.textView_card_name_and_icon);
    fillUp(position, textView);
    return view;
  }

  @Override public View getDropDownView(int position, View convertView, ViewGroup parent) {
    View view = LayoutInflater.from(getContext())
        .inflate(R.layout.layout_spinner_drop_cards_payments, parent, false);
    TextView textView = (TextView) view.findViewById(R.id.text_spinner_cards);
    textView.setTypeface(Configuration.getInstance().getFonts().getRegular());
    fillUp(position, textView);
    return view;
  }

  /**
   * Fills up the text view using the item information
   *
   * @param position Position to search in items
   * @param textView Text view to update
   */
  protected void fillUp(int position, TextView textView) {
    CollectionMethodDTO collectionMethodDTO = getItem(position).getMethod();
    textView.setText(collectionMethodDTO.getName(getContext()));
    String code = collectionMethodDTO.getCode();
    //The code null is for Bank Transfer
    if (code == null) {
      textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cc_bank_transfer, 0, 0, 0);
    } else {
      //For all the other payment methods
      String imageName = String.format("cc_%s", code).toLowerCase();
      Drawable imageCreditCard = ViewUtils.getDrawableResourceByName(getContext(), imageName,
          getContext().getPackageName());
      //Set the credit card image
      if (imageCreditCard != null) {
        textView.setCompoundDrawablesWithIntrinsicBounds(imageCreditCard, null, null, null);
      } else {
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cc_cu, 0, 0, 0);
      }
    }
  }
}
