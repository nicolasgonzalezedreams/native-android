package com.odigeo.app.android.lib.models.dto;

public class AccommodationSelectionDTO {

  protected String accommodationDealKey;
  protected String roomGroupDealKey;

  public String getAccommodationDealKey() {
    return accommodationDealKey;
  }

  public void setAccommodationDealKey(String accommodationDealKey) {
    this.accommodationDealKey = accommodationDealKey;
  }

  public String getRoomGroupDealKey() {
    return roomGroupDealKey;
  }

  public void setRoomGroupDealKey(String roomGroupDealKey) {
    this.roomGroupDealKey = roomGroupDealKey;
  }
}
