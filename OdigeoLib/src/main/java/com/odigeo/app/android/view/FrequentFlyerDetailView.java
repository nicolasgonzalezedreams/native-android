package com.odigeo.app.android.view;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.OdigeoSpinnerAdapter;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.navigator.BaseNavigator;
import com.odigeo.app.android.navigator.FrequentFlyerCodesNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.app.android.view.wrappers.UICarrierWrapper;
import com.odigeo.data.entity.extensions.UICarrier;
import com.odigeo.data.entity.shoppingCart.Carrier;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.presenter.FrequentFlyerDetailPresenter;
import com.odigeo.presenter.contracts.views.FrequentFlyerDetailViewInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/09/15
 */
public class FrequentFlyerDetailView extends BaseView<FrequentFlyerDetailPresenter>
    implements FrequentFlyerDetailViewInterface {

  public static final String FREQUENT_FLYER = "FREQUENT_FLYER";
  public static final String ITEM_POSITION = "ITEM_POSITION";
  public static final String CARRIERS_LIST = "CARRIERS";
  private TextInputLayout textInputLayoutCode;
  private EditText editTextCode;
  private Spinner spinnerAirline;
  private UserFrequentFlyer frequentFlyer;
  private int itemPosition;
  private List<UICarrier> carriers;
  private boolean isEditMode;

  /**
   * Retrieves a new instance to the fragment.
   *
   * @return A new and non null instance.
   */
  public static FrequentFlyerDetailView newInstance(UserFrequentFlyer frequentFlyer,
      List<UICarrier> carriers, Integer position) {
    Bundle args = new Bundle();
    args.putSerializable(CARRIERS_LIST, new UICarrierWrapper(carriers));
    if (frequentFlyer != null) {
      args.putSerializable(FREQUENT_FLYER, frequentFlyer);
      if (position != null) {
        args.putInt(ITEM_POSITION, position);
      }
    }
    FrequentFlyerDetailView fragment = new FrequentFlyerDetailView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // MUST be called before the super, because the fill of the information based on the
    // arguments, it's in the initComponent method.
    Bundle bundle = getArguments();
    frequentFlyer = ((UserFrequentFlyer) bundle.getSerializable(FREQUENT_FLYER));
    itemPosition = bundle.getInt(ITEM_POSITION);
    UICarrierWrapper uiCarrierWrapper = ((UICarrierWrapper) bundle.getSerializable(CARRIERS_LIST));
    if (uiCarrierWrapper != null) {
      List<UICarrier> temp = uiCarrierWrapper.getCarriers();
      // Makes a local copy list
      if (temp != null) {
        carriers = new ArrayList<>();
        for (UICarrier carrier : temp) {
          carriers.add(carrier);
        }
      }
    }
    isEditMode = frequentFlyer != null;
    View view = super.onCreateView(inflater, container, savedInstanceState);
    if (isEditMode) {
      ((BaseNavigator) getActivity()).setNavigationTitle(
          LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.FREQUENT_FLYER_EDIT));
    } else {
      ((BaseNavigator) getActivity()).setNavigationTitle(
          LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.FREQUENT_FLYER_NEW));
    }
    ((BaseNavigator) getActivity()).setUpToolbarButton(
        DrawableUtils.getTintedResource(R.drawable.ic_clear, R.color.navigation_bar_title,
            getActivity()));

    return view;
  }

  @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.sso_menu_add_traveller, menu);
    MenuItem item = menu.findItem(R.id.sso_confirmation_add_traveller);
    item.setTitle(LocalizablesFacade.getString(this.getActivity(), OneCMSKeys.DATA_STORAGE_SAVE));
    item.setIcon(
        DrawableUtils.getTintedResource(R.drawable.ic_done, R.color.actionbar_confirm_tint,
            getActivity()));
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.sso_confirmation_add_traveller) {
      ViewUtils.hideKeyboard(getActivity());
      if (checkInputs()) {

        mTracker.trackAnalyticsEvent(OneCMSKeys.CATEGORY_MY_AREA_PAX_DETAILS,
            OneCMSKeys.ACTION_FREQUENT_FLYER, OneCMSKeys.LABEL_FREQUENT_FLYER_CODE_SAVED);

        long frequentFlayerId = 0;
        if (frequentFlyer != null) {
          frequentFlayerId = frequentFlyer.getFrequentFlyerId();
        }
        ((FrequentFlyerCodesNavigator) getActivity()).addNewCode(frequentFlayerId,
            editTextCode.getText().toString(),
            ((UICarrier) spinnerAirline.getSelectedItem()).getCode());
        if (isEditMode) {
          ((FrequentFlyerCodesNavigator) getActivity()).removeCode(itemPosition);
        }
        getFragmentManager().popBackStackImmediate();
      }
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override protected FrequentFlyerDetailPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance().
        provideFrequentFlyerDetailPresenter(this, ((FrequentFlyerCodesNavigator) getActivity()));
  }

  @Override protected int getFragmentLayout() {
    return R.layout.frequent_flyer_detail;
  }

  @Override protected void initComponent(View view) {
    textInputLayoutCode = ((TextInputLayout) view.findViewById(R.id.text_input_code));
    editTextCode = ((EditText) view.findViewById(R.id.txt_frequent_flyer_code));
    spinnerAirline = ((Spinner) view.findViewById(R.id.spinner_airline));
    if (carriers == null) {
      carriers =
          FrequentFlyerCodesNavigator.convertBeans(Configuration.getInstance().getCarriers());
    }
    Collections.sort(carriers);
    // Add of the first empty option.
    if (carriers.size() == 0 || carriers.get(0).getCode() != null) {
      carriers.add(0, new UICarrier(new Carrier(null,
          LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.FREQUENT_FLYER_AIRLINE))));
    }
    OdigeoSpinnerAdapter<UICarrier> adapter = new OdigeoSpinnerAdapter<>(getActivity(), carriers);
    spinnerAirline.setAdapter(adapter);
    // Check if it's on selection mode, then loadWidgetImage the values passed.
    if (isEditMode) {
      // Initialize with the passed values.
      editTextCode.setText(frequentFlyer.getFrequentFlyerNumber());
      int position;
      int size = carriers.size();
      for (position = 0; position < size; position++) {
        UICarrier item = carriers.get(position);
        if (item.getCode() != null && frequentFlyer.getAirlineCode().equals(item.getCode())) {
          break;
        }
      }
      if (carriers.size() == 2) {
        spinnerAirline.setEnabled(false);
      }
      spinnerAirline.setSelection(position);
    }
  }

  @Override protected void setListeners() {
    // Nothing
  }

  @Override protected void initOneCMSText(View view) {
    String hint =
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.FREQUENT_FLYER_CODE_HINT);
    textInputLayoutCode.setHint(hint);
  }

  /**
   * Validate the information entered for the user.
   *
   * @return If all the information is correct.
   */
  private boolean checkInputs() {
    boolean result = true;
    String code = editTextCode.getText().toString().trim();
    // If the code is empty.
    if (TextUtils.isEmpty(code)) {
      textInputLayoutCode.setError(
          LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_FIELD_MANDATORY_ERROR));
      result = false;
    }
    // If it is not selected any item.
    if (spinnerAirline.getSelectedItemPosition() <= 0) {
      if (getView() != null) {
        Snackbar.make(getView(),
            LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.FREQUENT_FLYER_AIRLINE),
            Snackbar.LENGTH_SHORT).show();
      }
      result &= false;
    }
    return result;
  }
}
