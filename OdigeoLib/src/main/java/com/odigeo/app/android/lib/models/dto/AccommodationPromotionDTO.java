package com.odigeo.app.android.lib.models.dto;

public class AccommodationPromotionDTO {

  protected String type;
  protected String description;

  /**
   * Gets the value of the type property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getType() {
    return type;
  }

  /**
   * Sets the value of the type property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setType(String value) {
    this.type = value;
  }

  /**
   * Gets the value of the description property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getDescription() {
    return description;
  }

  /**
   * Sets the value of the description property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setDescription(String value) {
    this.description = value;
  }
}
