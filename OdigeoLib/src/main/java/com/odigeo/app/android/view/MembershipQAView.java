package com.odigeo.app.android.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.navigator.MembershipQANavigator;
import com.odigeo.app.android.view.adapters.MembershipQARecyclerAdapter;
import com.odigeo.data.entity.userData.Membership;
import com.odigeo.presenter.MembershipQAPresenter;
import com.odigeo.presenter.contracts.views.MembershipQAViewInterface;
import java.util.List;

public class MembershipQAView extends BaseView<MembershipQAPresenter>
    implements MembershipQAViewInterface {

  private RecyclerView recyclerView;
  private MembershipQARecyclerAdapter adapter;
  private TextView textViewNoMember;
  private Button addMembershipButton;

  public static MembershipQAView newInstance() {
    return new MembershipQAView();
  }

  @Override protected MembershipQAPresenter getPresenter() {
    return dependencyInjector.provideMembershipQAPresenter(this,
        (MembershipQANavigator) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_membershipqa_view;
  }

  @Override protected void initComponent(View view) {
    recyclerView = (RecyclerView) view.findViewById(R.id.recycler_membershipQA);
    textViewNoMember = (TextView) view.findViewById(R.id.textViewNotMember);
    addMembershipButton = (Button) view.findViewById(R.id.buttonCreateMembership);

    mPresenter.checkUserIsMemberForCurrentMarket();

    adapter = new MembershipQARecyclerAdapter(mPresenter.getAllMemberships());
    recyclerView.setAdapter(adapter);
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
  }

  @Override protected void setListeners() {
    addMembershipButton.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        mPresenter.addMembershipForCurrentMarket();
      }
    });
  }

  @Override protected void initOneCMSText(View view) {

  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mPresenter.getAllMemberships();
  }

  @Override public void hideNotMemberLabel() {
    textViewNoMember.setVisibility(View.GONE);
  }

  @Override public void showNotMemberLabel() {
    textViewNoMember.setVisibility(View.VISIBLE);
  }

  @Override public void hideAddMembershipButton() {
    addMembershipButton.setVisibility(View.GONE);
  }

  @Override public void showAddMembershipButton() {
    addMembershipButton.setVisibility(View.VISIBLE);
  }

  @Override public void updateMembershipList(List<Membership> memberships) {
    adapter.updateContent(memberships);
  }
}
