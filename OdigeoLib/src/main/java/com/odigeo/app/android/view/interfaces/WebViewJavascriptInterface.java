package com.odigeo.app.android.view.interfaces;

import android.webkit.JavascriptInterface;

/**
 * Javascript interface that defines @JavascriptInterface methods and a a listener to notify
 * and pass data to consumers
 */
public final class WebViewJavascriptInterface {

  private final Listener listener;

  public WebViewJavascriptInterface(Listener listener) {
    this.listener = listener;
  }

  @JavascriptInterface public void parseFormData(String formData) {
    listener.onFormDataParsed(formData);
  }

  public interface Listener {
    void onFormDataParsed(String formData);
  }
}
