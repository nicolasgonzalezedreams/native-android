package com.odigeo.app.android.navigator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import com.odigeo.app.android.model.BookingInfoViewModel;
import com.odigeo.app.android.view.PaymentView;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.shoppingCart.BankTransferData;
import com.odigeo.data.entity.shoppingCart.BankTransferResponse;
import com.odigeo.data.entity.shoppingCart.BookingDetail;
import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.MarketingRevenueDataAdapter;
import com.odigeo.data.entity.shoppingCart.ResumeDataRequest;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.userData.InsertCreditCardRequest;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackHelper;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackerFlowSession;
import com.odigeo.presenter.ExternalPaymentPresenter;
import com.odigeo.presenter.contracts.navigators.PaymentNavigatorInterface;
import java.io.Serializable;
import java.util.List;

public class PaymentNavigator extends BaseNavigator implements PaymentNavigatorInterface {

  private static final int EXTERNAL_PAYMENT_LAUNCH_CODE = 1;

  private PaymentView mPaymentView;
  private boolean mHasBeenRepricing;

  private CreateShoppingCartResponse mCreateShoppingCartResponse;
  private boolean mIsFullTransparency;
  private double mLastTicketsPrice;
  private double mLastInsurancePrice;
  private SearchOptions mSearchOptions;
  private BookingInfoViewModel bookingInfo;
  private CollectionMethodWithPrice collectionMethodWithPrice;

  public Fragment getInstancePaymentView() {
    if (mPaymentView == null) {
      mPaymentView = PaymentView.newInstance(mCreateShoppingCartResponse, mIsFullTransparency,
          mLastTicketsPrice, mLastInsurancePrice, mSearchOptions, collectionMethodWithPrice,
          bookingInfo);
      mPaymentView.setArguments(getIntent().getExtras());
    }
    return mPaymentView;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_payment);

    mCreateShoppingCartResponse = (CreateShoppingCartResponse) getIntent().getSerializableExtra(
        Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE);
    mIsFullTransparency = getIntent().getBooleanExtra(Constants.EXTRA_FULL_TRANSPARENCY, false);
    mLastTicketsPrice = getIntent().getDoubleExtra(Constants.EXTRA_REPRICING_TICKETS_PRICES, 0);
    mLastInsurancePrice = getIntent().getDoubleExtra(Constants.EXTRA_REPRICING_INSURANCES, 0);
    mSearchOptions =
        (SearchOptions) getIntent().getSerializableExtra(Constants.EXTRA_SEARCH_OPTIONS);
    collectionMethodWithPrice = (CollectionMethodWithPrice) getIntent().getSerializableExtra(
        Constants.COLLECTION_METHOD_WITH_PRICE);
    bookingInfo =
        (BookingInfoViewModel) getIntent().getSerializableExtra(Constants.EXTRA_BOOKING_INFO);

    mHasBeenRepricing = false;

    replaceFragment(R.id.flContainer, getInstancePaymentView());
    setNavigationTitle(localizables.getString(OneCMSKeys.PAYMENT_CONTROLLER_TITLE));

    SearchTrackHelper searchTrackHelper =
        SearchTrackerFlowSession.getInstance().getSearchTrackHelper();
    if (searchTrackHelper != null) {
      tracker.trackPaymentReached(Configuration.getInstance().getCurrentMarket().getCurrencyKey(),
          searchTrackHelper.getFlightType(), searchTrackHelper.getAdults(),
          searchTrackHelper.getKids(), searchTrackHelper.getInfants(), searchTrackHelper.getPrice(),
          searchTrackHelper.getOrigins(), searchTrackHelper.getDestinations(),
          searchTrackHelper.getDates(), searchTrackHelper.getAirlines(),
          searchTrackHelper.getDepartureCountries(), searchTrackHelper.getArrivalCountries());
    }
    mTuneTracker.trackViewBasket();
  }

  @Override public void onBackPressed() {
    Intent resultIntent = new Intent();
    resultIntent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE,
        mCreateShoppingCartResponse);
    setResult(Activity.RESULT_OK, resultIntent);
    super.onBackPressed();
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_goto_summary, menu);
    MenuItem item = menu.findItem(R.id.menu_item_goto_summary);
    item.setTitle(localizables.getString(OneCMSKeys.COMMON_TRIP_DETAILS));
    return super.onCreateOptionsMenu(menu);
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {
    int itemId = item.getItemId();
    if (itemId == R.id.menu_item_goto_summary) {
      onSummaryClick();
      return true;
    } else if (itemId == android.R.id.home) {
      onHomeClick();
      return true;
    } else {
      return super.onOptionsItemSelected(item);
    }
  }

  private void onHomeClick() {
    if (mHasBeenRepricing) {
      tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
          TrackerConstants.ACTION_REPRICING, TrackerConstants.LABEL_REPRICING_GO_BACK);
    } else {
      tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
          TrackerConstants.ACTION_NAVIGATION_ELEMENTS, TrackerConstants.LABEL_GO_BACK);
    }
    onBackPressed();
  }

  private void onSummaryClick() {
    tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
        TrackerConstants.ACTION_FLIGHT_SUMMARY, TrackerConstants.LABEL_OPEN_FLIGHT_SUMMARY);
    navigateToSummary();
  }

  @Override public void navigateToExternalPayment(BookingResponse bookingResponse,
      CollectionMethodType collectionMethodType) {
    Intent intent = new Intent(this, ExternalPaymentNavigator.class);
    intent.putExtra(Constants.EXTRA_BOOKING_RESPONSE, bookingResponse);
    intent.putExtra(Constants.EXTRA_COLLECTION_METHOD, collectionMethodType);
    startActivityForResult(intent, EXTERNAL_PAYMENT_LAUNCH_CODE);
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    switch (requestCode) {
      case (EXTERNAL_PAYMENT_LAUNCH_CODE): {
        if (resultCode == ExternalPaymentPresenter.EXTERNAL_PAYMENT_CODE_OK) {
          ResumeDataRequest resumeDataRequest =
              (ResumeDataRequest) data.getSerializableExtra(Constants.EXTRA_RESUME_DATA_REQUEST);
          mPaymentView.onExternalPaymentFinished(resumeDataRequest);
        }
        break;
      }
    }
  }

  @Override public void navigateToSummary() {
    OdigeoApp odigeoApp = (OdigeoApp) getApplication();
    Intent intent = new Intent(this, odigeoApp.getSummaryActivityClass());
    intent.putExtra(Constants.EXTRA_STEP_TO_SUMMARY, Step.PAYMENT);
    intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, mSearchOptions);
    intent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE, mCreateShoppingCartResponse);
    intent.putExtra(Constants.EXTRA_FULL_TRANSPARENCY, mIsFullTransparency);
    intent.putExtra(Constants.EXTRA_BOOKING_INFO, bookingInfo);
    startActivity(intent);
  }

  @Override public void navigateToBookingConfirmed(InsertCreditCardRequest savePaymentMethodRequest,
      String cardNumberObfuscated, CreateShoppingCartResponse createShoppingCartResponse,
      BookingDetail bookingDetail, List<MarketingRevenueDataAdapter> marketingRevenue,
      BankTransferResponse bankTransferResponse, List<BankTransferData> bankTransferData,
      Booking booking, ShoppingCartCollectionOption shoppingCartCollectionOption) {
    mCreateShoppingCartResponse = createShoppingCartResponse;

    OdigeoApp odigeoApp = (OdigeoApp) getApplication();
    Intent intent = new Intent(this, odigeoApp.getConfirmationActivityClass());
    intent.putExtra(Constants.EXTRA_SAVE_PAYMENT_METHOD, savePaymentMethodRequest);
    intent.putExtra(Constants.CREDIT_CARD_NUMBER_OBFUSCATED, cardNumberObfuscated);
    intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, mSearchOptions);
    intent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE, mCreateShoppingCartResponse);
    intent.putExtra(Constants.EXTRA_BOOKING_DETAIL, bookingDetail);
    intent.putExtra(Constants.EXTRA_BANK_TRANSFER_RESPONSE, bankTransferResponse);
    intent.putExtra(Constants.EXTRA_COLLECTION_OPTION_SELECTED, shoppingCartCollectionOption);
    intent.putExtra(Constants.EXTRA_BOOKING, booking);
    intent.putExtra(Constants.EXTRA_BOOKING_INFO, bookingInfo);

    Bundle bundle = new Bundle();
    bundle.putSerializable(Constants.EXTRA_MARKETING_REVENUE, (Serializable) marketingRevenue);
    bundle.putSerializable(Constants.EXTRA_BANK_TRANSFER_DATA, (Serializable) bankTransferData);
    intent.putExtras(bundle);
    startActivity(intent);
  }

  @Override public void navigateToBookingRejected(BookingDetail bookingDetail) {
    OdigeoApp odigeoApp = (OdigeoApp) getApplication();
    Intent intent = new Intent(this, odigeoApp.getConfirmationActivityClass());
    intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, mSearchOptions);
    intent.putExtra(Constants.EXTRA_BOOKING_DETAIL, bookingDetail);
    intent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE, mCreateShoppingCartResponse);
    intent.putExtra(Constants.EXTRA_BOOKING_INFO, bookingInfo);

    startActivity(intent);
  }

  @Override public void navigateToMockBooking(BookingDetail bookingDetail, Booking booking) {
    OdigeoApp odigeoApp = (OdigeoApp) getApplication();
    Intent intent = new Intent(this, odigeoApp.getConfirmationActivityClass());
    intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, mSearchOptions);
    intent.putExtra(Constants.EXTRA_BOOKING_DETAIL, bookingDetail);
    intent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE, mCreateShoppingCartResponse);
    intent.putExtra(Constants.EXTRA_BOOKING, booking);
    intent.putExtra(Constants.EXTRA_BOOKING_INFO, bookingInfo);
    startActivity(intent);
  }

  @Override public void setHasBeenRepricing() {
    mHasBeenRepricing = true;
  }
}