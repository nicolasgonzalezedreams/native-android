package com.odigeo.app.android.lib.config;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.Until;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.SpinnerItemModel;
import com.odigeo.app.android.lib.models.dto.BaggageSelectionDTO;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.geo.City;
import com.odigeo.interactors.provider.MarketProviderInterface;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by manuel on 31/08/14.
 */

public class SearchOptions implements Serializable {
  @Expose private long id;
  @Expose private int numberOfAdults;
  @Expose private int numberOfBabies;
  @Expose private int numberOfKids;
  @Expose private boolean directFlight;

  /**
   * Cabin class object
   *
   * @deprecated use {@link #cabinClassV2} instead
   */
  @Until(Constants.DTOVersion.BAGGAGE_DETAILS) @Deprecated @Expose
  private SpinnerItemModel<CabinClassDTO> cabinClass;
  @Expose private CabinClassDTO cabinClassV2;
  @Expose private TravelType travelType;
  @Expose private List<FlightSegment> flightSegments;
  @Expose private ResidentItinerary residentItinerary;
  @Expose private List<BaggageSelectionDTO> selectedBaggage;
  @Expose private List<BaggageSelectionDTO> includedBaggage;
  @Expose private int numBaggage;
  //Only used for the widget
  @Expose private double lowestPrice;
  @Expose private String locale;
  // Custom property
  /**
   * Position used to saved index for Image to show in different widgets
   */
  @Expose private int multiDestinationIndex;

  /**
   * The date when this options was searched
   */
  private long searchDate;
  @Expose private boolean isResident;
  @Expose private transient String mAirline;
  private boolean executeSearch;

  public SearchOptions() {
    Configuration config = Configuration.getInstance();

    this.numberOfAdults = config.getGeneralConstants().getDefaultNumberOfAdults();
    this.numberOfKids = config.getGeneralConstants().getDefaultNumberOfKids();
    this.numberOfBabies = config.getGeneralConstants().getDefaultNumberOfInfants();
    searchDate = 0L;

    List<CabinClassDTO> cabinClasses = Lists.getInstance().getCabinClasses();
    if (cabinClasses.size() > 0) {
      cabinClassV2 = cabinClasses.get(0);
    }

    //initialized only for compatibility
    this.multiDestinationIndex = 1;
  }

  public SearchOptions(int numberOfAdults, int numberOfBabies, int numberOfKids,
      boolean directFlight, CabinClassDTO cabinClassV2, TravelType travelType,
      List<FlightSegment> flightSegments, boolean isResident) {
    this.numberOfAdults = numberOfAdults;
    this.numberOfBabies = numberOfBabies;
    this.numberOfKids = numberOfKids;
    this.directFlight = directFlight;
    this.cabinClassV2 = cabinClassV2;
    this.travelType = travelType;
    this.flightSegments = flightSegments;
    this.isResident = isResident;
    //initialized only for compatibility
    this.multiDestinationIndex = 1;
  }

  public long getSearchDate() {
    return searchDate;
  }

  public void setSearchDate(long searchDate) {
    this.searchDate = searchDate;
  }

  public int getNumberOfAdults() {
    return numberOfAdults;
  }

  public void setNumberOfAdults(int numberOfAdults) {
    this.numberOfAdults = numberOfAdults;
  }

  public boolean isDirectFlight() {
    return directFlight;
  }

  public void setDirectFlight(boolean directFlight) {
    this.directFlight = directFlight;
  }

  public int getNumberOfBabies() {
    return numberOfBabies;
  }

  public void setNumberOfBabies(int numberOfBabies) {
    this.numberOfBabies = numberOfBabies;
  }

  public int getNumberOfKids() {
    return numberOfKids;
  }

  public void setNumberOfKids(int numberOfKids) {
    this.numberOfKids = numberOfKids;
  }

  public CabinClassDTO getCabinClass() {
    return cabinClassV2;
  }

  public void setCabinClass(CabinClassDTO cabinClass) {
    this.cabinClassV2 = cabinClass;
  }

  /**
   * In case that the object contains a deprecated value, change it to the new one
   */
  public void updateCabinClass() {
    if (cabinClass != null && cabinClassV2 == null) {
      cabinClassV2 = cabinClass.getItem();
    }
  }

  public TravelType getTravelType() {
    return travelType;
  }

  public void setTravelType(TravelType travelType) {
    this.travelType = travelType;
  }

  public int getTotalPassengers() {
    return numberOfAdults + numberOfBabies + numberOfKids;
  }

  public List<FlightSegment> getFlightSegments() {
    return flightSegments;
  }

  public void setFlightSegments(List<FlightSegment> flightSegments) {
    this.flightSegments = flightSegments;
  }

  public int getNumBaggages() {
    return numBaggage;
  }

  public void setNumBaggages(int numBaggages) {
    this.numBaggage = numBaggages;
  }

  @Override public boolean equals(Object searchOption) {
    boolean result = false;
    if (searchOption instanceof SearchOptions) {
      SearchOptions otherSearchOption = (SearchOptions) searchOption;
      if (flightSegments.size() == otherSearchOption.getFlightSegments().size()) {
        for (int i = 0; i < flightSegments.size(); i++) {
          FlightSegment thisSegment = flightSegments.get(i);
          FlightSegment otherSegment = otherSearchOption.getFlightSegments().get(i);
          if (isValidDepartureCity(thisSegment, otherSegment) && isValidArrivalCity(thisSegment,
              otherSegment) && isValidDate(thisSegment, otherSegment)) {
            result = true;
          } else {
            return false;
          }
        }
        if (!isValidTravelType(otherSearchOption)) {
          result = false;
        }
      }
    }
    return result;
  }

  private boolean isValidDepartureCity(FlightSegment thisSegment, FlightSegment otherSegment) {
    if (thisSegment.getDepartureCity() != null
        && otherSegment.getDepartureCity() != null
        && thisSegment.getDepartureCity().equals(otherSegment.getDepartureCity())) {
      return true;
    }
    return false;
  }

  private boolean isValidArrivalCity(FlightSegment thisSegment, FlightSegment otherSegment) {
    if (thisSegment.getArrivalCity() != null && otherSegment.getArrivalCity() != null && thisSegment
        .getArrivalCity()
        .equals(otherSegment.getArrivalCity())) {
      return true;
    }
    return false;
  }

  private boolean isValidDate(FlightSegment thisSegment, FlightSegment otherSegment) {
    if (thisSegment.getDate() != 0 && otherSegment.getDate() != 0) {
      //Remove milliseconds
      Calendar calendar = GregorianCalendar.getInstance();
      calendar.setTimeInMillis(thisSegment.getDate());
      calendar.set(Calendar.MILLISECOND, 0);
      Calendar calendarOther = GregorianCalendar.getInstance();
      calendarOther.setTimeInMillis(otherSegment.getDate());
      calendarOther.set(Calendar.MILLISECOND, 0);
      if (calendar.getTimeInMillis() == calendarOther.getTimeInMillis()) {
        return true;
      }
    }
    return false;
  }

  private boolean isValidTravelType(SearchOptions otherSearchOption) {
    return (travelType == otherSearchOption.getTravelType()
        && numberOfAdults == otherSearchOption.numberOfAdults
        && numberOfBabies == otherSearchOption.numberOfBabies
        && numberOfKids == otherSearchOption.numberOfKids
        && cabinClass == otherSearchOption.cabinClass
        && directFlight == otherSearchOption.directFlight);
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public ResidentItinerary getResident() {
    return residentItinerary;
  }

  public List<BaggageSelectionDTO> getSelectedBaggage() {
    return selectedBaggage;
  }

  public void setSelectedBaggage(List<BaggageSelectionDTO> selectedBaggage) {
    this.selectedBaggage = selectedBaggage;
  }

  public List<BaggageSelectionDTO> getIncludedBaggage() {
    return includedBaggage;
  }

  public void setIncludedBaggage(List<BaggageSelectionDTO> includedBaggage) {
    this.includedBaggage = includedBaggage;
  }

  public FlightSegment getFirstSegment() {
    return flightSegments.get(0);
  }

  public FlightSegment getLastSegment() {
    return flightSegments.get(flightSegments.size() - 1);
  }

  public double getLowestPrice() {
    return lowestPrice;
  }

  public void setLowestPrice(double lowestPrice) {
    this.lowestPrice = lowestPrice;
  }

  public String getLocale() {
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
  }

  public int getMultiDestinationIndex() {
    return multiDestinationIndex;
  }

  public void setMultiDestinationIndex(int multiDestinationIndex) {
    this.multiDestinationIndex = multiDestinationIndex;
  }

  public boolean isResident() {
    return isResident;
  }

  public void setResident(ResidentItinerary residentItinerary) {
    this.residentItinerary = residentItinerary;
  }

  public void setIsResident(boolean isResident) {
    this.isResident = isResident;
  }

  public String getAirline() {
    return mAirline;
  }

  public void setAirline(String airline) {
    mAirline = airline;
  }

  public boolean isExecuteSearch() {
    return executeSearch;
  }

  public void setExecuteSearch(boolean executeSearch) {
    this.executeSearch = executeSearch;
  }

  public static class Builder {
    private int mNumberOfAdults;
    private int mNumberOfInfants;
    private int mNumberOfKids;
    private boolean mDirectFlight;
    private CabinClassDTO mCabinClass;
    private TravelType mTravelType;
    private List<FlightSegment> mFlightSegments;
    private boolean mIsResident;
    private String mAirline;
    private boolean executeSearch = false;

    public Builder() {
      MarketProviderInterface marketProvider =
          AndroidDependencyInjector.getInstance().provideMarketProvider();
      mNumberOfAdults = marketProvider.getAdultsByDefault();
      mNumberOfKids = marketProvider.getKidsByDefault();
      mNumberOfInfants = marketProvider.getInfantsByDefault();
    }

    public Builder setNumberOfAdults(int numberOfAdults) {
      this.mNumberOfAdults = numberOfAdults;
      return this;
    }

    public Builder setNumberOfInfants(int numberOfInfants) {
      this.mNumberOfInfants = numberOfInfants;
      return this;
    }

    public Builder setNumberOfKids(int numberOfKids) {
      this.mNumberOfKids = numberOfKids;
      return this;
    }

    public Builder setDirectFlight(boolean directFlight) {
      this.mDirectFlight = directFlight;
      return this;
    }

    public Builder setCabinClass(CabinClassDTO cabinClass) {
      this.mCabinClass = cabinClass;
      return this;
    }

    public TravelType getTravelType() {
      return mTravelType;
    }

    public Builder setTravelType(TravelType travelType) {
      this.mTravelType = travelType;
      return this;
    }

    public List<FlightSegment> getFlightSegments() {
      return mFlightSegments;
    }

    public Builder setFlightSegments(List<FlightSegment> flightSegments) {
      this.mFlightSegments = flightSegments;
      return this;
    }

    public Builder setIsResident(boolean isResident) {
      this.mIsResident = isResident;
      return this;
    }

    public Builder addDeparture(int index, String iata) {
      List<FlightSegment> segments = getSegments();
      while (segments.size() - 1 < index) {
        segments.add(new FlightSegment());
      }
      City city = new City();
      city.setIataCode(iata);
      segments.get(index).setDepartureCity(city);
      return this;
    }

    public Builder addDeparture(int index, City city) {
      List<FlightSegment> segments = getSegments();
      while (segments.size() - 1 < index) {
        segments.add(new FlightSegment());
      }
      segments.get(index).setDepartureCity(city);
      return this;
    }

    public Builder addArrival(int index, String iata) {
      List<FlightSegment> segments = getSegments();
      while (segments.size() - 1 < index) {
        segments.add(new FlightSegment());
      }
      City city = new City();
      city.setIataCode(iata);
      segments.get(index).setArrivalCity(city);
      return this;
    }

    public Builder addArrival(int index, City city) {
      List<FlightSegment> segments = getSegments();
      while (segments.size() - 1 < index) {
        segments.add(new FlightSegment());
      }
      segments.get(index).setArrivalCity(city);
      return this;
    }

    public Builder addDepartureDate(int index, long time) {
      List<FlightSegment> segments = getSegments();
      while (segments.size() - 1 < index) {
        segments.add(new FlightSegment());
      }
      segments.get(index).setDate(time);
      return this;
    }

    private List<FlightSegment> getSegments() {
      if (mFlightSegments == null) {
        mFlightSegments = new ArrayList<>();
      }
      return mFlightSegments;
    }

    public Builder addAirline(String airline) {
      mAirline = airline;
      return this;
    }

    public SearchOptions build() {
      SearchOptions searchOptions =
          new SearchOptions(mNumberOfAdults, mNumberOfInfants, mNumberOfKids, mDirectFlight,
              mCabinClass, mTravelType, mFlightSegments, mIsResident);
      searchOptions.setSearchDate(System.currentTimeMillis());
      searchOptions.setAirline(mAirline);
      searchOptions.setExecuteSearch(executeSearch);
      return searchOptions;
    }

    public void setExecuteSearch(boolean shouldExecuteSearch) {
      executeSearch = shouldExecuteSearch;
    }
  }
}
