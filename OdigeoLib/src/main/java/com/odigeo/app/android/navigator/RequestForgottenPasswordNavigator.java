package com.odigeo.app.android.navigator;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.RequestForgottenPasswordInstructionsSentView;
import com.odigeo.app.android.view.RequestForgottenPasswordView;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.presenter.contracts.navigators.RequestForgottenPasswordNavigatorInterface;

public class RequestForgottenPasswordNavigator extends BaseNavigator
    implements RequestForgottenPasswordNavigatorInterface {

  private static final String USER_EMAIL = "USER_EMAIL";
  private RequestForgottenPasswordView mRequestForgottenPasswordView;
  private RequestForgottenPasswordInstructionsSentView
      mRequestForgottenPasswordInstructionsSentView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view);
    Bundle args = this.getIntent().getExtras();
    String email = "";
    if (args != null) {
      email = args.getString(USER_EMAIL);
    }
    replaceFragment(R.id.fl_container, getInstanceRequestForgottenPasswordView(email));
    setNavigationTitle(
        LocalizablesFacade.getString(this, OneCMSKeys.SSO_PASSWORDRECOVERY_HEADER).toString());
  }

  @Override public void onBackPressed() {
    navigateBack();
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      // Respond to the action bar's Up/Home button
      case android.R.id.home:
        navigateBack();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void navigateBack() {

    android.support.v4.app.Fragment currentFragment =
        getSupportFragmentManager().findFragmentById(R.id.fl_container);
    if (currentFragment != null) {
      if (!currentFragment.getClass().equals(RequestForgottenPasswordView.class)) {
        replaceFragment(R.id.fl_container, getInstanceRequestForgottenPasswordView(""));
        setTitle(
            LocalizablesFacade.getString(this, OneCMSKeys.SSO_PASSWORDRECOVERY_HEADER).toString());
        setUpToolbarButton(0);
      } else {
        startActivity(new Intent(this, LoginNavigator.class));
        finish();
      }
    } else {
      super.onBackPressed();
    }
  }

  private RequestForgottenPasswordView getInstanceRequestForgottenPasswordView(String email) {
    if (mRequestForgottenPasswordView == null) {
      mRequestForgottenPasswordView = RequestForgottenPasswordView.newInstance(email);
    }
    return mRequestForgottenPasswordView;
  }

  private RequestForgottenPasswordInstructionsSentView getInstanceRequestForgottenPasswordIntructionsSentView(
      String email) {
    if (mRequestForgottenPasswordInstructionsSentView == null) {
      mRequestForgottenPasswordInstructionsSentView =
          RequestForgottenPasswordInstructionsSentView.newInstance(email);
    }
    return mRequestForgottenPasswordInstructionsSentView;
  }

  @Override public void navigateToLogin() {
    Intent intent = new Intent(getApplicationContext(), LoginNavigator.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(intent);
    finish();
  }

  @Override public void showInstructionsPage(String email) {
    RequestForgottenPasswordInstructionsSentView view =
        getInstanceRequestForgottenPasswordIntructionsSentView(email);
    replaceFragment(R.id.fl_container, view);
  }
}
