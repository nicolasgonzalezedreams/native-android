package com.odigeo.app.android.lib.utils;

/**
 * Created by emiliano.desantis on 10/12/2014.
 */

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * This class is a version of DatePickerDialog that honors the locale set by the app instead of the
 * locale of the system. According to the <a href="http://code.google.com/p/android/issues/detail?id=25107">bug
 * report on Android framework</a>, The date picker is not using the String array of shortened
 * month
 * of the Context of the App[1]. Instead it uses the System locale[2].
 * <p/>
 * <p/>
 * [1]<code>context.getResources().getStringArray(R.array.short_months);</code> [2]<a
 * href="http://www.grepcode.com/file/repository.grepcode.com/java/ext/com.google.android/android/4.1.1_r1/android/widget/DatePicker.java#482">
 * <code> mShortMonths[i] = DateUtils.getMonthString(Calendar.JANUARY + i,
 * DateUtils.LENGTH_MEDIUM);</code> </a>
 * <p/>
 * This class is licensed under <a href="http://www.apache.org/licenses/LICENSE-2.0.html">Apache
 * License 2.0</a>
 *
 * @author Wat Chun Pang Gilbert
 */
public class AppLocaleDatePickerDialog extends DatePickerDialog implements OnDateChangedListener {

  private static final String TAG = AppLocaleDatePickerDialog.class.getSimpleName();
  private static final int SDK_VERSION = 14;
  private static final int MONTH = 12;
  private static String monthDayFormat;
  protected DatePicker mDatePicker;
  protected Calendar mCalendar;

  public AppLocaleDatePickerDialog(Context context, int theme, OnDateSetListener callBack, int year,
      int monthOfYear, int dayOfMonth) {
    super(context, theme, callBack, year, monthOfYear, dayOfMonth);

    pullDatePickerRefFromSuper();
    initPicker(context);

    //mCalendar = Calendar.getInstance();
    mCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

    mDatePicker.init(year, monthOfYear, dayOfMonth, this);
    updateTitle(year, monthOfYear, dayOfMonth);
  }

  public AppLocaleDatePickerDialog(Context context, OnDateSetListener callBack, int year,
      int monthOfYear, int dayOfMonth) {
    this(context, R.style.MaterialTheme_DialogTheme, callBack, year, monthOfYear, dayOfMonth);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      setTitle("");
    } else {
      getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
  }

  /**
   * @return the monthLongFormat
   */
  public static SimpleDateFormat getMonthDayFormat() {

    return OdigeoDateUtils.getGmtDateFormat(monthDayFormat);
  }

  /**
   * Use reflection to use the Locale of the application for the month spinner.
   * <p/>
   * PS: DAMN DATEPICKER DOESN'T HONOR Locale.getDefault() <a href="http://code.google.com/p/android/issues/detail?id=25107">Bug
   * Report</a>
   */
  public final void initPicker(Context context) {
    monthDayFormat = context.getString(R.string.templates__datelong1);

    String monthPickerVarName;

    android.content.res.Configuration config =
        new android.content.res.Configuration(context.getResources().getConfiguration());
    config.locale = Configuration.getCurrentLocale();

    //Calendar cal = Calendar.getInstance();

    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

    Map<String, Integer> ord = sortByValue(
        cal.getDisplayNames(Calendar.MONTH, Calendar.SHORT, Configuration.getCurrentLocale()));

    String[] months =
        Arrays.copyOf(ord.keySet().toArray(), ord.keySet().toArray().length, String[].class);
    DecimalFormat formatter = new DecimalFormat("00");
    for (int i = 0; i < months.length; i++) {
      months[i] = formatter.format(i + 1);
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
      monthPickerVarName = "mMonthSpinner";
    } else {
      monthPickerVarName = "mMonthPicker";
    }

    try {
      Field[] f = mDatePicker.getClass().getDeclaredFields();

      for (Field field : f) {
        if (field.getName().equals("mShortMonths")) {
          field.setAccessible(true);
          field.set(mDatePicker, months);
        } else if (field.getName().equals(monthPickerVarName)) {
          field.setAccessible(true);
          Object o = field.get(mDatePicker);
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            Method m = o.getClass().getDeclaredMethod("setDisplayedValues", String[].class);
            m.setAccessible(true);
            m.invoke(o, (Object) months);
          } else {
            Method m =
                o.getClass().getDeclaredMethod("setRange", int.class, int.class, String[].class);
            m.setAccessible(true);
            m.invoke(o, 1, months.length, months);
          }
        }
      }
    } catch (Exception e) {
      Log.e(TAG, e.getMessage(), e);
    }

    try {
      Method updateSpinner = mDatePicker.getClass().getDeclaredMethod("updateSpinners");
      updateSpinner.setAccessible(true);
      updateSpinner.invoke(mDatePicker);
      updateSpinner.setAccessible(false);
    } catch (Exception e) {
      Log.e(TAG, e.getMessage(), e);
    }
  }

  /**
   * Create references of private variables named: <i>mDatePicker</i> &amp; <i>mCalendar</i> of
   * the super class to this class.
   */
  private void pullDatePickerRefFromSuper() {
    Field[] superF = AppLocaleDatePickerDialog.class.getSuperclass().getDeclaredFields();
    for (Field fi : superF) {

      if (fi.getName().equals("mDatePicker")) {
        fi.setAccessible(true);
        try {
          mDatePicker = (DatePicker) fi.get(this);
        } catch (Exception e) {
          Log.e(TAG, e.getMessage(), e);
        }
      } else if (fi.getName().equals("mCalendar")) {
        fi.setAccessible(true);
        try {
          mCalendar = (Calendar) fi.get(this);
        } catch (Exception e) {
          Log.e(TAG, e.getMessage(), e);
        }
      }
    }
  }

  public void onDateChanged(DatePicker view, int year, int month, int day) {
    view.init(year, month, day, this);
    updateTitle(year, month, day);
  }

  protected final void updateTitle(int year, int month, int day) {
    mCalendar.set(Calendar.YEAR, year);
    mCalendar.set(Calendar.MONTH, month);
    mCalendar.set(Calendar.DAY_OF_MONTH, day);
    setTitle(getMonthDayFormat().format(mCalendar.getTime()));
  }

  private Map sortByValue(Map map) {
    List list = new LinkedList(map.entrySet());
    Collections.sort(list, new Comparator() {
      public int compare(Object o1, Object o2) {
        return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(
            ((Map.Entry) (o2)).getValue());
      }
    });

    Map result = new LinkedHashMap();
    for (Iterator it = list.iterator(); it.hasNext(); ) {
      Map.Entry entry = (Map.Entry) it.next();
      result.put(entry.getKey(), entry.getValue());
    }
    return result;
  }
}
