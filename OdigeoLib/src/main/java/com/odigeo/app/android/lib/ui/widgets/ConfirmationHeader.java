package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.base.TwoTextWithImage;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.shoppingCart.BookingStatus;

/**
 * Created by emiliano.desantis on 08/10/2014.
 */
public class ConfirmationHeader extends TwoTextWithImage {

  private TextView title;
  private TextView description;
  private ImageView icon;

  public ConfirmationHeader(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  private void init() {
    title = (TextView) findViewById(getIdTextView());
    description = (TextView) findViewById(getIdSecondTextView());
    icon = (ImageView) findViewById(getIdImageView());
  }

  public final void setStatusConfirmation(BookingStatus statusConfirmation) {
    if (statusConfirmation != null) {

      if (statusConfirmation == BookingStatus.CONTRACT) {
        title.setText(
            LocalizablesFacade.getString(getContext(), OneCMSKeys.INFOSTATUS_TITLE_CONTRACT));
        title.setTextColor(getResources().getColor(R.color.semantic_positive));
        description.setText(LocalizablesFacade.getString(getContext(),
            "confirmationviewcontroller_infostatus_detail_contract"));
        icon.setImageResource(R.drawable.confirmation_congrats);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
          setBackground(ContextCompat.getDrawable(getContext(),
              R.drawable.shape_background_notification_cards));
        } else {
          setBackgroundDrawable(ContextCompat.getDrawable(getContext(),
              R.drawable.shape_background_notification_cards));
        }

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
            getResources().getDimensionPixelSize(R.dimen.size_icon_header_confirm_success),
            getResources().getDimensionPixelSize(R.dimen.size_icon_header_confirm_success));
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        params.setMargins(
            (int) getResources().getDimension(R.dimen.confirmation_reject_card_margin_left), 0, 0,
            0);
        icon.setLayoutParams(params);
        icon.refreshDrawableState();
      } else if (statusConfirmation == BookingStatus.PENDING) {
        title.setText(
            LocalizablesFacade.getString(getContext(), OneCMSKeys.INFOSTATUS_TITLE_PENDING));
        title.setTextColor(getResources().getColor(R.color.semantic_negative_non_blocker));
        description.setText(LocalizablesFacade.getString(getContext(),
            "confirmationviewcontroller_infostatus_detail_pending"));
        icon.setImageResource(R.drawable.confirmation_warning2);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
          setBackground(ContextCompat.getDrawable(getContext(),
              R.drawable.shape_background_notification_cards));
        } else {
          setBackgroundDrawable(ContextCompat.getDrawable(getContext(),
              R.drawable.shape_background_notification_cards));
        }
      } else {
        title.setText(
            LocalizablesFacade.getString(getContext(), OneCMSKeys.INFOSTATUS_TITLE_REJECT));
        title.setTextColor(getResources().getColor(R.color.semantic_negative_blocker));
        description.setText(LocalizablesFacade.getString(getContext(),
            "confirmationviewcontroller_infostatus_detail_reject"));
        icon.setImageResource(R.mipmap.icon_reject_card);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
          setBackground(
              ContextCompat.getDrawable(getContext(), R.drawable.shape_background_error_cards));
        } else {
          setBackgroundDrawable(
              ContextCompat.getDrawable(getContext(), R.drawable.shape_background_error_cards));
        }
      }
    }
  }

  /**
   * Set the correct values for a Duplicated Booking
   */
  public final void setDuplicatedStatus() {

    title.setText(LocalizablesFacade.getString(getContext(), "duplicatebooking_information_title")
        .toString());
    title.setTextColor(getResources().getColor(R.color.semantic_negative_non_blocker));
    description.setText(
        LocalizablesFacade.getString(getContext(), "duplicatebooking_information_subtitle")
            .toString());
    icon.setImageResource(R.drawable.confirmation_warning2);

    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
      setBackground(
          ContextCompat.getDrawable(getContext(), R.drawable.shape_background_notification_cards));
    } else {
      setBackgroundDrawable(
          ContextCompat.getDrawable(getContext(), R.drawable.shape_background_notification_cards));
    }
  }

  @Override protected final int getLayout() {
    return R.layout.layout_confirmation_header;
  }

  @Override protected final int getIdTextView() {
    return R.id.title_header_confirmation;
  }

  @Override protected final int getIdImageView() {
    return R.id.icon_header_confirmation;
  }

  @Override protected final int getIdSecondTextView() {
    return R.id.text_header_confirmation;
  }
}
