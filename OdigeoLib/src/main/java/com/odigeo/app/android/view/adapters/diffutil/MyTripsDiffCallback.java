package com.odigeo.app.android.view.adapters.diffutil;

import android.support.v7.util.DiffUtil;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.interactors.provider.MyTripsProvider;

public class MyTripsDiffCallback extends DiffUtil.Callback {

  private final MyTripsProvider oldItems;
  private final MyTripsProvider newItems;

  public MyTripsDiffCallback(MyTripsProvider oldItems, MyTripsProvider newItems) {
    this.oldItems = oldItems;
    this.newItems = newItems;
  }

  @Override public int getOldListSize() {
    return oldItems.size();
  }

  @Override public int getNewListSize() {
    return newItems.size();
  }

  @Override public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
    Booking oldBooking = oldItems.get(oldItemPosition);
    Booking newBooking = newItems.get(newItemPosition);

    if (oldBooking == null && newBooking == null) {
      return true;
    }

    if (oldBooking != null
        && newBooking != null
        && oldBooking.getBookingId() == newBooking.getBookingId()) {
      return true;
    }

    return false;
  }

  @Override public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
    Booking oldBooking = oldItems.get(oldItemPosition);
    Booking newBooking = newItems.get(newItemPosition);

    return oldBooking == null && newBooking == null
        || oldBooking != null && newBooking != null && oldBooking.equals(newBooking);
  }
}
