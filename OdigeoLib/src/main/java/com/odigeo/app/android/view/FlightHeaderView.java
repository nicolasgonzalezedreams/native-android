package com.odigeo.app.android.view;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.geo.City;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Modular fragment to display a brief information of the flight as a header.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 29/09/15
 */
public class FlightHeaderView extends Fragment {

  public static final String FRAGMENT_TRIP_SEPARATOR = "•";
  public static final String SEARCH_OPTIONS = "SEARCH_OPTIONS";
  public static final String NUMBER_ESCAPE = "%d";
  public static final String DATES_SEPARATOR = " - ";
  private LinearLayout headerTripDestinationInformation;
  private LinearLayout headerTripDestinationInformationComplementary;
  private SearchOptions searchOptions;
  private Fonts mFonts;

  public FlightHeaderView() {
    mFonts = Configuration.getInstance().getFonts();
  }

  /**
   * Retrieves a new Instance of the Fragment.
   *
   * @param searchOptions SearchOptions with that will be build the View.
   * @return A new instance of the fragment.
   */
  public static FlightHeaderView newInstance(SearchOptions searchOptions) {
    Bundle args = new Bundle();
    FlightHeaderView fragment = new FlightHeaderView();
    if (searchOptions == null) {
      throw new RuntimeException("The search options can't be null");
    }
    args.putSerializable(SEARCH_OPTIONS, searchOptions);
    fragment.setArguments(args);
    return fragment;
  }

  @Nullable @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.flight_header_view, container, false);
    headerTripDestinationInformation = (LinearLayout) view.findViewById(R.id.tripDestinationInfo);
    headerTripDestinationInformationComplementary =
        (LinearLayout) view.findViewById(R.id.tripDestinationInfoComplementary);
    Bundle bundle = getArguments();
    if (bundle != null) {
      searchOptions = ((SearchOptions) bundle.getSerializable(SEARCH_OPTIONS));
    }
    setNumberOfPassengers(view);
    setDates(view);
    buildHeaderTripDestinationInformation();
    return view;
  }

  /**
   * Add the dates to the header view.
   *
   * @param view Reference to the view to extract the TextView where will be set the value.
   */
  private void setDates(View view) {
    SimpleDateFormat dateFormat =
        OdigeoDateUtils.getGmtDateFormat(getResources().getString(R.string.templates__datelong3));
    TextView dates = ((TextView) view.findViewById(R.id.tripDate));
    dates.setTypeface(mFonts.getBold());
    dates.setTextColor(getResources().getColor(R.color.top_brief_font_color_date));

    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(dateFormat.format(searchOptions.getFirstSegment().getDate()));
    //Round or multi-destination.
    if (searchOptions.getFlightSegments().size() > 1) {
      stringBuilder.append(DATES_SEPARATOR);
      stringBuilder.append(dateFormat.format(searchOptions.getLastSegment().getDate()));
    }
    dates.setText(stringBuilder.toString());
  }

  /**
   * Set the number of passengers on the view.
   *
   * @param view Reference to the view to extract the TextView where will be set the value.
   */
  private void setNumberOfPassengers(View view) {
    int padding = getActivity().getResources().getDimensionPixelSize(R.dimen.header_icon_padding);
    TextView numberPassengers = ((TextView) view.findViewById(R.id.numberPassengers));
    numberPassengers.setTypeface(mFonts.getBold());
    numberPassengers.setTextColor(getResources().getColor(R.color.top_brief_font_color_date));
    numberPassengers.setText(String.format(NUMBER_ESCAPE, searchOptions.getTotalPassengers()));
    int image;
    if (searchOptions.getTotalPassengers() == 1) {
      image = R.drawable.search_1pax;
    } else {
      image = R.drawable.search_multi_pax;
    }
    DrawableUtils.setImageToTextView(getActivity(), numberPassengers, image, padding);
  }

  /**
   * Build the view with the search options information.
   */
  private void buildHeaderTripDestinationInformation() {
    TravelType travelType = searchOptions.getTravelType();
    LinearLayout conteiner = headerTripDestinationInformation;
    if (travelType != TravelType.MULTIDESTINATION) {
      buildSegmentTrip(searchOptions.getFlightSegments().get(0).getDepartureCity(),
          searchOptions.getFlightSegments().get(0).getArrivalCity(), travelType, false, false,
          conteiner);
    } else {
      List<FlightSegment> flightSegments = searchOptions.getFlightSegments();
      int maxIndexFlightSegment = flightSegments.size() - 1;
      for (int i = 0; i < flightSegments.size(); i++) {
        if (i >= 3) {
          conteiner = headerTripDestinationInformationComplementary;
        }
        buildSegmentTrip(flightSegments.get(i).getDepartureCity(),
            flightSegments.get(i).getArrivalCity(), travelType,
            flightSegments.indexOf(flightSegments.get(i)) == maxIndexFlightSegment,
            (i == 2) ? true : false, conteiner);
      }
    }
  }

  /**
   * Build a segment trip on the view.
   *
   * @param departureCity Departure City to draw.
   * @param arrivalCity Arrival City to draw.
   * @param travelType Type of travel to draw.
   * @param lastPosition if it's the last scale.
   */
  private void buildSegmentTrip(City departureCity, City arrivalCity, TravelType travelType,
      boolean lastPosition, boolean firstPosition, LinearLayout conteiner) {
    TextView departureCityView = new TextView(getActivity());
    departureCityView.setTypeface(mFonts.getBold());
    TextView arrivalCityView = new TextView(getActivity());
    arrivalCityView.setTypeface(mFonts.getBold());
    departureCityView.setTextColor(getResources().getColor(R.color.top_brief_font_color_city));
    arrivalCityView.setTextColor(getResources().getColor(R.color.top_brief_font_color_city));

    String cityDepartureLabel =
        travelType == TravelType.MULTIDESTINATION ? departureCity.getIataCode()
            : departureCity.getCityName();

    String cityReturnLabel = travelType == TravelType.MULTIDESTINATION ? arrivalCity.getIataCode()
        : arrivalCity.getCityName();

    departureCityView.setText(cityDepartureLabel);
    arrivalCityView.setText(cityReturnLabel);

    conteiner.addView(departureCityView);
    conteiner.addView(createSeparatorCity(travelType));
    conteiner.addView(arrivalCityView);

    if (travelType == TravelType.MULTIDESTINATION && !(lastPosition || firstPosition)) {
      conteiner.addView(createSeparatorFragmentTrip());
    }
  }

  /**
   * Creates a separator for the view.
   *
   * @return A new TextView with the separator.
   */
  private View createSeparatorFragmentTrip() {
    TextView textView = new TextView(getActivity());
    textView.setText(FRAGMENT_TRIP_SEPARATOR);
    textView.setTextColor(Color.BLACK);
    textView.setTypeface(mFonts.getBold());
    return textView;
  }

  /**
   * Creates a separator for the case when it's a city.
   *
   * @param travelType Travel type to decide what need to draw.
   * @return A new View to attach to the header.
   */
  private View createSeparatorCity(TravelType travelType) {
    ImageView imageView = new ImageView(getActivity());
    int padding = getActivity().getResources().getDimensionPixelSize(R.dimen.header_icon_padding);
    imageView.setPadding(padding, padding, padding, padding);
    if (travelType == TravelType.ROUND) {
      imageView.setImageResource(R.drawable.search_dobleflecha);
    } else {
      imageView.setImageResource(R.drawable.flecha_topbrierf);
    }
    return imageView;
  }
}
