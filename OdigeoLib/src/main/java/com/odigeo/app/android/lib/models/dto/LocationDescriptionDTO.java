package com.odigeo.app.android.lib.models.dto;

import com.odigeo.data.entity.geo.LocationDescriptionType;
import java.io.Serializable;

/**
 * @author carlos.aller@odigeo.com
 * @since 14/06/13
 */
public class LocationDescriptionDTO implements Serializable {
  /**
   * Serial version UID
   */
  private static final long serialVersionUID = 8402882497651165027L;

  private LocationDescriptionType type;
  private String name;
  private String countryCode;
  private String cityName;

  public LocationDescriptionDTO() {
  }

  public LocationDescriptionType getType() {
    return type;
  }

  public void setType(LocationDescriptionType type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }
}
