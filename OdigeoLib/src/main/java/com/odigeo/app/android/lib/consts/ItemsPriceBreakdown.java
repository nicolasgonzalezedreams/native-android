package com.odigeo.app.android.lib.consts;

/**
 * Created by Irving Lóp on 26/09/2014.
 */
public final class ItemsPriceBreakdown {
  public static final String ADULT_PRICE = "ADULTS_PRICE";
  public static final String CHILDREN_PRICE = "CHILDREN_PRICE";
  public static final String INFANTS_PRICE = "INFANTS_PRICE";
  public static final String BAGGAGE_PRICE = "BAGGAGE_PRICE";
  public static final String INSURANCE_PRICE = "INSURANCE_PRICE";
  public static final String PAYMENT_METHOD_PRICE = "PAYMENT_METHOD_PRICE";
  public static final String SERVICE_CHARGES = "SERVICE_CHARGES";

  private ItemsPriceBreakdown() {

  }
}
