package com.odigeo.app.android.view.helpers;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;

public class DrawableUtils {

  /**
   * Get a drawable tinted with the color entered.
   *
   * @param drawableResource Drawable Resource to be tinted.
   * @param colorResource Color Resource to be tinted.
   * @param activity {@link Activity} which will handle the {@link Drawable} resource.
   * @return Drawable tinted.
   */
  public static Drawable getTintedResource(int drawableResource, int colorResource,
      Activity activity) {
    Drawable drawable;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
      drawable = activity.getDrawable(drawableResource);
    } else {
      drawable = activity.getResources().getDrawable(drawableResource);
    }
    drawable = DrawableCompat.wrap(drawable);
    DrawableCompat.setTint(drawable, activity.getResources().getColor(colorResource));
    return drawable;
  }

  public static Drawable getTintedResource(int drawableResource, int colorResource,
      Context context) {
    Drawable drawable;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
      drawable = context.getDrawable(drawableResource);
    } else {
      drawable = context.getResources().getDrawable(drawableResource);
    }
    drawable = DrawableCompat.wrap(drawable);
    DrawableCompat.setTint(drawable, context.getResources().getColor(colorResource));
    return drawable;
  }

  public static void setImageToTextView(Context context, TextView textView, int resource,
      int padding) {
    Drawable drawable = DrawableUtils.getTintedResource(resource, R.color.mono05, context);
    textView.setCompoundDrawablePadding(padding);
    textView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
  }

  public static Drawable getTintedDrawable(Context context, Drawable source,
      @ColorRes int colorResId) {
    Drawable tintedDrawable = DrawableCompat.wrap(source).mutate();
    DrawableCompat.setTint(tintedDrawable, ContextCompat.getColor(context, colorResId));
    return tintedDrawable;
  }

  public static Drawable getTintedDrawable(Context context, @DrawableRes int drawableResId,
      @ColorRes int colorResId) {
    Drawable source = ContextCompat.getDrawable(context, drawableResId);
    return getTintedDrawable(context, source, colorResId);
  }

  public static Bitmap convertToBitmap(Drawable drawable) {
    int heightPixels = drawable.getIntrinsicHeight();
    int widthPixels = drawable.getIntrinsicWidth();
    Bitmap mutableBitmap = Bitmap.createBitmap(widthPixels, heightPixels, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(mutableBitmap);
    drawable.setBounds(0, 0, widthPixels, heightPixels);
    drawable.draw(canvas);

    return mutableBitmap;
  }
}
