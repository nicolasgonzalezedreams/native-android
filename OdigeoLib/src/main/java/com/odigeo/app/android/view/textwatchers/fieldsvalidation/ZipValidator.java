package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

public class ZipValidator extends BaseValidator implements FieldValidator {

  private static final String CHARACTERS_ALLOWED =
      "-/, A-Za-zÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜäëïöüşŞıçÇ'ŒœßØøÅåÆæÞþÐ ";
  private static final String REGEX_ZIP = "[" + CHARACTERS_ALLOWED + "0-9]{1,10}";

  public ZipValidator() {
    super();
  }

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_ZIP);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
