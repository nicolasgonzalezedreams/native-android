package com.odigeo.app.android.lib.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import com.odigeo.app.android.lib.R;

/**
 * Created by Oscar Álvarez on 19/03/2015.
 */
public class FirstImportTripDialogFragment extends DialogFragment {

  public static final int TIME_TO_WIDENING = 5001;

  public static FirstImportTripDialogFragment newInstance() {
    return new FirstImportTripDialogFragment();
  }

  @Override public final View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.layout_first_import_trip_fragment, container, false);

    view.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        dismiss();
      }
    });
    new Handler().postDelayed(new Runnable() {
      @Override public void run() {
        if (FirstImportTripDialogFragment.this.getView().isShown()) {
          getFragmentManager().popBackStack();
          dismiss();
        }
      }
    }, TIME_TO_WIDENING);
    return view;
  }

  /**
   * The system calls this only when creating the layout in a dialog.
   */
  @Override public final Dialog onCreateDialog(Bundle savedInstanceState) {
    // The only reason you might override this method when using onCreateView() is
    // to modify any dialog characteristics. For example, the dialog includes a
    // title by default, but your custom layout might not need it. So here you can
    // remove the dialog title, but you must call the superclass to get the Dialog.
    Dialog dialog = super.onCreateDialog(savedInstanceState);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    return dialog;
  }
}
