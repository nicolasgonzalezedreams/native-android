package com.odigeo.app.android.lib.models;

import com.odigeo.app.android.lib.models.dto.LocationDTO;
import java.io.Serializable;
import java.util.List;

/**
 * Created by ManuelOrtiz on 29/10/2014.
 */
public class Airports implements Comparable<Airports>, Serializable {
  private String title;
  private List<LocationDTO> airports;
  private int order;

  public final String getTitle() {
    return title;
  }

  public final void setTitle(String title) {
    this.title = title;
  }

  public final List<LocationDTO> getAirports() {
    return airports;
  }

  public final void setAirports(List<LocationDTO> airports) {
    this.airports = airports;
  }

  public final int getOrder() {
    return order;
  }

  public final void setOrder(int order) {
    this.order = order;
  }

  @Override public final int compareTo(Airports another) {
    if (another == null) {
      return 1;
    }

    return order - another.order;
  }
}
