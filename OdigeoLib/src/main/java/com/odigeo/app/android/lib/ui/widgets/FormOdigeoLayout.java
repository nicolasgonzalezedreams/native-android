package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;

/**
 * Created by Irving on 02/10/2014.
 */

public class FormOdigeoLayout extends LinearLayout {
  private TextView txtTitle;
  private TextView textSubTitle;
  private LinearLayout lytFormTitleContainer;

  public FormOdigeoLayout(Context context, String tittle) {
    super(context);
    inflateLayout();
    init(tittle);
  }

  public FormOdigeoLayout(Context context, AttributeSet attrs) {
    super(context, attrs, 0);
    inflateLayout();

    TypedArray typedArray =
        context.obtainStyledAttributes(attrs, R.styleable.FormOdigeoLayout, 0, 0);
    String title = typedArray.getString(R.styleable.FormOdigeoLayout_textTitle);
    String subTitle = typedArray.getString(R.styleable.FormOdigeoLayout_textSubtitle);
    typedArray.recycle();

    init(title, subTitle);
  }

  public FormOdigeoLayout(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    inflateLayout();

    TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.FormOdigeoLayout);
    String title = typedArray.getString(R.styleable.FormOdigeoLayout_textTitle);
    String subTitle = typedArray.getString(R.styleable.FormOdigeoLayout_textSubtitle);
    typedArray.recycle();

    init(title, subTitle);
  }

  private void inflateLayout() {
    inflate(this.getContext(), R.layout.layout_form_odigeo, this);
    setBackgroundResource(R.drawable.background_card);
    txtTitle = (TextView) findViewById(R.id.text_title_form_odigeo);
    textSubTitle = (TextView) findViewById(R.id.text_subtitle_form_odigeo);
    lytFormTitleContainer = (LinearLayout) findViewById(R.id.lytFormTitleContainer);
  }

  private void init(String title) {
    if (title != null) {
      setTitleText(LocalizablesFacade.getString(getContext(), title));
    }
  }

  private void init(String title, String subTitle) {
    init(title);
    if (subTitle != null) {
      setSubtitleText(LocalizablesFacade.getString(getContext(), subTitle));
    }
  }

  private void setTitleText(CharSequence title) {
    txtTitle.setText(title);
  }

  public final String getTitle() {
    return txtTitle.getText().toString();
  }

  public final void setTitle(CharSequence title) {
    setTitleText(title);
  }

  private void setSubtitleText(CharSequence subTitle) {
    if (textSubTitle.getVisibility() != VISIBLE) {
      textSubTitle.setVisibility(VISIBLE);
    }
    textSubTitle.setText(subTitle);
  }

  public final void setTextSubTitle(CharSequence subTitle) {
    setSubtitleText(subTitle);
  }

  public final LinearLayout getTitleContainerLayout() {
    return lytFormTitleContainer;
  }
}
