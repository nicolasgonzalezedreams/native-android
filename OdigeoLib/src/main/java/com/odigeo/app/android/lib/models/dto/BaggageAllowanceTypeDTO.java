package com.odigeo.app.android.lib.models.dto;

public enum BaggageAllowanceTypeDTO {
  KG, PO, NI, SC, NP, SZ, VL, WE, ND;
}
