package com.odigeo.app.android.lib.models.dto;

/**
 * @author miguel
 */
public class PreferencesRequestDTO {

  protected String locale;
  protected String marketingPortal;
  protected String webSite;
  protected String buypath;

  /**
   * Gets the value of the locale property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getLocale() {
    return locale;
  }

  /**
   * Sets the value of the locale property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setLocale(String value) {
    this.locale = value;
  }

  /**
   * Gets the value of the marketingPortal property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getMarketingPortal() {
    return marketingPortal;
  }

  /**
   * Sets the value of the marketingPortal property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setMarketingPortal(String value) {
    this.marketingPortal = value;
  }

  /**
   * Gets the value of the webSite property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getWebSite() {
    return webSite;
  }

  /**
   * Sets the value of the webSite property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setWebSite(String value) {
    this.webSite = value;
  }

  /**
   * @return the buyPath
   */
  public String getBuypath() {
    return buypath;
  }

  /**
   * @param buyPath the buyPath to set
   */
  public void setBuypath(String buypath) {
    this.buypath = buypath;
  }
}
