package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.SeekBar;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.view.helpers.DrawableUtils;

/**
 * Created by emiliano.gudino on 17/11/2014.
 */
@Deprecated public class CustomSeekBar extends SeekBar {
  //    implements SeekBar.OnSeekBarChangeListener

  private static final String TIME_ABBREVIATION = "h";
  private static final float HORIZONTAL_MOV = 2.0f;
  private static final float VERTICAL_MOV = 25.0f;
  float horizontalAdjust;
  float verticalAdjust;
  private Drawable progressDrawable;
  private Bitmap labelBackground;
  private Bitmap labelBackgroundCircle;
  private Paint labelTextPaint;
  private Paint labelBackgroundPaint;
  private Point labelPos;
  private Rect barBounds;
  private Rect labelTextRect;
  private int viewWidth;
  private int barHeight;

  //    private PropertyChangeListener propertyChangeListener;

  public CustomSeekBar(Context context) {
    super(context);
    init();
  }

  public CustomSeekBar(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  /**
   * Initialization of the elements in the view.
   */
  private void init() {
    progressDrawable = getProgressDrawable();

    labelBackground = BitmapFactory.decodeResource(getResources(), R.drawable.bubble_small);
    labelBackgroundCircle = DrawableUtils.convertToBitmap(
        DrawableUtils.getTintedResource(R.drawable.filters_circle, R.color.slider_color,
            getContext()));

    labelTextPaint = new Paint();
    labelTextPaint.setColor(getResources().getColor(R.color.basic_dark));
    labelTextPaint.setTextSize(getResources().getDimension(R.dimen.size_font_S));
    labelTextPaint.setAntiAlias(true);
    labelTextPaint.setDither(true);

    labelBackgroundPaint = new Paint();

    barBounds = new Rect();
    labelTextRect = new Rect();

    labelPos = new Point();
  }

  @Override
  protected final synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);

    if (labelBackground != null) {

      viewWidth = getMeasuredWidth();
      barHeight = getMeasuredHeight();
      setMeasuredDimension(viewWidth, barHeight + labelBackground.getHeight());
    }
  }

  @Override protected final synchronized void onDraw(Canvas canvas) {
    if (labelBackground != null) {
      barBounds.left = getPaddingLeft();
      barBounds.top = labelBackground.getHeight() + getPaddingTop();
      barBounds.right = barBounds.left + viewWidth - getPaddingRight() - getPaddingLeft();
      barBounds.bottom = barBounds.top + barHeight - getPaddingBottom() - getPaddingTop();

      float progressPosX =
          barBounds.left + ((float) this.getProgress() / (float) this.getMax()) * barBounds.width();

      int labelOffset = 0;
      labelPos.x = (int) progressPosX - labelOffset;
      labelPos.y = getPaddingTop();

      progressDrawable = getProgressDrawable();
      progressDrawable.setBounds(barBounds.left, barBounds.top, barBounds.right, barBounds.bottom);
      progressDrawable.draw(canvas);

      String labelText = getProgress() + TIME_ABBREVIATION;
      labelTextPaint.getTextBounds(labelText, 0, labelText.length(), labelTextRect);

      adjustDimension();

      canvas.drawBitmap(labelBackground, labelPos.x, labelPos.y, labelBackgroundPaint);
      canvas.drawBitmap(labelBackgroundCircle, (labelPos.x) - horizontalAdjust,
          (labelPos.y) + verticalAdjust, labelBackgroundPaint);
      canvas.drawText(labelText,
          labelPos.x + labelBackground.getWidth() / 2 - labelTextRect.width() / 2,
          labelPos.y + labelBackground.getHeight() / 2 + labelTextRect.height() / 2,
          labelTextPaint);

      int thumbX = (int) progressPosX - getThumbOffset();
      BitmapDrawable thumbDrawable = new BitmapDrawable();
      thumbDrawable.setBounds(thumbX, barBounds.top, thumbX + thumbDrawable.getIntrinsicWidth(),
          barBounds.top + thumbDrawable.getIntrinsicHeight());
      thumbDrawable.draw(canvas);
    } else {
      super.onDraw(canvas);
    }
  }

  public final void adjustDimension() {

    float density = getResources().getDisplayMetrics().density;
    horizontalAdjust = HORIZONTAL_MOV * density;
    verticalAdjust = VERTICAL_MOV * density;
  }
}