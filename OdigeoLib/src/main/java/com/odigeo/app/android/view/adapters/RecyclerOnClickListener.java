package com.odigeo.app.android.view.adapters;

/**
 * Created by carlos.navarrete on 09/09/15.
 */
public interface RecyclerOnClickListener {
  void onClick(int position, int viewType);
}
