package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;
import com.odigeo.app.android.lib.pojos.RowStandardIconWidgetHolder;

/**
 * Created by Irving on 28/08/2014.
 */
@Deprecated public class RowStandardIconWidgetAdapter extends ArrayAdapter<String> {
  private final int idRowLayout;
  private final Context context;
  private final int idTextRowLayout;
  private final int idIconRowLayout;
  private final Fonts fonts;
  private String[] values;
  private int[] iconsIds;

  public RowStandardIconWidgetAdapter(Context context, int idRowLayout, String[] objects,
      int[] iconsIds, int idTextRowLayout, int idIconRowLayout) {
    super(context, idRowLayout, objects);
    this.context = context;
    this.idRowLayout = idRowLayout;
    this.idIconRowLayout = idIconRowLayout;
    this.idTextRowLayout = idTextRowLayout;
    if (objects.length == iconsIds.length) {
      values = objects;
      this.iconsIds = iconsIds;
    } else {
      Log.e(this.getClass().getCanonicalName(), "The arrays have different size");
    }
    fonts = Configuration.getInstance().getFonts();
  }

  public RowStandardIconWidgetAdapter(Context context, String[] values, int[] iconsIds) {
    super(context, R.layout.row_standard_icon_widget, values);
    this.context = context;
    this.idRowLayout = R.layout.row_standard_icon_widget;
    this.values = values;
    this.iconsIds = iconsIds;
    this.idIconRowLayout = R.id.icon_row_standardIconWidget;
    this.idTextRowLayout = R.id.text_row_standardIconWidget;
    fonts = Configuration.getInstance().getFonts();
  }

  public RowStandardIconWidgetAdapter(Context context, int idLayout, String[] values,
      int[] iconsIds) {
    super(context, R.layout.row_standard_icon_widget, values);
    this.context = context;
    this.idRowLayout = idLayout;
    this.values = values;
    this.iconsIds = iconsIds;
    this.idIconRowLayout = R.id.icon_row_standardIconWidget;
    this.idTextRowLayout = R.id.text_row_standardIconWidget;
    fonts = Configuration.getInstance().getFonts();
  }

  /* Pendiente****************

      Insertar Tipografía en el TExtView
   */
  @Override public View getView(int position, View convertView, ViewGroup parent) {
    View rowView = convertView;
    if (rowView == null) {
      LayoutInflater inflater =
          (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      rowView = inflater.inflate(idRowLayout, parent, false);
      RowStandardIconWidgetHolder viewHolder = new RowStandardIconWidgetHolder();
      viewHolder.icon = (ImageView) rowView.findViewById(idIconRowLayout);
      viewHolder.text = (TextView) rowView.findViewById(idTextRowLayout);

      viewHolder.text.setTypeface(fonts.getSemiBold());
      rowView.setTag(viewHolder);
    }

    // para evitar linea en el primer elemento de la lista de la pabtala About
    if (idRowLayout == R.layout.layout_about_list_row && position == 0) {
      rowView.findViewById(R.id.about_list_row_stroke).setVisibility(View.INVISIBLE);
    }

    RowStandardIconWidgetHolder viewHolder = (RowStandardIconWidgetHolder) rowView.getTag();
    viewHolder.icon.setImageResource(iconsIds[position]);
    viewHolder.text.setText(values[position]);
    return rowView;
  }
}
