package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.base.ButtonWithHint;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.data.entity.shoppingCart.TravellerTitle;
import com.odigeo.data.entity.shoppingCart.TravellerType;

/**
 * Passenger rows the passenger type and name
 *
 * @author Manuel Ortiz
 * @author Javier Silva
 * @since 10/12/2014
 */
public class PassengerResumeRow extends ButtonWithHint {
  private int imageSource;
  private Traveller passenger;

  public PassengerResumeRow(Context context, Traveller passenger) {
    super(context);
    this.passenger = passenger;

    initialize();
  }

  public PassengerResumeRow(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray aAttrs =
        context.obtainStyledAttributes(attrs, R.styleable.ImageWithTitleAndDescription, 0, 0);
    imageSource = aAttrs.getResourceId(R.styleable.ImageWithTitleAndDescription_android_src, 0);

    aAttrs.recycle();

    initialize();
  }

  private void initialize() {
    ImageView icon = (ImageView) findViewById(R.id.imgTravellerType);

    if (passenger != null) {
      if (passenger.getTravellerType() == TravellerType.ADULT) {
        icon.setImageResource(R.drawable.confirmation_adult);
        setHint(CommonLocalizables.getInstance().getCommonAdult(getContext()));
      } else if (passenger.getTravellerType() == TravellerType.CHILD) {
        icon.setImageResource(R.drawable.confirmation_child);
        setHint(CommonLocalizables.getInstance().getCommonChild(getContext()));
      } else if (passenger.getTravellerType() == TravellerType.INFANT) {
        icon.setImageResource(R.drawable.confirmation_infant);
        setHint(CommonLocalizables.getInstance().getCommonInfant(getContext()));
      }

      //Addition of the title
      StringBuilder stringPassenger;
      TravellerTitle type = passenger.getTitle();
      if (type == TravellerTitle.MR) {
        stringPassenger =
            new StringBuilder(LocalizablesFacade.getString(getContext(), "common_mr"));
      } else if (type == TravellerTitle.MRS) {
        stringPassenger =
            new StringBuilder(LocalizablesFacade.getString(getContext(), "common_mrs"));
      } else {
        stringPassenger =
            new StringBuilder(LocalizablesFacade.getString(getContext(), "common_ms"));
      }
      if (!TextUtils.isEmpty(stringPassenger)) {
        stringPassenger.append(" ");
      }
      stringPassenger.append(passenger.getName()).append(" ").append(passenger.getFirstLastName());
      setText(stringPassenger.toString());
    } else {
      if (icon != null) {
        icon.setImageResource(imageSource);
      }
    }
  }

  @Override public final int getLayout() {
    return R.layout.layout_confirmation_item_row;
  }
}
