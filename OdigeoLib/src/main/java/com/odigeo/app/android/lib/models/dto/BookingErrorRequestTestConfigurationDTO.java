package com.odigeo.app.android.lib.models.dto;

public class BookingErrorRequestTestConfigurationDTO {

  protected String providerId;
  protected ErrorTypeTestConfigurationDTO errorType;

  /**
   * Gets the value of the providerId property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getProviderId() {
    return providerId;
  }

  /**
   * Sets the value of the providerId property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setProviderId(String value) {
    this.providerId = value;
  }

  /**
   * Gets the value of the errorType property.
   *
   * @return possible object is
   * {@link ErrorTypeTestConfiguration }
   */
  public ErrorTypeTestConfigurationDTO getErrorType() {
    return errorType;
  }

  /**
   * Sets the value of the errorType property.
   *
   * @param value allowed object is
   * {@link ErrorTypeTestConfiguration }
   */
  public void setErrorType(ErrorTypeTestConfigurationDTO value) {
    this.errorType = value;
  }
}
