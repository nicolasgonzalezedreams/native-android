package com.odigeo.app.android.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Switch;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.OdigeoSpinnerLocalAdapter;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.base.OdigeoSpinner;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.utils.DrawablesFactory;
import com.odigeo.app.android.view.adapters.BaseAdaptersFactory;
import com.odigeo.app.android.view.animations.CollapseAndExpandAnimationUtil;
import com.odigeo.app.android.view.animations.FlipAnimation;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DateHelper;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.app.android.view.textwatchers.PaymentMethodBaseTextWatchersClient;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.RequestValidationHandler;
import com.odigeo.data.entity.extensions.ShoppingCartCollectionOptionSpinner;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.userData.CreditCard;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.BinCheckPresenter;
import com.odigeo.presenter.PaymentFormWidgetPresenter;
import com.odigeo.presenter.contracts.views.BinCheckViewInterface;
import com.odigeo.presenter.contracts.views.PaymentFormWidgetInterface;
import com.odigeo.presenter.listeners.OnClickSavedPaymentMethodRowListener;
import com.odigeo.presenter.listeners.PaymentWidgetFormListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_PAYMENT_DETAILS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_FLIGHTS_PAYMENT;
import static com.odigeo.presenter.ExpirateDateCreditCardValidator.validateDate;

public class PaymentFormWidgetView extends LinearLayout
    implements PaymentFormWidgetInterface, BinCheckViewInterface,
    OnClickSavedPaymentMethodRowListener {

  public static final int SPINNER_POPUP_HEIGHT = 500;
  private static final int FOLLOWING_YEARS_SINCE_NOW = 7;
  private static Map<String, Integer> paymentMethodResources;
  private Context mContext;
  private PaymentFormWidgetPresenter mPaymentFormWidgetPresenter;
  private ScrollView mSvPayment;
  private TextInputLayout mTilCreditCardNumber, mTilNameOnCard, mTilCVV;
  private TextView mTvCardDescriptionTooltip, mTvAMEXCardDescriptionTooltip,
      tvSavePaymentMethodInfo;
  private Spinner mSpMonthDate, mSpYearDate;
  private RadioButton rbtnCreditCard, rbtnBankTransfer, rbtnPaypal, rbtnTrustly, rbtnKlarna;
  private LinearLayout mLlCreditCardWidget, mLlBankTransferWidget, mLlPaypalWidget,
      mLlTrustlyWidget, mLlKlarnaWidget, mLlCVVToolTip, llSavedPaymentMethod, llStorePaymentMethod;
  private RelativeLayout mRlCVVTooltipAMEX, mRlCVVTooltipNormal;
  private TextView mTvPaymentFormTitle, mTvBankTrasnferFirstContainer,
      mTvBankTransferSecondContainer, mTvBankTransferThirdContainer, mTvExpiryDateError,
      mTvExpiryDate, mTvPaypalInformation, mTvTrustlyInformation, mTvKlarnaInformation,
      mTvPaypalHeader, mTvTrustlyHeader, mTvKlarnaHeader;
  private FlipAnimation mFlipAnimation;
  private ImageView mIvCard, mIvBankTransfer, mIvPaypal, mIvTrustly, mIvKlarna, mIvCVVInfoTooltip;
  private View mVcreditCardSeparator, mVBankTransferSeparator, mVPayPalSeparator,
      mVTrustlySeparator, mVKlarnaSeparator, mWidgetPreviousSelected, mVCvvTooltipSeparator;
  private Switch swStorePaymentMethod;
  private boolean isSpinnerMonthInitialization = true, isSpinnerYearInitialization = true;
  private DateHelper mDateHelper;
  private PaymentMethodBaseTextWatchersClient mPaymentMethodBaseTextWatchersClient;
  private List<ShoppingCartCollectionOption> mShoppingCartCollectionOption;
  private OdigeoSpinner mSpCreditCardMethod;
  private String mCreditCardMethodCode, mExpirationMonthHintSpinner, mExpirationYearHintSpinner,
      paypalHeader, truslyHeader, klarnaHeader;
  private TrackerControllerInterface mTracker;
  private List<SavedPaymentMethodFormView> savedPaymentMethodFormViews = new ArrayList<>();
  private int savedPaymentMethodTag = 0;
  private PaymentWidgetFormListener paymentWidgetFormListener;

  public PaymentFormWidgetView(Context context) {
    super(context);
  }

  public PaymentFormWidgetView(PaymentWidgetFormListener paymentWidgetFormListener, Context context,
      List<ShoppingCartCollectionOption> shoppingCartCollectionOption, ScrollView scrollView) {
    super(context);
    mContext = context;
    mShoppingCartCollectionOption = shoppingCartCollectionOption;
    mSvPayment = scrollView;
    this.paymentWidgetFormListener = paymentWidgetFormListener;
    bindViews();
    initContent();
    initPresenters();
    initTextInputLayouts();
    initSpinners();
    initListener();
  }

  private void bindViews() {
    LayoutInflater inflater =
        (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    inflater.inflate(R.layout.layout_payment_widget_form, this, true);
    bindPaymentFormViews();
    bindBanktransferViews();
    bindPayPalViews();
    bindTrustlyViews();
    bindKlarnaViews();

    mTracker = AndroidDependencyInjector.getInstance().provideTrackerController();
    mDateHelper = AndroidDependencyInjector.getInstance().provideDateHelper();
    paymentMethodResources = DrawablesFactory.createCreditCardResourcesMap();
    mFlipAnimation = new FlipAnimation(mIvCard, R.drawable.card_placeholder);
    mWidgetPreviousSelected = mLlCreditCardWidget;
  }

  private void bindPaymentFormViews() {
    llSavedPaymentMethod = (LinearLayout) findViewById(R.id.llSavePaymentMethods);
    mLlCreditCardWidget = (LinearLayout) findViewById(R.id.llCreditCardWidget);
    mTvExpiryDate = (TextView) findViewById(R.id.tvExpiryDate);
    mTvPaymentFormTitle = (TextView) findViewById(R.id.tvPaymentTitle);
    mVcreditCardSeparator = findViewById(R.id.vCreditCardSeparator);
    mTilCreditCardNumber = (TextInputLayout) findViewById(R.id.tilCreditCardNumber);
    mIvCard = (ImageView) findViewById(R.id.creditCardImage);
    mTilNameOnCard = (TextInputLayout) findViewById(R.id.tilNameOnCard);
    mSpMonthDate = (Spinner) findViewById(R.id.spMonthDate);
    mSpYearDate = (Spinner) findViewById(R.id.spYearDate);
    mTilCVV = (TextInputLayout) findViewById(R.id.tilCVV);
    mVCvvTooltipSeparator = findViewById(R.id.vCVVToltipSeparator);
    mLlCVVToolTip = (LinearLayout) findViewById(R.id.llCVVTooltip);
    mRlCVVTooltipAMEX = (RelativeLayout) findViewById(R.id.rlAMEXCardCVV);
    mRlCVVTooltipNormal = (RelativeLayout) findViewById(R.id.rlNormalCardCVV);
    mTvCardDescriptionTooltip = (TextView) findViewById(R.id.tvCVVCardDescription);
    mTvAMEXCardDescriptionTooltip = (TextView) findViewById(R.id.tvCVVAMEXCardDescription);
    mIvCVVInfoTooltip = (ImageView) findViewById(R.id.ivCVVInfoTooltip);
    mSpCreditCardMethod = (OdigeoSpinner) findViewById(R.id.spCreditcardType);
    rbtnCreditCard = (RadioButton) findViewById(R.id.rbtnCreditCard);
    mTvExpiryDateError = (TextView) findViewById(R.id.tvExpirtyDateError);
    swStorePaymentMethod = (Switch) findViewById(R.id.swStorePaymentMethod);
    tvSavePaymentMethodInfo = (TextView) findViewById(R.id.tvStorePaymentMethodLinksInfo);
    llStorePaymentMethod = (LinearLayout) findViewById(R.id.llStorePaymentMethod);
  }

  private void bindBanktransferViews() {
    rbtnBankTransfer = (RadioButton) findViewById(R.id.rbtnBanktransfer);
    mVBankTransferSeparator = findViewById(R.id.vBankTransferSeparator);
    mTvBankTransferSecondContainer = (TextView) findViewById(R.id.tvBankTransferSecondContainer);
    mTvBankTransferThirdContainer = (TextView) findViewById(R.id.tvBankTransferThirdContainer);
    mTvBankTrasnferFirstContainer = (TextView) findViewById(R.id.tvBankTransferFirstContainer);
    mLlBankTransferWidget = (LinearLayout) findViewById(R.id.llBanktransferWidget);
    mIvBankTransfer = (ImageView) findViewById(R.id.ivBankTransfer);
  }

  private void bindPayPalViews() {
    LinearLayout llPaypal = (LinearLayout) findViewById(R.id.widgetPaypal);
    mLlPaypalWidget = (LinearLayout) llPaypal.findViewById(R.id.llPaymentMethodWidget);
    rbtnPaypal = (RadioButton) llPaypal.findViewById(R.id.rbPaymentMethod);
    mIvPaypal = (ImageView) llPaypal.findViewById(R.id.ivPaymentMethod);
    mTvPaypalInformation = (TextView) llPaypal.findViewById(R.id.tvPaymentMethodContainer);
    mTvPaypalHeader = (TextView) llPaypal.findViewById(R.id.tvPaymentMethodHeader);
    mVPayPalSeparator = llPaypal.findViewById(R.id.vPaymentMethodSeparator);
  }

  private void bindTrustlyViews() {
    LinearLayout llTrustly = (LinearLayout) findViewById(R.id.widgetTrustly);
    mLlTrustlyWidget = (LinearLayout) llTrustly.findViewById(R.id.llPaymentMethodWidget);
    rbtnTrustly = (RadioButton) llTrustly.findViewById(R.id.rbPaymentMethod);
    mIvTrustly = (ImageView) llTrustly.findViewById(R.id.ivPaymentMethod);
    mTvTrustlyInformation = (TextView) llTrustly.findViewById(R.id.tvPaymentMethodContainer);
    mTvTrustlyHeader = (TextView) llTrustly.findViewById(R.id.tvPaymentMethodHeader);
    mVTrustlySeparator = llTrustly.findViewById(R.id.vPaymentMethodSeparator);
  }

  private void bindKlarnaViews() {
    LinearLayout llKlarna = (LinearLayout) findViewById(R.id.widgetKlarna);
    mLlKlarnaWidget = (LinearLayout) llKlarna.findViewById(R.id.llPaymentMethodWidget);
    rbtnKlarna = (RadioButton) llKlarna.findViewById(R.id.rbPaymentMethod);
    mIvKlarna = (ImageView) llKlarna.findViewById(R.id.ivPaymentMethod);
    mTvKlarnaInformation = (TextView) llKlarna.findViewById(R.id.tvPaymentMethodContainer);
    mTvKlarnaHeader = (TextView) llKlarna.findViewById(R.id.tvPaymentMethodHeader);
    mVKlarnaSeparator = llKlarna.findViewById(R.id.vPaymentMethodSeparator);
  }

  private void initContent() {
    mTvExpiryDate.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.CREDIT_CARD_EXPIRATION_DATE));
    mTvPaymentFormTitle.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.PAYMENT_FORMMODULE_HEADER));
    mTilCreditCardNumber.setHint(
        LocalizablesFacade.getString(mContext, OneCMSKeys.HINT_CREDIT_CARD_NUMBER));
    mTilNameOnCard.setHint(
        LocalizablesFacade.getString(mContext, OneCMSKeys.HINT_CREDIT_CARD_NAME));
    mTvBankTrasnferFirstContainer.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.BANKTRANSFER_FISRT_CONTAINER));
    mTvBankTransferSecondContainer.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.BANKTRANSFER_SECOND_CONTAINER));
    mTvBankTransferThirdContainer.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.BANKTRANSFER_THIRD_CONTAINER));
    mTilCVV.setHint(LocalizablesFacade.getString(mContext, OneCMSKeys.HINT_CREDIT_CARD_CVV));
    rbtnCreditCard.setText(LocalizablesFacade.getString(mContext, OneCMSKeys.CREDITCARD_WIDGET));
    rbtnBankTransfer.setText(LocalizablesFacade.getString(mContext, OneCMSKeys.BANKTRANSFER));
    mExpirationMonthHintSpinner =
        LocalizablesFacade.getString(mContext, OneCMSKeys.CREDIT_CARD_EXPIRATION_MONTH).toString();
    mTvExpiryDateError.setText(LocalizablesFacade.getString(mContext,
        OneCMSKeys.VALIDATION_ERROR_IDENTIFICATION_EXPIRATION));
    mExpirationYearHintSpinner =
        LocalizablesFacade.getString(mContext, OneCMSKeys.CREDIT_CARD_EXPIRATION_YEAR).toString();
    mTvPaypalInformation.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.PAYPAL_INFORMATION));
    paypalHeader =
        LocalizablesFacade.getString(mContext, OneCMSKeys.PAYPAL_PAY_SECURELY).toString();
    mIvPaypal.setImageResource(R.drawable.paypal_form_icon);
    rbtnPaypal.setText(R.string.paypal);
    mTvTrustlyInformation.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.TRUSLY_INFORMATION));
    truslyHeader =
        LocalizablesFacade.getString(mContext, OneCMSKeys.TRUSLY_PAY_SECURELY).toString();
    mIvTrustly.setImageResource(R.drawable.trustly_form_icon);
    rbtnTrustly.setText(R.string.trustly);
    mTvKlarnaInformation.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.KLARNA_INFORMATION));
    klarnaHeader =
        LocalizablesFacade.getString(mContext, OneCMSKeys.KLARNA_PAY_SECURELY).toString();
    mIvKlarna.setImageResource(R.drawable.klarna_form_icon);
    rbtnKlarna.setText(R.string.klarna);
    mTvCardDescriptionTooltip.setText(HtmlUtils.formatHtml(
        LocalizablesFacade.getRawString(mContext, OneCMSKeys.PAYMENT_CVV_TOOLTIP)));
    mTvAMEXCardDescriptionTooltip.setText(HtmlUtils.formatHtml(
        LocalizablesFacade.getRawString(mContext, OneCMSKeys.PAYMENT_CVV_TOOLTIP_AMEX)));
    mIvCVVInfoTooltip.setImageDrawable(
        DrawableUtils.getTintedResource(R.drawable.ic_info_outline_black_24dp,
            R.color.basic_middle, getContext()));
    tvSavePaymentMethodInfo.setText(HtmlUtils.formatHtml(
        LocalizablesFacade.getRawString(mContext, OneCMSKeys.SAVED_PAYMENT_METHOD_STORE_INFO)));
  }

  private void initPresenters() {
    BinCheckPresenter binCheckPresenter =
        AndroidDependencyInjector.getInstance().provideCheckBinPresenter(this);
    binCheckPresenter.initPresenter(mShoppingCartCollectionOption);
    mPaymentFormWidgetPresenter = AndroidDependencyInjector.getInstance()
        .providePaymentFormPresenter(this, binCheckPresenter, paymentWidgetFormListener);
    mPaymentFormWidgetPresenter.configurePaymentFormView(mShoppingCartCollectionOption);
  }

  private void initTextInputLayouts() {
    RequestValidationHandler requestValidationHandler =
        AndroidDependencyInjector.getInstance().provideCreditCardsRequestValidationHandler();

    mPaymentMethodBaseTextWatchersClient =
        new PaymentMethodBaseTextWatchersClient(this, mTilNameOnCard, mTilCreditCardNumber, mTilCVV,
            mContext, mPaymentFormWidgetPresenter, mTracker, mLlCVVToolTip,
            requestValidationHandler);
    mPaymentMethodBaseTextWatchersClient.initCreditCardFieldsOnFocusChange();
  }

  private void initSpinners() {
    initCreditCardSpinner();

    List<String> months = mDateHelper.getAllMonth();
    months.add(0, mExpirationMonthHintSpinner);
    SpinnerAdapter spinnerMonthAdapter = BaseAdaptersFactory.createStringAdapterWithHint(mContext,
        R.layout.spinner_expiry_date_month, months);
    configureSpinnerPopUp(SPINNER_POPUP_HEIGHT, mSpMonthDate);
    mSpMonthDate.setAdapter(spinnerMonthAdapter);

    List<String> years = mDateHelper.getFollowingYearsFromNow(FOLLOWING_YEARS_SINCE_NOW);
    years.add(0, mExpirationYearHintSpinner);
    SpinnerAdapter spinnerYearAdapter =
        BaseAdaptersFactory.createStringAdapterWithHint(mContext, R.layout.spinner_expiry_date_year,
            years);
    configureSpinnerPopUp(SPINNER_POPUP_HEIGHT, mSpYearDate);
    mSpYearDate.setAdapter(spinnerYearAdapter);
  }

  @Override public void initCreditCardSpinner() {
    List<ShoppingCartCollectionOption> creditCardPaymentMethods =
        mPaymentFormWidgetPresenter.createListForCreditCardSpinner();
    List<ShoppingCartCollectionOptionSpinner> shoppingCartCollectionCardSpinners =
        ShoppingCartCollectionMapper.mapToCollectionOptionSpinner(creditCardPaymentMethods,
            mContext);
    OdigeoSpinnerLocalAdapter<ShoppingCartCollectionOptionSpinner> mCreditCardAdapter =
        new OdigeoSpinnerLocalAdapter<>(getContext(), shoppingCartCollectionCardSpinners);
    mSpCreditCardMethod.setAdapter(mCreditCardAdapter);
  }

  private void configureSpinnerPopUp(int height, Spinner spinner) {
    try {
      Field popup = Spinner.class.getDeclaredField("mPopup");
      popup.setAccessible(true);
      android.widget.ListPopupWindow popupWindow =
          (android.widget.ListPopupWindow) popup.get(spinner);
      popupWindow.setHeight(height);
    } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  private void initListener() {
    rbtnBankTransfer.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        if (mWidgetPreviousSelected != mLlBankTransferWidget) {
          mPaymentFormWidgetPresenter.onBankTransferSelected();
          collapseAndExpandWidgets(mWidgetPreviousSelected, mLlBankTransferWidget);
          mWidgetPreviousSelected = mLlBankTransferWidget;
        }
      }
    });
    rbtnCreditCard.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        if (v.getId() == R.id.rbtnCreditCard && mWidgetPreviousSelected != mLlCreditCardWidget) {
          mPaymentFormWidgetPresenter.onCreditCardSelected();
          collapseAndExpandWidgets(mWidgetPreviousSelected, mLlCreditCardWidget);
          mWidgetPreviousSelected = mLlCreditCardWidget;
          mPaymentMethodBaseTextWatchersClient.checkIfAllCreditCardFieldsAreValid();
        }
      }
    });
    rbtnPaypal.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        if (mWidgetPreviousSelected != mLlPaypalWidget) {
          mPaymentFormWidgetPresenter.onPaypalSelected();
          collapseAndExpandWidgets(mWidgetPreviousSelected, mLlPaypalWidget);
          mWidgetPreviousSelected = mLlPaypalWidget;
        }
      }
    });
    rbtnTrustly.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        if (mWidgetPreviousSelected != mLlTrustlyWidget) {
          mPaymentFormWidgetPresenter.onTrustlySelected();
          collapseAndExpandWidgets(mWidgetPreviousSelected, mLlTrustlyWidget);
          mWidgetPreviousSelected = mLlTrustlyWidget;
        }
      }
    });
    rbtnKlarna.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        if (mWidgetPreviousSelected != mLlKlarnaWidget) {
          mPaymentFormWidgetPresenter.onKlarnaSelected();
          collapseAndExpandWidgets(mWidgetPreviousSelected, mLlKlarnaWidget);
          mWidgetPreviousSelected = mLlKlarnaWidget;
        }
      }
    });

    mSpCreditCardMethod.setSpinnerItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        ShoppingCartCollectionOptionSpinner collectionOption =
            ((ShoppingCartCollectionOptionSpinner) mSpCreditCardMethod.getAdapter()
                .getItem(position));

        String creditCardCode = collectionOption.getMethod().getCreditCardType().getCode();
        mPaymentFormWidgetPresenter.onCreditCardSelectedItemSpinner(creditCardCode);
        mPaymentMethodBaseTextWatchersClient.onSpinnerChangeItemValidateCreditCard();
      }

      @Override public void onNothingSelected(AdapterView<?> parent) {
      }
    });

    mSpMonthDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (!isSpinnerMonthInitialization) {
          String year = (String) mSpYearDate.getSelectedItem();
          if (StringUtils.isNumeric(year)) {
            validateExpirationDate();
            mPaymentMethodBaseTextWatchersClient.checkIfAllCreditCardFieldsAreValid();
          }
        } else {
          isSpinnerMonthInitialization = false;
        }
      }

      @Override public void onNothingSelected(AdapterView<?> parent) {
      }
    });

    mSpYearDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (!isSpinnerYearInitialization) {
          String month = (String) mSpMonthDate.getSelectedItem();
          if (StringUtils.isNumeric(month)) {
            validateExpirationDate();
            mPaymentMethodBaseTextWatchersClient.checkIfAllCreditCardFieldsAreValid();
          }
        } else {
          isSpinnerYearInitialization = false;
        }
      }

      @Override public void onNothingSelected(AdapterView<?> parent) {
      }
    });

    mIvCVVInfoTooltip.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        mPaymentFormWidgetPresenter.onClickCVVInfoTooltip(mLlCVVToolTip.getVisibility(),
            mCreditCardMethodCode);
      }
    });
  }

  @Override public void flipCardInUI(String paymentMethod) {
    int newCardImageResource = paymentMethodResources.get(paymentMethod);

    if (newCardImageResource == R.drawable.card_placeholder) {
      mFlipAnimation.flipLeft(newCardImageResource);
    } else {
      mFlipAnimation.flipRight(newCardImageResource);
    }

    mCreditCardMethodCode = paymentMethod;
    mPaymentFormWidgetPresenter.updateCVVValidator(mCreditCardMethodCode, true);
  }

  @Override public void showPaymentMethodSpinner() {
    mSpCreditCardMethod.setVisibility(VISIBLE);
  }

  @Override public void showBankTransferRadioButton() {
    rbtnBankTransfer.setVisibility(VISIBLE);
    mIvBankTransfer.setVisibility(VISIBLE);
  }

  @Override public void showBankTransferSeparator() {
    mVBankTransferSeparator.setVisibility(VISIBLE);
  }

  @Override public void showPaypalRadioButton() {
    rbtnPaypal.setVisibility(VISIBLE);
    mIvPaypal.setVisibility(VISIBLE);
  }

  @Override public void showTrustlyRadioButton() {
    rbtnTrustly.setVisibility(VISIBLE);
    mIvTrustly.setVisibility(VISIBLE);
  }

  @Override public void showKlarnaRadioButton() {
    rbtnKlarna.setVisibility(VISIBLE);
    mIvKlarna.setVisibility(VISIBLE);
  }

  @Override public void setPaypalFee(Double fee) {
    String feeWithCurrency = LocaleUtils.getLocalizedCurrencyFeeValue(fee);
    String footerWithCurrency = paypalHeader + feeWithCurrency;
    mTvPaypalHeader.setText(footerWithCurrency);
  }

  @Override public void setTrustlyFee(Double fee) {
    String feeWithCurrency = LocaleUtils.getLocalizedCurrencyFeeValue(fee);
    String footerWithCurrency = truslyHeader + feeWithCurrency;
    mTvTrustlyHeader.setText(footerWithCurrency);
  }

  @Override public void setKlarnaFee(Double fee) {
    String feeWithCurrency = LocaleUtils.getLocalizedCurrencyFeeValue(fee);
    String footerWithCurrency = klarnaHeader + feeWithCurrency;
    mTvKlarnaHeader.setText(footerWithCurrency);
  }

  @Override public void showPaypalWidget() {
    mLlPaypalWidget.setVisibility(VISIBLE);
  }

  @Override public void showTrustlyWidget() {
    mLlTrustlyWidget.setVisibility(VISIBLE);
  }

  @Override public void showKlarnaWidget() {
    mLlKlarnaWidget.setVisibility(VISIBLE);
  }

  @Override public void showPaypalSeparator() {
    mVPayPalSeparator.setVisibility(VISIBLE);
  }

  @Override public void showTrustlySeparator() {
    mVTrustlySeparator.setVisibility(VISIBLE);
  }

  @Override public void showKlarnaSeparator() {
    mVKlarnaSeparator.setVisibility(VISIBLE);
  }

  @Override public void showCVVTooltip() {
    mLlCVVToolTip.setVisibility(VISIBLE);
  }

  @Override public void hideBankTransfer() {
    rbtnBankTransfer.setVisibility(GONE);
    mIvBankTransfer.setVisibility(GONE);
    mLlBankTransferWidget.setVisibility(GONE);
    mVBankTransferSeparator.setVisibility(GONE);
  }

  @Override public void hidePaypalWidget() {
    rbtnPaypal.setVisibility(GONE);
    mIvPaypal.setVisibility(GONE);
    mLlPaypalWidget.setVisibility(GONE);
    mVPayPalSeparator.setVisibility(GONE);
  }

  @Override public void hideTrustlyWidget() {
    rbtnTrustly.setVisibility(GONE);
    mIvTrustly.setVisibility(GONE);
    mLlTrustlyWidget.setVisibility(GONE);
    mVTrustlySeparator.setVisibility(GONE);
  }

  @Override public void hideKlarnaWidget() {
    rbtnKlarna.setVisibility(GONE);
    mIvKlarna.setVisibility(GONE);
    mLlKlarnaWidget.setVisibility(GONE);
    mVKlarnaSeparator.setVisibility(GONE);
  }

  @Override public void hideCreditCardRadioButton() {
    rbtnCreditCard.setVisibility(GONE);
    mVcreditCardSeparator.setVisibility(GONE);
  }

  @Override public void showCreditCardRadioButton() {
    rbtnCreditCard.setVisibility(VISIBLE);
    mVcreditCardSeparator.setVisibility(VISIBLE);
  }

  @Override public void showBankTransferWidget() {
    mLlBankTransferWidget.setVisibility(VISIBLE);
  }

  @Override public void showCreditCardWidget() {
    mLlCreditCardWidget.setVisibility(VISIBLE);
  }

  @Override public void showCVVTooltipAllTypes() {
    mRlCVVTooltipAMEX.setVisibility(VISIBLE);
    mRlCVVTooltipNormal.setVisibility(VISIBLE);
    mVCvvTooltipSeparator.setVisibility(VISIBLE);
    CollapseAndExpandAnimationUtil.expand(mLlCVVToolTip);
  }

  @Override public void showCVVTooltipAMEX() {
    mRlCVVTooltipAMEX.setVisibility(VISIBLE);
    mRlCVVTooltipNormal.setVisibility(GONE);
    mVCvvTooltipSeparator.setVisibility(GONE);
    CollapseAndExpandAnimationUtil.expand(mLlCVVToolTip);
  }

  @Override public void showCVVTooltipVisaOrMastercard() {
    mRlCVVTooltipAMEX.setVisibility(GONE);
    mRlCVVTooltipNormal.setVisibility(VISIBLE);
    mVCvvTooltipSeparator.setVisibility(GONE);
    CollapseAndExpandAnimationUtil.expand(mLlCVVToolTip);
  }

  @Override public void showSavePaymentMethodSwitch() {
    llStorePaymentMethod.setVisibility(VISIBLE);
  }

  @Override public void trackCVVTooltip(String length) {
    mTracker.trackAnalyticsEvent(CATEGORY_FLIGHTS_PAYMENT, ACTION_PAYMENT_DETAILS, length);
  }

  @Override public void hideCVVTooltip() {
    CollapseAndExpandAnimationUtil.collapse(mLlCVVToolTip, false, null, mLlCreditCardWidget);
  }

  @Override public void hideCreditCardWidget() {
    mLlCreditCardWidget.setVisibility(GONE);
  }

  @Override public void hidePaymentMethodSpinner() {
    mSpCreditCardMethod.setVisibility(GONE);
  }

  @Override public void setCreditCardPaymentType() {
    mPaymentFormWidgetPresenter.setCreditCardCollectionType();
  }

  @Override public void savedPaymentMethodWasSelected() {
    mWidgetPreviousSelected = null;
  }

  @Override public void hideCreditCardSeparator() {
    mVcreditCardSeparator.setVisibility(GONE);
  }

  @Override public void isBinCheckValid(boolean isBinCheckFinished) {
    mPaymentMethodBaseTextWatchersClient.setBinCheckCallFinished(isBinCheckFinished);
    mPaymentMethodBaseTextWatchersClient.checkIfAllCreditCardFieldsAreValid();
  }

  @Override public void updateCVVAmexValidator() {
    mPaymentMethodBaseTextWatchersClient.updateCVVAmexValidator();
  }

  @Override public void updateCVVNormalValidator() {
    mPaymentMethodBaseTextWatchersClient.updateCVVCardValidator();
  }

  @Override public void trackBinCheckSuccess() {
    mTracker.trackAnalyticsEvent(CATEGORY_FLIGHTS_PAYMENT,
        TrackerConstants.ACTION_BIN_UPGRADE_DETECTION, TrackerConstants.LABEL_BIN_CARD_TYPE_OK);
  }

  @Override public void trackBinCheckNoDetected() {
    mTracker.trackAnalyticsEvent(CATEGORY_FLIGHTS_PAYMENT,
        TrackerConstants.ACTION_BIN_UPGRADE_DETECTION,
        TrackerConstants.LABEL_BIN_CARD_TYPE_NOT_DETECTED);
  }

  @Override public void setCreditCardCode(String code) {
    mCreditCardMethodCode = code;
  }

  @Override @Nullable public String getCVV() {
    if (mTilCVV.getEditText() != null) {
      return mTilCVV.getEditText().getText().toString();
    }
    return null;
  }

  @Override @Nullable public String getCreditCardOwner() {
    if (mTilNameOnCard.getEditText() != null) {
      return mTilNameOnCard.getEditText().getText().toString();
    }
    return null;
  }

  @Override @Nullable public String getCreditCardNumber() {
    if (mTilCreditCardNumber.getEditText() != null) {
      return mTilCreditCardNumber.getEditText().getText().toString();
    }
    return null;
  }

  @Override public String getExpirationMonth() {
    return (String) mSpMonthDate.getSelectedItem();
  }

  @Override public String getExpirationYear() {
    return (String) mSpYearDate.getSelectedItem();
  }

  @Override public int getPaymentMethodSpinnerVisibility() {
    return mSpCreditCardMethod.getVisibility();
  }

  @Override public String getPaymentMethodCodeDetected() {
    return mCreditCardMethodCode;
  }

  public PaymentFormWidgetPresenter getPresenter() {
    return mPaymentFormWidgetPresenter;
  }

  @Override public boolean validateExpirationDate() {
    if (!(validateDate((String) mSpMonthDate.getSelectedItem(),
        (String) mSpYearDate.getSelectedItem(), mDateHelper.getCurrentMonth(),
        mDateHelper.getCurrentYearLastTwoCharacters(), mTracker))) {
      mTvExpiryDateError.setVisibility(VISIBLE);
      return false;
    }
    mTvExpiryDateError.setVisibility(GONE);
    return true;
  }

  @Override public boolean validateCreditCardFields() {
    return mPaymentMethodBaseTextWatchersClient.thereAreNoErrorInCreditCardTexInputLayouts()
        && validateExpirationDate();
  }

  @Override public void onCheckAllValidations(boolean areCorrect) {
    mPaymentFormWidgetPresenter.fireValidationsListener(areCorrect);
  }

  @Override public void setPaymentMethodsUnchecked() {
    rbtnBankTransfer.setChecked(false);
    rbtnCreditCard.setChecked(false);
    rbtnPaypal.setChecked(false);
    rbtnTrustly.setChecked(false);
    rbtnKlarna.setChecked(false);
  }

  @Override public boolean isCreditCardFocused() {
    return mPaymentMethodBaseTextWatchersClient.thereHasBeenFocusOnAnyFieldInCreditCardWidget();
  }

  @Override public boolean isStoreMethodCheck() {
    return swStorePaymentMethod.isChecked();
  }

  @Override public void setBankTransferChecked() {
    rbtnBankTransfer.setChecked(true);
  }

  @Override public void setCreditCardChecked() {
    rbtnCreditCard.setChecked(true);
  }

  @Override public void performClickOnCreditCardRadioButton() {
    rbtnCreditCard.performClick();
  }

  @Override public boolean isSavedPaymentMethodAdded() {
    return llSavedPaymentMethod.getChildCount() > 0;
  }

  @Override public void setPaypalChecked() {
    rbtnPaypal.setChecked(true);
  }

  @Override public void setTrustlyChecked() {
    rbtnTrustly.setChecked(true);
  }

  @Override public void setKlarnaChecked() {
    rbtnKlarna.setChecked(true);
  }

  private void collapseAndExpandWidgets(View viewToCollapse, View viewToExpand) {
    CollapseAndExpandAnimationUtil.collapse(viewToCollapse, false, mSvPayment,
        viewToCollapse instanceof LinearLayout ? (LinearLayout) viewToCollapse.getParent()
            .getParent() : null);
    CollapseAndExpandAnimationUtil.expand(viewToExpand);
  }

  @Override public CollectionMethodType getCollectionMethodTypeDetected() {
    return mPaymentFormWidgetPresenter.getCollectionMethodTypeDetected();
  }

  @Override
  public void showSavedPaymentMethodRow(CreditCard creditCard, boolean checkRadioButtonByDefault) {
    SavedPaymentMethodFormView savedPaymentMethodFormView =
        new SavedPaymentMethodFormView(mContext, this, creditCard, mShoppingCartCollectionOption,
            mSvPayment);
    savedPaymentMethodFormView.setTag(savedPaymentMethodTag);
    llSavedPaymentMethod.addView(savedPaymentMethodFormView);
    savedPaymentMethodFormViews.add(savedPaymentMethodFormView);
    mPaymentFormWidgetPresenter.addSavedPaymentMethodPresenter(
        savedPaymentMethodFormView.getPresenter());
    savedPaymentMethodTag++;
  }

  @Override public void unCheckRadioButtons(Object tag) {
    setPaymentMethodsUnchecked();
    for (SavedPaymentMethodFormView savedPaymentMethodFormView : savedPaymentMethodFormViews) {
      if (!savedPaymentMethodFormView.getTag().equals(tag)) {
        savedPaymentMethodFormView.unCheckRadioButton();
      }
    }
    CollapseAndExpandAnimationUtil.collapse(mWidgetPreviousSelected, false, null,
        mWidgetPreviousSelected);
  }

  @Override public void OnClickSavedPaymentMethodRadioButton(Object tag, String creditCardCode) {
    unCheckRadioButtons(tag);
    setCreditCardPaymentType();
    savedPaymentMethodWasSelected();
    mPaymentFormWidgetPresenter.fireUpdateSavedPaymentMethodPricingBreakdownListener(
        creditCardCode);
  }

  @Override public void clearCreditCardFields() {
    mSpMonthDate.setSelection(0);
    mSpYearDate.setSelection(0);
    mPaymentMethodBaseTextWatchersClient.cleanPaymentFormFields();
  }
}
