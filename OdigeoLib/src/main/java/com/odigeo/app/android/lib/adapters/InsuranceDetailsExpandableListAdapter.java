package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.models.InsuranceDescriptionItem;
import java.util.List;

/**
 * Created by emiliano.desantis on 11/09/2014.
 */
public class InsuranceDetailsExpandableListAdapter extends ArrayAdapter<InsuranceDescriptionItem> {
  private final Context context;
  private final List<InsuranceDescriptionItem> itemsInsurances;
  private final LayoutInflater inflater;

  public InsuranceDetailsExpandableListAdapter(Context context,
      List<InsuranceDescriptionItem> items) {
    super(context, 0, items);

    this.context = context;
    this.itemsInsurances = items;

    inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Override public final View getView(int position, View convertView, ViewGroup parent) {

    View rowView = convertView;

    ViewHolder viewHolder = null;

    final boolean isSubtitle = itemsInsurances.get(position).isSubtitle();

    final String description = itemsInsurances.get(position).getDescription();

    if (rowView == null) {
      rowView = inflater.inflate(R.layout.row_insurance_details_list, parent, false);

      viewHolder = new ViewHolder();

      viewHolder.imgCheck = (ImageView) rowView.findViewById(R.id.imgCheck_row_condition_insurance);

      viewHolder.txtDescription =
          (TextView) rowView.findViewById(R.id.txtDescription_row_condition_insurance);

      rowView.setTag(viewHolder);
    } else {
      rowView = convertView;

      viewHolder = ((ViewHolder) rowView.getTag());
    }

    if (isSubtitle) {
      viewHolder.txtDescription.setTypeface(null, Typeface.BOLD);

      viewHolder.imgCheck.setVisibility(View.INVISIBLE);
    }

    viewHolder.txtDescription.setText(description);

    return rowView;
  }

  class ViewHolder {
    ImageView imgCheck;

    TextView txtDescription;
  }
}
