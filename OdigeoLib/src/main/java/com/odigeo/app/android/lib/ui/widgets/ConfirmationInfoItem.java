package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.ui.widgets.base.TextWithImage;

/**
 * Created by emiliano.desantis on 06/10/2014.
 */
public class ConfirmationInfoItem extends TextWithImage {

  public ConfirmationInfoItem(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected final int getLayout() {
    return R.layout.layout_confirmation_info_item;
  }

  @Override protected final int getIdTextView() {
    return R.id.text_info_item;
  }

  @Override protected final int getIdImageView() {
    return R.id.icon_info_item;
  }

  public final void removeSeparator() {
    ((View) findViewById(R.id.separator_info_item)).setVisibility(View.GONE);
  }

  public void setDrawable(@NonNull int drawableId) {
    ImageView iv = ((ImageView) findViewById(R.id.icon_info_item));
    iv.setImageResource(drawableId);
  }
}
