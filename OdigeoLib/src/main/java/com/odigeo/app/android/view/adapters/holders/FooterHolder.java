package com.odigeo.app.android.view.adapters.holders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.odigeo.app.android.lib.R;
import java.util.List;

public class FooterHolder extends BaseRecyclerViewHolder<Void> {

  public static FooterHolder newInstance(ViewGroup container) {

    View view = LayoutInflater.from(container.getContext())
        .inflate(R.layout.my_booking_footer, container, false);
    return new FooterHolder(view);
  }

  private FooterHolder(View v) {
    super(v);
  }

  @Override public void bind(int position, Void item) {
  }
}
