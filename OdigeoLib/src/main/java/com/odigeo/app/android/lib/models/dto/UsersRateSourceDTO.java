package com.odigeo.app.android.lib.models.dto;

public enum UsersRateSourceDTO {

  BV, TA, ED, VE;

  public static UsersRateSourceDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
