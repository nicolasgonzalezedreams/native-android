package com.odigeo.app.android.lib.consts;

/**
 * Created by manuel on 28/09/14.
 */
public final class RestServicesCodes {

  public static final String TIMEOUT = "SESSION_TIMEOUT";
  public static final String NO_RESULTS = "NO_RESULTS";
  private RestServicesCodes() {

  }
}
