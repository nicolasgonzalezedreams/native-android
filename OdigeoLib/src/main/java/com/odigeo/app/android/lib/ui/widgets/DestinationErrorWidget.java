package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;

/**
 * Created by ManuelOrtiz on 05/09/2014.
 */
public class DestinationErrorWidget extends LinearLayout {

  private TextView txtTitle;
  private TextView txtDescription;

  private String title;
  private String description;

  public DestinationErrorWidget(Context context, AttributeSet attrs) {
    super(context, attrs);

    View viewParent =
        LayoutInflater.from(context).inflate(R.layout.layout_destination_error_message, this);

    setTxtTitle((TextView) viewParent.findViewById(R.id.txtTitleDestinationErrorMessage));
    setTxtDescription(
        (TextView) viewParent.findViewById(R.id.txtDescriptionDestinationErrorMessage));
  }

  public final String getTitle() {
    return title;
  }

  public final void setTitle(String title) {
    this.title = title;

    getTxtTitle().setText(title);
  }

  public final String getDescription() {
    return description;
  }

  public final void setDescription(String description) {
    this.description = description;

    getTxtDescription().setText(description);
  }

  public final TextView getTxtTitle() {
    return txtTitle;
  }

  private void setTxtTitle(TextView txtTitle) {
    this.txtTitle = txtTitle;
  }

  public final TextView getTxtDescription() {
    return txtDescription;
  }

  private void setTxtDescription(TextView txtDescription) {
    this.txtDescription = txtDescription;
  }
}
