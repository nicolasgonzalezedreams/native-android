package com.odigeo.app.android.view.textwatchers.OnFocusChange;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;

public class SecondLastNameOnFocusChange extends BaseOnFocusChange
    implements View.OnFocusChangeListener {

  private boolean mIsSecondLastNameConditionRuleMandatory;

  public SecondLastNameOnFocusChange(Context context, TextInputLayout textInputLayout,
      FieldValidator fieldValidator, String oneCMSKey,
      boolean isSecondLastNameConditionRuleMandatory) {
    super(context, textInputLayout, fieldValidator, oneCMSKey);
    mIsSecondLastNameConditionRuleMandatory = isSecondLastNameConditionRuleMandatory;
  }

  @Override public void onFocusChange(View v, boolean hasFocus) {
    if (!hasFocus) {
      String text = ((EditText) v).getText().toString();
      if (((!text.isEmpty()) && (!mIsSecondLastNameConditionRuleMandatory))
          || (mIsSecondLastNameConditionRuleMandatory)) {
        super.validateField(((EditText) v).getText());
      }
    }
  }
}
