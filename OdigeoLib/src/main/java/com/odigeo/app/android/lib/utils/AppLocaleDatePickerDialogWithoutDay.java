package com.odigeo.app.android.lib.utils;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import com.odigeo.app.android.lib.R;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by emiliano.desantis on 19/12/2014.
 */
public class AppLocaleDatePickerDialogWithoutDay extends AppLocaleDatePickerDialog {
  private static String monthFormat;
  private boolean isShowingDay = false;

  public AppLocaleDatePickerDialogWithoutDay(Context context, int theme, OnDateSetListener callBack,
      int year, int monthOfYear, int dayOfMonth) {
    super(context, theme, callBack, year, monthOfYear, dayOfMonth);
    //        setShowingDay(true);
    init(context, year, monthOfYear);
  }

  public AppLocaleDatePickerDialogWithoutDay(Context context, OnDateSetListener callBack, int year,
      int monthOfYear, int dayOfMonth) {
    super(context, callBack, year, monthOfYear, dayOfMonth);
    init(context, year, monthOfYear);
    //        setShowingDay(true);
  }

  public static SimpleDateFormat getMonthFormat() {

    return OdigeoDateUtils.getGmtDateFormat(monthFormat);
  }

  private void init(Context context, int year, int month) {
    monthFormat = context.getString(R.string.templates__date3);
    updateTitleWithoutDay(year, month);
  }

  @Override public final void onDateChanged(DatePicker view, int year, int month, int day) {
    if (isShowingDay()) {
      view.init(year, month, day, this);
      updateTitle(year, month, day);
    } else {
      super.onDateChanged(view, year, month, day);
      //            this.setTitle((month + 1) + "-" + day + "-");
      updateTitleWithoutDay(year, month);
    }
  }

  @Override protected final void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    int day = getContext().getResources().getIdentifier("android:id/day", null, null);

    if (day != 0) {
      View dayPicker = findViewById(day);
      if (dayPicker != null) {
        dayPicker.setVisibility(View.GONE);
      }
    }
  }

  private void updateTitleWithoutDay(int year, int month) {
    mCalendar.set(Calendar.YEAR, year);
    mCalendar.set(Calendar.MONTH, month);
    setTitle(getMonthFormat().format(mCalendar.getTime()));
  }

  public final boolean isShowingDay() {
    return isShowingDay;
  }

  public final void setShowingDay(boolean isShowingDay) {
    this.isShowingDay = isShowingDay;
  }
}
