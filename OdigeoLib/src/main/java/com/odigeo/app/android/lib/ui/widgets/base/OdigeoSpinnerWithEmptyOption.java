package com.odigeo.app.android.lib.ui.widgets.base;

import android.content.Context;
import android.util.AttributeSet;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 19/05/15
 */
public class OdigeoSpinnerWithEmptyOption extends OdigeoSpinner {

  public OdigeoSpinnerWithEmptyOption(Context context) {
    super(context);
  }

  public OdigeoSpinnerWithEmptyOption(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public boolean isValid() {
    return getVisibility() == GONE || super.isValid() && !"".equals(
        spinner.getSelectedItem().toString());
  }
}
