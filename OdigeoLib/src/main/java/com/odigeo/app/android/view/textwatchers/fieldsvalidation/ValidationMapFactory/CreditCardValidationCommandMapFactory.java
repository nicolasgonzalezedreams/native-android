package com.odigeo.app.android.view.textwatchers.fieldsvalidation.ValidationMapFactory;

import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators.AmericanExpressValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators.DinersClubValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators.JCBValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators.MaestroValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators.MasterCardValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators.VisaValidator;
import java.util.HashMap;
import java.util.Map;

public class CreditCardValidationCommandMapFactory {

  public static Map<String, FieldValidator> getMap() {
    Map<String, FieldValidator> commandMap = new HashMap<>();
    commandMap.put("VI", new VisaValidator());
    commandMap.put("E1", new VisaValidator());
    commandMap.put("RV", new VisaValidator());
    commandMap.put("VV", new VisaValidator());
    commandMap.put("VB", new VisaValidator());
    commandMap.put("3V", new VisaValidator());
    commandMap.put("EA", new VisaValidator());
    commandMap.put("VD", new VisaValidator());
    commandMap.put("DL", new VisaValidator());
    commandMap.put("VE", new VisaValidator());

    commandMap.put("CM", new MasterCardValidator());
    commandMap.put("CA", new MasterCardValidator());
    commandMap.put("RM", new MasterCardValidator());
    commandMap.put("MC", new MasterCardValidator());
    commandMap.put("M6", new MasterCardValidator());
    commandMap.put("M4", new MasterCardValidator());
    commandMap.put("MP", new MasterCardValidator());
    commandMap.put("EC", new MasterCardValidator());
    commandMap.put("MI", new MasterCardValidator());

    commandMap.put("AX", new AmericanExpressValidator());

    commandMap.put("JC", new JCBValidator());

    commandMap.put("MA", new MaestroValidator());

    commandMap.put("DC", new DinersClubValidator());

    return commandMap;
  }
}
