package com.odigeo.app.android.lib.models.dto;

/**
 * Booking summary DTO object, contains an status enum and the booking id
 *
 * @author M.Sc. Javier Silva Perez
 * @since 22/04/15.
 */
public class BookingSummary {

  /**
   * Booking id
   */
  private Long id;

  /**
   * Status of the booking
   */
  private BookingSummaryStatus status;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public BookingSummaryStatus getStatus() {
    return status;
  }

  public void setStatus(BookingSummaryStatus status) {
    this.status = status;
  }
}
