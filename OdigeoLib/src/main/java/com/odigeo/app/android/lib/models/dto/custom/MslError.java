package com.odigeo.app.android.lib.models.dto.custom;

/**
 * @author miguel
 */
public class MslError {

  private String message;
  private Integer code;
  private Code codeString;

  public MslError() {
  }

  /**
   * @param message
   * @param code
   */
  public MslError(String message, Code code) {
    this.message = message;
    this.code = code.getErrorCode();
    this.codeString = code;
  }

  /**
   * @return the message
   */
  public final String getMessage() {
    return message;
  }

  /**
   * @param message the message to set
   */
  public final void setMessage(String message) {
    this.message = message;
  }

  /**
   * @return
   */
  public final Integer getCode() {
    return code;
  }

  /**
   * @param code
   */
  public final void setCode(Integer code) {
    this.code = code;
  }

  /**
   * @return
   */
  public final Code getCodeString() {
    return codeString;
  }

  /**
   * @param codeString
   */
  public final void setCodeString(Code codeString) {
    this.codeString = codeString;
    this.code = codeString.getErrorCode();
  }
}
