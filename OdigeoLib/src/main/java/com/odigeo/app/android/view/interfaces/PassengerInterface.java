package com.odigeo.app.android.view.interfaces;

import android.view.View;
import android.widget.ScrollView;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.dto.TravellerTypeDTO;
import com.odigeo.data.entity.extensions.UIUserTraveller;
import com.odigeo.data.entity.userData.UserTraveller;
import java.io.Serializable;
import java.util.List;

/**
 * Created by jose.cabrera on 01/10/2015.
 */
public interface PassengerInterface extends Serializable {

  List<UserTraveller> getUserTravellerOptions(TravellerTypeDTO travellerTypeDTO);

  List<FlightSegment> getFlightSegments();

  void getCountryResidence();

  void onTurnOnSwitchItemPassenger(int positionPassenger, UIUserTraveller travellerSelected);

  void getPhoneCode();

  void onTurnOffSwitchItemPassenger();

  void searchInPassengersTravellerSelected(UIUserTraveller userTravellerSelected);

  boolean userTravellerIsAlreadySelected(UIUserTraveller userTravellerSelected,
      int positionPassenger);

  void turnOffAllPassengerPassengersSwitches();

  void setPassengerAsContactPassenger(UIUserTraveller travellerSelected, int positionPassenger);

  void checkUserTravellerInContact(UIUserTraveller userTravellerSelected, int positionPassenger);

  View.OnFocusChangeListener getOnFocusChangeListener();

  long getTripReturnDate();

  ScrollView getScrollView();
}
