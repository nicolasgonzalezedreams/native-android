package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author miguel
 */
@Deprecated public class SectionDTO implements Serializable {

  protected List<TechnicalStopDTO> technicalStops;
  protected BaggageAllowanceTypeDTO baggageAllowanceType;
  protected long departureDate;
  protected long arrivalDate;
  protected CabinClassDTO cabinClass;
  protected List<Integer> fareInfoPassengers;
  protected String id;
  protected int from;
  protected String departureTerminal;
  protected int to;
  protected String arrivalTerminal;
  protected int carrier;
  protected Integer operatingCarrier;
  protected String aircraft;
  protected Integer baggageAllowanceQuantity;
  protected Long duration;

  protected boolean lcc;
  protected String miniFareWarningText;
  protected String miniFareURL;
  protected boolean isMiniFare;

  // this property is not obtained from the service
  private CarrierDTO operatingCarrierObject;
  private LocationDTO locationFrom;
  private LocationDTO locationTo;

  /**
   * @param geoNodeId
   * @param locations
   * @return
   */
  public static LocationDTO getLocation(final int geoNodeId, final List<LocationDTO> locations) {
    LocationDTO r = new LocationDTO();
    for (LocationDTO lo : locations) {
      if (lo.getGeoNodeId() == geoNodeId) {
        r = lo;
        break;
      }
    }
    return r;
  }

  /**
   * Gets the value of the id property.
   *
   * @return possible object is {@link String }
   */
  public String getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   *
   * @param value allowed object is {@link String }
   */
  public void setId(String value) {
    this.id = value;
  }

  /**
   * Gets the value of the from property.
   */
  public int getFrom() {
    return from;
  }

  /**
   * Sets the value of the from property.
   */
  public void setFrom(int value) {
    this.from = value;
  }

  /**
   * Gets the value of the departureTerminal property.
   *
   * @return possible object is {@link String }
   */
  public String getDepartureTerminal() {
    return departureTerminal;
  }

  /**
   * Sets the value of the departureTerminal property.
   *
   * @param value allowed object is {@link String }
   */
  public void setDepartureTerminal(String value) {
    this.departureTerminal = value;
  }

  /**
   * Gets the value of the to property.
   */
  public int getTo() {
    return to;
  }

  /**
   * Sets the value of the to property.
   */
  public void setTo(int value) {
    this.to = value;
  }

  /**
   * Gets the value of the arrivalTerminal property.
   *
   * @return possible object is {@link String }
   */
  public String getArrivalTerminal() {
    return arrivalTerminal;
  }

  /**
   * Sets the value of the arrivalTerminal property.
   *
   * @param value allowed object is {@link String }
   */
  public void setArrivalTerminal(String value) {
    this.arrivalTerminal = value;
  }

  /**
   * Gets the value of the carrier property.
   *
   * @return possible object is {@link String }
   */
  public int getCarrier() {
    return carrier;
  }

  /**
   * Sets the value of the carrier property.
   *
   * @param value allowed object is {@link String }
   */
  public void setCarrier(int value) {
    this.carrier = value;
  }

  /**
   * Gets the value of the cabinClass property.
   *
   * @return possible object is {@link com.odigeo.app.android.lib.models.CabinClass }
   */
  public CabinClassDTO getCabinClass() {
    return cabinClass;
  }

  /**
   * Sets the value of the cabinClass property.
   *
   * @param value allowed object is {@link com.odigeo.app.android.lib.models.CabinClass }
   */
  public void setCabinClass(CabinClassDTO value) {
    this.cabinClass = value;
  }

  /**
   * Gets the value of the operatingCarrier property.
   *
   * @return possible object is {@link String }
   */
  public Integer getOperatingCarrier() {
    return operatingCarrier;
  }

  /**
   * Sets the value of the operatingCarrier property.
   *
   * @param value allowed object is {@link String }
   */
  public void setOperatingCarrier(Integer value) {
    this.operatingCarrier = value;
  }

  /**
   * Gets the value of the aircraft property.
   *
   * @return possible object is {@link String }
   */
  public String getAircraft() {
    return aircraft;
  }

  /**
   * Sets the value of the aircraft property.
   *
   * @param value allowed object is {@link String }
   */
  public void setAircraft(String value) {
    this.aircraft = value;
  }

  /**
   * Gets the value of the baggageAllowanceType property.
   *
   * @return possible object is {@link BaggageAllowanceType }
   */
  public BaggageAllowanceTypeDTO getBaggageAllowanceType() {
    return baggageAllowanceType;
  }

  /**
   * Sets the value of the baggageAllowanceType property.
   *
   * @param value allowed object is {@link BaggageAllowanceType }
   */
  public void setBaggageAllowanceType(BaggageAllowanceTypeDTO value) {
    this.baggageAllowanceType = value;
  }

  /**
   * Gets the value of the baggageAllowanceQuantity property.
   *
   * @return possible object is {@link Integer }
   */
  public Integer getBaggageAllowanceQuantity() {
    return baggageAllowanceQuantity;
  }

  /**
   * Sets the value of the baggageAllowanceQuantity property.
   *
   * @param value allowed object is {@link Integer }
   */
  public void setBaggageAllowanceQuantity(Integer value) {
    this.baggageAllowanceQuantity = value;
  }

  /**
   * Gets the value of the duration property.
   *
   * @return possible object is {@link Long }
   */
  public Long getDuration() {
    return duration;
  }

  /**
   * Sets the value of the duration property.
   *
   * @param value allowed object is {@link Long }
   */
  public void setDuration(Long value) {
    this.duration = value;
  }

  /**
   * @return the technicalStops
   */
  public List<TechnicalStopDTO> getTechnicalStops() {
    return technicalStops;
  }

  /**
   * @param technicalStops the technicalStops to set
   */
  public void setTechnicalStops(List<TechnicalStopDTO> technicalStops) {
    this.technicalStops = technicalStops;
  }

  /**
   * @return the fareInfoPassengers
   */
  public List<Integer> getFareInfoPassengers() {
    return fareInfoPassengers;
  }

  /**
   * @param fareInfoPassengers the fareInfoPassengers to set
   */
  public void setFareInfoPassengers(List<Integer> fareInfoPassengers) {
    this.fareInfoPassengers = fareInfoPassengers;
  }

  /**
   * @return the departureDate
   */
  public long getDepartureDate() {
    return departureDate;
  }

  /**
   * @param departureDate the departureDate to set
   */
  public void setDepartureDate(long departureDate) {
    this.departureDate = departureDate;
  }

  /**
   * @return the arrivalDate
   */
  public long getArrivalDate() {
    return arrivalDate;
  }

  /**
   * @param arrivalDate the arrivalDate to set
   */
  public void setArrivalDate(long arrivalDate) {
    this.arrivalDate = arrivalDate;
  }

  /**
   * @return
   */
  public boolean isLcc() {
    return lcc;
  }

  /**
   * @param lcc
   */
  public void setLcc(boolean lcc) {
    this.lcc = lcc;
  }

  /**
   * @return the miniFareURL
   */
  public String getMiniFareURL() {
    return miniFareURL;
  }

  /**
   * @param miniFareURL the miniFareURL to set
   */
  public void setMiniFareURL(String miniFareURL) {
    this.miniFareURL = miniFareURL;
  }

  /**
   * @return the isMiniFare
   */
  public boolean isMiniFare() {
    return isMiniFare;
  }

  /**
   * @param isMiniFare the isMiniFare to set
   */
  public void setMiniFare(boolean isMiniFare) {
    this.isMiniFare = isMiniFare;
  }

  /**
   * @return the minifareWarningText
   */
  public String getMiniFareWarningText() {
    return miniFareWarningText;
  }

  /**
   * @param minifareWarningText the minifareWarningText to set
   */
  public void setMiniFareWarningText(String miniFareWarningText) {
    this.miniFareWarningText = miniFareWarningText;
  }

  public CarrierDTO getOperatingCarrierObject() {
    return operatingCarrierObject;
  }

  public void setOperatingCarrierObject(CarrierDTO operatingCarrierObject) {
    this.operatingCarrierObject = operatingCarrierObject;
  }

  public LocationDTO getLocationFrom() {
    return locationFrom;
  }

  public void setLocationFrom(LocationDTO locationFrom) {
    this.locationFrom = locationFrom;
  }

  public LocationDTO getLocationTo() {
    return locationTo;
  }

  public void setLocationTo(LocationDTO locationTo) {
    this.locationTo = locationTo;
  }
}
