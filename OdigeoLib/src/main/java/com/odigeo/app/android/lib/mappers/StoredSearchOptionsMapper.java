package com.odigeo.app.android.lib.mappers;

import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.data.db.helper.CitiesHandlerInterface;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.booking.SearchSegment;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.entity.geo.City;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by matias.dirusso on 26/7/16.
 */
public class StoredSearchOptionsMapper {

  private CitiesHandlerInterface citiesHandlerInterface;

  public StoredSearchOptionsMapper(CitiesHandlerInterface citiesHandlerInterface) {
    this.citiesHandlerInterface = citiesHandlerInterface;
  }

  public SearchOptions storedSearchToSearchOptionMapper(StoredSearch storedSearch) {

    SearchOptions searchOptions = new SearchOptions();
    searchOptions.setNumberOfAdults(storedSearch.getNumAdults());
    searchOptions.setNumberOfKids(storedSearch.getNumChildren());
    searchOptions.setNumberOfBabies(storedSearch.getNumInfants());
    searchOptions.setId(storedSearch.getStoredSearchId());

    switch (storedSearch.getTripType()) {
      case O:
        searchOptions.setTravelType(TravelType.SIMPLE);
        break;
      case R:
        searchOptions.setTravelType(TravelType.ROUND);
        break;
      case M:
        searchOptions.setTravelType(TravelType.MULTIDESTINATION);
        break;
    }
    CabinClassDTO cabinClass;
    if (storedSearch.getCabinClass().equals(StoredSearch.CabinClass.UNKNOWN)) {
      cabinClass = CabinClassDTO.DEFAULT;
    } else {
      cabinClass = CabinClassDTO.valueOf(storedSearch.getCabinClass().toString());
    }
    searchOptions.setCabinClass(cabinClass);
    searchOptions.setDirectFlight(storedSearch.getIsDirectFlight());
    searchOptions.setSearchDate(storedSearch.getCreationDate());

    List<FlightSegment> flightSegments = new ArrayList<>();
    for (SearchSegment searchSegment : storedSearch.getSegmentList()) {
      FlightSegment flightSegment = new FlightSegment();
      City arrivalCity = getCityFromIata(searchSegment.getDestinationIATACode());
      flightSegment.setArrivalCity(arrivalCity);
      City departureCity = getCityFromIata(searchSegment.getOriginIATACode());
      flightSegment.setDepartureCity(departureCity);
      flightSegment.setDate(searchSegment.getDepartureDate());
      flightSegments.add(flightSegment);
    }
    searchOptions.setFlightSegments(flightSegments);

    return searchOptions;
  }

  public StoredSearch searchOptionToStoredSearchMapper(SearchOptions searchOption)
      throws NullPointerException {
    StoredSearch.TripType tripType;
    switch (searchOption.getTravelType()) {
      case SIMPLE:
        tripType = StoredSearch.TripType.O;
        break;
      case ROUND:
        tripType = StoredSearch.TripType.R;
        break;
      case MULTIDESTINATION:
        tripType = StoredSearch.TripType.M;
        break;
      default:
        tripType = StoredSearch.TripType.UNKNOWN;
    }

    List<SearchSegment> searchSegments = new ArrayList<>();
    for (FlightSegment flightSegment : searchOption.getFlightSegments()) {
      if (flightSegment.getArrivalCity() == null || flightSegment.getDepartureCity() == null) {
        throw new NullPointerException();
      }

      City departureCity = flightSegment.getDepartureCity();
      City arrivalCity = flightSegment.getArrivalCity();

      citiesHandlerInterface.storeCity(departureCity);
      citiesHandlerInterface.storeCity(arrivalCity);

      SearchSegment searchSegment =
          new SearchSegment(0, 0, flightSegment.getDepartureCity().getIataCode(),
              flightSegment.getArrivalCity().getIataCode(), flightSegment.getDate(),
              searchSegments.size(), searchOption.getId());
      searchSegments.add(searchSegment);
    }
    StoredSearch.CabinClass cabinClass;
    if (searchOption.getCabinClass().equals(CabinClassDTO.DEFAULT)) {
      cabinClass = StoredSearch.CabinClass.UNKNOWN;
    } else {
      cabinClass = StoredSearch.CabinClass.valueOf(searchOption.getCabinClass().toString());
    }
    return new StoredSearch(0, searchOption.getId(), searchOption.getNumberOfAdults(),
        searchOption.getNumberOfKids(), searchOption.getNumberOfBabies(), cabinClass,
        searchOption.isDirectFlight(), tripType, false, searchSegments,
        searchOption.getSearchDate(), 0);
  }

  public List<SearchOptions> storedSearchToSearchOptionListMapper(
      List<StoredSearch> storedSearches) {
    List<SearchOptions> searchOptions = new ArrayList<>();
    for (StoredSearch storedSearch : storedSearches) {
      searchOptions.add(storedSearchToSearchOptionMapper(storedSearch));
    }
    return searchOptions;
  }

  public List<StoredSearch> searchOptionToStoredSearchListMapper(
      List<SearchOptions> searchOptions) {
    List<StoredSearch> storedSearches = new ArrayList<>();
    for (SearchOptions searchOption : searchOptions) {
      try {
        storedSearches.add(searchOptionToStoredSearchMapper(searchOption));
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return storedSearches;
  }

  private City getCityFromIata(String iata) {
    City city = citiesHandlerInterface.getCity(iata);
    if (city == null) {
      city = new City();
      city.setIataCode(iata);
    }

    return city;
  }
}
