package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class BookingError implements Serializable {

  private String request;
  private String type;
  private String innerType;
  private String description;
  private String stackTrace;

  public final String getRequest() {
    return request;
  }

  public final void setRequest(String request) {
    this.request = request;
  }

  public final String getType() {
    return type;
  }

  public final void setType(String type) {
    this.type = type;
  }

  public final String getInnerType() {
    return innerType;
  }

  public final void setInnerType(String innerType) {
    this.innerType = innerType;
  }

  public final String getDescription() {
    return description;
  }

  public final void setDescription(String description) {
    this.description = description;
  }

  public final String getStackTrace() {
    return stackTrace;
  }

  public final void setStackTrace(String stackTrace) {
    this.stackTrace = stackTrace;
  }
}
