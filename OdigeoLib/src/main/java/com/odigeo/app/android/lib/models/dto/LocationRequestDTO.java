package com.odigeo.app.android.lib.models.dto;

public class LocationRequestDTO {

  protected String name;
  protected String iataCode;
  protected Integer geoNodeId;

  public LocationRequestDTO(String iataCode, String name) {
    this.iataCode = iataCode;
    this.name = name;
  }

  /**
   * Gets the value of the name property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the value of the name property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setName(String value) {
    this.name = value;
  }

  /**
   * Gets the value of the iataCode property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getIataCode() {
    return iataCode;
  }

  /**
   * Sets the value of the iataCode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setIataCode(String value) {
    this.iataCode = value;
  }

  /**
   * Gets the value of the geoNodeId property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getGeoNodeId() {
    return geoNodeId;
  }

  /**
   * Sets the value of the geoNodeId property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setGeoNodeId(Integer value) {
    this.geoNodeId = value;
  }
}
