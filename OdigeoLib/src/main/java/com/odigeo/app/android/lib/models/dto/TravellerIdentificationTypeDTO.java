package com.odigeo.app.android.lib.models.dto;

import android.support.annotation.Nullable;
import com.odigeo.data.entity.BaseSpinnerItem;
import java.io.Serializable;

/**
 * <p>Java class for travellerIdentificationType.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;simpleType name="travellerIdentificationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PASSPORT"/>
 *     &lt;enumeration value="NIE"/>
 *     &lt;enumeration value="NIF"/>
 *     &lt;enumeration value="NATIONAL_ID_CARD"/>
 *     &lt;enumeration value="BIRTH_DATE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
public enum TravellerIdentificationTypeDTO implements Serializable, BaseSpinnerItem {

  EMPTY(EMPTY_STRING), PASSPORT(
      "msltravellerinformationdescription_identificationtypepassport"), NIE(
      "msltravellerinformationdescription_identificationtypenie"), NIF(
      "msltravellerinformationdescription_identificationtypenif"), NATIONAL_ID_CARD(
      "msltravellerinformationdescription_identificationtypenif"), BIRTH_DATE(
      "datepicker_birthdate"), CIF("msltravellerinformationdescription_identificationcif");

  private final String stringKey;

  TravellerIdentificationTypeDTO(String stringKey) {
    this.stringKey = stringKey;
  }

  public static TravellerIdentificationTypeDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

  @Override public String getShownTextKey() {
    return stringKey;
  }

  @Override public int getImageId() {
    return EMPTY_RESOURCE;
  }

  @Nullable @Override public String getShownText() {
    return null;
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public Object getData() {
    return null;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }
}
