package com.odigeo.app.android.lib.models.dto;

public enum RoomSmokingPreferenceDTO {

  UNKNOWN, INDIFFERENT, NON_SMOKING, SMOKING;

  public static RoomSmokingPreferenceDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
