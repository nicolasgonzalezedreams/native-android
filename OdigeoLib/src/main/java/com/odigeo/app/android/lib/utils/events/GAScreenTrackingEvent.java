package com.odigeo.app.android.lib.utils.events;

/**
 * Created by emiliano.desantis on 22/10/2014.
 */
public class GAScreenTrackingEvent {
  private String label;

  public GAScreenTrackingEvent(String label) {
    this.label = label;
  }

  public final String getLabel() {
    return label;
  }

  public final void setLabel(String label) {
    this.label = label;
  }
}
