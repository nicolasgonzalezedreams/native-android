package com.odigeo.app.android.navigator;

import android.content.Intent;
import android.os.Bundle;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.JoinUsView;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.presenter.contracts.navigators.JoinUsNavigatorInterface;

public class JoinUsNavigator extends BaseNavigator implements JoinUsNavigatorInterface {

  private JoinUsView mJoinUsView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view);
    replaceFragment(R.id.fl_container, getInstanceJoinUsView());

    setNavigationTitle(
        LocalizablesFacade.getString(this, OneCMSKeys.SSO_PERSONALAREA_JOIN).toString());

    startAppseeSession();
  }

  private JoinUsView getInstanceJoinUsView() {
    if (mJoinUsView == null) {
      mJoinUsView = JoinUsView.newInstance();
    }
    return mJoinUsView;
  }

  @Override public void showLoginView() {
    startActivity(new Intent(getApplicationContext(), LoginNavigator.class));
  }

  @Override public void showRegisterView() {
    Intent intent = new Intent(getApplicationContext(), RegisterNavigator.class);
    intent.putExtra(Constants.EXTRA_COMES_FROM_JOIN_US, true);
    startActivity(intent);
  }
}
