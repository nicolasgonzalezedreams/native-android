package com.odigeo.app.android.view;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.odigeo.DependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.navigator.BaseNavigator;
import com.odigeo.app.android.navigator.FrequentFlyerCodesNavigator;
import com.odigeo.app.android.view.adapters.FrequentFlyerAdapter;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.wrappers.UICarrierWrapper;
import com.odigeo.app.android.view.wrappers.UserFrequentFlyersWrapper;
import com.odigeo.data.entity.extensions.UICarrier;
import com.odigeo.data.entity.shoppingCart.Carrier;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.presenter.FrequentFlyerCodesPresenter;
import com.odigeo.presenter.contracts.navigators.FrequentFlyerCodesInterface;
import com.odigeo.presenter.contracts.navigators.FrequentFlyerCodesNavigatorInterface;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * @author Oscar Álvarez Date: 15/09/2015.
 * @version 1.0
 * @since 25/09/15
 */
public class FrequentFlyerCodesView extends BaseView<FrequentFlyerCodesPresenter>
    implements FrequentFlyerCodesInterface {
  public static final String CARRIERS_LIST = "CARRIERS_LIST";
  public static final String FREQUENT_FLYER_CODES = "FREQUENT_FLYER_CODES";
  private FloatingActionButton floatingActionButton;
  private List<CardBean> listFrequentFlyers;
  private RecyclerView recyclerView;
  private CoordinatorLayout coordinatorLayout;
  private FrequentFlyerAdapter adapter;
  private Map<String, String> carriersMap = new HashMap<>();
  private Stack<Pair<Integer, CardBean>> deleteStack = new Stack<>();
  private ItemTouchHelper.SimpleCallback callback =
      new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
            RecyclerView.ViewHolder target) {
          return false;
        }

        @Override public float getSwipeThreshold(RecyclerView.ViewHolder viewHolder) {
          return 1f;
        }

        @Override public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
          int position = viewHolder.getAdapterPosition();
          // Backing of the item.
          Pair<Integer, CardBean> pair = new Pair<>(position, listFrequentFlyers.get(position));
          deleteStack.push(pair);
          // Notifying of the deletion to the list and the recyclerView.
          listFrequentFlyers.remove(position);
          adapter.notifyItemRemoved(position);
          // Snack bar for undo operation
          int color;
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            color = getActivity().getResources()
                .getColor(R.color.secondary_brand, getActivity().getTheme());
          } else {
            color = getActivity().getResources().getColor(R.color.secondary_brand);
          }
          Snackbar.make(coordinatorLayout,
              LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.FREQUENT_FLYER_DELETED),
              Snackbar.LENGTH_LONG)
              .setActionTextColor(color)
              .setAction(
                  LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.FREQUENT_FLYER_UNDO),
                  new OnClickListener() {
                    @Override public void onClick(View v) {
                      Pair<Integer, CardBean> itemDeleted = deleteStack.pop();
                      listFrequentFlyers.add(itemDeleted.first, itemDeleted.second);
                      adapter.notifyItemInserted(itemDeleted.first);
                    }
                  })
              .show();
        }
      };

  /**
   * Creates a new instance of the Fragment.
   *
   * @param userFrequentFlyers List of frequent flyer codes.
   * @param groupCarriers List of the airlines grouped by carriers.
   * @return A new instances of the Fragment.
   */
  public static FrequentFlyerCodesView newInstance(List<UserFrequentFlyer> userFrequentFlyers,
      List<UICarrier> groupCarriers) {
    FrequentFlyerCodesView fragment = new FrequentFlyerCodesView();
    Bundle args = new Bundle();
    if (userFrequentFlyers != null) {
      args.putSerializable(FREQUENT_FLYER_CODES, new UserFrequentFlyersWrapper(userFrequentFlyers));
    }
    if (groupCarriers != null) {
      args.putSerializable(CARRIERS_LIST, new UICarrierWrapper(groupCarriers));
    }
    fragment.setArguments(args);
    return fragment;
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    processArguments(getArguments());
    View v = super.onCreateView(inflater, container, savedInstanceState);
    ((BaseNavigator) getActivity()).setNavigationTitle(
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.FREQUENT_FLYER_TITLE));
    ((BaseNavigator) getActivity()).setUpToolbarButton(0);

    // Bug in anchored views: https://code.google.com/p/android/issues/detail?id=221387
    floatingActionButton.post(new Runnable() {
      @Override public void run() {
        floatingActionButton.requestLayout();
      }
    });

    return v;
  }

  /**
   * Process the arguments of the Bundle.
   *
   * @param bundle Bundle from the Fragment.
   */
  private void processArguments(Bundle bundle) {
    if (listFrequentFlyers == null) {
      UserFrequentFlyersWrapper flyerCardCodeWrapper =
          ((UserFrequentFlyersWrapper) bundle.getSerializable(FREQUENT_FLYER_CODES));
      List<UserFrequentFlyer> frequentFlyers = null;
      if (flyerCardCodeWrapper != null) {
        frequentFlyers = flyerCardCodeWrapper.getFlyerCardCodes();
      }
      UICarrierWrapper uiCarrierWrapper =
          ((UICarrierWrapper) bundle.getSerializable(CARRIERS_LIST));
      List<UICarrier> carriers = null;
      if (uiCarrierWrapper != null) {
        carriers = uiCarrierWrapper.getCarriers();
        fillCarriersMap(carriers);
      } else {
        fillCarriersMap(null);
      }
      // Check if we sent an empty list of codes.
      if (frequentFlyers == null) {
        frequentFlyers = new ArrayList<>();
      }
      listFrequentFlyers = getListFrequentFlyers(frequentFlyers, carriers);
    }
  }

  /**
   * Retrieve the codes.
   */
  public void retrieveData() {
    ((FrequentFlyerCodesNavigator) getActivity()).returnFrequentFlyerCodes(
        adapter.getFrequentFlyerCodes());
  }

  /**
   * Creates, initialize and set the adapter for the RecyclerView.
   */
  private void initAdapter() {
    if (adapter == null) {
      if (listFrequentFlyers.isEmpty()) {
        ((FrequentFlyerCodesNavigator) getActivity()).showDetail(null, getAvailableCarriers(), -1);
      }
    } else {
      if (adapter.getItemCount() == 0) {
        getActivity().finish();
      }
    }
    adapter = new FrequentFlyerAdapter(listFrequentFlyers, getActivity());
    recyclerView.setAdapter(adapter);
  }

  /**
   * Fills the carriersMap with the information entered.
   *
   * @param carriers List of carriers to be mapped.
   */
  private void fillCarriersMap(List<UICarrier> carriers) {
    if (carriers != null) {
      for (UICarrier carrier : carriers) {
        carriersMap.put(carrier.getCode(), carrier.getName());
      }
    } else {
      List<UICarrier> configurationCarriers =
          FrequentFlyerCodesNavigator.convertBeans(Configuration.getInstance().getCarriers());
      for (UICarrier carrier : configurationCarriers) {
        carriersMap.put(carrier.getCode(), carrier.getName());
      }
    }
  }

  /**
   * Filter lists of carriers and frequent flyer codes to show just the ones available depending
   * on carrier.
   *
   * @param userFrequentFlyers List of frequent flyer codes.
   * @param carriers List of carriers.
   * @return List of {@link CardBean} objects.
   */
  private List<CardBean> getListFrequentFlyers(List<UserFrequentFlyer> userFrequentFlyers,
      List<UICarrier> carriers) {
    List<CardBean> listCards = new ArrayList<>();
    // Check if we have a valid carriers list.
    if (carriers == null) {
      carriers =
          FrequentFlyerCodesNavigator.convertBeans(Configuration.getInstance().getCarriers());
    }
    for (UICarrier carrier : carriers) {
      for (UserFrequentFlyer frequentFlyerCard : userFrequentFlyers) {
        if (carrier.getCode().equals(frequentFlyerCard.getAirlineCode())) {
          listCards.add(new CardBean(frequentFlyerCard, carrier));
          break;
        }
      }
    }
    return listCards;
  }

  @Override protected FrequentFlyerCodesPresenter getPresenter() {
    return DependencyInjector.provideFrequentFlyerCodesPresenter(this,
        ((FrequentFlyerCodesNavigatorInterface) getActivity()));
  }

  @Override protected int getFragmentLayout() {
    return R.layout.sso_frequent_flyer_codes;
  }

  @Override protected void initComponent(View view) {
    coordinatorLayout = ((CoordinatorLayout) view.findViewById(R.id.coordinator_layout));
    floatingActionButton = ((FloatingActionButton) view.findViewById(R.id.button_add_code));
    recyclerView = ((RecyclerView) view.findViewById(R.id.list_frequent_flyer_codes));
    recyclerView.setHasFixedSize(true);
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    initAdapter();
  }

  @Override protected void setListeners() {
    floatingActionButton.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        List<UICarrier> carriers = getAvailableCarriers();
        if (carriers.isEmpty()) {
          Snackbar.make(coordinatorLayout, LocalizablesFacade.getString(getActivity(),
              OneCMSKeys.FREQUENT_FLYER_CODES_LIMIT_MESSAGE), Snackbar.LENGTH_LONG).show();
        } else {
          ((FrequentFlyerCodesNavigator) getActivity()).showDetail(null, carriers, -1);
        }
      }
    });
    ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
    itemTouchHelper.attachToRecyclerView(recyclerView);
  }

  /**
   * Retrieves a list of carriers available to select.
   *
   * @return A list of carrier available.
   */
  private List<UICarrier> getAvailableCarriers() {
    List<UICarrier> carriers = new ArrayList<>();
    List<UICarrier> frequentFlyerCarriers = new ArrayList<>();
    if (adapter != null && adapter.getFrequentFlyerCodes() != null) {
      for (UserFrequentFlyer userFrequentFlyer : adapter.getFrequentFlyerCodes()) {
        String code = userFrequentFlyer.getAirlineCode();
        frequentFlyerCarriers.add(new UICarrier(new Carrier(code, carriersMap.get(code))));
      }
    }
    for (String code : carriersMap.keySet()) {
      UICarrier uiCarrier = new UICarrier(new Carrier(code, carriersMap.get(code)));
      if (!frequentFlyerCarriers.contains(uiCarrier)) {
        carriers.add(uiCarrier);
      }
    }
    return carriers;
  }

  @Override protected void initOneCMSText(View view) {
    // TODO: 21/09/15 Implement
  }

  /**
   * Add a new Frequent Flyer Code to the list.
   *
   * @param passengerCode code of frequent flyer.
   * @param carrierCode carrier of frequent flyer code.
   */
  public void addNewCode(long frequentFlyerId, String passengerCode, String carrierCode) {
    UICarrier carrierDTO = new UICarrier(new Carrier(carrierCode, carriersMap.get(carrierCode)));
    adapter.addNewItem(frequentFlyerId, passengerCode, carrierDTO);
  }

  /**
   * Removes an item from the adapter.
   *
   * @param position position in list.
   */
  public void removeCode(int position) {
    adapter.removeItem(position);
  }

  /**
   * Return the current adapter.
   *
   * @return adapter.
   */
  public FrequentFlyerAdapter getAdapter() {
    return adapter;
  }

  /**
   * Bean to set the views of Frequent Flyer Codes
   */
  public static class CardBean {
    UserFrequentFlyer frequentFlyer;
    UICarrier carrier;

    public CardBean(UserFrequentFlyer frequentFlyer, UICarrier carrier) {
      this.frequentFlyer = frequentFlyer;
      this.carrier = carrier;
    }

    public CardBean(long frequentFlyerId, String passengerCardNumber, UICarrier carrier) {
      this.frequentFlyer =
          new UserFrequentFlyer(0, frequentFlyerId, carrier.getCode(), passengerCardNumber, 0);
      this.carrier = carrier;
    }

    public UserFrequentFlyer getFrequentFlyer() {
      return frequentFlyer;
    }

    public void setFrequentFlyer(UserFrequentFlyer frequentFlyer) {
      this.frequentFlyer = frequentFlyer;
    }

    public UICarrier getCarrier() {
      return carrier;
    }

    public void setCarrier(UICarrier carrier) {
      this.carrier = carrier;
    }
  }
}
