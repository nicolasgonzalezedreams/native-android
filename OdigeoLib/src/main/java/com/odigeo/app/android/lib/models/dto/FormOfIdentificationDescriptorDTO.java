package com.odigeo.app.android.lib.models.dto;

import java.util.List;

public class FormOfIdentificationDescriptorDTO {

  protected List<FormOfIdentificationTypeDTO> types;

  public List<FormOfIdentificationTypeDTO> getTypes() {
    return types;
  }

  public void setTypes(List<FormOfIdentificationTypeDTO> types) {
    this.types = types;
  }
}
