package com.odigeo.app.android.utils.deeplinking.extractors;

import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 25/04/16
 */
public class CabinClassExtractor implements DeepLinkingExtractor {

  @Override public void extractParameter(SearchOptions.Builder builder, String key, String value) {
    CabinClassDTO cabinClassDTO;
    if (value.equals("UNKNOWN")) {
      cabinClassDTO = mapUnknownCabinClassFromDomainToOdigeoLib();
    } else {
      cabinClassDTO = CabinClassDTO.valueOf(value);
    }
    builder.setCabinClass(cabinClassDTO);
  }

  private CabinClassDTO mapUnknownCabinClassFromDomainToOdigeoLib() {
    return CabinClassDTO.DEFAULT;
  }
}