package com.odigeo.app.android.view;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.database.LocalizablesDBManager;
import com.odigeo.app.android.lib.utils.DeviceUtils;
import com.odigeo.presenter.LocalizablesUpdaterPresenter;
import com.odigeo.presenter.contracts.navigators.LocalizablesUpdaterNavigatorInterface;
import com.odigeo.presenter.contracts.views.LocalizablesUpdaterViewInterface;

public class LocalizablesUpdaterView extends BaseView<LocalizablesUpdaterPresenter>
    implements LocalizablesUpdaterViewInterface {

  private Button btnUpdateCurrent;
  private Button btnExtractDb;
  private TextView txtViewInfo;

  public static LocalizablesUpdaterView newInstance() {
    LocalizablesUpdaterView localizablesUpdaterView = new LocalizablesUpdaterView();
    return localizablesUpdaterView;
  }

  @Override protected LocalizablesUpdaterPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideLocalizablesUpdaterPresenter(this,
            (LocalizablesUpdaterNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.activity_odigeo_full_localizables_update;
  }

  @Override protected void initComponent(View view) {
    btnUpdateCurrent = (Button) view.findViewById(R.id.btn_update_current_localizable);
    btnExtractDb = (Button) view.findViewById(R.id.btn_extract_database);
    txtViewInfo = (TextView) view.findViewById(R.id.txt_vw_update_info);
  }

  @Override protected void setListeners() {
    btnUpdateCurrent.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        disableButton();
        mPresenter.updateCurrentMarket();
      }
    });

    btnExtractDb.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        showExtractConfirmationDialog(new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialogInterface, int i) {
            DeviceUtils.exportDatabase(getActivity(), LocalizablesDBManager.DB_NAME);
            showInformationDialog();
          }
        });
      }
    });
  }

  @Override protected void initOneCMSText(View view) {

  }

  private void showExtractConfirmationDialog(DialogInterface.OnClickListener onClickListener) {
    new AlertDialog.Builder(getActivity()).setTitle(R.string.debug_extract_database)
        .setMessage(R.string.debug_confirm_extract_database)
        .setPositiveButton(android.R.string.yes, onClickListener)
        .setNegativeButton(android.R.string.no, null)
        .show();
  }

  private void disableButton() {
    btnUpdateCurrent.setEnabled(false);
  }

  private void enableButton() {
    btnUpdateCurrent.setEnabled(true);
  }

  private void showInformationDialog() {
    new AlertDialog.Builder(getActivity()).setMessage(R.string.debug_extraction_success)
        .setPositiveButton(android.R.string.yes, null)
        .show();
  }

  @Override public void onUpdatingMarketSuccess(final String oneCMSTable) {
    getActivity().runOnUiThread(new Runnable() {
      @Override public void run() {
        String infoText = txtViewInfo.getText().toString() + oneCMSTable + "\n";
        txtViewInfo.setText(infoText);
        enableButton();
        Toast.makeText(getActivity(), R.string.onecms_update_finished, Toast.LENGTH_LONG).show();
      }
    });
  }

  @Override public void onUpdatingMarketFail() {
    getActivity().runOnUiThread(new Runnable() {
      @Override public void run() {
        enableButton();
        Toast.makeText(getActivity(), R.string.onecms_update_fail, Toast.LENGTH_LONG).show();
      }
    });
  }
}
