package com.odigeo.app.android.lib.models.dto.custom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Deprecated public class PricingBreakdown implements Serializable {

  protected List<PricingBreakdownStep> pricingBreakdownSteps;

  public PricingBreakdown() {
    pricingBreakdownSteps = new ArrayList<>();
  }

  public List<PricingBreakdownStep> getPricingBreakdownSteps() {
    return pricingBreakdownSteps;
  }

  public void setPricingBreakdownSteps(List<PricingBreakdownStep> pricingBreakdownSteps) {
    this.pricingBreakdownSteps = pricingBreakdownSteps;
  }

  public PricingBreakdown copy() {
    PricingBreakdown newObject = new PricingBreakdown();
    List<PricingBreakdownStep> pricingBreakdownSteps = new ArrayList<>();
    for (PricingBreakdownStep step : this.pricingBreakdownSteps) {
      PricingBreakdownStep newStep = new PricingBreakdownStep();
      List<PricingBreakdownItem> items = new ArrayList<>();
      for (PricingBreakdownItem item : step.getPricingBreakdownItems()) {
        PricingBreakdownItem newItem = new PricingBreakdownItem();
        newItem.setPriceItemType(item.getPriceItemType());
        newItem.setPriceItemAmount(item.getPriceItemAmount());
        items.add(newItem);
      }
      newStep.setPricingBreakdownItems(items);
      newStep.setStep(step.getStep());
      newStep.setTotalPrice(step.getTotalPrice());
      pricingBreakdownSteps.add(newStep);
    }
    newObject.setPricingBreakdownSteps(pricingBreakdownSteps);
    return newObject;
  }
}
