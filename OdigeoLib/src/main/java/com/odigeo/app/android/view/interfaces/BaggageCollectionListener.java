package com.odigeo.app.android.view.interfaces;

public interface BaggageCollectionListener {

  void onBaggageSelectionChange(boolean isDefaultBaggageSelection);
}
