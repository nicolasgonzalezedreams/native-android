package com.odigeo.app.android.lib.models.dto;

public enum MovementErrorTypeDTO {

  WRONG_CVV_2("WRONG_CVV2"), FRAUD_SUSPECT("FRAUD_SUSPECT"), INSUFFICIENT_CREDIT(
      "INSUFFICIENT_CREDIT"), WRONG_CREDITCARD_EXPIRATION_DATE(
      "WRONG_CREDITCARD_EXPIRATION_DATE"), WRONG_ELV_ACCOUNT("WRONG_ELV_ACCOUNT"), UNKNOWN(
      "UNKNOWN"), UNSUPPORTED_GATEWAY_COLLECTION_METHOD(
      "UNSUPPORTED_GATEWAY_COLLECTION_METHOD"), ORDER_NOT_AUTHORIZE(
      "ORDER_NOT_AUTHORIZE"), UNKNOWN_ORDER("UNKNOWN_ORDER"), CARD_NOT_IN_AUTHORIZERS_DB(
      "CARD_NOT_IN_AUTHORIZERS_DB"), ACQUIRER_UNAVAILABLE(
      "ACQUIRER_UNAVAILABLE"), INVALID_CARD_NUMBER("INVALID_CARD_NUMBER"), CARD_EXPIRED(
      "CARD_EXPIRED"), DATA_VALIDATION_ERROR("DATA_VALIDATION_ERROR"), UNKNOWN_ORIGIN_IP(
      "UNKNOWN_ORIGIN_IP"), INVALID_AMOUNT("INVALID_AMOUNT"), INVALID_CURRENCY(
      "INVALID_CURRENCY"), DECLINED("DECLINED"), ORDER_ALREADY_USED(
      "ORDER_ALREADY_USED"), TRANSACTION_NO_EXIST(
      "TRANSACTION_NO_EXIST"), AMOUNT_HIGHER_THAN_ALLOWED("AMOUNT_HIGHER_THAN_ALLOWED");
  private final String value;

  MovementErrorTypeDTO(String v) {
    value = v;
  }

  public static MovementErrorTypeDTO fromValue(String v) {
    for (MovementErrorTypeDTO c : MovementErrorTypeDTO.values()) {
      if (c.value.equals(v)) {
        return c;
      }
    }
    throw new IllegalArgumentException(v);
  }

  public String value() {
    return value;
  }

}
