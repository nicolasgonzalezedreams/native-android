package com.odigeo.app.android.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import com.localytics.android.Localytics;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.EnableNotificationsInteractor;

public class OdigeoFirebaseTokenRegistrationService extends IntentService {

  private static final String TAG = "FBRegIntentService";
  private SessionController mSessionController;
  private EnableNotificationsInteractor mEnableNotificationsInteractor;
  private PreferencesControllerInterface mPreferencesController;

  public OdigeoFirebaseTokenRegistrationService() {
    super(TAG);
    mSessionController = AndroidDependencyInjector.getInstance().provideSessionController();
    mEnableNotificationsInteractor =
        AndroidDependencyInjector.getInstance().provideEnableNotificationsInteractor();
    mPreferencesController = AndroidDependencyInjector.getInstance().providePreferencesController();
  }

  @Override protected void onHandleIntent(Intent intent) {
    try {
      if (mSessionController.getGcmToken().isEmpty()) {
        String fcmToken = FirebaseInstanceId.getInstance().getToken();
        sendRegistrationToServer(fcmToken);
        Log.d(TAG, "new fcmToken[" + fcmToken + "]");
        Localytics.setPushRegistrationId(fcmToken);
      } else {
        Log.d(TAG, "saved fcmToken[" + mSessionController.getGcmToken() + "]");
      }
    } catch (Exception e) {
      Log.e(this.getClass().getName(), e.getMessage());
    }
  }

  private void sendRegistrationToServer(final String token) {
    mSessionController.saveGcmToken(token);
    if (mPreferencesController.getBooleanValue(Constants.PREFERENCE_STATUS_FLIGHTS_NOTIFICATIONS,
        true)) {
      mEnableNotificationsInteractor.enableNotifications();
    }
  }
}
