package com.odigeo.app.android.view.adapters.holders;

import android.support.annotation.DrawableRes;

public interface DividerProviderViewHolder {
  @DrawableRes int getDividerDrawable();
}
