package com.odigeo.app.android.view;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.navigator.MyTripDetailsNavigator;
import com.odigeo.app.android.view.adapters.BookingAdapter;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.recyclerview.decorator.MyTripsItemDecorator;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.interactors.provider.MyTripsProvider;
import com.odigeo.presenter.MyTripsPresenter;
import com.odigeo.presenter.contracts.navigators.MyTripsNavigatorInterface;
import com.odigeo.presenter.contracts.views.MyTripsViewInterface;
import java.util.ArrayList;
import java.util.List;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;
import static android.view.View.VISIBLE;
import static com.odigeo.app.android.lib.consts.Constants.EXTRA_MY_TRIPS_WAITING_ANIMATION_ENABLED;
import static com.odigeo.app.android.lib.utils.ViewUtils.findById;
import static com.odigeo.app.android.view.animations.AnimationUtil.fadeIn;
import static com.odigeo.app.android.view.animations.AnimationUtil.fadeOut;
import static com.odigeo.app.android.view.constants.OneCMSKeys.SSO_ND_TRIPS;

public class MyTripsView extends BaseView<MyTripsPresenter> implements MyTripsViewInterface {

  private RecyclerView mRecyclerView;
  private BookingAdapter mBookingsAdapter;
  private ViewStub mViewStubEmpty;
  private SwipeRefreshLayout mSrlTrips;
  private FloatingActionButton mFabImportTrip;
  private boolean mAnimationsActivated;

  public static MyTripsView newInstance() {
    return new MyTripsView();
  }

  @Override protected MyTripsPresenter getPresenter() {
    return dependencyInjector.provideMyTripsPresenter(this,
        (MyTripsNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.my_bookings;
  }

  @Override protected void initComponent(View view) {
    mViewStubEmpty = findById(view, R.id.view_stub_trips);
    mSrlTrips = findById(view, R.id.srlTrips);
    mFabImportTrip = findById(view, R.id.fabImportTrip);
    mRecyclerView = findById(view, R.id.bookingsListRecyclerView);

    setupRecyclerView();

    mSrlTrips.setColorSchemeResources(R.color.primary_brand, R.color.primary_brand_dark);
  }

  private void setupRecyclerView() {
    mRecyclerView.setHasFixedSize(true);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    mRecyclerView.addItemDecoration(new MyTripsItemDecorator(getContext(), VERTICAL));
    mBookingsAdapter =
        new BookingAdapter(new MyTripsProvider(), dependencyInjector.provideDateHelper(),
            getPresenter());

    mBookingsAdapter.setBookingClickListener(new BookingAdapter.OnBookingClickListener() {
      @Override public void onClick(int position, Booking booking) {
        onBookingClicked(position, booking);
      }
    });
    mRecyclerView.setAdapter(mBookingsAdapter);
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mAnimationsActivated =
        getActivity().getIntent().getBooleanExtra(EXTRA_MY_TRIPS_WAITING_ANIMATION_ENABLED, true);
    setHasOptionsMenu(true);
  }

  @Override public void onResume() {
    super.onResume();
    getPresenter().setNavigationTitle(localizables.getString(SSO_ND_TRIPS));
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_MY_TRIPS_HOME);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = super.onCreateView(inflater, container, savedInstanceState);
    mPresenter.initializeLocalBookings();
    return view;
  }

  @Override
  public void notifyBookingsHasChanged(MyTripsProvider myTripsProvider, boolean shouldShowRateApp) {
    if (myTripsProvider != null) {
      mBookingsAdapter.setBookings(myTripsProvider, shouldShowRateApp);
    }
  }

  @Override public void onRequestFinished(final boolean isEmpty) {

    Handler handler = new Handler(Looper.getMainLooper());
    handler.post(new Runnable() {
      @Override public void run() {
        hideLoading();
        showEmptyTrips(isEmpty);
      }
    });
  }

  @Override public void showLoginViews() {
    Button btnLogin = findById(getEmptyView(), R.id.btnLogin);
    btnLogin.setVisibility(VISIBLE);
    btnLogin.setText(localizables.getString(OneCMSKeys.MY_TRIPS_LOG_IN_BUTTON).toUpperCase());
    btnLogin.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.openLogin();
      }
    });
  }

  @Override protected void setListeners() {
    mSrlTrips.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override public void onRefresh() {
        mPresenter.triggerUpdate();
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS,
            TrackerConstants.ACTION_SYNCHRONIZE_TRIPS,
            TrackerConstants.LABEL_SYNCHRONIZE_TRIPS_PULL_TRY);
      }
    });

    mFabImportTrip.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS,
            TrackerConstants.ACTION_UPCOMING_TRIPS, TrackerConstants.LABEL_ADD_BOOKING);
        mPresenter.navigateToImportMyTrips();
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
  }

  @Override public void initializeBookings(MyTripsProvider bookings, boolean shouldShowRateApp) {
    mBookingsAdapter.setBookings(bookings, shouldShowRateApp);
  }

  @Override public void showLoading() {
    if (mAnimationsActivated) {
      mSrlTrips.post(new Runnable() {
        @Override public void run() {
          mSrlTrips.setRefreshing(true);
        }
      });
    }
  }

  @Override public void hideLoading() {
    if (mAnimationsActivated) {
      mSrlTrips.post(new Runnable() {
        @Override public void run() {
          mSrlTrips.setRefreshing(false);
        }
      });
    }
  }

  @Override public void showEmptyTrips(final boolean isEmpty) {
    if (isEmpty) {
      fadeIn(mViewStubEmpty);
      fadeOut(mSrlTrips);
      fadeOut(mFabImportTrip);
      initializeEmptyView();
    } else {
      fadeOut(mViewStubEmpty);
      fadeIn(mSrlTrips);
      fadeIn(mFabImportTrip);
    }
  }

  private void initializeEmptyView() {
    if (getView() != null) {
      View emptyView = getEmptyView();
      TextView mMyTripsEmptyDescription = findById(emptyView, R.id.myTripsEmptyDescription);
      TextView mMyTripsEmptyTitle = findById(emptyView, R.id.myTripsEmptyTitle);
      Button btnImportTrip = findById(emptyView, R.id.btnImportTrip);

      mMyTripsEmptyTitle.setText(localizables.getString(OneCMSKeys.MY_TRIPS_EMPTY_TITLE));
      mMyTripsEmptyDescription.setText(
          localizables.getString(OneCMSKeys.MY_TRIPS_EMPTY_DESCRIPTION));
      btnImportTrip.setText(localizables.getString(OneCMSKeys.MY_TRIPS_IMPORT_TRIP).toUpperCase());

      btnImportTrip.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {

          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS,
              TrackerConstants.ACTION_EMPTY_TRIPS, TrackerConstants.LABEL_ADD_BOOKING);
          mPresenter.openImport();
        }
      });

      mPresenter.checkIfUserIsLoggedIn();
    }
  }

  private View getEmptyView() {
    return findById(getView(), R.id.rlEmptyList);
  }

  public void trackScreenLoadError() {
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS,
        TrackerConstants.ACTION_SYNCHRONIZE_TRIPS,
        TrackerConstants.LABEL_SYNCHRONIZE_TRIPS_EMPTY_ERROR);
  }

  public void trackPullToRefreshError() {
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS,
        TrackerConstants.ACTION_SYNCHRONIZE_TRIPS, TrackerConstants.LABEL_SYNCHRONIZE_TRIPS_ERROR);
  }

  private void onBookingClicked(int position, Booking booking) {
    mPresenter.openDetails(booking, position);
    if (booking.isPastBooking()) {
      mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS,
          TrackerConstants.ACTION_PAST_TRIPS, TrackerConstants.LABEL_MYTRIPS_SELECT_TRIP);
    } else {
      mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS,
          TrackerConstants.ACTION_UPCOMING_TRIPS, TrackerConstants.LABEL_MYTRIPS_SELECT_TRIP);
    }
  }

  public Pair<Bundle, ActivityOptionsCompat> getOptionsForUpcomingOneWayBookingTransition(
      int position) {
    View viewHolder = mRecyclerView.getLayoutManager().findViewByPosition(position);
    List<Pair> sharedElements = getSharedOptionsForBookingTransition(viewHolder);
    final ActivityOptionsCompat activityOptionsCompat =
        ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
            sharedElementsListToPair(sharedElements));
    final Bundle extras = getBundleExtrasForSharedTransition(viewHolder);
    return Pair.create(extras, activityOptionsCompat);
  }

  public Pair<Bundle, ActivityOptionsCompat> getOptionsForUpcomingRoundTripBookingTransition(
      int position) {
    View viewHolder = mRecyclerView.getLayoutManager().findViewByPosition(position);
    List<Pair> sharedElements = getSharedOptionsForBookingTransition(viewHolder);

    View bookingToDateTV = viewHolder.findViewById(R.id.booking_to_date);
    View bookingToIconIV = viewHolder.findViewById(R.id.booking_inbound_date_icon);
    sharedElements.add(
        Pair.create(bookingToDateTV, getString(R.string.my_trips_to_date_transition_tag)));
    sharedElements.add(
        Pair.create(bookingToIconIV, getString(R.string.my_trips_arrival_icon_transition_tag)));

    final ActivityOptionsCompat activityOptionsCompat =
        ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
            sharedElementsListToPair(sharedElements));
    final Bundle extras = getBundleExtrasForSharedTransition(viewHolder);
    return Pair.create(extras, activityOptionsCompat);
  }

  @NonNull private Pair[] sharedElementsListToPair(List<Pair> sharedElements) {
    Pair[] sharedPairs = new Pair[sharedElements.size()];
    return sharedElements.toArray(sharedPairs);
  }

  @NonNull private Bundle getBundleExtrasForSharedTransition(View viewHolder) {
    final TextView statusLabelTV = (TextView) viewHolder.findViewById(R.id.booking_status);
    final Bundle extras = new Bundle();
    extras.putFloat(MyTripDetailsNavigator.EXTRA_STATUS_LABEL_FONT_SIZE,
        statusLabelTV.getTextSize());
    return extras;
  }

  private List<Pair> getSharedOptionsForBookingTransition(View viewHolder) {
    View backgroundImageGradientIV =
        viewHolder.findViewById(R.id.booking_background_image_gradient);
    View backgroundImageIV = viewHolder.findViewById(R.id.booking_background_image);
    View bookingTripToLabelTV = viewHolder.findViewById(R.id.trip_to_label);
    View bookingDestinationTV = viewHolder.findViewById(R.id.booking_city);
    View bookingFromDateTV = viewHolder.findViewById(R.id.booking_from_date);
    View bookingFromIconIV = viewHolder.findViewById(R.id.booking_outbound_date_icon);
    View bookingStatusContainer = viewHolder.findViewById(R.id.booking_status_container);
    View bookingStatusIconIV = viewHolder.findViewById(R.id.booking_status_icon);
    View bookingStatusLabelTV = viewHolder.findViewById(R.id.booking_status);

    List<Pair> sharedElements = new ArrayList<>();
    sharedElements.add(Pair.create(backgroundImageGradientIV,
        getString(R.string.my_trips_image_gradient_transition_tag)));
    sharedElements.add(
        Pair.create(backgroundImageIV, getString(R.string.my_trips_image_transition_tag)));
    sharedElements.add(Pair.create(bookingTripToLabelTV,
        getString(R.string.my_trips_trip_to_label_transition_tag)));
    sharedElements.add(
        Pair.create(bookingDestinationTV, getString(R.string.my_trips_title_transition_tag)));
    sharedElements.add(
        Pair.create(bookingFromDateTV, getString(R.string.my_trips_from_date_transition_tag)));
    sharedElements.add(
        Pair.create(bookingFromIconIV, getString(R.string.my_trips_departure_icon_transition_tag)));
    sharedElements.add(Pair.create(bookingStatusContainer,
        getString(R.string.my_trips_status_container_transition_tag)));
    sharedElements.add(
        Pair.create(bookingStatusIconIV, getString(R.string.my_trips_status_icon_transition_tag)));
    sharedElements.add(Pair.create(bookingStatusLabelTV,
        getString(R.string.my_trips_status_label_transition_tag)));

    return sharedElements;
  }

  public ActivityOptionsCompat getOptionsForPastBookingTransition(int position) {
    View viewHolder = mRecyclerView.getLayoutManager().findViewByPosition(position);
    View backgroundImageIV = viewHolder.findViewById(R.id.booking_background_image);
    Pair<View, String> imageSharedElements =
        Pair.create(backgroundImageIV, getString(R.string.my_trips_image_transition_tag));
    return ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), imageSharedElements);
  }

  @Override public void showLoggedInIsUserBookings() {
    hideLoginViews();
    showEmptyTrips(false);
    getPresenter().triggerUpdate();
  }

  @Override public void hideLoginViews() {
    View emptyView = getEmptyView();
    if (emptyView != null) {
      Button loginButton = findById(emptyView, R.id.btnLogin);
      fadeOut(loginButton);
    }
  }
}
