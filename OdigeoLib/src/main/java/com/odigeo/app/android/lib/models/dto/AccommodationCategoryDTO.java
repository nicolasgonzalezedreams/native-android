package com.odigeo.app.android.lib.models.dto;

public enum AccommodationCategoryDTO {

  UNKNOWN, STARS_0, STARS_0_5, STARS_1, STARS_1_5, STARS_2, STARS_2_5, STARS_3, STARS_3_5, STARS_4, STARS_4_5, STARS_5, STARS_6, STARS_7;

  public static AccommodationCategoryDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
