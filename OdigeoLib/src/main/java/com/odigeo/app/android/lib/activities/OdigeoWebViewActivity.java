package com.odigeo.app.android.lib.activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.base.BlackDialog;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emiliano.desantis on 04/09/2014. Research sources: http://www.mkyong.com/android/android-webview-example/
 * http://developer.android.com/guide/webapps/webview.html http://www.stackoverflow.com/questions/14390908/
 */
public class OdigeoWebViewActivity extends OdigeoForegroundBaseActivity {

  private static final String WHATSAPP_PROTOCOL = "whatsapp://";
  private static final int DIALOG_TIMEOUT = 45000;
  private static final int VOUCHER_MESSAGE_TIME = 7000;
  /*Start of content incorporated from StackOverflow solution.*/
  private final List<String> history = new ArrayList<String>();
  private WebView webView;
  private BlackDialog dialog;
    /*End of content incorporated from StackOverflow solution.*/
  private boolean mShowHomeIcon = true;
  private String mLastURL;

  public final void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_webview);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    webView = (WebView) findViewById(R.id.webView);
    webView.getSettings().setJavaScriptEnabled(true);
    webView.getSettings().setDomStorageEnabled(true);

    ///// ***@irving--> show Dialog while page loading
    dialog = new BlackDialog(this, true);
    dialog.show(
        LocalizablesFacade.getString(this, OneCMSKeys.LOADINGVIEWCONTROLLER_MESSAGE_LOADING));
    //// ***

    final Activity a = this;

    new Handler().postDelayed(new Runnable() {

      public void run() {
        runHandlerDelayed(a);
      }
    }, DIALOG_TIMEOUT);

    try {
      Bundle extras = getIntent().getExtras();
      String url = extras.getString(Constants.EXTRA_URL_WEBVIEW);

      if (extras.containsKey(Constants.TITLE_WEB_ACTIVITY)) {
        String title = extras.getString(Constants.TITLE_WEB_ACTIVITY);
        getSupportActionBar().setTitle(title);
        mShowHomeIcon = extras.getBoolean(Constants.SHOW_HOME_ICON);
      }

      webView.loadUrl(url);

      boolean showVoucher = extras.getBoolean(Constants.EXTRA_SHOW_VOUCHER_IN_CAR, false);

      if (showVoucher) {
        showVoucherMessage();
      }
    } catch (Exception e) {

      Log.d("WebViewActivity", "Crashlytics exception logged");
    }


        /*Start of content incorporated from StackOverflow solution.*/
    webView.setWebViewClient(new WebViewClient() {

      @Override public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url != null && url.startsWith(WHATSAPP_PROTOCOL)) {
          try {
            view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
          } catch (ActivityNotFoundException e) {
            //Nothing
            Log.d("ERROR", "ActivityNotFoundException: " + e.getMessage());
          }
          return true;
        } else {
          return false;
        }
      }

      @Override public void onPageFinished(WebView view, String url) {

        Log.d("WebViewActivity", "logging page: " + url);
        mLastURL = url;
        super.onPageFinished(view, url);
        //// ***@irivng--> hide the dialog when the page is ready
        dialog.dismiss();
        //// ***
      }
    });

    webView.setOnTouchListener(new View.OnTouchListener() {

      @Override public boolean onTouch(View v, MotionEvent event) {

        onTouchWebView(v);
        return false;
      }
    });
        /*End of content incorporated from StackOverflow solution.*/

  }

  private void runHandlerDelayed(Activity a) {
    if (dialog.isShowing()) {
      dialog.dismiss();
      Toast.makeText(a,
          LocalizablesFacade.getString(this, OneCMSKeys.STRING_NEW_CANNOT_OPEN_WEB_PAGE),
          Toast.LENGTH_SHORT).show();
      a.finish();
    }
  }

  private void onTouchWebView(View v) {
    WebView.HitTestResult hr = ((WebView) v).getHitTestResult();

    if (hr != null && mLastURL != null) {
      if (history.isEmpty() || !history.get(history.size() - 1).equals(mLastURL)) {
        history.add(mLastURL);
      }
      Log.d("WebViewActivity", "getExtra = " + hr.getExtra() + "\t\t Type = " + hr.getType());
    }
  }

  @Override public final void onBackPressed() {
    if (webView.canGoBack()) {
      webView.goBack();
    } else {
      super.onBackPressed();
    }
  }

  //// *** options Menu
  @Override public final boolean onCreateOptionsMenu(Menu menu) {
    if (mShowHomeIcon) {
      getMenuInflater().inflate(R.menu.menu_goto_home, menu);
      return super.onCreateOptionsMenu(menu);
    } else {
      return super.onCreateOptionsMenu(menu);
    }
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == R.id.menu_item_goto_home_activity) {
      AlertDialog.Builder builder = new AlertDialog.Builder(this);
      builder.setMessage(
          LocalizablesFacade.getString(this, OneCMSKeys.WEBVIEWCONTROLLER_ALERTVIEW_DESCRIPTION))
          .setPositiveButton(LocalizablesFacade.getString(this, OneCMSKeys.COMMON_OK),
              new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                  finish();
                }
              })
          .setNegativeButton(LocalizablesFacade.getString(this, OneCMSKeys.COMMON_CANCEL),
              new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                  dialog.cancel();
                }
              });
      builder.create();
      builder.show();
    } else if (id == android.R.id.home) {
      onBackPressed();
    }
    return super.onOptionsItemSelected(item);
  }

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getApplication());
  }

  private void showVoucherMessage() {
    final TextView voucherMessage = (TextView) findViewById(R.id.tvVoucherMessage);

    voucherMessage.setVisibility(View.VISIBLE);

    new Handler().postDelayed(new Runnable() {
      @Override public void run() {
        voucherMessage.setVisibility(View.GONE);
      }
    }, VOUCHER_MESSAGE_TIME);
  }
}
