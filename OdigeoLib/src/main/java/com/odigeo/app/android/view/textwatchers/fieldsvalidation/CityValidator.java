package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

public class CityValidator extends BaseValidator implements FieldValidator {

  private static final String CHARACTERS_ALLOWED =
      "-/, A-Za-zÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜäëïöüşŞıçÇ'ŒœßØøÅåÆæÞþÐ ";
  private static final String REGEX_CITY = "[" + CHARACTERS_ALLOWED + "]{1,30}";

  public CityValidator() {
    super();
  }

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_CITY);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
