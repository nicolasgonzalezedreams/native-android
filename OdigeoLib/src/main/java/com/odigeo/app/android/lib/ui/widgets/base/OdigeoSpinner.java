package com.odigeo.app.android.lib.ui.widgets.base;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;

/**
 * Custom spinner view, this view will contain a floating hint spinner some custom fields
 * validations
 *
 * @author M.Sc. Javier Silva Perez
 * @author Ing. Fernando Sierra Pastrana
 * @since 13/05/15.
 */
public class OdigeoSpinner extends CustomField {
  protected Spinner spinner;
  private TextView hintView;
  private CharSequence hint;
  private int hintSize;
  private ColorStateList basicLight;

  public OdigeoSpinner(Context context) {
    super(context);
    initSpinner(context);
  }

  public OdigeoSpinner(Context context, AttributeSet attrs) {
    super(context, attrs);
    //Get the hint attributes to be set.
    TypedArray aAttrs = context.obtainStyledAttributes(attrs, R.styleable.ButtonTextAndHint, 0, 0);
    String hintKey = aAttrs.getString(R.styleable.ButtonTextAndHint_hint);
    if (hintKey != null) {
      hint = LocalizablesFacade.getString(context, hintKey);
    }
    hintSize = aAttrs.getDimensionPixelSize(R.styleable.ButtonTextAndHint_hintSize, 0);
    basicLight = aAttrs.getColorStateList(R.styleable.ButtonTextAndHint_hintColor);

    if (hintSize == 0) {
      hintSize = getResources().getDimensionPixelSize(R.dimen.size_font_XS);
    }

    if (basicLight == null) {
      basicLight = getResources().getColorStateList(R.color.basic_light);
    }
    aAttrs.recycle();
    initSpinner(context);
  }

  /**
   * Returns the adapter currently associated with this widget.
   *
   * @return The adapter used to provide this view's content.
   */
  public SpinnerAdapter getAdapter() {
    return spinner.getAdapter();
  }

  /**
   * Sets the Adapter used to provide the data which backs this Spinner.
   * <p/>
   * Note that Spinner overrides {@link android.widget.Adapter#getViewTypeCount()} on the Adapter
   * associated with this view. Calling {@link android.widget.Adapter#getItemViewType(int)
   * getItemViewType(int)} on the object returned from {@link #getAdapter()} will always return 0.
   * Calling {@link android.widget.Adapter#getViewTypeCount() getViewTypeCount()} will always
   * return 1.
   *
   * @see android.widget.AbsSpinner#setAdapter(SpinnerAdapter)
   */
  public void setAdapter(SpinnerAdapter adapter) {
    spinner.setAdapter(adapter);
  }

  /**
   * Sets the nested spinner item selected listener
   *
   * @param spinnerItemSelectedLister Item selected lister for the nested spinner
   */
  public void setSpinnerItemSelectedListener(
      final AdapterView.OnItemSelectedListener spinnerItemSelectedLister) {

    AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        spinnerItemSelectedLister.onItemSelected(parent, view, position, id);
        clearValidation();
      }

      @Override public void onNothingSelected(AdapterView<?> parent) {
        spinnerItemSelectedLister.onNothingSelected(parent);
      }
    };
    spinner.setOnItemSelectedListener(listener);
  }

  /**
   * Initialize view components, inflates the corresponding layout
   *
   * @param context Context to inflate the corresponding vieews
   */
  private void initSpinner(Context context) {
    inflate(context, R.layout.odigeo_spinner, this);
    spinner = ((Spinner) findViewById(R.id.odigeo_spinner));
    hintView = ((TextView) findViewById(R.id.hint));
    //Clean error message
    setError(null);
    updateHint();
  }

  @Override public boolean validate() {
    //If the view is not valid, show errors
    if (!isValid()) {
      setError(getErrorText());
      return false;
    }
    setError(null);
    return true;
  }

  public boolean validate(ScrollView scroll) {
    if (!validate()) {
      setFocusOnItem(scroll);
      return false;
    }
    return true;
  }

  @Override public boolean isValid() {
    // The view is valid, when the field is visible and is mandatory and has an item selected
    return getVisibility() == GONE || (getVisibility() == VISIBLE && !(isMandatory()
        && getSelectedItem() == null));
    //return false;
  }

  @Override public void clearValidation() {
    setError(null);
  }

  /**
   * Sets the right-hand compound drawable of the TextView to the "error" icon and sets an error
   * message that will be displayed in a popup when the TextView has focus.  The icon and error
   * message will be reset to null when any key events cause changes to the TextView's text.  If
   * the <code>error</code> is <code>null</code>, the error message and icon will be cleared.
   */
  public final void setError(CharSequence error) {
    if (spinner.getSelectedView() != null) {
      TextView textView = ((TextView) spinner.getSelectedView().findViewById(android.R.id.text1));
      if (textView == null) {
        throw new IllegalStateException(
            "The spinner item MUST have a TextView with \"android.R.id.text1\" id.");
      }
      textView.setError(error);
    }
  }

  /**
   * Sets the currently selected item. To support accessibility subclasses that override this
   * method must invoke the overriden super method first.
   *
   * @param position Index (starting at 0) of the data item to be selected.
   */
  public void setSelection(int position) {
    spinner.setSelection(position);
  }

  /**
   * Sets the currently selected item. To support accessibility subclasses that override this
   * method must invoke the overriden super method first.
   *
   * @param selectedItem Object that will be compared with the adapter items, to check which item
   * should be
   * selected, equals() method its used so consider overriding it
   */
  public void setSelection(@Nullable Object selectedItem) {
    //This item is Already selected
    if (selectedItem == null || (getSelectedItem() != null && getSelectedItem().equals(
        selectedItem))) {
      return;
    }

    SpinnerAdapter adapter = spinner.getAdapter();
    //Check if the adapter is not null
    if (adapter != null) {
      //Go over the adapter list and check if the requested item is equals to any item
      for (int i = 0; i < adapter.getCount(); i++) {
        if (adapter.getItem(i).equals(selectedItem)) {
          spinner.setSelection(i);
        }
      }
    }
  }

  /**
   * @return The data corresponding to the currently selected item, or null if there is nothing
   * selected.
   */
  public Object getSelectedItem() {
    return spinner.getSelectedItem();
  }

  /**
   * Updates the hint view.
   */
  private void updateHint() {
    if (TextUtils.isEmpty(hint)) {
      hintView.setVisibility(View.GONE);
    } else {
      hintView.setVisibility(View.VISIBLE);
    }
    hintView.setText(hint);
    if (basicLight != null) {
      hintView.setTextColor(basicLight);
    }
    hintView.setTextSize(TypedValue.COMPLEX_UNIT_PX, hintSize);
  }

  public void setHint(CharSequence hint) {
    this.hint = hint;
    updateHint();
  }

  public void setHintSize(int hintSize) {
    this.hintSize = hintSize;
    updateHint();
  }

  public void setBasicLight(ColorStateList basicLight) {
    this.basicLight = basicLight;
    updateHint();
  }

  /**
   * Retrieves a reference to a Spinner.
   *
   * @return The spinner on the view.
   */
  public Spinner getSpinner() {
    return spinner;
  }
}
