package com.odigeo.app.android.lib.pojos;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Irving on 28/08/2014.
 */
public class RowStandardIconWidgetHolder {
  public TextView text;
  public ImageView icon;
  public LinearLayout stroke;
}
