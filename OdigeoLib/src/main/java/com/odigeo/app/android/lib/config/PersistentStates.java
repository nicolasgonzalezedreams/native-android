package com.odigeo.app.android.lib.config;

/**
 * Created by manuel on 21/12/14.
 */
public enum PersistentStates {
  LOAD_SETTINGS, INITIAL, RESULTS, RESULT_SELECTED, SUMMARY, PASSENGER, INSURANCE, PAYMENT, POSTPAYMENT, DEFAULT, MYTRIPS, CONFIRMATION, SEARCH, Step;
}
