package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for FormOfIdentification complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="FormOfIdentification">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="value" use="required" type="{http://www.w3.org/2001/XMLSchema}string"
 * />
 *       &lt;attribute name="type" use="required" type="{http://api.edreams.com/booking/next}formOfIdentificationType"
 * />
 *       &lt;attribute name="typeCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class FormOfIdentificationDTO {

  protected String value;
  protected FormOfIdentificationTypeDTO type;
  protected String typeCode;

  /**
   * Gets the value of the value property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getValue() {
    return value;
  }

  /**
   * Sets the value of the value property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * Gets the value of the type property.
   *
   * @return possible object is
   * {@link FormOfIdentificationType }
   */
  public FormOfIdentificationTypeDTO getType() {
    return type;
  }

  /**
   * Sets the value of the type property.
   *
   * @param value allowed object is
   * {@link FormOfIdentificationType }
   */
  public void setType(FormOfIdentificationTypeDTO value) {
    this.type = value;
  }

  /**
   * Gets the value of the typeCode property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getTypeCode() {
    return typeCode;
  }

  /**
   * Sets the value of the typeCode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setTypeCode(String value) {
    this.typeCode = value;
  }
}
