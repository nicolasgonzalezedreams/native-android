package com.odigeo.app.android.lib.consts;

/**
 * Created by Irving Lóp on 10/11/2014.
 */
public enum HomeOptionsButtons {
  HOME_OPTION_FLIGHT, HOME_OPTION_HOTEL, HOME_OPTION_CAR, HOME_OPTION_COMBO, HOME_OPTION_MYTRIPS, HOME_OPTION_MYDATA, HOME_OPTION_LASTMINUTE, HOME_OPTION_PACKS
}
