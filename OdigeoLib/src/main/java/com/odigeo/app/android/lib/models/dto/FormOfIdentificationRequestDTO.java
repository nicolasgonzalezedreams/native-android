package com.odigeo.app.android.lib.models.dto;

public class FormOfIdentificationRequestDTO {

  protected String typeId;
  protected String typeName;
  protected String value;

  /**
   * Gets the value of the typeId property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getTypeId() {
    return typeId;
  }

  /**
   * Sets the value of the typeId property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setTypeId(String value) {
    this.typeId = value;
  }

  /**
   * Gets the value of the typeName property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getTypeName() {
    return typeName;
  }

  /**
   * Sets the value of the typeName property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setTypeName(String value) {
    this.typeName = value;
  }

  /**
   * Gets the value of the value property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getValue() {
    return value;
  }

  /**
   * Sets the value of the value property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setValue(String value) {
    this.value = value;
  }
}
