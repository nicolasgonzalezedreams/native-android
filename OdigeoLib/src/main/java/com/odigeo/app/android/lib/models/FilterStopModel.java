package com.odigeo.app.android.lib.models;

import java.io.Serializable;

/**
 * Created by ManuelOrtiz on 21/10/2014.
 */
public class FilterStopModel implements Serializable {
  private boolean directFlight;
  private boolean oneScale;
  private boolean plusTwoScales;
  private int scaleMaxDuration;

  public final boolean isDirectFlight() {
    return directFlight;
  }

  public final void setDirectFlight(boolean directFlight) {
    this.directFlight = directFlight;
  }

  public final boolean isOneScale() {
    return oneScale;
  }

  public final void setOneScale(boolean oneScale) {
    this.oneScale = oneScale;
  }

  public final boolean isPlusTwoScales() {
    return plusTwoScales;
  }

  public final void setPlusTwoScales(boolean plusTwoScales) {
    this.plusTwoScales = plusTwoScales;
  }

  public final int getScaleMaxDuration() {
    return scaleMaxDuration;
  }

  public final void setScaleMaxDuration(int scaleMaxDuration) {
    this.scaleMaxDuration = scaleMaxDuration;
  }
}
