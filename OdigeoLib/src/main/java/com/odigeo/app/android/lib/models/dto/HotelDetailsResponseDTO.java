package com.odigeo.app.android.lib.models.dto;

/**
 * @author miguel
 */
public class HotelDetailsResponseDTO extends BaseResponseDTO {

  protected AccommodationDetailDTO accommodationDetail;

  /**
   * @return the accommodationDetail
   */
  public AccommodationDetailDTO getAccommodationDetail() {
    return accommodationDetail;
  }

  /**
   * @param accommodationDetail the accommodationDetail to set
   */
  public void setAccommodationDetail(AccommodationDetailDTO accommodationDetail) {
    this.accommodationDetail = accommodationDetail;
  }
}
