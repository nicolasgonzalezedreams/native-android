package com.odigeo.app.android.lib.models.dto;

import java.util.List;

/**
 * Model to represent a checked booking
 */
public class CheckedBookingDTO {

  /**
   * List of booking summaries
   */
  List<BookingSummary> bookingSummaries;
  /**
   * Email of the bookings
   */
  private String email;

  /**
   * Constructor
   */
  public CheckedBookingDTO(final String email) {
    this.email = email;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the bookingSummaries
   */
  public List<BookingSummary> getBookingSummaries() {
    return bookingSummaries;
  }

  /**
   * @param bookingSummaries the bookingSummaries to set
   */
  public void setBookingSummaries(List<BookingSummary> bookingSummaries) {
    this.bookingSummaries = bookingSummaries;
  }
}
