package com.odigeo.app.android.lib.models;

import android.content.Context;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.HtmlUtils;

/**
 * Created by gaston.mira on 11/23/16.
 */

public class ShareAppModel extends BaseShareModel {

  private Context mContext;

  public ShareAppModel(Context context) {
    mContext = context;
  }

  @Override public void createTwitterShareText(String oneCmsKey, String storeUrl) {
    setTwitterMessage(LocalizablesFacade.getString(mContext, oneCmsKey, storeUrl).toString());
  }

  @Override public void createFacebookShareText(String storeUrl) {
    setFbMessage(storeUrl);
  }

  @Override public void createWhatsappShareText(String oneCmsKey, String storeUrl) {
    setWhatsAppMessage(LocalizablesFacade.getString(mContext, oneCmsKey, storeUrl).toString());
  }

  @Override public void createSmsShareText(String oneCmsKey, String storeUrl) {
    setSmsMessage(LocalizablesFacade.getString(mContext, oneCmsKey, storeUrl).toString());
  }

  @Override public void createCopyToClipboardShareText(String oneCmsKey, String storeUrl) {
    setCopyToClipboardMessage(
        LocalizablesFacade.getString(mContext, oneCmsKey, storeUrl).toString());
  }

  @Override public void createFbMessengerShareText(String oneCmsKey, String storeUrl) {
    setFbMessengerMessage(LocalizablesFacade.getString(mContext, oneCmsKey, storeUrl).toString());
  }

  @Override public void createEmailSubject(String oneCmsKey) {
    setEmailSubject(LocalizablesFacade.getString(mContext, oneCmsKey).toString());
  }

  @Override public void createEmailBody(String oneCmsKey, String storeUrl) {
    setEmailBodyMessage(HtmlUtils.formatHtml(LocalizablesFacade.getRawString(mContext, oneCmsKey,
        Configuration.getInstance().getBrandVisualName(), storeUrl)));
  }
}
