package com.odigeo.app.android.view.helpers;

import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;

public final class TrackingMyBookingHelper {

  public static void sendAddBookingTracking() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_MY_TRIPS,
            GAnalyticsNames.ACTION_EMPTY_TRIPS, GAnalyticsNames.LABEL_ADD_BOOKING));
  }

  public static void sendSearchFlightTracking() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_MY_TRIPS,
            GAnalyticsNames.ACTION_EMPTY_TRIPS, GAnalyticsNames.LABEL_SEARCH_FLIGHT));
  }
}
