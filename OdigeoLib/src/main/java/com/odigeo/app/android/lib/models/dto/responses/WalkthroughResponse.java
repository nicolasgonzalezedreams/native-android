package com.odigeo.app.android.lib.models.dto.responses;

import android.support.annotation.NonNull;
import com.odigeo.app.android.lib.models.WalkthroughPageWS;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * DTO class for walkthrough service include a list of items that should be shown in {@link
 * com.odigeo.app.android.lib.activities.OdigeoWalkthroughActivity}
 * <p/>
 * NOTE: This DTO was added by GLOBANT
 *
 * @author M. en C. Javier Silva Perez
 * @since 12/03/15.
 */
public class WalkthroughResponse implements Serializable {

  @NonNull List<WalkthroughPageWS> walkthrough;

  /**
   * Default constructor, initialize the list as empty
   */
  public WalkthroughResponse() {
    walkthrough = new LinkedList<>();
  }

  /**
   * Return the walkthrough pages items
   *
   * @return The list of pages returned by the server
   */
  @NonNull public List<WalkthroughPageWS> getWalkthrough() {
    return walkthrough;
  }

  /**
   * Initialize the list of pages
   *
   * @param walkthrough List of {@link com.odigeo.app.android.lib.models.WalkthroughPage} items
   */
  public void setWalkthrough(@NonNull List<WalkthroughPageWS> walkthrough) {
    this.walkthrough = walkthrough;
  }
}
