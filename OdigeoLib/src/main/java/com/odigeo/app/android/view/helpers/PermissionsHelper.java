package com.odigeo.app.android.view.helpers;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.LocalizablesFacade;

public class PermissionsHelper {

  public static final int LOCATION_REQUEST_CODE = 0;
  public static final int CALENDAR_REQUEST_CODE = 1;
  public static final int PHONE_REQUEST_CODE = 2;
  public static final int ACCOUNTS_GOOGLE_REQUEST_CODE = 3;
  public static final int ACCOUNTS_FACEBOOK_REQUEST_CODE = 4;
  public static final int EXTERNAL_STORAGE_REQUEST_CODE = 5;
  public static final int EXTERNAL_STORAGE_REQUEST_CODE_FOR_DELETING = 6;
  public static final int READ_EXTERNAL_STORAGE_REQUEST_CODE = 7;

  public static boolean askForPermissionIfNeeded(String permissionId, Activity context,
      String explanationMessageOneCMSKey, int permissionRequestCode) {

    int hasWriteContactsPermission = ContextCompat.checkSelfPermission(context, permissionId);
    if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
      if (ActivityCompat.shouldShowRequestPermissionRationale(context, permissionId)) {
        showExplanationMessage(context, explanationMessageOneCMSKey);
        return false;
      } else {
        ActivityCompat.requestPermissions(context, new String[] { permissionId },
            permissionRequestCode);
        return false;
      }
    } else {
      return true;
    }
  }

  public static boolean askForPermissionIfNeededInFragment(final String permissionId,
      Fragment fragment, String explanationMessageOneCMSKey, int permissionRequestCode) {
    int hasWriteContactsPermission =
        ContextCompat.checkSelfPermission(fragment.getActivity(), permissionId);
    if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
      if (fragment.shouldShowRequestPermissionRationale(permissionId)) {
        showExplanationMessage(fragment.getActivity(), explanationMessageOneCMSKey);
        return false;
      } else {
        fragment.requestPermissions(new String[] { permissionId }, permissionRequestCode);
        return false;
      }
    } else {
      return true;
    }
  }

  private static void showExplanationMessage(Activity context, String explanationMessageOneCMSKey) {
    DialogHelper dialogHelper = new DialogHelper(context);
    String brand = Configuration.getInstance().getBrand();
    String oneCMSText =
        LocalizablesFacade.getString(context, explanationMessageOneCMSKey, brand).toString();
    dialogHelper.showPermissionDeniedDialog(context, brand, oneCMSText);
  }
}
