package com.odigeo.app.android.lib.models.dto;

public enum SearchProductTypeDTO {
  FLIGHT, TRAIN;
}
