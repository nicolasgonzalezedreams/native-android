package com.odigeo.app.android.lib.models.dto;

public enum MovementStatusDTO {

  NEEDS_INTERACTION, INIT_INTERACTION, CANCEL, CANCELERR, DECLINED, PAID, PAYERROR, REFUNDERR, REFUNDED, USE_BATCH, STATUS, QUERYERR, REQUESTED;

  public static MovementStatusDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
