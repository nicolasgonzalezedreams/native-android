package com.odigeo.app.android.view.adapters.holders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.data.localizable.LocalizableProvider;
import java.util.List;

public abstract class BaseRecyclerViewHolder<T> extends RecyclerView.ViewHolder {

  protected final AndroidDependencyInjector dependencyInjector;
  protected final LocalizableProvider localizableProvider;

  BaseRecyclerViewHolder(View itemView) {
    super(itemView);
    dependencyInjector =
        (AndroidDependencyInjector) ((OdigeoApp) getContext().getApplicationContext()).getDependencyInjector();
    localizableProvider = dependencyInjector.provideLocalizables();
  }

  public void bind(int position, T item) {
  }

  public void bind(int position, T item, List<Object> payloads) {
  }

  Context getContext() {
    return itemView.getContext();
  }
}
