package com.odigeo.app.android.view;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.helper.EmailSender;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.providers.EmailProvider;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.custom.AboutInfoItem;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.AboutContactUsPresenter;
import com.odigeo.presenter.contracts.navigators.AboutUsNavigatorInterface;
import com.odigeo.presenter.contracts.views.AboutContactUsViewInterface;

public class AboutContactUsView extends BaseView<AboutContactUsPresenter>
    implements AboutContactUsViewInterface {

  private AboutInfoItem mBtnAboutContacsUsSendEmail;
  private AboutInfoItem mBtnAboutContactUsCallUsNameAndTitle;
  private TextView mTvAboutContactUsTitle;
  private EmailProvider emailProvider;

  @Override public void onAttach(Context context) {
    super.onAttach(context);

    emailProvider = AndroidDependencyInjector.getInstance().provideEmailProvider();
  }

  @Override public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    mPresenter.setActionBarTitle(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.ABOUTOPTIONSMODULE_CONTACTUS)
            .toString());
  }

  @Override protected AboutContactUsPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideAboutContactUsPresenter(this, (AboutUsNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_about_contact_us_fragment;
  }

  @Override protected void initComponent(View view) {
    mTvAboutContactUsTitle = (TextView) view.findViewById(R.id.tvAboutContactUsTitle);
    mBtnAboutContacsUsSendEmail =
        (AboutInfoItem) view.findViewById(R.id.wdgtAboutContactUsSendEmailNameAndTitle);
    mBtnAboutContactUsCallUsNameAndTitle =
        (AboutInfoItem) view.findViewById(R.id.wdgtAboutContactUsCallUsNameAndTitle);
  }

  @Override protected void setListeners() {
    mBtnAboutContacsUsSendEmail.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_ABOUT_US_CONTACT,
            TrackerConstants.ACTION_ABOUT_US_CONTACT, TrackerConstants.LABEL_ABOUT_SEND_EMAIL);
        EmailSender.send(emailProvider.getFeedbackEmail(),
            OneCMSKeys.LEAVEFEEDBACK_HELP_IMPROVE_EMAIL_SUBJECT, getContext());
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    mBtnAboutContacsUsSendEmail.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.ABOUTOPTIONSMODULE_SEND_MAIL)
            .toString());
    mBtnAboutContactUsCallUsNameAndTitle.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.ABOUT_BUTTONS_CALL_US).toString());
    mTvAboutContactUsTitle.setText(
        LocalizablesFacade.getString(getActivity().getApplicationContext(),
            OneCMSKeys.CONTACTUSVIEWCONTROLLER_HEADERTITLE));
  }

  @Override public void onDestroy() {
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_ABOUT_US_FAQ,
        TrackerConstants.ACTION_ABOUT_US, TrackerConstants.LABEL_GO_BACK);
    super.onDestroy();
  }
}
