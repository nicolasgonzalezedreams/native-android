package com.odigeo.app.android.navigator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoCountriesActivity;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.view.TravellerDetailView;
import com.odigeo.app.android.view.TravellersListView;
import com.odigeo.app.android.view.wrappers.UserFrequentFlyersWrapper;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.contracts.navigators.TravellersListNavigatorInterface;
import java.util.List;

public class TravellersNavigator extends BaseNavigator implements TravellersListNavigatorInterface {

  public static final String TAG_TRAVELLER_DETAIL = "TRAVELLER_DETAIL";
  private TravellersListView mTravellersListView;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view_with_toolbar);
    showView();
  }

  protected void showView() {
    replaceFragment(R.id.fl_container, getInstanceTravellersView());
  }

  private TravellersListView getInstanceTravellersView() {
    if (mTravellersListView == null) {
      mTravellersListView = TravellersListView.newInstance();
    }
    return mTravellersListView;
  }

  @Override public void showTravellerDetailView(String title, long id, boolean isDefaultTraveller) {
    replaceFragmentWithBackStack(R.id.fl_container,
        TravellerDetailView.newInstance(LocalizablesFacade.getString(this, title).toString(), id,
            isDefaultTraveller), TAG_TRAVELLER_DETAIL);
  }

  @Override public void showBackView() {
    super.onBackPressed();
  }

  @Override public void navigateToSelectCountry(String fragmentTag, int requestCode) {
    Intent intent = new Intent(TravellersNavigator.this, OdigeoCountriesActivity.class);
    if (requestCode == Constants.SELECTING_TRAVELLER_NATIONALITY) {
      intent.putExtra(Constants.EXTRA_COUNTRY_MODE, OdigeoCountriesActivity.SELECTION_MODE_COUNTRY);
    } else if (requestCode == Constants.SELECTING_BUYER_COUNTRY_OF_RESIDENCE) {
      intent.putExtra(Constants.EXTRA_COUNTRY_MODE,
          OdigeoCountriesActivity.SELECTION_MODE_RESIDENCE);
    }
    startActivityForResult(intent, requestCode);
  }

  @Override
  public void navigateToIdentifications(UserIdentification.IdentificationType typeIdentification,
      int requestCode, UserIdentification userIdentification) {
    Intent intent = new Intent(getApplicationContext(), IdentificationsNavigator.class);
    intent.putExtra(IdentificationsNavigator.IDENTIFICATION_TYPE, typeIdentification);
    if (userIdentification != null) {
      intent.putExtra(IdentificationsNavigator.IDENTIFICATION_ID_NUMBER,
          userIdentification.getIdentificationId());
      intent.putExtra(IdentificationsNavigator.IDENTIFICATION_DATE,
          userIdentification.getIdentificationExpirationDate());
      intent.putExtra(IdentificationsNavigator.IDENTIFICATION_COUNTRY,
          userIdentification.getIdentificationCountryCode());
    }
    startActivityForResult(intent, requestCode);
  }

  @Override public void navigateToCountryCode() {
    Intent intent = new Intent(getApplicationContext(), OdigeoCountriesActivity.class);
    intent.putExtra(Constants.EXTRA_COUNTRY_MODE,
        OdigeoCountriesActivity.SELECTION_MODE_PHONE_PREFIX);
    int requestCode = Constants.SELECTING_COUNTRY_PHONE_PREFIX;
    startActivityForResult(intent, requestCode);
  }

  @Override public void navigateToFrequentFlyerCodes() {
    Intent intent = new Intent(this, FrequentFlyerCodesNavigator.class);
    TravellerDetailView travellerDetailView =
        (TravellerDetailView) getSupportFragmentManager().findFragmentById(R.id.fl_container);
    List<UserFrequentFlyer> codes = travellerDetailView.getUserFrequentFlyer();
    intent.putExtra(Constants.EXTRA_FREQUENT_FLYER_LIST, new UserFrequentFlyersWrapper(codes));
    startActivityForResult(intent, Constants.REQUEST_CODE_FREQUENT_FLYER_CODES);
  }

  @Override public void returnIdentifications(String numberIdentification, long dateExpiration,
      String identificationCountryCode, UserIdentification.IdentificationType identificationType) {
    onHomeUpButtonPressed();
    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
    if (fragment != null && fragment instanceof TravellerDetailView) {
      TravellerDetailView travellerDetailView =
          (TravellerDetailView) getSupportFragmentManager().findFragmentById(R.id.fl_container);
      travellerDetailView.setIdentification(numberIdentification, dateExpiration,
          identificationCountryCode, identificationType);
    }
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      FragmentManager manager = getSupportFragmentManager();
      Fragment fragment = manager.findFragmentById(R.id.fl_container);
      if (fragment instanceof TravellersListView) {
        tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_DATA_PAX,
            TrackerConstants.ACTION_NAVIGATION_ELEMENTS, TrackerConstants.LABEL_GO_BACK);
        onHomeUpButtonPressed();
      } else if (fragment instanceof TravellerDetailView) {
        tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_DATA_PAX,
            TrackerConstants.ACTION_PAX_SUMMARY, TrackerConstants.LABEL_ADD_PASSENGER_CLICKS);
      }
    }
    return super.onOptionsItemSelected(item);
  }

  @Override protected void onHomeUpButtonPressed() {
    ViewUtils.hideKeyboard(this);
    int count = getSupportFragmentManager().getBackStackEntryCount();
    if (count == 0) {
      super.onBackPressed();
    } else {
      getSupportFragmentManager().popBackStackImmediate();
    }
  }

  @Override public void setValuesActionBar(String title, int idIcon) {
    setNavigationTitle(title);
    setUpToolbarButton(idIcon);
  }

  @Override public final void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == RESULT_OK
        && requestCode >= Constants.REQUEST_CODE_CARTE
        && requestCode <= Constants.REQUEST_CODE_FREQUENT_FLYER_CODES) {
      String numberIdentification =
          data.getStringExtra(IdentificationsNavigator.IDENTIFICATION_ID_NUMBER);
      long dateExpiry = data.getLongExtra(IdentificationsNavigator.IDENTIFICATION_DATE, 0L);
      String countryCode = data.getStringExtra(IdentificationsNavigator.IDENTIFICATION_COUNTRY);
      Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
      if (fragment != null && fragment instanceof TravellerDetailView) {
        TravellerDetailView travellerDetailView =
            (TravellerDetailView) getSupportFragmentManager().findFragmentById(R.id.fl_container);
        if (requestCode == Constants.REQUEST_CODE_CARTE) {
          travellerDetailView.setIdentification(numberIdentification, dateExpiry, countryCode,
              UserIdentification.IdentificationType.NATIONAL_ID_CARD);
        } else if (requestCode == Constants.REQUEST_CODE_PASSPORT) {
          travellerDetailView.setIdentification(numberIdentification, dateExpiry, countryCode,
              UserIdentification.IdentificationType.PASSPORT);
        } else if (requestCode == Constants.REQUEST_CODE_NIE) {
          travellerDetailView.setIdentification(numberIdentification, dateExpiry, countryCode,
              UserIdentification.IdentificationType.NIE);
        } else if (requestCode == Constants.REQUEST_CODE_NIF) {
          travellerDetailView.setIdentification(numberIdentification, dateExpiry, countryCode,
              UserIdentification.IdentificationType.NIF);
        } else if (requestCode == Constants.REQUEST_CODE_FREQUENT_FLYER_CODES) {
          frequentFlyerCodeListRetrieved(data, travellerDetailView);
        }
      }
    }

    if (resultCode == Activity.RESULT_OK) {
      Country countrySelected = (Country) data.getSerializableExtra(Constants.EXTRA_COUNTRY);
      if (countrySelected != null) {
        if (requestCode == Constants.SELECTING_TRAVELLER_NATIONALITY) {
          Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
          if (fragment != null && fragment instanceof TravellerDetailView) {
            TravellerDetailView travellerDetailView =
                (TravellerDetailView) getSupportFragmentManager().findFragmentById(
                    R.id.fl_container);
            travellerDetailView.setNationalitySelected(countrySelected);
          }
        } else if (requestCode == Constants.SELECTING_BUYER_COUNTRY_OF_RESIDENCE) {
          Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
          if (fragment != null && fragment instanceof TravellerDetailView) {
            TravellerDetailView travellerDetailView =
                (TravellerDetailView) getSupportFragmentManager().findFragmentById(
                    R.id.fl_container);
            travellerDetailView.setCountryResidenceSelected(countrySelected);
          }
        } else if (requestCode == Constants.SELECTING_COUNTRY_PHONE_PREFIX) {
          Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
          if (fragment != null && fragment instanceof TravellerDetailView) {
            TravellerDetailView travellerDetailView =
                (TravellerDetailView) getSupportFragmentManager().findFragmentById(
                    R.id.fl_container);
            travellerDetailView.setPhonePrefixSelected(countrySelected);
          }
        }
      }
    }
  }

  private void frequentFlyerCodeListRetrieved(Intent data,
      TravellerDetailView travellerDetailView) {
    UserFrequentFlyersWrapper wrapper = ((UserFrequentFlyersWrapper) data.getSerializableExtra(
        Constants.RESPONSE_LIST_FREQUENT_FLYER));
    if (wrapper != null) {
      List<UserFrequentFlyer> list = wrapper.getFlyerCardCodes();
      travellerDetailView.setUserFrequentFlyerList(list);
    }
  }

  @Override public void navigateToLogin() {
    Intent intent = new Intent(getApplicationContext(), LoginNavigator.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(intent);
    finish();
  }
}
