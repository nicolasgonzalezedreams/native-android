package com.odigeo.app.android.model;

import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import java.io.Serializable;
import java.util.List;

public class BookingInfoViewModel implements Serializable {

  private CollectionMethodWithPrice collectionMethodWithPrice;
  private List<SegmentWrapper> segmentsWrappers;

  public CollectionMethodWithPrice getCollectionMethodWithPrice() {
    return collectionMethodWithPrice;
  }

  public void setCollectionMethodWithPrice(CollectionMethodWithPrice collectionMethodWithPrice) {
    this.collectionMethodWithPrice = collectionMethodWithPrice;
  }

  public List<SegmentWrapper> getSegmentsWrappers() {
    return segmentsWrappers;
  }

  public void setSegmentsWrappers(List<SegmentWrapper> segmentsWrappers) {
    this.segmentsWrappers = segmentsWrappers;
  }
}
