package com.odigeo.app.android.lib.models.dto.responses;

import com.odigeo.app.android.lib.models.CarouselImages;
import java.io.Serializable;
import java.util.List;

/**
 * Created by arturo.padron on 10/02/2015.
 */
public class CarouselImagesResponse implements Serializable {

  private List<CarouselImages> images;

  public final List<CarouselImages> getImages() {
    return images;
  }

  public final void setImage(List<CarouselImages> images) {
    this.images = images;
  }
}
