package com.odigeo.app.android.lib.interfaces;

import android.app.Application;

/**
 * Created by manuel on 08/09/14.
 */
public interface IHomeEvents {
  void onFlightsPressed(Application application);

  void onHotelsPressed(Application application);

  void onCarsPressed(Application application);

  void onMyTripsPressed(Application application);

  void onMyInfoPressed(Application application);

  void onAboutPressed(Application application);

  void onSettingsPressed(Application application);
}
