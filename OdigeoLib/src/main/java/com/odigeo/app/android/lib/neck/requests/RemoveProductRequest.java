package com.odigeo.app.android.lib.neck.requests;

import android.util.Log;
import com.globant.roboneck.common.NeckCookie;
import com.google.gson.Gson;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.dto.requests.AddOrRemoveProductRequestModel;
import com.odigeo.app.android.lib.models.dto.responses.CreateShoppingCartResponse;
import com.odigeo.app.android.lib.neck.requests.base.BaseOdigeoRequests;
import com.odigeo.data.net.helper.DomainHelperInterface;
import java.util.List;

/**
 * Created by manuel on 26/09/14.
 */
@Deprecated public class RemoveProductRequest extends
    BaseOdigeoRequests<CreateShoppingCartResponse, CreateShoppingCartResponse, AddOrRemoveProductRequestModel> {

  private static final String PATH = "removeProduct";
  private DomainHelperInterface mDomainHelper;

  public RemoveProductRequest(AddOrRemoveProductRequestModel requestModel, List<NeckCookie> cookies,
      OdigeoSession odigeoSession, String deviceId, DomainHelperInterface domainHelper) {
    super(CreateShoppingCartResponse.class, requestModel, cookies, odigeoSession, deviceId);
    mDomainHelper = domainHelper;
  }

  @Override protected final String getUrl() {
    String url = String.format("%s%s", mDomainHelper.getUrl(), PATH);

    return url;
  }

  @Override protected final CreateShoppingCartResponse processContent(String responseBody) {

    CreateShoppingCartResponse response = super.processContent(responseBody);

    response.setCookies(getCookiesReceived());

    Log.i(Constants.TAG_LOG, "CreateShoppingCart Rest Services returned " + responseBody);

    if (response != null) {
      Log.d(Constants.TAG_LOG, "[REMOVE PRODUCT] Pricing break down : " + new Gson().toJson(
          response.getPricingBreakdown()));
    }

    return response;
  }
}
