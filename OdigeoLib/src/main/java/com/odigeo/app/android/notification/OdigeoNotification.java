package com.odigeo.app.android.notification;

import android.content.Intent;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.app.TaskStackBuilder;

public class OdigeoNotification {

  private Builder notificationBuilder;
  private Intent notificationIntent;
  private TaskStackBuilder notificationTaskStackBuilder;

  public Builder getNotificationBuilder() {
    return notificationBuilder;
  }

  public void setNotificationBuilder(Builder notificationBuilder) {
    this.notificationBuilder = notificationBuilder;
  }

  public Intent getNotificationIntent() {
    return notificationIntent;
  }

  public void setNotificationIntent(Intent notificationIntent) {
    this.notificationIntent = notificationIntent;
  }

  public TaskStackBuilder getNotificationTaskStackBuilder() {
    return notificationTaskStackBuilder;
  }

  public void setNotificationTaskStackBuilder(TaskStackBuilder notificationTaskStackBuilder) {
    this.notificationTaskStackBuilder = notificationTaskStackBuilder;
  }
}
