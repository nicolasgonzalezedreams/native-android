package com.odigeo.app.android.lib.config;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.Log;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.utils.DeviceUtils;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.interactors.provider.MarketProviderInterface;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import org.xmlpull.v1.XmlPullParserException;

/**
 * This class contains all the application configuration, such information is provided from a XML
 * file
 *
 * @author ManuelOrtiz on 28/08/2014.
 */
public class Configuration extends BaseConfiguration {

  public static final String TAG_FORCING_LATIN_KEYS = "ForcingLatinKeyboard";
  private static final String BACKSLASH = "\n";
  private static final String EMPTY_STRING = "";
  private static Configuration instance;

  protected Configuration() {
    fonts = new Fonts();
  }

  public static Configuration getInstance() {
    if (instance == null) {
      instance = new Configuration();
    }
    return instance;
  }

  /**
   * use {@link com.odigeo.interactors.provider.MarketProviderInterface}
   */
  @Deprecated public static Locale getCurrentLocale() {
    return LocaleUtils.strToLocale(Configuration.getInstance().getCurrentMarket().getLocale());
  }

  public List<Step> readFlowSequence(XmlResourceParser parser)
      throws XmlPullParserException, IOException {

    Step first = null;
    Step second = null;
    Step third = null;
    Step fourth = null;

    parser.next();

    while (!(parser.getEventType() == XmlResourceParser.END_TAG && parser.getName()
        .equals("FlowSequence"))) {
      String tagName = parser.getName();

      if (tagName != null && parser.getEventType() == XmlResourceParser.START_TAG) {

        if ("First".equals(tagName)) {
          first = Step.valueOf(parser.nextText());
        } else if ("Second".equals(tagName)) {
          second = Step.valueOf(parser.nextText());
        } else if ("Third".equals(tagName)) {
          third = Step.valueOf(parser.nextText());
        } else if ("Fourth".equals(tagName)) {
          fourth = Step.valueOf(parser.nextText());
        }
      }

      parser.next();
    }

    List<Step> flowSequence = new ArrayList<Step>();
    flowSequence.add(first);
    flowSequence.add(second);
    flowSequence.add(third);
    flowSequence.add(fourth);

    return flowSequence;
  }

  private List<Market> readMarkets(XmlResourceParser parser)
      throws XmlPullParserException, IOException {
    List<Market> markets = new ArrayList<Market>();
    Market currentMarket = null;
    setDefaultMarket(null);
    parser.next();
    while (!(parser.getEventType() == XmlResourceParser.END_TAG && parser.getName()
        .equals("Markets"))) {
      String tagName = parser.getName();
      currentMarket = readMarket(parser, markets, currentMarket, tagName);
      parser.next();
    }
    return markets;
  }

  /**
   * Reads the market.
   *
   * @param parser Parser who gonna be extract the info.
   * @param markets List of the markets.
   * @param currentMarket current market to be processed.
   * @param tagName Tag name.
   * @return Market
   * @throws org.xmlpull.v1.XmlPullParserException Exception for the Xml read.
   * @throws java.io.IOException Exception for the IO operations.
   */

  private Market readMarket(XmlResourceParser parser, List<Market> markets, Market currentMarket,
      String tagName) throws XmlPullParserException, IOException {
    Market market = currentMarket;
    if (tagName != null && parser.getEventType() == XmlResourceParser.START_TAG) {
      if ("Market".equals(tagName)) {
        market = new Market();
        createMarket(parser, markets, market);
      } else {
        setMarketDataFromBrandConfiguration(market, tagName, parser);
      }
    }
    return market;
  }

  /**
   * This method updates the market data
   *
   * @param market Market to be updated
   * @param tagName Parameter to set
   * @param parser File with data
   * @throws java.io.IOException
   * @throws org.xmlpull.v1.XmlPullParserException
   */
  private void setMarketDataFromBrandConfiguration(Market market, String tagName,
      XmlResourceParser parser) throws IOException, XmlPullParserException {
    if ("Website".equals(tagName)) {
      market.setWebsite(parser.nextText());
    } else if ("Language".equals(tagName)) {
      String language = parser.nextText();
      market.setLanguage(language);
      // Default table's name is language letters.
      market.setOneCMSTable(language);
    } else if ("Locale".equals(tagName)) {
      market.setLocale(parser.nextText());
    } else if ("OneCMSTable".equals(tagName)) {
      market.setOneCMSTable(parser.nextText());
    } else {
      setMarketData(market, tagName, parser);
    }
  }

  /**
   * This method sets different parameters to the market
   *
   * @param market Market to be updated
   * @param tagName Parameter to set
   * @param parser File with data
   * @throws java.io.IOException
   * @throws org.xmlpull.v1.XmlPullParserException
   */
  private void setMarketData(Market market, String tagName, XmlResourceParser parser)
      throws IOException, XmlPullParserException {
    if ("Currency".equals(tagName)) {
      market.setCurrencyKey(parser.nextText());
    } else if ("CO2".equals(tagName)) {
      market.setHasCo2(parser.getAttributeBooleanValue(null, "hasCO2", false));
    } else if ("CO2Link".equals(tagName)) {
      market.setCo2Link(parser.nextText());
    } else if ("TwitterUser".equals(tagName)) {
      market.setTwitterUser(parser.nextText());
    } else if ("ResidentItineraries".equals(tagName)) {
      market.setResidentItineraries(readResidentItineraries(parser));
    } else if ("GACustomDim".equals(tagName)) {
      market.setGACustomDim(parser.nextText());
    } else if ("NeedsExplicitAcceptance".equals(tagName)) {
      market.setNeedsExplicitAcceptance(Boolean.valueOf(parser.nextText()));
    } else if ("InsurancesVersion".equals(tagName)) {
      market.setInsurancesVersion(parser.nextText());
    } else {
      processLastMarketValues(parser, market, tagName);
    }
  }

  /**
   * Create the market.
   *
   * @param parser Parser who gonna extracted the info.
   * @param markets Object to be filled.
   * @param currentMarket Current object.
   */
  private void createMarket(XmlResourceParser parser, List<Market> markets, Market currentMarket) {
    markets.add(currentMarket);
    if (parser.getAttributeBooleanValue(null, "default", false)) {
      setDefaultMarket(currentMarket);
    }
    currentMarket.setName(parser.getAttributeValue(null, "name"));
    currentMarket.setKey(parser.getAttributeValue(null, "key"));
  }

  /**
   * Process last values of markets.
   *
   * @param parser Parser who gonna extract the info.
   * @param currentMarket object to be fill
   * @param tagName Tag name.
   * @throws org.xmlpull.v1.XmlPullParserException Exception for the Xml read.
   * @throws java.io.IOException Exception for the IO operations.
   */
  private void processLastMarketValues(XmlResourceParser parser, Market currentMarket,
      String tagName) throws XmlPullParserException, IOException {
    if (TAG_FORCING_LATIN_KEYS.equals(tagName)) {
      currentMarket.setForcingLatingKeyboard(Boolean.valueOf(parser.nextText()));
    } else if ("TimeFormat".equals(tagName)) {
      currentMarket.setTimeFormat(parser.nextText());
    } else if ("XSellingMarketKey".equals(tagName)) {
      currentMarket.setCrossSellingMarketKey(parser.nextText());
    } else if ("XSellingSearchResultsHotelsUrl".equals(tagName)) {
      currentMarket.setXSellingSearchResultsHotelsUrl(parser.nextText());
    } else if ("InsuranceEuropeanConditions".equals(tagName)) {
      currentMarket.setInsuranceEuropeanConditions(
          parser.getAttributeBooleanValue(null, "value", false));
    } else {
      setMarketUrls(currentMarket, tagName, parser);
    }
  }

  /**
   * This method updates the market parameters urls
   *
   * @param currentMarket market to update
   * @param tagName parameter checked
   * @param parser file with data
   * @throws java.io.IOException
   * @throws org.xmlpull.v1.XmlPullParserException
   */
  private void setMarketUrls(Market currentMarket, String tagName, XmlResourceParser parser)
      throws IOException, XmlPullParserException {
    if ("XSellingFilterHotelsUrl".equals(tagName)) {
      currentMarket.setXSellingFilterHotelsUrl(parser.nextText());
    } else if ("XSellingSearchResultsCarsUrl".equals(tagName)) {
      currentMarket.setxSellingSearchResultsCarsUrl(parser.nextText());
    } else if ("XSellingFilterCarsUrl".equals(tagName)) {
      currentMarket.setxSellingFilterCarsUrl(parser.nextText());
    }
  }

  private ImagesSources readImageSources(XmlResourceParser parser)
      throws XmlPullParserException, IOException {
    ImagesSources imagesSources = new ImagesSources();
    parser.next();
    while (!(parser.getEventType() == XmlResourceParser.END_TAG && parser.getName()
        .equals("ImagesSources"))) {
      String tagName = parser.getName();
      if (tagName != null && parser.getEventType() == XmlResourceParser.START_TAG) {
        processImage(parser, imagesSources, tagName);
      }

      parser.next();
    }

    return imagesSources;
  }

  /**
   * Process the image info.
   *
   * @param parser Parser who gonna be extract the info.
   * @param imagesSources Object who be filled.
   * @param tagName Tag name.
   * @throws org.xmlpull.v1.XmlPullParserException Exception for the Xml read.
   * @throws java.io.IOException Exception for the IO operations.
   */
  private void processImage(XmlResourceParser parser, ImagesSources imagesSources, String tagName)
      throws XmlPullParserException, IOException {
    if ("URL_AirlineLogos".equals(tagName)) {
      imagesSources.setUrlAirlineLogos(parser.nextText());
    } else if ("URL_MyTrips".equals(tagName)) {
      imagesSources.setUrlMyTrips(parser.nextText());
    } else if ("URL_Countries".equals(tagName)) {
      imagesSources.setUrlCountries(parser.nextText());
    } else if ("ImageFileType".equals(tagName)) {
      imagesSources.setImageFileType(parser.nextText());
    } else if ("MultidestinationFormat".equals(tagName)) {
      imagesSources.setMultidestinationFormat(parser.nextText());
    }
  }

  private GeneralConstants readGeneralConstants(XmlResourceParser parser)
      throws XmlPullParserException, IOException {
    GeneralConstants generalConstants = new GeneralConstants();
    parser.next();
    while (!(parser.getEventType() == XmlResourceParser.END_TAG && parser.getName()
        .equals("GeneralConstants"))) {
      String tagName = parser.getName();
      processGeneralValues(parser, generalConstants, tagName);
      parser.next();
    }
    return generalConstants;
  }

  /**
   * Process the values for general values.
   *
   * @param parser Parser who gonna extract the info.
   * @param generalConstants Object to be filled.
   * @param tagName Tag name.
   * @throws org.xmlpull.v1.XmlPullParserException Exception for the Xml read.
   * @throws java.io.IOException Exception for the IO operations.
   */
  private void processGeneralValues(XmlResourceParser parser, GeneralConstants generalConstants,
      String tagName) throws XmlPullParserException, IOException {
    if (tagName != null && parser.getEventType() == XmlResourceParser.START_TAG) {
      if ("MaxNumberOfLegs".equals(tagName)) {
        generalConstants.setMaxNumberOfLegs(Integer.parseInt(parser.nextText()));
      } else if ("MinNumberOfLegs".equals(tagName)) {
        generalConstants.setMinNumberOfLegs(Integer.parseInt(parser.nextText()));
      } else {
        processGeneralConstants(parser, generalConstants, tagName);
      }
    }
  }

  /**
   * Process other values to general constants.
   *
   * @param parser Parser who gonna extract the info.
   * @param generalConstants Object to be filled.
   * @param tagName Tag name.
   * @throws org.xmlpull.v1.XmlPullParserException Exception for the Xml read.
   * @throws java.io.IOException Exception for the IO operations.
   */
  private void processGeneralConstants(XmlResourceParser parser, GeneralConstants generalConstants,
      String tagName) throws XmlPullParserException, IOException {
    if ("MaxInfantPerAdult".equals(tagName)) {
      generalConstants.setMaxInfantPerAdult(Integer.parseInt(parser.nextText()));
    } else if ("MaxKidsPerAdult".equals(tagName)) {
      generalConstants.setMaxKidsPerAdult(Integer.parseInt(parser.nextText()));
    } else if ("MaxPassengers".equals(tagName)) {
      generalConstants.setMaxPassengers(Integer.parseInt(parser.nextText()));
    } else if ("DefaultNumberOfAdults".equals(tagName)) {
      generalConstants.setDefaultNumberOfAdults(Integer.parseInt(parser.nextText()));
    } else if ("DefaultNumberOfInfants".equals(tagName)) {
      generalConstants.setDefaultNumberOfInfants(Integer.parseInt(parser.nextText()));
    } else if ("DefaultNumberOfKids".equals(tagName)) {
      generalConstants.setDefaultNumberOfKids(Integer.parseInt(parser.nextText()));
    } else if ("NumResultsOpenedByDefaultSimpleSearch".equals(tagName)) {
      generalConstants.setNumResultsOpenedByDefaultSimpleSearch(
          Integer.parseInt(parser.nextText()));
    } else if ("NumResultsOpenedByDefaultMultiSearch".equals(tagName)) {
      generalConstants.setNumResultsOpenedByDefaultMultiSearch(Integer.parseInt(parser.nextText()));
    }
  }

  private List<CabinClassDTO> readCabinClasses(XmlResourceParser parser)
      throws XmlPullParserException, IOException {
    List<CabinClassDTO> cabinClasses = new ArrayList<>();
    parser.next();
    while (!(parser.getEventType() == XmlResourceParser.END_TAG && parser.getName()
        .equals("FlightClasses"))) {
      String tagName = parser.getName();
      if (tagName != null && parser.getEventType() == XmlResourceParser.START_TAG) {
        getCabinClassDTOSpinnerItemModel(parser, cabinClasses, tagName);
      }

      parser.next();
    }

    return cabinClasses;
  }

  /**
   * Get the cabin class spinner item model
   *
   * @param parser Parser who gonna extract the info.
   * @param cabinClasses Cabin classes.
   * @param tagName Tag name.
   * @throws org.xmlpull.v1.XmlPullParserException Exception for the Xml read.
   * @throws java.io.IOException Exception for the IO operations.
   */
  private void getCabinClassDTOSpinnerItemModel(XmlResourceParser parser,
      List<CabinClassDTO> cabinClasses, String tagName) throws XmlPullParserException, IOException {
    if ("Label".equals(tagName)) {
      parser.nextText();
    } else if ("Key".equals(tagName)) {
      String keyCabinClass = parser.nextText();

      cabinClasses.add(CabinClassDTO.valueOf(keyCabinClass));
    }
  }

  private List<LocaleToMarket> readLocalesToMarkets(XmlResourceParser parser)
      throws XmlPullParserException, IOException {
    List<LocaleToMarket> localeToMarketList = new ArrayList<LocaleToMarket>();

    defaultMarketId = parser.getAttributeValue(null, "marketDefault");

    parser.next();

    while (!(parser.getEventType() == XmlResourceParser.END_TAG && parser.getName()
        .equals("LocalesToMarkets"))) {
      String tagName = parser.getName();

      if ((tagName != null && parser.getEventType() == XmlResourceParser.START_TAG)
          && "Item".equals(tagName)) {
        localeToMarketList.add(new LocaleToMarket(parser.getAttributeValue(null, "locale"),
            parser.getAttributeValue(null, "market")));
                /*if ("Item".equals(tagName)) {
                    localeToMarketList.add(
                        new LocaleToMarket(
                            parser.getAttributeValue(null, "locale"),
                            parser.getAttributeValue(null, "market")));
                }*/
      }

      parser.next();
    }

    return localeToMarketList;
  }

  private List<CarrierDTO> readAirlines(XmlResourceParser parser)
      throws XmlPullParserException, IOException {
    List<CarrierDTO> carrierDTOs = new ArrayList<CarrierDTO>();

    parser.next();

    while (!(parser.getEventType() == XmlResourceParser.END_TAG && parser.getName()
        .equals("Airlines"))) {
      String tagName = parser.getName();

      if ((tagName != null && parser.getEventType() == XmlResourceParser.START_TAG)
          && "Airline".equals(tagName)) {
        carrierDTOs.add(new CarrierDTO(parser.getAttributeValue(null, "key"), parser.nextText()));
                /*if ("Airline".equals(tagName)) {
                    carrierDTOs.add(
                        new CarrierDTO(
                            parser.getAttributeValue(null, "key"),
                            parser.nextText()));
                }*/
      }

      parser.next();
    }

    return carrierDTOs;
  }

  private List<ResidentItinerary> readResidentItineraries(XmlResourceParser parser)
      throws XmlPullParserException, IOException {
    List<ResidentItinerary> residentItineraries = new ArrayList<ResidentItinerary>();
    ResidentItinerary residentItinerary = null;

    parser.next();

    while (!(parser.getEventType() == XmlResourceParser.END_TAG && parser.getName()
        .equals("ResidentItineraries"))) {
      String tagName = parser.getName();

      if (tagName != null && parser.getEventType() == XmlResourceParser.START_TAG) {
        if ("Itinerary".equals(tagName)) {
          residentItinerary = new ResidentItinerary();
          residentItineraries.add(residentItinerary);
        } else if ("Origin".equals(tagName)) {
          boolean isCountry = parser.getAttributeBooleanValue(null, "isCountry", false);

          residentItinerary.setOrigin(new ResidentItineraryPlace(parser.nextText(), isCountry));
        } else if ("Destination".equals(tagName)) {
          boolean isCountry = parser.getAttributeBooleanValue(null, "isCountry", false);

          residentItinerary.setDestination(
              new ResidentItineraryPlace(parser.nextText(), isCountry));
        } else if ("ResidentPlace".equals(tagName)) {
          residentItinerary.setResidentPlace(parser.nextText());
        }
      }

      parser.next();
    }

    return residentItineraries;
  }

  public void readAirlinesXml(XmlResourceParser parser) throws XmlPullParserException, IOException {
    while (parser.getEventType() != XmlResourceParser.END_DOCUMENT) {
      if ((parser.getEventType() != XmlResourceParser.START_DOCUMENT
          && parser.getEventType() == XmlResourceParser.START_TAG) && parser.getName()
          .equals("Airlines")) {
        this.setCarriers(readAirlines(parser));
      }
      parser.next();
    }
  }

  /**
   * Load all the configuration contained in the XML file, where its path is pass as a parameter
   *
   * @param parser Path of the XML File that will be loadWidgetImage
   */
  public Configuration readXmlFile(XmlResourceParser parser)
      throws XmlPullParserException, IOException {

    while (parser.getEventType() != XmlResourceParser.END_DOCUMENT) {
      if (parser.getEventType() != XmlResourceParser.START_DOCUMENT
          && parser.getEventType() == XmlResourceParser.START_TAG) {
        String tagName = parser.getName();
        Log.d(Constants.TAG_LOG, "Leyendo " + tagName);
        if ("ConfigurationBrand".equals(tagName)) {
          setFileVersion(parser.getAttributeValue(null, "fileVersion"));
        }
        setConfigurationData(tagName, parser);
      }

      parser.next();
    }

    this.brandKey = DeviceUtils.getShortBrandName(getBrand());

    return this;
  }

  /**
   * This method sets the Configuration data
   *
   * @param tagName Parameter to set
   * @param parser File with data
   * @throws java.io.IOException
   * @throws org.xmlpull.v1.XmlPullParserException
   */
  private void setConfigurationData(String tagName, XmlResourceParser parser)
      throws IOException, XmlPullParserException {
    if ("BrandVisualName".equals(tagName)) {
      setBrandVisualName(parser.nextText());
    } else if ("BrandPrefix".equals(tagName)) {
      setBrandPrefix(parser.nextText());
    } else if ("FlowSequence".equals(tagName)) {
      setFlowSequence(readFlowSequence(parser));
    } else if ("ImagesSources".equals(tagName)) {
      setImagesSources(readImageSources(parser));
    } else if ("HotelsAID".equals(tagName)) {
      this.setAid(parser.nextText());
    } else if ("FlightClasses".equals(tagName)) {
      Lists.getInstance().setCabinClasses(readCabinClasses(parser));
    } else if ("LocalesToMarkets".equals(tagName)) {
      this.setLocaleToMarkets(readLocalesToMarkets(parser));
    }
    setClassAttributes(tagName, parser);
    setConfigurationURLs(tagName, parser);
  }

  /**
   * This method sets the Configuration data
   *
   * @param tagName Parameter to set
   * @param parser File with data
   * @throws java.io.IOException
   * @throws org.xmlpull.v1.XmlPullParserException
   */
  private void setConfigurationURLs(String tagName, XmlResourceParser parser)
      throws IOException, XmlPullParserException {
    if ("AppStoreURL".equals(tagName)) {
      setAppStoreURL(parser.nextText());
    } else if ("LastMinuteURL".equals(tagName)) {
      this.setLastMinuteURL(parser.nextText());
    } else if ("PackageURL".equals(tagName)) {
      this.setPackageURL(parser.nextText());
    } else if ("ProdDomain".equals(tagName)) {
      this.setProdDomain(parser.nextText());
    }
  }

  /**
   * This method sets the Configuration data
   *
   * @param tagName Parameter to set
   * @param parser File with data
   * @throws java.io.IOException
   * @throws org.xmlpull.v1.XmlPullParserException
   */
  private void setClassAttributes(String tagName, XmlResourceParser parser)
      throws IOException, XmlPullParserException {
    if ("Markets".equals(tagName)) {
      this.markets = readMarkets(parser);
    } else if ("GeneralConstants".equals(tagName)) {
      generalConstants = readGeneralConstants(parser);
    } else if ("HotelsURL".equals(tagName)) {
      this.webViewHotelsURL = parser.nextText();
    } else if ("HotelsLabel".equals(tagName)) {
      this.hotelsLabel = parser.nextText();
    } else if ("Brand".equals(tagName)) {
      brand = parser.getAttributeValue(null, "name");
    }
  }

  public String getAnalyticsAppKey(Context context) {
    return context.getString(R.string.analytics_app_key);
  }

  public String getAnalyticsMarketKey(Context context) {
    String marketKey = "";

    List<String> analyticsMarkets =
        Arrays.asList(context.getResources().getStringArray(R.array.google_analytics_markets));

    for (int i = 0; i < analyticsMarkets.size(); i++) {
      if (analyticsMarkets.get(i).equals(getCurrentMarket().getKey())) {
        marketKey =
            Arrays.asList(context.getResources().getStringArray(R.array.google_analytics_keys))
                .get(i);
      }
    }

    return marketKey;
  }

  public String getGCMDefaultSenderID(Context context) {
    return context.getString(R.string.gcm_defaultSenderId);
  }

  public void setMarketWithLocale(String locale) throws Exception {
    for (Market market : Configuration.getInstance().getMarkets()) {
      if (market.getKey().equals(locale)) {
        Configuration.getInstance().setCurrentMarket(market);
        return;
      }
    }
    throw new Exception("No locale found");
  }
}
