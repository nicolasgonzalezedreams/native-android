package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AccommodationBooking implements Serializable {
  protected List<AccommodationBookingRoom> bookingRooms;
  protected List<AccommodationPromotionDTO> promotions;
  protected String accommodationDescription;
  protected String roomsDescription;
  protected String city;
  protected String address;
  protected String postalCode;
  protected String phoneNumber;
  protected String countryCode;
  protected String countryName;
  protected GeoCoordinatesDTO coordinates;
  protected String affiliation;
  protected String bono;
  protected String comment;
  protected String accommodationCancelPolicy;
  protected String roomsGroupCancelPolicy;
  protected String bookingCancelPolicy;
  protected long checkInDate;
  protected long checkOutDate;
  protected String dateTimeZone;
  protected int numberOfRooms;
  protected int numberOfAdults;
  protected int numberOfChildren;
  protected int numberOfInfants;
  protected String locator;
  protected String accommodationName;
  protected AccommodationCategoryDTO category;
  protected boolean paymentAtHotel;
  protected boolean merchant;
  protected ProviderPaymentMethodTypeDTO paymentMethodType;
  protected String providerId;
  protected BigDecimal totalProviderPrice;
  protected String providerPaymentCurrency;
  protected BoardTypeDTO boardType;
  protected BookingStatusDTO bookingStatus;
  protected long providerBookingId;
  protected boolean bonoSent;
  protected boolean dynpackFare;
  protected BigDecimal price;

  /**
   * Gets the value of the bookingRooms property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the bookingRooms property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getBookingRooms().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list
   * {@link AccommodationBookingRoom }
   */
  public final List<AccommodationBookingRoom> getBookingRooms() {
    if (bookingRooms == null) {
      bookingRooms = new ArrayList<AccommodationBookingRoom>();
    }
    return this.bookingRooms;
  }

  public final void setBookingRooms(List<AccommodationBookingRoom> bookingRooms) {
    this.bookingRooms = bookingRooms;
  }

  /**
   * Gets the value of the promotions property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the promotions property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getPromotions().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list
   * {@link com.odigeo.app.android.lib.models.dto.AccommodationPromotionDTO }
   */
  public final List<AccommodationPromotionDTO> getPromotions() {
    if (promotions == null) {
      promotions = new ArrayList<AccommodationPromotionDTO>();
    }
    return this.promotions;
  }

  public final void setPromotions(List<AccommodationPromotionDTO> promotions) {
    this.promotions = promotions;
  }

  /**
   * Gets the value of the accommodationDescription property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getAccommodationDescription() {
    return accommodationDescription;
  }

  /**
   * Sets the value of the accommodationDescription property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setAccommodationDescription(String value) {
    this.accommodationDescription = value;
  }

  /**
   * Gets the value of the roomsDescription property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getRoomsDescription() {
    return roomsDescription;
  }

  /**
   * Sets the value of the roomsDescription property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setRoomsDescription(String value) {
    this.roomsDescription = value;
  }

  /**
   * Gets the value of the city property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getCity() {
    return city;
  }

  /**
   * Sets the value of the city property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setCity(String value) {
    this.city = value;
  }

  /**
   * Gets the value of the address property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getAddress() {
    return address;
  }

  /**
   * Sets the value of the address property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setAddress(String value) {
    this.address = value;
  }

  /**
   * Gets the value of the postalCode property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getPostalCode() {
    return postalCode;
  }

  /**
   * Sets the value of the postalCode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setPostalCode(String value) {
    this.postalCode = value;
  }

  /**
   * Gets the value of the phoneNumber property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getPhoneNumber() {
    return phoneNumber;
  }

  /**
   * Sets the value of the phoneNumber property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setPhoneNumber(String value) {
    this.phoneNumber = value;
  }

  /**
   * Gets the value of the countryCode property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getCountryCode() {
    return countryCode;
  }

  /**
   * Sets the value of the countryCode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setCountryCode(String value) {
    this.countryCode = value;
  }

  /**
   * Gets the value of the countryName property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getCountryName() {
    return countryName;
  }

  /**
   * Sets the value of the countryName property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setCountryName(String value) {
    this.countryName = value;
  }

  /**
   * Gets the value of the coordinates property.
   *
   * @return possible object is
   * {@link com.odigeo.app.android.lib.models.dto.GeoCoordinatesDTO }
   */
  public final GeoCoordinatesDTO getCoordinates() {
    return coordinates;
  }

  /**
   * Sets the value of the coordinates property.
   *
   * @param value allowed object is
   * {@link com.odigeo.app.android.lib.models.dto.GeoCoordinatesDTO }
   */
  public final void setCoordinates(GeoCoordinatesDTO value) {
    this.coordinates = value;
  }

  /**
   * Gets the value of the affiliation property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getAffiliation() {
    return affiliation;
  }

  /**
   * Sets the value of the affiliation property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setAffiliation(String value) {
    this.affiliation = value;
  }

  /**
   * Gets the value of the bono property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getBono() {
    return bono;
  }

  /**
   * Sets the value of the bono property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setBono(String value) {
    this.bono = value;
  }

  /**
   * Gets the value of the comment property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getComment() {
    return comment;
  }

  /**
   * Sets the value of the comment property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setComment(String value) {
    this.comment = value;
  }

  /**
   * Gets the value of the accommodationCancelPolicy property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getAccommodationCancelPolicy() {
    return accommodationCancelPolicy;
  }

  /**
   * Sets the value of the accommodationCancelPolicy property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setAccommodationCancelPolicy(String value) {
    this.accommodationCancelPolicy = value;
  }

  /**
   * Gets the value of the roomsGroupCancelPolicy property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getRoomsGroupCancelPolicy() {
    return roomsGroupCancelPolicy;
  }

  /**
   * Sets the value of the roomsGroupCancelPolicy property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setRoomsGroupCancelPolicy(String value) {
    this.roomsGroupCancelPolicy = value;
  }

  /**
   * Gets the value of the bookingCancelPolicy property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getBookingCancelPolicy() {
    return bookingCancelPolicy;
  }

  /**
   * Sets the value of the bookingCancelPolicy property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setBookingCancelPolicy(String value) {
    this.bookingCancelPolicy = value;
  }

  /**
   * Gets the value of the checkInDate property.
   *
   * @return possible object is
   * {@link String }
   */
  public final long getCheckInDate() {
    return checkInDate;
  }

  /**
   * Sets the value of the checkInDate property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setCheckInDate(long value) {
    this.checkInDate = value;
  }

  /**
   * Gets the value of the checkOutDate property.
   *
   * @return possible object is
   * {@link String }
   */
  public final long getCheckOutDate() {
    return checkOutDate;
  }

  /**
   * Sets the value of the checkOutDate property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setCheckOutDate(long value) {
    this.checkOutDate = value;
  }

  /**
   * Gets the value of the numberOfRooms property.
   */
  public final int getNumberOfRooms() {
    return numberOfRooms;
  }

  /**
   * Sets the value of the numberOfRooms property.
   */
  public final void setNumberOfRooms(int value) {
    this.numberOfRooms = value;
  }

  /**
   * Gets the value of the numberOfAdults property.
   */
  public final int getNumberOfAdults() {
    return numberOfAdults;
  }

  /**
   * Sets the value of the numberOfAdults property.
   */
  public final void setNumberOfAdults(int value) {
    this.numberOfAdults = value;
  }

  /**
   * Gets the value of the numberOfChildren property.
   */
  public final int getNumberOfChildren() {
    return numberOfChildren;
  }

  /**
   * Sets the value of the numberOfChildren property.
   */
  public final void setNumberOfChildren(int value) {
    this.numberOfChildren = value;
  }

  /**
   * Gets the value of the numberOfInfants property.
   */
  public final int getNumberOfInfants() {
    return numberOfInfants;
  }

  /**
   * Sets the value of the numberOfInfants property.
   */
  public final void setNumberOfInfants(int value) {
    this.numberOfInfants = value;
  }

  /**
   * Gets the value of the locator property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getLocator() {
    return locator;
  }

  /**
   * Sets the value of the locator property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setLocator(String value) {
    this.locator = value;
  }

  /**
   * Gets the value of the accommodationName property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getAccommodationName() {
    return accommodationName;
  }

  /**
   * Sets the value of the accommodationName property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setAccommodationName(String value) {
    this.accommodationName = value;
  }

  /**
   * Gets the value of the category property.
   *
   * @return possible object is
   * {@link com.odigeo.app.android.lib.models.dto.AccommodationCategoryDTO }
   */
  public final AccommodationCategoryDTO getCategory() {
    return category;
  }

  /**
   * Sets the value of the category property.
   *
   * @param value allowed object is
   * {@link com.odigeo.app.android.lib.models.dto.AccommodationCategoryDTO }
   */
  public final void setCategory(AccommodationCategoryDTO value) {
    this.category = value;
  }

  /**
   * Gets the value of the paymentAtHotel property.
   */
  public final boolean isPaymentAtHotel() {
    return paymentAtHotel;
  }

  /**
   * Sets the value of the paymentAtHotel property.
   */
  public final void setPaymentAtHotel(boolean value) {
    this.paymentAtHotel = value;
  }

  /**
   * Gets the value of the merchant property.
   */
  public final boolean isMerchant() {
    return merchant;
  }

  /**
   * Sets the value of the merchant property.
   */
  public final void setMerchant(boolean value) {
    this.merchant = value;
  }

  /**
   * Gets the value of the paymentMethodType property.
   *
   * @return possible object is
   * {@link com.odigeo.app.android.lib.models.dto.ProviderPaymentMethodTypeDTO }
   */
  public final ProviderPaymentMethodTypeDTO getPaymentMethodType() {
    return paymentMethodType;
  }

  /**
   * Sets the value of the paymentMethodType property.
   *
   * @param value allowed object is
   * {@link com.odigeo.app.android.lib.models.dto.ProviderPaymentMethodTypeDTO }
   */
  public final void setPaymentMethodType(ProviderPaymentMethodTypeDTO value) {
    this.paymentMethodType = value;
  }

  /**
   * Gets the value of the providerId property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getProviderId() {
    return providerId;
  }

  /**
   * Sets the value of the providerId property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setProviderId(String value) {
    this.providerId = value;
  }

  /**
   * Gets the value of the totalProviderPrice property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public final BigDecimal getTotalProviderPrice() {
    return totalProviderPrice;
  }

  /**
   * Sets the value of the totalProviderPrice property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public final void setTotalProviderPrice(BigDecimal value) {
    this.totalProviderPrice = value;
  }

  /**
   * Gets the value of the providerPaymentCurrency property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getProviderPaymentCurrency() {
    return providerPaymentCurrency;
  }

  /**
   * Sets the value of the providerPaymentCurrency property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setProviderPaymentCurrency(String value) {
    this.providerPaymentCurrency = value;
  }

  /**
   * Gets the value of the boardType property.
   *
   * @return possible object is
   * {@link com.odigeo.app.android.lib.models.dto.BoardTypeDTO }
   */
  public final BoardTypeDTO getBoardType() {
    return boardType;
  }

  /**
   * Sets the value of the boardType property.
   *
   * @param value allowed object is
   * {@link com.odigeo.app.android.lib.models.dto.BoardTypeDTO }
   */
  public final void setBoardType(BoardTypeDTO value) {
    this.boardType = value;
  }

  /**
   * Gets the value of the bookingStatus property.
   *
   * @return possible object is
   * {@link com.odigeo.app.android.lib.models.dto.BookingStatusDTO }
   */
  public final BookingStatusDTO getBookingStatus() {
    return bookingStatus;
  }

  /**
   * Sets the value of the bookingStatus property.
   *
   * @param value allowed object is
   * {@link com.odigeo.app.android.lib.models.dto.BookingStatusDTO }
   */
  public final void setBookingStatus(BookingStatusDTO value) {
    this.bookingStatus = value;
  }

  /**
   * Gets the value of the providerBookingId property.
   */
  public final long getProviderBookingId() {
    return providerBookingId;
  }

  /**
   * Sets the value of the providerBookingId property.
   */
  public final void setProviderBookingId(long value) {
    this.providerBookingId = value;
  }

  /**
   * Gets the value of the bonoSent property.
   */
  public final boolean isBonoSent() {
    return bonoSent;
  }

  /**
   * Sets the value of the bonoSent property.
   */
  public final void setBonoSent(boolean value) {
    this.bonoSent = value;
  }

  /**
   * Gets the value of the dynpackFare property.
   */
  public final boolean isDynpackFare() {
    return dynpackFare;
  }

  /**
   * Sets the value of the dynpackFare property.
   */
  public final void setDynpackFare(boolean value) {
    this.dynpackFare = value;
  }

  public final String getDateTimeZone() {
    return dateTimeZone;
  }

  public final void setDateTimeZone(String dateTimeZone) {
    this.dateTimeZone = dateTimeZone;
  }

  public final BigDecimal getPrice() {
    return price;
  }

  public final void setPrice(BigDecimal price) {
    this.price = price;
  }
}
