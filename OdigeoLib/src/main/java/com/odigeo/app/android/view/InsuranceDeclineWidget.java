package com.odigeo.app.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.animations.CollapseAndExpandAnimationUtil;
import com.odigeo.app.android.view.constants.OneCMSKeys;

public class InsuranceDeclineWidget extends LinearLayout implements View.OnClickListener {

  Context mContext;
  ScrollView mScrollView;
  CheckBox mCbInsuranceDecline;
  TextView mTvInsuranceDeclineTitle, mTvInsuranceDeclineDescription, mTvInsuranceRejectTitle;
  LinearLayout mLlInsuranceDeclineExpandedContent, mLlInsuranceRejectContent;
  InsuranceDeclineWidgetListener mListener;

  public InsuranceDeclineWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public InsuranceDeclineWidget(Context context, ScrollView scrollView,
      InsuranceDeclineWidgetListener insuranceDeclineWidgetListener) {
    super(context);
    inflate(context, R.layout.insurance_decline_widget, this);
    mContext = context;
    mScrollView = scrollView;
    mListener = insuranceDeclineWidgetListener;
    setOnClickListener(this);
    bindViews();
    addContent();
  }

  public CheckBox getCheckbox() {
    return mCbInsuranceDecline;
  }

  public void setChecked(boolean value) {
    mCbInsuranceDecline.setChecked(value);
  }

  private void bindViews() {
    mCbInsuranceDecline = (CheckBox) findViewById(R.id.cbInsuranceDecline);
    mTvInsuranceDeclineTitle = (TextView) findViewById(R.id.tvInsuranceDeclineTitle);
    mTvInsuranceDeclineDescription = (TextView) findViewById(R.id.tvInsuranceDeclineDescription);
    mLlInsuranceDeclineExpandedContent =
        (LinearLayout) findViewById(R.id.llInsuranceDeclineExpandedContent);
    mTvInsuranceRejectTitle = (TextView) findViewById(R.id.tvInsuranceRejectTitle);
    mLlInsuranceRejectContent = (LinearLayout) findViewById(R.id.llInsuranceRejectContent);
  }

  private void addContent() {
    mTvInsuranceDeclineTitle.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.INSURANCE_WIDGET_DECLINE_TITLE));
    mTvInsuranceDeclineDescription.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.INSURANCE_WIDGET_DECLINE_DESCRIPTION));
    mTvInsuranceRejectTitle.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.INSURANCEMANAGER_REJECT_DESCRIPTION));
    String[] descriptionItems =
        LocalizablesFacade.getString(mContext, OneCMSKeys.INSURANCEMANAGER_REJECT_DESCRIPTION_EXTRA)
            .toString()
            .split("\n");
    for (String descriptionItem : descriptionItems) {
      LayoutInflater inflater =
          (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      View rowView =
          inflater.inflate(R.layout.insurance_decline_row_description, mLlInsuranceRejectContent,
              false);
      mLlInsuranceRejectContent.addView(rowView);
      TextView txtDescription = (TextView) rowView.findViewById(R.id.tvInsuranceRejectDescription);
      txtDescription.setText(descriptionItem.trim());
    }
  }

  @Override public void onClick(View view) {
    if (mCbInsuranceDecline.isChecked()) {
      setChecked(false);
      collapseRejectContent(this);
      mListener.onInsuranceDeclineUnselected();
    } else {
      setChecked(true);
      expandRejectContent();
      mListener.onInsuranceDeclineSelected(this);
    }
  }

  public void expandRejectContent() {
    CollapseAndExpandAnimationUtil.expand(mLlInsuranceDeclineExpandedContent);
  }

  public void collapseRejectContent(View goToView) {
    CollapseAndExpandAnimationUtil.collapse(mLlInsuranceDeclineExpandedContent, true, mScrollView,
        goToView);
  }

  public interface InsuranceDeclineWidgetListener {
    void onInsuranceDeclineSelected(InsuranceDeclineWidget insuranceDeclineWidget);

    void onInsuranceDeclineUnselected();
  }
}