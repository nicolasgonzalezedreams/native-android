package com.odigeo.app.android.view;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.navigator.AppRateFeedBackNavigator;
import com.odigeo.app.android.utils.BitmapUtils;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DialogHelper;
import com.odigeo.app.android.view.helpers.PermissionsHelper;
import com.odigeo.app.android.view.textwatchers.BaseTextWatcher;
import com.odigeo.app.android.view.textwatchers.OnFocusChange.BaseOnFocusChange;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.MailValidator;
import com.odigeo.presenter.AppRateHelpImprovePresenter;
import com.odigeo.presenter.contracts.views.AppRateHelpImproveViewInterface;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.odigeo.app.android.view.constants.OneCMSKeys.HELPCENTER_SENDING_ERROR;
import static com.odigeo.app.android.view.constants.OneCMSKeys.HELPCENTER_UPLOAD_SCREENSHOT_BUTTON;
import static com.odigeo.app.android.view.constants.OneCMSKeys.HINT_EMAIL;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_ACCESS_ALERT_INFO;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_GIVE_FEEDBACK_SUBTITLE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_GIVE_FEEDBACK_TITLE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_HELP_IMPROVE_EMAIL_SUBJECT;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_HELP_PLACEHOLDER;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_HELP_TITLE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_SEND_FEEDBACK_BUTTON;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_SEND_FEEDBACK_OK;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_UPLOAD_BUTTON_CHANGE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_UPLOAD_BUTTON_REMOVE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_UPLOAD_QUESTION;
import static com.odigeo.app.android.view.constants.OneCMSKeys.LEAVEFEEDBACK_WAITING_SENDING;

public class AppRateHelpImproveView extends BaseView<AppRateHelpImprovePresenter>
    implements AppRateHelpImproveViewInterface {

  public static final int PICK_IMAGE_REQUEST = 1;
  public static final String CHARACTERS_MAX = "/3.000";
  public static final int DEFAULT_CHARACTER_COUNT = 0;
  private TextView mTvAppRateHelpImproveTitle;
  private TextView mTvAppRateHelpImproveText;
  private TextView mTvAppRateHelpImproveSubtitle;
  private TextInputLayout mTilAppRateHelpImproveEmail;
  private EditText mEtAppRateHelpImproveComment;
  private TextView mTvAppRateHelpImproveCharacters;
  private Button mBtnScreenshot;
  private Button mBtnHelpImprove;
  private TextWatcher mTextWatcher;
  private ImageView mIvScreenshotOne;
  private ImageView mIvScreenshotTwo;
  private List<Bitmap> mScreensList;
  private DialogHelper mDialogHelper;

  public static AppRateHelpImproveView newInstance() {
    Bundle args = new Bundle();
    AppRateHelpImproveView fragment = new AppRateHelpImproveView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override protected AppRateHelpImprovePresenter getPresenter() {
    return dependencyInjector.provideAppRateHelpImprovePresenter(this,
        (AppRateFeedBackNavigator) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_apprate_helpimprove;
  }

  @Override protected void initComponent(View view) {
    mTvAppRateHelpImproveTitle = (TextView) view.findViewById(R.id.tv_apprate_helpimprove_title);
    mTvAppRateHelpImproveText = (TextView) view.findViewById(R.id.tv_apprate_helpimprove_text);
    mTvAppRateHelpImproveSubtitle =
        (TextView) view.findViewById(R.id.tv_apprate_helpimprove_subtitle);
    mEtAppRateHelpImproveComment =
        (EditText) view.findViewById(R.id.et_apprate_helpimprove_comment);
    mTilAppRateHelpImproveEmail =
        (TextInputLayout) view.findViewById(R.id.tilAppRateHelpImproveEmail);
    mTvAppRateHelpImproveCharacters =
        (TextView) view.findViewById(R.id.tv_apprate_helpimprove_characters);
    mBtnScreenshot = (Button) view.findViewById(R.id.btn_screenshot);
    mBtnHelpImprove = (Button) view.findViewById(R.id.btn_help_improve);
    mTextWatcher = new TextWatcher() {
      @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
        mTvAppRateHelpImproveCharacters.setText(s.length() + CHARACTERS_MAX);
      }

      @Override public void afterTextChanged(Editable s) {
        mPresenter.validateFields(mEtAppRateHelpImproveComment.getText().toString(),
            mTilAppRateHelpImproveEmail.getError());
      }
    };

    mTvAppRateHelpImproveCharacters.setText(DEFAULT_CHARACTER_COUNT + CHARACTERS_MAX);
    mIvScreenshotOne = (ImageView) view.findViewById(R.id.ivScreenshotOne);
    mIvScreenshotOne.setVisibility(View.GONE);
    mIvScreenshotTwo = (ImageView) view.findViewById(R.id.ivScreenshotTwo);
    mIvScreenshotTwo.setVisibility(View.GONE);
    mScreensList = new ArrayList<>();
    mDialogHelper = new DialogHelper(getActivity());

    setEmailErrorValidator();
    mPresenter.checkIfIsLogged();
  }

  @Override public void changeButtonHelpImproveStatus(boolean enable) {
    mBtnHelpImprove.setEnabled(enable);
  }

  private void setEmailErrorValidator() {
    if (mTilAppRateHelpImproveEmail.getEditText() != null) {
      mTilAppRateHelpImproveEmail.getEditText()
          .setOnFocusChangeListener(
              new BaseOnFocusChange(getContext(), mTilAppRateHelpImproveEmail, new MailValidator(),
                  OneCMSKeys.VALIDATION_ERROR_MAIL));
      mTilAppRateHelpImproveEmail.getEditText()
          .addTextChangedListener(new BaseTextWatcher(mTilAppRateHelpImproveEmail));
    }
  }

  @Override protected void setListeners() {
    mBtnHelpImprove.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        showProgress();
        mPresenter.sendFeedback(mTilAppRateHelpImproveEmail.getEditText().getText().toString(),
            localizables.getString(LEAVEFEEDBACK_HELP_IMPROVE_EMAIL_SUBJECT),
            mEtAppRateHelpImproveComment.getText().toString(),
            BitmapUtils.getEncodedBitmapByteArrayList(mScreensList));
        mTracker.trackAppRateHelpUsImproveView();
      }
    });

    mBtnScreenshot.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          if (PermissionsHelper.askForPermissionIfNeededInFragment(
              Manifest.permission.READ_EXTERNAL_STORAGE, AppRateHelpImproveView.this,
              localizables.getString(LEAVEFEEDBACK_ACCESS_ALERT_INFO),
              PermissionsHelper.READ_EXTERNAL_STORAGE_REQUEST_CODE)) {
            triggerGalleryIntent();
          }
        } else {
          triggerGalleryIntent();
        }
      }
    });
    mEtAppRateHelpImproveComment.addTextChangedListener(mTextWatcher);
    mEtAppRateHelpImproveComment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override public void onFocusChange(View v, boolean hasFocus) {
        mPresenter.validateFields(mEtAppRateHelpImproveComment.getText().toString(),
            mTilAppRateHelpImproveEmail.getError());
      }
    });
    mIvScreenshotOne.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        showFirstScreenshotDialog();
      }
    });

    mIvScreenshotTwo.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        showSecondScreenshotDialog();
      }
    });
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == PICK_IMAGE_REQUEST
        && resultCode == Activity.RESULT_OK
        && data != null
        && data.getData() != null) {

      Uri uri = data.getData();

      try {
        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);

        if (mIvScreenshotOne.getVisibility() == View.GONE) {

          mIvScreenshotOne.setVisibility(View.VISIBLE);
          mIvScreenshotOne.setImageBitmap(BitmapUtils.roundCorners(bitmap));
          mScreensList.add(bitmap);
        } else if (mIvScreenshotTwo.getVisibility() == View.GONE) {

          mIvScreenshotTwo.setVisibility(View.VISIBLE);
          mIvScreenshotTwo.setImageBitmap(BitmapUtils.roundCorners(bitmap));
          mBtnScreenshot.setVisibility(View.GONE);
          mScreensList.add(bitmap);
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private void triggerGalleryIntent() {
    Intent intent = new Intent();
    intent.setType("image/*");
    intent.setAction(Intent.ACTION_GET_CONTENT);
    startActivityForResult(Intent.createChooser(intent, null), PICK_IMAGE_REQUEST);
  }

  private void showFirstScreenshotDialog() {
    AlertDialog.Builder imageDialog = new AlertDialog.Builder(getActivity());
    imageDialog.setMessage(localizables.getString(LEAVEFEEDBACK_UPLOAD_QUESTION));

    imageDialog.setPositiveButton(localizables.getString(LEAVEFEEDBACK_UPLOAD_BUTTON_CHANGE),
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            mIvScreenshotOne.setVisibility(View.GONE);
            triggerGalleryIntent();
          }
        });

    imageDialog.setNegativeButton(localizables.getString(LEAVEFEEDBACK_UPLOAD_BUTTON_REMOVE),
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            mScreensList.remove(0);
            if (mIvScreenshotTwo.getVisibility() != View.GONE) {
              mIvScreenshotOne.setImageDrawable(mIvScreenshotTwo.getDrawable());
              mIvScreenshotTwo.setVisibility(View.GONE);
            } else {
              mIvScreenshotOne.setVisibility(View.GONE);
            }
            mBtnScreenshot.setVisibility(View.VISIBLE);
          }
        });
    imageDialog.show();
  }

  private void showSecondScreenshotDialog() {
    AlertDialog.Builder imageDialog = new AlertDialog.Builder(getActivity());
    imageDialog.setMessage(localizables.getString(LEAVEFEEDBACK_UPLOAD_QUESTION));
    imageDialog.setPositiveButton(localizables.getString(LEAVEFEEDBACK_UPLOAD_BUTTON_CHANGE),
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            mIvScreenshotTwo.setVisibility(View.GONE);
            triggerGalleryIntent();
          }
        });
    imageDialog.setNegativeButton(localizables.getString(LEAVEFEEDBACK_UPLOAD_BUTTON_REMOVE),
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            mScreensList.remove(1);
            mIvScreenshotTwo.setVisibility(View.GONE);
            mBtnScreenshot.setVisibility(View.VISIBLE);
          }
        });
    imageDialog.show();
  }

  @Override protected void initOneCMSText(View view) {
    mTvAppRateHelpImproveTitle.setText(localizables.getString(LEAVEFEEDBACK_GIVE_FEEDBACK_TITLE));
    mTvAppRateHelpImproveText.setText(localizables.getString(LEAVEFEEDBACK_GIVE_FEEDBACK_SUBTITLE));
    mBtnHelpImprove.setText(localizables.getString(LEAVEFEEDBACK_SEND_FEEDBACK_BUTTON));
    mEtAppRateHelpImproveComment.setHint(localizables.getString(LEAVEFEEDBACK_HELP_PLACEHOLDER));
    mTvAppRateHelpImproveSubtitle.setText(localizables.getString(LEAVEFEEDBACK_HELP_TITLE));
    mBtnScreenshot.setText(localizables.getString(HELPCENTER_UPLOAD_SCREENSHOT_BUTTON));

    if (mTilAppRateHelpImproveEmail.getEditText() != null) {
      mTilAppRateHelpImproveEmail.getEditText().setHint(localizables.getString(HINT_EMAIL));
    }
  }

  protected void showProgress() {
    mDialogHelper.showDialog(localizables.getString(LEAVEFEEDBACK_WAITING_SENDING));
  }

  @Override public void hideProgress() {
    mDialogHelper.hideDialog();
  }

  @Override public void showSuccessFeedback() {
    mDialogHelper.showDoneDialog(getActivity(),
        localizables.getString(LEAVEFEEDBACK_SEND_FEEDBACK_OK), R.drawable.ok_process,
        new DialogHelper.ProcessDoneListener() {
          @Override public void onDismiss() {
            mPresenter.navigateToHome();
          }
        });
  }

  @Override public void showErrorDialog() {
    mDialogHelper.showErrorDialog(getActivity(), localizables.getString(HELPCENTER_SENDING_ERROR));
  }

  @Override public void setEmail(String email) {
    if (mTilAppRateHelpImproveEmail.getEditText() != null) {
      mTilAppRateHelpImproveEmail.getEditText().setText(email);
    }
  }
}
