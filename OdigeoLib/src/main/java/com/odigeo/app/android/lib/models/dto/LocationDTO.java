package com.odigeo.app.android.lib.models.dto;

import android.text.TextUtils;
import com.odigeo.app.android.lib.config.Configuration;
import java.io.Serializable;

/**
 * @author miguel
 */
public class LocationDTO implements Serializable, Comparable<LocationDTO> {

  protected int geoNodeId;
  protected String cityName;
  protected String name;
  protected String type;
  protected String iataCode;
  protected String cityIataCode;
  protected String countryCode;
  protected String countryName;
  protected String timeZone;

  public LocationDTO() {
  }

  public LocationDTO(String iataCode) {
    this.iataCode = iataCode;
  }

  public LocationDTO(String name, String iataCode) {
    this.name = name;
    this.iataCode = iataCode;
  }

  /**
   * Gets the value of the geoNodeId property.
   */
  public int getGeoNodeId() {
    return geoNodeId;
  }

  /**
   * Sets the value of the geoNodeId property.
   */
  public void setGeoNodeId(int value) {
    this.geoNodeId = value;
  }

  /**
   * Gets the value of the cityName property.
   *
   * @return possible object is {@link String }
   */
  public String getCityName() {
    return cityName;
  }

  /**
   * Sets the value of the cityName property.
   *
   * @param value allowed object is {@link String }
   */
  public void setCityName(String value) {
    this.cityName = value;
  }

  /**
   * Gets the value of the name property in the correct case.
   *
   * @return possible object is {@link String }
   */
  public String getName() {
    if (TextUtils.isEmpty(name) || name.length() < 2) {
      return name;
    } else {
      return (String.valueOf(name.charAt(0))).toUpperCase(Configuration.getCurrentLocale())
          + name.substring(1);
    }
  }

  /**
   * Sets the value of the name property.
   *
   * @param value allowed object is {@link String }
   */
  public void setName(String value) {
    this.name = value;
  }

  /**
   * Gets the value of the type property.
   *
   * @return possible object is {@link String }
   */
  public String getType() {
    return type;
  }

  /**
   * Sets the value of the type property.
   *
   * @param value allowed object is {@link String }
   */
  public void setType(String value) {
    this.type = value;
  }

  /**
   * Gets the value of the iataCode property.
   *
   * @return possible object is {@link String }
   */
  public String getIataCode() {
    return iataCode;
  }

  /**
   * Sets the value of the iataCode property.
   *
   * @param value allowed object is {@link String }
   */
  public void setIataCode(String value) {
    this.iataCode = value;
  }

  /**
   * Gets the value of the cityIataCode property.
   *
   * @return possible object is {@link String }
   */
  public String getCityIataCode() {
    return cityIataCode;
  }

  /**
   * Sets the value of the cityIataCode property.
   *
   * @param value allowed object is {@link String }
   */
  public void setCityIataCode(String value) {
    this.cityIataCode = value;
  }

  /**
   * Gets the value of the countryCode property.
   *
   * @return possible object is {@link String }
   */
  public String getCountryCode() {
    return countryCode;
  }

  /**
   * Sets the value of the countryCode property.
   *
   * @param value allowed object is {@link String }
   */
  public void setCountryCode(String value) {
    this.countryCode = value;
  }

  /**
   * Gets the value of the countryName property.
   *
   * @return possible object is {@link String }
   */
  public String getCountryName() {
    return countryName;
  }

  /**
   * Sets the value of the countryName property.
   *
   * @param value allowed object is {@link String }
   */
  public void setCountryName(String value) {
    this.countryName = value;
  }

  /**
   * @return the timeZone
   */
  public String getTimezone() {
    return timeZone;
  }

  /**
   * @param timezone the timeZone to set
   */
  public void setTimezone(String timezone) {
    this.timeZone = timezone;
  }

  public boolean equals(Object another) {
    if (!(another instanceof LocationDTO)) {
      return false;
    }

    return iataCode != null && iataCode.equals(((LocationDTO) another).getIataCode());
  }

  @Override public int compareTo(LocationDTO another) {
    if (another == null) {
      return 1;
    }

    return name.compareTo(another.getName());
  }
}
