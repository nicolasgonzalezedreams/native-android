package com.odigeo.app.android.lib.models.dto;

import android.support.annotation.Nullable;
import com.odigeo.data.entity.BaseSpinnerItem;
import java.io.Serializable;

/**
 * <p>Java class for ResidentGroupLocality complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="ResidentGroupLocality">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class ResidentGroupLocalityDTO implements Serializable, BaseSpinnerItem {

  protected String code;
  protected String name;

  public ResidentGroupLocalityDTO(String code, String name) {
    this.code = code;
    this.name = name;
  }

  /**
   * Gets the value of the code property.
   *
   * @return possible object is {@link String }
   */
  public String getCode() {
    return code;
  }

  /**
   * Sets the value of the code property.
   *
   * @param value allowed object is {@link String }
   */
  public void setCode(String value) {
    this.code = value;
  }

  /**
   * Gets the value of the name property.
   *
   * @return possible object is {@link String }
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the value of the name property.
   *
   * @param value allowed object is {@link String }
   */
  public void setName(String value) {
    this.name = value;
  }

  @Override public String getShownTextKey() {
    return EMPTY_STRING;
  }

  @Override public int getImageId() {
    return EMPTY_RESOURCE;
  }

  @Nullable @Override public String getShownText() {
    return name;
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public Object getData() {
    return null;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }
}
