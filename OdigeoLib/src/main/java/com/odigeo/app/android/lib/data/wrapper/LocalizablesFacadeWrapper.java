package com.odigeo.app.android.lib.data.wrapper;

import android.content.Context;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.data.localizable.LocalizableProvider;

public class LocalizablesFacadeWrapper implements LocalizableProvider {

  private Context context;

  public LocalizablesFacadeWrapper(Context context) {

    this.context = context;
  }

  public String getString(String key) {

    return LocalizablesFacade.getString(context, key).toString();
  }

  @Override public String getString(String key, String... params) {

    return LocalizablesFacade.getString(context, key, params).toString();
  }

  public String getRawString(String key) {
    return LocalizablesFacade.getRawString(context, key);
  }
}
