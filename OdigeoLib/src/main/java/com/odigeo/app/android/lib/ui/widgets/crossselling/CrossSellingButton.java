package com.odigeo.app.android.lib.ui.widgets.crossselling;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Spannable;
import android.util.AttributeSet;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;
import com.odigeo.app.android.lib.consts.HomeOptionsButtons;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.ButtonMenuHomeWidget;
import com.odigeo.app.android.lib.utils.StyledBoldString;
import java.lang.ref.WeakReference;

/**
 * Button with styles to be used in the cross selling widget.
 */
public class CrossSellingButton extends ButtonMenuHomeWidget {

  public CrossSellingButton(Context context, HomeOptionsButtons optionsButton) {
    super(context, R.layout.layout_cross_selling_button, optionsButton);
    this.titleTextView = (TextView) findViewById(R.id.txtTitle);
    Fonts fonts = Configuration.getInstance().getFonts();
    this.titleTextView.setTypeface(fonts.getRegular());
  }

  public CrossSellingButton(Context context, AttributeSet attrs) {
    super(context, attrs, R.layout.layout_cross_selling_button);
    this.titleTextView = (TextView) findViewById(R.id.txtTitle);
    Fonts fonts = Configuration.getInstance().getFonts();
    this.titleTextView.setTypeface(fonts.getRegular());
    setAttributes(attrs);
  }

  private void setAttributes(AttributeSet attributes) {
    TypedArray array =
        getContext().obtainStyledAttributes(attributes, R.styleable.CrossWidgetButton);
    String crossSellingButton = array.getString(R.styleable.CrossWidgetButton_crossSellingTitle);
    int idResourceImage =
        array.getResourceId(R.styleable.CrossWidgetButton_iconResourceImage, R.drawable.icon);
    String boldString = crossSellingButton != null ? LocalizablesFacade.getRawString(getContext(),
        crossSellingButton) : "";

    Spannable spannable = new StyledBoldString(new WeakReference<>(getContext()), boldString,
        R.style.crossSellingButtonBoldText).getSpannable();

    this.titleTextView.setText(spannable, TextView.BufferType.SPANNABLE);
    this.iconImageView.setImageResource(idResourceImage);
  }
}
