package com.odigeo.app.android.lib.config;

/**
 * Created by manuel on 11/12/14.
 */
public class LocaleToMarket {
  private String locale;
  private String marketId;

  public LocaleToMarket(String locale, String marketId) {
    this.locale = locale;
    this.marketId = marketId;
  }

  public final String getLocale() {
    return locale;
  }

  public final void setLocale(String locale) {
    this.locale = locale;
  }

  public final String getMarketId() {
    return marketId;
  }

  public final void setMarketId(String marketId) {
    this.marketId = marketId;
  }
}
