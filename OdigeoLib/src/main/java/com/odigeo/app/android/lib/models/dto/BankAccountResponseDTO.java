package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class BankAccountResponseDTO implements Serializable {

  protected String bankName;
  protected String iban;
  protected String swift;

  /**
   * Gets the value of the bankName property.
   *
   * @return possible object is {@link String }
   */
  public String getBankName() {
    return bankName;
  }

  /**
   * Sets the value of the bankName property.
   *
   * @param value allowed object is {@link String }
   */
  public void setBankName(String value) {
    this.bankName = value;
  }

  /**
   * Gets the value of the iban property.
   *
   * @return possible object is {@link String }
   */
  public String getIban() {
    return iban;
  }

  /**
   * Sets the value of the iban property.
   *
   * @param value allowed object is {@link String }
   */
  public void setIban(String value) {
    this.iban = value;
  }

  /**
   * Gets the value of the swift property.
   *
   * @return possible object is {@link String }
   */
  public String getSwift() {
    return swift;
  }

  /**
   * Sets the value of the swift property.
   *
   * @param value allowed object is {@link String }
   */
  public void setSwift(String value) {
    this.swift = value;
  }
}
