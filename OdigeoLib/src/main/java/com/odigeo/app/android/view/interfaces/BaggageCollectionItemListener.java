package com.odigeo.app.android.view.interfaces;

public interface BaggageCollectionItemListener {

  void onBaggageSelectionItemChange();
}
