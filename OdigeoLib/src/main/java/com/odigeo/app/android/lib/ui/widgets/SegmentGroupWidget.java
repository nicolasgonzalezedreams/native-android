package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.interfaces.ISegmentGroupWidget;
import com.odigeo.app.android.lib.interfaces.ISegmentWidget;
import com.odigeo.app.android.lib.models.SegmentGroup;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.LocationDTO;
import com.odigeo.app.android.lib.models.dto.SectionDTO;
import com.odigeo.app.android.lib.models.dto.SegmentDTO;
import com.odigeo.app.android.lib.ui.widgets.base.PressableWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.TravelType;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Irving Lóp on 08/09/2014.
 */
public class SegmentGroupWidget extends LinearLayout implements ISegmentWidget {

  private static final int MAX_SEATS = 9;
  private static boolean isLockContinueButton;

  private final Context context;
  private final LinearLayout rootView;
  private final LinearLayout containerOptionsTravels;
  private final PressableWidget containerPressableWidget;
  private final ImageView airlineIcon;
  private final TextView airlineName;
  private final TextView txtCardInformation;
  private final Button mBtnAccept;

  private final List<SegmentWidget> segmentWidgets = new ArrayList<SegmentWidget>();
  private final SegmentGroup segmentGroup;
  private final TravelType travelType;
  private ISegmentGroupWidget listener;
  private SegmentWidget segmentWidgetSelected;

  private int mItineraryResultParentIndex;

  private OdigeoImageLoader imageLoader;

  public SegmentGroupWidget(Context context, SegmentGroup segmentGroup, TravelType travelType,
      int itineraryResultParentIndex) {
    super(context);

    this.context = context;
    this.segmentGroup = segmentGroup;
    this.travelType = travelType;
    mItineraryResultParentIndex = itineraryResultParentIndex;

    imageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();

    rootView = (LinearLayout) LayoutInflater.from(context)
        .inflate(R.layout.layout_resultswidget_section, this);
    TextView textHeader = (TextView) rootView.findViewById(R.id.text_header_section_resultWidget);
    this.airlineName = (TextView) rootView.findViewById(R.id.airline_name);
    this.airlineIcon = (ImageView) rootView.findViewById(R.id.airline_icon);
    this.containerOptionsTravels = (LinearLayout) findViewById(R.id.layout_travels);
    this.containerPressableWidget = (PressableWidget) findViewById(R.id.container_pressable_widget);
    txtCardInformation = (TextView) rootView.findViewById(R.id.txtCardInformation);

    Fonts fonts = Configuration.getInstance().getFonts();
    textHeader.setTypeface(fonts.getBold());
    airlineName.setTypeface(fonts.getRegular());

    textHeader.setText(getTitleHeader());

    try {
      setAirlineName(segmentGroup.getCarrier().getName());
      setAirlineIcon(segmentGroup.getCarrier().getCode());
    } catch (Exception e) {
      Log.e(Constants.TAG_LOG, String.format("Hubo NullPointerException: SegmentGroup[%s]",
          segmentGroup == null ? "null" : "existente"));

      if (segmentGroup != null) {
        Log.e(Constants.TAG_LOG,
            String.format("Hubo NullPointerException: SegmentGroup.getCarrier()[%s]",
                segmentGroup.getCarrier() == null ? "null" : "existente"));
      }
    }

    mBtnAccept = (Button) findViewById(R.id.button_standard_accept);

    mBtnAccept.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.COMMON_BUTTONCONTINUE).toString());
    mBtnAccept.setTypeface(Configuration.getInstance().getFonts().getBold());
    mBtnAccept.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        if (!isLockContinueButton) {
          if (listener != null) {
            listener.onItinerarySelect(mItineraryResultParentIndex);
          }

          String category = GAnalyticsNames.CATEGORY_FLIGHTS_RESULTS;
          String action = GAnalyticsNames.ACTION_RESULTS_LIST;
          String label = GAnalyticsNames.LABEL_PARTIAL_SELECT_RESULT + mItineraryResultParentIndex;

          BusProvider.getInstance().post(new GATrackingEvent(category, action, label));

          AndroidDependencyInjector.getInstance()
              .provideTrackerController()
              .trackLocalyticsEvent(TrackerConstants.CLICKED_CONTINUE_RESULTS);
        }
      }
    });
  }

  public static void unlockContinueButton() {
    isLockContinueButton = false;
  }

  public final void setListener(ISegmentGroupWidget listener) {
    this.listener = listener;
  }

  public final void drawSegments() {
    int i = 0;

    for (SegmentWrapper segmentWrapper : segmentGroup.getSegmentWrappers()) {
      SegmentWidget segmentWidget = createSegmentWidget(segmentWrapper);

      if (++i == segmentGroup.getSegmentWrappers().size()) {
        segmentWidget.setLastInForm(true);
      } else {
        segmentWidget.setLastInForm(false);
      }

      segmentWidgets.add(segmentWidget);
      containerOptionsTravels.addView(segmentWidget);
    }
  }

  private String getTitleHeader() {
    if (travelType == TravelType.SIMPLE) {
      return CommonLocalizables.getInstance().getCommonOutbound(getContext()).toString();
    } else if (travelType == TravelType.ROUND) {
      if (segmentGroup.getSegmentGroupIndex() == 0) {
        return CommonLocalizables.getInstance().getCommonOutbound(getContext()).toString();
      } else {
        return CommonLocalizables.getInstance().getCommonInbound(getContext()).toString();
      }
    }
    return String.format("%s %s", CommonLocalizables.getInstance().getCommonLeg(getContext()),
        segmentGroup.getSegmentGroupIndex() + 1);
  }

  public final List<SegmentWidget> getSegmentWidgets() {
    return this.segmentWidgets;
  }

  private void setAirlineName(String airlineNameText) {
    if (airlineNameText != null) {
      airlineName.setText(airlineNameText);
    }
  }

  private void setAirlineIcon(String airlineCarrier) {
    if (airlineCarrier != null) {
      String url = ViewUtils.getCarrierLogo(airlineCarrier);
      imageLoader.load(airlineIcon, url, R.drawable.placeholder_airline);
    }
  }

  private SegmentWidget createSegmentWidget(SegmentWrapper segmentWrapper) {

    SegmentWidget itemWidget = new SegmentWidget(context, segmentWrapper);
    itemWidget.setListener(this);

    SegmentDTO segment = segmentWrapper.getSegment();
    SectionDTO departure = segmentWrapper.getSectionsObjects().get(0);
    SectionDTO arrival =
        segmentWrapper.getSectionsObjects().get(segmentWrapper.getSectionsObjects().size() - 1);

    LocationDTO departureLocation = departure.getLocationFrom();
    LocationDTO arrivalLocation = arrival.getLocationTo();

    int differenceDays =
        OdigeoDateUtils.getDifferenceDays(departure.getDepartureDate(), arrival.getArrivalDate());

    itemWidget.setDepartureTime(OdigeoDateUtils.createDate(departure.getDepartureDate()));
    itemWidget.setArrivalTime(OdigeoDateUtils.createDate(arrival.getArrivalDate()), differenceDays);
    itemWidget.setDeparturePlace(departureLocation);
    itemWidget.setArrivalPlace(arrivalLocation);
    itemWidget.setTimeTravel(segment.getDuration().intValue());
    itemWidget.setOperatingCarrier(
        segmentWrapper.getSectionsObjects().get(0).getOperatingCarrierObject());
    if (segmentGroup.getBaggageIncluded() != null) {
      itemWidget.setBaggageAllowanceInfo(segmentGroup.getBaggageIncluded());
    }

    //Show the seats number only if there are less than 10
    if (segment.getSeats() != null && segment.getSeats() < MAX_SEATS) {
      itemWidget.setTextNumberSeats(segment.getSeats().toString());
    }

    int numScales = segment.getSections().size();
    if (numScales > 1) {
      itemWidget.setScales(numScales - 1);
      if (numScales == 2) {
        itemWidget.setExtraLines(departure.getLocationTo().getCityName(),
            segmentWrapper.getSectionsObjects().get(1).getDepartureDate()
                - departure.getArrivalDate());
      }
    }
    return itemWidget;
  }

  @Override public final void onSegmentSelected(SegmentWidget segmentWidget) {
    this.segmentWidgetSelected = segmentWidget;

    //Hide all the segments widgets of this segment group
    for (SegmentWidget segmentWidgetOther : this.getSegmentWidgets()) {
      segmentWidgetOther.setVisibility(GONE);
      segmentWidgetOther.setLastInForm(false);
    }

    //Show only the segment widget selected
    segmentWidget.setVisibility(VISIBLE);
    segmentWidget.setLastInForm(true);

    if (listener != null) {
      listener.onSegmentSelected(this);
    }
  }

  @Override public final void onSegmentUnselected(SegmentWidget segmentWidget) {
    deselect();

    if (listener != null) {
      listener.onSegmentGroupUnselected(this);
    }
  }

  /**
   * Deselect all the segments in the segment group.
   */
  public final void deselect() {
    hideButton();

    hideSegmentWidgetSelected();

    showAllSegmentsForGroup();
  }

  /**
   * Show all the segments widgets of this segment group.
   */
  private void showAllSegmentsForGroup() {
    int i = 0;
    for (SegmentWidget segmentWidgetOther : this.getSegmentWidgets()) {
      segmentWidgetOther.setChecked(false);
      segmentWidgetOther.setVisibility(VISIBLE);

      if (++i == this.getSegmentWidgets().size()) {
        segmentWidgetOther.setLastInForm(true);
      } else {
        segmentWidgetOther.setLastInForm(false);
      }
    }
  }

  /**
   * Hide the widget that was selected, because it could appear twice
   */
  private void hideSegmentWidgetSelected() {
    if (getSegmentWidgetSelected() != null) {
      getSegmentWidgetSelected().setVisibility(GONE);
    }
  }

  public final void hideButton() {
    mBtnAccept.setVisibility(GONE);
  }

  public final void showButton() {
    mBtnAccept.setVisibility(VISIBLE);
  }

  public final void drawSeparatorGroup() {
    LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
        context.getResources().getDimensionPixelOffset(R.dimen.size_separator_expandable_list));
    findViewById(R.id.layout_separatorgroup).setLayoutParams(params);
  }

  public final boolean isSelected() {
    SegmentWidget segmentWidgetSelected = getSegmentWidgetSelected();
    return segmentWidgetSelected != null && segmentWidgetSelected.isChecked();
  }

  public final int getItineraryResultParentIndex() {
    return mItineraryResultParentIndex;
  }

  public final void setItineraryResultParentIndex(int itineraryResultParentIndex) {
    mItineraryResultParentIndex = itineraryResultParentIndex;
  }

  public final SegmentWidget getSegmentWidgetSelected() {
    return this.segmentWidgetSelected;
  }

  public final void setSegmentWidgetSelected(SegmentWidget segmentWidget) {
    this.segmentWidgetSelected = segmentWidget;

    if (!segmentWidget.isChecked()) {
      segmentWidget.setChecked(true);
    }

    segmentWidget.setLastInForm(true);
  }

  public final void setLastInForm(boolean value) {
    containerPressableWidget.setLastInForm(true);
  }

  public final void setCardInformation(String strText, boolean isFullTransparency) {

    if (isFullTransparency) {
      hideCardInformation();
    } else {
      txtCardInformation.setTypeface(Configuration.getInstance().getFonts().getRegular());
      txtCardInformation.setText(strText);
      hideCardInformation();
      showCardInformation();
    }
  }

  public final void showCardInformation() {
    String strText = txtCardInformation.getText().toString();

    if (!TextUtils.isEmpty(strText)) {
      findViewById(R.id.layout_card_info).setVisibility(VISIBLE);
    }
  }

  public final void hideCardInformation() {
    findViewById(R.id.layout_card_info).setVisibility(GONE);
  }
}
