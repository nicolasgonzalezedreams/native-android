package com.odigeo.app.android.navigator;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MenuItem;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.BlockedAccountView;
import com.odigeo.app.android.view.FacebookAlreadyRegisteredView;
import com.odigeo.app.android.view.GoogleAlreadyRegisteredView;
import com.odigeo.app.android.view.LegacyUserView;
import com.odigeo.app.android.view.LoginView;
import com.odigeo.app.android.view.UserAlreadyRegisteredView;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.PermissionsHelper;
import com.odigeo.data.net.controllers.UserNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.controllers.GooglePlusController;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.exceptions.SocialLoginTimeOutException;
import com.odigeo.presenter.contracts.navigators.LoginNavigatorInterface;

import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_SSO_LOGIN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_SSO_LOGIN_FLIGHTS_PAX_PAGE;

public class LoginNavigator extends BaseNavigator implements LoginNavigatorInterface {

  private static final String USER_EMAIL = "USER_EMAIL";
  private LoginView mLoginFragment;
  private GoogleAlreadyRegisteredView mGoogleAlreadyRegisteredFragment;
  private FacebookAlreadyRegisteredView mFacebookAlreadyRegisteredFragment;
  private BlockedAccountView mBlockedAccountFragment;
  private LegacyUserView mLegacyUserFragment;
  private UserAlreadyRegisteredView mUserAlreadyRegisteredView;
  private boolean mIsComingFromPassenger = false;
  private boolean isComingFromMyTrips = false;
  private OdigeoApp odigeoApp;

  public static Intent startIntent(Context context) {
    return new Intent(context, LoginNavigator.class);
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view);
    String email = getEmailAlreadyRegistered();
    showLogIn(email);
    if (getCallingActivity() != null) {
      mIsComingFromPassenger =
          getCallingActivity().getClassName().equals(PassengerNavigator.class.getName());
      isComingFromMyTrips =
          getCallingActivity().getClassName().equals(MyTripsNavigator.class.getName());
    }
    odigeoApp = (OdigeoApp) getApplication();
  }

  @Override public void onBackPressed() {
    navigateBack();
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      // Respond to the action bar's Up/Home button
      case android.R.id.home:
        navigateBack();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void navigateBack() {

    Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
    if (currentFragment != null) {
      if (!currentFragment.getClass().equals(LoginView.class)) {
        replaceFragment(R.id.fl_container, getLoginFragment());
        setTitle(LocalizablesFacade.getString(this, OneCMSKeys.SSO_HEADER_LOGIN).toString());
        setUpToolbarButton(0);
      } else {
        goBack();
      }
    } else {
      goBack();
    }
  }

  private void goBack() {
    if (isTaskRoot()) {
      goBackCommingFromWalkThrough();
    } else {
      goBackCommingFromJoinUs();
    }
  }

  private void goBackCommingFromJoinUs() {
    super.onBackPressed();
  }

  private void goBackCommingFromWalkThrough() {
    Intent intent = new Intent(this, ((OdigeoApp) getApplication()).getHomeActivity());
    startActivity(intent);
    finish();
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    if (requestCode == GooglePlusController.REQUEST_RESOLVE_ERROR) {
      Fragment f = getSupportFragmentManager().findFragmentById(R.id.fl_container);
      f.onActivityResult(requestCode, resultCode, data);
    } else {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }

  @Override public void navigateToRequestForgottenPassword(String email) {
    Intent intent = new Intent(this, RequestForgottenPasswordNavigator.class);
    intent.putExtra(USER_EMAIL, email);
    startActivity(intent);
    finish();
  }

  @Override public void showBlockedAccountError(String email) {
    // Send the recover password to the legacy email.
    requestEmailForLegacyAndBlockedUser(email);
    BlockedAccountView view = getBlockedAccountFragment(email);
    replaceFragment(R.id.fl_container, view);
    setNavigationTitle(
        LocalizablesFacade.getString(this, OneCMSKeys.SSO_PASSWORDRECOVERY_HEADER).toString());
  }

  @Override public void showLegacyUserError(String email) {
    // Send the recover password to the legacy email.
    requestEmailForLegacyAndBlockedUser(email);
    LegacyUserView view = getLegacyUserFragment(email);
    replaceFragment(R.id.fl_container, view);
    setNavigationTitle(
        LocalizablesFacade.getString(this, OneCMSKeys.SSO_LEGACY_USER_HEADER).toString());
  }

  @Override public void showLoginView() {
    replaceFragment(R.id.fl_container, getLoginFragment());
    setNavigationTitle(LocalizablesFacade.getString(this, OneCMSKeys.SSO_HEADER_LOGIN).toString());
  }

  @Override public void showLoginViewUserAlreadyRegistered(String email) {
    UserAlreadyRegisteredView view = getUserAlreadyRegisteredView(email);
    replaceFragment(R.id.fl_container, view);
    setTitle(LocalizablesFacade.getString(this, OneCMSKeys.SSO_EXISTING_MAIL_TITLE));
    setUpToolbarButton(R.drawable.ic_clear);
    tracker.trackAnalyticsEvent(getSSOTrackingCategory(), TrackerConstants.ACTION_SIGN_IN,
        TrackerConstants.LABEL_SSO_EMAIL_ALREADY_USED_ODIGEO_ERROR);
  }

  @Override public void showUserRegisteredFacebook(String email) {
    Bundle bundle = new Bundle();
    bundle.putString("parentClass", "Login");
    FacebookAlreadyRegisteredView view = getFacebookAlreadyRegisteredFragment(email);
    replaceFragment(R.id.fl_container, view);
    setNavigationTitle(
        LocalizablesFacade.getString(this, OneCMSKeys.SSO_ERROR_ALREADY_USED).toString());
    tracker.trackAnalyticsEvent(getSSOTrackingCategory(), TrackerConstants.ACTION_SIGN_IN,
        TrackerConstants.LABEL_SSO_EMAIL_ALREADY_USED_FACEBOOK_ERROR);
  }

  @Override public void showUserRegisteredGoogle(String email) {
    Bundle bundle = new Bundle();
    bundle.putString("parentClass", "Login");
    GoogleAlreadyRegisteredView view = getGoogleAlreadyRegisteredFragment(email);
    replaceFragment(R.id.fl_container, view);
    setNavigationTitle(
        LocalizablesFacade.getString(this, OneCMSKeys.SSO_ERROR_ALREADY_USED).toString());
    tracker.trackAnalyticsEvent(getSSOTrackingCategory(), TrackerConstants.ACTION_SIGN_IN,
        TrackerConstants.LABEL_SSO_EMAIL_ALREADY_USED_GOOGLE_ERROR);
  }

  @Override public String getSSOTrackingCategory() {
    return mIsComingFromPassenger ? CATEGORY_SSO_LOGIN_FLIGHTS_PAX_PAGE : CATEGORY_SSO_LOGIN;
  }

  @Override public void navigateToHome() {
    Intent intent =
        new Intent(LoginNavigator.this, ((OdigeoApp) getApplication()).getHomeActivity());
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  @Override public void navigateToLogin() {

  }

  @Override public void onTimeOutError() {
    odigeoApp.trackNonFatal(new SocialLoginTimeOutException());
  }

  /**
   * Get login fragment instance
   *
   * @return {@link LoginView}
   */
  private LoginView getLoginFragment() {
    if (mLoginFragment == null) {
      mLoginFragment = LoginView.newInstance();
    }

    return mLoginFragment;
  }

  /**
   * Get {@link UserAlreadyRegisteredView}.
   *
   * @return {@link UserAlreadyRegisteredView}
   */
  private UserAlreadyRegisteredView getUserAlreadyRegisteredView(String email) {
    if (mUserAlreadyRegisteredView == null) {
      mUserAlreadyRegisteredView = UserAlreadyRegisteredView.newInstance(email);
    }
    return mUserAlreadyRegisteredView;
  }

  /**
   * Get Google Already Registered fragment instance
   *
   * @return {@link GoogleAlreadyRegisteredView}
   */
  private GoogleAlreadyRegisteredView getGoogleAlreadyRegisteredFragment(String email) {
    if (mGoogleAlreadyRegisteredFragment == null) {
      mGoogleAlreadyRegisteredFragment = GoogleAlreadyRegisteredView.newInstance(email);
    }

    return mGoogleAlreadyRegisteredFragment;
  }

  /**
   * Get Blocked Account fragment instance
   *
   * @return {@link BlockedAccountView}
   */
  private BlockedAccountView getBlockedAccountFragment(String email) {
    if (mBlockedAccountFragment == null) {
      mBlockedAccountFragment = BlockedAccountView.newInstance(email);
    } else {
      mBlockedAccountFragment.updateInfo(email);
    }

    return mBlockedAccountFragment;
  }

  /**
   * Get Legacy User fragment instance
   *
   * @return {@link LegacyUserView}
   */
  private LegacyUserView getLegacyUserFragment(String email) {
    if (mLegacyUserFragment == null) {
      mLegacyUserFragment = LegacyUserView.newInstance(email);
    }

    return mLegacyUserFragment;
  }

  /**
   * Get Facebook Already Registered fragment instance
   *
   * @return {@link FacebookAlreadyRegisteredView}
   */
  private FacebookAlreadyRegisteredView getFacebookAlreadyRegisteredFragment(String email) {
    if (mFacebookAlreadyRegisteredFragment == null) {
      mFacebookAlreadyRegisteredFragment = FacebookAlreadyRegisteredView.newInstance(email);
    }

    return mFacebookAlreadyRegisteredFragment;
  }

  private String getEmailAlreadyRegistered() {
    Bundle bundle = getIntent().getExtras();
    if (bundle != null && bundle.containsKey(UserAlreadyRegisteredView.USER_EMAIL)) {
      return bundle.getString(UserAlreadyRegisteredView.USER_EMAIL);
    }
    return null;
  }

  private void showLogIn(String email) {
    if (email == null) {
      showLoginView();
    } else {
      showLoginViewUserAlreadyRegistered(email);
    }
  }

  /**
   * Launch the request to recover the password for a legacy user or a blocked user.
   *
   * @param email Email address where will be send the email for password recover. Can't be null.
   */
  private void requestEmailForLegacyAndBlockedUser(final String email) {
    UserNetControllerInterface userNetControllerInterface =
        AndroidDependencyInjector.getInstance().provideUserNetController();
    userNetControllerInterface.requestForgottenPassword(new OnRequestDataListener<Boolean>() {
      @Override public void onResponse(Boolean object) {
        // Nothing
        Log.e(Constants.TAG_LOG, object.toString());
      }

      @Override public void onError(MslError error, String message) {
        // Nothing
        Log.e(Constants.TAG_LOG, error.toString());
      }
    }, email);
  }

  public void openMailApp() {
    Intent intent = new Intent(Intent.ACTION_MAIN);
    intent.addCategory(Intent.CATEGORY_APP_EMAIL);
    startActivity(intent);
  }

  @Override public void navigateToPendingEmailScreen() {
    Intent intent = new Intent(Intent.ACTION_MAIN);
    intent.addCategory(Intent.CATEGORY_APP_EMAIL);
    startActivity(intent);
    startActivity(Intent.createChooser(intent,
        LocalizablesFacade.getString(this, OneCMSKeys.SSO_GO_TO_MAIL)));
  }

  @Override public void returnToPassengerWithLoginSuccess() {
    mIsComingFromPassenger = false;
    setResult(RESULT_OK);
    finish();
  }

  @Override public boolean isComingFromPassenger() {
    return mIsComingFromPassenger;
  }

  @Override public boolean isComingFromMyTrips() {
    return isComingFromMyTrips;
  }

  @Override public void returnToMyTripsWithLoginSuccess() {
    isComingFromMyTrips = false;
    setResult(RESULT_OK);
    finish();
  }

  @Override public void onRequestPermissionsResult(int requestCode, String[] permissions,
      int[] grantResults) {
    switch (requestCode) {
      case PermissionsHelper.ACCOUNTS_FACEBOOK_REQUEST_CODE:
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT,
              TrackerConstants.LABEL_ACCOUNT_ACCESS_ALLOW);
          mLoginFragment.facebookLogin();
        } else {
          tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT,
              TrackerConstants.LABEL_ACCOUNT_ACCESS_DENY);
        }
        break;

      case PermissionsHelper.ACCOUNTS_GOOGLE_REQUEST_CODE:
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT,
              TrackerConstants.LABEL_ACCOUNT_ACCESS_ALLOW);
          mLoginFragment.googlePlusLogin();
        } else {
          tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT,
              TrackerConstants.LABEL_ACCOUNT_ACCESS_DENY);
        }
        break;
    }
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }
}
