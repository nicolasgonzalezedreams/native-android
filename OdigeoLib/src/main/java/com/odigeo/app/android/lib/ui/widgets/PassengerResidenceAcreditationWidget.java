package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.shoppingCart.ResidentValidationType;
import com.odigeo.data.entity.shoppingCart.ResidentsValidation;
import com.odigeo.data.entity.shoppingCart.SectionResult;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.data.entity.shoppingCart.TravellerResidentValidation;
import java.util.List;

/**
 * Created by Oscar Álvarez on 21/01/2015.
 */
public class PassengerResidenceAcreditationWidget extends LinearLayout {

  private LinearLayout listInbound;
  private LinearLayout listOutbound;
  private LinearLayout descriptionAlertLayout;
  private CustomTextView descriptionAlert;
  private List<SegmentWrapper> mSegmentWrappers;
  private SearchOptions mSearchOptions;
  private ResidentsValidation mResidentsValidation;
  private List<SectionResult> mSectionResults;
  private List<Traveller> mTravellers;

  public PassengerResidenceAcreditationWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
    initialize(context);
  }

  private void initialize(Context context) {
    inflate(context, R.layout.layout_passenger_residence_acreditation_widget, this);

    FormOdigeoLayout formResidenceAcreditationWidget =
        (FormOdigeoLayout) findViewById(R.id.formPassengersResumeAcreditationWidget);
    formResidenceAcreditationWidget.setTitle(
        LocalizablesFacade.getString(getContext(), "residentacreditationmodule_header_headertitle")
            .toString());
    listOutbound = (LinearLayout) findViewById(R.id.residence_acreditation_list_outbound);
    listInbound = (LinearLayout) findViewById(R.id.residence_acreditation_list_inbound);
    descriptionAlertLayout = (LinearLayout) findViewById(R.id.resident_validation_desc_alert);
    descriptionAlert = (CustomTextView) findViewById(R.id.text_resident_validation_desc_alert);
  }

  public void setShoppingCart(List<SegmentWrapper> segmentWrappers, SearchOptions searchOptions,
      ResidentsValidation residentsValidation, List<SectionResult> sectionResults,
      List<Traveller> travellers) {
    mSegmentWrappers = segmentWrappers;
    mSearchOptions = searchOptions;
    mResidentsValidation = residentsValidation;
    mSectionResults = sectionResults;
    mTravellers = travellers;
    setSubHeaders();
    setPassengers();
  }

  private void setSubHeaders() {
    if (mSearchOptions.getTravelType() == TravelType.ROUND) {
      PassengerResidenceAcreditationSubHeader subHeaderOutbound =
          (PassengerResidenceAcreditationSubHeader) findViewById(
              R.id.residence_acreditation_subheader_outbound);
      subHeaderOutbound.setVisibility(View.VISIBLE);
      subHeaderOutbound.setTitleType(
          LocalizablesFacade.getString(getContext(), "residentacreditationmodule_itinerary_to")
              .toString());
      subHeaderOutbound.setTitleTravelLine(
          String.format(getResources().getString(R.string.format_residence_header),
              mSearchOptions.getFlightSegments().get(0).getDepartureCity().getCityName(),
              mSearchOptions.getFlightSegments().get(0).getArrivalCity().getCityName(),
              mSegmentWrappers.get(0).getCarrier().getName()));

      PassengerResidenceAcreditationSubHeader subHeaderInbound =
          (PassengerResidenceAcreditationSubHeader) findViewById(
              R.id.residence_acreditation_subheader_inbound);
      subHeaderInbound.setVisibility(View.VISIBLE);
      subHeaderInbound.setTitleType(
          LocalizablesFacade.getString(getContext(), "residentacreditationmodule_itinerary_from")
              .toString());
      subHeaderInbound.setTitleTravelLine(
          String.format(getResources().getString(R.string.format_residence_header),
              mSearchOptions.getFlightSegments().get(1).getDepartureCity().getCityName(),
              mSearchOptions.getFlightSegments().get(1).getArrivalCity().getCityName(),
              mSegmentWrappers.get(1).getCarrier().getName()));
    }
  }

  private void setPassengers() {
    if (mResidentsValidation != null) {
      addAllPassengersToView();
    }
  }

  private void addAllPassengersToView() {
    boolean alertPendingVerificationReturned;
    boolean alertNotVerifiedReturned;
    boolean alertPendingVerificationSet = false;
    boolean alertNotVerifiedSet = false;
    int counterMsg = 0;
    String footnoteIndicator = "";
    List<TravellerResidentValidation> travellersResidentValidation =
        mResidentsValidation.getTravellersResidentValidation();

    for (int i = 0; i < travellersResidentValidation.size(); i++) {

      //condition for status negative of resident
      alertPendingVerificationReturned =
          getPassengerStatus(i, travellersResidentValidation) == ResidentValidationType.PENDING;
      alertNotVerifiedReturned =
          getPassengerStatus(i, travellersResidentValidation) == ResidentValidationType.KO;
      boolean lowCost = mSectionResults.get(i).getSection().isLcc();
      if (alertPendingVerificationReturned && !alertPendingVerificationSet) {
        descriptionAlertLayout.setVisibility(View.VISIBLE);
        alertPendingVerificationSet = true;
        footnoteIndicator = (counterMsg == 0 ? "*" : "**");
        descriptionAlert.setText(
            descriptionAlert.getText() + "\n" + footnoteIndicator + LocalizablesFacade.getString(
                getContext(), "residentacreditationmodule_pendingconditions_title").toString());
        counterMsg++;
      }
      if (alertNotVerifiedReturned && !alertNotVerifiedSet) {
        descriptionAlertLayout.setVisibility(View.VISIBLE);
        alertNotVerifiedSet = true;
        footnoteIndicator = (counterMsg == 0 ? "*" : "**");

        String auxMsg = getLowCostMsg(lowCost);

        descriptionAlert.setText(descriptionAlert.getText() + "\n" + footnoteIndicator + auxMsg);
        counterMsg++;
      }

      PassengerResidenceAcreditationRow passengerRow =
          new PassengerResidenceAcreditationRow(getContext());
      passengerRow.setPassenger(mTravellers.get(i),
          travellersResidentValidation.get(i).getStatus().getResidentValidationType(),
          footnoteIndicator);
      listOutbound.addView(passengerRow);
      if (mSearchOptions.getTravelType() == TravelType.ROUND) {
        passengerRow = new PassengerResidenceAcreditationRow(getContext());
        passengerRow.setPassenger(mTravellers.get(i),
            travellersResidentValidation.get(i).getStatus().getResidentValidationType(),
            footnoteIndicator);
        listInbound.addView(passengerRow);
      }
    }
  }

  private String getLowCostMsg(boolean lowCost) {
    String auxMsg;
    if (lowCost) {
      auxMsg = LocalizablesFacade.getString(getContext(),
          "residentacreditationmodule_conditionslcc_title").toString();
    } else {
      auxMsg = LocalizablesFacade.getString(getContext(),
          "residentacreditationmodule_conditionsgds_title").toString();
    }
    return auxMsg;
  }

  private ResidentValidationType getPassengerStatus(int i,
      List<TravellerResidentValidation> listTraveller) {
    //return status in function of validTraveller.getNumPassenger()
    for (TravellerResidentValidation trav : listTraveller) {
      if (trav.getNumPassenger().equals(String.valueOf(i + 1))) {
        return trav.getStatus().getResidentValidationType();
      }
    }
    return ResidentValidationType.KO;
  }
}