package com.odigeo.app.android.view.custom;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.interfaces.HistorySearchRowListener;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;

public class MultiHistorySearchRow extends DefaultHistorySearchRow {

  public MultiHistorySearchRow(Context context, SearchOptions search,
      HistorySearchRowListener listener) {
    super(context, search, listener);
  }

  public MultiHistorySearchRow(Context context, AttributeSet attributeSet) {
    super(context, attributeSet);
  }

  @Override public void drawHistorySearch() {
    SpannableStringBuilder ssb = new SpannableStringBuilder();
    boolean first = true;
    String arrivalCityName, departureCityName;

    for (FlightSegment segment : searchOptions.getFlightSegments()) {

      if (first) {
        first = false;
      } else {
        ssb.append("   •   ");
        int ssbLength = ssb.length();
        ssb.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.basic_light)),
            ssbLength - 7, ssbLength, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
      }

      departureCityName = getCityName(segment.getDepartureCity());
      arrivalCityName = getCityName(segment.getArrivalCity());

      ssb.append(departureCityName + "   ");
      ssb.setSpan(getArrowImageSpan(false), ssb.length() - 2, ssb.length() - 1,
          Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
      ssb.append(arrivalCityName);
      drawDate(OdigeoDateUtils.createDate(segment.getDate()));
    }

    mRowCities.setText(ssb, TextView.BufferType.SPANNABLE);
  }

  @Override public FlightSegment getCurrentFlightSegment() {
    return searchOptions.getLastSegment();
  }
}
