package com.odigeo.app.android.lib.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.CountriesAdapter;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Market;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListListener;
import com.odigeo.interactors.CountriesInteractor;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 2.0
 * @since 10/09/2014.
 */
public class OdigeoCountriesActivity extends OdigeoForegroundBaseActivity
    implements AdapterView.OnItemClickListener, OnRequestDataListListener<Country> {

  public static final int SELECTION_MODE_COUNTRY = 1;
  public static final int SELECTION_MODE_PHONE_PREFIX = 2;
  public static final int SELECTION_MODE_RESIDENCE = 3;
  public static final int SELECTION_MODE_IDENTIFICATION = 4;

  private final CountriesInteractor mCountriesInteractor =
      AndroidDependencyInjector.getInstance().provideCountriesInteractor();
  private CountriesAdapter mCountriesAdapter;
  private StickyListHeadersListView mStickyList;
  private int mSelectionMode;
  private int mPassengerIndex;
  private boolean mIsDataFromDatabase;
  private List<Country> mCountries;

  @Override public void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_sticky_fast);

    mSelectionMode = getIntent().getIntExtra(Constants.EXTRA_COUNTRY_MODE, SELECTION_MODE_COUNTRY);
    mPassengerIndex = getIntent().getIntExtra(Constants.EXTRA_PASSENGER_INDEX, 0);
    mIsDataFromDatabase = getIntent().getBooleanExtra(Constants.EXTRA_ALL_COUNTRY_LIST, true);

    initToolbar();
    initListView();
  }

  private void initListView() {
    mStickyList = (StickyListHeadersListView) findViewById(R.id.list);
    mStickyList.setOnItemClickListener(this);
    mStickyList.setDrawingListUnderStickyHeader(true);
    mStickyList.setAreHeadersSticky(true);
    updateContent();
  }

  private void updateContent() {
    if (mIsDataFromDatabase) {
      boolean filterPhoneCodes = mSelectionMode == SELECTION_MODE_PHONE_PREFIX;
      mCountriesInteractor.getCountries(LocaleUtils.getCurrentLanguageIso(), filterPhoneCodes,
          this);
    } else {
      buildMarketsList();
    }
  }

  private void buildMarketsList() {
    mCountries = new ArrayList<>();
    for (Market market : Configuration.getInstance().getMarkets()) {
      Country country = new Country(market.getKey(), market.getName());
      mCountries.add(country);
    }
    Collections.sort(mCountries);
    mCountriesAdapter = new CountriesAdapter(this, mCountries, mSelectionMode);
    mStickyList.setAdapter(mCountriesAdapter);
    mStickyList.setStickyHeaderTopOffset(0);
  }

  private void initToolbar() {
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      String oneCmsKey;
      switch (mSelectionMode) {
        case SELECTION_MODE_RESIDENCE:
          oneCmsKey = OneCMSKeys.HINT_COUNTRY_RESIDENCE;
          break;
        case SELECTION_MODE_PHONE_PREFIX:
          oneCmsKey = OneCMSKeys.VIEWCONTROLLER_BUYER_PHONECODE;
          break;
        case SELECTION_MODE_COUNTRY:
        default:
          oneCmsKey = OneCMSKeys.VIEWCONTROLLER_COUNTRY;
          break;
      }
      getSupportActionBar().setTitle(LocalizablesFacade.getString(this, oneCmsKey));
    }
  }

  public void onResume() {
    super.onResume();
    BusProvider.getInstance().post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_COUNTRY_LIST));
    String parentActivity = getIntent().getStringExtra(Constants.EXTRA_COUNTRY_ACT_PARENT);
    if (parentActivity != null && parentActivity.equals(
        Constants.EXTRA_COUNTRY_ACT_PARENT_SETTINGS)) {
      BusProvider.getInstance()
          .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_COUNTRY_LIST));
    } else {
      BusProvider.getInstance()
          .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_SETTINGS_COUNTRY_LIST));
    }
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      this.onBackPressed();
    }
    return super.onOptionsItemSelected(item);
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_destinations_activity, menu);
    MenuItem item = menu.findItem(R.id.btn_search_destination);
    item.setTitle(LocalizablesFacade.getString(this, OneCMSKeys.COMMON_FLIGHT).toString());
    SearchView searchView = (SearchView) menu.findItem(R.id.btn_search_destination).getActionView();
    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
      @Override public boolean onQueryTextSubmit(String query) {
        return false;
      }

      @Override public boolean onQueryTextChange(String newText) {
        List<Country> countries = filterCountryList(newText);
        if (!countries.isEmpty()) {
          mCountriesAdapter =
              new CountriesAdapter(getApplicationContext(), countries, mSelectionMode);
          mStickyList.setAdapter(mCountriesAdapter);
        }
        return true;
      }
    });
    return true;
  }

  private List<Country> filterCountryList(String filterLetter) {
    if (TextUtils.isEmpty(filterLetter)) {
      return mCountries;
    }
    List<Country> countryObjectsList = mCountries;
    List<Country> countries = new ArrayList<>();
    if (!TextUtils.isEmpty(filterLetter)) {
      for (Country c : countryObjectsList) {
        if (c.getName()
            .toUpperCase(LocaleUtils.getCurrentLocale())
            .startsWith(filterLetter.toUpperCase(LocaleUtils.getCurrentLocale()))) {
          countries.add(c);
        }
      }
    }
    return countries;
  }

  @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    Country country = mCountriesAdapter.getItem(position);
    Intent data = new Intent();
    data.putExtra(Constants.EXTRA_COUNTRY, country);
    data.putExtra(Constants.EXTRA_PASSENGER_INDEX, mPassengerIndex);
    setResult(RESULT_OK, data);
    finish();
  }

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getApplication());
  }

  @Override public void onResponse(List<Country> list) {
    mCountries = list;
    mCountriesAdapter =
        new CountriesAdapter(OdigeoCountriesActivity.this, mCountries, mSelectionMode);
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override public void run() {
        mStickyList.setAdapter(mCountriesAdapter);
        mStickyList.setStickyHeaderTopOffset(0);
      }
    });
  }

  @Override public void onError(MslError error, String message) {
    // Nothing
  }
}
