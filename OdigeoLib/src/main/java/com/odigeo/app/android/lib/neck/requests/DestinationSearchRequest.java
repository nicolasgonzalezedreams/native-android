package com.odigeo.app.android.lib.neck.requests;

import android.util.Log;
import android.util.Pair;
import com.globant.roboneck.common.NeckCookie;
import com.globant.roboneck.requests.BaseNeckHttpRequest;
import com.globant.roboneck.requests.BaseNeckRequestException.Error;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.neck.errors.NeckNotInternetError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.dataodigeo.location.TemporalDestination;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 2.1
 * @since 05/02/2015
 */
public class DestinationSearchRequest
    extends BaseNeckHttpRequest<TemporalDestination, TemporalDestination> {
  private static final String SERVICE_PATH = "autoComplete/";
  private static final int ERROR_CODE = 100;
  private final String productType;
  private final String locale;
  private final String website;
  private final String departureOrArrival;
  private final String criteria;
  private DomainHelperInterface mDomainHelper;
  private HeaderHelperInterface mHeaderHelper;

  /**
   * Constructor
   *
   * @param productType Flight
   * @param locale en_GB
   * @param website UK
   * @param departureOrArrival To indicate if it's a departure or an arrival
   * @param criteria Criteria of how to be returned the values
   */
  public DestinationSearchRequest(String productType, String locale, String website,
      String departureOrArrival, String criteria, DomainHelperInterface domainHelper,
      HeaderHelperInterface headerHelper) {
    super(TemporalDestination.class);
    this.productType = productType;
    this.locale = locale;
    this.website = website;
    this.departureOrArrival = departureOrArrival;
    this.criteria = criteria;
    mDomainHelper = domainHelper;
    mHeaderHelper = headerHelper;
  }

  @Override protected final String getUrl() {
    String criteriaEncoded = criteria;
    return String.format(mDomainHelper.getUrl()
            + SERVICE_PATH
            + "%s"
            + "?website=%s"
            + "&departureOrArrival=%s"
            + "&locale=%s"
            + "&productType=%s"
            + "&testRelatedLocations=true", criteriaEncoded, website, departureOrArrival, locale,
        productType);
  }

  @Override public final Object getCachekey() {
    return getUrl();
  }

  @Override protected final String getBody() {
    return null;
  }

  @Override public final long getCacheExpirationTime() {
    return DurationInMillis.ALWAYS_EXPIRED;
  }

  @Override protected final Map<String, String> getHeaders() {
    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    mHeaderHelper.putAccept(mapHeaders);
    return mapHeaders;
  }

  @Override protected final com.globant.roboneck.requests.BaseNeckHttpRequest.Method getMethod() {
    return Method.GET;
  }

  @Override protected final List<Pair<String, String>> getQueryParameters() {
    return null;
  }

  @Override protected final List<NeckCookie> getCookies() {
    return null;
  }

  @Override protected final TemporalDestination processContent(String responseBody) {
    try {
      return new Gson().fromJson(responseBody, TemporalDestination.class);
    } catch (JsonParseException e) {
      Log.e(Constants.TAG_LOG, e.getMessage(), e);
      return null;
    }
  }

  @Override protected final boolean isLogicError(TemporalDestination response) {
    return response == null || response.getCities() == null;
  }

  @Override protected final TemporalDestination getRequestModel(TemporalDestination model) {
    return model;
  }

  @Override protected final Error processError(int httpStatus, TemporalDestination response,
      String responseBody) {
    return new NeckNotInternetError("Cant connect to the Location Service", ERROR_CODE, 1);
  }
}
