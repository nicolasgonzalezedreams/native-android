package com.odigeo.app.android.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.odigeo.app.android.lib.R;

public class BubbleLayout extends FrameLayout {

  private BubbleBackground mBubbleBackground;
  private int mBackgroundColor;
  private int mStrokeColor;

  public BubbleLayout(Context context) {
    super(context);
  }

  public BubbleLayout(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.BubbleLayout);
    mBackgroundColor = a.getColor(R.styleable.BubbleLayout_bl_backgroundColor,
        ContextCompat.getColor(getContext(), R.color.recommended_bubble));
    mStrokeColor = a.getColor(R.styleable.BubbleLayout_bl_strokeColor,
        ContextCompat.getColor(getContext(), R.color.recommended_bubble_stroke));

    a.recycle();
    initPadding();
  }

  @Override protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
    super.onLayout(changed, left, top, right, bottom);
    initDrawable(0, getWidth(), 0, getHeight());
  }

  @Override protected void dispatchDraw(Canvas canvas) {
    if (mBubbleBackground != null) mBubbleBackground.draw(canvas);
    super.dispatchDraw(canvas);
  }

  private void initDrawable(int left, int right, int top, int bottom) {
    if (right < left || bottom < top) return;

    RectF rectF = new RectF(left, top, right, bottom);
    mBubbleBackground = new BubbleBackground(rectF, mStrokeColor, mBackgroundColor);
  }

  private void initPadding() {
    int paddingBottom = getPaddingBottom();
    paddingBottom += getResources().getDimensionPixelSize(R.dimen.recommended_bubble_arrow_height);
    setPadding(getPaddingLeft(), getPaddingTop(), getPaddingRight(), paddingBottom);
  }

  private class BubbleBackground extends Drawable {

    private RectF mRect;
    private Path mPath = new Path();
    private Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Path mStrokePath;
    private Paint mStrokePaint;
    private float mArrowWidth;
    private float mCornersRadius;
    private float mArrowHeight;
    private float mArrowPosition;
    private float mStrokeWidth;

    BubbleBackground(RectF rect, int strokeColor, int bubbleColor) {
      this.mRect = rect;
      mArrowWidth = getResources().getDimensionPixelSize(R.dimen.recommended_bubble_arrow_width);
      mCornersRadius =
          getResources().getDimensionPixelSize(R.dimen.recommended_bubble_corner_radius);
      mArrowHeight = getResources().getDimensionPixelSize(R.dimen.recommended_bubble_arrow_height);
      mArrowPosition = getIntrinsicWidth() - getResources().getDimensionPixelSize(
          R.dimen.recommended_bubble_arrow_position);
      mStrokeWidth = getResources().getDimensionPixelSize(R.dimen.recommended_bubble_stroke_width);

      mPaint.setColor(bubbleColor);
      mStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
      mStrokePaint.setColor(strokeColor);
      mStrokePath = new Path();

      initPath(mPath, mStrokeWidth);
      initPath(mStrokePath, 0);
    }

    @Override protected void onBoundsChange(Rect bounds) {
      super.onBoundsChange(bounds);
    }

    @Override public void draw(@NonNull Canvas canvas) {
      if (mStrokeWidth > 0) {
        canvas.drawPath(mStrokePath, mStrokePaint);
      }
      canvas.drawPath(mPath, mPaint);
    }

    @Override public int getOpacity() {
      return PixelFormat.TRANSLUCENT;
    }

    @Override public void setAlpha(int alpha) {
      mPaint.setAlpha(alpha);
    }

    @Override public void setColorFilter(ColorFilter cf) {
      mPaint.setColorFilter(cf);
    }

    @Override public int getIntrinsicWidth() {
      return (int) mRect.width();
    }

    @Override public int getIntrinsicHeight() {
      return (int) mRect.height();
    }

    private void initPath(Path path, float strokeWidth) {

      path.moveTo(mRect.left + mCornersRadius + strokeWidth, mRect.top + strokeWidth);
      path.lineTo(mRect.width() - mCornersRadius - strokeWidth, mRect.top + strokeWidth);
      path.arcTo(new RectF(mRect.right - mCornersRadius, mRect.top + strokeWidth,
          mRect.right - strokeWidth, mCornersRadius + mRect.top), 270, 90);

      path.lineTo(mRect.right - strokeWidth,
          mRect.bottom - mArrowHeight - mCornersRadius - strokeWidth);
      path.arcTo(
          new RectF(mRect.right - mCornersRadius, mRect.bottom - mCornersRadius - mArrowHeight,
              mRect.right - strokeWidth, mRect.bottom - mArrowHeight - strokeWidth), 0, 90);

      path.lineTo(mRect.left + mArrowWidth + mArrowPosition - (strokeWidth / 2),
          mRect.bottom - mArrowHeight - strokeWidth);
      path.lineTo(mRect.left + mArrowPosition + mArrowWidth / 2,
          mRect.bottom - strokeWidth - strokeWidth);
      path.lineTo(mRect.left + mArrowPosition + (strokeWidth / 2),
          mRect.bottom - mArrowHeight - strokeWidth);
      path.lineTo(mRect.left + Math.min(mCornersRadius, mArrowPosition) + strokeWidth,
          mRect.bottom - mArrowHeight - strokeWidth);

      path.arcTo(new RectF(mRect.left + strokeWidth, mRect.bottom - mCornersRadius - mArrowHeight,
          mCornersRadius + mRect.left, mRect.bottom - mArrowHeight - strokeWidth), 90, 90);
      path.lineTo(mRect.left + strokeWidth, mRect.top + mCornersRadius + strokeWidth);
      path.arcTo(
          new RectF(mRect.left + strokeWidth, mRect.top + strokeWidth, mCornersRadius + mRect.left,
              mCornersRadius + mRect.top), 180, 90);
      path.close();
    }
  }
}