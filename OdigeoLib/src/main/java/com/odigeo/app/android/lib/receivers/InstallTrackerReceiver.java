package com.odigeo.app.android.lib.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.localytics.android.ReferralReceiver;
import com.tune.TuneTracker;

/**
 * As multiple install_referrer receivers are not supported by Android, this receiver will broadcast
 * installment events to proper tracking
 * tools.
 *
 * @author ismaelmachado
 */
public class InstallTrackerReceiver extends BroadcastReceiver {

  @Override public void onReceive(Context context, Intent intent) {
    //Google Analytics tracking
    CampaignTrackingReceiver ga = new CampaignTrackingReceiver();
    ga.onReceive(context, intent);

    //Localytics tracking
    ReferralReceiver localytics = new ReferralReceiver();
    localytics.onReceive(context, intent);

    //Tune tracking
    TuneTracker tune = new TuneTracker();
    tune.onReceive(context, intent);
  }
}
