package com.odigeo.app.android.utils.deeplinking.extractors;

import com.odigeo.app.android.lib.config.SearchOptions;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 25/04/16
 */
public class ResidentExtractor implements DeepLinkingExtractor {

  @Override public void extractParameter(SearchOptions.Builder builder, String key, String value) {
    builder.setIsResident(Boolean.parseBoolean(value));
  }
}