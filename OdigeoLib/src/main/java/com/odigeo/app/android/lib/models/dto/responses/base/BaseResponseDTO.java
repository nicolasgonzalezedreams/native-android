package com.odigeo.app.android.lib.models.dto.responses.base;

import com.odigeo.app.android.lib.models.dto.ErrorResponseDTO;
import com.odigeo.app.android.lib.models.dto.ExtensionResponseDTO;
import com.odigeo.app.android.lib.models.dto.MessageResponseDTO;
import java.util.List;

/**
 * @author miguel
 */
@Deprecated public class BaseResponseDTO extends ErrorResponseDTO {

  protected List<MessageResponseDTO> messages;
  protected List<ExtensionResponseDTO> extensions;

  public BaseResponseDTO() {
  }

  /**
   * @return the messages
   */
  public List<MessageResponseDTO> getMessages() {
    return messages;
  }

  /**
   * @param messages the messages to set
   */
  public void setMessages(List<MessageResponseDTO> messages) {
    this.messages = messages;
  }

  /**
   * @return the extensions
   */
  public List<ExtensionResponseDTO> getExtensions() {
    return extensions;
  }

  /**
   * @param extensions the extensions to set
   */
  public void setExtensions(List<ExtensionResponseDTO> extensions) {
    this.extensions = extensions;
  }
}
