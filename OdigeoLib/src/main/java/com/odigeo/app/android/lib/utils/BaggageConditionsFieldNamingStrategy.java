package com.odigeo.app.android.lib.utils;

import com.google.gson.FieldNamingStrategy;
import com.odigeo.app.android.lib.consts.Constants;
import java.lang.reflect.Field;

/**
 * @author M. en C. Javier Silva Perez
 * @since 17/03/15.
 */
public class BaggageConditionsFieldNamingStrategy implements FieldNamingStrategy {

  Integer version;

  public BaggageConditionsFieldNamingStrategy(Integer version) {
    this.version = version;
  }

  @Override public final String translateName(Field f) {
    String name = f.getName();
    if (version >= Constants.DTOVersion.MSL_V2) {
      if ("baggageConditionsList".equals(name)) {
        return "baggageConditions";
      } else if ("baggageConditions".equals(name)) {
        return "baggageConditionsLegacy";
      }
    }
    return name;
  }
}
