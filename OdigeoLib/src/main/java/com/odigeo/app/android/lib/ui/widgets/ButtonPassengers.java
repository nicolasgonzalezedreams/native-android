package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.ui.widgets.base.ButtonWithHint;

/**
 * Created by manuel on 12/09/14.
 */
public class ButtonPassengers extends ButtonWithHint {

  private int noAdults;
  private int noKids;
  private int noBabies;

  public ButtonPassengers(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  private String createLabel() {
    String labelAdults;
    String labelKid;
    String labelBabies;

    labelAdults = getAdultsLabel(noAdults);
    labelKid = getKidsLabel(noKids);
    labelBabies = getBabiesLabel(noBabies);

    return String.format(getResources().getString(R.string.format_passenger_list), noAdults,
        labelAdults, noKids, labelKid, noBabies, labelBabies);
  }

  private String getAdultsLabel(int noAdults) {
    if (noAdults == 1) {
      return CommonLocalizables.getInstance().getCommonAdult(getContext()).toString();
    } else {
      return CommonLocalizables.getInstance().getCommonAdults(getContext()).toString();
    }
  }

  private String getKidsLabel(int noKids) {
    if (noKids == 1) {
      return CommonLocalizables.getInstance().getCommonChild(getContext()).toString();
    } else {
      return CommonLocalizables.getInstance().getCommonChildren(getContext()).toString();
    }
  }

  private String getBabiesLabel(int noBabies) {
    if (noBabies == 1) {
      return CommonLocalizables.getInstance().getCommonInfant(getContext()).toString();
    } else {
      return CommonLocalizables.getInstance().getCommonInfants(getContext()).toString();
    }
  }

  @Override public final int getLayout() {
    return R.layout.layout_search_trip_passenger_widget;
  }

  public final int getNoAdults() {
    return noAdults;
  }

  public final void setNoAdults(int noAdults) {
    this.noAdults = noAdults;

    setText(createLabel());
  }

  public final int getNoKids() {
    return noKids;
  }

  public final void setNoKids(int noKids) {
    this.noKids = noKids;

    setText(createLabel());
  }

  public final int getNoBabies() {
    return noBabies;
  }

  public final void setNoBabies(int noBabies) {
    this.noBabies = noBabies;

    setText(createLabel());
  }
}
