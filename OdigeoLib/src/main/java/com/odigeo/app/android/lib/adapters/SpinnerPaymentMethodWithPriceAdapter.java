package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import com.odigeo.app.android.lib.utils.ViewUtils;
import java.util.List;

/**
 * Created by ManuelOrtiz on 08/10/2014.
 *
 * @deprecated Use {@link OdigeoSpinnerAdapter} instead
 */
@Deprecated public class SpinnerPaymentMethodWithPriceAdapter
    extends ArrayAdapter<CollectionMethodWithPrice> {
  public SpinnerPaymentMethodWithPriceAdapter(Context context,
      List<CollectionMethodWithPrice> creditCardTypes) {
    super(context, R.layout.layout_spinner_simple_text, creditCardTypes);
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {
    View view =
        LayoutInflater.from(getContext()).inflate(R.layout.layout_spinner_simple_text, null);
    TextView textView = (TextView) view.findViewById(R.id.textView_card_name_and_icon);
    textView.setTypeface(Configuration.getInstance().getFonts().getRegular());

    CollectionMethodWithPrice collectionMethodWithPrice = getItem(position);
    textView.setText(collectionMethodWithPrice.getCollectionMethod().getName(getContext()));

    return view;
  }

  @Override public View getDropDownView(int position, View convertView, ViewGroup parent) {
    View view = LayoutInflater.from(getContext())
        .inflate(R.layout.layout_spinner_drop_cards_payments, null);
    TextView textView = (TextView) view.findViewById(R.id.text_spinner_cards);

    textView.setTypeface(Configuration.getInstance().getFonts().getRegular());

    fillUp(position, textView);
    return view;
  }

  /**
   * Fills up the text view using the item information
   *
   * @param position Position to search in items
   * @param textView Text view to update
   */
  protected void fillUp(int position, TextView textView) {
    //TODO: This method is duplicated but we can't remove it until the Spinners class were refactored
    CollectionMethodWithPrice collectionMethodWithPrice = getItem(position);

    textView.setText(collectionMethodWithPrice.getCollectionMethod().getName(getContext()));
    String code = collectionMethodWithPrice.getCollectionMethod().getCode();
    String imageName = String.format("cc_%s", code).toLowerCase();

    Drawable imageCreditCard =
        ViewUtils.getDrawableResourceByName(getContext(), imageName, getContext().getPackageName());

    //Set the credit card image
    if (imageCreditCard != null) {
      textView.setCompoundDrawablesWithIntrinsicBounds(imageCreditCard, null, null, null);
    } else {
      textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cc_cu, 0, 0, 0);
    }
  }
}
