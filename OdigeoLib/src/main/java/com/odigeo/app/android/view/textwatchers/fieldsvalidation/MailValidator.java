package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

public class MailValidator extends BaseValidator implements FieldValidator {

  public static final String REGEX_EMAIL =
      "[A-Z0-9a-z_%+-]+(\\.[A-Z0-9a-z_%+-]+)*@[A-Za-z0-9]+([\\-]*[A-Za-z0-9])*(\\"
          + ".[A-Za-z0-9]+([\\-]*[A-Za-z0-9])*)*\\.[A-Za-z]{2,4}";

  public MailValidator() {
    super();
  }

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_EMAIL);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
