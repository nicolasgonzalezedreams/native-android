package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class SelectableBaggageDescriptorDTO implements Serializable {

  protected BaggageDescriptorDTO baggageDescriptor;
  protected BigDecimal price;

  /**
   * Gets the value of the baggageDescriptor property.
   */
  public BaggageDescriptorDTO getBaggageDescriptor() {
    return baggageDescriptor;
  }

  /**
   * -     * Sets the value of the baggageDescriptor property.
   * -
   */
  public void setBaggageDescriptor(BaggageDescriptorDTO baggageDescriptor) {
    this.baggageDescriptor = baggageDescriptor;
  }

  /**
   * Gets the value of the price property.
   */
  public BigDecimal getPrice() {
    return price;
  }

  /**
   * Sets the value of the price property.
   */
  public void setPrice(BigDecimal price) {
    this.price = price;
  }
}
