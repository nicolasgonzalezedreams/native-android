package com.odigeo.app.android.lib.models;

import com.globant.roboneck.requests.BaseNeckRequestException;
import com.odigeo.app.android.lib.utils.NeckErrorCodes;

/**
 * Default error to be used for representing an empty response (http response code 204)
 *
 * @author M.Sc. Javier Silva Perez
 * @since 21/04/15.
 */
public class EmptyResponseError implements BaseNeckRequestException.Error {

  @Override public final String getMessage() {
    return NeckErrorCodes.EMPTY_RESPONSE_ERROR_MESSAGE;
  }

  @Override public final int getErrorCode() {
    return NeckErrorCodes.EMPTY_RESPONSE_ERROR_CODE;
  }

  @Override public final int getStatus() {
    return NeckErrorCodes.EMPTY_RESPONSE_ERROR_STATUS;
  }
}
