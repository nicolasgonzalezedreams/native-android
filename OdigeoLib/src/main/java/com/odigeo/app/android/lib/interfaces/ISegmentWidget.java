package com.odigeo.app.android.lib.interfaces;

import com.odigeo.app.android.lib.ui.widgets.SegmentWidget;

/**
 * Created by ManuelOrtiz on 15/09/2014.
 */
public interface ISegmentWidget {
  void onSegmentSelected(SegmentWidget segmentWidget);

  void onSegmentUnselected(SegmentWidget segmentWidget);
}
