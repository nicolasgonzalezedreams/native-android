package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.List;

public class ResidentGroupContainerDTO implements Serializable {

  protected List<ResidentGroupLocalityDTO> residentLocalities;
  protected ResidentGroupDTO name;

  public List<ResidentGroupLocalityDTO> getResidentLocalities() {
    return residentLocalities;
  }

  public void setResidentLocalities(List<ResidentGroupLocalityDTO> residentLocalities) {
    this.residentLocalities = residentLocalities;
  }

  public ResidentGroupDTO getName() {
    return name;
  }

  public void setName(ResidentGroupDTO value) {
    this.name = value;
  }
}
