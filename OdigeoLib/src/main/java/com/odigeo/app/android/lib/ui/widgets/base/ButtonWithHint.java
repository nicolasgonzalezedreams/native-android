package com.odigeo.app.android.lib.ui.widgets.base;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.Validations;

/**
 * Created by manuel on 12/09/14.
 */
public abstract class ButtonWithHint extends CustomField {
  protected static final int DEFAULT_TEXT_SIZE = 0;
  protected CharSequence emptyText;
  protected TextView txtVwText;
  protected TextView txtVwHint;
  private String text;
  private String hint;
  private int textSize;
  private int hintSize;
  private ColorStateList textColor;
  private ColorStateList hintColor;

  public ButtonWithHint(Context context) {
    super(context);

    inflateLayout();
  }

  public ButtonWithHint(Context context, AttributeSet attrs) {
    super(context, attrs);
    TypedArray aAttrs = context.obtainStyledAttributes(attrs, R.styleable.ButtonTextAndHint, 0, 0);

    text = aAttrs.getString(R.styleable.ButtonTextAndHint_odigeoText);
    hint = aAttrs.getString(R.styleable.ButtonTextAndHint_hint);
    textSize =
        aAttrs.getDimensionPixelSize(R.styleable.ButtonTextAndHint_textSize, DEFAULT_TEXT_SIZE);
    hintSize =
        aAttrs.getDimensionPixelSize(R.styleable.ButtonTextAndHint_hintSize, DEFAULT_TEXT_SIZE);
    textColor = aAttrs.getColorStateList(R.styleable.ButtonTextAndHint_textColor);
    hintColor = aAttrs.getColorStateList(R.styleable.ButtonTextAndHint_hintColor);

    aAttrs.recycle();

    inflateLayout();
  }

  public abstract int getLayout();

  private void inflateLayout() {
    inflate(this.getContext(), getLayout(), this);

    this.txtVwText = (TextView) findViewById(R.id.txtButtonWithHint_text);
    this.txtVwHint = (TextView) findViewById(R.id.txtButtonWithHint_hint);

    if (getTxtVwText() != null) {
      setTextViewSize(getTxtVwText(), textSize);
      setTextViewColor(getTxtVwText(), textColor);
    }

    if (getTxtHint() != null) {
      setTextViewColor(getTxtHint(), hintColor);
      setTextViewSize(getTxtHint(), hintSize);
    }

    if (text != null) {
      CharSequence charSequence = LocalizablesFacade.getString(getContext(), text);
      this.emptyText = charSequence;
      getTxtVwText().setText(charSequence);
    }

    if (hint != null) {
      txtVwHint.setText(LocalizablesFacade.getString(getContext(), hint));
    }
  }

  protected final void setTextViewSize(TextView textViewItem, int textSizeValue) {
    if (textSizeValue > 0) {
      textViewItem.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeValue);
    }
  }

  protected final void setTextViewColor(TextView textViewItem, ColorStateList colorValue) {
    if (colorValue != null) {
      textViewItem.setTextColor(colorValue);
    }
  }

  public final String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;

    if (getTxtVwText() != null) {
      getTxtVwText().setText(text);
    }

    if (getCustomFieldListener() != null) {
      getCustomFieldListener().onFieldValueChanged(this, getText());
    }
  }

  public final String getHintKey() {
    return hint;
  }

  public void setHint(CharSequence hint) {
    this.hint = hint.toString();

    if (getTxtHint() != null) {
      txtVwHint.setText(hint);
    }
  }

  protected final int getTextSize() {
    return textSize;
  }

  protected final void setTextSize(int textSize) {
    this.textSize = textSize;
  }

  protected final int getHintSize() {
    return hintSize;
  }

  protected final void setHintSize(int hintSize) {
    this.hintSize = hintSize;
  }

  protected final ColorStateList getTextColor() {
    return textColor;
  }

  protected final void setTextColor(ColorStateList textColor) {
    this.textColor = textColor;
  }

  protected final ColorStateList getTextHintColor() {
    return hintColor;
  }

  protected final void setTextHintColor(ColorStateList textHintColor) {
    this.hintColor = textHintColor;
  }

  protected final TextView getTxtVwText() {
    return txtVwText;
  }

  protected final TextView getTxtHint() {
    return txtVwHint;
  }

  @Override public boolean validate() {
    if (!isValid()) {
      getTxtVwText().requestFocus();
      getTxtVwText().setError(getErrorText());
      return false;
    }
    return true;
  }

  @Override public boolean isValid() {
    if (this.getVisibility() == GONE) {
      return true;
    }

    String value = "";
    if (getText() != null) {
      value = getText().trim();
    }

    return !(TextUtils.isEmpty(value) && isMandatory()) && (TextUtils.isEmpty(value)
        && !isMandatory() || Validations.validate(getValidationFormat(), value));
  }

  @Override public void clearValidation() {
    getTxtVwText().setError(null);
  }
}
