package com.odigeo.app.android.lib.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.EmptyViewWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.view.constants.OneCMSKeys;

/**
 * Created by Irving Lóp on 24/10/2014.
 */
public abstract class OdigeoNoConnectionActivity extends OdigeoBackgroundAnimatedActivity
    implements EmptyViewWidget.EmptyViewWidgetListener {

  public static void launch(Activity activity, int requestCode) {
    Intent intent =
        new Intent(activity, ((OdigeoApp) activity.getApplication()).getNoConnectionClass());
    activity.startActivityForResult(intent, requestCode);
  }

  public static void launch(android.support.v4.app.Fragment fragment, int requestCode) {
    Intent intent = new Intent(fragment.getActivity(),
        ((OdigeoApp) fragment.getActivity().getApplication()).getNoConnectionClass());
    fragment.startActivityForResult(intent, requestCode);
  }

  @Override public final String getActivityTitle() {
    return LocalizablesFacade.getString(this, OneCMSKeys.RESULTSVIEWCONTROLLER_TITLE_RESULTS)
        .toString();
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_noconnection);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    ((EmptyViewWidget) findViewById(R.id.view_activity_noconnection_emptyview)).setEmptyListener(
        this);

    postGAEventAppear();
  }

  @Override public final void clickEmptyViewButton() {
    setResult(RESULT_OK);
    OdigeoNoConnectionActivity.this.finish();
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == android.R.id.home) {
      postGAEventsGoBack();

      setResult(RESULT_CANCELED);
      finish();
    }
    return super.onOptionsItemSelected(item);
  }

  private void postGAEventsGoBack() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_CONNECTION_LOST,
            GAnalyticsNames.ACTION_NAVIGATION, GAnalyticsNames.LABEL_GO_BACK));
  }

  private void postGAEventAppear() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_CONNECTION_LOST,
            GAnalyticsNames.ACTION_CONNECTION_LOST,
            GAnalyticsNames.LABEL_CONNECTION_LOST_APPEARANCES));
  }

  //    private void postGAEventRetry() {
  //        BusProvider.getInstance().post(new GATrackingEvent(
  //            GAnalyticsNames.CATEGORY_CONNECTION_LOST, GAnalyticsNames.ACTION_CONNECTION_LOST,
  //            GAnalyticsNames.LABEL_CONNECTION_LOST_RETRY
  //        ));
  //    }

  public abstract void onGAEventTriggered(GATrackingEvent event);
}
