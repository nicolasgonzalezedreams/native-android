package com.odigeo.app.android.lib.utils.exceptions;

import com.google.gson.Gson;
import com.odigeo.data.entity.shoppingCart.BookingDetail;

public class BookingDetailException extends Throwable{

  private BookingDetailException(String json) {
    super(json);
  }

  public static BookingDetailException newInstance(BookingDetail bookingDetail) {
    String json = new Gson().toJson(bookingDetail);
    return new BookingDetailException(json);
  }
}
