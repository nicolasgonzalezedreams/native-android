package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AccommodationDealDTO implements Serializable {

  protected AccommodationDealInformationDTO accommodationDealInformation;
  protected AccommodationDealPriceCalculatorAdapterDTO price;
  protected List<AccommodationPromotionDTO> accommodationPromotions;
  protected List<AccommodationSearchReviewDTO> usersRates;
  protected String key;
  protected BigDecimal providerPrice;
  protected BigDecimal providerTaxes;
  protected String providerCurrency;
  protected Integer roomsLeft;
  protected BoardTypeDTO boardType;
  protected Integer score;
  protected Integer tripAdvisorId;

  /**
   * Gets the value of the accommodationDealInformation property.
   *
   * @return possible object is
   * {@link AccommodationDealInformation }
   */
  public AccommodationDealInformationDTO getAccommodationDealInformation() {
    return accommodationDealInformation;
  }

  /**
   * Sets the value of the accommodationDealInformation property.
   *
   * @param value allowed object is
   * {@link AccommodationDealInformation }
   */
  public void setAccommodationDealInformation(AccommodationDealInformationDTO value) {
    this.accommodationDealInformation = value;
  }

  /**
   * Gets the value of the price property.
   *
   * @return possible object is
   * {@link AccommodationDealPriceCalculatorAdapter }
   */
  public AccommodationDealPriceCalculatorAdapterDTO getPrice() {
    return price;
  }

  /**
   * Sets the value of the price property.
   *
   * @param value allowed object is
   * {@link AccommodationDealPriceCalculatorAdapter }
   */
  public void setPrice(AccommodationDealPriceCalculatorAdapterDTO value) {
    this.price = value;
  }

  /**
   * Gets the value of the accommodationPromotions property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the accommodationPromotions property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getAccommodationPromotions().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list
   * {@link AccommodationPromotion }
   */
  public List<AccommodationPromotionDTO> getAccommodationPromotions() {
    if (accommodationPromotions == null) {
      accommodationPromotions = new ArrayList<AccommodationPromotionDTO>();
    }
    return this.accommodationPromotions;
  }

  public void setAccommodationPromotions(List<AccommodationPromotionDTO> accommodationPromotions) {
    this.accommodationPromotions = accommodationPromotions;
  }

  /**
   * Gets the value of the usersRates property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the usersRates property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getUsersRates().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list
   * {@link AccommodationSearchReview }
   */
  public List<AccommodationSearchReviewDTO> getUsersRates() {
    if (usersRates == null) {
      usersRates = new ArrayList<AccommodationSearchReviewDTO>();
    }
    return this.usersRates;
  }

  public void setUsersRates(List<AccommodationSearchReviewDTO> usersRates) {
    this.usersRates = usersRates;
  }

  /**
   * Gets the value of the key property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getKey() {
    return key;
  }

  /**
   * Sets the value of the key property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setKey(String value) {
    this.key = value;
  }

  /**
   * Gets the value of the providerPrice property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  /**
   * Sets the value of the providerPrice property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setProviderPrice(BigDecimal value) {
    this.providerPrice = value;
  }

  /**
   * Gets the value of the providerTaxes property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getProviderTaxes() {
    return providerTaxes;
  }

  /**
   * Sets the value of the providerTaxes property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setProviderTaxes(BigDecimal value) {
    this.providerTaxes = value;
  }

  /**
   * Gets the value of the providerCurrency property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getProviderCurrency() {
    return providerCurrency;
  }

  /**
   * Sets the value of the providerCurrency property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setProviderCurrency(String value) {
    this.providerCurrency = value;
  }

  /**
   * Gets the value of the roomsLeft property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getRoomsLeft() {
    return roomsLeft;
  }

  /**
   * Sets the value of the roomsLeft property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setRoomsLeft(Integer value) {
    this.roomsLeft = value;
  }

  /**
   * Gets the value of the boardType property.
   *
   * @return possible object is
   * {@link BoardType }
   */
  public BoardTypeDTO getBoardType() {
    return boardType;
  }

  /**
   * Sets the value of the boardType property.
   *
   * @param value allowed object is
   * {@link BoardType }
   */
  public void setBoardType(BoardTypeDTO value) {
    this.boardType = value;
  }

  /**
   * Gets the value of the score property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getScore() {
    return score;
  }

  /**
   * Sets the value of the score property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setScore(Integer value) {
    this.score = value;
  }

  /**
   * Gets the value of the tripAdvisorId property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getTripAdvisorId() {
    return tripAdvisorId;
  }

  /**
   * Sets the value of the tripAdvisorId property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setTripAdvisorId(Integer value) {
    this.tripAdvisorId = value;
  }
}
