package com.odigeo.app.android.lib.models.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Java class for testDimension complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="testDimension">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="testDimensionPartitions" type="{http://api.edreams.com/booking/next}testDimensionPartition"
 * maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="label" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */

public class TestDimensionDTO {

  protected List<TestDimensionPartitionDTO> testDimensionPartitions;
  protected String label;

  /**
   * Gets the value of the testDimensionPartitions property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the testDimensionPartitions property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getTestDimensionPartitions().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list
   * {@link TestDimensionPartition }
   */
  public List<TestDimensionPartitionDTO> getTestDimensionPartitions() {
    if (testDimensionPartitions == null) {
      testDimensionPartitions = new ArrayList<TestDimensionPartitionDTO>();
    }
    return this.testDimensionPartitions;
  }

  /**
   * Gets the value of the label property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getLabel() {
    return label;
  }

  /**
   * Sets the value of the label property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setLabel(String value) {
    this.label = value;
  }
}
