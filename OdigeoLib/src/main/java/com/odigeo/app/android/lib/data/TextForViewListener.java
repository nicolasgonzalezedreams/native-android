package com.odigeo.app.android.lib.data;

/**
 * Interface for handling set a text into a view after a database result in background.
 *
 * @author Cristian Fanjul
 * @since 02/07/2015
 */
public interface TextForViewListener {
  void setViewText(CharSequence result);
}
