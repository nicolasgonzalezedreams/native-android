package com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators;

import com.odigeo.app.android.view.textwatchers.fieldsvalidation.BaseValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;

public class MaestroValidator extends BaseValidator implements FieldValidator {

  public static final String REGEX_MAESTRO = "^(5018|5020|5038|6304|6759|6761|6763)[0-9]{8,15}$";

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_MAESTRO);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
