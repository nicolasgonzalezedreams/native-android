package com.odigeo.app.android.navigator;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.view.AccountPreferencesView;
import com.odigeo.app.android.view.ChangePasswordView;
import com.odigeo.presenter.contracts.navigators.AccountPreferencesNavigatorInterface;

public class AccountPreferencesNavigator extends BaseNavigator
    implements AccountPreferencesNavigatorInterface {

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view_with_toolbar);
    showAccountPreferencesView();
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      onBackPressed();
      return false;
    } else {
      return super.onOptionsItemSelected(item);
    }
  }

  @Override public void showAccountPreferencesView() {
    replaceFragment(R.id.fl_container, AccountPreferencesView.newInstance());
  }

  @Override public void showResetPasswordScreen() {
    replaceFragmentWithBackStack(R.id.fl_container, ChangePasswordView.newInstance());
  }

  @Override public void finishNavigator() {
    finish();
  }

  @Override public void navigateToLogin() {
    Intent intent = new Intent(getApplicationContext(), LoginNavigator.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(intent);
    finish();
  }
}
