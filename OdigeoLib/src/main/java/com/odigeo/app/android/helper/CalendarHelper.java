package com.odigeo.app.android.helper;

import android.Manifest;
import android.app.Activity;
import android.content.AsyncQueryHandler;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.RemoteException;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresPermission;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.utils.deeplinking.DeepLinkingUtil;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DialogHelper;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.tools.CalendarHelperInterface;
import com.odigeo.tools.DateHelperInterface;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

public class CalendarHelper extends AsyncQueryHandler implements CalendarHelperInterface {

  private static final int CALENDAR_REMINDER_MINUTES = 180;
  private static final int CALENDAR_QUERY_TOKEN = 676;

  private DateHelperInterface mDateHelper;
  private Activity mActivity;
  private DialogHelper mDialogHelper;

  public CalendarHelper(Activity activity, DateHelperInterface dateHelper) {
    super(activity.getContentResolver());
    mDateHelper = dateHelper;
    mActivity = activity;
    mDialogHelper = new DialogHelper(mActivity);
  }

  @SuppressWarnings("MissingPermission") @Override
  protected void onQueryComplete(int token, Object cookie, Cursor cursor) {

    switch (token) {
      case CALENDAR_QUERY_TOKEN:
        onCalendarQueryComplete((Booking) cookie, cursor);
        break;
      default:
        break;
    }
  }

  @RequiresPermission(allOf = {
      Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR
  }) private void onCalendarQueryComplete(Booking booking, @NonNull Cursor cursor) {
    int calendarId = -1;

    if (cursor.moveToNext()) {
      calendarId = cursor.getInt(cursor.getColumnIndex(CalendarContract.Calendars._ID));
    }

    cursor.close();

    if (calendarId > 0) {

      ContentResolver contentResolver = mActivity.getContentResolver();

      ArrayList<ContentProviderOperation> operations = buildOperations(booking, calendarId);

      try {
        contentResolver.applyBatch(CalendarContract.AUTHORITY, operations);
      } catch (RemoteException | OperationApplicationException e) {
        showAddBookingToCalendarError();
      }
    } else {
      showAddBookingToCalendarError();
    }
  }

  private ArrayList<ContentProviderOperation> buildOperations(final Booking booking,
      int calendarId) {

    final ArrayList<ContentProviderOperation> operations = new ArrayList<>();

    for (Segment segment : booking.getSegments()) {
      List<Section> sections = segment.getSectionsList();
      for (Section section : sections) {

        buildEventOperation(booking, calendarId, operations, section);

        buildReminderOperation(operations);

        showAddBookingToCalendarSuccess();
      }
    }

    return operations;
  }

  private void buildEventOperation(final Booking booking, int calendarId,
      ArrayList<ContentProviderOperation> operations, Section section) {
    final String title = LocalizablesFacade.getString(mActivity, OneCMSKeys.CALENDAR_EVENT_TITLE,
        section.getCarrier().getCode() + section.getSectionId(),
        section.getFrom().getLocationCode(), section.getTo().getLocationCode()).toString();

    final String deepLink =
        DeepLinkingUtil.build(mActivity.getString(R.string.deeplink_scheme_app_identifier),
            mActivity.getString(R.string.deeplink_host_trips), new ArrayList<String>() {{
              add(String.valueOf(booking.getBookingId()));
            }}, new HashMap<String, String>() {{
              put("mail", booking.getBuyer().getEmail());
            }});

    final String description =
        LocalizablesFacade.getString(mActivity, OneCMSKeys.CALENDAR_EVENT_DESCRIPTION,
            String.valueOf(booking.getBookingId()), deepLink).toString();

    long departureOffset = 0;
    long arrivalOffset = 0;
    long localOffset = 0;

    if (section.getFrom().getTimezone() != null && section.getTo().getTimezone() != null) {

      TimeZone departureTimeZone = TimeZone.getTimeZone(section.getFrom().getTimezone());
      departureOffset = departureTimeZone.getRawOffset();

      TimeZone arrivalTimeZone = TimeZone.getTimeZone(section.getTo().getTimezone());
      arrivalOffset = arrivalTimeZone.getRawOffset();

      TimeZone localTimeZone = Calendar.getInstance().getTimeZone();
      localOffset = localTimeZone.getRawOffset();
    }

    ContentValues eventValues = new ContentValues();
    eventValues.put(CalendarContract.Events.CALENDAR_ID, calendarId);
    eventValues.put(CalendarContract.Events.TITLE, title);
    eventValues.put(CalendarContract.Events.DESCRIPTION, description);
    eventValues.put(CalendarContract.Events.DTSTART,
        mDateHelper.millisecondsLeastOffset(section.getDepartureDate()) + (localOffset
            - departureOffset));
    eventValues.put(CalendarContract.Events.DTEND,
        mDateHelper.millisecondsLeastOffset(section.getArrivalDate()) + (localOffset
            - arrivalOffset));
    eventValues.put(CalendarContract.Events.HAS_ALARM, true);
    eventValues.put(CalendarContract.Events.EVENT_TIMEZONE,
        Calendar.getInstance().getTimeZone().getID());
    eventValues.put(CalendarContract.Events.EVENT_LOCATION, section.getFrom().getMatchName());

    ContentProviderOperation event =
        ContentProviderOperation.newInsert(CalendarContract.Events.CONTENT_URI)
            .withValues(eventValues)
            .build();

    operations.add(event);
  }

  private void buildReminderOperation(ArrayList<ContentProviderOperation> operations) {
    ContentValues reminderValues = new ContentValues();
    reminderValues.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
    reminderValues.put(CalendarContract.Reminders.MINUTES, CALENDAR_REMINDER_MINUTES);

    ContentProviderOperation reminder =
        ContentProviderOperation.newInsert(CalendarContract.Reminders.CONTENT_URI)
            .withValues(reminderValues)
            .withValueBackReference(CalendarContract.Reminders.EVENT_ID, operations.size() - 1)
            .build();

    operations.add(reminder);
  }

  private void showAddBookingToCalendarError() {
    String errorMessage = LocalizablesFacade.getString(mActivity,
        OneCMSKeys.CONFIRMATIONVIEWCONTROLLER_EVENTADDTOCALENDARKOTITLE).toString();
    mDialogHelper.showDoneDialog(mActivity, errorMessage, R.drawable.hud_wrong_info,
        new DialogHelper.ProcessDoneListener() {
          @Override public void onDismiss() {
          }
        });
  }

  private void showAddBookingToCalendarSuccess() {
    String doneMessage = LocalizablesFacade.getString(mActivity,
        OneCMSKeys.CONFIRMATIONVIEWCONTROLLER_EVENTADDTOCALENDAROKTITLE).toString();
    mDialogHelper.showDoneDialog(mActivity, doneMessage, R.drawable.ok_process,
        new DialogHelper.ProcessDoneListener() {
          @Override public void onDismiss() {
          }
        });
  }

  @Override public void addBookingToCalendar(Booking booking) {

    if (booking.getSegments() != null && !booking.getSegments().isEmpty()) {

      startQuery(CALENDAR_QUERY_TOKEN, booking, CalendarContract.Calendars.CONTENT_URI, null,
          CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL + " = ?",
          new String[] { String.valueOf(CalendarContract.Calendars.CAL_ACCESS_OWNER) },
          CalendarContract.Calendars._ID);
    } else {
      showAddBookingToCalendarError();
    }
  }
}