package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.List;

public class SeatZonePreferencesSelectionItem implements Serializable {
  private int travellerNumber;
  private String rowSelection;
  private String columnSelection;
  private List<String> assignedSeats;
  private MoneyDTO providerPrice;
  private MoneyDTO totalPrice;

  public int getTravellerNumber() {
    return travellerNumber;
  }

  public void setTravellerNumber(int travellerNumber) {
    this.travellerNumber = travellerNumber;
  }

  public String getRowSelection() {
    return rowSelection;
  }

  public void setRowSelection(String rowSelection) {
    this.rowSelection = rowSelection;
  }

  public String getColumnSelection() {
    return columnSelection;
  }

  public void setColumnSelection(String columnSelection) {
    this.columnSelection = columnSelection;
  }

  public List<String> getAssignedSeats() {
    return assignedSeats;
  }

  public void setAssignedSeats(List<String> assignedSeats) {
    this.assignedSeats = assignedSeats;
  }

  public MoneyDTO getProviderPrice() {
    return providerPrice;
  }

  public void setProviderPrice(MoneyDTO providerPrice) {
    this.providerPrice = providerPrice;
  }

  public MoneyDTO getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(MoneyDTO totalPrice) {
    this.totalPrice = totalPrice;
  }
}
