package com.odigeo.app.android.lib.utils;

import android.app.Application;
import android.util.Log;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.GAProduct;
import com.odigeo.app.android.lib.models.GATransaction;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.data.tracker.CustomDimensionFormatter;
import com.odigeo.helper.ABTestHelper;
import java.util.List;
import java.util.Map;

@Deprecated public final class GATracker {

  private static final int AB_TEST_DIMEN_MIN_VALUE = 16;
  private static final int AB_TEST_DIMEN_MAX_VALUE = 30;

  private GATracker() {

  }

  /**
   * Creates a hit for traking an specific screen in google analytics
   *
   * @param screenName Screen name to be tracked
   * @param application Application used to track the hit
   */
  public static void trackScreen(String screenName, Application application) {
    // Get tracker
    Map<String, Tracker> mAnalyticsTrackers = ((OdigeoApp) application).getAnalyticsTrackers();

    for (Map.Entry<String, Tracker> entry : mAnalyticsTrackers.entrySet()) {
      // Set screen name
      entry.getValue().setScreenName(screenName);

      HitBuilders.ScreenViewBuilder screenViewBuilder = new HitBuilders.ScreenViewBuilder();
      screenViewBuilder = setAnalyticsCustomDimension(screenViewBuilder, (OdigeoApp) application);

      Log.d(Constants.TAG_LOG, "ScreenHit: " + screenName);
      entry.getValue().send(screenViewBuilder.build());
    }
  }

  /**
   * Sends a event hit to google analytics
   *
   * @param action Action of the event
   * @param label Label for the event
   * @param category Category to send
   * @param application Application used to send the hit
   */
  public static void trackEvent(String action, String label, String category,
      Application application) {
    if (application instanceof OdigeoApp) {
      // Get tracker
      Map<String, Tracker> mAnalyticsTrackers = ((OdigeoApp) application).getAnalyticsTrackers();

      for (Map.Entry<String, Tracker> entry : mAnalyticsTrackers.entrySet()) {

        HitBuilders.EventBuilder eventBuilder =
            new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label);
        eventBuilder = setAnalyticsCustomDimension(eventBuilder, (OdigeoApp) application);

        entry.getValue().send(eventBuilder.build());
      }
    } else {
      Log.i(Constants.TAG_LOG, "No es ApplicationEDreams");
    }
  }

  private static <T extends HitBuilders.HitBuilder> T setAnalyticsCustomDimension(
      HitBuilders.HitBuilder builder, OdigeoApp application) {

    builder = builder.setCustomDimension(1,
        Configuration.getInstance().getCurrentMarket().getGACustomDim());

    CustomDimensionFormatter customDimensionFormatter =
        application.getDependencyInjector().provideAnalyticsCustomDimensionFormatter();
    ABTestHelper abTestHelper = application.getDependencyInjector().provideABTestHelper();

    List<String> customDimensions =
        customDimensionFormatter.formatCustomDimension(abTestHelper.getDimensions());
    int dimensionCounter = AB_TEST_DIMEN_MIN_VALUE;
    if (dimensionCounter < AB_TEST_DIMEN_MAX_VALUE) {
      for (String customDimension : customDimensions) {
        builder = builder.setCustomDimension(dimensionCounter, customDimension);
        dimensionCounter++;
      }
    }

    return (T) builder;
  }

  /**
   * Track a general GA event to analytics
   *
   * @param event Event to be tracked
   * @param application Application to use
   */
  public static void trackEvent(GATrackingEvent event, Application application) {
    trackEvent(event.getAction(), event.getLabel(), event.getCategory(), application);
  }

  /**
   * Send a hit for a transaction to google analytics
   *
   * @param transaction Transaction to sent
   * @param application Application used to send the hit
   */
  public static void trackEcommerceTransaction(GATransaction transaction, Application application) {
    // Get tracker
    Map<String, Tracker> mAnalyticsTrackers = ((OdigeoApp) application).getAnalyticsTrackers();

    for (Map.Entry<String, Tracker> entry : mAnalyticsTrackers.entrySet()) {

      entry.getValue()
          .send(new HitBuilders.TransactionBuilder().setTransactionId(transaction.getId())
              .setAffiliation(transaction.getAffiliation())
              .setRevenue(transaction.getRevenue())
              .setTax(transaction.getTax())
              .setShipping(transaction.getShipping())
              .setCurrencyCode(transaction.getCurrencyCode())
              .setCustomDimension(1,
                  Configuration.getInstance().getCurrentMarket().getGACustomDim())
              .build());
    }
  }

  /**
   * Send a hit for a ecommerce product to google analytics
   *
   * @param product Product information to se sent
   * @param application Application used to track the hit
   */
  public static void trackEcommerceProduct(GAProduct product, Application application) {
    // Get tracker
    Map<String, Tracker> mAnalyticsTrackers = ((OdigeoApp) application).getAnalyticsTrackers();

    for (Map.Entry<String, Tracker> entry : mAnalyticsTrackers.entrySet()) {

      entry.getValue()
          .send(new HitBuilders.ItemBuilder().setTransactionId(product.getTransactionId())
              .setName(product.getItemName())
              .setSku(product.getItemSku())
              .setCategory(product.getItemCategory())
              .setPrice(product.getItemPrice())
              .setQuantity(product.getItemQuantity())
              .setCurrencyCode(product.getCurrencyCode())
              .setCustomDimension(1,
                  Configuration.getInstance().getCurrentMarket().getGACustomDim())
              .build());
    }
  }
}
