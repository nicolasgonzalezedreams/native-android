package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for fraudDecisionTypeStatus.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;simpleType name="fraudDecisionTypeStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ACCEPT"/>
 *     &lt;enumeration value="REJECT"/>
 *     &lt;enumeration value="REVIEW"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
public enum FraudDecisionTypeStatusDTO {

  ACCEPT, REJECT, REVIEW;

  public static FraudDecisionTypeStatusDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
