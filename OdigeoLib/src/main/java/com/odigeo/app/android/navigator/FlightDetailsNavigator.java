package com.odigeo.app.android.navigator;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.view.FlightDetailsView;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.presenter.contracts.navigators.FlightDetailsNavigatorInterface;

public class FlightDetailsNavigator extends BaseNavigator
    implements FlightDetailsNavigatorInterface {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view);

    Booking booking = (Booking) getIntent().getSerializableExtra(EXTRA_BOOKING);

    final ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setTitle(booking.getArrivalCityName(0));
    }

    setUpToolbarButton(0);

    if (savedInstanceState == null) {
      getSupportFragmentManager().beginTransaction()
          .add(R.id.fl_container, FlightDetailsView.newInstance(booking))
          .commit();
    }
  }
}
