package com.odigeo.app.android.view;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.PdfDownloader;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.view.animations.CollapseAndExpandAnimationUtil;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.textwatchers.ValidatePromoCodeTextWatcher;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.PromoCodeWidgetPresenter;
import com.odigeo.presenter.contracts.views.PromoCodeWidget;
import java.math.BigDecimal;

import static com.odigeo.app.android.view.constants.OneCMSKeys.PROMOCODES_CONDITIONS_URL;

public class PromoCodeWidgetView extends BaseCustomWidget<PromoCodeWidgetPresenter>
    implements PromoCodeWidget {

  private Button mBtnExpandPromoCode;

  private LinearLayout mLlPromoCodeContainerExpanded, mLlValidPromoCodeContainer;
  private TextView tvPromoCodeTitle;
  private TextView mTvValidCode;
  private LinearLayout mLlDeletePromoCodeContainer;
  private ProgressBar mPbDelete;
  private TextView mTvPromoCodeDiscountAmount;
  private ImageView mIvDeletePromoCode;

  private LinearLayout mLlValidateContainer;
  private TextInputLayout mTilPromoCode;
  private EditText mEtPromoCode;
  private ProgressBar mPbValidate;
  private Button mBtnValidatePromoCode;
  private TextView tvPromoCodeMessage;
  private TextView mTvTAC;
  private PdfDownloader pdfDownloader;
  private Context context;

  public PromoCodeWidgetView(Context context,
      CreateShoppingCartResponse createShoppingCartResponse) {
    super(context);
    pdfDownloader = new PdfDownloader(context);
    presenter.configurePromoCodeWidgetPresenter(createShoppingCartResponse);
    this.context = context;
    setListeners();
  }

  public PromoCodeWidgetView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public void initComponent() {
    mBtnExpandPromoCode = (Button) findViewById(R.id.btnExpandPromoCode);
    mLlPromoCodeContainerExpanded = (LinearLayout) findViewById(R.id.llPromoCodeContainerExpanded);
    tvPromoCodeTitle = (TextView) findViewById(R.id.tvPromoCodeTitle);
    mLlValidPromoCodeContainer = (LinearLayout) findViewById(R.id.llValidPromoCodeContainer);
    mTvValidCode = (TextView) findViewById(R.id.tvValidCode);
    mLlDeletePromoCodeContainer = (LinearLayout) findViewById(R.id.llDeletePromoCodeContainer);
    mPbDelete = (ProgressBar) findViewById(R.id.pbDelete);
    mTvPromoCodeDiscountAmount = (TextView) findViewById(R.id.tvPromoCodeDiscountAmount);
    mIvDeletePromoCode = (ImageView) findViewById(R.id.ivDeletePromoCode);
    mLlValidateContainer = (LinearLayout) findViewById(R.id.llValidateContainer);
    mTilPromoCode = (TextInputLayout) findViewById(R.id.tilPromoCode);
    mEtPromoCode = (EditText) findViewById(R.id.etPromoCode);
    mPbValidate = (ProgressBar) findViewById(R.id.pbValidate);
    mBtnValidatePromoCode = (Button) findViewById(R.id.btnValidatePromoCode);
    tvPromoCodeMessage = (TextView) findViewById(R.id.tvPromoCodeMessage);
    mTvTAC = (TextView) findViewById(R.id.tvTAC);
  }

  @Override public int getComponentLayout() {
    return R.layout.promo_code_widget;
  }

  @Override public void initOneCMSText() {
    mBtnExpandPromoCode.setText(localizables.getString(OneCMSKeys.PROMO_CODE_BUTTON_EXPAND));
    tvPromoCodeTitle.setText(localizables.getString(OneCMSKeys.PROMO_CODE_TITLE));
    mTilPromoCode.setHint(localizables.getString(OneCMSKeys.PROMO_CODE_PLACEHOLDER));
    mBtnValidatePromoCode.setText(
        localizables.getString(OneCMSKeys.PROMO_CODE_BUTTON_TITLE_VALIDATE));
    if (mTilPromoCode.getEditText() != null) {
      mTilPromoCode.getEditText()
          .addTextChangedListener(
              new ValidatePromoCodeTextWatcher(mTilPromoCode, mBtnValidatePromoCode));
    }
    tvPromoCodeMessage.setText(localizables.getString(OneCMSKeys.PROMO_CODE_SPECIAL_CONDITIONS_2));
    mTvTAC.setText(
        localizables.getString(OneCMSKeys.PROMO_CODE_TERMS_AND_CONDITIONS).toUpperCase());

    mTvTAC.setVisibility(
        localizables.getString(PROMOCODES_CONDITIONS_URL).isEmpty() ? GONE : VISIBLE);
  }

  @Override protected PromoCodeWidgetPresenter setPresenter() {
    return dependencyInjector.providePromoCodeWidgetPresenter(this);
  }

  private void setListeners() {
    mBtnExpandPromoCode.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        trackerController.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
            TrackerConstants.ACTION_COUPON_SECTION, TrackerConstants.LABEL_COUPON_OPEN_CLICKS);
        expandPromoCodeWidget();
      }
    });

    mIvDeletePromoCode.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        showConfirmationDeletePromoCode();
      }
    });

    mBtnValidatePromoCode.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        trackerController.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
            TrackerConstants.ACTION_COUPON_SECTION, TrackerConstants.LABEL_COUPON_VALIDATE_CLICKS);
        presenter.onValidatePromoCodeButtonClick();
      }
    });

    mTvTAC.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        presenter.onTACClick();
      }
    });
  }

  @Override public void expandPromoCodeWidget() {
    mBtnExpandPromoCode.setVisibility(INVISIBLE);
    CollapseAndExpandAnimationUtil.expandAndFadeIn(mLlPromoCodeContainerExpanded);
  }

  @Override public void showPromoCodeError(String errorCode, String description) {
    String errorMessage = localizables.getString(errorCode);

    if (errorMessage.equals(LocalizablesFacade.NOT_FOUND)) {
      errorMessage = description;
    }
    mTilPromoCode.setError(errorMessage);

    trackerController.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
        TrackerConstants.ACTION_COUPON_SECTION, TrackerConstants.LABEL_COUPON_NOT_OK);
  }

  @Override public String getInputCode() {
    return mEtPromoCode.getText().toString();
  }

  @Override public void clearInputCode() {
    mEtPromoCode.setText("");
  }

  @Override public String getValidCode() {
    return mTvValidCode.getText().toString();
  }

  @Override public void setValidCode(String code) {
    mTvValidCode.setText(code);
  }

  @Override public void setPromoCodeDiscountAmount(BigDecimal discountAmount) {
    String discountText =
        String.format(getResources().getString(R.string.currency_value), discountAmount,
            LocaleUtils.getCurrencySymbol());
    mTvPromoCodeDiscountAmount.setText(discountText);
  }

  @Override public void trackPromoCodeAdded() {
    trackerController.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
        TrackerConstants.ACTION_COUPON_SECTION, TrackerConstants.LABEL_COUPON_OK);
  }

  @Override public void showValidateButton() {
    mBtnValidatePromoCode.setVisibility(VISIBLE);
  }

  @Override public void hideValidateButton() {
    mBtnValidatePromoCode.setVisibility(INVISIBLE);
  }

  @Override public void showValidateProgressBar() {
    mPbValidate.setVisibility(VISIBLE);
  }

  @Override public void hideValidateProgressBar() {
    mPbValidate.setVisibility(INVISIBLE);
  }

  @Override public void showDeleteProgressBar() {
    mPbDelete.setVisibility(VISIBLE);
  }

  @Override public void hideDeleteProgressBar() {
    mPbDelete.setVisibility(INVISIBLE);
  }

  @Override public void showValidPromoCodeContainer() {
    mLlValidPromoCodeContainer.setVisibility(VISIBLE);
    ViewUtils.hideKeyboard((Activity) getContext());
  }

  @Override public void hideValidPromoCodeContainer() {
    mLlValidPromoCodeContainer.setVisibility(GONE);
  }

  @Override public void showDeletePromoCodeContainer() {
    mLlDeletePromoCodeContainer.setVisibility(VISIBLE);
  }

  @Override public void hideDeletePromoCodeContainer() {
    mLlDeletePromoCodeContainer.setVisibility(INVISIBLE);
  }

  @Override public void showValidateContainer() {
    mLlValidateContainer.setVisibility(VISIBLE);
  }

  @Override public void hideValidateContainer() {
    mLlValidateContainer.setVisibility(GONE);
  }

  @Override public void showTAC() {
    if (pdfDownloader.getStatus() == AsyncTask.Status.FINISHED) {
      pdfDownloader = new PdfDownloader(context);
    }
    pdfDownloader.execute(localizables.getString(PROMOCODES_CONDITIONS_URL));
  }

  public PromoCodeWidgetPresenter getPresenter() {
    return presenter;
  }

  private void showConfirmationDeletePromoCode() {
    CharSequence title = localizables.getString(OneCMSKeys.PAYMENT_ALERT_COUPON_TITLE);
    String message = localizables.getString(OneCMSKeys.PROMO_CODE_DELETE_MESSAGE);

    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
    builder.setTitle(title);
    builder.setMessage(message);

    builder.setPositiveButton(localizables.getString(OneCMSKeys.COMMON_YES),
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int arg1) {
            presenter.onDeletePromoCodeButtonClick();
          }
        });
    builder.setNegativeButton(localizables.getString(OneCMSKeys.COMMON_NO),
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int arg1) {
            // Nothing.
          }
        });
    builder.show();
  }
}