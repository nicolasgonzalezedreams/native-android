package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

/**
 * @author miguel
 */
@Deprecated public class FeeInfoDTO implements Serializable {

  protected FeeDetailsDTO searchFee;
  protected FeeDetailsDTO paymentFee;

  /**
   * Gets the value of the searchFee property.
   *
   * @return possible object is {@link FeeDetails }
   */
  public FeeDetailsDTO getSearchFee() {
    return searchFee;
  }

  /**
   * Sets the value of the searchFee property.
   *
   * @param value allowed object is {@link FeeDetails }
   */
  public void setSearchFee(FeeDetailsDTO value) {
    this.searchFee = value;
  }

  /**
   * Gets the value of the paymentFee property.
   *
   * @return possible object is {@link FeeDetails }
   */
  public FeeDetailsDTO getPaymentFee() {
    return paymentFee;
  }

  /**
   * Sets the value of the paymentFee property.
   *
   * @param value allowed object is {@link FeeDetails }
   */
  public void setPaymentFee(FeeDetailsDTO value) {
    this.paymentFee = value;
  }
}
