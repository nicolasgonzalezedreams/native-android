package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;

/**
 * Created by ManuelOrtiz on 30/09/2014.
 */
public class CustomNumberBaggagesPicker extends LinearLayout {

  private final Button minusButton;
  private final Button plusButton;
  private final TextView contentTextView;
  private NumberBaggaesPickerListener listener;
  private int maxNumber;
  private int minNumber;
  private int numberSelected;

  public CustomNumberBaggagesPicker(Context context, AttributeSet attrs) {
    super(context, attrs);
    View view = LayoutInflater.from(context).inflate(R.layout.widget_baggages_picker, this, true);

    minusButton = (Button) view.findViewById(R.id.minus_button);
    plusButton = (Button) view.findViewById(R.id.plus_button);
    contentTextView = (TextView) view.findViewById(R.id.content_text_view);

    setValue(0);

    addButtonListeners();
  }

  /**
   * Returns the current value from the picker.
   *
   * @return int current value
   */
  public final int getValue() {
    return numberSelected;
  }

  /**
   * Sets the base value for the picker.
   *
   * @param value current value
   */
  private void setValue(int value) {
    if (value <= maxNumber && value >= minNumber) {
      contentTextView.setText(String.format(" %d ", value));
      numberSelected = value;
    }

    setEnabling();
  }

  private void setEnabling() {
    minusButton.setEnabled(numberSelected != minNumber);
    plusButton.setEnabled(numberSelected != maxNumber);
  }

  public final void setMaxNumberOfTicketsSupported(int numTicketsSupported) {
    this.maxNumber = numTicketsSupported;
  }

  private void addButtonListeners() {

    minusButton.setOnClickListener(new OnClickListener() {

      @Override public void onClick(View v) {

        if (numberSelected > 0) {
          numberSelected--;
          setValue(numberSelected);

          if (listener != null) {
            listener.onNumberSelected(numberSelected);
          }
        }
      }
    });

    plusButton.setOnClickListener(new OnClickListener() {

      @Override public void onClick(View v) {
        if (numberSelected < maxNumber) {
          numberSelected++;
          setValue(numberSelected);

          if (listener != null) {
            listener.onNumberSelected(numberSelected);
          }
        }
      }
    });
  }

  public final void setMinNumber(int number) {
    minNumber = number;

    setValue(number);
  }

  public final void setMaxNumber(int number) {
    maxNumber = number;
    setEnabling();
  }

  public final void setNumberBaggesListener(NumberBaggaesPickerListener listener) {
    this.listener = listener;
  }

  public interface NumberBaggaesPickerListener {
    void onNumberSelected(int number);
  }
}
