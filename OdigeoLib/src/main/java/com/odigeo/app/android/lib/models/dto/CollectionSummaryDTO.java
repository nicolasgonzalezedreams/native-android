package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class CollectionSummaryDTO implements Serializable {

  protected List<CollectionAttemptDTO> attempts;
  protected String creationDate;
  protected String currency;
  protected BigDecimal collectedAmount;

  /**
   * Gets the value of the creationDate property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCreationDate() {
    return creationDate;
  }

  /**
   * Sets the value of the creationDate property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCreationDate(String value) {
    this.creationDate = value;
  }

  public List<CollectionAttemptDTO> getAttempts() {
    return attempts;
  }

  public void setAttempts(List<CollectionAttemptDTO> attempts) {
    this.attempts = attempts;
  }

  /**
   * Gets the value of the currency property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCurrency() {
    return currency;
  }

  /**
   * Sets the value of the currency property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCurrency(String value) {
    this.currency = value;
  }

  /**
   * Gets the value of the collectedAmount property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getCollectedAmount() {
    return collectedAmount;
  }

  /**
   * Sets the value of the collectedAmount property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setCollectedAmount(BigDecimal value) {
    this.collectedAmount = value;
  }
}
