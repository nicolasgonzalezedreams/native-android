package com.odigeo.app.android.lib.neck.requests;

import android.util.Log;
import android.util.Pair;
import com.globant.roboneck.common.NeckCookie;
import com.globant.roboneck.requests.BaseNeckHttpRequest;
import com.globant.roboneck.requests.BaseNeckRequestException.Error;
import com.google.gson.Gson;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.dto.responses.CarouselImagesResponse;
import com.odigeo.app.android.lib.neck.errors.NeckNotInternetError;
import java.util.List;
import java.util.Map;

public class CarouselSearchRequest
    extends BaseNeckHttpRequest<CarouselImagesResponse, CarouselImagesResponse> {
  private static final String PATH = "images/mobile/WL/%s/Images/home/%s/CarrouselImages.json";
  private static final int ERROR_CODE = 100;
  private final String brand;
  private final String market;

  public CarouselSearchRequest(String brand, String market) {
    super(CarouselImagesResponse.class);

    this.brand = brand;
    this.market = market;
  }

  @Override protected final String getUrl() {

    return String.format(Constants.CARROUSEL_IMAGES + PATH, brand, market);
  }

  @Override public final Object getCachekey() {
    return getUrl();
  }

  @Override protected final String getBody() {
    return null;
  }

  @Override public final long getCacheExpirationTime() {

    return DurationInMillis.ALWAYS_EXPIRED;
  }

  @Override protected final Map<String, String> getHeaders() {
    return null;
  }

  @Override protected final com.globant.roboneck.requests.BaseNeckHttpRequest.Method getMethod() {
    return Method.GET;
  }

  @Override protected final List<Pair<String, String>> getQueryParameters() {
    return null;
  }

  @Override protected final List<NeckCookie> getCookies() {
    return null;
  }

  @Override protected final CarouselImagesResponse processContent(String responseBody) {

    CarouselImagesResponse gsonResponse = null;

    try {
      gsonResponse = new Gson().fromJson(responseBody, CarouselImagesResponse.class);
    } catch (Exception e) {
      Log.e(Constants.TAG_LOG, e.getMessage());
    }

    return gsonResponse;
  }

  @Override protected final boolean isLogicError(CarouselImagesResponse response) {
    return response == null || response.getImages() == null;
  }

  @Override protected final CarouselImagesResponse getRequestModel(CarouselImagesResponse model) {
    return model;
  }

  @Override protected final Error processError(int httpStatus, CarouselImagesResponse response,
      String responseBody) {
    return new NeckNotInternetError("Can't connect to the Carousel Service", ERROR_CODE, 1);
  }
}
