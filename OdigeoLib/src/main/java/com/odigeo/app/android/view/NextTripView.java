package com.odigeo.app.android.view;

import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.navigator.NavigationDrawerNavigator;
import com.odigeo.app.android.view.animations.UpdateCarouselDotsAnimation;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DateHelper;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.app.android.view.interfaces.ListenerUpdateCarousel;
import com.odigeo.app.android.view.interfaces.MttCardBackground;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.SectionCard;
import com.odigeo.data.entity.extensions.UINextTrip;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.NextTripPresenter;
import com.odigeo.presenter.contracts.views.NextTripViewInterface;

/**
 * @author José Eduardo Cabrera Rivera
 * @version 1.0
 * @since 27/11/2015.
 */
public class NextTripView extends BaseView<NextTripPresenter>
    implements NextTripViewInterface, MttCardBackground {

  protected static final String KEY_NEXT_TRIP = "NEXT_TRIP";
  protected TextView mTvSubtitleCard;
  protected TextView mTvScheduleDepartureCity;
  protected TextView mTvScheduleArrivalCity;
  protected TextView mTvDateLastUpdate;
  protected SectionCard mCard;
  protected String stringLastUpdate;
  protected long mLastUpdate;
  protected int mCardPosition;
  private TextView mTvTitleCard;
  private TextView mTvHeaderDepartureCity;
  private TextView mTvHeaderArrivalCity;
  private TextView mTvDepartureCity;
  private TextView mTvTerminalDepartureCity;
  private TextView mTvHeaderScheduleDepartureCity;
  private TextView mTvHeaderTerminalDepartureCity;
  private TextView mTvArrivalCity;
  private TextView mTvTerminalArrivalCity;
  private TextView mTvHeaderScheduleArrivalCity;
  private TextView mTvHeaderTerminalArrivalCity;
  private TextView mTvFlightInfo;
  private TextView mTvFlightNumberAndCarrier;
  private TextView mTvTripStatus;
  private long mBookingId;
  private ImageView mIvIconCard;
  private ImageView mIvAutoRenewCardIcon;
  private String stringIsOnTime;
  private ListenerUpdateCarousel mListenerUpdateCarousel;
  private UpdateCarouselDotsAnimation mUpdateCarouselDotsAnimation;
  private boolean showOnTime = true;
  private DateHelper mDateHelper;

  private boolean mItIsPaused;

  public static NextTripView newInstance(SectionCard section, long lastUpdate) {
    Bundle args = new Bundle();
    args.putParcelable(KEY_NEXT_TRIP, new UINextTrip(section));
    args.putLong(Card.KEY_LAST_UPDATE, lastUpdate);
    NextTripView fragment = new NextTripView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle bundle = getArguments();
    mDateHelper = getDateHelper();
    if (bundle.containsKey(KEY_NEXT_TRIP)
        && bundle.containsKey(Card.KEY_LAST_UPDATE)
        && bundle.containsKey(Card.KEY_CARD_POSITION)) {
      mCard = (UINextTrip) bundle.getParcelable(KEY_NEXT_TRIP);
      mLastUpdate = bundle.getLong(Card.KEY_LAST_UPDATE);
      mCardPosition = bundle.getInt(Card.KEY_CARD_POSITION);
      mBookingId = mCard.getId();
    }
  }

  @Override public void onResume() {
    super.onResume();
    mListenerUpdateCarousel = (ListenerUpdateCarousel) getContext();

    if (mItIsPaused) {
      mUpdateCarouselDotsAnimation.runDotsAnimation();
    }
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }

  @Override public void onPause() {
    super.onPause();
    mListenerUpdateCarousel = null;
    mItIsPaused = true;
  }

  @Override protected NextTripPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideNextTripPresenter(this, (NavigationDrawerNavigator) getActivity());
  }

  private DateHelper getDateHelper() {
    return AndroidDependencyInjector.getInstance().provideDateHelper();
  }

  @Override protected int getFragmentLayout() {
    return R.layout.next_flight_on_time_card;
  }

  @Override protected void initComponent(View view) {
    mTvTitleCard = (TextView) view.findViewById(R.id.TvTitleCard);
    mTvSubtitleCard = (TextView) view.findViewById(R.id.TvSubtitleCard);
    mTvHeaderDepartureCity = (TextView) view.findViewById(R.id.TvHeaderDepartureCity);
    mTvDepartureCity = (TextView) view.findViewById(R.id.TvDepartureCity);
    mTvScheduleDepartureCity = (TextView) view.findViewById(R.id.TvScheduleDepartureCity);
    mTvHeaderTerminalDepartureCity =
        (TextView) view.findViewById(R.id.TvHeaderTerminalDepartureCity);
    mTvTerminalDepartureCity = (TextView) view.findViewById(R.id.TvTerminalDepartureCity);
    mTvHeaderScheduleDepartureCity =
        (TextView) view.findViewById(R.id.TvHeaderScheduleDepartureCity);
    mTvArrivalCity = (TextView) view.findViewById(R.id.TvArrivalCity);
    mTvHeaderArrivalCity = (TextView) view.findViewById(R.id.TvHeaderArrivalCity);
    mTvScheduleArrivalCity = (TextView) view.findViewById(R.id.TvScheduleArrivalCity);
    mTvHeaderTerminalArrivalCity = (TextView) view.findViewById(R.id.TvHeaderTerminalArrivalCity);
    mTvTerminalArrivalCity = (TextView) view.findViewById(R.id.TvTerminalArrivalCity);
    mTvHeaderScheduleArrivalCity = (TextView) view.findViewById(R.id.TvHeaderScheduleArrivalCity);
    mTvFlightInfo = (TextView) view.findViewById(R.id.TvFlightInfo);
    mTvFlightNumberAndCarrier = (TextView) view.findViewById(R.id.TvFlightNumberAndCarrier);
    mTvTripStatus = (TextView) view.findViewById(R.id.TvTripStatus);
    mTvDateLastUpdate = (TextView) view.findViewById(R.id.TvDateLastUpdate);
    mIvIconCard = (ImageView) view.findViewById(R.id.IvCardIcon);
    mIvAutoRenewCardIcon = (ImageView) view.findViewById(R.id.IvAutoRenewCardIcon);

    mItIsPaused = false;

    initUpdateCarouselDotsAnimation();
    initRefreshButton();
    setListeners(view);
    setUpdateListener();
  }

  private void initRefreshButton() {
    if (mCard.hasToShowRefreshButton()) {
      mIvAutoRenewCardIcon.setVisibility(View.VISIBLE);
      showOnTime = true;
    } else {
      mIvAutoRenewCardIcon.setVisibility(View.GONE);
      showOnTime = false;
    }
  }

  private void initUpdateCarouselDotsAnimation() {
    mUpdateCarouselDotsAnimation =
        AndroidDependencyInjector.getInstance().provideUpdateCarouselDotsAnimation();
    String updatingData =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_UPDATING).toString();
    mUpdateCarouselDotsAnimation.initAnimationData(updatingData, mTvDateLastUpdate);
  }

  @Override protected void setListeners() {
    initDataNextTripCard();
  }

  @Override public String getBackgroundImage() {
    String imageUrl = "";
    if (mCard != null) {
      imageUrl = mCard.getImage();
    }
    return imageUrl;
  }

  private void initDataNextTripCard() {
    String time = "<b>"
        + mDateHelper.getTimeGMT(mCard.getFrom().getDate(), getActivity())
        + "</b> "
        + mDateHelper.getDayAndMonthGMT(mCard.getFrom().getDate(), getActivity());
    mTvScheduleDepartureCity.setText(Html.fromHtml(time));
    mTvSubtitleCard.setText(mCard.getTripToHeader());
    mTvDepartureCity.setText(mCard.getFrom().getIata());
    if (mCard.getFrom().getTerminal() != null && !mCard.getFrom().getTerminal().isEmpty()) {
      mTvTerminalDepartureCity.setText(mCard.getFrom().getTerminal());
    } else {
      mTvTerminalDepartureCity.setText("-");
    }
    String time2 = "<b>"
        + mDateHelper.getTimeGMT(mCard.getTo().getDate(), getActivity())
        + "</b> "
        + mDateHelper.getDayAndMonthGMT(mCard.getTo().getDate(), getActivity());
    mTvScheduleArrivalCity.setText(Html.fromHtml(time2));
    mTvArrivalCity.setText(mCard.getTo().getIata());
    if (mCard.getTo().getTerminal() != null && !mCard.getTo().getTerminal().isEmpty()) {
      mTvTerminalArrivalCity.setText(mCard.getTo().getTerminal());
    } else {
      mTvTerminalArrivalCity.setText("-");
    }

    if (mLastUpdate == 0) {
      mTvDateLastUpdate.setVisibility(View.GONE);
    } else {
      String lastUpdateText = stringLastUpdate
          + " "
          + "<b>"
          + mDateHelper.getTime(mLastUpdate, getActivity())
          + "</b> "
          + mDateHelper.getDayAndMonth(mLastUpdate, getActivity());
      mTvDateLastUpdate.setText(Html.fromHtml(lastUpdateText));
    }

    if (showOnTime) {
      mTvTripStatus.setText(
          ViewUtils.paintStringTag(stringIsOnTime, R.color.carousel_card_font_ontime,
              getActivity()));
    } else {
      mTvTripStatus.setText("");
    }

    mTvFlightInfo.setText(mCard.getCarrierName() + " ");
    mTvFlightNumberAndCarrier.setText(mCard.getCarrier() + " " + mCard.getFlightId() + " ");
    mIvIconCard.setImageDrawable(
        DrawableUtils.getTintedResource(R.mipmap.next_flight_icon_card, R.color.carousel_card_icon,
            getActivity()));
    mIvAutoRenewCardIcon.setImageDrawable(
        DrawableUtils.getTintedResource(R.drawable.ic_autorenew, R.color.carousel_card_icon,
            getActivity()));
  }

  protected void setListeners(View view) {
    view.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        trackSeeInfo();
        mPresenter.showBookingDetail(mBookingId);
      }
    });
  }

  private void setUpdateListener() {
    mIvAutoRenewCardIcon.setOnTouchListener(new View.OnTouchListener() {
      @Override public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
          mIvAutoRenewCardIcon.setImageDrawable(
              DrawableUtils.getTintedResource(R.drawable.ic_autorenew,
                  R.color.carousel_card_icon_pressed, getActivity()));
          return true;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
          mIvAutoRenewCardIcon.setImageDrawable(
              DrawableUtils.getTintedResource(R.drawable.ic_autorenew, R.color.carousel_card_icon,
                  getActivity()));

          mListenerUpdateCarousel.onUpdateCarousel();
          startRefreshAnimation();
          mTracker.trackMTTCardEvent(TrackerConstants.LABEL_REFRESH_INFORMATION);
          return true;
        }
        return false;
      }
    });
  }

  private void startRefreshAnimation() {
    mUpdateCarouselDotsAnimation.runDotsAnimation();
    Animation startRotateAnimation =
        AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_refresh_button);
    mIvAutoRenewCardIcon.startAnimation(startRotateAnimation);
  }

  private void stopRefreshAnimation() {
    mUpdateCarouselDotsAnimation.stopDotsAnimation();
    mIvAutoRenewCardIcon.clearAnimation();
  }

  @Override public void onStop() {
    super.onStop();
    stopRefreshAnimation();
  }

  protected void trackSeeInfo() {
    String trackingLabel =
        TrackerConstants.LABEL_CARD_SEE_INFO_FORMAT.replace(TrackerConstants.CARD_TYPE_IDENTIFIER,
            TrackerConstants.LABEL_CARD_NEXT_FLIGHT_OK)
            .replace(TrackerConstants.CARD_POSITION_IDENTIFIER, String.valueOf(mCardPosition));
    mTracker.trackMTTCardEvent(trackingLabel);
  }

  @Override protected void initOneCMSText(View view) {
    mTvTitleCard.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_TITLE));
    mTvHeaderDepartureCity.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_ORIGIN_FROM));
    mTvHeaderScheduleDepartureCity.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_TIME));
    mTvHeaderScheduleArrivalCity.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_TIME));
    mTvHeaderTerminalDepartureCity.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_TERMINAL));
    mTvHeaderTerminalArrivalCity.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_TERMINAL));
    mTvHeaderArrivalCity.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_DESTINATION_TO));
    stringLastUpdate =
        String.valueOf(LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_LASTUPDATE));
    stringIsOnTime = LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.CARDS_ONTIME);
  }

  @Override public void setCardPosition(int position) {
    Bundle bundle = getArguments();
    bundle.putInt(Card.KEY_CARD_POSITION, position);
    this.setArguments(bundle);
  }
}
