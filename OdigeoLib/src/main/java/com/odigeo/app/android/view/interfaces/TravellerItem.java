package com.odigeo.app.android.view.interfaces;

/**
 * Created by carlos.navarrete on 26/09/15.
 */
public interface TravellerItem {

  int getType();

  String getElement();
}
