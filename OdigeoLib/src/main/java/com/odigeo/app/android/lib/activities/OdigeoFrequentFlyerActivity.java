package com.odigeo.app.android.lib.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.fragments.FrequentFlyerCodesFragment;
import com.odigeo.app.android.lib.fragments.FrequentFlyerEmptyFragment;
import com.odigeo.app.android.lib.fragments.FrequentFlyerFragment;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.FrequentFlyerCardCodeDTO;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Irving Lóp on 15/11/2014.
 */
@Deprecated public class OdigeoFrequentFlyerActivity extends OdigeoForegroundBaseActivity {

  private static final String TAG_FFCODES = "fragCodes";

  @Override public final void onResume() {
    super.onResume();

    getSupportActionBar().setTitle(
        LocalizablesFacade.getString(this, "frequentflyerviewcontroller_emptyscreen_title")
            .toString());
  }

  @Override public final void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    ArrayList<ArrayList<CarrierDTO>> groupCarriers =
        (ArrayList<ArrayList<CarrierDTO>>) getIntent().getSerializableExtra(
            Constants.EXTRA_FREQUENT_FLYER_CARRIERS);
    ArrayList<FrequentFlyerCardCodeDTO> frequentFlyerCodeList =
        (ArrayList<FrequentFlyerCardCodeDTO>) getIntent().getSerializableExtra(
            Constants.EXTRA_FREQUENT_FLYER_LIST);
    if (groupCarriers != null) {
      int indexPassenger = getIntent().getIntExtra(Constants.EXTRA_PASSENGER_INDEX, 0);
      FrequentFlyerCodesFragment fragment = FrequentFlyerCodesFragment.
          newInstance(indexPassenger, (ArrayList) groupCarriers, frequentFlyerCodeList);
      getFragmentManager().beginTransaction()
          .add(android.R.id.content, fragment, TAG_FFCODES)
          .commit();
    } else {
      if (frequentFlyerCodeList == null || frequentFlyerCodeList.isEmpty()) {
        getFragmentManager().beginTransaction()
            .add(android.R.id.content, new FrequentFlyerEmptyFragment())
            .commit();
      } else {
        FrequentFlyerFragment fragment = FrequentFlyerFragment.newInstance(frequentFlyerCodeList);
        getFragmentManager().beginTransaction().add(android.R.id.content, fragment).commit();
      }
    }
  }

  @Override public final void onBackPressed() {
    FrequentFlyerCodesFragment fragment =
        (FrequentFlyerCodesFragment) getFragmentManager().findFragmentByTag(TAG_FFCODES);
    if (fragment != null) {
      List<FrequentFlyerCardCodeDTO> flyerCardCodeDTOs = fragment.getCodes();
      if (flyerCardCodeDTOs != null && !flyerCardCodeDTOs.isEmpty()) {
        Intent intent = new Intent();
        intent.putExtra(Constants.EXTRA_FREQUENT_FLYER_LIST, (ArrayList) flyerCardCodeDTOs);
        intent.putExtra(Constants.EXTRA_PASSENGER_INDEX, fragment.getIndexPassenger());
        setResult(Activity.RESULT_OK, intent);
      }
    }
    super.onBackPressed();
  }

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getApplication());
  }
}
