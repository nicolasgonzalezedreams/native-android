package com.odigeo.app.android.lib.models.dto;

/**
 * @author Irving
 * @version 1.0
 * @since 18/03/2015.
 */
public class SectionImportTrip {

  private SectionDTO section;
  //    private int sectionType; // TODO: Maybe this is a Enum
  private long itineraryBookingId;
  private int id;

  public SectionDTO getSection() {
    return section;
  }

  public void setSection(SectionDTO section) {
    this.section = section;
  }

  public long getItineraryBookingId() {
    return itineraryBookingId;
  }

  public void setItineraryBookingId(long itineraryBookingId) {
    this.itineraryBookingId = itineraryBookingId;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}
