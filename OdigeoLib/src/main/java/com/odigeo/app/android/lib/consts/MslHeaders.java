package com.odigeo.app.android.lib.consts;

/**
 * @author Irving
 * @since 13/03/2015.
 * Contain some constants that uses in the header of http requests
 */
public final class MslHeaders {

  public static final String CONTENT_TYPE = "Content-Type";
  public static final String APP_JSON = "application/json";
  public static final String DEVICE_ID = "Device-ID";
  public static final String ACCEPT_ENCODING = "Accept-Encoding";
  public static final String ENCODINGS = "gzip,deflate,sdch";

  private MslHeaders() {

  }
}
