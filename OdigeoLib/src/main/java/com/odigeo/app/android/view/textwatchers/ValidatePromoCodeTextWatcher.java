package com.odigeo.app.android.view.textwatchers;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.widget.Button;

public class ValidatePromoCodeTextWatcher extends BaseTextWatcher {

  private Button mBtnValidate;

  public ValidatePromoCodeTextWatcher(TextInputLayout textInputLayout, Button btnValidate) {
    super(textInputLayout);
    mBtnValidate = btnValidate;
  }

  @Override public void afterTextChanged(Editable editableText) {
    super.afterTextChanged(editableText);
    if (editableText.toString().isEmpty()) {
      mBtnValidate.setEnabled(false);
    } else {
      mBtnValidate.setEnabled(true);
    }
  }
}
