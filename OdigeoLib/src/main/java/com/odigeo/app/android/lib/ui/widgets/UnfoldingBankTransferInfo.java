package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.pojos.banktransfer.BanktransferFirstStep;
import com.odigeo.app.android.lib.pojos.banktransfer.BanktransferSecondStep;
import com.odigeo.app.android.lib.pojos.banktransfer.BanktransferSimple;
import com.odigeo.app.android.lib.pojos.banktransfer.BanktransferWarning;
import com.odigeo.app.android.lib.ui.widgets.base.UnfoldingRowsWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.data.entity.shoppingCart.BankTransferData;
import com.odigeo.data.entity.shoppingCart.BankTransferResponse;
import java.math.BigDecimal;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @author Oscar Álvarez
 * @version 1.0
 * @since 30/04/15
 */
public class UnfoldingBankTransferInfo extends UnfoldingRowsWidget {
  private final Context context;

  /**
   * Constructor
   *
   * @param context Context where be called the Widget.
   * @param attrs Attributes to be set in the view.
   */
  public UnfoldingBankTransferInfo(Context context, AttributeSet attrs) {
    super(context, attrs);
    this.context = context;
  }

  public void setData(@NonNull BankTransferData bankTransferData,
      @NonNull BankTransferResponse bankTransferResponse, @NonNull BigDecimal totalValue) {
    BanktransferWarning warning = new BanktransferWarning();
    BanktransferFirstStep firstStep = new BanktransferFirstStep(bankTransferData, totalValue);
    BanktransferSecondStep secondStep = new BanktransferSecondStep(bankTransferData.getOrderId(),
        bankTransferResponse.getNotificationFax());
    BanktransferSimple notice = new BanktransferSimple();

    TextView tv = ((TextView) findViewById(R.id.txt_type_booking_Header));
    tv.setText(HtmlUtils.formatHtml(LocalizablesFacade.getString(context,
        "confirmation_howtocompletebooking_deposit_viabanktransfer").toString()));

    LinearLayout container = ((LinearLayout) findViewById(R.id.panelUnfolding));
    //Clear the parent
    container.removeAllViews();
    //Add the warning element.
    container.addView(warning.inflate(context, this));
    //Add the first step element.
    container.addView(firstStep.inflate(context, this));
    //Add the second step element.
    container.addView(secondStep.inflate(context, this));
    //Add final notice element.
    container.addView(notice.inflate(context, this));
  }

  @Override public int getLayout() {
    return R.layout.layout_banktransfer_folding;
  }

  @Override public int getIdSeparatorLine() {
    return R.id.contact_phone_info_line_separator;
  }

  @Override public boolean validate() {
    return true;
  }

  @Override public boolean isValid() {
    return true;
  }

  @Override public void clearValidation() {
    //Nothing
  }

  @Override protected void toggleNoticePanel() {
    if (panelUnfolding.getVisibility() == View.GONE) {
      postGABankTransferOpenEvent();
    }
    super.toggleNoticePanel();
  }

  /**
   * Trigger the Google Analytics event for the click to open the bank transfer details
   * container.
   */
  private void postGABankTransferOpenEvent() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_CONFIRMATION_PAGE_PENDING,
            GAnalyticsNames.ACTION_CONFIRMATION_BANK_TRANSFER,
            GAnalyticsNames.LABEL_CONFIRMATION_BANKTRANSFER));
  }
}
