package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

public class NIEValidator extends BaseValidator implements FieldValidator {

  private static final String REGEX_NIE = "([X-Zx-z]{1})(\\d{7})([A-Za-z]{1})";

  public NIEValidator() {
    super();
  }

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_NIE);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
