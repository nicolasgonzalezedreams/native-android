package com.odigeo.app.android.lib;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDelegate;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.AppEventsLogger;
import com.globant.roboneck.common.NeckPersister;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.localytics.android.Localytics;
import com.localytics.android.MessagingListenerAdapter;
import com.localytics.android.PushCampaign;
import com.odigeo.DependencyInjector;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.LocaleToMarket;
import com.odigeo.app.android.lib.config.Market;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.DistanceUnits;
import com.odigeo.app.android.lib.data.database.LocalizablesDBManager;
import com.odigeo.app.android.lib.fragments.OdigeoResultsFragment;
import com.odigeo.app.android.lib.models.AppSettings;
import com.odigeo.app.android.lib.receivers.UpgradeMigrationHandler;
import com.odigeo.app.android.lib.utils.DeviceUtils;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.dataodigeo.net.helper.semaphore.UIAutomationSemaphore;
import com.odigeo.dataodigeo.preferences.PreferencesController;
import com.odigeo.helper.CrashlyticsUtil;
import com.tune.Tune;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.xmlpull.v1.XmlPullParserException;

import static com.odigeo.dataodigeo.tracker.TrackerConstants.ANALYTICS_BRAND_TRACKER;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ANALYTICS_MARKET_TRACKER;

public abstract class OdigeoApp extends MultiDexApplication implements CrashlyticsUtil {

  public static final String TAG = OdigeoApp.class.getSimpleName();
  private static final int CACHE_SIZE = 2048;
  private static final int FULL_TRANSPARENCY_APP_VERSION = 3406;
  protected NeckPersister persister;
  protected DistanceUnits settingsUnits;
  //Re construct
  protected Intent intentGeolocationService = null;
  protected OdigeoSession odigeoSession;
  private boolean mHasMarketChanged = false;
  private RequestQueue mRequestQueue;
  private PreferencesControllerInterface mPreferencesController;
  private Map<String, Tracker> mAnalyticsTrackers;
  private GoogleAnalytics mGoogleAnalytics;
  private DependencyInjector dependencyInjector;

  // This method is for the tracker used on legacy code. Remove when GATracker is not used anymore.
  public Map<String, Tracker> getAnalyticsTrackers() {
    return mAnalyticsTrackers;
  }

  @Override protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    MultiDex.install(this);
  }

  @Override public void onCreate() {
    super.onCreate();
    Log.i(Constants.TAG_LOG, "Application onCreate()");

    AndroidDependencyInjector.setApplication(this);

    dependencyInjector = AndroidDependencyInjector.getInstance();

    UIAutomationSemaphore.init(AndroidDependencyInjector.getInstance().provideRequestQueue());

    mPreferencesController = AndroidDependencyInjector.getInstance().providePreferencesController();

    initConfiguration();
  }

  protected void initConfiguration() {
    FacebookSdk.sdkInitialize(this.getApplicationContext());
    AppEventsLogger.activateApp(this);

    if (Constants.isTestEnvironment) {
      FacebookSdk.setIsDebugEnabled(true);
      FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS);
    }

    initializeAppParameters();

    initializeCrashlytics();

    initializeTrackers();

    restartFullTransparencyDialog();

    initializeLocalytics();

    persister = createPersister();

    //Send to crashlytics
    String versionApp = DeviceUtils.getApplicationVersionName(getApplicationContext());
    setCrashlyticValue(Constants.APP_VERSION_NAME_CRASHLYTICS_PROPERTY, versionApp);

    UpgradeMigrationHandler.upgradeFromOldVersions(this);

    //This method restarts the shared preference value which indicates if the card must be shown.
    //It will only restart it if there was an app update.
    restartAppRateCard();

    LocalizablesDBManager localizablesManager = new LocalizablesDBManager(this);
    try {
      localizablesManager.createDataBase();
    } catch (IOException e) {
      trackNonFatal(e);
    }

    AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
  }

  public DependencyInjector getDependencyInjector() {
    return dependencyInjector;
  }

  @Override public void onConfigurationChanged(android.content.res.Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    // In case the language is changed in the device settings, loadSettings()
    // is called again to apply the selected market locale.
    loadSettings();
  }

  private void initializeTrackers() {
    Log.i(Constants.TAG_LOG, "initTracker");
    setupLegacyTracker();
    AndroidDependencyInjector injector = AndroidDependencyInjector.getInstance();
    injector.provideTrackerController().initTracker();
    setAnalyticsMarketKey();
    Tune.init(this, getString(R.string.tune_ad_id), getString(R.string.tune_conversion_key));
  }

  /*
  This setup is for the tracker used on legacy code. Ideally everything has to be by TrackerController.
  this has to be removed when everything is refactored and the deprecated GATracker class is not used anymore.
  */
  private void setupLegacyTracker() {
    mAnalyticsTrackers = new HashMap<>();
    mGoogleAnalytics = GoogleAnalytics.getInstance(this);
    mGoogleAnalytics.setLocalDispatchPeriod(1800);
    String analyticsBrandKey =
        Configuration.getInstance().getAnalyticsAppKey(getApplicationContext());
    if (analyticsBrandKey != null && !analyticsBrandKey.isEmpty()) {
      mAnalyticsTrackers.put(ANALYTICS_BRAND_TRACKER,
          mGoogleAnalytics.newTracker(analyticsBrandKey));
    }
  }

  private void setAnalyticsMarketKey() {
    String analyticsMarketKey =
        Configuration.getInstance().getAnalyticsMarketKey(getApplicationContext());
    if (analyticsMarketKey != null && !analyticsMarketKey.isEmpty()) {
      dependencyInjector.provideTrackerController().setAnalyticsMarketKey(analyticsMarketKey);

      // This line is for the tracker used on legacy code. Remove when GATracker is not used anymore.
      mAnalyticsTrackers.put(ANALYTICS_MARKET_TRACKER,
          mGoogleAnalytics.newTracker(analyticsMarketKey));
    } else {
      dependencyInjector.provideTrackerController().clearAnalyticsMarketKey();

      // This line is for the tracker used on legacy code. Remove when GATracker is not used anymore.
      mAnalyticsTrackers.remove(ANALYTICS_MARKET_TRACKER);
    }
  }

  /**
   * Initialize fonts, configuration and settings for aech app
   */
  private void initializeAppParameters() {
    try {
      Configuration.getInstance().
          readXmlFile(getResources().getXml(R.xml.brandconfiguration));
      Configuration.getInstance().readAirlinesXml(getResources().getXml(R.xml.airlines));
    } catch (XmlPullParserException | IOException e) {
      Log.e(Constants.TAG_LOG, "ERROR to parse brandConfiguration");
    }

    if (odigeoSession == null) {
      odigeoSession = new OdigeoSession();
    }

    Log.d(Constants.TAG_LOG, "Leyendo fuentes");
    setFonts();

    Log.d(Constants.TAG_LOG, "Leyendo settings");
    loadSettings();
  }

  private void restartFullTransparencyDialog() {
    int previousVersionCode =
        mPreferencesController.getIntValue(Constants.PREFERENCE_APP_VERSION_CODE);
    int currentVersionCode = Integer.parseInt(DeviceUtils.getApplicationVersionCode(this));

    if (currentVersionCode > previousVersionCode
        && currentVersionCode >= FULL_TRANSPARENCY_APP_VERSION) {
      mPreferencesController.saveBooleanValue(OdigeoResultsFragment.SHOW_FULL_TRANSPARENCY_DIALOG,
          true);
    }
  }

  /**
   * Restarts the boolean stored in sharedPreferences which indicates if the rate app cards must be
   * show in myTrips
   */
  private void restartAppRateCard() {
    int previousVersionCode = PreferencesManager.readLastAppVersionCode(this);
    int currentVersionCode = Integer.parseInt(DeviceUtils.getApplicationVersionCode(this));

    PreferencesControllerInterface preferencesController =
        AndroidDependencyInjector.getInstance().providePreferencesController();
    if (currentVersionCode > previousVersionCode) {
      preferencesController.saveBooleanValue(PreferencesController.MYTRIPS_APPRATE_SHOWCARD, true);
    }
  }

  /**
   * Search an specific font saved on assets
   *
   * @param idStringResource String resource that contains the name of the font to search
   * @return The type face loaded from assets
   */
  protected Typeface getFontAsset(@StringRes int idStringResource) {
    Context context = this.getApplicationContext();
    String fontName = context.getResources().getString(idStringResource);
    return Typeface.createFromAsset(getAssets(), fontName);
  }

  public NeckPersister getPersister() {
    return persister;
  }

  protected NeckPersister createPersister() {
    return new NeckPersister(CACHE_SIZE);
  }

  public Intent getIntentGeolocationService() {
    return intentGeolocationService;
  }

  public void setIntentGeolocationService(Intent intentGeolocationService) {
    this.intentGeolocationService = intentGeolocationService;
  }

  public RequestQueue getRequestQueue() {
    if (mRequestQueue == null) {
      mRequestQueue = Volley.newRequestQueue(getApplicationContext());
    }

    return mRequestQueue;
  }

  public <T> void addToRequestQueue(Request<T> req, String tag) {
    req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
    getRequestQueue().add(req);
  }

  public <T> void addToRequestQueue(Request<T> req) {
    req.setTag(TAG);
    getRequestQueue().add(req);
  }

  /* TODO: Temporal method. Remove it and user "addToRequestQueue" after 3.4.5 version */
  public <T> void addToRequestQueueNoCache(Request<T> req) {
    req.setTag(TAG);
    req.setShouldCache(false);
    getRequestQueue().add(req);
  }

  public void cancelPendingRequests(Object tag) {
    if (mRequestQueue != null) {
      mRequestQueue.cancelAll(tag);
    }
  }

  protected abstract void setFonts();

  public abstract Class getHomeActivity();

  public abstract Class getSearchFlightsActivityClass();

  public abstract Class getCalendarActivityClass();

  public abstract Class getDestinationActivityClass();

  public abstract Class getSearchResultsActivityClass();

  public abstract Class getFiltersActivityClass();

  public abstract Class getInsurancesActivityClass();

  public abstract Class getInsurancesConditionsActivityClass();

  public abstract Class getSummaryActivityClass();

  public abstract Class getMyInfoActivityClass();

  public abstract Class getMyTripDetailsActivityClass();

  public abstract Class getMyTripsActivityClass();

  public abstract Class getSettingsActivityClass();

  public abstract Class getPassengersActivityClass();

  public abstract Class getPaymentActivityClass();

  public abstract Class getPixelWebViewActivityClass();

  public abstract Class getConfirmationActivityClass();

  public abstract Class getNoConnectionClass();

  public abstract Class getWebViewActivityClass();

  public abstract Class getBookingDuplicatedActivityClass();

  public abstract Class getFrequentFlyer();

  public abstract Class getQAModeActivityClass();

  public abstract Class getWalkthroughActivity();

  public abstract Class getWidgetClass();

  public abstract void setCrashlyticValue(String key, String value);

  public abstract void initializeCrashlytics();

  public abstract boolean hasAnimatedBackground();

  public abstract Class getNavigationDrawerNavigator();

  /**
   * Return the name of the activity that should be shown after the current activity
   *
   * @param currentActivity Constant Representing the identifier of the activity that is showing
   * @return Name of the activity should be shown
   */
  public Step getNextActivityName(Step currentActivity) {

    List<Step> flow = Configuration.getInstance().getFlowSequence();

    int currentIndex = -1;
    int i = 0;

    while (i < flow.size() && currentIndex == -1) {
      if (flow.get(i) == currentActivity) {
        currentIndex = i;
      }

      i++;
    }

    //Index of the next activity
    currentIndex++;

    if (currentIndex < flow.size()) {

      Step nextActivity = flow.get(currentIndex);

      if (nextActivity == Step.INSURANCE && odigeoSession.isSkipInsurance()) {
        return getNextActivityName(Step.INSURANCE);
      }

      return nextActivity;
    }

    return Step.CONFIRMATION;
  }

  /**
   * Return the Class of the activity that should be shown after the current activity
   *
   * @param currentActivityName Constant Representing the identifier of the activity that is
   * showing
   * @return Class of the activity should be shown
   */
  public Class getNextActivityClass(Step currentActivityName) {

    Step nextActivityName = getNextActivityName(currentActivityName);

    if (nextActivityName == Step.SUMMARY) {
      return getSummaryActivityClass();
    }

    if (nextActivityName == Step.INSURANCE) {
      return getInsurancesActivityClass();
    }

    if (nextActivityName == Step.PASSENGER) {
      return getPassengersActivityClass();
    }

    if (nextActivityName == Step.PAYMENT) {
      return getPaymentActivityClass();
    }

    if (nextActivityName == Step.CONFIRMATION) {
      return getConfirmationActivityClass();
    }

    return getConfirmationActivityClass();
  }

  public void loadSettings() {
    Configuration configuration = Configuration.getInstance();

    String marketId = Configuration.getInstance().getDefaultMarketId();
    // Read Settings
    AppSettings appSettings = PreferencesManager.readSettings(getApplicationContext());
    if (appSettings.getMarketKey() != null) {
      Log.d(Constants.TAG_LOG, "Leyendo settings desde shared preferences");

      marketId = appSettings.getMarketKey();

      setSettingsUnits(appSettings.getDistanceUnit());

      configuration.getGeneralConstants().setDefaultNumberOfAdults(appSettings.getDefaultAdults());
      configuration.getGeneralConstants().setDefaultNumberOfKids(appSettings.getDefaultKids());
      configuration.getGeneralConstants().setDefaultNumberOfInfants(appSettings.getDefaultBabies());
    } else {
      Log.d(Constants.TAG_LOG, "Leyendo settings del device");

      Locale systemLocale = DeviceUtils.getSystemLocale(getResources());

      //Search for the Market, depending on the System Locale
      for (LocaleToMarket localeToMarket : Configuration.getInstance().getLocaleToMarkets()) {
        Locale currentLocale = LocaleUtils.strToLocale(localeToMarket.getLocale());

        if (systemLocale.equals(currentLocale)) {
          marketId = localeToMarket.getMarketId();

          break;
        }
      }
    }

    //Set the current market
    for (Market market : configuration.getMarkets()) {

      if (market.getKey().equals(marketId)) {
        configuration.setCurrentMarket(market);
        break;
      } else if (configuration.getCurrentMarket() == null && market.getKey()
          .equals(configuration.getDefaultMarketId())) {
        configuration.setCurrentMarket(market);
      }
    }

    // change Language of the app
    Resources res = getResources();
    android.content.res.Configuration conf = res.getConfiguration();

    Locale myLocale = LocaleUtils.strToLocale(configuration.getCurrentMarket().getLocale());

    if (!conf.locale.equals(myLocale)) {
      conf.locale = myLocale;
      DisplayMetrics dm = res.getDisplayMetrics();
      res.updateConfiguration(conf, dm);
    }
  }

  public DistanceUnits getSettingsUnits() {
    return settingsUnits;
  }

  public void setSettingsUnits(DistanceUnits settingsUnits) {
    this.settingsUnits = settingsUnits;
  }

  public OdigeoSession getOdigeoSession() {
    return odigeoSession;
  }

  public void setOdigeoSession(OdigeoSession odigeoSession) {
    this.odigeoSession = odigeoSession;
  }

  public boolean hasMarketChanged() {
    return mHasMarketChanged;
  }

  public void setHasMarketChanged(boolean hasMarketChanged) {
    mHasMarketChanged = hasMarketChanged;
    setAnalyticsMarketKey();
  }

  protected void configureBuildTypes(String mslUrl, boolean isDebug) {
    Constants.setIsTestEnvironment(isDebug);
    Constants.domain = mslUrl;
  }

  private void initializeLocalytics() {
    Localytics.autoIntegrate(this);

    Localytics.setMessagingListener(new MessagingListenerAdapter() {
      public int getSmallIcon() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
          return R.drawable.noticon;
        } else {
          return R.drawable.icon;
        }
      }

      @NonNull @Override public NotificationCompat.Builder localyticsWillShowPushNotification(
          @NonNull NotificationCompat.Builder builder, @NonNull PushCampaign campaign) {
        return builder.setSmallIcon(getSmallIcon())
            .setContentTitle(Configuration.getInstance().getBrandVisualName())
            .setDefaults(Notification.DEFAULT_ALL)
            .setAutoCancel(true)
            .setColor(ContextCompat.getColor(getApplicationContext(), R.color.accent_color));
      }
    });
  }
}
