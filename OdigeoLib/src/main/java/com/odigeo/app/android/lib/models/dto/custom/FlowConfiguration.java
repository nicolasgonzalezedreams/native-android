package com.odigeo.app.android.lib.models.dto.custom;

import com.odigeo.data.entity.shoppingCart.StepsConfiguration;
import java.io.Serializable;
import java.util.List;

@Deprecated public class FlowConfiguration implements Serializable {

  protected String showFeeFromStep;
  protected String showBaggageEstimationFromStep;
  protected String showCollectionEstimationFromStep;
  protected String insurancePriceMode;
  protected String insuranceOptout;
  protected String pricingModeLabel;
  protected List<StepsConfiguration> stepsConfiguration;

  /**
   * @return the showFeeFromStep
   */
  public final String getShowFeeFromStep() {
    return showFeeFromStep;
  }

  /**
   * @param showFeeFromStep the showFeeFromStep to set
   */
  public final void setShowFeeFromStep(String showFeeFromStep) {
    this.showFeeFromStep = showFeeFromStep;
  }

  /**
   * @return the showBaggageEstimationFromStep
   */
  public final String getShowBaggageEstimationFromStep() {
    return showBaggageEstimationFromStep;
  }

  /**
   * @param showBaggageEstimationFromStep the showBaggageEstimationFromStep to set
   */
  public final void setShowBaggageEstimationFromStep(String showBaggageEstimationFromStep) {
    this.showBaggageEstimationFromStep = showBaggageEstimationFromStep;
  }

  /**
   * @return the showCollectionEstimationFromStep
   */
  public final String getShowCollectionEstimationFromStep() {
    return showCollectionEstimationFromStep;
  }

  /**
   * @param showCollectionEstimationFromStep the showCollectionEstimationFromStep to set
   */
  public final void setShowCollectionEstimationFromStep(String showCollectionEstimationFromStep) {
    this.showCollectionEstimationFromStep = showCollectionEstimationFromStep;
  }

  /**
   * @return the stepsConfiguration
   */
  public final List<StepsConfiguration> getStepsConfiguration() {
    return stepsConfiguration;
  }

  /**
   * @param stepsConfiguration the stepsConfiguration to set
   */
  public final void setStepsConfiguration(List<StepsConfiguration> stepsConfiguration) {
    this.stepsConfiguration = stepsConfiguration;
  }

  /**
   * @return the insurancePriceMode
   */
  public final String getInsurancePriceMode() {
    return insurancePriceMode;
  }

  /**
   * @param insurancePriceMode the insurancePriceMode to set
   */
  public final void setInsurancePriceMode(String insurancePriceMode) {
    this.insurancePriceMode = insurancePriceMode;
  }

  /**
   * @return the insuranceOptout
   */
  public final String getInsuranceOptout() {
    return insuranceOptout;
  }

  /**
   * @param insuranceOptout the insuranceOptout to set
   */
  public final void setInsuranceOptout(String insuranceOptout) {
    this.insuranceOptout = insuranceOptout;
  }

  /**
   * @return the pricingModeLabel
   */
  public final String getPricingModeLabel() {
    return pricingModeLabel;
  }

  /**
   * @param pricingModeLabel the pricingModeLabel to set
   */
  public final void setPricingModeLabel(String pricingModeLabel) {
    this.pricingModeLabel = pricingModeLabel;
  }
}
