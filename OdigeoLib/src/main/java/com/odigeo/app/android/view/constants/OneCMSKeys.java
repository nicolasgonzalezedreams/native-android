package com.odigeo.app.android.view.constants;

public class OneCMSKeys {
  public static final String SSO_TRAVELLERS = "sso_travellers";
  public static final String SSO_CHANGEPASSWORD_TITLE = "sso_changepassword_title";
  public static final String SSO_CHANGEPASSWORD_CURRENT = "sso_changepassword_current";
  public static final String SSO_CHANGEPASSWORD_NEW = "sso_changepassword_new";
  public static final String SSO_CHANGEPASSWORD_REPEAT = "sso_changepassword_repeat";
  public static final String SSO_CHANGEPASSWORD_RESET = "sso_changepassword_reset";
  public static final String SSO_FORM_ERROR_PASSWORD_FORMAT = "sso_form_error_password_format";
  public static final String SSO_CHANGEPASSWORD_ERROR = "sso_changepassword_error";
  //Register View
  public static final String SSO_REGISTER_TITLE = "sso_register_title";
  public static final String SSO_ERROR_ALREADY_USED = "sso_error_already_used";
  public static final String SSO_REGISTERINFO_SIGNIN_FACEBOOK = "sso_registerinfo_signin_facebook";
  public static final String SSO_REGISTERINFO_SIGNIN_GOOGLE = "sso_registerinfo_signin_google";
  public static final String SSO_REGISTER_CONFIRMATION_EMAIL = "sso_register_confirmation_email";
  public static final String SSO_REGISTER_LEGAL = "android_sso_register_legal";
  public static final String SSO_TERMSANDCONDITIONS = "sso_termsandconditions";
  public static final String SSO_PRIVACY = "sso_privacy";
  public static final String SSO_REGISTER_NEWSLETTER = "sso_register_newsletter";
  public static final String SSO_SIGNIN_READY_TITLE = "sso_signin_ready_title";
  //Login View
  public static final String SSO_HEADER_LOGIN = "sso_header_login";
  public static final String SSO_ERROR_SERVER = "sso_error_server";
  public static final String SSO_SIGNIN_ERROR_PASSWORD_EMAIL_INCORRECT =
      "sso_signin_error_password_email_incorrect";
  public static final String SSO_INITSESSION_LOGINGIN = "sso_initsession_loggingin";
  public static final String SSO_ERROR_ACCOUNT_NOT_ACTIVATED_TITLE =
      "sso_error_account_not_activated_title";
  public static final String SSO_ACCOUNT_NOT_ACTIVATED_MSG = "sso_error_account_not_activated_msg";
  public static final String SSO_INITSESSION_WELCOME = "sso_initsession_welcome";
  public static final String SSO_SHOW_PASSWORD = "sso_show_password";
  public static final String SSO_WRONG_EMAIL_FORMAT = "sso_wrong_email_format";
  public static final String SSO_GO_TO_MAIL = "sso_go_to_email";
  public static final String SSO_ERROR_INCORRECT_USERNAME = "sso_error_username_incorrect";
  public static final String SSO_ERROR_EMAIL_NOT_REGISTER_OR_ACTIVE =
      "sso_form_error_email_noregisteroractive";
  //Recover Password View
  public static final String SSO_PASSWORDRECOVERY_HEADER = "sso_passwordrecovery_header";
  public static final String SSO_PASSWORDRECOVERY_FORGET_BUTTON =
      "sso_passwordrecovery_forget_button";
  public static final String SSO_RECOVERPASSWORD_SENDING_INSTRUCTIONS =
      "sso_initisession_recoverpassword_sending";
  public static final String SSO_PASSWORDRECOVERY_CLOSE = "sso_passwordrecovery_close";
  public static final String SSO_ERROR_USERNAME_INCORRECT = "sso_error_username_incorrect";
  public static final String SSO_ERROR_USERNAME_EMPTY = "sso_error_username_empty";
  public static final String SSO_ERROR_RECOVERY_PASSWORD_FAIL_TRY_FACEBOOK_GOOGLE =
      "sso_error_nopassword_to_email";
  public static final String SSO_SIGNIN_FORGOT_PASSWORD = "sso_signin_forgot_password";
  //Account Preferences View
  public static final String SSO_PERSONALAREA_PREFERENCES = "sso_personalarea_preferences";
  public static final String SSO_FORM_SHADOWTEXT_EMAIL = "sso_form_shadowtext_email";
  public static final String SSO_FORM_INSTRUCTIONSTEXT_EMAIL = "sso_passwordrecovery_forget_msg";
  public static final String SSO_FORM_SHADOWTEXT_PASSWORD = "sso_form_shadowtext_password";
  public static final String SSO_ACCOUNT_DELETE = "sso_account_delete";
  public static final String SSO_BLOCKED_MAIL_TITLE = "sso_blocked_mail_title";
  public static final String SSO_BLOCKED_MAIL_MESSAGE = "sso_blocked_mail_message";
  public static final String SSO_BLOCKED_MAIL_OK = "sso_blocked_mail_ok";
  public static final String SSO_DELETE_ACCOUNT_TITLE = "sso_delete_account_title";
  public static final String SSO_DELETE_ACCOUNT_MESSAGE = "sso_delete_account_message";
  public static final String SSO_DELETE_ACCOUNT_OK = "sso_delete_account_ok";
  public static final String SSO_DELETE_ACCOUNT_WAIT = "sso_delete_account_wait";
  public static final String SSO_DELETE_ACCOUNT_CANCEL = "sso_delete_account_cancel";
  public static final String SSO_PASSWORD_WRONG = "sso_password_wrong";
  public static final String SSO_NEW_PASSWORD_EQUAL_ERROR = "sso_new_password_equal_error";
  public static final String SSO_REPEAT_PASSWORD_ERROR = "sso_repeat_password_error";
  public static final String SSO_FIELD_MANDATORY_ERROR = "sso_field_mandatory_error";
  public static final String SSO_ACCOUNT_REMOVED_SUCCESS = "sso_account_remove_success";
  public static final String SSO_ACCOUNT_REMOVE_FAIL = "sso_account_remove_fail";
  public static final String SSO_PASSWORD_CHANGE_SUCCESS = "sso_password_change_success";
  public static final String SSO_PASSWORD_CHANGE_FAIL = "sso_password_change_fail";
  //Register Success View
  public static final String SSO_REGISTER_CHECKMAIL_MSG = "sso_register_checkemail_msg";
  public static final String SSO_REGISTER_CANTFIND_MSG = "sso_register_cantfind_msg";
  public static final String SSO_REGISTER_SUCCESS = "sso_register_success";
  //Join Us View
  public static final String SSO_LOGIN_BUTTON = "sso_login_button";
  public static final String SSO_CREATE_ACCOUNT = "sso_create_account_button";
  public static final String SSO_PERSONALAREA_JOIN = "sso_personalarea_join_button";
  //Navigation Drawer and others related
  public static final String SSO_ACCOUNTPREF_LOGOUT_CHECK = "sso_accountpref_logout_check";
  public static final String SSO_LOGOUT_BUTTON = "sso_logout_button";
  public static final String SSO_LOGOUT_TITLE = "sso_logout_title";
  public static final String SSO_ND_LOGIN_OR_REGISTER = "nd_login_or_register";
  public static final String SSO_ND_TRAVELLERS = "sso_travellers";
  public static final String SSO_ND_TRIPS = "combomenucelltitle_my_trips";
  public static final String SSO_ND_SETTINGS = "settingsviewcontroller_title";
  public static final String SSO_ND_ABOUT = "sso_nd_about";
  public static final String SSO_ND_ACCOUNT_PREFERENCES = "sso_personalarea_preferences";
  public static final String SSO_ND_SAVE_RECENT_SEARCH = "sso_nd_save_recent_search";
  public static final String SSO_ND_WHY_TO_LOGIN = "sso_nd_why_to_login";
  public static final String SSO_MYINFO_VALIDATE_ACCOUNT_MSG = "sso_myinfo_validate_account_msg";
  public static final String SSO_MYINFO_VALIDATE_YOUR_ACCOUNT_TITLE =
      "sso_myinfo_validate_your_account_title";
  //Settings
  public static final String APPSETTINGS_HEADER_NOTIFICATIONS = "appsettings_header_notifications";
  //About
  public static final String ABOUTOPTIONSMODULE_HEADERTITLE = "aboutoptionsmodule_headertitle";
  public static final String ABOUTOPTIONSMODULE_HELPCENTER_URL =
      "aboutoptionsmodule_about_option_helpcenter_url";
  public static final String ABOUTOPTIONSMODULE_CONTACTUS =
      "aboutoptionsmodule_about_option_contactus";
  public static final String ABOUTVIEWCONTROLLER_TITLE = "aboutviewcontroller_title";
  public static final String ABOUTOPTIONSMODULE_SEND_MAIL =
      "aboutoptionsmodule_about_option_send_email";
  public static final String ABOUT_FEEDBACK_DIALOG_TITLE = "email_helpimprove_actionsheet_title";
  public static final String ABOUT_BUTTONS_FAQ = "aboutoptionsmodule_about_option_faq";
  public static final String ABOUT_BUTTONS_CONTACT_US = "aboutoptionsmodule_about_option_contactus";
  public static final String ABOUT_BUTTONS_TERMS = "aboutoptionsmodule_about_option_terms";
  public static final String ABOUT_BUTTONS_FEEDBACK = "aboutoptionsmodule_about_option_feedback";
  public static final String ABOUT_BUTTONS_SHAREAPP = "aboutoptionsmodule_about_option_shareapp";
  public static final String ABOUT_BUTTONS_RATEAPP =
      "aboutoptionsmodule_about_option_leavefeedback";
  public static final String ABOUT_BUTTONS_TC_BRAND = "aboutoptionsmodule_about_option_terms_brand";
  public static final String ABOUT_BUTTONS_TC_AIRLINE =
      "aboutoptionsmodule_about_options_terms_airlines";
  public static final String ABOUT_TC_AIRLINE_URL =
      "aboutoptionsmodule_about_option_terms_airlines_url";
  public static final String ABOUT_BUTTONS_CALL_US = "aboutoptionsmodule_about_option_call_us";
  public static final String QUESTIONS_ABOUT_FLIGHTS_URL =
      "aboutoptionsmodule_about_option_questions_flights_url";
  public static final String ABOUT_BRAND_URL = "aboutoptionsmodule_about_option_about_brand_url";
  public static final String ABOUT_APP_URL = "aboutoptionsmodule_about_option_about_app_url";
  public static final String SECURE_SHOPPING_URL =
      "aboutoptionsmodule_about_option_secure_shopping_url";
  public static final String PROMOCODES_CONDITIONS_URL =
      "aboutoptionsmodule_about_option_promocode_url";
  public static final String PRIVACY_POLICY_URL =
      "aboutoptionsmodule_about_option_privacy_policy_url";
  //Email
  public static final String EMAIL_HELPIMPROVE_SUBJECT_ERROR_INAPP =
      "email_helpimprove_email_subject_errorinapp";
  public static final String EMAIL_HELPIMPROVE_SUBJECT_BOOKING_PROBLEM =
      "email_helpimprove_email_subject_bookingproblem";
  public static final String EMAIL_HELPIMPROVE_SUBJECT_COMMENT_SUGGESTION =
      "email_helpimprove_email_subject_commentsuggestion";
  // Config
  public static final String CONFIG_FEEDBACK_MAIL = "config_feedback_mail";
  //Sharing
  public static final String SHARING_APP_EMAIL_TITLE = "sharing_app_email_title";
  public static final String SHARING_APP_EMAIL_BODY = "sharing_app_email_body";
  public static final String SHARING_APP_TWITTER_DESCRIPTION =
      "sharing_app_twitter_description_new";
  public static final String SHARING_APP_WHATSAPP = "sharing_app_whatsapp_description";
  public static final String SHARING_APP_FB_MESSENGER = "sharing_app_fbmessenger_description";
  public static final String SHARING_APP_COPY_TO_CLIPBOARD =
      "sharing_app_copytoclipboard_description";
  public static final String SHARING_APP_SMS = "sharing_app_imessage_description";
  public static final String FAQVIEWCONTROLLER_HEADERTITLE = "faqviewcontroller_headertitle";
  public static final String CONTACTUSVIEWCONTROLLER_HEADERTITLE =
      "contactusviewcontroller_headertitle";
  public static final String ABOUTOPTIONSMODULE_ABOUT_OPTION_TERMS_URL =
      "aboutoptionsmodule_about_option_terms_url";
  public static final String HELPCENTER_BUTTON = "helpcenter_button";
  public static final String HELPCENTER_HELP_TITLE_2 = "helpcenter_help_title_2";
  public static final String HELPCENTER_SUBTITLE = "helpcenter_subtitle";
  public static final String HELPCENTER_ITEM1 = "helpcenter_item1";
  public static final String HELPCENTER_ITEM2 = "helpcenter_item2";
  public static final String HELPCENTER_ITEM3 = "helpcenter_item3";
  //Lecagy User Reset Password
  public static final String SSO_LEGACY_USER_SENT_MAIL = "sso_signin_error_password_mail";
  public static final String SSO_LEGACY_USER_HEADER = "sso_legacy_user_header";
  //User already registered
  public static final String SSO_EXISTING_MAIL_TITLE = "sso_existing_mail_title";
  public static final String SOO_REGISTERINFO_ALREADY_REGISTERED =
      "sso_registerinfo_already_registered_fbgoogle";
  //Facebook&Google
  public static final String SSO_FACEBOOK_ERROR_PERMISSIONS = "sso_facebook_error_permissions";
  public static final String SSO_FB_GOOGLE_ERROR_CREATING_ACCOUNT =
      "sso_create_account_error_email";
  //Passengers
  public static final String PASSENGER_NOT_PREMIUM_MEMBER =
      "passengersmanager_premium_member_nopax_warning";
  public static final String PASSENGER_TYPE_PREMIUM = "passengersmanager_premium_member";
  public static final String SSO_PASSENGERS_TITLE = "travellersviewcontroller_title";
  public static final String ADDING_PERSONAL_INFO_MESSAGE =
      "loadingviewcontroller_message_addingpersonalinfo";
  public static final String TITLE_CARD_PASSENGER = "travellersviewcontroller_labelheaderpassenger";
  public static final String FOOTER_LEGAL_DISCLAIMER =
      "passengerdetailmodule_footer_legal_disclaimer";
  public static final String PASSENGER_TYPE_NEW = "selectpicker_passengerpicker_newpassenger";
  public static final String PASSENGER_TYPE_ADULT = "mydatapassengercell_type_adult";
  public static final String PASSENGER_TYPE_CHILD = "mydatapassengercell_type_child";
  public static final String PASSENGER_TYPE_INFANT = "mydatapassengercell_type_infant";
  public static final String PASSENGER_TITTLE = "formfieldrow_placeholder_title";
  public static final String PASSENGER_TITLE_MR = "common_mr";
  public static final String PASSENGER_TITLE_MRS = "common_mrs";
  public static final String PASSENGER_TITLE_MS = "common_ms";
  public static final String PASSENGER_BAGGAGE_INCLUDED_ROUND_TRIP_INFO =
      "travellersviewcontroller_baggageconditions_tripbaggageincluded";
  public static final String PASSENGER_BAGGAGE_INCLUDED_LEG_INFO =
      "travellersviewcontroller_baggageconditions_legbaggageincluded";
  public static final String PASSENGER_BAGGAGE_INCLUDED_MAX_WEIGHT =
      "travellersviewcontroller_baggageconditions_maxbagweight";
  public static final String PASSENGER_BAGGAGE_SELECTION =
      "travellersviewcontroller_baggageconditions_addbaggage";
  public static final String PASSENGER_NO_BAGGAGE_SELECTION =
      "travellersviewcontroller_baggageconditions_nobaggageselected";
  public static final String PASSENGER_BAGGAGE_MAX_WEIGHT =
      "travellersviewcontroller_baggageconditions_maxbagweight";
  public static final String PASSENGER_BAGGAGE_NOT_INCLUDED_IN_TRIP =
      "travellersviewcontroller_baggageconditions_tripbaggagenotincludednotpurchasable";
  public static final String PASSENGER_BAGGAGE_NOT_INCLUDED_IN_SEGMENT =
      "travellersviewcontroller_baggageconditions_legbaggagenotincludednotpurchasable";
  public static final String PASSENGER_BAGGAGE_APPLY_BAGGAGE_SELECTION =
      "travellersviewcontroller_extend_selection_body";
  public static final String PASSENGER_CHILD_INFANT_BIRTHDATE_ERROR_TITTLE = "kidsgrewupcell_title";
  public static final String PASSENGER_CHILD_BIRTHDATE_ERROR_BODY =
      "kidsgrewupcell_description_child";
  public static final String PASSENGER_INFANT_BIRTHDATE_ERROR_BODY =
      "kidsgrewupcell_description_infant";
  // Contact us
  public static final String MARKET_SPECIFIC_CALLCENTER_PHONE =
      "marketspecific_callcenterphone_phone";
  public static final String MARKETSPECIFIC_CALLCENTERPHONE_PHONEABROAD =
      "marketspecific_callcenterphone_phoneabroad";
  public static final String MARKETSPECIFIC_CALLCENTERPHONE_PHONEABROADFEES =
      "marketspecific_callcenterphone_phoneabroadfees";
  public static final String MARKETSPECIFIC_CALLCENTERPHONE_PHONEFEES =
      "marketspecific_callcenterphone_phonefees";
  public static final String MARKETSPECIFIC_CALLCENTERSCHEDULE_PERIOD0 =
      "marketspecific_callcenterschedule_period0";
  public static final String MARKETSPECIFIC_CALLCENTERSCHEDULE_PERIOD1 =
      "marketspecific_callcenterschedule_period1";
  public static final String MARKETSPECIFIC_CALLCENTERSCHEDULE_PERIOD2 =
      "marketspecific_callcenterschedule_period2";
  public static final String CONFIRMATION_WHATSNEXT_CALLCENTERABROAD =
      "confirmation_whatsnext_callcenterabroad";
  public static final String PASSENGER_BAGGAGE_INCLUDED =
      "travellersviewcontroller_baggageconditions_included";
  public static final String PASSENGER_BAGGAGE_NOT_ALLOWED =
      "baggagesmanager_itinerary_notallowed_subtitle";
  public static final String PASSENGERDETAILMODULE_HEADER_LABELTITLE =
      "passengerdetailmodule_header_labeltitle";
  public static final String PASSENGER_GENERIC_FIELD_ERROR =
      "validation_error_invalid_genericfield";
  public static final String PASSENGER_ERROR_VALIDATION = "error_validation";
  public static final String PASSENGER_COLLAPSE_EDIT = "travellersviewcontroller_details_edit";
  public static final String PASSENGER_COLLAPSE_CLOSE = "travellersviewcontroller_details_close";
  public static final String PASSENGER_CPF_FIELD_ERROR = "validation_error_invalid_cpf";
  public static final String PASSENGER_PERSUASION_MESSAGE = "persuasivemessaging_passengersinfo";
  public static final String PASSENGER_BAGGAGECONDITIONS_HEADER_TITLE =
      "travellersviewcontroller_baggageconditions_header_title";
  public static final String PASSENGER_BAGGAGECONDITIONS_PERSUASION =
      "travellersviewcontroller_baggageconditions_persuasion";
  public static final String PASSENGER_BAGGAGECONDITIONS_PERSUASION_NO =
      "travellersviewcontroller_baggageconditions_persuasion_no";
  public static final String PASSENGER_WIDGET_LOGIN_ACTIVATION =
      "travellersviewcontroller_widget_login";
  public static final String PASSENGER_WIDGET_LOGIN_TITLE =
      "travellersviewcontroller_widget_login_title";
  public static final String PASSENGER_WIDGET_LOGIN_SUBTITLE =
      "travellersviewcontroller_widget_login_subtitle";
  public static final String PASSENGER_WIDGET_LOGIN_BUTTON =
      "travellersviewcontroller_widget_login_button";
  public static final String BAGGAGE_NAG_REMOVE_TITLE =
      "travellersviewcontroller_baggage_nag_remove_title";
  public static final String BAGGAGE_NAG_REMOVE_BODY =
      "travellersviewcontroller_baggage_nag_remove_body";
  public static final String BAGGAGE_NAG_REMOVE_BUTTON_POSITIVE =
      "travellersviewcontroller_baggage_nag_remove_button_positive";
  public static final String BAGGAGE_NAG_REMOVE_BUTTON_NEGATIVE =
      "travellersviewcontroller_baggage_nag_remove_button_negative";

  public static final String COMMON_OUTBOUND = "common_outbound";
  public static final String COMMON_INBOUND = "common_inbound";
  public static final String COMMON_LEG = "common_leg";
  //Travellers
  public static final String SSO_TRAVELLERS_NONE_SAVED = "sso_travellers_none_saved";
  public static final String SSO_TRAVELLERS_NONE_SAVED_INFO = "sso_travellers_none_saved_info";
  public static final String SSO_TRAVELLERS_ADD = "sso_travellers_add";
  public static final String SSO_TRAVELLERS_SAVED = "sso_travellers_saved";
  public static final String SSO_TRAVELLERS_SAVED_INFO = "sso_travellers_saved_info";
  public static final String SSO_TRAVELLERS_DOCUMENTATION = "sso_travellers_documentation";
  public static final String SSO_TRAVELLERS_CONTACT_DETAIL = "sso_passengers_contact_details";
  public static final String SSO_TRAVELLERS_DELETE = "sso_travellers_delete";
  public static final String SSO_TRAVELLERS_BASIC_INFO = "sso_travellers_basic_info";
  public static final String SSO_TRAVELLERS_EDIT_TITLE = "sso_travellers_edit_title";
  public static final String SSO_IDENTIFICATION_DELETE = "sso_identification_delete";
  public static final String SSO_CREATE_NEW_CONTACT_DETAIL =
      "sso_travellers_create_new_contact_detail";
  public static final String COMMON_OK = "common_ok";
  public static final String COMMON_CANCEL = "common_cancel";
  public static final String COMMON_OR = "common_or";
  public static final String COMMON_DELETE = "common_delete";
  public static final String COMMON_STOPS = "common_stops";
  public static final String COMMON_SHOW_MORE = "common_labelshowmore";
  public static final String COMMON_SHOW_LESS = "common_labelshowless";
  public static final String COMMON_CHECK = "paymentviewcontroller_button_check";
  public static final String COMMON_YES = "common_yes";
  public static final String COMMON_NO = "common_no";
  //Unknown error
  public static final String SSO_ERROR_WRONG = "sso_error_wrong";
  public static final String MY_TRIPS_IMPORT_ERROR = "importtrip_hud_importerror";
  public static final String MY_TRIPS_IMPORT_FINFING_BOOKING = "importtrip_hud_findingbooking";
  public static final String MY_TRIPS_IMPORT_MANUAL_TITLE = "importtrip_table_header";
  public static final String MY_TRIPS_IMPORT_MANUAL_DESCRIPTION = "importtrip_informativetext_text";
  public static final String MY_TRIPS_IMPORT_MANUAL_BUTTON = "importtrip_button_addtomytrips";
  public static final String MY_TRIPS_IMPORT_MANUAL_EMAIL_HINT = "sharekit_noaccountemail_title";
  public static final String MY_TRIPS_IMPORT_MANUAL_BOOKING_ID_HINT =
      "formfieldrow_placeholder_reservation_number";
  public static final String MY_TRIPS_IMPORT_ERROR_EMAIL_VALIDATION = "validation_error_mail";
  public static final String MY_TRIPS_IMPORT_ERROR_BOOKING_ID_VALIDATION =
      "validation_error_reservation_number";
  public static final String MY_TRIPS_DELETE_TRIP = "mytripsviewcontroller_deletetrip";
  public static final String MY_TRIPS_LIST_UPCOMING_TRIPS = "mytripsviewcontroller_upcomingtrips";
  public static final String MY_TRIPS_LIST_PAST_TRIPS = "mytripsviewcontroller_pasttrips";
  public static final String MY_TRIPS_LIST_CARD_TITLE_TRIP =
      "mytripsviewcontroller_card_title_trip";
  public static final String MY_TRIPS_LIST_CARD_TITLE_FLIGHT =
      "mytripsviewcontroller_card_title_flight";
  public static final String MY_TRIPS_LIST_CARD_TITLE_HOTEL =
      "mytripsviewcontroller_card_title_hotel";
  public static final String MY_TRIPS_BOOKING_STATUS_PENDING_MESSAGE =
      "checkbookingstatus_pending_message";
  public static final String MY_TRIPS_LIST_STATUS_PENDING = "checkbookingstatus_pending_short";
  public static final String MY_TRIPS_LIST_STATUS_CANCEL = "checkbookingstatus_cancelled_short";
  public static final String MY_TRIPS_LIST_STATUS_CONFIRM = "checkbookingstatus_confirmed_short";
  public static final String MY_TRIPS_FLIGHT_CARD_TITLE_OUTBOUND = "common_outbound";
  public static final String MY_TRIPS_FLIGHT_CARD_TITLE_INBOUND = "common_inbound";
  public static final String MY_TRIPS_FLIGHT_CARD_SCALES = "mytrips_flight_card_scales";
  public static final String MY_TRIPS_FLIGHT_CARD_DIRECT = "mytrips_flight_card_direct";
  public static final String MY_TRIPS_FLIGHT_CARD_DEPARTURE = "mytrips_flight_card_departure";
  public static final String MY_TRIPS_FLIGHT_CARD_ARRIVAL = "mytrips_flight_card_arrival";
  public static final String MY_TRIPS_FLIGHT_CARD_BTN_DETAILS =
      "mytrips_flight_card_btn_see_details";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_TITLE = "mytrips_details_trips_card_title";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_BOOKING_REFERENCE =
      "mytrips_details_trips_card_booking_reference";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_BRAND_BOOKING_REFERENCE =
      "mytrips_details_trips_card_brand_booking_reference";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_AIRLINE_REFERENTE =
      "mytrips_details_trips_card_airline_reference";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_PASSENGERS_INFORMATION =
      "mytrips_details_trips_card_passengers_information";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_ADULT = "mytrips_details_trips_card_adult";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_CHILD = "mytrips_details_trips_card_child";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_BABY = "mytrips_details_trips_card_baby";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_BAGGAGE_INCLUDED_EACH_WAY =
      "mytrips_details_trips_card_baggage_included_each_way";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_BAGGAGE_INCLUDED =
      "mytrips_details_trips_card_baggage_included";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_INSURANCE_DETAILS =
      "mytrips_details_trips_card_insurance_details";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_PASSENGERS =
      "mytrips_details_trips_card_passengers";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_BTN_CONDITIONS =
      "mytrips_details_trips_card_btn_see_conditions";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_FLIGHT_CANCELATION =
      "mytrips_details_trips_card_flight_cancellation";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_BILLING_INFORMATION =
      "mytrips_details_trips_card_billing_informations";
  public static final String MY_TRIPS_DETAILS_TRIP_CARD_BUYER = "mytrips_details_trips_card_buyer";
  public static final String MY_TRIPS_DETAILS_TRIP_EMAIL_ADDRESS =
      "mytrips_details_trips_card_email_address";
  public static final String MY_TRIPS_DETAILS_TRIP_PAYMENT_METHOD =
      "mytrips_details_trips_card_payment_method";
  public static final String MY_TRIPS_DETAILS_TRIP_TOTAL_PRICE =
      "mytrips_details_trips_card_total_price";
  public static final String MY_TRIPS_DETAIL_HELPCENTER_TITLE = "mytrips_detail_helpcenter_title";
  public static final String MY_TRIPS_DETAIL_HELPCENTER_SUBTITLE =
      "mytrips_detail_helpcenter_subtitle";

  public static final String MY_TRIPS_CROSS_TITLE = "mytrips_details_cross_title";
  public static final String MY_TRIPS_CROSS_DESCRIPTION = "mytrips_details_cross_description";
  public static final String MY_TRIPS_CROSS_BTN_HOTEL = "mytrips_details_cross_btn_book_hotel";
  public static final String MY_TRIPS_CROSS_BTN_CAR = "mytrips_details_cross_btn_rent_car";
  public static final String MY_TRIPS_EMPTY_TITLE =
      "mytripsviewcontroller_emptyview_highlightedtext";
  public static final String MY_TRIPS_EMPTY_DESCRIPTION =
      "mytripsviewcontroller_emptyview_descriptivetext";
  public static final String MY_TRIPS_SEARCH_BUTTON = "searchviewcontroller_searchbtn_text";
  public static final String MY_TRIPS_IMPORT_TRIP = "importtrip_button_addbooking";
  public static final String MY_TRIPS_LOG_IN_BUTTON = "mytripsviewcontroller_button_login";
  // Frequent Flyer
  public static final String FREQUENT_FLYER_TITLE = "frequentflyerviewcontroller_emptyscreen_title";
  public static final String FREQUENT_FLYER_AIRLINE = "validation_error_frequent_flyer_airline";
  public static final String FREQUENT_FLYER_DELETED = "frequentflyer_deleted";
  public static final String FREQUENT_FLYER_UNDO = "frequentflyer_undo";
  public static final String FREQUENT_FLYER_NEW = "frequentflyer_new";
  public static final String FREQUENT_FLYER_EDIT = "frequentflyer_edit";
  public static final String FREQUENT_FLYER_OPTIONAL =
      "formfieldrow_placeholder_frequent_flyer_group_optional";
  public static final String FREQUENT_FLYER_CODES_LIMIT_MESSAGE =
      "frequentflyer_codes_limit_message";
  public static final String FREQUENT_FLYER_CODE_HINT =
      "formfieldrow_placeholder_frequent_flyer_code";
  // Errors for travellers/passengers
  public static final String VALIDATION_ERROR_NAME = "validation_error_name";
  public static final String VALIDATION_ERROR_SURNAME = "validation_error_surname1";
  public static final String VALIDATION_ERROR_SECOND_SURNAME = "validation_error_surname2";
  public static final String VALIDATION_ERROR_NATIONALITY =
      "validation_error_national_country_code";
  public static final String VALIDATION_ERROR_MAIL = "validation_error_mail";
  public static final String VALIDATION_ERROR_ADDRESS = "validation_error_address";
  public static final String VALIDATION_ERROR_POSTAL_CODE = "validation_error_postal_code";
  public static final String VALIDATION_ERROR_PHONE_NUMBER = "validation_error_phone_number";
  public static final String VALIDATION_ERROR_CITY_NAME = "validation_error_city_name";
  public static final String VALIDATION_ERROR_STATE_NAME = "validation_error_state_name";
  public static final String VALIDATION_ERROR_COUNTRY_CODE =
      "validation_error_country_code_of_residence";
  public static final String VALIDATION_ERROR_PHONE_CODE = "validation_error_phone_code";
  public static final String VALIDATION_ERROR_CPF = "validation_error_invalid_cpf";
  public static final String VALIDATION_ERROR_BIRTH_DATE = "validation_error_birth_date";
  public static final String VALIDATION_ERROR_IDENTIFICATION = "validation_error_identification";
  public static final String VALIDATION_ERROR_IDENTIFICATION_EXPIRATION =
      "validation_error_identification_expiration_date";
  public static final String VALIDATION_ERROR_IDENTIFICATION_COUNTRY =
      "validation_error_identification_issue_country_code";
  public static final String VALIDATION_ERROR_SELECTING_COUNTRIES =
      "selectviewcontroller_idissuecontrycode";
  // Hints for travellers/passengers
  public static final String HINT_NAME = "formfieldrow_placeholder_name";
  public static final String HINT_MIDDLE_NAME = "formfieldrow_placeholder_middle_name";
  public static final String HINT_FIRST_LASTNAME = "formfieldrow_placeholder_surname1";
  public static final String HINT_SECOND_LASTNAME = "formfieldrow_placeholder_surname2";
  public static final String HINT_NATIONALITY = "formfieldrow_placeholder_national_country_code";
  public static final String HINT_BIRTH_DATE = "formfieldrow_placeholder_birth_date";
  public static final String HINT_ID_CARD = "identification_formmodule_header_national_id_card";
  public static final String HINT_PASSPORT = "identification_formmodule_header_passport";
  public static final String HINT_PASSENGER_TOGGLE_DESCRIPTION =
      "formfieldrow_placeholder_traveller_main";
  public static final String HINT_IDENTIFICATION_SECTION =
      "formfieldrow_placeholder_identification_group";
  public static final String HINT_IDENTIFICATION = "formfieldrow_placeholder_identification";
  public static final String HINT_IDENTIFICATION_EXPIRATION_DATE =
      "formfieldrow_placeholder_identification_expiration_date";
  public static final String HINT_IDENTIFICATION_COUNTRY =
      "formfieldrow_placeholder_identification_country";
  public static final String HINT_RESIDENT_TITLE = "residentpickerviewcontroller_title";
  public static final String HINT_NIF = "identification_formmodule_header_nif";
  public static final String HINT_NIE = "identification_formmodule_header_nie";
  public static final String HINT_EMAIL = "formfieldrow_placeholder_mail";
  public static final String HINT_STATE = "formfieldrow_placeholder_state_name";
  public static final String HINT_CITY = "formfieldrow_placeholder_city_name";
  public static final String HINT_ADDRESS = "formfieldrow_placeholder_address";
  public static final String HINT_ZIP_CODE = "formfieldrow_placeholder_postal_code";
  public static final String HINT_COUNTRY_RESIDENCE = "selectviewcontroller_countryofresidence";
  public static final String HINT_CPF = "formfieldrow_placeholder_cpf";
  public static final String HINT_PHONE_PREFIX = "formfieldrow_placeholder_phone_code";
  public static final String HINT_PHONE = "formfieldrow_placeholder_phone_number";
  public static final String HINT_MOBILE_PHONE = "formfieldrow_placeholder_mobile_phone_number";
  public static final String HINT_FREQUENT_FLYER_CODE =
      "formfieldrow_placeholder_frequent_flyer_group";
  public static final String INCREASE_REPRICING_TICKET =
      "repricingwidening_flightrepricing_messagehigher";
  public static final String DECREASE_REPRICING_TICKET =
      "repricingwidening_flightrepricing_messagelower";
  // Insurances
  public static final String MANDATORY_INSURANCE_TITLE =
      "insurancesviewcontroller_mandatoryinsurance_insurancetitle";
  public static final String INSURANCESVIEWCONTROLLER_TITLE = "insurancesviewcontroller_title";
  public static final String INSURANCESVIEWCONTROLLER_INFORMATIVEMODULE_TEXT =
      "insurancesviewcontroller_informativemodule_text";
  public static final String INSURANCESVIEWCONTROLLER_CONDITIONSMODULE_TEXT =
      "insurancesviewcontroller_conditionsmodule_text";
  public static final String INSURANCE_WIDGET_DECLINE_TITLE =
      "insurancesviewcontroller_rejectinsurance_labeltitle";
  public static final String INSURANCE_WIDGET_DECLINE_DESCRIPTION =
      "insurancesviewcontroller_rejectinsurance_labeldescription";
  public static final String INSURANCE_ADD_INSURANCE_MESSAGE =
      "loadingviewcontroller_message_addinginsurance";
  public static final String INSURANCE_WIDGET_RECOMMENDED_BUBBLE_TEXT =
      "insurancemanager_insuranceitem_recommended_image_text";
  public static final String INSURANCE_PER_PASSENGER_MESSAGE = "common_per_passenger";
  public static final String INSURANCEMANAGER_REJECT_DESCRIPTION =
      "insurancemanager_reject_description";
  public static final String INSURANCEMANAGER_REJECT_DESCRIPTION_EXTRA =
      "insurancemanager_reject_description_extra";
  public static final String INSURANCE_VIEW_CONTROLLER = "insurancesviewcontroller_";
  public static final String INSURANCE_TITLE = "_title";
  public static final String INSURANCE_DESCRIPTION = "_description";
  public static final String INSURANCE_DESCRIPTION_SHORT = "_description_short";
  public static final String INSURANCE_DOCUMENT_LINK = "_document_link";
  public static final String INSURANCE_TEXT_CONDITIONS = "_text_conditions";
  public static final String INSURANCE_TOTAL_PRICE = "_total_price";
  public static final String INSURANCE_TOTAL_PRICE_DETAIL = "_total_price_detail";
  public static final String INSURANCE_CONDITIONS_ACCEPTANCE =
      "insurancesviewcontroller_condition_acceptance";
  public static final String INSURANCE_CONDITIONS_PDF = "insurancesviewcontroller_conditions_pdf";
  public static final String LABEL_TITLE_TYPE_PASSENGER =
      "formfieldrow_placeholder_type_of_passenger";
  public static final String LABEL_TITLE_TITLE_PASSENGER = "selectpicker_titlename";
  public static final String LABEL_TITLE_DEFAULT_TRAVELLER =
      "formfieldrow_placeholder_default_traveller";
  public static final String COMMON_BUTTONCONTINUE = "common_buttoncontinue";
  public static final String DATA_STORAGE_SAVE = "mydatadetailsviewcontroller_save";
  public static final String WALKTHROUGH_TITLE = "odgwalkthroughview_title_text";
  public static final String WALKTHROUGH_DONE_BUTTON_TEXT = "odgwalkthroughview_donebutton_text";
  public static final String WALKTHROUGH_PAYPAL_TITLE = "odgwalkthroughview_paypal_title";
  public static final String WALKTHROUGH_PAYPAL_DESCRIPTION = "odgwalkthroughview_paypal_desc";
  public static final String WALKTHROUGH_STORE_PAYMENT_TITLE =
      "odgwalkthroughview_store_method_title";
  public static final String WALKTHROUGH_STORE_PAYMENT_BODY =
      "odgwalkthroughview_store_method_body";
  public static final String IMPORT_TRIP_OK = "importtrip_hud_bookingadded";
  public static final String IMPORT_TRIP_ALREADY = "importtrip_hud_bookingalreadyimported";
  public static final String SSO_SIGNIN_HEADER = "sso_signin_header";
  //Identification View
  public static final String COUNTRY_SELECTED_ERROR = "selectviewcontroller_idissuecontrycode";
  public static final String CREDIT_CARD_EXPIRATION_DATE =
      "formfieldrow_placeholder_credit_card_expiration_date";
  public static final String HEADER_NATIONAL_ID =
      "identification_formmodule_header_national_id_card";
  //Payments
  public static final String LOADING_MESSAGE_PAYING = "waitingview_booking";
  public static final String ALERT_BUY_BUTTON_TITLE = "repricingalert_buybutton_title";
  public static final String PAYMENT_CONTROLLER_TITLE = "paymentviewcontroller_title";
  public static final String PAYMENT_CONDITIONS_IMPLICIT_ACCEPTANCE =
      "payment_conditions_implicitacceptance";
  public static final String COMMON_TRIP_DETAILS = "common_tripdetails";
  public static final String HINT_CREDIT_CARD_NUMBER =
      "formfieldrow_placeholder_credit_card_number";
  public static final String HINT_CREDIT_CARD_NAME = "formfieldrow_placeholder_credit_card_name";
  public static final String HINT_CREDIT_CARD_CVV = "formfieldrow_placeholder_ccv_number";
  public static final String REPRICING_ALERT_MESSAGE = "repricingalert_repricingmessage";
  public static final String REPRICING_ALERT_TITLE = "repricingalert_repricingtitle";
  public static final String PAYMENT_FORMMODULE_HEADER = "payment_formmodule_header";
  public static final String PAYMENTVIEWCONTROLLER_INFORMATIVEMODULE_TEXT =
      "paymentviewcontroller_informativemodule_text";
  public static final String HINT_VALIDATION_ERROR_CVV = "validation_error_ccv_number";
  public static final String BANKTRANSFER = "formfieldrow_placeholder_bank_transfer";
  public static final String CREDITCARD_WIDGET = "paymentmanager_creditcard_method";
  public static final String CREDITCARD = "formfieldrow_placeholder_select_credit_card";
  public static final String CREDIT_CARD_EXPIRATION_MONTH = "formfieldrow_placeholder_expiry_month";
  public static final String CREDIT_CARD_EXPIRATION_YEAR = "formfieldrow_placeholder_expiry_year";
  public static final String BANKTRANSFER_FISRT_CONTAINER =
      "banktransfer_informationmodule_firststep";
  public static final String BANKTRANSFER_SECOND_CONTAINER =
      "banktransfer_informationmodule_secondstep";
  public static final String BANKTRANSFER_THIRD_CONTAINER =
      "banktransfer_informationmodule_thirdstep";
  public static final String PROMO_CODE_TITLE = "promocode_titlemaincell";
  public static final String PROMO_CODE_PLACEHOLDER = "promocode_textfield_placeholder";
  public static final String PROMO_CODE_SPECIAL_CONDITIONS_2 = "promocode_specialconditions_text2";
  public static final String PROMO_CODE_TERMS_AND_CONDITIONS = "promocode_titlecondtionscell";
  public static final String PROMO_CODE_BUTTON_TITLE_VALIDATE = "promocode_buttontitlevalidate";
  public static final String PROMO_CODE_DELETE_MESSAGE = "promocode_deletemessage";
  public static final String PROMO_CODE_BUTTON_EXPAND = "promocode_button_expand";
  public static final String PAYMENT_ALERT_COUPON_TITLE = "payment_alert_cupon_title";
  public static final String PAYMENT_CVV_TOOLTIP = "payment_cvv_tooltip";
  public static final String PAYMENT_CVV_TOOLTIP_AMEX = "payment_cvv_tooltip_amex";
  public static final String PAYMENY_CONDITIONS_EXPLICIT_ACCEPTANCE =
      "payment_conditions_explicitacceptance";
  public static final String TERMS_AND_CONDITIONS =
      "paymentviewcontroller_terms_and_conditions_title";
  public static final String AIRLINE_CONDITIONS = "paymentviewcontroller_airlines_conditions_title";
  public static final String SAVED_PAYMENT_METHOD_NOT_ACCEPTED =
      "paymentviewcontroller_saved_method_not_accepted";
  public static final String SAVED_PAYMENT_METHOD_EXPIRED =
      "paymentviewcontroller_saved_method_expired";
  public static final String SAVED_PAYMENT_METHOD_CARD = "paymentmethod_card_";
  public static final String SAVED_PAYMENT_METHOD_STORE_INFO =
      "paymentviewcontroller_store_payment_method";
  //Paypal
  public static final String PAYPAL_INFORMATION = "paymentmanager_paypal_method_information";
  public static final String PAYPAL_PAY_SECURELY = "paymentmanager_paypal_method_header";
  public static final String PAYPAL_CANCELATION = "webviewcontroller_paypal_close";
  //Trusly
  public static final String TRUSLY_INFORMATION = "paymentmanager_trustly_method_information";
  public static final String TRUSLY_PAY_SECURELY = "paymentmanager_trustly_method_header";
  //Klarna
  public static final String KLARNA_INFORMATION = "paymentmanager_klarna_method_information";
  public static final String KLARNA_PAY_SECURELY = "paymentmanager_klarna_method_header";
  //Error payment
  public static final String VALIDATION_ERROR_CREDIT_CARD = "validation_error_credit_card_number";
  public static final String VALIDATION_ERROR_CREDIT_CARD_NAME =
      "validation_error_credit_card_name";
  //Travel Guides
  public static final String TRAVELGUIDE_PDF_DOWNLOAD_TITLE = "travelguide_pdf_download_title";
  public static final String TRAVELGUIDE_PDF_EN_ONLY = "travelguide_pdf_en_only";
  public static final String TRAVELGUIDE_PDF_DOWNLOAD_BUTTON = "travelguide_pdf_download_button";
  public static final String TRAVELGUIDE_PDF_DOWNLOAD_DELETE_BUTTON =
      "travelguide_pdf_download_delete_button";
  public static final String TRAVELGUIDE_PDF_DOWNLOAD_OPEN_BUTTON =
      "travelguide_pdf_download_open_button";
  public static final String TRAVELGUIDE_PDF_DOWNLOAD_IN_PROGRESS =
      "travelguide_pdf_download_in_progress";
  public static final String TRAVELGUIDE_DELETE_WARNING = "travelguide_delete_warning";
  //No connection
  public static final String ERROR_NO_CONNECTION_TITLE = "noconnectioninformationview_title";
  public static final String ERROR_NO_CONNECTION_SUBTITLE = "noconnectioninformationview_subtitle";
  public static final String ERROR_NO_CONNECTION_TRY_AGAIN =
      "noconnectioninformationview_textbutton";
  //Travel Guides Errors
  public static final String TRAVEL_GUIDE_DOWNLOAD_FAIL_TITLE =
      "travelguide_error_download_fail_title";
  public static final String TRAVEL_GUIDE_DOWNLOAD_FAIL_TEXT =
      "travelguide_error_download_fail_text";
  public static final String TRAVEL_GUIDE_DOWNLOAD_FAIL_TRY_AGAIN =
      "travelguide_error_download_fail_button";
  public static final String TRAVEL_GUIDE_INSUFFICIENT_SPACE_TITLE =
      "travelguide_error_download_space_title";
  public static final String TRAVEL_GUIDE_INSUFFICIENT_SPACE_TEXT =
      "travelguide_error_download_space_text";
  public static final String TRAVEL_GUIDE_INSUFFICIENT_SPACE_STORAGE =
      "travelguide_error_download_space_button";
  //Destination
  public static final String LOCATIONS_SEARCH_BAR_RECENT_ORIGINS =
      "locationsviewcontroller_searchbar_recent_origins_placeholder";
  public static final String LOCATIONS_SEARCH_BAR_RECENT_DESTINATIONS =
      "locationsviewcontroller_searchbar_recent_destinations_placeholder";
  public static final String CANNOT_FIND_LOCATIONS = "string_new_cannot_find_locations";
  //HOME WIDGET
  public static final String FLIGHT_NUMBER = "flight_number";
  public static final String ODGWIDGETVIRGINSIMPLEVIEW_TITLE = "odgwidgetvirginsimpleview_title";
  public static final String ODGWIDGETVIRGINSIMPLEVIEW_SEARCH = "odgwidgetvirginsimpleview_search";
  public static final String WIDGET_FLIGHT_NUMBER = "widget_flight_number";
  public static final String EVENTCALENDAR_TAKEOFF =
      "confirmationviewcontroller_eventcalendar_takeoff";
  public static final String EVENTCALENDAR_LANDINGAT =
      "confirmationviewcontroller_eventcalendar_landingat";
  public static final String MYTRIPSVIEWCONTROLLER_DEPARTURE = "mytripsviewcontroller_departure";
  public static final String EVENTCALENDAR_ARRIVAL =
      "confirmationviewcontroller_eventcalendar_arrival";
  public static final String LATESTSEARCHES = "odgwidgetvirginsimpleview_latestsearches";
  public static final String DISCOVER = "odgwidgetvirginsimpleview_discover";
  // Filters
  public static final String SCALES_FILTER_VIEW_CONTROLLER_1_SCALES =
      "scalesfilterviewcontroller_1scales_text";
  public static final String WAITINGVIEW_AIRLINESCOUNTER_SUBTITLE =
      "waitingview_airlinescounter_subtitle";
  //Search view
  public static final String SEARCHVIEWCONTROLLER_LABELHEADER_TXT =
      "searchviewcontroller_labelheader_txt";
  public static final String NORESULTSSEARCHORFILTERSCELL_LABELRESETFILTERS_TEXT =
      "noresultssearchorfilterscell_labelresetfilters_text";
  public static final String NORESULTSSEARCHORFILTERSCELL_LABELRESETSEARCH_TEXT =
      "noresultssearchorfilterscell_labelresetsearch_text";
  public static final String ROUND_TRIP_TAB_TEXT = "common_loadroundtrip";
  public static final String ONE_WAY_TAB_TEXT = "common_loadonewaytrip";
  public static final String MULTI_TRIP_TAB_TEXT = "common_loadmultitrip";
  public static final String RESIDENTS_DIALOG_TEXT = "searchviewcontroller_alertview_text2";
  public static final String SEARCHVIEWCONTROLLER_HISTORY_DELETE_ALL =
      "searchviewcontroller_historic_deleteall";
  public static final String SEARCHVIEWCONTROLLER_TITLE = "searchviewcontroller_title";
  public static final String SEARCHVIEWCONTROLLER_GROWTH_WARNING_BABY_MESSAGE =
      "searchviewcontroller_growthwarning_babymessage";
  public static final String SEARCHVIEWCONTROLLER_GROWTH_WARNING_BABIES_MESSAGE =
      "searchviewcontroller_growthwarning_babiesmessage";
  public static final String SEARCHVIEWCONTROLLER_GROWTH_WARNING_CHILD_MESSAGE =
      "searchviewcontroller_growthwarning_childmessage";
  public static final String SEARCHVIEWCONTROLLER_GROWTH_WARNING_CHILDREN_MESSAGE =
      "searchviewcontroller_growthwarning_childrenmessage";
  public static final String SEARCHVIEWCONTROLLER_GROWTH_WARNING_MODIFY_OR_CONTINUE =
      "searchviewcontroller_growthwarning_modifyorcontinue";
  public static final String SEARCHVIEWCONTROLLER_GROWTH_WARNING_TITLE =
      "searchviewcontroller_growthwarning_title";
  public static final String SEARCHVIEWCONTROLLER_GROWTH_WARNING_CANCEL_BUTTON =
      "searchviewcontroller_growthwarning_cancelbutton";
  public static final String SEARCHVIEWCONTROLLER_GROWTH_WARNING_CONTINUE_BUTTON =
      "searchviewcontroller_growthwarning_continuebutton";
  public static final String RESIDENTACREDITATIONMODULE_ALERT_DIALOG_TITLE =
      "residentacreditationmodule_alert_dialog_title";
  public static final String RESIDENTACREDITATIONMODULE_ALERT_DIALOG_MESSAGE =
      "residentacreditationmodule_alert_dialog_message";
  //Home View
  public static final String FLIGHTS_TITLE = "mainmenucelltitle_home_option_flight";
  public static final String FLIGHTS_SUBTITLE = "mainmenucellsubtitle_home_option_flight";
  public static final String HOTELS_TITLE = "mainmenucelltitle_home_option_hotel";
  public static final String HOTELS_SUBTITLE = "mainmenucellsubtitle_home_option_hotel";
  public static final String CARS_TITLE = "mainmenucelltitle_home_option_car";
  public static final String CARS_SUBTITLE = "mainmenucellsubtitle_home_option_car";
  public static final String NO_TRIPS_SECONDARY_MESSAGE = "slidehome_notrip_secondarymessage";
  public static final String NO_TRIPS_MAIN_MESSAGE = "slidehome_notrip_mainmessage";
  public static final String LAST_MINUTE_TITLE = "mainmenucelltitle_home_option_lastminute";
  public static final String PACKS_TITLE = "mainmenucelltitle_home_option_packs";
  public static final String NO_TRIPS_MESSAGE = "homeviewcontroller_messageoverimage";
  public static final String DROP_OFF_DESCRIPTION = "homeviewcontroller_dropoff_description";
  public static final String DROP_OFF_SEARCH_LAUNCH = "homeviewcontroller_dropoff_search_launch";
  public static final String DROP_OFF_SEARCH_EDIT = "homeviewcontroller_dropoff_search_edit";
  public static final String DROP_OFF_NUMBER_OF_DAYS = "homeviewcontroller_dropoff_number_of_days";
  //Cards Home View
  public static final String CARDS_TITLE = "cards_title";
  public static final String CARDS_ORIGIN_FROM = "cards_origin_from";
  public static final String CARDS_TIME = "cards_time";
  public static final String CARDS_DESTINATION_TO = "cards_destination_to";
  public static final String CARDS_TERMINAL = "cards_terminal";
  public static final String CARDS_LASTUPDATE = "cards_lastupdate";
  public static final String CARDS_GATE = "cards_gate";
  public static final String CARDS_BELT = "cards_belt";
  public static final String CARDS_STATUS_DIVERTED_TITLE = "cards_status_diverted_title";
  public static final String CARDS_STATUS_DIVERTED_MSG = "cards_status_diverted_msg";
  public static final String CARDS_STATUS_CANCELLED = "cards_status_cancelled";
  public static final String CARDS_STATUS_CANCELLED_MSG = "cards_status_cancelled_msg";
  public static final String CARDS_ONTIME = "cards_ontime";
  public static final String CARDS_DELAYED = "cards_delayed";
  public static final String CARDS_UPDATING = "cards_updating";
  // Full Price
  public static final String FULL_PRICE_BOTTOM_TITLE = "fullprice_bottom_title";
  public static final String FULL_PRICE_PICKER_WILLPAY = "fullprice_picker_willpay";
  public static final String FULL_PRICE_SUMMARY_HEADER_NEW = "fullprice_summary_header_new";
  public static final String FULL_PRICE_RESULTS_HEADER_NEW = "fullprice_results_header_new";
  //Price breakdown
  public static final String PRICING_BREAKDOWN_TOTAL_PRICE = "pricingbreakdown_totalprice";
  public static final String PRICING_BREAKDOWN_TOTAL_PRICE_SUBTITLE =
      "fullprice_summary_pricebreakdown_new";
  public static final String PRICINGBREAKDOWN_PRICEBREAKDOWN = "pricingbreakdown_pricebreakdown";
  public static final String CROSSSELLINGMODULE_MYTRIPS_INFORMATION =
      "crosssellingmodule_mytrips_information";
  public static final String PRICING_LEGAL_CONDITIONS_DEFAULT =
      "pricinglegalconditions_price_conditions_default";
  public static final String PRICING_LEGAL_CONDITIONS_AE =
      "pricinglegalconditions_price_conditions_ae";
  public static final String PRICING_BREAKDOWN_SERVICE_CHARGES = "pricingbreakdown_service_charges";
  public static final String PRICING_BREAKDOWN_PROMOCODE_DISCOUNT =
      "pricingbreakdown_promocode_discount";
  public static final String PRICING_BREAKDOWN_HANDLING_FEE = "pricingbreakdown_handling_fee";
  public static final String PRICING_BREAKDOWN_PAYMENT_METHOD_PRICE_CREDITCARD =
      "pricingbreakdown_payment_method_price_creditcard";
  public static final String PRICING_BREAKDOWN_ONE_SUITCASE_PRICE =
      "pricingbreakdown_onesuitcaseprice";
  public static final String PRICING_BREAKDOWN_SEVERAL_SUITCASES_PRICE =
      "pricingbreakdown_severalsuitcasesprice";
  public static final String PRICING_BREAKDOWN_PREMIUM_DISCOUNT_APPLIED =
      "pricingbreakdown_premium_discount_applied";
  public static final String PRICING_BREAKDOWN_PREMIUM_DISCOUNT_NOT_APPLIED =
      "pricingbreakdown_premium_discount_not_applied";
  public static final String PRICING_BREAKDOWN_MEMBER_MUST_BE_PAX =
      "pricingbreakdown_user_must_be_pax";
  public static final String PRICING_BREAKDOWN_MEMBERSHIP_PERKS =
      "pricingbreakdown_membership_perks";
  public static final String FULLPRICE_TAX_REFUNDABLE_INFO = "fullprice_taxrefundableinfo";
  //Confirmation screen
  public static final String CONFIRMATION_WHATSNEXT_HEADERTITLE =
      "confirmation_whatsnext_headertitle";
  public static final String ADDITIONALINFOMODULE_HEADER_LABELTITLE =
      "additionalinfomodule_header_labeltitle";
  public static final String INFOSTATUS_TITLE_CONTRACT =
      "confirmationviewcontroller_infostatus_title_contract";
  public static final String INFOSTATUS_TITLE_PENDING =
      "confirmationviewcontroller_infostatus_title_pending";
  public static final String INFOSTATUS_TITLE_REJECT =
      "confirmationviewcontroller_infostatus_title_reject";
  public static final String INFOSTATUS_TITLE = "confirmationviewcontroller_title";
  public static final String ADD_TO_CALENDAR = "confirmationviewcontroller_addcalendar_button";
  //Buyer
  public static final String BUYERDETAILSMODULE_HEADER_LABELTITLE =
      "buyerdetailsmodule_header_labeltitle";

  public static final String BUYERDETAILSMODULE_CONFIRMATION_MAIL_LABEL_TITLE =
      "buyerdetailsmodule_confirmationmail_labeltitle";
  //Marshmallows permissions requests
  public static final String PERMISSION_LOCATION_AIRPORT_MESSAGE =
      "permission_location_airport_message";
  public static final String PERMISSION_PHONE_CONTACTUS_MESSAGE =
      "permission_phone_contactus_message";
  public static final String PERMISSION_CALENDAR_BOOKING_MESSAGE =
      "permission_calendar_booking_message";
  public static final String PERMISSION_ACCOUNT_SIGNIN_MESSAGE =
      "permission_account_signin_message";
  public static final String PERMISSION_ACCOUNT_REGISTER_MESSAGE =
      "permission_account_register_message";
  public static final String PERMISSION_STORAGE_TRAVELGUIDE_MESSAGE =
      "permission_storage_travelguide_message";
  public static final String ALERT_NOTIFICATIONS_SETTINGS = "alert_notifications_settings";
  //WebView
  public static final String LOADINGVIEWCONTROLLER_MESSAGE_LOADING =
      "loadingviewcontroller_message_loading";
  public static final String STRING_NEW_CANNOT_OPEN_WEB_PAGE = "string_new_cannot_open_web_page";
  public static final String WEBVIEWCONTROLLER_ALERTVIEW_DESCRIPTION =
      "webviewcontroller_alertview_description";
  public static final String WEBVIEWCONTROLLER_CARS_HOME_URL_ANDROID =
      "webviewcontroller_cars_home_url_android";
  public static final String WEBVIEWCONTROLLER_GROUND_TRANSPORTATION_TITLE =
      "webviewcontroller_android_ground_transportation_title";
  //Summary
  public static final String DETAILSVIEWCONTROLLER_INFORMATIVEMODULE_TEXT =
      "detailsviewcontroller_informativemodule_text";
  public static final String CO2TERMSANDCONDITIONS_TEXT = "co2termsandconditions_text";
  public static final String CROSSSELLINGMODULE_INFORMATIVETITLE =
      "crosssellingmodule_informativetitle";
  public static final String SUMMARY_TITLE = "summaryviewcontroller_title";
  //FAQ
  public static final String FAQ_URL_FLIGHT =
      "aboutoptionsmodule_about_option_questions_flights_url";
  public static final String FAQ_URL_ABOUT_BRAND =
      "aboutoptionsmodule_about_option_about_brand_url";
  public static final String FAQ_URL_ABOUT_APP = "aboutoptionsmodule_about_option_about_app_url";
  public static final String FAQ_URL_SECURE_SHOPPING =
      "aboutoptionsmodule_about_option_secure_shopping_url";
  public static final String ABOUTOPTIONSMODULE_ABOUT_OPTION_FAQ =
      "aboutoptionsmodule_about_option_faq";
  public static final String FAQ_BUTTON_FLIGHT =
      "aboutoptionsmodule_about_option_questions_flights";
  public static final String FAQ_BUTTON_BRAND = "aboutoptionsmodule_about_option_about_brand";
  public static final String FAQ_BUTTON_APP = "aboutoptionsmodule_about_option_about_app";
  public static final String FAQ_BUTTON_SECURE = "aboutoptionsmodule_about_option_secure_shopping";
  public static final String FAQ_FOOTER_LEGAL = "faqviewcontroller_legaltextfooter";
  //Searching
  public static final String WAITINGVIEW_INFORMATIVEMESSAGE = "waitingview_informativemessage";
  //MyTripDetails
  public static final String TRIP_DETAILS_DURATION = "tripdetailsviewcontroller_tripduration";
  public static final String CELLSTOP_LABEL_CHANGEPLANE = "cellstop_labelchangeplane";
  public static final String STOPCELL_STOP_DURATION = "stopcell_stopduration";
  public static final String CELLSTOP_LABEL_WARNIG = "cellstop_labelwarnig";
  public static final String CARDS_STATUS_DELAYED = "cards_status_delayed";
  public static final String VIEWCONTROLLER_BUYER_PHONECODE =
      "selectviewcontroller_buyer_phonecode";
  public static final String VIEWCONTROLLER_COUNTRY = "selectviewcontroller_country";
  public static final String COMMON_FLIGHT = "common_flight";
  public static final String TRIP_DETAILS_GROUND_TRANSPORTATION_TITLE =
      "mytrips_detail_ground_transports_title";
  public static final String TRIP_DETAILS_GROUND_TRANSPORTATION_MESSAGE =
      "mytrips_detail_ground_transports_message";
  public static final String TRIP_DETAILS_GROUND_TRANSPORTATION_BUTTON =
      "mytrips_detail_ground_transports_button";
  public static final String TRIP_DETAILS_GROUND_TRANSPORTATION_IMAGE_LINK =
      "mytrips_detail_ground_transports_image_link";
  public static final String TRIP_DETAILS_GROUND_TRANSPORTATION_URL =
      "mytrips_android_detail_ground_transports_url";
  public static final String TRIP_DETAILS_GROUND_TRANSPORTATION_ACTIVE =
      "mytrips_detail_ground_transports_active";
  //ConfirmationPage
  public static final String CONFIRMATION_GROUND_TRANSPORTATION_TITLE =
      "confirmationxselling_confirmation_ground_transports_title";
  public static final String CONFIRMATION_GROUND_TRANSPORTATION_MESSAGE =
      "confirmationxselling_confirmation_ground_transports_message";
  public static final String CONFIRMATION_GROUND_TRANSPORTATION_BUTTON =
      "confirmationxselling_confirmation_ground_transports_button";
  public static final String CONFIRMATION_GROUND_TRANSPORTATION_IMAGE_LINK =
      "confirmationxselling_confirmation_ground_transports_image_link";
  public static final String CONFIRMATION_GROUND_TRANSPORTATION_URL =
      "confirmationxselling_android_confirmation_ground_transports_url";
  public static final String CONFIRMATION_GROUND_TRANSPORTATION_ACTIVE =
      "confirmationxselling_confirmation_ground_transports_active";
  //Frequent flyer
  public static final String CATEGORY_MY_AREA_PAX_DETAILS = "my_area_pax_details";
  public static final String ACTION_FREQUENT_FLYER = "frequent_flyer";
  public static final String LABEL_FREQUENT_FLYER_CODE_SAVED = "frequent_flyer_code_saved";
  //MonthYearPicker
  public static final String PASSENGER_PICKER_OK_BTN = "passengerpicker_okbtn_titlelabel_text";
  //FullTransparency
  public static final String FULLPRICE_LAYER_TEXT_1_NEW = "fullprice_layer_text1_new";
  public static final String FULLPRICE_LAYER_TEXT_2_NEW = "fullprice_layer_text2_new";
  public static final String FULLPRICE_LAYER_TEXT_3_NEW = "fullprice_layer_text3_new";
  public static final String FULLPRICE_LAYER_TEXT = "fullprice_layer_text";
  public static final String FULLPRICE_LAYER_BUTTON_NEW = "fullprice_layer_button_new";
  public static final String FULLPRICE_LAYER_HEADER = "fullprice_layer_header";
  public static final String FULLPRICE_BUBBLE_MESSAGE = "fullprice_bubble_message_title_new";
  public static final String FULLPRICE_BUBBLE_ACTION = "fullprice_bubble_message_action";
  public static final String FULLPRICE_SUMMARY_PAYMENT_FEE = "fullprice_summary_paymentfee";
  public static final String FULLPRICE_SUMMARY_BRAND_REDUCTION = "fullprice_summary_brandreduction";
  //APP RATE
  public static final String LEAVEFEEDBACK_HEADER = "leavefeedback_header";
  // APP RATE THANKS SCREEN
  public static final String LEAVEFEEDBACK_THANKS_TITLE = "leavefeedback_thanks_title";
  public static final String LEAVEFEEDBACK_THANKS_SUBTITLE = "leavefeedback_thanks_subtitle";
  public static final String LEAVEFEEDBACK_THANKS_BUTTON = "leavefeedback_thanks_button";
  // APP RATE HELP IMPROVE SCREEN
  public static final String LEAVEFEEDBACK_GIVE_FEEDBACK_TITLE =
      "leavefeedback_give_feedback_title";
  public static final String LEAVEFEEDBACK_GIVE_FEEDBACK_SUBTITLE =
      "leavefeedback_give_feedback_subtitle";
  public static final String LEAVEFEEDBACK_SEND_FEEDBACK_BUTTON =
      "leavefeedback_send_feedback_button";
  public static final String LEAVEFEEDBACK_HELP_PLACEHOLDER = "leavefeedback_help_placeholder";
  public static final String HELPCENTER_UPLOAD_SCREENSHOT_BUTTON =
      "helpcenter_upload_screenshot_button";
  public static final String LEAVEFEEDBACK_HELP_TITLE = "leavefeedback_help_title";
  public static final String LEAVEFEEDBACK_WAITING_SENDING = "leavefeedback_waiting_sending";
  public static final String LEAVEFEEDBACK_SEND_FEEDBACK_OK = "leavefeedback_send_feedback_ok";
  public static final String LEAVEFEEDBACK_ACCESS_ALERT_INFO = "leavefeedback_access_alert_info";
  public static final String LEAVEFEEDBACK_UPLOAD_QUESTION = "leavefeedback_upload_question";
  public static final String LEAVEFEEDBACK_UPLOAD_BUTTON_CHANGE =
      "leavefeedback_upload_button_change";
  public static final String LEAVEFEEDBACK_UPLOAD_BUTTON_REMOVE =
      "leavefeedback_upload_button_remove";
  public static final String LEAVEFEEDBACK_HELP_IMPROVE_EMAIL_SUBJECT =
      "email_helpimprove_email_subject_commentsuggestion";
  public static final String HELPCENTER_SENDING_ERROR = "helpcenter_sending_error";
  //APP RATE YOUR APP EXPERIENCE
  public static final String ABOUTOPTIONSMODULE_ABOUT_OPTION_LEAVEFEEDBACK =
      "aboutoptionsmodule_about_option_leavefeedback";
  public static final String MYTRIPSVIEWCONTROLLER_RATE_TITLE = "mytripsviewcontroller_rate_title";
  public static final String MYTRIPSVIEWCONTROLLER_RATE_SUBTITLE =
      "mytripsviewcontroller_rate_subtitle";
  public static final String MYTRIPSVIEWCONTROLLER_RATE_BUTTON_GOOD =
      "mytripsviewcontroller_rate_button_good";
  public static final String MYTRIPSVIEWCONTROLLER_RATE_BUTTON_BAD =
      "mytripsviewcontroller_rate_button_bad";
  //SNACKBAR LATIN CHARSET ERROR
  public static final String ANDROID_ONLY_LATIN_CHARACTERS = "error_android_only_latin_characters";
  public static final String KEYBOARD_SELECT_MSG = "common_keyboard_select_msg";
  public static final String KEYBOARD_SELECT_BUTTON = "common_keyboard_select_button";
  public static final String STRING_NEW_THIRD_PARTY_APP_NEEDED =
      "string_new_third_party_app_needed";
  public static final String STRING_NEW_PDF_READER_NEEDED = "string_new_pdf_reader_needed";
  //Welcome message
  public static final String HOME_VIEM_CONTROLLER_HI_NAME = "homeviewcontroller_hi_name";
  //SHARE BOOKING
  public static final String SHARE_BOOKING_TITLE = "confirmationviewcontroller_share_header";
  public static final String SHARE_BOOKING_SUBTITLE = "confirmationviewcontroller_share_text";
  public static final String SHARE_BOOKING_BUTTON = "confirmationviewcontroller_share_button";
  public static final String SHARE_BOOKING_TWITTER_DESCRIPTION =
      "sharing_flights_confirmation_page_confirmed_twitter_body";
  public static final String SHARE_BOOKING_WHATSAPP_DESCRIPTION =
      "sharing_flights_confirmation_page_confirmed_whatsapp_description";
  public static final String SHARE_BOOKING_SMS_DESCRIPTION =
      "sharing_flights_confirmation_page_confirmed_imessage_description";
  public static final String SHARE_BOOKING_FB_MESSENGER_DESCRIPTION =
      "sharing_flights_confirmation_page_confirmed_fbmessenger_description";
  public static final String SHARE_BOOKING_CLIPBOARD_DESCRIPTION =
      "sharing_flights_confirmation_page_confirmed_copytoclipboard_description";
  public static final String SHARE_BOOKING_DIALOG_TITLE = "sharing_flightdetailsmytrips_title";
  public static final String SHARE_BOOKING_EMAIL_SUBJECT =
      "sharing_flights_confirmation_page_confirmed_email_subject";
  public static final String SHARE_BOOKING_EMAIL_BODY =
      "sharing_flights_confirmation_page_confirmed_email_body";
  public static final String SHARE_BOOKING_EMAIL_BODY_ONE_WAY =
      "sharing_flights_confirmation_page_confirmed_email_body_oneway";
  public static final String SHARE_BOOKING_DOWNLOAD_HYPERLINK_TEXT = "sharing_download_text";
  //BOOKING OUTDATED DIALOG
  public static final String BOOKING_OUTDATED_TITLE = "error_session_expired_title";
  public static final String BOOKING_OUTDATED_MESSAGE = "error_timeout";
  public static final String BOOKING_OUTDATED_RELAUNCH = "error_relaunch";
  public static final String BOOKING_OUTDATED_NEW_SEARCH = "error_newsearch";
  //Repricing
  public static final String REPRICINGWIDENING_BAGGAGEREPRICING_MESSAGE =
      "repricingwidening_baggagerepricing_message";
  public static final String REPRICINGWIDENING_FLIGHTREPRICING_MESSAGELOWER =
      "repricingwidening_flightrepricing_messagelower";
  public static final String REPRICINGWIDENING_FLIGHTREPRICING_MESSAGEHIGHER =
      "repricingwidening_flightrepricing_messagehigher";
  //Widening
  public static final String RESULTSVIEWCONTROLLER_WIDENINGFROMAIRPORT =
      "resultsviewcontroller_wideningfromairport";
  public static final String RESULTSVIEWCONTROLLER_WIDENINGTOAIRPORT =
      "resultsviewcontroller_wideningtoairport";
  public static final String RESULTSVIEWCONTROLLER_WIDENINGBOTHAIRPORTS =
      "resultsviewcontroller_wideningbothairports";
  public static final String RESULTSVIEWCONTROLLER_TITLE_RESULTS =
      "resultsviewcontroller_title_results";
  public static final String RESULTSVIEWCONTROLLER_PREMIUM_PRICE_PER_PAX =
      "resultsviewcontroller_premium_price_per_pax";
  //Swicth status flights notifications
  public static final String APP_SETTINGS_NOTIFICATIONS_FLIGHT_ACTION =
      "appsettings_notifications_flight_action";
  public static final String APP_SETTINGS_NOTIFICATIONS_FLIGHT_STATUS =
      "appsettings_notifications_flight_status";
  //Online checkin
  public static final String BOARDING_PASS_SECTION_TITLE = "boarding_pass_section_title";
  public static final String BOARDING_PASS_SECTION_SUBTITLE = "boarding_pass_section_subtitle";
  public static final String BOARDING_PASS_BUTTON = "boarding_pass_button";
  public static final String BOARDING_PASS_HELP_1 = "boarding_pass_help_1";
  public static final String BOARDING_PASS_HELP_2 = "boarding_pass_help_2";
  public static final String BOARDING_PASS_HELP_3 = "boarding_pass_help_3";
  public static final String BOARDING_PASS_MORE_HELP = "boarding_pass_more_help";
  public static final String BOARDING_PASS_NAVIGATION_TITLE = "boarding_pass_navigation_title";
  public static final String BOARDING_PASS_URL = "boarding_pass_url";
  //CONFIRMATION REJECT
  public static final String CONFIRMATIONVIEWCONTROLLER_PHONEPAYMENT_TITLE =
      "confirmationviewcontroller_phonepayment_title";
  public static final String CONFIRMATIONVIEWCONTROLLER_PHONEPAYMENT_SUBTITLE =
      "confirmationviewcontroller_phonepayment_subtitle";
  public static final String CONFIRMATIONVIEWCONTROLLER_PHONEPAYMENT_BUTTON =
      "confirmationviewcontroller_phonepayment_button";
  public static final String CONFIRMATIONVIEWCONTROLLER_PHONEPAYMENT_BUTTON_SUBTITLE =
      "confirmationviewcontroller_phonepayment_button_subtitle";
  public static final String CONFIRMATIONVIEWCONTROLLER_PHONEPAYMENT_PHONE_NUMBER =
      "confirmationviewcontroller_phonepayment_phone_number";
  //Expired session and payment error pop up
  public static final String PAYMENTVIEWCONTROLLER_PAYMENTRETRY_PHONE_NUMBER =
      "paymentviewcontroller_paymentretry_phone_number";
  public static final String ERROR_PAYMENT_TITLE = "paymentviewcontroller_title_paymentretry";
  public static final String ERROR_PAYMENT_SUBTITLE =
      "paymentviewcontroller_message_paymentretry_retry_with_phone";
  //Expired session and payment error pop up
  public static final String ERROR_SESSION_EXPIRED_SUBTITLE =
      "error_session_expired_retry_with_phone_information";
  public static final String ERROR_SESSION_EXPIRED_PHONE_BUTTON =
      "error_session_expired_retry_with_phone_button";
  public static final String ERROR_SESSION_EXPIRED_PHONE_NUMBER =
      "error_session_expired_phone_number";
  public static final String PAYMENT_METHOD_PHONE_PRICING_INFO =
      "paymentmethod_phone_number_pricing_information";
  //Baggage Allowance
  public static final String RESULTSVIEWCONTROLLER_BAGGAGE_INCLUDED =
      "resultsviewcontroller_baggage_included";
  public static final String RESULTSVIEWCONTROLLER_BAGGAGE_NOT_INCLUDED =
      "resultsviewcontroller_baggage_not_included";
  //Payment Secure Panel
  public static final String SECURE_SHOPPING_FOOTER_TITLE = "secureshoppingfooter_title";
  public static final String SECURE_SHOPPING_FOOTER_SECURED_BY = "secureshoppingfooter_subtitle";
  // LOGIN AGAIN AUTH 000
  public static final String ALERT_NOTIFICATIONS_LOGIN_AGAIN = "alert_notifications_login_again";
  //Calendar Screen
  public static final String CALENDAR_DEPARTURE_TITLE = "timefilterviewcontroller_labeloutbound";
  public static final String CALENDAR_RETURN_TITLE = "timefilterviewcontroller_labelinbound";
  public static final String CALENDAR_HOLIDAYS_COUNTRY = "calendar_holidays_country";
  public static final String CALENDAR_HOLIDAYS_REGION = "calendar_holidays_region";
  public static final String CALENDAR_HOLIDAYS_COUNTRY_CAPTION =
      "calendar_holidays_country_caption";
  public static final String CALENDAR_HOLIDAYS_REGION_CAPTION = "calendar_holidays_region_caption";
  public static final String CALENDAR_EVENT_TITLE = "calendar_event_title";
  public static final String CALENDAR_EVENT_DESCRIPTION = "calendar_event_description";
  public static final String CALENDAR_ONE_WAY_TRIP = "calendar_one_way_trip";
  public static final String CALENDAR_SKIP_RETURN = "calendar_skip_return";
  public static final String CALENDAR_PICK_YOUR_DATES = "calendar_pick_your_dates";
  public static final String CALENDAR_PICK_YOUR_DATE = "calendar_pick_your_date";
  public static final String CALENDAR_SELECT_DATE = "calendar_select_date";
  public static final String CALENDAR_ERROR_CHOOSE_DATE_BETWEEN =
      "calendar_error_choose_date_between";
  public static final String CALENDAR_ERROR_CHOOSE_DATE_AFTER = "calendar_error_choose_date_after";
  public static final String CONFIRMATIONVIEWCONTROLLER_EVENTADDTOCALENDAROKTITLE =
      "confirmationviewcontroller_eventaddedtocalendaroktitle";
  public static final String CONFIRMATIONVIEWCONTROLLER_EVENTADDTOCALENDARKOTITLE =
      "confirmationviewcontroller_eventaddedtocalendarkotitle";
  //Payment Purchase Widget
  public static final String PAYMENT_PURCHASE_WIDGET_TITLE =
      "insurancesviewcontroller_mandatoryinsurance_webviewtitle";
  //DUPLICATE BOOKINGS
  public static final String CONFIRMATION_DUPLICATE_BOOKING_INFORMATION_TITLE =
      "duplicatebooking_information_title";
  public static final String CONFIRMATION_DUPLICATE_BOOKING_INFORMATION_SUBTITLE =
      "duplicatebooking_information_subtitle";
  public static final String DUPLICATE_BOOKING_WHATS_NEXT_SUBTITLE_CONFIRMED =
      "duplicatebooking_whatsnext_subtitle_confirmed";
  // Templates
  public static final String TEMPLATE_TIME_DURATION = "templates_time_duration";
  public static final String TEMPLATE_DATELONG1 = "templates_datelong1";
}
