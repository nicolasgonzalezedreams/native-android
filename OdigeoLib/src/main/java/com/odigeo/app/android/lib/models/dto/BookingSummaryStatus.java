package com.odigeo.app.android.lib.models.dto;

/**
 * Possible booking status, this are used in bookigstatus web service
 *
 * @author M.Sc. Javier Silva Perez
 * @since 22/04/15.
 */
public enum BookingSummaryStatus {

  OK, PENDING, KO;

  public static BookingSummaryStatus fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
