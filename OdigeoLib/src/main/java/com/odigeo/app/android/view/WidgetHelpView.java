package com.odigeo.app.android.view;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoWebViewActivity;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.view.constants.OneCMSKeys;

import static com.odigeo.app.android.lib.utils.HtmlUtils.underlineUsingHref;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_OPEN_HELP_CENTER;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_MY_TRIPS_TRIP_INFO;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_HELP_CENTER_CLICKS;

public class WidgetHelpView extends BaseCustomView {
  private TextView titleTextView;
  private TextView descriptionTextView;
  private LinearLayout container;

  public WidgetHelpView(Context context) {
    super(context);
  }

  public WidgetHelpView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public void initComponent() {
    titleTextView = (TextView) findViewById(R.id.helpview_tv_title);
    descriptionTextView = (TextView) findViewById(R.id.helpview_tv_subtitle);
    container = (LinearLayout) findViewById(R.id.helpview_container);
    container.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        openHelpScreen();
      }
    });
  }

  @Override public int getComponentLayout() {
    return R.layout.widget_help;
  }

  @Override public void initOneCMSText() {
    titleTextView.setText(localizables.getString(OneCMSKeys.MY_TRIPS_DETAIL_HELPCENTER_TITLE));
    Spanned textForDescription = Html.fromHtml(underlineUsingHref(
        localizables.getRawString(OneCMSKeys.MY_TRIPS_DETAIL_HELPCENTER_SUBTITLE)));
    descriptionTextView.setText(textForDescription);
  }

  private void openHelpScreen() {
    trackEventClickOnHelp();
    Intent intent = new Intent(getContext(), OdigeoWebViewActivity.class);
    intent.putExtra(Constants.EXTRA_URL_WEBVIEW,
        localizables.getRawString(OneCMSKeys.ABOUTOPTIONSMODULE_HELPCENTER_URL));
    getContext().startActivity(intent);
  }

  private void trackEventClickOnHelp() {
    trackerController.trackAnalyticsEvent(CATEGORY_MY_TRIPS_TRIP_INFO, ACTION_OPEN_HELP_CENTER,
        LABEL_HELP_CENTER_CLICKS);
  }
}
