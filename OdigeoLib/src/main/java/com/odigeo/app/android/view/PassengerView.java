package com.odigeo.app.android.view;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.fragments.MSLErrorUtilsDialogFragment;
import com.odigeo.app.android.lib.models.dto.custom.Code;
import com.odigeo.app.android.lib.models.dto.custom.MslError;
import com.odigeo.app.android.lib.ui.widgets.TimeoutCounterWidget;
import com.odigeo.app.android.lib.ui.widgets.TopBriefWidget;
import com.odigeo.app.android.lib.ui.widgets.base.BlackDialog;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.exceptions.PassengerCountryException;
import com.odigeo.app.android.lib.utils.exceptions.PassengerValidationException;
import com.odigeo.app.android.lib.utils.exceptions.ShoppingCartResponseException;
import com.odigeo.app.android.model.BookingInfoViewModel;
import com.odigeo.app.android.view.adapters.BaseAdaptersFactory;
import com.odigeo.app.android.view.animations.CollapseAndExpandAnimationUtil;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.custom.SpinnerWithTitle;
import com.odigeo.app.android.view.dialogs.BookingOutdatedDialog;
import com.odigeo.app.android.view.dialogs.DatePickerDialogView;
import com.odigeo.app.android.view.helpers.BookingFlowTimeoutHelper;
import com.odigeo.app.android.view.helpers.DateHelper;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.app.android.view.interfaces.ListenerPassengerNavigator;
import com.odigeo.app.android.view.interfaces.ListenerPassengerWidget;
import com.odigeo.app.android.view.snackbars.CustomSnackbar;
import com.odigeo.app.android.view.snackbars.PersuasionMessageCountDown;
import com.odigeo.app.android.view.textwatchers.BuyerBaseTextWatchersClient;
import com.odigeo.app.android.view.textwatchers.IdentificationBaseTextWatcherClient;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.extensions.UIUserTraveller;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.entity.shoppingCart.AvailableProductsResponse;
import com.odigeo.data.entity.shoppingCart.BuyerIdentificationType;
import com.odigeo.data.entity.shoppingCart.Carrier;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.ItinerarySortCriteria;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.shoppingCart.TravellerRequiredFields;
import com.odigeo.data.entity.shoppingCart.TravellerType;
import com.odigeo.data.entity.shoppingCart.request.BaggageSelectionRequest;
import com.odigeo.data.entity.shoppingCart.request.TravellerRequest;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.dataodigeo.location.LocationController;
import com.odigeo.dataodigeo.location.LocationControllerListener;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.PassengerPresenter;
import com.odigeo.presenter.contracts.navigators.PassengerNavigatorInterface;
import com.odigeo.presenter.contracts.views.PassengerViewInterface;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.odigeo.app.android.view.helpers.PermissionsHelper.LOCATION_REQUEST_CODE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_CUSTOMER_DETAILS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_FLIGHTS_PAX;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CONTINUE_PASSENGERS_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_CLICK_CONTINUE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_CLICK_CONTINUE_FAIL;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_PAX_SELECTED;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.SCREEN_PASSENGERS;

public class PassengerView extends BaseView<PassengerPresenter>
    implements PassengerViewInterface, BookingFlowTimeoutHelper.TimeoutListener,
    ListenerPassengerNavigator, ListenerPassengerWidget, LocationControllerListener {

  private static final long REPRICING_TIME = 8000;
  private BookingInfoViewModel bookingInfo;
  private final String mLocaleMarket = LocaleUtils.localeToString(Configuration.getCurrentLocale());
  private final String mLanguageIsoCode = LocaleUtils.getCurrentLanguageIso();

  private LinearLayout llBuyerContainer;
  private int mContactWidgetId;
  private DialogFragment mDialogBirthDateFragment;
  private FragmentManager mFragmentManager;
  private DateHelper mDateHelper;
  private ScrollView mPassengerScrollView;
  private Button mButtonContinue;
  private LinearLayout mLlwidening, mPassengerWidgetsContainer, mLlFooterWithMail;
  private Spinner mSpContactPassengerOptions;
  private ImageView mCnivContactPassengerImageProfile;
  private SpinnerWithTitle mSpwtIdentificationTypes;
  private TextInputLayout mTilName, mTilLastName, mTilIdentification, mTilCpf, mTilDateOfBirth,
      mTilAddress, mTilCityName, mTilStateName, mTilZipCode, mTilCountryCode, mTilPhoneCode,
      mTilPhoneNumber, mTilAlternativePhoneNumber, mTilMail;
  private TextView mTvContactPassengerItemTitle, mTvBuyerEmailStored, mTvConfirmationEmail,
      mTvLegalDisclamerFooter, mTvWidening;
  private String mPersuasionMessage, mPersuasionCloseMessage, mNameEmptyContactDetail,
      mLoadingDialogText, mPhoneNumberCode, mEditCollapse, mCloseCollapse;
  private BlackDialog mLoadingDialog;
  private IdentificationBaseTextWatcherClient mIdentificationBaseTextWatcherClient;
  private BuyerBaseTextWatchersClient mBuyerBaseTextWatchersClient;
  private boolean mHasToShowTimeoutDialog = true;
  private TimeoutCounterWidget mTimer;
  private PersuasionMessageCountDown mPersuasionMessageCountDown;
  private BookingFlowTimeoutHelper mTimerHelper;
  private List<UserTraveller> mLocalContacts;
  private List<PassengerWidgetView> mPassengerWidgetsList;
  private Runnable mRunnableRepricing;
  private Handler mHandlerRepricing = new Handler();
  private Button mBtnOpenOrCloseDetails;
  private RelativeLayout mRlBuyerFieldsContainer;
  private RelativeLayout mRlBuyerHeader;
  private TextView mTvContactPassengerOptionsTitle;
  private Country mCountryResidentCode;
  private LocationController mLocationController;
  private View mView;
  private Snackbar mSbPersuasion;
  private boolean mWasPersuasionMessageClosed = false;

  private CreateShoppingCartResponse mCreateShoppingCartResponse;
  private AvailableProductsResponse mAvailableProductsResponse;
  private boolean mIsFullTransparency;
  private double mLastTicketsPrice;
  private SearchOptions mSearchOptions;
  private PassengerWidgetView defaultPassengerWidgetView;
  private ItinerarySortCriteria itinerarySortCriteria;
  private TopBriefWidget mTopBriefWidget;

  public static PassengerView newInstance(CreateShoppingCartResponse createShoppingCartResponse,
      AvailableProductsResponse availableProductsResponse, boolean isFullTransparency,
      double lastTicketsPrice, SearchOptions searchOptions, BookingInfoViewModel bookingInfo) {

    PassengerView passengerView = new PassengerView();
    Bundle bundle = new Bundle();
    bundle.putSerializable(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE,
        createShoppingCartResponse);
    bundle.putSerializable(Constants.EXTRA_AVAILABLE_PRODUCTS_RESPONSE, availableProductsResponse);
    bundle.putBoolean(Constants.EXTRA_FULL_TRANSPARENCY, isFullTransparency);
    bundle.putDouble(Constants.EXTRA_REPRICING_TICKETS_PRICES, lastTicketsPrice);
    bundle.putSerializable(Constants.EXTRA_SEARCH_OPTIONS, searchOptions);
    bundle.putSerializable(Constants.EXTRA_BOOKING_INFO, bookingInfo);
    passengerView.setArguments(bundle);
    return passengerView;
  }

  @Override protected PassengerPresenter getPresenter() {
    return dependencyInjector.providePassengersPresenter(this,
        (PassengerNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.passengers_booking_flow;
  }

  @Override protected void initComponent(View view) {
    initParentView(view);
    mView = view;
    initRunnableRepricing();
    initDateHelper();
    initComponentContact();
    initTimeoutBookingFlow();
    initBuyerTextInputLayoutClient();
    initBuyerAdapter();
    initIdentificationTextInputLayoutClient();
    initLoginWidget();
  }

  @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    initTopBrief();
  }

  private void initTopBrief() {

    mTopBriefWidget.setSearchOptions(mSearchOptions);
    if (mIsFullTransparency || mPresenter.isMemberForCurrentMarket()) {
      mTopBriefWidget.setMembershipApplied(
          mPresenter.shouldApplyMembershipPerks(mCreateShoppingCartResponse.getPricingBreakdown()));
      mTopBriefWidget.updatePrice(
          mPresenter.getTotalPrice(mCreateShoppingCartResponse.getShoppingCart(),
              mCreateShoppingCartResponse.getPricingBreakdown()));
    }
  }

  private void initRunnableRepricing() {
    mRunnableRepricing = new Runnable() {
      @Override public void run() {
        mLlwidening.setVisibility(View.GONE);
      }
    };
  }

  private void initLocationController() {
    mLocationController = AndroidDependencyInjector.getInstance().provideLocationController();
    mLocationController.connect();
  }

  private void initDateHelper() {
    mDateHelper = AndroidDependencyInjector.getInstance().provideDateHelper();
  }

  @Override
  public void addPassengersWidgetViews(List<TravellerRequiredFields> requiredTravellerInformation) {
    mContactWidgetId = requiredTravellerInformation.size();
    mPassengerWidgetsList = new ArrayList<>();

    for (int i = 0; i < requiredTravellerInformation.size(); i++) {
      TravellerType widgetTravellersType = requiredTravellerInformation.get(i).getTravellerType();
      List<UserTraveller> travellersListForWidget =
          mPresenter.getWidgetPassengersType(widgetTravellersType);

      PassengerWidgetView passengerWidgetView =
          new PassengerWidgetView(getActivity(), getActivity().getSupportFragmentManager(), i,
              requiredTravellerInformation.get(i), mDateHelper, this, travellersListForWidget, this,
              mCreateShoppingCartResponse.getShoppingCart()
                  .getItineraryShoppingItem()
                  .getItinerary(), mCreateShoppingCartResponse.getShoppingCart().getTravellers(),
              mSearchOptions.getTravelType());

      passengerWidgetView.setId(i);
      mPassengerWidgetsList.add(passengerWidgetView);
      mPassengerWidgetsContainer.addView(passengerWidgetView);
    }
  }

  private void initParentView(View view) {
    mPassengerScrollView = (ScrollView) view.findViewById(R.id.booking_flow_passenger_scroll_view);
    mButtonContinue = (Button) view.findViewById(R.id.continueButtonPassengers);
    mTimer = (TimeoutCounterWidget) view.findViewById(R.id.timeoutCounterWidget);
    mPassengerWidgetsContainer = (LinearLayout) view.findViewById(R.id.containerPassengerItem);
    llBuyerContainer = (LinearLayout) view.findViewById(R.id.rlBuyerContainer);
    mTvLegalDisclamerFooter = (TextView) view.findViewById(R.id.legal_disclaimer_footer);
    mLlwidening = (LinearLayout) view.findViewById(R.id.widening);
    mTvWidening = (TextView) mLlwidening.findViewById(R.id.tvWideningLeyend);
    mTopBriefWidget = ((TopBriefWidget) view.findViewById(R.id.topbrief_passenger));
  }

  private void initBuyerAdapter() {
    mNameEmptyContactDetail = localizables.getString(OneCMSKeys.SSO_CREATE_NEW_CONTACT_DETAIL);
    mLocalContacts = mPresenter.getBuyers(mNameEmptyContactDetail, mContactWidgetId);
    List<String> contactsName = mPresenter.formatPassengersNameToShowOnSpinner(mLocalContacts);
    ArrayAdapter<String> contactsAdapter =
        BaseAdaptersFactory.createStringAdapter(getContext(), contactsName);
    mSpContactPassengerOptions.setAdapter(contactsAdapter);
  }

  @Override
  public void initIdentificationAdapter(List<BuyerIdentificationType> identificationTypes) {
    String identification;
    List<String> identificationSpinnerList = new ArrayList<>();

    for (int i = 0; i < identificationTypes.size(); i++) {
      identification = identificationTypes.get(i).value();
      identificationSpinnerList.add(identification);
    }

    ArrayAdapter<String> contactsAdapter =
        BaseAdaptersFactory.createStringAdapter(getContext(), identificationSpinnerList);
    mSpwtIdentificationTypes.setAdapter(contactsAdapter);
  }

  private void initIdentificationTextInputLayoutClient() {
    mIdentificationBaseTextWatcherClient =
        new IdentificationBaseTextWatcherClient(getContext(), mTilIdentification,
            mBuyerBaseTextWatchersClient);
  }

  private void initLoginWidget() {
    RelativeLayout rlBookingFlowPassengerLogin =
        (RelativeLayout) mView.findViewById(R.id.rlBookingFlowPassengerLogin);
    String loginWigetActivationKey =
        localizables.getRawString(OneCMSKeys.PASSENGER_WIDGET_LOGIN_ACTIVATION);

    if (mPresenter.hasToShowLoginWidget(loginWigetActivationKey,
        LocalizablesFacade.NOT_FOUND_TEXT)) {
      rlBookingFlowPassengerLogin.setVisibility(View.VISIBLE);

      TextView tvBookingFlowPassengerLoginTitle =
          (TextView) mView.findViewById(R.id.tvBookingFlowPassengerLoginTitle);
      TextView tvBookingFlowPassengerLoginSubtitle =
          (TextView) mView.findViewById(R.id.tvBookingFlowPassengerLoginSubtitle);
      TextView tvBookingFlowPassengerLoginButton =
          (TextView) mView.findViewById(R.id.tvBookingFlowPassengerLoginButton);

      tvBookingFlowPassengerLoginTitle.setText(
          localizables.getString(OneCMSKeys.PASSENGER_WIDGET_LOGIN_TITLE));
      tvBookingFlowPassengerLoginSubtitle.setText(
          localizables.getString(OneCMSKeys.PASSENGER_WIDGET_LOGIN_SUBTITLE));
      tvBookingFlowPassengerLoginButton.setText(
          localizables.getString(OneCMSKeys.PASSENGER_WIDGET_LOGIN_BUTTON));

      tvBookingFlowPassengerLoginButton.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {

          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAX,
              TrackerConstants.ACTION_SSO_PAX_PAGE, TrackerConstants.LABEL_GO_TO_LOGIN);
          mPresenter.onTapLogin();
        }
      });
    } else {
      rlBookingFlowPassengerLogin.setVisibility(View.GONE);
    }
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == Constants.REQUEST_CODE_DO_LOGIN && resultCode == Activity.RESULT_OK) {
      refreshViewAfterUserLogin();
    }
  }

  private void refreshViewAfterUserLogin() {
    if (mSbPersuasion != null) {
      mWasPersuasionMessageClosed = !mSbPersuasion.isShown();
    }
    mPassengerWidgetsContainer.removeAllViews();
    mPresenter.loadLocalPassengersByType();
    initComponent(mView);
    setListeners();
    mPresenter.refreshPresenterDataAfterUserLogin(mLocalContacts);
    mPresenter.onUserLoggedIn();
    initTopBrief();
  }

  private void initComponentContact() {
    mTvBuyerEmailStored = (TextView) llBuyerContainer.findViewById(R.id.tvBuyerEmailStored);
    mTvConfirmationEmail = (TextView) llBuyerContainer.findViewById(R.id.tvConfirmationEmail);
    mTvContactPassengerItemTitle =
        (TextView) llBuyerContainer.findViewById(R.id.tvContactPassengerItemTitle);
    mLlFooterWithMail = (LinearLayout) llBuyerContainer.findViewById(R.id.llFooterWithMail);
    mSpContactPassengerOptions =
        (Spinner) llBuyerContainer.findViewById(R.id.spContactPassengerOptions);
    mTvContactPassengerOptionsTitle =
        (TextView) llBuyerContainer.findViewById(R.id.tvContactPassengerOptionsTitle);
    mCnivContactPassengerImageProfile =
        (ImageView) llBuyerContainer.findViewById(R.id.cnivContactPassengerImageProfile);
    mTilName = (TextInputLayout) llBuyerContainer.findViewById(R.id.tilName);
    mTilLastName = (TextInputLayout) llBuyerContainer.findViewById(R.id.tilLastName);
    mSpwtIdentificationTypes =
        (SpinnerWithTitle) llBuyerContainer.findViewById(R.id.spwtIdentificationType);
    mTilIdentification = (TextInputLayout) llBuyerContainer.findViewById(R.id.tilIdentification);
    mTilCpf = (TextInputLayout) llBuyerContainer.findViewById(R.id.tilCpf);
    mTilDateOfBirth = (TextInputLayout) llBuyerContainer.findViewById(R.id.tilDateOfBirth);
    mTilAddress = (TextInputLayout) llBuyerContainer.findViewById(R.id.tilAddress);
    mTilCityName = (TextInputLayout) llBuyerContainer.findViewById(R.id.tilCityName);
    mTilStateName = (TextInputLayout) llBuyerContainer.findViewById(R.id.tilStateName);
    mTilZipCode = (TextInputLayout) llBuyerContainer.findViewById(R.id.tilZipCode);
    mTilCountryCode = (TextInputLayout) llBuyerContainer.findViewById(R.id.tilCountryCode);
    mTilPhoneCode = (TextInputLayout) llBuyerContainer.findViewById(R.id.tilPhoneCode);
    mTilPhoneNumber = (TextInputLayout) llBuyerContainer.findViewById(R.id.tilPhoneNumber);
    mTilAlternativePhoneNumber =
        (TextInputLayout) llBuyerContainer.findViewById(R.id.tilAlternativePhoneNumber);
    mTilMail = (TextInputLayout) llBuyerContainer.findViewById(R.id.tilMail);
    mBtnOpenOrCloseDetails = (Button) llBuyerContainer.findViewById(R.id.btnOpenOrCloseDetails);
    mRlBuyerFieldsContainer =
        (RelativeLayout) llBuyerContainer.findViewById(R.id.rlBuyerFieldsContainer);
    mRlBuyerHeader =
        (RelativeLayout) llBuyerContainer.findViewById(R.id.llHeaderContactPassengerDetail);

    int padding =
        getActivity().getResources().getDimensionPixelSize(R.dimen.padding_row_standardIconWidget);
    DrawableUtils.setImageToTextView(getActivity(), mTvBuyerEmailStored, R.drawable.ic_email,
        padding);
  }

  private void initTimeoutBookingFlow() {
    mTimerHelper = new BookingFlowTimeoutHelper(this);
    mTimerHelper.checkTimeoutConditions(getArguments());
  }

  private void initBuyerTextInputLayoutClient() {
    mBuyerBaseTextWatchersClient =
        new BuyerBaseTextWatchersClient(getActivity().getApplicationContext(), mTilName,
            mTilLastName, mTilIdentification, mTilCpf, mTilAddress, mTilCityName, mTilStateName,
            mTilZipCode, mTilPhoneNumber, mTilAlternativePhoneNumber, mTilMail, mTilPhoneCode,
            mTilCountryCode);
  }

  @Override protected void setListeners() {
    setSpinnerContactListener();
    setSpinnerContactIdentificationListener();
    setContinueButtonListener();
    setTextInputDateOfBirth();
    setTextInputPhoneCodeListener();
    setTextInputCountryResident();
    setTimerListener();
    setButtonCollapseListener();
  }

  private void setSpinnerContactListener() {
    mSpContactPassengerOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (mSpContactPassengerOptions.getVisibility() == View.VISIBLE) {
          String contactSelected = (String) mSpContactPassengerOptions.getSelectedItem();
          if (isEmptyContactSelected(contactSelected)) {
            expandFieldsContainer();
            showContactName();
            showContactLastName();
            mPresenter.cleanContactFields();
          } else if (isLocalContactSelected(contactSelected)) {
            mPresenter.setNewDefaultPassengerSpinner(position);
          }
        }
      }

      @Override public void onNothingSelected(AdapterView<?> parent) {

      }
    });
  }

  private void setSpinnerContactIdentificationListener() {
    mSpwtIdentificationTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (mSpwtIdentificationTypes.getVisibility() == View.VISIBLE) {
          String identificationTypeSelected = (String) mSpwtIdentificationTypes.getSelectedItem();
          BuyerIdentificationType buyerIdentificationType =
              BuyerIdentificationType.fromValue(identificationTypeSelected);
          mIdentificationBaseTextWatcherClient.setBuyerBehaviour(buyerIdentificationType);
        }
      }

      @Override public void onNothingSelected(AdapterView<?> parent) {

      }
    });
  }

  private void setContinueButtonListener() {
    mButtonContinue.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {

        updateLastFocusedTextInputLayoutError();
        if ((!haveToFocusOnPassengerTextInputError()) && (!haveToFocusOnContactTextInputError())) {

          List<TravellerRequest> widgetTravellers = getTravellers();

          trackAllEvents(widgetTravellers);

          mPresenter.addPassengersToShoppingCart(widgetTravellers, mCreateShoppingCartResponse,
              mAvailableProductsResponse, itinerarySortCriteria);
        }
      }
    });
  }

  private void updateLastFocusedTextInputLayoutError() {
    TextInputLayout lastFocusedTilBuyer =
        (TextInputLayout) mRlBuyerFieldsContainer.getFocusedChild();

    if (lastFocusedTilBuyer != null) {
      mBuyerBaseTextWatchersClient.updateTextInputLayoutError(lastFocusedTilBuyer);
    }

    for (PassengerWidgetView passengerWidgetView : mPassengerWidgetsList) {
      TextInputLayout lastFocusedTilPassenger = passengerWidgetView.getLastFocused();
      if (lastFocusedTilPassenger != null) {
        passengerWidgetView.updateTextInputLayoutError(lastFocusedTilPassenger);
      }
    }
  }

  private void setTimerListener() {
    mTimer.setListener(mTimerHelper.getTimeoutWidgetListener());
  }

  private void setButtonCollapseListener() {
    mBtnOpenOrCloseDetails.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        if (mRlBuyerFieldsContainer.getVisibility() == View.VISIBLE) {
          CollapseAndExpandAnimationUtil.collapse(mRlBuyerFieldsContainer, true,
              mPassengerScrollView, (View) mRlBuyerFieldsContainer.getParent().getParent());
          showCollapseEditText();
        } else {
          CollapseAndExpandAnimationUtil.expand(mRlBuyerFieldsContainer);
          showCollapseCloseText();
        }
      }
    });
  }

  private boolean haveToFocusOnPassengerTextInputError() {
    for (int i = 0; i < mPassengerWidgetsContainer.getChildCount(); i++) {
      PassengerWidgetView passengerWidgetView =
          (PassengerWidgetView) mPassengerWidgetsContainer.getChildAt(i);
      if (passengerWidgetView.haveToFocusOnPassengerTextInputError()) {
        return true;
      }
    }
    return false;
  }

  private boolean haveToFocusOnContactTextInputError() {
    final TextInputLayout textInputLayout =
        mBuyerBaseTextWatchersClient.getTextInputLayoutWithError();
    if (textInputLayout != null) {
      expandFieldsContainer();
      textInputLayout.setFocusable(true);
      textInputLayout.requestFocus();
      scrollToTextInputLayoutWithError(textInputLayout);
      return true;
    }
    return false;
  }

  private void scrollToTextInputLayoutWithError(final TextInputLayout textInputLayout) {
    mPassengerScrollView.post(new Runnable() {
      @Override public void run() {
        View parent = (View) textInputLayout.getParent().getParent();
        View grandParent = (View) parent.getParent().getParent();
        int inputHeight = textInputLayout.getHeight();
        int top = textInputLayout.getTop() + parent.getTop() + grandParent.getTop() + inputHeight;
        int height = mPassengerScrollView.getHeight();
        mPassengerScrollView.smoothScrollTo(0, top - height / 2);
      }
    });
  }

  private void setTextInputDateOfBirth() {
    if (mTilDateOfBirth.getEditText() != null) {
      mTilDateOfBirth.getEditText().setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {

          mFragmentManager = getActivity().getSupportFragmentManager();
          mDialogBirthDateFragment.show(mFragmentManager, "DialogDateFragment");
        }
      });
    }
  }

  private void setTextInputPhoneCodeListener() {
    if (mTilPhoneCode.getEditText() != null) {
      mTilPhoneCode.getEditText().setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {

          mPresenter.navigateToPhonePrefix();
        }
      });
    }
  }

  private void setTextInputCountryResident() {
    if (mTilCountryCode.getEditText() != null) {
      mTilCountryCode.getEditText().setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {

          mPresenter.navigateToCountryResident(mContactWidgetId);
        }
      });
    }
  }

  @Override protected void initOneCMSText(View view) {
    mButtonContinue.setText(localizables.getString(OneCMSKeys.COMMON_BUTTONCONTINUE));
    mTvLegalDisclamerFooter.setText((localizables.getString(OneCMSKeys.FOOTER_LEGAL_DISCLAIMER)));
    initContactOneCMS();
  }

  private void initContactOneCMS() {
    mPersuasionMessage = localizables.getString(OneCMSKeys.PASSENGER_PERSUASION_MESSAGE);
    mPersuasionCloseMessage = localizables.getString(OneCMSKeys.SSO_PASSWORDRECOVERY_CLOSE);
    mEditCollapse = localizables.getString(OneCMSKeys.PASSENGER_COLLAPSE_EDIT);
    mCloseCollapse = localizables.getString(OneCMSKeys.PASSENGER_COLLAPSE_CLOSE);
    mLoadingDialogText = localizables.getString(OneCMSKeys.ADDING_PERSONAL_INFO_MESSAGE);
    mTvContactPassengerItemTitle.setText(
        localizables.getString(OneCMSKeys.SSO_TRAVELLERS_CONTACT_DETAIL));
    mTvContactPassengerOptionsTitle.setText(
        localizables.getString(OneCMSKeys.SSO_TRAVELLERS_CONTACT_DETAIL));
    mTilName.setHint(localizables.getString(OneCMSKeys.HINT_NAME));
    mTilLastName.setHint(localizables.getString(OneCMSKeys.HINT_FIRST_LASTNAME));
    mTilIdentification.setHint(localizables.getString(OneCMSKeys.HINT_IDENTIFICATION));
    mTilCpf.setHint(localizables.getString(OneCMSKeys.HINT_CPF));
    mTilDateOfBirth.setHint(localizables.getString(OneCMSKeys.HINT_BIRTH_DATE));
    mTilAddress.setHint(localizables.getString(OneCMSKeys.HINT_ADDRESS));
    mTilCityName.setHint(localizables.getString(OneCMSKeys.HINT_CITY));
    mTilStateName.setHint(localizables.getString(OneCMSKeys.HINT_STATE));
    mTilZipCode.setHint(localizables.getString(OneCMSKeys.HINT_ZIP_CODE));
    mTilCountryCode.setHint(localizables.getString(OneCMSKeys.HINT_COUNTRY_RESIDENCE));
    mTilPhoneCode.setHint(localizables.getString(OneCMSKeys.HINT_PHONE_PREFIX));
    mTilPhoneNumber.setHint(localizables.getString(OneCMSKeys.HINT_PHONE));
    mTilAlternativePhoneNumber.setHint(localizables.getString(OneCMSKeys.HINT_MOBILE_PHONE));
    mTilMail.setHint(localizables.getString(OneCMSKeys.HINT_EMAIL));
    mTvConfirmationEmail.setText(
        localizables.getString(OneCMSKeys.SSO_REGISTER_CONFIRMATION_EMAIL));
    mBtnOpenOrCloseDetails.setText(mCloseCollapse);
  }

  @Override public void initPersuasionSnackBar() {
    if (!mWasPersuasionMessageClosed) {
      CustomSnackbar customSnackbar =
          new CustomSnackbar(getContext(), getView(), mPersuasionMessage, mPersuasionCloseMessage,
              Snackbar.LENGTH_INDEFINITE);
      mSbPersuasion = customSnackbar.provideCustomSnackbar();
      mSbPersuasion.show();
      mPersuasionMessageCountDown = PersuasionMessageCountDown.getInstance();
      mPersuasionMessageCountDown.setSnackBar(mSbPersuasion);
    }
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mCreateShoppingCartResponse = (CreateShoppingCartResponse) getArguments().getSerializable(
        Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE);
    mAvailableProductsResponse = (AvailableProductsResponse) getArguments().getSerializable(
        Constants.EXTRA_AVAILABLE_PRODUCTS_RESPONSE);
    mIsFullTransparency = getArguments().getBoolean(Constants.EXTRA_FULL_TRANSPARENCY);
    mLastTicketsPrice = getArguments().getDouble(Constants.EXTRA_REPRICING_TICKETS_PRICES);
    mSearchOptions = (SearchOptions) getArguments().getSerializable(Constants.EXTRA_SEARCH_OPTIONS);
    bookingInfo =
        (BookingInfoViewModel) getArguments().getSerializable(Constants.EXTRA_BOOKING_INFO);
    mPresenter.loadLocalPassengersByType();
    initLocationController();
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mTracker.trackAnalyticsScreen(SCREEN_PASSENGERS);
    initPresenter();
  }

  @Override public void initContactCustomKeyBoard() {
    mBuyerBaseTextWatchersClient.initBuyerOnFocusChange();
  }

  @Override public void initPassengerCustomKeyboard() {
    for (PassengerWidgetView passengerWidgetView : mPassengerWidgetsList) {
      passengerWidgetView.initPassengerOnFocusChangeCustomKeyboard();
    }
  }

  private void initPresenter() {
    BigDecimal collectionMethodWithPriceValue = BigDecimal.ZERO;
    if (bookingInfo.getCollectionMethodWithPrice() != null) {
      collectionMethodWithPriceValue = bookingInfo.getCollectionMethodWithPrice().getPrice();
    }

    mPresenter.initializePresenter(mLanguageIsoCode, mLocalContacts,
        mCreateShoppingCartResponse.getShoppingCart().getRequiredTravellerInformation(),
        mCreateShoppingCartResponse.getShoppingCart().getRequiredBuyerInformation(),
        mCreateShoppingCartResponse.getTotalPriceTickets(), mLastTicketsPrice,
        Configuration.getCurrentLocale(), Configuration.getInstance().getShowPersuasive(),
        collectionMethodWithPriceValue);
  }

  public void updateCreateShoppingCartResponse(
      CreateShoppingCartResponse createShoppingCartResponse) {
    mCreateShoppingCartResponse = createShoppingCartResponse;
    initTopBrief();
  }

  @Override public void onResume() {
    super.onResume();
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_PASSENGERS);
    mLocationController.subscribe(this);
    mHasToShowTimeoutDialog = true;
  }

  @Override public void onStop() {
    super.onStop();
    mLocationController.unSubscribe();
    if (mPersuasionMessageCountDown != null) mPersuasionMessageCountDown.cancel();
    mHasToShowTimeoutDialog = false;
  }

  @Override public void onDestroy() {
    super.onDestroy();
  }

  @Override public void startTimer(int time) {
    mTimer.startTimer(time);
  }

  @Override public void setNullTextWatchersError() {
    mBuyerBaseTextWatchersClient.setNullErrorInTextWatchers();
  }

  @Override public void hideContactHeader() {
    mRlBuyerHeader.setVisibility(View.GONE);
  }

  @Override public void hideFieldsContainer() {
    mRlBuyerFieldsContainer.setVisibility(View.GONE);
  }

  @Override public void hideContactName() {
    mTilName.setVisibility(View.GONE);
  }

  @Override public void hideContactLastName() {
    mTilLastName.setVisibility(View.GONE);
  }

  @Override public void hideContactEditableMail() {
    mTilMail.setVisibility(View.GONE);
  }

  @Override public void hideContactIdentification() {
    mSpwtIdentificationTypes.setVisibility(View.GONE);
    mTilIdentification.setVisibility(View.GONE);
  }

  @Override public void hideContactDateOfBirth() {
    mTilDateOfBirth.setVisibility(View.GONE);
  }

  @Override public void hideContactCPF() {
    mTilCpf.setVisibility(View.GONE);
  }

  @Override public void hideContactAddress() {
    mTilAddress.setVisibility(View.GONE);
  }

  @Override public void hideContactCity() {
    mTilCityName.setVisibility(View.GONE);
  }

  @Override public void hideContactState() {
    mTilStateName.setVisibility(View.GONE);
  }

  @Override public void hideContactZipCode() {
    mTilZipCode.setVisibility(View.GONE);
  }

  @Override public void hideContactPhoneNumber() {
    mTilPhoneNumber.setVisibility(View.GONE);
  }

  @Override public void hideContactAlternativePhoneNumber() {
    mTilAlternativePhoneNumber.setVisibility(View.GONE);
  }

  @Override public void hideContactCountryCode() {
    mTilCountryCode.setVisibility(View.GONE);
  }

  @Override public void hideFooterMail() {
    mLlFooterWithMail.setVisibility(View.GONE);
  }

  @Override public void hideShowOrHideButton() {
    mBtnOpenOrCloseDetails.setVisibility(View.GONE);
  }

  @Override public void hideLoadingDialog() {
    if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
      mLoadingDialog.dismiss();
    }
  }

  @Override public void showCollapseEditText() {
    mBtnOpenOrCloseDetails.setText(mEditCollapse);
  }

  @Override public void showCollapseCloseText() {
    mBtnOpenOrCloseDetails.setText(mCloseCollapse);
  }

  @Override public void setScrollParameters() {
    mTimer.setVisibility(View.VISIBLE);
  }

  @Override public void showContactImage() {
    Drawable tintedResource =
        DrawableUtils.getTintedResource(R.drawable.ic_contact, R.color.basic_light, getActivity());
    mCnivContactPassengerImageProfile.setImageDrawable(tintedResource);
  }

  @Override public void showTimeoutWidget(int seconds, int minutes) {
    mTimer.setVisibility(View.VISIBLE);
    mTimer.setMinutes(seconds, minutes);
    mTimer.bringToFront();
  }

  @Override public void showIncreaseTicketRepricingMessage(double difference) {
    String price = LocaleUtils.getLocalizedCurrencyValue(difference, mLocaleMarket);
    String increaseRepricingMessage =
        localizables.getString(OneCMSKeys.INCREASE_REPRICING_TICKET, price);
    showRepricingWidening(increaseRepricingMessage);
  }

  @Override public void showDecreaseTicketRepricingMessage(double difference) {
    String price = LocaleUtils.getLocalizedCurrencyValue(difference, mLocaleMarket);
    String decreaseRepricingMessage =
        localizables.getString(OneCMSKeys.DECREASE_REPRICING_TICKET, price);
    showRepricingWidening(decreaseRepricingMessage);
  }

  private void showRepricingWidening(String message) {
    mTvWidening.setText(message);
    mLlwidening.setVisibility(View.VISIBLE);
    mHandlerRepricing.postDelayed(mRunnableRepricing, REPRICING_TIME);
  }

  @Override public void showUnEditableEmail(String email) {
    mLlFooterWithMail.setVisibility(View.VISIBLE);
    mTvBuyerEmailStored.setText(email);
  }

  @Override public void showLoadingDialog() {
    mLoadingDialog = new BlackDialog(getActivity(), true);
    mLoadingDialog.show(mLoadingDialogText);
  }

  @Override public void showContactSpinnerItem(int position) {
    mSpContactPassengerOptions.setSelection(position);
  }

  @Override public void showContactName(String name) {
    if (mTilName.getEditText() != null) {
      showTextInTextInputLayout(mTilName, name);
    }
  }

  @Override public void showContactLastName(String lastName) {
    if (mTilLastName.getEditText() != null) {
      showTextInTextInputLayout(mTilLastName, lastName);
    }
  }

  @Override public void showContactName() {
    mTilName.setVisibility(View.VISIBLE);
  }

  @Override public void showContactLastName() {
    mTilLastName.setVisibility(View.VISIBLE);
  }

  @Override public void showContactDateOfBirth(String dateOfBirth) {
    if ((mTilDateOfBirth.getEditText() != null) && (mTilDateOfBirth.getVisibility()
        == View.VISIBLE)) {
      showTextInTextInputLayout(mTilDateOfBirth, dateOfBirth);
    }
  }

  @Override public void showContactCPF(String cpf) {
    if ((mTilCpf.getEditText() != null) && (mTilCpf.getVisibility() == View.VISIBLE)) {
      showTextInTextInputLayout(mTilCpf, cpf);
    }
  }

  @Override public void showContactIdentification(String identification) {
    if ((mTilIdentification.getEditText() != null) && (mTilIdentification.getVisibility()
        == View.VISIBLE)) {
      showTextInTextInputLayout(mTilIdentification, identification);
    }
  }

  @Override public void showContactAddress(String address) {
    if ((mTilAddress.getEditText() != null) && (mTilAddress.getVisibility() == View.VISIBLE)) {
      showTextInTextInputLayout(mTilAddress, address);
    }
  }

  @Override public void showContactCity(String city) {
    if ((mTilCityName.getEditText() != null) && (mTilCityName.getVisibility() == View.VISIBLE)) {
      showTextInTextInputLayout(mTilCityName, city);
    }
  }

  @Override public void showContactState(String state) {
    if ((mTilStateName.getEditText() != null) && (mTilStateName.getVisibility() == View.VISIBLE)) {
      showTextInTextInputLayout(mTilStateName, state);
    }
  }

  @Override public void showContactResidentCountry(String country) {
    if ((mTilCountryCode.getEditText() != null) && (mTilCountryCode.getVisibility()
        == View.VISIBLE)) {
      showTextInTextInputLayout(mTilCountryCode, country);
    }
  }

  @Override public void showContactZipCode(String zipcode) {
    if ((mTilZipCode.getEditText() != null) && (mTilZipCode.getVisibility() == View.VISIBLE)) {
      showTextInTextInputLayout(mTilZipCode, zipcode);
    }
  }

  @Override public void showContactPhoneNumber(String phoneNumber) {
    if ((mTilPhoneNumber.getEditText() != null) && (mTilPhoneNumber.getVisibility()
        == View.VISIBLE)) {
      showTextInTextInputLayout(mTilPhoneNumber, phoneNumber);
    }
  }

  @Override public void showContactAlternativePhoneNumber(String alternativePhone) {
    if ((mTilAlternativePhoneNumber.getEditText() != null)
        && (mTilAlternativePhoneNumber.getVisibility() == View.VISIBLE)) {
      showTextInTextInputLayout(mTilAlternativePhoneNumber, alternativePhone);
    }
  }

  @Override public void showPhoneCode(String phoneCodeAndCountry, Country countryPhoneCode) {
    if ((mTilPhoneCode.getEditText() != null) && (mTilPhoneCode.getVisibility() == View.VISIBLE)) {
      showTextInTextInputLayout(mTilPhoneCode, phoneCodeAndCountry);
      if ((countryPhoneCode != null) && (countryPhoneCode.getCountryCode() != null)) {
        mPhoneNumberCode = countryPhoneCode.getCountryCode();
      }
    }
  }

  @Override public void showContactCountryCode(Country country) {
    if ((mTilCountryCode.getEditText() != null) && (mTilCountryCode.getVisibility()
        == View.VISIBLE)) {
      mCountryResidentCode = country;
      if (country != null) {
        mCountryResidentCode = country;
        showTextInTextInputLayout(mTilCountryCode, mCountryResidentCode.getName());
      }
    }
  }

  @Override public void showContactEditableMail(String mail) {
    if ((mTilMail.getEditText() != null) && (mTilMail.getVisibility() == View.VISIBLE)) {
      showTextInTextInputLayout(mTilMail, mail);
    }
  }

  @SuppressWarnings("ConstantConditions")
  private void showTextInTextInputLayout(@NonNull TextInputLayout textInputLayout,
      @Nullable String text) {
    if (text == null) {
      textInputLayout.getEditText().getText().clear();
    } else {
      textInputLayout.getEditText().setText(text);
    }
  }

  @Override public void setIdentificationTypePosition(int position) {
    if (mSpwtIdentificationTypes.isEnabled() && mSpwtIdentificationTypes.getCount() > 0) {
      mSpwtIdentificationTypes.setSelection(position);
    }
  }

  @Override public void setBirthDateCalendarMaxAndMin(Date minDate, Date maxDate) {
    long maxCalendarDate = maxDate.getTime();
    long minCalendarDate = minDate.getTime();
    mDialogBirthDateFragment = DatePickerDialogView.newInstance(maxCalendarDate, minCalendarDate,
        new DatePickerDialogView.onClickDatePickerListener() {
          @Override public void onDateDateSelected(int year, int month, int day) {

            long passengerBirthdate = mDateHelper.getTimeStampFromDayMonthYear(year, month, day);
            setBirthDate(passengerBirthdate);
          }
        });
  }

  private void setBirthDate(long date) {
    if (mTilDateOfBirth.getEditText() != null) {
      Date birthDate = new Date(date);
      String templateDate = getResources().getString(R.string.templates__date2);
      SimpleDateFormat simpleDateFormat = OdigeoDateUtils.getGmtDateFormat(templateDate);
      mTilDateOfBirth.getEditText().setText(simpleDateFormat.format(birthDate));
    }
  }

  @Override public void showValidationErrorMessage() {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setCancelable(true);
    builder.setTitle(localizables.getString(OneCMSKeys.PASSENGER_ERROR_VALIDATION));
    String errorMessage = localizables.getString(OneCMSKeys.PASSENGER_GENERIC_FIELD_ERROR);
    builder.setPositiveButton(localizables.getString(OneCMSKeys.COMMON_OK), null);
    builder.setMessage(errorMessage);
    builder.create();
    builder.show();
  }

  @Override public void showOutdatedBookingId() {
    BookingOutdatedDialog bookingOutdatedDialog = new BookingOutdatedDialog(getActivity());
    bookingOutdatedDialog.show();
  }

  @Override public void invalidCPF() {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setCancelable(true);
    builder.setTitle(localizables.getString(OneCMSKeys.PASSENGER_ERROR_VALIDATION));
    String errorMessage = localizables.getString(OneCMSKeys.PASSENGER_CPF_FIELD_ERROR);
    builder.setPositiveButton(localizables.getString(OneCMSKeys.COMMON_OK), null);
    builder.setMessage(errorMessage);
    builder.create();
    builder.show();
  }

  @Override public void showTimeOutErrorMessage() {
    if (mHasToShowTimeoutDialog) {
      MslError error = new MslError();
      error.setCode(Code.SESSION_TIMEOUT.getErrorCode());
      error.setCodeString(Code.SESSION_TIMEOUT);
      MSLErrorUtilsDialogFragment fragmentErrorDialog =
          MSLErrorUtilsDialogFragment.newInstance(null, error);
      if (fragmentErrorDialog != null && getActivity() != null) {
        fragmentErrorDialog.setParentScreen(Step.PASSENGER.toString());
        fragmentErrorDialog.setCancelable(false);
        fragmentErrorDialog.show(getChildFragmentManager(), "");
      }
    }
  }

  @Override public void showGeneralError() {
    MslError error = new MslError();
    error.setCode(Code.GENERAL_ERROR.getErrorCode());
    error.setCodeString(Code.GENERAL_ERROR);
    MSLErrorUtilsDialogFragment fragmentErrorDialog =
        MSLErrorUtilsDialogFragment.newInstance(null, error);
    if (fragmentErrorDialog != null && getActivity() != null) {
      fragmentErrorDialog.setParentScreen(Step.PASSENGER.toString());
      fragmentErrorDialog.setCancelable(false);
      fragmentErrorDialog.show(getChildFragmentManager(), "");
    }
  }

  @Override public String getContactName() {
    if (mTilName.getEditText() != null && !mTilName.getEditText().getText().toString().isEmpty()) {
      return mTilName.getEditText().getText().toString();
    } else {
      return defaultPassengerWidgetView.getPassengerName();
    }
  }

  @Override public String getContactLastName() {
    if (mTilLastName.getEditText() != null && !mTilLastName.getEditText()
        .getText()
        .toString()
        .isEmpty()) {
      return mTilLastName.getEditText().getText().toString();
    } else {
      return defaultPassengerWidgetView.getPassengerFirstLastName();
    }
  }

  @Override public int getBuyerIndexSelected() {
    return mSpContactPassengerOptions.getSelectedItemPosition();
  }

  @Override public String getIdentificationBuyerType() {
    return mSpwtIdentificationTypes.getSelectedItem().toString();
  }

  @Override @Nullable public String getContactIdentification() {
    if (mTilIdentification.getEditText() != null) {
      return mTilIdentification.getEditText().getText().toString();
    }
    return null;
  }

  @Override @Nullable public String getContactCPF() {
    if (mTilCpf.getEditText() != null) {
      return mTilCpf.getEditText().getText().toString();
    }
    return null;
  }

  @Override @Nullable public String getContactDateOfBirth() {
    if (mTilDateOfBirth.getEditText() != null) {
      return mTilDateOfBirth.getEditText().getText().toString();
    }
    return null;
  }

  @Override @Nullable public String getContactAddress() {
    if (mTilAddress.getEditText() != null) {
      return mTilAddress.getEditText().getText().toString();
    }
    return null;
  }

  @Override @Nullable public String getContactCity() {
    if (mTilCityName.getEditText() != null) {
      return mTilCityName.getEditText().getText().toString();
    }
    return null;
  }

  @Override @Nullable public String getContactState() {
    if (mTilStateName.getEditText() != null) {
      return mTilStateName.getEditText().getText().toString();
    }
    return null;
  }

  @Override @Nullable public String getContactZipCode() {
    if (mTilZipCode.getEditText() != null) {
      return mTilZipCode.getEditText().getText().toString();
    }
    return null;
  }

  @Override @Nullable public String getCountryCode() {
    if (mCountryResidentCode != null) {
      return mCountryResidentCode.getCountryCode();
    }
    return null;
  }

  @Override @Nullable public String getContactPhoneCode() {
    if (mTilPhoneCode.getEditText() != null) {
      return mTilPhoneCode.getEditText().getText().toString();
    }
    return null;
  }

  @Override @Nullable public String getContactPhoneNumber() {
    if (mTilPhoneNumber.getEditText() != null) {
      return mTilPhoneNumber.getEditText().getText().toString();
    }
    return null;
  }

  @Override public String getPhoneCode() {
    return mPhoneNumberCode;
  }

  @Override @Nullable public String getContactAlternativePhoneNumber() {
    if (mTilAlternativePhoneNumber.getEditText() != null) {
      return mTilAlternativePhoneNumber.getEditText().getText().toString();
    }
    return null;
  }

  @Override @Nullable public String getContactEmail() {
    if (mTilMail.getEditText() != null) {
      return mTilMail.getEditText().getText().toString();
    }
    return null;
  }

  @Override
  public void navigateToFrequentFlyerPassenger(List<UserFrequentFlyer> frequentFlyersSelected,
      List<Carrier> passengerCarriers, int passengerPosition) {
    List<Carrier> flightCarriers = mCreateShoppingCartResponse.getShoppingCart()
        .getItineraryShoppingItem()
        .getItinerary()
        .getLegend()
        .getCarriers();
    mPresenter.navigateToFrequentFlyer(frequentFlyersSelected, passengerCarriers, passengerPosition,
        flightCarriers);
  }

  @Override public void navigateToCountryResidentPassenger(int widgetPosition) {
    mPresenter.navigateToCountryResident(widgetPosition);
  }

  @Override public void navigateToCountryNationalityPassenger(int widgetPosition) {
    mPresenter.navigateToCountryNationality(widgetPosition);
  }

  @Override public void navigateToCountryIdentificationPassenger(int widgetPosition) {
    mPresenter.navigateToCountryIdentification(widgetPosition);
  }

  @Override public void navigateToSearch() {
    mPresenter.navigateToSearch();
  }

  public void onSetPassengerFrequentFlyer(List<UserFrequentFlyer> frequentFlyersCards,
      int passengerWidgetId) {
    PassengerWidgetView passengerWidgetView =
        (PassengerWidgetView) mPassengerWidgetsContainer.getChildAt(passengerWidgetId);
    passengerWidgetView.setFrequentFlyer(frequentFlyersCards);
  }

  public void onSetResidentCountry(Country residentCountry, int widgetId) {
    if (widgetId == mContactWidgetId) {
      mPresenter.setResidentCountry(residentCountry);
    } else {
      PassengerWidgetView passengerWidgetView =
          (PassengerWidgetView) mPassengerWidgetsContainer.getChildAt(widgetId);
      passengerWidgetView.showCountryResident(residentCountry);
    }
  }

  public void onSetNationalityCountry(Country nationalityCountry, int widgetId) {
    PassengerWidgetView passengerWidgetView =
        (PassengerWidgetView) mPassengerWidgetsContainer.getChildAt(widgetId);
    passengerWidgetView.showCountryNationality(nationalityCountry);
  }

  public void onSetIdentificationCountry(Country identificationCountry, int widgetId) {
    PassengerWidgetView passengerWidgetView =
        (PassengerWidgetView) mPassengerWidgetsContainer.getChildAt(widgetId);
    passengerWidgetView.showCountryIdentification(identificationCountry);
  }

  public void onSetContactPhonePrefix(Country contactPhonePrefix) {
    mPhoneNumberCode = contactPhonePrefix.getCountryCode();
    mPresenter.setPhoneCode(contactPhonePrefix);
  }

  @Override public void onSetDefaultPassengerInformation(UserTraveller passenger,
      int passengerPositionSpinner) {
    mPresenter.setDefaultPassenger(passenger, passengerPositionSpinner);
  }

  @Override public void setNewDefaultPassengerSpinner(int newDefaultPassengerPositionSpinner) {
    mPresenter.setNewDefaultPassengerSpinner(newDefaultPassengerPositionSpinner);
  }

  @Override public void setDefaultPassenger(boolean hasMoreNonChildPassengers) {
    defaultPassengerWidgetView = (PassengerWidgetView) mPassengerWidgetsContainer.getChildAt(0);
    defaultPassengerWidgetView.setPassengerAsDefaultPassenger(hasMoreNonChildPassengers);
  }

  @Override public void trackCrashlyticException(Exception e) {
    dependencyInjector.provideCrashlyticsController().trackNonFatal(e);
  }

  @Override
  public void trackShoppingCartResponseException(CreateShoppingCartResponse shoppingCartResponse) {
    dependencyInjector.provideCrashlyticsController()
        .trackNonFatal(ShoppingCartResponseException.newInstance(shoppingCartResponse));
  }

  @Override
  public void trackPassengerValidationException(CreateShoppingCartResponse shoppingCartResponse) {
    dependencyInjector.provideCrashlyticsController()
        .trackNonFatal(PassengerValidationException.newInstance(shoppingCartResponse));
  }

  @Override public ScrollView getScrollView() {
    return mPassengerScrollView;
  }

  @Override
  public void applyDefaultBaggageSelection(List<BaggageSelectionRequest> baggageSelections) {
    for (int i = 1; i < mPassengerWidgetsList.size(); i++) {
      mPassengerWidgetsList.get(i).updateBaggageSelection(baggageSelections);
    }
  }

  @Override public void disableApplyDefaultBaggageSelection() {
    defaultPassengerWidgetView.disableApplyBaggageSelection();
  }

  @Override
  public void propagateItinerarySortCriteria(ItinerarySortCriteria itinerarySortCriteria) {
    this.itinerarySortCriteria = itinerarySortCriteria;
  }

  @Override public void showMembershipPrices() {
    double price = mPresenter.getTotalPrice(mCreateShoppingCartResponse.getShoppingCart(),
        mCreateShoppingCartResponse.getPricingBreakdown());

    double membershipPerksFee =
        mPresenter.getMembershipPerks(mCreateShoppingCartResponse.getPricingBreakdown());
    OdigeoSession odigeoSession = ((OdigeoApp) getActivity().getApplication()).getOdigeoSession();
    mTopBriefWidget.setMembershipApplied(true);

    if (mPresenter.isUserLoggedIn() && odigeoSession.getMembershipPerksDTO() != null) {
      price += odigeoSession.getMembershipPerksDTO().getFee().doubleValue();
      odigeoSession.setMembershipPerksDTO(null);
    } else if (membershipPerksFee == 0d) {
      price +=
          mPresenter.getSlashedMembershipPerks(mCreateShoppingCartResponse.getPricingBreakdown());
    }

    mTopBriefWidget.updatePrice(price);
  }

  @Override public void showPricesWithoutMembershipPerks() {
    double price = mPresenter.getTotalPriceWithoutMembershipPerks(
        mCreateShoppingCartResponse.getShoppingCart(),
        mCreateShoppingCartResponse.getPricingBreakdown());
    mTopBriefWidget.setMembershipApplied(false);
    mTopBriefWidget.updatePrice(price);
  }

  @Override public boolean IsPassengerSelectedInAnotherWidgetSpinner(int widgetPosition,
      String travellerNameSelected, TravellerType passengerSpinnerType) {
    for (int i = 0; i < mPassengerWidgetsContainer.getChildCount(); i++) {
      if (mPassengerWidgetsContainer.getChildAt(i).getId() != widgetPosition) {

        PassengerWidgetView passengerWidgetView =
            (PassengerWidgetView) mPassengerWidgetsContainer.getChildAt(i);
        String passengerName = passengerWidgetView.getPassengerSelectedItemSpinner();
        TravellerType travellerType = passengerWidgetView.getPassengerWidgetType();

        if ((passengerName != null) && (passengerName.equals(travellerNameSelected)) && (
            passengerWidgetView.getId()
                != widgetPosition) && (travellerType == passengerSpinnerType)) {
          return true;
        }
      }
    }
    return false;
  }

  private boolean isEmptyContactSelected(String contactSelected) {
    return ((contactSelected != null) && contactSelected.equals(mNameEmptyContactDetail) && (
        mLocalContacts.size()
            > 1));
  }

  private boolean isLocalContactSelected(String contactSelected) {
    return ((contactSelected != null) && (!contactSelected.equals(mNameEmptyContactDetail)));
  }

  private List<TravellerRequest> getTravellers() {
    List<TravellerRequest> travellersRequest = new ArrayList<>();
    for (PassengerWidgetView passengerWidgetView : mPassengerWidgetsList) {
      travellersRequest.add(passengerWidgetView.getTraveller());
    }
    return travellersRequest;
  }

  @Override public List<UserTraveller> getUserTravellers() {
    List<UserTraveller> userTravellerList = new ArrayList<>();
    for (PassengerWidgetView passengerWidgetView : mPassengerWidgetsList) {
      userTravellerList.add(passengerWidgetView.getUserTraveller());
    }
    return userTravellerList;
  }

  private void expandFieldsContainer() {
    if (mRlBuyerFieldsContainer.getVisibility() == View.GONE) {
      mRlBuyerFieldsContainer.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
      mRlBuyerFieldsContainer.setVisibility(View.VISIBLE);
      showCollapseCloseText();
    }
  }

  private void trackAllEvents(List<TravellerRequest> widgetTravellers) {
    mTracker.trackPAXViewPage(CONTINUE_PASSENGERS_EVENT, CATEGORY_FLIGHTS_PAX,
        ACTION_CUSTOMER_DETAILS, LABEL_CLICK_CONTINUE, LABEL_CLICK_CONTINUE_FAIL);

    String label = LABEL_PAX_SELECTED + mSpContactPassengerOptions.getSelectedItemPosition();
    mTracker.trackAnalyticsEvent(CATEGORY_FLIGHTS_PAX, ACTION_CUSTOMER_DETAILS, label);

    mTracker.trackLocalyticsEvent(TrackerConstants.CLICKED_CONTINUE_PASSENGER);

    trackBuyerEmail();

    trackBaggagesEvent(widgetTravellers);
  }

  private void trackBuyerEmail() {
    String buyerEmail = "";

    if (mTvBuyerEmailStored.getText() != null
        && mTvBuyerEmailStored.getText().toString().length() > 0) {
      buyerEmail = mTvBuyerEmailStored.getText().toString();
    } else if (mTilMail.getEditText() != null
        && mTilMail.getEditText().getText() != null
        && mTilMail.getEditText().getText().length() > 0) {
      buyerEmail = mTilMail.getEditText().getText().toString();
    }

    mTracker.setUserLocalyticsData(getContactName(), getContactLastName(), buyerEmail);
  }

  private void trackBaggagesEvent(List<TravellerRequest> travellerRequestList) {
    int adultPassengerCount = 0;
    int childPassengerCount = 0;
    int adultPassengerBaggagePieces = 0;
    int childPassengerBaggagePieces = 0;

    for (int i = 0; i < travellerRequestList.size(); i++) {

      String passengerType = travellerRequestList.get(i).getTravellerTypeName();

      if (passengerType.equals(UIUserTraveller.TypeOfTraveller.ADULT.toString())) {
        adultPassengerCount++;
        adultPassengerBaggagePieces += travellerRequestList.get(i).getBaggagePiecesCount();
      } else if (passengerType.equals(UIUserTraveller.TypeOfTraveller.INFANT.toString())
          || passengerType.equals(UIUserTraveller.TypeOfTraveller.CHILD.toString())) {
        childPassengerCount++;
        childPassengerBaggagePieces += travellerRequestList.get(i).getBaggagePiecesCount();
      }
    }

    mTracker.trackPassengerBaggage(adultPassengerCount, childPassengerCount,
        adultPassengerBaggagePieces, childPassengerBaggagePieces);
  }

  @Override public void onLocationReady() {
    autoCompleteGeolocalizationInfo();
  }

  @Override public void autoCompleteGeolocalizationInfo() {
    City city = mLocationController.getNearestCity();
    if (city != null) {
      mPresenter.showContactGeolocalizationInfo(city.getCountryCode());
    }
  }

  @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    switch (requestCode) {
      case LOCATION_REQUEST_CODE:
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          mLocationController.connect();
        }
    }
  }

  @Override public void trackCountryException(String countryCode, String mLanguageIsoCode) {
    dependencyInjector.provideCrashlyticsController()
        .trackNonFatal(PassengerCountryException.newInstance(countryCode, mLanguageIsoCode));
  }
}
