package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

public class StateValidator extends BaseValidator implements FieldValidator {

  private static final String CHARACTERS_ALLOWED =
      "-/, A-Za-zÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜäëïöüşŞıŠšĞğŸÿƒµçÇ'ŒœßØøÅåÆæÞþÐ ";
  private static final String REGEX_LATIN_CHARS = "[" + CHARACTERS_ALLOWED + "]{1,30}";

  public StateValidator() {
    super();
  }

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_LATIN_CHARS);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
