package com.odigeo.app.android.lib.models.dto.responses;

import com.globant.roboneck.common.NeckCookie;
import com.odigeo.app.android.lib.models.dto.HotelSearchResultsDTO;
import com.odigeo.app.android.lib.models.dto.MembershipPerksDTO;
import com.odigeo.app.android.lib.models.dto.SearchResultsPageDTO;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.shoppingCart.FlowConfigurationResponse;
import java.io.Serializable;
import java.util.List;

public class FlightSearchResponse extends PreferencesAwareResponseDTO implements Serializable {

  private SearchResultsPageDTO itineraryResultsPage;
  private HotelSearchResultsDTO hotelResults;

  private Boolean requiresDisambiguation;
  private Boolean requiresResidentConfirmation;

  private FlowConfigurationResponse flowConfiguration;

  private TravelType travelType;
  private List<NeckCookie> cookies;

  private String locale;
  private String currency;
  private String mkportal;
  private String webSite;

  public final String getLocale() {
    return locale;
  }

  public final void setLocale(String locale) {
    this.locale = locale;
  }

  public final String getCurrency() {
    return currency;
  }

  public final void setCurrency(String currency) {
    this.currency = currency;
  }

  public final SearchResultsPageDTO getItineraryResultsPage() {
    return itineraryResultsPage;
  }

  public final void setItineraryResultsPage(SearchResultsPageDTO itineraryResultsPage) {
    this.itineraryResultsPage = itineraryResultsPage;
  }

  public final String getWebSite() {
    return webSite;
  }

  public final void setWebSite(String webSite) {
    this.webSite = webSite;
  }

  public final Boolean getRequiresDisambiguation() {
    return requiresDisambiguation;
  }

  public final void setRequiresDisambiguation(Boolean requiresDisambiguation) {
    this.requiresDisambiguation = requiresDisambiguation;
  }

  public final String getMkportal() {
    return mkportal;
  }

  public final void setMkportal(String mkportal) {
    this.mkportal = mkportal;
  }

  public final Boolean getRequiresResidentConfirmation() {
    return requiresResidentConfirmation;
  }

  public final void setRequiresResidentConfirmation(Boolean requiresResidentConfirmation) {
    this.requiresResidentConfirmation = requiresResidentConfirmation;
  }

  public final FlowConfigurationResponse getFlowConfiguration() {
    return flowConfiguration;
  }

  public final void setFlowConfiguration(FlowConfigurationResponse flowConfiguration) {
    this.flowConfiguration = flowConfiguration;
  }

  public final HotelSearchResultsDTO getHotelResults() {
    return hotelResults;
  }

  public final void setHotelResults(HotelSearchResultsDTO hotelResults) {
    this.hotelResults = hotelResults;
  }

  public final TravelType getTravelType() {
    return travelType;
  }

  public final void setTravelType(TravelType travelType) {
    this.travelType = travelType;
  }

  public final List<NeckCookie> getCookies() {
    return cookies;
  }

  public final void setCookies(List<NeckCookie> cookies) {
    this.cookies = cookies;
  }

}
