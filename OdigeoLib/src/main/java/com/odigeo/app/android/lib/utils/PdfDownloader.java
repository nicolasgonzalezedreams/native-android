package com.odigeo.app.android.lib.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.base.BlackDialog;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class PdfDownloader extends AsyncTask<String, Integer, Boolean> {
  private static final String APPLICATION_PDF = "application/pdf";
  private static final int BUFFER_SIZE = 4096;
  private static final String CHOOSER_TITLE = "PDF";
  private final Context mContext;
  private BlackDialog mProgressDialog;
  private String mPdfName;
  private File mPdf;
  private boolean mIsPdfReaderAvailable;

  public PdfDownloader(Context context) {
    mContext = context;
    mProgressDialog = new BlackDialog(mContext, true);
  }

  PdfDownloader(Context context, String pdfName) {
    mContext = context;
    mPdfName = pdfName;
    mProgressDialog = new BlackDialog(mContext, true);
  }

  @Override protected Boolean doInBackground(String... urls) {
    if (urls != null && urls.length == 1 && mIsPdfReaderAvailable) {
      String url = urls[0];
      if (mPdfName == null) {
        mPdfName = Uri.parse(url).getLastPathSegment();
      }
      mPdf = getPdfFile();
      return mPdf.exists() || url != null && downloadPdf(url);
    } else {
      return null;
    }
  }

  @Override protected void onPreExecute() {
    super.onPreExecute();
    mIsPdfReaderAvailable = checkPdfReader();
    if (mIsPdfReaderAvailable) {
      showProgressDialog();
    } else {
      warnAboutPdfReaderMissing();
    }
  }

  @Override protected void onPostExecute(Boolean success) {
    mProgressDialog.dismiss();
    if (success != null) {
      if (success) {
        openPdf();
      } else {
        showConnectionError();
      }
    }
  }

  private void showProgressDialog() {
    CharSequence message =
        LocalizablesFacade.getString(mContext, OneCMSKeys.LOADINGVIEWCONTROLLER_MESSAGE_LOADING);
    mProgressDialog.show(message);
  }

  private boolean downloadPdf(String urlFile) {
    boolean result = true;
    HttpURLConnection connection = null;
    try {
      URL url = new URL(normalizePdfUrl(urlFile));
      connection = ((HttpURLConnection) url.openConnection());
      int responseCode = connection.getResponseCode();
      if (responseCode == HttpURLConnection.HTTP_OK) {
        saveFile(connection);
      } else {
        result = false;
      }
    } catch (IOException e) {
      Log.e(Constants.TAG_LOG, e.getMessage(), e);
      result = false;
    } finally {
      if (connection != null) {
        connection.disconnect();
      }
    }
    return result;
  }

  private String normalizePdfUrl(String url) {

    if (url.startsWith("http://") || url.startsWith("https://")) {
      return url;
    }

    return "http://" + url;
  }

  private void saveFile(HttpURLConnection connection) throws IOException {
    InputStream inputStream = connection.getInputStream();
    FileOutputStream outputStream = new FileOutputStream(mPdf);
    int bytesRead;
    byte[] buffer = new byte[BUFFER_SIZE];
    while ((bytesRead = inputStream.read(buffer)) != -1) {
      outputStream.write(buffer, 0, bytesRead);
    }
    outputStream.close();
    inputStream.close();
  }

  private File getPdfFile() {
    File cacheFileDir = mContext.getExternalCacheDir();
    if (cacheFileDir == null) {
      cacheFileDir = mContext.getCacheDir();
    }
    return new File(cacheFileDir, mPdfName);
  }

  private void showConnectionError() {
    Toast.makeText(mContext,
        LocalizablesFacade.getString(mContext, OneCMSKeys.ERROR_NO_CONNECTION_TITLE),
        Toast.LENGTH_SHORT).show();
  }

  private void openPdf() {
    Intent target = new Intent(Intent.ACTION_VIEW);
    target.setDataAndType(Uri.fromFile(mPdf), APPLICATION_PDF);
    target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
    Intent intent = Intent.createChooser(target, CHOOSER_TITLE);
    mContext.startActivity(intent);
  }

  private void warnAboutPdfReaderMissing() {
    new AlertDialog.Builder(mContext).setTitle(
        LocalizablesFacade.getString(mContext, OneCMSKeys.STRING_NEW_THIRD_PARTY_APP_NEEDED))
        .setMessage(LocalizablesFacade.getString(mContext, OneCMSKeys.STRING_NEW_PDF_READER_NEEDED))
        .setCancelable(false)
        .setPositiveButton(LocalizablesFacade.getString(mContext, OneCMSKeys.COMMON_OK), null)
        .show();
  }

  private boolean checkPdfReader() {
    Intent intent = new Intent(Intent.ACTION_VIEW);
    intent.setType(APPLICATION_PDF);
    PackageManager manager = mContext.getPackageManager();
    List<ResolveInfo> list = manager.queryIntentActivities(intent, 0);
    return list != null && !list.isEmpty();
  }
}
