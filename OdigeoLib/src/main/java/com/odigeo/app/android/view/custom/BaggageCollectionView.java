package com.odigeo.app.android.view.custom;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.model.BaggageCollectionItemViewModel;
import com.odigeo.app.android.view.BaseCustomWidget;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.interfaces.BaggageCollectionItemListener;
import com.odigeo.app.android.view.interfaces.BaggageCollectionListener;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.shoppingCart.BaggageConditions;
import com.odigeo.data.entity.shoppingCart.Itinerary;
import com.odigeo.data.entity.shoppingCart.SegmentTypeIndex;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.data.entity.shoppingCart.request.BaggageSelectionRequest;
import com.odigeo.presenter.BaggageCollectionPresenter;
import com.odigeo.presenter.contracts.views.BaggageCollection;
import com.odigeo.presenter.contracts.views.BaggageCollectionItem;
import java.util.ArrayList;
import java.util.List;

public class BaggageCollectionView extends BaseCustomWidget<BaggageCollectionPresenter>
    implements BaggageCollection, BaggageCollectionItemListener {

  private LinearLayout baggageContainer, baggageItemsContainer;
  private List<BaggageCollectionItem> baggageCollectionItemsList;
  private TextView baggageHeaderTitle, baggageHeaderSubtitle;
  private BaggageCollectionListener baggageCollectionListener;
  private boolean isDefaultBaggageSelection;

  public BaggageCollectionView(Context context, List<BaggageConditions> baggageConditionsList,
      Itinerary itinerary, List<Traveller> travellers, int passengerWidgetPosition,
      TravelType travelType, BaggageCollectionListener baggageCollectionListener) {
    super(context);
    this.baggageCollectionListener = baggageCollectionListener;

    presenter.processBaggage(baggageConditionsList, itinerary, travellers, passengerWidgetPosition,
        travelType);
  }

  public BaggageCollectionView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public void initComponent() {
    baggageContainer = (LinearLayout) findViewById(R.id.baggageCollectionContainer);
    baggageHeaderTitle = (TextView) findViewById(R.id.tvBaggageHeaderTitle);
    baggageHeaderSubtitle = (TextView) findViewById(R.id.tvBaggageHeaderSubtitle);
    baggageItemsContainer = (LinearLayout) findViewById(R.id.llContainer);
    baggageCollectionItemsList = new ArrayList<>();
  }

  @Override public int getComponentLayout() {
    return R.layout.baggage_collection;
  }

  @Override public void initOneCMSText() {
    baggageHeaderTitle.setText(
        localizables.getString(OneCMSKeys.PASSENGER_BAGGAGECONDITIONS_HEADER_TITLE));
    baggageHeaderSubtitle.setText(HtmlUtils.formatHtml(
        localizables.getString(OneCMSKeys.PASSENGER_BAGGAGECONDITIONS_PERSUASION_NO)));
  }

  @Override protected BaggageCollectionPresenter setPresenter() {
    return dependencyInjector.provideBaggageCollectionPresenter(this);
  }

  @Override public void showHeaderSubtitleInfo() {
    baggageHeaderSubtitle.setVisibility(VISIBLE);
  }

  @Override public void showPersuasiveMessage() {
    baggageHeaderSubtitle.setText(HtmlUtils.formatHtml(
        localizables.getString(OneCMSKeys.PASSENGER_BAGGAGECONDITIONS_PERSUASION)));
  }

  @Override public void setDefaultBaggageSelectionToAllPassengers(boolean isChecked) {
    isDefaultBaggageSelection = isChecked;
  }

  @Override public void hideBaggageCollection() {
    baggageContainer.setVisibility(GONE);
  }

  @Override public List<BaggageSelectionRequest> getBaggageSelections() {
    return presenter.getBaggageSelections(baggageCollectionItemsList);
  }

  @Override
  public void updateBaggageSelections(List<BaggageSelectionRequest> baggageSelectionRequestList) {
    for (BaggageCollectionItem baggageCollectionItem : baggageCollectionItemsList) {
      BaggageSelectionRequest baggageSelectionRequest = getBaggageSelectionBySegmentIndex
          (baggageSelectionRequestList, baggageCollectionItem.getSegmentTypeIndex());
      if (baggageSelectionRequest != null) {
        baggageCollectionItem.updateSelection(baggageSelectionRequest.getBaggageDescriptor());
      } else {
        baggageCollectionItem.clearSelection();
      }
    }
  }

  private BaggageSelectionRequest getBaggageSelectionBySegmentIndex(
      List<BaggageSelectionRequest> baggageSelectionRequestList,
      SegmentTypeIndex segmentTypeIndex) {
    if (segmentTypeIndex == null && !baggageSelectionRequestList.isEmpty()) {
      return baggageSelectionRequestList.get(0);
    }

    for (BaggageSelectionRequest baggageSelectionRequest : baggageSelectionRequestList) {
      if (baggageSelectionRequest.getSegmentTypeIndex().equals(segmentTypeIndex)) {
        return baggageSelectionRequest;
      }
    }
    return null;
  }

  @Override
  public void addBaggageCollectionItem(BaggageConditions baggageConditions, String segmentDirection,
      String carrierCode, String departure, String arrival, int preselectedBaggagePosition,
      boolean hasSelectableBaggage, boolean isRoundTrip) {
    BaggageCollectionItemViewModel baggageCollectionItemViewModel =
        new BaggageCollectionItemViewModel(segmentDirection, carrierCode, departure, arrival,
            isRoundTrip);
    BaggageCollectionItemView baggageCollectionItemView =
        new BaggageCollectionItemView(getContext(), baggageCollectionItemViewModel, this,
            baggageConditions, preselectedBaggagePosition);
    if (hasSelectableBaggage) {
      baggageCollectionItemsList.add(baggageCollectionItemView);
    }
    baggageItemsContainer.addView(baggageCollectionItemView);
  }

  @Override public void addSeparatorOnBaggageList() {
    View v = new View(getContext());
    v.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
        (int) getResources().getDisplayMetrics().density));
    v.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.mono02));

    baggageItemsContainer.addView(v);
  }

  @Override public String getOutBound() {
    return localizables.getString(OneCMSKeys.COMMON_OUTBOUND);
  }

  @Override public String getInBound() {
    return localizables.getString(OneCMSKeys.COMMON_INBOUND);
  }

  @Override public String getLeg(int position) {
    return localizables.getString(OneCMSKeys.COMMON_LEG) + " " + (position + 1);
  }

  @Override public String getRoundTrip() {
    return localizables.getString(OneCMSKeys.ROUND_TRIP_TAB_TEXT);
  }

  @Override public void onBaggageSelectionItemChange() {
    baggageCollectionListener.onBaggageSelectionChange(isDefaultBaggageSelection);
  }
}