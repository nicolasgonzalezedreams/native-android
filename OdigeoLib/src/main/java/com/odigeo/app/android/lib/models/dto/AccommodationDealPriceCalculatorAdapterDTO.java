package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class AccommodationDealPriceCalculatorAdapterDTO {

  protected FeeInfoDTO feeInfo;
  protected BigDecimal totalPrice;
  protected BigDecimal markup;

  /**
   * Gets the value of the feeInfo property.
   *
   * @return possible object is
   * {@link FeeInfoDTO }
   */
  public FeeInfoDTO getFeeInfo() {
    return feeInfo;
  }

  /**
   * Sets the value of the feeInfo property.
   *
   * @param value allowed object is
   * {@link FeeInfoDTO }
   */
  public void setFeeInfo(FeeInfoDTO value) {
    this.feeInfo = value;
  }

  /**
   * Gets the value of the totalPrice property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  /**
   * Sets the value of the totalPrice property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setTotalPrice(BigDecimal value) {
    this.totalPrice = value;
  }

  /**
   * Gets the value of the markup property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getMarkup() {
    return markup;
  }

  /**
   * Sets the value of the markup property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setMarkup(BigDecimal value) {
    this.markup = value;
  }
}
