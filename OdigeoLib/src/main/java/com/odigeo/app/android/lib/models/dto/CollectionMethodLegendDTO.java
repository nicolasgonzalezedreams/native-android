package com.odigeo.app.android.lib.models.dto;

public class CollectionMethodLegendDTO {

  protected CollectionMethodDTO collectionMethod;
  protected int id;

  /**
   * Gets the value of the collectionMethod property.
   *
   * @return possible object is
   * {@link CollectionMethod }
   */
  public CollectionMethodDTO getCollectionMethod() {
    return collectionMethod;
  }

  /**
   * Sets the value of the collectionMethod property.
   *
   * @param value allowed object is
   * {@link CollectionMethod }
   */
  public void setCollectionMethod(CollectionMethodDTO value) {
    this.collectionMethod = value;
  }

  /**
   * Gets the value of the id property.
   */
  public int getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   */
  public void setId(int value) {
    this.id = value;
  }
}
