package com.odigeo.app.android.lib.models;

/**
 * Created by ManuelOrtiz on 29/09/2014.
 */
public class PersonTitle {
  private static final int HASH_CODE = 19;
  private static final int HASH_CODE_MULTIPLIER = 31;
  private String id;
  private String stringId;
  private String titleName;

  public PersonTitle(String id) {
    this.id = id;
  }

  public PersonTitle(String id, String stringId) {
    this.id = id;
    this.stringId = stringId;
  }

  public final String getId() {
    return id;
  }

  public final void setId(String id) {
    this.id = id;
  }

  public final String getStringId() {
    return stringId;
  }

  public final void setStringId(String stringId) {
    this.stringId = stringId;
  }

  public final String getTitleName() {
    return titleName;
  }

  public final void setTitleName(String titleName) {
    this.titleName = titleName;
  }

  @Override public final boolean equals(Object object) {
    if (!(object instanceof PersonTitle)) {
      return false;
    }
    return this.getId().equals(((PersonTitle) object).getId());
  }

  public final int hashCode() {
    int hashCode = HASH_CODE * HASH_CODE_MULTIPLIER + id.hashCode();
    return hashCode;
  }
}
