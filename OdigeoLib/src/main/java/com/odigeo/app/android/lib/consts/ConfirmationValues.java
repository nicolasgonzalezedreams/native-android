package com.odigeo.app.android.lib.consts;

/**
 * Created by Irving Lóp on 11/10/2014.
 */
public final class ConfirmationValues {
  public static final String BOOKING_ERROR = "BOOKING_ERROR";
  public static final String BOOKING_PAYMENT_RETRY = "BOOKING_PAYMENT_RETRY";
  public static final String BOOKING_CONFIRMED = "BOOKING_CONFIRMED";
  public static final String BOOKING_REPRICING = "BOOKING_REPRICING";
  public static final String BOOKING_FLOW = "BOOKING_FLOW";
  public static final String BOOKING_STOP = "BOOKING_STOP";

  private ConfirmationValues() {

  }
}
