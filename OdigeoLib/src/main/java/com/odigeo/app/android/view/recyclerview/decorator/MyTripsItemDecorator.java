package com.odigeo.app.android.view.recyclerview.decorator;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.odigeo.app.android.view.adapters.holders.DividerProviderViewHolder;

public class MyTripsItemDecorator extends DividerItemDecoration {

  public MyTripsItemDecorator(Context context, int orientation) {
    super(context, orientation);
  }

  @Override public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
      RecyclerView.State state) {

    RecyclerView.ViewHolder viewHolder = parent.getChildViewHolder(view);
    if (viewHolder instanceof DividerProviderViewHolder) {
      DividerProviderViewHolder dividerProviderViewHolder = (DividerProviderViewHolder) viewHolder;
      setDrawable(
          ContextCompat.getDrawable(view.getContext(), dividerProviderViewHolder.getDividerDrawable()));
      super.getItemOffsets(outRect, view, parent, state);
    }
  }
}
