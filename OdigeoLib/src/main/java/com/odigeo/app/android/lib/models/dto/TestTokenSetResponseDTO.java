package com.odigeo.app.android.lib.models.dto;

import java.util.ArrayList;
import java.util.List;

public class TestTokenSetResponseDTO {

  protected TestTokenSetCookieDTO cookie;
  protected List<PartitionResponseDTO> partitions;

  /**
   * Gets the value of the cookie property.
   *
   * @return possible object is
   * {@link TestTokenSetCookie }
   */
  public TestTokenSetCookieDTO getCookie() {
    return cookie;
  }

  /**
   * Sets the value of the cookie property.
   *
   * @param value allowed object is
   * {@link TestTokenSetCookie }
   */
  public void setCookie(TestTokenSetCookieDTO value) {
    this.cookie = value;
  }

  /**
   * Gets the value of the partitions property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the partitions property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getPartitions().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list
   * {@link PartitionResponseDTO }
   */
  public List<PartitionResponseDTO> getPartitions() {
    if (partitions == null) {
      partitions = new ArrayList<PartitionResponseDTO>();
    }
    return this.partitions;
  }
}
