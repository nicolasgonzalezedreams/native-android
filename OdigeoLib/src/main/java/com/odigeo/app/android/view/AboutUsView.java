package com.odigeo.app.android.view;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.ShareAppModel;
import com.odigeo.app.android.providers.EmailProvider;
import com.odigeo.app.android.utils.ShareUtils;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.custom.AboutInfoItem;
import com.odigeo.app.android.view.dialogs.FeedBackDialog;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.AboutUsPresenter;
import com.odigeo.presenter.contracts.navigators.AboutUsNavigatorInterface;
import com.odigeo.presenter.contracts.views.AboutUsViewInterface;

public class AboutUsView extends BaseView<AboutUsPresenter> implements AboutUsViewInterface {

  private AboutInfoItem mBtnAboutFaq;
  private AboutInfoItem mBtnAboutContact;
  private AboutInfoItem mBtnAboutCondVent;
  private AboutInfoItem mBtnAboutFeedback;
  private AboutInfoItem mBtnAboutShare;
  private AboutInfoItem mBtnAboutRate;
  private Button mBtnHelpCenter;
  private TextView mTvAboutTitle;
  private TextView mTvHelpCenterTitle;
  private TextView mTvHelpCenterSubtitle;
  private TextView mTvHelpCenterItem1;
  private TextView mTvHelpCenterItem2;
  private TextView mTvHelpCenterItem3;
  private FeedBackDialog feedBackDialog;
  private EmailProvider emailProvider;

  @Override public void onAttach(Context context) {
    super.onAttach(context);

    emailProvider = AndroidDependencyInjector.getInstance().provideEmailProvider();
  }

  @Override public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    mPresenter.setActionBarTitle(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.ABOUTVIEWCONTROLLER_TITLE)
            .toString());
  }

  @Override protected AboutUsPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideAboutUsPresenter(this, (AboutUsNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_about_fragment;
  }

  @Override protected void initComponent(View view) {
    mBtnHelpCenter = (Button) view.findViewById(R.id.button_help_center);
    mBtnAboutFaq = (AboutInfoItem) view.findViewById(R.id.wdgtAboutFaqNameAndTitle);
    mBtnAboutContact = (AboutInfoItem) view.findViewById(R.id.wdgtAboutContactNameAndTitle);
    mBtnAboutCondVent = (AboutInfoItem) view.findViewById(R.id.wdgtAboutCondVentNameAndTitle);
    mBtnAboutFeedback = (AboutInfoItem) view.findViewById(R.id.wdgtAboutFeedbackNameAndTitle);
    mBtnAboutShare = (AboutInfoItem) view.findViewById(R.id.wdgtAboutShareNameAndTitle);
    mBtnAboutRate = (AboutInfoItem) view.findViewById(R.id.wdgtAboutRateNameAndTitle);

    mTvAboutTitle = (TextView) view.findViewById(R.id.tvAboutTitle);
    mTvHelpCenterTitle = (TextView) view.findViewById(R.id.helpcenter_title);
    mTvHelpCenterSubtitle = (TextView) view.findViewById(R.id.helpcenter_subtitle);
    mTvHelpCenterItem1 = (TextView) view.findViewById(R.id.text_helpcenter_item1);
    mTvHelpCenterItem2 = (TextView) view.findViewById(R.id.text_helpcenter_item2);
    mTvHelpCenterItem3 = (TextView) view.findViewById(R.id.text_helpcenter_item3);
  }

  @Override protected void setListeners() {
    mBtnAboutFaq.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        postGAEventAboutOption(GAnalyticsNames.LABEL_ABOUT_FAQ);
        mPresenter.openAboutFaq();
      }
    });
    mBtnAboutContact.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_CONTACT_US);
        postGAEventAboutOption(GAnalyticsNames.LABEL_ABOUT_CONTACT_US);
        mPresenter.openAboutContactUs();
      }
    });
    mBtnAboutCondVent.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_ABOUT_TERMS_AND_CONDITIONS);
        postGAEventAboutOption(GAnalyticsNames.LABEL_ABOUT_T_CS);
        mPresenter.openTermsAndConditions();
      }
    });
    mBtnAboutFeedback.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        postGAEventAboutOption(GAnalyticsNames.LABEL_ABOUT_FEEDBACK);

        feedBackDialog = new FeedBackDialog(getActivity(), emailProvider);
        feedBackDialog.show();
      }
    });
    mBtnAboutShare.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        postGAEventAboutOption(GAnalyticsNames.LABEL_ABOUT_SHARE);
        ShareAppModel shareAppModel = new ShareAppModel(getContext());
        String storeUrl = Configuration.getInstance().getAppStoreURL();
        shareAppModel.createFacebookShareText(storeUrl);
        shareAppModel.createCopyToClipboardShareText(OneCMSKeys.SHARING_APP_COPY_TO_CLIPBOARD,
            storeUrl);
        shareAppModel.createTwitterShareText(OneCMSKeys.SHARING_APP_TWITTER_DESCRIPTION, storeUrl);
        shareAppModel.createFbMessengerShareText(OneCMSKeys.SHARING_APP_FB_MESSENGER, storeUrl);
        shareAppModel.createWhatsappShareText(OneCMSKeys.SHARING_APP_WHATSAPP, storeUrl);
        shareAppModel.createSmsShareText(OneCMSKeys.SHARING_APP_SMS, storeUrl);
        shareAppModel.createEmailSubject(OneCMSKeys.SHARING_APP_EMAIL_TITLE);
        shareAppModel.createEmailBody(OneCMSKeys.SHARING_APP_EMAIL_BODY, storeUrl);
        ShareUtils.getInstance().share(getContext(), shareAppModel);
      }
    });
    mBtnAboutRate.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        postGAEventAboutOption(GAnalyticsNames.LABEL_ABOUT_RATE);
        mPresenter.openAppRate();
      }
    });
    mBtnHelpCenter.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mTracker.trackHelpCenterButtonAbout();
        mPresenter.openHelpCenter();
      }
    });
  }

  @Override public void onStop() {
    super.onStop();

    if (feedBackDialog != null) {
      feedBackDialog.dismiss();
    }
  }

  @Override protected void initOneCMSText(View view) {
    mBtnHelpCenter.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.HELPCENTER_BUTTON).toString());
    mBtnAboutFaq.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.ABOUT_BUTTONS_FAQ).toString());
    mBtnAboutContact.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.ABOUT_BUTTONS_CONTACT_US)
            .toString());
    mBtnAboutCondVent.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.ABOUT_BUTTONS_TERMS).toString());
    mBtnAboutFeedback.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.ABOUT_BUTTONS_FEEDBACK).toString());
    mBtnAboutShare.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.ABOUT_BUTTONS_SHAREAPP).toString());
    mBtnAboutRate.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.ABOUT_BUTTONS_RATEAPP,
            Configuration.getInstance().getBrand()).toString());

    mTvAboutTitle.setText(LocalizablesFacade.getString(getActivity().getApplicationContext(),
        OneCMSKeys.ABOUTOPTIONSMODULE_HEADERTITLE));
    mTvHelpCenterTitle.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.HELPCENTER_HELP_TITLE_2));
    mTvHelpCenterSubtitle.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.HELPCENTER_SUBTITLE));
    mTvHelpCenterItem1.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.HELPCENTER_ITEM1));
    mTvHelpCenterItem2.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.HELPCENTER_ITEM2));
    mTvHelpCenterItem3.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.HELPCENTER_ITEM3));
  }

  private void postGAEventAboutOption(String label) {
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_ABOUT_US,
        TrackerConstants.ACTION_ABOUT_US, label);
  }
}
