package com.odigeo.app.android.lib.modules;

import android.content.Context;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 22/09/15
 */
public class GAWidgetEvents {
  /**
   * If it's clicked a recent search of the widget.
   */
  public static final String RECENT_TAP = "recent";
  /**
   * If it's clicked the button for a new search.
   */
  public static final String NEW_TAP = "new";

  /**
   * Constructor to prevent the instantiation.
   */
  private GAWidgetEvents() {
    // Nothing
  }

  /**
   * Post a Google Analytics event for the Widget on the empty case, when it's clicked the button
   * to launch a new search.
   */
  public static void postEmptyTap(Context context) {
    BusProvider.getInstance().register(context);
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_WIDGET,
            GAnalyticsNames.ACTION_WIDGET_EMPTY, GAnalyticsNames.LABEL_WIDGET_EMPTY));
    BusProvider.getInstance().unregister(context);
  }

  /**
   * Post a Google Analytics event for the Widget on the search case, when it's clicked a search
   * or the button to launch a new search.
   *
   * @param tapType The type of event to be called. MUST be {@link #RECENT_TAP} or {@link #NEW_TAP}
   * value.
   */
  public static void postSearchTap(String tapType, Context context) {
    BusProvider.getInstance().register(context);
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_WIDGET,
            GAnalyticsNames.ACTION_WIDGET_SEARCH,
            String.format(GAnalyticsNames.LABEL_WIDGET_SEARCH, tapType)));
    BusProvider.getInstance().unregister(context);
  }

  /**
   * Post a Google Analytics event for the Widget on the booked case, when it's clicked the
   * widget.
   */
  public static void postBookedTap(Context context) {
    BusProvider.getInstance().register(context);
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_WIDGET,
            GAnalyticsNames.ACTION_WIDGET_BOOKED, GAnalyticsNames.LABEL_WIDGET_BOOKED));
    BusProvider.getInstance().unregister(context);
  }

  /**
   * Post a Google Analytics event for the first update of the Widget.
   */
  public static void postFirstUpdate(Context context) {
    BusProvider.getInstance().register(context);
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_WIDGET,
            GAnalyticsNames.ACTION_WIDGET_FIRST_UPDATE, GAnalyticsNames.LABEL_WIDGET_FIRST_UPDATE));
    BusProvider.getInstance().unregister(context);
  }
}
