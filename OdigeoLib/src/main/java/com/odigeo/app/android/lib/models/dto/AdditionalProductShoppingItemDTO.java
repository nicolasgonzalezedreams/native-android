package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class AdditionalProductShoppingItemDTO {

  protected AdditionalProductDTO additionalProduct;

  protected BigDecimal totalPrice;
  protected long id;

  /**
   * Gets the value of the additionalProduct property.
   *
   * @return possible object is
   * {@link AdditionalProduct }
   */
  public AdditionalProductDTO getAdditionalProduct() {
    return additionalProduct;
  }

  /**
   * Sets the value of the additionalProduct property.
   *
   * @param value allowed object is
   * {@link AdditionalProduct }
   */
  public void setAdditionalProduct(AdditionalProductDTO value) {
    this.additionalProduct = value;
  }

  /**
   * Gets the value of the totalPrice property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  /**
   * Sets the value of the totalPrice property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setTotalPrice(BigDecimal value) {
    this.totalPrice = value;
  }

  /**
   * Gets the value of the id property.
   */
  public long getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   */
  public void setId(long value) {
    this.id = value;
  }
}
