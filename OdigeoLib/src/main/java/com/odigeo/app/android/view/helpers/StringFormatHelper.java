package com.odigeo.app.android.view.helpers;

import com.odigeo.data.entity.booking.Country;

public class StringFormatHelper {

  private static final String PATTERN_CODE_PHONE = "(%s) %s";
  private static final String PATTERN_PASSENGER_CARD_TITLE = "%s %02d (%s)";
  private static final String PATTERN_NAME_EMPTY_PASSENGER = "%s %02d";

  public static final String formatPhoneCode(Country country) {
    return String.format(PATTERN_CODE_PHONE, country.getPhonePrefix(), country.getName());
  }

  public static final String formatPassengerTittle(String passengerFromOneCms, int numberPassenger,
      String passengerType) {
    return String.format(PATTERN_PASSENGER_CARD_TITLE, passengerFromOneCms, numberPassenger,
        passengerType);
  }

  public static final String formatDefaultPassengerName(String passengerFromOneCms,
      int numberPassenger) {
    return String.format(PATTERN_NAME_EMPTY_PASSENGER, passengerFromOneCms, numberPassenger);
  }
}
