package com.odigeo.app.android.view.snackbars;

import android.content.Context;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.utils.HtmlUtils;

public class CustomSnackbar {

  private static final int SNACKBAR_MAX_LINES = 5;

  private Context mContext;
  private Snackbar mSnackbar;
  private String mPersuasionMessage, mCloseMessage;
  private int mSnackbarDuration;
  private View mView;
  private CustomSnackbarListener mListener;

  public CustomSnackbar(Context context, View view, String persuasionMessage, String closeMessage,
      int snackbarDuration, CustomSnackbarListener listener) {
    mContext = context;
    mPersuasionMessage = persuasionMessage;
    mCloseMessage = closeMessage;
    mSnackbarDuration = snackbarDuration;
    mView = view;
    mListener = listener;
  }

  public CustomSnackbar(Context context, View view, String persuasionMessage, String closeMessage,
      int snackbarDuration) {
    mContext = context;
    mPersuasionMessage = persuasionMessage;
    mCloseMessage = closeMessage;
    mSnackbarDuration = snackbarDuration;
    mView = view;
  }

  public CustomSnackbar(Context context, View view, String persuasionMessage,
      int snackbarDuration) {
    mContext = context;
    mPersuasionMessage = persuasionMessage;
    mSnackbarDuration = snackbarDuration;
    mView = view;
  }

  public Snackbar provideCustomSnackbar() {
    mSnackbar = Snackbar.make(mView, HtmlUtils.formatHtml(mPersuasionMessage), mSnackbarDuration)
        .setAction(mCloseMessage, new View.OnClickListener() {
          @Override public void onClick(View v) {
            if (mListener != null) {
              mListener.onSnackbarActionClick();
            } else {
              mSnackbar.dismiss();
            }
          }
        });
    customizeSnackbar();
    mSnackbar.setActionTextColor(ContextCompat.getColor(mContext, R.color.snack_bar_action));
    return mSnackbar;
  }

  public Snackbar provideCustomSimpleSnackbar() {

    mSnackbar = Snackbar.make(mView, HtmlUtils.formatHtml(mPersuasionMessage), mSnackbarDuration);
    customizeSnackbar();
    return mSnackbar;
  }

  private void customizeSnackbar() {
    ViewGroup snackBarLayout = (ViewGroup) mSnackbar.getView();

    for (int i = 0; i < snackBarLayout.getChildCount(); i++) {
      View child = snackBarLayout.getChildAt(i);
      if (child instanceof TextView && !(child instanceof Button)) {
        TextView tvSnackbarMessage = (TextView) child;
        tvSnackbarMessage.setMaxLines(SNACKBAR_MAX_LINES);
      }
    }

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
      snackBarLayout.setBackground(
          ContextCompat.getDrawable(mContext, R.color.snack_bar_background));
    } else {
      snackBarLayout.setBackgroundDrawable(
          ContextCompat.getDrawable(mContext, R.color.snack_bar_background));
    }
  }

  public void show() {
    provideCustomSnackbar().show();
  }

  public void dismiss() {
    if (mSnackbar != null && mSnackbar.isShown()) {
      mSnackbar.dismiss();
    }
  }

  public interface CustomSnackbarListener {

    void onSnackbarActionClick();
  }
}