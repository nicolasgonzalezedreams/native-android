package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;

/**
 * Created by Irving on 22/10/2014.
 */
public class EmptyViewWidget extends LinearLayout implements View.OnClickListener {
  private EmptyViewWidgetListener listener;

  private ImageView image;
  private TextView titleTextView;
  private TextView subTitleTextView;
  private Button actionButton;

  public EmptyViewWidget(Context context) {
    super(context);
    inflateLayout();
  }

  public EmptyViewWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
    inflateLayout();
    setAttributes(attrs);
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.layout_widget_empty_view, this);
    image = (ImageView) findViewById(R.id.image_empty_view_image);
    titleTextView = (TextView) findViewById(R.id.text_empty_view_title);
    subTitleTextView = (TextView) findViewById(R.id.text_empty_view_subtitle);
    actionButton = (Button) findViewById(R.id.button_empty_view_action);
    actionButton.setOnClickListener(this);
    actionButton.setTypeface(Configuration.getInstance().getFonts().getBold());
  }

  public final void setEmptyListener(EmptyViewWidgetListener listener) {
    this.listener = listener;
  }

  private void setAttributes(AttributeSet attributes) {
    TypedArray array = getContext().obtainStyledAttributes(attributes, R.styleable.EmptyViewWidget);
    int idResourceImage =
        array.getResourceId(R.styleable.EmptyViewWidget_idResourceImage, R.drawable.icon);
    String textTitle = array.getString(R.styleable.EmptyViewWidget_textTitle);
    String textSubTitle = array.getString(R.styleable.EmptyViewWidget_textSubtitle);
    String textButton = array.getString(R.styleable.EmptyViewWidget_textButton);

    setImage(idResourceImage);
    if (textTitle != null) {
      titleTextView.setText(LocalizablesFacade.getString(getContext(), textTitle));
    }
    if (textSubTitle != null) {
      subTitleTextView.setText(LocalizablesFacade.getString(getContext(), textSubTitle));
    }
    if (textButton != null) {
      actionButton.setText(LocalizablesFacade.getString(getContext(), textButton));
    }
  }

  private void setImage(int idResourceImage) {
    image.setImageResource(idResourceImage);
  }

  private void setTextTitle(String textTitle) {
    titleTextView.setText(textTitle);
  }

  private void setTextSubTitle(String textSubTitle) {
    subTitleTextView.setText(textSubTitle);
  }

  private void setTextButton(String textButton) {
    actionButton.setText(textButton);
  }

  @Override public final void onClick(View v) {
    if (listener != null) {
      postGAEventRetry();

      if (v.getId() == actionButton.getId()) {
        listener.clickEmptyViewButton();
      }
    }
  }

  private void postGAEventRetry() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_CONNECTION_LOST,
            GAnalyticsNames.ACTION_CONNECTION_LOST, GAnalyticsNames.LABEL_CONNECTION_LOST_RETRY));
  }

  public interface EmptyViewWidgetListener {
    void clickEmptyViewButton();
  }
}
