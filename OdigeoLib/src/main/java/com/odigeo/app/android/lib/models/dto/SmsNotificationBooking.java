package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class SmsNotificationBooking implements Serializable {

  private BigDecimal totalPrice;
  private BigDecimal providerPrice;

  private BookingStatusDTO bookingStatus;
  private SmsNotification smsNotification;
  private Phone phone;

  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  public void setProviderPrice(BigDecimal providerPrice) {
    this.providerPrice = providerPrice;
  }

  public BookingStatusDTO getBookingStatus() {
    return bookingStatus;
  }

  public void setBookingStatus(BookingStatusDTO bookingStatus) {
    this.bookingStatus = bookingStatus;
  }

  public SmsNotification getSmsNotification() {
    return smsNotification;
  }

  public void setSmsNotification(SmsNotification smsNotification) {
    this.smsNotification = smsNotification;
  }

  public Phone getPhone() {
    return phone;
  }

  public void setPhone(Phone phone) {
    this.phone = phone;
  }

  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }
}
