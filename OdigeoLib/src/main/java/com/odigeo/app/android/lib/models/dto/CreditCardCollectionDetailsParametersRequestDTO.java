package com.odigeo.app.android.lib.models.dto;

public class CreditCardCollectionDetailsParametersRequestDTO {

  protected String cardTypeCode;
  protected String cardOwner;
  protected String cardNumber;
  protected String cardSecurityNumber;
  protected String cardExpirationMonth;
  protected String cardExpirationYear;
  protected int installmentsNumber;

  /**
   * Gets the value of the cardTypeCode property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCardTypeCode() {
    return cardTypeCode;
  }

  /**
   * Sets the value of the cardTypeCode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCardTypeCode(String value) {
    this.cardTypeCode = value;
  }

  /**
   * Gets the value of the cardOwner property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCardOwner() {
    return cardOwner;
  }

  /**
   * Sets the value of the cardOwner property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCardOwner(String value) {
    this.cardOwner = value;
  }

  /**
   * Gets the value of the cardNumber property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCardNumber() {
    return cardNumber;
  }

  /**
   * Sets the value of the cardNumber property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCardNumber(String value) {
    this.cardNumber = value;
  }

  /**
   * Gets the value of the cardSecurityNumber property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCardSecurityNumber() {
    return cardSecurityNumber;
  }

  /**
   * Sets the value of the cardSecurityNumber property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCardSecurityNumber(String value) {
    this.cardSecurityNumber = value;
  }

  /**
   * Gets the value of the cardExpirationMonth property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCardExpirationMonth() {
    return cardExpirationMonth;
  }

  /**
   * Sets the value of the cardExpirationMonth property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCardExpirationMonth(String value) {
    this.cardExpirationMonth = value;
  }

  /**
   * Gets the value of the cardExpirationYear property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCardExpirationYear() {
    return cardExpirationYear;
  }

  /**
   * Sets the value of the cardExpirationYear property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCardExpirationYear(String value) {
    this.cardExpirationYear = value;
  }

  /**
   * Gets the value of the installmentsNumber property.
   */
  public int getInstallmentsNumber() {
    return installmentsNumber;
  }

  /**
   * Sets the value of the installmentsNumber property.
   */
  public void setInstallmentsNumber(int value) {
    this.installmentsNumber = value;
  }
}
