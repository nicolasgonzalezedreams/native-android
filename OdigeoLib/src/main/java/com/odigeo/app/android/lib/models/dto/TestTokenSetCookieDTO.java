package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for TestTokenSetCookie complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="TestTokenSetCookie">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="cookieValue" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="date" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class TestTokenSetCookieDTO {

  protected String cookieValue;
  protected String date;

  /**
   * Gets the value of the cookieValue property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCookieValue() {
    return cookieValue;
  }

  /**
   * Sets the value of the cookieValue property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCookieValue(String value) {
    this.cookieValue = value;
  }

  /**
   * Gets the value of the date property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getDate() {
    return date;
  }

  /**
   * Sets the value of the date property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setDate(String value) {
    this.date = value;
  }
}
