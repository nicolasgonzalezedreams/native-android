package com.odigeo.app.android.lib.models.dto;

import com.odigeo.data.entity.shoppingCart.PromotionalCodeUsageResponse;
import java.math.BigDecimal;
import java.util.List;

@Deprecated public class ShoppingCartDTO {

  protected ShoppingCartPriceCalculatorDTO price;
  protected ItineraryShoppingItemDTO itineraryShoppingItem;
  protected OtherProductsShoppingItemsDTO otherProductsShoppingItems;
  protected List<TravellerInformationDescriptionDTO> requiredTravellerInformation;
  protected BuyerInformationDescriptionDTO requiredBuyerInformation;
  protected List<FormOfIdentificationDescriptorDTO> requiredFoidInformation;
  protected BuyerDTO buyer;
  protected List<TravellerDTO> travellers;
  protected List<ShoppingCartCollectionOptionDTO> collectionOptions;
  protected List<FormOfIdentificationDTO> foids;
  protected List<PromotionalCodeUsageResponse> promotionalCodes;
  protected long bookingId;
  protected BigDecimal totalPrice;
  protected BigDecimal baggageTotalFee;
  protected BigDecimal collectionTotalFee;
  protected AccommodationShoppingItemDTO accommodationShoppingItem;
  protected PromotionalCampaignGroupDTO promotionalCampaignGroup;
  protected String cyberSourceMerchantId;

  /**
   * Gets the value of the price property.
   *
   * @return possible object is
   * {@link ShoppingCartPriceCalculatorDTO }
   */
  public ShoppingCartPriceCalculatorDTO getPrice() {
    return price;
  }

  /**
   * Sets the value of the price property.
   *
   * @param value allowed object is
   * {@link ShoppingCartPriceCalculatorDTO }
   */
  public void setPrice(ShoppingCartPriceCalculatorDTO value) {
    this.price = value;
  }

  /**
   * Gets the value of the itineraryShoppingItem property.
   *
   * @return possible object is
   * {@link ItineraryShoppingItemDTO }
   */
  public ItineraryShoppingItemDTO getItineraryShoppingItem() {
    return itineraryShoppingItem;
  }

  /**
   * Sets the value of the itineraryShoppingItem property.
   *
   * @param value allowed object is
   * {@link ItineraryShoppingItemDTO }
   */
  public void setItineraryShoppingItem(ItineraryShoppingItemDTO value) {
    this.itineraryShoppingItem = value;
  }

  /**
   * Gets the value of the requiredBuyerInformation property.
   *
   * @return possible object is
   * {@link com.odigeo.app.android.lib.models.dto.BuyerInformationDescriptionDTO }
   */
  public BuyerInformationDescriptionDTO getRequiredBuyerInformation() {
    return requiredBuyerInformation;
  }

  /**
   * Sets the value of the requiredBuyerInformation property.
   *
   * @param value allowed object is
   * {@link com.odigeo.app.android.lib.models.dto.BuyerInformationDescriptionDTO }
   */
  public void setRequiredBuyerInformation(BuyerInformationDescriptionDTO value) {
    this.requiredBuyerInformation = value;
  }

  /**
   * Gets the value of the buyer property.
   *
   * @return possible object is
   * {@link BuyerLegacyDTO }
   */
  public BuyerDTO getBuyer() {
    return buyer;
  }

  /**
   * Sets the value of the buyer property.
   *
   * @param value allowed object is
   * {@link BuyerLegacyDTO }
   */
  public void setBuyer(BuyerDTO value) {
    this.buyer = value;
  }

    /*
     * Gets the value of the fraudResultContainer property.
     *
     * @return possible object is
     * {@link FraudResultContainer }
     *

    public FraudResultContainerDTO getFraudResultContainer() {
        return fraudResultContainer;
    }*/

    /*
     * Sets the value of the fraudResultContainer property.
     *
     * @param value allowed object is
     *              {@link FraudResultContainer }
     *

    public void setFraudResultContainer(FraudResultContainerDTO value) {
        this.fraudResultContainer = value;
    }*/

  /**
   * Gets the value of the bookingId property.
   */
  public long getBookingId() {
    return bookingId;
  }

  /**
   * Sets the value of the bookingId property.
   */
  public void setBookingId(long value) {
    this.bookingId = value;
  }

  /**
   * Gets the value of the totalPrice property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  /**
   * Sets the value of the totalPrice property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setTotalPrice(BigDecimal value) {
    this.totalPrice = value;
  }

  /**
   * Gets the value of the baggageTotalFee property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getBaggageTotalFee() {
    return baggageTotalFee;
  }

  /**
   * Sets the value of the baggageTotalFee property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setBaggageTotalFee(BigDecimal value) {
    this.baggageTotalFee = value;
  }

  /**
   * Gets the value of the collectionTotalFee property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getCollectionTotalFee() {
    return collectionTotalFee;
  }

  /**
   * Sets the value of the collectionTotalFee property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setCollectionTotalFee(BigDecimal value) {
    this.collectionTotalFee = value;
  }

  public List<PromotionalCodeUsageResponse> getPromotionalCodes() {
    return promotionalCodes;
  }

  public void setPromotionalCodes(List<PromotionalCodeUsageResponse> promotionalCodes) {
    this.promotionalCodes = promotionalCodes;
  }

  public List<ShoppingCartCollectionOptionDTO> getCollectionOptions() {
    return collectionOptions;
  }

  public void setCollectionOptions(List<ShoppingCartCollectionOptionDTO> collectionOptions) {
    this.collectionOptions = collectionOptions;
  }

  public List<FormOfIdentificationDTO> getFoids() {
    return foids;
  }

  public void setFoids(List<FormOfIdentificationDTO> foids) {
    this.foids = foids;
  }

  public List<FormOfIdentificationDescriptorDTO> getRequiredFoidInformation() {
    return requiredFoidInformation;
  }

  public void setRequiredFoidInformation(
      List<FormOfIdentificationDescriptorDTO> requiredFoidInformation) {
    this.requiredFoidInformation = requiredFoidInformation;
  }

  public List<TravellerDTO> getTravellers() {
    return travellers;
  }

  public void setTravellers(List<TravellerDTO> travellers) {
    this.travellers = travellers;
  }

  public List<TravellerInformationDescriptionDTO> getRequiredTravellerInformation() {
    return requiredTravellerInformation;
  }

  public void setRequiredTravellerInformation(
      List<TravellerInformationDescriptionDTO> requiredTravellerInformation) {
    this.requiredTravellerInformation = requiredTravellerInformation;
  }

  public AccommodationShoppingItemDTO getAccommodationShoppingItem() {
    return accommodationShoppingItem;
  }

  public void setAccommodationShoppingItem(AccommodationShoppingItemDTO accommodationShoppingItem) {
    this.accommodationShoppingItem = accommodationShoppingItem;
  }

  /**
   * Gets the value of the otherProductsShoppingItemContainer property.
   */
  public OtherProductsShoppingItemsDTO getOtherProductsShoppingItems() {
    return otherProductsShoppingItems;
  }

  /**
   * Sets the value of the otherProductsShoppingItemContainer property.
   */
  public void setOtherProductsShoppingItems(
      OtherProductsShoppingItemsDTO otherProductsShoppingItems) {
    this.otherProductsShoppingItems = otherProductsShoppingItems;
  }

  public PromotionalCampaignGroupDTO getPromotionalCampaignGroup() {
    return promotionalCampaignGroup;
  }

  public void setPromotionalCampaignGroup(PromotionalCampaignGroupDTO promotionalCampaignGroup) {
    this.promotionalCampaignGroup = promotionalCampaignGroup;
  }

  public String getCyberSourceMerchantId() {
    return cyberSourceMerchantId;
  }

  public void setCyberSourceMerchantId(String cyberSourceMerchantId) {
    this.cyberSourceMerchantId = cyberSourceMerchantId;
  }
}
