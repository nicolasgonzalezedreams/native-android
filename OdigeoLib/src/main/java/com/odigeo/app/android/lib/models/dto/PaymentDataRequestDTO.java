package com.odigeo.app.android.lib.models.dto;

public class PaymentDataRequestDTO extends BaseRequestDTO {

  protected ShoppingCartBookingRequestDTO bookingRequest;
  protected TestConfigurationConfirmRequestDTO testConfigurationConfirmRequest;

  /**
   * Gets the value of the bookingRequest property.
   *
   * @return possible object is
   * {@link ShoppingCartBookingRequest }
   */
  public ShoppingCartBookingRequestDTO getBookingRequest() {
    return bookingRequest;
  }

  /**
   * Sets the value of the bookingRequest property.
   *
   * @param value allowed object is
   * {@link ShoppingCartBookingRequest }
   */
  public void setBookingRequest(ShoppingCartBookingRequestDTO value) {
    this.bookingRequest = value;
  }

  /**
   * Gets the value of the testConfigurationConfirmRequest property.
   *
   * @return possible object is
   * {@link TestConfigurationConfirmRequest }
   */
  public TestConfigurationConfirmRequestDTO getTestConfigurationConfirmRequest() {
    return testConfigurationConfirmRequest;
  }

  /**
   * Sets the value of the testConfigurationConfirmRequest property.
   *
   * @param value allowed object is
   * {@link TestConfigurationConfirmRequest }
   */
  public void setTestConfigurationConfirmRequest(TestConfigurationConfirmRequestDTO value) {
    this.testConfigurationConfirmRequest = value;
  }
}
