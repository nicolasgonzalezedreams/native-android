package com.odigeo.app.android.lib.ui.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.odigeo.app.android.lib.utils.ChamferedRectangle;

@Deprecated public class CustomLinearLayout extends LinearLayout {

  public CustomLinearLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
    initView();
  }

  public CustomLinearLayout(Context context) {
    super(context);
    initView();
  }

  @SuppressLint("NewApi") private void initView() {
    int sdk = android.os.Build.VERSION.SDK_INT;

    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
      setBackgroundDrawable(new ChamferedRectangle());
    } else {
      setBackground(new ChamferedRectangle());
    }
  }
}
