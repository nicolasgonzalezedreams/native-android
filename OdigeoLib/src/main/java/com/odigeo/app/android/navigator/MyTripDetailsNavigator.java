package com.odigeo.app.android.navigator;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.SharedElementCallback;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.exceptions.BookingException;
import com.odigeo.app.android.utils.BookingUtils;
import com.odigeo.app.android.view.CrossSellingWidget;
import com.odigeo.app.android.view.components.DetailsOfYourTripProvider;
import com.odigeo.app.android.view.components.NextFlightProvider;
import com.odigeo.app.android.view.components.OnlineCheckinProvider;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.custom.GroundTransportationViewBase;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.app.android.view.helpers.PermissionsHelper;
import com.odigeo.app.android.view.imageutils.BookingImageUtil;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import com.odigeo.presenter.MyTripDetailsPresenter;
import com.odigeo.presenter.contracts.navigators.MyTripDetailsNavigatorInterface;
import com.odigeo.presenter.contracts.views.MyTripDetailsViewInterface;
import com.odigeo.tools.CalendarHelperInterface;
import com.odigeo.tools.DateHelperInterface;
import java.util.List;
import org.apache.commons.lang3.text.WordUtils;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.odigeo.app.android.lib.utils.ViewUtils.findById;
import static com.odigeo.app.android.utils.BookingUtils.DEFAULT_CITY_DELIMITER;
import static com.odigeo.app.android.view.constants.OneCMSKeys.ADD_TO_CALENDAR;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_BOOKING_STATUS_PENDING_MESSAGE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LIST_CARD_TITLE_TRIP;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LIST_STATUS_CANCEL;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LIST_STATUS_CONFIRM;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LIST_STATUS_PENDING;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_NAVIGATION_ELEMENTS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_ON_LOAD_EVENTS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_PERMISSIONS_ALERT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_UPCOMING_TRIPS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_APP_PERMISSIONS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_MY_TRIPS_TRIP_INFO;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_AUTO_CHECKIN_SHOWN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_CALENDAR_ACCESS_ALLOW;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_CALENDAR_ACCESS_DENY;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_GO_BACK;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_MYTRIPS_UPCOMING_TRIP_NO;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_MYTRIPS_UPCOMING_TRIP_YES;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_STORAGE_ACCESS_ALLOW;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_STORAGE_ACCESS_DENY;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.SCREEN_MY_TRIPS_DETAILS;

public class MyTripDetailsNavigator extends BaseNavigator
    implements MyTripDetailsNavigatorInterface, MyTripDetailsViewInterface,
    GroundTransportationViewBase.GroundTransportationWidgetListener {

  public static final String BOOKING_ID_FORMAT = "#%s";
  public static final String EXTRA_STATUS_LABEL_FONT_SIZE = ":Extra:StatusLabelFontSize";

  private Booking booking;
  private CalendarHelperInterface mCalendarHelper;
  private DetailsOfYourTripProvider mDetailsOfYourTripProvider;
  private DateHelperInterface dateHelper;
  private CrossSellingWidget mCrossSellingWidget;
  private NextFlightProvider mNextFlightProvider;
  private OnlineCheckinProvider mOnlineCheckinProvider;
  private OdigeoImageLoader<ImageView> imageLoader;
  private MyTripDetailsPresenter presenter;
  private LinearLayout mLlContainerCards;
  private SwipeRefreshLayout swipeRefreshLayout;
  private BookingImageUtil myTripsImageUtil;

  public static Intent startIntent(Context context, Booking booking) {
    Intent intent = new Intent(context, MyTripDetailsNavigator.class);
    intent.putExtra(EXTRA_BOOKING, booking);
    return intent;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.my_trips_details_view);
    mCalendarHelper = dependencyInjector.provideCalendarHelper(this);
    dateHelper = dependencyInjector.provideDateHelper();
    booking = (Booking) getIntent().getSerializableExtra(EXTRA_BOOKING);
    imageLoader = dependencyInjector.provideImageLoader();
    myTripsImageUtil = dependencyInjector.provideMyTripsImageUtil();
    presenter = getPresenter();
    mDetailsOfYourTripProvider = new DetailsOfYourTripProvider(this, booking, tracker, localizables,
        dependencyInjector.provideLocaleHelper(), dependencyInjector.provideFontProvider());
    mNextFlightProvider =
        new NextFlightProvider(this, booking, tracker, dependencyInjector.provideDateHelper(),
            dependencyInjector.provideDurationFormatter(), localizables);
    mOnlineCheckinProvider =
        new OnlineCheckinProvider(this, booking, tracker, dependencyInjector.provideDateHelper());

    trackScreenView();

    setupViews();
  }

  private void trackScreenView() {
    if (booking.isPastBooking()) {
      tracker.trackAnalyticsEvent(CATEGORY_MY_TRIPS_TRIP_INFO, ACTION_UPCOMING_TRIPS,
          LABEL_MYTRIPS_UPCOMING_TRIP_NO);
    } else {
      tracker.trackAnalyticsEvent(CATEGORY_MY_TRIPS_TRIP_INFO, ACTION_UPCOMING_TRIPS,
          LABEL_MYTRIPS_UPCOMING_TRIP_YES);
    }
  }

  private void setupViews() {
    mLlContainerCards = findById(this, R.id.llContainerCards);
    swipeRefreshLayout = findById(this, R.id.swipe_refresh_layout);
    String onlineCheckinUrl = localizables.getRawString(OneCMSKeys.BOARDING_PASS_URL);
    if (presenter.hasToShowOnlineCheckin(booking, onlineCheckinUrl,
        LocalizablesFacade.NOT_FOUND_TEXT)) {
      tracker.trackAnalyticsEvent(CATEGORY_MY_TRIPS_TRIP_INFO, ACTION_ON_LOAD_EVENTS,
          LABEL_AUTO_CHECKIN_SHOWN);
      mLlContainerCards.addView(mOnlineCheckinProvider.getOnlineCheckin(mLlContainerCards));
    }
    mLlContainerCards.addView(mNextFlightProvider.getNextFlight(mLlContainerCards));
    mLlContainerCards.addView(mDetailsOfYourTripProvider.getDetailsOfYourTrips(mLlContainerCards));

    showXSellView();

    setupHeader();
  }

  private void setupHeader() {
    setupToolbar();
    setupBookingImage();
    final String arrivalCityName = BookingUtils.getArrivalCityName(booking, DEFAULT_CITY_DELIMITER);
    final TextView bookingDestinationTitleTV = findById(this, R.id.booking_destination_title);
    bookingDestinationTitleTV.setText(arrivalCityName);
    final TextView bookingTripToLabelTV = findById(this, R.id.booking_trip_to_label);
    bookingTripToLabelTV.setText(localizables.getString(MY_TRIPS_LIST_CARD_TITLE_TRIP));
    final AppBarLayout appBarLayout = findById(this, R.id.app_bar_layout);
    final CollapsingToolbarLayout collapsingToolbarLayout = findById(this, R.id.collapsing_toolbar);
    collapsingToolbarLayout.setTitle(" ");
    final View outboundDateContainer = findById(this, R.id.booking_outbound_date_container);
    final View inboundDateContainer = findById(this, R.id.booking_inbound_date_container);

    appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
      boolean isShown = true;
      float scrollRange = -1f;

      @Override public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (scrollRange == -1f) {
          scrollRange = appBarLayout.getTotalScrollRange();
        }
        if (scrollRange + verticalOffset == 0) {
          collapsingToolbarLayout.setTitle(arrivalCityName);
          isShown = true;
        } else {
          if (isShown) {
            collapsingToolbarLayout.setTitle(" ");
            isShown = false;
          }
        }

        float offsetMultiplier = (scrollRange + verticalOffset) / scrollRange;
        final float alphaMultiplier = verticalOffset == 0 ? 1f : Math.abs(offsetMultiplier);
        ViewCompat.setAlpha(bookingTripToLabelTV, alphaMultiplier);
        ViewCompat.setAlpha(outboundDateContainer, alphaMultiplier);
        ViewCompat.setAlpha(inboundDateContainer, alphaMultiplier);
        ViewCompat.setAlpha(bookingDestinationTitleTV, alphaMultiplier);
      }
    });

    presenter.onCheckIfUserIsLoggedIn(booking);

    String formattedOutboundDate =
        BookingUtils.formatSegmentDate(booking.getFirstSegment(), dateHelper, localizables);
    TextView outBoundDate = findById(this, R.id.booking_outbound_date);
    outBoundDate.setText(HtmlUtils.formatHtml(formattedOutboundDate));
    if (booking.isOneWay()) {
      inboundDateContainer.setVisibility(GONE);
    } else {
      String formattedInboundDate =
          BookingUtils.formatSegmentDate(booking.getLastSegment(), dateHelper, localizables);
      final TextView inboundDateTV = findById(this, R.id.booking_inbound_date);
      inboundDateTV.setText(HtmlUtils.formatHtml(formattedInboundDate));
    }

    setupBookingStatusViews(false);
  }

  private void setupBookingImage() {
    ImageView ivImgCity = findById(this, R.id.booking_image);
    if (booking.isPastBooking()) {
      ColorMatrix matrix = new ColorMatrix();
      matrix.setSaturation(0);
      ivImgCity.setColorFilter(new ColorMatrixColorFilter(matrix));
    }
    myTripsImageUtil.loadBookingImage(this, imageLoader, ivImgCity, booking, false);
  }

  private void setupToolbar() {
    Toolbar toolbar = findById(this, R.id.toolbar);
    setSupportActionBar(toolbar);
    final ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(true);
    }
  }

  private void showXSellView() {
    if (presenter.onCheckUpcomingBooking(booking)) {
      mCrossSellingWidget =
          new CrossSellingWidget(this, booking, dependencyInjector.provideCrossCarUrlHandler(),
              dependencyInjector.provideMarketProvider(),
              dependencyInjector.provideTrackerController(), dependencyInjector.provideDateHelper(),
              dependencyInjector.provideABTestHelper(), this);
      mLlContainerCards.addView(mCrossSellingWidget.getCrossSelling());
    }
  }

  private MyTripDetailsPresenter getPresenter() {
    return dependencyInjector.provideMyTripDetailsPresenter(this, this);
  }

  @Override public void onStart() {
    super.onStart();
    onStartNavigator();
  }

  @Override public void onResume() {
    super.onResume();
    tracker.trackAnalyticsScreen(SCREEN_MY_TRIPS_DETAILS);
  }

  @Override public void onStop() {
    super.onStop();
    onStopNavigator();
  }

  @Override public void onBackPressed() {
    super.onBackPressed();

    final TextView bookingDestinationTitleTV = findById(this, R.id.booking_destination_title);
    final TextView bookingTripToLabelTV = findById(this, R.id.booking_trip_to_label);
    final View outboundDateContainer = findById(this, R.id.booking_outbound_date_container);
    final View inboundDateContainer = findById(this, R.id.booking_inbound_date_container);
    ViewCompat.animate(bookingTripToLabelTV).alpha(1f).start();
    ViewCompat.animate(outboundDateContainer).alpha(1f).start();
    ViewCompat.animate(inboundDateContainer).alpha(1f).start();
    ViewCompat.animate(bookingDestinationTitleTV).alpha(1f).start();

    supportFinishAfterTransition();
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_my_booking, menu);

    MenuItem calendarMenuItem = menu.findItem(R.id.menu_add_to_calendar);
    calendarMenuItem.setTitle(localizables.getString(ADD_TO_CALENDAR));
    calendarMenuItem.setIcon(
        DrawableUtils.getTintedDrawable(this, calendarMenuItem.getIcon(), android.R.color.white));
    return super.onCreateOptionsMenu(menu);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    final int id = item.getItemId();
    if (id == android.R.id.home) {
      tracker.trackAnalyticsEvent(CATEGORY_MY_TRIPS_TRIP_INFO, ACTION_NAVIGATION_ELEMENTS,
          LABEL_GO_BACK);
      supportFinishAfterTransition();
    }
    if (id == R.id.menu_add_to_calendar) {
      presenter.onAddToCalendar(booking);
    }

    return super.onOptionsItemSelected(item);
  }

  @Override public void navigateToBookingGuideViewer(Booking booking) {
    Intent intent = new Intent(this, BookingGuideViewerNavigator.class);
    intent.putExtra(BookingGuideViewerNavigator.EXTRA_BOOKING, booking);
    startActivity(intent);
  }

  @Override public void addToCalendar(Booking booking) {
    if (PermissionsHelper.askForPermissionIfNeeded(Manifest.permission.WRITE_CALENDAR, this,
        OneCMSKeys.PERMISSION_CALENDAR_BOOKING_MESSAGE, PermissionsHelper.CALENDAR_REQUEST_CODE)) {
      mCalendarHelper.addBookingToCalendar(booking);
    }
  }

  @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {

    switch (requestCode) {

      case PermissionsHelper.CALENDAR_REQUEST_CODE:
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          tracker.trackAnalyticsEvent(CATEGORY_APP_PERMISSIONS, ACTION_PERMISSIONS_ALERT,
              LABEL_CALENDAR_ACCESS_ALLOW);
          addToCalendar(booking);
        } else {
          tracker.trackAnalyticsEvent(CATEGORY_APP_PERMISSIONS, ACTION_PERMISSIONS_ALERT,
              LABEL_CALENDAR_ACCESS_DENY);
        }
        break;

      case PermissionsHelper.EXTERNAL_STORAGE_REQUEST_CODE:
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          tracker.trackAnalyticsEvent(CATEGORY_APP_PERMISSIONS, ACTION_PERMISSIONS_ALERT,
              LABEL_STORAGE_ACCESS_ALLOW);
          mCrossSellingWidget.onStoragePermissionAccepted();
        } else {
          tracker.trackAnalyticsEvent(CATEGORY_APP_PERMISSIONS, ACTION_PERMISSIONS_ALERT,
              LABEL_STORAGE_ACCESS_DENY);
        }
        break;

      case PermissionsHelper.EXTERNAL_STORAGE_REQUEST_CODE_FOR_DELETING:
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          tracker.trackAnalyticsEvent(CATEGORY_APP_PERMISSIONS, ACTION_PERMISSIONS_ALERT,
              LABEL_STORAGE_ACCESS_ALLOW);
          mCrossSellingWidget.deleteGuide();
        } else {
          tracker.trackAnalyticsEvent(CATEGORY_APP_PERMISSIONS, ACTION_PERMISSIONS_ALERT,
              LABEL_STORAGE_ACCESS_DENY);
        }
        break;
    }

    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }

  @Override public void exitFromNavigator() {
    String errorMessage =
        LocalizablesFacade.getString(this, OneCMSKeys.SSO_CHANGEPASSWORD_ERROR).toString();
    Toast.makeText(MyTripDetailsNavigator.this, errorMessage, Toast.LENGTH_SHORT).show();
    this.finish();
  }

  @Override public void navigateToGroundTransportation(String url) {
    final OdigeoApp odigeoApp = (OdigeoApp) getApplicationContext();
    Intent intent = new Intent(this, odigeoApp.getWebViewActivityClass());
    intent.putExtra(Constants.EXTRA_URL_WEBVIEW, url);
    intent.putExtra(Constants.TITLE_WEB_ACTIVITY,
        LocalizablesFacade.getString(this, OneCMSKeys.WEBVIEWCONTROLLER_GROUND_TRANSPORTATION_TITLE)
            .toString());
    intent.putExtra(Constants.SHOW_HOME_ICON, true);
    startActivity(intent);
  }

  private void onStartNavigator() {
    try {

      if (presenter.onCheckHasArrivalGuide(booking)) {
        if (mCrossSellingWidget != null) {
          mCrossSellingWidget.onAttachView();
        }
      }
    } catch (NullPointerException e) {
      dependencyInjector.provideCrashlyticsController()
          .trackNonFatal(BookingException.newInstance(booking, e));
      presenter.finishDetailsView();
    }
  }

  private void onStopNavigator() {
    if (mCrossSellingWidget != null) {
      mCrossSellingWidget.onDetachView();
    }
  }

  @Override public boolean isActive() {
    return ViewCompat.isAttachedToWindow(mLlContainerCards);
  }

  private void setupBookingStatusViews(boolean shouldUpdateStatus) {
    final View bookingStatusContainer = findById(this, R.id.booking_status_container);
    if (booking.isPastBooking()) {
      bookingStatusContainer.setVisibility(GONE);
      return;
    }

    @ColorRes final int statusColorResId;
    @DrawableRes int statusIconResId;
    final String bookingStatusKey;
    if (booking.isConfirmed()) {
      statusColorResId = R.color.dark_sea_foam;
      statusIconResId = R.drawable.ic_checked;
      bookingStatusKey = MY_TRIPS_LIST_STATUS_CONFIRM;
    } else if (booking.isCancelled()) {
      statusColorResId = R.color.booking_status_cancelled;
      statusIconResId = R.drawable.ic_canceled;
      bookingStatusKey = MY_TRIPS_LIST_STATUS_CANCEL;
    } else {
      statusColorResId = R.color.booking_status_pending;
      statusIconResId = R.drawable.ic_pending;
      bookingStatusKey = MY_TRIPS_LIST_STATUS_PENDING;
    }

    final View bookingStatusRevealView = findById(this, R.id.booking_status_reveal_view);
    final View bookingStatusView = findById(this, R.id.booking_status_color_view);
    if (shouldUpdateStatus && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      final ColorDrawable backgroundColor = (ColorDrawable) bookingStatusRevealView.getBackground();
      final int revealCenterX = bookingStatusRevealView.getWidth() / 2;
      final int revealCenterY = bookingStatusRevealView.getHeight() / 2;
      final float revealRadius = (float) Math.hypot(revealCenterX, revealCenterY);
      Animator animator =
          ViewAnimationUtils.createCircularReveal(bookingStatusRevealView, revealCenterX,
              revealCenterY, 0, revealRadius);
      animator.addListener(new AnimatorListenerAdapter() {
        @Override public void onAnimationStart(Animator animation) {
          bookingStatusRevealView.setBackgroundColor(backgroundColor.getColor());
          bookingStatusRevealView.setBackgroundColor(
              ContextCompat.getColor(getApplicationContext(), statusColorResId));
        }
      });
      bookingStatusRevealView.setBackgroundColor(backgroundColor.getColor());
      bookingStatusView.setBackgroundColor(backgroundColor.getColor());
      animator.start();
    } else {
      bookingStatusRevealView.setBackgroundColor(ContextCompat.getColor(this, statusColorResId));
    }
    ImageView bookingStatusIconIV = findById(this, R.id.booking_status_icon);
    bookingStatusIconIV.setImageDrawable(ContextCompat.getDrawable(this, statusIconResId));

    final TextView bookingStatusLabelTV = findById(this, R.id.booking_status_label);
    bookingStatusLabelTV.setText(
        WordUtils.capitalize(localizables.getString(bookingStatusKey).toLowerCase()));

    TextView bookingStatusDescriptionTV = findById(this, R.id.booking_status_description);
    final TextView bookingIdLabelTV = findById(this, R.id.booking_id_label);
    TextView bookingStatusPendingMessageTV = findById(this, R.id.booking_status_pending_message);

    if (booking.isPending()) {
      bookingIdLabelTV.setVisibility(GONE);
      bookingStatusDescriptionTV.setVisibility(GONE);
      bookingStatusPendingMessageTV.setText(
          localizables.getString(MY_TRIPS_BOOKING_STATUS_PENDING_MESSAGE));
      bookingStatusPendingMessageTV.setVisibility(VISIBLE);
    } else {
      bookingStatusDescriptionTV.setText(marketProvider.getBrandVisualName());
      bookingIdLabelTV.setText(String.format(BOOKING_ID_FORMAT, booking.getBookingId()));
      bookingStatusDescriptionTV.setVisibility(VISIBLE);
      bookingIdLabelTV.setVisibility(VISIBLE);
      bookingStatusPendingMessageTV.setVisibility(GONE);
    }

    if (getIntent().getExtras() != null) {
      final float statusStartTextSize =
          getIntent().getExtras().getFloat(EXTRA_STATUS_LABEL_FONT_SIZE);
      final CharSequence statusStartText = bookingStatusLabelTV.getText().toString().toUpperCase();
      setEnterSharedElementCallback(new SharedElementCallback() {
        private float endTextSize = -1;
        private CharSequence endText = null;

        @Override
        public void onSharedElementStart(List<String> sharedElementNames, List<View> sharedElements,
            List<View> sharedElementSnapshots) {
          endTextSize = bookingStatusLabelTV.getTextSize();
          endText = bookingStatusLabelTV.getText();
          bookingStatusLabelTV.setTextSize(TypedValue.COMPLEX_UNIT_PX, statusStartTextSize);
          bookingStatusLabelTV.setText(statusStartText);
        }

        @Override
        public void onSharedElementEnd(List<String> sharedElementNames, List<View> sharedElements,
            List<View> sharedElementSnapshots) {
          if (endTextSize >= 0) {
            bookingStatusLabelTV.setTextSize(TypedValue.COMPLEX_UNIT_PX, endTextSize);
            bookingStatusLabelTV.setText(endText);
          }
        }
      });
    }
  }

  @Override public void setupRefreshBooking() {
    swipeRefreshLayout.setColorSchemeResources(R.color.primary_brand, R.color.primary_brand_dark);
    final AppBarLayout appBarLayout = findById(this, R.id.app_bar_layout);
    appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
      @Override public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        swipeRefreshLayout.setEnabled(swipeRefreshLayout.isRefreshing() || verticalOffset == 0);
      }
    });
    swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override public void onRefresh() {
        presenter.onRefreshBookingStatus(booking);
      }
    });
  }

  @Override public void hideRefreshing() {
    swipeRefreshLayout.setRefreshing(false);
  }

  @Override public void disableRefreshBooking() {
    swipeRefreshLayout.setEnabled(false);
  }

  @Override public void updateBookingStatus(Booking updatedBooking) {
    booking = updatedBooking;
    setupBookingStatusViews(true);
  }
}
