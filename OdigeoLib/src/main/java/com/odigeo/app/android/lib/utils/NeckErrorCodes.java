package com.odigeo.app.android.lib.utils;

/**
 * Static class to save all the possible neck error codes used in this project
 *
 * @author M.Sc. Javier Silva Perez
 * @since 21/04/15.
 */
public final class NeckErrorCodes {

  public static final int EMPTY_RESPONSE_ERROR_CODE = 204;

  public static final int EMPTY_RESPONSE_ERROR_STATUS = 1;

  public static final String EMPTY_RESPONSE_ERROR_MESSAGE = "Http Empty response";

  private NeckErrorCodes() {

  }
}
