package com.odigeo.app.android.utils;

import java.util.HashSet;
import java.util.Set;

import static com.odigeo.app.android.lib.consts.Constants.BRAND_EDREAMS;
import static com.odigeo.app.android.lib.consts.Constants.BRAND_GOVOYAGES;
import static com.odigeo.app.android.lib.consts.Constants.BRAND_OPODO;
import static com.odigeo.app.android.lib.consts.Constants.BRAND_TRAVELLINK;

public final class BrandUtils {

  public final static String MARKET_KEY_DE = "de";
  public final static String MARKET_KEY_COM = "com";
  public final static String MARKET_KEY_UK = "gb";
  public final static String MARKET_KEY_FR = "fr";
  public final static String MARKET_KEY_IT = "it";
  public final static String MARKET_KEY_SE = "se";
  public final static String MARKET_KEY_DK = "dk";
  public final static String MARKET_KEY_NO = "no";
  public final static String MARKET_KEY_FI = "fi";
  public final static String MARKET_KEY_PL = "pl";
  public final static String MARKET_KEY_IS = "is";

  public final static String LOCALE_KEY_SE = "sv_SE";
  public final static String LOCALE_KEY_DK = "da_DK";
  public final static String LOCALE_KEY_NO = "no_NO";
  public final static String LOCALE_KEY_FI = "fi_FI";
  public final static String LOCALE_KEY_PL = "pl_PL";
  public final static String LOCALE_KEY_IS = "is_IS";
  public final static String LOCALE_KEY_ES = "es_ES";

  private static final Set<String> NORDIC_MARKETS = new HashSet<String>() {{
    add(MARKET_KEY_SE);
    add(MARKET_KEY_DK);
    add(MARKET_KEY_NO);
    add(MARKET_KEY_FI);
    add(MARKET_KEY_PL);
    add(MARKET_KEY_IS);
  }};

  private static final Set<String> NORDIC_LOCALES = new HashSet<String>() {{
    add(LOCALE_KEY_SE);
    add(LOCALE_KEY_DK);
    add(LOCALE_KEY_NO);
    add(LOCALE_KEY_FI);
    add(LOCALE_KEY_PL);
    add(LOCALE_KEY_IS);
  }};

  public static boolean isNordicCountryByMarket(String marketKey) {
    return NORDIC_MARKETS.contains(marketKey);
  }

  public static boolean isNordicCountryByLocale(String locale) {
    return NORDIC_LOCALES.contains(locale);
  }

  public static boolean isEdreamsBrand(String brand) {
    return brand.equalsIgnoreCase(BRAND_EDREAMS);
  }

  public static boolean isOpodoBrand(String brand) {
    return brand.equalsIgnoreCase(BRAND_OPODO);
  }

  public static boolean isOpodoNordicsBrand(String brand, String marketKey) {
    return isOpodoBrand(brand) && isNordicCountryByMarket(marketKey);
  }

  public static boolean isGoVoyagesBrand(String brand) {
    return brand.equalsIgnoreCase(BRAND_GOVOYAGES);
  }

  public static boolean isTravellinkBrand(String brand) {
    return brand.equalsIgnoreCase(BRAND_TRAVELLINK);
  }

  public static boolean isUkMarket(String market) {
    return market.equalsIgnoreCase(MARKET_KEY_UK);
  }

  public static boolean isNordicMarket(String brand, String marketKey) {
    return isTravellinkBrand(brand) || isOpodoNordicsBrand(brand, marketKey);
  }
}
