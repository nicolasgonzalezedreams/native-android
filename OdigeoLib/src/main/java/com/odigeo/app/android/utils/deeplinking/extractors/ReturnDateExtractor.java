package com.odigeo.app.android.utils.deeplinking.extractors;

import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.view.helpers.DateHelper;
import com.odigeo.data.entity.TravelType;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 25/04/16
 */
public class ReturnDateExtractor implements DeepLinkingExtractor {

  @Override public void extractParameter(SearchOptions.Builder builder, String key, String value) {
    DateHelper dateHelper = new DateHelper();
    long time = dateHelper.dateToMiliseconds(value, DateHelper.yyyy_MM_dd_format);
    if (builder.getTravelType() == TravelType.ROUND) {
      builder.addDepartureDate(1, time);
      FlightSegment departureSegment = builder.getFlightSegments().get(0);
      builder.addDeparture(1, departureSegment.getArrivalCity().getIataCode());
      builder.addArrival(1, departureSegment.getDepartureCity().getIataCode());
    }
  }
}