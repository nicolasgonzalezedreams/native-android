package com.odigeo.app.android.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.PdfDownloader;
import com.odigeo.app.android.navigator.RegisterNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DialogHelper;
import com.odigeo.app.android.view.helpers.PermissionsHelper;
import com.odigeo.app.android.view.interfaces.AuthDialogActionInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.RegisterViewPresenter;
import com.odigeo.presenter.contracts.views.RegisterViewInterface;

import static android.Manifest.permission.GET_ACCOUNTS;
import static com.odigeo.app.android.view.helpers.PermissionsHelper.ACCOUNTS_FACEBOOK_REQUEST_CODE;
import static com.odigeo.app.android.view.helpers.PermissionsHelper.ACCOUNTS_GOOGLE_REQUEST_CODE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_REGISTER_FORM;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_SSO_REGISTER;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.FACEBOOK_LOGIN_TAP_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.GOOGLE_PLUS_LOGIN_TAP_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_CREATE_ACCOUNT_FACEBOOK_DATALOCAL;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_CREATE_ACCOUNT_FACEBOOK_DATANO;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_CREATE_ACCOUNT_GOOGLE_DATALOCAL;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_CREATE_ACCOUNT_GOOGLE_DATANO;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_CREATE_ACCOUNT_ODIGEO_DATALOCAL;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_CREATE_ACCOUNT_ODIGEO_DATANO;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_LINKS_OPEN_CONDITIONS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_PASSWORD_UNMASK;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.REGISTER_TAP_EVENT;

public class RegisterView extends SocialLoginHelperView implements RegisterViewInterface {

  public static final String URL = "URL";
  String mLegalClauseText;
  String mTermsConditions;
  String mPrivacyPolicy;
  private Button mBtnSignUp;
  private Button mBtnFacebookSignUp;
  private Button mBtnGoogleSignUp;
  private EditText mEtEmail;
  private EditText mEtPassword;
  private AppCompatCheckBox mCbShowPassword;
  private TextView mTvPasswordMin;
  private TextView mTvReceiveConfirmation;
  private TextView tvConditions;
  private AlphaAnimation fadeIn;
  private AlphaAnimation fadeOut;
  private Boolean mIsMailFormatCorrect = false;
  TextWatcher onTextChanged = new TextWatcher() {
    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
      ((RegisterViewPresenter) mPresenter).validateUsernameAndPasswordFormat(
          mEtEmail.getText().toString(), mEtPassword.getText().toString());
      mIsMailFormatCorrect = mPresenter.validateUsernameFormat(mEtEmail.getText().toString());
    }

    @Override public void afterTextChanged(Editable s) {

    }
  };
  private AppCompatCheckBox mCbSubscribeNewsletter;
  private TextView mTvSubscribeNewsletter;
  private String mTxtMailConfirmation;
  private String mTxtWrongMailFormat;

  public RegisterView() {
  }

  public static RegisterView newInstance() {
    return new RegisterView();
  }

  @Override protected RegisterViewPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideRegisterPresenter(this, (RegisterNavigator) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_register_view;
  }

  @Override protected void initComponent(View view) {
    mBtnSignUp = (Button) view.findViewById(R.id.btnSignUp);
    mBtnFacebookSignUp = (Button) view.findViewById(R.id.btnFacebookSignUp);
    mBtnGoogleSignUp = (Button) view.findViewById(R.id.btnGoogleSignUp);
    mEtEmail = (EditText) view.findViewById(R.id.etEmail);
    mEtPassword = (EditText) view.findViewById(R.id.etPassword);
    mCbShowPassword = (AppCompatCheckBox) view.findViewById(R.id.cbShowPassword);
    mTvPasswordMin = (TextView) view.findViewById(R.id.tvPasswordMin);
    mTvReceiveConfirmation = (TextView) view.findViewById(R.id.tvReceiveConfirmation);
    tvConditions = (TextView) view.findViewById(R.id.tvConditions);
    mCbSubscribeNewsletter = (AppCompatCheckBox) view.findViewById(R.id.cbSubscribeNewsletter);
    mTvSubscribeNewsletter = (TextView) view.findViewById(R.id.tvSubscribeNewsletter);
    dialogHelper = new DialogHelper(getActivity());
    enableSignUpButton(false);
  }

  @Override protected void setListeners() {
    mBtnSignUp.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        showProgress();
        String email = mEtEmail.getText().toString();
        String password = mEtPassword.getText().toString();
        Boolean hasRegisterToNewsletter;
        if (mCbSubscribeNewsletter.isChecked()) {
          hasRegisterToNewsletter = true;
          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_SSO_REGISTER,
              TrackerConstants.ACTION_REGISTER_FORM,
              TrackerConstants.LABEL_SSO_REGISTER_NEWSLETTER);
        } else {
          hasRegisterToNewsletter = null;
          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_SSO_REGISTER,
              TrackerConstants.ACTION_REGISTER_FORM,
              TrackerConstants.LABEL_SSO_UNREGISTER_NEWSLETTER);
        }
        ((RegisterViewPresenter) mPresenter).signUpPassword(email, password,
            hasRegisterToNewsletter);
        if (mPresenter.hasLocalData()) {
          mTracker.trackRegisterPage(CATEGORY_SSO_REGISTER, ACTION_REGISTER_FORM,
              LABEL_CREATE_ACCOUNT_ODIGEO_DATALOCAL, REGISTER_TAP_EVENT, email.isEmpty(),
              password.isEmpty(), true, false, false);
        } else {
          mTracker.trackRegisterPage(CATEGORY_SSO_REGISTER, ACTION_REGISTER_FORM,
              LABEL_CREATE_ACCOUNT_ODIGEO_DATANO, REGISTER_TAP_EVENT, email.isEmpty(),
              password.isEmpty(), true, false, false);
        }
      }
    });

    mBtnFacebookSignUp.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          if (PermissionsHelper.askForPermissionIfNeeded(GET_ACCOUNTS, getActivity(),
              OneCMSKeys.PERMISSION_ACCOUNT_REGISTER_MESSAGE, ACCOUNTS_FACEBOOK_REQUEST_CODE)) {
            facebookRegister();
          }
        } else {
          facebookRegister();
        }
      }
    });

    mBtnGoogleSignUp.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          if (PermissionsHelper.askForPermissionIfNeeded(GET_ACCOUNTS, getActivity(),
              OneCMSKeys.PERMISSION_ACCOUNT_REGISTER_MESSAGE, ACCOUNTS_GOOGLE_REQUEST_CODE)) {
            googlePlusRegister();
          }
        } else {
          googlePlusRegister();
        }
      }
    });

    mCbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
          mEtPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        } else {
          mEtPassword.setInputType(
              InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
          mTracker.trackAnalyticsEvent(CATEGORY_SSO_REGISTER, ACTION_REGISTER_FORM,
              LABEL_PASSWORD_UNMASK);
        }
      }
    });

    mEtEmail.addTextChangedListener(onTextChanged);

    mEtPassword.addTextChangedListener(onTextChanged);

    prepareFadeAnimations();

    mEtPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override public void onFocusChange(View view, boolean hasFocus) {
        if (hasFocus) {
          mTvPasswordMin.startAnimation(fadeIn);
        } else {
          mTvPasswordMin.startAnimation(fadeOut);
        }
      }
    });

    mEtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override public void onFocusChange(View view, boolean hasFocus) {
        if (hasFocus) {
          mTvReceiveConfirmation.setText(mTxtMailConfirmation);
          mTvReceiveConfirmation.setTextColor(getResources().getColor(R.color.com_facebook_blue));
          mTvReceiveConfirmation.startAnimation(fadeIn);
        } else {
          mTvReceiveConfirmation.startAnimation(fadeOut);
          if (!mIsMailFormatCorrect) {
            mTvReceiveConfirmation.setText(mTxtWrongMailFormat);
            mTvReceiveConfirmation.setTextColor(
                getResources().getColor(R.color.semantic_negative_blocker));
            mTvReceiveConfirmation.startAnimation(fadeIn);
          }
        }
      }
    });

    //Set two links in tvConditions to open either Privacy policy or Terms and conditions
    String termsAndConditionsURL = LocalizablesFacade.getString(getContext(),
        OneCMSKeys.ABOUTOPTIONSMODULE_ABOUT_OPTION_TERMS_URL).toString();
    String privacyPolicyURL =
        LocalizablesFacade.getString(getContext(), OneCMSKeys.PRIVACY_POLICY_URL).toString();

    SpannableStringBuilder spanTxt = new SpannableStringBuilder(mLegalClauseText);
    setSpanTextLink(spanTxt, mLegalClauseText, mTermsConditions, termsAndConditionsURL);
    setSpanTextLink(spanTxt, mLegalClauseText, mPrivacyPolicy, privacyPolicyURL);

    tvConditions.setMovementMethod(LinkMovementMethod.getInstance());
    tvConditions.setText(spanTxt, TextView.BufferType.SPANNABLE);
  }

  @Override protected void initOneCMSText(View view) {
    TextInputLayout lmEtEmail = (TextInputLayout) view.findViewById(R.id.etEmailLayout);
    lmEtEmail.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_FORM_SHADOWTEXT_EMAIL));
    TextInputLayout lmEtPassword = (TextInputLayout) view.findViewById(R.id.etPasswordLayout);
    lmEtPassword.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_FORM_SHADOWTEXT_PASSWORD));

    TextView tvOr = ((TextView) view.findViewById(R.id.tvOr));
    tvOr.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.COMMON_OR));

    mBtnGoogleSignUp.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_REGISTERINFO_SIGNIN_GOOGLE));
    mBtnFacebookSignUp.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_REGISTERINFO_SIGNIN_FACEBOOK));
    mBtnSignUp.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_CREATE_ACCOUNT));
    mTxtMailConfirmation =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_REGISTER_CONFIRMATION_EMAIL)
            .toString();
    mTxtWrongMailFormat =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_WRONG_EMAIL_FORMAT).toString();

    mTvPasswordMin.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_FORM_ERROR_PASSWORD_FORMAT));

    mCbShowPassword.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_SHOW_PASSWORD));

    mBtnSignUp.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.SSO_CREATE_ACCOUNT).toString());

    mLegalClauseText = LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_REGISTER_LEGAL)
        .toString()
        .replace("<b>", "")
        .replace("</b>", "");
    mTermsConditions =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_TERMSANDCONDITIONS).toString();
    mPrivacyPolicy = LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_PRIVACY).toString();
    mLegalClauseText = String.format(mLegalClauseText, mTermsConditions, mPrivacyPolicy);
    mTvSubscribeNewsletter.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_REGISTER_NEWSLETTER));
  }

  @Override public void onResume() {
    super.onResume();
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_REGISTER);
  }

  private void setSpanTextLink(SpannableStringBuilder spanTxt, String source, String textToSpan,
      final String url) {
    int start = source.indexOf(textToSpan);

        /*assure that the OneCMS key was found and returns a valid text*/
    if (start != -1) {
      spanTxt.setSpan(new ClickableSpan() {
        @Override public void onClick(View widget) {
          mTracker.trackAnalyticsEvent(CATEGORY_SSO_REGISTER, ACTION_REGISTER_FORM,
              LABEL_LINKS_OPEN_CONDITIONS);
          executeURL(url);
        }
      }, start, start + textToSpan.length(), 0);
      spanTxt.setSpan(
          new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.basic_light)),
          start, start + textToSpan.length(), 0);
    }
  }

  public void googlePlusRegister() {
    showProgress();
    loginGoogle();
    String email = mEtEmail.getText().toString();
    String password = mEtPassword.getText().toString();
    if (mPresenter.hasLocalData()) {
      mTracker.trackRegisterPage(CATEGORY_SSO_REGISTER, ACTION_REGISTER_FORM,
          LABEL_CREATE_ACCOUNT_GOOGLE_DATALOCAL, GOOGLE_PLUS_LOGIN_TAP_EVENT, email.isEmpty(),
          password.isEmpty(), false, false, true);
    } else {
      mTracker.trackRegisterPage(CATEGORY_SSO_REGISTER, ACTION_REGISTER_FORM,
          LABEL_CREATE_ACCOUNT_GOOGLE_DATANO, GOOGLE_PLUS_LOGIN_TAP_EVENT, email.isEmpty(),
          password.isEmpty(), false, false, true);
    }
  }

  public void facebookRegister() {
    showProgress();
    loginFacebook();
    String email = mEtEmail.getText().toString();
    String password = mEtPassword.getText().toString();
    if (mPresenter.hasLocalData()) {
      mTracker.trackRegisterPage(CATEGORY_SSO_REGISTER, ACTION_REGISTER_FORM,
          LABEL_CREATE_ACCOUNT_FACEBOOK_DATALOCAL, FACEBOOK_LOGIN_TAP_EVENT, email.isEmpty(),
          password.isEmpty(), false, true, false);
    } else {
      mTracker.trackRegisterPage(CATEGORY_SSO_REGISTER, ACTION_REGISTER_FORM,
          LABEL_CREATE_ACCOUNT_FACEBOOK_DATANO, FACEBOOK_LOGIN_TAP_EVENT, email.isEmpty(),
          password.isEmpty(), false, true, false);
    }
  }

  private void executeURL(String url) {
    if (url != null && url.length() > 0) {
      if (HtmlUtils.checkUrlIsPdf(url)) {
        PdfDownloader task = new PdfDownloader(getActivity());
        task.execute(url);
      } else {
        launchNewWebView(url);
      }
    }
  }

  private void launchNewWebView(String url) {
    OdigeoApp odigeoApp = (OdigeoApp) getActivity().getApplication();
    Intent intent = new Intent(getActivity(), odigeoApp.getWebViewActivityClass());
    Bundle extras = new Bundle();
    extras.putString(URL, url);
    intent.putExtras(extras);
    getActivity().startActivity(intent);
  }

  private void prepareFadeAnimations() {
    fadeIn = new AlphaAnimation(0.0f, 1.0f);
    fadeOut = new AlphaAnimation(1.0f, 0.0f);
    fadeIn.setDuration(200);
    fadeIn.setFillAfter(true);
    fadeOut.setDuration(200);
    fadeOut.setFillAfter(true);
  }

  @Override public void enableSignUpButton(boolean enabled) {
    mBtnSignUp.setEnabled(enabled);
  }

  @Override public void showRegisterFail() {
    dialogHelper.showErrorDialog(getActivity(),
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ERROR_SERVER).toString());
  }

  @Override public void showUserAlreadyExist() {
    ((RegisterViewPresenter) mPresenter).showLogInUserAlreadyRegistered(
        mEtEmail.getText().toString());
  }

  @Override public void showAuthError() {
    dialogHelper.showAuthErrorDialog(this, new AuthDialogActionInterface() {
      @Override public void OnAuthDialogOK() {
        mPresenter.onAuthOkClicked();
      }
    });
  }

  @Override public void onLogoutError(MslError error, String message) {
    Log.e(error.name(), message);
  }
}
