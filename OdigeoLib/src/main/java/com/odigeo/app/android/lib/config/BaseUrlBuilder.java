package com.odigeo.app.android.lib.config;

import java.util.HashMap;
import java.util.Map;

/**
 * Base class that builds an url to be called via GET.
 *
 * @author cristian.fanjul
 * @since 25/02/2015
 */
public abstract class BaseUrlBuilder {

  protected Map<String, Object> data;
  protected boolean isSearch = false;

  public BaseUrlBuilder(boolean isSearch) {
    this.data = new HashMap<>();
    this.isSearch = isSearch;
  }

  /**
   * The search results URL to call is defined here.
   *
   * @return The results url
   */
  protected abstract String getSearchResultsUrl();

  /**
   * The filter or home URL to call is defined here.
   *
   * @return The filter url
   */
  protected abstract String getFilterUrl();

  /**
   * Determines if the search url has to be used.
   *
   * @return Is a search.
   */
  public final boolean isSearch() {
    return isSearch;
  }

  public final void setSearch(boolean isSearch) {
    this.isSearch = isSearch;
  }

  /**
   * The url with all the parameters set is provided by this method.
   *
   * @return The url.
   */
  public final String getUrl() {
    if (isSearch) {
      return buildUrl(getSearchResultsUrl(), data);
    }

    return buildUrl(getFilterUrl(), data);
  }

  /**
   * Builds an URL from a bundle.
   *
   * @param baseUrl Url base.
   * @param data Parameters.
   * @return The url.
   */
  protected final String buildUrl(String baseUrl, Map<String, Object> data) {
    String prefix = "?";
    if (baseUrl.indexOf('?') != -1) {
      prefix = "&";
    }
    StringBuilder queryString = new StringBuilder("");
    if (data != null) {
      for (String key : data.keySet()) {
        queryString.append(prefix)
            .append(key)
            .append("=")
            .append(data.get(key).toString().replace(" ", "%20"));
        prefix = "&";
      }
    }
    return baseUrl + queryString.toString();
  }
}
