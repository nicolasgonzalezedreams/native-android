package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class GeoCoordinatesDTO implements Serializable {

  protected BigDecimal latitude;
  protected BigDecimal longitude;

  /**
   * Gets the value of the latitude property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getLatitude() {
    return latitude;
  }

  /**
   * Sets the value of the latitude property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setLatitude(BigDecimal value) {
    this.latitude = value;
  }

  /**
   * Gets the value of the longitude property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getLongitude() {
    return longitude;
  }

  /**
   * Sets the value of the longitude property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setLongitude(BigDecimal value) {
    this.longitude = value;
  }
}
