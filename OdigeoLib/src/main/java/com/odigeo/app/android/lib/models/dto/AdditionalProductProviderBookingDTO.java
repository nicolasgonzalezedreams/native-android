package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class AdditionalProductProviderBookingDTO {

  protected Long id;
  protected String refId;
  protected BigDecimal providerPrice;
  protected String currency;
  protected String description;
  protected AdditionalProductTypeDTO additionalProductType;

  /**
   * Gets the value of the id property.
   *
   * @return possible object is
   * {@link Long }
   */
  public Long getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   *
   * @param value allowed object is
   * {@link Long }
   */
  public void setId(Long value) {
    this.id = value;
  }

  /**
   * Gets the value of the refId property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getRefId() {
    return refId;
  }

  /**
   * Sets the value of the refId property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setRefId(String value) {
    this.refId = value;
  }

  /**
   * Gets the value of the providerPrice property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  /**
   * Sets the value of the providerPrice property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setProviderPrice(BigDecimal value) {
    this.providerPrice = value;
  }

  /**
   * Gets the value of the currency property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCurrency() {
    return currency;
  }

  /**
   * Sets the value of the currency property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCurrency(String value) {
    this.currency = value;
  }

  /**
   * Gets the value of the description property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getDescription() {
    return description;
  }

  /**
   * Sets the value of the description property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setDescription(String value) {
    this.description = value;
  }

  /**
   * Gets the value of the additionalProductType property.
   *
   * @return possible object is
   * {@link com.odigeo.app.android.lib.models.dto.AdditionalProductTypeDTO }
   */
  public AdditionalProductTypeDTO getAdditionalProductType() {
    return additionalProductType;
  }

  /**
   * Sets the value of the additionalProductType property.
   *
   * @param value allowed object is
   * {@link com.odigeo.app.android.lib.models.dto.AdditionalProductTypeDTO }
   */
  public void setAdditionalProductType(AdditionalProductTypeDTO value) {
    this.additionalProductType = value;
  }
}
