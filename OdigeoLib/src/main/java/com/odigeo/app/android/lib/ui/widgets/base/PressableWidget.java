package com.odigeo.app.android.lib.ui.widgets.base;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.odigeo.app.android.lib.R;

/**
 * Created by ManuelOrtiz on 18/11/2014.
 */
public class PressableWidget extends LinearLayout {

  public static final int[] ISTHELASTINFORM = {
      R.attr.isTheLastInForm
  };
  public static final int[] ISTHEFIRSTINFORM = {
      R.attr.isTheFirstInForm
  };
  public static final int[] ISINLEFTSIDE = {
      R.attr.isInLeftSide
  };
  public static final int[] ISINLRIGHTSIDE = {
      R.attr.isInRightSide
  };
  private static final int STANDARD_SPACE_FORM = 4;
  private boolean lastInForm;
  private boolean firstInForm;
  private boolean inLeftSide;
  private boolean inRightSide;

  public PressableWidget(Context context) {
    super(context);
  }

  public PressableWidget(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray aAttrs = context.obtainStyledAttributes(attrs, R.styleable.PressableWidget, 0, 0);

    setLastInForm(aAttrs.getBoolean(R.styleable.PressableWidget_isTheLastInForm, false));
    setFirstInForm(aAttrs.getBoolean(R.styleable.PressableWidget_isTheFirstInForm, false));
    setInLeftSide(aAttrs.getBoolean(R.styleable.PressableWidget_isInLeftSide, false));
    setInRightSide(aAttrs.getBoolean(R.styleable.PressableWidget_isInRightSide, false));

    aAttrs.recycle();
  }

  @Override protected int[] onCreateDrawableState(int extraSpace) {

    int[] drawableState = super.onCreateDrawableState(extraSpace + STANDARD_SPACE_FORM);

    if (isFirstInForm()) {
      mergeDrawableStates(drawableState, ISTHEFIRSTINFORM);
    }

    if (isLastInForm()) {
      mergeDrawableStates(drawableState, ISTHELASTINFORM);
    }

    if (isInLeftSide()) {
      mergeDrawableStates(drawableState, ISINLEFTSIDE);
    }

    if (isInRightSide()) {
      mergeDrawableStates(drawableState, ISINLRIGHTSIDE);
    }

    return drawableState;
  }

  public boolean isLastInForm() {
    return lastInForm;
  }

  public final void setLastInForm(boolean lastInForm) {
    this.lastInForm = lastInForm;

    refreshDrawableState();
  }

  public boolean isFirstInForm() {
    return firstInForm;
  }

  public final void setFirstInForm(boolean firstInForm) {
    this.firstInForm = firstInForm;

    refreshDrawableState();
  }

  public boolean isInLeftSide() {
    return inLeftSide;
  }

  public final void setInLeftSide(boolean inLeftSide) {
    this.inLeftSide = inLeftSide;

    refreshDrawableState();
  }

  public boolean isInRightSide() {
    return inRightSide;
  }

  public final void setInRightSide(boolean inRightSide) {
    this.inRightSide = inRightSide;

    refreshDrawableState();
  }
}
