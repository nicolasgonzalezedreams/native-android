package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

/**
 * <p>Java class for requiredField.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;simpleType name="requiredField">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MANDATORY"/>
 *     &lt;enumeration value="OPTIONAL"/>
 *     &lt;enumeration value="NOT_REQUIRED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
public enum RequiredFieldDTO implements Serializable {

  MANDATORY, OPTIONAL, NOT_REQUIRED;

  public static RequiredFieldDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
