package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class CollectionMethodKeyPriceDTO {

  protected Integer collectionMethodKey;
  protected BigDecimal price;

  /**
   * Gets the value of the collectionMethodKey property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getCollectionMethodKey() {
    return collectionMethodKey;
  }

  /**
   * Sets the value of the collectionMethodKey property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setCollectionMethodKey(Integer value) {
    this.collectionMethodKey = value;
  }

  /**
   * Gets the value of the price property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getPrice() {
    return price;
  }

  /**
   * Sets the value of the price property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setPrice(BigDecimal value) {
    this.price = value;
  }
}
