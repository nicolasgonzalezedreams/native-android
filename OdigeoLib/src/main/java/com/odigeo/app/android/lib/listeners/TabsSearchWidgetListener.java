package com.odigeo.app.android.lib.listeners;

import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.fragments.RoundTripFragment;
import com.odigeo.app.android.lib.fragments.SimpleTripFragment;
import com.odigeo.data.tracker.TrackerControllerInterface;

import static com.odigeo.dataodigeo.tracker.TrackerConstants.TAB_MULTI_STOPS_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.TAB_ONE_WAY_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.TAB_ROUND_TRIP_EVENT;

/**
 * @author Irving
 * @since 01/09/2014
 */
public class TabsSearchWidgetListener implements android.support.v7.app.ActionBar.TabListener {

  public android.support.v4.app.Fragment fragment;

  public TabsSearchWidgetListener(android.support.v4.app.Fragment fragment) {
    this.fragment = fragment;
  }

  @Override public void onTabSelected(android.support.v7.app.ActionBar.Tab tab,
      android.support.v4.app.FragmentTransaction fragmentTransaction) {
    TrackerControllerInterface trackerController =
        AndroidDependencyInjector.getInstance().provideTrackerController();
    if (fragment instanceof RoundTripFragment) {
      trackerController.trackAppseeEvent(TAB_ROUND_TRIP_EVENT);
    } else if (fragment instanceof SimpleTripFragment) {
      trackerController.trackAppseeEvent(TAB_ONE_WAY_EVENT);
    } else {
      trackerController.trackAppseeEvent(TAB_MULTI_STOPS_EVENT);
    }
    fragmentTransaction.replace(android.R.id.content, fragment);
  }

  @Override public void onTabUnselected(android.support.v7.app.ActionBar.Tab tab,
      android.support.v4.app.FragmentTransaction fragmentTransaction) {
    fragmentTransaction.remove(fragment);
  }

  @Override public void onTabReselected(android.support.v7.app.ActionBar.Tab tab,
      android.support.v4.app.FragmentTransaction fragmentTransaction) {
    //Nothing to do here
  }
}
