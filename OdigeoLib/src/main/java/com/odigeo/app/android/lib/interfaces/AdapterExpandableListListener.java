package com.odigeo.app.android.lib.interfaces;

import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;
import com.odigeo.app.android.lib.ui.widgets.SegmentGroupWidget;
import java.util.List;

/**
 * Created by Irving Lóp on 13/09/2014.
 */
public interface AdapterExpandableListListener {
  void onSegmentSelected(SegmentGroupWidget segmentGroupWidget, int segmentIndex);

  void onItinerarySelected(FareItineraryDTO itineraryResult, List<SegmentWrapper> segments);

  void onSelectDifferentItinerary(FareItineraryDTO itineraryResult, int itineraryResultIndex);

  void onWidgetSelected(boolean isSelected);
}
