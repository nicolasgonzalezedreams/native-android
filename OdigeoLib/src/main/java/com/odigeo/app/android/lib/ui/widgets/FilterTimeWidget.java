package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.models.FilterTimeModel;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.data.entity.geo.City;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Irving Lóp on 14/10/2014.
 */
public class FilterTimeWidget extends LinearLayout implements PropertyChangeListener {
  private static final int MAX_VALUE = 15;
  private FilterRangeBarDateWidget arrivalDateWidget;
  private FilterRangeBarDateWidget departureDateWidget;
  private FilterSeekBarWidget durationWidget;
  private City departureCity;
  private City arrivalCity;
  private Date dateFlight;
  private int numberWidget;
  private TextView mTvFilterTimeTitle;
  private TextView mTvFilterTimeSubtitle;

  public FilterTimeWidget(Context context) {
    super(context);
    inflateLayout();
  }

  public FilterTimeWidget(Context context, AttributeSet atrrs) {
    super(context, atrrs);
    inflateLayout();
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.layout_filter_time_widget, this);
    departureDateWidget = (FilterRangeBarDateWidget) findViewById(R.id.date_filter_date_depart);
    departureDateWidget.setParentNumberWidget(numberWidget);
    departureDateWidget.setTravelMode(Constants.SEEKERBAR_DEPARTURE);

    arrivalDateWidget = (FilterRangeBarDateWidget) findViewById(R.id.date_filter_date_arrival);
    arrivalDateWidget.setParentNumberWidget(numberWidget);
    arrivalDateWidget.setTravelMode(Constants.SEEKERBAR_ARRIVAL);

    durationWidget = (FilterSeekBarWidget) findViewById(R.id.time_filter_date_hours);
    durationWidget.setParentNumberWidget(numberWidget);

    mTvFilterTimeTitle = (TextView) findViewById(R.id.tvFilterTimeTitle);
    mTvFilterTimeSubtitle = (TextView) findViewById(R.id.tvFilterTimeSubtitle);

    durationWidget.setMaxValue(MAX_VALUE);
    durationWidget.setValueSelected(MAX_VALUE);

    departureDateWidget.addChangeListener(this);
    arrivalDateWidget.addChangeListener(this);
    durationWidget.setPropertyChangeListener(this);
  }

  public final String getSubtitle() {
    SimpleDateFormat dateFormat = OdigeoDateUtils.getGmtDateFormat("EEE, dd MMM yy");

    if (getDateFlight() != null) {
      return String.format("%s - %s", getCodesCities(), dateFormat.format(getDateFlight()));
    }
    return null;
  }

  public final String getCodesCities() {
    if (getDepartureCity() != null && getArrivalCity() != null) {
      return String.format("%s - %s", getDepartureCity().getIataCode(),
          getArrivalCity().getIataCode());
    }
    return null;
  }

  public final void setTitle(String title) {
    mTvFilterTimeTitle.setText(title);
  }

  public final FilterTimeModel getTimeModel() {
    FilterTimeModel timeModel = new FilterTimeModel();
    timeModel.setMaxFlightHoursSelected(durationWidget.getSelectedValue());
    timeModel.setDepartureMinDateSelected(departureDateWidget.getStartDateSelected());
    timeModel.setDepartureMaxDateSelected(departureDateWidget.getEndDateSelected());
    timeModel.setArrivalMinDateSelected(arrivalDateWidget.getStartDateSelected());
    timeModel.setArrivalMaxDateSelected(arrivalDateWidget.getEndDateSelected());
    return timeModel;
  }

  public final void setSelectedTimeModel(FilterTimeModel timeModel) {
    departureDateWidget.setDateStartSelected(timeModel.getDepartureMinDateSelected());
    departureDateWidget.setDateEndSelected(timeModel.getDepartureMaxDateSelected());

    arrivalDateWidget.setDateStartSelected(timeModel.getArrivalMinDateSelected());
    arrivalDateWidget.setDateEndSelected(timeModel.getArrivalMaxDateSelected());

    durationWidget.setValueSelected(timeModel.getMaxFlightHoursSelected());
  }

  public final City getDepartureCity() {
    return departureCity;
  }

  public final void setDepartureCity(City departureCity) {
    this.departureCity = departureCity;

    if (departureCity != null) {
      departureDateWidget.setSubtitleText(departureCity.getCityName());
    }

    durationWidget.setSubTitleText(getCodesCities());
    mTvFilterTimeSubtitle.setText(getSubtitle());
  }

  public final City getArrivalCity() {
    return arrivalCity;
  }

  public final void setArrivalCity(City arrivalCity) {
    this.arrivalCity = arrivalCity;

    if (arrivalCity != null) {
      arrivalDateWidget.setSubtitleText(arrivalCity.getCityName());
    }

    durationWidget.setSubTitleText(getCodesCities());
    mTvFilterTimeSubtitle.setText(getSubtitle());
  }

  public final void setDepartureMinDate(Date departureMinDate) {
    this.departureDateWidget.setDateMin(departureMinDate);
  }

  public final void setArrivalMinDate(Date arrivalMinDate) {
    this.arrivalDateWidget.setDateMin(arrivalMinDate);
  }

  public final void setDepartureMaxDate(Date departureMaxDate) {
    this.departureDateWidget.setDateMax(departureMaxDate);
  }

  public final void setArrivalMaxDate(Date arrivalMaxDate) {
    this.arrivalDateWidget.setDateMax(arrivalMaxDate);
  }

  public final void setMaxFlightHours(int maxFlightHours) {

    durationWidget.setMaxValue(maxFlightHours);
    durationWidget.setValueSelected(maxFlightHours);
  }

  public final Date getDateFlight() {
    return dateFlight;
  }

  public final void setDateFlight(Date dateFlight) {
    this.dateFlight = dateFlight;

    mTvFilterTimeSubtitle.setText(getSubtitle());
  }

  public final int getNumberWidget() {
    return numberWidget;
  }

  public final void setNumberWidget(int numberWidget) {
    this.numberWidget = numberWidget;
  }

  @Override public final void propertyChange(PropertyChangeEvent event) {
    if (event.getPropertyName().equals(Constants.SEEKERBAR_DEPARTURE)) {
      // GATracker - Filters - stops page
      BusProvider.getInstance()
          .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_FILTERS,
              GAnalyticsNames.ACTION_RESULTS_FILTERS,
              GAnalyticsNames.LABEL_PARTIAL_TIMES_DEPARTURE + event.getNewValue()
                  .toString()
                  .replace(" ", "_") + GAnalyticsNames.LABEL_PARTIAL_SEGMENT + getNumberWidget()));
    } else if (event.getPropertyName().equals(Constants.SEEKERBAR_ARRIVAL)) {
      // GATracker - Filters - stops page
      BusProvider.getInstance()
          .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_FILTERS,
              GAnalyticsNames.ACTION_RESULTS_FILTERS,
              GAnalyticsNames.LABEL_PARTIAL_TIMES_ARRIVAL + event.getNewValue()
                  .toString()
                  .replace(" ", "_") + GAnalyticsNames.LABEL_PARTIAL_SEGMENT + getNumberWidget()));
    } else if (event.getPropertyName().equals(Constants.SEEKERBAR_DURATION)) {
      // GATracker - Filters - stops page
      BusProvider.getInstance()
          .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_FILTERS,
              GAnalyticsNames.ACTION_RESULTS_FILTERS,
              GAnalyticsNames.LABEL_PARTIAL_TIMES_DURATION_MAX + event.getNewValue()
                  .toString()
                  .replace(" ", "_") + GAnalyticsNames.LABEL_PARTIAL_SEGMENT + getNumberWidget()));
    }
  }
}
