package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

/**
 * <p>Java class for AdditionalProduct complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="AdditionalProduct">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="reference" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="providerPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="providerCurrency" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="price" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="additionalProductType" type="{http://api.edreams.com/booking/next}additionalProductType"
 * />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */

public class AdditionalProductDTO {

  protected String reference;
  protected BigDecimal providerPrice;
  protected String providerCurrency;
  protected BigDecimal price;
  protected String description;
  protected AdditionalProductTypeDTO additionalProductType;

  /**
   * Gets the value of the reference property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getReference() {
    return reference;
  }

  /**
   * Sets the value of the reference property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setReference(String value) {
    this.reference = value;
  }

  /**
   * Gets the value of the providerPrice property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  /**
   * Sets the value of the providerPrice property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setProviderPrice(BigDecimal value) {
    this.providerPrice = value;
  }

  /**
   * Gets the value of the providerCurrency property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getProviderCurrency() {
    return providerCurrency;
  }

  /**
   * Sets the value of the providerCurrency property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setProviderCurrency(String value) {
    this.providerCurrency = value;
  }

  /**
   * Gets the value of the price property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getPrice() {
    return price;
  }

  /**
   * Sets the value of the price property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setPrice(BigDecimal value) {
    this.price = value;
  }

  /**
   * Gets the value of the description property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getDescription() {
    return description;
  }

  /**
   * Sets the value of the description property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setDescription(String value) {
    this.description = value;
  }

  /**
   * Gets the value of the additionalProductType property.
   *
   * @return possible object is
   * {@link AdditionalProductType }
   */
  public AdditionalProductTypeDTO getAdditionalProductType() {
    return additionalProductType;
  }

  /**
   * Sets the value of the additionalProductType property.
   *
   * @param value allowed object is
   * {@link AdditionalProductType }
   */
  public void setAdditionalProductType(AdditionalProductTypeDTO value) {
    this.additionalProductType = value;
  }
}
