package com.odigeo.app.android.lib.models.dto;

/**
 * @author Irving
 * @version 1.0
 * @since 18/03/2015
 */
public class BookingItinerary {

  //    private int type; // TODO: Maybe this is a ENUM object
  private long arrivalDate;
  private long departureDate;
  private LocationDTO arrival;
  private LocationDTO departure;
  private String tripType;

  public final long getArrivalDate() {
    return arrivalDate;
  }

  public final void setArrivalDate(long arrivalDate) {
    this.arrivalDate = arrivalDate;
  }

  public final long getDepartureDate() {
    return departureDate;
  }

  public final void setDepartureDate(long departureDate) {
    this.departureDate = departureDate;
  }

  public final LocationDTO getArrival() {
    return arrival;
  }

  public final void setArrival(LocationDTO arrival) {
    this.arrival = arrival;
  }

  public final LocationDTO getDeparture() {
    return departure;
  }

  public final void setDeparture(LocationDTO departure) {
    this.departure = departure;
  }

  public final String getTripType() {
    return tripType;
  }

  public final void setTripType(String tripType) {
    this.tripType = tripType;
  }
}
