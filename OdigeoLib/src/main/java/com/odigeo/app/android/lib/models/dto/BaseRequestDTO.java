package com.odigeo.app.android.lib.models.dto;

import java.util.List;

public abstract class BaseRequestDTO {

  protected List<ExtensionRequestDTO> extensions;

  public List<ExtensionRequestDTO> getExtensions() {
    return extensions;
  }

  public void setExtensions(List<ExtensionRequestDTO> extensions) {
    this.extensions = extensions;
  }
}
