package com.odigeo.app.android.lib.data.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.odigeo.app.android.lib.consts.Constants;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class LocalizablesDBManager extends SQLiteOpenHelper {
  public static final String DB_NAME = "localizables.db";
  private static final int DB_VERSION = 1;
  private final File dbFile;

  private final Context myContext;

  public LocalizablesDBManager(Context context) {
    super(context, DB_NAME, null, DB_VERSION);
    dbFile = context.getDatabasePath(DB_NAME);
    this.myContext = context;
  }

  /**
   * Creates the database if it does not exits.
   *
   * @throws IOException
   */
  public void createDataBase() throws IOException {
    if (!existDataBase()) {
      try {
        copyDataBase();
      } catch (IOException e) {
        Log.e(Constants.TAG_LOG, e.getMessage(), e);
      }
    }
  }

  /**
   * Verifies if the database file already exists.
   *
   * @return If the database exists.
   */
  private boolean existDataBase() {
    return dbFile.exists();
  }

  /**
   * Moves the database file from assets to the main application folder.
   *
   * @throws IOException
   */
  private void copyDataBase() throws IOException {
    InputStream myInput = myContext.getAssets().open(DB_NAME);
    OutputStream myOutput = new FileOutputStream(dbFile);
    byte[] buffer = new byte[1024];
    int length = myInput.read(buffer);
    while (length > 0) {
      myOutput.write(buffer, 0, length);
      length = myInput.read(buffer);
    }
    myOutput.flush();
    myOutput.close();
    myInput.close();
  }

  /**
   * Update localizables.dbFile file
   */
  public void updateDatabase() {
    try {
      copyDataBase();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override public void onCreate(SQLiteDatabase db) {
    // Nothing here.
  }

  @Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    // Nothing here.
  }
}
