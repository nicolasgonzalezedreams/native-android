package com.odigeo.app.android.lib.models.dto;

public class DailyTimeRangeDTO {

  protected String fromTime;
  protected String toTime;

  /**
   * Gets the value of the fromTime property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getFromTime() {
    return fromTime;
  }

  /**
   * Sets the value of the fromTime property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setFromTime(String value) {
    this.fromTime = value;
  }

  /**
   * Gets the value of the toTime property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getToTime() {
    return toTime;
  }

  /**
   * Sets the value of the toTime property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setToTime(String value) {
    this.toTime = value;
  }
}
