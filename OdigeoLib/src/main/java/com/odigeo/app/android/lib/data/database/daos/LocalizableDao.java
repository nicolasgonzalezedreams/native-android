package com.odigeo.app.android.lib.data.database.daos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.database.BaseDao;
import com.odigeo.app.android.lib.data.database.model.Localizable;

public class LocalizableDao extends BaseDao<Localizable> {

  public static final String KEY_COLUMN = "key";
  public static final String LOCALIZABLE_COLUMN = "value";
  private static final int UPDATE_FAIL = 0;
  public static String defaultLanguage;

  private final String tableName;

  public LocalizableDao(Context context, String tableName) {
    super();
    this.tableName = tableName;
    this.defaultLanguage =
        context.getResources().getString(R.string.one_cms_default_language_table);
  }

  /**
   * Finds a String through a DAO object.
   *
   * @param key The key value for a localizable.
   * @return An object Localizable.
   */
  public static Localizable getLocalizableByKey(@NonNull Context context, @NonNull String key) {
    String language = Configuration.getInstance().getCurrentMarket().getOneCMSTable();
    LocalizableDao localizableDao = new LocalizableDao(context, language);
    return localizableDao.findOneByKey(key);
  }

  /**
   * Looks for the string in the current table. If the key does not exist, checks in the default
   * table.
   */
  public synchronized Localizable findOneByKey(String key) {

    Cursor cursor =
        getDb().rawQuery("SELECT * FROM " + tableName + " WHERE " + KEY_COLUMN + " = '" + key + "'",
            null);
    if (isCursorEmpty(cursor)) {
      cursor = getDb().rawQuery(
          "SELECT * FROM " + defaultLanguage + " WHERE " + KEY_COLUMN + " = '" + key + "'", null);
    }
    Localizable localizable = parseCursor(cursor);
    cursor.close();

    return localizable;
  }

  /**
   * Return a Localizable object from the data obtained from database.
   */
  private Localizable parseCursor(Cursor cursor) {
    Localizable localizable = new Localizable();
    if (cursor != null) {
      if (cursor.moveToFirst()) {
        localizable.setKey(cursor.getString(cursor.getColumnIndex(KEY_COLUMN)));
        localizable.setValue(cursor.getString(cursor.getColumnIndex(LOCALIZABLE_COLUMN)));
      }
    }
    return localizable;
  }

  /**
   * All the elements are deleted.
   */
  public int deleteAll() {
    return getDb().delete(tableName, null, null);
  }

  @Override public synchronized long insert(Localizable object) {
    ContentValues languageTableValues = getContentValue(object);

    return getDb().insert(tableName, null, languageTableValues);
  }

  @Override public synchronized boolean update(Localizable object) {
    ContentValues languageTableValues = getContentValue(object);

    String[] args = new String[] { object.getKey() };

    long newLocalizableId = getDb().update(tableName, languageTableValues, KEY_COLUMN + "=?", args);

    return newLocalizableId != UPDATE_FAIL;
  }

  @Override public synchronized boolean delete(Localizable object) {
    return false;
  }

  /**
   * Returns a content values for an object.
   */
  private ContentValues getContentValue(Localizable object) {
    ContentValues languageTableValues = new ContentValues();
    languageTableValues.put(KEY_COLUMN, object.getKey());
    languageTableValues.put(LOCALIZABLE_COLUMN, object.getValue());
    return languageTableValues;
  }
}
