package com.odigeo.app.android.lib.utils;

import com.squareup.otto.Bus;

/**
 * Created by manuel on 31/08/14.
 */
public final class BusProvider {
  private static Bus instance;

  private BusProvider() {

  }

  public static synchronized Bus getInstance() {
    if (instance == null) {
      instance = new Bus();
    }

    return instance;
  }
}
