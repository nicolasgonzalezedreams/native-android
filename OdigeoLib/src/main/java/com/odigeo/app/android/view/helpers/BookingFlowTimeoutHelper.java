package com.odigeo.app.android.view.helpers;

import android.os.Bundle;
import android.os.Handler;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.interfaces.ITimeoutWidget;
import com.odigeo.app.android.lib.utils.TimeoutCounterWidgetInitializer;

/**
 * Created by Julio Kun on 28/09/2015.
 */
public class BookingFlowTimeoutHelper {

  private TimeoutListener listener;
  private final Runnable timerRunnable = new Runnable() {
    @Override public void run() {
      if (listener != null) {
        listener.showTimeoutWidget(Constants.TIMEOUT_COUNTER_WIDGET_PASSENGERS * 60,
            Constants.TIMEOUT_COUNTER_WIDGET_PASSENGERS);
      }
    }
  };
  private final Runnable scrollRunnable = new Runnable() {
    @Override public void run() {
      if (listener != null) {
        listener.setScrollParameters();
      }
    }
  };
  private final ITimeoutWidget timeoutWidgetListener = new ITimeoutWidget() {
    @Override public void showTimeoutWidget() {
      listener.showTimeoutWidget(Constants.TIMEOUT_COUNTER_WIDGET_PASSENGERS * 60,
          Constants.TIMEOUT_COUNTER_WIDGET_PASSENGERS);
    }

    @Override public void setScrollParameters() {
      listener.setScrollParameters();
    }

    @Override public void setWidgetClosed() {

    }

    @Override public void showErrorDialog() {
      listener.showTimeOutErrorMessage();
    }
  };
  private TimeoutCounterWidgetInitializer timeoutCounterWidgetInitializer;

  public BookingFlowTimeoutHelper(TimeoutListener listener) {
    timeoutCounterWidgetInitializer = TimeoutCounterWidgetInitializer.getInstance();
    this.listener = listener;
  }

  public void checkTimeoutConditions(Bundle arguments) {

    Handler timeoutHandler = timeoutCounterWidgetInitializer.getHandler();

    if (arguments != null && arguments.containsKey(Constants.TIMEOUT_WIDGET_PREVIOUS_TIME)) {

      int time = arguments.getInt(Constants.TIMEOUT_WIDGET_PREVIOUS_TIME, 0);
      if (time != 0) {
        if (arguments.getBoolean(Constants.TIMEOUT_WIDGET_SHOWN, false)) {
          listener.showTimeoutWidget(time, Constants.TIMEOUT_COUNTER_WIDGET_PASSENGERS);
          timeoutHandler.postDelayed(scrollRunnable,
              Constants.TIMEOUT_COUNTER_SCROLL_PARAMS_REFORMAT);
        } else {
          listener.startTimer(time);
        }
      } else {
        //do nothing.
        timeoutCounterWidgetInitializer.addSubscriber(timeoutWidgetListener);
      }
    } else {

      timeoutHandler.postDelayed(timerRunnable, Constants.TIMEOUT_COUNTER_WIDGET_DELAY);
      timeoutHandler.postDelayed(scrollRunnable, Constants.TIMEOUT_COUNTER_WIDGET_DELAY
          + Constants.TIMEOUT_COUNTER_SCROLL_PARAMS_REFORMAT);
    }
  }

  public ITimeoutWidget getTimeoutWidgetListener() {
    return this.timeoutWidgetListener;
  }

  public void onDestroy() {
    timeoutCounterWidgetInitializer.removeSubscriber(timeoutWidgetListener);

    Handler timeoutHandler = timeoutCounterWidgetInitializer.getHandler();
    timeoutHandler.removeCallbacksAndMessages(null);
  }

  public interface TimeoutListener {

    void startTimer(int time);

    void showTimeoutWidget(int seconds, int minutes);

    void setScrollParameters();

    void showTimeOutErrorMessage();
  }
}