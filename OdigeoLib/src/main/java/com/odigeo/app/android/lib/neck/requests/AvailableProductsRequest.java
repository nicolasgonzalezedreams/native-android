package com.odigeo.app.android.lib.neck.requests;

import android.util.Log;
import com.globant.roboneck.common.NeckCookie;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.dto.requests.AvailableProductsRequestModel;
import com.odigeo.app.android.lib.models.dto.responses.AvailableProductsResponse;
import com.odigeo.app.android.lib.neck.requests.base.BaseOdigeoRequests;
import com.odigeo.data.net.helper.DomainHelperInterface;
import java.util.List;

/**
 * Created by ManuelOrtiz on 10/09/2014.
 */
public class AvailableProductsRequest extends
    BaseOdigeoRequests<AvailableProductsResponse, AvailableProductsResponse, AvailableProductsRequestModel> {

  private static final String PATH = "getAvailableProducts";
  private DomainHelperInterface mDomainHelper;

  /**
   * TODO: Para que se envian la cookies si despues se envie el objeto odigeoSession
   * que ya contiene LAS COOKIES
   */
  public AvailableProductsRequest(AvailableProductsRequestModel requestModel,
      List<NeckCookie> cookies, OdigeoSession odigeoSession, String deviceId,
      DomainHelperInterface domainHelper) {
    super(AvailableProductsResponse.class, requestModel, cookies, odigeoSession, deviceId);
    mDomainHelper = domainHelper;
  }

  @Override protected final String getUrl() {
    String url = String.format("%s%s", mDomainHelper.getUrl(), PATH);

    return url;
  }

  @Override protected AvailableProductsResponse processContent(String responseBody) {

    AvailableProductsResponse response = super.processContent(responseBody);
    Log.i(Constants.TAG_LOG, "CreateShoppingCart Rest Services returned " + responseBody);

    return response;
  }
}
