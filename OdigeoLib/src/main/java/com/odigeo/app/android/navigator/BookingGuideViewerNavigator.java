package com.odigeo.app.android.navigator;

import android.os.Bundle;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.view.ArrivalGuideView;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.presenter.contracts.navigators.ArrivalGuideViewerNavigatorInterface;

/**
 * Created by julio.kun on 11/18/2015.
 */
public class BookingGuideViewerNavigator extends BaseNavigator
    implements ArrivalGuideViewerNavigatorInterface {

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view);
    setUpToolbarButton(0);
    ArrivalGuideView view =
        ArrivalGuideView.getInstance((Booking) getIntent().getSerializableExtra(EXTRA_BOOKING));
    replaceFragment(R.id.fl_container, view);
  }

  @Override public void showDestinationTitle(String destination) {
    setNavigationTitle(destination);
  }
}
