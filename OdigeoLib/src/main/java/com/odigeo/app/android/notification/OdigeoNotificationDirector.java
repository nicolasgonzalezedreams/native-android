package com.odigeo.app.android.notification;

public class OdigeoNotificationDirector {

  private OdigeoNotificationBuilder odigeoNotificationBuilder;

  public void setOdigeoNotificationBuilder(OdigeoNotificationBuilder odigeoNotificationBuilder) {
    this.odigeoNotificationBuilder = odigeoNotificationBuilder;
  }

  public void createNotification() {
    odigeoNotificationBuilder.buildNotificationBuilder();
    odigeoNotificationBuilder.buildNotificationIntent();
    odigeoNotificationBuilder.buildNotificationTaskStackBuilder();
    odigeoNotificationBuilder.buildPendingIntent();
    odigeoNotificationBuilder.sendNotification();
  }
}
