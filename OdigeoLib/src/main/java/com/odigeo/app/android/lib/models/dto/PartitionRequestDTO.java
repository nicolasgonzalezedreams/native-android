package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for partitionRequest complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="partitionRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="dimensionLabel" use="required" type="{http://www.w3.org/2001/XMLSchema}string"
 * />
 *       &lt;attribute name="partitionNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}int"
 * />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class PartitionRequestDTO {

  protected String dimensionLabel;
  protected int partitionNumber;

  /**
   * Gets the value of the dimensionLabel property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getDimensionLabel() {
    return dimensionLabel;
  }

  /**
   * Sets the value of the dimensionLabel property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setDimensionLabel(String value) {
    this.dimensionLabel = value;
  }

  /**
   * Gets the value of the partitionNumber property.
   */
  public int getPartitionNumber() {
    return partitionNumber;
  }

  /**
   * Sets the value of the partitionNumber property.
   */
  public void setPartitionNumber(int value) {
    this.partitionNumber = value;
  }
}
