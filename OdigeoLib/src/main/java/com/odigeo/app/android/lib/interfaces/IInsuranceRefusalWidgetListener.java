package com.odigeo.app.android.lib.interfaces;

/**
 * Created by manuel on 16/09/14.
 */
public interface IInsuranceRefusalWidgetListener {
  void onInsuranceRefusalSelected();

  void onInsuranceRefusalUnselected();
}
