package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class Agent implements Serializable {
  private String firstName;
  private String lastName;

  public final String getFirstName() {
    return firstName;
  }

  public final void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public final String getLastName() {
    return lastName;
  }

  public final void setLastName(String lastName) {
    this.lastName = lastName;
  }
}
