package com.odigeo.app.android.view.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.helper.EmailSender;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.providers.EmailProvider;
import com.odigeo.app.android.view.constants.OneCMSKeys;

public class FeedBackDialog {
  private final Dialog mDialog;
  private final Context mContext;
  private final EmailProvider emailProvider;
  private TextView mTvTitle;
  private Button mBtnErrorInApp;
  private Button mBtnProblem;
  private Button mBtnComment;
  private Button mBtnCancel;

  public FeedBackDialog(Context context, EmailProvider emailProvider) {
    mContext = context;
    mDialog = new Dialog(context);
    mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    mDialog.setContentView(R.layout.feedback_dialog);
    this.emailProvider = emailProvider;
    initComponent();
    setListeners();
    initOneCMSText();
  }

  private void initComponent() {
    mTvTitle = (TextView) mDialog.findViewById(R.id.tvTitle);
    mBtnErrorInApp = (Button) mDialog.findViewById(R.id.btnErrorInApp);
    mBtnProblem = (Button) mDialog.findViewById(R.id.btnProblem);
    mBtnComment = (Button) mDialog.findViewById(R.id.btnComment);
    mBtnCancel = (Button) mDialog.findViewById(R.id.btnCancel);
  }

  private void setListeners() {
    mBtnErrorInApp.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        EmailSender.send(emailProvider.getFeedbackEmail(),
            OneCMSKeys.EMAIL_HELPIMPROVE_SUBJECT_ERROR_INAPP, mContext);
        dismiss();
      }
    });

    mBtnProblem.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        EmailSender.send(emailProvider.getFeedbackEmail(),
            OneCMSKeys.EMAIL_HELPIMPROVE_SUBJECT_BOOKING_PROBLEM, mContext);
        dismiss();
      }
    });

    mBtnComment.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        EmailSender.send(emailProvider.getFeedbackEmail(),
            OneCMSKeys.EMAIL_HELPIMPROVE_SUBJECT_COMMENT_SUGGESTION, mContext);
        dismiss();
      }
    });

    mBtnCancel.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        dismiss();
      }
    });
  }

  private void initOneCMSText() {
    mTvTitle.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.ABOUT_FEEDBACK_DIALOG_TITLE));
    mBtnErrorInApp.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.EMAIL_HELPIMPROVE_SUBJECT_ERROR_INAPP)
            .toString());
    mBtnProblem.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.EMAIL_HELPIMPROVE_SUBJECT_BOOKING_PROBLEM)
            .toString());
    mBtnComment.setText(LocalizablesFacade.getString(mContext,
        OneCMSKeys.EMAIL_HELPIMPROVE_SUBJECT_COMMENT_SUGGESTION).toString());
    mBtnCancel.setText(LocalizablesFacade.getString(mContext, OneCMSKeys.COMMON_CANCEL).toString());
  }

  public void show() {
    mDialog.show();
  }

  public void dismiss() {
    mDialog.dismiss();
  }
}
