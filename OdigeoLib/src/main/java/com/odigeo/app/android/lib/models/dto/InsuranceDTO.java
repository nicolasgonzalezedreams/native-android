package com.odigeo.app.android.lib.models.dto;

import com.odigeo.app.android.lib.models.InsuranceDescriptionItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author miguel
 */
@Deprecated public class InsuranceDTO implements Serializable {

  public static final int SUB_STRING_DESCRIPTION_START = 3;
  private List<InsuranceUrlDTO> conditionsUrls;
  private String policy;
  private String provider;
  // custom
  private String title;
  private String description;
  private String subtitle;
  private String type;
  private List<InsuranceDescriptionItem> insuranceDescriptionItems =
      new ArrayList<InsuranceDescriptionItem>();
  private String conditionAcceptance;
  private String documentLink;
  private String textConditions;
  private String totalPrice;
  private String totalPriceDetail;

  /**
   * Gets the value of the policy property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getPolicy() {
    return policy;
  }

  /**
   * Sets the value of the policy property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setPolicy(String value) {
    this.policy = value;
  }

  /**
   * Gets the value of the provider property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getProvider() {
    return provider;
  }

  /**
   * Sets the value of the provider property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setProvider(String value) {
    this.provider = value;
  }

  public List<InsuranceUrlDTO> getConditionsUrls() {
    return conditionsUrls;
  }

  public void setConditionsUrls(List<InsuranceUrlDTO> conditionsUrls) {
    this.conditionsUrls = conditionsUrls;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public List<InsuranceDescriptionItem> getInsuranceDescriptionItems() {
    return insuranceDescriptionItems;
  }

  public void setInsuranceDescriptionItems(
      List<InsuranceDescriptionItem> insuranceDescriptionItems) {
    this.insuranceDescriptionItems = insuranceDescriptionItems;
  }

  public void parseDescriptionsItems() {
    insuranceDescriptionItems = new ArrayList<InsuranceDescriptionItem>();
    //If the textTitle is null then show the policy as the textTitle
    if (title == null || title.isEmpty()) {
      title = policy;
    }

    if (description != null && !description.isEmpty()) {
      String[] rowsDescription = description.split("\n");

      for (String itemDescription : rowsDescription) {
        String newDescription = itemDescription.trim();

        if (newDescription.startsWith("{b ")) {
          newDescription =
              newDescription.substring(SUB_STRING_DESCRIPTION_START, itemDescription.length() - 1);
          if (newDescription.contains("}")) {     //(newDescription.endsWith(".}")) {
            newDescription = newDescription.replace("}", "");
          }
          newDescription =
              newDescription.substring(0, 1).toUpperCase() + newDescription.substring(1);

          //Remove break lines and spaces
          newDescription = newDescription.replaceAll("\n", "");
          newDescription = newDescription.trim();

          if (!("").equals(newDescription)) {
            insuranceDescriptionItems.add(new InsuranceDescriptionItem(newDescription, true));
          }
        } else {
          //Remove break lines and spaces
          newDescription = newDescription.replaceAll("\n", "");
          newDescription = newDescription.trim();
          addNewDescription(newDescription);
        }
        //                if (newDescription.startsWith(" ")) {
        //                    newDescription = newDescription.substring(1, itemDescription.length() - 1);
        ////                    insuranceDescriptionItems.add(new InsuranceDescriptionItem(newDescription, true));
        //                }
        //
        //                if (newDescription.endsWith(".}")) {
        //                    newDescription = newDescription.substring(0, itemDescription.length() - 1);
        //                    insuranceDescriptionItems.add(new InsuranceDescriptionItem(newDescription, true));
        //                } else {
        //                    insuranceDescriptionItems.add(new InsuranceDescriptionItem(newDescription, false));
        //                }
      }
    }
  }

  /**
   * This method adds a new description to the insurance description list
   *
   * @param newDescription new description to be added
   */
  private void addNewDescription(String newDescription) {
    if (!("").equals(newDescription)) {
      insuranceDescriptionItems.add(new InsuranceDescriptionItem(newDescription, false));
    }
  }

  public String getConditionAcceptance() {
    return conditionAcceptance;
  }

  public void setConditionAcceptance(String conditionAcceptance) {
    this.conditionAcceptance = conditionAcceptance;
  }

  public String getDocumentLink() {
    return documentLink;
  }

  public void setDocumentLink(String documentLink) {
    this.documentLink = documentLink;
  }

  public String getTextConditions() {
    return textConditions;
  }

  public void setTextConditions(String textConditions) {
    this.textConditions = textConditions;
  }

  public String getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(String totalPrice) {
    this.totalPrice = totalPrice;
  }

  public String getTotalPriceDetail() {
    return totalPriceDetail;
  }

  public void setTotalPriceDetail(String totalPriceDetail) {
    this.totalPriceDetail = totalPriceDetail;
  }
}
