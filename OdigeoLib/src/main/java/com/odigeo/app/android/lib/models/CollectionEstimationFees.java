package com.odigeo.app.android.lib.models;

import java.util.Map;

/**
 * Created by manuel on 23/12/14.
 */
public class CollectionEstimationFees {
  private int id;
  private Map<String, CollectionMethodWithPrice> collectionMethodFees;
  private CollectionMethodWithPrice cheapest;

  public final int getId() {
    return id;
  }

  public final void setId(int id) {
    this.id = id;
  }

  public final Map<String, CollectionMethodWithPrice> getCollectionMethodFees() {
    return collectionMethodFees;
  }

  public final void setCollectionMethodFees(
      Map<String, CollectionMethodWithPrice> collectionMethodFees) {
    this.collectionMethodFees = collectionMethodFees;
  }

  public final CollectionMethodWithPrice getCheapest() {
    return cheapest;
  }

  public final void setCheapest(CollectionMethodWithPrice cheapest) {
    this.cheapest = cheapest;
  }
}
