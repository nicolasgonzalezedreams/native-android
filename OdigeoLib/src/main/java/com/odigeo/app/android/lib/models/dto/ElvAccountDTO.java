package com.odigeo.app.android.lib.models.dto;

public class ElvAccountDTO {

  protected String accountNumber;
  protected String sortCode;

  /**
   * Gets the value of the accountNumber property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getAccountNumber() {
    return accountNumber;
  }

  /**
   * Sets the value of the accountNumber property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setAccountNumber(String value) {
    this.accountNumber = value;
  }

  /**
   * Gets the value of the sortCode property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getSortCode() {
    return sortCode;
  }

  /**
   * Sets the value of the sortCode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setSortCode(String value) {
    this.sortCode = value;
  }
}
