package com.odigeo.app.android.lib.models;

import android.text.TextUtils;
import com.odigeo.app.android.lib.interfaces.OdigeoInterfaces;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.entity.geo.LocationDescriptionType;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Irving Lóp on 02/09/2014.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 2.1
 * @since 06/02/2015
 */
public class CityItemListDestination extends City implements OdigeoInterfaces.ItemListDestination {
  private boolean subItem = false;

  /**
   * Method to build a new instance from a {@link City} object
   *
   * @param city City where be extracted the information to build the new object
   * @param searchText Text of who the user search, this its used to select the correct name to be
   * displayed.
   * @return A new and non null instance
   */
  public static CityItemListDestination fromCity(City city, String searchText) {
    CityItemListDestination item = new CityItemListDestination();
    //Check if hasn't a valid searchText or if the cityName contains the searchText,
    // then put the cityName
    if (TextUtils.isEmpty(searchText) || city.getCityName()
        .toLowerCase(Locale.getDefault())
        .contains(searchText)) {
      item.setCityName(city.getCityName());
    } else {
      //Check one by one who is the correct string to show, if neither fits, put the cityName
      String name = city.getCityName();
      if (city.getLocationNames() != null) {
        for (String locationName : city.getLocationNames()) {
          String normalizedName = Normalizer.normalize(locationName, Normalizer.Form.NFD)
              .replaceAll("[^\\p{ASCII}]", "");
          if (normalizedName.toLowerCase(Locale.getDefault()).contains(searchText)) {
            name = locationName;
            break;
          }
        }
      }
      item.setCityName(name);
    }
    item.setCoordinates(city.getCoordinates());
    item.setCountryCode(city.getCountryCode());
    item.setGeoNodeId(city.getGeoNodeId());
    item.setIataCode(city.getIataCode());
    item.setCountryName(city.getCountryName());
    item.setName(city.getName());
    item.setType(city.getType());
    item.setDistanceToCurrentLocation(city.getDistanceToCurrentLocation());
    return item;
  }

  /**
   * Expand the cities into city and every airport existent in each city.
   *
   * @param cities Cities to expand.
   * @param searchedText Searched text to filter and select the correct name to display.
   * @return List with all the expanded elements.
   */
  public static List<OdigeoInterfaces.ItemListDestination> expandList(List<City> cities,
      String searchedText) {
    List<OdigeoInterfaces.ItemListDestination> listDestinations = new ArrayList<>();
    for (City city : cities) {
      //Check if the Name of the city & the IATA code are equals.
      if (city.getName().equals(city.getIataCode())) {
        //Change the Name for the city name and the type for CITY.
        city.setName(city.getCityName());
        city.setType(LocationDescriptionType.CITY);
      }
      //Check if it's a simple item without sub-items.
      if (city.getRelatedLocations() != null && city.getRelatedLocations().size() > 1) {
        CityItemListDestination listParentItem =
            CityItemListDestination.fromCity(city, searchedText);

        listDestinations.add(listParentItem);
        for (City subCity : city.getRelatedLocations()) {
          CityItemListDestination listItem =
              CityItemListDestination.fromCity(subCity, searchedText);
          listItem.setSubItem(true);
          listDestinations.add(listItem);
        }

        listParentItem.setGroupParent(true);
      } else {
        listDestinations.add(CityItemListDestination.fromCity(city, searchedText));
      }
    }
    return listDestinations;
  }

  public final boolean isSubItem() {
    return subItem;
  }

  public final void setSubItem(boolean isSubItem) {
    this.subItem = isSubItem;
  }

  @Override public final boolean isSection() {
    return false;
  }
}
