package com.odigeo.app.android.lib.models;

/**
 * Enums that saves the Odigeo screen size
 *
 * @author M. en C. Javier Silva Perez
 * @since 12/03/15.
 */
public enum OdigeoScreenSize {

  LOW("S"), MEDIUM("M"), HIGH("L"), XHIGH("XL"), XXHIGH("XXL"), XXXHIGH("XXL");

  private String odigeoId;

  OdigeoScreenSize(String odigeoId) {
    this.odigeoId = odigeoId;
  }

  public String getOdigeoId() {
    return odigeoId;
  }

  public void setOdigeoId(String odigeoId) {
    this.odigeoId = odigeoId;
  }

}
