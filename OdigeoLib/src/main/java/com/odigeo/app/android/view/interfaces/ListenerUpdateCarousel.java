package com.odigeo.app.android.view.interfaces;

public interface ListenerUpdateCarousel {
  void onUpdateCarousel();
}
