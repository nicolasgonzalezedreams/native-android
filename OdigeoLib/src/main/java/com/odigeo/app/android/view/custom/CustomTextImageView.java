package com.odigeo.app.android.view.custom;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import java.util.UUID;

public class CustomTextImageView extends LinearLayout {

  LayoutInflater inflater;

  public CustomTextImageView(Context context) {
    super(context);
    init();
  }

  public CustomTextImageView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public CustomTextImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
  public CustomTextImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr,
      int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
    init();
  }

  private void init() {
    setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
    inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  public void addText(CharSequence text) {
    TextView textView = (TextView) inflater.inflate(R.layout.custom_text_layout, null);
    textView.setText(text);
    addView(textView);
  }

  public void addImage(int resourceId) {
    ImageView imageView = (ImageView) inflater.inflate(R.layout.custom_image_layout, null);
    imageView.setImageResource(resourceId);
    addView(imageView);
  }

  public void clearAll() {
    removeAllViews();
  }
}