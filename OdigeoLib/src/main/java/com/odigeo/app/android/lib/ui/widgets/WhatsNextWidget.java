package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.dto.BookingSummaryStatus;
import com.odigeo.app.android.lib.ui.widgets.base.PressableWidget;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.shoppingCart.BookingStatus;
import java.util.List;

/**
 * Created by emiliano.desantis on 07/10/2014.
 */
public class WhatsNextWidget extends LinearLayout {

  private BookingStatus statusDTO;
  private LinearLayout container;
  private ConfirmationInfoItem2Texts emailConfirmationWidget;
  private Context mContext;
  private TextView mTvConfirmationTitle;

  public WhatsNextWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
    mContext = context;
  }

  private void initialize(BookingStatus statusConfirmation) {
    if (statusConfirmation != null) {
      if (statusConfirmation == BookingStatus.CONTRACT) {
        inflate(getContext(), R.layout.layout_confirmation_whatsnext_success, this);
      } else {
        inflate(getContext(), R.layout.layout_confirmation_whatsnext_pending, this);
      }

      emailConfirmationWidget =
          (ConfirmationInfoItem2Texts) findViewById(R.id.wdgtConfirmationMail);
    }

    mTvConfirmationTitle = (TextView) findViewById(R.id.tvConfirmationTitle);
    mTvConfirmationTitle.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.CONFIRMATION_WHATSNEXT_HEADERTITLE));
  }

  /**
   * Set status to the booking
   *
   * @param statusConfirmation Current status to set.
   */
  public void setStatus(BookingStatus statusConfirmation) {
    this.statusDTO = statusConfirmation;
    this.removeAllViewsInLayout();
    initialize(statusConfirmation);
    container = (LinearLayout) findViewById(R.id.llWhatNextContainer);
    refreshDrawableState();
  }

  /**
   * Set status to the booking
   *
   * @param statusConfirmation Current status.
   */
  public void setStatus(BookingSummaryStatus statusConfirmation) {

    if (statusConfirmation == BookingSummaryStatus.OK) {
      setStatus(BookingStatus.CONTRACT);
    } else if (statusConfirmation == BookingSummaryStatus.PENDING) {
      setStatus(BookingStatus.PENDING);
    } else {
      setStatus(BookingStatus.REJECTED);
    }
  }

  public void setEmailConfirmation(String email) {
    if (emailConfirmationWidget != null) {
      emailConfirmationWidget.setText(email);
    }
  }

  public void setBookingReference(List<String> referenceIds) {
    if ((statusDTO == BookingStatus.CONTRACT || statusDTO == BookingStatus.PENDING)
        && referenceIds != null) {

      PressableWidget lastForm = null;
      for (String reference : referenceIds) {
        ConfirmationInfoItem2Texts item = new ConfirmationInfoItem2Texts(getContext());
        item.setText(reference);
        item.setIdImage(R.drawable.confirmation_update);
        CharSequence secondText = LocalizablesFacade.getString(getContext(),
            "confirmation_whatsnext_bookingid_placeholder");
        item.setSecondText(secondText);
        lastForm = item;
        container.addView(item);
      }

      if (lastForm != null) {
        emailConfirmationWidget.setLastInForm(false);
        lastForm.setLastInForm(true);
      }
    }
  }
}
