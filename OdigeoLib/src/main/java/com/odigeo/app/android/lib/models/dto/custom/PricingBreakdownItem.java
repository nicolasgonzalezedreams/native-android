package com.odigeo.app.android.lib.models.dto.custom;

import java.io.Serializable;

@Deprecated public class PricingBreakdownItem implements Serializable {

  protected PricingBreakdownItemType priceItemType;
  protected float priceItemAmount;

  public PricingBreakdownItem() {

  }

  public PricingBreakdownItem(PricingBreakdownItemType priceItemType, float priceItemAmount) {
    this.priceItemType = priceItemType;
    this.priceItemAmount = priceItemAmount;
  }

  public float getPriceItemAmount() {
    return priceItemAmount;
  }

  public void setPriceItemAmount(float priceItemAmount) {
    this.priceItemAmount = priceItemAmount;
  }

  public PricingBreakdownItemType getPriceItemType() {
    return priceItemType;
  }

  public void setPriceItemType(PricingBreakdownItemType priceItemType) {
    this.priceItemType = priceItemType;
  }
}
