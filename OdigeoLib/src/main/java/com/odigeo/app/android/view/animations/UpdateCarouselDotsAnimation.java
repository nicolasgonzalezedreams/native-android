package com.odigeo.app.android.view.animations;

import android.os.Handler;
import android.widget.TextView;

public class UpdateCarouselDotsAnimation {

  private static final int MAX_N_DOTS = 4;

  private Handler mHandlerDotsAnimation;
  private Runnable mRunnableDotsAnimation;

  private String mUpdatingDataInfoAnimated;
  private String mUpdateDataInfo;
  private boolean isFragmentVisible;
  private TextView mTextView;
  private int mNumberDots = 0;
  private String mDot = ".";

  public UpdateCarouselDotsAnimation(Handler handlerDotsAnimation) {
    this.mHandlerDotsAnimation = handlerDotsAnimation;
  }

  public void initAnimationData(String updateDataInfo, TextView textViewUpdate) {
    this.mUpdateDataInfo = updateDataInfo;
    this.mTextView = textViewUpdate;
  }

  public void runDotsAnimation() {
    isFragmentVisible = true;
    mRunnableDotsAnimation = new Runnable() {
      public void run() {
        if (isFragmentVisible) {
          drawDot();
          mHandlerDotsAnimation.postDelayed(this, 500);
        }
      }
    };
    mHandlerDotsAnimation.postDelayed(mRunnableDotsAnimation, 100);
  }

  private void drawDot() {
    if ((mNumberDots % MAX_N_DOTS) == 0) {
      mUpdatingDataInfoAnimated = mUpdateDataInfo + " ";
      mDot = "";
    } else {
      mDot = ".";
    }
    mUpdatingDataInfoAnimated = mUpdatingDataInfoAnimated + mDot;
    mNumberDots++;
    mTextView.setText(mUpdatingDataInfoAnimated);
  }

  public void stopDotsAnimation() {
    isFragmentVisible = false;
    mHandlerDotsAnimation.removeCallbacks(mRunnableDotsAnimation);
  }
}

