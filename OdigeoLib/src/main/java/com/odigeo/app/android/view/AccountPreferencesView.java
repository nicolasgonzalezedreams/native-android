package com.odigeo.app.android.view;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.navigator.BaseNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.dialogs.CustomDialogView;
import com.odigeo.app.android.view.helpers.DialogHelper;
import com.odigeo.app.android.view.interfaces.AuthDialogActionInterface;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.AccountPreferencesPresenter;
import com.odigeo.presenter.contracts.navigators.AccountPreferencesNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.AccountPreferencesViewInterface;

public class AccountPreferencesView extends BaseView<AccountPreferencesPresenter>
    implements AccountPreferencesViewInterface {

  private EditText editTextEmail;
  private EditText editTextPassword;
  private Button btnRemoveAccount;
  private TextInputLayout mAccountPreferencesPassword;
  private DialogHelper makingActionDialog;
  private ImageView editButton;

  public static AccountPreferencesView newInstance() {
    return new AccountPreferencesView();
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = super.onCreateView(inflater, container, savedInstanceState);
    ((BaseNavigator) getActivity()).setUpToolbarButton(0);
    return view;
  }

  @Override public void onResume() {
    super.onResume();
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_MYINFO_ACCOUNT);
  }

  @Override protected AccountPreferencesPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideAccountPreferencesPresenter(this,
            (AccountPreferencesNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.sso_account_preferences;
  }

  @Override protected void initComponent(View view) {
    editTextEmail = (EditText) view.findViewById(R.id.edit_text_account_preferences_email);
    editTextPassword = (EditText) view.findViewById(R.id.edit_text_account_preferences_password);
    btnRemoveAccount =
        (Button) view.findViewById(R.id.sso_account_preferences_semantic_negative_blocker_button);
    mAccountPreferencesPassword =
        (TextInputLayout) view.findViewById(R.id.sso_account_preferences_password);
    editButton = ((ImageView) view.findViewById(R.id.image_edit));

    mPresenter.initializeUserEmail();

    makingActionDialog = new DialogHelper(getActivity());
  }

  @Override public void initializeUserEmail(String email) {
    editTextEmail.setText(email);
  }

  @Override protected void setListeners() {

    editTextEmail.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.changeEmailAction();
      }
    });

    editTextPassword.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.editPasswordAction();
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_DATA_ACC_DETAILS,
            TrackerConstants.ACTION_ACCOUNT_SUMMARY, TrackerConstants.LABEL_OPEN_PASSWORD);
      }
    });

    btnRemoveAccount.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.deleteAccountAction();
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_DATA_ACC_DETAILS,
            TrackerConstants.ACTION_ACCOUNT_SUMMARY, TrackerConstants.LABEL_DELETE_ACCOUNT_CLICKS);
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    ((BaseNavigator) getActivity()).setNavigationTitle(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_PERSONALAREA_PREFERENCES)
            .toString());
    ((TextInputLayout) view.findViewById(R.id.sso_account_preferences_email)).setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_FORM_SHADOWTEXT_EMAIL));
    ((TextInputLayout) view.findViewById(R.id.sso_account_preferences_password)).setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_FORM_SHADOWTEXT_PASSWORD));
    btnRemoveAccount.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ACCOUNT_DELETE));
  }

  @Override public void showCantChangeEmailDialog() {
    String title =
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_BLOCKED_MAIL_TITLE);
    String message =
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_BLOCKED_MAIL_MESSAGE);
    String positiveButtonText =
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_BLOCKED_MAIL_OK);
    OnClickListener positiveListener = new OnClickListener() {
      @Override public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
      }
    };
    new CustomDialogView(this.getContext(), title, message, positiveButtonText,
        positiveListener).show();
  }

  @Override public void showDeleteAccountDialog() {
    String title =
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_DELETE_ACCOUNT_TITLE);
    String message =
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_DELETE_ACCOUNT_MESSAGE);
    String positiveButtonText =
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_DELETE_ACCOUNT_OK);
    String negativeButtonText =
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_DELETE_ACCOUNT_CANCEL);
    OnClickListener positiveListener = new OnClickListener() {
      @Override public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
        mPresenter.removeAccount();
      }
    };

    OnClickListener negativeListener = new OnClickListener() {
      @Override public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
      }
    };

    new CustomDialogView(this.getContext(), title, message, positiveButtonText, positiveListener,
        negativeButtonText, negativeListener).show();
  }

  @Override public void showRemovingAccountDialog() {
    makingActionDialog = new DialogHelper(getActivity());
    makingActionDialog.showDialog(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_DELETE_ACCOUNT_WAIT).toString());
  }

  @Override public void accountRemoved(boolean wasAccountRemoved) {
    String message = wasAccountRemoved ? LocalizablesFacade.getString(getActivity(),
        OneCMSKeys.SSO_ACCOUNT_REMOVED_SUCCESS).toString()
        : LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ACCOUNT_REMOVE_FAIL)
            .toString();
    int imgResource = wasAccountRemoved ? R.drawable.ok_process : R.drawable.hud_wrong_info;
    makingActionDialog.showDoneDialog(getActivity(), message, imgResource,
        new DialogHelper.ProcessDoneListener() {
          @Override public void onDismiss() {
            mPresenter.deleteView();
          }
        });
  }

  @Override public void showAuthError() {
    makingActionDialog.showAuthErrorDialog(this, new AuthDialogActionInterface() {
      @Override public void OnAuthDialogOK() {
        mPresenter.onAuthOkClicked();
      }
    });
  }

  @Override public void onLogoutError(com.odigeo.data.net.error.MslError error, String message) {
    Log.e(error.name(), message);
  }
}
