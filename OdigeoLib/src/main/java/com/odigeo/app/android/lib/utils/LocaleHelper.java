package com.odigeo.app.android.lib.utils;

import android.support.annotation.NonNull;
import java.util.Locale;

public interface LocaleHelper {

  Locale getCurrentLocale();

  String getCurrentLanguageIso();

  String getCurrentIsoLocale();

  String localeToString(Locale locale);

  String getCurrencySymbol(String locale);

  String getCurrencySymbol();

  String getCurrencyCode(String locale);

  String getCurrencyCode();

  String getLocalizedCurrencyValue(double value);

  String getLocalizedCurrencyValue(double value, Locale locale, boolean validateDecimals);

  String getLocalizedNumber(int value, String locale);

  String getLocalizedNumber(int value);

  String getLocalizedNumber(double value, String locale);

  String getLocalizedNumber(double value);

  int getLocalizedSearchLength();

  boolean isJapaneseCharacter(String query);

  /**
   * Deprecated, use {@link com.odigeo.interactors.provider.MarketProviderInterface}
   */
  @Deprecated Locale strToLocale(String strLocale);

  @NonNull String getLocalizedCurrencyFeeValue(Double fee);
}
