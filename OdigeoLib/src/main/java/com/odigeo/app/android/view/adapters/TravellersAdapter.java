package com.odigeo.app.android.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.extensions.UIUserTraveller;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by carlos.navarrete on 07/09/15.
 */

public class TravellersAdapter extends RecyclerView.Adapter {

  public static final int TRAVELLERS_VIEW_TYPE = 0;
  public static final int TRAVELLERS_HEADER_VIEW_TYPE = 1;
  public static final int TRAVELLERS_FOOTER_VIEW_TYPE = 2;
  public static final int EMPTY_VIEW = 3;

  private List<UIUserTraveller> mList;

  private TravellersListeners mTravellersListener;
  private Context mContext;

  public TravellersAdapter(List<UserTraveller> userTravellers, Context context,
      TravellersListeners listener) {
    mContext = context;
    this.mList = new ArrayList<>();
    if (userTravellers != null) {
      for (int i = 0; i < userTravellers.size(); i++) {
        if (userTravellers.get(i) != null) {
          mList.add(new UIUserTraveller(userTravellers.get(i)));
        } else {
          mList.add(null);
        }
      }
    }
    if (mList.isEmpty()) {
      this.mList.add(null);
    }
    this.mTravellersListener = listener;
  }

  @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
    switch (i) {
      case TRAVELLERS_VIEW_TYPE:
        return createTravellerViewHolder(viewGroup);
      case TRAVELLERS_HEADER_VIEW_TYPE:
        return createHeaderTravellerViewHolder(viewGroup);
      case TRAVELLERS_FOOTER_VIEW_TYPE:
        return createFooterTravellerViewHolder(viewGroup);
      case EMPTY_VIEW:
        return createEmptyTravellerViewHolder(viewGroup);
    }
    throw new IllegalArgumentException();
  }

  @Override public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
    switch (viewHolder.getItemViewType()) {
      case TRAVELLERS_VIEW_TYPE:
        TravellerViewHolder mTravellerViewHolder = (TravellerViewHolder) viewHolder;
        initTravellerViewHolderItem(mTravellerViewHolder, mList.get(i), i);
        break;
      case TRAVELLERS_HEADER_VIEW_TYPE:
        HeaderViewHolder mHeaderViewHolder = (HeaderViewHolder) viewHolder;
        initHeaderViewHolder(mHeaderViewHolder);
        break;
      case TRAVELLERS_FOOTER_VIEW_TYPE:
        FooterViewHolder mFooterViewHolder = (FooterViewHolder) viewHolder;
        initFooterViewHolder(mFooterViewHolder);
        break;
      case EMPTY_VIEW:
        EmptyViewHolder mEmptyViewHolder = (EmptyViewHolder) viewHolder;
        initEmptyView(mEmptyViewHolder);
        break;
    }
  }

  @Override public int getItemViewType(int position) {
    if (mList.size() > 1) {
      if (mList.get(position) == null) {
        if (position == 0) {
          return TRAVELLERS_HEADER_VIEW_TYPE;
        } else {
          return TRAVELLERS_FOOTER_VIEW_TYPE;
        }
      } else {
        return TRAVELLERS_VIEW_TYPE;
      }
    } else {
      return EMPTY_VIEW;
    }
  }

  @Override public int getItemCount() {
    return mList.size();
  }

  private RecyclerView.ViewHolder createTravellerViewHolder(ViewGroup viewGroup) {
    View travellerView = LayoutInflater.from(viewGroup.getContext())
        .inflate(R.layout.sso_travellers_list_row, viewGroup, false);
    return new TravellerViewHolder(travellerView);
  }

  private RecyclerView.ViewHolder createHeaderTravellerViewHolder(ViewGroup viewGroup) {
    View headerTravellerView = LayoutInflater.from(viewGroup.getContext())
        .inflate(R.layout.sso_travellers_list_header, viewGroup, false);
    return new HeaderViewHolder(headerTravellerView);
  }

  private RecyclerView.ViewHolder createFooterTravellerViewHolder(ViewGroup viewGroup) {
    View footer = LayoutInflater.from(viewGroup.getContext())
        .inflate(R.layout.sso_travellers_list_footer, viewGroup, false);
    return new FooterViewHolder(footer);
  }

  private RecyclerView.ViewHolder createEmptyTravellerViewHolder(ViewGroup viewGroup) {
    View mView = LayoutInflater.from(viewGroup.getContext())
        .inflate(R.layout.sso_travellers_empty_view, viewGroup, false);
    return new EmptyViewHolder(mView);
  }

  private void initHeaderViewHolder(HeaderViewHolder mHeaderViewHolder) {
    mHeaderViewHolder.mHeaderText.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.SSO_TRAVELLERS_SAVED_INFO));
    mHeaderViewHolder.mHeaderList.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.SSO_TRAVELLERS_SAVED));
  }

  private void initFooterViewHolder(FooterViewHolder mFooterViewHolder) {
    mFooterViewHolder.mButtonFooterAction.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.SSO_TRAVELLERS_ADD));
    mFooterViewHolder.mButtonFooterAction.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        if (mTravellersListener != null) {
          mTravellersListener.onClick(false);
        }
      }
    });
  }

  private void initTravellerViewHolderItem(TravellerViewHolder mTravellerViewHolder,
      final UIUserTraveller travellerItem, int position) {
    if (travellerItem.getUserProfile() != null) {
      mTravellerViewHolder.mNameTraveller.setText(travellerItem.getUserProfile()
          .getName()
          .concat(" ")
          .concat(travellerItem.getUserProfile().getFirstLastName()));
      if (travellerItem.getUserProfile().isDefaultTraveller()) {
        mTravellerViewHolder.mImageDefault.setVisibility(View.VISIBLE);
      } else {
        mTravellerViewHolder.mImageDefault.setVisibility(View.INVISIBLE);
      }
    }

    UserTraveller.TypeOfTraveller travellerType = travellerItem.getTypeOfTraveller();
    if (travellerType.equals(UserTraveller.TypeOfTraveller.CHILD)) {
      mTravellerViewHolder.mImageProfile.setImageResource(R.drawable.confirmation_child);
      mTravellerViewHolder.mTypeTraveller.setText(
          LocalizablesFacade.getString(mContext, OneCMSKeys.PASSENGER_TYPE_CHILD));
    } else if (travellerItem.getTypeOfTraveller().equals(UserTraveller.TypeOfTraveller.INFANT)) {
      mTravellerViewHolder.mTypeTraveller.setText(
          LocalizablesFacade.getString(mContext, OneCMSKeys.PASSENGER_TYPE_INFANT));
      mTravellerViewHolder.mImageProfile.setImageResource(R.drawable.confirmation_infant);
    } else {
      mTravellerViewHolder.mImageProfile.setImageResource(R.drawable.confirmation_adult);
      mTravellerViewHolder.mTypeTraveller.setText(
          LocalizablesFacade.getString(mContext, OneCMSKeys.PASSENGER_TYPE_ADULT));
    }

    if (position == mList.size() - 2) {
      mTravellerViewHolder.separator.setVisibility(View.GONE);
    }

    mTravellerViewHolder.parentView.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mTravellersListener.onClickTraveller(travellerItem.getUserTravellerId(),
            travellerItem.getUserProfile().isDefaultTraveller());
      }
    });
  }

  private void initEmptyView(EmptyViewHolder mEmptyViewHolder) {
    mEmptyViewHolder.mTextViewTitle.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.SSO_TRAVELLERS_NONE_SAVED));
    mEmptyViewHolder.mTextViewSubTitle.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.SSO_TRAVELLERS_NONE_SAVED_INFO));
    mEmptyViewHolder.mButtonAddPassenger.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.SSO_TRAVELLERS_ADD));
    mEmptyViewHolder.mButtonAddPassenger.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        if (mTravellersListener != null) {
          mTravellersListener.onClick(true);
        }
      }
    });
  }

  private static class HeaderViewHolder extends RecyclerView.ViewHolder {

    TextView mHeaderText;
    TextView mHeaderList;

    public HeaderViewHolder(View itemHeaderView) {
      super(itemHeaderView);
      mHeaderText = (TextView) itemView.findViewById(R.id.sso_travellers_list_header_textview);
      mHeaderList = (TextView) itemView.findViewById(R.id.sso_travellers_list_header_list);
    }
  }

  private static class TravellerViewHolder extends RecyclerView.ViewHolder {

    ImageView mImageProfile;
    ImageView mImageDefault;
    TextView mTypeTraveller;
    TextView mNameTraveller;
    View separator;
    View parentView;

    public TravellerViewHolder(View itemView) {
      super(itemView);
      parentView = itemView;
      mImageDefault = (ImageView) itemView.findViewById(R.id.imgTravellerTypeDefault);
      mImageProfile = (ImageView) itemView.findViewById(R.id.imgTravellerType);
      mTypeTraveller = (TextView) itemView.findViewById(R.id.txtButtonWithHint_hint);
      mNameTraveller = (TextView) itemView.findViewById(R.id.txtButtonWithHint_text);
      separator = itemView.findViewById(R.id.id_layout_separator);
    }
  }

  private static class FooterViewHolder extends RecyclerView.ViewHolder {

    private Button mButtonFooterAction;

    public FooterViewHolder(View itemFooterView) {
      super(itemFooterView);
      mButtonFooterAction = (Button) itemFooterView.findViewById(
          R.id.sso_travellers_list_footer_button_add_passenger);
    }
  }

  private static class EmptyViewHolder extends RecyclerView.ViewHolder {

    public TextView mTextViewTitle;
    public TextView mTextViewSubTitle;
    public Button mButtonAddPassenger;

    public EmptyViewHolder(View itemView) {
      super(itemView);
      mTextViewTitle = (TextView) itemView.findViewById(R.id.sso_travellers_empty_view_title);
      mTextViewSubTitle = (TextView) itemView.findViewById(R.id.sso_travellers_empty_view_subtitle);
      mButtonAddPassenger = (Button) itemView.findViewById(R.id.sso_travellers_empty_view_action);
    }
  }
}

