package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class PromotionalCodeRequestDTO {

  protected String code;
  protected BigDecimal discount;

  /**
   * Gets the value of the code property.
   *
   * @return possible object is {@link String }
   */
  public String getCode() {
    return code;
  }

  /**
   * Sets the value of the code property.
   *
   * @param value allowed object is {@link String }
   */
  public void setCode(String value) {
    this.code = value;
  }

  /**
   * Gets the value of the discount property.
   *
   * @return possible object is {@link java.math.BigDecimal }
   */
  public BigDecimal getDiscount() {
    return discount;
  }

  /**
   * Sets the value of the discount property.
   *
   * @param value allowed object is {@link java.math.BigDecimal }
   */
  public void setDiscount(BigDecimal value) {
    this.discount = value;
  }
}
