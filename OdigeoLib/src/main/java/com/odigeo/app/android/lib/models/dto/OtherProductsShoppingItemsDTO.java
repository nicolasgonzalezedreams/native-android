package com.odigeo.app.android.lib.models.dto;

import java.util.ArrayList;
import java.util.List;

public class OtherProductsShoppingItemsDTO {

  protected List<InsuranceShoppingItemDTO> insuranceShoppingItems;
  protected List<AdditionalProductShoppingItemDTO> additionalProductShoppingItems;

  /**
   * Gets the value of the insuranceShoppingItems property.
   */
  public List<InsuranceShoppingItemDTO> getInsuranceShoppingItems() {
    if (insuranceShoppingItems == null) {
      insuranceShoppingItems = new ArrayList<InsuranceShoppingItemDTO>();
    }
    return this.insuranceShoppingItems;
  }

  /**
   * Sets the value of the insuranceShoppingItems property.
   */
  public void setInsuranceShoppingItems(List<InsuranceShoppingItemDTO> insuranceShoppingItems) {
    this.insuranceShoppingItems = insuranceShoppingItems;
  }

  /**
   * Gets the value of the additionalProductShoppingItems property.
   */
  public List<AdditionalProductShoppingItemDTO> getAdditionalProductShoppingItems() {
    if (additionalProductShoppingItems == null) {
      additionalProductShoppingItems = new ArrayList<AdditionalProductShoppingItemDTO>();
    }
    return this.additionalProductShoppingItems;
  }

  /**
   * Sets the value of the additionalProductShoppingItems property.
   */
  public void setAdditionalProductShoppingItems(
      List<AdditionalProductShoppingItemDTO> additionalProductShoppingItems) {
    this.additionalProductShoppingItems = additionalProductShoppingItems;
  }
}
