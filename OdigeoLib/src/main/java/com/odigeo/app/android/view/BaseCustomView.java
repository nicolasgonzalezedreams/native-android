package com.odigeo.app.android.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.odigeo.DependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.presenter.contracts.views.BaseCustomComponentInterface;

public abstract class BaseCustomView extends LinearLayout implements BaseCustomComponentInterface {

  protected DependencyInjector dependencyInjector;
  protected LocalizableProvider localizables;
  protected TrackerControllerInterface trackerController;

  public BaseCustomView(Context context) {

    super(context);
    init();
  }

  public BaseCustomView(Context context, @Nullable AttributeSet attrs) {

    super(context, attrs);
    init();
  }

  private void init() {
    inflate(getContext(), getComponentLayout(), this);
    dependencyInjector = ((OdigeoApp) getContext().getApplicationContext()).getDependencyInjector();
    localizables = dependencyInjector.provideLocalizables();
    trackerController = dependencyInjector.provideTrackerController();
    initComponent();
    initOneCMSText();
  }
}
