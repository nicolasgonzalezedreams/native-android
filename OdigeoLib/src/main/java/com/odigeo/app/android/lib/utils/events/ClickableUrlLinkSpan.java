package com.odigeo.app.android.lib.utils.events;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.odigeo.app.android.lib.R;

/**
 * Created by Irving on 04/03/2015.
 *
 * @author Irving
 * @since 04/03/2015.
 *
 * Enable click in a specific text to a web page
 */
public class ClickableUrlLinkSpan extends ClickableSpan {

  private final Activity activity;
  private final Intent intent;

  /**
   * Default constructor
   *
   * @param activity The activity when launch the intent for web view
   * @param url The page to show
   */
  public ClickableUrlLinkSpan(Activity activity, String url) {
    this.activity = activity;
    this.intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
  }

  /**
   * The event to launch when you click on specific text with this ClickableSpan
   *
   * @param widget The View that containing the specific text
   */
  @Override public final void onClick(View widget) {

    activity.startActivity(intent);
  }

  /**
   * Style for the specific text
   *
   * @param ds paint of the TextView
   */
  @Override public final void updateDrawState(TextPaint ds) {

    ds.setColor(activity.getResources().getColor(R.color.primary_brand));
    ds.setUnderlineText(true);
  }
}
