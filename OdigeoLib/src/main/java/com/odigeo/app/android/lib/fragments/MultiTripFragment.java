package com.odigeo.app.android.lib.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.odigeo.app.android.lib.activities.OdigeoSearchActivity;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.fragments.base.TripFragmentBase;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.ui.widgets.TripLegWidget;
import com.odigeo.data.entity.TravelType;
import java.util.List;

/**
 * Created by Irving on 01/09/2014.
 */

public class MultiTripFragment extends TripFragmentBase {

  /**
   * Creates a new instance of this fragment using search options and flight segments
   *
   * @param searchOptions Search options to loadWidgetImage
   * @param flightSegments Flight segments to show
   * @return A new instance of this fragment
   */
  public static MultiTripFragment newInstance(SearchOptions searchOptions,
      List<FlightSegment> flightSegments) {
    MultiTripFragment multiTripFragment = new MultiTripFragment();
    multiTripFragment.setSearchOptions(searchOptions);
    multiTripFragment.setFlightSegments(flightSegments);
    return multiTripFragment;
  }

  @Override public final View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = super.onCreateView(inflater, container, savedInstanceState);

    if (isShouldLaunchSearch()) {
      setShouldLaunchSearch(false);
      launchSearch(false);
    }

    return rootView;
  }

  @Override public void setControlsValues() {
    super.setControlsValues();

    getSearchTripWidget().getResidentsWidget().setVisibility(View.GONE);

    if (getFlightSegments().size() == getSearchTripWidget().getTripLegWidgets().size()) {
      int counter = 0;
      List<TripLegWidget> tripLegWidgets = getSearchTripWidget().getTripLegWidgets();
      for (TripLegWidget tripLegWidget : tripLegWidgets) {
        if (counter > 0 && tripLegWidget.getFlightSegment().getDepartureCity() == null) {
          tripLegWidget.getFlightSegment()
              .setDepartureCity(
                  tripLegWidgets.get(counter - 1).getFlightSegment().getArrivalCity());
        }
        tripLegWidget.updateData();
        counter++;
      }
      getSearchTripWidget().updateViews();
    } else {
      getSearchTripWidget().initializeMultidestination();
    }
  }

  @Override public final boolean isDataValid() {

    for (FlightSegment flightSegment : getFlightSegments()) {
      if (flightSegment.getDate() == 0
          || flightSegment.getDepartureCity() == null
          || flightSegment.getArrivalCity() == null
          || flightSegment.getArrivalCity()
          .getIataCode()
          .equals(flightSegment.getDepartureCity().getIataCode())) {
        return false;
      }
    }

    return true;
  }

  @Override public final TravelType getTravelType() {
    return TravelType.MULTIDESTINATION;
  }

  @Override protected void setSegments() {

    if (getActivity() != null
        && ((OdigeoSearchActivity) getActivity()).getFlightSegmentsMultiple() != null) {

      List<FlightSegment> fligtSegments =
          ((OdigeoSearchActivity) getActivity()).getFlightSegmentsMultiple();

      if (!fligtSegments.isEmpty()
          && getSearchTripWidget().getTripLegWidgets().size() == fligtSegments.size()) {
        int index = 0;
        for (TripLegWidget tripLegWidget : getSearchTripWidget().getTripLegWidgets()) {
          tripLegWidget.setFlightSegment(fligtSegments.get(index++));
        }
      }
    }
  }

  @Override public boolean isShownInThisFragment(SearchOptions searchOptions) {
    return searchOptions.getTravelType() == TravelType.MULTIDESTINATION;
  }
}
