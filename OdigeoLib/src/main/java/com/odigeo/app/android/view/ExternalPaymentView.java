package com.odigeo.app.android.view;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.interfaces.WebViewJavascriptInterface;
import com.odigeo.builder.UrlBuilder;
import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.presenter.ExternalPaymentPresenter;
import com.odigeo.presenter.contracts.ExternalPaymentNavigatorInterface;
import com.odigeo.presenter.contracts.views.ExternalPaymentViewInterface;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Map;

public class ExternalPaymentView extends BaseView<ExternalPaymentPresenter>
    implements ExternalPaymentViewInterface, WebViewJavascriptInterface.Listener {

  private static final String TAG = "ExternalPaymentView";

  private WebView webViewExternalPayment;
  private BookingResponse bookingResponse;

  private ProgressBar progressBar;

  // This url and javaScriptButton are stored with Paypal purposes
  private String currentUrl;
  private String javaScriptCancelButton;
  private WebViewClient webViewClient = new WebViewClient() {

    @Override public boolean shouldOverrideUrlLoading(WebView view, String url) {
      view.loadUrl(url);
      return true;
    }

    @Override public void onPageFinished(WebView view, String url) {
      super.onPageFinished(view, url);

      mPresenter.onParseFormData();
      mPresenter.onUrlLoadedFinished();
    }

    @Override public void onPageStarted(WebView view, String url, Bitmap favicon) {
      super.onPageStarted(view, url, favicon);
      currentUrl = url;
      if (shouldInterceptPageChange(url)) {
        mPresenter.onUrlChanged(url);
      }
    }
  };

  public ExternalPaymentView setInstance(BookingResponse bookingResponse) {
    Bundle bundle = new Bundle();
    bundle.putSerializable(Constants.EXTRA_BOOKING_RESPONSE, bookingResponse);
    setArguments(bundle);
    return this;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    bookingResponse =
        (BookingResponse) getArguments().getSerializable(Constants.EXTRA_BOOKING_RESPONSE);
  }

  @Override protected ExternalPaymentPresenter getPresenter() {
    return dependencyInjector.provideExternalPaymentPresenter(this,
        (ExternalPaymentNavigatorInterface) getContext());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.external_payment_view;
  }

  @Override protected void initComponent(View view) {
    webViewExternalPayment = (WebView) view.findViewById(R.id.wbExternalPayment);
    progressBar = (ProgressBar) view.findViewById(R.id.progressBarLoading);
    webViewExternalPayment.getSettings().setJavaScriptEnabled(true);
    webViewExternalPayment.setWebViewClient(webViewClient);
    webViewExternalPayment.addJavascriptInterface(new WebViewJavascriptInterface(this), "android");
  }

  @Override public void onResume() {
    super.onResume();

    mPresenter.loadUrl(bookingResponse.getUserInteractionNeededResponse());
  }

  public void clickOnCancelWebViewButton() {
    webViewExternalPayment.loadUrl("javascript:" + javaScriptCancelButton);
  }

  public boolean canGoBack() {
    return webViewExternalPayment.canGoBack();
  }

  public void goBack() {
    webViewExternalPayment.goBack();
  }

  @Override protected void setListeners() {

  }

  @Override protected void initOneCMSText(View view) {
    javaScriptCancelButton =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.PAYPAL_CANCELATION).toString();
  }

  public void trackGoBack() {
  }

  @Override public void hideProgressBar() {
    progressBar.setVisibility(View.GONE);
  }

  @Override public void loadUrl(String paymentUrl) {
    webViewExternalPayment.loadUrl(paymentUrl);
  }

  public String getCurrentUrl() {
    return currentUrl;
  }

  protected boolean shouldInterceptPageChange(String url) {
    return !mPresenter.isPaymentFinishedOrCancelled(url);
  }

  @Override public void onBackPressed() {
  }

  @Override public void loadPostUrl(String redirectUrl, Map<String, String> params) {
    try {
      final byte[] queryString = new UrlBuilder.Builder().params(params)
          .build()
          .getQueryString()
          .getBytes(Charset.forName("UTF-8"));

      webViewExternalPayment.postUrl(redirectUrl, queryString);
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      crashlytics.trackNonFatal(e);
    }
  }

  @Override public void onFormDataParsed(String formData) {
    mPresenter.onFormDataParsed(formData);
  }
}
