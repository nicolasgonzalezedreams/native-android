package com.odigeo.app.android.lib.models.dto;

public class UserInteractionNeededRequestDTO {

  protected String returnUrl;

  /**
   * Gets the value of the returnUrl property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getReturnUrl() {
    return returnUrl;
  }

  /**
   * Sets the value of the returnUrl property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setReturnUrl(String value) {
    this.returnUrl = value;
  }
}
