package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.ui.widgets.base.ButtonWithAndWithoutHint;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ManuelOrtiz on 11/09/2014.
 */
public class ButtonSearchDate extends ButtonWithAndWithoutHint {

  private Date date;

  public ButtonSearchDate(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public final int getEmptyLayout() {
    return R.layout.layout_search_trip_destination_empty_widget;
  }

  @Override public final int getLayout() {
    return R.layout.layout_search_trip_date_widget;
  }

  public final Date getDate() {
    return date;
  }

  public final void setDate(Date date) {
    this.date = date;

    if (date != null) {
      SimpleDateFormat simpleDateFormat;
      if (Configuration.getInstance().getCurrentMarket().getKey().equals("JP")) {
        simpleDateFormat =
            OdigeoDateUtils.getGmtDateFormat(getResources().getString(R.string.templates__date2));
      } else {
        simpleDateFormat = OdigeoDateUtils.getGmtDateFormat(
            getResources().getString(R.string.templates__datelong1));
      }

      this.setText(simpleDateFormat.format(date));
    } else {
      this.clear();
    }
  }
}

