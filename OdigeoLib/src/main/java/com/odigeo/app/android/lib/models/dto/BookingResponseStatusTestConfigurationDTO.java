package com.odigeo.app.android.lib.models.dto;

public enum BookingResponseStatusTestConfigurationDTO {

  BROKEN_FLOW, BOOKING_ERROR, BOOKING_PAYMENT_RETRY, BOOKING_STOP;

  public static BookingResponseStatusTestConfigurationDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
