package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.globant.roboneck.base.RoboNeckFragment;
import com.globant.roboneck.requests.BaseNeckRequestException;
import com.globant.roboneck.requests.BaseNeckRequestListener;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.odigeo.DependencyInjector;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoNoConnectionActivity;
import com.odigeo.app.android.lib.activities.OdigeoSearchResultsActivity;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.dto.responses.FlightSearchResponse;
import com.odigeo.app.android.lib.neck.requests.FlightRequest;
import com.odigeo.app.android.lib.ui.widgets.OdometerWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.DeviceUtils;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.shoppingCart.Step;
import java.util.List;
import java.util.Random;

/**
 * Created by manuel on 12/09/14.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 2.1.2
 * @since 25/02/2015
 */
public class OdigeoSearchWaitingResultsFragment extends RoboNeckFragment {
  private static final int LAP_TIME = 40;
  private static final int MAX_AIRLINES_NUMBER = 549;
  private static final String INIT_COUNT = "1";
  private static final String SEPARATOR = ", ";
  private static final int MAX_COMBINATIONS = 1200000;
  private static final int MAX_RANGE_COMBINATIONS_NUMBER = 500;
  private static final int FIRST_NUMBER_AIRLINES_COUNTER = 100;
  private final Handler handler = new Handler();
  private OdigeoApp odigeoApp;
  private OdigeoSession odigeoSession;
  private OdigeoSearchResultsActivity activityParent;
  private OdometerWidget counterAirlines;
  private TextView txtNumberCombinations;
  private final Runnable runnable = new Runnable() {
    @Override public void run() {
      int currentValue = 0;
      try {
        currentValue = Integer.parseInt(txtNumberCombinations.getText().toString());
      } catch (Exception e) {
        Log.e(this.getClass().toString(), e.getMessage());
      }
      int nextValue = currentValue + new Random().nextInt(MAX_RANGE_COMBINATIONS_NUMBER);
      if (nextValue > MAX_COMBINATIONS) {
        nextValue = MAX_COMBINATIONS;
      }
      String strNextValue = String.valueOf(nextValue);
      txtNumberCombinations.setText(strNextValue);
      handler.postDelayed(runnable, LAP_TIME);
    }
  };

  @Override public void onAttach(Activity activity) {
    super.onAttach(activity);
    activityParent = (OdigeoSearchResultsActivity) activity;
    odigeoApp = (OdigeoApp) activityParent.getApplication();
    odigeoSession = odigeoApp.getOdigeoSession();
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View root = inflater.inflate(R.layout.layout_search_searching_fragment, container, false);
    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {
      TextView txtDestination = (TextView) root.findViewById(R.id.txtDestination);
      txtNumberCombinations = (TextView) root.findViewById(R.id.txtNumberCombinations);

      TextView txtConnectingNotice = (TextView) root.findViewById(R.id.txtConnectingNotice);
      txtConnectingNotice.setText(
          LocalizablesFacade.getString(getContext(), OneCMSKeys.WAITINGVIEW_INFORMATIVEMESSAGE));

      //Initialize the animation
      txtNumberCombinations.setText(INIT_COUNT);
      counterAirlines = (OdometerWidget) root.findViewById(R.id.wdgtAirlines);
      counterAirlines.setTextHeader(
          LocalizablesFacade.getString(getActivity(), "waitingview_airlinescounter_title")
              .toString());
      counterAirlines.setTextFooter(
          LocalizablesFacade.getString(getActivity(), "waitingview_airlinescounter_subtitle")
              .toString());
      counterAirlines.setTargetNumber(MAX_AIRLINES_NUMBER);
      counterAirlines.setTargetInterval(LAP_TIME);
      counterAirlines.setInitialNumber(FIRST_NUMBER_AIRLINES_COUNTER);
      txtDestination.setText(HtmlUtils.formatHtml(getDestinationsNames()));
      postGAScreenTrackingWaitingPage();
    }
    return root;
  }

  @Override public void onStart() {
    super.onStart();
    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {
      DependencyInjector dependencyInjector = AndroidDependencyInjector.getInstance();
      FlightRequest flightRequest = new FlightRequest(activityParent.getRequestModel(),
          DeviceUtils.getDeviceID(getActivity()), dependencyInjector.provideDomainHelper());

      flightRequest.setTimeoutInSeconds(Constants.TIMEOUT_SEARCH);
      FlightListener listener = new FlightListener();
      spiceManager.executeCacheRequest(flightRequest, listener);
      counterAirlines.startAnimation();
      handler.postDelayed(runnable, LAP_TIME);
    }
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {

    super.onViewCreated(view, savedInstanceState);
    if (odigeoSession == null || !odigeoSession.isDataHasBeenLoaded()) {
      Util.sendToHome(activityParent, odigeoApp);
    }
  }

  private String getDestinationsNames() {
    //Set the destinations names
    List<FlightSegment> flightSegments = activityParent.getSearchOptions().getFlightSegments();
    StringBuilder destinationNames = new StringBuilder();
    if (flightSegments.get(0).getArrivalCity().getCityName() != null) {
      destinationNames.append(flightSegments.get(0).getArrivalCity().getCityName());
    }
    //If it is multisearch, add the others destinations names
    if (activityParent.getSearchOptions().getTravelType() == TravelType.MULTIDESTINATION) {
      for (int i = 1; i < flightSegments.size(); i++) {
        if (flightSegments.get(i).getArrivalCity().getCityName() != null) {
          destinationNames.append(SEPARATOR);
          destinationNames.append(flightSegments.get(i).getArrivalCity().getCityName());
        }
      }
    }
    return destinationNames.toString();
  }

  @Override public void onStop() {
    handler.removeCallbacks(runnable);
    super.onStop();
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == Activity.RESULT_CANCELED) {
      getActivity().finish();
    }
  }

  private void postGATrackingEventErrorNoConnection() {
    BusProvider.getInstance()
        .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_ERROR_NO_CONNECTION));
  }

  private void postGAScreenTrackingWaitingPage() {
    BusProvider.getInstance()
        .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_WAITING_PAGE_ONE));
  }

  public class FlightListener extends BaseNeckRequestListener<FlightSearchResponse> {

    @Override public void onRequestSuccessful(FlightSearchResponse flightSearchResponse) {
      Log.i(Constants.TAG_LOG, "onRequestSuccessful Flights Rest Service ");

      if (flightSearchResponse != null) {
        MSLErrorUtilsDialogFragment errorFragment =
            MSLErrorUtilsDialogFragment.newInstance(flightSearchResponse.getMessages(),
                flightSearchResponse.getError());

        if (errorFragment != null) {
          errorFragment.setParentScreen(Step.RESULTS.toString());
          errorFragment.show(getFragmentManager(), errorFragment.getCode());
        } else {
          flightSearchResponse.setTravelType(activityParent.getSearchOptions().getTravelType());
          activityParent.onFlightsRequestSuccess(flightSearchResponse);
        }
      }
    }

    @Override public void onRequestError(BaseNeckRequestException.Error error) {
      Log.e(Constants.TAG_LOG, "onRequestError Flights Rest Service : " + error.getMessage());

      if (error.getErrorCode() == Constants.ERROR_CODE_SPICE_TIMEOUT) {
        counterAirlines.setTargetNumber(1);
        Util.showTimeoutException(getFragmentManager(), Step.SEARCH);
      } else {
        postGATrackingEventErrorNoConnection();

        OdigeoNoConnectionActivity.launch(OdigeoSearchWaitingResultsFragment.this,
            Constants.REQUEST_CODE_FLIGHTLISTENER);
      }
    }

    @Override public void onRequestException(SpiceException exception) {
      if (Util.isSpiceTimeoutException(exception)) {
        counterAirlines.setTargetNumber(1);
        Util.showTimeoutException(getFragmentManager(), Step.SEARCH);
      } else {
        Log.e(Constants.TAG_LOG,
            "onRequestException Flights Rest Service : " + exception.getMessage());

        postGATrackingEventErrorNoConnection();

        OdigeoNoConnectionActivity.launch(OdigeoSearchWaitingResultsFragment.this,
            Constants.REQUEST_CODE_FLIGHTLISTENER);
      }
    }
  }
}
