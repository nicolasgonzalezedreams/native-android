package com.odigeo.app.android.lib.models.dto.custom;

/**
 * @author Odigeo
 */
@Deprecated public enum MessageCode {

  WNG_006("WNG-006"), WNG_007("WNG-007"), WNG_008("WNG-008"), WNG_009("WNG-009"), WNG_010(
      "WNG-010"), WNG_019("WNG-019"), WNG_022("WNG-022"), WNG_024("WNG-024"), INT_002("INT-002"),

  //Warning returned from DAPI when no results in search response

  //Velocity
  VEL_001("VEL-001"), VEL_002("VEL-002"), VEL_003("VEL-003");

  private final String messageCode;

  /**
   * @param warningCode
   */
  private MessageCode(final String messageCode) {
    this.messageCode = messageCode;
  }

  /**
   * @return warningCode
   */
  public String getMessageCode() {
    return messageCode;
  }
}
