package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for preferences complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="preferences">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="locale" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="mkportal" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="webSite" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class PreferencesDTO {

  protected String currency;
  protected String locale;
  protected String mkportal;
  protected String webSite;

  /**
   * Gets the value of the currency property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCurrency() {
    return currency;
  }

  /**
   * Sets the value of the currency property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCurrency(String value) {
    this.currency = value;
  }

  /**
   * Gets the value of the locale property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getLocale() {
    return locale;
  }

  /**
   * Sets the value of the locale property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setLocale(String value) {
    this.locale = value;
  }

  /**
   * Gets the value of the mkportal property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getMkportal() {
    return mkportal;
  }

  /**
   * Sets the value of the mkportal property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setMkportal(String value) {
    this.mkportal = value;
  }

  /**
   * Gets the value of the webSite property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getWebSite() {
    return webSite;
  }

  /**
   * Sets the value of the webSite property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setWebSite(String value) {
    this.webSite = value;
  }
}
