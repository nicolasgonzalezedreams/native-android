package com.odigeo.app.android.lib.models.dto.custom;

/**
 * @author miguel
 */
public enum Code {

  // Define application errors
  GENERAL_ERROR(1000), PROVIDER_DOWN(1002), // Define data received errors
  INVALID_LOCATION(2000), INVALID_USER_DATA(2001), SEARCH_TIMEOUT(2002), SESSION_TIMEOUT(
      2002), NO_RESULTS(2003), INVALID_DEVICE_ID(2004), INVALID_DAPI_CONTEXT(2005), BROKEN_FLOW(
      2006), STOP_FLOW(2007), VERSION_ERROR(2008), VERSION_TO_BE_DEPRECATED(2009), // Retry errors
  SELECTION_ITINERARY_PRODUCT_ERROR(3006), CONNECTION_TIMEOUT(4000), NO_ERROR(0);

  private final Integer errorCode;

  /**
   * @param errorCode
   */
  private Code(final Integer errorCode) {
    this.errorCode = errorCode;
  }

  /**
   * @return
   */
  public Integer getErrorCode() {
    return errorCode;
  }

}
