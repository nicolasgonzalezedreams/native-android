package com.odigeo.app.android.lib.models.dto.requests;

import com.odigeo.app.android.lib.models.dto.InvoiceRequestDTO;
import com.odigeo.app.android.lib.models.dto.ItinerarySortCriteriaDTO;
import com.odigeo.app.android.lib.models.dto.PaymentDataRequestDTO;
import java.io.Serializable;

/**
 * Created by ManuelOrtiz on 08/10/2014.
 */
public class BookingRequestModel implements Serializable {

  protected PaymentDataRequestDTO paymentData;
  protected long bookingId;
  protected String clientBookingReferenceId;
  protected ItinerarySortCriteriaDTO sortCriteria;
  protected InvoiceRequestDTO invoice;

  /**
   * Gets the value of the paymentData property.
   *
   * @return possible object is
   * {@link PaymentDataRequestDTO }
   */
  public final PaymentDataRequestDTO getPaymentData() {
    return paymentData;
  }

  /**
   * Sets the value of the paymentData property.
   *
   * @param value allowed object is
   * {@link PaymentDataRequestDTO }
   */
  public final void setPaymentData(PaymentDataRequestDTO value) {
    this.paymentData = value;
  }

  /**
   * Gets the value of the bookingId property.
   */
  public final long getBookingId() {
    return bookingId;
  }

  /**
   * Sets the value of the bookingId property.
   */
  public final void setBookingId(long value) {
    this.bookingId = value;
  }

  /**
   * Gets the value of the clientBookingReferenceId property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getClientBookingReferenceId() {
    return clientBookingReferenceId;
  }

  /**
   * Sets the value of the clientBookingReferenceId property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setClientBookingReferenceId(String value) {
    this.clientBookingReferenceId = value;
  }

  /**
   * Gets the value of the sortCriteria property.
   */
  public final ItinerarySortCriteriaDTO getSortCriteria() {
    return sortCriteria;
  }

  /**
   * Sets the value of the sortCriteria property.
   */
  public final void setSortCriteria(ItinerarySortCriteriaDTO sortCriteria) {
    this.sortCriteria = sortCriteria;
  }

  /**
   * Gets the value of the invoice property.
   */
  public InvoiceRequestDTO getInvoice() {
    return invoice;
  }

  /**
   * Sets the value of the sortCriteria property.
   */
  public void setInvoice(InvoiceRequestDTO invoice) {
    this.invoice = invoice;
  }
}
