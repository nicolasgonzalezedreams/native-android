package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class PostsellServiceOptionProduct implements Serializable {

  private int id;
  private ProductTypeDTO productType;
  private boolean mandatory;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public ProductTypeDTO getProductType() {
    return productType;
  }

  public void setProductType(ProductTypeDTO productType) {
    this.productType = productType;
  }

  public boolean isMandatory() {
    return mandatory;
  }

  public void setMandatory(boolean mandatory) {
    this.mandatory = mandatory;
  }
}
