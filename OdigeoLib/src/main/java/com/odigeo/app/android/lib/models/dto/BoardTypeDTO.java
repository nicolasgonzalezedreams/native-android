package com.odigeo.app.android.lib.models.dto;

public enum BoardTypeDTO {

  RO, HB, AI, BB, FB, UN;

  public static BoardTypeDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
