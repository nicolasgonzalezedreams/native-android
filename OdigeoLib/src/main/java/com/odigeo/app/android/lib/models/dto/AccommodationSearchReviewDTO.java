package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class AccommodationSearchReviewDTO {

  protected BigDecimal averageUsersRate;
  protected int numberOfUsersRates;
  protected UsersRateSourceDTO usersRateSource;

  /**
   * Gets the value of the averageUsersRate property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getAverageUsersRate() {
    return averageUsersRate;
  }

  /**
   * Sets the value of the averageUsersRate property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setAverageUsersRate(BigDecimal value) {
    this.averageUsersRate = value;
  }

  /**
   * Gets the value of the numberOfUsersRates property.
   */
  public int getNumberOfUsersRates() {
    return numberOfUsersRates;
  }

  /**
   * Sets the value of the numberOfUsersRates property.
   */
  public void setNumberOfUsersRates(int value) {
    this.numberOfUsersRates = value;
  }

  /**
   * Gets the value of the usersRateSource property.
   *
   * @return possible object is
   * {@link UsersRateSource }
   */
  public UsersRateSourceDTO getUsersRateSource() {
    return usersRateSource;
  }

  /**
   * Sets the value of the usersRateSource property.
   *
   * @param value allowed object is
   * {@link UsersRateSource }
   */
  public void setUsersRateSource(UsersRateSourceDTO value) {
    this.usersRateSource = value;
  }
}
