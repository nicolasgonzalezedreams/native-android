package com.odigeo.app.android.view.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.ui.widgets.base.OdigeoWaitingDialog;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.dialogs.CustomDialogView;
import com.odigeo.app.android.view.interfaces.AuthDialogActionInterface;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.dataodigeo.net.controllers.FacebookController;

public class DialogHelper {

  private static final int FEEDBACK_DIALOG_LIFETIME = 2500;
  private OdigeoWaitingDialog waitingDialog;
  private final Activity activity;
  private final LocalizableProvider localizableProvider;

  public DialogHelper(Activity activity) {
    this.activity = activity;
    this.localizableProvider =
        ((OdigeoApp) activity.getApplicationContext()).getDependencyInjector()
            .provideLocalizables();
  }

  /**
   * Show a waiting dialog with the string. To remove needs to call {@link #hideDialog()} or {@link
   * #showDoneDialog(Activity, String, int, * ProcessDoneListener)}
   *
   * @param message String to be showed in the dialog.
   */
  public void showDialog(final String message) {
    waitingDialog = new OdigeoWaitingDialog(activity, true);
    activity.runOnUiThread(new Runnable() {
      @Override public void run() {
        waitingDialog.show(message);
      }
    });
  }

  /**
   * Show a waiting dialog with the string. To remove needs to call {@link #hideDialog()} or {@link
   * #showDoneDialog(Activity, String, int, * ProcessDoneListener)}
   *
   * @param message CharSequence to be showed in the dialog.
   */
  public void showDialog(final CharSequence message) {
    waitingDialog = new OdigeoWaitingDialog(activity, true);
    activity.runOnUiThread(new Runnable() {
      @Override public void run() {
        waitingDialog.show(message);
      }
    });
  }

  /**
   * Remove the dialog shown.
   */
  public void hideDialog() {
    activity.runOnUiThread(new Runnable() {
      @Override public void run() {
        if (waitingDialog != null) {
          waitingDialog.dismiss();
        }
      }
    });
  }

  /**
   * Show the dialog to notify that the user is logged.
   *
   * @param activity activity where the dialog will be attached.
   * @param message message to be shown in the dialog.
   */
  public void showDoneDialog(final Activity activity, final String message,
      @DrawableRes final int idImgResource, final ProcessDoneListener listener) {
    if (waitingDialog != null) {
      waitingDialog.dismiss();
    }
    activity.runOnUiThread(new Runnable() {
      @Override public void run() {
        waitingDialog = new OdigeoWaitingDialog(activity, idImgResource);
        waitingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
          @Override public void onDismiss(DialogInterface dialog) {
            listener.onDismiss();
          }
        });
        waitingDialog.show(message, FEEDBACK_DIALOG_LIFETIME);
      }
    });
  }

  public void showErrorDialog(Context context, String error) {
    AlertDialog.Builder builder = new AlertDialog.Builder(context);
    builder.setTitle(localizableProvider.getString("error_flow"));
    builder.setMessage(error);
    builder.setPositiveButton(localizableProvider.getString("common_ok"),
        new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
          }
        });
    builder.create().show();
  }

  /**
   * Show a dialog to ask the user for Marshmallow's permissions.
   *
   * @param activity activity where the dialog will be attached.
   * @param explanation explanation to be shown in the dialog.
   * @param title title to be shown in the dialog.
   */

  public void showPermissionDeniedDialog(final Activity activity, String title,
      String explanation) {
    Intent intent = new Intent();
    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
    Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
    intent.setData(uri);
    showDialog(activity, title, explanation, intent);
  }

  /**
   * Show a dialog to ask the user to activate App notifications.
   *
   * @param activity activity where the dialog will be attached.
   * @param explanation explanation to be shown in the dialog.
   * @param title title to be shown in the dialog.
   */

  public void showNotificationsDisabledDialog(final Activity activity, String title,
      String explanation) {
    Intent intent = new Intent();
    intent.setAction(Settings.ACTION_SETTINGS);
    showDialog(activity, title, explanation, intent);
  }

  private void showDialog(final Activity activity, String title, String explanation,
      final Intent action) {
    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
    builder.setTitle(title);
    builder.setMessage(explanation);
    builder.setPositiveButton(localizableProvider.getString(OneCMSKeys.COMMON_OK),
        new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
          }
        });
    builder.setNegativeButton(
        localizableProvider.getString(OneCMSKeys.ALERT_NOTIFICATIONS_SETTINGS),
        new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
            activity.startActivity(action);
          }
        });
    builder.create().show();
  }

  public void showAuthErrorDialog(@Nullable final Fragment fragment,
      final AuthDialogActionInterface listener) {
    DialogInterface.OnClickListener positiveAction = new DialogInterface.OnClickListener() {
      @Override public void onClick(DialogInterface dialog, int which) {
        listener.OnAuthDialogOK();
        if (fragment != null) {
          FacebookController facebookController =
              AndroidDependencyInjector.getInstance().provideFacebookHelper(fragment, null);
          facebookController.logout();
        }
      }
    };

    DialogInterface.OnClickListener negativeAction = new DialogInterface.OnClickListener() {
      @Override public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
      }
    };

    String title = Configuration.getInstance().getBrandVisualName();
    String message = localizableProvider.getString(OneCMSKeys.ALERT_NOTIFICATIONS_LOGIN_AGAIN);
    String positiveText = localizableProvider.getString(OneCMSKeys.COMMON_OK);
    String negativeText = localizableProvider.getString(OneCMSKeys.COMMON_CANCEL);

    CustomDialogView dialog =
        new CustomDialogView(activity, title, message, positiveText, positiveAction, negativeText,
            negativeAction);
    dialog.show();
  }

  public void showAuthErrorDialog(final AuthDialogActionInterface listener) {
    showAuthErrorDialog(null, listener);
  }

  /**
   * Listener to notify when a process finished.
   */
  public interface ProcessDoneListener {

    /**
     * Executes an action after the process finishing.
     */
    void onDismiss();
  }
}