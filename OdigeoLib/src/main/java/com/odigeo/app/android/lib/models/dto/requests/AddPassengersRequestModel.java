package com.odigeo.app.android.lib.models.dto.requests;

import com.odigeo.app.android.lib.models.dto.BaseRequestDTO;
import com.odigeo.app.android.lib.models.dto.BuyerRequestDTO;
import com.odigeo.app.android.lib.models.dto.ItinerarySortCriteriaDTO;
import com.odigeo.app.android.lib.models.dto.TravellerRequestDTO;
import com.odigeo.data.entity.shoppingCart.Step;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ManuelOrtiz on 17/09/2014.
 */
public class AddPassengersRequestModel extends BaseRequestDTO implements Serializable {

  protected BuyerRequestDTO buyer;
  protected List<TravellerRequestDTO> travellerRequests;
  protected long bookingId;
  protected ItinerarySortCriteriaDTO sortCriteria;
  protected Step step;

  public AddPassengersRequestModel() {
    travellerRequests = new ArrayList<TravellerRequestDTO>();
  }

  /**
   * @return the travellerRequests
   */
  public final List<TravellerRequestDTO> getTravellerRequests() {
    return travellerRequests;
  }

  /**
   * @param travellerRequests the travellerRequests to set
   */
  public final void setTravellerRequests(List<TravellerRequestDTO> travellerRequests) {
    this.travellerRequests = travellerRequests;
  }

  /**
   * Gets the value of the buyer property.
   *
   * @return possible object is
   * {@link BuyerRequestDTO }
   */
  public final BuyerRequestDTO getBuyer() {
    return buyer;
  }

  /**
   * Sets the value of the buyer property.
   *
   * @param value allowed object is
   * {@link BuyerRequestDTO }
   */
  public final void setBuyer(BuyerRequestDTO value) {
    this.buyer = value;
  }

  /**
   * Gets the value of the bookingId property.
   */
  public final long getBookingId() {
    return bookingId;
  }

  /**
   * Sets the value of the bookingId property.
   */
  public final void setBookingId(long value) {
    this.bookingId = value;
  }

  /**
   * Gets the value of the sortCriteria property.
   */
  public final ItinerarySortCriteriaDTO getSortCriteria() {
    return sortCriteria;
  }

  /**
   * Sets the value of the sortCriteria property.
   */
  public final void setSortCriteria(ItinerarySortCriteriaDTO sortCriteria) {
    this.sortCriteria = sortCriteria;
  }

  /**
   * Gets the value of the step property.
   */
  public final Step getStep() {
    return step;
  }

  /**
   * Sets the value of the step property.
   */
  public final void setStep(Step step) {
    this.step = step;
  }
}
