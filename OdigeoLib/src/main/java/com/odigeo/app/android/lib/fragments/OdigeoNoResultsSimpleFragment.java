package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.fragments.base.OdigeoNoResultsFragment;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;

/**
 * Simple no results fragment
 *
 * @author Manuel Ortiz
 * @author Javier Silva
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 2.1
 * @since 06/02/2015
 */
public class OdigeoNoResultsSimpleFragment extends OdigeoNoResultsFragment {

  /**
   * Creates a new instance for this no result fragment
   *
   * @param searchOptions Search options used in the fragment
   * @return A new instance
   */
  public static OdigeoNoResultsSimpleFragment newInstance(SearchOptions searchOptions) {
    OdigeoNoResultsSimpleFragment noResultsSimpleFragment = new OdigeoNoResultsSimpleFragment();
    noResultsSimpleFragment.setSearchOptions(searchOptions);
    return noResultsSimpleFragment;
  }

  @Override public final void onAttach(Activity activity) {
    super.onAttach(activity);

    postGAEventSearchNoResults();

    postGAScreenSearchHadNoResults();
  }

  @Override public final boolean isDataValid() {
    FlightSegment segmentDeparture = getSearchOptions().getFlightSegments().get(0);

    return segmentDeparture.getDate() != 0
        && segmentDeparture.getDepartureCity() != null
        && segmentDeparture.getArrivalCity() != null
        && !segmentDeparture.getDepartureCity()
        .getIataCode()
        .equalsIgnoreCase(segmentDeparture.getArrivalCity().getIataCode());
  }

  @Override public final void onStart() {
    super.onStart();

    if (searchTripWidget != null) {
      searchTripWidget.hideReturnDate();
    }
  }

  @Override public void onCabinClassSelected(CabinClassDTO cabinClassSelected) {
    //just for implements
  }

  private void postGAScreenSearchHadNoResults() {

    // GAnalytics screen tracking - Create new passenger
    BusProvider.getInstance().post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_NO_RESULTS
        //                        , getActivity().getApplication()
    ));
  }

  private void postGAEventSearchNoResults() {

    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_RESULTS,
            GAnalyticsNames.ACTION_RESULTS_LIST, GAnalyticsNames.LABEL_SEARCH_NO_RESULTS));
  }
}
