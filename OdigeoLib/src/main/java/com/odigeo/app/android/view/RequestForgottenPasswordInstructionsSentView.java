package com.odigeo.app.android.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.navigator.RequestForgottenPasswordNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.RequestForgottenPasswordInstructionsSentPresenter;
import com.odigeo.presenter.contracts.views.RequestForgottenPasswordIntructionsSentViewInterface;

/**
 * Created by matias.dirusso on 21/08/2015.
 */
public class RequestForgottenPasswordInstructionsSentView
    extends BaseView<RequestForgottenPasswordInstructionsSentPresenter>
    implements RequestForgottenPasswordIntructionsSentViewInterface {

  private static final String USER_EMAIL = "USER_EMAIL";
  private Button mGoToLoginButton;
  private TextView mTxtEmail;

  public static RequestForgottenPasswordInstructionsSentView newInstance(String email) {
    RequestForgottenPasswordInstructionsSentView view =
        new RequestForgottenPasswordInstructionsSentView();
    Bundle args = new Bundle();
    args.putString(USER_EMAIL, email);
    view.setArguments(args);

    return view;
  }

  @Override public void onResume() {
    super.onResume();
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_LOGIN_PASSWORD_RECOVERY_SENT);
  }

  @Override protected RequestForgottenPasswordInstructionsSentPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideRequestForgottenPasswordIntructionsSentPresenter(this,
            (RequestForgottenPasswordNavigator) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_restore_pass_instructions;
  }

  @Override protected void initComponent(View view) {
    mGoToLoginButton = (Button) view.findViewById(R.id.btnGoToLogin);
    mTxtEmail = (TextView) view.findViewById(R.id.txt_vw_blocked_acc_email);
    mTxtEmail.setText(getEmail());
  }

  @Override protected void setListeners() {
    mGoToLoginButton.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.goToLoginPage();
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_SSO_PWD_RECOVERY_SENT,
            TrackerConstants.ACTION_PWD_RECOVERY, TrackerConstants.LABEL_GO_TO_LOGIN);
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    mGoToLoginButton.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_LOGIN_BUTTON).toString());
  }

  private String getEmail() {
    Bundle args = this.getArguments();
    String email = "";
    if (args != null) {
      email = args.getString(USER_EMAIL);
    }
    return email;
  }
}
