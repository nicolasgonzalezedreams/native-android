package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

import com.odigeo.interactors.ValidateCreditCardChecksum;

public class CreditCardValidator extends BaseValidator implements FieldValidator {

  private static final String REGEX_CREDITCARD = "[0-9]{12,19}";

  public CreditCardValidator() {
    super();
  }

  @Override public boolean validateField(String fieldInformation) {
    String creditCardWithoutSpaces = fieldInformation.replaceAll("\\D", "");
    return (checkField(creditCardWithoutSpaces, REGEX_CREDITCARD)
        && (ValidateCreditCardChecksum.usingLuhn(creditCardWithoutSpaces)));
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
