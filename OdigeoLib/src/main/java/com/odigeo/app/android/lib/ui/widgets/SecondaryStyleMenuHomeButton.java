package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;
import com.odigeo.app.android.lib.consts.HomeOptionsButtons;

/**
 * Created by Irving on 25/08/2014.
 */
public class SecondaryStyleMenuHomeButton extends ButtonMenuHomeWidget {
  public SecondaryStyleMenuHomeButton(Context context, HomeOptionsButtons optionsButton) {
    super(context, R.layout.layout_home_menu_button_style2, optionsButton);
    Fonts fonts = Configuration.getInstance().getFonts();
    super.titleTextView.setTypeface(fonts.getRegular());
  }

  public SecondaryStyleMenuHomeButton(Context context, AttributeSet attrs) {
    super(context, attrs, R.layout.layout_home_menu_button_style2);
    Fonts fonts = Configuration.getInstance().getFonts();
    super.titleTextView.setTypeface(fonts.getRegular());
  }
}