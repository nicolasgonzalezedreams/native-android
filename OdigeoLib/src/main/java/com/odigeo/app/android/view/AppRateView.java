package com.odigeo.app.android.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.navigator.AppRateNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.presenter.AppRatePresenter;
import com.odigeo.presenter.contracts.views.AppRateViewInterface;

import static com.odigeo.app.android.lib.utils.ViewUtils.findById;

public class AppRateView extends BaseView<AppRatePresenter> implements AppRateViewInterface {

  private TextView appRateTitleTV;
  private TextView appRateSubtitleTV;
  private Button likeItBtn;
  private Button couldBeBetterBtn;

  public static AppRateView newInstance() {
    Bundle args = new Bundle();
    AppRateView fragment = new AppRateView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override protected AppRatePresenter getPresenter() {
    return dependencyInjector.provideAppRatePresenter(this, (AppRateNavigator) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_apprate;
  }

  @Override protected void initComponent(View view) {
    appRateTitleTV = findById(view, R.id.tv_apprate_title);
    appRateSubtitleTV = findById(view, R.id.tv_apprate_subtitle);
    likeItBtn = findById(view, R.id.btnLikeIt);
    couldBeBetterBtn = findById(view, R.id.btnCouldBeBetter);
  }

  @Override protected void setListeners() {
    likeItBtn.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.navigateToThanksScreen();
        mTracker.trackYourAppExperienceLikeItButton();
      }
    });
    couldBeBetterBtn.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.navigateToHelpImproveScreen();
        mTracker.trackYourAppExperienceNotLikeItButton();
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    appRateTitleTV.setText(
        localizables.getString(OneCMSKeys.ABOUTOPTIONSMODULE_ABOUT_OPTION_LEAVEFEEDBACK,
            dependencyInjector.provideMarketProvider().getBrand()));
    String title = localizables.getString(OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_TITLE);
    String subtitle = localizables.getString(OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_SUBTITLE);
    appRateSubtitleTV.setText(title + ". " + subtitle);
    likeItBtn.setText(localizables.getString(OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_BUTTON_GOOD));
    couldBeBetterBtn.setText(
        localizables.getString(OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_BUTTON_BAD));
  }
}
