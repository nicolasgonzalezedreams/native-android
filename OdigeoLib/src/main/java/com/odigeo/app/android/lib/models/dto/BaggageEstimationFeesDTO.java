package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class BaggageEstimationFeesDTO implements Serializable {

  protected BaggageConditionsDTO baggageConditions;
  protected int id;

  /**
   * Gets the value of the baggageConditionsList property.
   *
   * @return possible object is
   * {@link BaggageConditions }
   */
  public BaggageConditionsDTO getBaggageConditions() {
    return baggageConditions;
  }

  /**
   * Sets the value of the baggageConditionsList property.
   *
   * @param value allowed object is
   * {@link BaggageConditions }
   */
  public void setBaggageConditions(BaggageConditionsDTO value) {
    this.baggageConditions = value;
  }

  /**
   * Gets the value of the id property.
   */
  public int getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   */
  public void setId(int value) {
    this.id = value;
  }
}
