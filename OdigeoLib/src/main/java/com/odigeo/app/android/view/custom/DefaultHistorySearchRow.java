package com.odigeo.app.android.view.custom;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.VisibleForTesting;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.interfaces.HistorySearchRowListener;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.data.entity.geo.City;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class DefaultHistorySearchRow extends RelativeLayout {

  private static final int POINT_FONT_SIZE = 20;
  public Context context;
  public SearchOptions searchOptions;
  public TextView mRowCities;
  public TextView datesTextView;
  private TextView noPassengers;
  private ImageView iconPassengers;
  private ImageView iconDestination;
  private SimpleDateFormat dateFormat;
  private HistorySearchRowListener listener;
  private OdigeoImageLoader imageLoader;

  public DefaultHistorySearchRow(Context context, SearchOptions search,
      HistorySearchRowListener listener) {
    super(context);

    this.context = context;
    this.searchOptions = search;
    this.listener = listener;
    init();
  }

  public DefaultHistorySearchRow(Context context, AttributeSet attributeSet) {
    super(context, attributeSet);
    init();
  }

  private void init() {
    inflateLayout();
    initViews();
    setListeners();

    if (hasSearchOptions()) {
      drawHistorySearch();
    }
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.layout_history_search_row, this);
    mRowCities = (TextView) findViewById(R.id.layout_cities_history_search);
    datesTextView = (TextView) findViewById(R.id.dates_history_search);
    noPassengers = (TextView) findViewById(R.id.number_passengers_row_history_search);
    iconPassengers = (ImageView) findViewById(R.id.icon_passenger_row_history_search);
    iconDestination = (ImageView) findViewById(R.id.icon_destination);
  }

  private void initViews() {
    dateFormat =
        OdigeoDateUtils.getGmtDateFormat(getContext().getString(R.string.templates__time4));
    imageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      datesTextView.setTextColor(getResources().getColor(R.color.basic_middle, context.getTheme()));
    } else {
      datesTextView.setTextColor(getResources().getColor(R.color.basic_middle));
    }

    if (searchOptions.getFlightSegments() != null && searchOptions.getFlightSegments().size() > 0) {
      FlightSegment flightSegment = getCurrentFlightSegment();
      loadCardImage(flightSegment);
    }
  }

  private void setListeners() {
    setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        if (listener != null) {
          listener.onClickRowSearch(searchOptions);
        }
      }
    });
  }

  private void loadCardImage(FlightSegment segment) {
    if (segment != null) {
      String imageUrl = ViewUtils.getSearchImage(context, segment.getArrivalCity().getIataCode());
      imageLoader.loadCircleTransformation(iconDestination, imageUrl,
          R.drawable.latest_search_placeholder);
    }
  }

  protected void drawNumberOfPassengers(int totalPassengers) {
    noPassengers.setText(String.valueOf(totalPassengers));
    if (totalPassengers == 1) {
      iconPassengers.setImageResource(R.drawable.search_1pax_h);
    } else {
      iconPassengers.setImageResource(R.drawable.search_multi_pax_h);
    }
  }

  protected void drawDate(Date date) {
    if (date != null) {
      String dateString = datesTextView.getText().toString();
      if (!dateString.isEmpty()) {
        datesTextView.append(" " + getResources().getString(R.string.format_added_date_aux) + " ");
      }
      datesTextView.append(dateFormat.format(date));
    }
  }

  protected ImageSpan getArrowImageSpan(boolean isRound) {
    return new ImageSpan(getArrowDrawable(isRound), DynamicDrawableSpan.ALIGN_BASELINE);
  }

  protected String getCityName(City city) {
    return city.getCityName() == null ? city.getIataCode() : city.getCityName();
  }

  private Drawable getArrowDrawable(boolean isRound) {
    Drawable drawable;

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      drawable = getResources().getDrawable(
          isRound ? R.drawable.search_dobleflecha_h : R.drawable.flecha_history,
          context.getTheme());
    } else {
      drawable = getResources().getDrawable(
          isRound ? R.drawable.search_dobleflecha_h : R.drawable.flecha_history);
    }

    drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());

    return drawable;
  }

  private boolean hasSearchOptions() {
    return searchOptions != null
        && searchOptions.getFlightSegments() != null
        && !searchOptions.getFlightSegments().isEmpty();
  }

  @VisibleForTesting public TextView getDatesTextView() {
    return datesTextView;
  }

  @VisibleForTesting public TextView getNoPassengers() {
    return noPassengers;
  }

  public abstract void drawHistorySearch();

  public abstract FlightSegment getCurrentFlightSegment();
}
