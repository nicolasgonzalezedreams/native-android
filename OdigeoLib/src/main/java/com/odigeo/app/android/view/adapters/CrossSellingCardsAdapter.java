package com.odigeo.app.android.view.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.view.adapters.holders.CrossSellingCardViewHolder;
import com.odigeo.presenter.model.CrossSellingCard;
import com.odigeo.presenter.model.CrossSellingType;
import java.util.Collection;
import java.util.List;

public class CrossSellingCardsAdapter extends RecyclerView.Adapter<CrossSellingCardViewHolder> {

  private final List<CrossSellingCard> xSellCards;
  private final OnCrossSellItemSelected onCrossSellItemSelected;
  private int guideCardPosition = -1;

  public void showGuideDownloading() {
    notifyItemChanged(guideCardPosition, new ProgressPayload(false));
  }

  public void hideGuideDownloading() {
    notifyItemChanged(guideCardPosition, new ProgressPayload(true));
  }

  public interface OnCrossSellItemSelected {
    void onCrossSellingItemSelected(CrossSellingCard card);
  }

  public CrossSellingCardsAdapter(Collection<CrossSellingCard> cards,
      OnCrossSellItemSelected onCrossSellItemSelected) {
    this.xSellCards = (List<CrossSellingCard>) cards;
    this.onCrossSellItemSelected = onCrossSellItemSelected;
  }

  @Override public CrossSellingCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.layout_xsell_card_item, parent, false);
    return new CrossSellingCardViewHolder(itemView, onCrossSellItemSelected);
  }

  @Override public void onBindViewHolder(CrossSellingCardViewHolder holder, int position) {
    holder.bind(position, getItem(position));
  }

  @Override public void onBindViewHolder(CrossSellingCardViewHolder holder, int position,
      List<Object> payloads) {

    if (getItemViewType(position) == CrossSellingType.TYPE_GUIDE) {
      holder.bind(position, getItem(position), payloads);
    } else {
      onBindViewHolder(holder, position);
    }
  }

  @Override public int getItemCount() {
    return xSellCards.size();
  }

  private CrossSellingCard getItem(int position) {
    return xSellCards.get(position);
  }

  @Override public int getItemViewType(int position) {
    CrossSellingCard card = getItem(position);

    if (card.getType() == CrossSellingType.TYPE_GUIDE) {
      guideCardPosition = position;
    }

    return card.getType();
  }

  public static final class ProgressPayload {
    public final boolean finished;

    ProgressPayload(boolean finished) {
      this.finished = finished;
    }
  }
}
