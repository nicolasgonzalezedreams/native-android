package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

public class NationalIdCardValidator extends BaseValidator implements FieldValidator {

  private static final String REGEX_NATIONAL_ID_CARD = "[0-9A-Za-z]{1,20}";

  public NationalIdCardValidator() {
    super();
  }

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_NATIONAL_ID_CARD);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
