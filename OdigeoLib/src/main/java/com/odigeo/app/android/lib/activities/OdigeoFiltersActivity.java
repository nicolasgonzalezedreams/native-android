package com.odigeo.app.android.lib.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.fragments.OdigeoFilterAirlinesFragment;
import com.odigeo.app.android.lib.fragments.OdigeoFilterAirportsFragment;
import com.odigeo.app.android.lib.fragments.OdigeoFilterStopsFragment;
import com.odigeo.app.android.lib.fragments.OdigeoFilterTimesFragment;
import com.odigeo.app.android.lib.interfaces.IFilterTabs;
import com.odigeo.app.android.lib.models.Airports;
import com.odigeo.app.android.lib.models.FilterStopModel;
import com.odigeo.app.android.lib.models.FilterTimeModel;
import com.odigeo.app.android.lib.models.FiltersSelected;
import com.odigeo.app.android.lib.models.SegmentGroup;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;
import com.odigeo.app.android.lib.models.dto.SectionDTO;
import com.odigeo.app.android.lib.ui.widgets.FilterTabsButtonWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Irving Lóp on 14/10/2014.
 */
public abstract class OdigeoFiltersActivity extends OdigeoBackgroundAnimatedActivity
    implements IFilterTabs {

  public static final String TAG_TIME_FRAGMENT = "timeFrag";
  public static final String TAG_STOPS_FRAGMENT = "stopsFrag";
  public static final String TAG_AIRPORTS_FRAGMENT = "portsFrag";
  public static final String TAG_AIRLINES_FRAGMENT = "linesFrag";

  public static final int SECONDS = 60;

  private OdigeoFilterTimesFragment timeFragment;
  private OdigeoFilterStopsFragment stopsFragment;
  private OdigeoFilterAirportsFragment airportsFragment;
  private OdigeoFilterAirlinesFragment airlinesFragment;

  private SearchOptions searchOptions;
  private List<FareItineraryDTO> itineraryResults;
  private List<FareItineraryDTO> itineraryResultsFiltered;
  private List<CarrierDTO> carriers;
  private List<Airports> listAirports;

  private int currentlyFragment;

  private FiltersSelected filtersSelected;

  private OdigeoSession odigeoSession;

  @Override public final String getActivityTitle() {
    return LocalizablesFacade.getString(this, "filtersviewcontroller_title").toString();
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    OdigeoApp odigeoApp = (OdigeoApp) getApplication();
    odigeoSession = odigeoApp.getOdigeoSession();

    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {

      setContentView(R.layout.activity_filters);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);

      currentlyFragment = Constants.ID_TIME_FRAGMENT;
      searchOptions = (SearchOptions) getIntent().getSerializableExtra(
          Constants.INTENT_SEARCH_OPTIONS_TO_FILTERS);

      carriers =
          (List<CarrierDTO>) getIntent().getSerializableExtra(Constants.INTENT_CARRIERS_TO_FILTER);

      filtersSelected =
          (FiltersSelected) getIntent().getSerializableExtra(Constants.EXTRA_FILTERS_SELECTED);

      itineraryResults = odigeoSession.getItineraryResults();

      setListAirports(
          (List<Airports>) getIntent().getSerializableExtra(Constants.EXTRA_AIRPORTS_BY_CITY));

      if (savedInstanceState == null) {
        getFragmentManager().beginTransaction()
            .add(R.id.frame_container_filters, getCurrentlyFragment(), getCurrentlyTag())
            .commit();
      }

      ((FilterTabsButtonWidget) findViewById(R.id.filterTabsButtom_filters)).setListener(this);
    }
  }

  /**
   * This method initializes the filters' data
   */
  private void setFiltersData() {
    itineraryResultsFiltered = new ArrayList<FareItineraryDTO>();

    if (filtersSelected == null) {
      filtersSelected = new FiltersSelected();
    }

    if (airlinesFragment != null) {
      getFiltersSelected().setCarriers(airlinesFragment.getSelectedCarriers());
    }

    if (timeFragment != null) {
      getFiltersSelected().setTimes(timeFragment.getFilterModels());
    }

    if (stopsFragment != null) {
      getFiltersSelected().setStops(stopsFragment.getFilterStopModels());
    }

    if (airportsFragment != null) {
      getFiltersSelected().setAirports(airportsFragment.getAirportsSelected());
    }
  }

  public final void filterList() {

    setFiltersData();

    for (FareItineraryDTO itineraryResult : itineraryResults) {
      FareItineraryDTO newItineraryResult = null;

      for (SegmentGroup segmentGroup : itineraryResult.getSegmentGroups()) {
        SegmentGroup newSegmentGroup = null;

        for (SegmentWrapper segmentWrapper : segmentGroup.getSegmentWrappers()) {

          boolean isSelectable = filter(segmentGroup, segmentWrapper);

          if (isSelectable) {
            if (newItineraryResult == null) {
              newItineraryResult = new FareItineraryDTO();
              newItineraryResult.setCollectionMethodFeesObject(
                  itineraryResult.getCollectionMethodFeesObject());
              newItineraryResult.setBaggageFees(itineraryResult.getBaggageFees());
              newItineraryResult.setCollectionMethodFees(itineraryResult.getCollectionMethodFees());
              newItineraryResult.setItinerary(itineraryResult.getItinerary());
              newItineraryResult.setKey(itineraryResult.getKey());
              newItineraryResult.setPrice(itineraryResult.getPrice());
              newItineraryResult.setResident(itineraryResult.getResident());
              newItineraryResult.setSegmentGroups(new ArrayList<SegmentGroup>());
            }

            if (newSegmentGroup == null) {
              newSegmentGroup = new SegmentGroup();
              newSegmentGroup.setPrice(segmentGroup.getPrice());
              newSegmentGroup.setCarrier(segmentGroup.getCarrier());
              newSegmentGroup.setItineraryResultParent(newItineraryResult);
              newSegmentGroup.setSegmentWrappers(new ArrayList<SegmentWrapper>());
              newSegmentGroup.setSegmentGroupIndex(segmentGroup.getSegmentGroupIndex());
              newSegmentGroup.setBaggageIncluded(segmentGroup.getBaggageIncluded());

              newItineraryResult.getSegmentGroups().add(newSegmentGroup);
            }

            newSegmentGroup.getSegmentWrappers().add(segmentWrapper);
            newItineraryResult.getCarriers().add(segmentWrapper.getCarrier());
            newItineraryResult.setMembershipPerks(itineraryResult.getMembershipPerks());
          }
        }
      }

      //If the itinerary resulted has the number of segment groups should it has
      addResult(newItineraryResult, itineraryResult);
    }
  }

  /**
   * This method filters by carrier, time, stops and airport
   *
   * @param segmentGroup Segment group from the itinerary result being filtered
   * @param segmentWrapper Segment wrapper from the segment group
   * @return Returns true if the result is selectable
   */
  private boolean filter(SegmentGroup segmentGroup, SegmentWrapper segmentWrapper) {
    boolean isSelectable = true;

    //filter by carrier
    if (getFiltersSelected().getCarriers() != null && !getFiltersSelected().getCarriers()
        .contains(segmentWrapper.getCarrier())) {
      isSelectable = false;
    }

    //Filter by Times
    isSelectable = filterByTimes(isSelectable, segmentWrapper, segmentGroup);

    //Filter by Stops
    isSelectable = filterByStops(isSelectable, segmentWrapper, segmentGroup);

    //Filter by airport
    isSelectable = filterByAirport(isSelectable, segmentWrapper);

    return isSelectable;
  }

  /**
   * This method filters the result by times
   *
   * @param isSelectable If the result is selectable
   * @param segmentWrapper Segment wrapper with the results
   * @param segmentGroup Segment group from the itinerary result being filtered
   * @return Returns true if the result is still selectable
   */
  private boolean filterByTimes(boolean isSelectable, SegmentWrapper segmentWrapper,
      SegmentGroup segmentGroup) {
    if (isSelectable && getFiltersSelected().getTimes() != null && !isInTime(segmentWrapper,
        getFiltersSelected().getTimes().get(segmentGroup.getSegmentGroupIndex()))) {
      return false;
    }
    return isSelectable;
  }

  /**
   * This method filters the result by stops
   *
   * @param isSelectable If the result is selectable
   * @param segmentWrapper Segment wrapper with the results
   * @param segmentGroup Segment group from the itinerary result being filtered
   * @return Returns true if the result is still selectable
   */
  private boolean filterByStops(boolean isSelectable, SegmentWrapper segmentWrapper,
      SegmentGroup segmentGroup) {
    if (isSelectable && getFiltersSelected().getStops() != null && !isInScale(segmentWrapper,
        getFiltersSelected().getStops().get(segmentGroup.getSegmentGroupIndex()))) {
      return false;
    }
    return isSelectable;
  }

  /**
   * This method filters the group by airport
   *
   * @param isSelectable If the result is selectable
   * @param segmentWrapper Segment wrapper with the results
   * @return Returns true if the result is still selectable
   */
  private boolean filterByAirport(boolean isSelectable, SegmentWrapper segmentWrapper) {
    if (isSelectable && getFiltersSelected().getAirports() != null) {
      int sectionsWithAirports = 0;

      for (SectionDTO section : segmentWrapper.getSectionsObjects()) {
        if (getFiltersSelected().getAirports().contains(section.getLocationFrom())
            && getFiltersSelected().getAirports().contains(section.getLocationTo())) {
          sectionsWithAirports++;
        }
      }

      if (segmentWrapper.getSectionsObjects().size() != sectionsWithAirports) {
        return false;
      }
    }
    return isSelectable;
  }

  /**
   * This method adds a result to the filtered list
   *
   * @param newItineraryResult new result to add
   * @param itineraryResult results
   */
  private void addResult(FareItineraryDTO newItineraryResult, FareItineraryDTO itineraryResult) {
    if (newItineraryResult != null
        && newItineraryResult.getSegmentGroups() != null
        && newItineraryResult.getSegmentGroups().size() == itineraryResult.getSegmentGroups()
        .size()) {
      itineraryResultsFiltered.add(newItineraryResult);
    }
  }

  /**
   * This method checks if the scales count is valid, given the flight model
   *
   * @param segmentWrapper Segment wrapper from the segment group
   * @param stopModel Stop model to analyze
   * @return Returns true if the stop model is valid
   */
  private boolean validScaleCount(SegmentWrapper segmentWrapper, FilterStopModel stopModel) {
    boolean isValid = false;

    if (stopModel.isDirectFlight() && segmentWrapper.getSectionsObjects().size() == 1) {
      isValid = true;
    }

    if (stopModel.isOneScale() && segmentWrapper.getSectionsObjects().size() == 2) {
      isValid = true;
    }

    if (stopModel.isPlusTwoScales() && segmentWrapper.getSectionsObjects().size() > 2) {
      isValid = true;
    }

    return isValid;
  }

  public final boolean isInScale(SegmentWrapper segmentWrapper, FilterStopModel stopModel) {
    boolean isValid = validScaleCount(segmentWrapper, stopModel);

    for (int flight = 0; flight < segmentWrapper.getSectionsObjects().size() - 1; flight++) {
      SectionDTO section = segmentWrapper.getSectionsObjects().get(flight);
      long timeScale = segmentWrapper.getSectionsObjects().get(flight + 1).getDepartureDate()
          - section.getArrivalDate();
      Calendar calendar = Calendar.getInstance();
      calendar.setTimeInMillis(timeScale);
      calendar.setTimeZone(TimeZone.getTimeZone("GMT"));
      int hour = calendar.get(Calendar.HOUR_OF_DAY);
      int minutes = calendar.get(Calendar.MINUTE);
      if (hour > stopModel.getScaleMaxDuration() || (hour == stopModel.getScaleMaxDuration()
          && minutes > 0)) {
        isValid = false;
        break;
      }
    }

    return isValid;
  }

  public boolean isInTime(SegmentWrapper segmentWrapper, FilterTimeModel timeModel) {
    Date departureDate =
        OdigeoDateUtils.createDate(segmentWrapper.getSectionsObjects().get(0).getDepartureDate());
    Date arrivalDate = OdigeoDateUtils.createDate(segmentWrapper.getSectionsObjects()
        .get(segmentWrapper.getSectionsObjects().size() - 1)
        .getArrivalDate());

    boolean isValid = true;

    if (timeModel.getDepartureMinDateSelected().compareTo(departureDate) > 0
        || timeModel.getDepartureMaxDateSelected().compareTo(departureDate) < 0) {
      isValid = false;
    }

    if (timeModel.getArrivalMinDateSelected().compareTo(arrivalDate) > 0
        || timeModel.getArrivalMaxDateSelected().compareTo(arrivalDate) < 0) {
      isValid = false;
    }

    //Calculate travel duration in hours
    long durationInHours = 0;
    if (segmentWrapper.getSegment().getDuration() > 0) {
      durationInHours = segmentWrapper.getSegment().getDuration() / SECONDS;
    }

    if (durationInHours > timeModel.getMaxFlightHoursSelected()) {
      isValid = false;
    }

    return isValid;
  }

  @Override public final boolean onClickFilterTab(int newIndex) {
    filterList();
    odigeoSession.setItineraryResultsFiltered(itineraryResultsFiltered);
    if (isFilterValid(currentlyFragment)) {
      updateFragment(newIndex);
      return true;
    } else {
      return false;
    }
  }

  private void updateFragment(int index) {
    currentlyFragment = index;
    FragmentTransaction transaction = getFragmentManager().beginTransaction();
    transaction.replace(R.id.frame_container_filters, getCurrentlyFragment(), getCurrentlyTag());
    transaction.commit();
  }

  private Fragment getCurrentlyFragment() {
    Fragment fragment = getFragmentManager().findFragmentByTag(getCurrentlyTag());
    if (fragment == null) {
      switch (currentlyFragment) {
        case Constants.ID_TIME_FRAGMENT:
          timeFragment = new OdigeoFilterTimesFragment();
          timeFragment.setRetainInstance(true);
          return timeFragment;
        case Constants.ID_STOPS_FRAGMENT:
          stopsFragment = new OdigeoFilterStopsFragment();
          stopsFragment.setRetainInstance(true);
          return stopsFragment;
        case Constants.ID_AIRPORTS_FRAGMENT:
          airportsFragment = new OdigeoFilterAirportsFragment();
          airportsFragment.setRetainInstance(true);
          return airportsFragment;
        case Constants.ID_AIRLINES_FRAGMENT:
          airlinesFragment = new OdigeoFilterAirlinesFragment();
          airlinesFragment.setRetainInstance(true);
          return airlinesFragment;
        default:
          break;
      }
    }
    return fragment;
  }

  private String getCurrentlyTag() {
    switch (currentlyFragment) {
      case Constants.ID_TIME_FRAGMENT:
        return TAG_TIME_FRAGMENT;
      case Constants.ID_STOPS_FRAGMENT:
        return TAG_STOPS_FRAGMENT;
      case Constants.ID_AIRPORTS_FRAGMENT:
        return TAG_AIRPORTS_FRAGMENT;
      case Constants.ID_AIRLINES_FRAGMENT:
        return TAG_AIRLINES_FRAGMENT;
      default:
        break;
    }
    return "unknow";
  }

  @Override public final boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_filters_activity, menu);
    MenuItem item = menu.findItem(R.id.menu_item_filters_apply);
    item.setTitle(
        LocalizablesFacade.getString(this, "filtersviewcontroller_buttonapply").toString());
    return super.onCreateOptionsMenu(menu);
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == android.R.id.home) {
      postGATrackingEventGoBack();

      finish();
    } else if (id == R.id.menu_item_filters_apply) {
      if (isFilterValid(currentlyFragment)) {
        // GATracker - Flight filters page - event 2
        postGATrackingEventApplyFilters();
        applyFilter();
      }
    }

    return super.onOptionsItemSelected(item);
  }

  public final SearchOptions getSearchOptions() {
    return searchOptions;
  }

  public final List<FareItineraryDTO> getItineraryResults() {
    return itineraryResults;
  }

  public final List<CarrierDTO> getCarriers() {
    return carriers;
  }

  private void applyFilter() {
    filterList();

    odigeoSession.setItineraryResultsFiltered(itineraryResultsFiltered);

    Intent intent = new Intent();
    intent.putExtra(Constants.EXTRA_FILTERS_SELECTED, getFiltersSelected());
    setResult(RESULT_OK, intent);
    finish();
  }

  public final FiltersSelected getFiltersSelected() {
    return filtersSelected;
  }

  public final List<Airports> getListAirports() {
    return listAirports;
  }

  public final void setListAirports(List<Airports> listAirports) {
    this.listAirports = listAirports;
  }

  public boolean validateAirportFragment() {
    if (airportsFragment != null && !airportsFragment.isAirportsSelectedValid()) {
      Toast.makeText(this,
          LocalizablesFacade.getString(this, "airportfilterviewcontroller_errormessage"),
          Toast.LENGTH_LONG).show();
      return false;
    }
    return true;
  }

  public boolean validateAirlinesFragment() {
    if (airlinesFragment != null && airlinesFragment.getSelectedCarriers().isEmpty()) {
      Toast.makeText(this,
          LocalizablesFacade.getString(this, "airlinefilterviewcontroller_errormessage"),
          Toast.LENGTH_LONG).show();
      return false;
    }

    return true;
  }

  public boolean isFilterValid(int fragment) {
    if (fragment == Constants.ID_AIRLINES_FRAGMENT) {
      return validateAirlinesFragment();
    } else if (fragment == Constants.ID_AIRPORTS_FRAGMENT) {
      return validateAirportFragment();
    } else {
      return true;
    }
  }

  private void postGATrackingEventApplyFilters() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_FILTERS,
            GAnalyticsNames.ACTION_RESULTS_FILTERS, GAnalyticsNames.LABEL_FILTERS_APPLY));
  }

  private void postGATrackingEventGoBack() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_FILTERS,
            GAnalyticsNames.ACTION_NAVIGATION, GAnalyticsNames.LABEL_FILTERS_GO_BACK));
  }

  public abstract void onGAEventTriggered(GATrackingEvent event);
}
