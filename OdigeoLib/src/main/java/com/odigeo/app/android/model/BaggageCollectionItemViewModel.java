package com.odigeo.app.android.model;

public class BaggageCollectionItemViewModel {

  private String segmentDirection;
  private String carrierCode;
  private String departure;
  private String arrival;
  private boolean isRoundTrip;

  public BaggageCollectionItemViewModel(String segmentDirection, String carrierCode,
      String departure, String arrival, boolean isRoundTrip) {
    this.segmentDirection = segmentDirection;
    this.carrierCode = carrierCode;
    this.departure = departure;
    this.arrival = arrival;
    this.isRoundTrip = isRoundTrip;
  }

  public String getSegmentDirection() {
    return segmentDirection;
  }

  public String getCarrierCode() {
    return carrierCode;
  }

  public String getDeparture() {
    return departure;
  }

  public String getArrival() {
    return arrival;
  }

  public boolean isRoundTrip() {
    return isRoundTrip;
  }
}