package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class BookingFeeSearchResultDTO {

  protected DailyTimeRangeDTO feeZeroTimeRange;
  protected BigDecimal maximumFixedFeeAmount;
  protected BigDecimal maximumVariableFeeAmount;
  protected Boolean feeZero;

  /**
   * Gets the value of the feeZeroTimeRange property.
   *
   * @return possible object is
   * {@link DailyTimeRange }
   */
  public DailyTimeRangeDTO getFeeZeroTimeRange() {
    return feeZeroTimeRange;
  }

  /**
   * Sets the value of the feeZeroTimeRange property.
   *
   * @param value allowed object is
   * {@link DailyTimeRange }
   */
  public void setFeeZeroTimeRange(DailyTimeRangeDTO value) {
    this.feeZeroTimeRange = value;
  }

  /**
   * Gets the value of the maximumFixedFeeAmount property.
   *
   * @return possible object is
   * {@link BigDecimal }
   */
  public BigDecimal getMaximumFixedFeeAmount() {
    return maximumFixedFeeAmount;
  }

  /**
   * Sets the value of the maximumFixedFeeAmount property.
   *
   * @param value allowed object is
   * {@link BigDecimal }
   */
  public void setMaximumFixedFeeAmount(BigDecimal value) {
    this.maximumFixedFeeAmount = value;
  }

  /**
   * Gets the value of the maximumVariableFeeAmount property.
   *
   * @return possible object is
   * {@link BigDecimal }
   */
  public BigDecimal getMaximumVariableFeeAmount() {
    return maximumVariableFeeAmount;
  }

  /**
   * Sets the value of the maximumVariableFeeAmount property.
   *
   * @param value allowed object is
   * {@link BigDecimal }
   */
  public void setMaximumVariableFeeAmount(BigDecimal value) {
    this.maximumVariableFeeAmount = value;
  }

  /**
   * Gets the value of the feeZero property.
   *
   * @return possible object is
   * {@link Boolean }
   */
  public Boolean isFeeZero() {
    return feeZero;
  }

  /**
   * Sets the value of the feeZero property.
   *
   * @param value allowed object is
   * {@link Boolean }
   */
  public void setFeeZero(Boolean value) {
    this.feeZero = value;
  }
}
