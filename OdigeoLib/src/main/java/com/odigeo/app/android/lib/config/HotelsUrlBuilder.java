package com.odigeo.app.android.lib.config;

import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.utils.BrandUtils;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.interactors.provider.MarketProviderInterface;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * An URL with parameters is built to be used in cross selling section.
 * <p/>
 * Example URL: http://mobile-hotels.edreams.com/searchresults.html?aid=381292;iata=AMS;iata_orr=1;checkin_monthday=28;checkin_year_month=2015-12;checkout_monthday=30;checkout_year_month=2015-12;selected_currency=EUR;lang=es;label=edr-mobile-es-appandroid
 *
 * @author cristian.fanjul
 * @since 25/02/2015
 */
public class HotelsUrlBuilder extends BaseUrlBuilder {

  // Parameters
  public static final String APPLICATION_ID = "aid";
  public static final String LABEL = "label";
  public static final String LANGUAGE = "lang";
  public static final String CURRENCY = "selected_currency";
  public static final String IATA = "iata";
  public static final String IATA_ORR = "iata_orr";
  public static final String CHECK_IN_DAY = "checkin_monthday";
  public static final String CHECK_IN_YEAR_MONTH = "checkin_year_month";
  public static final String CHECK_OUT_DAY = "checkout_monthday";
  public static final String CHECK_OUT_YEAR_MONTH = "checkout_year_month";
  public static final String NO_ROOMS = "no_rooms";
  public static final String GROUP_ADULTS = "group_adults";
  public static final String GROUP_CHILDREN = "group_children";

  // Filter or home page
  public static final String DO_AVAILABILITY_CHECK = "do_availability_check";
  public static final String SS = "ss"; // Ex: "paris"
  public static final String SI = "si";
  public static final String OPODO_NORDICS_AID = "1262771";
  public static final String OPODO_NORDICS_LABEL = "opo-link-%s-%s-conf-appand-of";
  private static final int DO_AVAILABILITY_CHECK_VALUE = 1;
  private static final String SI_VALUE = "ci";
  private static final Map<String, String> LANGUAGE_EXCEPTIONS_MAP =
      new HashMap<String, String>() {{
        put("en", "en-gb");
      }};

  private static final Map<String, String> LABEL_EXCEPTIONS_MAP = new HashMap<String, String>() {{
    put("da", "dk");
    put("de", "de-com");
  }};

  private String screenLabel;

  private MarketProviderInterface marketProvider;

  /**
   * Default constructor where the kind of search: home page with filter or results page.
   *
   * @param isSearch Determines if the user must be redirected to the results page.
   */
  public HotelsUrlBuilder(boolean isSearch, String screenLabel,
      MarketProviderInterface marketProvider) {
    super(isSearch);
    this.screenLabel = screenLabel;
    this.marketProvider = marketProvider;
    setBasicParametersForUrl(screenLabel);
  }

  /**
   * Send to search results constructor. Parameters are taken automatically from mSearchOptions in
   * order to search a new Hotel.
   */
  public HotelsUrlBuilder(SearchOptions searchOptions, String screenLabel,
      MarketProviderInterface marketProvider) {
    super(true);
    this.marketProvider = marketProvider;
    setBasicParametersForUrl(screenLabel);
    setParametersFromSearchOptions(searchOptions);
  }

  /**
   * Send to search results constructor. Parameters are taken automatically from mSearchOptions in
   * order to search a new Hotel.
   */
  public HotelsUrlBuilder(Booking searchOptions, String screenLabel,
      MarketProviderInterface marketProvider) {
    super(true);
    this.marketProvider = marketProvider;
    setBasicParametersForUrl(screenLabel);
    setParametersFromSearchOptions(searchOptions);
  }

  /**
   * Parameters are required one by one in order to search a new hotel.
   */
  public HotelsUrlBuilder(String currency, String iata, String checkInDay, String checkInYearMonth,
      String checkOutDay, String checkOutYearMonth, String screenLabel) {
    super(true);
    setBasicParametersForUrl(screenLabel);
    setCurrency(currency);
    setIata(iata);
    setCheckinDay(checkInDay);
    setCheckInYearMonth(checkInYearMonth);
    setCheckoutDay(checkOutDay);
    setCheckoutYearMonth(checkOutYearMonth);
  }

  /**
   * Parameters required to perform a search are set. This will be used to send a passenger
   * directly to the results page.
   */
  public final void setParametersFromSearchOptions(SearchOptions searchOptions) {
    setIata(searchOptions.getFirstSegment().getLastSection().getLocationTo().getIataCode());
    setIataOrr("1");

    setCheckInDateValues(
        OdigeoDateUtils.createDate(searchOptions.getFirstSegment().getArrivalDate()));
    setCheckOutDateValues(OdigeoDateUtils.createDate(searchOptions.getLastSegment().getDate()));

    setRoomsForAdultsNumber(searchOptions.getNumberOfAdults());

    setGroupInfo(searchOptions.getNumberOfAdults(), searchOptions.getNumberOfKids());
  }

  /**
   * Parameters required to perform a search are set. This will be used to send a passenger
   * directly to the results page.
   */
  public final void setParametersFromSearchOptions(Booking searchOptions) {
    setCurrency(marketProvider.getCurrencyKey());
    setIata(searchOptions.getArrivalAirportCode());
    setIataOrr("1");

    setCheckInDateValues(OdigeoDateUtils.createDate(searchOptions.getArrivalDate(0)));
    setCheckOutDateValues(OdigeoDateUtils.createDate(searchOptions.getArrivalLastLeg()));

    setRoomsForAdultsNumber(searchOptions.getNumberOfAdults());

    setGroupInfo(searchOptions.getNumberOfAdults(), searchOptions.getNumberOfKids());
  }

  /**
   * Parameters required to send a passenger to the home screen with filters.
   */
  public final void setParametersForFilters(SearchOptions searchOptions) {
    setBasicParametersForUrl(this.screenLabel);

    setDoAvailabilityCheck(DO_AVAILABILITY_CHECK_VALUE);
    setSi(SI_VALUE);
    setSs(searchOptions.getFirstSegment().getArrivalCity().getName());

    setCheckInDateValues(
        OdigeoDateUtils.createDate(searchOptions.getFirstSegment().getArrivalDate()));
    setCheckOutDateValues(OdigeoDateUtils.createDate(searchOptions.getLastSegment().getDate()));

    setRoomsForAdultsNumber(searchOptions.getNumberOfAdults());

    setGroupInfo(searchOptions.getNumberOfAdults(), searchOptions.getNumberOfKids());
  }

  /**
   * Parameters required to send a passenger to the home screen with filters.
   */
  public final void setParametersForFilters(Booking searchOptions) {
    setBasicParametersForUrl(this.screenLabel);

    setDoAvailabilityCheck(DO_AVAILABILITY_CHECK_VALUE);
    setSi(SI_VALUE);
    setSs(searchOptions.getArrivalCityName(0));

    setCheckInDateValues(OdigeoDateUtils.createDate(searchOptions.getArrivalLastLeg()));
    setCheckOutDateValues(OdigeoDateUtils.createDate(searchOptions.getLastDepartureDate()));

    setRoomsForAdultsNumber(searchOptions.getNumberOfAdults());

    setGroupInfo(searchOptions.getNumberOfAdults(), searchOptions.getNumberOfKids());
  }

  private void setGroupInfo(int numberOfAdults, int numberOfKids) {
    if (numberOfAdults > 0) {
      setGroupAdults(numberOfAdults);
    }
    if (numberOfKids > 0) {
      setGroupChildren(numberOfKids);
    }
  }

  /**
   * Different parts of a date are taken from a date and set as a parameter for check in parts.
   */
  public final void setCheckInDateValues(Date arrivalDate) {
    // Check in dates
    String ciDay = String.valueOf(OdigeoDateUtils.getDayOfMonth(arrivalDate));
    String ciMonth = String.valueOf(OdigeoDateUtils.getMonth(arrivalDate));
    String ciYear = String.valueOf(OdigeoDateUtils.getYear(arrivalDate));

    setCheckinDay(ciDay);
    setCheckInYearMonth(ciYear + "-" + ciMonth);
  }

  /**
   * Different parts of a date are taken from a date and set as a parameter for check out parts.
   */
  public final void setCheckOutDateValues(Date arrivalDate) {
    String coDay = String.valueOf(OdigeoDateUtils.getDayOfMonth(arrivalDate));
    String coMonth = String.valueOf(OdigeoDateUtils.getMonth(arrivalDate));
    String coYear = String.valueOf(OdigeoDateUtils.getYear(arrivalDate));

    setCheckoutDay(coDay);
    setCheckoutYearMonth(coYear + "-" + coMonth);
  }

  /**
   * Parameters that are basic or don't change like aid (app id), and label are set.
   */
  private void setBasicParametersForUrl(String screenLabel) {

    String applicationId = marketProvider.getAid();
    String hotelLabel = marketProvider.getHotelsLabel();

    if (BrandUtils.isOpodoNordicsBrand(marketProvider.getBrand(), marketProvider.getMarketKey())) {
      applicationId = OPODO_NORDICS_AID;
      hotelLabel = OPODO_NORDICS_LABEL;
    }

    setApplicationId(applicationId);
    setLabel(hotelLabel, marketProvider.getMarketKey(), screenLabel);

    setLanguage(marketProvider.getLanguage());
    setCurrency(marketProvider.getCurrencyKey());
  }

  @Override protected final String getSearchResultsUrl() {
    return marketProvider.getxSellingSearchResultsHotelsUrl();
  }

  @Override protected final String getFilterUrl() {
    return marketProvider.getxSellingFilterHotelsUrl();
  }

  public final void setApplicationId(String aid) {
    data.put(APPLICATION_ID, aid);
  }

  public final void setLabel(String hotelLabel, String marketKey, String screenLabel) {

    if (shouldApplyNordicsException(marketKey, LABEL_EXCEPTIONS_MAP)) {
      marketKey = LABEL_EXCEPTIONS_MAP.get(marketKey);
    }

    String label = String.format(hotelLabel, marketKey, screenLabel);
    data.put(LABEL, label);
  }

  public final void setLanguage(String lang) {

    if (shouldApplyNordicsException(lang, LANGUAGE_EXCEPTIONS_MAP)) {
      lang = LANGUAGE_EXCEPTIONS_MAP.get(lang);
    }
    data.put(LANGUAGE, lang);
  }

  private boolean shouldApplyNordicsException(String value, Map<String, String> exceptionsMap) {
    return BrandUtils.isNordicMarket(marketProvider.getBrand(), marketProvider.getMarketKey())
        && exceptionsMap.containsKey(value);
  }

  public final void setCurrency(String currency) {
    data.put(CURRENCY, currency);
  }

  public final void setIata(String iata) {
    data.put(IATA, iata);
  }

  public final void setIataOrr(String iataOrr) {
    data.put(IATA_ORR, iataOrr);
  }

  public final void setCheckinDay(String checkInDayValue) {
    data.put(CHECK_IN_DAY, checkInDayValue);
  }

  public final void setCheckInYearMonth(String checkInYearMonthValue) {
    data.put(CHECK_IN_YEAR_MONTH, checkInYearMonthValue);
  }

  public final void setCheckoutDay(String checkoutDayValue) {
    data.put(CHECK_OUT_DAY, checkoutDayValue);
  }

  public final void setCheckoutYearMonth(String checkOutYearMonthValue) {
    data.put(CHECK_OUT_YEAR_MONTH, checkOutYearMonthValue);
  }

  public final void setDoAvailabilityCheck(int doAvailabilityCheck) {
    data.put(DO_AVAILABILITY_CHECK, doAvailabilityCheck);
  }

  public final void setSi(String si) {
    data.put(SI, si);
  }

  public final void setSs(String ss) {
    data.put(SS, ss);
  }

  public final void setNroRooms(int nroRooms) {
    data.put(NO_ROOMS, nroRooms);
  }

  public final void setGroupAdults(int groupAdults) {
    data.put(GROUP_ADULTS, groupAdults);
  }

  public final void setGroupChildren(int groupChildren) {
    data.put(GROUP_CHILDREN, groupChildren);
  }

  public final void setRoomsForAdultsNumber(int adultsNumber) {
    switch (adultsNumber) {
      case 1:
      case 2:
      case 3:
      case 4:
        setNroRooms(1);
        break;
      case 5:
        setNroRooms(2);
        break;
      case 6:
      case 7:
        setNroRooms(3);
        break;
      case 8:
      case 9:
        setNroRooms(4);
        break;
      default:
        setNroRooms(1);
        break;
    }
  }
}
