package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

/**
 * @author miguel
 */
public class SectionResultDTO implements Serializable {

  protected SectionDTO section;
  protected int id;

  /**
   * Gets the value of the section property.
   *
   * @return possible object is {@link SectionDTO }
   */
  public SectionDTO getSection() {
    return section;
  }

  /**
   * Sets the value of the section property.
   *
   * @param value allowed object is {@link SectionDTO }
   */
  public void setSection(SectionDTO value) {
    this.section = value;
  }

  /**
   * Gets the value of the id property.
   */
  public int getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   */
  public void setId(int value) {
    this.id = value;
  }
}
