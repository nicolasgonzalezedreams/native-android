package com.odigeo.app.android.lib.utils;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class ChamferedRectangle extends Drawable {

  private static final int TRIANGLE_SIZE = 20;

  @Override public final void draw(Canvas canvas) {

    //set the color
    Paint paint = new Paint();
    paint.setColor(Color.WHITE);
    paint.setStyle(Style.FILL);

    Rect bounds = getBounds();

    //draw the triangle
    Path polygonPath = new Path();
    polygonPath.reset();
    polygonPath.moveTo(0, 0);

    int halfWidth = bounds.right / 2;

    //triangle
    polygonPath.lineTo(halfWidth - (TRIANGLE_SIZE * 2) / 2, 0);
    polygonPath.lineTo(halfWidth, TRIANGLE_SIZE);
    polygonPath.lineTo(halfWidth + (TRIANGLE_SIZE * 2) / 2, 0);

    //continue with the rectangle
    polygonPath.lineTo(bounds.right, 0);
    polygonPath.lineTo(bounds.right, bounds.bottom);
    polygonPath.lineTo(0, bounds.bottom);
    polygonPath.lineTo(0, 0);
    polygonPath.setLastPoint(0, 0);

    canvas.drawPath(polygonPath, paint);
  }

  @Override public final int getOpacity() {
    return 0;
  }

  @Override public void setAlpha(int alpha) {
    //Nothing to do here
  }

  @Override public void setColorFilter(ColorFilter cf) {
    //Nothing to do here
  }
}
