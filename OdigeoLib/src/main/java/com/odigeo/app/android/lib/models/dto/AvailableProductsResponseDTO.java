package com.odigeo.app.android.lib.models.dto;

import java.util.List;

/**
 * @author miguel
 */
public class AvailableProductsResponseDTO extends BaseResponseDTO {

  private List<InsuranceOfferDTO> insuranceOffers;

  public AvailableProductsResponseDTO() {
  }

  /**
   *
   * @return
   */
  public List<InsuranceOfferDTO> getInsuranceOffers() {
    return insuranceOffers;
  }

  /**
   *
   * @param insuranceOffers
   */
  public void setInsuranceOffers(List<InsuranceOfferDTO> insuranceOffers) {
    this.insuranceOffers = insuranceOffers;
  }
}
