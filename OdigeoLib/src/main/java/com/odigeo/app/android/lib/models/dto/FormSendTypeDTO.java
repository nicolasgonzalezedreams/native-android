package com.odigeo.app.android.lib.models.dto;

public enum FormSendTypeDTO {

  GET, POST;

  public static FormSendTypeDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
