package com.odigeo.app.android.view.textwatchers;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.RequestValidationHandler;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.presenter.PaymentFormWidgetPresenter;
import com.odigeo.presenter.contracts.views.PaymentFormWidgetInterface;

public class CreditCardTextWatcher extends BaseTextWatcher {

  private static final int CREDIT_CARD_BLOCK_LENGTH = 4;
  private final FieldValidator fieldValidator;
  private final PaymentMethodBaseTextWatchersClient paymentMethodBaseTextWatchersClient;
  private final CollectionMethodType collectionMethodType;
  private final RequestValidationHandler requestValidationHandler;
  private final PaymentFormWidgetInterface paymentFormWidget;
  private PaymentFormWidgetPresenter paymentFormPresenter;
  private TextInputLayout tilCreditCardNumber;
  private boolean isFieldCorrect = false;

  CreditCardTextWatcher(TextInputLayout tilCreditCardNumber,
      PaymentFormWidgetInterface paymentFormWidget, PaymentFormWidgetPresenter paymentFormPresenter,
      FieldValidator fieldValidator,
      PaymentMethodBaseTextWatchersClient paymentMethodBaseTextWatchersClient,
      CollectionMethodType collectionMethodType,
      RequestValidationHandler requestValidationHandler) {

    super(tilCreditCardNumber);
    this.paymentMethodBaseTextWatchersClient = paymentMethodBaseTextWatchersClient;
    this.paymentFormPresenter = paymentFormPresenter;
    this.tilCreditCardNumber = tilCreditCardNumber;
    this.fieldValidator = fieldValidator;
    this.collectionMethodType = collectionMethodType;
    this.requestValidationHandler = requestValidationHandler;
    this.paymentFormWidget = paymentFormWidget;
  }

  @Override
  public void onTextChanged(CharSequence text, int startIndex, int beforeIndex, int length) {
    if (collectionMethodType.equals(CollectionMethodType.CREDITCARD)) {
      if (length != 0) {
        setCreditCardFormattedText(text);
      }
      paymentFormPresenter.runBinCheck(text, length);
    }
  }

  private void setCreditCardFormattedText(CharSequence text) {
    String creditCardNumberFormatted = formatCreditCardNumber(text.toString());
    if (!creditCardNumberFormatted.equals(text.toString())) {
      setStringToEditText(creditCardNumberFormatted);
    }
  }

  private String formatCreditCardNumber(String creditCardNumber) {
    String creditCardResult = "";
    String digitsCreditCard = removeWhiteSpaces(creditCardNumber);

    String[] creditCardsBlocks = splitStringInBlocks(digitsCreditCard, CREDIT_CARD_BLOCK_LENGTH);
    for (int i = 0; i < creditCardsBlocks.length; i++) {
      creditCardResult = creditCardResult.concat(creditCardsBlocks[i]);

      if (thereIsBlockOfFourDigits(i, creditCardsBlocks)) {
        creditCardResult = creditCardResult.concat(" ");
      }
    }

    return creditCardResult;
  }

  private boolean thereIsBlockOfFourDigits(int i, String[] setOfBlock) {
    return (i < setOfBlock.length - 1);
  }

  private String[] splitStringInBlocks(String s, int interval) {
    int arrayLength = (int) Math.ceil(((s.length() / (double) interval)));
    String[] result = new String[arrayLength];
    int j = 0;
    int lastIndex = result.length - 1;
    for (int i = 0; i < lastIndex; i++) {
      result[i] = s.substring(j, j + interval);
      j += interval;
    }
    result[lastIndex] = s.substring(j);
    return result;
  }

  private String removeWhiteSpaces(String creditCardNumber) {
    return creditCardNumber.replaceAll("\\s", "");
  }

  private void setStringToEditText(String creditCardNumber) {
    EditText creditCardEditText = tilCreditCardNumber.getEditText();

    if (creditCardEditText != null) {
      creditCardEditText.removeTextChangedListener(this);
      creditCardEditText.setText(creditCardNumber);
      creditCardEditText.addTextChangedListener(this);

      creditCardEditText.setSelection(creditCardNumber.length());
    }
  }

  @Override public void afterTextChanged(Editable editableText) {
    super.afterTextChanged(editableText);
    isFieldCorrect = fieldValidator.validateField(editableText.toString());

    if (paymentFormWidget.getPaymentMethodSpinnerVisibility() == View.VISIBLE) {
      String creditCardCode = paymentFormWidget.getPaymentMethodCodeDetected();
      String creditCardNumber = editableText.toString().replaceAll("\\s", "");
      isFieldCorrect = isFieldCorrect && requestValidationHandler.handleRequest(creditCardCode,
          creditCardNumber);
    }

    paymentMethodBaseTextWatchersClient.onCharacterWriteOnField(isFieldCorrect,
        tilCreditCardNumber);
  }
}
