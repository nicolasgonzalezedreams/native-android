package com.odigeo.app.android.lib.ui.widgets;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.PdfDownloader;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.shoppingCart.InsuranceShoppingItem;

/**
 * Created by emiliano.desantis on 25/09/2014.
 */
@Deprecated public class InsuranceMandatoryWidget extends RelativeLayout {

  private Activity activity;

  private InsuranceShoppingItem insurance;

  public InsuranceMandatoryWidget(Context context, AttributeSet attrs) {

    super(context, attrs);

    initialize();
  }

  public InsuranceMandatoryWidget(Activity context, InsuranceShoppingItem insurance) {
    super(context);

    this.insurance = insurance;
    this.activity = context;
    initialize();
  }

  private void initialize() {
    inflate(getContext(), R.layout.widget_insurance_mandatory, this);

    getRootView().setOnClickListener(new OnClickListener() {

      @Override public void onClick(View v) {
        if (insurance != null
            && insurance.getInsurance() != null
            && insurance.getInsurance().getConditionsUrls() != null
            && !insurance.getInsurance().getConditionsUrls().isEmpty()) {
          String urlConditions = insurance.getInsurance().getConditionsUrls().get(0).getUrl();
          // GAnalytics screen tracking - Insurances terms and conditions
          postGAScreenTrackingInsurancesTerms();
          PdfDownloader task2 = new PdfDownloader(activity);
          task2.execute(urlConditions);
        }
      }
    });

    TextView insuranceTitleTextView = (TextView) findViewById(R.id.mandatory_insurance_title);
    TextView insuranceTextTextView = (TextView) findViewById(R.id.mandatory_insurance_text);

    insuranceTitleTextView.setTypeface(Configuration.getInstance().getFonts().getRegular());
    insuranceTextTextView.setTypeface(Configuration.getInstance().getFonts().getBold());

    insuranceTitleTextView.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.MANDATORY_INSURANCE_TITLE));
    if (insurance != null && insurance.getInsurance() != null) {
      insuranceTextTextView.setText(insurance.getInsurance().getTitle());
    }
    this.setClickable(true);
  }

  private void postGAScreenTrackingInsurancesTerms() {
    BusProvider.getInstance()
        .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_INSURANCES_T_CS));
  }
}
