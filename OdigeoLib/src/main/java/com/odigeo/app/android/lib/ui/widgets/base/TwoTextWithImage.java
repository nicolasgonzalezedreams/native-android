package com.odigeo.app.android.lib.ui.widgets.base;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;

/**
 * Created by Irving Lóp on 11/10/2014.
 */
public abstract class TwoTextWithImage extends TextWithImage {
  private String secondText;
  private ColorStateList colorSecondText;

  private TextView secondTextView;

  public TwoTextWithImage(Context context) {
    super(context);
    inflateLayout();
  }

  public TwoTextWithImage(Context context, AttributeSet attrs) {
    super(context, attrs);
    inflateLayout();
    setAttributes(attrs);
  }

  private void inflateLayout() {
    secondTextView = (TextView) findViewById(getIdSecondTextView());
  }

  private void setAttributes(AttributeSet attributes) {
    TypedArray typedArray =
        getContext().obtainStyledAttributes(attributes, R.styleable.TextWithImage);
    secondText = typedArray.getString(R.styleable.TwoTextWithImage_secondText);
    colorSecondText = typedArray.getColorStateList(R.styleable.TwoTextWithImage_colorSecondText);
    typedArray.recycle();

    if (secondText != null) {
      secondTextView.setText(LocalizablesFacade.getString(getContext(), secondText));
    }
    setColorSecondText(colorSecondText);
  }

  public final void setSecondText(CharSequence text) {
    if (text != null) {
      this.secondText = text.toString();
      secondTextView.setText(text);
    }
  }

  public final void setColorSecondText(ColorStateList color) {
    if (color != null) {
      colorSecondText = color;
      secondTextView.setTextColor(colorSecondText);
    }
  }

  protected abstract int getIdSecondTextView();
}
