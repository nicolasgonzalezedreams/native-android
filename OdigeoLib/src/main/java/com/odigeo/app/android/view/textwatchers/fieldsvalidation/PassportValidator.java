package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

public class PassportValidator extends BaseValidator implements FieldValidator {

  private static final String REGEX_PASSPORT = "[A-Za-z0-9]{1,20}";

  public PassportValidator() {
    super();
  }

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_PASSPORT);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
