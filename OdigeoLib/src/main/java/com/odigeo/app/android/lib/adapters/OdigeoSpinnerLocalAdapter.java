package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.holders.BaseHolder;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.data.entity.BaseSpinnerItem;
import java.util.List;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 12/01/16
 */
public class OdigeoSpinnerLocalAdapter<T extends BaseSpinnerItem> extends BaseOdigeoAdapter<T> {

  public OdigeoSpinnerLocalAdapter(Context context, List<T> objects) {
    super(context, objects);
  }

  @Override protected int getLayout() {
    return R.layout.spinner_offline_item;
  }

  @Override protected View buildViewAndHolder(View convertView, ViewGroup parent, int layout) {
    if (convertView == null) {
      convertView = LayoutInflater.from(getContext()).inflate(layout, parent, false);
      ViewHolder holder = new ViewHolder(convertView);
      convertView.setTag(holder);
    }
    return convertView;
  }

  @Override protected void drawItem(BaseHolder baseHolder, BaseSpinnerItem item) {
    ViewHolder holder = (ViewHolder) baseHolder;
    //If has a String to show.
    if (!TextUtils.isEmpty(item.getShownText())) {
      holder.textView.setText(item.getShownText());
    } else if (!item.getShownTextKey().equals(BaseSpinnerItem.EMPTY_STRING)) {
      //If has a valid resource to show.
      holder.textView.setText(LocalizablesFacade.getString(getContext(), item.getShownTextKey()));
    } else {
      //If the item, has no text string or text resource, clean the text view
      holder.textView.setText(null);
    }

    //If has a valid image resource.
    if (item.getImageId() != BaseSpinnerItem.EMPTY_RESOURCE) {
      holder.imageView.setVisibility(View.VISIBLE);
      holder.imageView.setImageResource(item.getImageId());
    } else {
      holder.imageView.setVisibility(View.GONE);
    }
  }

  private static class ViewHolder extends BaseHolder {

    final ImageView imageView;
    final TextView textView;

    ViewHolder(View view) {
      super(view);
      textView = ((TextView) view.findViewById(android.R.id.text1));
      imageView = ((ImageView) view.findViewById(R.id.text_image));
    }
  }
}
