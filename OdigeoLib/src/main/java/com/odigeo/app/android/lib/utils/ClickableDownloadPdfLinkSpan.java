package com.odigeo.app.android.lib.utils;

import android.app.Activity;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.odigeo.app.android.lib.R;

/**
 * Created by Irving on 30/01/2015.
 */
public class ClickableDownloadPdfLinkSpan extends ClickableSpan {

  private final Activity activity;
  private final String namePDF;
  private final String link;

  public ClickableDownloadPdfLinkSpan(Activity activity, String namePDF, String link) {
    this.activity = activity;
    this.namePDF = namePDF;
    this.link = link;
  }

  @Override public final void onClick(View widget) {

    PdfDownloader pdfDownload = new PdfDownloader(activity, namePDF);
    pdfDownload.execute(link);
  }

  @Override public final void updateDrawState(TextPaint ds) {

    ds.setColor(activity.getResources().getColor(R.color.basic_middle));
    ds.setUnderlineText(true);
  }
}
