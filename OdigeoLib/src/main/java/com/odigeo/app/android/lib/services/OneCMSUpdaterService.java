package com.odigeo.app.android.lib.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.util.Log;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.utils.DeviceUtils;
import com.odigeo.data.localizable.OneCMSAlarmsController;
import com.odigeo.data.net.error.MslError;
import com.odigeo.interactors.LocalizablesInteractor;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.presenter.listeners.OnGetLocalizablesListener;

public class OneCMSUpdaterService extends Service {

  private static final String TAG = "OneCMSUpdaterService";

  private LocalizablesInteractor localizablesInteractor;
  private MarketProviderInterface marketProviderInterface;
  private OneCMSAlarmsController oneCMSAlarmsController;

  public OneCMSUpdaterService() {
    super();
    localizablesInteractor =
        AndroidDependencyInjector.getInstance().provideLocalizablesInteractor();
    marketProviderInterface = AndroidDependencyInjector.getInstance().provideMarketProvider();
    oneCMSAlarmsController =
        AndroidDependencyInjector.getInstance().provideOneCMSAlarmsController();
  }

  @Override public void onCreate() {
    super.onCreate();
  }

  @Override public int onStartCommand(Intent intent, int flags, int startId) {
    if (DeviceUtils.isGoodNetworkConnectivityAvailable(OneCMSUpdaterService.this)) {
      updateLocalizables();
    } else {
      oneCMSAlarmsController.retryOneCMSUpdate();
    }

    return START_NOT_STICKY;
  }

  @Override public void onDestroy() {
    super.onDestroy();
  }

  @Override public IBinder onBind(Intent intent) {
    return null;
  }

  private void updateLocalizables() {
    String marketKey = marketProviderInterface.getOneCMSTableForCurrentMarket();
    localizablesInteractor.getLocalizables(marketKey, new OnGetLocalizablesListener() {
      @Override public void onSuccess() {
        Log.i(TAG, "OneCMS update success");
      }

      @Override public void onError(MslError error, String message) {
        oneCMSAlarmsController.retryOneCMSUpdate();
      }
    });
  }


}
