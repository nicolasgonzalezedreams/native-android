package com.odigeo.app.android.receivers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import com.odigeo.app.android.alarms.BookingStatusAlarmManager;
import com.odigeo.app.android.services.BookingStatusSyncService;

public class BookingStatusAlarmReceiver extends WakefulBroadcastReceiver {

  @Override public void onReceive(Context context, Intent intent) {
    if (BookingStatusAlarmManager.ALARM_BOOKING_STATUS_ACTION.equals(intent.getAction())) {
      Intent newIntent = new Intent(context, BookingStatusSyncService.class);
      startWakefulService(context, newIntent);
    }
  }
}
