package com.odigeo.app.android.lib.models.dto;

import android.support.annotation.Nullable;
import com.odigeo.app.android.lib.utils.Validations;
import com.odigeo.data.entity.BaseSpinnerItem;
import java.io.Serializable;

@Deprecated public enum BuyerIdentificationTypeDTO implements Serializable, BaseSpinnerItem {

  NATIONAL_ID_CARD("msltravellerinformationdescription_identificationtypenational_id_card",
      Validations.VALIDATION_NATIONAL_ID_CARD), NIE(
      "msltravellerinformationdescription_identificationtypenie", Validations.VALIDATION_NIE), CIF(
      "msltravellerinformationdescription_identificationcif", Validations.VALIDATION_CIF), PASSPORT(
      "msltravellerinformationdescription_identificationtypepassport",
      Validations.VALIDATION_PASSPORT), NIF(
      "msltravellerinformationdescription_identificationtypenif",
      Validations.VALIDATION_NIF), BIRTH_DATE("datepicker_birthdate",
      Validations.VALIDATION_BIRTH_DATE), EMPTY(EMPTY_STRING, Validations.VALIDATION_NONE);

  String shownTextKey;

  /**
   * The reference to the validation type to be used after selecting this buyer identification
   * type
   */
  int validationType;

  /**
   * Enum constructor
   *
   * @param shownTextId Id of the text that will be shown when using this enum
   * @param validationType Validation type, to use after selecting a buyer information type
   */
  BuyerIdentificationTypeDTO(String shownTextId, int validationType) {
    this.shownTextKey = shownTextId;
    this.validationType = validationType;
  }

  public static BuyerIdentificationTypeDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

  @Override public String getShownTextKey() {
    return shownTextKey;
  }

  @Override public int getImageId() {
    return BaseSpinnerItem.EMPTY_RESOURCE;
  }

  @Override public String getShownText() {
    return null;
  }

  public int getValidationType() {
    return validationType;
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public Object getData() {
    return null;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }
}
