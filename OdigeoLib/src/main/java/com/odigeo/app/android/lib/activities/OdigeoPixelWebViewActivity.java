package com.odigeo.app.android.lib.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.base.BlackDialog;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.squareup.otto.Subscribe;

/**
 * Created by Irving on 01/10/2014.
 */
public class OdigeoPixelWebViewActivity extends OdigeoForegroundBaseActivity
    implements OnClickListener {
  private WebView pixelWebView;
  private BlackDialog dialog;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.layout_activity_pixel_webview);

    pixelWebView = (WebView) findViewById(R.id.webViewPixel);
    View backBotton = findViewById(R.id.image_back_button_pixel);
    View homeButtom = findViewById(R.id.image_home_button_pixel);

    pixelWebView.getSettings().setJavaScriptEnabled(true);

    backBotton.setOnClickListener(this);
    homeButtom.setOnClickListener(this);

    dialog = new BlackDialog(this, true);
    dialog.show(LocalizablesFacade.getString(this, "loadingviewcontroller_message_loading"));

    try {
      Bundle extras = getIntent().getExtras();
      String url = extras.getString(Constants.INTENT_URL_WEBVIEW);
      pixelWebView.loadUrl(url);
    } catch (Exception e) {

      Log.d("WebViewActivity", "Crashlytics exception logged");
    }

    pixelWebView.setWebViewClient(new WebViewClient() {
      @Override public void onPageFinished(WebView view, String url) {

        super.onPageFinished(view, url);
        dialog.dismiss();

        //TODO This URL should be set as a parameter inside the brandConfiguration
        if (("com.opodo.flights.pixellback://pixellback/").equals(url)) {
          gotoBack();
        }
      }

      @Override public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.contains(Constants.INTENT_URL_PREFIX)) {
          String emptyValue = "";
          String newUrl = url.replace(Constants.INTENT_URL_PREFIX, emptyValue);
          if (HtmlUtils.checkUrlIsPdf(url)) {
            HtmlUtils.launchNewWebView(newUrl, OdigeoPixelWebViewActivity.this);
          } else {
            dialog.show(LocalizablesFacade.getString(OdigeoPixelWebViewActivity.this,
                "loadingviewcontroller_message_loading"));
            view.loadUrl(newUrl);
          }
          return true;
        }
        return false;
      }
    });
  }

  @Override public boolean onKeyDown(int keyCode, KeyEvent event) {
    if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
      gotoBack();
      return true;
    }
    return super.onKeyDown(keyCode, event);
  }

  private void gotoBack() {
    if (pixelWebView.canGoBack()) {
      pixelWebView.goBack();
    } else {
      finish();
    }
  }

  @Override public void onClick(View v) {
    int idView = v.getId();
    if (idView == R.id.image_back_button_pixel) {
      gotoBack();
    } else if (idView == R.id.image_home_button_pixel) {
      AlertDialog.Builder builder = new AlertDialog.Builder(this);
      builder.setMessage(
          LocalizablesFacade.getString(this, "webviewcontroller_alertview_description"))
          .setPositiveButton(LocalizablesFacade.getString(this, "common_ok"),
              new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                  finish();
                }
              })
          .setNegativeButton(LocalizablesFacade.getString(this, "common_cancel"),
              new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                  //TODO onClick method
                }
              });
      builder.create();
      builder.show();
    }
  }

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getApplication());
  }
}
