package com.odigeo.app.android.lib.models.dto;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.data.entity.BaseSpinnerItem;
import java.io.Serializable;

/**
 * @author miguel
 */
public class CarrierDTO
    implements Serializable, Comparable<CarrierDTO>, Parcelable, BaseSpinnerItem {

  /**
   * Creator class to be used to create a parcel of the class.
   */
  public static final Parcelable.Creator<CarrierDTO> CREATOR =
      new ClassLoaderCreator<CarrierDTO>() {
        @Override public CarrierDTO createFromParcel(Parcel source, ClassLoader loader) {
          return new CarrierDTO(source);
        }

        @Override public CarrierDTO createFromParcel(Parcel source) {
          return new CarrierDTO(source);
        }

        @Override public CarrierDTO[] newArray(int size) {
          return new CarrierDTO[size];
        }
      };
  protected String code;
  protected String name;
  //In DTO´s for Import Trip,the JSON has this values
  protected int id;

  public CarrierDTO() {
    // Nothing
  }

  public CarrierDTO(String code) {
    this.code = code;
  }

  public CarrierDTO(String code, String name) {
    this.code = code;
    this.name = name;
  }

  /**
   * Constructor for Parcelable.
   *
   * @param input Parcel to be read.
   */
  private CarrierDTO(Parcel input) {
    String[] data = new String[3];
    input.readStringArray(data);
    id = Integer.parseInt(data[0]);
    code = data[1];
    name = data[2];
  }

  /**
   * Gets the value of the code property.
   *
   * @return possible object is {@link String }
   */
  public String getCode() {
    return code;
  }

  /**
   * Sets the value of the code property.
   *
   * @param value allowed object is {@link String }
   */
  public void setCode(String value) {
    this.code = value;
  }

  /**
   * Gets the value of the name property.
   *
   * @return possible object is {@link String }
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the value of the name property.
   *
   * @param value allowed object is {@link String }
   */
  public void setName(String value) {
    this.name = value;
  }

  public boolean equals(Object otherCarrier) {
    if (otherCarrier == null) {
      return false;
    }

    if (!(otherCarrier instanceof CarrierDTO)) {
      return false;
    }

    return ((CarrierDTO) otherCarrier).getCode().equals(code);
  }

  @Override public int compareTo(CarrierDTO another) {

    if (another == null) {
      return 1;
    }

    return name.compareTo(another.getName());
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeStringArray(new String[] { String.valueOf(id), code, name });
  }

  @Override public String getShownTextKey() {
    return EMPTY_STRING;
  }

  @Override public int getImageId() {
    return EMPTY_RESOURCE;
  }

  @Nullable @Override public String getShownText() {
    return name;
  }

  @Nullable @Override public String getImageUrl() {
    if (code == null) {
      return null;
    } else {
      return Configuration.getInstance().getImagesSources().getUrlAirlineLogos() + code + ".png";
    }
  }

  @Nullable @Override public Object getData() {
    return null;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }
}
