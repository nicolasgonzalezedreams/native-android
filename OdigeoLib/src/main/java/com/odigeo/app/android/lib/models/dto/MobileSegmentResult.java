package com.odigeo.app.android.lib.models.dto;

import java.util.ArrayList;
import java.util.Collection;

public final class MobileSegmentResult {

  private Collection<MobileSectionResult> sections;

  /**
   * @return the mobileSections
   */
  public Collection<MobileSectionResult> getSections() {
    if (this.sections == null) {
      this.sections = new ArrayList<MobileSectionResult>();
    }
    return sections;
  }
}
