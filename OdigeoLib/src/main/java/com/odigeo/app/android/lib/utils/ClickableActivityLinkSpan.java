package com.odigeo.app.android.lib.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.odigeo.app.android.lib.R;

/**
 * Created by Irving on 03/02/2015.
 *
 * @author Irving
 * @since 03/02/2015
 *
 * Enable click in a specific text to an activity
 */
public class ClickableActivityLinkSpan extends ClickableSpan {

  private final Activity fromActivity;
  private final Intent intent;

  public ClickableActivityLinkSpan(Activity fromActivity, Class destinationClass) {

    this.fromActivity = fromActivity;
    this.intent = new Intent(fromActivity, destinationClass);
  }

  public final void setBundle(Bundle bundle) {
    this.intent.putExtras(bundle);
  }

  @Override public final void onClick(View widget) {

    fromActivity.startActivity(intent);
  }

  @Override public final void updateDrawState(TextPaint ds) {

    ds.setColor(fromActivity.getResources().getColor(R.color.basic_middle));
    ds.setUnderlineText(true);
  }
}
