package com.odigeo.app.android.view.custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.dataodigeo.tracker.TrackerConstants;

public class TripDetailsGroundTransportationView extends GroundTransportationViewBase {

  public TripDetailsGroundTransportationView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public TripDetailsGroundTransportationView(Context context, Booking booking,
      GroundTransportationWidgetListener listener) {
    super(context, booking, listener);
  }

  @Override protected void trackInteraction() {
    trackerController.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS_TRIP_INFO,
        TrackerConstants.ACTION_XSELL_WIDGET, TrackerConstants.LABEL_GT_ON_CLICK);
  }

  @Override protected String getTitle() {
    String title = localizables.getString(OneCMSKeys.TRIP_DETAILS_GROUND_TRANSPORTATION_TITLE);
    return String.format(title, getCityName());
  }

  @Override protected String getDescription() {
    return localizables.getString(OneCMSKeys.TRIP_DETAILS_GROUND_TRANSPORTATION_MESSAGE);
  }

  @Override protected String getCTAText() {
    return localizables.getString(OneCMSKeys.TRIP_DETAILS_GROUND_TRANSPORTATION_BUTTON);
  }

  @Override protected String getRawUrl() {
    return localizables.getString(OneCMSKeys.TRIP_DETAILS_GROUND_TRANSPORTATION_URL);
  }

  @Override protected String getImageUrl() {
    return localizables.getString(OneCMSKeys.TRIP_DETAILS_GROUND_TRANSPORTATION_IMAGE_LINK);
  }
}
