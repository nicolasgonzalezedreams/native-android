package com.odigeo.app.android.lib.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.globant.roboneck.common.NeckSpiceManager;
import com.localytics.android.Localytics;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.OdigeoSpinnerAdapter;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Lists;
import com.odigeo.app.android.lib.config.Market;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.DistanceUnits;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.fragments.PassengerDialogPickerFragment;
import com.odigeo.app.android.lib.interfaces.OdigeoInterfaces;
import com.odigeo.app.android.lib.models.AppSettings;
import com.odigeo.app.android.lib.ui.widgets.ButtonPassengers;
import com.odigeo.app.android.lib.ui.widgets.CountryButtonWithImage;
import com.odigeo.app.android.lib.ui.widgets.SwitchWidget;
import com.odigeo.app.android.lib.ui.widgets.base.OdigeoSpinner;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.DeviceUtils;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.navigator.LoginNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DateHelper;
import com.odigeo.app.android.view.helpers.DialogHelper;
import com.odigeo.app.android.view.interfaces.AuthDialogActionInterface;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.localizable.OneCMSAlarmsController;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.dataodigeo.location.LocationController;
import com.odigeo.dataodigeo.net.NetTool;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.EnableNotificationsInteractor;
import com.odigeo.interactors.LogoutInteractor;
import com.odigeo.interactors.MoveFlightStatusSwitchInteractor;
import com.odigeo.interactors.VisitsInteractor;
import com.odigeo.presenter.listeners.FlightStatusSwitchListener;
import com.odigeo.presenter.listeners.VisitInteractorListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Locale;

public abstract class OdigeoSettingsActivity extends OdigeoBackgroundAnimatedActivity
    implements View.OnClickListener, OdigeoInterfaces.PassengerPickerFragmentListener,
    PropertyChangeListener {

  protected NeckSpiceManager spiceManager = new NeckSpiceManager();
  LocationController locationController;
  private OdigeoApp odigeoApp;
  private OdigeoSession odigeoSession;
  private CountryButtonWithImage countryButton;
  private TextView languageTextView;
  private TextView coinTextView;
  private ButtonPassengers passengerButton;
  private OdigeoSpinner unitsSpinner;
  private SwitchWidget mSwitchFlightStatus;
  private MoveFlightStatusSwitchInteractor mMoveFlightStatusSwitchInteractor;
  private NetTool mNetTool;
  private TextView mLegalFlightStatus;
  private Snackbar mSnackBar;
  private boolean statusNotifications;
  private boolean mIsStatusFlightEnabled;
  private View mFlightSwitchSeparator;
  private DateHelper mDateHelper;
  private PreferencesControllerInterface mPreferencesController;
  private LogoutInteractor mLogoutInteractor;
  private DialogHelper mDialogHelper;
  private SessionController mSessionController;
  private OneCMSAlarmsController oneCMSAlarmsController;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mPreferencesController = AndroidDependencyInjector.getInstance().providePreferencesController();
    locationController = AndroidDependencyInjector.getInstance().provideLocationController();
    setContentView(R.layout.activity_settings);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    mMoveFlightStatusSwitchInteractor =
        AndroidDependencyInjector.getInstance().provideNotificationSwitchInteractor();
    mLogoutInteractor = AndroidDependencyInjector.getInstance().provideLogoutInteractor();
    mSessionController = AndroidDependencyInjector.getInstance().provideSessionController();
    oneCMSAlarmsController =
        AndroidDependencyInjector.getInstance().provideOneCMSAlarmsController();
    mDialogHelper = new DialogHelper(this);
    odigeoApp = (OdigeoApp) getApplication();
    odigeoSession = odigeoApp.getOdigeoSession();
    countryButton = (CountryButtonWithImage) findViewById(R.id.button_settings_country);
    languageTextView = (TextView) findViewById(R.id.text_settings_language_label);
    coinTextView = (TextView) findViewById(R.id.text_settings_money_label);
    passengerButton = (ButtonPassengers) findViewById(R.id.button_settings_passengers);
    unitsSpinner = (OdigeoSpinner) findViewById(R.id.spinner_settings_dimens_units);
    TextView txtCopyright = (TextView) findViewById(R.id.text_settings_credits);
    mSwitchFlightStatus = (SwitchWidget) findViewById(R.id.switchFlightStatus);
    mLegalFlightStatus = (TextView) findViewById(R.id.text_settings_notifcations_flight);
    mFlightSwitchSeparator = findViewById(R.id.id_layout_separator);

    mSwitchFlightStatus.setVisibility(View.GONE);
    mLegalFlightStatus.setVisibility(View.GONE);
    mFlightSwitchSeparator.setVisibility(View.GONE);
    mMoveFlightStatusSwitchInteractor.handleFlightStatusSwitchVisibility(
        new FlightStatusSwitchListener() {
          @Override public void showFlightStatusSwitch() {
            runOnUiThread(new Runnable() {
              @Override public void run() {
                mSwitchFlightStatus.setVisibility(View.VISIBLE);
                mLegalFlightStatus.setVisibility(View.VISIBLE);
                mFlightSwitchSeparator.setVisibility(View.VISIBLE);
              }
            });
          }
        }, OneCMSKeys.APP_SETTINGS_NOTIFICATIONS_FLIGHT_ACTION);

    mNetTool = AndroidDependencyInjector.getInstance().provideNetTool();
    mDateHelper = new DateHelper();
    String copyrightBrand = getString(R.string.string_copyright,
        DeviceUtils.getApplicationVersionName(getApplicationContext()),
        Configuration.getInstance().getBrandVisualName()) + " " + mDateHelper.getCurrentYear();
    txtCopyright.setText(copyrightBrand);

    TextView tvNotificationTitle = (TextView) findViewById(R.id.tvNotificationTitle);
    TextView tvSettingTitle = (TextView) findViewById(R.id.tvSettingTitle);

    tvNotificationTitle.setText(LocalizablesFacade.getString(getApplicationContext(),
        OneCMSKeys.APPSETTINGS_HEADER_NOTIFICATIONS));
    tvSettingTitle.setText(
        LocalizablesFacade.getString(getApplicationContext(), OneCMSKeys.SSO_ND_SETTINGS));

    //Switch settings
    SwitchWidget switchNotifications = (SwitchWidget) findViewById(R.id.switchSettings);
    switchNotifications.setLabel(
        LocalizablesFacade.getString(this, "appsettings_notifications_switch",
            Configuration.getInstance().getBrandVisualName()));

    statusNotifications = PreferencesManager.readStatusNotifications(getApplicationContext());

    Localytics.setNotificationsDisabled(statusNotifications);

    Log.d(Constants.TAG_LOG, "statusNotifications: " + statusNotifications);
    switchNotifications.setChecked(!statusNotifications);
    switchNotifications.setListener(new SwitchWidget.SwitchListener() {
      @Override public void onSwitchOn() {
        Log.d(Constants.TAG_LOG, "onSwitchOn");
        statusNotifications = false;
        PreferencesManager.setStatusNotifications(getApplicationContext(), statusNotifications);
        Localytics.setNotificationsDisabled(statusNotifications);
      }

      @Override public void onSwitchOff() {
        Log.d(Constants.TAG_LOG, "onSwitchOff");
        statusNotifications = true;
        PreferencesManager.setStatusNotifications(getApplicationContext(), statusNotifications);
        Localytics.setNotificationsDisabled(statusNotifications);
      }
    });

    mIsStatusFlightEnabled =
        mPreferencesController.getBooleanValue(Constants.PREFERENCE_STATUS_FLIGHTS_NOTIFICATIONS,
            true);
    mSwitchFlightStatus.setChecked(mIsStatusFlightEnabled);
    mLegalFlightStatus.setText(LocalizablesFacade.getString(getApplicationContext(),
        OneCMSKeys.APP_SETTINGS_NOTIFICATIONS_FLIGHT_STATUS));
    mSwitchFlightStatus.setLabel(LocalizablesFacade.getString(getApplicationContext(),
        OneCMSKeys.APP_SETTINGS_NOTIFICATIONS_FLIGHT_ACTION));

    countryButton.setOnClickListener(this);
    passengerButton.setOnClickListener(this);

    Market market = Configuration.getInstance().getCurrentMarket();

    setMarket(market);

    countryButton.setChangeListener(this);

    setListeners();

    init();
  }

  private void setListeners() {

    mSwitchFlightStatus.setListener(new SwitchWidget.SwitchListener() {
      @Override public void onSwitchOn() {
        if (mNetTool.isThereInternetConnection()) {
          mIsStatusFlightEnabled = true;
          mMoveFlightStatusSwitchInteractor.turnOnNotifications(
              new OnAuthRequestDataListener<Boolean>() {
                @Override public void onAuthError() {
                  mLogoutInteractor.logout(new OnRequestDataListener<Boolean>() {
                    @Override public void onResponse(Boolean object) {
                      mDialogHelper.showAuthErrorDialog(new AuthDialogActionInterface() {
                        @Override public void OnAuthDialogOK() {
                          Intent intent = new Intent(getApplicationContext(), LoginNavigator.class);
                          intent.addFlags(
                              Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                          startActivity(intent);
                          finish();
                        }
                      });
                      mSessionController.saveLogoutWasMade(true);
                    }

                    @Override public void onError(MslError error, String message) {
                      Log.e(error.name(), message);
                    }
                  });
                }

                @Override public void onResponse(Boolean object) {

                }

                @Override public void onError(MslError error, String message) {
                  setFlightStatusSwitch(false);
                  showSnackBarFlightStatusError(
                      LocalizablesFacade.getString(OdigeoSettingsActivity.this,
                          OneCMSKeys.SSO_ERROR_WRONG));
                }
              });
          mPreferencesController.saveBooleanValue(Constants.PREFERENCE_STATUS_FLIGHTS_NOTIFICATIONS,
              mIsStatusFlightEnabled);
          mTracker.trackSwitchNotificationsFlightStatus(mIsStatusFlightEnabled);
        } else {
          setFlightStatusSwitch(false);
          showSnackBarFlightStatusError(LocalizablesFacade.getString(getApplicationContext(),
              OneCMSKeys.ERROR_NO_CONNECTION_SUBTITLE));
        }
      }

      @Override public void onSwitchOff() {
        if (mNetTool.isThereInternetConnection()) {
          mIsStatusFlightEnabled = false;
          mMoveFlightStatusSwitchInteractor.turnOffNotifications(
              new OnRequestDataListener<Boolean>() {
                @Override public void onResponse(Boolean object) {

                }

                @Override public void onError(MslError error, String message) {
                  setFlightStatusSwitch(true);
                  showSnackBarFlightStatusError(
                      LocalizablesFacade.getString(OdigeoSettingsActivity.this,
                          OneCMSKeys.SSO_ERROR_WRONG));
                }
              });
          mPreferencesController.saveBooleanValue(Constants.PREFERENCE_STATUS_FLIGHTS_NOTIFICATIONS,
              mIsStatusFlightEnabled);
          mTracker.trackSwitchNotificationsFlightStatus(mIsStatusFlightEnabled);
        } else {
          setFlightStatusSwitch(true);
          showSnackBarFlightStatusError(LocalizablesFacade.getString(getApplicationContext(),
              OneCMSKeys.ERROR_NO_CONNECTION_SUBTITLE));
        }
      }
    });

    unitsSpinner.setSpinnerItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        DistanceUnits item = (DistanceUnits) unitsSpinner.getAdapter().getItem(position);
        postGAEventSettingsDistanceUnitChanged(
            LocalizablesFacade.getString(OdigeoSettingsActivity.this, item.getShownTextKey())
                .toString());
      }

      @Override public void onNothingSelected(AdapterView<?> parent) {

      }
    });
  }

  private void setFlightStatusSwitch(boolean isActive) {
    mIsStatusFlightEnabled = isActive;
    mSwitchFlightStatus.setChecked(isActive);
  }

  private void showSnackBarFlightStatusError(CharSequence errorText) {
    ScrollView parentView = (ScrollView) this.findViewById(R.id.scroll_view_activity_settings);
    mSnackBar = Snackbar.make(parentView, errorText, Snackbar.LENGTH_LONG);
    mSnackBar.show();
  }

  @Override public void onResume() {
    super.onResume();
    BusProvider.getInstance().post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_SETTINGS));
  }

  @Override public void onStart() {
    super.onStart();
    spiceManager.start(this);
    getSupportActionBar().setTitle(
        LocalizablesFacade.getString(this, "settingsviewcontroller_title"));
  }

  @Override public void onStop() {
    spiceManager.shouldStop();
    super.onStop();
  }

  @Override public void onClick(View v) {

    int idView = v.getId();
    if (idView == R.id.button_settings_country) {
      Intent intent = new Intent(OdigeoSettingsActivity.this, OdigeoCountriesActivity.class);
      intent.putExtra(Constants.EXTRA_COUNTRY_MODE, OdigeoCountriesActivity.SELECTION_MODE_COUNTRY);
      intent.putExtra(Constants.EXTRA_ALL_COUNTRY_LIST, false);
      intent.putExtra(Constants.EXTRA_COUNTRY_ACT_PARENT,
          Constants.EXTRA_COUNTRY_ACT_PARENT_SETTINGS);
      startActivityForResult(intent, Constants.REQUEST_CODE_LANGUAGE);
    } else if (idView == R.id.button_settings_passengers) {

      PassengerDialogPickerFragment dialog = new PassengerDialogPickerFragment();
      dialog.setNumAdults(passengerButton.getNoAdults());
      dialog.setNumKids(passengerButton.getNoKids());
      dialog.setNumBabies(passengerButton.getNoBabies());
      dialog.show(getSupportFragmentManager(), "Pickerfragment");
    }
  }

  private void init() {

    setMarket(Configuration.getInstance().getCurrentMarket());
    unitsSpinner.setAdapter(
        new OdigeoSpinnerAdapter<>(this, Lists.getInstance().getDistanceUnits()));
    unitsSpinner.setSelection(odigeoApp.getSettingsUnits());
    passengerButton.setNoAdults(
        Configuration.getInstance().getGeneralConstants().getDefaultNumberOfAdults());
    passengerButton.setNoKids(
        Configuration.getInstance().getGeneralConstants().getDefaultNumberOfKids());
    passengerButton.setNoBabies(
        Configuration.getInstance().getGeneralConstants().getDefaultNumberOfInfants());
    locationController.connect();
  }

  private void setMarket(Market market) {
    countryButton.setCountry(new Country(market.getKey(), market.getName()));
    languageTextView.setText(
        LocalizablesFacade.getString(this, "formfieldrow_placeholder_settings_language",
            LocalizablesFacade.getString(this, "string_new_language").toString()));
    coinTextView.setText(
        LocalizablesFacade.getString(this, "formfieldrow_placeholder_settings_currency",
            LocaleUtils.getCurrencySymbol(), LocaleUtils.getCurrencyCode()));
  }

  public void setLocale(String lang) {

    Resources res = getResources();
    android.content.res.Configuration conf = res.getConfiguration();
    Locale myLocale = LocaleUtils.strToLocale(lang);
    if (!conf.locale.equals(myLocale)) {
      Configuration.getInstance().getCurrentMarket().setLocale(lang);
      conf.locale = myLocale;
      DisplayMetrics dm = res.getDisplayMetrics();
      res.updateConfiguration(conf, dm);
      Intent refresh = new Intent(this, odigeoApp.getSettingsActivityClass());
      Localytics.setCustomDimension(Constants.LOCALYTICS_MARKET_CUSTOM_INDEX,
          Configuration.getInstance().getCurrentMarket().getName());
      startActivity(refresh);
      finish();
    }
  }

  private void updatePassengers(int numAdults, int numKids, int numBabies) {
    passengerButton.setNoAdults(numAdults);
    passengerButton.setNoKids(numKids);
    passengerButton.setNoBabies(numBabies);
    Configuration.getInstance().getGeneralConstants().setDefaultNumberOfAdults(numAdults);
    Configuration.getInstance().getGeneralConstants().setDefaultNumberOfKids(numKids);
    Configuration.getInstance().getGeneralConstants().setDefaultNumberOfInfants(numBabies);
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK) {
      CommonLocalizables.getInstance().clearAll();
      if (requestCode == Constants.REQUEST_CODE_LANGUAGE) {
        Country countryCurrently = (Country) data.getSerializableExtra(Constants.EXTRA_COUNTRY);
        for (Market market : Configuration.getInstance().getMarkets()) {
          if (market.getKey().equals(countryCurrently.getCountryCode())) {
            Configuration.getInstance().setCurrentMarket(market);
            setLocale(market.getLocale());
            setMarket(market);
            odigeoApp.setHasMarketChanged(true);
            break;
          }
        }

        oneCMSAlarmsController.retryOneCMSUpdate();

        if (mPreferencesController.getBooleanValue(
            Constants.PREFERENCE_STATUS_FLIGHTS_NOTIFICATIONS, true)) {
          updateChannel();
        }
        updateABManager();
      }
    }
  }

  private void updateChannel() {
    EnableNotificationsInteractor mEnableNotificationsInteractor =
        AndroidDependencyInjector.getInstance().provideEnableNotificationsInteractor();
    mEnableNotificationsInteractor.enableNotifications();
  }

  private void updateABManager() {
    VisitsInteractor visitsInteractor =
        AndroidDependencyInjector.getInstance().provideVisitsInteractor();
    visitsInteractor.fetch(new VisitInteractorListener() {
      @Override public void onSuccess() {
        mTracker.updateDimensionForABTest();
      }
    });

  }

  @Override public void onDialogPositiveClick(int numAdults, int numKids, int numBabies) {
    postGAEventSettingsPassengerAmountsChanged(numAdults, numKids, numBabies);
    updatePassengers(numAdults, numKids, numBabies);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int idMenu = item.getItemId();
    if (idMenu == android.R.id.home) {
      onBackPressed();
    }
    return super.onOptionsItemSelected(item);
  }

  @Override public void onBackPressed() {

    postGAEventGoBackFromSettings();

    DistanceUnits selectedItem = (DistanceUnits) unitsSpinner.getSelectedItem();
    odigeoApp.setSettingsUnits(selectedItem);
    AppSettings appSettings = new AppSettings();
    appSettings.setDefaultAdults(passengerButton.getNoAdults());
    appSettings.setDefaultKids(passengerButton.getNoKids());
    appSettings.setDefaultBabies(passengerButton.getNoBabies());
    appSettings.setDistanceUnit(selectedItem);
    appSettings.setMarketKey(Configuration.getInstance().getCurrentMarket().getKey());
    PreferencesManager.saveSettings(getApplicationContext(), appSettings);

    updateDistanceToCities(appSettings);

    setResult(RESULT_OK);

    int count = getSupportFragmentManager().getBackStackEntryCount();
    if (count == 0) {
      Intent intent = new Intent(this, odigeoApp.getHomeActivity());
      startActivity(intent);
      finish();
    } else {
      getSupportFragmentManager().popBackStackImmediate();
    }
  }

  private void updateDistanceToCities(AppSettings appSettings) {

    List<City> lstCities = locationController.getNearestPlacesList();
    Location currentLocation = locationController.getCurrentLocation();
    if (currentLocation == null || lstCities == null) {
      return;
    }

    //Re Calculate distances from currentLocation
    for (City city : lstCities) {
      Location locationCity = new Location("");
      locationCity.setLatitude(city.getCoordinates().getLatitude());
      locationCity.setLongitude(city.getCoordinates().getLongitude());

      city.setDistanceToCurrentLocation(
          Util.calculateDistance(currentLocation, locationCity, appSettings.getDistanceUnit()));
    }
  }

  @Override public void propertyChange(PropertyChangeEvent event) {
    if (event.getSource() instanceof CountryButtonWithImage
        && event.getPropertyName() != null
        && event.getOldValue() != null
        && event.getNewValue() != null
        && event.getOldValue() != event.getNewValue()) {
      postGAEventSettingsCountryChanged(event.getOldValue().toString(),
          event.getNewValue().toString());
    }
  }

  @Override public String getActivityTitle() {
    return Configuration.getInstance().getBrandVisualName();
  }

  public void postGAEventSettingsCountryChanged(String oldCountry, String newCountry) {
    String label = GAnalyticsNames.LABEL_PARTIAL_COUNTRY + newCountry + "_" + oldCountry;

    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_CONFIGURATION_SCREEN,
            GAnalyticsNames.ACTION_APP_OPTIONS, label));
  }

  public void postGAEventSettingsDistanceUnitChanged(String unitName) {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_CONFIGURATION_SCREEN,
            GAnalyticsNames.ACTION_APP_OPTIONS,
            GAnalyticsNames.LABEL_PARTIAL_DISTANCE_UNITS + unitName));
  }

  public void postGAEventGoBackFromSettings() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_CONFIGURATION_SCREEN,
            GAnalyticsNames.ACTION_NAVIGATION, GAnalyticsNames.LABEL_GO_BACK));
  }

  public void postGAEventSettingsPassengerAmountsChanged(int adults, int children, int babies) {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_CONFIGURATION_SCREEN,
            GAnalyticsNames.ACTION_APP_OPTIONS,
            GAnalyticsNames.LABEL_PARTIAL_NUMBER_PAX + adults + "_" + children + "_" + babies));
  }

  public abstract void onGAEventTriggered(GATrackingEvent event);
}
