package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TravellerInformationDescriptionDTO implements Serializable {

  protected List<TravellerIdentificationTypeDTO> identificationTypes;
  protected List<BaggageConditionsDTO> baggageConditions;
  protected List<AirlineGroupDTO> frequentFlyerAirlineGroups;
  protected List<ResidentGroupContainerDTO> residentGroups;
  protected List<TravellerMealDTO> availableMeals;
  protected TravellerTypeDTO travellerType;
  protected RequiredFieldDTO needsTitle;
  protected RequiredFieldDTO needsName;
  protected RequiredFieldDTO needsGender;
  protected RequiredFieldDTO needsNationality;
  protected RequiredFieldDTO needsBirthDate; //si
  protected RequiredFieldDTO needsCountryOfResidence;
  protected RequiredFieldDTO needsIdentification;
  protected RequiredFieldDTO needsIdentificationExpirationDate;
  protected RequiredFieldDTO needsIdentificationCountry;

  /**
   * Gets the value of the baggageConditionsList property.
   *
   * @return possible object is {@link BaggageConditionsDTO }
   */

  public List<BaggageConditionsDTO> getBaggageConditionsList() {
    if (baggageConditions == null) {
      baggageConditions = new ArrayList<>();
    }
    return baggageConditions;
  }

  public List<BaggageConditionsDTO> getBaggageConditions() {
    return baggageConditions;
  }

  /**
   * -     * Sets the value of the baggageConditionsList property.
   * -     *
   * -     * @param value
   * -     *     allowed object is {@link BaggageConditionsDTO }
   * -
   */
  public void setBaggageConditions(List<BaggageConditionsDTO> value) {
    this.baggageConditions = value;
  }

  public List<TravellerMealDTO> getAvailableMeals() {
    return availableMeals;
  }

  public void setAvailableMeals(List<TravellerMealDTO> availableMeals) {
    this.availableMeals = availableMeals;
  }

  public List<TravellerIdentificationTypeDTO> getIdentificationTypes() {
    return identificationTypes;
  }

  public void setIdentificationTypes(List<TravellerIdentificationTypeDTO> identificationTypes) {
    this.identificationTypes = identificationTypes;
  }

  public List<AirlineGroupDTO> getFrequentFlyerAirlineGroups() {
    return frequentFlyerAirlineGroups;
  }

  public void setFrequentFlyerAirlineGroups(List<AirlineGroupDTO> frequentFlyerAirlineGroups) {
    this.frequentFlyerAirlineGroups = frequentFlyerAirlineGroups;
  }

  public List<ResidentGroupContainerDTO> getResidentGroups() {
    return residentGroups;
  }

  public void setResidentGroups(List<ResidentGroupContainerDTO> residentGroups) {
    this.residentGroups = residentGroups;
  }

  /**
   * Gets the value of the travellerType property.
   *
   * @return possible object is {@link TravellerTypeDTO }
   */

  public TravellerTypeDTO getTravellerType() {
    return travellerType;
  }

  /**
   * -     * Sets the value of the travellerType property.
   * -     *
   * -     * @param value
   * -     *     allowed object is {@link TravellerTypeDTO }
   * -
   */
  public void setTravellerType(TravellerTypeDTO value) {
    this.travellerType = value;
  }

  /**
   * Gets the value of the needsTitle property.
   *
   * @return possible object is {@link RequiredFieldDTO }
   */

  public RequiredFieldDTO getNeedsTitle() {
    return needsTitle;
  }

  /**
   * -     * Sets the value of the needsTitle property.
   * -     *
   * -     * @param value
   * -     *     allowed object is {@link RequiredFieldDTO }
   * -
   */
  public void setNeedsTitle(RequiredFieldDTO value) {
    this.needsTitle = value;
  }

  /**
   * Gets the value of the needsName property.
   *
   * @return possible object is {@link RequiredFieldDTO }
   */

  public RequiredFieldDTO getNeedsName() {
    return needsName;
  }

  /**
   * -     * Sets the value of the needsName property.
   * -     *
   * -     * @param value
   * -     *     allowed object is {@link RequiredFieldDTO }
   * -
   */
  public void setNeedsName(RequiredFieldDTO value) {
    this.needsName = value;
  }

  /**
   * Gets the value of the needsGender property.
   *
   * @return possible object is {@link RequiredFieldDTO }
   */

  public RequiredFieldDTO getNeedsGender() {
    return needsGender;
  }

  /**
   * -     * Sets the value of the needsGender property.
   * -     *
   * -     * @param value
   * -     *     allowed object is {@link RequiredFieldDTO }
   * -
   */
  public void setNeedsGender(RequiredFieldDTO value) {
    this.needsGender = value;
  }

  /**
   * Gets the value of the needsNationality property.
   *
   * @return possible object is {@link RequiredFieldDTO }
   */

  public RequiredFieldDTO getNeedsNationality() {
    return needsNationality;
  }

  /**
   * -     * Sets the value of the needsNationality property.
   * -     *
   * -     * @param value
   * -     *     allowed object is {@link RequiredFieldDTO }
   * -
   */
  public void setNeedsNationality(RequiredFieldDTO value) {
    this.needsNationality = value;
  }

  /**
   * Gets the value of the needsBirthDate property.
   *
   * @return possible object is {@link RequiredFieldDTO }
   */

  public RequiredFieldDTO getNeedsBirthDate() {
    return needsBirthDate;
  }

  /**
   * -     * Sets the value of the needsBirthDate property.
   * -     *
   * -     * @param value
   * -     *     allowed object is {@link RequiredFieldDTO }
   * -
   */
  public void setNeedsBirthDate(RequiredFieldDTO value) {
    this.needsBirthDate = value;
  }

  /**
   * Gets the value of the needsCountryOfResidence property.
   *
   * @return possible object is {@link RequiredFieldDTO }
   */

  public RequiredFieldDTO getNeedsCountryOfResidence() {
    return needsCountryOfResidence;
  }

  /**
   * -     * Sets the value of the needsCountryOfResidence property.
   * -     *
   * -     * @param value
   * -     *     allowed object is {@link RequiredFieldDTO }
   * -
   */
  public void setNeedsCountryOfResidence(RequiredFieldDTO value) {
    this.needsCountryOfResidence = value;
  }

  /**
   * Gets the value of the needsIdentification property.
   *
   * @return possible object is {@link RequiredFieldDTO }
   */

  public RequiredFieldDTO getNeedsIdentification() {
    return needsIdentification;
  }

  /**
   * -     * Sets the value of the needsIdentification property.
   * -     *
   * -     * @param value
   * -     *     allowed object is {@link RequiredFieldDTO }
   * -
   */
  public void setNeedsIdentification(RequiredFieldDTO value) {
    this.needsIdentification = value;
  }

  /**
   * Gets the value of the needsIdentificationExpirationDate property.
   *
   * @return possible object is {@link RequiredFieldDTO }
   */

  public RequiredFieldDTO getNeedsIdentificationExpirationDate() {
    return needsIdentificationExpirationDate;
  }

  /**
   * -     * Sets the value of the needsIdentificationExpirationDate property.
   * -     *
   * -     * @param value
   * -     *     allowed object is {@link RequiredFieldDTO }
   * -
   */
  public void setNeedsIdentificationExpirationDate(RequiredFieldDTO value) {
    this.needsIdentificationExpirationDate = value;
  }

  /**
   * Gets the value of the needsIdentificationCountry property.
   *
   * @return possible object is {@link RequiredFieldDTO }
   */

  public RequiredFieldDTO getNeedsIdentificationCountry() {
    return needsIdentificationCountry;
  }

  /**
   * -     * Sets the value of the needsIdentificationCountry property.
   * -     *
   * -     * @param value
   * -     *     allowed object is {@link RequiredFieldDTO }
   * -
   */
  public void setNeedsIdentificationCountry(RequiredFieldDTO value) {
    this.needsIdentificationCountry = value;
  }
}
