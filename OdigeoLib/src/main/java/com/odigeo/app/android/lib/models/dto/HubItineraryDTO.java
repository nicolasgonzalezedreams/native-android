package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for HubItinerary complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="HubItinerary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="oth" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="htd" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class HubItineraryDTO {

  protected Integer id;
  protected Integer oth;
  protected Integer htd;

  /**
   * Gets the value of the id property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setId(Integer value) {
    this.id = value;
  }

  /**
   * Gets the value of the oth property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getOth() {
    return oth;
  }

  /**
   * Sets the value of the oth property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setOth(Integer value) {
    this.oth = value;
  }

  /**
   * Gets the value of the htd property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getHtd() {
    return htd;
  }

  /**
   * Sets the value of the htd property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setHtd(Integer value) {
    this.htd = value;
  }
}
