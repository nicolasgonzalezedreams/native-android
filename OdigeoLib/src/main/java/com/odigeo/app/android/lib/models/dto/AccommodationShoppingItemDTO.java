package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class AccommodationShoppingItemDTO {

  protected BigDecimal price;
  protected AccommodationDealInformationDTO accommodationDealInformation;
  protected RoomGroupDealInformationDTO roomGroupDealInformation;
  protected String urlHotelConditions;
  protected String urlHotelPrivacy;

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public AccommodationDealInformationDTO getAccommodationDealInformation() {
    return accommodationDealInformation;
  }

  public void setAccommodationDealInformation(
      AccommodationDealInformationDTO accommodationDealInformation) {
    this.accommodationDealInformation = accommodationDealInformation;
  }

  public RoomGroupDealInformationDTO getRoomGroupDealInformation() {
    return roomGroupDealInformation;
  }

  public void setRoomGroupDealInformation(RoomGroupDealInformationDTO roomGroupDealInformation) {
    this.roomGroupDealInformation = roomGroupDealInformation;
  }

  public String getUrlHotelConditions() {
    return urlHotelConditions;
  }

  public void setUrlHotelConditions(String urlHotelConditions) {
    this.urlHotelConditions = urlHotelConditions;
  }

  public String getUrlHotelPrivacy() {
    return urlHotelPrivacy;
  }

  public void setUrlHotelPrivacy(String urlHotelPrivacy) {
    this.urlHotelPrivacy = urlHotelPrivacy;
  }
}
