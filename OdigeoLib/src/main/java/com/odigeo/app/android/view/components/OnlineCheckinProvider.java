package com.odigeo.app.android.view.components;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.animations.CollapseAndExpandAnimationUtil;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.tools.DateHelperInterface;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class OnlineCheckinProvider {

  private static final String HMAC256 = "HmacSHA256";
  private static final String SECRET_KEY = "MIpwH-CsQlJ*wzyQYZ-_tql6";

  private static final String HTTP = "http";
  private static final String KEY_SOURCE = "source";
  private static final String KEY_AIRLINE = "airline";
  private static final String KEY_BRAND = "brand";
  private static final String KEY_PNR = "PNR";
  private static final String KEY_EMAIL = "email";
  private static final String KEY_NAME = "name";
  private static final String KEY_LASTNAME = "lastName";
  private static final String KEY_LOCALE = "locale";
  private static final String KEY_DEPARTURE_AIRPORT = "departureAirport";
  private static final String KEY_ARRIVAL_AIRPORT = "arrivalAirport";
  private static final String KEY_FLIGHT_NUMBER = "flightNumber";
  private static final String KEY_DEPARTURE_DATE_TIME = "departureDatetime";
  private static final String KEY_ARRIVAL_DATE_TIME = "arrivalDatetime";
  private static final String KEY_HMAC = "hmac";
  private static final String KEY_HEADER = "header";
  private static final String KEY_FOOTER = "footer";
  private static final String KEY_BAGS = "bags";
  private static final String KEY_SEATS = "seats";

  private static final String VALUE_IFRAME = "iframe";
  private static final String VALUE_BRAND_EDREAMS = "edreams";
  private static final String VALUE_BRAND_OPODO = "opodo";
  private static final String VALUE_BRAND_GO_VOYAGES = "govoyages";
  private static final String VALUE_BRAND_TRAVELLINK = "travellink";
  private static final String VALUE_OFF = "off";

  private static final String AIRLINE_ICON_URL_FORMAT = "%s%s.png";

  private final Activity activity;
  private final TrackerControllerInterface tracker;
  private final Booking booking;
  private final DateHelperInterface dateHelper;
  private final String formatDate;

  private View mView;
  private TextView mTvOnlineCheckinTitle;
  private TextView mTvOnlineCheckinSubTitle;
  private TextView mTvDepartureIata;
  private TextView mTvArrivalIata;
  private TextView mTvPnrCode;
  private TextView mTvCheckinStepOne;
  private TextView mTvCheckinStepTwo;
  private TextView mTvCheckinStepThree;
  private TextView mTvHowItWorks;
  private Button mBtnGetBoardingPass;
  private ImageView mIvAirlineIcon;
  private LinearLayout mLlHowItWorksExpanded;
  private String mBrand;
  private String mFlightNumber;

  private String mBuyerName;
  private String mBuyerLastName;
  private String mBuyerEmail;
  private String mPnr;
  private String mCarrierCode;
  private String mDepartureDateTime;
  private String mArrivalDateTime;
  private String mDepartureAirport;
  private String mArrivalAirport;
  private String mLocale;
  private String mUrl;
  private String mIsHeaderShown;
  private String mIsFooterShown;
  private String mBags;
  private String mSeats;

  private OdigeoImageLoader imageLoader;

  public OnlineCheckinProvider(Activity activity, Booking booking,
      TrackerControllerInterface tracker, DateHelperInterface dateHelper) {
    this.activity = activity;
    this.booking = booking;
    this.dateHelper = dateHelper;
    this.tracker = tracker;
    this.formatDate = activity.getResources().getString(R.string.format_online_checkin_date);
  }

  private static String generateHmac(String msg, String keyString, String inWhichHas) {
    String digest = null;
    try {
      SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), inWhichHas);
      Mac mac = Mac.getInstance(inWhichHas);
      mac.init(key);

      byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

      StringBuilder hash = new StringBuilder();
      for (byte aByte : bytes) {
        String hex = Integer.toHexString(0xFF & aByte);
        if (hex.length() == 1) {
          hash.append('0');
        }
        hash.append(hex);
      }
      digest = hash.toString();
    } catch (UnsupportedEncodingException | InvalidKeyException | NoSuchAlgorithmException e) {
      Log.e("OnlineCheckin", e.getMessage());
    }
    return digest;
  }

  public View getOnlineCheckin(ViewGroup parent) {
    initView(parent);
    setListeners();
    return mView;
  }

  private void initView(ViewGroup parent) {
    imageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();

    mView = activity.getLayoutInflater()
        .inflate(R.layout.my_trip_details_online_checkin, parent, false);

    mTvOnlineCheckinTitle = (TextView) mView.findViewById(R.id.TvOnlineCheckinTitle);
    mTvOnlineCheckinSubTitle = (TextView) mView.findViewById(R.id.TvOnlineCheckinSubTitle);
    mTvDepartureIata = (TextView) mView.findViewById(R.id.TvDepartureIata);
    mTvArrivalIata = (TextView) mView.findViewById(R.id.TvArrivalIata);
    mTvPnrCode = (TextView) mView.findViewById(R.id.TvPnrCode);
    mTvCheckinStepOne = (TextView) mView.findViewById(R.id.TvCheckinStepOne);
    mTvCheckinStepTwo = (TextView) mView.findViewById(R.id.TvCheckinStepTwo);
    mTvCheckinStepThree = (TextView) mView.findViewById(R.id.TvCheckinStepThree);
    mTvHowItWorks = (TextView) mView.findViewById(R.id.TvHowItWorks);
    mBtnGetBoardingPass = (Button) mView.findViewById(R.id.BtnGetBoardingPass);
    mIvAirlineIcon = (ImageView) mView.findViewById(R.id.IvAirlineIcon);
    mLlHowItWorksExpanded = (LinearLayout) mView.findViewById(R.id.LlHowItWorksExpanded);

    initOneCMSText();
    setFlightData();
  }

  private void initOneCMSText() {
    mTvOnlineCheckinTitle.setText(getTextFromOneCMS(OneCMSKeys.BOARDING_PASS_SECTION_TITLE));
    mTvOnlineCheckinSubTitle.setText(getTextFromOneCMS(OneCMSKeys.BOARDING_PASS_SECTION_SUBTITLE));
    mBtnGetBoardingPass.setText(getTextFromOneCMS(OneCMSKeys.BOARDING_PASS_BUTTON));
    mTvCheckinStepOne.setText(getTextFromOneCMS(OneCMSKeys.BOARDING_PASS_HELP_1));
    mTvCheckinStepTwo.setText(getTextFromOneCMS(OneCMSKeys.BOARDING_PASS_HELP_2));
    mTvCheckinStepThree.setText(getTextFromOneCMS(OneCMSKeys.BOARDING_PASS_HELP_3));
    mTvHowItWorks.setText(getTextFromOneCMS(OneCMSKeys.BOARDING_PASS_MORE_HELP));
  }

  private String getTextFromOneCMS(String key) {
    return LocalizablesFacade.getString(activity, key).toString();
  }

  private void setFlightData() {
    mTvDepartureIata.setText(
        booking.getFirstSegment().getFirstSection().getFrom().getLocationCode());
    mTvArrivalIata.setText(booking.getFirstSegment().getFirstSection().getTo().getLocationCode());
    mTvPnrCode.setText(booking.getFirstSegment().getFirstSection().getItineraryBooking().getPnr());
    loadAirlineIcon();
  }

  private void loadAirlineIcon() {
    String airlineIconUrl = String.format(AIRLINE_ICON_URL_FORMAT,
        Configuration.getInstance().getImagesSources().getUrlAirlineLogos(),
        booking.getFirstSegment().getFirstSection().getCarrier().getCode());

    imageLoader.load(mIvAirlineIcon, airlineIconUrl, R.drawable.placeholder_airline);
  }

  private void setListeners() {
    mBtnGetBoardingPass.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        setBookingCommonParams();
        setBookingSectionParams(booking.getFirstSegment().getFirstSection());
        openWebView(getUrlWithParams());
        tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS_TRIP_INFO,
            TrackerConstants.ACTION_AUTO_CHECK_IN,
            TrackerConstants.LABEL_AUTO_CHECKIN_REQUEST_BOARDING_PASS_CLICKS);
      }
    });

    mTvHowItWorks.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        if (mLlHowItWorksExpanded.getVisibility() == View.VISIBLE) {
          CollapseAndExpandAnimationUtil.collapse(mLlHowItWorksExpanded, false, null, null);
        } else {
          CollapseAndExpandAnimationUtil.expand(mLlHowItWorksExpanded);
          tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS_TRIP_INFO,
              TrackerConstants.ACTION_AUTO_CHECK_IN,
              TrackerConstants.LABEL_AUTO_CHECKIN_HOW_IT_WORKS_OPEN);
        }
      }
    });
  }

  private void setBookingCommonParams() {
    mBrand = getBrandParam();
    mBuyerName = booking.getBuyer().getName();
    mBuyerLastName = booking.getBuyer().getLastname();
    mBuyerEmail = booking.getBuyer().getEmail();
    mLocale = Configuration.getInstance().getCurrentMarket().getLocale();
    mUrl = getTextFromOneCMS(OneCMSKeys.BOARDING_PASS_URL);
    mIsFooterShown = VALUE_OFF;
    mIsHeaderShown = VALUE_OFF;
    mBags = VALUE_OFF;
    mSeats = VALUE_OFF;
  }

  private void setBookingSectionParams(Section section) {

    mPnr = section.getItineraryBooking().getPnr();
    mCarrierCode = section.getCarrier().getCode();
    mFlightNumber = section.getSectionId();
    mArrivalDateTime = dateHelper.millisecondsToDateGMT(section.getArrivalDate(), formatDate);
    mDepartureDateTime = dateHelper.millisecondsToDateGMT(section.getDepartureDate(), formatDate);
    mDepartureAirport = section.getFrom().getLocationCode();
    mArrivalAirport = section.getTo().getLocationCode();
  }

  private String getBrandParam() {
    String brandConfig = Configuration.getInstance().getBrand();
    if (brandConfig.equalsIgnoreCase(Constants.BRAND_EDREAMS)) {
      return VALUE_BRAND_EDREAMS;
    } else if (brandConfig.equalsIgnoreCase(Constants.BRAND_OPODO)) {
      return VALUE_BRAND_OPODO;
    } else if (brandConfig.equalsIgnoreCase(Constants.BRAND_GOVOYAGES)) {
      return VALUE_BRAND_GO_VOYAGES;
    } else {
      return VALUE_BRAND_TRAVELLINK;
    }
  }

  private void openWebView(String url) {
    String title = getTextFromOneCMS(OneCMSKeys.BOARDING_PASS_NAVIGATION_TITLE);
    Intent intent = new Intent(activity.getApplicationContext(),
        ((OdigeoApp) activity.getApplication()).getWebViewActivityClass());
    intent.putExtra(Constants.EXTRA_URL_WEBVIEW, url);
    intent.putExtra(Constants.TITLE_WEB_ACTIVITY, title);
    intent.putExtra(Constants.SHOW_HOME_ICON, false);
    activity.startActivity(intent);
  }

  private String createStringsForUrl() {

    return mPnr
        + mCarrierCode
        + mArrivalAirport
        + mArrivalDateTime
        + mBags
        + mBrand
        + mDepartureAirport
        + mDepartureDateTime
        + mBuyerEmail
        + mFlightNumber
        + mIsFooterShown
        + mIsHeaderShown
        + mBuyerLastName
        + mLocale
        + mBuyerName
        + mSeats
        + VALUE_IFRAME;
  }

  private String getUrlWithParams() {
    String hash = generateHmac(createStringsForUrl(), SECRET_KEY, HMAC256);
    Uri.Builder builder = new Uri.Builder();
    Uri url = Uri.parse(mUrl);
    builder.scheme(HTTP)
        .authority(url.getHost())
        .appendPath(url.getPath())
        .appendQueryParameter(KEY_SOURCE, VALUE_IFRAME)
        .appendQueryParameter(KEY_AIRLINE, mCarrierCode)
        .appendQueryParameter(KEY_BRAND, mBrand)
        .appendQueryParameter(KEY_PNR, mPnr)
        .appendQueryParameter(KEY_EMAIL, mBuyerEmail)
        .appendQueryParameter(KEY_NAME, mBuyerName)
        .appendQueryParameter(KEY_LASTNAME, mBuyerLastName)
        .appendQueryParameter(KEY_LOCALE, mLocale)
        .appendQueryParameter(KEY_FOOTER, mIsFooterShown)
        .appendQueryParameter(KEY_HEADER, mIsHeaderShown)
        .appendQueryParameter(KEY_DEPARTURE_AIRPORT, mDepartureAirport)
        .appendQueryParameter(KEY_ARRIVAL_AIRPORT, mArrivalAirport)
        .appendQueryParameter(KEY_FLIGHT_NUMBER, mFlightNumber)
        .appendQueryParameter(KEY_DEPARTURE_DATE_TIME, mDepartureDateTime)
        .appendQueryParameter(KEY_ARRIVAL_DATE_TIME, mArrivalDateTime)
        .appendQueryParameter(KEY_BAGS, mBags)
        .appendQueryParameter(KEY_SEATS, mSeats)
        .appendQueryParameter(KEY_HMAC, hash);

    return builder.build().toString();
  }
}
