package com.odigeo.app.android.view.custom;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.ListPopupWindow;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.model.BaggageCollectionItemViewModel;
import com.odigeo.app.android.view.BaseCustomWidget;
import com.odigeo.app.android.view.adapters.BaggageOptionsAdapter;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.dialogs.CustomDialogView;
import com.odigeo.app.android.view.interfaces.BaggageCollectionItemListener;
import com.odigeo.data.entity.shoppingCart.BaggageConditions;
import com.odigeo.data.entity.shoppingCart.BaggageDescriptor;
import com.odigeo.data.entity.shoppingCart.SegmentTypeIndex;
import com.odigeo.data.entity.shoppingCart.SelectableBaggageDescriptor;
import com.odigeo.data.entity.shoppingCart.request.BaggageSelectionRequest;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import com.odigeo.presenter.BaggageCollectionItemPresenter;
import com.odigeo.presenter.contracts.views.BaggageCollectionItem;
import java.util.List;

import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_NAGS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_PAX_BAGGAGE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_FLIGHTS_PAX;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_ADD_BAGGAGE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_NAG_BAGGAGE_REMOVAL_APPEARANCES;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_NAG_BAGGAGE_REMOVAL_CANCEL;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_NAG_BAGGAGE_REMOVAL_REMOVE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_REMOVE_BAGGAGE;

public class BaggageCollectionItemView extends BaseCustomWidget<BaggageCollectionItemPresenter>
    implements BaggageCollectionItem {

  private static final String NAG_REMOVE_BAGGAGE_CONTENT_B = "_b";

  private OdigeoImageLoader<ImageView> imageLoader;
  private LinearLayout baggageSelectedView;
  private Button addBaggage;
  private ListPopupWindow baggageOptionsWindow;
  private SelectableBaggageDescriptor selectedItem;
  private BaggageCollectionItemViewModel baggageCollectionItemViewModel;
  private BaggageCollectionItemListener baggageCollectionItemListener;

  public BaggageCollectionItemView(Context context,
      BaggageCollectionItemViewModel baggageCollectionItemViewModel,
      BaggageCollectionItemListener baggageCollectionItemListener,
      BaggageConditions baggageConditions, int preselectedBaggagePosition) {
    super(context);
    this.baggageCollectionItemViewModel = baggageCollectionItemViewModel;
    this.baggageCollectionItemListener = baggageCollectionItemListener;
    presenter.processBaggage(baggageConditions, preselectedBaggagePosition);
  }

  public BaggageCollectionItemView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public void initComponent() {
    imageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();
    baggageSelectedView = (LinearLayout) findViewById(R.id.baggageSelectedView);
    addBaggage = (Button) findViewById(R.id.addBaggage);
    addBaggage.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        baggageOptionsWindow.show();
      }
    });
    baggageOptionsWindow = new ListPopupWindow(getContext());
  }

  @Override public int getComponentLayout() {
    return R.layout.baggage_collection_item;
  }

  @Override public void initOneCMSText() {
    addBaggage.setText(
        localizables.getString(OneCMSKeys.PASSENGER_BAGGAGE_SELECTION).toUpperCase());
  }

  @Override public BaggageCollectionItemPresenter setPresenter() {
    return dependencyInjector.provideBaggageCollectionItemPresenter(this);
  }

  @Override public void showTravelInfo() {
    TextView segmentDirection = (TextView) findViewById(R.id.segmentDirection);
    segmentDirection.setText(baggageCollectionItemViewModel.getSegmentDirection());
    ImageView airlineIcon = (ImageView) findViewById(R.id.airlineIcon);
    imageLoader.load(airlineIcon, String.format("%s%s.png",
        Configuration.getInstance().getImagesSources().getUrlAirlineLogos(),
        baggageCollectionItemViewModel.getCarrierCode()));

    TextView departure = (TextView) findViewById(R.id.departure);
    departure.setText(baggageCollectionItemViewModel.getDeparture());
    TextView arrival = (TextView) findViewById(R.id.arrival);
    arrival.setText(baggageCollectionItemViewModel.getArrival());

    if (baggageCollectionItemViewModel.isRoundTrip()) {
      ImageView arrow = (ImageView) findViewById(R.id.arrow);
      arrow.setImageDrawable(
          ContextCompat.getDrawable(getContext(), R.drawable.search_dobleflecha_h));
    }
  }

  @Override public void showBaggageNotIncluded() {
    TextView baggageNotIncludedInfo = (TextView) findViewById(R.id.baggageNotIncludedInfo);
    baggageNotIncludedInfo.setText(
        localizables.getString(OneCMSKeys.PASSENGER_BAGGAGE_NOT_ALLOWED));
    baggageNotIncludedInfo.setVisibility(VISIBLE);
  }

  @Override public void setBaggageIncludedWithoutKilos(int pieces) {
    String baggageIncludedText;
    if (baggageCollectionItemViewModel.isRoundTrip()) {
      baggageIncludedText =
          localizables.getRawString(OneCMSKeys.PASSENGER_BAGGAGE_INCLUDED_ROUND_TRIP_INFO);
    } else {
      baggageIncludedText =
          localizables.getRawString(OneCMSKeys.PASSENGER_BAGGAGE_INCLUDED_LEG_INFO);
    }
    showBaggageIncluded(String.format(baggageIncludedText, pieces));
  }

  @Override public void setBaggageIncludedWithKilos(int pieces, int kilos) {
    String baggageIncludedText;
    if (baggageCollectionItemViewModel.isRoundTrip()) {
      baggageIncludedText =
          localizables.getRawString(OneCMSKeys.PASSENGER_BAGGAGE_INCLUDED_ROUND_TRIP_INFO);
    } else {
      baggageIncludedText =
          localizables.getRawString(OneCMSKeys.PASSENGER_BAGGAGE_INCLUDED_LEG_INFO);
    }

    String baggageIncludedWithMaxWeight =
        localizables.getRawString(OneCMSKeys.PASSENGER_BAGGAGE_INCLUDED_MAX_WEIGHT);
    showBaggageIncluded(String.format(baggageIncludedText, pieces) + " " + String.format(
        baggageIncludedWithMaxWeight, kilos));
  }

  private void showBaggageIncluded(String includedText) {
    LinearLayout baggageIncludedView = (LinearLayout) findViewById(R.id.baggageIncludedView);
    TextView baggageIncluded = (TextView) findViewById(R.id.baggageIncluded);
    baggageIncluded.setText(localizables.getString(OneCMSKeys.PASSENGER_BAGGAGE_INCLUDED));
    TextView baggageIncludedInfo = (TextView) findViewById(R.id.baggageIncludedInfo);
    baggageIncludedInfo.setText(Html.fromHtml(includedText));
    baggageIncludedView.setVisibility(VISIBLE);
  }

  @Override public void showAddBaggageOption() {
    addBaggage.setVisibility(VISIBLE);
  }

  @Override public void hideAddBaggageOption() {
    addBaggage.setVisibility(GONE);
  }

  @Override public void showBaggageOptions(
      List<SelectableBaggageDescriptor> selectableBaggageDescriptorList) {
    baggageOptionsWindow.setAnchorView(addBaggage);
    final BaggageOptionsAdapter baggageOptionsAdapter =
        new BaggageOptionsAdapter(getContext(), selectableBaggageDescriptorList);
    baggageOptionsWindow.setAdapter(baggageOptionsAdapter);
    baggageOptionsWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        presenter.onBaggageOptionSelected(baggageOptionsAdapter.getItem(position));
        baggageOptionsWindow.dismiss();
        baggageCollectionItemListener.onBaggageSelectionItemChange();
        trackerController.trackAnalyticsEvent(CATEGORY_FLIGHTS_PAX, ACTION_PAX_BAGGAGE,
            LABEL_ADD_BAGGAGE);
      }
    });
  }

  @Override public SelectableBaggageDescriptor getSelectedItem() {
    return selectedItem;
  }

  @Override public void setSelectedItem(SelectableBaggageDescriptor selectedBaggageDescriptor) {
    this.selectedItem = selectedBaggageDescriptor;
  }

  @Override public BaggageSelectionRequest getBaggageSelectionRequest() {
    return presenter.buildBaggageSelectionRequest();
  }

  @Override public void showBaggageSelected(SelectableBaggageDescriptor selectedBaggageDescriptor) {
    baggageSelectedView.setVisibility(VISIBLE);

    TextView babbageInfo = (TextView) findViewById(R.id.babbageInfo);
    TextView baggageComplementaryInfo = (TextView) findViewById(R.id.baggageComplementaryInfo);
    ImageView removeBaggage = (ImageView) findViewById(R.id.removeBaggage);
    removeBaggage.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View view) {
        presenter.onRemoveBaggageClick();
      }
    });

    String price = "+" + LocaleUtils.getLocalizedCurrencyValue(
        selectedBaggageDescriptor.getPrice().doubleValue());
    babbageInfo.setText(
        selectedBaggageDescriptor.getBaggageDescriptor().getPieces() + " (" + price + ")");

    if (selectedBaggageDescriptor.getBaggageDescriptor().getKilos() != null
        && selectedBaggageDescriptor.getBaggageDescriptor().getKilos() > 0) {
      String maxWeight =
          String.format(localizables.getRawString(OneCMSKeys.PASSENGER_BAGGAGE_MAX_WEIGHT),
              selectedBaggageDescriptor.getBaggageDescriptor().getKilos());
      baggageComplementaryInfo.setVisibility(VISIBLE);
      baggageComplementaryInfo.setText(maxWeight);
    }
  }

  @Override public void hideBaggageSelected() {
    baggageSelectedView.setVisibility(GONE);
  }

  @Override public SegmentTypeIndex getSegmentTypeIndex() {
    return presenter.getSegmentTypeIndex();
  }

  @Override public void updateSelection(BaggageDescriptor baggageDescriptor) {
    presenter.updateBaggageSelected(baggageDescriptor);
  }

  @Override public void clearSelection() {
    presenter.onClearBaggageSelected();
  }

  @Override public void showConfirmRemoveBaggage() {
    trackerController.trackAnalyticsEvent(CATEGORY_FLIGHTS_PAX, ACTION_NAGS,
        LABEL_NAG_BAGGAGE_REMOVAL_APPEARANCES);

    CustomDialogView customDialogView = new CustomDialogView(getContext(),
        localizables.getString(OneCMSKeys.BAGGAGE_NAG_REMOVE_TITLE + NAG_REMOVE_BAGGAGE_CONTENT_B),
        localizables.getString(OneCMSKeys.BAGGAGE_NAG_REMOVE_BODY + NAG_REMOVE_BAGGAGE_CONTENT_B),
        localizables.getString(
            OneCMSKeys.BAGGAGE_NAG_REMOVE_BUTTON_POSITIVE + NAG_REMOVE_BAGGAGE_CONTENT_B),
        new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialog, int which) {
            trackerController.trackAnalyticsEvent(CATEGORY_FLIGHTS_PAX, ACTION_NAGS,
                LABEL_NAG_BAGGAGE_REMOVAL_CANCEL);
          }
        }, localizables.getString(
        OneCMSKeys.BAGGAGE_NAG_REMOVE_BUTTON_NEGATIVE + NAG_REMOVE_BAGGAGE_CONTENT_B),
        new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialog, int which) {
            trackerController.trackAnalyticsEvent(CATEGORY_FLIGHTS_PAX, ACTION_NAGS,
                LABEL_NAG_BAGGAGE_REMOVAL_REMOVE);
            presenter.onBaggageSelectedRemoved();
          }
        });
    customDialogView.show();
  }

  @Override public void notifyBaggageSelectionHasChanged() {
    baggageCollectionItemListener.onBaggageSelectionItemChange();
  }

  @Override public void trackBaggageRemoved() {
    trackerController.trackAnalyticsEvent(CATEGORY_FLIGHTS_PAX, ACTION_PAX_BAGGAGE,
        LABEL_REMOVE_BAGGAGE);
  }
}