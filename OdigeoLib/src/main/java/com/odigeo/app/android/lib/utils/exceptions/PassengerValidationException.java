package com.odigeo.app.android.lib.utils.exceptions;

import com.google.gson.Gson;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;

public class PassengerValidationException extends Exception {

  private PassengerValidationException(String detailMessage) {
    super(detailMessage);
  }

  public static PassengerValidationException newInstance(
      CreateShoppingCartResponse shoppingCartResponse) {
    Gson gson = new Gson();
    String json = gson.toJson(shoppingCartResponse);
    return new PassengerValidationException(json);
  }
}
