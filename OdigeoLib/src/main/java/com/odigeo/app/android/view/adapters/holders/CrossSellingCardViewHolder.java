package com.odigeo.app.android.view.adapters.holders;

import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.view.adapters.CrossSellingCardsAdapter;
import com.odigeo.presenter.model.CrossSellingCard;
import com.odigeo.presenter.model.CrossSellingType;
import java.util.List;

import static com.odigeo.app.android.lib.utils.ViewUtils.findById;

public class CrossSellingCardViewHolder extends BaseRecyclerViewHolder<CrossSellingCard> {

  private final TextView labelTV;
  private final ImageView iconIV;
  private final TextView titleTV;
  private final TextView subTitleTV;
  private final View progressBarContainer;
  private final CrossSellingCardsAdapter.OnCrossSellItemSelected onCrossSellItemSelected;

  public CrossSellingCardViewHolder(View itemView,
      CrossSellingCardsAdapter.OnCrossSellItemSelected onCrossSellItemSelected) {
    super(itemView);
    this.labelTV = findById(itemView, R.id.xsell_item_label);
    this.iconIV = findById(itemView, R.id.xsell_item_icon);
    this.titleTV = findById(itemView, R.id.xsell_item_title);
    this.subTitleTV = findById(itemView, R.id.xsell_item_subtitle);
    this.progressBarContainer = findById(itemView, R.id.xsell_item_progress_container);
    this.onCrossSellItemSelected = onCrossSellItemSelected;
  }

  @Override public void bind(int position, final CrossSellingCard item) {
    String label = item.getLabel();
    if (label != null && !label.isEmpty() && item.isShowLabel()) {
      labelTV.setText(label);
      labelTV.setVisibility(View.VISIBLE);
    } else {
      labelTV.setVisibility(View.INVISIBLE);
    }

    @DrawableRes int iconResId;
    switch (item.getType()) {
      case CrossSellingType.TYPE_HOTEL:
        iconResId = R.drawable.ic_hotel;
        break;
      case CrossSellingType.TYPE_CARS:
        iconResId = R.drawable.ic_rent_car;
        break;
      case CrossSellingType.TYPE_GROUND:
        iconResId = R.drawable.ic_ground_transportation;
        break;
      default:
        iconResId = R.drawable.ic_city_guide_default;
        break;
    }

    iconIV.setImageDrawable(ContextCompat.getDrawable(getContext(), iconResId));
    titleTV.setText(item.getTitle());
    subTitleTV.setText(item.getSubtitle());

    itemView.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        onCrossSellItemSelected.onCrossSellingItemSelected(item);
      }
    });
  }

  @Override public void bind(int position, CrossSellingCard item, @Nullable List<Object> payloads) {
    bind(position, item);

    if (payloads != null && !payloads.isEmpty()) {
      CrossSellingCardsAdapter.ProgressPayload progressPayload =
          (CrossSellingCardsAdapter.ProgressPayload) payloads.get(0);
      progressBarContainer.setVisibility(progressPayload.finished ? View.INVISIBLE : View.VISIBLE);
    }
  }
}
