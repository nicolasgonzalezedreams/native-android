package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author miguel
 */
public class SearchResultsPageDTO implements Serializable {

  protected List<FareItineraryDTO> itineraryResults;
  protected ItinerariesLegendDTO legend;
  protected long searchId;
  protected int startIndex;
  protected CabinClassDTO cabinClass;
  protected ItinerarySortCriteriaDTO sortCriteria;
  protected ItineraryFullPriceLabelTypeDTO fullpriceLabelType;
  protected RepricingInfoResponseDTO repricingInfoResponse;
  protected BookingFeeSearchResultDTO bookingSearchResultLegend;
  protected boolean fullTransparency;

  /**
   * Gets the value of the legend property.
   *
   * @return possible object is {@link ItinerariesLegend }
   */
  public ItinerariesLegendDTO getLegend() {
    return legend;
  }

  /**
   * Sets the value of the legend property.
   *
   * @param value allowed object is {@link ItinerariesLegend }
   */
  public void setLegend(ItinerariesLegendDTO value) {
    this.legend = value;
  }

  /**
   * Gets the value of the searchId property.
   */
  public long getSearchId() {
    return searchId;
  }

  /**
   * Sets the value of the searchId property.
   */
  public void setSearchId(long value) {
    this.searchId = value;
  }

  /**
   * Gets the value of the startIndex property.
   */
  public int getStartIndex() {
    return startIndex;
  }

  /**
   * Sets the value of the startIndex property.
   */
  public void setStartIndex(int value) {
    this.startIndex = value;
  }

  /**
   * Gets the value of the cabinClass property.
   *
   * @return possible object is {@link com.odigeo.app.android.lib.models.CabinClass }
   */
  public CabinClassDTO getCabinClass() {
    return cabinClass;
  }

  /**
   * Sets the value of the cabinClass property.
   *
   * @param value allowed object is {@link com.odigeo.app.android.lib.models.CabinClass }
   */
  public void setCabinClass(CabinClassDTO value) {
    this.cabinClass = value;
  }

  /**
   * Gets the value of the sortCriteria property.
   *
   * @return possible object is {@link ItinerarySortCriteria }
   */
  public ItinerarySortCriteriaDTO getSortCriteria() {
    return sortCriteria;
  }

  /**
   * Sets the value of the sortCriteria property.
   *
   * @param value allowed object is {@link ItinerarySortCriteria }
   */
  public void setSortCriteria(ItinerarySortCriteriaDTO value) {
    this.sortCriteria = value;
  }

  /**
   * @return the itineraryResults
   */
  public List<FareItineraryDTO> getItineraryResults() {
    return itineraryResults;
  }

  /**
   * @param itineraryResults the itineraryResults to set
   */
  public void setItineraryResults(List<FareItineraryDTO> itineraryResults) {
    this.itineraryResults = itineraryResults;
  }

  /**
   * @return the ItineraryFullPriceLabelTypeDTO
   */
  public ItineraryFullPriceLabelTypeDTO getFullpriceLabelType() {
    return fullpriceLabelType;
  }

  /**
   * @param ItineraryFullPriceLabelTypeDTO the FullPriceLabelToSet to set
   */
  public void setFullpriceLabelType(ItineraryFullPriceLabelTypeDTO fullpriceLabelType) {
    this.fullpriceLabelType = fullpriceLabelType;
  }

  /**
   * @return the RepricingInfoResponseDTO
   */
  public RepricingInfoResponseDTO getRepricingInfoResponse() {
    return repricingInfoResponse;
  }

  /**
   * @param RepricingInfoResponseDTO the RepricingInfoResponse to set
   */
  public void setRepricingInfoResponse(RepricingInfoResponseDTO repricingInfoResponse) {
    this.repricingInfoResponse = repricingInfoResponse;
  }

  /**
   * @return the BookingFeeSearchResultDTO
   */
  public BookingFeeSearchResultDTO getBookingSearchResultLegend() {
    return bookingSearchResultLegend;
  }

  /**
   * @param BookingFeeSearchResultDTO the BookingFeeSearchResult to set
   */
  public void setBookingSearchResultLegend(BookingFeeSearchResultDTO bookingSearchResultLegend) {
    this.bookingSearchResultLegend = bookingSearchResultLegend;
  }

  public boolean isFullTransparency() {
    return fullTransparency;
  }

  public void setFullTransparency(boolean fullTransparency) {
    this.fullTransparency = fullTransparency;
  }
}
