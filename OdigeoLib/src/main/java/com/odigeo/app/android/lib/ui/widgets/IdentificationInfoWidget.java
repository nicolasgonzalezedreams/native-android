package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.PassengerIdentification;
import com.odigeo.app.android.lib.models.dto.BuyerIdentificationTypeDTO;
import com.odigeo.app.android.lib.utils.Validations;
import com.odigeo.data.entity.booking.Country;
import java.util.Date;

/**
 * Created by Pablo on 25/10/2014.
 */

public class IdentificationInfoWidget extends ScrollView implements View.OnClickListener {

  private FormOdigeoLayoutWithIcon formIdentificationWidget;
  private EditTextForm wdgtEditNumber;
  private ButtonDateForm wdgtDateExpirationPicker;
  private ButtonCountry wdgtCountry;

  //    private Button wdgtClear;

  private PassengerIdentification passengerIdentification;

  public IdentificationInfoWidget(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray aAttrs =
        context.obtainStyledAttributes(attrs, R.styleable.IdentificationInfoWidget, 0, 0);

    if (!isInEditMode()) {
      initialize(context);

      String title = aAttrs.getString(R.styleable.IdentificationInfoWidget_title);

      if (title != null) {
        formIdentificationWidget.setTitle(
            LocalizablesFacade.getString(getContext(), title).toString());
      }

      wdgtEditNumber.setValidationFormat(
          aAttrs.getInteger(R.styleable.IdentificationInfoWidget_fieldValidationFormat,
              Validations.VALIDATION_NONE));

      wdgtDateExpirationPicker.setMinDate(new Date());
      wdgtDateExpirationPicker.setOKLabel(
          LocalizablesFacade.getString(getContext(), "passengerpicker_okbtn_titlelabel_text")
              .toString());
    }

    aAttrs.recycle();
  }

  private void initialize(Context context) {
    inflate(context, R.layout.layout_identification_info_widget, this);
    findViews();
    this.setClickable(true);
  }

  private void findViews() {

    formIdentificationWidget =
        (FormOdigeoLayoutWithIcon) getRootView().findViewById(R.id.formIdentificationWidget);
    wdgtEditNumber = (EditTextForm) getRootView().findViewById(R.id.wdgtEditNumber);
    wdgtCountry = (ButtonCountry) getRootView().findViewById(R.id.wdgtTxtCountry);
    wdgtDateExpirationPicker =
        (ButtonDateForm) getRootView().findViewById(R.id.wdgtDateExpirationPicker);
    formIdentificationWidget.setOnClickIconListener(this);
  }

  @Override public final void onClick(View v) {
    wdgtEditNumber.setText("");
    wdgtCountry.setCountry(null);
    wdgtCountry.clear();
    wdgtDateExpirationPicker.setDate(null);
  }

  private void showPassengerInfo() {

    if (passengerIdentification != null) {

      if (passengerIdentification.getNumber() != null && wdgtEditNumber != null) {
        wdgtEditNumber.setText(passengerIdentification.getNumber());
      }

      if (passengerIdentification.getExpirationDate() != null && wdgtDateExpirationPicker != null) {
        wdgtDateExpirationPicker.setDate(passengerIdentification.getExpirationDate());
      }

      if (passengerIdentification.getCountry() != null && wdgtCountry != null) {
        wdgtCountry.setCountry(passengerIdentification.getCountry());
      }
    }
  }

  public final PassengerIdentification getPassengerIdentification() {
    PassengerIdentification passengerIdentification = null;

    if (!isEmpty()) {
      passengerIdentification = new PassengerIdentification();

      passengerIdentification.setNumber(wdgtEditNumber.getText());
      passengerIdentification.setCountry(wdgtCountry.getCountry());
      passengerIdentification.setExpirationDate(wdgtDateExpirationPicker.getDate());

      if (wdgtEditNumber.getValidationFormat() == Validations.VALIDATION_NIE) {
        passengerIdentification.setIdentificationType(BuyerIdentificationTypeDTO.NIE);
      } else if (wdgtEditNumber.getValidationFormat() == Validations.VALIDATION_CIF) {
        passengerIdentification.setIdentificationType(BuyerIdentificationTypeDTO.CIF);
      } else if (wdgtEditNumber.getValidationFormat() == Validations.VALIDATION_PASSPORT) {
        passengerIdentification.setIdentificationType(BuyerIdentificationTypeDTO.PASSPORT);
      } else if (wdgtEditNumber.getValidationFormat() == Validations.VALIDATION_NIF) {
        passengerIdentification.setIdentificationType(BuyerIdentificationTypeDTO.NIF);
      } else if (wdgtEditNumber.getValidationFormat() == Validations.VALIDATION_NIE) {
        passengerIdentification.setIdentificationType(BuyerIdentificationTypeDTO.NIE);
      } else if (wdgtEditNumber.getValidationFormat() == Validations.VALIDATION_NATIONAL_ID_CARD) {
        passengerIdentification.setIdentificationType(BuyerIdentificationTypeDTO.NATIONAL_ID_CARD);
      } else if (wdgtEditNumber.getValidationFormat() == Validations.VALIDATION_BIRTH_DATE) {
        passengerIdentification.setIdentificationType(BuyerIdentificationTypeDTO.BIRTH_DATE);
      }
    }

    return passengerIdentification;
  }

  public final void setPassengerIdentification(PassengerIdentification passengerIdentification) {
    this.passengerIdentification = passengerIdentification;
    showPassengerInfo();
  }

  public final boolean validate() {
    return isEmpty() || (wdgtEditNumber.validate()
        && wdgtDateExpirationPicker.validate()
        && wdgtCountry.validate());
  }

  public final boolean isEmpty() {
    if (wdgtEditNumber.getText() == null) {
      if (wdgtCountry.getCountry() == null && wdgtDateExpirationPicker.getDate() == null) {
        return true;
      }
    }

    return false;
  }

  public final Country getCountry() {
    return wdgtCountry.getCountry();
  }

  public final void setCountry(Country country) {
    wdgtCountry.setCountry(country);
  }

  /**
   * Gets the identification number, null if the text view is empty
   *
   * @return Return the identification number, null if its empty
   */
  @Nullable public final String getNumber() {
    return wdgtEditNumber.getText();
  }

  public final void setNumber(String number) {
    wdgtEditNumber.setText(number);
  }

  public final Date getExpirationDate() {
    return wdgtDateExpirationPicker.getDate();
  }

  public final void setExpirationDate(Date expirationDate) {
    wdgtDateExpirationPicker.setDate(expirationDate);
  }

  public final String getTitle() {
    return formIdentificationWidget.getTitle();
  }

  public final void setTitle(String title) {
    formIdentificationWidget.setTitle(title);
  }

  public final ButtonCountry getWdgtCountry() {
    return wdgtCountry;
  }
}
