package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.ResidentItinerary;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.data.entity.geo.City;

/**
 * Created by Oscar Álvarez on 17/11/14.
 */
public class ResidentsWidget extends LinearLayout {
  private SwitchWidget switchResidents;
  private LinearLayout containerResidentMessages;

  private OnResidentSwitchListener listener;

  public ResidentsWidget(Context context, AttributeSet attrs) {
    super(context, attrs);

    initialize(context);

    switchResidents.setListener(new SwitchWidget.SwitchListener() {
      @Override public void onSwitchOn() {
        notifyResidentSwitchChange(true);
      }

      @Override public void onSwitchOff() {
        notifyResidentSwitchChange(false);
      }
    });
  }

  public final void setResidentItinerary(ResidentItinerary residentItinerary) {
    if (residentItinerary != null) {
      switchResidents.setLabel(
          LocalizablesFacade.getString(getContext(), "searchviewcontroller_switchcelltitle_text",
              residentItinerary.getResidentPlace()));
    }
  }

  private void initialize(Context context) {
    inflate(context, R.layout.layout_residents, this);

    switchResidents = (SwitchWidget) findViewById(R.id.switchResidents);
    containerResidentMessages = (LinearLayout) findViewById(R.id.containerResidentMessages);
  }

  public final void setCity(City city) {
    switchResidents.setLabel(
        LocalizablesFacade.getString(getContext(), "searchviewcontroller_switchcelltitle_text",
            city.getCityName()));
  }

  public final SwitchWidget getSwitchWidget() {

    return switchResidents;
  }

  public final ConfirmationInfoItem getSecondText() {
    return (ConfirmationInfoItem) findViewById(R.id.residentMessage2);
  }

  public final ConfirmationInfoItem getFirstText() {
    return (ConfirmationInfoItem) findViewById(R.id.residentMessage1);
  }

  public void setResident(boolean isResident) {
    if (isResident) {
      containerResidentMessages.setVisibility(View.VISIBLE);
    } else {
      containerResidentMessages.setVisibility(View.GONE);
    }

    getSwitchWidget().setChecked(isResident);
  }

  private void notifyResidentSwitchChange(boolean isResident) {
    setResident(isResident);

    if (listener != null) {
      listener.onResidentSwitchClicked(isResident);
    }
  }

  public void setOnResidentSwitchListener(OnResidentSwitchListener onResidentSwitchListener) {
    this.listener = onResidentSwitchListener;
  }

  public interface OnResidentSwitchListener {
    void onResidentSwitchClicked(boolean checked);
  }
}
