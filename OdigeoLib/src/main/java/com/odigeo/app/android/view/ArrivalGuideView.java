package com.odigeo.app.android.view;

import android.content.Intent;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Toast;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.view.adapters.GuidesPagerAdapter;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.presenter.ArrivalGuidePresenter;
import com.odigeo.presenter.contracts.navigators.ArrivalGuideViewerNavigatorInterface;
import com.odigeo.presenter.contracts.views.ArrivalGuideViewInterface;
import java.io.File;
import java.io.IOException;

/**
 * Created by matias.dirusso on 12/15/2015.
 */
public class ArrivalGuideView extends BaseView<ArrivalGuidePresenter>
    implements ArrivalGuideViewInterface {

  private static final String EXTRA_BOOKING = "booking";
  private ViewPager mPager;
  private int mGuidePagesAmount;
  private Toast mToast;
  ViewPager.OnPageChangeListener changeListener = new ViewPager.OnPageChangeListener() {
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override public void onPageSelected(int position) {
      displayCurrentPageCount(position);
    }

    @Override public void onPageScrollStateChanged(int state) {
    }
  };

  public static ArrivalGuideView getInstance(Booking booking) {
    ArrivalGuideView view = new ArrivalGuideView();
    Bundle args = new Bundle();
    args.putSerializable(EXTRA_BOOKING, booking);
    view.setArguments(args);
    return view;
  }

  @Override protected ArrivalGuidePresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideArrivalGuidesPresenter(this, (ArrivalGuideViewerNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.guides_view_pager;
  }

  @Override protected void initComponent(View view) {

    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

      mPresenter.showArrivalGuideExternal(getBooking(), Environment.getExternalStorageDirectory(),
          getActivity().getPackageName());
    } else {

      mPager = (ViewPager) view.findViewById(R.id.view_pager_guides);
      mPresenter.showArrivalGuideInApp(getBooking(), Environment.getExternalStorageDirectory(),
          getActivity().getPackageName());
    }
  }

  public void initPdfViewer(File pdfFile) {

    try {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        PdfRenderer renderer = new PdfRenderer(
            ParcelFileDescriptor.open(pdfFile, ParcelFileDescriptor.MODE_READ_ONLY));
        mGuidePagesAmount = renderer.getPageCount();

        GuidesPagerAdapter guidesPagerAdapter =
            new GuidesPagerAdapter(renderer, ArrivalGuideView.this, mGuidePagesAmount);

        mPager.setAdapter(guidesPagerAdapter);
        mPager.addOnPageChangeListener(changeListener);
        displayCurrentPageCount(0);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void displayCurrentPageCount(int position) {
    if (mToast != null) {
      mToast.cancel();
    }
    mToast = Toast.makeText(getActivity().getApplicationContext(),
        (position + 1) + "/" + mGuidePagesAmount, Toast.LENGTH_SHORT);
    mToast.show();
  }

  public void showPdfInExternalApp(File pdfFile) {

    Uri path = Uri.fromFile(pdfFile);
    Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
    pdfIntent.setDataAndType(path, "application/pdf");
    pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(pdfIntent);
    getActivity().finish();
  }

  @Override protected void setListeners() {
  }

  @Override protected void initOneCMSText(View view) {

  }

  private Booking getBooking() {
    return (Booking) getArguments().getSerializable(EXTRA_BOOKING);
  }
}
