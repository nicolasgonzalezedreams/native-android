package com.odigeo.app.android.lib.utils;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Converts bold elements inside a String into styled words.
 *
 * @author cristian.fanjul
 * @since 18/03/2015
 */
public class StyledBoldString {

  private static final String OPEN_BOLD_TAG = "<b>";
  private static final String CLOSE_BOLD_TAG = "</b>";
  private final Context context;
  private final String value;
  private final int style;

  /**
   * Converts bold elements inside a String into styled words.
   *
   * @param contextWeakReference Context.
   * @param value String format.
   * @param style Resource.
   */
  public StyledBoldString(WeakReference<Context> contextWeakReference, String value, int style) {
    this.context = contextWeakReference.get();
    this.value = value;
    this.style = style;
  }

  /**
   * Indexes with bold words are taken and a style from a resource is applied.
   */
  public final Spannable getSpannable() {
    List<BoldWord> boldWordsList = getIndexes();

    SpannableString result =
        new SpannableString(value.replace(OPEN_BOLD_TAG, "").replace(CLOSE_BOLD_TAG, ""));

    for (BoldWord boldWord : boldWordsList) {
      result.setSpan(new TextAppearanceSpan(context, style), boldWord.getStartIndex(),
          boldWord.getEndIndex(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    return result;
  }

  /**
   * Finds the indexes of bold words and puts them into a list.
   */
  private List<BoldWord> getIndexes() {
    String modifiedValue = value;
    List<BoldWord> boldWordsList = new ArrayList<BoldWord>();
    boolean endOfString = false;

    while (!endOfString) {
      BoldWord boldWord = new BoldWord();

      if (modifiedValue.indexOf(OPEN_BOLD_TAG) != -1) {
        boldWord.setStartIndex(modifiedValue.indexOf(OPEN_BOLD_TAG));
        modifiedValue = modifiedValue.replaceFirst(OPEN_BOLD_TAG, "");
      }
      if (modifiedValue.indexOf(CLOSE_BOLD_TAG) != -1) {
        boldWord.setEndIndex(modifiedValue.indexOf(CLOSE_BOLD_TAG));
        modifiedValue = modifiedValue.replaceFirst(CLOSE_BOLD_TAG, "");
        boldWordsList.add(boldWord);
      } else {
        endOfString = true;
      }
    }
    return boldWordsList;
  }

  /**
   * Start and end indexes for a bold word in a string are allowed to save in this object.
   */
  public class BoldWord {
    int startIndex = -1;
    int endIndex = -1;

    /**
     * Start and end indexes for a bold word in a string are allowed to save in this object
     */
    public BoldWord() {

    }

    public final int getStartIndex() {
      return startIndex;
    }

    public final void setStartIndex(int startIndex) {
      this.startIndex = startIndex;
    }

    public final int getEndIndex() {
      return endIndex;
    }

    public final void setEndIndex(int endIndex) {
      this.endIndex = endIndex;
    }
  }
}
