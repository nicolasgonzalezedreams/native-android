package com.odigeo.app.android.navigator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoNoConnectionActivity;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.model.BookingInfoViewModel;
import com.odigeo.app.android.view.InsurancesView;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.shoppingCart.AvailableProductsResponse;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.FlowConfigurationResponse;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.contracts.navigators.InsurancesNavigatorInterface;

public class InsurancesNavigator extends BaseNavigator implements InsurancesNavigatorInterface {

  private static final int PAYMENT_LAUNCH_CODE = 3;

  private InsurancesView mInsurancesView;

  private CreateShoppingCartResponse mCreateShoppingCartResponse;
  private AvailableProductsResponse mAvailableProductsResponse;
  private FlowConfigurationResponse mFlowConfigurationResponse;
  private boolean mIsFullTransparency;
  private double mLastTicketsPrice;
  private SearchOptions mSearchOptions;
  private BookingInfoViewModel bookingInfo;

  public Fragment getInstanceInsurancesView() {
    if (mInsurancesView == null) {
      mInsurancesView =
          InsurancesView.newInstance(mCreateShoppingCartResponse, mAvailableProductsResponse,
              mFlowConfigurationResponse, mIsFullTransparency, mLastTicketsPrice, mSearchOptions,
              bookingInfo);
      mInsurancesView.setArguments(getIntent().getExtras());
    }
    return mInsurancesView;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view);
    mCreateShoppingCartResponse = (CreateShoppingCartResponse) getIntent().getSerializableExtra(
        Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE);
    mAvailableProductsResponse = (AvailableProductsResponse) getIntent().getSerializableExtra(
        Constants.EXTRA_AVAILABLE_PRODUCTS_RESPONSE);
    mFlowConfigurationResponse = (FlowConfigurationResponse) getIntent().getSerializableExtra(
        Constants.EXTRA_FLOW_CONFIGURATION_RESPONSE);
    mIsFullTransparency = getIntent().getBooleanExtra(Constants.EXTRA_FULL_TRANSPARENCY, false);
    mLastTicketsPrice = getIntent().getDoubleExtra(Constants.EXTRA_REPRICING_TICKETS_PRICES, 0);
    mSearchOptions =
        (SearchOptions) getIntent().getSerializableExtra(Constants.EXTRA_SEARCH_OPTIONS);
    bookingInfo = (BookingInfoViewModel) getIntent().getSerializableExtra(
        Constants.EXTRA_BOOKING_INFO);

    replaceFragment(R.id.fl_container, getInstanceInsurancesView());
    setNavigationTitle(
        LocalizablesFacade.getString(this, OneCMSKeys.INSURANCESVIEWCONTROLLER_TITLE).toString());
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    switch (requestCode) {
      case PAYMENT_LAUNCH_CODE:
        if (resultCode == Activity.RESULT_OK) {
          mCreateShoppingCartResponse = (CreateShoppingCartResponse) data.getSerializableExtra(
              Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE);
          mInsurancesView.updateCreateShoppingCartResponse(mCreateShoppingCartResponse);
        }
        break;
    }
  }

  @Override public void onBackPressed() {
    Intent resultIntent = new Intent();
    resultIntent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE,
        mCreateShoppingCartResponse);
    setResult(Activity.RESULT_OK, resultIntent);
    super.onBackPressed();
  }

  @Override public void navigateToSummary() {
    OdigeoApp odigeoApp = (OdigeoApp) getApplication();
    Intent intent = new Intent(this, odigeoApp.getSummaryActivityClass());
    intent.putExtra(Constants.EXTRA_STEP_TO_SUMMARY, Step.INSURANCE);
    intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, mSearchOptions);
    intent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE, mCreateShoppingCartResponse);
    intent.putExtra(Constants.EXTRA_FULL_TRANSPARENCY, mIsFullTransparency);
    intent.putExtra(Constants.EXTRA_BOOKING_INFO, bookingInfo);
    startActivity(intent);
  }

  @Override public void navigateToPayment(CreateShoppingCartResponse createShoppingCartResponse,
      double currentInsurancePrice) {
    mCreateShoppingCartResponse.setPricingBreakdown(
        createShoppingCartResponse.getPricingBreakdown());
    mCreateShoppingCartResponse.getShoppingCart()
        .setTotalPrice(createShoppingCartResponse.getShoppingCart().getTotalPrice());
    mCreateShoppingCartResponse.getShoppingCart()
        .getOtherProductsShoppingItems()
        .setInsuranceShoppingItems(createShoppingCartResponse.getShoppingCart()
            .getOtherProductsShoppingItems()
            .getInsuranceShoppingItems());

    Intent intent = new Intent(this, PaymentNavigator.class);
    intent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE, mCreateShoppingCartResponse);
    intent.putExtra(Constants.EXTRA_FULL_TRANSPARENCY, mIsFullTransparency);
    intent.putExtra(Constants.EXTRA_REPRICING_TICKETS_PRICES, mLastTicketsPrice);
    intent.putExtra(Constants.EXTRA_REPRICING_INSURANCES, currentInsurancePrice);
    intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, mSearchOptions);
    intent.putExtra(Constants.EXTRA_BOOKING_INFO, bookingInfo);
    startActivityForResult(intent, PAYMENT_LAUNCH_CODE);
  }

  @Override public void navigateToNoConnectionActivityFromRemoveInsuranceRequest() {
    tracker.trackAnalyticsScreen(TrackerConstants.SCREEN_ERROR_NO_CONNECTION);
    OdigeoNoConnectionActivity.launch(this, Constants.REQUEST_CODE_REMOVEPRODUCTLISTENER);
  }

  @Override public void navigateToNoConnectionActivityFromAddInsurancesRequest() {
    tracker.trackAnalyticsScreen(TrackerConstants.SCREEN_ERROR_NO_CONNECTION);
    OdigeoNoConnectionActivity.launch(this, Constants.REQUEST_CODE_ADDPRODUCTLISTENER);
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_goto_summary, menu);
    MenuItem item = menu.findItem(R.id.menu_item_goto_summary);
    item.setTitle(LocalizablesFacade.getString(this, OneCMSKeys.COMMON_TRIP_DETAILS).toString());
    return super.onCreateOptionsMenu(menu);
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {
    int i = item.getItemId();
    if (i == R.id.menu_item_goto_summary) {
      tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_INSURANCE,
          TrackerConstants.ACTION_FLIGHT_SUMMARY, TrackerConstants.LABEL_OPEN_FLIGHT_SUMMARY);
      navigateToSummary();
      return true;
    } else if (i == android.R.id.home) {
      tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_INSURANCE,
          TrackerConstants.ACTION_NAVIGATION_ELEMENTS, TrackerConstants.LABEL_GO_BACK);

      onBackPressed();
      return true;
    } else {
      return super.onOptionsItemSelected(item);
    }
  }
}