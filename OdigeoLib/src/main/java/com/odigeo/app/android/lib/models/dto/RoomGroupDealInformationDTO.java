package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class RoomGroupDealInformationDTO {

  protected AccommodationDealPriceCalculatorAdapterDTO price;
  protected String cancellationPolicy;
  protected BigDecimal providerPrice;
  protected BigDecimal providerTaxes;
  protected String providerCurrency;

  public AccommodationDealPriceCalculatorAdapterDTO getPrice() {
    return price;
  }

  public void setPrice(AccommodationDealPriceCalculatorAdapterDTO value) {
    this.price = value;
  }

  public String getCancellationPolicy() {
    return cancellationPolicy;
  }

  public void setCancellationPolicy(String value) {
    this.cancellationPolicy = value;
  }

  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  public void setProviderPrice(BigDecimal value) {
    this.providerPrice = value;
  }

  public BigDecimal getProviderTaxes() {
    return providerTaxes;
  }

  public void setProviderTaxes(BigDecimal value) {
    this.providerTaxes = value;
  }

  public String getProviderCurrency() {
    return providerCurrency;
  }

  public void setProviderCurrency(String value) {
    this.providerCurrency = value;
  }
}
