package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class InsuranceBookingCoveragePassengerPriceDTO {

  protected TravellerTypeDTO travellerType;
  protected BigDecimal coverageImport;
  protected BigDecimal providerPrice;
  protected Integer paxNum;

  /**
   * Gets the value of the travellerType property.
   *
   * @return possible object is
   * {@link com.odigeo.app.android.lib.models.dto.TravellerTypeDTO }
   */
  public TravellerTypeDTO getTravellerType() {
    return travellerType;
  }

  /**
   * Sets the value of the travellerType property.
   *
   * @param value allowed object is
   * {@link com.odigeo.app.android.lib.models.dto.TravellerTypeDTO }
   */
  public void setTravellerType(TravellerTypeDTO value) {
    this.travellerType = value;
  }

  /**
   * Gets the value of the coverageImport property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getCoverageImport() {
    return coverageImport;
  }

  /**
   * Sets the value of the coverageImport property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setCoverageImport(BigDecimal value) {
    this.coverageImport = value;
  }

  /**
   * Gets the value of the providerPrice property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  /**
   * Sets the value of the providerPrice property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setProviderPrice(BigDecimal value) {
    this.providerPrice = value;
  }

  /**
   * Gets the value of the paxNum property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getPaxNum() {
    return paxNum;
  }

  /**
   * Sets the value of the paxNum property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setPaxNum(Integer value) {
    this.paxNum = value;
  }
}
