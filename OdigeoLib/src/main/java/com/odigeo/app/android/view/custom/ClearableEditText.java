package com.odigeo.app.android.view.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.odigeo.app.android.lib.R;

/**
 * View to wrap a {@link android.support.design.widget.TextInputLayout}, a {@link
 * android.widget.EditText}, and a {@link android.widget.ImageView} to make clearable the EditText.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 02/10/15
 */
public class ClearableEditText extends FrameLayout {
  private TextInputLayout mTextInputLayout;
  private EditText mEditText;
  private ImageView mImageView;
  private OnEditTextClickListener mOnEditTextClickListener;
  private OnClearClickListener mOnClearClickListener;
  private OnClickListener mOnClickListener;

  public ClearableEditText(Context context) {
    super(context);
    initialize(context);
  }

  public ClearableEditText(Context context, AttributeSet attrs) {
    super(context, attrs);
    initialize(context);
  }

  public ClearableEditText(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    initialize(context);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public ClearableEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
    initialize(context);
  }

  private void initialize(Context context) {
    View root = LayoutInflater.from(context).inflate(R.layout.clearable_edit_text, this, false);

    mTextInputLayout = ((TextInputLayout) root.findViewById(R.id.text_input_layout));
    mEditText = mTextInputLayout.getEditText();
    mImageView = ((ImageView) root.findViewById(R.id.image_view));
    addView(root);

    mEditText.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        if (mOnEditTextClickListener != null) {
          mOnEditTextClickListener.onClick(mEditText);
        }
      }
    });
    mEditText.addTextChangedListener(new TextWatcher() {
      @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // Nothing
      }

      @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
        showClear();
      }

      @Override public void afterTextChanged(Editable s) {
        // Nothing
      }
    });
    mImageView.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        if (mOnClearClickListener != null) {
          mOnClearClickListener.onClick();
        }
      }
    });
  }

  public void setHint(CharSequence hint) {
    mTextInputLayout.setHint(hint);
  }

  public void setError(CharSequence error) {
    mTextInputLayout.setError(error);
  }

  public EditText getEditText() {
    return mEditText;
  }

  public void setText(CharSequence text) {
    mEditText.setText(text);
    showClear();
  }

  private void showClear() {
    if (TextUtils.isEmpty(mEditText.getText().toString())) {
      mImageView.setVisibility(View.GONE);
    } else {
      mImageView.setVisibility(View.VISIBLE);
    }
  }

  public void setOnEditTextClickListener(OnEditTextClickListener onEditTextClickListener) {
    this.mOnEditTextClickListener = onEditTextClickListener;
  }

  public void setOnClearClickListener(OnClearClickListener onClearClickListener) {
    this.mOnClearClickListener = onClearClickListener;
  }

  public void setOnClickListener(OnClickListener onClickListener) {
    this.mOnClickListener = onClickListener;
  }

  public interface OnEditTextClickListener {
    void onClick(EditText editText);
  }

  public interface OnClearClickListener {
    void onClick();
  }
}
