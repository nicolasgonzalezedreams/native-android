package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;

/**
 * Created by Irving on 07/10/2014.
 */
public class FooterExpandableWidget extends RelativeLayout implements View.OnClickListener {
  private boolean isOpen;
  private String labelShowMore;
  private String labelShowLess;

  private ImageView expandableImageView;
  private TextView expandableTextView;

  private View dynamicLayout;

  public FooterExpandableWidget(Context context, View dynamicLayout) {
    super(context);
    this.dynamicLayout = dynamicLayout;
    inflateLayout();
  }

  public FooterExpandableWidget(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray typedArray =
        context.obtainStyledAttributes(attrs, R.styleable.FooterExpandableWidget, 0, 0);
    labelShowMore = typedArray.getString(R.styleable.FooterExpandableWidget_labelShowMore);
    labelShowLess = typedArray.getString(R.styleable.FooterExpandableWidget_labelShowLess);
    typedArray.recycle();
    inflateLayout();
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.widget_footer_expandable, this);
    expandableTextView = (CustomTextView) findViewById(R.id.text_footer_expandable);
    expandableImageView = (ImageView) findViewById(R.id.image_footer_expandable);
    isOpen = false;
    this.setOnClickListener(this);
    if (labelShowMore != null) {
      expandableTextView.setText(LocalizablesFacade.getString(getContext(), labelShowMore));
    }
    hideDynamicLayout();
  }

  @Override public final void onClick(View v) {
    isOpen = !isOpen;
    if (isOpen) {
      postGAEventLabelPaymentInfoOpen();

      dynamicLayout.setVisibility(VISIBLE);
      expandableImageView.setImageResource(R.drawable.less_info);
      expandableTextView.setText(LocalizablesFacade.getString(getContext(), labelShowLess));
    } else {
      dynamicLayout.setVisibility(GONE);
      expandableImageView.setImageResource(R.drawable.more_info);
      expandableTextView.setText(LocalizablesFacade.getString(getContext(), labelShowMore));
    }
  }

  private void hideDynamicLayout() {
    if (dynamicLayout != null) {
      isOpen = false;
      dynamicLayout.setVisibility(GONE);
    }
  }

  private void showDynamicLayout() {
    if (dynamicLayout != null) {
      isOpen = true;
      dynamicLayout.setVisibility(VISIBLE);
    }
  }

  public final void setLabelShowMore(String labelShowMore) {
    this.labelShowMore = labelShowMore;
  }

  public final void setLabelShowLess(String labelShowLess) {
    this.labelShowLess = labelShowLess;
  }

  public final void setDynamicLayout(View dynamicLayout) {
    this.dynamicLayout = dynamicLayout;
    if (isOpen) {
      showDynamicLayout();
    } else {
      hideDynamicLayout();
    }
  }

  private void postGAEventLabelPaymentInfoOpen() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_SUMMARY,
            GAnalyticsNames.ACTION_NAVIGATION, GAnalyticsNames.LABEL_PAYMENT_INFO_OPEN));
  }
}
