package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for FlightFareInformationPassenger complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="FlightFareInformationPassenger">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="bookingClass" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="paxType" type="{http://api.edreams.com/booking/next}travellerType" />
 *       &lt;attribute name="fareBasisId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="resident" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean"
 * />
 *       &lt;attribute name="fareType" type="{http://api.edreams.com/booking/next}fareType" />
 *       &lt;attribute name="agreementsType" type="{http://api.edreams.com/booking/next}agreementsType"
 * />
 *       &lt;attribute name="fareRules" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class FlightFareInformationPassengerDTO {

  protected String bookingClass;
  protected TravellerTypeDTO paxType;
  protected String fareBasisId;
  protected boolean resident;
  protected FareTypeDTO fareType;
  protected AgreementsTypeDTO agreementsType;
  protected String fareRules;

  /**
   * Gets the value of the bookingClass property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getBookingClass() {
    return bookingClass;
  }

  /**
   * Sets the value of the bookingClass property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setBookingClass(String value) {
    this.bookingClass = value;
  }

  /**
   * Gets the value of the paxType property.
   *
   * @return possible object is
   * {@link TravellerType }
   */
  public TravellerTypeDTO getPaxType() {
    return paxType;
  }

  /**
   * Sets the value of the paxType property.
   *
   * @param value allowed object is
   * {@link TravellerType }
   */
  public void setPaxType(TravellerTypeDTO value) {
    this.paxType = value;
  }

  /**
   * Gets the value of the fareBasisId property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getFareBasisId() {
    return fareBasisId;
  }

  /**
   * Sets the value of the fareBasisId property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setFareBasisId(String value) {
    this.fareBasisId = value;
  }

  /**
   * Gets the value of the resident property.
   */
  public boolean isResident() {
    return resident;
  }

  /**
   * Sets the value of the resident property.
   */
  public void setResident(boolean value) {
    this.resident = value;
  }

  /**
   * Gets the value of the fareType property.
   *
   * @return possible object is
   * {@link FareType }
   */
  public FareTypeDTO getFareType() {
    return fareType;
  }

  /**
   * Sets the value of the fareType property.
   *
   * @param value allowed object is
   * {@link FareType }
   */
  public void setFareType(FareTypeDTO value) {
    this.fareType = value;
  }

  /**
   * Gets the value of the agreementsType property.
   *
   * @return possible object is
   * {@link AgreementsType }
   */
  public AgreementsTypeDTO getAgreementsType() {
    return agreementsType;
  }

  /**
   * Sets the value of the agreementsType property.
   *
   * @param value allowed object is
   * {@link AgreementsType }
   */
  public void setAgreementsType(AgreementsTypeDTO value) {
    this.agreementsType = value;
  }

  /**
   * Gets the value of the fareRules property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getFareRules() {
    return fareRules;
  }

  /**
   * Sets the value of the fareRules property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setFareRules(String value) {
    this.fareRules = value;
  }
}
