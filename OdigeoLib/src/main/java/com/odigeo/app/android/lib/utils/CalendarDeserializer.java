package com.odigeo.app.android.lib.utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;
import java.util.Calendar;

/**
 * Created by ManuelOrtiz on 14/10/2014.
 */
public class CalendarDeserializer implements JsonDeserializer<Calendar> {

  // Source: http://stackoverflow.com/questions/308683/ [Imported by Emiliano De Santis on 12/22/14]
  @Override public final Calendar deserialize(JsonElement arg0, Type arg1,
      JsonDeserializationContext arg2) {

    long milliseconds = arg0.getAsLong();

    Calendar c = Calendar.getInstance();
    c.setTimeInMillis(milliseconds);

        /*
        TimeZone z = c.getTimeZone();

        int offset = z.getRawOffset();

        Log.d(Constants.TAG_LOG, "Offset hora = " + offset);


        if (z.inDaylightTime(new Date())) {
            offset = offset + z.getDSTSavings();
        }

        int offsetHrs = offset / 1000 / 60 / 60;
        int offsetMins = offset / 1000 / 60 % 60;

        c.add(Calendar.HOUR_OF_DAY, (-offsetHrs));
        c.add(Calendar.MINUTE, (-offsetMins));
        */

    return c;
  }
}
