package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.TextView;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.LocalizableBackground;
import com.odigeo.app.android.lib.data.TextForViewListener;
import com.odigeo.data.localizable.LocalizableProvider;

@Deprecated public class CustomTextView extends TextView implements TextForViewListener {
  private static final int REFERENCE_NOT_FOUND = -1;

  private LocalizableProvider localizableProvider;

  public CustomTextView(Context context) {
    super(context);
  }

  public CustomTextView(Context context, AttributeSet attrs) {
    super(context, attrs);

    initialize(context, attrs);
  }

  public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);

    initialize(context, attrs);
  }

  private void initialize(Context context, AttributeSet attrs) {
    setFocusable(true);
    setFocusableInTouchMode(true);

    localizableProvider = ((OdigeoApp) context.getApplicationContext()).getDependencyInjector()
        .provideLocalizables();

    if (!isInEditMode()) {
      parseAttributes(context, attrs);
    }
  }

  /**
   * Parse the attributes. This view will show a Web styled text and a typeface set.
   *
   * @param context The Context the view is running in, through which it can access the current
   * theme,
   * resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   */
  private void parseAttributes(Context context, AttributeSet attrs) {

    if (!isInEditMode()) {
      TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.TextViewWithFont);

      int typefaceValue = values.getInt(R.styleable.TextViewWithFont_typeface, REFERENCE_NOT_FOUND);
      int webTextId =
          values.getResourceId(R.styleable.TextViewWithFont_webText, REFERENCE_NOT_FOUND);

      String odigeoDbKey = values.getString(R.styleable.TextViewWithFont_odigeoText);

      boolean isAsync = values.getBoolean(R.styleable.TextViewWithFont_isAsync, false);

      values.recycle();

      if (typefaceValue != REFERENCE_NOT_FOUND) {
        setTypeface(Configuration.getInstance().getFonts().get(typefaceValue));
      }

      if (webTextId != REFERENCE_NOT_FOUND) {
        setText(Html.fromHtml(getResources().getString(webTextId)));
      }

      if (odigeoDbKey != null) {
        if (isAsync) {
          // Complete text Asynchronously.
          new LocalizableBackground(getContext(), this, odigeoDbKey);
        } else {
          setText(localizableProvider.getString(odigeoDbKey));
        }
      }
    }
  }

  @Override public void setViewText(CharSequence text) {
    setText(text);
  }
}
