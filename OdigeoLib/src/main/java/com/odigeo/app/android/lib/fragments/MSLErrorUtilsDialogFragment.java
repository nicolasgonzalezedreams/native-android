package com.odigeo.app.android.lib.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.dto.MessageResponseDTO;
import com.odigeo.app.android.lib.models.dto.custom.Code;
import com.odigeo.app.android.lib.models.dto.custom.MessageCode;
import com.odigeo.app.android.lib.models.dto.custom.MslError;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.CallTelephoneHelper;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import java.util.List;

/**
 * Created by Irving Lóp on 30/10/2014.
 */
public class MSLErrorUtilsDialogFragment extends android.support.v4.app.DialogFragment {

  private MslError mslError;
  private String parentScreen;
  private TrackerControllerInterface mTrackerController;

  public static MSLErrorUtilsDialogFragment newInstance(List<MessageResponseDTO> messages,
      MslError error) {

    MslError newError = error;
    if (newError == null && messages != null) {
      for (MessageResponseDTO message : messages) {
        if (message.getCode().startsWith("ERR-") || message.getCode()
            .equals(MessageCode.INT_002.getMessageCode())) {
          newError = new MslError();
          newError.setCodeString(Code.BROKEN_FLOW);
        } else if (message.getCode().startsWith("INT-")) {
          newError = new MslError();
          newError.setCodeString(Code.GENERAL_ERROR);
        }
      }
    }

    if (newError != null) {
      MSLErrorUtilsDialogFragment fragment = new MSLErrorUtilsDialogFragment();
      fragment.setMslError(newError);
      return fragment;
    }

    return null;
  }

  public final String getCode() {

    return mslError.getCodeString().name();
  }

  /**
   * public static MSLErrorUtilsDialogFragment setInstance(MslError error) {
   * MSLErrorUtilsDialogFragment fragment = new MSLErrorUtilsDialogFragment();
   * fragment.setMslError(error); return fragment; }
   */

  @NonNull @Override public final Dialog onCreateDialog(Bundle savedInstanceState) {

    return getAlertDialog(mslError.getCodeString()).create();
  }

  private AlertDialog.Builder getAlertDialog(final Code errorCode) {

    mTrackerController = AndroidDependencyInjector.getInstance().provideTrackerController();
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setCancelable(true);
    if (errorCode == Code.SEARCH_TIMEOUT || errorCode == Code.SESSION_TIMEOUT) {
      createDialog(errorCode, builder);
      postGAScreenErrorSessionExpired();

      postGAEventError(getParentScreen(), TrackerConstants.ACTION_ERROR_SESSION_EXPIRED,
          TrackerConstants.LABEL_SESSION_EXPIRED);
    } else if (errorCode == Code.SELECTION_ITINERARY_PRODUCT_ERROR) {
      builder.setTitle(LocalizablesFacade.getString(getActivity(), "error_booking_title"))
          .setMessage(LocalizablesFacade.getString(getActivity(), "error_booking_information"))
          .setPositiveButton(LocalizablesFacade.getString(getActivity(), "error_relaunch"),
              new ReLaunchSearch())
          .setNegativeButton(LocalizablesFacade.getString(getActivity(), "error_newsearch"),
              new NewSearch());

      postGAScreenTrackingErrorUnexpected();
    } else if (errorCode == Code.GENERAL_ERROR
        || errorCode == Code.PROVIDER_DOWN
        || errorCode == Code.INVALID_DEVICE_ID
        || errorCode == Code.INVALID_DAPI_CONTEXT
        || errorCode == Code.BROKEN_FLOW) {
      builder.setTitle(LocalizablesFacade.getString(getActivity(), "error_booking_title"))
          .setMessage(LocalizablesFacade.getString(getActivity(), "error_booking_information"))
          .setPositiveButton(LocalizablesFacade.getString(getActivity(), "error_relaunch"),
              new ReLaunchSearch())
          .setNegativeButton(LocalizablesFacade.getString(getActivity(), "error_newsearch"),
              new NewSearch());

      postGAScreenTrackingErrorUnexpected();
    } else {
      builder = checkErrors(builder, errorCode);
    }
    return builder;
  }

  private void createDialog(Code errorCode, AlertDialog.Builder builder) {

    LayoutInflater inflater =
        (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View layout = inflater.inflate(R.layout.dialog_session_expired_msl,
        (ViewGroup) getActivity().findViewById(R.id.dialog_expired_linear_layout));
    builder.setView(layout);
    fillDialogInformation(errorCode, layout);
  }

  private void fillDialogInformation(Code errorCode, View layout) {

    TextView dialogTitle = (TextView) layout.findViewById(R.id.dialog_expired_title);
    TextView dialogSubtitle = (TextView) layout.findViewById(R.id.dialog_expired_subtitle);
    Button callButton = (Button) layout.findViewById(R.id.btn_call);
    Button relaunchButton = (Button) layout.findViewById(R.id.btn_second_dialog_expired);
    Button newSearchButton = (Button) layout.findViewById(R.id.btn_third_dialog_expired);

    dialogTitle.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.BOOKING_OUTDATED_TITLE));

    CharSequence subtitle =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.PAYMENT_METHOD_PHONE_PRICING_INFO);
    dialogSubtitle.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.ERROR_SESSION_EXPIRED_SUBTITLE,
            subtitle.toString()));

    CharSequence phoneNumber =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.ERROR_SESSION_EXPIRED_PHONE_NUMBER);
    if (!phoneNumber.toString().isEmpty()) {
      callButton.setVisibility(View.VISIBLE);
      callButton.setText(
          LocalizablesFacade.getString(getActivity(), OneCMSKeys.ERROR_SESSION_EXPIRED_PHONE_BUTTON,
              phoneNumber.toString()));

      callButton.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {

          postGAEventError(getParentScreen(), TrackerConstants.ACTION_ERROR_SESSION_EXPIRED,
              TrackerConstants.LABEL_SESSION_EXPIRED_CALL);
          CallTelephoneHelper.callPhoneNumberInFragment(getContext(),
              MSLErrorUtilsDialogFragment.this, LocalizablesFacade.getString(getContext(),
                  OneCMSKeys.ERROR_SESSION_EXPIRED_PHONE_NUMBER).toString());
        }
      });
    }

    relaunchButton.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.BOOKING_OUTDATED_RELAUNCH));
    relaunchButton.setOnClickListener(new ReLaunchSearch(getParentScreen(), errorCode,
        TrackerConstants.LABEL_SESSION_EXPIRING_SAME_SEARCH));

    newSearchButton.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.BOOKING_OUTDATED_NEW_SEARCH));
    newSearchButton.setOnClickListener(new NewSearch(getParentScreen(), errorCode,
        TrackerConstants.LABEL_SESSION_EXPIRING_NEW_SEARCH));
  }

  /**
   * This method checks the different types of errors to show the appropriate dialog
   *
   * @param builder Alert dialog to update
   * @param errorCode Error Code to check
   * @return Updated dialog if necessary
   */
  private AlertDialog.Builder checkErrors(AlertDialog.Builder builder, Code errorCode) {

    AlertDialog.Builder newBuilder = builder;
    if (errorCode == Code.INVALID_USER_DATA) {
      newBuilder.setTitle(LocalizablesFacade.getString(getActivity(), "error_validation"))
          .setPositiveButton(LocalizablesFacade.getString(getActivity(), "common_ok"), null);

      postGAScreenTrackingErrorUnexpected();
    } else if (errorCode == Code.STOP_FLOW) {
      newBuilder.setTitle(LocalizablesFacade.getString(getActivity(), "error_flow"))
          .setPositiveButton(LocalizablesFacade.getString(getActivity(), "common_ok"), null);

      postGAScreenTrackingErrorUnexpected();
    } else if (errorCode == Code.VERSION_ERROR) {
      newBuilder.setTitle(LocalizablesFacade.getString(getActivity(), "error_version"))
          .setPositiveButton(LocalizablesFacade.getString(getActivity(), "common_ok"), null);
    } else {
      newBuilder = checkTimeOut(newBuilder, errorCode);
    }
    return newBuilder;
  }

  /**
   * This method checks if the error is time out
   *
   * @param builder AlertDialog alertDialog to show
   * @param errorCode Error code
   * @return Updated alertDialog if there was time out
   */
  private AlertDialog.Builder checkTimeOut(AlertDialog.Builder builder, Code errorCode) {

    AlertDialog.Builder newBuilder = builder;
    if (errorCode == Code.CONNECTION_TIMEOUT) {
      newBuilder.setTitle(
          LocalizablesFacade.getString(getActivity(), "error_session_expired_title"))
          .setMessage(LocalizablesFacade.getString(getActivity(), "error_timeout"))
          .setPositiveButton(LocalizablesFacade.getString(getActivity(), "error_relaunch"),
              new ReLaunchSearch(getParentScreen(), errorCode, null))
          .setNegativeButton(LocalizablesFacade.getString(getActivity(), "error_newsearch"),
              new NewSearch(getParentScreen(), errorCode, null));
    } else {
      newBuilder.setTitle(LocalizablesFacade.getString(getActivity(), "error_booking_title"))
          .setMessage(LocalizablesFacade.getString(getActivity(), "error_booking_information"))
          .setPositiveButton(LocalizablesFacade.getString(getActivity(), "error_relaunch"),
              new ReLaunchSearch(getParentScreen(), errorCode,
                  TrackerConstants.LABEL_UNKNOWN_SAME_SEARCH))
          .setNegativeButton(LocalizablesFacade.getString(getActivity(), "error_newsearch"),
              new NewSearch(getParentScreen(), errorCode,
                  TrackerConstants.LABEL_UNKNOWN_NEW_SEARCH));
    }
    return newBuilder;
  }

  public final void setMslError(MslError mslError) {

    this.mslError = mslError;
  }

  public final String getParentScreen() {

    return parentScreen;
  }

  public final void setParentScreen(String parentScreen) {

    this.parentScreen = parentScreen;
  }

  private void postGAScreenErrorSessionExpired() {

    mTrackerController.trackAnalyticsScreen(TrackerConstants.SCREEN_ERROR_SESSION_EXPIRED);
  }

  private void postGAScreenTrackingErrorUnexpected() {

    mTrackerController.trackAnalyticsScreen(TrackerConstants.SCREEN_ERROR_UNEXPECTED);
  }

  private void postGAEventError(String step, String action, String label) {

    String category = "";

    if (step.equals(Step.SUMMARY.toString())) {
      category = TrackerConstants.CATEGORY_FLIGHTS_SUMMARY;
    } else if (step.equals(Step.PASSENGER.toString())) {
      category = TrackerConstants.CATEGORY_FLIGHTS_PAX;
    } else if (step.equals(Step.PAYMENT.toString())) {
      category = TrackerConstants.CATEGORY_FLIGHTS_PAYMENT;
    } else if (step.equals(Step.INSURANCE.toString())) {
      category = TrackerConstants.CATEGORY_FLIGHTS_INSURANCE;
    }

    mTrackerController.trackAnalyticsEvent(category, action, label);
  }

  class ReLaunchSearch implements DialogInterface.OnClickListener, View.OnClickListener {

    private final String step;
    private final Code errorCode;
    private final boolean post;
    private String action;
    private String label;

    public ReLaunchSearch() {

      this.step = "";
      this.errorCode = Code.NO_ERROR;
      this.post = false;
    }

    public ReLaunchSearch(String action, String label) {

      this.action = action;
      this.label = label;
      this.step = "";
      this.errorCode = Code.NO_ERROR;
      this.post = false;
    }

    public ReLaunchSearch(String step, Code errorCode, String label) {

      this.step = step;
      this.errorCode = errorCode;
      this.label = label;

      post = true;
    }

    public String getAction() {

      return action;
    }

    public void setAction(String action) {

      this.action = action;
    }

    public String getLabel() {

      return label;
    }

    public void setLabel(String label) {

      this.label = label;
    }

    @Override public void onClick(DialogInterface dialog, int which) {

      trackAndDoIntent();
    }

    @Override public void onClick(View v) {

      trackAndDoIntent();
    }

    private void trackAndDoIntent() {

      if (post && label != null) {
        postGAEventError(step, TrackerConstants.ACTION_ERROR_SESSION_EXPIRED, label);
      }

      OdigeoApp odigeoApp = (OdigeoApp) getActivity().getApplication();
      Intent intent = new Intent(getActivity(), odigeoApp.getSearchFlightsActivityClass());
      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      intent.putExtra(Constants.EXTRA_ACTION_RESEARCH, true);
      getActivity().startActivity(intent);
    }
  }

  class NewSearch implements DialogInterface.OnClickListener, View.OnClickListener {

    private final String step;
    private final Code errorCode;
    private final boolean post;
    private String action;
    private String label;

    public NewSearch() {

      this.step = "";
      this.errorCode = Code.NO_ERROR;
      post = false;
    }

    public NewSearch(String step, Code errorCode, String label) {

      this.step = step;
      this.errorCode = errorCode;
      this.label = label;

      post = true;
    }

    public String getAction() {

      return action;
    }

    public void setAction(String action) {

      this.action = action;
    }

    public String getLabel() {

      return label;
    }

    public void setLabel(String label) {

      this.label = label;
    }

    public void onClick(DialogInterface dialog, int id) {

      trackAndGoToIntent();
    }

    @Override public void onClick(View v) {

      trackAndGoToIntent();
    }

    private void trackAndGoToIntent() {

      if (post && label != null) {
        postGAEventError(step, TrackerConstants.ACTION_ERROR_SESSION_EXPIRED, label);
      }

      OdigeoApp odigeoApp = (OdigeoApp) getActivity().getApplication();
      Intent intent = new Intent(getActivity(), odigeoApp.getSearchFlightsActivityClass());
      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      getActivity().startActivity(intent);
    }
  }
}