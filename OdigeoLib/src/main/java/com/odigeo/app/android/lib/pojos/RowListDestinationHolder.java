package com.odigeo.app.android.lib.pojos;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Irving Lóp on 01/09/2014.
 */
public class RowListDestinationHolder {
  public ImageView icon;
  public TextView cityName;
  public TextView countryName;
  public TextView cityCode;
}
