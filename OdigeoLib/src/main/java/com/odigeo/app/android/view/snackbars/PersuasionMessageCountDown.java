package com.odigeo.app.android.view.snackbars;

import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;

public class PersuasionMessageCountDown extends CountDownTimer {

  private static long COUNT_DOWN = (60000 * 9) + (30000); // 570000
  private static long CLOCK_TICKS = 1000;
  private static PersuasionMessageCountDown ourInstance;
  private Snackbar mSnackbar;

  private PersuasionMessageCountDown(long startTime, long interval) {
    super(startTime, interval);
  }

  public static PersuasionMessageCountDown getInstance() {
    if (ourInstance == null) {
      ourInstance = new PersuasionMessageCountDown(COUNT_DOWN, CLOCK_TICKS);
    }
    return ourInstance;
  }

  public void setSnackBar(Snackbar snackBar) {
    mSnackbar = snackBar;
  }

  @Override public void onTick(long millisUntilFinished) {

  }

  @Override public void onFinish() {
    if ((mSnackbar != null) && (mSnackbar.isShown())) {
      mSnackbar.dismiss();
    }
  }
}
