package com.odigeo.app.android.lib.models.dto;

public enum CollectionStatusTestConfigurationDTO {

  COLLECTED, COLLECTION_DECLINED, COLLECTION_ERROR;

  public static CollectionStatusTestConfigurationDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
