package com.odigeo.app.android.lib.config;

import android.content.Context;
import android.os.Build;
import android.webkit.WebView;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.configuration.BaseConfigurationInterface;
import com.odigeo.data.entity.shoppingCart.Step;
import java.util.List;

/**
 * Class created in order to reduce Configuration.java size (and fix PMD problems).
 *
 * @author Cristian Fanjul
 * @since 15/04/2015
 */
public class BaseConfiguration implements BaseConfigurationInterface {
  protected ImagesSources imagesSources;
  protected Fonts fonts;
  protected String webViewHotelsURL;
  protected String hotelsLabel;
  protected String lastMinuteURL;
  protected String packageURL;
  protected String aid;
  protected String brand;
  protected String brandVisualName;
  protected String brandPrefix;
  protected String brandKey;    // BRAND: O=opodo, E=Edreams, G=goVoyage
  protected String fileVersion;
  protected String appStoreURL;
  protected String prodDomain;
  protected List<Market> markets;
  protected Market currentMarket;
  protected Market defaultMarket;
  protected String userAgent;
  protected GeneralConstants generalConstants;
  protected List<Step> flowSequence;
  protected List<LocaleToMarket> localeToMarkets;
  protected List<CarrierDTO> carriers;
  protected String defaultMarketId;
  protected boolean hasToShowPersuasive = false;
  private String adPlatBrand;

  public final Fonts getFonts() {
    return fonts;
  }

  public final void setFonts(Fonts fonts) {
    this.fonts = fonts;
  }

  public final boolean getShowPersuasive() {
    return hasToShowPersuasive;
  }

  public final String getWebViewHotelsURL() {
    return webViewHotelsURL;
  }

  public final String getHotelsLabel() {
    return hotelsLabel;
  }

  public final String getBrandKey() {
    return brandKey;
  }

  public final String getBrand() {
    return brand;
  }

  //Throw an exception if its value is different to BRAND_OPODO, BRAND_GOVOYAGES, BRAND_EDREAMS
  public final void setBrand(String brand) {
    this.brand = brand;
  }

  public final ImagesSources getImagesSources() {
    return imagesSources;
  }

  public final void setImagesSources(ImagesSources imagesSources) {
    this.imagesSources = imagesSources;
  }

  public final String getFileVersion() {
    return fileVersion;
  }

  public final void setFileVersion(String fileVersion) {
    this.fileVersion = fileVersion;
  }

  public final String getAppStoreURL() {
    return appStoreURL;
  }

  public final void setAppStoreURL(String appStoreURL) {
    this.appStoreURL = appStoreURL;
  }

  public List<Market> getMarkets() {
    return markets;
  }

  public Market getCurrentMarket() {
    return currentMarket;
  }

  public final void setCurrentMarket(Market currentMarket) {
    this.currentMarket = currentMarket;
  }

  public final String getAid() {
    return aid;
  }

  public final void setAid(String aid) {
    this.aid = aid;
  }

  public final GeneralConstants getGeneralConstants() {
    return generalConstants;
  }

  public final String getBrandVisualName() {
    return brandVisualName;
  }

  public final void setBrandVisualName(String brandVisualName) {
    this.brandVisualName = brandVisualName;
  }

  public final List<Step> getFlowSequence() {
    return flowSequence;
  }

  public final void setFlowSequence(List<Step> flowSequence) {
    this.flowSequence = flowSequence;
  }

  public final void setDefaultMarket(Market defaultMarket) {
    this.defaultMarket = defaultMarket;
  }

  public final String getLastMinuteURL() {
    return lastMinuteURL;
  }

  public final void setLastMinuteURL(String lastMinuteURL) {
    this.lastMinuteURL = lastMinuteURL;
  }

  public final String getPackageURL() {
    return packageURL;
  }

  public final void setPackageURL(String packageURL) {
    this.packageURL = packageURL;
  }

  public final String getProdDomain() {
    return prodDomain;
  }

  public final void setProdDomain(String prodDomain) {
    this.prodDomain = prodDomain;
  }

  public final String getBrandPrefix() {
    return brandPrefix;
  }

  public final void setBrandPrefix(String brandPrefix) {
    this.brandPrefix = brandPrefix;
  }

  public final List<CarrierDTO> getCarriers() {
    return carriers;
  }

  public final void setCarriers(List<CarrierDTO> carriers) {
    this.carriers = carriers;
  }

  public final String getDefaultMarketId() {
    return defaultMarketId;
  }

  public final List<LocaleToMarket> getLocaleToMarkets() {
    return localeToMarkets;
  }

  public final void setLocaleToMarkets(List<LocaleToMarket> localeToMarkets) {
    this.localeToMarkets = localeToMarkets;
  }

  public final void updateUserAgent(Context context) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
      userAgent = new WebView(context).getSettings().getUserAgentString();
    } else {
      userAgent = System.getProperty("http.agent");
    }
  }

  public final String getUserAgent() {
    return this.userAgent;
  }
}
