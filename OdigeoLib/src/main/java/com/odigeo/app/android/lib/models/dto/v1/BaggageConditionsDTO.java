package com.odigeo.app.android.lib.models.dto.v1;

import java.io.Serializable;
import java.util.List;

/**
 * Deprecated class, its used for compatibility with v1 shared preferences
 *
 * @author M. en C. Javier Silva Perez
 * @since 17/03/15.
 * @deprecated This class is used by MSL V1, use {@link com.odigeo.app.android.lib.models.dto.BaggageConditionsDTO}
 * instead
 */
@Deprecated public class BaggageConditionsDTO implements Serializable {

  protected List<BaggageFeeDTO> baggageFees;
  protected int baggageIncludedInPrice;

  public List<BaggageFeeDTO> getBaggageFees() {
    return baggageFees;
  }

  public void setBaggageFees(List<BaggageFeeDTO> baggageFees) {
    this.baggageFees = baggageFees;
  }

  /**
   * Gets the value of the baggageIncludedInPrice property.
   */
  public int getBaggageIncludedInPrice() {
    return baggageIncludedInPrice;
  }

  /**
   * Sets the value of the baggageIncludedInPrice property.
   */
  public void setBaggageIncludedInPrice(int value) {
    this.baggageIncludedInPrice = value;
  }
}

