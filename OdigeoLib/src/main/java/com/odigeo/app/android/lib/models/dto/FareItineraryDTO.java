package com.odigeo.app.android.lib.models.dto;

import com.odigeo.app.android.lib.models.CollectionEstimationFees;
import com.odigeo.app.android.lib.models.SegmentGroup;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FareItineraryDTO implements Serializable {

  protected FareItineraryPriceCalculatorDTO price;
  protected String key;
  protected Integer collectionMethodFees;
  protected Integer baggageFees;
  protected Boolean resident;
  protected List<Integer> firstSegments;
  protected List<Integer> secondSegments;
  protected List<Integer> thirdSegments;
  protected List<Integer> fourthSegments;
  protected List<Integer> fifthSegments;
  protected List<Integer> sixthSegments;
  protected List<String> firstSegmentsKeys;
  protected List<String> secondSegmentsKeys;
  protected List<String> thirdSegmentsKeys;
  protected List<String> fourthSegmentsKeys;
  protected List<String> fifthSegmentsKeys;
  protected List<String> sixthSegmentsKeys;
  protected Integer itinerary;
  protected BaggageIncludedDTO firstSegmentBaggageIncluded;
  protected BaggageIncludedDTO secondSegmentBaggageIncluded;
  protected BaggageIncludedDTO thirdSegmentBaggageIncluded;
  protected BaggageIncludedDTO fourthSegmentBaggageIncluded;
  protected BaggageIncludedDTO fifthSegmentBaggageIncluded;
  protected BaggageIncludedDTO sixthSegmentBaggageIncluded;
  protected MembershipPerksDTO membershipPerks;

  //This is use only for the DataAdapter, it is never returned from the service
  private List<SegmentGroup> segmentGroups;
  private Set<CarrierDTO> carriers;
  private CollectionEstimationFees collectionMethodFeesObject;

  public FareItineraryDTO() {
    segmentGroups = new ArrayList<SegmentGroup>();
    carriers = new HashSet<CarrierDTO>();
  }

  public FareItineraryPriceCalculatorDTO getPrice() {
    return price;
  }

  public void setPrice(FareItineraryPriceCalculatorDTO value) {
    this.price = value;
  }

  /**
   * Gets the value of the key property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getKey() {
    return key;
  }

  /**
   * Sets the value of the key property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setKey(String value) {
    this.key = value;
  }

  /**
   * Gets the value of the collectionMethodFees property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getCollectionMethodFees() {
    return collectionMethodFees;
  }

  /**
   * Sets the value of the collectionMethodFees property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setCollectionMethodFees(Integer value) {
    this.collectionMethodFees = value;
  }

  /**
   * Gets the value of the baggageFees property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getBaggageFees() {
    return baggageFees;
  }

  /**
   * Sets the value of the baggageFees property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setBaggageFees(Integer value) {
    this.baggageFees = value;
  }

  /**
   * Gets the value of the resident property.
   *
   * @return possible object is
   * {@link Boolean }
   */
  public Boolean isResident() {
    return resident;
  }

  public Boolean getResident() {
    return this.resident;
  }

  /**
   * Sets the value of the resident property.
   *
   * @param value allowed object is
   * {@link Boolean }
   */
  public void setResident(Boolean value) {
    this.resident = value;
  }

  /**
   * Gets the value of the itinerary property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getItinerary() {
    return itinerary;
  }

  /**
   * Sets the value of the itinerary property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setItinerary(Integer value) {
    this.itinerary = value;
  }

  /**
   * @return the firstSegments
   */
  public List<Integer> getFirstSegments() {
    return firstSegments;
  }

  /**
   * @param firstSegments the firstSegments to set
   */
  public void setFirstSegments(List<Integer> firstSegments) {
    this.firstSegments = firstSegments;
  }

  /**
   * @return the secondSegments
   */
  public List<Integer> getSecondSegments() {
    return secondSegments;
  }

  /**
   * @param secondSegments the secondSegments to set
   */
  public void setSecondSegments(List<Integer> secondSegments) {
    this.secondSegments = secondSegments;
  }

  /**
   * @return the thirdSegments
   */
  public List<Integer> getThirdSegments() {
    return thirdSegments;
  }

  /**
   * @param thirdSegments the thirdSegments to set
   */
  public void setThirdSegments(List<Integer> thirdSegments) {
    this.thirdSegments = thirdSegments;
  }

  /**
   * @return the fourthSegments
   */
  public List<Integer> getFourthSegments() {
    return fourthSegments;
  }

  /**
   * @param fourthSegments the fourthSegments to set
   */
  public void setFourthSegments(List<Integer> fourthSegments) {
    this.fourthSegments = fourthSegments;
  }

  /**
   * @return the fifthSegments
   */
  public List<Integer> getFifthSegments() {
    return fifthSegments;
  }

  /**
   * @param fifthSegments the fifthSegments to set
   */
  public void setFifthSegments(List<Integer> fifthSegments) {
    this.fifthSegments = fifthSegments;
  }

  /**
   * @return the sixthSegments
   */
  public List<Integer> getSixthSegments() {
    return sixthSegments;
  }

  /**
   * @param sixthSegments the sixthSegments to set
   */
  public void setSixthSegments(List<Integer> sixthSegments) {
    this.sixthSegments = sixthSegments;
  }

  /**
   * @return the firstSegmentsKeys
   */
  public List<String> getFirstSegmentsKeys() {
    return firstSegmentsKeys;
  }

  /**
   * @param firstSegmentsKeys the firstSegmentsKeys to set
   */
  public void setFirstSegmentsKeys(List<String> firstSegmentsKeys) {
    this.firstSegmentsKeys = firstSegmentsKeys;
  }

  /**
   * @return the secondSegmentsKeys
   */
  public List<String> getSecondSegmentsKeys() {
    return secondSegmentsKeys;
  }

  /**
   * @param secondSegmentsKeys the secondSegmentsKeys to set
   */
  public void setSecondSegmentsKeys(List<String> secondSegmentsKeys) {
    this.secondSegmentsKeys = secondSegmentsKeys;
  }

  /**
   * @return the thirdSegmentsKeys
   */
  public List<String> getThirdSegmentsKeys() {
    return thirdSegmentsKeys;
  }

  /**
   * @param thirdSegmentsKeys the thirdSegmentsKeys to set
   */
  public void setThirdSegmentsKeys(List<String> thirdSegmentsKeys) {
    this.thirdSegmentsKeys = thirdSegmentsKeys;
  }

  /**
   * @return the fourthSegmentsKeys
   */
  public List<String> getFourthSegmentsKeys() {
    return fourthSegmentsKeys;
  }

  /**
   * @param fourthSegmentsKeys the fourthSegmentsKeys to set
   */
  public void setFourthSegmentsKeys(List<String> fourthSegmentsKeys) {
    this.fourthSegmentsKeys = fourthSegmentsKeys;
  }

  /**
   * @return the fifthSegmentsKeys
   */
  public List<String> getFifthSegmentsKeys() {
    return fifthSegmentsKeys;
  }

  /**
   * @param fifthSegmentsKeys the fifthSegmentsKeys to set
   */
  public void setFifthSegmentsKeys(List<String> fifthSegmentsKeys) {
    this.fifthSegmentsKeys = fifthSegmentsKeys;
  }

  /**
   * @return the sixthSegmentsKeys
   */
  public List<String> getSixthSegmentsKeys() {
    return sixthSegmentsKeys;
  }

  /**
   * @param sixthSegmentsKeys the sixthSegmentsKeys to set
   */
  public void setSixthSegmentsKeys(List<String> sixthSegmentsKeys) {
    this.sixthSegmentsKeys = sixthSegmentsKeys;
  }

  public List<SegmentGroup> getSegmentGroups() {
    return segmentGroups;
  }

  public void setSegmentGroups(List<SegmentGroup> segmentGroups) {
    this.segmentGroups = segmentGroups;
  }

  public Set<CarrierDTO> getCarriers() {
    return carriers;
  }

  public void setCarriers(Set<CarrierDTO> carriers) {
    this.carriers = carriers;
  }

  public CollectionEstimationFees getCollectionMethodFeesObject() {
    return collectionMethodFeesObject;
  }

  public void setCollectionMethodFeesObject(CollectionEstimationFees collectionMethodFeesObject) {
    this.collectionMethodFeesObject = collectionMethodFeesObject;
  }

  public BaggageIncludedDTO getFirstSegmentBaggageIncluded() {
    return firstSegmentBaggageIncluded;
  }

  public void setFirstSegmentBaggageIncluded(BaggageIncludedDTO firstSegmentBaggageIncluded) {
    this.firstSegmentBaggageIncluded = firstSegmentBaggageIncluded;
  }

  public BaggageIncludedDTO getSecondSegmentBaggageIncluded() {
    return secondSegmentBaggageIncluded;
  }

  public void setSecondSegmentBaggageIncluded(BaggageIncludedDTO secondSegmentBaggageIncluded) {
    this.secondSegmentBaggageIncluded = secondSegmentBaggageIncluded;
  }

  public BaggageIncludedDTO getThirdSegmentBaggageIncluded() {
    return thirdSegmentBaggageIncluded;
  }

  public void setThirdSegmentBaggageIncluded(BaggageIncludedDTO thirdSegmentBaggageIncluded) {
    this.thirdSegmentBaggageIncluded = thirdSegmentBaggageIncluded;
  }

  public BaggageIncludedDTO getFourthSegmentBaggageIncluded() {
    return fourthSegmentBaggageIncluded;
  }

  public void setFourthSegmentBaggageIncluded(BaggageIncludedDTO fourthSegmentBaggageIncluded) {
    this.fourthSegmentBaggageIncluded = fourthSegmentBaggageIncluded;
  }

  public BaggageIncludedDTO getFifthSegmentBaggageIncluded() {
    return fifthSegmentBaggageIncluded;
  }

  public void setFifthSegmentBaggageIncluded(BaggageIncludedDTO fifthSegmentBaggageIncluded) {
    this.fifthSegmentBaggageIncluded = fifthSegmentBaggageIncluded;
  }

  public BaggageIncludedDTO getSixthSegmentBaggageIncluded() {
    return sixthSegmentBaggageIncluded;
  }

  public void setSixthSegmentBaggageIncluded(BaggageIncludedDTO sixthSegmentBaggageIncluded) {
    this.sixthSegmentBaggageIncluded = sixthSegmentBaggageIncluded;
  }

  public MembershipPerksDTO getMembershipPerks() {
    return membershipPerks;
  }

  public void setMembershipPerks(MembershipPerksDTO membershipPerks) {
    this.membershipPerks = membershipPerks;
  }
}
