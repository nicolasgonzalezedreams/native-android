package com.odigeo.app.android.utils;

import com.odigeo.app.android.lib.R;
import com.odigeo.constants.PaymentMethodsKeys;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DrawablesFactory {

  public static Map<String, Integer> createCreditCardResourcesMap() {
    return Collections.unmodifiableMap(new HashMap<String, Integer>() {{
      put(PaymentMethodsKeys.UNKNOWN, R.drawable.card_placeholder);
      put(PaymentMethodsKeys.VI_LOCAL, R.drawable.cc_vi);
      put(PaymentMethodsKeys.MD_LOCAL, R.drawable.cc_ca);
      put(PaymentMethodsKeys.AX_LOCAL, R.drawable.cc_ax);
      put(PaymentMethodsKeys.JC_LOCAL, R.drawable.cc_jc);
      put(PaymentMethodsKeys.DC_LOCAL, R.drawable.cc_dc);
      put(PaymentMethodsKeys.VI, R.drawable.cc_vi);
      put(PaymentMethodsKeys.VD, R.drawable.cc_vd);
      put(PaymentMethodsKeys.VE, R.drawable.cc_ve);
      put(PaymentMethodsKeys.E1, R.drawable.cc_e1);
      put(PaymentMethodsKeys.MA, R.drawable.cc_ma);
      put(PaymentMethodsKeys.CA, R.drawable.cc_ca);
      put(PaymentMethodsKeys.MD, R.drawable.cc_md);
      put(PaymentMethodsKeys.MP, R.drawable.cc_mp);
      put(PaymentMethodsKeys.EV, R.drawable.cc_ev);
      put(PaymentMethodsKeys.ME, R.drawable.cc_me);
      put(PaymentMethodsKeys.VB, R.drawable.cc_vb);
      put(PaymentMethodsKeys.SOLO, R.drawable.cc_solo);
      put(PaymentMethodsKeys.DU, R.drawable.cc_du);
      put(PaymentMethodsKeys.DL, R.drawable.cc_dl);
      put(PaymentMethodsKeys.DS, R.drawable.cc_ds);
      put(PaymentMethodsKeys.CB, R.drawable.cc_cb);
      put(PaymentMethodsKeys.VV, R.drawable.cc_vv);
      put(PaymentMethodsKeys.AX, R.drawable.cc_ax);
      put(PaymentMethodsKeys.DC, R.drawable.cc_dc);
      put(PaymentMethodsKeys.JC, R.drawable.cc_jc);
      put(PaymentMethodsKeys.COF, R.drawable.cc_cf);
      put(PaymentMethodsKeys.UNKNOWN, R.drawable.card_placeholder);
    }});
  }
}
