package com.odigeo.app.android.lib.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.interfaces.AdapterExpandableListListener;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;
import com.odigeo.data.entity.TravelType;
import java.util.List;

public class DefaultResultsExpandableListAdapter extends ResultsActivityExpandableListAdapter {

  public DefaultResultsExpandableListAdapter(Activity context,
      List<FareItineraryDTO> itineraryResults, TravelType travelType, int divider, int numSegments,
      String locale, AdapterExpandableListListener adapterExpandableListListener) {
    super(context, itineraryResults, travelType, divider, numSegments, locale,
        adapterExpandableListListener);
  }

  @Override public View inflateHeader(int groupPosition, boolean isExpanded, View convertView,
      ViewGroup parent) {
    return inflateGroupViewNonMembers(groupPosition, isExpanded, convertView, parent);
  }

  private View inflateGroupViewNonMembers(int groupPosition, boolean isExpanded, View convertView,
      ViewGroup parent) {
    View view = inflater.inflate(R.layout.layout_expandablelist_header_price, parent, false);

    itineraryContainers[groupPosition] = view;

    if (isFullTransparency()) {
      TextView tvFullPriceSentence = (TextView) view.findViewById(R.id.fullPriceSentence);
      tvFullPriceSentence.setText(mFullPriceSentence);
      tvFullPriceSentence.setTypeface(Configuration.getInstance().getFonts().getRegular());
      tvFullPriceSentence.setVisibility(View.VISIBLE);
    } else {
      setItineraryAirlinesImages(groupPosition);
    }
    setItineraryPrice(groupPosition);
    ((ExpandableListView) parent).expandGroup(groupPosition);
    return view;
  }
}
