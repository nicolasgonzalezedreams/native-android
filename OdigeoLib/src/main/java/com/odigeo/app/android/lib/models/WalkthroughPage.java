package com.odigeo.app.android.lib.models;

import java.io.Serializable;

/**
 * This item defines the different pages to be shown in {@link com.odigeo.app.android.lib.activities.OdigeoWalkthroughActivity}
 * or any of its subclasses, each item must define the url, title and description
 *
 * @author M. en C. Javier Silva Perez
 * @version 1.0
 * @since 09/03/15
 */
public class WalkthroughPage implements Serializable {

  private String title;

  private String description;

  /**
   * Creates a new page item using all its parameters
   *
   * @param title Page title
   * @param description description of the page
   */
  public WalkthroughPage(String title, String description) {
    this.title = title;
    this.description = description;
  }

  public WalkthroughPage() {
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
