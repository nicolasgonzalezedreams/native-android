package com.odigeo.app.android.lib.interfaces;

/**
 * Created by Irving Lóp on 14/10/2014.
 */
public interface IFilterTabs {
  boolean onClickFilterTab(int newIndex);
}
