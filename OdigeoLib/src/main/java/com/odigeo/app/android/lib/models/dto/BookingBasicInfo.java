package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class BookingBasicInfo implements Serializable {

  private long id;
  private String clientBookingReferenceId;
  private String websiteCode;
  private String brandCode;
  private String marketingPortalCode;
  private Partner partner;
  private long creationDate;
  private long lastUpdate;
  private Agent agent;

  public final String getBrandCode() {
    return brandCode;
  }

  public final void setBrandCode(String brandCode) {
    this.brandCode = brandCode;
  }

  public final long getId() {
    return id;
  }

  public final void setId(long id) {
    this.id = id;
  }

  public final String getMarketingPortalCode() {
    return marketingPortalCode;
  }

  public final void setMarketingPortalCode(String marketingPortalCode) {
    this.marketingPortalCode = marketingPortalCode;
  }

  public final String getWebsiteCode() {
    return websiteCode;
  }

  public final void setWebsiteCode(String websiteCode) {
    this.websiteCode = websiteCode;
  }

  public final String getClientBookingReferenceId() {
    return clientBookingReferenceId;
  }

  public final void setClientBookingReferenceId(String clientBookingReferenceId) {
    this.clientBookingReferenceId = clientBookingReferenceId;
  }

  public final Partner getPartner() {
    return partner;
  }

  public final void setPartner(Partner partner) {
    this.partner = partner;
  }

  public final long getCreationDate() {
    return creationDate;
  }

  public final void setCreationDate(long creationDate) {
    this.creationDate = creationDate;
  }

  public final long getLastUpdate() {
    return lastUpdate;
  }

  public final void setLastUpdate(long lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public final Agent getAgent() {
    return agent;
  }

  public final void setAgent(Agent agent) {
    this.agent = agent;
  }
}
