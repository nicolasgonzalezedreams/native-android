package com.odigeo.app.android.lib.utils.exceptions;

import com.google.gson.Gson;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;

public class ShoppingCartResponseException extends Exception {

  private ShoppingCartResponseException(String detailMessage) {
    super(detailMessage);
  }

  public static ShoppingCartResponseException newInstance(
      CreateShoppingCartResponse shoppingCartResponse) {
    Gson gson = new Gson();
    String json = gson.toJson(shoppingCartResponse);
    return new ShoppingCartResponseException(json);
  }
}
