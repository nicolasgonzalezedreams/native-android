package com.odigeo.app.android.view;

import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.navigator.NavigationDrawerNavigator;
import com.odigeo.app.android.view.animations.UpdateCarouselDotsAnimation;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DateHelper;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.app.android.view.interfaces.ListenerUpdateCarousel;
import com.odigeo.app.android.view.interfaces.MttCardBackground;
import com.odigeo.data.entity.carrousel.BoardingGateCard;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.extensions.UIBoardingCard;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.BoardingGatePresenter;
import com.odigeo.presenter.contracts.views.BoardingGateViewInterface;

/**
 * Created by gonzalo.lodi on 1/27/2016.
 */
public class BoardingGateCardView extends BaseView<BoardingGatePresenter>
    implements BoardingGateViewInterface, MttCardBackground {

  private static final String KEY_BOARDING_CARD = "KEY_BOARDING_CARD";
  protected BoardingGateCard mCard;
  private TextView mTvTitleCard;
  private TextView mTvSubtitleCard;
  private ImageView mIvIconCard;
  private TextView mTvHeaderGate;
  private TextView mTvGateNumber;
  private TextView mTvTerminalNumber;
  private TextView mTvDateLastUpdate;
  private long mBookingId;
  private long mLastUpdate;
  private int mCardPosition;
  private ImageView mIvAutoRenewCardIcon;

  private String stringLastUpdate;
  private String stringTerminal;
  private ListenerUpdateCarousel mListenerUpdateCarousel;
  private UpdateCarouselDotsAnimation mUpdateCarouselDotsAnimation;
  private boolean mItIsPaused;
  private DateHelper mDateHelper;

  public static BoardingGateCardView newInstance(BoardingGateCard card, long lastUpdate) {
    Bundle args = new Bundle();
    args.putParcelable(KEY_BOARDING_CARD, new UIBoardingCard(card));
    args.putLong(Card.KEY_LAST_UPDATE, lastUpdate);
    BoardingGateCardView fragment = new BoardingGateCardView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle bundle = getArguments();
    mDateHelper = getDateHelper();
    if (bundle.containsKey(KEY_BOARDING_CARD)
        && bundle.containsKey(Card.KEY_LAST_UPDATE)
        && bundle.containsKey(Card.KEY_CARD_POSITION)) {
      mCard = (UIBoardingCard) bundle.getParcelable(KEY_BOARDING_CARD);
      mLastUpdate = bundle.getLong(Card.KEY_LAST_UPDATE);
      mCardPosition = bundle.getInt(Card.KEY_CARD_POSITION);
    }
    mBookingId = mCard.getId();
  }

  @Override public void onResume() {
    super.onResume();
    mListenerUpdateCarousel = (ListenerUpdateCarousel) getContext();

    if (mItIsPaused) {
      mUpdateCarouselDotsAnimation.runDotsAnimation();
    }
  }

  @Override public void onPause() {
    super.onPause();
    mListenerUpdateCarousel = null;
    mItIsPaused = true;
  }

  @Override protected BoardingGatePresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideBoardingGatePresenter(this, (NavigationDrawerNavigator) getActivity());
  }

  private DateHelper getDateHelper() {
    return AndroidDependencyInjector.getInstance().provideDateHelper();
  }

  @Override protected int getFragmentLayout() {
    return R.layout.boarding_gate_card;
  }

  @Override protected void initComponent(View view) {
    mTvTitleCard = (TextView) view.findViewById(R.id.TvTitleCard);
    mTvSubtitleCard = (TextView) view.findViewById(R.id.TvSubtitleCard);
    mIvIconCard = (ImageView) view.findViewById(R.id.IvCardIcon);
    mTvHeaderGate = (TextView) view.findViewById(R.id.TvHeaderGate);
    mTvGateNumber = (TextView) view.findViewById(R.id.TvGateNumber);
    mTvTerminalNumber = (TextView) view.findViewById(R.id.TvTerminalNumber);
    mTvDateLastUpdate = (TextView) view.findViewById(R.id.TvDateLastUpdate);
    mIvAutoRenewCardIcon = (ImageView) view.findViewById(R.id.IvAutoRenewCardIcon);

    mItIsPaused = false;

    initUpdateCarouselDotsAnimation();
    initRefreshButton();
    setUpdateListener();
    setListeners(view);
  }

  private void initRefreshButton() {
    if (mCard.hasToShowRefreshButton()) {
      mIvAutoRenewCardIcon.setVisibility(View.VISIBLE);
    } else {
      mIvAutoRenewCardIcon.setVisibility(View.GONE);
    }
  }

  private void initUpdateCarouselDotsAnimation() {
    mUpdateCarouselDotsAnimation =
        AndroidDependencyInjector.getInstance().provideUpdateCarouselDotsAnimation();
    String updatingData =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_UPDATING).toString();
    mUpdateCarouselDotsAnimation.initAnimationData(updatingData, mTvDateLastUpdate);
  }

  @Override public String getBackgroundImage() {
    String imageUrl = "";
    if (mCard != null) {
      imageUrl = mCard.getImage();
    }
    return imageUrl;
  }

  private void initDataBoardingTripCard() {
    mTvSubtitleCard.setText(mCard.getTripToHeader());
    mTvGateNumber.setText(mCard.getGate());

    String terminal;
    if ((mCard.getFrom().getTerminal() != null) && (!mCard.getFrom().getTerminal().isEmpty())) {
      terminal = stringTerminal + " " + mCard.getFrom().getTerminal();
    } else {
      terminal = stringTerminal + " -";
    }

    mTvTerminalNumber.setText(terminal);
    if (mLastUpdate == 0) {
      mTvDateLastUpdate.setVisibility(View.GONE);
    } else {
      String lastUpdateText = stringLastUpdate
          + " "
          + "<b>"
          + mDateHelper.getTime(mLastUpdate, getActivity())
          + "</b> "
          + mDateHelper.getDayAndMonth(mLastUpdate, getActivity());
      mTvDateLastUpdate.setText(Html.fromHtml(lastUpdateText));
    }

    mIvIconCard.setImageDrawable(DrawableUtils.getTintedResource(R.mipmap.boarding_gate_icon_card,
        R.color.carousel_card_icon, getActivity()));
    mIvAutoRenewCardIcon.setImageDrawable(
        DrawableUtils.getTintedResource(R.drawable.ic_autorenew, R.color.carousel_card_icon,
            getActivity()));
  }

  @Override protected void setListeners() {
    initDataBoardingTripCard();
  }

  private void setUpdateListener() {
    mIvAutoRenewCardIcon.setOnTouchListener(new View.OnTouchListener() {
      @Override public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
          mIvAutoRenewCardIcon.setImageDrawable(
              DrawableUtils.getTintedResource(R.drawable.ic_autorenew,
                  R.color.carousel_card_icon_pressed, getActivity()));
          return true;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
          mIvAutoRenewCardIcon.setImageDrawable(
              DrawableUtils.getTintedResource(R.drawable.ic_autorenew, R.color.carousel_card_icon,
                  getActivity()));

          mListenerUpdateCarousel.onUpdateCarousel();
          startRefreshAnimation();
          mTracker.trackMTTCardEvent(TrackerConstants.LABEL_REFRESH_INFORMATION);
          return true;
        }
        return false;
      }
    });
  }

  private void startRefreshAnimation() {
    mUpdateCarouselDotsAnimation.runDotsAnimation();
    Animation startRotateAnimation =
        AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_refresh_button);
    mIvAutoRenewCardIcon.startAnimation(startRotateAnimation);
  }

  private void stopRefreshAnimation() {
    mUpdateCarouselDotsAnimation.stopDotsAnimation();
    mIvAutoRenewCardIcon.clearAnimation();
  }

  @Override public void onStop() {
    super.onStop();
    stopRefreshAnimation();
  }

  protected void setListeners(View view) {
    view.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        String trackingLabel = TrackerConstants.LABEL_CARD_SEE_INFO_FORMAT.replace(
            TrackerConstants.CARD_TYPE_IDENTIFIER, TrackerConstants.LABEL_CARD_BOARDING_GATE)
            .replace(TrackerConstants.CARD_POSITION_IDENTIFIER, String.valueOf(mCardPosition));
        mTracker.trackMTTCardEvent(trackingLabel);
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    mTvTitleCard.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_TITLE));
    mTvHeaderGate.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_GATE));
    stringTerminal =
        String.valueOf(LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_TERMINAL));
    stringLastUpdate =
        String.valueOf(LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_LASTUPDATE));
  }

  @Override public void setCardPosition(int position) {
    Bundle bundle = getArguments();
    bundle.putInt(Card.KEY_CARD_POSITION, position);
    this.setArguments(bundle);
  }
}
