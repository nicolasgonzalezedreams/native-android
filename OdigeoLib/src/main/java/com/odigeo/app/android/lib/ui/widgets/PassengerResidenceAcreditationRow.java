package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.data.entity.shoppingCart.ResidentValidationType;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.data.entity.shoppingCart.TravellerType;

/**
 * Created by Oscar Álvarez on 29/01/2015.
 */
public class PassengerResidenceAcreditationRow extends LinearLayout {
  CustomTextView passengerType;
  CustomTextView passengerName;
  CustomTextView passengerStatusTitle;
  CustomTextView passengerStatusDesc;
  ImageView statusIcon;

  public PassengerResidenceAcreditationRow(Context context) {
    super(context);
    initialize(context);
  }

  private void initialize(Context context) {
    inflate(context, R.layout.layout_passenger_residence_acreditation_row, this);
    passengerType = (CustomTextView) findViewById(R.id.txtStatusValidationPassengerType);
    passengerName = (CustomTextView) findViewById(R.id.txtStatusValidationPassengerName);
    passengerStatusTitle = (CustomTextView) findViewById(R.id.txtStatusValidationPassengerTitle);
    passengerStatusDesc = (CustomTextView) findViewById(R.id.txtStatusValidationPassengerDesc);
    statusIcon = (ImageView) findViewById(R.id.imgStatusValidationPassenger);
  }

  public final void setPassenger(Traveller traveller, ResidentValidationType status,
      String footnoteRef) {
    passengerType.setText(getPassengerType(traveller.getTravellerType()));
    passengerName.setText(traveller.getName() + " " + traveller.getFirstLastName());
    if (status == ResidentValidationType.OK) {
      passengerStatusTitle.setText(LocalizablesFacade.getString(getContext(),
          "residentacreditationmodule_validationok_message"));
      passengerStatusTitle.setTextColor(getResources().getColor(R.color.semantic_positive));
      passengerStatusDesc.setText(LocalizablesFacade.getString(getContext(),
          "residentacreditationmodule_validationok_takeyourdocuments"));
      statusIcon.setImageResource(R.drawable.validation_resident_status_verified);
    } else if (status == ResidentValidationType.PENDING) {
      passengerStatusTitle.setText(LocalizablesFacade.getString(getContext(),
          "residentacreditationmodule_validationpending_message").toString() + " " + footnoteRef);
      passengerStatusTitle.setTextColor(getResources().getColor(R.color.semantic_information_dark));
      statusIcon.setImageResource(R.drawable.validation_resident_status_pending);
      passengerStatusDesc.setVisibility(View.GONE);
    } else if (status == ResidentValidationType.KO) {
      passengerStatusTitle.setText(LocalizablesFacade.getString(getContext(),
          "residentacreditationmodule_validationko_message").toString() + " " + footnoteRef);
      passengerStatusTitle.setTextColor(getResources().getColor(R.color.semantic_negative_blocker));
      passengerStatusDesc.setText(LocalizablesFacade.getString(getContext(),
          "residentacreditationmodule_validationok_takeyourdocuments").toString());
      statusIcon.setImageResource(R.drawable.validation_resident_status_not_verified);
    }
    //***set description and title
  }

  private String getPassengerType(TravellerType type) {
    if (type == TravellerType.ADULT) {
      return CommonLocalizables.getInstance().getCommonAdult(getContext()).toString();
    } else if (type == TravellerType.CHILD) {
      return CommonLocalizables.getInstance().getCommonChild(getContext()).toString();
    } else if (type == TravellerType.INFANT) {
      return CommonLocalizables.getInstance().getCommonInfant(getContext()).toString();
    }
    return "";
  }
}
