package com.odigeo.app.android.lib.models.dto;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.data.entity.BaseSpinnerItem;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Deprecated public class ShoppingCartCollectionOptionDTO implements Serializable, BaseSpinnerItem {
  protected CollectionMethodDTO method;
  protected List<InstallmentInformationDescriptionDTO> installments;
  protected BigDecimal fee;
  protected String bankTransferText;
  //Visual fields
  @NonNull private String text;
  @DrawableRes private int image;

  /**
   * Gets the value of the method property.
   *
   * @return possible object is {@link CollectionMethodDTO }
   */
  public CollectionMethodDTO getMethod() {
    return method;
  }

  /**
   * Sets the value of the method property.
   *
   * @param value allowed object is {@link CollectionMethodDTO }
   */
  public void setMethod(CollectionMethodDTO value) {
    this.method = value;
  }

  /**
   * Gets the value of the fee property.
   *
   * @return possible object is {@link java.math.BigDecimal }
   */
  public BigDecimal getFee() {
    return fee;
  }

  /**
   * Sets the value of the fee property.
   *
   * @param value allowed object is {@link java.math.BigDecimal }
   */
  public void setFee(BigDecimal value) {
    this.fee = value;
  }

  /**
   * @return
   */
  public List<InstallmentInformationDescriptionDTO> getInstallments() {
    return installments;
  }

  /**
   * @param installments
   */
  public void setInstallments(List<InstallmentInformationDescriptionDTO> installments) {
    this.installments = installments;
  }

  /**
   * @return the bankTransferText
   */
  public String getBankTransferText() {
    return bankTransferText;
  }

  /**
   * @param bankTransferText the bankTransferText to set
   */
  public void setBankTransferText(String bankTransferText) {
    this.bankTransferText = bankTransferText;
  }

  /**
   * Generate the resources to be showed.
   *
   * @param context Context where be called.
   */
  public void generateResources(@NonNull Context context) {
    if (method == null) {
      text = EMPTY_STRING;
      image = EMPTY_RESOURCE;
    } else {
      text = method.getName(context);

      //Get the image resource.
      String code = method.getCode();
      String imageName = String.format("cc_%s", code).toLowerCase();
      image =
          ViewUtils.getResourceIdByName(context, imageName, "drawable", context.getPackageName());
      //If not found the resource, set the default.
      if (image == 0) {
        image = R.drawable.cc_cu;
      }
    }
  }

  @Override public String getShownTextKey() {
    return EMPTY_STRING;
  }

  @Override public int getImageId() {
    return image;
  }

  @Nullable @Override public String getShownText() {
    return text;
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public Object getData() {
    return null;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }

  @Override public String toString() {
    return text;
  }
}
