package com.odigeo.app.android.lib.fragments.base;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import com.globant.roboneck.base.RoboNeckFragment;
import com.google.gson.Gson;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoSearchActivity;
import com.odigeo.app.android.lib.config.ResidentItinerary;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.fragments.PassengerDialogPickerFragment;
import com.odigeo.app.android.lib.interfaces.IHistorySearchListener;
import com.odigeo.app.android.lib.interfaces.OdigeoInterfaces;
import com.odigeo.app.android.lib.interfaces.SearchTripListener;
import com.odigeo.app.android.lib.mappers.StoredSearchOptionsMapper;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.app.android.lib.ui.widgets.SearchTripWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.lib.utils.exceptions.FlightSegmentException;
import com.odigeo.app.android.view.custom.HistorySearchWidget;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackHelper;
import com.odigeo.interactors.RemoveHistorySearchesInteractor;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ManuelOrtiz on 01/09/2014.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 2.1.2
 * @since 19/02/2015
 */
public abstract class TripFragmentBase extends RoboNeckFragment
    implements SearchTripListener, IHistorySearchListener {

  //The callback is the activity host, is to inform him about events with the UI inside the fragment
  protected SearchTripWidget searchTripWidget;
  private HistorySearchWidget historySearchW;
  private SearchOptions searchOptions;
  private List<FlightSegment> flightSegments;
  private OdigeoInterfaces.SearchListener listener;
  private ScrollView mScrollView;
  private List<SearchOptions> mSearchOptionsList;

  private TrackerControllerInterface mTracker;
  private boolean shouldLaunchSearch;
  private OdigeoSearchActivity parentActivity;
  private RemoveHistorySearchesInteractor mRemoveSearchesHistoryInteractor;
  private StoredSearchOptionsMapper storedSearchOptionsMapper;

  private SearchTrackHelper mSearchTrackHelper;
  private OdigeoApp application;

  public abstract boolean isDataValid();

  public abstract TravelType getTravelType();

  protected abstract void setSegments();

  public abstract boolean isShownInThisFragment(SearchOptions searchOptions);

  public void setListener(OdigeoInterfaces.SearchListener listener) {
    this.listener = listener;
  }

  @Override public void onAttach(Activity activity) {
    super.onAttach(activity);
    parentActivity = (OdigeoSearchActivity) activity;
    Log.i(Constants.TAG_LOG,
        "onAttach(" + getTravelType().name() + ") Activity= " + (getActivity() == null ? "NULL"
            : " existente"));
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mRemoveSearchesHistoryInteractor =
        AndroidDependencyInjector.getInstance().provideRemoveHistorySearchesInteractor();
    storedSearchOptionsMapper =
        AndroidDependencyInjector.getInstance().provideStoredSearchOptionsMapper();
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_searchactivity_roundtrip, container, false);
    mTracker = AndroidDependencyInjector.getInstance().provideTrackerController();
    historySearchW = (HistorySearchWidget) rootView.findViewById(R.id.history_search_trip_widget);
    historySearchW.setListener(this);
    searchTripWidget = (SearchTripWidget) rootView.findViewById(R.id.trip_fragment_searchActivity);
    searchTripWidget.setSegments(getFlightSegments());
    searchTripWidget.setTravelType(getTravelType());
    searchTripWidget.setListener(this);
    mScrollView = (ScrollView) rootView.findViewById(R.id.scroll_search);
    initComponents();
    Log.i(Constants.TAG_LOG,
        "onCreateView(" + getTravelType().name() + ") Activity= " + (getActivity() == null ? "NULL"
            : " existente"));
    return rootView;
  }

  public void initComponents() {
    mSearchTrackHelper = new SearchTrackHelper();
  }

  @Override public void onResume() {
    super.onResume();
    application = (OdigeoApp) getActivity().getApplication();
    setControlsValues();
    drawHistorySearches();
  }

  public void launchSearch(boolean isResident) {
    this.setSegments();
    this.setControlsValues();
    parentActivity.onClickSearch(getSearchTripWidget(), isResident);
  }

  /**
   * Sets the values of the controls chosen by the user
   */
  public void setControlsValues() {
    if (getSearchTripWidget() == null) {
      return;
    }

    //Set if it is a direct flight
    getSearchTripWidget().getChkDirectFlight().setChecked(searchOptions.isDirectFlight());

    // Set Type of Flight

    if (searchOptions.getTravelType() == null) {
      searchOptions.setTravelType(searchTripWidget.getTravelType());
    }

    //Set the cabin class selected
    getSearchTripWidget().getSpinnerClass().setSelection(searchOptions.getCabinClass());

    //Set the number of passengers selected
    getSearchTripWidget().setPassengers(searchOptions.getNumberOfAdults(),
        searchOptions.getNumberOfKids(), searchOptions.getNumberOfBabies());

    //Should be enabled the Search Flights Buttons?
    getSearchTripWidget().getBtnSearchFlight().setEnabled(isDataValid());

    getSearchTripWidget().getResidentsWidget()
        .setResident(((OdigeoSearchActivity) getActivity()).isResident());
  }

  public boolean areControlsAlreadySet() {
    return (getSearchTripWidget().getCityFrom() != null) || (getSearchTripWidget().getCityTo()
        != null);
  }

  public void setSearchesList(List<SearchOptions> searches) {
    mSearchOptionsList = searches;
  }

  /*
  * Show in history only the type of searches that belongs to fragment
  * */
  public List<SearchOptions> filterSearches(List<SearchOptions> searches) {
    List<SearchOptions> result = new ArrayList<>();

    for (SearchOptions searchOptions : searches) {
      if (isShownInThisFragment(searchOptions)) {
        result.add(searchOptions);
      }
    }

    return result;
  }

  /**
   * Draw the last searches
   */
  public void drawHistorySearches() {
    List<SearchOptions> filteredSearches = filterSearches(mSearchOptionsList);
    showHistorySearchWidget(filteredSearches);
  }

  private void showHistorySearchWidget(List<SearchOptions> searchOptionsList) {
    if (searchOptionsList != null && !searchOptionsList.isEmpty()) {
      historySearchW.setVisibility(View.VISIBLE);
      historySearchW.setHistoryList(searchOptionsList);
      historySearchW.drawFirstHistorySearches();
    }
  }

  @Override public void onClickSearch(SearchTripWidget searchTripWidget, boolean isResident) {
    if (listener != null) {

      // GATracking - search box - event 2
      if (getTravelType().equals(TravelType.SIMPLE)) {
        postGAEventFlightType(GAnalyticsNames.LABEL_ONE_WAY);
        postTrackerOneWayFlightSummary();
      } else if (getTravelType().equals(TravelType.ROUND)) {
        postGAEventFlightType(GAnalyticsNames.LABEL_ROUND_TRIP);
        postTrackerRoundTripFlightSummary();
      } else if (getTravelType().equals(TravelType.MULTIDESTINATION)) {
        postGAEventFlightType(GAnalyticsNames.LABEL_MULTI_DEST);
        postTrackerMultiStopFlightSummary();
      }

      postGAEventFlightPassengers();

      // GATracking - search box - event 4
      if (searchOptions.isDirectFlight()) {
        postGAEventFlightType(GAnalyticsNames.LABEL_DIRECT_FLIGHTS_CHECKED);
      }

      // GATracking - search box - event 5
      postGAEventFlightType(
          GAnalyticsNames.LABEL_PARTIAL_CABIN_CLASS + LocalizablesFacade.getString(getActivity(),
              searchOptions.getCabinClass().getShownTextKey()).toString().replace(" ", "_"));

      postGAEventLabelSearchFlights();

      trackSearch(searchOptions, flightSegments);
      listener.onClickSearch(searchTripWidget, isResident);
    }
  }

  @Override public void onClickDirectFlight(boolean checked) {
    searchOptions.setDirectFlight(checked);
  }

  @Override public void onClickButtonPassenger() {
    PassengerDialogPickerFragment dialog = new PassengerDialogPickerFragment();
    dialog.setNumAdults(searchOptions.getNumberOfAdults());
    dialog.setNumKids(searchOptions.getNumberOfKids());
    dialog.setNumBabies(searchOptions.getNumberOfBabies());
    dialog.show(getFragmentManager(), "Pickerfragment");
  }

  @Override public void onCabinClassSelected(CabinClassDTO cabinClass) {
    Log.d(Constants.TAG_LOG, "Cabin class selected " + cabinClass);
    searchOptions.setCabinClass(cabinClass);
  }

  @Override public void onResidentSwitchClicked(boolean checked) {
    ((OdigeoSearchActivity) getActivity()).setResident(checked);
  }

  private void trackSearch(SearchOptions searchOptions, List<FlightSegment> flightSegments) {
    buildSearchTrack(searchOptions);
    fillTrackSegments(flightSegments);
    mTracker.trackSearchLaunched(mSearchTrackHelper.getFlightType(), mSearchTrackHelper.getAdults(),
        mSearchTrackHelper.getKids(), mSearchTrackHelper.getInfants(),
        mSearchTrackHelper.getOrigins(), mSearchTrackHelper.getDestinations(),
        mSearchTrackHelper.getDates(), mSearchTrackHelper.getDepartureCountries(),
        mSearchTrackHelper.getArrivalCountries());
  }

  private void buildSearchTrack(SearchOptions searchOptions) {
    mSearchTrackHelper.clear();
    mSearchTrackHelper.setFlightType(searchOptions.getTravelType().ordinal());
    mSearchTrackHelper.setAdults(searchOptions.getNumberOfAdults());
    mSearchTrackHelper.setKids(searchOptions.getNumberOfKids());
    mSearchTrackHelper.setInfants(searchOptions.getNumberOfBabies());
  }

  private void fillTrackSegments(List<FlightSegment> flightSegments) {
    String departure, arrival, arrivalCountry, departureCountry;
    long date;
    for (FlightSegment flightSegment : flightSegments) {
      if(flightSegment.getDepartureCity() != null && flightSegment.getArrivalCity() != null) {
        departure = flightSegment.getDepartureCity().getIataCode();
        arrival = flightSegment.getArrivalCity().getIataCode();
        date = flightSegment.getDate();
        arrivalCountry = flightSegment.getArrivalCity().getCountryCode();
        departureCountry = flightSegment.getDepartureCity().getCountryCode();
        mSearchTrackHelper.addSegment(departure, arrival, date, departureCountry, arrivalCountry);
      }
    }
  }

  @Override public void onClickHistorySearchW(SearchOptions searchOptions) {
    if (listener != null) {
      trackSearch(searchOptions, searchOptions.getFlightSegments());
      fillTrackSegments(searchOptions.getFlightSegments());
      listener.onClickHistorySearch(searchOptions);
      if (searchOptions.getTravelType() == TravelType.SIMPLE) {
        postTrackerOneWayFlightSummary();
      } else if (searchOptions.getTravelType() == TravelType.ROUND) {
        postTrackerRoundTripFlightSummary();
      } else {
        postTrackerMultiStopFlightSummary();
      }
    }
  }

  @Override public void onDeleteHistorySearchW() {
    if (listener != null) {
      listener.onDeleteHistorySearchW(this);
    }
  }

  public void removeHistory(StoredSearch.TripType travelType) {
    mRemoveSearchesHistoryInteractor.removeAllSearches(getRemoveSearchDataListener(), travelType);
  }

  public OnRequestDataListener<Boolean> getRemoveSearchDataListener() {
    return new OnRequestDataListener<Boolean>() {
      @Override public void onResponse(Boolean synched) {
        if (historySearchW != null) {
          historySearchW.post(new Runnable() {
            @Override public void run() {
              if (getActivity() != null) {
                Util.triggerWidgetUpdate(getActivity(),
                    ((OdigeoApp) getActivity().getApplication()).getWidgetClass());
                historySearchW.clearItems();
                historySearchW.setVisibility(View.GONE);
                historySearchW.invalidate();
                mSearchOptionsList = storedSearchOptionsMapper.storedSearchToSearchOptionListMapper(
                    AndroidDependencyInjector.getInstance()
                        .provideSearchesHandler()
                        .getCompleteSearchesFromDB());
                ((OdigeoSearchActivity) getActivity()).setSearchOptionsList(mSearchOptionsList);
                ((OdigeoSearchActivity) getActivity()).updateHistory();
              }
            }
          });
        }
      }

      @Override public void onError(MslError error, String message) {
      }
    };
  }

  public SearchOptions getSearchOptions() {
    return searchOptions;
  }

  public void setSearchOptions(SearchOptions searchOptions) {

    this.searchOptions = searchOptions;
  }

  public List<FlightSegment> getFlightSegments() {
    return flightSegments;
  }

  public void setFlightSegments(List<FlightSegment> flightSegments) {
    this.flightSegments = flightSegments;
    setSegments();
  }

  public SearchTripWidget getSearchTripWidget() {
    return searchTripWidget;
  }

  public ScrollView getScrollView() {
    return mScrollView;
  }

  public boolean isViewCreated() {
    return getActivity() != null;
  }

  public boolean isShouldLaunchSearch() {
    return shouldLaunchSearch;
  }

  public void setShouldLaunchSearch(boolean shouldLaunchSearch) {
    this.shouldLaunchSearch = shouldLaunchSearch;
  }

  public void showResidentWidget(ResidentItinerary residentItinerary) {

    if (searchTripWidget != null) {
      searchTripWidget.showResidentsWidget(residentItinerary);
    }
  }

  private int calculateTripDuration(long departureDate, long arrivalDate) {
    long diff = arrivalDate - departureDate;
    return (int) diff / (60 * 60 * 1000) % 24;
  }

  private void postTrackerOneWayFlightSummary() {
    TrackerControllerInterface trackerController =
        AndroidDependencyInjector.getInstance().provideTrackerController();
    FlightSegment flightSegment = flightSegments.get(0);
    if (flightSegment.getDepartureCity() == null || flightSegment.getArrivalCity() == null) {
      trackInvalidFlightSegmentEvent(flightSegment);
    } else {
      trackerController.trackOneWaySearchFlights(flightSegment.getDepartureCity().getIataCode(),
          flightSegment.getArrivalCity().getIataCode(), flightSegment.getDate(),
          searchTripWidget.getCellPassengers().getNoAdults(),
          searchTripWidget.getCellPassengers().getNoKids(),
          searchTripWidget.getCellPassengers().getNoBabies(), searchOptions.isDirectFlight(),
          searchTripWidget.getSpinnerClass().getSelectedItem().toString(), 0);
    }
  }

  private void trackInvalidFlightSegmentEvent(FlightSegment flightSegment) {
    if (getTravelType() != null) {
      application.setString("Travel type", getTravelType().name());
    }
    if (getSearchOptions() != null) {
      application.setString("Search Option", new Gson().toJson(getSearchOptions()));
    }
    application.trackNonFatal(FlightSegmentException.newInstance(flightSegment));
  }

  private void postTrackerRoundTripFlightSummary() {
    TrackerControllerInterface trackerController =
        AndroidDependencyInjector.getInstance().provideTrackerController();
    if (flightSegments.size() < 2
        && searchOptions != null
        && searchOptions.getFlightSegments() != null
        && searchOptions.getFlightSegments().size() > 1) {
      flightSegments = searchOptions.getFlightSegments();
    }

    FlightSegment departureFlight = flightSegments.get(0);
    FlightSegment arrivalFlight = flightSegments.get(1);
    if (departureFlight.getDepartureCity() == null || departureFlight.getArrivalCity() == null) {
      trackInvalidFlightSegmentEvent(departureFlight);
    } else if (arrivalFlight.getDepartureCity() == null || arrivalFlight.getArrivalCity() == null) {
      trackInvalidFlightSegmentEvent(arrivalFlight);
    } else {
      trackerController.trackRoundTripSearchFlights(
          departureFlight.getDepartureCity().getIataCode(),
          departureFlight.getArrivalCity().getIataCode(), departureFlight.getDate(),
          arrivalFlight.getDate(), searchTripWidget.getCellPassengers().getNoAdults(),
          searchTripWidget.getCellPassengers().getNoKids(),
          searchTripWidget.getCellPassengers().getNoBabies(), searchOptions.isDirectFlight(),
          searchTripWidget.getSpinnerClass().getSelectedItem().toString(),
          OdigeoDateUtils.calculateTripDuration(departureFlight.getDate(),
              arrivalFlight.getDate()));
    }
  }

  private void postTrackerMultiStopFlightSummary() {
    TrackerControllerInterface trackerController =
        AndroidDependencyInjector.getInstance().provideTrackerController();
    FlightSegment flightSegment1 = flightSegments.get(0);
    FlightSegment flightSegment2 = flightSegments.get(1);
    FlightSegment flightSegment3 = flightSegments.size() >= 3 ? flightSegments.get(2) : null;
    trackerController.trackMultiStopSearchFlights(flightSegment1.getDepartureCity().getIataCode(),
        flightSegment1.getArrivalCity().getIataCode(), flightSegment1.getDate(),
        flightSegment2.getDepartureCity().getIataCode(),
        flightSegment2.getArrivalCity().getIataCode(), flightSegment2.getDate(),
        flightSegment3 != null ? flightSegment3.getDepartureCity().getIataCode() : null,
        flightSegment3 != null ? flightSegment3.getArrivalCity().getIataCode() : null,
        flightSegment3 != null ? flightSegment3.getDate() : 0,
        flightSegment3 != null ? flightSegment3.getDate() : flightSegment2.getDate(),
        searchTripWidget.getCellPassengers().getNoAdults(),
        searchTripWidget.getCellPassengers().getNoKids(),
        searchTripWidget.getCellPassengers().getNoBabies(), searchOptions.isDirectFlight(),
        searchTripWidget.getSpinnerClass().getSelectedItem().toString(),
        OdigeoDateUtils.calculateTripDuration(flightSegment1.getDate(),
            flightSegment3 == null ? flightSegment2.getDate() : flightSegment3.getDate()));
  }

  private void postGAEventFlightPassengers() {
    // GATracking - search box - event 3
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHT_SEARCH,
            GAnalyticsNames.ACTION_SEARCHER_FLIGHTS, GAnalyticsNames.LABEL_PARTIAL_NUMBER_PAX
            + searchOptions.getNumberOfAdults()
            + "_"
            + searchOptions.getNumberOfKids()
            + "_"
            + searchOptions.getNumberOfBabies()));
  }

  private void postGAEventFlightType(String label) {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHT_SEARCH,
            GAnalyticsNames.ACTION_SEARCHER_FLIGHTS, label));
  }

  private void postGAEventLabelSearchFlights() {
    // GATracking - search box - event 6
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHT_SEARCH,
            GAnalyticsNames.ACTION_SEARCHER_FLIGHTS, GAnalyticsNames.LABEL_SEARCH_FLIGHTS));
  }

  /**
   * Use it in combination with {@link #setShouldLaunchSearch(boolean)}, to avoid undesirable
   * behaviors.
   *
   * @return If the fragment have all the necessary information to perform the search.
   */
  public boolean isReadyForSearch() {
    //First check if have a valid segments
    if (flightSegments == null || flightSegments.isEmpty()) {
      return false;
    }

    boolean result = true;
    TravelType travelType = getTravelType();
    //Validations for a simple flight
    if (travelType == TravelType.SIMPLE) {
      result = checkSingleFlightReadyToSearch(flightSegments.get(0));
    } else if (travelType == TravelType.ROUND || travelType == TravelType.MULTIDESTINATION) {
      result = checkRoundReadyToSearch();
    }
    return result;
  }

  /**
   * Check if the segments are ready to search for a round flight, checking the departure city for
   * the first segment and the arrival for
   * the others
   *
   * @return True if the segment has date and departure or arrival city, FALSE other wise
   */
  private boolean checkRoundReadyToSearch() {
    int count = 0;
    boolean result = true;
    for (FlightSegment segment : flightSegments) {
      //Validations for a round flight

      //If its the first segment, then check only departure information
      if (count == 0) {
        if (segment.getDepartureCity() == null || segment.getDate() == 0) {
          if (segment.getDepartureCity() != null) {
            //Set Departure City
            getSearchTripWidget().setCityFrom(segment.getDepartureCity());
          }
          result = false;
          break;
        }
      } else {
        //Check only the arrival information
        if (segment.getDate() == 0 || segment.getArrivalCity() == null) {
          if (segment.getArrivalCity() != null) {
            //Set Arrival City
            getSearchTripWidget().setCityTo(segment.getArrivalCity());
          }
          result = false;
        }
      }
      count++;
    }
    return result;
  }

  /**
   * Checks for a single segment if its ready to search
   *
   * @param segment Segment to check
   * @return TRUE if every field in the segment is not null
   */
  private Boolean checkSingleFlightReadyToSearch(FlightSegment segment) {
    boolean result = !(segment.getDepartureCity() == null
        || segment.getArrivalCity() == null
        || segment.getDate() == 0);
    if (segment.getDepartureCity() != null && getSearchTripWidget() != null) {
      //Set Departure City
      getSearchTripWidget().setCityFrom(segment.getDepartureCity());
    }
    if (segment.getArrivalCity() != null && getSearchTripWidget() != null) {
      //Set Arrival City
      getSearchTripWidget().setCityTo(segment.getArrivalCity());
    }
    return result;
  }

  public SearchTrackHelper getSearchTrackHelper() {
    if (isNotCompleteSearchTracker(mSearchTrackHelper)) {
      buildSearchTrack(searchOptions);
      fillTrackSegments(flightSegments);
    }

    return mSearchTrackHelper;
  }

  private boolean isNotCompleteSearchTracker(SearchTrackHelper mSearchTrackHelper) {
    return mSearchTrackHelper.getAdults() == null
        || mSearchTrackHelper.getKids() == null
        || mSearchTrackHelper.getInfants() == null
        || mSearchTrackHelper.getDestinations() == null
        || mSearchTrackHelper.getDates() == null
        || mSearchTrackHelper.getOrigins() == null;
  }
}
