package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

/**
 * Created by matias.dirusso on 19/4/2016.
 */
public class PromotionalCampaignGroupDTO {

  protected String id;
  protected String description;
  protected BigDecimal maxAccumulativeDiscount;
  protected String currency;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public BigDecimal getMaxAccumulativeDiscount() {
    return maxAccumulativeDiscount;
  }

  public void setMaxAccumulativeDiscount(BigDecimal maxAccumulativeDiscount) {
    this.maxAccumulativeDiscount = maxAccumulativeDiscount;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }
}
