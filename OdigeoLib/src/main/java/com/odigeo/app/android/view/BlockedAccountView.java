package com.odigeo.app.android.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.BlockedAccountPresenter;
import com.odigeo.presenter.contracts.navigators.LoginNavigatorInterface;
import com.odigeo.presenter.contracts.views.BlockedAccountViewInterface;

/**
 * Created by ximena.perez on 21/08/2015.
 */
public class BlockedAccountView extends BaseView<BlockedAccountPresenter>
    implements BlockedAccountViewInterface {

  private static final String USER_EMAIL = "USER_EMAIL";
  private Button mBtnGetMail;
  private TextView mTvUsername;
  private String email = null;

  public BlockedAccountView() {
    // Required empty public constructor
  }

  public static BlockedAccountView newInstance(String email) {
    BlockedAccountView view = new BlockedAccountView();
    Bundle args = new Bundle();
    args.putString(USER_EMAIL, email);
    view.setArguments(args);

    return view;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle args = this.getArguments();
    if (args != null) {
      if (email == null) {
        email = args.getString(USER_EMAIL);
      }
    }
  }

  @Override public void onResume() {
    super.onResume();
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_LOGIN_ACCOUNT_BLOCKED);
  }

  @Override protected BlockedAccountPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideBlockedAccountPresenter(this, (LoginNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_account_blocked;
  }

  @Override protected void initComponent(View view) {
    mBtnGetMail = (Button) view.findViewById(R.id.btnGoToMail);
    mTvUsername = (TextView) view.findViewById(R.id.txt_vw_blocked_acc_email);
    mTvUsername.setText(getEmail());
  }

  @Override protected void setListeners() {
    mBtnGetMail.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.goToMail();
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_SSO_BLOCKED,
            TrackerConstants.ACTION_SSO_BLOCKED, TrackerConstants.LABEL_GO_TO_MAIL);
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    mBtnGetMail.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.SSO_GO_TO_MAIL).toString());
  }

  public String getEmail() {
    return email;
  }

  public void updateInfo(String email) {
    this.email = email;
  }
}
