package com.odigeo.app.android.utils;

import android.text.TextUtils;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.tools.DateHelperInterface;
import org.apache.commons.lang3.text.WordUtils;

import java.util.ArrayList;
import java.util.List;

import static com.odigeo.app.android.view.constants.OneCMSKeys.TEMPLATE_DATELONG1;

public class BookingUtils {

  public static final String DEFAULT_CITY_DELIMITER = " · ";

  public static String getArrivalCityName(Booking booking, CharSequence delimiter) {
    String arrivalCityName;
    if (booking.isMultiSegment()) {
      List<String> cities = new ArrayList<>(booking.getSegments().size());
      for (Segment segment : booking.getSegments()) {
        cities.add(segment.getArrivalCity().getCityName());
      }
      arrivalCityName = TextUtils.join(delimiter, cities);
    } else {
      return booking.getFirstSegment().getLastSection().getTo().getCityName();
    }
    return arrivalCityName;
  }

  public static String formatSegmentDate(Segment segment, DateHelperInterface dateHelper,
      LocalizableProvider localizableProvider) {
    return WordUtils.capitalize(
        dateHelper.millisecondsToDateGMT(segment.checkAndAddSegmentDelayDate(),
            localizableProvider.getString(TEMPLATE_DATELONG1)));
  }
}
