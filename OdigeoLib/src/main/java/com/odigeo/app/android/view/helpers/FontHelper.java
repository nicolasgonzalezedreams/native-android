package com.odigeo.app.android.view.helpers;

/**
 * Created by Javier on 25/9/15public static final String
 */
public class FontHelper {

  //MyTripsDetails
  public static final String ICON_MY_TRIP_DETAILS_PASSENGERS = String.valueOf((char) 0xe600);
  public static final String ICON_MY_TRIP_DETAILS_ADULT = String.valueOf((char) 0xe601);
  public static final String ICON_MY_TRIP_DETAILS_CHILD = String.valueOf((char) 0xe602);
  public static final String ICON_MY_TRIP_DETAILS_BABY = String.valueOf((char) 0xe603);
  public static final String ICON_MY_TRIP_DETAILS_PLANE = String.valueOf((char) 0xe604);
  public static final String ICON_MY_TRIP_DETAILS_TICK = String.valueOf((char) 0xe605);
  public static final String ICON_MY_TRIP_DETAILS_AIRLINE_REFERENCE = String.valueOf((char) 0xe606);
  public static final String ICON_MY_TRIP_DETAILS_BRAND_BOOKING_REFERENCE =
      String.valueOf((char) 0xe607);
  public static final String ICON_MY_TRIP_DETAILS_CREDIT_CARD = String.valueOf((char) 0xe608);
  public static final String ICON_MY_TRIP_DETAILS_MAIL = String.valueOf((char) 0xe609);
  public static final String ICON_MY_TRIP_DETAILS_TOTAL_PRICE = String.valueOf((char) 0xe60A);
  public static final String ICON_MY_TRIP_DETAILS_BAGGAGE_OK = String.valueOf((char) 0xe60B);
  public static final String ICON_MY_TRIP_DETAILS_BILLING = String.valueOf((char) 0xe60C);
  public static final String ICON_MY_TRIP_DETAILS_BOOKING_REFERENCE = String.valueOf((char) 0xe60D);
  public static final String ICON_MY_TRIP_DETAILS_CALENDAR = String.valueOf((char) 0xe60E);
  public static final String ICON_MY_TRIP_DETAILS_INSURANCES = String.valueOf((char) 0xe60F);
  public static final String ICON_MY_TRIP_DETAILS_FOLD = String.valueOf((char) 0xe610);
  public static final String ICON_MY_TRIP_DETAILS_UNFOLD = String.valueOf((char) 0xe611);

  public static final String ICON_MY_TRIP_DETAILS_ARROW_SIMPLE = "→";
  public static final String ICON_MY_TRIP_DETAILS_ARROW_DOUBLE = "⇆";

  public static final String ICON_MY_TRIP_DETAILS_MIDDLE_DOT = "•";

  public static String hexToString(String hex) {
    StringBuilder output = new StringBuilder();

    for (int i = 0; i < hex.length(); i += 2) {
      String str = hex.substring(i, i + 2);
      output.append((char) Integer.parseInt(str, 16));
    }

    return output.toString();
  }
}
        
