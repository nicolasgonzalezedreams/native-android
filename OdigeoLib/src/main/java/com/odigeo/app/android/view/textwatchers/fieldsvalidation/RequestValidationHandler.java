package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

import java.util.Map;

public class RequestValidationHandler {

  private final Map<String, FieldValidator> commandMap;

  public RequestValidationHandler(Map<String, FieldValidator> commandMap) {
    this.commandMap = commandMap;
  }

  public boolean handleRequest(String fieldName, String fieldInformation) {
    FieldValidator fieldValidator = commandMap.get(fieldName);
    return fieldValidator == null || fieldValidator.validateField(fieldInformation);
  }
}
