package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.OdigeoSpinnerLocalAdapter;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import com.odigeo.app.android.lib.ui.widgets.base.OdigeoSpinner;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import java.util.List;

/**
 * Created by manuel on 17/10/14.
 */
public class FullPriceWidget extends LinearLayout implements View.OnClickListener {

  private final OdigeoSpinner spinnerFullPrice;
  private final LinearLayout containerFullPriceBody;
  private final LinearLayout containerFullPriceHeader;
  private final View fullPriceSeparator;
  private final ImageView btnExpandCollapseHeader;
  private final TextView txtPaymentSelected;
  private final TextView mTxtPickerWillpay;
  private FullPriceListener listener;

  private boolean isExpanded;

  public FullPriceWidget(Context context, AttributeSet attrs) {
    super(context, attrs);

    inflate(getContext(), R.layout.layout_fullprice_widget, this);

    containerFullPriceBody = (LinearLayout) findViewById(R.id.body_fullprice_widget);
    spinnerFullPrice = (OdigeoSpinner) findViewById(R.id.spinnerFullPrice);
    containerFullPriceHeader = (LinearLayout) findViewById(R.id.fullPriceHeader);
    fullPriceSeparator = findViewById(R.id.fullPriceSeparator);
    btnExpandCollapseHeader = (ImageView) findViewById(R.id.less_info_full_price);
    txtPaymentSelected = (TextView) findViewById(R.id.txtPaymentSelected);

    mTxtPickerWillpay = (TextView) findViewById(R.id.txtPickerWillpay);
    mTxtPickerWillpay.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.FULL_PRICE_PICKER_WILLPAY));

    containerFullPriceHeader.setOnClickListener(this);
    spinnerFullPrice.setSpinnerItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        CollectionMethodWithPrice item =
            (CollectionMethodWithPrice) spinnerFullPrice.getAdapter().getItem(position);
        String selectedItem =
            LocalizablesFacade.getString(getContext(), OneCMSKeys.FULL_PRICE_BOTTOM_TITLE,
                item.getCollectionMethod().getCreditCardType().getName()).toString();

        if (!txtPaymentSelected.getText().toString().equals(selectedItem)) {
          //Collapse the widget
          setPaymentMethod(item);
        }
      }

      @Override public void onNothingSelected(AdapterView<?> parent) {
        // Nothing here.
      }
    });

    containerFullPriceBody.setClickable(true);
  }

  public void setPaymentMethod(CollectionMethodWithPrice item) {
    if (isExpanded) {
      toggleFullPriceContainer();
    }

    if (item != null) {
      txtPaymentSelected.setText(
          LocalizablesFacade.getString(getContext(), OneCMSKeys.FULL_PRICE_BOTTOM_TITLE,
              item.getCollectionMethod().getCreditCardType().getName()));
    }
    if (listener != null) {
      listener.onItemSelected(item);
    }
  }

  public final void setListener(FullPriceListener listener) {
    this.listener = listener;
  }

  public final void setCollectionMethods(
      List<CollectionMethodWithPrice> collectionMethodWithPrices) {
    //Fill out spinner items images
    for (CollectionMethodWithPrice price : collectionMethodWithPrices) {
      String code = price.getCollectionMethod().getCode();
      String imageName = String.format("cc_%s", code).toLowerCase();
      int idResource = ViewUtils.getResourceIdByName(getContext(), imageName, "drawable",
          getContext().getPackageName());

      if (idResource == 0) {
        idResource = R.drawable.cc_cu;
      }

      price.setImageId(idResource);
    }
    spinnerFullPrice.setAdapter(
        new OdigeoSpinnerLocalAdapter<>(getContext(), collectionMethodWithPrices));
    txtPaymentSelected.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.FULL_PRICE_BOTTOM_TITLE,
            collectionMethodWithPrices.get(0).getCollectionMethod().getCreditCardType().getName()));
  }

  @Override public final void onClick(View v) {
    if (v.getId() == containerFullPriceHeader.getId()) {
      toggleFullPriceContainer();
    }
  }

  private void toggleFullPriceContainer() {
    containerFullPriceBody.setVisibility(isExpanded ? GONE : VISIBLE);
    fullPriceSeparator.setVisibility(isExpanded ? GONE : VISIBLE);
    btnExpandCollapseHeader.setImageResource(
        isExpanded ? R.drawable.less_info : R.drawable.more_info);
    isExpanded = !isExpanded;
    if (isExpanded) {
      postGAEventFullPriceClicked();
    }
  }

  public CollectionMethodWithPrice getSelectedItem() {
    return ((CollectionMethodWithPrice) spinnerFullPrice.getSelectedItem());
  }

  public final void setSelectedItem(CollectionMethodWithPrice collectionMethodWithPrice) {
    spinnerFullPrice.setSelection(collectionMethodWithPrice);
  }

  public OdigeoSpinner getSpinnerFullPrice() {
    return this.spinnerFullPrice;
  }

  /**
   * Trigger the analytics event whe the full price is clicked.
   */
  private void postGAEventFullPriceClicked() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_RESULTS,
            GAnalyticsNames.ACTION_FULL_PRICE_SELECTOR,
            GAnalyticsNames.LABEL_OPEN_FULL_PRICE_SELECTOR));
  }

  public interface FullPriceListener {

    void onItemSelected(CollectionMethodWithPrice item);
  }
}
