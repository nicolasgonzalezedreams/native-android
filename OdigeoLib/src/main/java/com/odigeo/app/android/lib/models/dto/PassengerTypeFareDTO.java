package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

/**
 * <p>Java class for PassengerTypeFare complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="PassengerTypeFare">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="fare" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="tax" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class PassengerTypeFareDTO {

  protected BigDecimal fare;
  protected BigDecimal tax;

  protected TravellerTypeDTO passengerType;

  /**
   * Gets the value of the fare property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getFare() {
    return fare;
  }

  /**
   * Sets the value of the fare property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setFare(BigDecimal value) {
    this.fare = value;
  }

  /**
   * Gets the value of the tax property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getTax() {
    return tax;
  }

  /**
   * Sets the value of the tax property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setTax(BigDecimal value) {
    this.tax = value;
  }

  public TravellerTypeDTO getPassengerType() {
    return passengerType;
  }

  public void setPassengerType(TravellerTypeDTO passengerType) {
    this.passengerType = passengerType;
  }
}
