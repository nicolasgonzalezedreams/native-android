package com.odigeo.app.android.lib.ui.widgets;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;

/**
 * Created by Pablo on 27/10/2014.
 */

public class VelocityInformationWidget extends RelativeLayout {
  private TextView textView;

  public VelocityInformationWidget(Context context, AttributeSet attrs) {

    super(context, attrs);

    initialize();
  }

  public VelocityInformationWidget(Activity context) {
    super(context);

    initialize();
  }

  private void initialize() {
    inflate(getContext(), R.layout.widget_insurance_mandatory, this);

    textView = (TextView) getRootView().findViewById(R.id.mandatory_insurance_title);
    textView.setTypeface(Configuration.getInstance().getFonts().getRegular());
  }

  public void setText(String text) {
    textView.setText(text);
  }
}
