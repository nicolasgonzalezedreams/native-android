package com.odigeo.app.android.navigator;

import android.content.Intent;
import android.os.Bundle;
import com.localytics.android.Localytics;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoWalkthroughActivity;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.app.android.services.OdigeoFirebaseTokenRegistrationService;
import com.odigeo.app.android.view.SplashView;
import com.odigeo.data.entity.carrousel.CampaignCard;
import com.odigeo.presenter.contracts.navigators.SplashNavigatorInterface;

public class SplashNavigator extends BaseNavigator implements SplashNavigatorInterface {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view);

    //Workaround for issue regarding the launcher activity restarting each time you send the app to background
    if (!isTaskRoot()) {
      Intent intent = getIntent();
      String action = intent.getAction();
      if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && action != null && action.equals(
          Intent.ACTION_MAIN)) {
        if (getIntent().hasExtra(CampaignCard.LOCALYTICS_CAMPAIGN_CARD_KEY)) {
          PreferencesManager.setCampaignCardKey(this,
              getIntent().getStringExtra(CampaignCard.LOCALYTICS_CAMPAIGN_CARD_KEY));
        }
        finish();
        return;
      }
    }

    replaceFragment(R.id.fl_container, SplashView.getInstance());
  }

  @Override protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
    Localytics.onNewIntent(this, intent);
  }

  @Override public void goToWalkThroughActivity() {
    Intent intent = new Intent(this, ((OdigeoApp) getApplication()).getWalkthroughActivity());
    intent.putExtra(OdigeoWalkthroughActivity.EXTRA_WALKTHROUGH_CONTENT,
        getIntent().getSerializableExtra(OdigeoWalkthroughActivity.EXTRA_WALKTHROUGH_CONTENT));
    startActivity(intent);
    finish();
  }

  @Override public void goToHomeActivity() {
    Intent intent = new Intent(this, ((OdigeoApp) getApplication()).getHomeActivity());
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

    if (getIntent().hasExtra(CampaignCard.LOCALYTICS_CAMPAIGN_CARD_KEY)) {
      PreferencesManager.setCampaignCardKey(this,
          getIntent().getStringExtra(CampaignCard.LOCALYTICS_CAMPAIGN_CARD_KEY));
    }

    startActivity(intent);
    finish();
  }

  @Override public void startFirebaseRegistrationTokenService() {
    startService(new Intent(this, OdigeoFirebaseTokenRegistrationService.class));
  }
}
