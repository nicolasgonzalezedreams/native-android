package com.odigeo.app.android.lib.models.dto.requests;

/**
 * Created by ManuelOrtiz on 10/09/2014.
 */
public class AvailableProductsRequestModel {

  protected long bookingId;

  public AvailableProductsRequestModel(long bookingId) {
    this.bookingId = bookingId;
  }

  /**
   * Gets the value of the bookingId property.
   */
  public final long getBookingId() {
    return bookingId;
  }

  /**
   * Sets the value of the bookingId property.
   */
  public final void setBookingId(long value) {
    this.bookingId = value;
  }
}
