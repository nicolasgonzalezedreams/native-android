package com.odigeo.app.android.lib.models.dto;

import android.support.annotation.Nullable;
import com.odigeo.data.entity.BaseSpinnerItem;

public enum CabinClassDTO implements BaseSpinnerItem {

  BUSINESS("sectioncell_cabinclassbusiness"), FIRST("sectioncell_cabinclassfirst"), TOURIST(
      "sectioncell_cabinclasstourist"), PREMIUM_ECONOMY(
      "sectioncell_cabinclasspremiumeconomy"), ECONOMIC_DISCOUNTED(
      "sectioncell_cabinclasseconomicdiscounted"), DEFAULT("searchflights_classcabin_showall");

  String stringRes;

  CabinClassDTO(String stringRes) {
    this.stringRes = stringRes;
  }

  @Override public String getShownTextKey() {
    return stringRes;
  }

  @Override public int getImageId() {
    return BaseSpinnerItem.EMPTY_RESOURCE;
  }

  @Nullable @Override public String getShownText() {
    return null;
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public Object getData() {
    return null;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }
}
