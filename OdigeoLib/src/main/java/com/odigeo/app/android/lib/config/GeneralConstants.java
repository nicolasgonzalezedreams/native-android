package com.odigeo.app.android.lib.config;

/**
 * Created by ManuelOrtiz on 22/09/2014.
 */
public class GeneralConstants {
  private int maxNumberOfLegs;
  private int minNumberOfLegs;
  private int numResultsOpenedByDefaultSimpleSearch;
  private int numResultsOpenedByDefaultMultiSearch;

  /**
   * Number maximum the infants per adult
   */
  private int maxInfantPerAdult;
  /**
   * Number maximum the babies per adult
   */
  private int maxKidsPerAdult;
  /**
   * Number maximum the passengers admitted
   */
  private int maxPassengers;

  private int defaultNumberOfAdults;
  private int defaultNumberOfInfants;
  private int defaultNumberOfKids;

  public final int getMaxNumberOfLegs() {
    return maxNumberOfLegs;
  }

  public final void setMaxNumberOfLegs(int maxNumberOfLegs) {
    this.maxNumberOfLegs = maxNumberOfLegs;
  }

  public final int getMinNumberOfLegs() {
    return minNumberOfLegs;
  }

  public final void setMinNumberOfLegs(int minNumberOfLegs) {
    this.minNumberOfLegs = minNumberOfLegs;
  }

  public final int getNumResultsOpenedByDefaultSimpleSearch() {
    return numResultsOpenedByDefaultSimpleSearch;
  }

  public final void setNumResultsOpenedByDefaultSimpleSearch(
      int numResultsOpenedByDefaultSimpleSearch) {
    this.numResultsOpenedByDefaultSimpleSearch = numResultsOpenedByDefaultSimpleSearch;
  }

  public final int getNumResultsOpenedByDefaultMultiSearch() {
    return numResultsOpenedByDefaultMultiSearch;
  }

  public final void setNumResultsOpenedByDefaultMultiSearch(
      int numResultsOpenedByDefaultMultiSearch) {
    this.numResultsOpenedByDefaultMultiSearch = numResultsOpenedByDefaultMultiSearch;
  }

  public final int getMaxInfantPerAdult() {
    return maxInfantPerAdult;
  }

  public final void setMaxInfantPerAdult(int maxInfantPerAdult) {
    this.maxInfantPerAdult = maxInfantPerAdult;
  }

  public final int getMaxKidsPerAdult() {
    return maxKidsPerAdult;
  }

  public final void setMaxKidsPerAdult(int maxKidsPerAdult) {
    this.maxKidsPerAdult = maxKidsPerAdult;
  }

  public final int getMaxPassengers() {
    return maxPassengers;
  }

  public final void setMaxPassengers(int maxPassengers) {
    this.maxPassengers = maxPassengers;
  }

  public final int getDefaultNumberOfAdults() {
    return defaultNumberOfAdults;
  }

  public final void setDefaultNumberOfAdults(int defaultNumberOfAdults) {
    this.defaultNumberOfAdults = defaultNumberOfAdults;
  }

  public final int getDefaultNumberOfInfants() {
    return defaultNumberOfInfants;
  }

  public final void setDefaultNumberOfInfants(int defaultNumberOfInfants) {
    this.defaultNumberOfInfants = defaultNumberOfInfants;
  }

  public final int getDefaultNumberOfKids() {
    return defaultNumberOfKids;
  }

  public final void setDefaultNumberOfKids(int defaultNumberOfKids) {
    this.defaultNumberOfKids = defaultNumberOfKids;
  }
}
