package com.odigeo.app.android.lib.interfaces;

/**
 * Created by emiliano.desantis on 29/12/2014.
 */
public interface SoftKeyboardListener {
  void onSoftKeyboardShown(boolean isShowing);
}
