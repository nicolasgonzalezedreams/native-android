package com.odigeo.app.android.lib.interfaces;

/**
 * Created by manuel on 23/09/14.
 */
public interface ITripLegWidgetListener {
  void onClickDestinationFrom(int noSegment);

  void onClickDestinationTo(int noSegment);

  void onClickDepartureDate(int noSegment);

  void onClickRemoveWidget(int noSegment);
}
