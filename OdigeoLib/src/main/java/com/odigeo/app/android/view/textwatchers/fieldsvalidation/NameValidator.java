package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

public class NameValidator extends BaseValidator implements FieldValidator {

  private static final String CHARACTERS_ALLOWED =
      "-/, A-Za-zÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜäëïöüşŞıŠšĞğŸÿƒµçÇ'ŒœßØøÅåÆæÞþÐ ";
  public static final String REGEX_LATIN_CHARS = "[" + CHARACTERS_ALLOWED + "]{2,30}";

  public NameValidator() {
    super();
  }

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_LATIN_CHARS);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
