package com.odigeo.app.android.navigator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.view.FrequentFlyerCodesView;
import com.odigeo.app.android.view.FrequentFlyerDetailView;
import com.odigeo.app.android.view.wrappers.CarrierWrapper;
import com.odigeo.app.android.view.wrappers.UserFrequentFlyersWrapper;
import com.odigeo.data.entity.extensions.UICarrier;
import com.odigeo.data.entity.shoppingCart.Carrier;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.presenter.contracts.navigators.FrequentFlyerCodesNavigatorInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>This screen is used for add and select Frequent Flyer codes from a given list or from a
 * created list. Can retrieve just one code (in the case of {@link PassengerNavigator}) or a list of
 * Frequent Flyer Codes (in the case of {@link TravellersNavigator}). For each case:</p> <p>Retrieve
 * just one Frequent Flyer Code: Use the method {@link Activity#startActivityForResult(Intent, int, * Bundle)} with request Code {@link Constants#REQUEST_CODE_FREQUENT_FLYER_CODES} and add extras,
 * {@link Constants#EXTRA_FREQUENT_FLYER_LIST} as {@link UserFrequentFlyersWrapper} and {@link
 * Constants#EXTRA_FREQUENT_FLYER_CARRIERS} as {@link CarrierWrapper}, the list of {@link Carrier}
 * inside, don't care about the order, the class will order them.</p> <p>Retrieve a list of Frequent
 * Flyer Codes: Use the method {@link Activity#startActivityForResult(Intent, int, Bundle)} with
 * request Code {@link Constants#REQUEST_CODE_FREQUENT_FLYER_CODES}.</p>
 *
 * @author Oscar Álvarez
 * @version 1.0
 * @since 15/09/2015.
 */
public class FrequentFlyerCodesNavigator extends BaseNavigator
    implements FrequentFlyerCodesNavigatorInterface {
  private List<UserFrequentFlyer> frequentFlyers;
  private List<UICarrier> carriers;
  private FrequentFlyerCodesView frequentFlyerCodesView;

  public static List<UICarrier> convertBeans(List<CarrierDTO> carrierDTOs) {
    List<UICarrier> carriers = new ArrayList<>();
    if (carrierDTOs != null) {
      for (CarrierDTO carrierDTO : carrierDTOs) {
        carriers.add(new UICarrier(new Carrier(carrierDTO.getCode(), carrierDTO.getName())));
      }
    }
    return carriers;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view_with_toolbar);
    findViewById(R.id.fl_container).setBackgroundResource(R.drawable.general_background);

    //Fill the list of frequent flyer codes and carriers list, if exist.
    UserFrequentFlyersWrapper frequentFlyerCodesWrapper =
        ((UserFrequentFlyersWrapper) getIntent().getSerializableExtra(
            Constants.EXTRA_FREQUENT_FLYER_LIST));
    if (frequentFlyerCodesWrapper != null) {
      frequentFlyers = frequentFlyerCodesWrapper.getFlyerCardCodes();
    }

    CarrierWrapper carrierWrapper = ((CarrierWrapper) getIntent().getSerializableExtra(
        Constants.EXTRA_FREQUENT_FLYER_CARRIERS));

    if (carrierWrapper != null && carrierWrapper.getCarriers() != null) {
      carriers = new ArrayList<>();
      for (Carrier carrier : carrierWrapper.getCarriers()) {
        carriers.add(new UICarrier(carrier));
      }
      Collections.sort(carriers);
    }
    showFrequentFlyerView();
  }

  public void showDetail(UserFrequentFlyer frequentFlyer, List<UICarrier> carriers,
      Integer position) {
    replaceFragmentWithBackStack(R.id.fl_container,
        FrequentFlyerDetailView.newInstance(frequentFlyer, carriers, position));
  }

  @Override public void showFrequentFlyerView() {
    frequentFlyerCodesView = FrequentFlyerCodesView.newInstance(frequentFlyers, carriers);
    replaceFragment(R.id.fl_container, frequentFlyerCodesView);
  }

  @Override public void addNewCode(long frequentFlyerId, String passengerCode, String carrierCode) {
    frequentFlyerCodesView.addNewCode(frequentFlyerId, passengerCode, carrierCode);
  }

  @Override public void removeCode(int position) {
    frequentFlyerCodesView.removeCode(position);
  }

  public void returnFrequentFlyerCodes(List<UserFrequentFlyer> codes) {
    Intent i = new Intent();
    i.putExtra(Constants.RESPONSE_LIST_FREQUENT_FLYER, new UserFrequentFlyersWrapper(codes));
    setResult(Activity.RESULT_OK, i);
    finish();
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      FragmentManager manager = getSupportFragmentManager();
      Fragment fragment = manager.findFragmentById(R.id.fl_container);
      if (fragment instanceof FrequentFlyerCodesView) {
        ((FrequentFlyerCodesView) fragment).retrieveData();
        return true;
      } else if (fragment instanceof FrequentFlyerDetailView) {
        onBackPressed();
        return true;
      }
    }
    return false;
  }

  @Override public void onBackPressed() {
    FragmentManager manager = getSupportFragmentManager();
    Fragment fragment = manager.findFragmentById(R.id.fl_container);
    if (fragment instanceof FrequentFlyerCodesView) {
      ((FrequentFlyerCodesView) fragment).retrieveData();
    }
    super.onBackPressed();
  }
}
