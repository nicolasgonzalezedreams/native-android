package com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators;

import com.odigeo.app.android.view.textwatchers.fieldsvalidation.BaseValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;

public class VisaValidator extends BaseValidator implements FieldValidator {

  public static final String REGEX_VISA = "(?:^4([0-9]{12}|[0-9]{15}))$";

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_VISA);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
