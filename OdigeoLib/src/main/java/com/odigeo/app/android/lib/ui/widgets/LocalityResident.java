package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.OdigeoSpinnerAdapter;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.dto.ResidentGroupContainerDTO;
import com.odigeo.app.android.lib.models.dto.ResidentGroupDTO;
import com.odigeo.app.android.lib.models.dto.ResidentGroupLocalityDTO;
import com.odigeo.app.android.lib.ui.widgets.base.CustomField;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 2.0
 * @since 19/05/2015
 */
@Deprecated public class LocalityResident extends CustomField {
  private Spinner residentGroupSpinner;
  private Spinner residentLocalitySpinner;
  private List<ResidentGroupContainerDTO> residentGroups;
  private TextView textViewError;

  public LocalityResident(Context context, AttributeSet attrs) {
    super(context, attrs);
    inflateLayout();
  }

  private void inflateLayout() {
    inflate(this.getContext(), R.layout.layout_resident_widget, this);
    residentGroupSpinner = (Spinner) findViewById(R.id.spinnerResidentGroup);
    residentLocalitySpinner = (Spinner) findViewById(R.id.spinnerResidentLocality);
    ((TextView) findViewById(R.id.textViewHint)).setText(
        LocalizablesFacade.getString(this.getContext(), "residentpickerviewcontroller_title"));
    textViewError = ((CustomTextView) findViewById(R.id.textViewError));
    residentGroupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        ResidentGroupDTO item =
            ((ResidentGroupDTO) residentGroupSpinner.getAdapter().getItem(position));
        List<ResidentGroupLocalityDTO> groupLocalityList = new ArrayList<>();

        if (residentGroups != null) {
          for (ResidentGroupContainerDTO groupContainerDTO : residentGroups) {
            if (groupContainerDTO.getName() == item) {
              if (groupContainerDTO.getResidentLocalities() != null) {
                groupLocalityList.addAll(groupContainerDTO.getResidentLocalities());
              }
              break;
            }
          }
        }
        OdigeoSpinnerAdapter<ResidentGroupLocalityDTO> adapter =
            new OdigeoSpinnerAdapter<>(getContext(), groupLocalityList);
        residentLocalitySpinner.setAdapter(adapter);
      }

      @Override public void onNothingSelected(AdapterView<?> parent) {
        //Nothing
      }
    });
  }

  /**
   * Set the values to be showed in the spinner.
   *
   * @param residentGroupContainers List of item to be displayed.
   */
  public final void setResidentGroups(List<ResidentGroupContainerDTO> residentGroupContainers) {
    this.residentGroups = residentGroupContainers;
    if (residentGroupContainers == null) {
      return;
    }
    //Initialize resident groups spinner
    List<ResidentGroupDTO> residentGroups = new ArrayList<>();
    for (ResidentGroupContainerDTO residentGroupContainerDTO : residentGroupContainers) {
      ResidentGroupDTO residentGroupItemModel = residentGroupContainerDTO.getName();
      residentGroups.add(residentGroupItemModel);
    }
    OdigeoSpinnerAdapter<ResidentGroupDTO> adapter =
        new OdigeoSpinnerAdapter<>(getContext(), residentGroups);
    residentGroupSpinner.setAdapter(adapter);
  }

  @Override public final boolean validate() {
    return this.getVisibility() != VISIBLE || isValid();
  }

  @Override public final boolean isValid() {
    boolean isValid = getSelectedItem() != null;
    if (!isValid) {
      textViewError.setText(LocalizablesFacade.getString(this.getContext(),
          "validation_error_locality_code_of_residence"));
    } else {
      textViewError.setText("");
    }
    return getSelectedItem() != null;
  }

  @Override public final void clearValidation() {
    textViewError.setText("");
  }

  public final ResidentGroupLocalityDTO getSelectedItem() {
    return ((ResidentGroupLocalityDTO) residentLocalitySpinner.getSelectedItem());
  }
}

