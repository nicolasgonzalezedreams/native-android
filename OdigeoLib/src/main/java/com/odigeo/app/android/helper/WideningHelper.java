package com.odigeo.app.android.helper;

import android.content.Context;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;

public class WideningHelper {
  private static final String AIRPORT_FORMAT = "%s (%s)";

  private WideningHelper() {
    // Nothing
  }

  public static String getMessageForDepartureAirport(Context context, String airportName,
      String cityName, String airpotIataCode) {
    return LocalizablesFacade.getString(context,
        OneCMSKeys.RESULTSVIEWCONTROLLER_WIDENINGFROMAIRPORT,
        String.format(AIRPORT_FORMAT, airportName, airpotIataCode), cityName).toString();
  }

  public static String getMessageForArrivalAirport(Context context, String airportName,
      String cityName, String airpotIataCode) {
    return LocalizablesFacade.getString(context, OneCMSKeys.RESULTSVIEWCONTROLLER_WIDENINGTOAIRPORT,
        String.format(AIRPORT_FORMAT, airportName, airpotIataCode), cityName).toString();
  }

  public static String getMessageForBothAirports(Context context, String departureCityName,
      String arrivalCityName) {
    return LocalizablesFacade.getString(context,
        OneCMSKeys.RESULTSVIEWCONTROLLER_WIDENINGBOTHAIRPORTS, departureCityName, arrivalCityName)
        .toString();
  }
}
