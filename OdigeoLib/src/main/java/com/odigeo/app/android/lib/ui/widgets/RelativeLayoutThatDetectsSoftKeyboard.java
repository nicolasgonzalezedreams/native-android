package com.odigeo.app.android.lib.ui.widgets;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.odigeo.app.android.lib.interfaces.SoftKeyboardListener;

/**
 * Imported by Emiliano De Santis on 12/15/2014.
 * Source: http://stackoverflow.com/questions/2150078
 */
public class RelativeLayoutThatDetectsSoftKeyboard extends RelativeLayout {

  private static final int PIXELS = 128;
  private SoftKeyboardListener softKeyboardListener;

  public RelativeLayoutThatDetectsSoftKeyboard(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public final void setSoftKeyboardListener(SoftKeyboardListener softKeyboardListener) {
    this.softKeyboardListener = softKeyboardListener;
  }

  @Override protected final void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    int height = MeasureSpec.getSize(heightMeasureSpec);
    Activity activity = (Activity) getContext();
    Rect rect = new Rect();
    activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
    int statusBarHeight = rect.top;
    int screenHeight = activity.getWindowManager().getDefaultDisplay().getHeight();
    int diff = (screenHeight - statusBarHeight) - height;
    if (softKeyboardListener != null) {
      softKeyboardListener.onSoftKeyboardShown(
          diff > PIXELS); // assume all soft keyboards are at least 128 pixels high
    }
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
  }

  //    public interface SoftKeyboardListener {
  //        public void onSoftKeyboardShown(boolean isShowing);
  //    }
}
