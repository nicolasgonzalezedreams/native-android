package com.odigeo.app.android.view;

import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.navigator.NavigationDrawerNavigator;
import com.odigeo.app.android.view.animations.UpdateCarouselDotsAnimation;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DateHelper;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.app.android.view.interfaces.ListenerUpdateCarousel;
import com.odigeo.app.android.view.interfaces.MttCardBackground;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.DivertedSectionCard;
import com.odigeo.data.entity.extensions.UIDivertedCard;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.DivertedTripPresenter;
import com.odigeo.presenter.contracts.views.DivertedTripViewInterface;

/**
 * Created by matia on 1/27/2016.
 */
public class DivertedTripView extends BaseView<DivertedTripPresenter>
    implements DivertedTripViewInterface, MttCardBackground {

  private static final String KEY_DIVERTED_CARD = "KEY_DIVERTED_CARD";
  protected DivertedSectionCard mCard;
  private TextView mTvTitleCard;
  private TextView mTvDestination;
  private TextView mTvCancelledAlert;
  private TextView mTvCancelledInfo;
  private TextView mTvDateLastUpdate;
  private long mBookingId;
  private long mLastUpdate;
  private int mCardPosition;
  private ImageView mIvAutoRenewCardIcon;
  private ImageButton mIbtnRemoveCard;

  private String stringLastUpdate;
  private String stringTo;
  private ListenerUpdateCarousel mListenerUpdateCarousel;
  private UpdateCarouselDotsAnimation mUpdateCarouselDotsAnimation;
  private boolean mItIsPaused;
  private DateHelper mDateHelper;

  public static DivertedTripView newInstance(DivertedSectionCard card, long lastUpdate) {
    Bundle args = new Bundle();
    args.putParcelable(KEY_DIVERTED_CARD, new UIDivertedCard(card));
    args.putLong(Card.KEY_LAST_UPDATE, lastUpdate);
    DivertedTripView fragment = new DivertedTripView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle bundle = getArguments();
    mDateHelper = getDateHelper();
    if (bundle.containsKey(KEY_DIVERTED_CARD)
        && bundle.containsKey(Card.KEY_LAST_UPDATE)
        && bundle.containsKey(Card.KEY_CARD_POSITION)) {
      mCard = (UIDivertedCard) bundle.getParcelable(KEY_DIVERTED_CARD);
      mLastUpdate = bundle.getLong(Card.KEY_LAST_UPDATE);
      mCardPosition = bundle.getInt(Card.KEY_CARD_POSITION);
    }
    mBookingId = mCard.getId();
  }

  @Override public void onResume() {
    super.onResume();
    mListenerUpdateCarousel = (ListenerUpdateCarousel) getContext();

    if (mItIsPaused) {
      mUpdateCarouselDotsAnimation.runDotsAnimation();
    }
  }

  @Override public void onPause() {
    super.onPause();
    mListenerUpdateCarousel = null;
    mItIsPaused = true;
  }

  @Override protected DivertedTripPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideDivertedTripPresenter(this, (NavigationDrawerNavigator) getActivity());
  }

  private DateHelper getDateHelper() {
    return AndroidDependencyInjector.getInstance().provideDateHelper();
  }

  @Override protected int getFragmentLayout() {
    return R.layout.cancelled_card;
  }

  @Override protected void initComponent(View view) {
    mTvTitleCard = (TextView) view.findViewById(R.id.TvTitleCard);
    mTvDestination = (TextView) view.findViewById(R.id.TvDestination);
    mTvCancelledAlert = (TextView) view.findViewById(R.id.TvCancelledAlert);
    mTvCancelledInfo = (TextView) view.findViewById(R.id.TvCancelledInfo);
    mTvDateLastUpdate = (TextView) view.findViewById(R.id.TvDateLastUpdate);
    mIvAutoRenewCardIcon = (ImageView) view.findViewById(R.id.IvAutoRenewCardIcon);
    mIbtnRemoveCard = (ImageButton) view.findViewById(R.id.ibtnRemoveCard);

    mItIsPaused = false;

    initUpdateCarouselDotsAnimation();
    initRefreshButton();
    setListeners(view);
    setUpdateListener();
  }

  private void initRefreshButton() {
    if (mCard.hasToShowRefreshButton()) {
      mIvAutoRenewCardIcon.setVisibility(View.VISIBLE);
    } else {
      mIvAutoRenewCardIcon.setVisibility(View.GONE);
    }
  }

  private void initUpdateCarouselDotsAnimation() {
    mUpdateCarouselDotsAnimation =
        AndroidDependencyInjector.getInstance().provideUpdateCarouselDotsAnimation();
    String updatingData =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_UPDATING).toString();
    mUpdateCarouselDotsAnimation.initAnimationData(updatingData, mTvDateLastUpdate);
  }

  @Override public String getBackgroundImage() {
    String imageUrl = "";
    if (mCard != null) {
      imageUrl = mCard.getImage();
    }
    return imageUrl;
  }

  private void initDataDivertedTripCard() {
    String fromTo = mCard.getFrom().getIata() + " " + stringTo + " " + mCard.getTo().getIata();
    mTvDestination.setText(fromTo);
    if (mLastUpdate == 0) {
      mTvDateLastUpdate.setVisibility(View.GONE);
    } else {
      String lastUpdateText = stringLastUpdate
          + " "
          + "<b>"
          + mDateHelper.getTime(mLastUpdate, getActivity())
          + "</b> "
          + mDateHelper.getDayAndMonth(mLastUpdate, getActivity());
      mTvDateLastUpdate.setText(Html.fromHtml(lastUpdateText));
    }
    mIvAutoRenewCardIcon.setImageDrawable(
        DrawableUtils.getTintedResource(R.drawable.ic_autorenew, R.color.carousel_card_icon,
            getActivity()));
  }

  @Override protected void setListeners() {
    initDataDivertedTripCard();
    mIbtnRemoveCard.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.removeBookingCard(mBookingId);
      }
    });
  }

  protected void setListeners(View view) {
    view.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        String trackingLabel = TrackerConstants.LABEL_CARD_SEE_INFO_FORMAT.replace(
            TrackerConstants.CARD_TYPE_IDENTIFIER, TrackerConstants.LABEL_CARD_DIVERTED)
            .replace(TrackerConstants.CARD_POSITION_IDENTIFIER, String.valueOf(mCardPosition));
        mTracker.trackMTTCardEvent(trackingLabel);
        mPresenter.showBookingDetail(mBookingId);
      }
    });
  }

  private void setUpdateListener() {

    mIvAutoRenewCardIcon.setOnTouchListener(new View.OnTouchListener() {
      @Override public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
          mIvAutoRenewCardIcon.setImageDrawable(
              DrawableUtils.getTintedResource(R.drawable.ic_autorenew,
                  R.color.carousel_card_icon_pressed, getActivity()));
          return true;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
          mIvAutoRenewCardIcon.setImageDrawable(
              DrawableUtils.getTintedResource(R.drawable.ic_autorenew, R.color.carousel_card_icon,
                  getActivity()));

          mListenerUpdateCarousel.onUpdateCarousel();
          startRefreshAnimation();
          mTracker.trackMTTCardEvent(TrackerConstants.LABEL_REFRESH_INFORMATION);
          return true;
        }
        return false;
      }
    });
  }

  private void startRefreshAnimation() {
    mUpdateCarouselDotsAnimation.runDotsAnimation();
    Animation startRotateAnimation =
        AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_refresh_button);
    mIvAutoRenewCardIcon.startAnimation(startRotateAnimation);
  }

  private void stopRefreshAnimation() {
    mUpdateCarouselDotsAnimation.stopDotsAnimation();
    mIvAutoRenewCardIcon.clearAnimation();
  }

  @Override public void onStop() {
    super.onStop();
    stopRefreshAnimation();
  }

  @Override protected void initOneCMSText(View view) {
    mTvCancelledAlert.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_STATUS_DIVERTED_TITLE));
    mTvCancelledInfo.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_STATUS_DIVERTED_MSG));
    mTvTitleCard.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.FLIGHT_NUMBER, mCard.getFlightId(),
            ""));
    stringLastUpdate =
        String.valueOf(LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_LASTUPDATE));
    stringTo = String.valueOf(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_DESTINATION_TO));
  }

  @Override public void setCardPosition(int position) {
    Bundle bundle = getArguments();
    bundle.putInt(Card.KEY_CARD_POSITION, position);
    this.setArguments(bundle);
  }
}
