package com.odigeo.app.android.utils.deeplinking.extractors;

import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.data.entity.TravelType;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 25/04/16
 */
public class ArrivalCityExtractor implements DeepLinkingExtractor {

  @Override public void extractParameter(SearchOptions.Builder builder, String key, String value) {
    if (builder.getTravelType() != TravelType.MULTIDESTINATION) {
      builder.addArrival(0, value);
    } else {
      int index = Integer.parseInt(key.substring(key.length() - 1));
      builder.addArrival(index, value);
    }
  }
}