package com.odigeo.app.android.lib.models.dto;

public class InitSessionResponseDTO {

  protected TestTokenSetResponseDTO testTokenSet;
  protected String hotelsAB;

  public String getHotelsAB() {
    return hotelsAB;
  }

  public void setHotelsAB(String hotelsAB) {
    this.hotelsAB = hotelsAB;
  }

  /**
   * Gets the value of the testTokenSet property.
   *
   * @return possible object is
   * {@link TestTokenSetResponse }
   */
  public TestTokenSetResponseDTO getTestTokenSet() {
    return testTokenSet;
  }

  /**
   * Sets the value of the testTokenSet property.
   *
   * @param value allowed object is
   * {@link TestTokenSetResponse }
   */
  public void setTestTokenSet(TestTokenSetResponseDTO value) {
    this.testTokenSet = value;
  }
}
