package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.support.annotation.IdRes;
import android.util.AttributeSet;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.ui.widgets.base.UnfoldingRowsWidget;

/**
 * Created by emiliano.desantis on 29/09/2014.
 */
public class ContactPhoneInfoWidget extends UnfoldingRowsWidget {

  public ContactPhoneInfoWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public final int getLayout() {
    return R.layout.layout_container_confirmation_contactphoneinfo;
  }

  @IdRes @Override public final int getIdSeparatorLine() {
    return R.id.contact_phone_info_line_separator;
  }

  @Override public final boolean validate() {
    return false;
  }

  @Override public final boolean isValid() {
    return false;
  }

  @Override public void clearValidation() {
    //Nothing to do here
  }
}
