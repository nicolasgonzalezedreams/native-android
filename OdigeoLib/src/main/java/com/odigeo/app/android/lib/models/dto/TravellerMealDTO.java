package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

/**
 * <p>Java class for travellerMeal.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;simpleType name="travellerMeal">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="STANDARD"/>
 *     &lt;enumeration value="KOSHER"/>
 *     &lt;enumeration value="HALAL"/>
 *     &lt;enumeration value="LOW_CHOLESTEROL"/>
 *     &lt;enumeration value="LOW_SODIUM"/>
 *     &lt;enumeration value="VEGAN"/>
 *     &lt;enumeration value="VEGETARIAN"/>
 *     &lt;enumeration value="BABY"/>
 *     &lt;enumeration value="CHILD"/>
 *     &lt;enumeration value="DIABETIC"/>
 *     &lt;enumeration value="GLUTEN_FREE"/>
 *     &lt;enumeration value="HINDU"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
public enum TravellerMealDTO implements Serializable {

  STANDARD, KOSHER, HALAL, LOW_CHOLESTEROL, LOW_SODIUM, VEGAN, VEGETARIAN, BABY, CHILD, DIABETIC, GLUTEN_FREE, HINDU;

  public static TravellerMealDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
