package com.odigeo.app.android.lib.models;

import com.odigeo.app.android.lib.models.dto.LocationDTO;
import java.io.Serializable;

/**
 * Created by manuel on 03/11/14.
 */
public class Section implements Serializable {
  private long departureDate;
  private long arrivalDate;
  private String carrier;
  private String operatingCarrier;
  private LocationDTO locationFrom;
  private LocationDTO locationTo;
  private String id;

  public Section() {
  }

  public Section(String iataCode) {
    locationFrom = new LocationDTO(iataCode);
    locationTo = new LocationDTO(iataCode);
  }

  public long getDepartureDate() {
    return departureDate;
  }

  public void setDepartureDate(long departureDate) {
    this.departureDate = departureDate;
  }

  public long getArrivalDate() {
    return arrivalDate;
  }

  public void setArrivalDate(long arrivalDate) {
    this.arrivalDate = arrivalDate;
  }

  public String getCarrier() {
    return carrier;
  }

  public void setCarrier(String carrier) {
    this.carrier = carrier;
  }

  public String getOperatingCarrier() {
    return operatingCarrier;
  }

  public void setOperatingCarrier(String operatingCarrier) {
    this.operatingCarrier = operatingCarrier;
  }

  public LocationDTO getLocationFrom() {
    return locationFrom;
  }

  public void setLocationFrom(LocationDTO locationFrom) {
    this.locationFrom = locationFrom;
  }

  public LocationDTO getLocationTo() {
    return locationTo;
  }

  public void setLocationTo(LocationDTO locationTo) {
    this.locationTo = locationTo;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
