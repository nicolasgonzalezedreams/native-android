package com.odigeo.app.android.lib.data.database;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.utils.exceptions.DataBaseException;
import java.io.IOException;

/**
 * @author Cristian Fanjul
 * @since 11/05/2015
 */
public abstract class BaseDao<T> {
  private final SQLiteDatabase db;

  public BaseDao() {
    try {
      db = AndroidDependencyInjector.getInstance().provideSQLiteDatabase();
    } catch (IOException | SQLException e) {
      throw new DataBaseException(e.getMessage(), e);
    }
  }

  /**
   * Insert object method.
   *
   * @return object Id in database.
   */
  public abstract long insert(T object);

  /**
   * Update object method.
   */
  public abstract boolean update(T object);

  /**
   * Delete object method.
   */
  public abstract boolean delete(T object);

  /**
   * Checks if a cursor es empty.
   */
  protected boolean isCursorEmpty(Cursor cursor) {
    if (!cursor.moveToFirst() || cursor.getCount() == 0) {
      return true;
    }
    return false;
  }

  /**
   * Tries to update an object. If the update fails, means the object does not exist and needs to
   * be inserted.
   */
  public void insertOrUpdate(T object) {
    if (!update(object)) {
      insert(object);
    }
  }

  /**
   * Return database instance.
   */
  public final SQLiteDatabase getDb() {
    return this.db;
  }
}
