package com.odigeo.app.android.view.helpers;

import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.tools.DurationFormatter;

public class OdigeoDurationFormatter implements DurationFormatter {

  private static final int MINUTES_PER_HOUR = 60;

  LocalizableProvider localizableProvider;

  public OdigeoDurationFormatter(LocalizableProvider localizableProvider) {

    this.localizableProvider = localizableProvider;
  }

  @Override public String format(long minutes) {
    long hours = minutes / MINUTES_PER_HOUR;
    long min = minutes % MINUTES_PER_HOUR;

    return format(hours, min);
  }

  @Override public String format(long hour, long minutes) {
    return String.format(localizableProvider.getString(OneCMSKeys.TEMPLATE_TIME_DURATION), hour,
        minutes);
  }
}
