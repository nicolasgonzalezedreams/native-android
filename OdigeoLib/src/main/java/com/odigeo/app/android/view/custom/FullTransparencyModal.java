package com.odigeo.app.android.view.custom;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.tracker.TrackerControllerInterface;

public class FullTransparencyModal extends RelativeLayout {

  private static final int ANIMATION_DURATION = 500;
  private static final int MEASURE_SPEC_MAX_VALUE = 10000000;

  private TrackerControllerInterface mTrackerController;
  private FrameLayout flFullTransparencyContainer;
  private RelativeLayout mRlFullTransparencyPanel;
  private TextView mTvFtText1, mTvFtText2, mTvFtText3;
  private Button mBtnOK;

  public FullTransparencyModal(Context context) {
    super(context);
    inflate(context, R.layout.modal_full_transparency, this);
    mTrackerController = AndroidDependencyInjector.getInstance().provideTrackerController();
    mTrackerController.trackFullTransparencyDialogAppearance(true);
    bindViews();
    addContent();
  }

  private void bindViews() {
    flFullTransparencyContainer = (FrameLayout) findViewById(R.id.flFullTransparencyContainer);
    mRlFullTransparencyPanel = (RelativeLayout) findViewById(R.id.rlFullTransparencyPanel);
    mTvFtText1 = (TextView) findViewById(R.id.tvFtText1);
    mTvFtText2 = (TextView) findViewById(R.id.tvFtText2);
    mTvFtText3 = (TextView) findViewById(R.id.tvFtText3);
    mBtnOK = (Button) findViewById(R.id.btnOK);
    mBtnOK.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        mTrackerController.trackFullTransparencyDialogAppearance(false);
        hidePanel();
      }
    });
  }

  private void addContent() {
    mTvFtText1.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.FULLPRICE_LAYER_TEXT_1_NEW));
    mTvFtText2.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.FULLPRICE_LAYER_TEXT_2_NEW));
    mTvFtText3.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.FULLPRICE_LAYER_TEXT_3_NEW));
    mBtnOK.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.FULLPRICE_LAYER_BUTTON_NEW));
  }

  public void showPanel() {

    int wrapSpec =
        View.MeasureSpec.makeMeasureSpec(MEASURE_SPEC_MAX_VALUE, View.MeasureSpec.AT_MOST);
    mRlFullTransparencyPanel.measure(wrapSpec, wrapSpec);
    TranslateAnimation animate =
        new TranslateAnimation(0, 0, mRlFullTransparencyPanel.getMeasuredHeight(), 0);
    animate.setDuration(ANIMATION_DURATION);
    animate.setFillAfter(true);
    mRlFullTransparencyPanel.startAnimation(animate);
    mRlFullTransparencyPanel.setVisibility(View.VISIBLE);
  }

  private void hidePanel() {
    int wrapSpec =
        View.MeasureSpec.makeMeasureSpec(MEASURE_SPEC_MAX_VALUE, View.MeasureSpec.AT_MOST);
    mRlFullTransparencyPanel.measure(wrapSpec, wrapSpec);
    TranslateAnimation animate =
        new TranslateAnimation(0, 0, 0, mRlFullTransparencyPanel.getHeight());
    animate.setDuration(ANIMATION_DURATION);
    animate.setFillAfter(true);
    animate.setAnimationListener(new Animation.AnimationListener() {
      @Override public void onAnimationStart(Animation animation) {

      }

      @Override public void onAnimationEnd(Animation animation) {
        flFullTransparencyContainer.setVisibility(GONE);
      }

      @Override public void onAnimationRepeat(Animation animation) {

      }
    });
    mRlFullTransparencyPanel.startAnimation(animate);
  }
}