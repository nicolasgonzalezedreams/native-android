package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

/**
 * <p>Java class for CrossFaringItinerary complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="CrossFaringItinerary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="out" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="in" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */

public class CrossFaringItineraryDTO implements Serializable {

  protected Integer id;
  protected Integer out;
  protected Integer in;

  /**
   * Gets the value of the id property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setId(Integer value) {
    this.id = value;
  }

  /**
   * Gets the value of the out property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getOut() {
    return out;
  }

  /**
   * Sets the value of the out property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setOut(Integer value) {
    this.out = value;
  }

  /**
   * Gets the value of the in property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getIn() {
    return in;
  }

  /**
   * Sets the value of the in property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setIn(Integer value) {
    this.in = value;
  }
}
