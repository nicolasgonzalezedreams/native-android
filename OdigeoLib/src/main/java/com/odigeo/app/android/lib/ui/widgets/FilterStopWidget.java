package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.consts.ScalesTypes;
import com.odigeo.app.android.lib.models.FilterStopModel;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class FilterStopWidget extends LinearLayout
    implements View.OnClickListener, PropertyChangeListener {
  private static final int NUM_OPTIONS = 3;
  private static final int MAX_VALUE = 15;
  private FilterSeekBarWidget seekScales;
  private FilterCheckBoxWidget[] scaleOptions;
  private FlightSegment flightSegment;
  private int numOptionsSelected;
  private int widgetNumber;

  public FilterStopWidget(Context context, FlightSegment flightSegment, String textTitle) {
    super(context);
    inflateLayout();

    setFlightSegment(flightSegment);
    setTitle(textTitle);
    init();
  }

  public FilterStopWidget(Context context, AttributeSet attrs) {

    super(context, attrs);
    inflateLayout();
    init();
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.layout_filter_stop_widget, this);
    scaleOptions = new FilterCheckBoxWidget[NUM_OPTIONS];
    scaleOptions[0] = (FilterCheckBoxWidget) findViewById(R.id.check_filter_stop_direct);
    scaleOptions[0].setExtra("Direct");

    scaleOptions[1] = (FilterCheckBoxWidget) findViewById(R.id.check_filter_stop_one_scale);
    scaleOptions[1].setExtra("OneLeg");

    scaleOptions[2] = (FilterCheckBoxWidget) findViewById(R.id.check_filter_stop_more_scales);
    scaleOptions[2].setExtra("TwoLegs");

    seekScales = (FilterSeekBarWidget) findViewById(R.id.seekBar_filter_stop_maxNumber);
  }

  private void init() {
    initScaleOptionsListener();
    setTrueAllOptions();

    seekScales.setPropertyChangeListener(this);
  }

  private void initScaleOptionsListener() {
    for (FilterCheckBoxWidget widget : scaleOptions) {
      widget.setOnClickListener(this);
    }
  }

  private void setTrueAllOptions() {
    for (FilterCheckBoxWidget widget : scaleOptions) {
      widget.setCheck(true);
    }
    numOptionsSelected = scaleOptions.length;
  }

  private void drawFlight() {
    if (flightSegment != null) {
      ((TextView) findViewById(R.id.tvFilterTimeSubtitle)).setText(getSubtitle());
      seekScales.setSubTitle(getCodesCities());

      seekScales.setMaxValue(MAX_VALUE);
    }
  }

  public final void setMaxValueSeekBar(int maxValueSeekBar) {
    seekScales.setMaxValue(maxValueSeekBar);
  }

  private String getSubtitle() {
    SimpleDateFormat dateFormat = OdigeoDateUtils.getGmtDateFormat("EEE, dd MMM yy");

    return String.format("%s - %s", getCodesCities(), dateFormat.format(flightSegment.getDate()));
  }

  private String getCodesCities() {
    return String.format("%s - %s", flightSegment.getDepartureCity().getIataCode(),
        flightSegment.getArrivalCity().getIataCode());
  }

  private void setFlightSegment(FlightSegment flightSegment) {
    this.flightSegment = flightSegment;
    if (flightSegment != null) {
      drawFlight();
    }
  }

  private void setTitle(String title) {
    if (title != null) {
      ((TextView) findViewById(R.id.tvFilterTimeTitle)).setText(title);
    }
  }

  @Override public final void onClick(View v) {
    FilterCheckBoxWidget widget = (FilterCheckBoxWidget) v;
    if (widget.isCheck()) {
      if (numOptionsSelected > 1) {
        widget.setCheck(false);
        numOptionsSelected--;

        if (widget.getExtra().equals("Direct")) {
          //                    stops_X_Y_segment_T
          // GATracker - Filters - stops page
          postGAEventFilteredFlight(false, 0);
        } else if (widget.getExtra().equals("OneLeg")) {
          //                    stops_X_Y_segment_T
          // GATracker - Filters - stops page
          postGAEventFilteredFlight(false, 1);
        } else if (widget.getExtra().equals("TwoLegs")) {
          //                    stops_X_Y_segment_T
          // GATracker - Filters - stops page
          postGAEventFilteredFlight(false, 2);
        }
      }
    } else {
      widget.setCheck(true);
      numOptionsSelected++;

      if (widget.getExtra().equals("Direct")) {
        //                    stops_X_Y_segment_T
        // GATracker - Filters - stops page
        postGAEventFilteredFlight(true, 0);
      } else if (widget.getExtra().equals("OneLeg")) {
        //                    stops_X_Y_segment_T
        // GATracker - Filters - stops page
        postGAEventFilteredFlight(true, 1);
      } else if (widget.getExtra().equals("TwoLegs")) {
        //                    stops_X_Y_segment_T
        // GATracker - Filters - stops page
        postGAEventFilteredFlight(true, 2);
      }
    }
  }

  public final List<ScalesTypes> getScalesSelected() {
    List<ScalesTypes> scalesSelected = new ArrayList<ScalesTypes>();
    for (int i = 0; i < scaleOptions.length; i++) {
      if (scaleOptions[i].isCheck()) {
        scalesSelected.add(getScaleTypeByIndex(i));
      }
    }
    return scalesSelected;
  }

  public final void setScalesSelected(List<ScalesTypes> scalesSelected) {
    scaleOptions[0].setCheck(false);
    scaleOptions[1].setCheck(false);
    scaleOptions[2].setCheck(false);

    for (ScalesTypes scaleType : scalesSelected) {

      if (scaleType == ScalesTypes.DIRECT) {
        scaleOptions[0].setCheck(true);
      } else if (scaleType == ScalesTypes.ONE_SCALE) {
        scaleOptions[1].setCheck(true);
      } else if (scaleType == ScalesTypes.MORE_SCALES) {
        scaleOptions[2].setCheck(true);
      }
    }
  }

  private ScalesTypes getScaleTypeByIndex(int index) {
    if (index == 0) {
      return ScalesTypes.DIRECT;
    } else if (index == 1) {
      return ScalesTypes.ONE_SCALE;
    }
    return ScalesTypes.MORE_SCALES;
  }

  public final FilterStopModel getStopModel() {

    FilterStopModel stopModel = new FilterStopModel();

    stopModel.setDirectFlight(false);
    stopModel.setOneScale(false);
    stopModel.setPlusTwoScales(false);
    stopModel.setScaleMaxDuration(seekScales.getSelectedValue());

    List<ScalesTypes> scalesTypes = getScalesSelected();
    for (ScalesTypes currentScaleType : scalesTypes) {
      if (currentScaleType == ScalesTypes.ONE_SCALE) {
        stopModel.setOneScale(true);
      } else if (currentScaleType == ScalesTypes.DIRECT) {
        stopModel.setDirectFlight(true);
      } else if (currentScaleType == ScalesTypes.MORE_SCALES) {
        stopModel.setPlusTwoScales(true);
      }
    }

    return stopModel;
  }

  public final void setDataModel(FilterStopModel model) {
    seekScales.setValueSelected(model.getScaleMaxDuration());

    scaleOptions[0].setCheck(false);
    scaleOptions[1].setCheck(false);
    scaleOptions[2].setCheck(false);

    if (model.isDirectFlight()) {
      scaleOptions[0].setCheck(true);
    }
    if (model.isOneScale()) {
      scaleOptions[1].setCheck(true);
    }
    if (model.isPlusTwoScales()) {
      scaleOptions[2].setCheck(true);
    }

    seekScales.setValueSelected(model.getScaleMaxDuration());
  }

  public final int getWidgetNumber() {
    return widgetNumber;
  }

  public final void setWidgetNumber(int widgetNumber) {
    this.widgetNumber = widgetNumber;
  }

  private void postGAEventFilteredFlight(boolean isChecked, int legs) {
    int flightLegs = 0;

    flightLegs = legs;

    String labelUnchecked = GAnalyticsNames.LABEL_PARTIAL_UNCHECKED;
    String labelChecked = GAnalyticsNames.LABEL_PARTIAL_CHECKED;

    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_FILTERS,
            GAnalyticsNames.ACTION_RESULTS_FILTERS,
            GAnalyticsNames.LABEL_PARTIAL_STOPS
                + flightLegs
                + "_"
                + (isChecked ? labelChecked : labelUnchecked)
                + GAnalyticsNames.LABEL_PARTIAL_SEGMENT
                + this.getWidgetNumber()));
  }

  @Override public void propertyChange(PropertyChangeEvent event) {
    //TODO add action to propertyChange
  }
}
