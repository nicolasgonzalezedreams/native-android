package com.odigeo.app.android.navigator;

import android.os.Bundle;

import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.view.MembershipQAView;
import com.odigeo.presenter.contracts.navigators.MembershipQANavigatorInterface;

public class MembershipQANavigator extends BaseNavigator implements MembershipQANavigatorInterface {

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view);
    setTitle(R.string.qa_mode_membership_title);
    replaceFragment(R.id.fl_container, MembershipQAView.newInstance());
  }
}
