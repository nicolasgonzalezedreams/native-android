package com.odigeo.app.android.lib.models.dto;

/**
 * @author miguel
 */
public class SearchRequestDTO extends BaseRequestDTO {

  protected ItinerarySearchRequestDTO itinerarySearchRequest;
  protected AccommodationSearchRequestDTO accommodationSearchRequest;
  protected Integer buypath;
  private PreferencesRequestDTO preferences;

  /**
   * Gets the value of the itinerarySearchRequest property.
   *
   * @return possible object is {@link ItinerarySearchRequestDTO }
   */
  public ItinerarySearchRequestDTO getItinerarySearchRequest() {
    return itinerarySearchRequest;
  }

  /**
   * Sets the value of the itinerarySearchRequest property.
   *
   * @param value allowed object is {@link ItinerarySearchRequestDTO }
   */
  public void setItinerarySearchRequest(ItinerarySearchRequestDTO value) {
    this.itinerarySearchRequest = value;
  }

  public PreferencesRequestDTO getPreferences() {
    return preferences;
  }

  public void setPreferences(PreferencesRequestDTO preferences) {
    this.preferences = preferences;
  }

  public AccommodationSearchRequestDTO getAccommodationSearchRequest() {
    return accommodationSearchRequest;
  }

  public void setAccommodationSearchRequest(
      AccommodationSearchRequestDTO accommodationSearchRequest) {
    this.accommodationSearchRequest = accommodationSearchRequest;
  }

  /**
   * Gets the value of the buypath property.
   *
   * @return possible object is {@link Integer }
   */
  public Integer getBuypath() {
    return buypath;
  }

  /**
   * Sets the value of the buypath property.
   *
   * @param value allowed object is {@link Integer }
   */
  public void setBuypath(Integer buypath) {
    this.buypath = buypath;
  }
}
