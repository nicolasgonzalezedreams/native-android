package com.odigeo.app.android.lib.fragments.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoSearchResultsActivity;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.interfaces.OdigeoInterfaces;
import com.odigeo.app.android.lib.interfaces.SearchTripListener;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.app.android.lib.ui.widgets.SearchTripWidget;
import com.odigeo.app.android.lib.ui.widgets.TopBriefWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.view.constants.OneCMSKeys;

/**
 * Created by manuel on 12/09/14.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 2.1
 * @since 06/02/2015
 */
public abstract class OdigeoNoResultsFragment extends android.support.v4.app.Fragment
    implements SearchTripListener {

  protected SearchTripWidget searchTripWidget;
  private SearchOptions searchOptions;
  private OdigeoInterfaces.SearchListener listener;
  private OdigeoSearchResultsActivity parentActivity;
  private OdigeoSession odigeoSession;
  private OdigeoApp odigeoApp;

  public abstract boolean isDataValid();

  public final void setListener(OdigeoInterfaces.SearchListener listener) {
    this.listener = listener;
  }

  @Override public void onAttach(Activity activity) {
    super.onAttach(activity);
    parentActivity = (OdigeoSearchResultsActivity) activity;
    odigeoApp = (OdigeoApp) parentActivity.getApplication();
    odigeoSession = odigeoApp.getOdigeoSession();
  }

  @Override public final View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View viewRoot = inflater.inflate(R.layout.layout_search_noresults, container, false);

    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {
      TextView tvResetSearch = (TextView) viewRoot.findViewById(R.id.tvResetSearch);
      tvResetSearch.setText(LocalizablesFacade.getString(getContext(),
          OneCMSKeys.NORESULTSSEARCHORFILTERSCELL_LABELRESETSEARCH_TEXT));

      setSearchTripWidget((SearchTripWidget) viewRoot.findViewById(R.id.searchWidgetNoResults));

      getSearchTripWidget().setSegments(searchOptions.getFlightSegments());
      getSearchTripWidget().setTravelType(searchOptions.getTravelType());
      getSearchTripWidget().setListener(this);

      ((TopBriefWidget) viewRoot.findViewById(R.id.topbrief_results)).addData(searchOptions);
    }
    return viewRoot;
  }

  @Override public final void onResume() {
    super.onResume();
    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {
      setControlsValues();
    }
  }

  @Override public final void onViewCreated(View view, Bundle savedInstanceState) {

    super.onViewCreated(view, savedInstanceState);
    if (odigeoSession == null || !odigeoSession.isDataHasBeenLoaded()) {
      Util.sendToHome(parentActivity, odigeoApp);
    }
  }

  /**
   * Sets the values of the controls chosen by the user
   */
  public void setControlsValues() {
    FlightSegment departureSegment = getSearchOptions().getFlightSegments().get(0);

    if (getSearchOptions().getFlightSegments().size() > 1) {
      getSearchOptions().getFlightSegments().get(1);
    }

    //Set Departure City
    if (departureSegment.getDepartureCity() != null) {
      getSearchTripWidget().setCityFrom(departureSegment.getDepartureCity());
    }

    //Set Arrival City
    if (departureSegment.getArrivalCity() != null) {
      getSearchTripWidget().setCityTo(departureSegment.getArrivalCity());
    }

    //Set the departure Date
    if (departureSegment.getDate() != 0) {
      getSearchTripWidget().setDepartureDate(
          OdigeoDateUtils.createDate(departureSegment.getDate()));
    }

    //Set if it is a direct flight
    getSearchTripWidget().getChkDirectFlight().setChecked(getSearchOptions().isDirectFlight());

    //Set the cabin class selected
    getSearchTripWidget().getSpinnerClass().setSelection(getSearchOptions().getCabinClass());

    //Set the number of passengers selected
    getSearchTripWidget().setPassengers(this.getSearchOptions().getNumberOfAdults(),
        this.getSearchOptions().getNumberOfKids(), this.getSearchOptions().getNumberOfBabies());

    //Should be enabled the Search Flights Buttons?
    getSearchTripWidget().getBtnSearchFlight().setEnabled(isDataValid());
  }

  public final SearchOptions getSearchOptions() {

    return searchOptions;
  }

  public final void setSearchOptions(SearchOptions options) {
    this.searchOptions = options;
  }

  @Override public final void onClickSearch(SearchTripWidget searchTripWidget, boolean isResident) {
    if (listener != null) {
      postGAEventNewSearch();
      listener.onClickSearch(searchTripWidget, false);
    }
  }

  @Override public void onClickDirectFlight(boolean checked) {
    //Nothing to do here
  }

  @Override public void onClickButtonPassenger() {
    //Nothing to do here
  }

  @Override public void onCabinClassSelected(CabinClassDTO cabinClassSelected) {
    //Nothing to do here
  }

  @Override public void onResidentSwitchClicked(boolean checked) {
    //Nothing to do here
  }

  @Override public final void onActivityResult(int requestCode, int resultCode, Intent data) {
    //If it is the Flight Return answer in the calendar Activity
    //if(requestCode == FlightsDirection.RETURN.getCredentialTypeValue())
    setControlsValues();
  }

  public final SearchTripWidget getSearchTripWidget() {
    return searchTripWidget;
  }

  public final void setSearchTripWidget(SearchTripWidget searchTripWidget) {
    this.searchTripWidget = searchTripWidget;
  }

  private void postGAEventNewSearch() {

    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_RESULTS,
            GAnalyticsNames.ACTION_RESULTS_LIST, GAnalyticsNames.LABEL_NO_RESULTS_NEW_SEARCH));
  }
}
