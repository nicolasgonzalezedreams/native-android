package com.odigeo.app.android.lib.models.dto;

public class ProviderBookingItineraryDTO {

  protected Long providerItineraryId;

  /**
   * @return the providerItineraryId
   */
  public Long getProviderItineraryId() {
    return providerItineraryId;
  }

  /**
   * @param providerItineraryId the providerItineraryId to set
   */
  public void setProviderItineraryId(Long providerItineraryId) {
    this.providerItineraryId = providerItineraryId;
  }
}
