package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class CreditCardTypeDTO implements Serializable {

  private String code;
  private String name;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean equals(Object other) {
    if (!(other instanceof CreditCardTypeDTO)) {
      return false;
    }

    return getCode().equals(((CreditCardTypeDTO) other).getCode());
  }
}
