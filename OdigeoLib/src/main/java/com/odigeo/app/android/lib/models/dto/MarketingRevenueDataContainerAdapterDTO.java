package com.odigeo.app.android.lib.models.dto;

import java.util.ArrayList;
import java.util.List;

public class MarketingRevenueDataContainerAdapterDTO {

  private List<MarketingRevenueDataAdapterDTO> marketingRevenue;

  /**
   * Gets the value of the marketingRevenue property.
   *
   * @return A list of MarketingRevenueDataAdapterDTO
   */
  public List<MarketingRevenueDataAdapterDTO> getMarketingRevenue() {
    if (marketingRevenue == null) {
      marketingRevenue = new ArrayList<MarketingRevenueDataAdapterDTO>();
    }
    return marketingRevenue;
  }

  /**
   * Sets the value of the marketingRevenue property.
   */

  public void setMarketingRevenue(List<MarketingRevenueDataAdapterDTO> marketingRevenue) {
    this.marketingRevenue = marketingRevenue;
  }
}
