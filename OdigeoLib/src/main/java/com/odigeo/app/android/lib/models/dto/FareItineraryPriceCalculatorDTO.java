package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class FareItineraryPriceCalculatorDTO implements Serializable {

  protected FeeInfoDTO feeInfo;
  protected BigDecimal sortPrice;
  protected BigDecimal markup;

  /**
   * Gets the value of the feeInfo property.
   *
   * @return possible object is {@link FeeInfoDTO }
   */
  public FeeInfoDTO getFeeInfo() {
    return feeInfo;
  }

  /**
   * Sets the value of the feeInfo property.
   *
   * @param value allowed object is {@link FeeInfoDTO }
   */
  public void setFeeInfo(FeeInfoDTO value) {
    this.feeInfo = value;
  }

  /**
   * Gets the value of the sortPrice property.
   *
   * @return possible object is {@link java.math.BigDecimal }
   */
  public BigDecimal getSortPrice() {
    return sortPrice;
  }

  /**
   * Sets the value of the sortPrice property.
   *
   * @param value allowed object is {@link java.math.BigDecimal }
   */
  public void setSortPrice(BigDecimal value) {
    this.sortPrice = value;
  }

  /**
   * Gets the value of the markup property.
   *
   * @return possible object is {@link java.math.BigDecimal }
   */
  public BigDecimal getMarkup() {
    return markup;
  }

  /**
   * Sets the value of the markup property.
   *
   * @param value allowed object is {@link java.math.BigDecimal }
   */
  public void setMarkup(BigDecimal value) {
    this.markup = value;
  }
}
