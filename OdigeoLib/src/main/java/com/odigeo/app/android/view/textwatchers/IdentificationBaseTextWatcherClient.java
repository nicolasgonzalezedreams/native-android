package com.odigeo.app.android.view.textwatchers;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.textwatchers.OnFocusChange.BaseOnFocusChange;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.CIFValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.NIEValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.NIFValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.NationalIdCardValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.PassportValidator;
import com.odigeo.data.entity.shoppingCart.BuyerIdentificationType;
import com.odigeo.data.entity.shoppingCart.TravellerIdentificationType;

public class IdentificationBaseTextWatcherClient {

  TextInputLayout mTextInputLayout;
  Context mContext;
  PassengerBaseTextWatchersClient mPassengerBaseTextWatcherClient;
  BuyerBaseTextWatchersClient mBuyerBaseTextWatcherClient;

  public IdentificationBaseTextWatcherClient(Context context, TextInputLayout textInputLayout,
      BuyerBaseTextWatchersClient buyerBaseTextWatcherClient) {
    mTextInputLayout = textInputLayout;
    mContext = context;
    mBuyerBaseTextWatcherClient = buyerBaseTextWatcherClient;
  }

  public IdentificationBaseTextWatcherClient(Context context, TextInputLayout textInputLayout,
      PassengerBaseTextWatchersClient passengerBaseTextWatcherClient) {
    mTextInputLayout = textInputLayout;
    mContext = context;
    mPassengerBaseTextWatcherClient = passengerBaseTextWatcherClient;
  }

  public void setTravellerBehaviour(TravellerIdentificationType travellerIdentificationType) {

    BaseOnFocusChange identificationBaseOnFocusChange = null;

    switch (travellerIdentificationType) {
      case CIF:
        identificationBaseOnFocusChange = CIFBaseTextWatcher();
        break;
      case NIE:
        identificationBaseOnFocusChange = NIEBaseTextWatcher();
        break;
      case NIF:
        identificationBaseOnFocusChange = NIFBaseTextWatcher();
        break;
      case NATIONAL_ID_CARD:
        identificationBaseOnFocusChange = NationalIDBaseTextWatcher();
        break;
      case PASSPORT:
        identificationBaseOnFocusChange = PassportBaseTextWatcher();
        break;
      default:
        break;
    }
    if (mTextInputLayout.getEditText() != null) {
      mTextInputLayout.getEditText().setOnFocusChangeListener(identificationBaseOnFocusChange);
      mTextInputLayout.getEditText().addTextChangedListener(new BaseTextWatcher(mTextInputLayout));
      mPassengerBaseTextWatcherClient.updateIdentificationAfterChangeInSpinner(mTextInputLayout);
    }
  }

  public void setBuyerBehaviour(BuyerIdentificationType buyerIdentificationType) {

    BaseOnFocusChange identificationBaseOnFocusChange = null;

    switch (buyerIdentificationType) {
      case CIF:
        identificationBaseOnFocusChange = CIFBaseTextWatcher();
        break;
      case NIE:
        identificationBaseOnFocusChange = NIEBaseTextWatcher();
        break;
      case NIF:
        identificationBaseOnFocusChange = NIFBaseTextWatcher();
        break;
      case NATIONAL_ID_CARD:
        identificationBaseOnFocusChange = NationalIDBaseTextWatcher();
        break;
      case PASSPORT:
        identificationBaseOnFocusChange = PassportBaseTextWatcher();
        break;
      default:
        break;
    }
    if (mTextInputLayout.getEditText() != null) {
      mTextInputLayout.getEditText().setOnFocusChangeListener(identificationBaseOnFocusChange);
      mTextInputLayout.getEditText().addTextChangedListener(new BaseTextWatcher(mTextInputLayout));
      mBuyerBaseTextWatcherClient.updateIdentificationAfterChangeInSpinner(mTextInputLayout);
    }
  }

  private BaseOnFocusChange CIFBaseTextWatcher() {
    return new BaseOnFocusChange(mContext, mTextInputLayout, new CIFValidator(),
        OneCMSKeys.VALIDATION_ERROR_IDENTIFICATION);
  }

  private BaseOnFocusChange NIEBaseTextWatcher() {
    return new BaseOnFocusChange(mContext, mTextInputLayout, new NIEValidator(),
        OneCMSKeys.VALIDATION_ERROR_IDENTIFICATION);
  }

  private BaseOnFocusChange NIFBaseTextWatcher() {
    return new BaseOnFocusChange(mContext, mTextInputLayout, new NIFValidator(),
        OneCMSKeys.VALIDATION_ERROR_IDENTIFICATION);
  }

  private BaseOnFocusChange NationalIDBaseTextWatcher() {
    return new BaseOnFocusChange(mContext, mTextInputLayout, new NationalIdCardValidator(),
        OneCMSKeys.VALIDATION_ERROR_IDENTIFICATION);
  }

  private BaseOnFocusChange PassportBaseTextWatcher() {
    return new BaseOnFocusChange(mContext, mTextInputLayout, new PassportValidator(),
        OneCMSKeys.VALIDATION_ERROR_IDENTIFICATION);
  }
}
