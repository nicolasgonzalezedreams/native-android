package com.odigeo.app.android.view.textwatchers;

import android.content.Context;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.text.InputFilter;
import android.widget.LinearLayout;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.interfaces.TextWatcherValidationsListener;
import com.odigeo.app.android.view.textwatchers.OnFocusChange.BaseOnFocusChange;
import com.odigeo.app.android.view.textwatchers.OnFocusChange.CVVOnFocusChange;
import com.odigeo.app.android.view.textwatchers.OnFocusChange.CreditCardOnFocusChange;
import com.odigeo.app.android.view.textwatchers.OnFocusChange.NameOnCardFocusChange;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.CVVAmexValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.CVVAnyCardValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.CVVCardValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.CreditCardValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.NameValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.RequestValidationHandler;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.presenter.PaymentFormWidgetPresenter;
import com.odigeo.presenter.contracts.views.PaymentFormWidgetInterface;
import java.util.HashMap;

public class PaymentMethodBaseTextWatchersClient implements TextWatcherValidationsListener {

  private static final int AMEX_CVV_LENGTH = 4;
  private static final int NORMAL_CVV_LENGHT = 3;

  private TextInputLayout mTilNameOnCard, mTilCreditCardNumber, mTilCVV;
  private boolean isBinCheckCallFinished = false;
  private LinearLayout mLlCVVTooltip;
  private PaymentFormWidgetPresenter mPaymentFormWidgetPresenter;
  private Context mContext;
  private HashMap<TextInputLayout, Boolean> mFieldsValidationsCreditCard = new HashMap<>();
  private PaymentFormWidgetInterface mPaymentFormWidgetInterface;
  private TrackerControllerInterface mTracker;
  private RequestValidationHandler requestValidationHandler;
  private CreditCardValidator creditCardValidator = new CreditCardValidator();

  public PaymentMethodBaseTextWatchersClient(PaymentFormWidgetInterface paymentFormWidgetInterface,
      TextInputLayout tilNameOnCard, TextInputLayout tilCreditCardNumber, TextInputLayout tilCVV,
      Context context, PaymentFormWidgetPresenter paymentFormWidgetPresenter,
      TrackerControllerInterface tracker, LinearLayout llCVVTooltip,
      RequestValidationHandler requestValidationHandler) {
    mPaymentFormWidgetInterface = paymentFormWidgetInterface;
    mTilNameOnCard = tilNameOnCard;
    mTilCreditCardNumber = tilCreditCardNumber;
    mTilCVV = tilCVV;
    mContext = context;
    mPaymentFormWidgetPresenter = paymentFormWidgetPresenter;
    mTracker = tracker;
    mLlCVVTooltip = llCVVTooltip;
    this.requestValidationHandler = requestValidationHandler;
    initValidationsHashmap();
  }

  private void initValidationsHashmap() {
    mFieldsValidationsCreditCard.put(mTilCreditCardNumber, false);
    mFieldsValidationsCreditCard.put(mTilNameOnCard, false);
    mFieldsValidationsCreditCard.put(mTilCVV, false);
  }

  public void initCreditCardFieldsOnFocusChange() {
    if (mTilCreditCardNumber.getEditText() != null) {
      mTilCreditCardNumber.getEditText()
          .addTextChangedListener(
              new CreditCardTextWatcher(mTilCreditCardNumber, mPaymentFormWidgetInterface,
                  mPaymentFormWidgetPresenter, new CreditCardValidator(), this,
                  CollectionMethodType.CREDITCARD, requestValidationHandler));
      mTilCreditCardNumber.getEditText()
          .setOnFocusChangeListener(
              new CreditCardOnFocusChange(mContext, mTilCreditCardNumber, new CreditCardValidator(),
                  OneCMSKeys.VALIDATION_ERROR_CREDIT_CARD, mTracker, mLlCVVTooltip,
                  mPaymentFormWidgetInterface, requestValidationHandler));
    }

    if (mTilCVV.getEditText() != null) {
      if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
        TextInputLayout.LayoutParams params =
            (TextInputLayout.LayoutParams) mTilCVV.getLayoutParams();
        params.setMargins(0, 0, 0, 0);
        mTilCVV.setLayoutParams(params);
        mTilCVV.requestLayout();
      }

      mTilCVV.getEditText()
          .addTextChangedListener(new CVVTextWatcher(mTilCVV, new CVVCardValidator(), this));
      mTilCVV.getEditText()
          .setOnFocusChangeListener(
              new CVVOnFocusChange(mContext, mTilCVV, new CVVAnyCardValidator(),
                  OneCMSKeys.HINT_VALIDATION_ERROR_CVV, mTracker, mLlCVVTooltip));
      setMaxLengthCVV(NORMAL_CVV_LENGHT);
    }

    if (mTilNameOnCard.getEditText() != null) {
      mTilNameOnCard.getEditText()
          .addTextChangedListener(
              new NameOnCardTextWatcher(mTilNameOnCard, new NameValidator(), this));
      mTilNameOnCard.getEditText()
          .setOnFocusChangeListener(
              new NameOnCardFocusChange(mContext, mTilNameOnCard, new NameValidator(),
                  OneCMSKeys.VALIDATION_ERROR_CREDIT_CARD_NAME, mTracker));
    }
  }

  public void updateCVVAmexValidator() {
    if (mTilCVV.getEditText() != null) {
      mTilCVV.getEditText()
          .addTextChangedListener(new CVVTextWatcher(mTilCVV, new CVVAmexValidator(), this));
      mTilCVV.getEditText()
          .setOnFocusChangeListener(new CVVOnFocusChange(mContext, mTilCVV, new CVVAmexValidator(),
              OneCMSKeys.HINT_VALIDATION_ERROR_CVV, mTracker, mLlCVVTooltip));
      setMaxLengthCVV(AMEX_CVV_LENGTH);
    }
  }

  public void updateCVVCardValidator() {
    if (mTilCVV.getEditText() != null) {
      mTilCVV.getEditText()
          .addTextChangedListener(new CVVTextWatcher(mTilCVV, new CVVAnyCardValidator(), this));
      mTilCVV.getEditText()
          .setOnFocusChangeListener(new CVVOnFocusChange(mContext, mTilCVV, new CVVCardValidator(),
              OneCMSKeys.HINT_VALIDATION_ERROR_CVV, mTracker, mLlCVVTooltip));
      setMaxLengthCVV(NORMAL_CVV_LENGHT);
    }
  }

  private void setMaxLengthCVV(int maxLength) {
    if (mTilCVV.getEditText() != null) {
      InputFilter[] filterArray = new InputFilter[1];
      filterArray[0] = new InputFilter.LengthFilter(maxLength);
      mTilCVV.getEditText().setFilters(filterArray);
    }
  }

  public void setBinCheckCallFinished(boolean isBinCheckCallFinished) {
    this.isBinCheckCallFinished = isBinCheckCallFinished;
  }

  public boolean thereAreNoErrorInCreditCardTexInputLayouts() {
    validateField(mTilCreditCardNumber);
    validateField(mTilNameOnCard);
    validateField(mTilCVV);
    return (isThereNoErrorInTextInputLayout(mTilCreditCardNumber)
        && isThereNoErrorInTextInputLayout(mTilNameOnCard)
        && isThereNoErrorInTextInputLayout(mTilCVV));
  }

  public void onSpinnerChangeItemValidateCreditCard() {
    if (mTilCreditCardNumber.getEditText() == null) return;

    String creditCardCode = mPaymentFormWidgetInterface.getPaymentMethodCodeDetected();
    String creditCardNumber =
        mTilCreditCardNumber.getEditText().getText().toString().replaceAll("\\s", "");

    boolean isCardCorrect =
        requestValidationHandler.handleRequest(creditCardCode, creditCardNumber);
    isCardCorrect = isCardCorrect && creditCardValidator.validateField(creditCardNumber);

    if (isCardCorrect) {
      mTilCreditCardNumber.setError(null);
      mTilCreditCardNumber.setErrorEnabled(false);
    }

    onCharacterWriteOnField(isCardCorrect, mTilCreditCardNumber);
  }

  private void validateField(TextInputLayout textInputLayout) {
    if (textInputLayout.getEditText() != null) {
      BaseOnFocusChange baseOnFocusChange =
          (BaseOnFocusChange) textInputLayout.getEditText().getOnFocusChangeListener();
      baseOnFocusChange.validateField(textInputLayout.getEditText().getEditableText());
    }
  }

  private boolean isThereNoErrorInTextInputLayout(TextInputLayout textInputLayout) {
    return (textInputLayout.getError() == null || textInputLayout.getError().toString().isEmpty());
  }

  @Override public void onCharacterWriteOnField(boolean informationIsCorrect,
      TextInputLayout textInputLayoutValidated) {
    if (mPaymentFormWidgetInterface.getCollectionMethodTypeDetected()
        == CollectionMethodType.CREDITCARD) {
      mFieldsValidationsCreditCard.put(textInputLayoutValidated, informationIsCorrect);
      checkIfAllCreditCardFieldsAreValid();
    }
  }

  public void checkIfAllCreditCardFieldsAreValid() {
    boolean isCorrectAllFields = true;
    for (HashMap.Entry<TextInputLayout, Boolean> entry : mFieldsValidationsCreditCard.entrySet()) {
      boolean value = entry.getValue();
      isCorrectAllFields = isCorrectAllFields && value;
    }
    if (isCorrectAllFields) {
      isCorrectAllFields =
          mPaymentFormWidgetInterface.validateExpirationDate() && isBinCheckCallFinished;
    }
    mPaymentFormWidgetInterface.onCheckAllValidations(isCorrectAllFields);
  }

  public boolean thereHasBeenFocusOnAnyFieldInCreditCardWidget() {
    if (mTilNameOnCard.getEditText() != null
        && mTilCVV.getEditText() != null
        && mTilCreditCardNumber.getEditText() != null) {
      NameOnCardFocusChange onFocusChangeName =
          (NameOnCardFocusChange) mTilNameOnCard.getEditText().getOnFocusChangeListener();
      CreditCardOnFocusChange onFocusChangeCreditCard =
          (CreditCardOnFocusChange) mTilCreditCardNumber.getEditText().getOnFocusChangeListener();
      CVVOnFocusChange onFocusChangeCVV =
          (CVVOnFocusChange) mTilCVV.getEditText().getOnFocusChangeListener();

      return onFocusChangeName.getThereWasFocusAtLeastOnce()
          && onFocusChangeCreditCard.getThereWasFocusAtLeastOnce()
          && onFocusChangeCVV.getThereWasFocusAtLeastOnce();
    }
    return false;
  }

  public void cleanPaymentFormFields() {
    cleanField(mTilNameOnCard);
    cleanField(mTilCVV);
    cleanField(mTilCreditCardNumber);
  }

  private void cleanField(TextInputLayout textInputLayout) {
    if (textInputLayout.getEditText() != null && !textInputLayout.getEditText()
        .getText()
        .toString()
        .isEmpty()) {
      textInputLayout.getEditText().getText().clear();
    }
  }
}
