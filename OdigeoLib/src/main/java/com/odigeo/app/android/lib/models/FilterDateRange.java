package com.odigeo.app.android.lib.models;

import com.odigeo.data.entity.geo.City;
import java.util.Date;

/**
 * Created by Irving Lóp on 16/10/2014.
 * this model si for FilterDateWidget
 */
public class FilterDateRange {
  private City city;
  private Date startDate;
  private Date endDate;

  private String textTitle;

  public final City getCity() {
    return city;
  }

  public final void setCity(City city) {
    this.city = city;
  }

  public final Date getStartDate() {
    return startDate;
  }

  public final void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public final Date getEndDate() {
    return endDate;
  }

  public final void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public final String getTextTitle() {
    return textTitle;
  }

  public final void setTextTitle(String textTitle) {
    this.textTitle = textTitle;
  }
}
