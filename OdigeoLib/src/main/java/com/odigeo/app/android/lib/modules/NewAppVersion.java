package com.odigeo.app.android.lib.modules;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.DeviceUtils;
import com.odigeo.app.android.lib.utils.PreferencesManager;

/**
 * Created by ManuelOrtiz on 12/11/2014.
 */
public final class NewAppVersion {

  private NewAppVersion() {

  }

  /**
   * Checks if a new version is available
   *
   * @param context The context to search the current version
   */
  public static void checkForNewVersion(Context context) {
    //New Version available
    VersionInAppStore versionInAppStore = new VersionInAppStore();
    versionInAppStore.setContext(context);
    versionInAppStore.execute();
  }

  public static void showNewVersionAvailable(final Context context, final String newVersion) {
    AlertDialog.Builder alertDialogBuilder =
        new AlertDialog.Builder(context, R.style.StackedAlertDialogStyle);

    alertDialogBuilder.setTitle(
        LocalizablesFacade.getString(context, "string_newversion_title", newVersion));

    // set dialog message
    DialogInterface.OnClickListener positiveClickListener = new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        try {
          context.startActivity(new Intent(Intent.ACTION_VIEW,
              Uri.parse(Configuration.getInstance().getAppStoreURL())));
        } catch (ActivityNotFoundException anfe) {
          Log.e(Constants.TAG_LOG, anfe.getMessage());
        }

        dialog.cancel();
      }
    };
    DialogInterface.OnClickListener negativeClickListener = new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {

        //Don't ask to download this version again
        PreferencesManager.setUpdateVersionToIgnore(newVersion, context.getApplicationContext());

        dialog.cancel();
      }
    };
    DialogInterface.OnClickListener neutralClickListener = new DialogInterface.OnClickListener() {
      @Override public void onClick(DialogInterface dialog, int which) {

        dialog.cancel();
      }
    };
    alertDialogBuilder.setMessage(LocalizablesFacade.getString(context, "string_newversion_message",
        Configuration.getInstance().getBrandVisualName()))
        .setCancelable(false)
        .setPositiveButton(LocalizablesFacade.getString(context, "string_newversion_download"),
            positiveClickListener)
        .setNegativeButton(LocalizablesFacade.getString(context, "string_newversion_ignore"),
            negativeClickListener)
        .setNeutralButton(LocalizablesFacade.getString(context, "string_newversion_remindme"),
            neutralClickListener);

    try {
      // create alert dialog
      AlertDialog alertDialog = alertDialogBuilder.create();

      // show it
      alertDialog.show();
      final Button button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
      LinearLayout linearLayout = (LinearLayout) button.getParent();
      linearLayout.setOrientation(LinearLayout.VERTICAL);
    } catch (Exception e) {
      Log.e(Constants.TAG_LOG, e.getMessage());
    }
  }

  public static class VersionInAppStore extends AsyncTask<Void, Void, String> {

    private Context context;

    @Override protected String doInBackground(Void... params) {

      return DeviceUtils.getApplicationVersionNameInPlayStore();
    }

    @Override protected void onPostExecute(String newVersion) {
      String currentVersionName = DeviceUtils.getApplicationVersionName(context);

      Log.d(Constants.TAG_LOG, "Update Version is = " + newVersion);

      if (newVersion != null) {
        if (newVersion.equals(currentVersionName)) {
          Log.d(Constants.TAG_LOG, "Current version is updated.");
        } else {
          String versionToIgnore =
              PreferencesManager.readUpdateVersionToIgnore(context.getApplicationContext());

          if (newVersion.equals(versionToIgnore)) {
            Log.d(Constants.TAG_LOG, "The user selected to ignore this update");
          } else {
            //if(versionCompare(currentVersionName,newVersion))
            showNewVersionAvailable(context, newVersion);
          }
        }
      } else {
        Log.d(Constants.TAG_LOG, "The version of the update couldn't been retrieved");
      }
    }

    public void setContext(Context context) {
      this.context = context;
    }
  }
}
