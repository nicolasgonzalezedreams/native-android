package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.ui.widgets.base.EditWithHint;

/**
 * Created by ManuelOrtiz on 03/10/2014.
 */
public class EditTextForm extends EditWithHint {

  public EditTextForm(Context context, AttributeSet attrs) {
    super(context, attrs);
    getEditText().setHintTextColor(getContext().getResources().getColor(R.color.basic_light));
  }

  @Override public final int getLayout() {
    return R.layout.layout_edit_form_widget;
  }
}
