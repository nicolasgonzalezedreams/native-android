package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for fareType.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;simpleType name="fareType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PUBLIC"/>
 *     &lt;enumeration value="NEGOTIATED"/>
 *     &lt;enumeration value="PRIVATE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
public enum FareTypeDTO {

  PUBLIC, NEGOTIATED, PRIVATE;

  public static FareTypeDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
