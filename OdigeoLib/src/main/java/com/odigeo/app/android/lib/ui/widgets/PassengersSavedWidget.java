package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.models.Passenger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Crux on 01/10/2014.
 */
public class PassengersSavedWidget extends LinearLayout {
  private FormOdigeoLayout formPassengerWidget;
  private List<PassengerSavedRow> passengerRows;

  public PassengersSavedWidget(Context context, AttributeSet attrs) {

    super(context, attrs);

    initialize(context);
  }

  private void initialize(Context context) {
    inflate(context, R.layout.layout_passengers_saved_widget, this);

    formPassengerWidget = (FormOdigeoLayout) findViewById(R.id.formPassengerWidget);
  }

  public void setPassengers(List<Passenger> passengers) {
    //Clear all the  rows
    if (passengerRows != null) {
      for (PassengerSavedRow passengerSavedRow : passengerRows) {
        formPassengerWidget.removeView(passengerSavedRow);
      }
    }

    passengerRows = new ArrayList<PassengerSavedRow>();

    //Add all the required rows
    for (Passenger passenger : passengers) {
      PassengerSavedRow passengerSavedRow = addPassenger(passenger);

      passengerRows.add(passengerSavedRow);
      formPassengerWidget.addView(passengerSavedRow);
    }

    //Set the last row for a different style
    if (!passengerRows.isEmpty()) {
      passengerRows.get(passengerRows.size() - 1).setLastInForm(true);
    }
  }

  private PassengerSavedRow addPassenger(Passenger passenger) {
    return new PassengerSavedRow(getContext(), passenger);
  }
}
