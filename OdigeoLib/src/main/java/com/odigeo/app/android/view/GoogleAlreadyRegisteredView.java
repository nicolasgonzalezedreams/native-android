package com.odigeo.app.android.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DialogHelper;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.GoogleAlreadyRegisteredPresenter;
import com.odigeo.presenter.contracts.navigators.SocialLoginNavigatorInterface;
import com.odigeo.presenter.contracts.views.GoogleAlreadyRegisteredViewInterface;

public class GoogleAlreadyRegisteredView extends SocialLoginHelperView
    implements GoogleAlreadyRegisteredViewInterface {

  private static final String USER_EMAIL = "USER_EMAIL";
  private static final String PARENT_CLASS_NAME_STRING_KEY = "parentClass";
  private static final String PARENT_CLASS_REGISTER = "Register";
  private static final String PARENT_CLASS_LOGIN = "Login";
  private Button mBtnGoogleLogin;

  public GoogleAlreadyRegisteredView() {
  }

  public static GoogleAlreadyRegisteredView newInstance(String email) {
    GoogleAlreadyRegisteredView view = new GoogleAlreadyRegisteredView();
    Bundle args = new Bundle();
    args.putString(USER_EMAIL, email);
    view.setArguments(args);

    return view;
  }

  private String getEmail() {
    Bundle args = this.getArguments();
    String email = "";
    if (args != null) {
      email = args.getString(USER_EMAIL);
    }
    return email;
  }

  @Override protected GoogleAlreadyRegisteredPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideGoogleRegisteredPresenter(this, (SocialLoginNavigatorInterface) getActivity());
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override public void onResume() {
    super.onResume();
    if (getArguments().getString(PARENT_CLASS_NAME_STRING_KEY, "").equals(PARENT_CLASS_REGISTER)) {
      mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_REGISTER_EMAIL_ALREADY_USED);
    } else if (getArguments().getString(PARENT_CLASS_NAME_STRING_KEY, "")
        .equals(PARENT_CLASS_LOGIN)) {
      mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_LOGIN_WRONG_TYPE);
    }
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_google_already_registered_view;
  }

  @Override protected void initComponent(View view) {
    mBtnGoogleLogin = (Button) view.findViewById(R.id.btnGoogleLogin);
    TextView tvUsername = (TextView) view.findViewById(R.id.txt_vw_restore_pass_paragraph1);
    tvUsername.setText(getEmail());
    dialogHelper = new DialogHelper(getActivity());
  }

  @Override protected void setListeners() {
    mBtnGoogleLogin.setOnClickListener(new View.OnClickListener() {

      @Override public void onClick(View v) {
        showProgress();
        loginGoogle();
        if (getArguments().getString(PARENT_CLASS_NAME_STRING_KEY, "")
            .equals(PARENT_CLASS_REGISTER)) {
          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_SSO_REGISTER,
              TrackerConstants.ACTION_REGISTER_FORM,
              TrackerConstants.LABEL_SSO_EMAIL_ALREADY_USED_GOOGLE_LOGIN);
        } else if (getArguments().getString(PARENT_CLASS_NAME_STRING_KEY, "")
            .equals(PARENT_CLASS_LOGIN)) {
          mTracker.trackAnalyticsEvent(getSSOTrackingCategory(), TrackerConstants.ACTION_SIGN_IN,
              TrackerConstants.LABEL_SSO_EMAIL_ALREADY_USED_GOOGLE_LOGIN);
        }
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    TextView tvAlreadyRegistered =
        (TextView) view.findViewById(R.id.txt_vw_restore_pass_paragraph2);
    tvAlreadyRegistered.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SOO_REGISTERINFO_ALREADY_REGISTERED,
            GOOGLE_SOURCE));
    mBtnGoogleLogin.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.SSO_REGISTERINFO_SIGNIN_GOOGLE)
            .toString());
  }
}
