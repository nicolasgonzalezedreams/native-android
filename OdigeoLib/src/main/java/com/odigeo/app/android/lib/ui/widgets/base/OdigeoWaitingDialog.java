package com.odigeo.app.android.lib.ui.widgets.base;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.text.Spanned;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;

/**
 * Author: Oscar Álvarez
 * Date: 04/09/2015.
 */
public class OdigeoWaitingDialog extends Dialog {

  private final Activity context;
  @DrawableRes private final int idImageResource;
  private final boolean isAnimated;
  private Handler handler;
  private Runnable waitingForDismissRunnable;

  /**
   * This constructor allows the default image to rotate
   *
   * @param context Context from the Dialog is called
   * @param isAnimated Define whether the image should be rotated or not
   */
  public OdigeoWaitingDialog(Activity context, boolean isAnimated) {
    super(context);
    this.context = context;
    this.isAnimated = isAnimated;
    this.idImageResource = R.drawable.loading;
  }

  /**
   * This constructor allows to choose a static image for the dialog
   *
   * @param context Context from the Dialog is called
   * @param idImageResource Resource id for the image for the Dialog
   */
  public OdigeoWaitingDialog(Activity context, @DrawableRes int idImageResource) {
    super(context);
    this.context = context;
    this.isAnimated = false;
    this.idImageResource = idImageResource;
  }

  /**
   * This constructor allows to choose a image for the dialog and rotate the image
   *
   * @param context Context from the Dialog is called
   * @param idImageResource Resource id for the image for the Dialog
   * @param isAnimated Define whether the image should be rotated or not
   */
  public OdigeoWaitingDialog(Activity context, @DrawableRes int idImageResource,
      boolean isAnimated) {
    super(context);
    this.context = context;
    this.isAnimated = isAnimated;
    this.idImageResource = idImageResource;
  }

  @Override protected final void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(R.layout.layout_odigeo_waiting_dialog);
    ImageView imageView = (ImageView) findViewById(R.id.waiting_dialog_flights);
    ProgressBar progressBar = (ProgressBar) findViewById(R.id.waiting_dialog_progress);
    imageView.setImageResource(idImageResource);
    imageView.setVisibility(isAnimated ? View.GONE : View.VISIBLE);
    progressBar.setVisibility(isAnimated ? View.VISIBLE : View.GONE);
    this.setCancelable(false);
  }

  /**
   * Show Dialog with a specific string for a certain time
   *
   * @param message String with specific message
   * @param lifeTime Time in milliseconds where the dialog is displayed
   */
  public final void show(String message, int lifeTime) {
    show(message);
    launchHandler(lifeTime);
  }

  /**
   * Show Dialog with a specific string
   *
   * @param message String to show in Dialog
   */

  public final void show(CharSequence message) {
    this.show();
    ((TextView) findViewById(R.id.waiting_dialog_text)).setText(message);
  }

  /**
   * Show Dialog with a string with special format for a certain time
   *
   * @param message String to show in Dialog
   * @param lifeTime Time in milliseconds where the dialog is displayed
   */
  public void show(CharSequence message, int lifeTime) {
    show(message);
    launchHandler(lifeTime);
  }

  /**
   * Show Dialog with a specific string
   *
   * @param idStringRes Resource id of the string to be shown
   */

  public final void show(String idStringRes) {
    this.show();
    ((TextView) findViewById(R.id.waiting_dialog_text)).setText(idStringRes);
  }

  /**
   * Show Dialog with a message with special format
   *
   * @param message Message with the special format
   */
  public final void show(Spanned message) {
    this.show();
    ((TextView) findViewById(R.id.waiting_dialog_text)).setText(message);
  }

  /**
   * Define the time you show the dialog
   *
   * @param lifeTime time in milliseconds
   */
  private void launchHandler(int lifeTime) {
    if (lifeTime > 0) {
      handler = new Handler();
      waitingForDismissRunnable = new Runnable() {
        @Override public void run() {
          if (isShowing()) {
            dismiss();
          }
        }
      };
      handler.postDelayed(waitingForDismissRunnable, lifeTime);
    }
  }

  @Override public void dismiss() {
    if (waitingForDismissRunnable != null && handler != null) {
      handler.removeCallbacks(waitingForDismissRunnable);
    }
    super.dismiss();
  }

  @Override public final Window getWindow() {
    Window window = super.getWindow();
    window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    return window;
  }
}
