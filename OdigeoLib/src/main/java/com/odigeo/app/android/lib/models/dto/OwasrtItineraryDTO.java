package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for OwasrtItinerary complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="OwasrtItinerary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="itinerary" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="greyListed" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class OwasrtItineraryDTO {

  protected Integer id;
  protected Integer itinerary;
  protected Boolean greyListed;

  /**
   * Gets the value of the id property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setId(Integer value) {
    this.id = value;
  }

  /**
   * Gets the value of the itinerary property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getItinerary() {
    return itinerary;
  }

  /**
   * Sets the value of the itinerary property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setItinerary(Integer value) {
    this.itinerary = value;
  }

  /**
   * Gets the value of the greyListed property.
   *
   * @return possible object is
   * {@link Boolean }
   */
  public Boolean isGreyListed() {
    return greyListed;
  }

  /**
   * Sets the value of the greyListed property.
   *
   * @param value allowed object is
   * {@link Boolean }
   */
  public void setGreyListed(Boolean value) {
    this.greyListed = value;
  }
}
