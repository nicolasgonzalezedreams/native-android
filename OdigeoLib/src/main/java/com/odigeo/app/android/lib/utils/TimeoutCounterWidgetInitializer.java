package com.odigeo.app.android.lib.utils;

import android.os.Handler;
import com.odigeo.app.android.lib.interfaces.ITimeoutWidget;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ximena.perez on 04/03/2015.
 */
public class TimeoutCounterWidgetInitializer {

  private static volatile TimeoutCounterWidgetInitializer instance;
  private Runnable scrollFixRunnable;
  private Runnable timeoutAlert;
  private boolean widgetClosed;
  private Handler handler;

  private List<ITimeoutWidget> suscribed;

  private TimeoutCounterWidgetInitializer() {
    widgetClosed = false;
    handler = new Handler();
    suscribed = new ArrayList<ITimeoutWidget>();
  }

  public static TimeoutCounterWidgetInitializer getInstance() {
    if (instance == null) {
      synchronized (TimeoutCounterWidgetInitializer.class) {
        if (instance == null) {
          instance = new TimeoutCounterWidgetInitializer();
        }
      }
    }

    return instance;
  }

  public void addSubscriber(ITimeoutWidget subscriber) {
    this.suscribed.add(subscriber);
  }

  public void removeSubscriber(ITimeoutWidget subscriber) {
    this.suscribed.remove(subscriber);
  }

  /**
   * Initializes the scroll Runnable, used to fix the scrollView params after widget pops up.
   */
  public void initializeScrollRunnable() {
    scrollFixRunnable = new Runnable() {
      @Override public void run() {
        for (ITimeoutWidget widget : suscribed) {
          widget.setScrollParameters();
        }
      }
    };
  }

  /**
   * This method initializes the runnable used to show the widget
   */
  public void initializeTimeoutRunnable() {
    timeoutAlert = new Runnable() {
      @Override public void run() {
        for (ITimeoutWidget widget : suscribed) {
          widget.showTimeoutWidget();
        }
      }
    };
  }

  public Runnable getScrollFixRunnable() {
    return scrollFixRunnable;
  }

  public Runnable getTimeoutRunnable() {
    return timeoutAlert;
  }

  public boolean isWidgetClosed() {
    return widgetClosed;
  }

  public void setWidgetClosed(boolean closed) {
    widgetClosed = closed;
  }

  public Handler getHandler() {
    return handler;
  }
}
