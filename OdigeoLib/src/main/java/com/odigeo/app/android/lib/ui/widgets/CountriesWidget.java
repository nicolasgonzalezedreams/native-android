package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.ListCountriesActivityAdapter;
import java.util.List;

/**
 * Created by yamil.marques on 10/09/2014.
 */
public class CountriesWidget extends RelativeLayout {

  private View rootView;
  private List<String> listCountries;
  private ListView listViewCountries;
  private TextView tvTitle;
  private ListCountriesActivityAdapter countriesAdapter;

  public CountriesWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
    setRootView(LayoutInflater.from(context).inflate(R.layout.countries_widget, this));
    setListViewCountries((ListView) getRootView().findViewById(R.id.listviewCountries));
    setTvTitle((TextView) getRootView().findViewById(R.id.tv_title));
  }

  public final void setList(List<String> countriesList) {
    listCountries = countriesList;
    setCountriesAdapter(new ListCountriesActivityAdapter(getContext(), countriesList));
    getListViewCountries().setAdapter(getCountriesAdapter());
  }

  @Override public final View getRootView() {
    return rootView;
  }

  private void setRootView(View rootView) {
    this.rootView = rootView;
  }

  public final List<String> getListCountries() {
    return listCountries;
  }

  public final ListView getListViewCountries() {
    return listViewCountries;
  }

  private void setListViewCountries(ListView listViewCountries) {
    this.listViewCountries = listViewCountries;
  }

  public final ListCountriesActivityAdapter getCountriesAdapter() {
    return countriesAdapter;
  }

  public final void setCountriesAdapter(ListCountriesActivityAdapter countriesAdapter) {
    this.countriesAdapter = countriesAdapter;
  }

  public final TextView getTvTitle() {
    return tvTitle;
  }

  private void setTvTitle(TextView tvTitle) {
    this.tvTitle = tvTitle;
  }

  public final void setOnItemClickListenerToListView(AdapterView.OnItemClickListener event) {
    listViewCountries.setOnItemClickListener(event);
  }
}
