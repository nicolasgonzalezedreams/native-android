package com.odigeo.app.android.helper;

import android.support.annotation.VisibleForTesting;
import com.odigeo.app.android.utils.BrandUtils;
import com.odigeo.helper.CrossCarUrlHandlerInterface;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.tools.DateHelperInterface;
import java.util.HashMap;
import java.util.Map;

import static com.odigeo.app.android.lib.consts.Constants.BRAND_TRAVELLINK;
import static com.odigeo.app.android.utils.BrandUtils.MARKET_KEY_COM;
import static com.odigeo.app.android.utils.BrandUtils.MARKET_KEY_DE;
import static com.odigeo.app.android.utils.BrandUtils.MARKET_KEY_FR;
import static com.odigeo.app.android.utils.BrandUtils.MARKET_KEY_IT;
import static com.odigeo.app.android.utils.BrandUtils.MARKET_KEY_UK;

public class CrossCarUrlHandler implements CrossCarUrlHandlerInterface {

  public final static long ONE_WAY_TIME_WITH_CARS = 259200000;

  public final static String HOST_URL_EDREAMS = "http://rentacar.edreams.com/";
  public final static String HOST_URL_GOVOYAGE = "http://voiture.govoyages.com/";
  public final static String HOST_URL_OPODO_UK = "http://carhire.opodo.co.uk/";
  public final static String HOST_URL_OPODO_NORDICS = "http://cars.opodo.com/";
  public final static String HOST_URL_TRAVELLINK = "http://cars.travellink.com/";
  //PickUp parameters
  public final static String PICKUP_YEAR = "puYear";
  public final static String PICKUP_MONTH = "puMonth";
  public final static String PICKUP_DAY = "puDay";
  public final static String PICKUP_HOUR = "puHour";
  public final static String PICKUP_MINUTE = "puMinute";
  //DropOff parameters
  public final static String DROPOFF_YEAR = "doYear";
  public final static String DROPOFF_MONTH = "doMonth";
  public final static String DROPOFF_DAY = "doDay";
  public final static String DROPOFF_HOUR = "doHour";
  public final static String DROPOFF_MINUTE = "doMinute";
  //Basic parameters
  public final static String PREF_LANG = "preflang";
  public final static String COR = "cor";
  public final static String PREF_CURRENCY = "prefcurrency";
  public final static String AD_PLAT = "adplat";
  public final static String AD_CAMP = "adcamp";
  public final static String ENABLER = "enabler";
  public final static String AFFILIATE_CODE = "affiliateCode";
  public static final String ENABLER_PROMOAPP = "promoapp";
  public final static String FROM = "from";
  public final static String TO = "to";
  public final static String DRIVER_AGE = "driversAge";
  public final static String FORCE_MOBILE = "forceMobile";
  public final static String SEARCH_LOADER = "SearchLoader.do";
  public static final String OPODO_AFFILIATE_CODE = "opodo-scandi";
  public static final String PLAT_CONNECTAPP = "connectApp";
  private final static String HOST_URL_OPODO_DE = "http://mietwagen.opodo.de/";
  private final static String HOST_URL_OPODO_COM = "http://carhire.opodo.com/";
  private final static String HOST_URL_OPODO_FR = "http://voiture.opodo.fr/";
  private final static String HOST_URL_OPODO_IT = "http://auto.opodo.it/";
  // Platforms
  private static final String PLAT_ANDROID = "android";
  private final MarketProviderInterface MARKET_PROVIDER;
  private final DateHelperInterface DATE_HELPER;

  private HashMap<String, String> mUrlParams;

  public CrossCarUrlHandler(MarketProviderInterface market, DateHelperInterface dateHelper) {
    MARKET_PROVIDER = market;
    DATE_HELPER = dateHelper;
    mUrlParams = new HashMap<>();
  }

  @Override public String getCarUrl(String campaign) {
    addBasicParams(campaign);
    return getHost() + buildPath();
  }

  private void addBasicParams(String campaign) {
    mUrlParams.put(PREF_LANG, MARKET_PROVIDER.getLanguage());
    mUrlParams.put(COR, MARKET_PROVIDER.getCrossSellingMarketKey());
    mUrlParams.put(PREF_CURRENCY, MARKET_PROVIDER.getCurrencyKey());
    mUrlParams.put(AD_CAMP, campaign);

    String brand = MARKET_PROVIDER.getBrand();

    if (BrandUtils.isTravellinkBrand(brand)) {
      mUrlParams.put(AD_PLAT, PLAT_CONNECTAPP);
      mUrlParams.put(AFFILIATE_CODE, BRAND_TRAVELLINK.toLowerCase());
    } else if (BrandUtils.isOpodoBrand(brand)) {
      mUrlParams.put(AD_PLAT, PLAT_CONNECTAPP);
      if (BrandUtils.isOpodoNordicsBrand(brand, MARKET_PROVIDER.getCrossSellingMarketKey())
          && !campaign.equalsIgnoreCase(CAMPAIGN_HEADER)) {
        mUrlParams.put(AFFILIATE_CODE, OPODO_AFFILIATE_CODE);
      }
    } else if (BrandUtils.isUkMarket(MARKET_PROVIDER.getCrossSellingMarketKey())) {
      mUrlParams.put(ENABLER, ENABLER_PROMOAPP);
      mUrlParams.put(AD_PLAT, PLAT_CONNECTAPP);
    }
  }

  private String buildPath() {
    String path = "?";

    for (String paramName : mUrlParams.keySet()) {
      path += paramName + "=" + mUrlParams.get(paramName) + "&";
    }

    return path.substring(0, path.length() - 1);
  }

  @Override
  public String getCarUrlWithSearchLoader(String arrival, long arrivalDate, long lastDepartureDate,
      long passengerBirthdate, String campaign) {
    addBasicParams(campaign);
    setPickUpDate(arrivalDate);
    setAge(passengerBirthdate);
    setFrom(arrival);
    setTo(arrival, arrivalDate, lastDepartureDate);
    setForceMobile();

    if (lastDepartureDate > arrivalDate) {
      setDropoffDate(lastDepartureDate);
    } else {
      setDropoffDate(arrivalDate + ONE_WAY_TIME_WITH_CARS);
    }

    return getHost() + SEARCH_LOADER + buildPath();
  }

  private void setForceMobile() {
    mUrlParams.put(FORCE_MOBILE, "true");
  }

  private void setFrom(String arrival) {
    mUrlParams.put(FROM, arrival);
  }

  private void setTo(String arrival, long arrivalDate, long lastDepartureDate) {
    if (lastDepartureDate > arrivalDate) {
      mUrlParams.put(TO, arrival);
    }
  }

  private void setAge(long date) {
    mUrlParams.put(DRIVER_AGE, String.valueOf(DATE_HELPER.getAge(date)));
  }

  private void setPickUpDate(long date) {
    long gmtDate = DATE_HELPER.millisecondsLeastOffset(date);
    mUrlParams.put(PICKUP_YEAR, String.valueOf(DATE_HELPER.getYear(gmtDate)));
    mUrlParams.put(PICKUP_MONTH, String.valueOf(DATE_HELPER.getMonth(gmtDate)));
    mUrlParams.put(PICKUP_DAY, String.valueOf(DATE_HELPER.getDay(gmtDate)));
    mUrlParams.put(PICKUP_HOUR, String.valueOf(DATE_HELPER.getHour(gmtDate)));
    mUrlParams.put(PICKUP_MINUTE, String.valueOf(DATE_HELPER.getMinute(gmtDate)));
  }

  private void setDropoffDate(long date) {
    long gmtDate = DATE_HELPER.millisecondsLeastOffset(date);
    mUrlParams.put(DROPOFF_YEAR, String.valueOf(DATE_HELPER.getYear(gmtDate)));
    mUrlParams.put(DROPOFF_MONTH, String.valueOf(DATE_HELPER.getMonth(gmtDate)));
    mUrlParams.put(DROPOFF_DAY, String.valueOf(DATE_HELPER.getDay(gmtDate)));
    mUrlParams.put(DROPOFF_HOUR, String.valueOf(DATE_HELPER.getHour(gmtDate)));
    mUrlParams.put(DROPOFF_MINUTE, String.valueOf(DATE_HELPER.getMinute(gmtDate)));
  }

  private String getHost() {
    String language = MARKET_PROVIDER.getCrossSellingMarketKey();
    String host = "";

    String brand = MARKET_PROVIDER.getBrand();

    if (BrandUtils.isEdreamsBrand(brand)) {
      host = HOST_URL_EDREAMS;
    } else if (BrandUtils.isGoVoyagesBrand(brand)) {
      host = HOST_URL_GOVOYAGE;
    } else if (BrandUtils.isOpodoNordicsBrand(brand, MARKET_PROVIDER.getCrossSellingMarketKey())) {
      host = HOST_URL_OPODO_NORDICS;
    } else if (BrandUtils.isOpodoBrand(brand)) {
      switch (language) {
        case MARKET_KEY_DE:
          host = HOST_URL_OPODO_DE;
          break;
        case MARKET_KEY_COM:
          host = HOST_URL_OPODO_COM;
          break;
        case MARKET_KEY_UK:
          host = HOST_URL_OPODO_UK;
          break;
        case MARKET_KEY_FR:
          host = HOST_URL_OPODO_FR;
          break;
        case MARKET_KEY_IT:
          host = HOST_URL_OPODO_IT;
          break;
      }
    } else if (BrandUtils.isTravellinkBrand(brand)) {
      return HOST_URL_TRAVELLINK;
    }

    return host;
  }

  @VisibleForTesting public Map<String, String> getUrlParams() {
    return mUrlParams;
  }
}
