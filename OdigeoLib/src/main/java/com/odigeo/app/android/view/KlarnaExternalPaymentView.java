package com.odigeo.app.android.view;

import com.odigeo.dataodigeo.tracker.TrackerConstants;

public class KlarnaExternalPaymentView extends ExternalPaymentView {

  @Override public void trackGoBack() {
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_KLARNA_PAGE,
        TrackerConstants.ACTION_NAVIGATION_ELEMENTS, TrackerConstants.LABEL_GO_BACK);
  }

  @Override protected boolean shouldInterceptPageChange(String url) {
    return true;
  }
}