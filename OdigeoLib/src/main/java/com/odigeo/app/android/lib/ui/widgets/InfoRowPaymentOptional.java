package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.ui.widgets.base.TextWithImage;

/**
 * Created by Irving Lóp on 09/10/2014.
 */
public class InfoRowPaymentOptional extends TextWithImage {
  public InfoRowPaymentOptional(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected final int getLayout() {
    return R.layout.layout_inforow_payment_optional;
  }

  @Override protected final int getIdTextView() {
    return R.id.text_info_row_payment_optional;
  }

  @Override protected final int getIdImageView() {
    return R.id.image_info_row_payment_optional;
  }
}
