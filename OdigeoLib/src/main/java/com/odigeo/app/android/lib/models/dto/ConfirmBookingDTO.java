package com.odigeo.app.android.lib.models.dto;

public class ConfirmBookingDTO {

  protected PaymentDataRequestDTO paymentData;
  protected long bookingId;
  protected String clientBookingReferenceId;
  protected ItinerarySortCriteriaDTO sortCriteria;

  /**
   * Gets the value of the paymentData property.
   *
   * @return possible object is
   * {@link PaymentDataRequest }
   */
  public PaymentDataRequestDTO getPaymentData() {
    return paymentData;
  }

  /**
   * Sets the value of the paymentData property.
   *
   * @param value allowed object is
   * {@link PaymentDataRequest }
   */
  public void setPaymentData(PaymentDataRequestDTO value) {
    this.paymentData = value;
  }

  /**
   * Gets the value of the bookingId property.
   */
  public long getBookingId() {
    return bookingId;
  }

  /**
   * Sets the value of the bookingId property.
   */
  public void setBookingId(long value) {
    this.bookingId = value;
  }

  /**
   * Gets the value of the clientBookingReferenceId property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getClientBookingReferenceId() {
    return clientBookingReferenceId;
  }

  /**
   * Sets the value of the clientBookingReferenceId property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setClientBookingReferenceId(String value) {
    this.clientBookingReferenceId = value;
  }

  /**
   * Gets the value of the sortCriteria property.
   */
  public ItinerarySortCriteriaDTO getSortCriteria() {
    return sortCriteria;
  }

  /**
   * Sets the value of the sortCriteria property.
   */
  public void setSortCriteria(ItinerarySortCriteriaDTO sortCriteria) {
    this.sortCriteria = sortCriteria;
  }
}
