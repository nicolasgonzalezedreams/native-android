package com.odigeo.app.android.view.textwatchers;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;

public class BaseTextWatcher implements TextWatcher {

  private TextInputLayout mTextInputLayout;

  public BaseTextWatcher(TextInputLayout textInputLayout) {
    this.mTextInputLayout = textInputLayout;
  }

  @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

  }

  @Override public void onTextChanged(CharSequence s, int start, int before, int count) {

  }

  @Override public void afterTextChanged(Editable editableText) {
    if ((mTextInputLayout.getEditText() != null)
        && (mTextInputLayout.getError() != null)
        && (!mTextInputLayout.getError().toString().isEmpty())) {
      mTextInputLayout.setError(null);
      mTextInputLayout.setErrorEnabled(false);
    }
  }
}