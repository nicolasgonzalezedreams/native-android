package com.odigeo.app.android.lib.models.dto;

/**
 * Created by matias.dirusso on 19/4/2016.
 */
@Deprecated public class ResidentValidationRetryDTO extends ResidentsValidationDTO {
  protected int numberMaximumOfRetries;
  protected int numberOfAvailableRetries;

  public final int getNumberMaximumOfRetries() {
    return numberMaximumOfRetries;
  }

  public final void setNumberMaximumOfRetries(int numberMaximumOfRetries) {
    this.numberMaximumOfRetries = numberMaximumOfRetries;
  }

  public final void setNumberOfAvailableRetries(int numberMaximumOfRetries) {
    this.numberMaximumOfRetries = numberMaximumOfRetries;
  }
}
