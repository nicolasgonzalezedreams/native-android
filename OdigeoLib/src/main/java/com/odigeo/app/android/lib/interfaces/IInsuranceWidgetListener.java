package com.odigeo.app.android.lib.interfaces;

import com.odigeo.app.android.lib.models.dto.InsuranceOfferDTO;

/**
 * Created by manuel on 14/09/14.
 */
public interface IInsuranceWidgetListener {
  void onInsuranceSelected(InsuranceOfferDTO insuranceOffer);

  void onInsuranceUnselected(InsuranceOfferDTO insuranceOffer);
}
