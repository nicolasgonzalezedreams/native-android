package com.odigeo.app.android.lib.utils;

import android.os.AsyncTask;

/**
 * This class goal is to be used to read or write the hard drive. Tippically every call to database
 * should use asynchronous task, as files finding and manipulation. For example, look for an image
 * in the hard drive or calling a service.
 *
 * @author cfanjul
 */
public class MyAsyncTask extends AsyncTask<MyAsyncTask.DoInBackground, Void, Void> {

  protected DoOnPostExecute doOnPostExecute;
  protected DoOnCancel onCancelListener;

  public MyAsyncTask() {
  }

  @Override protected final Void doInBackground(DoInBackground... params) {
    params[0].executeInBackground();
    return null;
  }

  @Override protected final void onPostExecute(Void v) {
    super.onPostExecute(v);
    if (this.doOnPostExecute != null) {
      this.doOnPostExecute.postExecute();
    }
  }

  @Override protected final void onCancelled(Void result) {
    super.onCancelled(result);
    if (this.onCancelListener != null) {
      this.onCancelListener.onCancel();
    }
  }

  public final void setDoOnPostExecute(DoOnPostExecute doOnPostExecute) {
    this.doOnPostExecute = doOnPostExecute;
  }

  public final void setDoOnCancelListener(DoOnCancel onCancelListener) {
    this.onCancelListener = onCancelListener;
  }

  /**
   * Interface created to be used with MyAsyncTask.
   *
   * @author cfanjul
   */
  public interface DoInBackground {
    void executeInBackground();
  }

  /**
   * Interface created to be used with MyAsyncTask.
   *
   * @author cfanjul
   */
  public interface DoOnPostExecute {
    void postExecute();
  }

  public interface DoOnCancel {
    void onCancel();
  }
}
