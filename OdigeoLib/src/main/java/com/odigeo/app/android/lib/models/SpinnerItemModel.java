package com.odigeo.app.android.lib.models;

import java.io.Serializable;

@Deprecated public class SpinnerItemModel<T extends Enum> implements Serializable {
  protected T item;
  protected String stringKey;
  protected String text;

  public SpinnerItemModel() {

  }

  public SpinnerItemModel(T item) {
    this.item = item;
  }

  public SpinnerItemModel(T item, String stringKey) {
    this.item = item;
    this.stringKey = stringKey;
  }

  public T getItem() {
    return item;
  }

  public void setItem(T item) {
    this.item = item;
  }

  public String getStringKey() {
    return stringKey;
  }

  public void setStringKey(String stringKey) {
    this.stringKey = stringKey;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }
}
