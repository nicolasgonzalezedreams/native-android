package com.odigeo.app.android.view;

import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.animations.UpdateCarouselDotsAnimation;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DateHelper;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.app.android.view.interfaces.ListenerUpdateCarousel;
import com.odigeo.app.android.view.interfaces.MttCardBackground;
import com.odigeo.data.entity.carrousel.BaggageBeltCard;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.extensions.UIBaggageBeltCard;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.BasePresenter;
import com.odigeo.presenter.contracts.views.CardViewInterface;

/**
 * Created by gonzalo.lodi on 1/28/2016.
 */
public class BaggageBeltCardView extends BaseView implements CardViewInterface, MttCardBackground {

  private static final String KEY_BAGGAGE_BELT_CARD = "KEY_BAGGAGE_BELT_CARD";

  protected TextView mTvTitleCard;
  protected BaggageBeltCard mCard;
  private TextView mTvSubtitleCard;
  private ImageView mIvIconCard;
  private TextView mTvHeaderBaggage;
  private TextView mTvBeltNumber;
  private TextView mTvTerminalNumber;
  private TextView mTvDateLastUpdate;
  private ImageView mIvAutoRenewCardIcon;
  private long mLastUpdate;
  private int mCardPosition;

  private String stringLastUpdate;
  private String stringTerminal;
  private ListenerUpdateCarousel mListenerUpdateCarousel;
  private UpdateCarouselDotsAnimation mUpdateCarouselDotsAnimation;
  private boolean mItIsPaused;
  private DateHelper mDateHelper;

  public static BaggageBeltCardView newInstance(BaggageBeltCard card, long lastUpdate) {
    Bundle args = new Bundle();
    args.putParcelable(KEY_BAGGAGE_BELT_CARD, new UIBaggageBeltCard(card));
    args.putLong(Card.KEY_LAST_UPDATE, lastUpdate);
    BaggageBeltCardView fragment = new BaggageBeltCardView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle args = getArguments();
    mDateHelper = getDateHelper();
    if (args.containsKey(KEY_BAGGAGE_BELT_CARD)
        && args.containsKey(Card.KEY_LAST_UPDATE)
        && args.containsKey(Card.KEY_CARD_POSITION)) {
      mCard = args.getParcelable(KEY_BAGGAGE_BELT_CARD);
      mLastUpdate = args.getLong(Card.KEY_LAST_UPDATE);
      mCardPosition = args.getInt(Card.KEY_CARD_POSITION);
    }
  }

  @Override public void onResume() {
    super.onResume();
    mListenerUpdateCarousel = (ListenerUpdateCarousel) getContext();

    if (mItIsPaused) {
      mUpdateCarouselDotsAnimation.runDotsAnimation();
    }
  }

  @Override public void onPause() {
    super.onPause();
    mListenerUpdateCarousel = null;
    mItIsPaused = true;
  }

  @Override protected BasePresenter getPresenter() {
    return null;
  }

  private DateHelper getDateHelper() {
    return AndroidDependencyInjector.getInstance().provideDateHelper();
  }

  @Override protected int getFragmentLayout() {
    return R.layout.boarding_gate_card;
  }

  @Override protected void initComponent(View view) {
    mTvTitleCard = (TextView) view.findViewById(R.id.TvTitleCard);
    mTvSubtitleCard = (TextView) view.findViewById(R.id.TvSubtitleCard);
    mIvIconCard = (ImageView) view.findViewById(R.id.IvCardIcon);
    mTvHeaderBaggage = (TextView) view.findViewById(R.id.TvHeaderGate);
    mTvBeltNumber = (TextView) view.findViewById(R.id.TvGateNumber);
    mTvTerminalNumber = (TextView) view.findViewById(R.id.TvTerminalNumber);
    mTvDateLastUpdate = (TextView) view.findViewById(R.id.TvDateLastUpdate);
    mIvAutoRenewCardIcon = (ImageView) view.findViewById(R.id.IvAutoRenewCardIcon);

    mItIsPaused = false;

    initUpdateCarouselDotsAnimation();
    initRefreshButton();
    setUpdateListener();
    setListeners(view);
  }

  private void initRefreshButton() {
    if (mCard.hasToShowRefreshButton()) {
      mIvAutoRenewCardIcon.setVisibility(View.VISIBLE);
    } else {
      mIvAutoRenewCardIcon.setVisibility(View.GONE);
    }
  }

  private void initUpdateCarouselDotsAnimation() {
    mUpdateCarouselDotsAnimation =
        AndroidDependencyInjector.getInstance().provideUpdateCarouselDotsAnimation();
    String updatingData =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_UPDATING).toString();
    mUpdateCarouselDotsAnimation.initAnimationData(updatingData, mTvDateLastUpdate);
  }

  @Override public String getBackgroundImage() {
    String imageUrl = "";
    if (mCard != null) {
      imageUrl = mCard.getImage();
    }
    return imageUrl;
  }

  private void initDataBoardingTripCard() {
    mTvSubtitleCard.setText(mCard.getTripToHeader());
    mTvBeltNumber.setText(mCard.getBaggageBelt());

    String terminal;
    if ((mCard.getTo().getTerminal() != null) && (!mCard.getTo().getTerminal().isEmpty())) {
      terminal = stringTerminal + " " + mCard.getTo().getTerminal();
    } else {
      terminal = stringTerminal + " " + "-";
    }

    mTvTerminalNumber.setText(terminal);
    if (mLastUpdate == 0) {
      mTvDateLastUpdate.setVisibility(View.GONE);
    } else {
      String lastUpdateText = stringLastUpdate
          + " "
          + "<b>"
          + mDateHelper.getTime(mLastUpdate, getActivity())
          + "</b> "
          + mDateHelper.getDayAndMonth(mLastUpdate, getActivity());
      mTvDateLastUpdate.setText(Html.fromHtml(lastUpdateText));
    }
    mIvIconCard.setImageDrawable(
        DrawableUtils.getTintedResource(R.mipmap.baggage_icon_card, R.color.carousel_card_icon,
            getActivity()));
    mIvAutoRenewCardIcon.setImageDrawable(
        DrawableUtils.getTintedResource(R.drawable.ic_autorenew, R.color.carousel_card_icon,
            getActivity()));
  }

  @Override protected void setListeners() {
    initDataBoardingTripCard();
  }

  private void setUpdateListener() {
    mIvAutoRenewCardIcon.setOnTouchListener(new View.OnTouchListener() {
      @Override public boolean onTouch(View v, MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
          mIvAutoRenewCardIcon.setImageDrawable(
              DrawableUtils.getTintedResource(R.drawable.ic_autorenew,
                  R.color.carousel_card_icon_pressed, getActivity()));
          return true;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
          mIvAutoRenewCardIcon.setImageDrawable(
              DrawableUtils.getTintedResource(R.drawable.ic_autorenew, R.color.carousel_card_icon,
                  getActivity()));
          mListenerUpdateCarousel.onUpdateCarousel();
          startRefreshAnimation();
          mTracker.trackMTTCardEvent(TrackerConstants.LABEL_REFRESH_INFORMATION);
          return true;
        }
        return false;
      }
    });
  }

  private void startRefreshAnimation() {
    mUpdateCarouselDotsAnimation.runDotsAnimation();
    Animation startRotateAnimation =
        AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_refresh_button);
    mIvAutoRenewCardIcon.startAnimation(startRotateAnimation);
  }

  private void stopRefreshAnimation() {
    mUpdateCarouselDotsAnimation.stopDotsAnimation();
    mIvAutoRenewCardIcon.clearAnimation();
  }

  @Override public void onStop() {
    super.onStop();
    stopRefreshAnimation();
  }

  protected void setListeners(View view) {
    view.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        String trackingLabel = TrackerConstants.LABEL_CARD_SEE_INFO_FORMAT.replace(
            TrackerConstants.CARD_TYPE_IDENTIFIER, TrackerConstants.LABEL_CARD_BAGGAGE_BELT)
            .replace(TrackerConstants.CARD_POSITION_IDENTIFIER, String.valueOf(mCardPosition));
        mTracker.trackMTTCardEvent(trackingLabel);
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    mTvTitleCard.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_TITLE));
    mTvHeaderBaggage.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_BELT));
    stringTerminal =
        String.valueOf(LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_TERMINAL));
    stringLastUpdate =
        String.valueOf(LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARDS_LASTUPDATE));
  }

  @Override public void setCardPosition(int position) {
    Bundle bundle = getArguments();
    bundle.putInt(Card.KEY_CARD_POSITION, position);
    this.setArguments(bundle);
  }
}
