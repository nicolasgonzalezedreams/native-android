package com.odigeo.app.android.lib.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MenuItem;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Lists;
import com.odigeo.app.android.lib.config.ResidentItinerary;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.fragments.MultiTripFragment;
import com.odigeo.app.android.lib.fragments.RoundTripFragment;
import com.odigeo.app.android.lib.fragments.SimpleTripFragment;
import com.odigeo.app.android.lib.fragments.base.TripFragmentBase;
import com.odigeo.app.android.lib.interfaces.OdigeoInterfaces;
import com.odigeo.app.android.lib.interfaces.OdigeoInterfaces.PassengerPickerFragmentListener;
import com.odigeo.app.android.lib.listeners.TabsSearchWidgetListener;
import com.odigeo.app.android.lib.mappers.StoredSearchOptionsMapper;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.Passenger;
import com.odigeo.app.android.lib.models.dto.TravellerTypeDTO;
import com.odigeo.app.android.lib.modules.Residents;
import com.odigeo.app.android.lib.ui.widgets.SearchTripWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.Validations;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.lib.utils.exceptions.SearchOptionsException;
import com.odigeo.app.android.navigator.helpers.GoogleIndexingAPIHelper;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.PermissionsHelper;
import com.odigeo.data.db.helper.CitiesHandlerInterface;
import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.entity.FlightsDirection;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.dataodigeo.location.LocationController;
import com.odigeo.dataodigeo.location.LocationControllerListener;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackHelper;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackerFlowSession;
import com.odigeo.dataodigeo.tracker.tune.TuneHelper;
import com.odigeo.dataodigeo.tracker.tune.TuneSegment;
import com.odigeo.interactors.SearchFlightInteractor;
import com.odigeo.tools.DateUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.odigeo.app.android.view.helpers.PermissionsHelper.LOCATION_REQUEST_CODE;
import static com.odigeo.data.entity.TravelType.MULTIDESTINATION;
import static com.odigeo.data.entity.TravelType.ROUND;
import static com.odigeo.data.entity.TravelType.SIMPLE;

public abstract class OdigeoSearchActivity extends OdigeoBackgroundAnimatedActivity
    implements PassengerPickerFragmentListener, OdigeoInterfaces.SearchListener,
    LocationControllerListener {

  private static final String EMPTY_STRING = "";
  protected ActionBar.Tab roundTripTab;
  protected ActionBar.Tab simpleTripTab;
  protected ActionBar.Tab multiTripTab;
  private RoundTripFragment roundTripFragment;
  private SimpleTripFragment simpleTripFragment;
  private MultiTripFragment multiTripFragment;
  private OdigeoApp odigeoApp;

  private SearchOptions searchOptions;

  private List<FlightSegment> flightSegmentsSimple;
  private List<FlightSegment> flightSegmentsRound;
  private List<FlightSegment> flightSegmentsMultiple;

  private SearchOptions widgetSearch;
  private Boolean hasWidgetSearch;
  private boolean mFromDeepLinking;

  private GoogleIndexingAPIHelper mGoogleIndexingAPIHelper;
  private LocationController locationController;

  private List<SearchOptions> mSearchOptionsList;
  private SearchesHandlerInterface mSearchesHandler;

  private SearchFlightInteractor mSearchFlightInteractor;
  private PreferencesControllerInterface mPreferencesControllerInterface;

  private StoredSearchOptionsMapper storedSearchOptionsMapper;

  private CitiesHandlerInterface citiesHandlerInterface;

  private boolean isResident = false;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    locationController = AndroidDependencyInjector.getInstance().provideLocationController();
    locationController.connect();

    mPreferencesControllerInterface =
        AndroidDependencyInjector.getInstance().providePreferencesController();
    citiesHandlerInterface = AndroidDependencyInjector.getInstance().provideCitiesHandler();

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      PermissionsHelper.askForPermissionIfNeeded(ACCESS_FINE_LOCATION, this,
          OneCMSKeys.PERMISSION_LOCATION_AIRPORT_MESSAGE, LOCATION_REQUEST_CODE);
    }

    odigeoApp = (OdigeoApp) getApplication();

    mSearchesHandler = AndroidDependencyInjector.getInstance().provideSearchesHandler();
    mSearchOptionsList = new ArrayList<>();
    storedSearchOptionsMapper =
        AndroidDependencyInjector.getInstance().provideStoredSearchOptionsMapper();

    mGoogleIndexingAPIHelper =
        new GoogleIndexingAPIHelper(getApplicationContext(), GoogleIndexingAPIHelper.SEARCH);
    mSearchFlightInteractor =
        AndroidDependencyInjector.getInstance().provideSearchFlightInteractor();
    setContentView(R.layout.activity_search);

    hasWidgetSearch = Boolean.FALSE;
    if (savedInstanceState != null) {
      flightSegmentsSimple = (ArrayList<FlightSegment>) savedInstanceState.getSerializable(
          Constants.FLIGHT_SEGMENTS_SIMPLE);
      flightSegmentsRound = (ArrayList<FlightSegment>) savedInstanceState.getSerializable(
          Constants.FLIGHT_SEGMENTS_ROUND);
      flightSegmentsMultiple = (ArrayList<FlightSegment>) savedInstanceState.getSerializable(
          Constants.FLIGHT_SEGMENTS_MULTIPLE);

      searchOptions =
          (SearchOptions) savedInstanceState.getSerializable(Constants.EXTRA_SEARCH_OPTIONS);

      if (searchOptions == null) {
        createDefaultSearchOptions();
      }
    } else {
      initializeSegments();

      searchOptions =
          (SearchOptions) getIntent().getSerializableExtra(Constants.EXTRA_SEARCH_OPTIONS);
      if (searchOptions == null) {
        createDefaultSearchOptions();
      } else {
        mFromDeepLinking = true;
      }

      //Check if has a extra from the widget
      widgetSearch =
          (SearchOptions) getIntent().getSerializableExtra(Constants.EXTRA_WIDGET_SEARCH_OPTIONS);
      hasWidgetSearch = Boolean.TRUE;
    }

    ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);

    initializeTripFragment();
    createTabs(actionBar);
    initializeNearestCity();
    startAppseeSession();
  }

  @Override public void onResume() {
    super.onResume();
    if (mFromDeepLinking) {
      updateSearchOptions();
      updateSearch();
    }

    if (locationController.getNearestCity() == null) {
      locationController.subscribe(this);
      locationController.startLocationUpdates();
    }
    BusProvider.getInstance().post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_SEARCH));
    if (searchOptions != null) {
      if (searchOptions.getFlightSegments() != null) {
        validateResidents(searchOptions.getFlightSegments().get(0));
      }
    }
    fillSearchWithWidget();
    mSearchOptionsList = storedSearchOptionsMapper.storedSearchToSearchOptionListMapper(
        mSearchesHandler.getCompleteSearchesFromDB());
    updateHistory();
  }

  @Override protected void onPause() {
    locationController.stopLocationUpdates();
    locationController.unSubscribe();
    super.onPause();
  }

  @Override public void onStart() {
    super.onStart();
    mGoogleIndexingAPIHelper.onStart(getString(R.string.gia_flights));
  }

  @Override public void onStop() {
    mGoogleIndexingAPIHelper.onStop();
    super.onStop();
  }

  @Override public String getActivityTitle() {
    return LocalizablesFacade.getString(this, OneCMSKeys.SEARCHVIEWCONTROLLER_TITLE).toString();
  }

  private void createDefaultSearchOptions() {
    //Default search options
    searchOptions = new SearchOptions();
    searchOptions.setSearchDate(System.currentTimeMillis());
    if (!Lists.getInstance().getCabinClasses().isEmpty()) {
      searchOptions.setCabinClass(Lists.getInstance().getCabinClasses().get(0));
    }
  }

  @Override public void onLocationReady() {

    Intent intent = new Intent(); //Setup the intent with the return data.
    intent.putExtra(Constants.EXTRA_FLIGHT_DIRECTION, FlightsDirection.DEPARTURE);
    intent.putExtra(Constants.EXTRA_DESTINATION, locationController.getNearestCity());

    onActivityResult(Constants.SELECTING_DESTINATION, RESULT_OK, intent);

    if (getSupportActionBar().getSelectedTab() == simpleTripTab
        && !simpleTripFragment.areControlsAlreadySet()) {
      simpleTripFragment.setControlsValues();
    } else if (getSupportActionBar().getSelectedTab() == roundTripTab
        && !roundTripFragment.areControlsAlreadySet()) {
      roundTripFragment.setControlsValues();
    } else if (getSupportActionBar().getSelectedTab() == multiTripTab
        && !multiTripFragment.areControlsAlreadySet()) {
      multiTripFragment.setControlsValues();
    }
  }

  private void initializeNearestCity() {
    City nearestCity = locationController.getNearestCity();
    boolean needUpdate = false;
    if (flightSegmentsSimple.get(0).getDepartureCity() == null) {
      flightSegmentsSimple.get(0).setDepartureCity(nearestCity);
      simpleTripFragment.setFlightSegments(flightSegmentsSimple);
      needUpdate = true;
    }
    if (flightSegmentsRound.get(0).getDepartureCity() == null) {
      flightSegmentsRound.get(0).setDepartureCity(nearestCity);
      roundTripFragment.setFlightSegments(flightSegmentsRound);
      needUpdate = true;
    }
    if (flightSegmentsMultiple.get(0).getDepartureCity() == null) {
      flightSegmentsMultiple.get(0).setDepartureCity(nearestCity);
      multiTripFragment.setFlightSegments(flightSegmentsMultiple);
      needUpdate = true;
    }
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      ActionBar.Tab selectedTab = actionBar.getSelectedTab();
      if (selectedTab != null && needUpdate) {
        if (actionBar.getTabAt(Constants.ID_TAB_ROUNDTRIP) != null) {
          actionBar.selectTab(actionBar.getTabAt(Constants.ID_TAB_ROUNDTRIP));
        }
      }
    }
    onSuccessNearestCities();
  }

  /**
   * Wraps functionality of onSuccessBinDetails method. Set nearest cities from segment.
   */
  private void onSuccessNearestCities() {
    City nearestCity = locationController.getNearestCity();
    boolean needUpdate = false;
    if (flightSegmentsSimple.get(0).getDepartureCity() == null) {
      flightSegmentsSimple.get(0).setDepartureCity(nearestCity);
      simpleTripFragment.setFlightSegments(flightSegmentsSimple);
      needUpdate = true;
    }
    if (flightSegmentsRound.get(0).getDepartureCity() == null) {
      flightSegmentsRound.get(0).setDepartureCity(nearestCity);
      roundTripFragment.setFlightSegments(flightSegmentsRound);
      needUpdate = true;
    }
    if (flightSegmentsMultiple.get(0).getDepartureCity() == null) {
      flightSegmentsMultiple.get(0).setDepartureCity(nearestCity);
      multiTripFragment.setFlightSegments(flightSegmentsMultiple);
      needUpdate = true;
    }
    if (getSupportActionBar() != null) {
      ActionBar.Tab selectedTab = getSupportActionBar().getSelectedTab();
      if (selectedTab != null && needUpdate) {
        getSupportActionBar().selectTab(roundTripTab);
      }
    }
  }

  private void initializeSegments() {
    City nearestCity = locationController.getNearestCity();
    flightSegmentsSimple = new ArrayList<>();
    flightSegmentsRound = new ArrayList<>();
    flightSegmentsMultiple = new ArrayList<>();

    flightSegmentsSimple.add(new FlightSegment());
    flightSegmentsRound.add(new FlightSegment());
    flightSegmentsRound.add(new FlightSegment());

    //Add the minimal quantity of legs in multisearch
    for (int i = 0; i < Configuration.getInstance().getGeneralConstants().getMinNumberOfLegs();
        i++) {
      flightSegmentsMultiple.add(new FlightSegment());
    }

    if (nearestCity != null) {
      flightSegmentsSimple.get(0).setDepartureCity(nearestCity);
      flightSegmentsRound.get(0).setDepartureCity(nearestCity);
      flightSegmentsMultiple.get(0).setDepartureCity(nearestCity);
    }
  }

  private void initializeTripFragment() {
    if (roundTripFragment == null) {
      roundTripFragment = RoundTripFragment.newInstance(searchOptions, flightSegmentsRound);
      roundTripFragment.initComponents();
      roundTripFragment.setListener(this);
    } else {
      roundTripFragment.setFlightSegments(flightSegmentsRound);
      roundTripFragment.setControlsValues();
    }

    if (simpleTripFragment == null) {
      simpleTripFragment = SimpleTripFragment.newInstance(searchOptions, flightSegmentsSimple);
      simpleTripFragment.initComponents();
      simpleTripFragment.setListener(this);
    } else {
      simpleTripFragment.setFlightSegments(flightSegmentsSimple);
      simpleTripFragment.setControlsValues();
    }

    if (multiTripFragment == null) {
      multiTripFragment = MultiTripFragment.newInstance(searchOptions, flightSegmentsMultiple);
      multiTripFragment.initComponents();
      multiTripFragment.setListener(this);
    } else {
      multiTripFragment.setFlightSegments(flightSegmentsMultiple);
    }
    updateHistory();
  }

  public void updateHistory() {
    roundTripFragment.setSearchesList(mSearchOptionsList);
    simpleTripFragment.setSearchesList(mSearchOptionsList);
    multiTripFragment.setSearchesList(mSearchOptionsList);
  }

  private void updateSearchOptions() {
    City arrivalCity, departureCity;

    for (FlightSegment flightSegment : searchOptions.getFlightSegments()) {

      arrivalCity = getFlightSegmentCity(flightSegment.getArrivalCity());
      departureCity = getFlightSegmentCity(flightSegment.getDepartureCity());

      flightSegment.setArrivalCity(arrivalCity);
      flightSegment.setDepartureCity(departureCity);
    }
  }

  private City getFlightSegmentCity(City city) {
    if (city.getCityName() == null || city.getCityName().equals(city.getIataCode())) {
      City completeCity = citiesHandlerInterface.getCity(city.getIataCode());
      return completeCity == null ? city : completeCity;
    }

    return city;
  }

  /**
   * This method check and fill the search with the information retrieved from the intent from the
   * widget click event.
   */
  private void fillSearchWithWidget() {
    if (widgetSearch != null && hasWidgetSearch) {
      onClickHistorySearch(widgetSearch);
      hasWidgetSearch = Boolean.FALSE;
    }
  }

  /**
   * This method perform the necessary operations to update the searched values into the 3 types of
   * flights.
   */
  private void updateSearch() {
    flightSegmentsSimple.set(0, cloneFlightSegment(searchOptions.getFlightSegments().get(0)));
    flightSegmentsRound.set(0, cloneFlightSegment(searchOptions.getFlightSegments().get(0)));
    if (searchOptions.getFlightSegments().size() > 1) {
      flightSegmentsRound.set(1, cloneFlightSegment(searchOptions.getFlightSegments().get(1)));
    } else {
      FlightSegment departure = searchOptions.getFlightSegments().get(0);
      flightSegmentsRound.remove(1);
      FlightSegment returnSegment = new FlightSegment();
      returnSegment.setArrivalCity(departure.getDepartureCity());
      returnSegment.setDepartureCity(departure.getArrivalCity());
      flightSegmentsRound.add(returnSegment);
    }
    flightSegmentsMultiple.clear();
    for (int i = 0; i < searchOptions.getFlightSegments().size(); i++) {
      flightSegmentsMultiple.add(cloneFlightSegment(searchOptions.getFlightSegments().get(i)));
    }
    if (searchOptions.getFlightSegments().size() < 2) {
      flightSegmentsMultiple.add(new FlightSegment());
    } else {
      if (!searchOptions.getTravelType().equals(MULTIDESTINATION)) {
        flightSegmentsMultiple.remove(flightSegmentsMultiple.size() - 1);
        flightSegmentsMultiple.add(new FlightSegment());
      }
    }
    simpleTripFragment.setFlightSegments(flightSegmentsSimple);
    roundTripFragment.setFlightSegments(flightSegmentsRound);
    multiTripFragment.setFlightSegments(flightSegmentsMultiple);
    simpleTripFragment.setSearchOptions(searchOptions);
    roundTripFragment.setSearchOptions(searchOptions);
    multiTripFragment.setSearchOptions(searchOptions);
    selectTabSearched();
    executeSearchFromDeeplink();
  }

  private void selectTabSearched() {
    try {
      if (searchOptions.getTravelType() == null) {
        recoverTravelType();
      }

      switch (searchOptions.getTravelType()) {
        case SIMPLE:
          refreshOrSelectTab(simpleTripTab, simpleTripFragment);
          break;
        case ROUND:
          refreshOrSelectTab(roundTripTab, roundTripFragment);
          break;
        case MULTIDESTINATION:
          refreshOrSelectTab(multiTripTab, multiTripFragment);
      }
    } catch (Exception e) {
      //TODO 07/07/2017 Review Crashlytics in order to track searchOptions status when it crashes (https://jira.odigeo
      // .com/browse/DISCOVER-513)
      ((OdigeoApp) getApplication()).trackNonFatal(
          SearchOptionsException.newInstance(searchOptions, e));
      showDefaultSearchView();
    }
  }

  private void showDefaultSearchView() {
    mFromDeepLinking = false;
    createDefaultSearchOptions();
  }

  private void recoverTravelType() {
    int numSegments = searchOptions.getFlightSegments().size();
    if (numSegments == 0) {
      searchOptions.setTravelType(ROUND);
    } else if (numSegments == 1) {
      searchOptions.setTravelType(SIMPLE);
    } else if (numSegments == 2) {
      searchOptions.setTravelType(ROUND);
    } else {
      searchOptions.setTravelType(MULTIDESTINATION);
    }
  }

  private void refreshOrSelectTab(ActionBar.Tab selectedTab, Fragment fragment) {
    ActionBar supportActionBar = getSupportActionBar();
    if (supportActionBar.getSelectedTab().equals(selectedTab)) {
      fragment.onResume();
    } else {
      supportActionBar.selectTab(selectedTab);
    }
  }

  private void executeSearchFromDeeplink() {
    if (mFromDeepLinking) {
      if (searchOptions.isExecuteSearch()) {
        executeSearch();
      }
      mFromDeepLinking = false;
    }
  }

  private void createTabs(ActionBar actionBar) {
    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

    roundTripTab = actionBar.newTab()
        .setText(LocalizablesFacade.getString(this, OneCMSKeys.ROUND_TRIP_TAB_TEXT)
            .toString()
            .toUpperCase());
    simpleTripTab = actionBar.newTab()
        .setText(LocalizablesFacade.getString(this, OneCMSKeys.ONE_WAY_TAB_TEXT)
            .toString()
            .toUpperCase());
    multiTripTab = actionBar.newTab()
        .setText(LocalizablesFacade.getString(this, OneCMSKeys.MULTI_TRIP_TAB_TEXT)
            .toString()
            .toUpperCase());

    roundTripTab.setTabListener(new TabsSearchWidgetListener(roundTripFragment));
    simpleTripTab.setTabListener(new TabsSearchWidgetListener(simpleTripFragment));
    multiTripTab.setTabListener(new TabsSearchWidgetListener(multiTripFragment));

    actionBar.addTab(roundTripTab);
    actionBar.addTab(simpleTripTab);
    actionBar.addTab(multiTripTab);
    actionBar.selectTab(roundTripTab);
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK) {
      //If it is the Flight Return answer in the calendar Activity
      if (requestCode == Constants.SELECTING_DESTINATION) {

        //Get Data from Destinations activity
        int noSegment = data.getIntExtra(Constants.EXTRA_FLIGHT_SEGMENT_NUMBER, 0);
        setDestinationSelected(data, noSegment);
        processRoundTrip(noSegment);

        //In the multiple trip, the departure from a segment,
        // is the same that the arrival from the previous segment
        if (noSegment < flightSegmentsMultiple.size() - 1) {
          flightSegmentsMultiple.get(noSegment + 1)
              .setDepartureCity(flightSegmentsMultiple.get(noSegment).getArrivalCity());
        }
      } else if (requestCode == Constants.SELECTING_DATE) {
        returningFromSelectingDate(data);
      } else if (requestCode == Constants.REQUEST_CODE_SEARCH) {
        searchOptions = ((SearchOptions) data.getSerializableExtra(Constants.EXTRA_SEARCH_OPTIONS));
        if (searchOptions != null) {
          updateSearch();
        }
      }
    }
  }

  @Override public void onNewIntent(Intent intent) {
    super.onNewIntent(intent);

    if (intent.getBooleanExtra(Constants.EXTRA_ACTION_RESEARCH, false)) {
      switch (searchOptions.getTravelType()) {
        case SIMPLE:
          onClickSearch(simpleTripFragment.getSearchTripWidget(), false);
          break;

        case ROUND:
          onClickSearch(roundTripFragment.getSearchTripWidget(), false);
          break;

        case MULTIDESTINATION:
          onClickSearch(multiTripFragment.getSearchTripWidget(), false);
          break;
      }
    } else if (intent.getSerializableExtra(Constants.EXTRA_WIDGET_SEARCH_OPTIONS) != null) {
      widgetSearch =
          (SearchOptions) intent.getSerializableExtra(Constants.EXTRA_WIDGET_SEARCH_OPTIONS);
      hasWidgetSearch = Boolean.TRUE;
      fillSearchWithWidget();
    } else {
      int noBabies = intent.getIntExtra(Constants.EXTRA_NUMBER_OF_BABIES, 0);
      int noKids = intent.getIntExtra(Constants.EXTRA_NUMBER_OF_KIDS, 0);

      //Passenger growing up during trip
      if (noBabies > 0 || noKids > 0) {

        onDialogPositiveClick(searchOptions.getNumberOfAdults() + noKids,
            searchOptions.getNumberOfKids() + noBabies - noKids,
            searchOptions.getNumberOfBabies() - noBabies);
      }
    }
  }

  @Override public void onSaveInstanceState(Bundle saveInstance) {
    saveInstance.putSerializable(Constants.FLIGHT_SEGMENTS_SIMPLE,
        (ArrayList<FlightSegment>) flightSegmentsSimple);

    saveInstance.putSerializable(Constants.FLIGHT_SEGMENTS_ROUND,
        (ArrayList<FlightSegment>) flightSegmentsRound);

    saveInstance.putSerializable(Constants.FLIGHT_SEGMENTS_MULTIPLE,
        (ArrayList<FlightSegment>) flightSegmentsMultiple);

    saveInstance.putSerializable(Constants.EXTRA_SEARCH_OPTIONS, searchOptions);
  }

  @Override public void onRequestPermissionsResult(int requestCode, String[] permissions,
      int[] grantResults) {
    switch (requestCode) {
      case LOCATION_REQUEST_CODE:
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          locationController.connect();
          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT,
              TrackerConstants.LABEL_LOCATION_ACCESS_ALLOW);
        } else {
          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT,
              TrackerConstants.LABEL_LOCATION_ACCESS_DENY);
        }
        return;
    }
  }

  /**
   * Method to set correctly the destination selected.
   * <p/>
   * Method to set correctly the destination selected.
   *
   * @param data Intent where be extracted the information.
   * @param noSegment Segment where be located in this moment.
   */
  private void setDestinationSelected(Intent data, int noSegment) {
    FlightsDirection flightDirection =
        (FlightsDirection) data.getSerializableExtra(Constants.EXTRA_FLIGHT_DIRECTION);
    City destinationSelected = (City) data.getSerializableExtra(Constants.EXTRA_DESTINATION);
    //Set the destination selected
    if (flightDirection == FlightsDirection.RETURN) {
      if (noSegment == 0) {
        flightSegmentsSimple.get(noSegment).setArrivalCity(destinationSelected);
        validateResidents(flightSegmentsSimple.get(0));
      }
      if (noSegment < 2) {
        flightSegmentsRound.get(noSegment).setArrivalCity(destinationSelected);
        validateResidents(flightSegmentsRound.get(0));
      }
      flightSegmentsMultiple.get(noSegment).setArrivalCity(destinationSelected);
    } else {
      if (noSegment == 0) {
        flightSegmentsSimple.get(noSegment).setDepartureCity(destinationSelected);
        validateResidents(flightSegmentsSimple.get(0));
      }
      if (noSegment < 2) {
        flightSegmentsRound.get(noSegment).setDepartureCity(destinationSelected);
        validateResidents(flightSegmentsRound.get(0));
      }
      if (flightSegmentsMultiple.size() > noSegment) {
        flightSegmentsMultiple.get(noSegment).setDepartureCity(destinationSelected);
      }
    }
  }

  /**
   * To process the behavior of the round trip when we return from select destination.
   *
   * @param noSegment Segment where be located in this moment.
   */
  private void processRoundTrip(int noSegment) {
    //In the round trip, the second segment is the inverse from the first segment
    if (noSegment == 0) {
      flightSegmentsRound.get(1).setDepartureCity(flightSegmentsRound.get(0).getArrivalCity());
      flightSegmentsRound.get(1).setArrivalCity(flightSegmentsRound.get(0).getDepartureCity());
    }
  }

  /**
   * Its fire When the user press positive button on the passengers dialog
   *
   * @param numAdults Number of adults chosen for travel
   * @param numKids Number of children chosen for travel
   * @param numBabies Number of infants chosen for travel
   */
  @Override public void onDialogPositiveClick(int numAdults, int numKids, int numBabies) {

    searchOptions.setNumberOfAdults(numAdults);
    searchOptions.setNumberOfKids(numKids);
    searchOptions.setNumberOfBabies(numBabies);

    if (this.getSupportActionBar().getSelectedTab().getPosition() == Constants.ID_TAB_ROUNDTRIP) {
      roundTripFragment.setControlsValues();
    } else if (this.getSupportActionBar().getSelectedTab().getPosition()
        == Constants.ID_TAB_SIMPLETRIP) {
      simpleTripFragment.setControlsValues();
    } else if (this.getSupportActionBar().getSelectedTab().getPosition()
        == Constants.ID_TAB_MULTITRIP) {
      multiTripFragment.setControlsValues();
    }
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      postGAEventTrackingGoBack();
      Util.sendToHome(this, odigeoApp);
    }
    return super.onOptionsItemSelected(item);
  }

  @Override public void onClickSearch(SearchTripWidget searchTripWidget, boolean isResident) {
    Constants.cityBitmap = null; //for opodo's background refresh

    if (!mFromDeepLinking) {
      mPreferencesControllerInterface.saveBooleanValue(
          PreferencesControllerInterface.SHOULD_ADD_DROPOFF_CARD, true);
    }

    if (searchOptions == null) {
      createDefaultSearchOptions();
    }

    if (!isResident) {
      isResident = searchTripWidget.getResidentsWidget().getSwitchWidget().isChecked();
      if (!isResident) {
        searchOptions.setResident(null);
      }
    }
    if (isResident && (searchOptions.getNumberOfBabies() > 0
        || searchOptions.getNumberOfKids() > 0)) {
      onlyForAdultsDialog();
    } else if (!arePassengerGrowing(searchTripWidget)) {
      searchOptions.setSearchDate(new Date().getTime());
      executeSearch(searchTripWidget);
    }
  }

  @Override public void onClickHistorySearch(SearchOptions searchOptions) {
    this.searchOptions = searchOptions;
    updateSearch();

    printLogCatHistorySearch(searchOptions);

    ActionBar supportActionBar = getSupportActionBar();
    if (supportActionBar != null) {
      if (searchOptions.getTravelType() == SIMPLE) {
        supportActionBar.selectTab(simpleTripTab);
        validateResidentsHistory(simpleTripFragment);
      } else if (searchOptions.getTravelType() == ROUND) {
        supportActionBar.selectTab(roundTripTab);
        validateResidentsHistory(roundTripFragment);
      } else {
        supportActionBar.selectTab(multiTripTab);
        launchSearchFromHistory(multiTripFragment, false);
      }
    }
  }

  @Override public void onDeleteHistorySearchW(TripFragmentBase fragment) {
    askToDelete(fragment);
  }

  /**
   * To encapsulate the behavior when the onActivityResult has come from a Selection of date.
   *
   * @param data Intent where be extracted the information.
   */
  private void returningFromSelectingDate(Intent data) {
    //Get Data from Calendar activity
    int noSegment = data.getIntExtra(Constants.EXTRA_FLIGHT_SEGMENT_NUMBER, 0);
    TravelType travelType = (TravelType) data.getSerializableExtra(Constants.EXTRA_TRAVEL_TYPE);

    List<Date> dates = (ArrayList<Date>) data.getSerializableExtra(Constants.EXTRA_SELECTED_DATES);

    // If Skip Return button clicked, move to One Way tab
    if (travelType == SIMPLE
        && this.getSupportActionBar().getSelectedTab().getPosition()
        == Constants.ID_TAB_ROUNDTRIP) {
      this.getSupportActionBar()
          .selectTab(this.getSupportActionBar().getTabAt(Constants.ID_TAB_SIMPLETRIP));
    }

    if (dates != null) {
      Date departureDate = dates.get(0);
      Date returnDate = dates.get(1);

      if ((departureDate == null) && (returnDate != null)) {
        flightSegmentsRound.get(1).setDate(returnDate.getTime());
      } else if ((departureDate != null) && (returnDate == null)) {
        long departureDateTime = departureDate.getTime();
        flightSegmentsSimple.get(0).setDate(departureDateTime);
        if (flightSegmentsRound.get(1).getDate() == 0
            || departureDateTime <= flightSegmentsRound.get(1).getDate()) {
          flightSegmentsRound.get(0).setDate(departureDateTime);
        }
      } else {

        Calendar dateDeparture = Calendar.getInstance();
        dateDeparture.setTime(departureDate);
        dateDeparture.set(Calendar.MILLISECOND, 0);
        dateDeparture.set(Calendar.SECOND, 0);

        Calendar dateReturn = Calendar.getInstance();
        dateReturn.setTime(returnDate);
        dateReturn.set(Calendar.MILLISECOND, 0);
        dateReturn.set(Calendar.SECOND, 0);

        if (dateReturn.compareTo(dateDeparture) >= 0) {
          flightSegmentsSimple.get(0).setDate(dateDeparture.getTimeInMillis());
          flightSegmentsRound.get(0).setDate(dateDeparture.getTimeInMillis());
          flightSegmentsRound.get(1).setDate(dateReturn.getTimeInMillis());
        } else {
          if (travelType != MULTIDESTINATION) {
            flightSegmentsSimple.get(0).setDate(dateDeparture.getTimeInMillis());
            flightSegmentsRound.get(0).setDate(dateDeparture.getTimeInMillis());
            flightSegmentsRound.get(1).setDate(0);
          }
        }
      }

      for (int i = 0; i < flightSegmentsMultiple.size() && i < dates.size(); i++) {
        if (dates.get(i) != null) {
          flightSegmentsMultiple.get(i).setDate(dates.get(i).getTime());
        } else {
          flightSegmentsMultiple.get(i).setDate(0);
        }
        if (multiTripFragment != null && multiTripFragment.getSearchTripWidget() != null) {
          multiTripFragment.getSearchTripWidget().getTripLegWidgets().get(i).updateData();
        }
      }

      for (int j = 0; (j < (dates.size() - 1) && travelType == MULTIDESTINATION); j++) {
        Date minDateMultiple = dates.get(j);
        //when the first section date is set again check that other sections hasn't an earlier date
        for (int i = j; i < dates.size(); i++) {
          if (dates.get(i) != null && minDateMultiple != null && DateUtils.isAfter(minDateMultiple,
              dates.get(i)) && i != j) {
            flightSegmentsMultiple.get(i).setDate(0);
          }
        }
      }
    }
  }

  private boolean arePassengerGrowing(SearchTripWidget searchTripWidget) {
    boolean arePassengerGrowing = false;

    List<Passenger> passengersKidsGrowing = new ArrayList<>();
    List<Passenger> passengersBabiesGrowing = new ArrayList<>();

    if (searchOptions.getNumberOfBabies() > 0) {
      passengersBabiesGrowing = getPassengerGrowingUp(TravellerTypeDTO.INFANT, searchTripWidget);
    }

    if (searchOptions.getNumberOfKids() > 0) {
      passengersKidsGrowing = getPassengerGrowingUp(TravellerTypeDTO.CHILD, searchTripWidget);
    }

    if (!passengersBabiesGrowing.isEmpty() || !passengersKidsGrowing.isEmpty()) {
      arePassengerGrowing = true;
      askToModify(passengersBabiesGrowing, passengersKidsGrowing, searchTripWidget);
    }

    return arePassengerGrowing;
  }

  private String textPassengersGrowing(List<Passenger> passengers) {
    if (passengers == null || passengers.isEmpty()) {
      return EMPTY_STRING;
    }

    String passengerNames = EMPTY_STRING;
    int i = 0;
    while (i < passengers.size()) {
      passengerNames += passengers.get(i).getName();
      i++;
      if (i < passengers.size()) {
        passengerNames += ", ";
      }
    }

    String strBody = null;
    TravellerTypeDTO travellerTypeDTO = passengers.get(0).getTravellerType();

    if (travellerTypeDTO == TravellerTypeDTO.INFANT) {
      if (passengers.size() == 1) {
        strBody = LocalizablesFacade.getString(this,
            OneCMSKeys.SEARCHVIEWCONTROLLER_GROWTH_WARNING_BABY_MESSAGE, passengerNames).toString();
      } else {
        strBody = LocalizablesFacade.getString(this,
            OneCMSKeys.SEARCHVIEWCONTROLLER_GROWTH_WARNING_BABIES_MESSAGE, passengerNames)
            .toString();
      }
    } else if (travellerTypeDTO == TravellerTypeDTO.CHILD) {
      if (passengers.size() == 1) {
        strBody = LocalizablesFacade.getString(this,
            OneCMSKeys.SEARCHVIEWCONTROLLER_GROWTH_WARNING_CHILD_MESSAGE, passengerNames)
            .toString();
      } else {
        strBody = LocalizablesFacade.getString(this,
            OneCMSKeys.SEARCHVIEWCONTROLLER_GROWTH_WARNING_CHILDREN_MESSAGE, passengerNames)
            .toString();
      }
    }

    return strBody;
  }

  private void askToModify(final List<Passenger> passengersBabiesGrowing,
      final List<Passenger> passengersKidsGrowing, final SearchTripWidget searchTripWidget) {

    String msgBabies = textPassengersGrowing(passengersBabiesGrowing);
    String msgKids = textPassengersGrowing(passengersKidsGrowing);

    String msgBody = !msgBabies.equals(EMPTY_STRING) ? msgBabies + " " : EMPTY_STRING;

    if (!msgKids.equals(EMPTY_STRING)) {
      msgBody += msgKids;
    }

    msgBody += " " + LocalizablesFacade.getString(this,
        OneCMSKeys.SEARCHVIEWCONTROLLER_GROWTH_WARNING_MODIFY_OR_CONTINUE).toString();

    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

    alertDialogBuilder.setTitle(
        LocalizablesFacade.getString(this, OneCMSKeys.SEARCHVIEWCONTROLLER_GROWTH_WARNING_TITLE));

    // set dialog message
    DialogInterface.OnClickListener listenerPositiveButton = new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.cancel();
        executeSearch(searchTripWidget);
      }
    };
    alertDialogBuilder.setMessage(msgBody)
        .setCancelable(false)
        .setNegativeButton(LocalizablesFacade.getString(this,
            OneCMSKeys.SEARCHVIEWCONTROLLER_GROWTH_WARNING_CANCEL_BUTTON),
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                onNegativeClick(passengersBabiesGrowing, passengersKidsGrowing);
              }
            })
        .setPositiveButton(LocalizablesFacade.getString(this,
            OneCMSKeys.SEARCHVIEWCONTROLLER_GROWTH_WARNING_CONTINUE_BUTTON),
            listenerPositiveButton);

    // create alert dialog
    AlertDialog alertDialog = alertDialogBuilder.create();

    // show it
    alertDialog.show();
  }

  /**
   * Method for wrap the behavior on the dialog click event.
   * <p/>
   * Method for wrap the behavior on the dialog click event.
   *
   * @param passengersBabiesGrowing List of passengers who can grow.
   * @param passengersKidsGrowing List of passengers who can grow.
   */
  private void onNegativeClick(List<Passenger> passengersBabiesGrowing,
      List<Passenger> passengersKidsGrowing) {
    int noPassengers;
    if (!passengersBabiesGrowing.isEmpty()) {
      noPassengers = passengersBabiesGrowing.size();
      if (noPassengers > searchOptions.getNumberOfBabies()) {
        noPassengers = searchOptions.getNumberOfBabies();
      }
      onDialogPositiveClick(searchOptions.getNumberOfAdults(),
          searchOptions.getNumberOfKids() + noPassengers,
          searchOptions.getNumberOfBabies() - noPassengers);
    }
    if (!passengersKidsGrowing.isEmpty()) {
      noPassengers = passengersKidsGrowing.size();
      if (noPassengers > searchOptions.getNumberOfKids()) {
        noPassengers = searchOptions.getNumberOfKids();
      }
      onDialogPositiveClick(searchOptions.getNumberOfAdults() + noPassengers,
          searchOptions.getNumberOfKids() - noPassengers, searchOptions.getNumberOfBabies());
    }
  }

  private List<Passenger> getPassengerGrowingUp(TravellerTypeDTO travellerType,
      SearchTripWidget searchTripWidget) {
    List<Passenger> passengersGrowingList = new ArrayList<Passenger>();

    List<Passenger> passengers =
        PreferencesManager.readPassengers(getApplicationContext(), travellerType);

    if (passengers != null && travellerType != TravellerTypeDTO.ADULT) {
      for (Passenger passenger : passengers) {
        if (passenger.getBirthDate() != null && passenger.getTravellerType() == travellerType) {
          FlightSegment lastSegment = searchTripWidget.getFlightSegments()
              .get(searchTripWidget.getFlightSegments().size() - 1);

          int age = OdigeoDateUtils.getAgeInDate(passenger.getBirthDate(),
              OdigeoDateUtils.createDate(lastSegment.getDate()));

          int maxAge = travellerType == TravellerTypeDTO.INFANT ? Constants.MAX_AGE_INFANT
              : Constants.MAX_AGE_CHILD;

          if (age >= maxAge) {
            passengersGrowingList.add(passenger);
          }
        }
      }
    }

    return passengersGrowingList;
  }

  private void executeSearch(SearchTripWidget searchTripWidget) {
    if (searchOptions == null) {
      createDefaultSearchOptions();
    }

    searchOptions.setFlightSegments(searchTripWidget.getFlightSegments());
    searchOptions.setTravelType(searchTripWidget.getTravelType());

    executeSearch();
  }

  private void executeSearch() {
    if (searchOptions == null) {
      createDefaultSearchOptions();
    }

    synchronizeSearch();

    //Trigger a widget update.
    Util.triggerWidgetUpdate(this, odigeoApp.getWidgetClass());

    Intent intentWaiting = new Intent(this, odigeoApp.getSearchResultsActivityClass());

    SearchTrackHelper mSearchTrackHelper = null;
    switch (searchOptions.getTravelType()) {
      case SIMPLE:
        mSearchTrackHelper = simpleTripFragment.getSearchTrackHelper();
        break;

      case ROUND:
        mSearchTrackHelper = roundTripFragment.getSearchTrackHelper();
        break;

      case MULTIDESTINATION:
        mSearchTrackHelper = multiTripFragment.getSearchTrackHelper();
        break;
    }

    SearchTrackerFlowSession.getInstance().setSearchTrackHelper(mSearchTrackHelper);
    intentWaiting.putExtra(Constants.EXTRA_SEARCH_OPTIONS, searchOptions);

    trackSearchWithTune(true);

    this.startActivityForResult(intentWaiting, Constants.REQUEST_CODE_SEARCH);
  }

  private void synchronizeSearch() {
    try {
      StoredSearch storedSearch =
          storedSearchOptionsMapper.searchOptionToStoredSearchMapper(searchOptions);
      if (!mSearchOptionsList.contains(searchOptions)) {
        storedSearch.setStoredSearchId(0);
        mSearchFlightInteractor.addNewFlightSearch(storedSearch);
      } else {
        int actualIndex = mSearchOptionsList.indexOf(searchOptions);
        SearchOptions actualSearchOption = mSearchOptionsList.get(actualIndex);
        actualSearchOption.setSearchDate(searchOptions.getSearchDate());
        storedSearch =
            storedSearchOptionsMapper.searchOptionToStoredSearchMapper(actualSearchOption);
        mSearchFlightInteractor.updateFlightSearch(storedSearch);
      }
    } catch (NullPointerException ex) {
      ex.printStackTrace();
    }
  }

  private void trackSearchWithTune(boolean isSearch) {
    extractSearchOptionsDataToTuneHelper();
    mTuneTracker.trackSearch(isSearch);
  }

  private void extractSearchOptionsDataToTuneHelper() {
    TuneHelper.clear();
    int passengers = searchOptions.getNumberOfAdults();
    passengers += searchOptions.getNumberOfKids();
    passengers += searchOptions.getNumberOfBabies();
    for (FlightSegment flightSegment : searchOptions.getFlightSegments()) {
      if (flightSegment.getDepartureCity() != null && flightSegment.getArrivalCity() != null) {
        String departure = flightSegment.getDepartureCity().getIataCode();
        String arrival = flightSegment.getArrivalCity().getIataCode();
        long date = flightSegment.getDate();
        long returnDate = flightSegment.getArrivalDate();
        TuneHelper.addSegment(new TuneSegment(departure, arrival, date, returnDate));
      }
    }
    TuneHelper.setPassengers(passengers);
  }

  public FlightSegment cloneFlightSegment(FlightSegment original) {
    if (original == null) {
      return new FlightSegment();
    }

    FlightSegment newFlight = new FlightSegment();
    newFlight.setDepartureCity(original.getDepartureCity());
    newFlight.setArrivalCity(original.getArrivalCity());
    if (Validations.checkDateForSearch(OdigeoDateUtils.createDate(original.getDate()))) {
      newFlight.setDate(original.getDate());
    } else {
      newFlight.setDate(0);
    }
    if (Validations.checkDateForSearch(OdigeoDateUtils.createDate(original.getArrivalDate()))) {
      newFlight.setArrivalDate(original.getArrivalDate());
    } else {
      newFlight.setArrivalDate(0);
    }
    return newFlight;
  }

  private void deleteHistory(TripFragmentBase fragment) {
    switch (fragment.getTravelType()) {
      case SIMPLE:
        simpleTripFragment.removeHistory(StoredSearch.TripType.O);
        break;

      case ROUND:
        roundTripFragment.removeHistory(StoredSearch.TripType.R);
        break;

      case MULTIDESTINATION:
        multiTripFragment.removeHistory(StoredSearch.TripType.M);
        break;
    }
  }

  private void askToDelete(final TripFragmentBase fragment) {
    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

    alertDialogBuilder.setTitle(LocalizablesFacade.getString(this, OneCMSKeys.INFOSTATUS_TITLE));

    DialogInterface.OnClickListener positiveClickListener = new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        deleteHistory(fragment);
        dialog.cancel();

        AndroidDependencyInjector.getInstance()
            .provideTrackerController()
            .trackLocalyticsEventWithOneAtribute(TrackerConstants.FLIGHTS_SEARCH_SUMMARY_EVENT,
                TrackerConstants.CLEARED_SEARCH_HISTORY, TrackerConstants.YES);
      }
    };
    DialogInterface.OnClickListener negativeClickListener = new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.cancel();

        AndroidDependencyInjector.getInstance()
            .provideTrackerController()
            .trackLocalyticsEventWithOneAtribute(TrackerConstants.FLIGHTS_SEARCH_SUMMARY_EVENT,
                TrackerConstants.CLEARED_SEARCH_HISTORY, TrackerConstants.NO);
      }
    };

    alertDialogBuilder.setMessage(
        LocalizablesFacade.getString(this, OneCMSKeys.MY_TRIPS_DELETE_TRIP))
        .setCancelable(false)
        .setPositiveButton(LocalizablesFacade.getString(this, OneCMSKeys.COMMON_YES),
            positiveClickListener)
        .setNegativeButton(LocalizablesFacade.getString(this, OneCMSKeys.COMMON_NO),
            negativeClickListener);

    // create alert dialog
    AlertDialog alertDialog = alertDialogBuilder.create();

    // show it
    alertDialog.show();
  }

  /**
   * Check if necessary to show resident dialog.
   *
   * @param tripFragment Type of trip selected.
   */
  private void validateResidentsHistory(TripFragmentBase tripFragment) {

    FlightSegment flightSegment = searchOptions.getFlightSegments().get(0);

    City origin = flightSegment.getDepartureCity();
    City destination = flightSegment.getArrivalCity();

    ResidentItinerary residentItinerary = Residents.applyResidentDiscount(origin, destination);
    if (residentItinerary != null) {
      showDialogResidentHistory(tripFragment, residentItinerary);
    } else {
      launchSearchFromHistory(tripFragment, false);
    }
  }

  /**
   * Show dialog to residents when is selected an option from history searches saved.
   *
   * @param tripFragment Type of trip selected.
   */

  private void showDialogResidentHistory(final TripFragmentBase tripFragment,
      final ResidentItinerary residentItinerary) {

    String title =
        LocalizablesFacade.getString(this, OneCMSKeys.PAYMENT_ALERT_COUPON_TITLE).toString();
    String message =
        LocalizablesFacade.getString(this, OneCMSKeys.RESIDENTS_DIALOG_TEXT).toString();
    String btnAgree = LocalizablesFacade.getString(this, OneCMSKeys.COMMON_OK).toString();
    String btnCancel = LocalizablesFacade.getString(this, OneCMSKeys.COMMON_CANCEL).toString();

    AlertDialog.Builder ad = new AlertDialog.Builder(this);
    ad.setTitle(title);
    ad.setMessage(message);

    ad.setPositiveButton(btnAgree, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
        searchOptions.setResident(residentItinerary);
        tripFragment.setSearchOptions(searchOptions);
        dialog.dismiss();
        launchSearchFromHistory(tripFragment, true);
      }
    });
    ad.setNegativeButton(btnCancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
        dialog.dismiss();
      }
    });
    ad.show();
  }

  /**
   * Perform the launch of a search from a history value. Perform the launch of a search from a
   * history value.
   *
   * @param fragmentBase Fragment to perform the task.
   */
  private void launchSearchFromHistory(@NonNull TripFragmentBase fragmentBase, boolean isResident) {
    if (fragmentBase.isViewCreated()) {
      if (fragmentBase.isReadyForSearch()) {
        fragmentBase.launchSearch(isResident);
        fragmentBase.getFlightSegments();
      }
    } else {
      fragmentBase.setShouldLaunchSearch(fragmentBase.isReadyForSearch());
    }
  }

  public void validateResidents(FlightSegment flightSegments) {

    City origin = flightSegments.getDepartureCity();
    City destination = flightSegments.getArrivalCity();

    ResidentItinerary residentItinerary = Residents.applyResidentDiscount(origin, destination);

    searchOptions.setResident(residentItinerary);

    if (roundTripFragment != null) {
      roundTripFragment.showResidentWidget(residentItinerary);
    }

    if (simpleTripFragment != null) {
      simpleTripFragment.showResidentWidget(residentItinerary);
    }
  }

  private void onlyForAdultsDialog() {
    String title =
        LocalizablesFacade.getString(this, OneCMSKeys.RESIDENTACREDITATIONMODULE_ALERT_DIALOG_TITLE)
            .toString();
    String message = LocalizablesFacade.getString(this,
        OneCMSKeys.RESIDENTACREDITATIONMODULE_ALERT_DIALOG_MESSAGE).toString();
    CharSequence btnAgree = LocalizablesFacade.getString(this, OneCMSKeys.COMMON_OK);

    AlertDialog.Builder ad = new AlertDialog.Builder(this);
    ad.setTitle(title);
    ad.setMessage(message);

    ad.setPositiveButton(btnAgree, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
        dialog.dismiss();
      }
    });
    ad.show();
  }

  public void setSearchOptions(SearchOptions searchOptions) {
    this.searchOptions = searchOptions;
  }

  public void setSearchOptionsList(List<SearchOptions> searchOptionsList) {
    mSearchOptionsList = searchOptionsList;
  }

  public List<FlightSegment> getFlightSegmentsSimple() {
    return flightSegmentsSimple;
  }

  public List<FlightSegment> getFlightSegmentsRound() {
    return flightSegmentsRound;
  }

  public List<FlightSegment> getFlightSegmentsMultiple() {
    return flightSegmentsMultiple;
  }

  private void postGAEventTrackingGoBack() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHT_SEARCH,
            GAnalyticsNames.ACTION_SEARCHER_FLIGHTS, GAnalyticsNames.LABEL_GO_BACK));
  }

  /**
   * Data about a search is showed in console.
   */
  private void printLogCatHistorySearch(SearchOptions searchOptions) {
    Log.i(Constants.TAG_LOG, "Launch search: "
        + searchOptions.getTravelType().name()
        + " - From: "
        + searchOptions.getLastSegment().getDepartureCity().getCityName()
        + "("
        + searchOptions.getLastSegment().getDate()
        + ")"
        + " - To: "
        + searchOptions.getLastSegment().getArrivalCity().getCityName()
        + "("
        + searchOptions.getLastSegment().getArrivalDate()
        + ")"
        + " - Class: "
        + searchOptions.getCabinClass()
        + " - Is direct: "
        + searchOptions.isDirectFlight()
        + " - Adults: "
        + searchOptions.getNumberOfAdults()
        + " - Kids: "
        + searchOptions.getNumberOfKids()
        + " - Babies: "
        + searchOptions.getNumberOfBabies()
        + " - Total passengers: "
        + searchOptions.getTotalPassengers());
  }

  public abstract void onGAEventTriggered(GATrackingEvent event);

  public boolean isResident() {
    return isResident;
  }

  public void setResident(boolean resident) {
    isResident = resident;
  }
}
