package com.odigeo.app.android.notification;

import android.content.Context;

public abstract class OdigeoNotificationBuilder {

  protected OdigeoNotification odigeoNotification;
  protected Context context;

  public OdigeoNotificationBuilder() {
    odigeoNotification = new OdigeoNotification();
  }

  public abstract void buildNotificationBuilder();

  public abstract void buildNotificationIntent();

  public abstract void buildNotificationTaskStackBuilder();

  public abstract void buildPendingIntent();

  public abstract void sendNotification();
}
