package com.odigeo.app.android.lib.ui.widgets.base;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;

/**
 * Created by manuel on 12/09/14.
 */
public class ImageWithTitleAndDescription extends LinearLayout {
  private int imageSource;
  private String title;
  private String text;

  private ImageView imageView;
  private TextView txtText;
  private TextView txtTitle;

  public ImageWithTitleAndDescription(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray aAttrs =
        context.obtainStyledAttributes(attrs, R.styleable.ImageWithTitleAndDescription, 0, 0);

    imageSource = aAttrs.getResourceId(R.styleable.ImageWithTitleAndDescription_android_src, 0);
    title = aAttrs.getString(R.styleable.ImageWithTitleAndDescription_textTitle);
    text = aAttrs.getString(R.styleable.ImageWithTitleAndDescription_odigeoText);

    aAttrs.recycle();

    inflateLayout();
  }

  private void inflateLayout() {

    inflate(this.getContext(), R.layout.layout_search_widget_image_title_description, this);

    imageView = (ImageView) findViewById(R.id.imgWidgetImageTitleDescription);
    txtTitle = (TextView) findViewById(R.id.titleWidgetImageTitleDescription);
    txtText = (TextView) findViewById(R.id.textWidgetImageTitleDescription);

    if (imageView != null) {
      imageView.setImageResource(imageSource);
    }

    if (txtTitle != null) {
      txtTitle.setText(LocalizablesFacade.getString(getContext(), title));
    }

    if (txtText != null) {
      txtText.setText(LocalizablesFacade.getString(getContext(), text));
    }
  }

  public final String getTitle() {
    return title;
  }

  public final void setTitle(String title) {
    this.title = title;
  }

  public final int getImageSource() {
    return imageSource;
  }

  public final void setImageSource(int imageSource) {
    this.imageSource = imageSource;
  }

  public final String getText() {
    return text;
  }

  public final void setText(String text) {
    this.text = text;
  }
}
