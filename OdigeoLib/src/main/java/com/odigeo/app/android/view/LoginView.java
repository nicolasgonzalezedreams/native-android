package com.odigeo.app.android.view;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.navigator.LoginNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.PermissionsHelper;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.LoginViewPresenter;
import com.odigeo.presenter.contracts.views.LoginViewInterface;

import static android.Manifest.permission.GET_ACCOUNTS;
import static com.odigeo.app.android.view.helpers.PermissionsHelper.ACCOUNTS_FACEBOOK_REQUEST_CODE;
import static com.odigeo.app.android.view.helpers.PermissionsHelper.ACCOUNTS_GOOGLE_REQUEST_CODE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_SIGN_IN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.FACEBOOK_LOGIN_TAP_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.GOOGLE_PLUS_LOGIN_TAP_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_FORGOT_PASSWORD_CLICKS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_PASSWORD_UNMASK;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SIGN_IN_FACEBOOK_DATALOCAL;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SIGN_IN_FACEBOOK_DATANO;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SIGN_IN_GOOGLE_DATALOCAL;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SIGN_IN_GOOGLE_DATANO;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SIGN_IN_ODIGEO_DATALOCAL;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SIGN_IN_ODIGEO_DATANO;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SSO_EMAIL_INCORRECT_FORMAT_ERROR;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SSO_PWD_PCI_COMPLIANCE_ERROR;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SSO_WRONG_SIGNIN_CREDENTIALS_ERROR;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LOGIN_TAP_EVENT;

public class LoginView extends SocialLoginHelperView implements LoginViewInterface {

  protected EditText mUsername;
  protected Button mBtnLogin;
  protected AppCompatCheckBox mCbShowPassword;
  protected Boolean mIsMailFormatCorrect;
  private EditText mPassword;
  private Button mBtnFacebook;
  private Button mBtnGoogle;
  private TextView mTvError;
  private TextView mTxtRequestForgottenPassword;
  private TextView mTvPasswordMin;
  TextWatcher onTextChanged = new TextWatcher() {
    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
      mTvPasswordMin.setTextColor(getResources().getColor(R.color.com_facebook_blue));
      ((LoginViewPresenter) mPresenter).validateUsernameAndPasswordFormat(
          mUsername.getText().toString().trim(), mPassword.getText().toString().trim());
      mIsMailFormatCorrect =
          mPresenter.validateUsernameFormat(mUsername.getText().toString().trim());
    }

    @Override public void afterTextChanged(Editable s) {

    }
  };
  private TextView mTvWrongMailFormat;
  private AlphaAnimation fadeIn;
  private AlphaAnimation fadeOut;
  private AlphaAnimation fadeOutPasswordMessage;
  private Boolean mIsTvWrongFormatMailVisible;

  public static LoginView newInstance() {
    return new LoginView();
  }

  @Override public void onResume() {
    super.onResume();
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_LOGIN);
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
  }

  @Override protected LoginViewPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideLoginPresenter(this, (LoginNavigator) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_login_view;
  }

  @Override protected void initComponent(View view) {
    mUsername = (EditText) view.findViewById(R.id.etEmail);
    mPassword = (EditText) view.findViewById(R.id.etPassword);
    mBtnLogin = (Button) view.findViewById(R.id.btnLogin);
    mBtnFacebook = (Button) view.findViewById(R.id.btnFacebookSignUp);
    mBtnGoogle = (Button) view.findViewById(R.id.btnGoogleSignUp);
    mTxtRequestForgottenPassword = (TextView) view.findViewById(R.id.txtRequestForgottenPassword);
    mTvError = (TextView) view.findViewById(R.id.tvError);
    mCbShowPassword = (AppCompatCheckBox) view.findViewById(R.id.cbShowPassword);
    mTvPasswordMin = (TextView) view.findViewById(R.id.tvPasswordMin);
    mTvWrongMailFormat = (TextView) view.findViewById(R.id.tvWrongMailFormat);
    mIsMailFormatCorrect = false;
    mIsTvWrongFormatMailVisible = false;
    enableLoginButton(false);
  }

  @Override protected void setListeners() {

    setLoginLegacyListeners();

    mBtnFacebook.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          if (PermissionsHelper.askForPermissionIfNeeded(GET_ACCOUNTS, getActivity(),
              OneCMSKeys.PERMISSION_ACCOUNT_SIGNIN_MESSAGE, ACCOUNTS_FACEBOOK_REQUEST_CODE)) {
            facebookLogin();
          }
        } else {
          facebookLogin();
        }
      }
    });

    mBtnGoogle.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          if (PermissionsHelper.askForPermissionIfNeeded(GET_ACCOUNTS, getActivity(),
              OneCMSKeys.PERMISSION_ACCOUNT_SIGNIN_MESSAGE, ACCOUNTS_GOOGLE_REQUEST_CODE)) {
            googlePlusLogin();
          }
        } else {
          googlePlusLogin();
        }
      }
    });
  }

  public void googlePlusLogin() {
    showProgress();
    loginGoogle();
    String username = mUsername.getText().toString().trim();
    String password = mPassword.getText().toString().trim();
    if (mPresenter.hasLocalData()) {
      mTracker.trackLoginPage(getSSOTrackingCategory(), ACTION_SIGN_IN,
          LABEL_SIGN_IN_GOOGLE_DATALOCAL, GOOGLE_PLUS_LOGIN_TAP_EVENT, username.isEmpty(),
          password.isEmpty(), false, false, true, false);
    } else {
      mTracker.trackLoginPage(getSSOTrackingCategory(), ACTION_SIGN_IN, LABEL_SIGN_IN_GOOGLE_DATANO,
          GOOGLE_PLUS_LOGIN_TAP_EVENT, username.isEmpty(), password.isEmpty(), false, false, true,
          false);
    }
  }

  public void facebookLogin() {
    showProgress();
    loginFacebook();
    String username = mUsername.getText().toString().trim();
    String password = mPassword.getText().toString().trim();
    if (mPresenter.hasLocalData()) {
      mTracker.trackLoginPage(getSSOTrackingCategory(), ACTION_SIGN_IN,
          LABEL_SIGN_IN_FACEBOOK_DATALOCAL, FACEBOOK_LOGIN_TAP_EVENT, username.isEmpty(),
          password.isEmpty(), false, true, false, false);
    } else {
      mTracker.trackLoginPage(getSSOTrackingCategory(), ACTION_SIGN_IN,
          LABEL_SIGN_IN_FACEBOOK_DATANO, FACEBOOK_LOGIN_TAP_EVENT, username.isEmpty(),
          password.isEmpty(), false, true, false, false);
    }
  }

  protected void setLoginLegacyListeners() {

    mBtnLogin.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        String username = mUsername.getText().toString().trim();
        String password = mPassword.getText().toString().trim();
        hideErrorMessage();
        showProgress();
        ((LoginViewPresenter) mPresenter).loginWithPassword(username, password);
        if (mPresenter.hasLocalData()) {
          mTracker.trackLoginPage(getSSOTrackingCategory(), ACTION_SIGN_IN,
              LABEL_SIGN_IN_ODIGEO_DATALOCAL, LOGIN_TAP_EVENT, username.isEmpty(),
              password.isEmpty(), true, false, false, false);
        } else {
          mTracker.trackLoginPage(getSSOTrackingCategory(), ACTION_SIGN_IN,
              LABEL_SIGN_IN_ODIGEO_DATANO, LOGIN_TAP_EVENT, username.isEmpty(), password.isEmpty(),
              true, false, false, false);
        }
      }
    });

    mTxtRequestForgottenPassword.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        String username = mUsername.getText().toString().trim();
        String password = mPassword.getText().toString().trim();
        mTracker.trackLoginRequestPassPage(getSSOTrackingCategory(), ACTION_SIGN_IN,
            LABEL_FORGOT_PASSWORD_CLICKS, username.isEmpty(), password.isEmpty(), false, false,
            false, true);
        ((LoginViewPresenter) mPresenter).requestForgottenPassword(username);
      }
    });

    mCbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
          mPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        } else {
          mPassword.setInputType(
              InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
          mTracker.trackAnalyticsEvent(getSSOTrackingCategory(), ACTION_SIGN_IN,
              LABEL_PASSWORD_UNMASK);
        }
      }
    });

    mUsername.addTextChangedListener(onTextChanged);

    mPassword.addTextChangedListener(onTextChanged);

    prepareFadeAnimations();

    mPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override public void onFocusChange(View view, boolean hasFocus) {
        if (hasFocus) {
          mTvPasswordMin.startAnimation(fadeIn);
        } else {
          mTvPasswordMin.startAnimation(fadeOutPasswordMessage);
        }
      }
    });

    mUsername.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                         @Override public void onFocusChange(View view, boolean hasFocus) {
                                           if (hasFocus) {
                                             if (mIsTvWrongFormatMailVisible) {
                                               mTvWrongMailFormat.startAnimation(fadeOut);
                                               mIsTvWrongFormatMailVisible = false;
                                             }
                                           } else {
                                             if (!mIsMailFormatCorrect) {
                                               if (!mIsTvWrongFormatMailVisible) {
                                                 mTvWrongMailFormat.startAnimation(fadeIn);
                                                 mIsTvWrongFormatMailVisible = true;
                                               }
                                             } else {
                                               if (mIsTvWrongFormatMailVisible) {
                                                 mTvWrongMailFormat.startAnimation(fadeOut);
                                                 mIsTvWrongFormatMailVisible = false;
                                               }
                                             }
                                           }
                                         }
                                       }

    );
  }

  @Override protected void initOneCMSText(View view) {
    TextInputLayout tilUsername = (TextInputLayout) view.findViewById(R.id.etEmailLayout);
    TextInputLayout tilPassword = (TextInputLayout) view.findViewById(R.id.etPasswordLayout);
    tilUsername.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_FORM_SHADOWTEXT_EMAIL));
    tilPassword.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_FORM_SHADOWTEXT_PASSWORD));
    TextView tvOr = ((TextView) view.findViewById(R.id.tvOr));
    tvOr.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.COMMON_OR));
    mBtnFacebook.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_REGISTERINFO_SIGNIN_FACEBOOK));
    mBtnGoogle.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_REGISTERINFO_SIGNIN_GOOGLE));
    mBtnLogin.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_LOGIN_BUTTON));
    mCbShowPassword.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_SHOW_PASSWORD));
    mTvWrongMailFormat.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_WRONG_EMAIL_FORMAT));
    mTvPasswordMin.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_FORM_ERROR_PASSWORD_FORMAT));
    mTxtRequestForgottenPassword.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_SIGNIN_FORGOT_PASSWORD));
  }

  private void prepareFadeAnimations() {
    fadeIn = new AlphaAnimation(0.0f, 1.0f);
    fadeOut = new AlphaAnimation(1.0f, 0.0f);
    fadeOutPasswordMessage = new AlphaAnimation(1.0f, 0.0f);
    fadeOutPasswordMessage.setDuration(200);
    fadeOutPasswordMessage.setFillAfter(true);
    fadeIn.setDuration(200);
    fadeIn.setFillAfter(true);
    fadeOut.setDuration(200);
    fadeOut.setFillAfter(true);
  }

  @Override public void setUsernameError() {
    mUsername.setError(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ERROR_INCORRECT_USERNAME));
    mTracker.trackAnalyticsEvent(getSSOTrackingCategory(), ACTION_SIGN_IN,
        LABEL_SSO_EMAIL_INCORRECT_FORMAT_ERROR);
  }

  @Override public void setCredentialsError() {
    mTvError.setVisibility(View.VISIBLE);
    mTvError.setText(LocalizablesFacade.getString(getActivity(),
        OneCMSKeys.SSO_SIGNIN_ERROR_PASSWORD_EMAIL_INCORRECT));
    mTracker.trackAnalyticsEvent(getSSOTrackingCategory(), ACTION_SIGN_IN,
        LABEL_SSO_WRONG_SIGNIN_CREDENTIALS_ERROR);
  }

  @Override public void setUserDoesNotExistError() {
    mTvError.setVisibility(View.VISIBLE);
    mTvError.setText(LocalizablesFacade.getString(getActivity(),
        OneCMSKeys.SSO_ERROR_EMAIL_NOT_REGISTER_OR_ACTIVE));
  }

  @Override public void setPasswordDoesNotValidate() {
    mTvPasswordMin.setTextColor(getResources().getColor(R.color.semantic_negative_blocker));
    mTracker.trackAnalyticsEvent(getSSOTrackingCategory(), ACTION_SIGN_IN,
        LABEL_SSO_PWD_PCI_COMPLIANCE_ERROR);
  }

  @Override public void setServerError() {
    mTvError.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ERROR_SERVER));
    mTvError.setVisibility(View.VISIBLE);
  }

  @Override public void showPendingEmailDialog(String email) {
    AlertDialog.Builder pendingMailDialog = new AlertDialog.Builder(getActivity());
    pendingMailDialog.setTitle(LocalizablesFacade.getString(getActivity(),
        OneCMSKeys.SSO_ERROR_ACCOUNT_NOT_ACTIVATED_TITLE));
    pendingMailDialog.setMessage(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ACCOUNT_NOT_ACTIVATED_MSG, email)
            .toString());
    pendingMailDialog.setPositiveButton(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_GO_TO_MAIL),
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            ((LoginViewPresenter) mPresenter).goToEmailPendingScreen();
          }
        });
    pendingMailDialog.setNegativeButton(android.R.string.cancel,
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
          }
        });
    pendingMailDialog.show();
  }

  public void hideErrorMessage() {
    mTvError.setVisibility(View.INVISIBLE);
  }

  @Override public void enableLoginButton(boolean isEnabled) {
    mBtnLogin.setEnabled(isEnabled);
  }
}
