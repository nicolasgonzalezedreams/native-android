package com.odigeo.app.android.view.adapters;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.data.entity.userData.Membership;
import java.util.List;

public class MembershipQARecyclerAdapter extends Adapter<MembershipQARecyclerAdapter
    .MembershipViewHolder> {

  private List<Membership> memberships;

  public MembershipQARecyclerAdapter(List<Membership> memberships) {

    this.memberships = memberships;
  }

  @Override public MembershipViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.membership_qa_item, parent, false);

    return new MembershipViewHolder(view);
  }

  @Override public void onBindViewHolder(MembershipViewHolder holder, int position) {
    Membership membership = memberships.get(position);
    holder.textViewMembershipId.setText(membership.getMemberId()+"");
    holder.textViewName.setText(membership.getFirstName());
    holder.textViewSurname.setText(membership.getLastNames());
    holder.textViewMarket.setText(membership.getWebsite());
  }

  @Override public int getItemCount() {
    return memberships.size();
  }

  public void updateContent(List<Membership> allMemberships) {
    this.memberships.clear();
    this.memberships.addAll(allMemberships);
    notifyDataSetChanged();
  }

  public class MembershipViewHolder extends RecyclerView.ViewHolder {

    public TextView textViewMembershipId;
    public TextView textViewName;
    public TextView textViewSurname;
    public TextView textViewMarket;

    public MembershipViewHolder(View itemView) {
      super(itemView);

      textViewMembershipId = (TextView) itemView.findViewById(R.id.textViewMembershipId);
      textViewName = (TextView) itemView.findViewById(R.id.textViewName);
      textViewSurname = (TextView) itemView.findViewById(R.id.textViewSurname);
      textViewMarket = (TextView) itemView.findViewById(R.id.textViewMarketList);
    }
  }
}
