package com.odigeo.app.android.view;

import android.content.Context;
import com.odigeo.data.entity.extensions.ShoppingCartCollectionOptionSpinner;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import java.util.ArrayList;
import java.util.List;

class ShoppingCartCollectionMapper {

  static List<ShoppingCartCollectionOptionSpinner> mapToCollectionOptionSpinner(
      List<ShoppingCartCollectionOption> shoppingCartCollectionOptions, Context context) {
    List<ShoppingCartCollectionOptionSpinner> shoppingCartCollectionOptionSpinners =
        new ArrayList<>();

    for (ShoppingCartCollectionOption shoppingCartCollectionOption : shoppingCartCollectionOptions) {
      ShoppingCartCollectionOptionSpinner shoppingCartCollectionOptionSpinner =
          new ShoppingCartCollectionOptionSpinner();
      shoppingCartCollectionOptionSpinner.setMethod(shoppingCartCollectionOption.getMethod());
      shoppingCartCollectionOptionSpinner.setBankTransferText(
          shoppingCartCollectionOption.getBankTransferText());
      shoppingCartCollectionOptionSpinner.setFee(shoppingCartCollectionOption.getFee());
      shoppingCartCollectionOptionSpinner.setInstallments(
          shoppingCartCollectionOption.getInstallments());
      shoppingCartCollectionOptionSpinners.add(shoppingCartCollectionOptionSpinner);
    }

    if (!shoppingCartCollectionOptionSpinners.isEmpty()) {
      for (ShoppingCartCollectionOptionSpinner shoppingCartCollectionOptionSpinner : shoppingCartCollectionOptionSpinners) {
        shoppingCartCollectionOptionSpinner.generateResources(context);
      }
    }
    return shoppingCartCollectionOptionSpinners;
  }
}
