package com.odigeo.app.android.view.animations;

import android.support.annotation.Nullable;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CollapseAndExpandAnimationUtil {

  private static double DELAYED_TIME = 1.5;
  private static double DELAYED_TIME_CLOSE = 2;
  private static long FADE_IN_TIME = 250;
  private static long FADE_OUT_TIME = 250;

  public static void collapse(@Nullable final View colapseView, boolean hasToScroll,
      final ScrollView scroll, @Nullable final View goToView) {

    if (colapseView == null || goToView == null) return;

    final int initialHeight = colapseView.getMeasuredHeight();

    Animation animation = new Animation() {
      @Override protected void applyTransformation(float interpolatedTime, Transformation t) {
        if (interpolatedTime == 1) {
          colapseView.setVisibility(View.GONE);
        } else {
          colapseView.getLayoutParams().height =
              initialHeight - (int) (initialHeight * interpolatedTime);
          colapseView.requestLayout();
        }
      }

      @Override public boolean willChangeBounds() {
        return true;
      }
    };

    if (hasToScroll && scroll != null && goToView != null) {
      animation.setAnimationListener(new CollapseAnimationListener(goToView, goToView, scroll));
    }
    animation.setInterpolator(new FastOutLinearInInterpolator());
    animation.setDuration((int) ((initialHeight * DELAYED_TIME) / colapseView.getContext()
        .getResources()
        .getDisplayMetrics().density));
    colapseView.startAnimation(animation);
  }

  public static void expand(@Nullable final View v) {

    v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    final int targetHeight = v.getMeasuredHeight();

    // Older versions of android (pre API 21) cancel animations for views with a height of 0.
    v.getLayoutParams().height = 1;
    v.setVisibility(View.VISIBLE);
    Animation animation = new Animation() {
      @Override protected void applyTransformation(float interpolatedTime, Transformation t) {
        v.getLayoutParams().height = interpolatedTime == 1 ? LinearLayout.LayoutParams.WRAP_CONTENT
            : (int) (targetHeight * interpolatedTime);
        v.requestLayout();
      }

      @Override public boolean willChangeBounds() {
        return true;
      }
    };

    animation.setInterpolator(new FastOutLinearInInterpolator());
    animation.setDuration((int) ((targetHeight * DELAYED_TIME) / v.getContext()
        .getResources()
        .getDisplayMetrics().density));
    v.startAnimation(animation);
  }

  public static void expandAndFadeIn(final View v) {
    v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    final int targetHeight = v.getMeasuredHeight();

    // Older versions of android (pre API 21) cancel animations for views with a height of 0.
    v.getLayoutParams().height = 1;
    v.setVisibility(View.VISIBLE);
    v.setAlpha(0);
    Animation animation = new Animation() {
      @Override protected void applyTransformation(float interpolatedTime, Transformation t) {
        v.getLayoutParams().height = interpolatedTime == 1 ? LinearLayout.LayoutParams.WRAP_CONTENT
            : (int) (targetHeight * interpolatedTime);
        v.requestLayout();
      }

      @Override public boolean willChangeBounds() {
        return true;
      }
    };

    animation.setAnimationListener(new Animation.AnimationListener() {
      @Override public void onAnimationStart(Animation animation) {

      }

      @Override public void onAnimationEnd(Animation animation) {
        v.setAlpha(1);
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator());
        fadeIn.setDuration(FADE_IN_TIME);
        v.startAnimation(fadeIn);
      }

      @Override public void onAnimationRepeat(Animation animation) {

      }
    });
    animation.setInterpolator(new FastOutLinearInInterpolator());
    animation.setDuration((int) ((targetHeight * DELAYED_TIME) / v.getContext()
        .getResources()
        .getDisplayMetrics().density));
    v.startAnimation(animation);
  }

  public static void fadeOut(final View childView) {

    final float fromOpaque = 1;
    final float toTransparent = 0;

    Animation fadeOutAnimation = new AlphaAnimation(fromOpaque, toTransparent);
    fadeOutAnimation.setInterpolator(new DecelerateInterpolator());
    fadeOutAnimation.setDuration(FADE_OUT_TIME);
    fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
      @Override public void onAnimationStart(Animation animation) {

      }

      @Override public void onAnimationEnd(Animation animation) {
        childView.setAlpha(0);
      }

      @Override public void onAnimationRepeat(Animation animation) {

      }
    });

    childView.startAnimation(fadeOutAnimation);
  }
}