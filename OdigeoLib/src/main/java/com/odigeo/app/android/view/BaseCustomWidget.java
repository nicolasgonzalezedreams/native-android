package com.odigeo.app.android.view;

import android.content.Context;
import android.util.AttributeSet;
import com.odigeo.presenter.contracts.components.BaseCustomComponentPresenter;

public abstract class BaseCustomWidget<P extends BaseCustomComponentPresenter>
    extends BaseCustomView {

  protected P presenter;

  public BaseCustomWidget(Context context) {
    super(context);
    init();
  }

  public BaseCustomWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  private void init() {
    presenter = setPresenter();
  }

  protected abstract P setPresenter();
}
