package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class BookingComponentPriceBreakdownDTO implements Serializable {

  protected String componentName;
  protected BigDecimal componentCollectionAmount;

  /**
   * Gets the value of the componentName property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getComponentName() {
    return componentName;
  }

  /**
   * Sets the value of the componentName property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setComponentName(String value) {
    this.componentName = value;
  }

  /**
   * Gets the value of the componentCollectionAmount property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getComponentCollectionAmount() {
    return componentCollectionAmount;
  }

  /**
   * Sets the value of the componentCollectionAmount property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setComponentCollectionAmount(BigDecimal value) {
    this.componentCollectionAmount = value;
  }
}
