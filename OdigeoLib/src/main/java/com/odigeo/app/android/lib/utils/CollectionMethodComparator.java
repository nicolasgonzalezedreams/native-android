package com.odigeo.app.android.lib.utils;

import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import java.util.Comparator;

/**
 * Created by manuel on 17/10/14.
 */
public class CollectionMethodComparator implements Comparator<CollectionMethodWithPrice> {
  @Override public final int compare(CollectionMethodWithPrice collectionMethodWithPrice,
      CollectionMethodWithPrice collectionMethodWithPrice2) {
    if (collectionMethodWithPrice == null || collectionMethodWithPrice.getPrice() == null) {
      return 1;
    }

    if (collectionMethodWithPrice2 == null || collectionMethodWithPrice2.getPrice() == null) {
      return -1;
    }

    return collectionMethodWithPrice.getPrice().compareTo(collectionMethodWithPrice2.getPrice());
  }
}
