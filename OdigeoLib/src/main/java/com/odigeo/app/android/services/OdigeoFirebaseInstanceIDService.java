package com.odigeo.app.android.services;

import android.content.Intent;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.dataodigeo.session.SessionController;

/**
 * A service that extends FirebaseInstanceIdService to handle the creation, rotation, and updating
 * of registration tokens.
 * This is required for sending to specific devices or for creating device groups.
 */

public class OdigeoFirebaseInstanceIDService extends FirebaseInstanceIdService {

  @Override public void onTokenRefresh() {
    invalidateGCMToken();
    Intent intent = new Intent(this, OdigeoFirebaseTokenRegistrationService.class);
    startService(intent);
  }

  private void invalidateGCMToken() {
    SessionController sessionController =
        AndroidDependencyInjector.getInstance().provideSessionController();
    sessionController.invalidateGcmToken();
  }
}
