package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by manuel on 26/09/14.
 */
public class InsuranceShoppingItemDTO implements Serializable {
  protected InsuranceDTO insurance;
  protected BigDecimal totalPrice;
  protected boolean selectable;
  protected String id;
  protected BigDecimal providerPrice;

  public InsuranceShoppingItemDTO() {

  }

  public InsuranceShoppingItemDTO(String id) {
    this.id = id;
  }

  /**
   * Gets the value of the insurance property.
   *
   * @return possible object is
   * {@link InsuranceDTO }
   */
  public InsuranceDTO getInsurance() {
    return insurance;
  }

  /**
   * Sets the value of the insurance property.
   *
   * @param value allowed object is
   * {@link InsuranceDTO }
   */
  public void setInsurance(InsuranceDTO value) {
    this.insurance = value;
  }

  /**
   * Gets the value of the totalPrice property.
   *
   * @return possible object is
   * {@link BigDecimal }
   */
  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  /**
   * Sets the value of the totalPrice property.
   *
   * @param value allowed object is
   * {@link BigDecimal }
   */
  public void setTotalPrice(BigDecimal value) {
    this.totalPrice = value;
  }

  /**
   * Gets the value of the selectable property.
   */
  public boolean isSelectable() {
    return selectable;
  }

  /**
   * Sets the value of the selectable property.
   */
  public void setSelectable(boolean value) {
    this.selectable = value;
  }

  /**
   * Gets the value of the id property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setId(String value) {
    this.id = value;
  }

  public boolean equals(Object object) {
    if (!(object instanceof InsuranceShoppingItemDTO)) {
      return false;
    }

    return this.getId().equals(((InsuranceShoppingItemDTO) object).getId());
  }

  /**
   * Gets the value of the providerPrice property.
   *
   * @return possible object is
   * {@link BigDecimal }
   */
  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  /**
   * Sets the value of the providerPrice property.
   *
   * @param value allowed object is
   * {@link BigDecimal }
   */
  public void setProviderPrice(BigDecimal value) {
    this.providerPrice = value;
  }
}

