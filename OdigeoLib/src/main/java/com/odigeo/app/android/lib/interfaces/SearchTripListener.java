package com.odigeo.app.android.lib.interfaces;

import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.app.android.lib.ui.widgets.SearchTripWidget;

/**
 * Created by manuel on 12/09/14.
 */
public interface SearchTripListener {

  void onClickSearch(SearchTripWidget searchTripWidget, boolean isResident);

  void onClickDirectFlight(boolean checked);

  void onClickButtonPassenger();

  void onCabinClassSelected(CabinClassDTO cabinClassSelected);

  void onResidentSwitchClicked(boolean checked);
}
