package com.odigeo.app.android.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.JoinUsPresenter;
import com.odigeo.presenter.contracts.navigators.JoinUsNavigatorInterface;
import com.odigeo.presenter.contracts.views.JoinUsViewInterface;

import static com.odigeo.dataodigeo.tracker.TrackerConstants.REGISTER_TAP_EVENT;

public class JoinUsView extends BaseView<JoinUsPresenter> implements JoinUsViewInterface {

  private Button mButtonLogIn;
  private Button mButtonRegisterAccount;

  public static JoinUsView newInstance() {
    return new JoinUsView();
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override public void onResume() {
    super.onResume();
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_MYINFO_SSO);
  }

  @Override protected JoinUsPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideJoinUsPresenter(this, (JoinUsNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_ready_to_get_started;
  }

  @Override protected void initComponent(View view) {
    mButtonLogIn = (Button) view.findViewById(R.id.button_login);
    mButtonRegisterAccount = (Button) view.findViewById(R.id.button_create_account);
  }

  @Override protected void setListeners() {
    mButtonLogIn.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mTracker.trackJoinUsButtons(null, true, false, TrackerConstants.CATEGORY_MY_AREA,
            TrackerConstants.ACTION_SSO_MODULE, TrackerConstants.LABEL_GO_TO_LOGIN);
        mPresenter.goToLogin();
      }
    });

    mButtonRegisterAccount.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mTracker.trackJoinUsButtons(REGISTER_TAP_EVENT, false, true,
            TrackerConstants.CATEGORY_MY_AREA, TrackerConstants.ACTION_SSO_MODULE,
            TrackerConstants.LABEL_GO_TO_REGISTER);
        mPresenter.goToRegisterAccount();
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    mButtonLogIn.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_LOGIN_BUTTON));
    mButtonRegisterAccount.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_CREATE_ACCOUNT));
  }
}
