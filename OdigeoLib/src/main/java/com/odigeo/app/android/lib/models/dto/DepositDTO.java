package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class DepositDTO implements Serializable {

  protected BigDecimal amount;
  protected String currency;
  protected String limitDate;

  /**
   * Gets the value of the amount property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getAmount() {
    return amount;
  }

  /**
   * Sets the value of the amount property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setAmount(BigDecimal value) {
    this.amount = value;
  }

  /**
   * Gets the value of the currency property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCurrency() {
    return currency;
  }

  /**
   * Sets the value of the currency property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCurrency(String value) {
    this.currency = value;
  }

  /**
   * Gets the value of the limitDate property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getLimitDate() {
    return limitDate;
  }

  /**
   * Sets the value of the limitDate property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setLimitDate(String value) {
    this.limitDate = value;
  }
}
