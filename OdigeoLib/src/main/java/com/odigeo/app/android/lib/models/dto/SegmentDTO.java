package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author miguel
 */
public class SegmentDTO implements Serializable {

  protected List<Integer> sections;
  protected int carrier;
  protected CabinClassDTO cabinClass;
  protected Long duration;
  protected Integer seats;
  protected Boolean timesUndefined;

  //This attribute is not obtained from the rest service
  private boolean isSelected;

  /**
   * Gets the value of the carrier property.
   *
   * @return possible object is {@link String }
   */
  public int getCarrier() {
    return carrier;
  }

  /**
   * Sets the value of the carrier property.
   *
   * @param value allowed object is {@link String }
   */
  public void setCarrier(int value) {
    this.carrier = value;
  }

  /**
   * Gets the value of the cabinClass property.
   *
   * @return possible object is {@link com.odigeo.app.android.lib.models.CabinClass }
   */
  public CabinClassDTO getCabinClass() {
    return cabinClass;
  }

  /**
   * Sets the value of the cabinClass property.
   *
   * @param value allowed object is {@link com.odigeo.app.android.lib.models.CabinClass }
   */
  public void setCabinClass(CabinClassDTO value) {
    this.cabinClass = value;
  }

  /**
   * Gets the value of the duration property.
   *
   * @return possible object is {@link Long }
   */
  public Long getDuration() {
    return duration;
  }

  /**
   * Sets the value of the duration property.
   *
   * @param value allowed object is {@link Long }
   */
  public void setDuration(Long value) {
    this.duration = value;
  }

  /**
   * Gets the value of the seats property.
   *
   * @return possible object is {@link Integer }
   */
  public Integer getSeats() {
    return seats;
  }

  /**
   * Sets the value of the seats property.
   *
   * @param value allowed object is {@link Integer }
   */
  public void setSeats(Integer value) {
    this.seats = value;
  }

  /**
   * Gets the value of the timesUndefined property.
   *
   * @return possible object is {@link Boolean }
   */
  public Boolean isTimesUndefined() {
    return timesUndefined;
  }

  /**
   * Sets the value of the timesUndefined property.
   *
   * @param value allowed object is {@link Boolean }
   */
  public void setTimesUndefined(Boolean value) {
    this.timesUndefined = value;
  }

  /**
   * @return the sections
   */
  public List<Integer> getSections() {
    return sections;
  }

  /**
   * @param sections the sections to set
   */
  public void setSections(List<Integer> sections) {
    this.sections = sections;
  }

  public boolean isSelected() {
    return isSelected;
  }

  public void setSelected(boolean isSelected) {
    this.isSelected = isSelected;
  }
}
