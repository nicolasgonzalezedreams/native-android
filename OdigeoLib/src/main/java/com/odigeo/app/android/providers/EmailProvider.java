package com.odigeo.app.android.providers;

import android.content.Context;
import com.odigeo.app.android.lib.data.LocalizablesFacade;

import static com.odigeo.app.android.view.constants.OneCMSKeys.CONFIG_FEEDBACK_MAIL;

public class EmailProvider {

  private final Context context;

  public EmailProvider(Context context) {
    this.context = context;
  }

  public String getFeedbackEmail(String... params) {
    return LocalizablesFacade.getString(context, CONFIG_FEEDBACK_MAIL, params).toString();
  }
}
