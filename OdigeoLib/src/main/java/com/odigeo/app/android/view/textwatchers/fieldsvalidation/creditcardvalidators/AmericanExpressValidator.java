package com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators;

import com.odigeo.app.android.view.textwatchers.fieldsvalidation.BaseValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;

public class AmericanExpressValidator extends BaseValidator implements FieldValidator {

  public static final String REGEX_AMEX = "3[47][0-9]{13}";

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_AMEX);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
