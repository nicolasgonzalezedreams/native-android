package com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators;

import com.odigeo.app.android.view.textwatchers.fieldsvalidation.BaseValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;

public class JCBValidator extends BaseValidator implements FieldValidator {

  public static final String REGEX_JCB = "(?:^(21|18)[0-9]{13}|(^3[0-9]{15}))$";

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_JCB);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
