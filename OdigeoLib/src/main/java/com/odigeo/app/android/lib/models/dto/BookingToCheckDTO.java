package com.odigeo.app.android.lib.models.dto;

import java.util.List;

/**
 * Booking to check model
 */
public final class BookingToCheckDTO {

  /**
   * Booking email
   */
  private String email;

  /**
   * List of booking identifiers to check
   */
  private List<Long> bookingIds;

  /**
   * Constructor
   */
  public BookingToCheckDTO() {
  }

  /**
   * Constructor
   *
   * @param email Used how to key to create the object
   * @param bookingIds List of Ids to use with the key.
   */
  public BookingToCheckDTO(String email, List<Long> bookingIds) {
    this.email = email;
    this.bookingIds = bookingIds;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the bookingIds
   */
  public List<Long> getBookingIds() {
    return bookingIds;
  }

  /**
   * @param bookingIds the bookingIds to set
   */
  public void setBookingIds(List<Long> bookingIds) {
    this.bookingIds = bookingIds;
  }
}
