package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.view.BaseCustomWidget;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.geo.City;
import com.odigeo.presenter.TopBriefWidgetPresenter;
import com.odigeo.presenter.contracts.views.TopBriefViewInterface;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TopBriefWidget extends BaseCustomWidget<TopBriefWidgetPresenter>
    implements TopBriefViewInterface {
  public static final int TEXT_SIZE = 20;
  private static final int SEGMENTS_PER_ROW = 3;
  private static final int MARGIN_ACROW = 8;
  private SearchOptions searchOptions;
  private LinearLayout topbriefCitiesContainer;
  private LinearLayout topbriefCitiesContainer2;
  private LinearLayout topbriefDatesContainer;
  private TextView txtNoPassengers;
  private TextView mTvTotalPrice;
  private TextView mTvTotalPriceMessage;
  private ImageView imgPassengers;
  private Fonts fonts;
  private SimpleDateFormat simpleDateFormat;
  private boolean isMembershipApplied = false;

  public TopBriefWidget(Context context) {
    super(context);
  }

  public TopBriefWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public void initComponent() {
    topbriefCitiesContainer = (LinearLayout) findViewById(R.id.topbriefCitiesContainer);
    topbriefCitiesContainer2 = (LinearLayout) findViewById(R.id.topbriefCitiesContainer2);
    topbriefDatesContainer = (LinearLayout) findViewById(R.id.topbriefDatesContainer);
    txtNoPassengers = (TextView) findViewById(R.id.text_AmountPassengers_topbrief);
    imgPassengers = (ImageView) findViewById(R.id.imgPersonCaret);
    mTvTotalPrice = (TextView) findViewById(R.id.tv_topbrief_total_price);
    mTvTotalPriceMessage = (TextView) findViewById(R.id.tv_topbrief_message_price);

    if (!isInEditMode()) {
      simpleDateFormat =
          OdigeoDateUtils.getGmtDateFormat(getResources().getString(R.string.templates__datelong3));

      fonts = Configuration.getInstance().getFonts();
      txtNoPassengers.setTypeface(fonts.getRegular());
    }
  }

  @Override public int getComponentLayout() {
    return R.layout.layout_topbrief;
  }

  @Override public void initOneCMSText() {

  }

  @Override protected TopBriefWidgetPresenter setPresenter() {
    return dependencyInjector.provideTopBriefWidgetPresenter(this);
  }

  private void addData() {
    topbriefCitiesContainer.removeAllViews();
    topbriefDatesContainer.removeAllViews();

    txtNoPassengers.setText(String.valueOf(searchOptions.getTotalPassengers()));

    if (searchOptions.getTotalPassengers() == 1) {
      imgPassengers.setImageResource(R.drawable.search_1pax);
    } else {
      imgPassengers.setImageResource(R.drawable.search_multi_pax);
    }

    if (searchOptions.getTravelType() != TravelType.MULTIDESTINATION) {
      addSegment(searchOptions.getFlightSegments().get(0).getDepartureCity(),
          searchOptions.getFlightSegments().get(0).getArrivalCity(), 0);
    } else {
      int i = 0;
      for (FlightSegment flightSegment : searchOptions.getFlightSegments()) {
        addSegment(flightSegment.getDepartureCity(), flightSegment.getArrivalCity(), i++);
      }
    }

    //Add the first and last date
    topbriefDatesContainer.addView(createTextDate(
        OdigeoDateUtils.createDate(searchOptions.getFlightSegments().get(0).getDate()), true));

    if (searchOptions.getFlightSegments().size() > 1) {
      FlightSegment lastSegment =
          searchOptions.getFlightSegments().get(searchOptions.getFlightSegments().size() - 1);

      topbriefDatesContainer.addView(
          createTextDate(OdigeoDateUtils.createDate(lastSegment.getDate()), false));
    }
  }

  private void addSegment(City departure, City arrival, int segmentIndex) {
    if (departure == null || arrival == null) {
      return;
    }

    LinearLayout containerCities = topbriefCitiesContainer;

    if (segmentIndex >= SEGMENTS_PER_ROW) {
      containerCities = topbriefCitiesContainer2;
    }

    //If it is not the first of the row
    if (segmentIndex % SEGMENTS_PER_ROW != 0) {
      containerCities.addView(createTextArrow("•"));
    }

    String cityDepartureLabel =
        getSearchOptions().getTravelType() == TravelType.MULTIDESTINATION ? departure.getIataCode()
            : departure.getCityName();

    String cityReturnLabel =
        getSearchOptions().getTravelType() == TravelType.MULTIDESTINATION ? arrival.getIataCode()
            : arrival.getCityName();

    containerCities.addView(createTextCity(cityDepartureLabel));

    if (searchOptions.getTravelType() == TravelType.SIMPLE) {
      containerCities.addView(createImageDirection(TravelType.SIMPLE));
    } else if (searchOptions.getTravelType() == TravelType.ROUND) {
      containerCities.addView(createImageDirection(TravelType.ROUND));
    } else {
      containerCities.addView(createImageDirection(TravelType.SIMPLE));
    }

    containerCities.addView(createTextCity(cityReturnLabel));
  }

  private ImageView createImageDirection(TravelType travelType) {
    ImageView imageView = (ImageView) inflate(getContext(), R.layout.layout_topbrief_arrow, null);
    LinearLayout.LayoutParams params =
        new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    params.setMargins(MARGIN_ACROW, 0, MARGIN_ACROW, 0);
    if (travelType == TravelType.ROUND) {
      imageView.setImageResource(R.drawable.search_dobleflecha);
    } else if (travelType == TravelType.SIMPLE) {
      imageView.setImageResource(R.drawable.flecha_topbrierf);
    }
    imageView.setLayoutParams(params);
    return imageView;
  }

  private TextView createTextArrow(String textRow) {
    TextView textView = (TextView) inflate(getContext(), R.layout.layout_topbrief_city, null);
    textView.setText(textRow);
    textView.setTypeface(null, Typeface.BOLD);
    textView.setTextSize(TEXT_SIZE);
    textView.setGravity(Gravity.TOP);
    return textView;
  }

  private TextView createTextCity(String cityName) {
    TextView txtCity = (TextView) inflate(getContext(), R.layout.layout_topbrief_city, null);
    txtCity.setTypeface(fonts.getBold());
    txtCity.setText(HtmlUtils.formatHtml(cityName));
    txtCity.setTextColor(getResources().getColor(R.color.top_brief_font_color_city));
    return txtCity;
  }

  private TextView createTextDate(Date date, boolean isFirst) {
    if (date == null) {
      return new TextView(getContext());
    }

    TextView txtDate = (TextView) inflate(getContext(), R.layout.layout_topbrief_date, null);
    txtDate.setTypeface(fonts.getRegular());
    txtDate.setTextColor(getResources().getColor(R.color.top_brief_font_color_date));
    if (isFirst) {
      txtDate.setText(simpleDateFormat.format(date));
    } else {
      String finalDate = " - " + simpleDateFormat.format(date);
      txtDate.setText(finalDate);
    }

    return txtDate;
  }

  public SearchOptions getSearchOptions() {
    return searchOptions;
  }

  public final void setSearchOptions(@NonNull SearchOptions searchOptions) {
    this.searchOptions = searchOptions;

    addData();
  }

  public final void addData(@NonNull SearchOptions searchOptions) {
    this.searchOptions = searchOptions;
    topbriefCitiesContainer.removeAllViews();
    topbriefDatesContainer.removeAllViews();

    txtNoPassengers.setText(String.valueOf(searchOptions.getTotalPassengers()));

    if (searchOptions.getTotalPassengers() == 1) {
      imgPassengers.setImageResource(R.drawable.search_1pax);
    } else {
      imgPassengers.setImageResource(R.drawable.search_multi_pax);
    }

    if (searchOptions.getTravelType() != TravelType.MULTIDESTINATION) {
      addSegment(searchOptions.getFlightSegments().get(0).getDepartureCity(),
          searchOptions.getFlightSegments().get(0).getArrivalCity(), 0);
    } else {
      int i = 0;
      for (FlightSegment flightSegment : searchOptions.getFlightSegments()) {
        addSegment(flightSegment.getDepartureCity(), flightSegment.getArrivalCity(), i++);
      }
    }

    //Add the first and last date
    topbriefDatesContainer.addView(createTextDate(
        OdigeoDateUtils.createDate(searchOptions.getFlightSegments().get(0).getDate()), true));

    if (searchOptions.getFlightSegments().size() > 1) {
      FlightSegment lastSegment =
          searchOptions.getFlightSegments().get(searchOptions.getFlightSegments().size() - 1);

      topbriefDatesContainer.addView(
          createTextDate(OdigeoDateUtils.createDate(lastSegment.getDate()), false));
    }
  }

  public void updatePrice(Double totalPrice) {
    String subtitle = localizables.getRawString(OneCMSKeys.FULL_PRICE_SUMMARY_HEADER_NEW);
    if (!subtitle.isEmpty() || presenter.shouldShowMembershipLabel()) {
      mTvTotalPrice.setVisibility(VISIBLE);
      mTvTotalPriceMessage.setVisibility(VISIBLE);
      String priceFormatted = LocaleUtils.getLocalizedCurrencyValue(totalPrice,
          Configuration.getInstance().getCurrentMarket().getLocale());
      mTvTotalPrice.setText(priceFormatted);
      presenter.configureTotalPrice();
    }
  }

  public void setMembershipApplied(boolean membershipApplied) {
    isMembershipApplied = membershipApplied;
  }

  @Override public boolean getMembershipApplied() {
    return isMembershipApplied;
  }
  
  @Override public void showMembershipWithDiscount() {
    mTvTotalPriceMessage.setText(
        localizables.getString(OneCMSKeys.PRICING_BREAKDOWN_PREMIUM_DISCOUNT_APPLIED));
    mTvTotalPriceMessage.setTextColor(
        ContextCompat.getColor(getContext(), R.color.semantic_positive));
  }

  @Override public void showMembershipWithNoDiscount() {
    mTvTotalPriceMessage.setText(
        localizables.getString(OneCMSKeys.PRICING_BREAKDOWN_PREMIUM_DISCOUNT_NOT_APPLIED));
    mTvTotalPriceMessage.setTextColor(
        ContextCompat.getColor(getContext(), R.color.secondary_brand_dark));
  }

  @Override public void showFullPrice() {
    mTvTotalPriceMessage.setText(localizables.getString(OneCMSKeys.FULL_PRICE_SUMMARY_HEADER_NEW));
  }
}