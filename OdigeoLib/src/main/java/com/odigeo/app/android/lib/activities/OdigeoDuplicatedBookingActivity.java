package com.odigeo.app.android.lib.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.ConfirmationHeader;
import com.odigeo.app.android.lib.ui.widgets.ConfirmationInfoItem2Texts;
import com.odigeo.app.android.lib.ui.widgets.base.PressableWidget;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.view.helpers.PermissionsHelper;
import com.odigeo.data.entity.shoppingCart.BookingStatus;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.squareup.otto.Subscribe;
import java.util.List;

/**
 * Created by ma.martinez on 30/01/15.
 *
 * @author Irving
 * @since 24/02/15
 */
public class OdigeoDuplicatedBookingActivity extends OdigeoForegroundBaseActivity
    implements View.OnClickListener {

  private TextView passenger;
  private TextView flight;
  private TextView home;
  private LinearLayout container;

  private OdigeoApp odigeoApp;
  private List<String> mClientBookingPnrs;
  private String mPhone;

  @Override public void setTitle(CharSequence title) {
    String titleActivity =
        LocalizablesFacade.getString(this, "duplicatebooking_information_title").toString();
    super.setTitle(titleActivity);
  }

  @Override protected final void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_duplicated_booking);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setTitle(
        LocalizablesFacade.getString(this, "duplicatebooking_information_title").toString());

    odigeoApp = (OdigeoApp) getApplication();

    OdigeoSession odigeoSession = odigeoApp.getOdigeoSession();

    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {

      passenger = (TextView) findViewById(R.id.text_data_passenger);
      flight = (TextView) findViewById(R.id.text_data_flights);
      home = (TextView) findViewById(R.id.text_home_app);
      container = (LinearLayout) findViewById(R.id.layout_ids_container_duplicated_activity);
      TextView description = (TextView) findViewById(R.id.text_description);
      ConfirmationHeader confirmationHeader =
          (ConfirmationHeader) findViewById(R.id.duplicated_booking_header_confirmation);

      Typeface regularTypeface = Configuration.getInstance().getFonts().getRegular();
      passenger.setTypeface(regularTypeface);
      flight.setTypeface(regularTypeface);
      home.setTypeface(regularTypeface);
      description.setTypeface(regularTypeface);

      BookingStatus status =
          (BookingStatus) getIntent().getSerializableExtra(Constants.EXTRA_BOOKING_STATUS);

      mClientBookingPnrs = getIntent().getStringArrayListExtra(Constants.EXTRA_BOOKING_PNRS);
      setBookingReference(mClientBookingPnrs);

      if (status == BookingStatus.CONTRACT) {
        description.setText(
            LocalizablesFacade.getString(this, "duplicatebooking_whatsnext_subtitle_confirmed")
                .toString());
      } else {
        description.setText(
            LocalizablesFacade.getString(this, "duplicatebooking_whatsnext_subtitle_pending")
                .toString());
      }

      confirmationHeader.setDuplicatedStatus();
      createListeners();
    } else {
      Util.sendToHome(this, odigeoApp);
    }
  }

  public final void setBookingReference(List<String> referenceIds) {

    PressableWidget lastForm = null;
    for (String reference : referenceIds) {
      ConfirmationInfoItem2Texts item =
          new ConfirmationInfoItem2Texts(this.getApplicationContext());
      item.setText(reference);
      item.setIdImage(R.drawable.confirmation_update);
      CharSequence secondText =
          LocalizablesFacade.getString(this, "confirmation_whatsnext_bookingid_placeholder");
      item.setSecondText(secondText);
      lastForm = item;
      container.addView(item);
    }

    if (lastForm != null) {
      lastForm.setLastInForm(true);
    }
  }

  private void createListeners() {
    passenger.setOnClickListener(this);
    flight.setOnClickListener(this);
    home.setOnClickListener(this);
  }

  private void launchActivity(Class classOpen) {
    Intent intent = new Intent(OdigeoDuplicatedBookingActivity.this, classOpen);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  @Override public final void onClick(View v) {

    int idView = v.getId();
    if (idView == passenger.getId()) {
      finish();
    } else if (idView == flight.getId()) {
      launchActivity(odigeoApp.getSearchFlightsActivityClass());
    } else if (idView == home.getId()) {
      launchActivity(odigeoApp.getHomeActivity());
    }
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      this.finish();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getApplication());
  }

  public void setPhoneCallNumber(String phone) {
    mPhone = phone;
  }

  @Override public void onRequestPermissionsResult(int requestCode, String[] permissions,
      int[] grantResults) {
    switch (requestCode) {
      case PermissionsHelper.PHONE_REQUEST_CODE:
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT, TrackerConstants.LABEL_PHONE_ACCESS_ALLOW);
          Intent callIntent = new Intent(Intent.ACTION_CALL);
          callIntent.setData(Uri.parse("tel:" + mPhone));
          startActivity(callIntent);
        } else {
          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT, TrackerConstants.LABEL_PHONE_ACCESS_DENY);
        }
    }
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }
}
