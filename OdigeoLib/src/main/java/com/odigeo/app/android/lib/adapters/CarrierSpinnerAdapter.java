package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import java.util.List;

/**
 * Parametrized adapter to be used with the {@link com.odigeo.app.android.lib.ui.widgets.base.OdigeoSpinner}.
 *
 * @author M. Sc. Javier Silva Pérez
 * @version 2.0
 * @since 18/05/15
 */
@Deprecated public class CarrierSpinnerAdapter extends ArrayAdapter<CarrierDTO> {

  OdigeoImageLoader mImageLoader;

  public CarrierSpinnerAdapter(Context context, List<CarrierDTO> objects) {

    super(context, R.layout.layout_spinner_simple_text, objects);

    mImageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();
  }

  @Override public final View getView(int position, View convertView, ViewGroup parent) {
    return inflateView(R.layout.layout_spinner_text_with_right_image, position, convertView,
        parent);
  }

  @Override public final View getDropDownView(int position, View convertView, ViewGroup parent) {
    return inflateView(R.layout.layout_spinner_drop_text_with_left_image, position, convertView,
        parent);
  }

  /**
   * Method to the common inflate of the items.
   *
   * @param position Position to be inflated.
   * @param convertView View corresponding to the position.
   * @param parent Parent of the item to be inflated.
   * @return A non empty and set view.
   */
  private View inflateView(@LayoutRes int layout, int position, @Nullable View convertView,
      @NonNull ViewGroup parent) {
    CarrierDTO item = getItem(position);
    ViewHolder holder;
    if (convertView == null) {
      convertView = LayoutInflater.from(getContext()).inflate(layout, parent, false);
      TextView textView = (TextView) convertView.findViewById(R.id.text_spinner_drop);
      ImageView imageView = (ImageView) convertView.findViewById(R.id.image_spinner_drop);
      holder = new ViewHolder(textView, imageView);
      convertView.setTag(holder);
    } else {
      holder = ((ViewHolder) convertView.getTag());
    }

    holder.textView.setText(item.getShownText());

    String url = ViewUtils.getCarrierLogo(getItem(position).getCode());
    mImageLoader.load(holder.imageView, url, R.drawable.icon);

    return convertView;
  }

  /**
   * Class to retain the instances of the views in a item.
   */
  private static class ViewHolder {
    final TextView textView;
    final ImageView imageView;

    /**
     * Constructor
     *
     * @param view View from gonna be linked the views.
     */
    ViewHolder(View view, ImageView imageView) {
      textView = ((TextView) view);
      this.imageView = imageView;
    }
  }
}
