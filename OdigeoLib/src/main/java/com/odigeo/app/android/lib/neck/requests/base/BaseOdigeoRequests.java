package com.odigeo.app.android.lib.neck.requests.base;

import android.util.Log;
import android.util.Pair;
import com.globant.roboneck.common.NeckCookie;
import com.globant.roboneck.requests.BaseNeckHttpRequest;
import com.globant.roboneck.requests.BaseNeckRequestException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.neck.errors.NeckNotInternetError;
import com.odigeo.app.android.lib.utils.BaggageConditionsFieldNamingStrategy;
import com.odigeo.dataodigeo.net.helper.semaphore.UIAutomationSemaphore;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ManuelOrtiz on 17/09/2014.
 */
//T and R are ResponseObject Type,
//S is RequestObjectModel Type

/**
 * Base request for all the services used in odigeo
 *
 * @param <R> Response model class
 * @param <S> Request class
 * @author Manuel Ortiz
 * @since 09/17/2014
 */
public abstract class BaseOdigeoRequests<T, R, S> extends BaseNeckHttpRequest<T, R> {
  private static final int ERRORCODE = 100;
  private static final String ACCEPT_BASE_MSL = "application/vnd.com.odigeo.msl.";
  private static final String ACCEPT_MSL_V3 = "v3";
  private static final String ACCEPT_TYPE = "json";
  private static final String ACCEPT_ENCODE = "charset=utf-8";
  private final List<NeckCookie> lstCookies;
  private final S requestModel;
  private final Class<T> clazz;
  private final OdigeoSession odigeoSession;
  private String deviceId;

  public BaseOdigeoRequests(Class<T> clazz, S requestModel, List<NeckCookie> cookies,
      OdigeoSession odigeoSession, String deviceId) {
    super(clazz);

    this.clazz = clazz;
    this.requestModel = requestModel;
    this.lstCookies = cookies;
    this.odigeoSession = odigeoSession;
    this.deviceId = deviceId;

    UIAutomationSemaphore.getInstance().idleUp();
  }

  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  @Override protected List<NeckCookie> getCookies() {
    return lstCookies;
  }

  @Override public Object getCachekey() {
    return getUrl();
  }

  @Override protected Map<String, String> getHeaders() {
    Map<String, String> map = new HashMap<String, String>();

    map.put("Content-Type", "application/json");
    map.put("Device-ID", this.deviceId);
    map.put("Accept-Encoding", "gzip,deflate,sdch");
    map.put("User-Agent", Configuration.getInstance().getUserAgent());
    map.put("Accept", ACCEPT_BASE_MSL + ACCEPT_MSL_V3 + "+" + ACCEPT_TYPE + ";" + ACCEPT_ENCODE);
    return map;
  }

  @Override protected String getBody() {
    //Generate JSON to string
    Gson gson = new Gson();
    String strRequest = gson.toJson(requestModel);

    Log.i(Constants.TAG_LOG, strRequest);

    return strRequest;
  }

  @Override protected R processContent(String responseBody) {

    Log.d(Constants.TAG_LOG, "MSL response:\n" + responseBody);

    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.setVersion(Constants.DTOVersion.CURRENT);
    gsonBuilder.setFieldNamingStrategy(
        new BaggageConditionsFieldNamingStrategy(Constants.DTOVersion.CURRENT));

    Gson gson = gsonBuilder.create();

    R gsonResponse = null;

    if (odigeoSession != null) {
      odigeoSession.setSessionBookingCookies(getCookies());
    }

    try {
      gsonResponse = (R) gson.fromJson(responseBody, clazz);
    } catch (Exception e) {
      Log.e(Constants.TAG_LOG, e.getMessage());
    }

    UIAutomationSemaphore.getInstance().idleDown();

    return gsonResponse;
  }

  @Override protected boolean isLogicError(R response) {
    return response == null;
  }

  @Override protected T getRequestModel(R response) {
    return (T) response;
  }

  @Override protected BaseNeckRequestException.Error processError(int httpStatus, R response,
      String responseBody) {

    return new NeckNotInternetError("Can't connect to the Rest Service", ERRORCODE, 1);
  }

  @Override protected List<Pair<String, String>> getQueryParameters() {
    return null;
  }

  @Override public long getCacheExpirationTime() {
    return DurationInMillis.ALWAYS_EXPIRED;
  }

  @Override protected Method getMethod() {
    return Method.POST;
  }
}
