package com.odigeo.app.android.view.animations;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class AnimationUtil {

  public static void fadeOut(final View view) {

    int shortAnimTime = view.getContext().getResources().getInteger(android.R.integer
        .config_shortAnimTime);

    view.setVisibility(GONE);
    view.animate().setDuration(shortAnimTime).alpha(0).setListener(new AnimatorListenerAdapter() {
      @Override public void onAnimationEnd(Animator animation) {
        view.setVisibility(GONE);
      }
    });
  }

  public static void fadeIn(final View view) {

    int shortAnimTime = view.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime);

    view.setVisibility(VISIBLE);
    view.animate().setDuration(shortAnimTime).alpha(1).setListener(new AnimatorListenerAdapter() {
      @Override public void onAnimationEnd(Animator animation) {
        view.setVisibility(VISIBLE);
      }
    });
  }
}
