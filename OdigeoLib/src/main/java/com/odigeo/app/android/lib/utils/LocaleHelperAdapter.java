package com.odigeo.app.android.lib.utils;

import android.support.annotation.NonNull;
import java.util.Locale;

public class LocaleHelperAdapter implements LocaleHelper {

  @Override public Locale getCurrentLocale() {
    return LocaleUtils.getCurrentLocale();
  }

  @Override public String getCurrentLanguageIso() {
    return LocaleUtils.getCurrentLanguageIso();
  }

  @Override public String getCurrentIsoLocale() {
    return LocaleUtils.getCurrentIsoLocale();
  }

  @Override public String localeToString(Locale locale) {
    return LocaleUtils.localeToString(locale);
  }

  @Override public String getCurrencySymbol(String locale) {
    return LocaleUtils.getCurrencySymbol();
  }

  @Override public String getCurrencySymbol() {
    return LocaleUtils.getCurrencySymbol();
  }

  @Override public String getCurrencyCode(String locale) {
    return LocaleUtils.getCurrencyCode();
  }

  @Override public String getCurrencyCode() {
    return LocaleUtils.getCurrencyCode();
  }

  @Override public String getLocalizedCurrencyValue(double value) {
    return LocaleUtils.getLocalizedCurrencyValue(value);
  }

  @Override
  public String getLocalizedCurrencyValue(double value, Locale locale, boolean validateDecimals) {
    return LocaleUtils.getLocalizedCurrencyValue(value, locale, validateDecimals);
  }

  @Override public String getLocalizedNumber(int value, String locale) {
    return LocaleUtils.getLocalizedNumber(value, locale);
  }

  @Override public String getLocalizedNumber(int value) {
    return LocaleUtils.getLocalizedNumber(value);
  }

  @Override public String getLocalizedNumber(double value, String locale) {
    return LocaleUtils.getLocalizedNumber(value, locale);
  }

  @Override public String getLocalizedNumber(double value) {
    return LocaleUtils.getLocalizedNumber(value);
  }

  @Override public int getLocalizedSearchLength() {
    return LocaleUtils.getLocalizedSearchLength();
  }

  @Override public boolean isJapaneseCharacter(String query) {
    return LocaleUtils.isJapaneseCharacter(query);
  }

  @Override public Locale strToLocale(String strLocale) {
    return LocaleUtils.strToLocale(strLocale);
  }

  @NonNull @Override public String getLocalizedCurrencyFeeValue(Double fee) {
    return LocaleUtils.getLocalizedCurrencyFeeValue(fee);
  }
}
