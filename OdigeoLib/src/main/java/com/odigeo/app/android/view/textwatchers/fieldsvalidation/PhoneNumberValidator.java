package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

public class PhoneNumberValidator extends BaseValidator implements FieldValidator {

  private static final String REGEX_PHONE = "[0-9]{7,15}";

  public PhoneNumberValidator() {
    super();
  }

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_PHONE);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
