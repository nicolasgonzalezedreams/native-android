package com.odigeo.app.android.view.textwatchers.OnFocusChange;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.tracker.TrackerConstants;

public class NameOnCardFocusChange extends BaseOnFocusChange {

  private TextInputLayout mTilNameOnCard;
  private TrackerControllerInterface mTracker;
  private boolean mThereWasFocusAtLeastOnce = false;

  public NameOnCardFocusChange(Context context, TextInputLayout textInputLayout,
      FieldValidator fieldValidator, String oneCMSKey, TrackerControllerInterface tracker) {
    super(context, textInputLayout, fieldValidator, oneCMSKey);
    mTilNameOnCard = textInputLayout;
    mTracker = tracker;
  }

  @Override public void onFocusChange(View v, boolean hasFocus) {
    super.onFocusChange(v, hasFocus);
    boolean isFieldCorrect = super.getIsFieldCorrect();
    if (!isFieldCorrect && mTilNameOnCard.getEditText() != null) {
      String fieldInformation = mTilNameOnCard.getEditText().getText().toString();
      if (fieldInformation.isEmpty()) {
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
            TrackerConstants.ACTION_PAYMENT_DETAILS_ERROR,
            TrackerConstants.LABEL_CARD_HOLDER_MISSING);
      } else {
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
            TrackerConstants.ACTION_PAYMENT_DETAILS_ERROR,
            TrackerConstants.LABEL_CARD_HOLDER_INVALID);
      }
    }
    if (hasFocus) mThereWasFocusAtLeastOnce = true;
  }

  public boolean getThereWasFocusAtLeastOnce() {
    return mThereWasFocusAtLeastOnce;
  }
}
