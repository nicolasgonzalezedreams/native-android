package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.presenter.model.QAModeUrlModel;
import java.util.List;

public class QAModeAdapter extends BaseAdapter {

  private List<QAModeUrlModel> urlList;
  private LayoutInflater layoutInflater;

  public QAModeAdapter(Context context, List<QAModeUrlModel> urlList) {
    this.urlList = urlList;
    this.layoutInflater = LayoutInflater.from(context);
  }

  @Override public int getCount() {
    return urlList.size();
  }

  @Override public QAModeUrlModel getItem(int position) {
    return urlList.get(position);
  }

  @Override public long getItemId(int position) {
    return position;
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {
    final View rowView;
    if (convertView == null) {
      rowView = layoutInflater.inflate(R.layout.row_qa_mode_list, parent, false);
    } else {
      rowView = convertView;
    }

    final QAModeUrlModel item = urlList.get(position);
    final TextView txtURL = (TextView) rowView.findViewById(R.id.qa_mode_url);
    txtURL.setText(item.getUrl());

    if (item.isSelected()) {
      ((ListView) parent).setItemChecked(position, true);
    }

    return rowView;
  }

  public List<QAModeUrlModel> getList() {
    return urlList;
  }

  public void updateList(List<QAModeUrlModel> urls) {
    urlList = urls;
    notifyDataSetChanged();
  }
}
