package com.odigeo.app.android;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.widget.ImageView;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.odigeo.DependencyInjector;
import com.odigeo.app.android.alarms.BookingStatusAlarmManager;
import com.odigeo.app.android.helper.CalendarHelper;
import com.odigeo.app.android.helper.CrossCarUrlHandler;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.database.LocalizablesDatabase;
import com.odigeo.app.android.lib.data.wrapper.LocalizablesFacadeWrapper;
import com.odigeo.app.android.lib.mappers.StoredSearchOptionsMapper;
import com.odigeo.app.android.lib.receivers.OneCMSUpdaterReceiver;
import com.odigeo.app.android.lib.utils.LocaleHelper;
import com.odigeo.app.android.lib.utils.LocaleHelperAdapter;
import com.odigeo.app.android.lib.utils.LocalizablesUtils;
import com.odigeo.app.android.notification.OdigeoNotificationManager;
import com.odigeo.app.android.providers.AndroidAssetsProvider;
import com.odigeo.app.android.providers.EmailProvider;
import com.odigeo.app.android.providers.FontProvider;
import com.odigeo.app.android.providers.MarketProvider;
import com.odigeo.app.android.view.animations.UpdateCarouselDotsAnimation;
import com.odigeo.app.android.view.helpers.DateHelper;
import com.odigeo.app.android.view.helpers.OdigeoDurationFormatter;
import com.odigeo.app.android.view.imageutils.BookingImageUtil;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.RequestValidationHandler;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.ValidationMapFactory.CreditCardValidationCommandMapFactory;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.ValidationMapFactory.IdentificationValidationCommandMapFactory;
import com.odigeo.builder.CardFactory;
import com.odigeo.builder.SectionCardBuilder;
import com.odigeo.data.ab.RemoteConfigControllerInterface;
import com.odigeo.data.db.dao.BaggageDBDAOInterface;
import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.db.dao.BuyerDBDAOInterface;
import com.odigeo.data.db.dao.CarrierDBDAOInterface;
import com.odigeo.data.db.dao.CityDBDAOInterface;
import com.odigeo.data.db.dao.FlightStatsDBDAOInterface;
import com.odigeo.data.db.dao.GuideDBDAOInterface;
import com.odigeo.data.db.dao.InsuranceDBDAOInterface;
import com.odigeo.data.db.dao.ItineraryBookingDBDAOInterface;
import com.odigeo.data.db.dao.LocationBookingDBDAOInterface;
import com.odigeo.data.db.dao.MembershipDBDAOInterface;
import com.odigeo.data.db.dao.SearchSegmentDBDAOInterface;
import com.odigeo.data.db.dao.SectionDBDAOInterface;
import com.odigeo.data.db.dao.SegmentDBDAOInterface;
import com.odigeo.data.db.dao.StoredSearchDBDAOInterface;
import com.odigeo.data.db.dao.TravellerDBDAOInterface;
import com.odigeo.data.db.dao.UserAddressDBDAOInterface;
import com.odigeo.data.db.dao.UserDBDAOInterface;
import com.odigeo.data.db.dao.UserFrequentFlyerDBDAOInterface;
import com.odigeo.data.db.dao.UserIdentificationDBDAOInterface;
import com.odigeo.data.db.dao.UserProfileDBDAOInterface;
import com.odigeo.data.db.dao.UserTravellerDBDAOInterface;
import com.odigeo.data.db.helper.BookingsHandlerInterface;
import com.odigeo.data.db.helper.CitiesHandlerInterface;
import com.odigeo.data.db.helper.CountriesDbHelperInterface;
import com.odigeo.data.db.helper.MembershipHandlerInterface;
import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.db.helper.TravellersHandlerInterface;
import com.odigeo.data.db.helper.UserCreateOrUpdateHandlerInterface;
import com.odigeo.data.download.DownloadController;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.data.localizable.OneCMSAlarmsController;
import com.odigeo.data.net.controllers.AddPassengerNetControllerInterface;
import com.odigeo.data.net.controllers.AddProductsNetControllerInterface;
import com.odigeo.data.net.controllers.ArrivalGuideNetControllerInterface;
import com.odigeo.data.net.controllers.AutoCompleteNetControllerInterface;
import com.odigeo.data.net.controllers.BinCheckNetControllerInterface;
import com.odigeo.data.net.controllers.BookingNetControllerInterface;
import com.odigeo.data.net.controllers.CarrouselNetControllerInterface;
import com.odigeo.data.net.controllers.ConfirmBookingNetControllerInterface;
import com.odigeo.data.net.controllers.CountryNetControllerInterface;
import com.odigeo.data.net.controllers.LocalizablesNetController;
import com.odigeo.data.net.controllers.NotificationNetControllerInterface;
import com.odigeo.data.net.controllers.RemoveProductsNetControllerInterface;
import com.odigeo.data.net.controllers.ResumeBookingNetController;
import com.odigeo.data.net.controllers.SavePaymentMethodNetController;
import com.odigeo.data.net.controllers.SearchesNetControllerInterface;
import com.odigeo.data.net.controllers.SendMailNetControllerInterface;
import com.odigeo.data.net.controllers.ShoppingCartNetControllerInterface;
import com.odigeo.data.net.controllers.UserNetControllerInterface;
import com.odigeo.data.net.controllers.VisitUserNetController;
import com.odigeo.data.net.controllers.VisitsNetController;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.preferences.CarouselManagerInterface;
import com.odigeo.data.preferences.PackageController;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.repositories.CrossSellingRepository;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.data.tracker.CustomDimensionFormatter;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.data.tracker.TuneTrackerInterface;
import com.odigeo.data.trustdefender.TrustDefenderController;
import com.odigeo.dataodigeo.alarms.OneCMSAlarmsControllerImpl;
import com.odigeo.dataodigeo.db.dao.BaggageDBDAO;
import com.odigeo.dataodigeo.db.dao.BookingDBDAO;
import com.odigeo.dataodigeo.db.dao.BuyerDBDAO;
import com.odigeo.dataodigeo.db.dao.CarrierDBDAO;
import com.odigeo.dataodigeo.db.dao.CityDBDAO;
import com.odigeo.dataodigeo.db.dao.FlightStatsDBDAO;
import com.odigeo.dataodigeo.db.dao.GuideDBDAO;
import com.odigeo.dataodigeo.db.dao.InsuranceDBDAO;
import com.odigeo.dataodigeo.db.dao.ItineraryBookingDBDAO;
import com.odigeo.dataodigeo.db.dao.LocationBookingDBDAO;
import com.odigeo.dataodigeo.db.dao.MembershipDBDAO;
import com.odigeo.dataodigeo.db.dao.SearchSegmentDBDAO;
import com.odigeo.dataodigeo.db.dao.SectionDBDAO;
import com.odigeo.dataodigeo.db.dao.SegmentDBDAO;
import com.odigeo.dataodigeo.db.dao.StoredSearchDBDAO;
import com.odigeo.dataodigeo.db.dao.TravellerDBDAO;
import com.odigeo.dataodigeo.db.dao.UserAddressDBDAO;
import com.odigeo.dataodigeo.db.dao.UserDBDAO;
import com.odigeo.dataodigeo.db.dao.UserFrequentFlyerDBDAO;
import com.odigeo.dataodigeo.db.dao.UserIdentificationDBDAO;
import com.odigeo.dataodigeo.db.dao.UserProfileDBDAO;
import com.odigeo.dataodigeo.db.dao.UserTravellerDBDAO;
import com.odigeo.dataodigeo.db.helper.BookingsHandler;
import com.odigeo.dataodigeo.db.helper.CitiesHandler;
import com.odigeo.dataodigeo.db.helper.CountriesDbHelper;
import com.odigeo.dataodigeo.db.helper.MembershipHandler;
import com.odigeo.dataodigeo.db.helper.SearchesHandler;
import com.odigeo.dataodigeo.db.helper.TravellersHandler;
import com.odigeo.dataodigeo.db.helper.UserCreateOrUpdateHandler;
import com.odigeo.dataodigeo.download.DownloadControllerImpl;
import com.odigeo.dataodigeo.firebase.remoteconfig.FirebaseAnalyticsController;
import com.odigeo.dataodigeo.firebase.remoteconfig.FirebaseRemoteConfigController;
import com.odigeo.dataodigeo.location.LocationController;
import com.odigeo.dataodigeo.net.NetTool;
import com.odigeo.dataodigeo.net.controllers.AddPassengerNetController;
import com.odigeo.dataodigeo.net.controllers.AddProductsNetController;
import com.odigeo.dataodigeo.net.controllers.ArrivalGuideNetController;
import com.odigeo.dataodigeo.net.controllers.AuthToken;
import com.odigeo.dataodigeo.net.controllers.AutoCompleteNetController;
import com.odigeo.dataodigeo.net.controllers.BookingNetController;
import com.odigeo.dataodigeo.net.controllers.CarouselNetController;
import com.odigeo.dataodigeo.net.controllers.CheckBinController;
import com.odigeo.dataodigeo.net.controllers.ConfirmBookingNetController;
import com.odigeo.dataodigeo.net.controllers.CountryNetController;
import com.odigeo.dataodigeo.net.controllers.FacebookController;
import com.odigeo.dataodigeo.net.controllers.GooglePlusController;
import com.odigeo.dataodigeo.net.controllers.LocalizablesNetControllerImpl;
import com.odigeo.dataodigeo.net.controllers.NotificationNetController;
import com.odigeo.dataodigeo.net.controllers.RemoveProductsNetController;
import com.odigeo.dataodigeo.net.controllers.ResumeBookingNetControllerImpl;
import com.odigeo.dataodigeo.net.controllers.SavePaymentMethodNetControllerImpl;
import com.odigeo.dataodigeo.net.controllers.SearchesNetController;
import com.odigeo.dataodigeo.net.controllers.SendMailNetController;
import com.odigeo.dataodigeo.net.controllers.ShoppingCartNetController;
import com.odigeo.dataodigeo.net.controllers.TokenController;
import com.odigeo.dataodigeo.net.controllers.UserNetController;
import com.odigeo.dataodigeo.net.controllers.VisitUserNetControllerImpl;
import com.odigeo.dataodigeo.net.controllers.VisitsNetControllerImpl;
import com.odigeo.dataodigeo.net.helper.CustomRequestQueue;
import com.odigeo.dataodigeo.net.helper.DomainHelper;
import com.odigeo.dataodigeo.net.helper.HeaderHelper;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import com.odigeo.dataodigeo.net.helper.image.glide.GlideImageLoader;
import com.odigeo.dataodigeo.net.mapper.AddPassengerNetJSONBuilder;
import com.odigeo.dataodigeo.preferences.CarouselManager;
import com.odigeo.dataodigeo.preferences.PackageControllerImpl;
import com.odigeo.dataodigeo.preferences.PreferencesController;
import com.odigeo.dataodigeo.provider.FileProviderImpl;
import com.odigeo.dataodigeo.repositories.xsell.CrossSellingDataRepository;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.dataodigeo.session.SessionControllerImpl;
import com.odigeo.dataodigeo.tracker.AnalyticsCustomDimensionFormatter;
import com.odigeo.dataodigeo.tracker.CrashlyticsControllerImpl;
import com.odigeo.dataodigeo.tracker.TrackerController;
import com.odigeo.dataodigeo.tracker.TuneTracker;
import com.odigeo.dataodigeo.trustdefender.TrustDefenderControllerImpl;
import com.odigeo.dataodigeo.trustdefender.TrustDefenderHelper;
import com.odigeo.dataodigeo.welcome.WelcomeProvider;
import com.odigeo.helper.ABTestHelper;
import com.odigeo.helper.CrashlyticsUtil;
import com.odigeo.helper.CrossCarUrlHandlerInterface;
import com.odigeo.interactors.CreateShoppingCartInteractor;
import com.odigeo.interactors.DeleteBookingsInteractor;
import com.odigeo.interactors.LogoutInteractor;
import com.odigeo.interactors.managers.BookingStatusAlarmManagerInterface;
import com.odigeo.interactors.notification.OdigeoNotificationManagerInterface;
import com.odigeo.interactors.provider.AssetsProvider;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.interactors.provider.MyTripsProvider;
import com.odigeo.presenter.BaggageCollectionItemPresenter;
import com.odigeo.presenter.BaggageCollectionPresenter;
import com.odigeo.presenter.SocialLoginPresenter;
import com.odigeo.presenter.contracts.views.BaggageCollection;
import com.odigeo.presenter.contracts.views.BaggageCollectionItem;
import com.odigeo.presenter.listeners.LocalizablesUtilsListener;
import com.odigeo.provider.FileProvider;
import com.odigeo.tools.CalendarHelperInterface;
import com.odigeo.tools.ConditionRulesHelper;
import com.odigeo.tools.CreditCardRequestToStoreCreditCardRequestMapper;
import com.odigeo.tools.DurationFormatter;
import com.threatmetrix.TrustDefender.TrustDefender;
import java.io.IOException;

public class AndroidDependencyInjector extends DependencyInjector {

  protected static Context mContext;
  private static AndroidDependencyInjector mInstance;
  private static Application mApplication;
  private CustomRequestQueue mRequestQueue;
  private OdigeoImageLoader<ImageView> mOdigeoImageLoader;
  private TokenController tokenController;
  private SessionController sessionController;
  private DurationFormatter durationFormatter;
  private AssetsProvider assetsProvider;
  private CrossSellingRepository crossSellingRepository;
  private FileProvider fileProvider;

  /**
   * @return the instance of the injector
   * @deprecated Application class has a instance of the inyector
   * Please use OdigeoApp.getDependencyInjector
   */
  @Deprecated public static AndroidDependencyInjector getInstance() {
    if (mInstance == null) {
      mInstance = new AndroidDependencyInjector();
    }
    return mInstance;
  }

  public static void setApplication(Application application) {
    mApplication = application;
    mContext = application.getBaseContext();
  }

  public RequestHelper provideRequestHelper() {
    return new RequestHelper(provideRequestQueue());
  }

  public CustomRequestQueue provideRequestQueue() {
    if (mRequestQueue == null) {
      mRequestQueue = CustomRequestQueue.createCustomRequestQueue(mContext);
      mRequestQueue.start();
    }
    return mRequestQueue;
  }

  @Override public CarouselManagerInterface provideCarouselManager() {
    return new CarouselManager(mContext, providePreferencesController());
  }

  @Override public NetTool provideNetTool() {
    return new NetTool(mContext);
  }

  @Override public CarrouselNetControllerInterface provideCarouselNetController() {
    return new CarouselNetController(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper());
  }

  @Override public UserNetControllerInterface provideUserNetController() {
    return new UserNetController(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideSessionController(), provideTokenController());
  }

  @Override public NotificationNetControllerInterface provideNotificationsNetController() {
    return new NotificationNetController(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideSessionController(), provideTokenController());
  }

  @Override public BookingNetControllerInterface provideBookingNetController() {
    return new BookingNetController(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideSessionController(), provideTokenController());
  }

  @Override public CountryNetControllerInterface provideCountryNetController() {
    return new CountryNetController(provideRequestQueue(), provideDomainHelper(),
        provideHeaderHelper());
  }

  @Override public SavePaymentMethodNetController provideSavePaymentMethodNetController() {
    return new SavePaymentMethodNetControllerImpl(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideTokenController(), provideGsonWithNulls(),
        provideSessionController());
  }

  @Override
  protected CreditCardRequestToStoreCreditCardRequestMapper provideCreditCardRequestToStoreCreditCardRequestMapper() {
    return new CreditCardRequestToStoreCreditCardRequestMapper();
  }

  @Override public TrustDefenderController provideTrustDefenderController() {
    return new TrustDefenderControllerImpl(mContext, provideCrashlyticsController(),
        new TrustDefenderHelper(TrustDefender.getInstance()), Constants.isTestEnvironment);
  }

  @Override public LocalizablesNetController provideLocalizablesNetController() {
    return new LocalizablesNetControllerImpl(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideCrashlyticsController());
  }

  @Override public LocalizablesUtilsListener provideLocalizablesListener() {
    return new LocalizablesUtils(mContext);
  }

  @Override public OneCMSAlarmsController provideOneCMSAlarmsController() {
    return new OneCMSAlarmsControllerImpl(mContext, OneCMSUpdaterReceiver.class,
        provideCrashlyticsController());
  }

  public DomainHelperInterface provideDomainHelper() {
    return DomainHelper.newInstance(Constants.domain);
  }

  public HeaderHelperInterface provideHeaderHelper() {
    HeaderHelper headerHelper = HeaderHelper.newInstance(mContext);
    headerHelper.setmWebSite(provideMarketProvider().getWebsite());
    headerHelper.setmLanguage(provideMarketProvider().getLanguage());
    headerHelper.setLocale(provideMarketProvider().getLocale());
    headerHelper.setmBrand(provideMarketProvider().getBrandKey());
    return headerHelper;
  }

  @Override public SearchesNetControllerInterface provideSearchesNetController() {
    return new SearchesNetController(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideTokenController());
  }

  @Override public UserCreateOrUpdateHandlerInterface provideUserCreateOrUpdateDBHandler() {
    return new UserCreateOrUpdateHandler(provideUserDBDAO(), provideUserTravellersDBDAO(),
        provideUserProfileDBDao(), provideUserAddressDBDao(), provideUserFrequentFlyerDBDao(),
        provideUserIdentificationDBDao(), provideTravellersHandler(), provideStoredSearchesDBDao(),
        provideSearchSegmentDBDao(), provideMembershipDBDao(), provideCrashlyticsController());
  }

  @Override public SessionController provideSessionController() {
    if (sessionController == null) {
      sessionController = new SessionControllerImpl(mContext, providePreferencesController());
    }
    return sessionController;
  }

  @Override public PreferencesControllerInterface providePreferencesController() {
    return new PreferencesController(mContext, provideDomainHelper(), provideMarketProvider());
  }

  @Override public UserDBDAOInterface provideUserDBDAO() {
    return new UserDBDAO(mContext);
  }

  @Override public DownloadController provideDownloadController() {
    return new DownloadControllerImpl(mContext);
  }

  @Override public BookingDBDAOInterface provideBookingDBDAO() {
    return new BookingDBDAO(mContext);
  }

  @Override public BuyerDBDAOInterface provideBuyerDBDAO() {
    return new BuyerDBDAO(mContext);
  }

  @Override public TravellerDBDAOInterface provideTravellerDBDAO() {
    return new TravellerDBDAO(mContext);
  }

  @Override public BaggageDBDAOInterface provideBaggageDBDAO() {
    return new BaggageDBDAO(mContext);
  }

  @Override public SegmentDBDAOInterface provideSegmentDBDAO() {
    return new SegmentDBDAO(mContext);
  }

  @Override public CarrierDBDAOInterface provideCarrierDBDAO() {
    return new CarrierDBDAO(mContext);
  }

  @Override public SectionDBDAOInterface provideSectionDBDAO() {
    return new SectionDBDAO(mContext);
  }

  @Override public ItineraryBookingDBDAOInterface provideItineraryDBDAO() {
    return new ItineraryBookingDBDAO(mContext);
  }

  @Override public LocationBookingDBDAOInterface provideLocationBookingDBDAO() {
    return new LocationBookingDBDAO(mContext);
  }

  @Override public InsuranceDBDAOInterface provideInsuranceDBDAO() {
    return new InsuranceDBDAO(mContext);
  }

  @Override public UserTravellerDBDAOInterface provideUserTravellersDBDAO() {
    return new UserTravellerDBDAO(mContext, provideUserDBDAO());
  }

  @Override public UserProfileDBDAOInterface provideUserProfileDBDao() {
    return new UserProfileDBDAO(mContext);
  }

  @Override public UserAddressDBDAOInterface provideUserAddressDBDao() {
    return new UserAddressDBDAO(mContext);
  }

  @Override public UserIdentificationDBDAOInterface provideUserIdentificationDBDao() {
    return new UserIdentificationDBDAO(mContext);
  }

  @Override public UserFrequentFlyerDBDAOInterface provideUserFrequentFlyerDBDao() {
    return new UserFrequentFlyerDBDAO(mContext);
  }

  @Override public StoredSearchDBDAOInterface provideStoredSearchesDBDao() {
    return new StoredSearchDBDAO(mContext);
  }

  @Override public SearchSegmentDBDAOInterface provideSearchSegmentDBDao() {
    return new SearchSegmentDBDAO(mContext);
  }

  @Override public CityDBDAOInterface provideCityDBDao() {
    return new CityDBDAO(mContext);
  }

  @Override public MembershipDBDAOInterface provideMembershipDBDao() {
    return new MembershipDBDAO(mContext);
  }

  @Override public TravellersHandlerInterface provideTravellersHandler() {
    return new TravellersHandler(provideUserDBDAO(), provideUserTravellersDBDAO(),
        provideUserProfileDBDao(), provideUserAddressDBDao(), provideUserIdentificationDBDao(),
        provideUserFrequentFlyerDBDao(), provideMembershipDBDao());
  }

  @Override public TrackerControllerInterface provideTrackerController() {
    return TrackerController.newInstance(mApplication,
        Configuration.getInstance().getCurrentMarket().getGACustomDim(), provideABTestHelper(),
        provideAnalyticsCustomDimensionFormatter());
  }

  @Override public CustomDimensionFormatter provideAnalyticsCustomDimensionFormatter() {
    return new AnalyticsCustomDimensionFormatter(mContext);
  }

  @Override public BookingsHandlerInterface provideBookingsHandler() {
    return new BookingsHandler(mApplication, provideBookingDBDAO(), provideBuyerDBDAO(),
        provideTravellerDBDAO(), provideBaggageDBDAO(), provideSegmentDBDAO(),
        provideCarrierDBDAO(), provideSectionDBDAO(), provideItineraryDBDAO(),
        provideLocationBookingDBDAO(), provideInsuranceDBDAO(), provideFlightStatsDBDAO(),
        provideDeleteBookingsInteractor());
  }

  private Gson provideGson() {
    return new Gson();
  }

  private Gson provideGsonWithNulls() {
    return new GsonBuilder().serializeNulls().create();
  }

  @Override public SearchesHandlerInterface provideSearchesHandler() {
    return new SearchesHandler(provideStoredSearchesDBDao(), provideSearchSegmentDBDao());
  }

  @Override public CitiesHandlerInterface provideCitiesHandler() {
    return new CitiesHandler(provideCityDBDao());
  }

  @Override public MembershipHandlerInterface provideMembershipHandler() {
    return new MembershipHandler(provideMembershipDBDao());
  }

  /**
   * Inject {@link DeleteBookingsInteractor}
   *
   * @return {@link DeleteBookingsInteractor}
   */
  public DeleteBookingsInteractor provideDeleteBookingsInteractor() {
    return new DeleteBookingsInteractor(provideBookingDBDAO(), provideTravellerDBDAO(),
        provideBuyerDBDAO(), provideInsuranceDBDAO(), provideBaggageDBDAO(), provideSegmentDBDAO(),
        provideSectionDBDAO());
  }

  public LogoutInteractor provideLogoutInteractor() {
    return new LogoutInteractor(provideSessionController(), provideNotificationsNetController(),
        provideBookingsHandler(), provideSearchesHandler(), provideVisitUserInteractor(),
        provideMembershipHandler());
  }

  @Override public ABTestHelper provideABTestHelper() {
    return new ABTestHelper(providePreferencesController(), provideCrashlyticsController());
  }

  @Override public ArrivalGuideNetControllerInterface provideArrivalGuideNetController() {
    return new ArrivalGuideNetController(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideTokenController());
  }

  @Override public AddPassengerNetJSONBuilder provideAddPassengerNetJSONBuilder() {
    return new AddPassengerNetJSONBuilder(provideDateHelper());
  }

  @Override public AddProductsNetControllerInterface provideAddProductsNetController() {

    return new AddProductsNetController(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideGsonWithNulls());
  }

  @Override public BinCheckNetControllerInterface provideBinCheckNetController() {
    return new CheckBinController(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideGsonWithNulls());
  }

  @Override public RemoveProductsNetControllerInterface provideRemoveProductsNetController() {

    return new RemoveProductsNetController(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideGsonWithNulls());
  }

  @Override public AddPassengerNetControllerInterface provideAddPassengerNetController() {
    return new AddPassengerNetController(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideGsonWithNulls());
  }

  @Override public ShoppingCartNetControllerInterface provideShoppingCartNetController() {
    return new ShoppingCartNetController(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideGsonWithNulls());
  }

  @Override public GuideDBDAOInterface provideGuideDBDAO() {
    return new GuideDBDAO(mContext);
  }

  @Override public FlightStatsDBDAOInterface provideFlightStatsDBDAO() {
    return new FlightStatsDBDAO(mContext);
  }

  @Override public CountriesDbHelperInterface provideCountriesDbHelper() {
    return new CountriesDbHelper(mContext);
  }

  @Override public OdigeoNotificationManagerInterface provideOdigeoNotificationManager() {
    return new OdigeoNotificationManager(mContext);
  }

  @Override public BookingStatusAlarmManagerInterface provideBookingStatusAlarmManager() {
    return new BookingStatusAlarmManager(mContext, provideBookingDBDAO());
  }

  @Override public CardFactory provideCardFactory() {
    return new CardFactory();
  }

  @Override public SectionCardBuilder provideSectionCardBuilder() {
    return new SectionCardBuilder(provideCardFactory(), provideDateHelper());
  }

  @Override public DateHelper provideDateHelper() {
    return new DateHelper();
  }

  public CalendarHelperInterface provideCalendarHelper(Activity activity) {
    return new CalendarHelper(activity, provideDateHelper());
  }

  @Override public ConditionRulesHelper provideConditionRulesHelper() {
    return new ConditionRulesHelper();
  }

  @Override public ConfirmBookingNetControllerInterface provideConfirmBookingNetController() {
    return new ConfirmBookingNetController(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideGson());
  }

  @Override public ResumeBookingNetController provideResumeBookingNetController() {
    return new ResumeBookingNetControllerImpl(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideGson());
  }

  @Override public SendMailNetControllerInterface provideSendMailNetController() {
    return new SendMailNetController(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper());
  }

  @Override public AutoCompleteNetControllerInterface provideAutoCompleteNetController() {
    return new AutoCompleteNetController(provideRequestQueue(), provideDomainHelper(),
        provideHeaderHelper());
  }

  @Override public MarketProviderInterface provideMarketProvider() {
    return new MarketProvider(provideConfiguration(), provideEmailProvider());
  }

  @Override public MyTripsProvider provideMyTripsProvider() {
    return new MyTripsProvider();
  }

  public Configuration provideConfiguration() {
    return Configuration.getInstance();
  }

  public EmailProvider provideEmailProvider() {
    return new EmailProvider(mApplication);
  }

  @Override public TuneTrackerInterface provideTuneTracker() {
    return TuneTracker.getInstance();
  }

  public OdigeoImageLoader<ImageView> provideImageLoader() {
    if (mOdigeoImageLoader == null) {
      mOdigeoImageLoader = new GlideImageLoader(mContext);
    }
    return mOdigeoImageLoader;
  }

  public LocationController provideLocationController() {
    return LocationController.getInstance(mContext, provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), Configuration.getInstance().getCurrentMarket().getLocale());
  }

  public FacebookController provideFacebookHelper(Fragment fragment,
      SocialLoginPresenter socialLoginPresenter) {
    return new FacebookController(fragment, socialLoginPresenter);
  }

  public GooglePlusController provideGooglePlusHelper(Fragment fragment,
      SocialLoginPresenter socialLoginPresenter) {
    return new GooglePlusController(fragment, socialLoginPresenter);
  }

  public UpdateCarouselDotsAnimation provideUpdateCarouselDotsAnimation() {
    return new UpdateCarouselDotsAnimation(new Handler());
  }

  private TokenController provideTokenController() {
    if (tokenController == null) {
      tokenController = new TokenController(provideSessionController(), mContext, new AuthToken());
    }
    return tokenController;
  }

  public SQLiteDatabase provideSQLiteDatabase() throws IOException, SQLException {
    return LocalizablesDatabase.getInstance().getDatabase(mContext);
  }

  @Override public CrossCarUrlHandlerInterface provideCrossCarUrlHandler() {
    return new CrossCarUrlHandler(provideMarketProvider(), provideDateHelper());
  }

  @Override public VisitsNetController provideVisitsNetController() {
    return new VisitsNetControllerImpl(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideGson());
  }

  @Override public CrashlyticsController provideCrashlyticsController() {
    return new CrashlyticsControllerImpl((CrashlyticsUtil) mApplication);
  }

  @Override public PackageController providePackageController() {
    return new PackageControllerImpl(mContext);
  }

  @Override public RemoteConfigControllerInterface provideRemoteConfigController() {
    return FirebaseRemoteConfigController.newInstance(FirebaseRemoteConfig.getInstance(),
        FirebaseAnalyticsController.getInstance(mContext));
  }

  public WelcomeProvider provideWelcomeProvider() {
    return new WelcomeProvider(provideTravellersHandler());
  }

  @Override public LocalizableProvider provideLocalizables() {
    return new LocalizablesFacadeWrapper(mContext);
  }

  public LocaleHelper provideLocaleHelper() {
    return new LocaleHelperAdapter();
  }

  @Override public BaggageCollectionItemPresenter provideBaggageCollectionItemPresenter(
      BaggageCollectionItem baggageCollectionItem) {
    return new BaggageCollectionItemPresenter(baggageCollectionItem, provideABTestHelper());
  }

  @Override public BaggageCollectionPresenter provideBaggageCollectionPresenter(
      BaggageCollection baggageCollection) {
    return new BaggageCollectionPresenter(baggageCollection);
  }

  @Override public AssetsProvider provideAssetsProvider() {
    if (assetsProvider == null) {
      assetsProvider = new AndroidAssetsProvider(mApplication);
    }
    return assetsProvider;
  }

  @Override public FileProvider provideFileProvider() {
    if (fileProvider == null) {
      fileProvider = new FileProviderImpl(mApplication);
    }
    return fileProvider;
  }

  public StoredSearchOptionsMapper provideStoredSearchOptionsMapper() {
    return new StoredSearchOptionsMapper(provideCitiesHandler());
  }

  @Override public DurationFormatter provideDurationFormatter() {
    if (durationFormatter == null) {
      durationFormatter = new OdigeoDurationFormatter(provideLocalizables());
    }
    return durationFormatter;
  }

  @Override public CreateShoppingCartInteractor provideCreateShoppingCartInteractor() {
    return new CreateShoppingCartInteractor(provideShoppingCartNetController(),
        provideTrustDefenderController(), providePreferencesController());
  }

  @Override public CrossSellingRepository provideCrossSellingRepository() {
    if (crossSellingRepository == null) {
      crossSellingRepository =
          new CrossSellingDataRepository(provideLocalizables(), provideGuideDBDAO());
    }
    return crossSellingRepository;
  }

  @Override public VisitUserNetController provideVisitUserNetController() {

    return new VisitUserNetControllerImpl(provideRequestHelper(), provideDomainHelper(),
        provideHeaderHelper(), provideTokenController());
  }

  public RequestValidationHandler provideCreditCardsRequestValidationHandler() {
    return new RequestValidationHandler(CreditCardValidationCommandMapFactory.getMap());
  }

  public RequestValidationHandler provideIdentificationRequestValidationHandler() {
    return new RequestValidationHandler(IdentificationValidationCommandMapFactory.getMap());
  }

  public BookingImageUtil provideMyTripsImageUtil() {
    return new BookingImageUtil();
  }

  public FontProvider provideFontProvider() {
    return new FontProvider(mContext);
  }
}
