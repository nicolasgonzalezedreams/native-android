package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;
import com.odigeo.app.android.lib.consts.DistanceUnits;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.interfaces.OdigeoInterfaces.ItemListDestination;
import com.odigeo.app.android.lib.models.CityItemListDestination;
import com.odigeo.app.android.lib.models.HeaderItemListDestination;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.data.entity.geo.LocationDescriptionType;
import java.util.List;

/**
 * Created by Irving Lóp on 02/09/2014.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 2.1
 * @since 06/02/2015
 */
public class DestinationAdapter extends BaseAdapter {
  public static final int HEADER = 0;
  public static final int DESTINATION = 1;
  public static final int LOCATION = 2;
  private static final int VIEW_TYPES_COUNT = 3;
  private static final Fonts FONTS = Configuration.getInstance().getFonts();
  private final Context context;
  private List<ItemListDestination> listDestinations;
  private String query;

  /**
   * Constructor.
   *
   * @param context Context where be called the adapter.
   * @param items List of item who be displayed.
   */
  public DestinationAdapter(Context context, List<ItemListDestination> items) {
    this.context = context;
    listDestinations = items;
  }

  public List<ItemListDestination> getListDestinations() {
    return listDestinations;
  }

  public void setListDestinations(List<ItemListDestination> listDestinations) {
    this.listDestinations = listDestinations;
  }

  public String getQuery() {
    return query;
  }

  public void setQuery(String query) {
    this.query = query;
  }

  @Override public int getCount() {
    return listDestinations.size();
  }

  @Override public ItemListDestination getItem(int position) {
    return listDestinations.get(position);
  }

  @Override public long getItemId(int position) {
    return position;
  }

  @Override public int getViewTypeCount() {
    return VIEW_TYPES_COUNT;
  }

  @Override public int getItemViewType(int position) {
    ItemListDestination item = listDestinations.get(position);
    //Check if it's a header item.
    if (item instanceof HeaderItemListDestination) {
      return HEADER;
    } else if (item instanceof CityItemListDestination) {
      //If it's a normal item, check if has a distance <= 0, then the item it's a destination search item.
      if (((CityItemListDestination) item).getDistanceToCurrentLocation() <= 0) {
        return DESTINATION;
      } else {
        //In other hand, here the item it's a item from location search.
        return LOCATION;
      }
    }
    //In some other case return -1 to indicate doesn't fit in some type of view.
    return -1;
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {
    ItemListDestination item = listDestinations.get(position);
    LayoutInflater inflater = LayoutInflater.from(context);
    View view = convertView;
    //Check the type of the view
    switch (getItemViewType(position)) {
      case HEADER:
        if (view == null) {
          view = inflater.inflate(R.layout.header_list_destination_location, parent, false);
          view.setTag(new HeaderViewHolder(view));
        }
        HeaderItemListDestination itemHeader = ((HeaderItemListDestination) item);
        HeaderViewHolder holderHeader = ((HeaderViewHolder) view.getTag());
        holderHeader.txtHeader.setText(itemHeader.getTitleHeader());
        //Special verification for recent items searched.
        if (holderHeader.imgHeader != null) {
          holderHeader.imgHeader.setImageResource(itemHeader.getIdIconHeader());
        }
        break;
      case DESTINATION:
        if (view == null) {
          view = inflater.inflate(R.layout.row_list_destination, parent, false);
          view.setTag(new DestinationViewHolder(view));
        }
        CityItemListDestination itemDestination = ((CityItemListDestination) item);
        DestinationViewHolder holderDestination = ((DestinationViewHolder) view.getTag());
        fillDestinationItem(itemDestination, holderDestination);
        //If it's a SubItem
        RelativeLayout.LayoutParams params =
            ((RelativeLayout.LayoutParams) holderDestination.imgType.getLayoutParams());
        if (itemDestination.isSubItem()) {
          params.leftMargin =
              context.getResources().getDimensionPixelSize(R.dimen.secondary_button_marging_side);
        } else {
          params.leftMargin =
              context.getResources().getDimensionPixelSize(R.dimen.standard_padding);
        }
        holderDestination.imgType.setLayoutParams(params);
        break;
      case LOCATION:
      default:
        if (view == null) {
          view = inflater.inflate(R.layout.row_list_destination_location, parent, false);
          view.setTag(new LocationViewHolder(view));
        }
        CityItemListDestination itemLocation = ((CityItemListDestination) item);
        LocationViewHolder holderLocation = ((LocationViewHolder) view.getTag());
        fillDestinationItem(itemLocation, holderLocation);
        break;
    }
    return view;
  }

  /**
   * Method to fill the {@link #DESTINATION} and {@link #LOCATION} item of the list
   *
   * @param itemDestination Item where be extracted the data.
   * @param holder View Holder with all the references of the views.
   */
  private void fillDestinationItem(CityItemListDestination itemDestination,
      DestinationViewHolder holder) {
    holder.txtCityName.setTypeface(FONTS.getBold());
    holder.txtIataCode.setTypeface(FONTS.getRegular());
    holder.txtCountry.setTypeface(FONTS.getRegular());
    holder.txtIataCode.setText(getColoredText(itemDestination.getIataCode()));
    if (itemDestination.getType() == LocationDescriptionType.CITY) {
      holder.txtCountry.setText(itemDestination.getCountryName());
    } else {
      holder.txtCountry.setText(
          itemDestination.getCityName() + ", " + itemDestination.getCountryName());
    }
    holder.txtIataCode.setSelected(false);
    // If there is a coincidence with the IataCode.
    if (itemDestination.getIataCode() != null && query != null && itemDestination.getIataCode()
        .toLowerCase(LocaleUtils.getCurrentLocale())
        .startsWith(query.toLowerCase(LocaleUtils.getCurrentLocale()))) {
      holder.txtIataCode.setSelected(true);
    }
    setImgTypeAndCityName(itemDestination, holder);
    //Special behavior for location
    if (holder instanceof LocationViewHolder) {
      //Set the distance in the distance unit configured
      DistanceUnits distanceUnit = PreferencesManager.readSettings(context).getDistanceUnit();
      String distanceUnitName = Util.getDistanceUnitName(distanceUnit, context);
      String strDistance = String.format(context.getString(R.string.distance_format),
          itemDestination.getDistanceToCurrentLocation(), distanceUnitName);
      ((LocationViewHolder) holder).txtDistance.setText(strDistance);
    }
  }

  /**
   * Method to set the ImgType and CityName for the specific item in the list passed.
   *
   * @param itemDestination Item where be extracted the data.
   * @param holder View Holder with all the references of the views.
   */
  private void setImgTypeAndCityName(CityItemListDestination itemDestination,
      DestinationViewHolder holder) {
    if (itemDestination.getType() == LocationDescriptionType.CITY) {
      setCityValues(itemDestination, holder);
    } else if (itemDestination.getType() == LocationDescriptionType.AIRPORT) {
      setAirportValues(itemDestination, holder);
    } else if (itemDestination.getType() == LocationDescriptionType.IATA_CODE) {
      setIataValues(itemDestination, holder);
    }
  }

  /**
   * Set the values for a Iata item type.
   *
   * @param itemDestination Item with all the information to be displayed.
   * @param holder Holder with all the references to the views.
   */
  private void setIataValues(CityItemListDestination itemDestination,
      DestinationViewHolder holder) {
    //It's a city.
    if (itemDestination.getIataCode() != null && itemDestination.getIataCode()
        .equalsIgnoreCase(itemDestination.getName())) {
      holder.txtCityName.setText(getColoredText(itemDestination.getName()));
      if (holder.imgType != null) {
        holder.imgType.setImageResource(R.drawable.search_buildings);
      }
    } else { //Is an airport
      holder.txtCityName.setText(getColoredText(itemDestination.getName()));
      if (holder.imgType != null) {
        holder.imgType.setImageResource(R.drawable.search_plane);
      }
    }
  }

  /**
   * Set the values for a Airport item type.
   *
   * @param itemDestination Item with all the information to be displayed.
   * @param holder Holder with all the references to the views.
   */
  private void setAirportValues(CityItemListDestination itemDestination,
      DestinationViewHolder holder) {
    if (holder.imgType != null) {
      holder.imgType.setImageResource(R.drawable.search_plane);
    }
    holder.txtCityName.setText(getColoredText(itemDestination.getName()));
  }

  /**
   * Set the values for a Destination item type.
   *
   * @param itemDestination Item with all the information to be displayed.
   * @param holder Holder with all the references to the views.
   */
  private void setCityValues(CityItemListDestination itemDestination,
      DestinationViewHolder holder) {
    if (holder.imgType != null) {
      holder.imgType.setImageResource(R.drawable.search_buildings);
    }
    if (itemDestination.isGroupParent()) {
      String allAirports = " - <b><i>"
          + LocalizablesFacade.getString(context, "locationsviewcontroller_allairports")
          + "</i></b>";
      holder.txtCityName.setText(getColoredText(itemDestination.getCityName() + allAirports));
    } else {
      holder.txtCityName.setText(getColoredText(itemDestination.getCityName()));
    }
  }

  /**
   * Retrieves a spannable object with the query string highlighted.
   *
   * @param placeName String where be searched.
   * @return Object with the query string highlighted.
   */
  private Spanned getColoredText(String placeName) {
    int indexCharColor = 0;
    if (query != null && placeName.toLowerCase(LocaleUtils.getCurrentLocale())
        .startsWith(query.toLowerCase(LocaleUtils.getCurrentLocale()))) {
      indexCharColor = query.length();
    }
    Spannable colorCityName = (Spannable) HtmlUtils.formatHtml(placeName);
    if (indexCharColor != 0) {
      colorCityName.setSpan(new ForegroundColorSpan(
              context.getResources().getColor(R.color.origin_destination_hightlight)), 0,
          indexCharColor, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    return colorCityName;
  }

  /**
   * Class to implementation of the View Holder pattern.
   */
  public static final class HeaderViewHolder {
    final ImageView imgHeader;
    final TextView txtHeader;

    public HeaderViewHolder(View rootView) {
      imgHeader = ((ImageView) rootView.findViewById(R.id.imgType));
      txtHeader = ((TextView) rootView.findViewById(R.id.txtHeader));
    }
  }

  /**
   * Class to implementation of the View Holder pattern.
   */
  public static class DestinationViewHolder {
    final ImageView imgType;
    final TextView txtCityName;
    final TextView txtCountry;
    final TextView txtIataCode;

    public DestinationViewHolder(View rootView) {
      imgType = ((ImageView) rootView.findViewById(R.id.imgType));
      txtCityName = ((TextView) rootView.findViewById(R.id.txtCityName));
      txtCountry = ((TextView) rootView.findViewById(R.id.txtCountry));
      txtIataCode = ((TextView) rootView.findViewById(R.id.txtIataCode));
    }
  }

  /**
   * Class to implementation of the View Holder pattern.
   */
  public static final class LocationViewHolder extends DestinationViewHolder {
    final TextView txtDistance;

    public LocationViewHolder(View rootView) {
      super(rootView);
      txtDistance = ((TextView) rootView.findViewById(R.id.txtDistance));
    }
  }
}
