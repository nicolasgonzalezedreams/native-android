package com.odigeo.app.android.lib.neck.requests;

import android.util.Log;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.dto.requests.FlightsRequestModel;
import com.odigeo.app.android.lib.models.dto.responses.FlightSearchResponse;
import com.odigeo.app.android.lib.neck.requests.base.BaseOdigeoRequests;
import com.odigeo.data.net.helper.DomainHelperInterface;

public class FlightRequest
    extends BaseOdigeoRequests<FlightSearchResponse, FlightSearchResponse, FlightsRequestModel> {

  private static final String PATH = "search/";
  private DomainHelperInterface mDomainHelper;

  public FlightRequest(FlightsRequestModel requestModel, String deviceId,
      DomainHelperInterface domainHelper) {
    super(FlightSearchResponse.class, requestModel, null, null, deviceId);
    mDomainHelper = domainHelper;
  }

  @Override protected final String getUrl() {

    String url = String.format("%s%s", mDomainHelper.getUrl(), PATH);
    return url;
  }

  @Override protected final FlightSearchResponse processContent(String responseBody) {

    Log.i(Constants.TAG_LOG_MSL, responseBody);
    FlightSearchResponse response = super.processContent(responseBody);

    if (response != null) {
      response.setCookies(getCookiesReceived());
    }

    return response;
  }
}
