package com.odigeo.app.android.view;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.custom.CalendarHeaderView;
import com.odigeo.app.android.view.helpers.DateHelper;
import com.odigeo.calendar.CalendarCellDecorator;
import com.odigeo.calendar.CalendarCellView;
import com.odigeo.calendar.CalendarPickerView;
import com.odigeo.calendar.CalendarPickerView.OnDateSelectedListener;
import com.odigeo.calendar.CalendarPickerView.SelectionMode;
import com.odigeo.data.entity.FlightsDirection;
import com.odigeo.data.entity.TravelType;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.CalendarPresenter;
import com.odigeo.presenter.contracts.navigators.CalendarNavigatorInterface;
import com.odigeo.presenter.contracts.views.CalendarViewInterface;
import com.odigeo.tools.DateUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CalendarView extends BaseView<CalendarPresenter>
    implements CalendarViewInterface, OnDateSelectedListener {

  final Handler handler = new Handler();
  private final int SKIP_LABEL_TIME = 4000;
  private LinearLayout linearLayoutSkipReturn;
  final Runnable runnable = new Runnable() {
    @Override public void run() {
      hideSkipReturnButton();
    }
  };
  private TextView tvSkipReturnButton;
  private CalendarHeaderView calendarHeaderView;
  private CalendarPickerView calendarPickerView;
  private LinearLayout linearLayoutConfirmationButton;
  private RelativeLayout confirmationButton;
  private TextView tvConfirmationButtonDates;
  private String countryHolidaysDays;
  private String countryHolidaysCaption;
  private String regionHolidaysDays;
  private String regionHolidaysCaption;
  private String selectionErrorMessageRange;
  private String selectionErrorMessageFrom;
  private DateHelper dateHelper = new DateHelper();

  public static CalendarView newInstance(int segmentNumber, TravelType travelType,
      FlightsDirection flightsDirection, ArrayList<Date> dates) {
    CalendarView calendarView = new CalendarView();
    Bundle args = new Bundle();
    args.putInt(Constants.EXTRA_FLIGHT_SEGMENT_NUMBER, segmentNumber);
    args.putSerializable(Constants.EXTRA_TRAVEL_TYPE, travelType);
    args.putSerializable(Constants.EXTRA_FLIGHT_DIRECTION, flightsDirection);
    args.putSerializable(Constants.EXTRA_SELECTED_DATES, dates);
    calendarView.setArguments(args);
    return calendarView;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    TravelType travelType =
        (TravelType) getArguments().getSerializable(Constants.EXTRA_TRAVEL_TYPE);

    Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
    if (null != toolbar) {
      if (TravelType.ROUND == travelType) {
        toolbar.setTitle(localizables.getString(OneCMSKeys.CALENDAR_PICK_YOUR_DATES));
      } else {
        toolbar.setTitle(localizables.getString(OneCMSKeys.CALENDAR_PICK_YOUR_DATE));
      }

      toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
      toolbar.setNavigationOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          getActivity().onBackPressed();
        }
      });
    }

    mPresenter.initializePresenter(getArguments().getInt(Constants.EXTRA_FLIGHT_SEGMENT_NUMBER, 0),
        travelType,
        (ArrayList<Date>) getArguments().getSerializable(Constants.EXTRA_SELECTED_DATES));
    mPresenter.showSelectedDates();

    mPresenter.setCountryHolidays(DateUtils.convertToDate(countryHolidaysDays));
    mPresenter.setRegionHolidays(DateUtils.convertToDate(regionHolidaysDays));
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      getActivity().onBackPressed();
    }

    return super.onOptionsItemSelected(item);
  }

  @Override protected CalendarPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideCalendarPresenter(this, (CalendarNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_calendar;
  }

  @Override protected void initComponent(View view) {
    linearLayoutSkipReturn = (LinearLayout) view.findViewById(R.id.skip_return_layout);
    tvSkipReturnButton = (TextView) view.findViewById(R.id.skip_return_button);
    calendarHeaderView = (CalendarHeaderView) view.findViewById(R.id.calendar_header);
    calendarPickerView = (CalendarPickerView) view.findViewById(R.id.calendar_view);
    linearLayoutConfirmationButton =
        (LinearLayout) view.findViewById(R.id.confirmation_button_layout);
    confirmationButton = (RelativeLayout) view.findViewById(R.id.confirmation_button);
    tvConfirmationButtonDates = (TextView) view.findViewById(R.id.confirmation_button_dates_text);

    ((TextView) view.findViewById(R.id.skip_return_text)).setText(
        localizables.getString(OneCMSKeys.CALENDAR_ONE_WAY_TRIP));
    ((TextView) view.findViewById(R.id.confirmation_button_continue_text)).setText(
        localizables.getString(OneCMSKeys.COMMON_BUTTONCONTINUE));
    ((TextView) view.findViewById(R.id.departure_label)).setText(
        localizables.getString(OneCMSKeys.MY_TRIPS_FLIGHT_CARD_TITLE_OUTBOUND));
    ((TextView) view.findViewById(R.id.return_label)).setText(
        localizables.getString(OneCMSKeys.MY_TRIPS_FLIGHT_CARD_TITLE_INBOUND));

    resetDepartureDateHeader();
    resetReturnDateHeader();
    openingView();
  }

  @Override protected void setListeners() {
    List<CalendarCellDecorator> decoratorList = new ArrayList<>();
    decoratorList.add(new CalendarView.MonthDecorator());
    calendarPickerView.setDecorators(decoratorList);
    calendarPickerView.setOnDateSelectedListener(this);

    tvSkipReturnButton.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHT_SEARCH,
            TrackerConstants.ACTION_SEARCHER_FLIGHTS,
            TrackerConstants.LABEL_CALENDAR_SELECT_ONE_WAY);
        mPresenter.onSkipReturnButtonClicked();
      }
    });
    confirmationButton.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHT_SEARCH,
            TrackerConstants.ACTION_SEARCHER_FLIGHTS, TrackerConstants.LABEL_CALENDAR_CONTINUE);
        mPresenter.onConfirmationButtonClicked();
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    tvSkipReturnButton.setText(localizables.getString(OneCMSKeys.CALENDAR_SKIP_RETURN));

    getCalendarErrorMessages();
    getCountryHolidaysConfiguration();
    getRegionalHolidaysConfiguration();
  }

  private void getCountryHolidaysConfiguration() {
    countryHolidaysCaption = getLocalizable(OneCMSKeys.CALENDAR_HOLIDAYS_COUNTRY_CAPTION);
    countryHolidaysDays = getLocalizable(OneCMSKeys.CALENDAR_HOLIDAYS_COUNTRY);
    if (countryHolidaysCaption == null || countryHolidaysDays == null) {
      countryHolidaysCaption = null;
      countryHolidaysDays = null;
    }
  }

  private void getRegionalHolidaysConfiguration() {
    regionHolidaysCaption = getLocalizable(OneCMSKeys.CALENDAR_HOLIDAYS_REGION_CAPTION);
    regionHolidaysDays = getLocalizable(OneCMSKeys.CALENDAR_HOLIDAYS_REGION);
    if (regionHolidaysCaption == null || regionHolidaysDays == null) {
      regionHolidaysCaption = null;
      regionHolidaysDays = null;
    }
  }

  private void getCalendarErrorMessages() {
    selectionErrorMessageRange = getLocalizable(OneCMSKeys.CALENDAR_ERROR_CHOOSE_DATE_BETWEEN);
    selectionErrorMessageFrom = getLocalizable(OneCMSKeys.CALENDAR_ERROR_CHOOSE_DATE_AFTER);
    if (selectionErrorMessageRange == null || selectionErrorMessageFrom == null) {
      selectionErrorMessageRange = null;
      selectionErrorMessageFrom = null;
    }
  }

  private String getLocalizable(String oneCMSKey) {
    String value = localizables.getRawString(oneCMSKey);

    if (value.equals("") || value.equals(String.format(LocalizablesFacade.NOT_FOUND, oneCMSKey))) {
      return null;
    }

    return value;
  }

  @Override
  public void setCalendarBounds(final Date minDate, Date maxDate, List<Date> lstSelectedDates,
      int mode, boolean avoidScrolling) {
    SelectionMode selectionMode = (mode > 1) ? SelectionMode.RANGE : SelectionMode.MULTIPLE;

    calendarPickerView.init(DateUtils.convertToLocalTime(minDate),
        DateUtils.convertToLocalTime(maxDate), Configuration.getCurrentLocale(),
        countryHolidaysCaption, regionHolidaysCaption, mPresenter.getSegmentNumber(),
        selectionErrorMessageRange, selectionErrorMessageFrom)
        .inMode(selectionMode)
        .withSelectedDates(DateUtils.convertToLocalTime(lstSelectedDates), avoidScrolling);
  }

  @Override public void selectDateOnCalendar(Date date) {
    calendarPickerView.selectDate(date);
  }

  @Override public void setCalendarFilter(final Date minSelectableDateInLocal) {
    calendarPickerView.setDateSelectableFilter(new CalendarPickerView.DateSelectableFilter() {
      @Override public boolean isDateSelectable(Date dateInLocal) {
        return DateUtils.compareDatesWithoutTime(dateInLocal, minSelectableDateInLocal) >= 0;
      }
    });
  }

  @Override public void setSegmentCalendarFilter() {
    calendarPickerView.setDateSelectableFilter(new CalendarPickerView.DateSelectableFilter() {
      @Override public boolean isDateSelectable(Date dateInLocal) {
        return mPresenter.isDateSelectable(dateInLocal);
      }
    });
  }

  @Override public void showSkipReturnButton() {
    if (linearLayoutSkipReturn.getVisibility() != View.VISIBLE) {
      linearLayoutSkipReturn.setVisibility(View.VISIBLE);
      Animation showFromTop = AnimationUtils.loadAnimation(getActivity(), R.anim.show_from_top);
      showFromTop.setInterpolator(new DecelerateInterpolator());
      showFromTop.setAnimationListener(new Animation.AnimationListener() {
        @Override public void onAnimationStart(Animation animation) {

        }

        @Override public void onAnimationEnd(Animation animation) {
          handler.postDelayed(runnable, SKIP_LABEL_TIME);
        }

        @Override public void onAnimationRepeat(Animation animation) {

        }
      });
      linearLayoutSkipReturn.startAnimation(showFromTop);
    } else {
      handler.removeCallbacks(runnable);
      handler.postDelayed(runnable, SKIP_LABEL_TIME);
    }
  }

  @Override public void hideSkipReturnButton() {
    handler.removeCallbacks(runnable);
    if (linearLayoutSkipReturn.getVisibility() == View.VISIBLE) {
      linearLayoutSkipReturn.setVisibility(View.GONE);
      Animation hideToTop = AnimationUtils.loadAnimation(getActivity(), R.anim.hide_to_top);
      hideToTop.setInterpolator(new AccelerateInterpolator());
      linearLayoutSkipReturn.startAnimation(hideToTop);
    }
  }

  @Override public void showConfirmationButton() {
    if (linearLayoutConfirmationButton.getVisibility() != View.VISIBLE) {
      linearLayoutConfirmationButton.setVisibility(View.VISIBLE);
      Animation showFromBottom =
          AnimationUtils.loadAnimation(getActivity(), R.anim.show_from_bottom);
      showFromBottom.setInterpolator(new DecelerateInterpolator());
      linearLayoutConfirmationButton.startAnimation(showFromBottom);
    }
  }

  @Override public void hideConfirmationButton() {
    if (linearLayoutConfirmationButton.getVisibility() == View.VISIBLE) {
      linearLayoutConfirmationButton.setVisibility(View.GONE);
      Animation hideToBottom = AnimationUtils.loadAnimation(getActivity(), R.anim.hide_to_bottom);
      hideToBottom.setInterpolator(new AccelerateInterpolator());
      linearLayoutConfirmationButton.startAnimation(hideToBottom);
    }
  }

  @Override public void setConfirmationButtonDates(Date departureDate, Date returnDate) {
    if (departureDate != null && returnDate != null) {
      String outbound =
          dateHelper.getGmtDateFormat(getResources().getString(R.string.templates__date1))
              .format(departureDate);
      String inbound =
          dateHelper.getGmtDateFormat(getResources().getString(R.string.templates__date1))
              .format(returnDate);
      tvConfirmationButtonDates.setText(
          getString(R.string.format_round_trip_dates, outbound, inbound));
    } else if (departureDate != null) {
      setConfirmationButtonDate(departureDate);
    } else if (returnDate != null) {
      setConfirmationButtonDate(returnDate);
    }
  }

  @Override public void setConfirmationButtonDate(Date departureDate) {
    String outbound =
        dateHelper.getGmtDateFormat(getResources().getString(R.string.templates__date1))
            .format(departureDate);
    tvConfirmationButtonDates.setText(outbound);
  }

  @Override public void setDepartureDateHeader(Date departureDate) {
    calendarHeaderView.setDepartureDate(departureDate);
  }

  @Override public void setReturnDateHeader(Date returnDate) {
    calendarHeaderView.setReturnDate(returnDate);

    if (mPresenter.getTravelType() == TravelType.ROUND) {
      hideSkipReturnButton();
    }
  }

  private void openingView() {
    calendarHeaderView.openingView();
  }

  @Override public void resetDepartureDateHeader() {
    calendarHeaderView.resetDepartureDate();
  }

  @Override public void resetReturnDateHeader() {
    calendarHeaderView.resetReturnDate();
  }

  @Override public void showRoundTripHeader() {
    calendarHeaderView.setVisibility(View.VISIBLE);
  }

  @Override public void hideRoundTripHeader() {
    calendarHeaderView.setVisibility(View.GONE);
  }

  @Override public void setSelectorBackground(TravelType travelType) {
    switch (travelType) {
      case SIMPLE:
        setOneWaySelectorBackground();
        break;

      case ROUND:
        setRoundTripSelectorBackground();
        break;

      case MULTIDESTINATION:
        setMultiStopSelectorBackground();
        break;
    }
  }

  private void setOneWaySelectorBackground() {
    if (calendarPickerView != null) {
      calendarPickerView.setDayBackgroundResId(R.drawable.selector_calendar_background_one_way);
    }
  }

  private void setRoundTripSelectorBackground() {
    if (calendarPickerView != null) {
      calendarPickerView.setDayBackgroundResId(R.drawable.selector_calendar_background_round_trip);
    }
  }

  private void setMultiStopSelectorBackground() {
    if (calendarPickerView != null) {
      calendarPickerView.setDayBackgroundResId(R.drawable.selector_calendar_background_multi_stop);
    }
  }

  @Override public void onDateSelected(Date date) {
    mPresenter.onDateSelected(date);
  }

  @Override public void onDateUnselected(Date date) {
  }

  @Override public void onResume() {
    super.onResume();
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_CALENDAR);
  }

  @Override public void onDestroy() {
    super.onDestroy();
    handler.removeCallbacks(runnable);
  }

  private class MonthDecorator implements CalendarCellDecorator {

    @Override public void decorate(CalendarCellView cellView, Date date) {
      checkHolidayStyle(cellView, date);
    }

    private void checkHolidayStyle(CalendarCellView cellView, Date date) {
      Drawable drawable = null;

      if (cellView.isSelectable()) {
        if (mPresenter.isCountryHoliday(date)) {
          drawable = ContextCompat.getDrawable(getActivity(), R.drawable.selector_calendar_country);
        } else if (mPresenter.isRegionHoliday(date)) {
          drawable = ContextCompat.getDrawable(getActivity(), R.drawable.selector_calendar_region);
        }
      }

      cellView.getHolidayIndicator().setImageDrawable(drawable);
    }
  }
}
