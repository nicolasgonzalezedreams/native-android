package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class AccommodationDealInformationDTO implements Serializable {

  protected String name;
  protected String address;
  protected String cityName;
  protected String description;
  protected String thumbnailUrl;
  protected String cancelationPolicyDescription;
  protected GeoCoordinatesDTO coordinates;
  protected Double accuracy;
  protected AccommodationCategoryDTO category;
  protected boolean paymentAtDestination;

  /**
   * Gets the value of the name property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the value of the name property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setName(String value) {
    this.name = value;
  }

  /**
   * Gets the value of the address property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getAddress() {
    return address;
  }

  /**
   * Sets the value of the address property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setAddress(String value) {
    this.address = value;
  }

  /**
   * Gets the value of the cityName property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCityName() {
    return cityName;
  }

  /**
   * Sets the value of the cityName property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCityName(String value) {
    this.cityName = value;
  }

  /**
   * Gets the value of the description property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getDescription() {
    return description;
  }

  /**
   * Sets the value of the description property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setDescription(String value) {
    this.description = value;
  }

  /**
   * Gets the value of the thumbnailUrl property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

  /**
   * Sets the value of the thumbnailUrl property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setThumbnailUrl(String value) {
    this.thumbnailUrl = value;
  }

  /**
   * Gets the value of the cancelationPolicyDescription property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCancelationPolicyDescription() {
    return cancelationPolicyDescription;
  }

  /**
   * Sets the value of the cancelationPolicyDescription property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCancelationPolicyDescription(String value) {
    this.cancelationPolicyDescription = value;
  }

  /**
   * Gets the value of the coordinates property.
   *
   * @return possible object is
   * {@link GeoCoordinates }
   */
  public GeoCoordinatesDTO getCoordinates() {
    return coordinates;
  }

  /**
   * Sets the value of the coordinates property.
   *
   * @param value allowed object is
   * {@link GeoCoordinates }
   */
  public void setCoordinates(GeoCoordinatesDTO value) {
    this.coordinates = value;
  }

  /**
   * Gets the value of the accuracy property.
   *
   * @return possible object is
   * {@link Double }
   */
  public Double getAccuracy() {
    return accuracy;
  }

  /**
   * Sets the value of the accuracy property.
   *
   * @param value allowed object is
   * {@link Double }
   */
  public void setAccuracy(Double value) {
    this.accuracy = value;
  }

  /**
   * Gets the value of the category property.
   *
   * @return possible object is
   * {@link AccommodationCategory }
   */
  public AccommodationCategoryDTO getCategory() {
    return category;
  }

  /**
   * Sets the value of the category property.
   *
   * @param value allowed object is
   * {@link AccommodationCategory }
   */
  public void setCategory(AccommodationCategoryDTO value) {
    this.category = value;
  }

  /**
   * Gets the value of the paymentAtDestination property.
   */
  public boolean isPaymentAtDestination() {
    return paymentAtDestination;
  }

  /**
   * Sets the value of the paymentAtDestination property.
   */
  public void setPaymentAtDestination(boolean value) {
    this.paymentAtDestination = value;
  }
}
