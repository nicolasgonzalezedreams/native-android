package com.odigeo.app.android.lib.activities;

import android.os.Bundle;
import com.odigeo.app.android.lib.R;

/**
 * Created by Oscar Álvarez on 21/10/2014.
 */
public abstract class OdigeoLoginActivity extends OdigeoForegroundBaseActivity {

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_account_blocked);
    //        setContentView(R.layout.layout_restore_password);
  }
}
