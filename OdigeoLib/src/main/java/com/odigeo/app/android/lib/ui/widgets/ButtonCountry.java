package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.ui.widgets.base.ButtonWithAndWithoutHint;
import com.odigeo.data.entity.booking.Country;

/**
 * Created by emiliano.desantis on 02/10/2014.
 */
public class ButtonCountry extends ButtonWithAndWithoutHint {
  private Country country;

  public ButtonCountry(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public final int getEmptyLayout() {
    return R.layout.layout_form_text_field_empty;
  }

  @Override public final int getLayout() {
    return R.layout.layout_form_text_field;
  }

  public final Country getCountry() {
    return country;
  }

  public final void setCountry(Country country) {
    this.country = country;

    if (country == null) {
      isEmpty = false;
      setText(null);
    } else {
      setText(country.getName());
    }
  }
}
