package com.odigeo.app.android.lib.models.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Javier Marsicano on 27/04/16.
 */
public class InvoiceRequestDTO {

  @SerializedName("required") private boolean mRequired;
  @SerializedName("professional") private boolean mProfessional;
  @SerializedName("fullName") private String mFullName;
  @SerializedName("address") private String mAddress;
  @SerializedName("city") private String mCity;
  @SerializedName("postalCode") private String mPostalCode;
  @SerializedName("country") private String mCountry;
  @SerializedName("id") private String mId;
  @SerializedName("invoiceMail") private String mInvoiceMail;

  public boolean isRequired() {
    return mRequired;
  }

  public void setRequired(boolean required) {
    this.mRequired = required;
  }

  public boolean isProfessional() {
    return mProfessional;
  }

  public void setProfessional(boolean professional) {
    this.mProfessional = professional;
  }

  public String getFullName() {
    return mFullName;
  }

  public void setFullName(String fullName) {
    this.mFullName = fullName;
  }

  public String getAddress() {
    return mAddress;
  }

  public void setAddress(String address) {
    this.mAddress = address;
  }

  public String getCity() {
    return mCity;
  }

  public void setCity(String city) {
    this.mCity = city;
  }

  public String getPostalCode() {
    return mPostalCode;
  }

  public void setPostalCode(String postalCode) {
    this.mPostalCode = postalCode;
  }

  public String getCountry() {
    return mCountry;
  }

  public void setCountry(String country) {
    this.mCountry = country;
  }

  public String getId() {
    return mId;
  }

  public void setId(String id) {
    this.mId = id;
  }

  public String getInvoiceMail() {
    return mInvoiceMail;
  }

  public void setInvoiceMail(String invoiceMail) {
    this.mInvoiceMail = invoiceMail;
  }
}
