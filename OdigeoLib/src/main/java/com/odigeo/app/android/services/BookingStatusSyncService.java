package com.odigeo.app.android.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.receivers.BookingStatusAlarmReceiver;
import com.odigeo.interactors.UpdateBookingStatusInteractor;

public class BookingStatusSyncService extends IntentService {

  public BookingStatusSyncService() {
    super("SchedulingService");
  }

  @Override protected void onHandleIntent(Intent intent) {
    Log.d("BookingStatus", "BookingStatusSyncService.onHandleIntent");
    UpdateBookingStatusInteractor updateBookingStatusInteractor =
        AndroidDependencyInjector.getInstance().provideUpdateBookingStatusInteractor();
    updateBookingStatusInteractor.updateBookingStatus(true);
    BookingStatusAlarmReceiver.completeWakefulIntent(intent);
  }
}
