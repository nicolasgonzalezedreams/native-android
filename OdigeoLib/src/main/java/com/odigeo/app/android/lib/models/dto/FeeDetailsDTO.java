package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author miguel
 */
@Deprecated public class FeeDetailsDTO implements Serializable {

  protected BigDecimal tax;
  protected BigDecimal discount;

  /**
   * Gets the value of the tax property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getTax() {
    return tax;
  }

  /**
   * Sets the value of the tax property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setTax(BigDecimal value) {
    this.tax = value;
  }

  /**
   * Gets the value of the discount property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getDiscount() {
    return discount;
  }

  /**
   * Sets the value of the discount property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setDiscount(BigDecimal value) {
    this.discount = value;
  }
}
