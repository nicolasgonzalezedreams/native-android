package com.odigeo.app.android.view.wrappers;

import com.odigeo.data.entity.extensions.UICarrier;
import java.io.Serializable;
import java.util.List;

/**
 * This class is used to wrap a list of {@link UICarrier} and give us the capacity to pass the list
 * through Activities as a {@link Serializable} Extra.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 25/09/15
 */
public class UICarrierWrapper implements Serializable {
  private final List<UICarrier> carriers;

  /**
   * Constructor
   *
   * @param carriers The list of {@link UICarrier} to be wrap.
   */
  public UICarrierWrapper(List<UICarrier> carriers) {
    this.carriers = carriers;
  }

  public List<UICarrier> getCarriers() {
    return carriers;
  }
}
