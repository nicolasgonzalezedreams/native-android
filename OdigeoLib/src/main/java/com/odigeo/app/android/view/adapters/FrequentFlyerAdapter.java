package com.odigeo.app.android.view.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.navigator.FrequentFlyerCodesNavigator;
import com.odigeo.app.android.view.FrequentFlyerCodesView.CardBean;
import com.odigeo.data.entity.extensions.UICarrier;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 21/09/15
 */
public class FrequentFlyerAdapter extends RecyclerView.Adapter<FrequentFlyerAdapter.ViewHolder> {
  private final List<CardBean> items;
  private final Activity activity;
  private final OdigeoImageLoader mImageLoader;

  public FrequentFlyerAdapter(List<CardBean> items, Activity activity) {
    this.items = items;
    this.activity = activity;
    mImageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.widget_frequent_flyer_code, parent, false);
    return new ViewHolder(v);
  }

  @Override public void onBindViewHolder(ViewHolder holder, final int position) {
    holder.airline.setText(items.get(position).getCarrier().getName());

    mImageLoader.load(holder.airlineView, items.get(position).getCarrier().getImageUrl(),
        R.drawable.ff_carrier_icon_placeholder);

    holder.code.setText(items.get(position).getFrequentFlyer().getFrequentFlyerNumber());

    final List<UICarrier> carriers = new ArrayList<>();

    carriers.add(items.get(position).getCarrier());

    holder.editView.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        ((FrequentFlyerCodesNavigator) activity).showDetail(items.get(position).getFrequentFlyer(),
            carriers, position);
      }
    });
  }

  /**
   * Add a new Frequent Flyer Code to the list.
   *
   * @param passengerCode code of frequent flyer.
   * @param carrier carrier of frequent flyer code.
   */
  public void addNewItem(long frequentFlyerId, String passengerCode, UICarrier carrier) {
    CardBean item = new CardBean(frequentFlyerId, passengerCode, carrier);
    items.add(item);
    notifyDataSetChanged();
  }

  /**
   * Remove the item in the position selected.
   *
   * @param position Position to be removed.
   */
  public void removeItem(int position) {
    items.remove(position);
    notifyDataSetChanged();
  }

  /**
   * Return the current list of frequent flyer codes as {@link UserFrequentFlyer} objects.
   *
   * @return The list of {@link UserFrequentFlyer} on the view.
   */
  public List<UserFrequentFlyer> getFrequentFlyerCodes() {
    List<UserFrequentFlyer> list = new ArrayList<>();
    for (CardBean bean : items) {
      UserFrequentFlyer code = new UserFrequentFlyer();
      code.setAirlineCode(bean.getCarrier().getCode());
      code.setFrequentFlyerNumber(bean.getFrequentFlyer().getFrequentFlyerNumber());
      code.setFrequentFlyerId(bean.getFrequentFlyer().getFrequentFlyerId());
      list.add(code);
    }
    return list;
  }

  @Override public int getItemCount() {
    return items.size();
  }

  /**
   * View Holder for the Frequent Flyer Card.
   */
  class ViewHolder extends RecyclerView.ViewHolder {

    ImageView airlineView;
    ImageView editView;
    TextView code;
    TextView airline;
    View cardView;

    ViewHolder(View view) {
      super(view);
      airlineView = ((ImageView) view.findViewById(R.id.imageViewFFCAirline));
      editView = ((ImageView) view.findViewById(R.id.imageViewEdit));
      code = ((TextView) view.findViewById(R.id.textViewFFCcode));
      airline = ((TextView) view.findViewById(R.id.textViewFFCairline));
      cardView = view.findViewById(R.id.cardViewFFC);
    }
  }
}