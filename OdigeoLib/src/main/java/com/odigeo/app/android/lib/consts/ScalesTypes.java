package com.odigeo.app.android.lib.consts;

/**
 * Created by Irving on 15/10/2014.
 */
public enum ScalesTypes {
  DIRECT, ONE_SCALE, MORE_SCALES
}
