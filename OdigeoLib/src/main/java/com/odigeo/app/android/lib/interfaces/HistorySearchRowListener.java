package com.odigeo.app.android.lib.interfaces;

import com.odigeo.app.android.lib.config.SearchOptions;

/**
 * Created by Irving Lóp on 13/10/2014.
 */
public interface HistorySearchRowListener {
  void onClickRowSearch(SearchOptions searchOptions);
}
