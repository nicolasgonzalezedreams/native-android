package com.odigeo.app.android.navigator;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.view.MenuItem;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.utils.deeplinking.DeepLinkingUtil;
import com.odigeo.app.android.utils.deeplinking.exception.DeepLinkingException;
import com.odigeo.app.android.view.MyTripsManualImportView;
import com.odigeo.app.android.view.MyTripsView;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.contracts.navigators.MyTripsNavigatorInterface;
import java.util.Map;

import static com.odigeo.app.android.lib.consts.Constants.REQUEST_CODE_DO_LOGIN;
import static com.odigeo.app.android.view.constants.OneCMSKeys.SSO_ND_TRIPS;

public class MyTripsNavigator extends BaseNavigator implements MyTripsNavigatorInterface {

  private MyTripsView view;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view);

    if (savedInstanceState == null) {
      view = MyTripsView.newInstance();
      getSupportFragmentManager().beginTransaction().add(R.id.fl_container, view).commit();
    } else {
      view = (MyTripsView) getSupportFragmentManager().findFragmentById(R.id.fl_container);
    }

    setNavigationTitle(localizables.getString(SSO_ND_TRIPS));
    readDeepLinkIntent();
  }

  @Override public void navigateToTripDetails(Booking booking, int position) {
    Bundle extras = new Bundle();
    ActivityOptionsCompat optionsCompat;
    if (booking.isPastBooking()) {
      optionsCompat = view.getOptionsForPastBookingTransition(position);
    } else if (booking.isOneWay()) {
      Pair<Bundle, ActivityOptionsCompat> params =
          view.getOptionsForUpcomingOneWayBookingTransition(position);
      extras = params.first;
      optionsCompat = params.second;
    } else {
      Pair<Bundle, ActivityOptionsCompat> params =
          view.getOptionsForUpcomingRoundTripBookingTransition(position);
      extras = params.first;
      optionsCompat = params.second;
    }
    Intent intent = MyTripDetailsNavigator.startIntent(this, booking);
    intent.putExtras(extras);
    ActivityCompat.startActivity(MyTripsNavigator.this, intent, optionsCompat.toBundle());
  }

  @Override public void navigateToTripDetailsWithoutTransition(Booking booking) {
    Intent intent = MyTripDetailsNavigator.startIntent(this, booking);
    startActivity(intent);
  }

  @Override public void navigateToPastTripDetails(Booking booking, int position) {
    ActivityOptionsCompat optionsCompat = view.getOptionsForPastBookingTransition(position);
    Intent intent = new Intent(MyTripsNavigator.this, MyTripDetailsNavigator.class);
    intent.putExtra(MyTripDetailsNavigator.EXTRA_BOOKING, booking);
    ActivityCompat.startActivity(MyTripsNavigator.this, intent, optionsCompat.toBundle());
  }

  @Override public void navigateToImportMyTrips() {
    replaceFragmentWithBackStack(R.id.fl_container, MyTripsManualImportView.newInstance());
    setNavigationTitle(
        LocalizablesFacade.getString(this, "importtrip_navigationbar_title").toString());
  }

  @Override public void navigateToSearchFlights() {
    //TODO CHANGE THIS
    Intent intent = new Intent(MyTripsNavigator.this,
        ((OdigeoApp) getApplication()).getSearchFlightsActivityClass());
    startActivity(intent);
  }

  @Override public void navigateToLogin() {
    Intent intent = LoginNavigator.startIntent(this);
    startActivityForResult(intent, REQUEST_CODE_DO_LOGIN);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    if (id == android.R.id.home) {
      tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS,
          TrackerConstants.ACTION_NAVIGATION_ELEMENTS, TrackerConstants.LABEL_GO_BACK);
      super.onBackPressed();
    }

    return super.onOptionsItemSelected(item);
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    switch (requestCode) {
      case REQUEST_CODE_DO_LOGIN:
        handleLoginResult(resultCode);
        break;
    }
  }

  private void handleLoginResult(int resultCode) {
    if (resultCode == RESULT_OK) {
      view.showLoggedInIsUserBookings();
    }
  }

  private void readDeepLinkIntent() {
    if (getIntent() != null) {
      String action = getIntent().getAction();
      Uri uri = getIntent().getData();
      if (Intent.ACTION_VIEW.equals(action)
          && uri != null
          && uri.getScheme() != null
          && uri.getScheme().equals(getString(R.string.deeplink_scheme_app_identifier))) {
        DeepLinkingUtil.DeepLinkType deepLinkType =
            DeepLinkingUtil.getCampaignCardType(getApplicationContext(), uri);
        switch (deepLinkType) {
          case TRIPS:
            try {
              Map.Entry<String, Booking> entry =
                  DeepLinkingUtil.getBookingFromDeepLinkingIntent(getIntent());
              if (entry != null) {
                Booking booking = entry.getValue();
                if (booking != null) {
                  navigateToTripDetailsWithoutTransition(booking);
                } else {
                  navigateToImportMyTrips();
                }
              }
            } catch (DeepLinkingException e) {
              e.printStackTrace();
            }
            break;
        }
      }
    }
  }
}