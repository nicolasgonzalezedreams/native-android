package com.odigeo.app.android.lib.models;

import com.odigeo.app.android.lib.models.dto.BuyerIdentificationTypeDTO;
import com.odigeo.data.entity.booking.Country;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by ManuelOrtiz on 13/10/2014.
 */
public class PassengerIdentification implements Serializable {

  private BuyerIdentificationTypeDTO identificationType;
  private String number;
  private Date expirationDate;
  private Country country;

  public PassengerIdentification(BuyerIdentificationTypeDTO identificationType, String number,
      Date expirationDate, Country country) {
    this.identificationType = identificationType;
    this.number = number;
    this.expirationDate = expirationDate;
    this.country = country;
  }

  public PassengerIdentification() {
  }

  public final void setPassengerIdentification(BuyerIdentificationTypeDTO identificationType,
      String number, Date expirationDate, Country country) {
    this.identificationType = identificationType;
    this.number = number;
    this.expirationDate = expirationDate;
    this.country = country;
  }

  public final BuyerIdentificationTypeDTO getIdentificationType() {
    return identificationType;
  }

  public final void setIdentificationType(BuyerIdentificationTypeDTO identificationType) {
    this.identificationType = identificationType;
  }

  public final String getNumber() {
    return number;
  }

  public final void setNumber(String number) {
    this.number = number;
  }

  public final Date getExpirationDate() {
    return expirationDate;
  }

  public final void setExpirationDate(Date expirationDate) {
    this.expirationDate = expirationDate;
  }

  public final Country getCountry() {
    return country;
  }

  public final void setCountry(Country country) {
    this.country = country;
  }
}
