package com.odigeo.app.android.utils.deeplinking.extractors;

import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.data.entity.TravelType;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 25/04/16
 */
public class FlightTypeExtractor implements DeepLinkingExtractor {
  private static final String ONE_WAY = "O";
  private static final String ROUND = "R";

  @Override public void extractParameter(SearchOptions.Builder builder, String key, String value) {
    switch (value) {
      case ONE_WAY:
        builder.setTravelType(TravelType.SIMPLE);
        break;
      case ROUND:
        builder.setTravelType(TravelType.ROUND);
        break;
      default:
        builder.setTravelType(TravelType.MULTIDESTINATION);
        break;
    }
  }
}