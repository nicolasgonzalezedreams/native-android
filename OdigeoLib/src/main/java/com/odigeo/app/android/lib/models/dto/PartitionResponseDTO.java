package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for partitionResponse complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="partitionResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="dimensionLabel" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="partitionNumber" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class PartitionResponseDTO {

  protected String dimensionLabel;
  protected Integer partitionNumber;

  /**
   * Gets the value of the dimensionLabel property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getDimensionLabel() {
    return dimensionLabel;
  }

  /**
   * Sets the value of the dimensionLabel property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setDimensionLabel(String value) {
    this.dimensionLabel = value;
  }

  /**
   * Gets the value of the partitionNumber property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getPartitionNumber() {
    return partitionNumber;
  }

  /**
   * Sets the value of the partitionNumber property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setPartitionNumber(Integer value) {
    this.partitionNumber = value;
  }
}
