package com.odigeo.app.android.view.custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.utils.GroundTransportationUrlBuilder;
import com.odigeo.app.android.view.BaseCustomView;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;

public abstract class GroundTransportationViewBase extends BaseCustomView {

  public static final String TRIP_TYPE_OW = "one_way";
  public static final String TRIP_TYPE_RT = "round_trip";
  public static final String TRIP_TYPE_MS = "multi_stop";
  private Booking booking;
  private OdigeoImageLoader<ImageView> imageLoader;
  private GroundTransportationWidgetListener listener;
  private ImageView ivGroundTransportation;
  private TextView tvTitle;
  private TextView tvDescription;
  private Button btnActionButton;
  private GroundTransportationUrlBuilder groundTransportationUrlBuilder;

  public GroundTransportationViewBase(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    initOneCMSText();
  }

  public GroundTransportationViewBase(Context context, Booking booking,
      GroundTransportationWidgetListener listener) {
    super(context);
    this.booking = booking;
    this.listener = listener;
    this.groundTransportationUrlBuilder = new GroundTransportationUrlBuilder(booking);
    initOneCMSText();
  }

  @Override public void initComponent() {
    imageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();
    ivGroundTransportation = (ImageView) findViewById(R.id.IvGroundTransportation);
    tvTitle = (TextView) findViewById(R.id.TvTitle);
    tvDescription = (TextView) findViewById(R.id.TvDescription);
    btnActionButton = (Button) findViewById(R.id.btnActionButton);

    setListeners();
    loadImage();
  }

  @Override public int getComponentLayout() {
    return R.layout.layout_ground_transportation_widget;
  }

  @Override public void initOneCMSText() {
    tvTitle.setText(getTitle());
    tvDescription.setText(getDescription());
    btnActionButton.setText(getCTAText());
  }

  private void setListeners() {
    btnActionButton.setOnClickListener(new OnClickListener() {

      @Override public void onClick(View view) {
        trackInteraction();
        String url = getWebViewUrl();
        listener.navigateToGroundTransportation(url);
      }
    });
  }

  protected void loadImage() {
    String imageUrl = getImageUrl();
    imageLoader.load(ivGroundTransportation, imageUrl, R.drawable.campaign_card_placeholder);
  }

  protected String getCityName() {
    return booking == null ? null : booking.getFirstSegment().getArrivalCity().getCityName();
  }

  protected String getWebViewUrl() {
    return groundTransportationUrlBuilder.build(getRawUrl());
  }

  protected abstract void trackInteraction();

  protected abstract String getTitle();

  protected abstract String getDescription();

  protected abstract String getCTAText();

  protected abstract String getRawUrl();

  protected abstract String getImageUrl();

  public interface GroundTransportationWidgetListener {
    void navigateToGroundTransportation(String url);
  }
}
