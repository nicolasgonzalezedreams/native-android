package com.odigeo.app.android.lib.models.dto;

import java.util.ArrayList;
import java.util.List;

public class RoomGroupDealDTO {

  protected RoomGroupDealInformationDTO roomGroupDealInformation;
  protected String key;
  protected List<String> roomsKeys;

  public RoomGroupDealInformationDTO getRoomGroupDealInformation() {
    return roomGroupDealInformation;
  }

  public void setRoomGroupDealInformation(RoomGroupDealInformationDTO value) {
    this.roomGroupDealInformation = value;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String value) {
    this.key = value;
  }

  public List<String> getRoomsKeys() {
    if (roomsKeys == null) {
      roomsKeys = new ArrayList<String>();
    }
    return this.roomsKeys;
  }
}
