package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class MovementDTO {

  protected String orderId;
  protected String code;
  protected MovementErrorTypeDTO errorType;
  protected String errorDescription;
  protected MovementActionDTO action;
  protected MovementStatusDTO status;
  protected BigDecimal amount;
  protected String creationDate;
  protected String commerceId;
  protected String terminalId;
  protected String transactionId;
  protected String transactionSubId;
  protected String commerceStatus;
  protected String commerceStatusSubcode;

  /**
   * Gets the value of the orderId property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getOrderId() {
    return orderId;
  }

  /**
   * Sets the value of the orderId property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setOrderId(String value) {
    this.orderId = value;
  }

  /**
   * Gets the value of the code property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCode() {
    return code;
  }

  /**
   * Sets the value of the code property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCode(String value) {
    this.code = value;
  }

  /**
   * Gets the value of the errorType property.
   *
   * @return possible object is
   * {@link MovementErrorType }
   */
  public MovementErrorTypeDTO getErrorType() {
    return errorType;
  }

  /**
   * Sets the value of the errorType property.
   *
   * @param value allowed object is
   * {@link MovementErrorType }
   */
  public void setErrorType(MovementErrorTypeDTO value) {
    this.errorType = value;
  }

  /**
   * Gets the value of the errorDescription property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getErrorDescription() {
    return errorDescription;
  }

  /**
   * Sets the value of the errorDescription property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setErrorDescription(String value) {
    this.errorDescription = value;
  }

  /**
   * Gets the value of the action property.
   *
   * @return possible object is
   * {@link MovementAction }
   */
  public MovementActionDTO getAction() {
    return action;
  }

  /**
   * Sets the value of the action property.
   *
   * @param value allowed object is
   * {@link MovementAction }
   */
  public void setAction(MovementActionDTO value) {
    this.action = value;
  }

  /**
   * Gets the value of the status property.
   *
   * @return possible object is
   * {@link MovementStatus }
   */
  public MovementStatusDTO getStatus() {
    return status;
  }

  /**
   * Sets the value of the status property.
   *
   * @param value allowed object is
   * {@link MovementStatus }
   */
  public void setStatus(MovementStatusDTO value) {
    this.status = value;
  }

  /**
   * Gets the value of the amount property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getAmount() {
    return amount;
  }

  /**
   * Sets the value of the amount property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setAmount(BigDecimal value) {
    this.amount = value;
  }

  /**
   * Gets the value of the creationDate property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCreationDate() {
    return creationDate;
  }

  /**
   * Sets the value of the creationDate property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCreationDate(String value) {
    this.creationDate = value;
  }

  /**
   * Gets the value of the commerceId property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCommerceId() {
    return commerceId;
  }

  /**
   * Sets the value of the commerceId property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCommerceId(String value) {
    this.commerceId = value;
  }

  /**
   * Gets the value of the terminalId property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getTerminalId() {
    return terminalId;
  }

  /**
   * Sets the value of the terminalId property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setTerminalId(String value) {
    this.terminalId = value;
  }

  /**
   * Gets the value of the transactionId property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getTransactionId() {
    return transactionId;
  }

  /**
   * Sets the value of the transactionId property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setTransactionId(String value) {
    this.transactionId = value;
  }

  /**
   * Gets the value of the transactionSubId property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getTransactionSubId() {
    return transactionSubId;
  }

  /**
   * Sets the value of the transactionSubId property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setTransactionSubId(String value) {
    this.transactionSubId = value;
  }

  /**
   * Gets the value of the commerceStatus property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCommerceStatus() {
    return commerceStatus;
  }

  /**
   * Sets the value of the commerceStatus property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCommerceStatus(String value) {
    this.commerceStatus = value;
  }

  /**
   * Gets the value of the commerceStatusSubcode property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCommerceStatusSubcode() {
    return commerceStatusSubcode;
  }

  /**
   * Sets the value of the commerceStatusSubcode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCommerceStatusSubcode(String value) {
    this.commerceStatusSubcode = value;
  }
}
