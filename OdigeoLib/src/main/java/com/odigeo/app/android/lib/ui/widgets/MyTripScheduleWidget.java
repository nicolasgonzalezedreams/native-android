package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;

/**
 * Created by emiliano.desantis on 10/10/2014.
 */
public class MyTripScheduleWidget extends LinearLayout {
  protected static final int DEFAULT_TEXT_SIZE = 0;
  private String titleText;
  private String hourText;
  private String dateText;
  private int titleTextSize;
  private int hourTextSize;
  private int dateTextSize;
  private ColorStateList titleTextColor;
  private ColorStateList hourTextColor;
  private ColorStateList dateTextColor;
  private CustomTextView txtTitle;
  private CustomTextView txtHour;
  private CustomTextView txtDate;

  public MyTripScheduleWidget(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray aAttrs =
        context.obtainStyledAttributes(attrs, R.styleable.MyTripScheduleWidget, 0, 0);

    titleText = aAttrs.getString(R.styleable.MyTripScheduleWidget_titleText);
    titleTextSize = aAttrs.getDimensionPixelSize(R.styleable.MyTripScheduleWidget_titleTextSize,
        DEFAULT_TEXT_SIZE);
    titleTextColor = aAttrs.getColorStateList(R.styleable.MyTripScheduleWidget_titleColor);
    hourText = aAttrs.getString(R.styleable.MyTripScheduleWidget_hourText);
    hourTextSize = aAttrs.getDimensionPixelSize(R.styleable.MyTripScheduleWidget_hourTextSize,
        DEFAULT_TEXT_SIZE);
    hourTextColor = aAttrs.getColorStateList(R.styleable.MyTripScheduleWidget_hourTextColor);
    dateText = aAttrs.getString(R.styleable.MyTripScheduleWidget_dateText);
    dateTextSize = aAttrs.getDimensionPixelSize(R.styleable.MyTripScheduleWidget_dateTextSize,
        DEFAULT_TEXT_SIZE);
    dateTextColor = aAttrs.getColorStateList(R.styleable.MyTripScheduleWidget_dateTextColor);

    aAttrs.recycle();

    inflateLayout();
  }

  private void inflateLayout() {
    inflate(this.getContext(), R.layout.layout_mytrip_widget_schedule, this);

    txtTitle = (CustomTextView) findViewById(R.id.txtScheduleType);
    txtHour = (CustomTextView) findViewById(R.id.txtHour);
    txtDate = (CustomTextView) findViewById(R.id.txtDate);

    setTitle(titleText);
    setTitleTextColor(titleTextColor);
    setTitleTextSize(titleTextSize);

    setHour(hourText);
    setHourTextColor(hourTextColor);
    setHourTextSize(hourTextSize);

    setDate(dateText);
    setDateTextColor(dateTextColor);
    setDateTextSize(dateTextSize);
  }

  public final String getTitle() {
    return txtTitle.getText().toString();
  }

  public final void setTitle(String titleText) {
    this.titleText = titleText;
    txtTitle.setText(titleText);
  }

  public final String getHour() {
    return txtHour.getText().toString();
  }

  public final void setHour(String hourText) {
    this.hourText = hourText;
    txtHour.setText(hourText);
  }

  public final String getDate() {
    return txtDate.getText().toString();
  }

  public final void setDate(String dateText) {
    this.dateText = dateText;
    txtDate.setText(dateText);
  }

  protected final int getTitleTextSize() {
    return titleTextSize;
  }

  protected final void setTitleTextSize(int titleTextSize) {
    this.titleTextSize = titleTextSize;

    setTextViewSize(txtTitle, titleTextSize);
  }

  protected final int getHourTextSize() {
    return hourTextSize;
  }

  protected final void setHourTextSize(int hourTextSize) {
    this.hourTextSize = hourTextSize;

    setTextViewSize(txtHour, hourTextSize);
  }

  protected final int getDateTextSize() {
    return dateTextSize;
  }

  protected final void setDateTextSize(int dateTextSize) {
    this.dateTextSize = dateTextSize;

    setTextViewSize(txtDate, dateTextSize);
  }

  protected final ColorStateList getTitleTextColor() {
    return titleTextColor;
  }

  protected final void setTitleTextColor(ColorStateList titleTextColor) {
    this.titleTextColor = titleTextColor;
    txtTitle.setTextColor(titleTextColor);
  }

  protected final ColorStateList getHourTextColor() {
    return hourTextColor;
  }

  protected final void setHourTextColor(ColorStateList hourTextColor) {
    this.hourTextColor = hourTextColor;
    txtHour.setTextColor(hourTextColor);
  }

  protected final ColorStateList getDateTextColor() {
    return dateTextColor;
  }

  protected final void setDateTextColor(ColorStateList dateTextColor) {
    this.dateTextColor = dateTextColor;
    txtDate.setTextColor(dateTextColor);
  }

  protected final void setTextViewSize(TextView textViewItem, int textSizeValue) {
    if (textSizeValue > 0) {
      textViewItem.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeValue);
    }
  }
}
