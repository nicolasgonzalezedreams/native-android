package com.odigeo.app.android.view.imageutils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ImageView;
import com.odigeo.app.android.lib.R;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;

public class BookingImageUtil {

  public void loadBookingImage(Context context, OdigeoImageLoader<ImageView> imageLoader,
      ImageView imageView, Booking booking, boolean addDefaultAnimations) {
    if (booking.getTripType().equals(Booking.TRIP_TYPE_MULTI_SEGMENT)) {
      imageLoader.load(imageView, R.drawable.trip_default_placeholder_image, addDefaultAnimations);
    } else {
      String imageUrl = getBookingImage(context, booking);
      imageLoader.load(imageView, imageUrl, R.drawable.trips_loading_placeholder,
          R.drawable.trip_default_placeholder_image, addDefaultAnimations);
    }
  }

  public void loadRoundedBookingImage(Context context, OdigeoImageLoader<ImageView> imageLoader,
      ImageView imageView, Booking booking) {
    int radius = (int) context.getResources().getDimension(R.dimen.mytrips_radius_round_icon);
    if (booking.getTripType().equals(Booking.TRIP_TYPE_MULTI_SEGMENT)) {
      imageLoader.loadRoundedTransformation(imageView, R.drawable.trip_default_placeholder_image,
          radius);
    } else {
      String imageUrl = getBookingImage(context, booking);
      imageLoader.loadRoundedTransformation(imageView, imageUrl,
          R.drawable.trips_loading_placeholder, R.drawable.trip_default_placeholder_image, radius);
    }
  }

  private String getBookingImage(Context context, @NonNull Booking booking) {
    String path = context.getString(R.string.booking_image_path);

    if (booking.getTripType().equals(Booking.TRIP_TYPE_MULTI_SEGMENT)) {
      return path + String.format(context.getString(R.string.booking_multidestiny_placeholder),
          booking.getMultiDestinyIndex());
    } else {
      Segment firstSegment = booking.getFirstSegment();
      Section lastSection = firstSegment.getLastSection();

      return path + String.format(context.getString(R.string.booking_image_filename),
          lastSection.getTo().getLocationCode());
    }
  }
}
