package com.odigeo.app.android.view.dialogs;

import java.util.Date;

/**
 * Created by carlos.navarrete on 11/09/15.
 */
@Deprecated public interface OnGetDateSelectedListener {

  void onGetDate(Date date);
}
