package com.odigeo.app.android.view;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.navigator.NavigationDrawerNavigator;
import com.odigeo.app.android.utils.deeplinking.DeepLinkingUtil;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.SearchDropOffCard;
import com.odigeo.data.entity.extensions.UISearchDropOffCard;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.SearchDropOffCardViewPresenter;
import com.odigeo.presenter.contracts.views.SearchDropOffCardViewInterface;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SearchDropOffCardView extends BaseView<SearchDropOffCardViewPresenter>
    implements SearchDropOffCardViewInterface {

  private static final String KEY_DROPOFF_CARD = "KEY_PROMO_CARD";
  protected SearchDropOffCard card;
  private TextView tvDropOffDeparture;
  private TextView tvDropOffArrival;
  private TextView tvDropOffDates;
  private TextView tvDropOffBody;
  private TextView tvDropOffPassengers;
  private TextView tvDropOffActionButtonOne;
  private TextView tvDropOffActionButtonTwo;
  private ImageView ivDropOffImage;
  private int cardPosition;

  private SimpleDateFormat dateFormat;
  private SimpleDateFormat deeplinkDateFormat;

  private OdigeoImageLoader imageLoader;

  private OdigeoApp application;

  private boolean hasClickedOnDeeplink = false;

  public static Fragment newInstance(SearchDropOffCard card) {
    Bundle args = new Bundle();
    args.putParcelable(KEY_DROPOFF_CARD, new UISearchDropOffCard(card));
    SearchDropOffCardView fragment = new SearchDropOffCardView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    application = (OdigeoApp) getActivity().getApplication();
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    getCardFromArguments();
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override protected SearchDropOffCardViewPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideSearchDropOffCardViewPresenter(this, (NavigationDrawerNavigator) getActivity());
  }

  @Override public void onPause() {
    super.onPause();
    if (hasClickedOnDeeplink) mPresenter.updateDropOffCardStatus();
  }

  @Override protected int getFragmentLayout() {
    return R.layout.dropoff_card;
  }

  @Override protected void initComponent(View view) {
    dateFormat =
        OdigeoDateUtils.getGmtDateFormat(getContext().getString(R.string.templates__time4));
    deeplinkDateFormat =
        OdigeoDateUtils.getGmtDateFormat(getContext().getString(R.string.format_deeplink_date));
    imageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();

    tvDropOffDeparture = (TextView) view.findViewById(R.id.TvDropOffDeparture);
    tvDropOffArrival = (TextView) view.findViewById(R.id.TvDropOffArrival);
    tvDropOffDates = (TextView) view.findViewById(R.id.TvDropOffDates);
    tvDropOffBody = (TextView) view.findViewById(R.id.TvDropOffBody);
    tvDropOffPassengers = (TextView) view.findViewById(R.id.TvDropOffPassengers);
    tvDropOffActionButtonOne = (TextView) view.findViewById(R.id.TvDropOffActionButtonOne);
    tvDropOffActionButtonTwo = (TextView) view.findViewById(R.id.TvDropOffActionButtonTwo);
    ivDropOffImage = (ImageView) view.findViewById(R.id.IvDropOffImage);
    initDataDropOffCard();
    loadCardImage();
  }

  @Override protected void setListeners() {
    tvDropOffActionButtonOne.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        try {
          String deepLinkUrl = getBookNowDeeplinkUrl(card);
          if (!TextUtils.isEmpty(deepLinkUrl)) {
            hasClickedOnDeeplink = true;
            String label = getTrackerLabel(TrackerConstants.LABEL_CARD_DROPOFF_LAUNCH);
            mTracker.trackMTTCardEvent(label);
            openDeeplinkInActionButton(deepLinkUrl);
          }
        } catch (Exception exception) {
          logIntoCrashlytics(exception);
        }
      }
    });

    tvDropOffActionButtonTwo.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        try {
          String deeplinkUrl = getEditSearchDeeplinkUrl(card);
          if (!TextUtils.isEmpty(deeplinkUrl)) {
            hasClickedOnDeeplink = true;
            String label = getTrackerLabel(TrackerConstants.LABEL_CARD_DROPOFF_EDIT);
            mTracker.trackMTTCardEvent(label);
            openDeeplinkInActionButton(deeplinkUrl);
          }
        } catch (Exception exception) {
          logIntoCrashlytics(exception);
        }
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    tvDropOffBody.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.DROP_OFF_DESCRIPTION));
    tvDropOffActionButtonOne.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.DROP_OFF_SEARCH_LAUNCH));
    tvDropOffActionButtonTwo.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.DROP_OFF_SEARCH_EDIT));
  }

  private void openDeeplinkInActionButton(String url) {
    try {
      Intent deepLinkIntent = new Intent(Intent.ACTION_VIEW);
      deepLinkIntent.setData(Uri.parse(url));
      startActivity(deepLinkIntent);
    } catch (ActivityNotFoundException e) {
      Log.e(getTag(), e.getMessage(), e);
      logIntoCrashlytics(e);
    }
  }

  @Override public void setCardPosition(int position) {
    Bundle bundle = getArguments();
    bundle.putInt(Card.KEY_CARD_POSITION, position);
    this.setArguments(bundle);
  }

  private void getCardFromArguments() {
    Bundle bundle = getArguments();

    if (bundle.containsKey(KEY_DROPOFF_CARD)) {
      card = (UISearchDropOffCard) bundle.getParcelable(KEY_DROPOFF_CARD);
    }

    if (bundle.containsKey(Card.KEY_CARD_POSITION)) {
      cardPosition = bundle.getInt(Card.KEY_CARD_POSITION);
    } else {
      cardPosition = 1;
    }
  }

  private void loadCardImage() {
    String imageUrl = ViewUtils.getSearchImage(getActivity(), card.getArrivalCityIATA());
    imageLoader.load(ivDropOffImage, imageUrl, R.drawable.campaign_card_placeholder);
  }

  private void drawSegment() {
    String departureCity = mPresenter.getCityName(card.getDepartureCityIATA());
    String arrivalCity = mPresenter.getCityName(card.getArrivalCityIATA());

    tvDropOffDeparture.setText(departureCity);
    tvDropOffArrival.setText(arrivalCity);
  }

  private void drawDates() {
    Date date = new Date(card.getDepartureDate());
    tvDropOffDates.append(dateFormat.format(date));

    if (card.getArrivalDate() > 0) {
      tvDropOffDates.append(" " + getResources().getString(R.string.format_added_date_aux) + " ");
      date = new Date(card.getArrivalDate());
      tvDropOffDates.append(dateFormat.format(date));
    }
  }

  private void initDataDropOffCard() {
    drawSegment();
    drawDates();
    drawPassengers();
  }

  private void drawPassengers() {
    int totalPassengers = card.getNumAdults() + card.getNumChildren() + card.getNumInfants();
    tvDropOffPassengers.setText(String.valueOf(totalPassengers));
  }

  private void logIntoCrashlytics(Throwable throwable) {
    if (application != null) {
      application.trackNonFatal(throwable);
      application.setBool("Card is null", card == null);
      application.setBool("Card is added", isAdded());
      application.setBool("Card is detached", isDetached());
      application.setBool("Card is hidden", isHidden());
      application.setBool("Card is in layout", isInLayout());
      application.setBool("Card is removing", isRemoving());
      application.setBool("Card is resumed", isResumed());
      application.setBool("Card is visible", isVisible());
      application.setBool("Arguments are null", getArguments() == null);
    }
  }

  public String getBookNowDeeplinkUrl(SearchDropOffCard dropOffCard) {
    String deeplink = getDeeplinkUrl(dropOffCard);
    deeplink += getParameterForDeeplink(DeepLinkingUtil.EXECUTE_SEARCH, String.valueOf(true));
    return deeplink;
  }

  public String getEditSearchDeeplinkUrl(SearchDropOffCard dropOffCard) {
    String deeplink = getDeeplinkUrl(dropOffCard);
    deeplink += getParameterForDeeplink(DeepLinkingUtil.EXECUTE_SEARCH, String.valueOf(false));
    return deeplink;
  }

  private String getDeeplinkUrl(SearchDropOffCard dropOffCard) {
    String deeplink = getSearchDeeplinkHeader();
    deeplink += getParameterForDeeplink(DeepLinkingUtil.TYPE, dropOffCard.getTripType().toString());

    Date departureDate = OdigeoDateUtils.createDate(dropOffCard.getDepartureDate());
    deeplink += getParameterForDeeplink(DeepLinkingUtil.DEPARTURE_TIME,
        deeplinkDateFormat.format(departureDate));

    deeplink += getParameterForDeeplink(DeepLinkingUtil.FROM, dropOffCard.getDepartureCityIATA());
    deeplink += getParameterForDeeplink(DeepLinkingUtil.TO, dropOffCard.getArrivalCityIATA());
    deeplink +=
        getParameterForDeeplink(DeepLinkingUtil.ADULTS, String.valueOf(dropOffCard.getNumAdults()));
    deeplink += getParameterForDeeplink(DeepLinkingUtil.CHILDREN,
        String.valueOf(dropOffCard.getNumChildren()));
    deeplink += getParameterForDeeplink(DeepLinkingUtil.INFANTS,
        String.valueOf(dropOffCard.getNumInfants()));
    deeplink +=
        getParameterForDeeplink(DeepLinkingUtil.DIRECT, String.valueOf(dropOffCard.isDirect()));

    String cabinClass = dropOffCard.getCabinClass().name();

    deeplink += getParameterForDeeplink(DeepLinkingUtil.CLASS, cabinClass);

    if (dropOffCard.getArrivalDate() > 0) {
      Date arrivalDate = OdigeoDateUtils.createDate(dropOffCard.getArrivalDate());
      deeplink += getParameterForDeeplink(DeepLinkingUtil.RETURN_TIME,
          deeplinkDateFormat.format(arrivalDate));
    }

    deeplink += getParameterForDeeplink(DeepLinkingUtil.COLLECTION_METHOD, String.valueOf(false));
    deeplink += getParameterForDeeplink(DeepLinkingUtil.AIRLINE_CODES, String.valueOf(false));
    deeplink += getParameterForDeeplink(DeepLinkingUtil.INTERNAL_SEARCH, String.valueOf(true));

    return deeplink;
  }

  private String getParameterForDeeplink(String key, String value) {
    return DeepLinkingUtil.PARAMETER_SPLITTER + key + DeepLinkingUtil.VALUE_SPLITTER + value;
  }

  private String getSearchDeeplinkHeader() {
    String header = getActivity().getString(R.string.deeplink_scheme_app_identifier);
    header +=
        DeepLinkingUtil.HOST_SPLITTER + getActivity().getString(R.string.deeplink_host_search);
    header +=
        DeepLinkingUtil.ACTION_SPLITTER + getActivity().getString(R.string.deeplink_prefix_search);
    return header;
  }

  private String getTrackerLabel(String type) {
    return TrackerConstants.LABEL_CARD_SEARCH_CLICKED.replace(
        TrackerConstants.CARD_POSITION_IDENTIFIER, String.valueOf(cardPosition))
        .replace(TrackerConstants.CARD_TYPE_IDENTIFIER, type);
  }
}
