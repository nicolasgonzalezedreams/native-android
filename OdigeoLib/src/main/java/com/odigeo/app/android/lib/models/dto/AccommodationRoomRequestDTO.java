package com.odigeo.app.android.lib.models.dto;

import java.util.ArrayList;
import java.util.List;

public class AccommodationRoomRequestDTO {

  protected int numAdults;
  protected List<Integer> childrenAges;

  /**
   * Gets the value of the numAdults property.
   */
  public int getNumAdults() {
    return numAdults;
  }

  /**
   * Sets the value of the numAdults property.
   */
  public void setNumAdults(int value) {
    this.numAdults = value;
  }

  /**
   * Gets the value of the childrenAges property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the childrenAges property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getChildrenAges().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list
   * {@link Integer }
   */
  public List<Integer> getChildrenAges() {
    if (childrenAges == null) {
      childrenAges = new ArrayList<Integer>();
    }
    return this.childrenAges;
  }
}
