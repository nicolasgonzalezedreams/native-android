package com.odigeo.app.android.lib.models.dto;

import java.util.List;

public class InsuranceProviderBookingsDTO {

  protected List<InsuranceProviderBookingDTO> bookings;

  public List<InsuranceProviderBookingDTO> getBookings() {
    return bookings;
  }

  public void setBookings(List<InsuranceProviderBookingDTO> bookings) {
    this.bookings = bookings;
  }
}
