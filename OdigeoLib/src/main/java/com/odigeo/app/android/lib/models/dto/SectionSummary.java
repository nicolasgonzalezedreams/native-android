package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class SectionSummary implements Serializable {

  private LocationDTO origin;
  private LocationDTO destination;
  private String carrierCode;
  private String sectionId;
  private int segmentIndex;
  private int sectionIndex;

  public LocationDTO getOrigin() {
    return origin;
  }

  public void setOrigin(LocationDTO origin) {
    this.origin = origin;
  }

  public LocationDTO getDestination() {
    return destination;
  }

  public void setDestination(LocationDTO destination) {
    this.destination = destination;
  }

  public String getCarrierCode() {
    return carrierCode;
  }

  public void setCarrierCode(String carrierCode) {
    this.carrierCode = carrierCode;
  }

  public String getSectionId() {
    return sectionId;
  }

  public void setSectionId(String sectionId) {
    this.sectionId = sectionId;
  }

  public int getSegmentIndex() {
    return segmentIndex;
  }

  public void setSegmentIndex(int segmentIndex) {
    this.segmentIndex = segmentIndex;
  }

  public int getSectionIndex() {
    return sectionIndex;
  }

  public void setSectionIndex(int sectionIndex) {
    this.sectionIndex = sectionIndex;
  }
}
