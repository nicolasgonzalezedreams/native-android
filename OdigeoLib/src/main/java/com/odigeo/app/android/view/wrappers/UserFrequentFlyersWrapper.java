package com.odigeo.app.android.view.wrappers;

import com.odigeo.data.entity.userData.UserFrequentFlyer;
import java.io.Serializable;
import java.util.List;

/**
 * This class is used to wrap a list of {@link com.odigeo.app.android.lib.models.dto.FrequentFlyerCardCodeDTO}
 * and give us the capacity to pass the list through Activities as a {@link Serializable} Extra.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 25/09/15
 */
public class UserFrequentFlyersWrapper implements Serializable {
  private List<UserFrequentFlyer> flyerCardCodes;

  /**
   * Constructor
   *
   * @param frequentFlyers List to be wrapped
   */
  public UserFrequentFlyersWrapper(List<UserFrequentFlyer> frequentFlyers) {
    this.flyerCardCodes = frequentFlyers;
  }

  public List<UserFrequentFlyer> getFlyerCardCodes() {
    return flyerCardCodes;
  }
}
