package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.fragments.base.OdigeoNoResultsFragment;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;

/**
 * Created by manuel on 12/09/14.
 */
public class OdigeoNoResultsRoundFragment extends OdigeoNoResultsFragment {

  /**
   * Creates a new instance for this no result fragment
   *
   * @param searchOptions Search options used in the fragment
   * @return A new instance
   */
  public static OdigeoNoResultsRoundFragment newInstance(SearchOptions searchOptions) {
    OdigeoNoResultsRoundFragment noResultsRoundFragment = new OdigeoNoResultsRoundFragment();
    noResultsRoundFragment.setSearchOptions(searchOptions);
    return noResultsRoundFragment;
  }

  @Override public final void onAttach(Activity activity) {
    super.onAttach(activity);

    postGAEventSearchNoResults();

    postGAScreenSearchHadNoResults();
  }

  @Override public final boolean isDataValid() {
    FlightSegment segmentDeparture = getSearchOptions().getFlightSegments().get(0);
    FlightSegment segmentReturn = getSearchOptions().getFlightSegments().get(1);

    return segmentDeparture.getDate() != 0
        && segmentReturn.getDate() != 0
        && segmentDeparture.getDepartureCity() != null
        && segmentDeparture.getArrivalCity() != null
        && !segmentDeparture.getDepartureCity()
        .getIataCode()
        .equalsIgnoreCase(segmentDeparture.getArrivalCity().getIataCode());
  }

  @Override public final void setControlsValues() {
    super.setControlsValues();

    //Set the departure Date
    //if (getSearchOptions().getFlightSegments().get(1).getDate() != 0) {
    getSearchTripWidget().setReturnDate(
        OdigeoDateUtils.createDate(getSearchOptions().getFlightSegments().get(1).getDate()));
    //}
  }

  private void postGAScreenSearchHadNoResults() {

    BusProvider.getInstance().post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_NO_RESULTS));
  }

  private void postGAEventSearchNoResults() {

    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_RESULTS,
            GAnalyticsNames.ACTION_RESULTS_LIST, GAnalyticsNames.LABEL_SEARCH_NO_RESULTS));
  }
}
