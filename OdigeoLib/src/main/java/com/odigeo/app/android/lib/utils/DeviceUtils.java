package com.odigeo.app.android.lib.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Market;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.OdigeoScreenSize;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.http.conn.util.InetAddressUtils;
import org.jsoup.Jsoup;

/**
 * Created by ximena.perez on 19/03/2015.
 */
public final class DeviceUtils {

  /**
   * Save the correspondence between odigeo screen size and android device resolution, the
   * correspondence must be: <ul><li>S - LDPI</li><li>M - MDPI</li><li>L - HDPI</li><li>XL -
   * XHDPI</li><li>XXL - XXLDPI</li><li>XXL - XXXHDPI (Because Odigeo does not include this
   * resolution)</li></ul>
   */
  private static final Map<Integer, OdigeoScreenSize> ODIGEO_SCREEN_SIZE_MAP = new HashMap<>();

  private static final float MIN_FREE_SPACE = 5;
  private static final int BYTES = 1024;
  private static final int DECIMAL = 10;
  private static final double XXXHIGH = 4.0;
  private static final double XXHIGH = 3.0;
  private static final double XHIGH = 2.0;
  private static final double HIGH = 1.5;
  private static final double MEDIUM = 1.0;
  private static final int TIME_TIMEOUT = 30000;

  static {
    ODIGEO_SCREEN_SIZE_MAP.put(DisplayMetrics.DENSITY_LOW, OdigeoScreenSize.LOW);
    ODIGEO_SCREEN_SIZE_MAP.put(DisplayMetrics.DENSITY_MEDIUM, OdigeoScreenSize.MEDIUM);
    ODIGEO_SCREEN_SIZE_MAP.put(DisplayMetrics.DENSITY_HIGH, OdigeoScreenSize.HIGH);
    ODIGEO_SCREEN_SIZE_MAP.put(DisplayMetrics.DENSITY_XHIGH, OdigeoScreenSize.XHIGH);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
      ODIGEO_SCREEN_SIZE_MAP.put(DisplayMetrics.DENSITY_XXHIGH, OdigeoScreenSize.XXHIGH);
    }

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
      ODIGEO_SCREEN_SIZE_MAP.put(DisplayMetrics.DENSITY_XXXHIGH, OdigeoScreenSize.XXXHIGH);
    }
  }

  private DeviceUtils() {

  }

  /**
   * This method checks if there's space available on the SD card
   *
   * @return True if there's space available
   */
  public static boolean checkSDCard() {

    long freeSpaceInBytes = Environment.getExternalStorageDirectory().getUsableSpace();

    //Log.d("SIZE KB: ", String.valueOf( (float) Math.round((freeSpaceInBytes / (1024)) * 10) / 10) );

    if (android.os.Environment.getExternalStorageState()
        .equals(android.os.Environment.MEDIA_MOUNTED)) {
      return (((float) Math.round((freeSpaceInBytes / (BYTES * BYTES)) * DECIMAL) / DECIMAL)
          >= MIN_FREE_SPACE); // freeSpaceInBytes to MB if freeSpaceInBytes >5 true
    }

    return false;
  }

  /**
   * This method calls a specific phone number
   *
   * @param phone Phone number to be called
   * @param context Application context
   */
  public static void callPhoneNumber(String phone, Context context) {
    Intent callIntent = new Intent(Intent.ACTION_CALL);
    callIntent.setData(Uri.parse("tel:" + phone));
    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    context.startActivity(callIntent);
  }

  /**
   * This method prepares the creation of a file
   *
   * @param context Application context
   * @param fileName The name of the file
   * @return The file created
   */
  public static File prepareFileCreation(Context context, String fileName) {
    ContextWrapper contextWrapper = new ContextWrapper(context);

    String folderName = Configuration.getInstance().getBrand();

    String finalPath;

    if (DeviceUtils.checkSDCard()) {

      finalPath = String.format("%s/%s/%s", Environment.getExternalStorageDirectory().toString(),
          folderName, fileName);
    } else {
      File directory = contextWrapper.getDir(folderName, Context.MODE_PRIVATE);
      finalPath = String.format("%s/%s/%s", directory.getPath(), folderName, fileName);
    }

    File newFile = new File(finalPath);
    DeviceUtils.createDirectory(finalPath);

    return newFile;
  }

  /**
   * This method gets the system's Locale
   *
   * @param resources Application resources
   * @return Application's configuration's locale
   */
  public static Locale getSystemLocale(Resources resources) {
    android.content.res.Configuration conf = resources.getConfiguration();

    return conf.locale;
  }

  /**
   * This method creates a new directory on a given path
   *
   * @param path Path for the new directory
   * @return true if the directory was created successfully
   */
  public static boolean createDirectory(String path) {
    File directory = new File(path);
    return createDirectory(directory);
  }

  /**
   * This method creates a new directory if the given directory doesn't exist
   *
   * @param directory directory to be created
   * @return true if the directory was created successfully
   */
  public static boolean createDirectory(File directory) {
    File existDirectory = directory;

    //If it's a file, get the directory
    if (!existDirectory.isDirectory()) {
      existDirectory = directory.getParentFile();
    }

    if (!existDirectory.exists()) {
      existDirectory.mkdir();
      return true;
    }

    return false;
  }

  /**
   * Return the device Id for request to MSL
   *
   * @param context Context from when we need to call this method
   * @return the string with correct format for the device id
   */
  public static String getDeviceID(Context context) {
    return getDeviceID(getApplicationVersionName(context), getApplicationVersionCode(context),
        getOdigeoScreenSize(context));
  }

  /**
   * Gets the odigeo screen size id
   *
   * @param context Context to look the screen size
   * @return An string representing the odigeo id
   */
  public static String getOdigeoScreenSize(Context context) {
    DisplayMetrics dm = context.getResources().getDisplayMetrics();
    float density = dm.density;
    int densityToCheck = DisplayMetrics.DENSITY_DEFAULT;
    if (density >= XXXHIGH && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
      //"xxxhdpi";
      densityToCheck = DisplayMetrics.DENSITY_XXXHIGH;
    }
    if (density >= XXHIGH
        && density < XXXHIGH
        && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
      //xxhdpi
      densityToCheck = DisplayMetrics.DENSITY_XXHIGH;
    }
    if (density >= XHIGH) {
      //xhdpi
      densityToCheck = DisplayMetrics.DENSITY_XHIGH;
    }
    if (density >= HIGH && density < XHIGH) {
      //hdpi
      densityToCheck = DisplayMetrics.DENSITY_HIGH;
    }
    if (density >= MEDIUM && density < HIGH) {
      //mdpi
      densityToCheck = DisplayMetrics.DENSITY_MEDIUM;
    }

    return ODIGEO_SCREEN_SIZE_MAP.get(densityToCheck).getOdigeoId();
  }

  /**
   * This method gets the device's ID
   * <p/>
   * DeviceID Format:  CHANNEL;MODEL;OS-VERSION;BRAND;MARKET;WIFI-MAC-ADDRESS;LANGUAGE;LOCALE;APPVERSION;APPBUILD;SCALE
   * Examples:         ANDROID;GSI;4.0.1;O;FR;00:24:81:22:ec:78;it;es_ES;0.15;0;L
   *
   * @param appVersion Version number of the application
   * @param appBuild Build number of the application
   * @param screenSize The size of the device's screen
   * @return The device's ID
   */
  public static String getDeviceID(String appVersion, String appBuild, String screenSize) {

    Market market = Configuration.getInstance().getCurrentMarket();

    String macAddress = DeviceUtils.getMACAddress("wlan0");

    if (macAddress == null || macAddress.equals(Constants.MAC_ADDRESS_EMPTY)) {
      macAddress = DeviceUtils.getMACAddress(null);
    }

    return String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", Constants.CHANNEL, Build.MODEL,
        Build.VERSION.RELEASE, Configuration.getInstance().getBrandKey(), market.getWebsite(),
        macAddress, market.getLanguage(), market.getLocale(), appVersion, appBuild, screenSize);
  }

  /**
   * Returns MAC address of the given interface name.
   *
   * @param interfaceName eth0, wlan0 or NULL=use first interface
   * @return mac address or empty string
   */
  public static String getMACAddress(String interfaceName) {
    String macAddress = Constants.MAC_ADDRESS_EMPTY;

    try {
      List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
      for (NetworkInterface intf : interfaces) {
        if (interfaceName != null && !intf.getName().equalsIgnoreCase(interfaceName)) {
          continue;
        }
        byte[] mac = intf.getHardwareAddress();
        if (mac == null) {
          continue;
        }
        StringBuilder buf = new StringBuilder();
        for (byte aMac : mac) {
          buf.append(String.format("%02X:", aMac));
        }
        if (buf.length() > 0) {
          buf.deleteCharAt(buf.length() - 1);
        }
        macAddress = buf.toString();
      }
    } catch (Exception ex) {
      Log.e(Constants.TAG_LOG, ex.getMessage());
    }

    return macAddress;
  }

  /**
   * This method gets the IP address
   *
   * @param useIPv4 if it's necessary to use the IPv4
   * @return IP address
   */
  public static String getIPAddress(boolean useIPv4) {
    try {
      List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
      for (NetworkInterface intf : interfaces) {
        List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
        for (InetAddress addr : addrs) {
          if (!addr.isLoopbackAddress()) {
            String sAddr = addr.getHostAddress().toUpperCase();
            boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
            if (useIPv4) {
              if (isIPv4) {
                return sAddr;
              }
            } else {
              if (!isIPv4) {
                int delim = sAddr.indexOf('%'); // drop ip6 port suffix
                return delim < 0 ? sAddr : sAddr.substring(0, delim);
              }
            }
          }
        }
      }
    } catch (Exception ex) {
      Log.e(Constants.TAG_LOG, ex.getMessage());
    } // for now eat exceptions
    return "";
  }

  /**
   * This method gets the application version name
   *
   * @param context Application context
   * @return Application's version name
   */
  @Deprecated public static String getApplicationVersionName(Context context) {
    PackageManager packageManager = context.getPackageManager();
    try {
      PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);

      return packageInfo.versionName;
    } catch (Exception e) {
      Log.e(Constants.TAG_LOG, e.getMessage());
    }
    return "0";
  }

  /**
   * This method gets the application version code
   *
   * @param context Application context
   * @return Application's version code
   */
  @Deprecated public static String getApplicationVersionCode(Context context) {
    PackageManager packageManager = context.getPackageManager();
    try {
      PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);

      return String.valueOf(packageInfo.versionCode);
    } catch (Exception e) {

      Log.e(Constants.TAG_LOG, e.getMessage());
    }
    return "0";
  }

  /**
   * This method gets the application version name in the PlayStore
   *
   * @return Application's version name in the PlayStore
   */
  public static String getApplicationVersionNameInPlayStore() {
    String newVersion = null;

    try {
      newVersion = Jsoup.connect(Configuration.getInstance().getAppStoreURL())
          .timeout(TIME_TIMEOUT)
          .userAgent(
              "Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
          .referrer("http://www.google.com")
          .get()
          .select("div[itemprop=softwareVersion]")
          .first()
          .ownText();
      Log.d(Constants.TAG_LOG, "Jsoup = " + newVersion);
    } catch (Exception e) {
      Log.d(Constants.TAG_LOG,
          "getApplicationVersionNameInPlayStore() exception " + e.getMessage());
    }

    if (newVersion != null) {
      newVersion = newVersion.trim();
    }

    return newVersion;
  }

  /**
   * From a brand name, get it shorted name
   *
   * @param brand Brand name
   * @return the short brand name
   */
  public static String getShortBrandName(String brand) {
    if (brand.equalsIgnoreCase(Constants.BRAND_GOVOYAGES)) {
      return "G";
    }

    if (brand.equalsIgnoreCase(Constants.BRAND_OPODO)) {
      return "O";
    }

    if (brand.equalsIgnoreCase(Constants.BRAND_TRAVELLINK)) {
      return "T";
    }

    return "E";
  }

  /**
   * Check the device network connectivity.
   */
  public static boolean isGoodNetworkConnectivityAvailable(Context context) {
    ConnectivityManager connManager =
        (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

    NetworkInfo netInfo = connManager.getActiveNetworkInfo();

    if (netInfo != null && netInfo.isConnected()) {
      if (netInfo.getType() == ConnectivityManager.TYPE_WIFI
          || netInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
        return true;
      }
    }

    return false;
  }

  /**
   * Exports the database to a file renamed with current datetime.
   */
  @SuppressLint("SimpleDateFormat") public static void exportDatabase(Context context,
      String databaseName) {
    FileChannel src = null;
    FileChannel dst = null;
    String packageName = context.getPackageName();

    try {
      String filePath = Environment.getExternalStorageDirectory() + File.separator + "Odigeo";
      createDirIfNotExists(filePath);
      File sd = new File(filePath);
      File data = Environment.getDataDirectory();

      if (sd.canWrite()) {
        String currentDBPath = "//data//" + packageName + "//databases//" + databaseName + "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());

        String backupDBPath = packageName + "_backup_" + currentDateandTime + ".db";
        File currentDB = new File(data, currentDBPath);
        File backupDB = new File(sd, backupDBPath);

        if (currentDB.exists()) {
          src = new FileInputStream(currentDB).getChannel();
          dst = new FileOutputStream(backupDB).getChannel();
          dst.transferFrom(src, 0, src.size());
          src.close();
          dst.close();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        src.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
      try {
        dst.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Creates a folder in a given path.
   */
  public static boolean createDirIfNotExists(String path) {
    boolean ret = true;

    File file = new File(path);
    if (!file.exists()) {
      if (!file.mkdirs()) {
        ret = false;
      }
    }
    return ret;
  }
}
