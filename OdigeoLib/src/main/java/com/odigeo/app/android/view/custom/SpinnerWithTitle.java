package com.odigeo.app.android.view.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;

public class SpinnerWithTitle extends LinearLayout {

  private Spinner mSpOptions;
  private TextView mTvTitle;

  public SpinnerWithTitle(Context context) {
    super(context);
  }

  public SpinnerWithTitle(Context context, AttributeSet attrs) {
    super(context, attrs);
    initComponent(context);
  }

  public SpinnerWithTitle(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    initComponent(context);
  }

  private void initComponent(Context context) {
    View view = LayoutInflater.from(context).inflate(R.layout.spinner_with_text, this, false);
    mTvTitle = (TextView) view.findViewById(R.id.tvTitle);
    mSpOptions = (Spinner) view.findViewById(R.id.spOptions);
    addView(view);
  }

  public void setAdapter(ArrayAdapter<String> spinnerAdapter) {
    mSpOptions.setAdapter(spinnerAdapter);
  }

  public void setText(String text) {
    mTvTitle.setText(text);
  }

  public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener listener) {
    mSpOptions.setOnItemSelectedListener(listener);
  }

  public Object getSelectedItem() {
    return mSpOptions.getSelectedItem();
  }

  public void setSelection(int index) {
    mSpOptions.setSelection(index);
  }

  public int getCount() {
    return mSpOptions.getAdapter().getCount();
  }
}