package com.odigeo.app.android.lib.models.dto;

import java.util.List;

/**
 * @author miguel
 */
public class BaseResponseDTO extends ErrorResponseDTO {

  protected List<MessageResponseDTO> messages;
  protected List<ExtensionResponseDTO> extensions;

  public BaseResponseDTO() {
  }

  /**
   * @return the messages
   */
  public List<MessageResponseDTO> getMessages() {
    return messages;
  }

  /**
   * @param messages the messages to set
   */
  public void setMessages(List<MessageResponseDTO> messages) {
    this.messages = messages;
  }

  /**
   * @return the extensions
   */
  public List<ExtensionResponseDTO> getExtensions() {
    return extensions;
  }

  /**
   * @param extensions the extensions to set
   */
  public void setExtensions(List<ExtensionResponseDTO> extensions) {
    this.extensions = extensions;
  }
}
