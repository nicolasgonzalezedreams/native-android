package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for FraudTrackingItem complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="FraudTrackingItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="bookingId" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="data" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="active" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */

public class FraudTrackingItemDTO {
  protected Long id;
  protected Long bookingId;
  protected String data;
  protected String type;
  protected Boolean active;

  /**
   * Gets the value of the id property.
   *
   * @return possible object is {@link Long }
   */
  public Long getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   *
   * @param value allowed object is {@link Long }
   */
  public void setId(Long value) {
    this.id = value;
  }

  /**
   * Gets the value of the bookingId property.
   *
   * @return possible object is {@link Long }
   */
  public Long getBookingId() {
    return bookingId;
  }

  /**
   * Sets the value of the bookingId property.
   *
   * @param value allowed object is {@link Long }
   */
  public void setBookingId(Long value) {
    this.bookingId = value;
  }

  /**
   * Gets the value of the data property.
   *
   * @return possible object is {@link String }
   */
  public String getData() {
    return data;
  }

  /**
   * Sets the value of the data property.
   *
   * @param value allowed object is {@link String }
   */
  public void setData(String value) {
    this.data = value;
  }

  /**
   * Gets the value of the type property.
   *
   * @return possible object is {@link String }
   */
  public String getType() {
    return type;
  }

  /**
   * Sets the value of the type property.
   *
   * @param value allowed object is {@link String }
   */
  public void setType(String value) {
    this.type = value;
  }

  /**
   * Gets the value of the active property.
   *
   * @return possible object is {@link Boolean }
   */
  public Boolean isActive() {
    return active;
  }

  /**
   * Sets the value of the active property.
   *
   * @param value allowed object is {@link Boolean }
   */
  public void setActive(Boolean value) {
    this.active = value;
  }
}
