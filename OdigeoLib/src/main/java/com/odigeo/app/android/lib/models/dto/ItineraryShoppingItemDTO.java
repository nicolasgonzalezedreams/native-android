package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Deprecated public class ItineraryShoppingItemDTO {

  protected ItineraryDTO itinerary;
  protected BigDecimal price;
  protected BigDecimal baggageFee;
  protected String lastTicketingDate;
  protected List<PassengersConflictDetailsDTO> passengersConflictDetails;
  protected List<String> pnrs;

  /**
   * Gets the value of the itinerary property.
   */
  public ItineraryDTO getItinerary() {
    return itinerary;
  }

  /**
   * Sets the value of the itinerary property.
   */
  public void setItinerary(ItineraryDTO value) {
    this.itinerary = value;
  }

  /**
   * Gets the value of the price property.
   *
   * @return possible object is {@link BigDecimal }
   */
  public BigDecimal getPrice() {
    return price;
  }

  /**
   * Sets the value of the price property.
   *
   * @param value allowed object is {@link BigDecimal }
   */
  public void setPrice(BigDecimal value) {
    this.price = value;
  }

  /**
   * Gets the value of the baggageFee property.
   *
   * @return possible object is {@link BigDecimal }
   */
  public BigDecimal getBaggageFee() {
    return baggageFee;
  }

  /**
   * Sets the value of the baggageFee property.
   *
   * @param value allowed object is {@link BigDecimal }
   */
  public void setBaggageFee(BigDecimal value) {
    this.baggageFee = value;
  }

  /**
   * Gets the value of the lastTicketingDate property.
   *
   * @return possible object is {@link String }
   */
  public String getLastTicketingDate() {
    return lastTicketingDate;
  }

  /**
   * Sets the value of the lastTicketingDate property.
   */
  public void setLastTicketingDate(String lastTicketingDate) {
    this.lastTicketingDate = lastTicketingDate;
  }

  /**
   * Gets the value of the passengersConflictDetails property.
   *
   * @return possible object is {@link BigDecimal }
   */
  public List<PassengersConflictDetailsDTO> getPassengersConflictDetails() {
    if (passengersConflictDetails == null) {
      return new ArrayList<PassengersConflictDetailsDTO>();
    }
    return this.passengersConflictDetails;
  }

  /**
   * Sets the value of the passengersConflictDetails property.
   */
  public void setPassengersConflictDetails(
      List<PassengersConflictDetailsDTO> passengersConflictDetails) {
    this.passengersConflictDetails = passengersConflictDetails;
  }

  /**
   * Gets the value of the pnrs property.
   *
   * @return possible object is {@link List<String> }
   */
  public List<String> getPnrs() {
    if (pnrs == null) {
      return new ArrayList<String>();
    }
    return pnrs;
  }

  /**
   * Sets the value of the pnrs property.
   */
  public void setPnrs(List<String> pnrs) {
    this.pnrs = pnrs;
  }
}
