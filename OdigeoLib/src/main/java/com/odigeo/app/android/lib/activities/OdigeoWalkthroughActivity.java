package com.odigeo.app.android.lib.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.fragments.WalkthroughPageFragment;
import com.odigeo.app.android.lib.models.DefaultWalkthroughPage;
import com.odigeo.app.android.lib.models.WalkthroughPage;
import com.odigeo.app.android.lib.models.WalkthroughPageStatic;
import com.odigeo.app.android.lib.ui.widgets.CirclePageIndicator;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.navigator.LoginNavigator;
import com.odigeo.app.android.navigator.RegisterNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.ab.RemoteConfigControllerInterface;
import com.odigeo.dataodigeo.session.CredentialsInterface;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Activity that contains the view pager for the walkthrough, this activity will show each
 * walkthrough item in a page the user can skip or
 * swipe it
 *
 * @author M. en C. Javier Silva Perez
 * @version 1.0
 * @since 06/03/15
 */
public abstract class OdigeoWalkthroughActivity extends OdigeoForegroundBaseActivity {

  public static final String EXTRA_WALKTHROUGH_CONTENT = "EXTRA_WALKTHROUGH_CONTENT";
  private static final int ONE_PAGE = 1;

  private List<WalkthroughPage> pages;
  private Button btnSearchFlights;
  private Button btnLogIn;
  private Button btnRegister;
  private OdigeoApp mOdigeoApp;
  private RemoteConfigControllerInterface remoteConfigController;
  /**
   * The pager widget, which handles animation and allows swiping horizontally to access previous
   * and next wizard steps.
   */
  private ViewPager mPager;

  @Override protected void onStart() {
    super.onStart();
    if (((OdigeoApp) getApplication()).hasAnimatedBackground()) {
      ViewUtils.loadAnimate(findViewById(R.id.image_nube_top), R.anim.translate_nube_top);
      ViewUtils.loadAnimate(findViewById(R.id.image_nube_botton), R.anim.translate_nube_bottom);
    }
  }

  @Override public void onStop() {
    super.onStop();
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_walkthrough);
    ActionBar actionBar = getSupportActionBar();
    remoteConfigController =
        AndroidDependencyInjector.getInstance().provideRemoteConfigController();
    if (actionBar != null) {
      String title =
          remoteConfigController.shouldShowDefaultWalkthrough() ? LocalizablesFacade.getString(
              getApplicationContext(), OneCMSKeys.SSO_SIGNIN_HEADER).toString() : LocalizablesFacade
              .getString(getApplicationContext(), OneCMSKeys.WALKTHROUGH_TITLE)
              .toString();
      ;
      actionBar.setTitle(title);
    }
    pages = createStaticWalkthroughPages(this);

    // Instantiate a ViewPager and a PagerAdapter.
    mPager = (ViewPager) findViewById(R.id.view_pager);
    //The pager adapter, which provides the pages to the view pager widget.
    PagerAdapter pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
    mPager.setAdapter(pagerAdapter);

    //Bind the title indicator to the adapter
    final CirclePageIndicator circlePageIndicator =
        (CirclePageIndicator) findViewById(R.id.pager_indicator);

    if (pagerAdapter.getCount() > ONE_PAGE) {
      circlePageIndicator.setViewPager(mPager);

      mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
          circlePageIndicator.onPageScrolled(position, positionOffset, positionOffsetPixels);
        }

        @Override public void onPageSelected(int position) {
          circlePageIndicator.onPageSelected(position);
          postGAEventScreenLoads(position);
        }

        @Override public void onPageScrollStateChanged(int state) {
          //If the page indicator should be animated while swiping between pages
          circlePageIndicator.onPageScrollStateChanged(state);
        }
      });
    } else {
      circlePageIndicator.setVisibility(View.GONE);
    }

    prepareButtons();
    PreferencesManager.setFirstTimeInHome(getApplicationContext(), true);

    mOdigeoApp = (OdigeoApp) getApplication();
    OdigeoSession odigeoSession = mOdigeoApp.getOdigeoSession();
    odigeoSession.setDataHasBeenLoaded(true);
  }

  private void prepareButtons() {
    if (remoteConfigController.shouldShowDefaultWalkthrough()) {
      prepareDefaultButtons();
    } else {
      prepareWalkthroughButtons();
    }
  }

  private void prepareWalkthroughButtons() {
    btnSearchFlights = (Button) findViewById(R.id.button_search_flights_walkthrough);
    btnLogIn = (Button) findViewById(R.id.button_login_walkthrough);

    btnSearchFlights.setText(
        LocalizablesFacade.getString(this, OneCMSKeys.MY_TRIPS_SEARCH_BUTTON).toString());

    btnSearchFlights.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        Intent intent = new Intent(OdigeoWalkthroughActivity.this,
            ((OdigeoApp) getApplication()).getHomeActivity());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.EXTRA_JUMP_TO_SEARCH, true);
        startActivity(intent);
        finish();
      }
    });

    CredentialsInterface credentials =
        AndroidDependencyInjector.getInstance().provideSessionController().getCredentials();
    if (credentials == null) {
      btnLogIn.setText(LocalizablesFacade.getString(this, OneCMSKeys.SSO_LOGIN_BUTTON).toString());
      btnLogIn.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
          startActivity(new Intent(getApplicationContext(), LoginNavigator.class));
          finish();
        }
      });
    } else {
      btnLogIn.setVisibility(View.GONE);
      LinearLayout.LayoutParams params =
          (LinearLayout.LayoutParams) btnSearchFlights.getLayoutParams();
      int rightMargin = (int) getResources().getDimension(R.dimen.button_margin_right);
      params.rightMargin = rightMargin;
      btnSearchFlights.setLayoutParams(params);
    }
  }

  private void prepareDefaultButtons() {
    btnRegister = (Button) findViewById(R.id.button_search_flights_walkthrough);
    btnLogIn = (Button) findViewById(R.id.button_login_walkthrough);

    btnRegister.setText(
        LocalizablesFacade.getString(this, OneCMSKeys.SSO_CREATE_ACCOUNT).toString());

    btnRegister.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        startActivity(new Intent(getApplicationContext(), RegisterNavigator.class));
        finish();
      }
    });

    btnLogIn.setText(LocalizablesFacade.getString(this, OneCMSKeys.SSO_LOGIN_BUTTON).toString());
    btnLogIn.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        startActivity(new Intent(getApplicationContext(), LoginNavigator.class));
        finish();
      }
    });
  }

  @Override public void onBackPressed() {
    if (mPager.getCurrentItem() == 0) {
      // If the user is currently looking at the first step, allow the system to handle the
      // Back button. This calls finish() on this activity and pops the back stack.
      super.onBackPressed();
    } else {
      // Otherwise, select the previous step.
      mPager.setCurrentItem(mPager.getCurrentItem() - 1);
    }
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_walkthrough, menu);
    MenuItem itemAbout = menu.findItem(R.id.menu_item_skip_walkthrough);
    itemAbout.setTitle(
        LocalizablesFacade.getString(this, OneCMSKeys.WALKTHROUGH_DONE_BUTTON_TEXT).toString());
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int itemId = item.getItemId();
    if (itemId == R.id.menu_item_skip_walkthrough) {
      goHome();
    }
    return super.onOptionsItemSelected(item);
  }

  private void goHome() {
    Intent intent = new Intent(OdigeoWalkthroughActivity.this,
        ((OdigeoApp) getApplication()).getHomeActivity());
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
    finish();
  }

  /**
   * Creates a dummy array for testing walkthrough activity
   *
   * @param context Context to look for the dummy strings
   * @return A list filled out with the pages to show
   */
  public List<WalkthroughPage> createStaticWalkthroughPages(Context context) {
    if (remoteConfigController.shouldShowDefaultWalkthrough()) {
      return loadDefaultWalkthrough();
    } else {
      return loadWalkthrough(context);
    }
  }

  private boolean marketSupportsPaypal() {
    List<String> paypalAvailableMarkets =
        Arrays.asList(getResources().getStringArray(R.array.paypal_available_markets));

    return paypalAvailableMarkets.contains(Configuration.getInstance().getCurrentMarket().getKey());
  }

  private List<WalkthroughPage> loadDefaultWalkthrough() {
    List<WalkthroughPage> pages = new LinkedList<>();

    DefaultWalkthroughPage page = new DefaultWalkthroughPage();
    pages.add(page);
    return pages;
  }

  /*
      This method is specific for paypal walkthrough,
      should change every time we want a new walkthrough.
   */
  private List<WalkthroughPage> loadWalkthrough(Context context) {
    List<WalkthroughPage> pages = new LinkedList<>();

    String title = LocalizablesFacade.getString(context, OneCMSKeys.WALKTHROUGH_STORE_PAYMENT_TITLE)
        .toString();
    String description =
        LocalizablesFacade.getString(context, OneCMSKeys.WALKTHROUGH_STORE_PAYMENT_BODY).toString();

    pages.add(new WalkthroughPageStatic(R.drawable.savepaymentmethod, title, description));

    return pages;
  }

  /**
   * Tracking of Google Analytics. Send in label the position of screen loaded
   */
  private void postGAEventScreenLoads(int position) {
    StringBuffer label = new StringBuffer(GAnalyticsNames.LABEL_WALKTHROUGH_SCREEN_X);
    label.append(position + 1);
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_WALKTHROUGH_NEW_UPDATES,
            GAnalyticsNames.ACTION_WALKTHROUGH_NEW_UPDATES, label.toString()));
  }

  public abstract void onGAEventTriggered(GATrackingEvent event);

  /**
   * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in sequence.
   */
  private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

    public ScreenSlidePagerAdapter(FragmentManager fm) {
      super(fm);
    }

    @Override public android.support.v4.app.Fragment getItem(int position) {
      //Create a new instance of the page using the corresponding item
      return WalkthroughPageFragment.newInstance(pages.get(position));
    }

    @Override public int getCount() {
      return pages.size();
    }
  }
}
