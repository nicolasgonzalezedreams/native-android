package com.odigeo.app.android.view.textwatchers;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;

public class NameOnCardTextWatcher extends BaseTextWatcher {

  private FieldValidator mFieldValidator;
  private boolean mIsFieldCorrect = false;
  private TextInputLayout mTilNameOnCard;
  private PaymentMethodBaseTextWatchersClient mPaymentMethodBaseTextWatchersClient;

  public NameOnCardTextWatcher(TextInputLayout textInputLayout, FieldValidator fieldValidator,
      PaymentMethodBaseTextWatchersClient paymentMethodBaseTextWatchersClient) {
    super(textInputLayout);
    mFieldValidator = fieldValidator;
    mTilNameOnCard = textInputLayout;
    mPaymentMethodBaseTextWatchersClient = paymentMethodBaseTextWatchersClient;
  }

  @Override public void afterTextChanged(Editable editableText) {
    super.afterTextChanged(editableText);
    mIsFieldCorrect = mFieldValidator.validateField(editableText.toString());
    mPaymentMethodBaseTextWatchersClient.onCharacterWriteOnField(mIsFieldCorrect, mTilNameOnCard);
  }
}

