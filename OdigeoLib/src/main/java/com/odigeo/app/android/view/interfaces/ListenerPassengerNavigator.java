package com.odigeo.app.android.view.interfaces;

import com.odigeo.data.entity.shoppingCart.Carrier;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import java.util.List;

public interface ListenerPassengerNavigator {
  void navigateToFrequentFlyerPassenger(List<UserFrequentFlyer> frequentFlyers,
      List<Carrier> carriers, int passengerPosition);

  void navigateToCountryResidentPassenger(int widgetPosition);

  void navigateToCountryNationalityPassenger(int widgetPosition);

  void navigateToCountryIdentificationPassenger(int widgetPosition);

  void navigateToSearch();
}
