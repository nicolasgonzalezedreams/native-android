package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoCountriesActivity;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.data.entity.booking.Country;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Show list of countries
 */
public class CountriesAdapter extends BaseAdapter
    implements StickyListHeadersAdapter, SectionIndexer {

  public static final String PHONE_PREFIX_FORMAT = "%s (%s)";
  private final int mAdapterMode;
  private final LayoutInflater mInflater;
  private final List<Country> mCountries;
  private final Map<String, Integer> mAlphaIndexer = new HashMap<>();
  private String[] mSections;

  /**
   * Constructor
   *
   * @param context context of current state of the application/object
   * @param countries List countries
   * @param mAdapterMode Mode of adapter
   */
  public CountriesAdapter(Context context, List<Country> countries, int mAdapterMode) {
    this.mAdapterMode = mAdapterMode;
    mInflater = LayoutInflater.from(context);
    mCountries = countries;
    buildIndexes();
  }

  private void buildIndexes() {
    int size = mCountries.size();
    for (int index = 0; index < size; index++) {
      String character = mCountries.get(index).getName().substring(0, 1);
      if (!mAlphaIndexer.containsKey(character)) {
        mAlphaIndexer.put(character, index);
      }
    }
    List<String> sectionList = new ArrayList<>(mAlphaIndexer.keySet());
    Collections.sort(sectionList);
    mSections = new String[sectionList.size()];
    sectionList.toArray(mSections);
  }

  @Override public int getCount() {
    return mCountries.size();
  }

  @Override public Country getItem(int position) {
    return mCountries.get(position);
  }

  @Override public long getItemId(int position) {
    return getItem(position).getId();
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {
    if (convertView == null) {
      convertView = mInflater.inflate(R.layout.layout_countries_list_item, parent, false);
      convertView.setTag(new ViewHolder(convertView));
    }
    ViewHolder holder = (ViewHolder) convertView.getTag();
    drawItem(position, holder);
    return convertView;
  }

  private void drawItem(int position, ViewHolder holder) {
    Country country = mCountries.get(position);
    holder.text.setTextColor(Color.BLACK);
    holder.text.setTypeface(Configuration.getInstance().getFonts().getRegular());
    if (this.mAdapterMode == OdigeoCountriesActivity.SELECTION_MODE_PHONE_PREFIX) {
      holder.text.setText(
          String.format(PHONE_PREFIX_FORMAT, country.getName(), country.getPhonePrefix()));
    } else {
      holder.text.setText(country.getName());
    }
  }

  @Override public View getHeaderView(int position, View convertView, ViewGroup parent) {
    if (convertView == null) {
      convertView = mInflater.inflate(R.layout.header_list_countries, parent, false);
      convertView.setTag(new HeaderViewHolder(convertView));
    }
    HeaderViewHolder holder = (HeaderViewHolder) convertView.getTag();
    holder.text.setTypeface(Configuration.getInstance().getFonts().getBold());
    holder.text.setText(mCountries.get(position).getName().substring(0, 1));
    return convertView;
  }

  @Override public long getHeaderId(int position) {
    return mCountries.get(position).getName().subSequence(0, 1).charAt(0);
  }

  @Override public Object[] getSections() {
    return mSections;
  }

  @Override public int getPositionForSection(int sectionIndex) {
    return mAlphaIndexer.get(mSections[sectionIndex]);
  }

  @Override public int getSectionForPosition(int position) {
    return 0;
  }

  static class HeaderViewHolder {

    final TextView text;

    public HeaderViewHolder(View view) {
      text = (TextView) view.findViewById(R.id.text1);
    }
  }

  static class ViewHolder {

    final TextView text;

    public ViewHolder(View view) {
      text = (TextView) view.findViewById(R.id.text);
    }
  }
}
