package com.odigeo.app.android.lib.data;

import android.content.Context;
import com.odigeo.app.android.lib.utils.MyAsyncTask;

/**
 * This class allows to call the database in background and find a String. Then the result is
 * asigned to a listener, that contains the text that should be put into a view.
 *
 * @author Cristian Fanjul
 * @since 02/07/2015
 */
public class LocalizableBackground {
  private CharSequence value;

  public LocalizableBackground(final Context context, final TextForViewListener textForViewListener,
      final String key, final String... params) {

    MyAsyncTask myAsyncTask = new MyAsyncTask();
    MyAsyncTask.DoInBackground doInBackground = new MyAsyncTask.DoInBackground() {
      @Override public void executeInBackground() {
        value = LocalizablesFacade.getString(context, key, params);
      }
    };

    MyAsyncTask.DoOnPostExecute doOnPostExecute = new MyAsyncTask.DoOnPostExecute() {
      @Override public void postExecute() {
        textForViewListener.setViewText(value);
      }
    };
    myAsyncTask.setDoOnPostExecute(doOnPostExecute);
    myAsyncTask.execute(doInBackground);
  }
}
