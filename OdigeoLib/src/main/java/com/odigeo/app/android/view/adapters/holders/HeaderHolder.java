package com.odigeo.app.android.view.adapters.holders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import java.util.List;

public class HeaderHolder extends BaseRecyclerViewHolder<Boolean> {

  public final TextView header;

  public static HeaderHolder newInstance(ViewGroup container) {

    View view = LayoutInflater.from(container.getContext())
        .inflate(R.layout.my_booking_header, container, false);
    return new HeaderHolder(view);
  }

  protected HeaderHolder(View v) {
    super(v);
    header = (TextView) v.findViewById(R.id.tvHeader);
  }

  @Override public void bind(int position, Boolean isPastTrip) {
    if (isPastTrip) {
      header.setText(localizableProvider.getString(OneCMSKeys.MY_TRIPS_LIST_PAST_TRIPS));
    }
  }
}
