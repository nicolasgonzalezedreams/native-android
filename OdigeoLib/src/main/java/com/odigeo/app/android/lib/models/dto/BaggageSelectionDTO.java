package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

/**
 * Created by ximena.perez on 27/01/2015.
 */
public class BaggageSelectionDTO implements Serializable {

  protected int segmentIndex;
  protected BaggageDescriptorDTO baggageDescriptor;
  protected SegmentTypeIndexDTO segmentTypeIndex;
  private int pieces;
  private int kilos;

  public BaggageSelectionDTO() {

  }

  /**
   * Parametrized constructor
   *
   * @param baggageDescriptor Baggage description
   * @param segmentTypeIndex Segment index to use
   */
  public BaggageSelectionDTO(BaggageDescriptorDTO baggageDescriptor,
      SegmentTypeIndexDTO segmentTypeIndex) {
    this.baggageDescriptor = baggageDescriptor;
    this.segmentTypeIndex = segmentTypeIndex;
  }

  /**
   * @return
   */
  public BaggageDescriptorDTO getBaggageDescriptor() {
    return baggageDescriptor;
  }

  /**
   * @param baggageDescriptor
   */
  public void setBaggageDescriptor(BaggageDescriptorDTO baggageDescriptor) {
    this.baggageDescriptor = baggageDescriptor;
  }

  /**
   * @return
   */
  public SegmentTypeIndexDTO getSegmentTypeIndex() {
    return segmentTypeIndex;
  }

  /**
   * @param segmentTypeIndex
   */
  public void setSegmentTypeIndex(SegmentTypeIndexDTO segmentTypeIndex) {
    this.segmentTypeIndex = segmentTypeIndex;
  }

  public int getPieces() {
    return pieces;
  }

  public void setPieces(int pieces) {
    this.pieces = pieces;
  }

  public int getKilos() {
    return kilos;
  }

  public void setKilos(int kilos) {
    this.kilos = kilos;
  }

  public int getSegmentIndex() {
    return segmentIndex;
  }

  public void setSegmentIndex(int segmentIndex) {
    this.segmentIndex = segmentIndex;
  }
}

