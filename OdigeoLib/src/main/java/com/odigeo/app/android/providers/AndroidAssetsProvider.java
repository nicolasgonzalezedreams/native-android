package com.odigeo.app.android.providers;

import android.content.Context;
import android.support.annotation.Nullable;
import com.odigeo.interactors.provider.AssetsProvider;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class AndroidAssetsProvider implements AssetsProvider {

  private final Context context;

  public AndroidAssetsProvider(Context context) {
    this.context = context;
  }

  @Override @Nullable public String loadAssetAsString(String path) {

    StringBuilder builder = new StringBuilder();

    try {
      InputStream in = context.getAssets().open(path);
      BufferedReader reader =
          new BufferedReader(new InputStreamReader(in, Charset.forName("UTF-8")));
      String readed;

      while ((readed = reader.readLine()) != null) {
        builder.append(readed);
      }

      in.close();

      return builder.toString();
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }
}
