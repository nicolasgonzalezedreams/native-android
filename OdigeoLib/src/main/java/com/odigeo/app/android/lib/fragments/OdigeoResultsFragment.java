package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.floatingexpandiblelistview.FloatingGroupExpandableListView;
import com.floatingexpandiblelistview.WrapperExpandableListAdapter;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.odigeo.DependencyInjector;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.helper.WideningHelper;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoBackgroundAnimatedActivity;
import com.odigeo.app.android.lib.activities.OdigeoNoConnectionActivity;
import com.odigeo.app.android.lib.activities.OdigeoSearchResultsActivity;
import com.odigeo.app.android.lib.adapters.DefaultResultsExpandableListAdapter;
import com.odigeo.app.android.lib.adapters.MembershipResultsExpandableListAdapter;
import com.odigeo.app.android.lib.adapters.ResultsActivityExpandableListAdapter;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.interfaces.AdapterExpandableListListener;
import com.odigeo.app.android.lib.models.Airports;
import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.CollectionMethodDTO;
import com.odigeo.app.android.lib.models.dto.CollectionMethodKeyPriceDTO;
import com.odigeo.app.android.lib.models.dto.CollectionMethodLegendDTO;
import com.odigeo.app.android.lib.models.dto.CollectionMethodTypeDTO;
import com.odigeo.app.android.lib.models.dto.ExtensionResponseDTO;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;
import com.odigeo.app.android.lib.models.dto.LocationDTO;
import com.odigeo.app.android.lib.models.dto.responses.FlightSearchResponse;
import com.odigeo.app.android.lib.modules.Filters;
import com.odigeo.app.android.lib.ui.widgets.FullPriceWidget;
import com.odigeo.app.android.lib.ui.widgets.SegmentGroupWidget;
import com.odigeo.app.android.lib.ui.widgets.TopBriefWidget;
import com.odigeo.app.android.lib.ui.widgets.base.BlackDialog;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.CollectionMethodComparator;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.custom.FullTransparencyModal;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.app.android.view.snackbars.CustomSnackbar;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.entity.geo.LocationDescriptionType;
import com.odigeo.data.entity.shoppingCart.AvailableProductsResponse;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartRequestModel;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.ExtensionRequest;
import com.odigeo.data.entity.shoppingCart.ItinerarySelectionRequest;
import com.odigeo.data.entity.shoppingCart.ItinerarySortCriteria;
import com.odigeo.data.entity.shoppingCart.PricingMode;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.net.controllers.ShoppingCartNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.net.mapper.AvailableProductsResponseMapper;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.CreateShoppingCartInteractor;
import com.odigeo.presenter.listeners.OnCreateShoppingCartListener;
import com.odigeo.dataodigeo.net.mapper.CreateShoppingCartResponseMapper;
import com.odigeo.interactors.provider.MarketProviderInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_FULL_PRICE_INFORMATION;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_FULL_PRICE_SELECTOR;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_FLIGHTS_RESULT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_BOTTOM_BAR_SHOWN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_CC_SELECTOR_SHOWN;

public class OdigeoResultsFragment extends Fragment
    implements AdapterExpandableListListener, FullPriceWidget.FullPriceListener {

  public static final int RESULT_FROM_FILTER = 451;
  public static final String SHOW_FULL_TRANSPARENCY_DIALOG = "show_fulltransparency_dialog";
  public static final String FULL_TRANSPARENCY_FIRST_TIME = "FULL_TRANSPARENCY_FIRST_TIME";
  private static final int SNACKBAR_DURATION = 9000;
  private static final int DELAY_MILLIS = 200;
  private OdigeoSearchResultsActivity parentActivity;
  private FloatingGroupExpandableListView expandableListView;
  private ResultsActivityExpandableListAdapter expandableListAdapter;
  private List<SegmentWrapper> segmentsWrapperSelected;
  private BlackDialog flightsDialog;
  private FullPriceWidget fullPriceWidget;
  private LinearLayout pricePerPassengerWidget;
  private List<CollectionMethodWithPrice> collectionMethodWithPrices;
  private CollectionMethodWithPrice methodWithPriceSelected;
  private CollectionMethodWithPrice itineraryMethodWithPriceSelected;
  private List<String> segmentsKeysSelected;
  private String keyItineraryResultSelected;
  private OdigeoApp odigeoApp;
  private OdigeoSession odigeoSession;
  private SearchOptions mSearchOptions;
  private boolean mIsFullTransparency;
  private Snackbar mSnackbar;
  private boolean wideningDepartureFound;
  private boolean wideningArrivalFound;
  private City wideningDepartureCity;
  private City wideningArrivalCity;
  private CreateShoppingCartResponse createShoppingCartResponse;
  private AvailableProductsResponse mAvailableProductsResponse;
  private TrackerControllerInterface mTrackerController;
  private boolean isFragmentVisible;
  private DependencyInjector dependencyInjector;
  private MarketProviderInterface marketProviderInterface;
  private SessionController sessionController;

  public static OdigeoResultsFragment newInstance(SearchOptions searchOptions) {

    OdigeoResultsFragment newFragment = new OdigeoResultsFragment();
    Bundle bundleArgs = new Bundle();
    bundleArgs.putSerializable(Constants.EXTRA_SEARCH_OPTIONS, searchOptions);
    newFragment.setArguments(bundleArgs);
    return newFragment;
  }

  @Override public final void onCreate(Bundle saveInstance) {

    super.onCreate(saveInstance);

    odigeoApp = (OdigeoApp) getActivity().getApplication();
    odigeoSession = odigeoApp.getOdigeoSession();
    dependencyInjector = ((OdigeoApp) getContext().getApplicationContext()).getDependencyInjector();
    mSearchOptions = (SearchOptions) getArguments().getSerializable(Constants.EXTRA_SEARCH_OPTIONS);
    mTrackerController = AndroidDependencyInjector.getInstance().provideTrackerController();
    marketProviderInterface = dependencyInjector.provideMarketProvider();
    sessionController = dependencyInjector.provideSessionController();

    if (odigeoSession != null
        && odigeoSession.getItineraryResults() != null
        && odigeoSession.getItineraryResultsFiltered().isEmpty()) {
      //Get the itinerary results filtered
      if (checkSearchForAirport()) {
        filterResultsBySearch();
      } else {
        odigeoSession.setItineraryResultsFiltered(odigeoSession.getItineraryResults());
      }
    }
  }

  @Override public void onActivityCreated(Bundle savedInstanceState) {

    super.onActivityCreated(savedInstanceState);
  }

  @Override public void onPause() {

    super.onPause();
    isFragmentVisible = false;
  }

  /**
   * Verify the segment flight if the search is for iatacode or airport
   *
   * @return true if the city is iata code or airport
   */
  private boolean checkSearchForAirport() {

    for (FlightSegment flightSegment : mSearchOptions.getFlightSegments()) {
      if (flightSegment.getDepartureCity().getType() == LocationDescriptionType.IATA_CODE
          || flightSegment.getDepartureCity().getType() == LocationDescriptionType.AIRPORT
          || flightSegment.getArrivalCity().getType() == LocationDescriptionType.IATA_CODE
          || flightSegment.getArrivalCity().getType() == LocationDescriptionType.AIRPORT) {
        return true;
      }
    }
    return false;
  }

  private void filterResultsBySearch() {

    if (mSearchOptions.getTravelType() == TravelType.SIMPLE) {
      odigeoSession.setItineraryResultsFiltered(filterResultsBySearchSimpleTrip());
    } else if (mSearchOptions.getTravelType() == TravelType.ROUND) {
      odigeoSession.setItineraryResultsFiltered(filterResultsBySearchRoundTrip());
    } else {
      odigeoSession.setItineraryResultsFiltered(filterResultsBySearchMultipleStopTrip());
    }
  }

  private List<FareItineraryDTO> filterResultsBySearchSimpleTrip() {
    //simple trip
    City departureCity = mSearchOptions.getFlightSegments().get(0).getDepartureCity();
    City arrivalCity = mSearchOptions.getFlightSegments().get(0).getArrivalCity();
    List<FareItineraryDTO> filteredItineraryDTOs = odigeoSession.getItineraryResults();

    wideningArrivalCity = arrivalCity;
    wideningDepartureCity = departureCity;

    if (isAirport(departureCity)) {
      List<FareItineraryDTO> filteredItineraryDeparture =
          Filters.getFilterDepartureAtIndexSegmentGroup(departureCity.getIataCode(),
              filteredItineraryDTOs, 0);
      if (filteredItineraryDeparture.isEmpty()) {
        // if list is empty we need to saved flight for widening and restart filters
        wideningDepartureFound = true;
      } else {
        filteredItineraryDTOs = filteredItineraryDeparture;
      }
    }
    if (isAirport(arrivalCity)) {
      List<FareItineraryDTO> filteredItineraryArrival =
          Filters.getFilterArrivalAtIndexSegmentGroup(arrivalCity.getIataCode(),
              filteredItineraryDTOs, 0);
      if (filteredItineraryArrival.isEmpty()) {
        wideningArrivalFound = true;
      } else {
        filteredItineraryDTOs = filteredItineraryArrival;
      }
    }
    return filteredItineraryDTOs;
  }

  private List<FareItineraryDTO> filterResultsBySearchRoundTrip() {
    //round trip
    City departureCity = mSearchOptions.getFlightSegments().get(0).getDepartureCity();
    City arrivalCity = mSearchOptions.getFlightSegments().get(0).getArrivalCity();
    List<FareItineraryDTO> filteredItineraryDTOs = odigeoSession.getItineraryResults();

    wideningArrivalCity = arrivalCity;
    wideningDepartureCity = departureCity;

    if (isAirport(departureCity)) {
      List<FareItineraryDTO> filteredItineraryDeparture =
          Filters.getFilterDepartureAtIndexSegmentGroup(departureCity.getIataCode(),
              filteredItineraryDTOs, 0);
      if (!filteredItineraryDeparture.isEmpty()) {
        filteredItineraryDeparture =
            Filters.getFilterArrivalAtIndexSegmentGroup(departureCity.getIataCode(),
                filteredItineraryDeparture, 1);
      }
      if (filteredItineraryDeparture.isEmpty()) {
        // if list is empty we need to saved flight for widening and restart filters
        wideningDepartureFound = true;
      } else {
        filteredItineraryDTOs = filteredItineraryDeparture;
      }
    }
    if (isAirport(arrivalCity)) {
      List<FareItineraryDTO> filteredItineraryArrival =
          Filters.getFilterArrivalAtIndexSegmentGroup(arrivalCity.getIataCode(),
              filteredItineraryDTOs, 0);
      if (!filteredItineraryArrival.isEmpty()) {
        filteredItineraryArrival =
            Filters.getFilterDepartureAtIndexSegmentGroup(arrivalCity.getIataCode(),
                filteredItineraryArrival, 1);
      }
      if (filteredItineraryArrival.isEmpty()) {
        wideningArrivalFound = true;
      } else {
        filteredItineraryDTOs = filteredItineraryArrival;
      }
    }
    return filteredItineraryDTOs;
  }

  private List<FareItineraryDTO> filterResultsBySearchMultipleStopTrip() {
    //multi-stops
    boolean localWideningArrivalFound;
    boolean localWideningDepartureFound;
    List<FareItineraryDTO> filteredItineraryDTOs = odigeoSession.getItineraryResults();
    for (int indexSegment = 0; indexSegment < mSearchOptions.getFlightSegments().size();
        indexSegment++) {
      localWideningArrivalFound = false;
      localWideningDepartureFound = false;
      City departureCity = mSearchOptions.getFlightSegments().get(indexSegment).getDepartureCity();
      City arrivalCity = mSearchOptions.getFlightSegments().get(indexSegment).getArrivalCity();

      if (isAirport(departureCity)) {
        List<FareItineraryDTO> filteredItineraryDeparture =
            Filters.getFilterDepartureAtIndexSegmentGroup(departureCity.getIataCode(),
                filteredItineraryDTOs, indexSegment);
        if (filteredItineraryDeparture.isEmpty()) {
          // if list is empty we need to saved flight for widening and restart filters
          localWideningDepartureFound = true;
        } else {
          filteredItineraryDTOs = filteredItineraryDeparture;
        }
      }
      if (isAirport(arrivalCity)) {
        List<FareItineraryDTO> filteredItineraryArrival =
            Filters.getFilterArrivalAtIndexSegmentGroup(arrivalCity.getIataCode(),
                filteredItineraryDTOs, indexSegment);
        if (filteredItineraryArrival.isEmpty()) {
          localWideningArrivalFound = true;
        } else {
          filteredItineraryDTOs = filteredItineraryArrival;
        }
      }
      validateWideningMultiStop(localWideningDepartureFound, localWideningArrivalFound,
          departureCity, arrivalCity);
    }
    return filteredItineraryDTOs;
  }

  private void validateWideningMultiStop(boolean localWideningDepartureFound,
      boolean localWideningArrivalFound, City departureCity, City arrivalCity) {

    if ((localWideningArrivalFound || localWideningDepartureFound) && !(wideningArrivalFound
        || wideningDepartureFound)) {
      wideningDepartureFound = localWideningDepartureFound;
      wideningArrivalFound = localWideningArrivalFound;
      wideningArrivalCity = arrivalCity;
      wideningDepartureCity = departureCity;
    }
  }

  /**
   * Verify if the search is for iatacode or airport
   */
  private boolean isAirport(City destinationCity) {

    return destinationCity.getType() == LocationDescriptionType.IATA_CODE
        || destinationCity.getType() == LocationDescriptionType.AIRPORT;
  }

  private boolean isValidCollectionMethodWithPrices() {

    return (collectionMethodWithPrices != null && !collectionMethodWithPrices.isEmpty());
  }

  // Loads this fragment on the parent activity
  @Override public final void onAttach(Activity activity) {

    super.onAttach(activity);

    // Store reference to parent activity
    parentActivity = (OdigeoSearchResultsActivity) getActivity();
  }

  @Override
  public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {

    super.onCreateView(inflater, container, bundle);
    // This fragment adds options of its own to the activity menu
    setHasOptionsMenu(true);

    View rootView = inflater.inflate(R.layout.layout_search_results_fragment, container, false);
    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {
      expandableListView = (FloatingGroupExpandableListView) rootView.findViewById(
          R.id.expandableListView_resultsActivity);
      fullPriceWidget = (FullPriceWidget) rootView.findViewById(R.id.fullPriceWidget);
      pricePerPassengerWidget = (LinearLayout) rootView.findViewById(R.id.pricePerPassengerWidget);
      ((TopBriefWidget) rootView.findViewById(R.id.topbrief_results)).addData(mSearchOptions);
      fullPriceWidget.setListener(this);
      expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
        @Override
        public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

          return true; // This way the expander cannot be collapsed
        }
      });
      if (mIsFullTransparency) {
        fullPriceWidget.setVisibility(View.GONE);
        pricePerPassengerWidget.setVisibility(View.GONE);
      } else if (!isValidCollectionMethodWithPrices()) {
        fullPriceWidget.setVisibility(View.GONE);
      } else {  //Show credit card selector
        AndroidDependencyInjector.getInstance()
            .provideTrackerController()
            .trackAnalyticsEvent(CATEGORY_FLIGHTS_RESULT, ACTION_FULL_PRICE_SELECTOR,
                LABEL_CC_SELECTOR_SHOWN);
        fullPriceWidget.setCollectionMethods(collectionMethodWithPrices);
        pricePerPassengerWidget.setVisibility(View.GONE);
      }

      showWidening();
    }
    return rootView;
  }

  private void setFullTransparencyResultsList(View view) {

    if (mIsFullTransparency) {
      expandableListAdapter.setFullPriceSentence(
          LocalizablesFacade.getString(view.getContext(), OneCMSKeys.FULL_PRICE_RESULTS_HEADER_NEW)
              .toString());
    } else {
      expandableListAdapter.setFullPriceSentence("");
    }
  }

  @Override public final void onViewCreated(View view, Bundle savedInstanceState) {

    super.onViewCreated(view, savedInstanceState);

    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded() && !isItineraryNull()) {

      CollectionMethodWithPrice cheapest = null;
      if (isValidCollectionMethodWithPrices()) {
        cheapest = collectionMethodWithPrices.get(0);
      }

      List<FareItineraryDTO> itineraryDTOList = getFareItinerary();

      int divider = parentActivity.getPricingMode() == PricingMode.PER_PASSENGER
          ? mSearchOptions.getTotalPassengers() : 1;

      if(parentActivity.userIsMember()) {
        expandableListAdapter = new MembershipResultsExpandableListAdapter(getActivity(),
            itineraryDTOList, parentActivity.getSearchResponse().getTravelType(),
            divider, mSearchOptions.getFlightSegments().size(), marketProviderInterface.getLocale
            (), this);
      } else {
        expandableListAdapter = new DefaultResultsExpandableListAdapter(getActivity(),
            itineraryDTOList, parentActivity.getSearchResponse().getTravelType(),
            divider, mSearchOptions.getFlightSegments().size(), marketProviderInterface.getLocale
            (), this);
      }

      expandableListAdapter.setItineraryContainers();
      expandableListAdapter.setSelectedMethod(cheapest);
      expandableListAdapter.setSelectedMethod(fullPriceWidget.getSelectedItem());
      setFullTransparencyResultsList(view);

      WrapperExpandableListAdapter wrapperAdapter =
          new WrapperExpandableListAdapter(expandableListAdapter);
      expandableListView.setAdapter(wrapperAdapter);

      //            if (parentActivity.getCardSelected() != null) {
      if (getMethodWithPriceSelected() != null) {
        fullPriceWidget.setSelectedItem(getMethodWithPriceSelected());
        fullPriceWidget.setPaymentMethod(getMethodWithPriceSelected());
      }
    } else {
      Log.d(Constants.TAG_LOG, "INACTIVITY Mandando a home desde fragment");
      parentActivity.showNoResultsFragment();
    }
  }

  @Override public void onResume() {

    super.onResume();
    SegmentGroupWidget.unlockContinueButton();
    if (mIsFullTransparency) {
      showFullTransparencySnackbar();
    }
    isFragmentVisible = true;
  }

  private CollectionMethodWithPrice getMethodWithPriceSelected() {

    if (parentActivity.getCardSelected() != null) {
      return parentActivity.getCardSelected();
    } else if (fullPriceWidget.getSelectedItem() == null && isValidCollectionMethodWithPrices()) {
      return collectionMethodWithPrices.get(0);
    }
    return null;
  }

  //It is fired when the user pushes the continue button
  @Override public final void onItinerarySelected(FareItineraryDTO itineraryResult,
      List<SegmentWrapper> segments) {
    segmentsWrapperSelected = segments;
    //Set the card selected
    if (fullPriceWidget.getSelectedItem() != null) {
      methodWithPriceSelected = parentActivity.getCardSelected();
    }

    segmentsKeysSelected = new ArrayList<String>();
    for (SegmentWrapper segmentWrapper : segments) {
      segmentsKeysSelected.add(segmentWrapper.getKey());
    }

    if(sessionController.getCredentials() == null) {
      odigeoSession.setMembershipPerksDTO(itineraryResult.getMembershipPerks());
    }

    keyItineraryResultSelected = itineraryResult.getKey();

    if (methodWithPriceSelected != null) {

      String methodCode =
          methodWithPriceSelected.getCollectionMethod().getCreditCardType().getCode();

      if (methodCode == null || methodCode.equals("CHEAPEST")) {
        methodCode = itineraryResult.getCollectionMethodFeesObject()
            .getCheapest()
            .getCollectionMethod()
            .getCreditCardType()
            .getCode();
      }
      itineraryMethodWithPriceSelected =
          itineraryResult.getCollectionMethodFeesObject().getCollectionMethodFees().get(methodCode);
    }



    createShoppingCart(keyItineraryResultSelected, segmentsKeysSelected);
  }

  public final void onSelectDifferentItinerary(FareItineraryDTO itineraryResult,
      int itineraryResultIndex) {
    // This scroll to the child item, instead of the group item
    expandableListView.setSelectedGroup(itineraryResultIndex);
  }

  @Override
  public final void onSegmentSelected(SegmentGroupWidget segmentGroupWidget, int segmentIndex) {

    int noSegmentsGrouped = 0;
    for (int i = 0; i <= segmentGroupWidget.getItineraryResultParentIndex(); i++) {
      if (!expandableListView.isGroupExpanded(i)) {
        noSegmentsGrouped++;
      }
    }

    //Roundtrip segmentGroupWidget.getItineraryResultParentIndex() * 3 + 1
    final int index = (segmentGroupWidget.getItineraryResultParentIndex() - noSegmentsGrouped)
        * (mSearchOptions.getFlightSegments().size() + 1) + 1 + segmentIndex + noSegmentsGrouped;

    expandableListView.postDelayed(new Runnable() {
      @Override public void run() {

        expandableListView.smoothScrollToPosition(index);
        expandableListView.setOnScrollListener(new AbsListView.OnScrollListener() {
          @Override public void onScrollStateChanged(AbsListView view, int scrollState) {

            expandableListView.deferNotifyDataSetChanged();
          }

          @Override
          public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
              int totalItemCount) {
            //just for anonymous class
          }
        });
      }
    }, DELAY_MILLIS);
    Log.i(Constants.TAG_LOG, "ITINERARY: "
        + segmentGroupWidget.getItineraryResultParentIndex()
        + " SegmentGroup = "
        + segmentIndex
        + " result= "
        + index);
  }

  private void showWidening() {

    String wideningMessage =
        getWideningMessage(wideningDepartureFound, wideningArrivalFound, wideningDepartureCity,
            wideningArrivalCity);

    if (wideningMessage != null) {
      ((OdigeoBackgroundAnimatedActivity) getActivity()).showWideningMessage(wideningMessage);
    }
  }

  @Nullable
  private String getWideningMessage(boolean wideningDepartureFound, boolean wideningArrivalFound,
      City departureCity, City arrivalCity) {

    if (wideningDepartureFound && wideningArrivalFound) {
      return WideningHelper.getMessageForBothAirports(getContext(), departureCity.getCityName(),
          arrivalCity.getCityName());
    } else if (wideningArrivalFound) {
      return WideningHelper.getMessageForArrivalAirport(getContext(), arrivalCity.getName(),
          arrivalCity.getCityName(), arrivalCity.getIataCode());
    } else if (wideningDepartureFound) {
      return WideningHelper.getMessageForDepartureAirport(getContext(), departureCity.getName(),
          departureCity.getCityName(), departureCity.getIataCode());
    }
    return null;
  }

  private void createShoppingCart(String itineraryResultKey, List<String> segmentsKeys) {

    final ShoppingCartListener shoppingCartListener = new ShoppingCartListener();
    final CreateShoppingCartRequestModel shoppingCartModel =
        createShoppingCartRequestModel(itineraryResultKey, segmentsKeys);

    Log.i(Constants.TAG_LOG, "IN createShoppingCart()");

    flightsDialog = new BlackDialog(getActivity(), true);
    flightsDialog.show(LocalizablesFacade.getString(getActivity(),
        "loadingviewcontroller_message_creatingshoppingcart"));

    final CreateShoppingCartInteractor createShoppingCartInteractor = AndroidDependencyInjector
        .getInstance().provideCreateShoppingCartInteractor();

    createShoppingCartInteractor.createShoppingCart(shoppingCartModel, new OnCreateShoppingCartListener() {
      @Override public void onSuccess(CreateShoppingCartResponse shoppingCartResponse) {
        createShoppingCartResponse = shoppingCartResponse;
        createShoppingCartResponse.setSortCriteria(
            shoppingCartModel.getItinerarySelectionRequest().getSortCriteria());

        dependencyInjector.provideVisitUserInteractor().loginVisitUser();
        shoppingCartListener.onRequestSuccessful(createShoppingCartResponse);
      }

      @Override public void onError(MslError error, String message) {
        shoppingCartListener.onRequestError(message);
      }
    });
  }

  private void getAvailableProducts(
      com.odigeo.app.android.lib.models.dto.responses.CreateShoppingCartResponse createShoppingCartResponse) {

    final AvailableProductsListener availableProductsListener = new AvailableProductsListener();
    ShoppingCartNetControllerInterface shoppingCartNetController =
        AndroidDependencyInjector.getInstance().provideShoppingCartNetController();

    shoppingCartNetController.getAvailableProducts(
        createShoppingCartResponse.getShoppingCart().getBookingId(),
        new OnRequestDataListener<String>() {
          @Override public void onResponse(String object) {

            AvailableProductsResponseMapper availableProductsResponseMapper =
                new AvailableProductsResponseMapper();
            try {
              mAvailableProductsResponse =
                  availableProductsResponseMapper.parseAvailableProductsResponse(object);

              //Parse to old shoppingCart
              com.odigeo.app.android.lib.models.dto.responses.AvailableProductsResponse
                  oldAvailableProductsResponse;
              oldAvailableProductsResponse = new Gson().fromJson(object,
                  com.odigeo.app.android.lib.models.dto.responses.AvailableProductsResponse.class);
              availableProductsListener.onRequestSuccessful(oldAvailableProductsResponse);
            } catch (JsonSyntaxException e) {
              Log.e(getClass().getName(), e.getMessage());
              e.printStackTrace();
              availableProductsListener.onRequestException(e);
            }
          }

          @Override public void onError(MslError error, String message) {

            availableProductsListener.onRequestError(message);
          }
        });
  }

  private CreateShoppingCartRequestModel createShoppingCartRequestModel(String itineraryResultKey,
      List<String> segmentsKeys) {

    FlightSearchResponse flightSearchResponse = parentActivity.getSearchResponse();
    CreateShoppingCartRequestModel model = new CreateShoppingCartRequestModel();

    model.setSearchId(flightSearchResponse.getItineraryResultsPage().getSearchId());

    if (flightSearchResponse.getExtensions() != null) {
      List<ExtensionRequest> extensionRequest = new ArrayList<>();

      for (ExtensionResponseDTO extension : flightSearchResponse.getExtensions()) {
        extensionRequest.add(new ExtensionRequest(extension.getName(), extension.getValue()));
      }

      model.setExtensions(extensionRequest);
    }

    //ItinerarySelectionRequest
    model.setItinerarySelectionRequest(new ItinerarySelectionRequest());
    model.getItinerarySelectionRequest().setFareItineraryKey(itineraryResultKey);
    model.getItinerarySelectionRequest().setSegmentKeys(segmentsKeys);
    model.getItinerarySelectionRequest()
        .setSortCriteria(ItinerarySortCriteria.valueOf(
            flightSearchResponse.getItineraryResultsPage().getSortCriteria().value()));

    return model;
  }

  //It is fired, when a collection method type is selected

  @Override public void onItemSelected(CollectionMethodWithPrice item) {

    postGAEventFullPriceSelected(item.getCollectionMethod().getName(getActivity()).
        replace(' ', '_'));
    if (expandableListAdapter != null) {
      parentActivity.setCardSelected(item);
      expandableListAdapter.setSelectedMethod(item);
      expandableListAdapter.notifyDataSetChanged();
      expandableListView.smoothScrollBy(1, 100);
    }
  }

  public List<FareItineraryDTO> getFareItinerary() {

    return (odigeoSession.getItineraryResultsFiltered() != null
        && !odigeoSession.getItineraryResultsFiltered().isEmpty())
        ? odigeoSession.getItineraryResultsFiltered() : odigeoSession.getItineraryResults();
  }

  private boolean isItineraryNull() {

    return odigeoSession.getItineraryResultsFiltered() == null
        && odigeoSession.getItineraryResults() == null;
  }

  @Override public final void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    inflater.inflate(R.menu.menu_goto_filters, menu);
    MenuItem item = menu.findItem(R.id.menu_item_goto_filters);
    item.setIcon(DrawableUtils.getTintedResource(R.mipmap.search_filters, R.color.semantic_icon,
        getContext()));
    item.setTitle(
        LocalizablesFacade.getString(getActivity(), "filtersviewcontroller_title").toString());
    super.onCreateOptionsMenu(menu, inflater);
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {

    int id = item.getItemId();
    if (id == R.id.menu_item_goto_filters) {
      if (mSnackbar != null && mSnackbar.isShown()) {
        mSnackbar.dismiss();
      }
      openFiltersActivity();

      postGAEventLabelFiltersOpen();

      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void openFiltersActivity() {

    OdigeoApp app = (OdigeoApp) getActivity().getApplication();

    //Order the carriers list
    ArrayList<CarrierDTO> lstCarriers = (ArrayList<CarrierDTO>) parentActivity.getSearchResponse().
        getItineraryResultsPage().getLegend().getCarriers();

    if (lstCarriers == null) {
      lstCarriers = new ArrayList<CarrierDTO>();
    }

    Collections.sort(lstCarriers);

    //Get all the segments grouped in 3 groups : departure city, arrival city, scale segments
    List<FlightSegment> segments = mSearchOptions.getFlightSegments();

    City departureCity = segments.get(0).getDepartureCity();
    City arrivalCity = getArrivalCityFilter(segments);

    //Get the list of airports group by city

    List<Airports> airportsGrouped = getAirportsGroupedByCity(departureCity, arrivalCity);

    //Create the intent
    Intent intent = new Intent(parentActivity, app.getFiltersActivityClass());
    intent.putExtra(Constants.INTENT_SEARCH_OPTIONS_TO_FILTERS, mSearchOptions);
    intent.putExtra(Constants.EXTRA_FILTERS_SELECTED, parentActivity.getFiltersSelected());
    intent.putExtra(Constants.INTENT_CARRIERS_TO_FILTER, lstCarriers);
    intent.putExtra(Constants.EXTRA_AIRPORTS_BY_CITY, (ArrayList<Airports>) airportsGrouped);
    parentActivity.startActivityForResult(intent, RESULT_FROM_FILTER);
  }

  private City getArrivalCityFilter(List<FlightSegment> segments) {

    if (mSearchOptions.getTravelType() == TravelType.SIMPLE
        || mSearchOptions.getTravelType() == TravelType.ROUND) {
      return segments.get(0).getArrivalCity();
    } else if (mSearchOptions.getTravelType() == TravelType.MULTIDESTINATION) {
      return segments.get(segments.size() - 1).getArrivalCity();
    }
    return null;
  }

  private List<Airports> getAirportsGroupedByCity(City departureCity, City arrivalCity) {

    List<Airports> airportsGrouped = new ArrayList<Airports>();
    HashMap<String, List<LocationDTO>> hashMap = new HashMap<String, List<LocationDTO>>();
    if (parentActivity.getSearchResponse().getItineraryResultsPage().getLegend().getLocations()
        != null) {

      for (LocationDTO location : parentActivity.getSearchResponse()
          .getItineraryResultsPage()
          .getLegend()
          .getLocations()) {
        if (location.getType().equalsIgnoreCase(LocationDescriptionType.AIRPORT.name())) {

          String cityName = getCityByLocation(location, departureCity, arrivalCity);

          if (!hashMap.containsKey(cityName)) {
            hashMap.put(cityName, new ArrayList<LocationDTO>());

            Airports airports = new Airports();
            airports.setTitle(cityName);
            airports.setAirports(hashMap.get(cityName));
            airportsGrouped.add(airports);

            airports.setOrder(getAirportsOrder(location, departureCity, arrivalCity));
          }

          hashMap.get(cityName).add(location);
        }
      }

      //Order all the information
      Collections.sort(airportsGrouped);

      for (Airports airports : airportsGrouped) {
        Collections.sort(airports.getAirports());
      }
    }
    return airportsGrouped;
  }

  private String getCityByLocation(LocationDTO location, City departureCity, City arrivalCity) {

    String cityName = LocalizablesFacade.getString(getActivity(),
        "airportfilterviewcontroller_titletransitairports").toString();
    if (location.getCityName().equals(departureCity.getCityName())) {
      cityName = location.getCityName();
    } else if ((location.getCityIataCode().equals(arrivalCity.getIataCode()))
        || (location.getIataCode().equals(arrivalCity.getIataCode()))) {
      cityName = location.getCityName();
    }
    return cityName;
  }

  private int getAirportsOrder(LocationDTO location, City departureCity, City arrivalCity) {

    if ((location.getCityIataCode().equals(departureCity.getIataCode())) || (location.getIataCode()
        .equals(departureCity.getIataCode()))) {
      return AirportOrder.FIRST_ORDER;
    } else if ((location.getCityIataCode().equals(arrivalCity.getIataCode()))
        || (location.getIataCode().equals(arrivalCity.getIataCode()))) {
      return AirportOrder.THIRD_ORDER;
    } else {
      return AirportOrder.SECOND_ORDER;
    }
  }

  public void setFullTransparency(boolean fullTransparency) {

    mIsFullTransparency = fullTransparency;
  }

  public final void setCollectionMethodWithPrices(
      List<CollectionMethodWithPrice> collectionMethodWithPrices) {

    this.collectionMethodWithPrices = collectionMethodWithPrices;
  }

  public final List<CollectionMethodWithPrice> getCollectionMethodFees(
      FlightSearchResponse flightSearchResponse) {
    //Create a hash of the collectionMethods
    HashMap<Integer, CollectionMethodDTO> collectionMethods =
        new HashMap<Integer, CollectionMethodDTO>();

    for (CollectionMethodLegendDTO collectionMethodGroup : flightSearchResponse.getItineraryResultsPage()
        .getLegend()
        .getCollectionMethods()) {
      collectionMethods.put(collectionMethodGroup.getId(),
          collectionMethodGroup.getCollectionMethod());
    }

    //Parse all the collectionMethodFees to CollectionMethodWithPrice
    //List of credit cards with its price
    List<CollectionMethodWithPrice> collectionMethodWithPrices =
        new ArrayList<CollectionMethodWithPrice>();

    for (CollectionMethodKeyPriceDTO methodKeyPriceDTO : flightSearchResponse.getItineraryResultsPage()
        .getLegend()
        .getCollectionEstimationFees()
        .get(0)
        .getCollectionMethodFees()) {

      if (collectionMethods.containsKey(methodKeyPriceDTO.getCollectionMethodKey())) {

        CollectionMethodDTO collectionMethodDTO =
            collectionMethods.get(methodKeyPriceDTO.getCollectionMethodKey());

        //Only Credit Card payment method
        if (collectionMethodDTO != null
            && collectionMethodDTO.getType() == CollectionMethodTypeDTO.CREDITCARD) {

          CollectionMethodWithPrice methodWithPrice = new CollectionMethodWithPrice();

          methodWithPrice.setCollectionMethodKey(methodKeyPriceDTO.getCollectionMethodKey());

          methodWithPrice.setCollectionMethod(collectionMethodDTO);

          methodWithPrice.setPrice(methodKeyPriceDTO.getPrice());

          collectionMethodWithPrices.add(methodWithPrice);
        }
      }
    }

    Collections.sort(collectionMethodWithPrices, new CollectionMethodComparator());

    return collectionMethodWithPrices;
  }

  private void showFullTransparencySnackbar() {

    String snackBarMessage =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.FULLPRICE_BUBBLE_MESSAGE).toString();
    String snackBarAction =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.FULLPRICE_BUBBLE_ACTION).toString();
    final RelativeLayout parentView =
        (RelativeLayout) getView().findViewById(R.id.root_activity_result);
    CustomSnackbar customSnackbar =
        new CustomSnackbar(getActivity(), parentView, snackBarMessage, SNACKBAR_DURATION);

    mSnackbar = customSnackbar.provideCustomSnackbar();
    mSnackbar.setAction(snackBarAction, new View.OnClickListener() {
      @Override public void onClick(View v) {

        mSnackbar.dismiss();
        FullTransparencyModal fullTransparencyModal = new FullTransparencyModal(getContext());
        parentView.addView(fullTransparencyModal);
        fullTransparencyModal.showPanel();
      }
    });
    mSnackbar.show();
    mTrackerController.trackAnalyticsEvent(CATEGORY_FLIGHTS_RESULT, ACTION_FULL_PRICE_INFORMATION,
        LABEL_BOTTOM_BAR_SHOWN);
  }

  @Override public final void onActivityResult(int requestCode, int resultCode, Intent data) {

    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == Activity.RESULT_OK
        && requestCode == Constants.REQUEST_CODE_SHOPPINGCARLISTENER) {
      createShoppingCart(keyItineraryResultSelected, segmentsKeysSelected);
    } else if (resultCode == Activity.RESULT_OK
        && requestCode == Constants.REQUEST_CODE_AVAILABLEPRODUCTSLISTENER) {
      createShoppingCart(keyItineraryResultSelected, segmentsKeysSelected);
    }
  }

  private void postGAScreenTrackingErrorNoConnection() {
    // GAnalytics screen tracking - Error case: no connection
    BusProvider.getInstance()
        .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_ERROR_NO_CONNECTION));
  }

  private void postGAEventLabelFiltersOpen() {

    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_RESULTS,
            GAnalyticsNames.ACTION_RESULTS_LIST, GAnalyticsNames.LABEL_FILTERS_OPEN_CLICKS));
  }

  /**
   * Trigger the event for item selection on full price
   *
   * @param type payment type selected
   */
  private void postGAEventFullPriceSelected(@NonNull String type) {

    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_RESULTS,
            GAnalyticsNames.ACTION_FULL_PRICE_SELECTOR,
            GAnalyticsNames.LABEL_APPLY_PAYMENT + type));
  }

  @Override public void onWidgetSelected(boolean isSelected) {
    if (!mIsFullTransparency
        && fullPriceWidget.getSpinnerFullPrice().getAdapter() != null) {
      if (isSelected) {
        fullPriceWidget.setVisibility(View.GONE);
      } else {
        fullPriceWidget.setVisibility(View.VISIBLE);
      }
    }
  }

  private class AvailableProductsListener {

    void onRequestSuccessful(
        com.odigeo.app.android.lib.models.dto.responses.AvailableProductsResponse availableProductsResponse) {

      Log.i(Constants.TAG_LOG, "onRequestSuccessful AvailableProducts Rest Service");
      MSLErrorUtilsDialogFragment errorFragment =
          MSLErrorUtilsDialogFragment.newInstance(availableProductsResponse.getMessages(),
              availableProductsResponse.getError());

      if (errorFragment != null && isFragmentVisible) {
        errorFragment.setParentScreen(Step.RESULTS.toString());
        errorFragment.setCancelable(false);
        errorFragment.show(getFragmentManager(), errorFragment.getCode());
      } else {
        parentActivity.setCreateShoppingCartResponse(createShoppingCartResponse);
        parentActivity.setAvailableProductsResponse(mAvailableProductsResponse);
        parentActivity.onProductsRequestSuccess(availableProductsResponse);
      }
      flightsDialog.dismiss();
    }

    void onRequestError(String message) {

      Log.i(Constants.TAG_LOG, "onRequestError AvailableProducts Rest Service : " + message);
      flightsDialog.dismiss();

      postGAScreenTrackingErrorNoConnection();

      OdigeoNoConnectionActivity.launch(OdigeoResultsFragment.this,
          Constants.REQUEST_CODE_AVAILABLEPRODUCTSLISTENER);
    }

    void onRequestException(Exception exception) {

      Log.i(Constants.TAG_LOG,
          "onRequestException AvailableProducts Rest Service : " + exception.getMessage());
      flightsDialog.dismiss();

      OdigeoNoConnectionActivity.launch(OdigeoResultsFragment.this,
          Constants.REQUEST_CODE_AVAILABLEPRODUCTSLISTENER);
    }
  }

  private class ShoppingCartListener {

    void onRequestSuccessful(CreateShoppingCartResponse createShoppingCartResponse) {

      String oldShoppinCartString = new Gson().toJson(createShoppingCartResponse);
      com.odigeo.app.android.lib.models.dto.responses.CreateShoppingCartResponse
          oldShoppingCartResponse;
      oldShoppingCartResponse = new Gson().fromJson(oldShoppinCartString,
          com.odigeo.app.android.lib.models.dto.responses.CreateShoppingCartResponse.class);

      MSLErrorUtilsDialogFragment errorFragment =
          MSLErrorUtilsDialogFragment.newInstance(oldShoppingCartResponse.getMessages(),
              oldShoppingCartResponse.getError());

      if (errorFragment != null && isFragmentVisible) {
        errorFragment.setParentScreen(Step.RESULTS.toString());
        errorFragment.setCancelable(false);
        errorFragment.show(getFragmentManager(), errorFragment.getCode());
      } else {

        if (parentActivity.onShoppingCartRequestSuccess(oldShoppingCartResponse,
            segmentsWrapperSelected, itineraryMethodWithPriceSelected)) {

          getAvailableProducts(oldShoppingCartResponse);
        }
      }
    }

    void onRequestError(String message) {

      Log.i(Constants.TAG_LOG, "onRequestError Shopping Cart Rest Service : " + message);
      flightsDialog.dismiss();

      postGAScreenTrackingErrorNoConnection();

      OdigeoNoConnectionActivity.launch(OdigeoResultsFragment.this,
          Constants.REQUEST_CODE_SHOPPINGCARLISTENER);
    }

    void onRequestException(Exception exception) {

      Log.i(Constants.TAG_LOG,
          "onRequestException Shopping Cart Rest Service : " + exception.getMessage());
      flightsDialog.dismiss();

      OdigeoNoConnectionActivity.launch(OdigeoResultsFragment.this,
          Constants.REQUEST_CODE_SHOPPINGCARLISTENER);
    }
  }

  final class AirportOrder {

    static final int FIRST_ORDER = 1;
    static final int SECOND_ORDER = 2;
    static final int THIRD_ORDER = 3;

    private AirportOrder() {

    }
  }
}