package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.ui.widgets.base.EditWithHint;

/**
 * Created by ManuelOrtiz on 03/10/2014.
 */
public class EditTextFormWithClear extends EditWithHint {

  private final ImageView imgClear;
  private int lengthToClear = -1;
  private EditTextFormWithClearListener listener;

  public EditTextFormWithClear(Context context, AttributeSet attrs) {

    super(context, attrs);
    getEditText().setText(null);
    getEditText().addTextChangedListener(createTextWatcher());
    getEditText().setOnClickListener(null);
    getEditText().setBackgroundColor(0);

    imgClear = (ImageView) findViewById(R.id.imgClearEditText);
    imgClear.setVisibility(GONE);

    if (imgClear != null) {
      imgClear.setOnClickListener(new OnClickListener() {
        @Override public void onClick(View v) {
          setText(null);
          hideError();

          if (listener != null) {
            listener.onClear();
          }
        }
      });
    }
  }

  private TextWatcher createTextWatcher() {
    return new TextWatcher() {
      @Override public void beforeTextChanged(CharSequence s, int start, int count,
          int after) { /*Nothing to do here*/ }

      @Override public void onTextChanged(CharSequence s, int start, int before,
          int count) { /*Nothing to do here*/ }

      @Override public void afterTextChanged(Editable s) {
        if (imgClear != null) {
          imgClear.setVisibility(View.GONE);
          if (s != null && getLengthToClear() != -1 && s.length() >= getLengthToClear()) {
            imgClear.setVisibility(View.VISIBLE);
          }
        }
      }
    };
  }

  @Override public final int getLayout() {
    return R.layout.layout_edit_form_widget_with_clear;
  }

  private int getLengthToClear() {
    return lengthToClear;
  }

  public final void setLengthToClear(int lengthToClear) {
    this.lengthToClear = lengthToClear;
  }

  public final EditTextFormWithClearListener getListener() {
    return listener;
  }

  public final void setListener(EditTextFormWithClearListener listener) {
    this.listener = listener;
  }

  public interface EditTextFormWithClearListener {

    void onClear();
  }
}
