package com.odigeo.app.android.view;

import android.content.Context;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.view.animations.CollapseAndExpandAnimationUtil;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.shoppingCart.InsuranceShoppingItem;
import com.odigeo.data.entity.shoppingCart.PricingBreakdown;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItem;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItemType;
import com.odigeo.data.entity.shoppingCart.ShoppingCart;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.presenter.PriceBreakdownWidgetPresenter;
import com.odigeo.presenter.contracts.views.PriceBreakdownWidgetInterface;
import java.math.BigDecimal;
import java.util.List;

import static com.odigeo.app.android.lib.data.LocalizablesFacade.NOT_FOUND;

public class PriceBreakdownWidgetView extends BaseCustomWidget<PriceBreakdownWidgetPresenter>
    implements PriceBreakdownWidgetInterface {

  private static final String MARKET_KEY_UAE = "AE";

  private LinearLayout priceBreakdownExpandableContent, mLlPricingBreakdownItems,
      llMembershipNotApplied;
  private TextView priceBreakdownPriceTitle, priceBreakdownPriceSubtitle, priceBreakdownPrice,
      seeDetails, tvPriceBreakdownTitle, tvMessageBreakDown, taxRefundableInfo,
      tvPriceBreakdownMembershipApplied, tvMembershipNotAppliedWarning;

  private Context context;
  private ScrollView scrollView;
  private SearchOptions searchOptions;
  private CollectionMethodWithPrice collectionMethodWithPrice;
  private Step step;

  private Double totalPrice;
  private InsuranceShoppingItem insuranceShoppingItem;

  public PriceBreakdownWidgetView(Context context) {
    super(context);
    this.context = context;
  }

  public PriceBreakdownWidgetView(Context context, AttributeSet attrs) {
    super(context, attrs);
    this.context = context;
  }

  @Override public void initComponent() {
    priceBreakdownPriceTitle = (TextView) findViewById(R.id.tvPriceBreakdownPriceTitle);
    priceBreakdownPriceSubtitle = (TextView) findViewById(R.id.tvPriceBreakdownPriceSubtitle);
    priceBreakdownPrice = (TextView) findViewById(R.id.tvPriceBreakdownPrice);
    priceBreakdownExpandableContent =
        (LinearLayout) findViewById(R.id.llPriceBreakdownExpandableContent);
    mLlPricingBreakdownItems = (LinearLayout) findViewById(R.id.llPricingBreakdownItems);
    tvPriceBreakdownTitle = (TextView) findViewById(R.id.tvPriceBreakdownTitle);
    tvMessageBreakDown = (TextView) findViewById(R.id.text_message_breakdown);
    tvPriceBreakdownMembershipApplied =
        (TextView) findViewById(R.id.tvPriceBreakdownMembershipApplied);
    tvMembershipNotAppliedWarning = (TextView) findViewById(R.id.tvMembershipNotAppliedWarning);
    llMembershipNotApplied = (LinearLayout) findViewById(R.id.llMembershipNotApplied);

    final String showMoreText = localizables.getString(OneCMSKeys.COMMON_SHOW_MORE).toUpperCase();
    final String showLessText = localizables.getString(OneCMSKeys.COMMON_SHOW_LESS).toUpperCase();

    seeDetails = (TextView) findViewById(R.id.tvSeeDetails);
    seeDetails.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        if (seeDetails.getText().equals(showMoreText)) {
          expandDescriptionContent();
          seeDetails.setText(showLessText);
          tvPriceBreakdownMembershipApplied.setVisibility(View.GONE);
        } else {
          collapseDescriptionContent();
          seeDetails.setText(showMoreText);
          tvPriceBreakdownMembershipApplied.setVisibility(View.VISIBLE);
        }
      }
    });

    taxRefundableInfo = (TextView) findViewById(R.id.taxrefundableinfo);
  }

  @Override public int getComponentLayout() {
    return R.layout.price_breakdown_widget;
  }

  @Override public void initOneCMSText() {
    priceBreakdownPriceTitle.setText(
        localizables.getString(OneCMSKeys.PRICING_BREAKDOWN_TOTAL_PRICE));
    String subtitle = localizables.getRawString(OneCMSKeys.PRICING_BREAKDOWN_TOTAL_PRICE_SUBTITLE);
    if (subtitle.isEmpty()) {
      priceBreakdownPriceSubtitle.setVisibility(GONE);
    } else {
      priceBreakdownPriceSubtitle.setText(subtitle);
    }
    tvPriceBreakdownTitle.setText(
        localizables.getString(OneCMSKeys.PRICINGBREAKDOWN_PRICEBREAKDOWN));

    seeDetails.setText(localizables.getString(OneCMSKeys.COMMON_SHOW_MORE).toUpperCase());
    tvMembershipNotAppliedWarning.setText(
        localizables.getString(OneCMSKeys.PRICING_BREAKDOWN_MEMBER_MUST_BE_PAX));
  }

  @Override protected PriceBreakdownWidgetPresenter setPresenter() {
    return dependencyInjector.providePriceBreakdownWidgetPresenter(this);
  }

  public Double getTotalPrice() {
    return totalPrice;
  }

  public PriceBreakdownWidgetPresenter getPresenter() {
    return presenter;
  }

  @Override public boolean isCollectionMethodWithPriceNull() {
    return collectionMethodWithPrice == null;
  }

  @Override public BigDecimal getCollectionMethodWithPrice() {
    if(collectionMethodWithPrice == null) return null;
    else return collectionMethodWithPrice.getPrice();
  }

  public void addData(ScrollView scrollView, SearchOptions searchOptions,
      PricingBreakdown pricingBreakdown, Step step,
      CollectionMethodWithPrice collectionMethodWithPrice, ShoppingCart shoppingCart) {
    this.scrollView = scrollView;
    this.searchOptions = searchOptions;
    this.collectionMethodWithPrice = collectionMethodWithPrice;
    this.step = step;
    presenter.configurePresenter(step, shoppingCart, pricingBreakdown);
  }

  @Override public void showPricesBreakdown() {
    List<PricingBreakdownItem> pricingBreakdownItems = presenter.getPricingBreakdownItems();

    if (!pricingBreakdownItems.isEmpty()) {
      totalPrice = presenter.getTotalPrice();

      List<InsuranceShoppingItem> insurances =
          presenter.getShoppingCart().getOtherProductsShoppingItems().getInsuranceShoppingItems();
      if (insurances != null && !insurances.isEmpty()) {
        for (InsuranceShoppingItem insurance : insurances) {
          if (insurance.isSelectable()) {
            insuranceShoppingItem = insurance;
          }
        }
      }
    }

    mLlPricingBreakdownItems.removeAllViewsInLayout();

    int row = 0;
    for (PricingBreakdownItem item : pricingBreakdownItems) {

      String detailString = getTextDetailItem(item.getPriceItemType(), row);
      if (detailString != null) {
        LayoutInflater inflater =
            (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView =
            inflater.inflate(R.layout.price_breakdown_item, mLlPricingBreakdownItems, false);
        mLlPricingBreakdownItems.addView(rowView);
        TextView tvItemDescription = (TextView) rowView.findViewById(R.id.tvItemDescription);
        tvItemDescription.setText(detailString);

        TextView tvItemAmount = (TextView) rowView.findViewById(R.id.tvItemAmount);
        double priceItemAmount = presenter.getPriceItemAmountWithType(item.getPriceItemType(), row,
            insuranceShoppingItem).doubleValue();
        tvItemAmount.setText(
            dependencyInjector.provideMarketProvider().getLocalizedCurrencyValue(priceItemAmount));

        configureMembershipPerksLabels(item, tvItemDescription, tvItemAmount);
      }

      row++;
    }

    String priceFormatted =
        dependencyInjector.provideMarketProvider().getLocalizedCurrencyValue(totalPrice);
    priceBreakdownPrice.setText(Util.getTextStylePrice(priceFormatted));

    configureMembershipApplianceLabels();
  }

  private void configureMembershipPerksLabels(PricingBreakdownItem item, TextView tvItemDescription,
      TextView tvItemAmount) {
    if (item.getPriceItemType() == PricingBreakdownItemType.MEMBERSHIP_PERKS) {
      tvItemDescription.setTextColor(
          ContextCompat.getColor(getContext(), R.color.semantic_positive));
      tvItemAmount.setTextColor(
          ContextCompat.getColor(getContext(), R.color.semantic_positive));
    } else if (item.getPriceItemType() == PricingBreakdownItemType.SLASHED_MEMBERSHIP_PERKS) {
      tvItemDescription.setTextColor(
          ContextCompat.getColor(getContext(), R.color.secondary_brand_dark));
      tvItemAmount.setTextColor(
          ContextCompat.getColor(getContext(), R.color.secondary_brand_dark));
      tvItemAmount.setPaintFlags(tvItemAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

      llMembershipNotApplied.setVisibility(View.VISIBLE);
    }
  }

  private void configureMembershipApplianceLabels() {
    if (presenter.shouldShowMembershipAppliedLabel()) {
      tvPriceBreakdownMembershipApplied.setVisibility(View.VISIBLE);

      if (presenter.membershipIsApplied()) {
        tvPriceBreakdownMembershipApplied.setText(
            localizables.getString(OneCMSKeys.PRICING_BREAKDOWN_PREMIUM_DISCOUNT_APPLIED));
        tvPriceBreakdownMembershipApplied.setTextColor(
            ContextCompat.getColor(getContext(), R.color.semantic_positive));
      } else {
        tvPriceBreakdownMembershipApplied.setText(
            localizables.getString(OneCMSKeys.PRICING_BREAKDOWN_PREMIUM_DISCOUNT_NOT_APPLIED));
        tvPriceBreakdownMembershipApplied.setTextColor(
            ContextCompat.getColor(getContext(), R.color.secondary_brand_dark));
      }
    } else {
      tvPriceBreakdownMembershipApplied.setVisibility(View.GONE);
    }
  }

  @Override public void checkUAEMessage() {
    if (step == Step.SUMMARY && dependencyInjector.provideMarketProvider()
        .getMarketKey()
        .equals(MARKET_KEY_UAE.toLowerCase())) {
      tvMessageBreakDown.setText(localizables.getString(OneCMSKeys.PRICING_LEGAL_CONDITIONS_AE));
    } else {
      tvMessageBreakDown.setText(
          localizables.getString(OneCMSKeys.PRICING_LEGAL_CONDITIONS_DEFAULT));
    }
  }

  @Override public void checkTaxRefundableInfo() {
    presenter.onTaxRefundableInfoCheck();
  }

  @Override public void showTaxRefundableInfo(Double taxRefundableAmount) {

    String taxRefundableText = localizables.getString(OneCMSKeys.FULLPRICE_TAX_REFUNDABLE_INFO,
        dependencyInjector.provideMarketProvider().getLocalizedCurrencyValue(taxRefundableAmount));

    if (!taxRefundableText.equals(
        String.format(NOT_FOUND, OneCMSKeys.FULLPRICE_TAX_REFUNDABLE_INFO))) {
      taxRefundableInfo.setText(taxRefundableText);
      taxRefundableInfo.setVisibility(VISIBLE);
    }
  }

  private String getTextDetailItem(PricingBreakdownItemType priceItemType, int row) {

    if (priceItemType == PricingBreakdownItemType.ADULTS_PRICE) {
      return String.format(getResources().getString(R.string.format_passenger_item),
          searchOptions.getNumberOfAdults(),
          Util.getAdultsLabel(searchOptions.getNumberOfAdults(), getContext()).toLowerCase());
    } else if (priceItemType == PricingBreakdownItemType.CHILDREN_PRICE) {
      return String.format(getResources().getString(R.string.format_passenger_item),
          searchOptions.getNumberOfKids(),
          Util.getKidsLabel(searchOptions.getNumberOfKids(), getContext()).toLowerCase());
    } else if (priceItemType == PricingBreakdownItemType.INFANTS_PRICE) {
      return String.format(getResources().getString(R.string.format_passenger_item),
          searchOptions.getNumberOfBabies(),
          Util.getBabiesLabel(searchOptions.getNumberOfBabies(), getContext()).toLowerCase());
    } else if (priceItemType == PricingBreakdownItemType.BAGGAGE_PRICE) {
      return getBaggagePriceString();
    } else if (priceItemType == PricingBreakdownItemType.INSURANCE_PRICE) {
      if (insuranceShoppingItem.getInsurance().getTitle() == null
          || insuranceShoppingItem.getInsurance().getTitle().isEmpty()) {
        return insuranceShoppingItem.getInsurance().getPolicy();
      } else {
        return insuranceShoppingItem.getInsurance().getTitle();
      }
    } else if (priceItemType == PricingBreakdownItemType.MEMBERSHIP_PERKS
        || priceItemType == PricingBreakdownItemType.SLASHED_MEMBERSHIP_PERKS) {
      return localizables.getString(OneCMSKeys.PRICING_BREAKDOWN_MEMBERSHIP_PERKS);
    } else {
      return checkItemType(priceItemType, row);
    }
  }

  private String checkItemType(PricingBreakdownItemType priceItemType, int row) {
    if (priceItemType == PricingBreakdownItemType.SERVICE_CHARGES) {
      return localizables.getString(OneCMSKeys.PRICING_BREAKDOWN_SERVICE_CHARGES);
    } else if (priceItemType == PricingBreakdownItemType.PROMOCODE_DISCOUNT) {
      return localizables.getString(OneCMSKeys.PRICING_BREAKDOWN_PROMOCODE_DISCOUNT,
          dependencyInjector.provideMarketProvider().getBrandVisualName());
    } else if (priceItemType == PricingBreakdownItemType.HANDLING_FEE) {
      return localizables.getString(OneCMSKeys.PRICING_BREAKDOWN_HANDLING_FEE,
          dependencyInjector.provideMarketProvider().getBrandVisualName());
    } else if (priceItemType == PricingBreakdownItemType.PAYMENT_METHOD_PRICE) {
      return localizables.getString(OneCMSKeys.PRICING_BREAKDOWN_PAYMENT_METHOD_PRICE_CREDITCARD);
    } else if (priceItemType == PricingBreakdownItemType.PAYMENT_METHOD_PRICE_FULLPRICE) {
      double priceItem = presenter.getPriceItemAmountWithType(
          PricingBreakdownItemType.PAYMENT_METHOD_PRICE_FULLPRICE, row, insuranceShoppingItem)
          .doubleValue();
      if (priceItem > 0) {
        return localizables.getString(OneCMSKeys.FULLPRICE_SUMMARY_PAYMENT_FEE);
      } else if (priceItem < 0) {
        return localizables.getString(OneCMSKeys.FULLPRICE_SUMMARY_BRAND_REDUCTION,
            dependencyInjector.provideMarketProvider().getBrandVisualName());
      } else {
        return priceItemType.name();
      }
    }
    return priceItemType.name();
  }

  private String getBaggagePriceString() {
    int numSuitcasses = searchOptions.getNumBaggages();
    if (numSuitcasses == 1) {
      return localizables.getString(OneCMSKeys.PRICING_BREAKDOWN_ONE_SUITCASE_PRICE);
    } else {
      return localizables.getString(OneCMSKeys.PRICING_BREAKDOWN_SEVERAL_SUITCASES_PRICE,
          String.valueOf(numSuitcasses));
    }
  }

  private void expandDescriptionContent() {
    CollapseAndExpandAnimationUtil.expand(priceBreakdownExpandableContent);
  }

  private void collapseDescriptionContent() {
    CollapseAndExpandAnimationUtil.collapse(priceBreakdownExpandableContent, true, scrollView,
        (View) priceBreakdownExpandableContent.getParent().getParent());
  }
}