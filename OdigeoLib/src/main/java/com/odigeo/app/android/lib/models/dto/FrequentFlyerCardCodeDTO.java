package com.odigeo.app.android.lib.models.dto;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;

public class FrequentFlyerCardCodeDTO
    implements Serializable, Parcelable, Comparable<FrequentFlyerCardCodeDTO> {

  public static final Parcelable.Creator<FrequentFlyerCardCodeDTO> CREATOR =
      new Parcelable.Creator<FrequentFlyerCardCodeDTO>() {

        @Override public FrequentFlyerCardCodeDTO createFromParcel(Parcel source) {
          return new FrequentFlyerCardCodeDTO(source);
        }

        @Override public FrequentFlyerCardCodeDTO[] newArray(int size) {
          return new FrequentFlyerCardCodeDTO[size];
        }
      };
  protected String carrierCode;
  protected String passengerCardNumber;

  public FrequentFlyerCardCodeDTO() {
    //Nothing
  }

  /**
   * Constructor for parcelable object.
   *
   * @param in Parcel to be read.
   */
  private FrequentFlyerCardCodeDTO(Parcel in) {
    String[] data = new String[2];
    in.readStringArray(data);
    carrierCode = data[0];
    passengerCardNumber = data[1];
  }

  /**
   * Gets the value of the carrierCode property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCarrierCode() {
    return carrierCode;
  }

  /**
   * Sets the value of the carrierCode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCarrierCode(String value) {
    this.carrierCode = value;
  }

  /**
   * Gets the value of the passengerCardNumber property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getPassengerCardNumber() {
    return passengerCardNumber;
  }

  /**
   * Sets the value of the passengerCardNumber property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setPassengerCardNumber(String value) {
    this.passengerCardNumber = value;
  }

  @Override public boolean equals(Object object) {
    if (object == null) {
      return false;
    }
    if (!(object instanceof FrequentFlyerCardCodeDTO)) {
      return false;
    }

    FrequentFlyerCardCodeDTO other = (FrequentFlyerCardCodeDTO) object;
    return carrierCode.equals(other.getCarrierCode()) && passengerCardNumber.equals(
        other.getPassengerCardNumber());
  }

  @Override public int compareTo(FrequentFlyerCardCodeDTO another) {

    if (another == null) {
      return 1;
    }

    return carrierCode.compareTo(another.carrierCode);
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeStringArray(new String[] { carrierCode, passengerCardNumber });
  }
}
