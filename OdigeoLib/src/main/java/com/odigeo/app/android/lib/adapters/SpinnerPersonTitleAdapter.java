package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.models.SpinnerItemModel;
import com.odigeo.app.android.lib.models.dto.TravellerTitleDTO;
import java.util.List;

/**
 * Created by Irving Lóp on 22/09/2014.
 *
 * @deprecated Use {@link OdigeoSpinnerAdapter} instead
 */
@Deprecated public class SpinnerPersonTitleAdapter
    extends ArrayAdapter<SpinnerItemModel<TravellerTitleDTO>> {
  private final List<SpinnerItemModel<TravellerTitleDTO>> personTitles;
  private final Context context;

  public SpinnerPersonTitleAdapter(Context context,
      List<SpinnerItemModel<TravellerTitleDTO>> personTitles) {
    super(context, R.layout.layout_spinner_simple_text, personTitles);
    this.context = context;
    this.personTitles = personTitles;
  }

  @Override public int getCount() {
    return personTitles.size();
  }

  @Override public SpinnerItemModel<TravellerTitleDTO> getItem(int position) {
    return personTitles.get(position);
  }

  @Override public long getItemId(int position) {
    return position;
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {
    View view = LayoutInflater.from(context).inflate(R.layout.layout_spinner_simple_text, null);
    TextView textView = (TextView) view.findViewById(R.id.textView_card_name_and_icon);
    textView.setTypeface(Configuration.getInstance().getFonts().getRegular());
    textView.setText(personTitles.get(position).getText());
    return view;
  }

  @Override public View getDropDownView(int position, View convertView, ViewGroup parent) {
    View view =
        LayoutInflater.from(context).inflate(R.layout.layout_spinner_drop_simple_text, null);
    TextView textView = (TextView) view.findViewById(R.id.textView_card_name_and_icon);
    textView.setTypeface(Configuration.getInstance().getFonts().getRegular());
    textView.setText(personTitles.get(position).getText());
    return view;
  }
}
