package com.odigeo.app.android.utils.deeplinking;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.utils.deeplinking.exception.DeepLinkingException;
import com.odigeo.app.android.utils.deeplinking.extractors.AdultsExtractor;
import com.odigeo.app.android.utils.deeplinking.extractors.ArrivalCityExtractor;
import com.odigeo.app.android.utils.deeplinking.extractors.CabinClassExtractor;
import com.odigeo.app.android.utils.deeplinking.extractors.ChildrenExtractor;
import com.odigeo.app.android.utils.deeplinking.extractors.DeepLinkingExtractor;
import com.odigeo.app.android.utils.deeplinking.extractors.DepartureCityExtractor;
import com.odigeo.app.android.utils.deeplinking.extractors.DepartureDateExtractor;
import com.odigeo.app.android.utils.deeplinking.extractors.DirectFlightExtractor;
import com.odigeo.app.android.utils.deeplinking.extractors.ExecuteSearchExtractor;
import com.odigeo.app.android.utils.deeplinking.extractors.FlightTypeExtractor;
import com.odigeo.app.android.utils.deeplinking.extractors.InfantsExtractor;
import com.odigeo.app.android.utils.deeplinking.extractors.ResidentExtractor;
import com.odigeo.app.android.utils.deeplinking.extractors.ReturnDateExtractor;
import com.odigeo.data.entity.booking.Booking;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 22/04/16
 */
public class DeepLinkingUtil {
  public static final String VALUE_SPLITTER = "=";
  public static final String PARAMETER_SPLITTER = ";";
  public static final String HOST_SPLITTER = "://";
  public static final String ACTION_SPLITTER = "/";
  public static final String TYPE = "type";
  public static final String FROM = "from";
  public static final String ADULTS = "adults";
  public static final String CHILDREN = "children";
  public static final String INFANTS = "infants";
  public static final String DIRECT = "direct";
  public static final String CLASS = "class";
  public static final String RESIDENT = "resident";
  public static final String TO = "to";
  public static final String DEPARTURE_TIME = "dep";
  public static final String RETURN_TIME = "ret";
  public static final String TRIPS = "trips";
  public static final String EXECUTE_SEARCH = "executeSearch";
  public static final String COLLECTION_METHOD = "collectionmethod";
  public static final String AIRLINE_CODES = "airlinecodes";
  public static final String INTERNAL_SEARCH = "internalSearch";
  public static final String EMPTY_STRING = "";
  public static final Map<String, DeepLinkingExtractor> EXTRACTORS = new HashMap<>();
  public static SearchOptions.Builder sBuilder;

  static {
    EXTRACTORS.put(TYPE, new FlightTypeExtractor());
    EXTRACTORS.put(FROM, new DepartureCityExtractor());
    EXTRACTORS.put(TO, new ArrivalCityExtractor());
    EXTRACTORS.put(DEPARTURE_TIME, new DepartureDateExtractor());
    EXTRACTORS.put(RETURN_TIME, new ReturnDateExtractor());
    EXTRACTORS.put(ADULTS, new AdultsExtractor());
    EXTRACTORS.put(CHILDREN, new ChildrenExtractor());
    EXTRACTORS.put(INFANTS, new InfantsExtractor());
    EXTRACTORS.put(DIRECT, new DirectFlightExtractor());
    EXTRACTORS.put(CLASS, new CabinClassExtractor());
    EXTRACTORS.put(RESIDENT, new ResidentExtractor());
    EXTRACTORS.put(EXECUTE_SEARCH, new ExecuteSearchExtractor());
  }

  private DeepLinkingUtil() {
    //nothing
  }

  public static DeepLinkType getCampaignCardType(Context context, Uri uri) {
    DeepLinkType deepLinkType = DeepLinkType.OTHER;
    if (context != null && uri != null && uri.getHost() != null) {
      if (uri.getHost().equals(context.getString(R.string.deeplink_host_search)) && uri.getPath()
          .contains(context.getString(R.string.deeplink_prefix_search))) {
        deepLinkType = DeepLinkType.SEARCH;
      }
      if (uri.getHost().equals(context.getString(R.string.deeplink_host_login))) {
        deepLinkType = DeepLinkType.SSO;
      }
      if (uri.getHost().equals(context.getString(R.string.deeplink_host_cars))) {
        deepLinkType = DeepLinkType.CARS;
      }
      if (uri.getHost().equals(context.getString(R.string.deeplink_host_hotels))) {
        deepLinkType = DeepLinkType.HOTELS;
      }
      if (uri.getHost().equals(context.getString(R.string.deeplink_host_trips))) {
        deepLinkType = DeepLinkType.TRIPS;
      }
    }
    return deepLinkType;
  }

  public static SearchOptions getSearchOptionsFromDeepLinkingIntent(Intent intent)
      throws DeepLinkingException {
    sBuilder = new SearchOptions.Builder();
    Uri uri = intent.getData();
    if (uri != null) {
      String parametersPath = uri.getLastPathSegment();
      if (parametersPath.contains(PARAMETER_SPLITTER)) {
        parametersPath = parametersPath.substring(parametersPath.indexOf(PARAMETER_SPLITTER) + 1);
        String[] parameters = parametersPath.split(PARAMETER_SPLITTER);
        for (String parameter : parameters) {
          try {
            readAndExtractParameter(parameter);
          } catch (IndexOutOfBoundsException e) {
            throw new DeepLinkingException(uri.toString(), e);
          }
        }
        return sBuilder.build();
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  public static Map.Entry<String, Booking> getBookingFromDeepLinkingIntent(Intent intent)
      throws DeepLinkingException {
    Uri uri = intent.getData();
    if (uri != null) {
      String bookingId = uri.getLastPathSegment();
      if (bookingId != null && !bookingId.equals(EMPTY_STRING) && !bookingId.equals(TRIPS)) {
        String emailParameter = uri.getQuery();
        String[] emailParameters = emailParameter.split(VALUE_SPLITTER);
        Booking booking = AndroidDependencyInjector.getInstance()
            .provideBookingInteractor()
            .getBookingById(Long.parseLong(bookingId));
        return new AbstractMap.SimpleEntry<>(emailParameters[1], booking);
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  private static void readAndExtractParameter(String parameter) throws IndexOutOfBoundsException {
    String[] parameterItems = parameter.split(VALUE_SPLITTER);
    String value = parameterItems[1];
    String key = parameterItems[0];

    try {
      DeepLinkingExtractor extractor = EXTRACTORS.get(getMapKey(key));
      extractor.extractParameter(sBuilder, key, value);
    } catch (NullPointerException exception) {
      // It doesnt process unhandled parameter
    }
  }

  private static String getMapKey(String key) {
    int lastIndex = key.length() - 1;
    char lastChar = key.charAt(lastIndex);
    if (Character.isDigit(lastChar)) {
      return key.substring(0, lastIndex);
    } else {
      return key;
    }
  }

  public static String build(String scheme, String authority, List<String> paths,
      Map<String, String> params) {

    if (StringUtils.isAnyEmpty(scheme, authority)) {
      return null;
    }

    StringBuilder builder = new StringBuilder(scheme).append("://").append(authority);

    for (String path : paths) {
      builder.append("/").append(path);
    }

    if (!params.isEmpty()) {
      builder.append("?");

      boolean mustAddSeparator = false;
      for (Map.Entry<String, String> param : params.entrySet()) {

        if (mustAddSeparator && params.size() > 1) {
          builder.append("&");
        }

        builder.append(param.getKey()).append("=").append(param.getValue());

        mustAddSeparator = true;
      }
    }

    return builder.toString();
  }

  public enum DeepLinkType {
    SEARCH, CARS, HOTELS, SSO, TRIPS, OTHER
  }
}
