package com.odigeo.app.android.view.adapters.holders;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.utils.BookingUtils;
import com.odigeo.app.android.view.adapters.BookingAdapter;
import com.odigeo.app.android.view.custom.CustomTextImageView;
import com.odigeo.app.android.view.imageutils.BookingImageUtil;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import com.odigeo.tools.DateHelperInterface;
import java.util.List;

import static com.odigeo.data.entity.booking.Booking.TRIP_TYPE_MULTI_SEGMENT;
import static com.odigeo.data.entity.booking.Booking.TRIP_TYPE_ROUND_TRIP;

public class PastBookingCardHolder extends BaseRecyclerViewHolder<Booking> {

  private Context context;

  private final DateHelperInterface dateHelper;
  private final View card;
  private final ImageView ivTrip;
  private final TextView tvDate;
  private final CustomTextImageView originDepartureNamesText;

  private final BookingAdapter.OnBookingClickListener bookingClickListener;

  private final OdigeoImageLoader<ImageView> imageLoader;
  private final BookingImageUtil bookingImageUtil;

  public static PastBookingCardHolder newInstance(ViewGroup container,
      DateHelperInterface dateHelper,
      BookingAdapter.OnBookingClickListener onBookingClickListener) {

    View view = LayoutInflater.from(container.getContext())
        .inflate(R.layout.my_past_booking_item, container, false);
    return new PastBookingCardHolder(view, dateHelper, onBookingClickListener);
  }

  PastBookingCardHolder(View itemView, DateHelperInterface dataHelper,
      BookingAdapter.OnBookingClickListener onBookingClickListener) {
    super(itemView);
    this.dateHelper = dataHelper;
    this.card = itemView;
    this.context = card.getContext();
    this.bookingClickListener = onBookingClickListener;
    ivTrip = (ImageView) itemView.findViewById(R.id.booking_background_image);
    tvDate = (TextView) itemView.findViewById(R.id.tvDate);
    originDepartureNamesText =
        (CustomTextImageView) itemView.findViewById(R.id.originDepartureText);

    imageLoader = dependencyInjector.provideImageLoader();
    bookingImageUtil = dependencyInjector.provideMyTripsImageUtil();
  }

  private void setListeners(View root, final Booking item, final int position) {
    root.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {

        if (bookingClickListener != null) {
          bookingClickListener.onClick(position, item);
        }
      }
    });
  }

  @Override public void bind(int position, Booking booking) {
    setImageViewAsBlackAndWhite();

    bookingImageUtil.loadRoundedBookingImage(context, imageLoader, ivTrip, booking);

    setOriginDepartureCityNames(booking);
    tvDate.setText(formatDates(booking));
    setListeners(card, booking, position);
  }

  private void setOriginDepartureCityNames(Booking booking) {
    originDepartureNamesText.clearAll();
    addFirstCity(booking);
    if (isMultiTrip(booking)) {
      addMultiTripsCities(booking);
    } else {
      addLastCity(booking);
    }
  }

  public void setImageViewAsBlackAndWhite() {
    ColorMatrix matrix = new ColorMatrix();
    matrix.setSaturation(0);
    ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
    ivTrip.setColorFilter(filter);
  }

  private void addMultiTripsCities(Booking booking) {
    List<Segment> segments = booking.getSegments();
    for (Segment segment : segments) {
      originDepartureNamesText.addText(segment.getLastSection().getTo().getCityName());
      if (!segment.equals(segments.get(segments.size() - 1))) {
        originDepartureNamesText.addImage(getTripType(booking));
      }
    }
  }

  private void addLastCity(Booking booking) {
    originDepartureNamesText.addText(
        booking.getFirstSegment().getLastSection().getTo().getCityName());
  }

  private void addFirstCity(Booking booking) {
    originDepartureNamesText.addText(
        booking.getFirstSegment().getFirstSection().getFrom().getCityName());
    originDepartureNamesText.addImage(getTripType(booking));
  }

  private String formatDates(Booking booking) {
    if (isRoundTrip(booking)) {
      String datesFormat = context.getResources().getString(R.string.format_round_trip_dates);

      String date = String.format(datesFormat,
          BookingUtils.formatSegmentDate(booking.getFirstSegment(), dateHelper,
              localizableProvider),
          BookingUtils.formatSegmentDate(booking.getLastSegment(), dateHelper,
              localizableProvider));

      return date;
    } else {
      return BookingUtils.formatSegmentDate(booking.getFirstSegment(), dateHelper,
          localizableProvider);
    }
  }

  public int getTripType(Booking booking) {
    if (isRoundTrip(booking)) {
      return R.drawable.arrow_round_trip;
    } else {
      return R.drawable.arrow_one_way_trip;
    }
  }

  private boolean isRoundTrip(Booking booking) {
    return booking.getTripType().equals(TRIP_TYPE_ROUND_TRIP);
  }

  private boolean isMultiTrip(Booking booking) {
    return booking.getTripType().equals(TRIP_TYPE_MULTI_SEGMENT);
  }

  public View getCard() {
    return card;
  }
}

