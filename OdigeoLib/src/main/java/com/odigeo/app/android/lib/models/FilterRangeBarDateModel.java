package com.odigeo.app.android.lib.models;

import java.util.Date;

/**
 * Created by Irving on 19/10/2014.
 */
public class FilterRangeBarDateModel {
  private String titleString;
  private String cityName;
  private Date minDate;
  private Date maxDate;

  public final String getTitleString() {
    return titleString;
  }

  public final void setTitleString(String titleString) {
    this.titleString = titleString;
  }

  public final String getCityName() {
    return cityName;
  }

  public final void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public final Date getMinDate() {
    return minDate;
  }

  public final void setMinDate(Date minDate) {
    this.minDate = minDate;
  }

  public final Date getMaxDate() {
    return maxDate;
  }

  public final void setMaxDate(Date maxDate) {
    this.maxDate = maxDate;
  }
}
