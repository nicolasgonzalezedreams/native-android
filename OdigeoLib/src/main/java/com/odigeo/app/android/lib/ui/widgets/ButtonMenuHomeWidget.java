package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.HomeOptionsButtons;

/**
 * Created by Irving on 26/08/2014.
 */
public class ButtonMenuHomeWidget extends RelativeLayout {
  public View rootView;
  public ImageView iconImageView;
  public TextView titleTextView;
  private HomeOptionsButtons optionsButton;

  public ButtonMenuHomeWidget(Context context, int idLayout, HomeOptionsButtons optionsButton) {
    super(context);
    init(context, idLayout);
    this.optionsButton = optionsButton;
  }

  public ButtonMenuHomeWidget(Context context, AttributeSet attrs, int idLayout) {
    super(context, attrs);
    init(context, idLayout);
  }

  /**
   * Initialization of the view.
   *
   * @param context Context where be constructed.
   * @param idLayout Layout to be inflated.
   */
  private void init(Context context, int idLayout) {
    setRootView(LayoutInflater.from(context).inflate(idLayout, this));
    setIconImageView((ImageView) getRootView().findViewById(R.id.icon_menu_home_widget));
    setTitleTextView((TextView) getRootView().findViewById(R.id.title_menuHomeWidget));
    this.setClickable(true);
  }

  public final void setIcon(int idIcon) {
    getIconImageView().setImageResource(idIcon);
  }

  public final void setTitle(CharSequence title) {
    getTitleTextView().setText(title);
  }

  @Override public final View getRootView() {
    return rootView;
  }

  public final void setRootView(View rootView) {
    this.rootView = rootView;
  }

  public final ImageView getIconImageView() {
    return iconImageView;
  }

  public final void setIconImageView(ImageView iconImageView) {
    this.iconImageView = iconImageView;
  }

  public final TextView getTitleTextView() {
    return titleTextView;
  }

  public final void setTitleTextView(TextView titleTextView) {
    this.titleTextView = titleTextView;
  }

  public final HomeOptionsButtons getOptionsButton() {
    return optionsButton;
  }

  public final void setOptionsButton(HomeOptionsButtons optionsButton) {
    this.optionsButton = optionsButton;
  }
}
