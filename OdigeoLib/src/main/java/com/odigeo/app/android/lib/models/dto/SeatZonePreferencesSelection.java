package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public class SeatZonePreferencesSelection implements Serializable {
  private List<Integer> sections;
  private Collection<SeatZonePreferencesSelectionItem> seatZonePreferencesSelectionItems;

  public List<Integer> getSections() {
    return sections;
  }

  public void setSections(List<Integer> sections) {
    this.sections = sections;
  }

  public Collection<SeatZonePreferencesSelectionItem> getSeatZonePreferencesSelectionItems() {
    return seatZonePreferencesSelectionItems;
  }

  public void setSeatZonePreferencesSelectionItems(
      Collection<SeatZonePreferencesSelectionItem> seatZonePreferencesSelectionItems) {
    this.seatZonePreferencesSelectionItems = seatZonePreferencesSelectionItems;
  }
}
