package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.LocationDTO;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import java.util.ArrayList;
import java.util.List;

public class FilterListWidget extends LinearLayout implements View.OnClickListener {
  private static final int TIME_FADE_ANIMATION_TEXT_BUTTON = 1000;

  private TextView textButtomSelectAll;
  private LinearLayout layoutContainer;
  //    private FormOdigeoLayout formOdigeoLayout;

  private FilterCheckBoxWidget[] options;

  private int numOptionsSelected;
  private boolean isAllSelected;

  private boolean isUsedOnAirlineFilter;
  private boolean isUsedOnAirportFilter;

  private int segmentNumber;

  private String cityName;
  private Context mContext;

  public FilterListWidget(Context context) {
    super(context);
    mContext = context;
    inflateLayout();
  }

  public FilterListWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
    mContext = context;

    inflateLayout();

    TypedArray aAttrs = context.obtainStyledAttributes(attrs, R.styleable.FilterListWidget, 0, 0);
    isUsedOnAirlineFilter =
        aAttrs.getBoolean(R.styleable.FilterListWidget_isUsedOnAirlineFilter, false);
    isUsedOnAirportFilter =
        aAttrs.getBoolean(R.styleable.FilterListWidget_isUsedOnAirportFilter, false);

    if (isUsedOnAirlineFilter) {
      setTitle(
          LocalizablesFacade.getString(mContext, OneCMSKeys.WAITINGVIEW_AIRLINESCOUNTER_SUBTITLE));
    }
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.layout_filter_list_widget, this);
    setLayoutContainer((LinearLayout) findViewById(R.id.layout_filter_list_container));
    LinearLayout buttonSelectAll =
        (LinearLayout) findViewById(R.id.layout_filter_list_button_select_all);
    textButtomSelectAll = (TextView) findViewById(R.id.text_filter_list_select_all);

    buttonSelectAll.setOnClickListener(this);
  }

  private void init() {
    isAllSelected = true;
    drawOptions();
    setAllSelected(isAllSelected);
    numOptionsSelected = options.length;
  }

  private void drawOptions() {
    if (options != null) {
      for (FilterCheckBoxWidget widget : options) {
        getLayoutContainer().addView(widget);
      }
    }
  }

  public final String getTitle() {
    return ((TextView) findViewById(R.id.tvFilterTimeTitle)).getText().toString();
  }

  public final void setTitle(CharSequence textTitle) {
    if (textTitle != null) {
      ((TextView) findViewById(R.id.tvFilterTimeTitle)).setText(textTitle.toString());
    }
  }

  public final void setValues(List<String> nameOptions) {
    if (nameOptions != null && !nameOptions.isEmpty()) {
      options = new FilterCheckBoxWidget[nameOptions.size()];
      for (int i = 0; i < options.length; i++) {
        options[i] = new FilterCheckBoxWidget(getContext(), nameOptions.get(i));
        options[i].setOnClickListener(this);

        if (i == nameOptions.size() - 1) {
          options[i].setLastInForm(true);
        }
      }
    }
    init();
  }

  public final void setCarriers(List<CarrierDTO> carriers) {
    if (carriers != null && !carriers.isEmpty()) {
      options = new FilterCheckBoxWidget[carriers.size()];
      for (int i = 0; i < options.length; i++) {
        options[i] = new FilterCheckBoxWidget(getContext(), carriers.get(i).getName());
        options[i].setExtra(carriers.get(i).getCode());
        options[i].setOnClickListener(this);

        if (i == carriers.size() - 1) {
          options[i].setLastInForm(true);
        }
      }
    }
    init();
  }

  public final void setAirports(List<LocationDTO> locations) {
    if (locations != null && !locations.isEmpty()) {
      options = new FilterCheckBoxWidget[locations.size()];
      for (int i = 0; i < options.length; i++) {
        options[i] = new FilterCheckBoxWidget(getContext(), locations.get(i).getName());
        options[i].setExtra(locations.get(i).getIataCode());
        options[i].setOnClickListener(this);

        if (i == locations.size() - 1) {
          options[i].setLastInForm(true);
        }
      }
    }
    init();
  }

  @Override public final void onClick(View v) {
    if (v.getId() == R.id.layout_filter_list_button_select_all) {
      isAllSelected = !isAllSelected;
      if (isAllSelected) {
        isAllSelectedButtonTrue();
      } else {
        isAllSelectedButtonFalse();
      }
      setAllSelected(isAllSelected);

      if (isUsedOnAirportFilter) {
        postGAEventFilterSelection(
            GAnalyticsNames.LABEL_PARTIAL_AIRPORTS_SELECT_ALL_SEGMENT + this.getSegmentNumber());
      }
    } else {
      isNotUsedOnAirportFilter(v);
    }
  }

  private void isNotUsedOnAirportFilter(View v) {
    FilterCheckBoxWidget widget = (FilterCheckBoxWidget) v;
    if (widget.isCheck()) {
      if (numOptionsSelected > 1) {
        widget.setCheck(false);
        numOptionsSelected--;

        if (isUsedOnAirlineFilter) {
          postGAEventFilterSelection(GAnalyticsNames.LABEL_PARTIAL_AIRLINE_UNCHECKED
              + ((FilterCheckBoxWidget) v).getTextCheck().replace(" ", "_").toLowerCase());
        } else if (isUsedOnAirportFilter) {
          postGAEventFilterSelection(GAnalyticsNames.LABEL_PARTIAL_AIRPORT_CHECKED
              + ((FilterCheckBoxWidget) v).getExtra()
              + GAnalyticsNames.LABEL_PARTIAL_SEGMENT
              + this.getSegmentNumber());
        }
      }
    } else {
      widget.setCheck(true);
      numOptionsSelected++;

      if (isUsedOnAirlineFilter) {
        postGAEventFilterSelection(GAnalyticsNames.LABEL_PARTIAL_AIRLINE_CHECKED
            + ((FilterCheckBoxWidget) v).getTextCheck().replace(" ", "_").toLowerCase());
      } else if (isUsedOnAirportFilter) {
        postGAEventFilterSelection(GAnalyticsNames.LABEL_PARTIAL_AIRPORT_CHECKED
            + ((FilterCheckBoxWidget) v).getExtra()
            + GAnalyticsNames.LABEL_PARTIAL_SEGMENT
            + this.getSegmentNumber());
      }
    }
    if (numOptionsSelected == options.length) {
      isAllSelected = true;
      changeTextButton(textButtomSelectAll,
          LocalizablesFacade.getString(getContext(), "airportfilterviewcontroller_deselectall"));
    } else if (numOptionsSelected == options.length - 1) {
      isAllSelected = false;
      changeTextButton(textButtomSelectAll,
          LocalizablesFacade.getString(getContext(), "airportfilterviewcontroller_selectall"));
    }
  }

  private void isAllSelectedButtonFalse() {
    changeTextButton(textButtomSelectAll,
        LocalizablesFacade.getString(getContext(), "airportfilterviewcontroller_selectall"));
    numOptionsSelected = 0;
    if (isUsedOnAirlineFilter) {
      postGAEventFilterSelection(GAnalyticsNames.LABEL_AIRLINES_DESELECT_ALL);
    }
  }

  private void isAllSelectedButtonTrue() {
    changeTextButton(textButtomSelectAll,
        LocalizablesFacade.getString(getContext(), "airportfilterviewcontroller_deselectall"));
    numOptionsSelected = options.length;
    if (isUsedOnAirlineFilter) {
      postGAEventFilterSelection(GAnalyticsNames.LABEL_AIRLINES_SELECT_ALL);
    }
  }

  private void changeTextButton(TextView button, CharSequence text) {

    Animation in = new AlphaAnimation(0.0f, 1.0f);
    in.setDuration(TIME_FADE_ANIMATION_TEXT_BUTTON);

    Animation out = new AlphaAnimation(1.0f, 0.0f);
    out.setDuration(TIME_FADE_ANIMATION_TEXT_BUTTON);
    button.setText(text);
  }

  private void setAllSelected(boolean value) {
    if (options != null && options.length > 0) {
      for (FilterCheckBoxWidget widget : options) {
        widget.setCheck(value);
      }
    }
  }

  public final List<String> getSelectedNames() {
    List<String> results = new ArrayList<String>();
    for (FilterCheckBoxWidget widget : options) {
      if (widget.isCheck()) {
        results.add(widget.getTextCheck());
      }
    }
    return results;
  }

  public final void setSelectedNames(List<String> names) {
    for (FilterCheckBoxWidget widget : options) {
      if (names.contains(widget.getTextCheck())) {
        widget.setCheck(true);
      } else {
        widget.setCheck(false);
      }
    }
  }

  public final LinearLayout getLayoutContainer() {
    return layoutContainer;
  }

  private void setLayoutContainer(LinearLayout layoutContainer) {
    this.layoutContainer = layoutContainer;
  }

  public final String getCityName() {
    return cityName;
  }

  public final void setCityName(String cityName) {
    this.cityName = cityName;

    setTitle(String.format("%s", cityName));
  }

  public final void setNoFormatCityName(String cityName) {
    this.cityName = cityName;

    setTitle(String.format("%s", cityName));
  }

  public final boolean isUsedOnAirportFilter() {
    return isUsedOnAirportFilter;
  }

  public final void setUsedOnAirportFilter(boolean isUsedOnAirportFilter) {
    this.isUsedOnAirportFilter = isUsedOnAirportFilter;
  }

  public final boolean isUsedOnAirlineFilter() {
    return isUsedOnAirlineFilter;
  }

  public final void setUsedOnAirlineFilter(boolean isUsedOnAirlineFilter) {
    this.isUsedOnAirlineFilter = isUsedOnAirlineFilter;
  }

  public final int getSegmentNumber() {
    return segmentNumber;
  }

  public final void setSegmentNumber(int segmentNumber) {
    this.segmentNumber = segmentNumber;
  }

  private void postGAEventFilterSelection(String label) {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_FILTERS,
            GAnalyticsNames.ACTION_RESULTS_FILTERS, label));
  }
}
