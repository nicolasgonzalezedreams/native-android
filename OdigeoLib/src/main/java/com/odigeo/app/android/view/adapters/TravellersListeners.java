package com.odigeo.app.android.view.adapters;

/**
 * Created by carlos.navarrete on 09/09/15.
 */
public interface TravellersListeners {
  void onClick(boolean isDefaultTraveller);

  void onClickTraveller(long userTravellerId, boolean isDefaultTraveller);
}
