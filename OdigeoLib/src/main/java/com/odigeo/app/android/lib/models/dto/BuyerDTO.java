package com.odigeo.app.android.lib.models.dto;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Created by Javier Marsicano on 02/05/16.
 */
public class BuyerDTO implements Serializable {

  @SerializedName("lastNames") private String mLastNames;
  @SerializedName("identification") private String mIdentification;
  @SerializedName("buyerIdentificationType") private String mBuyerIdentificationType;
  @SerializedName("country") private String mCountry;
  @SerializedName("name") private String mName;
  @SerializedName("address") private String mAddress;
  @SerializedName("mail") private String mMail;
  @SerializedName("cityName") private String mCityName;
  @SerializedName("phoneNumber") private PhoneDTO mPhoneNumber;
  @SerializedName("alternativePhoneNumber") private PhoneDTO mAlternativePhoneNumber;
  @SerializedName("cpf") private String mCpf;
  @SerializedName("zipCode") private String mZipCode;
  @SerializedName("stateName") private String mStateName;

  public String getLastNames() {
    return mLastNames;
  }

  public void setLastNames(String lastNames) {
    this.mLastNames = lastNames;
  }

  public String getIdentification() {
    return mIdentification;
  }

  public void setIdentification(String identification) {
    this.mIdentification = identification;
  }

  public String getBuyerIdentificationType() {
    return mBuyerIdentificationType;
  }

  public void setBuyerIdentificationType(String buyerIdentificationType) {
    this.mBuyerIdentificationType = buyerIdentificationType;
  }

  public String getCountry() {
    return mCountry;
  }

  public void setCountry(String country) {
    this.mCountry = country;
  }

  public String getName() {
    return mName;
  }

  public void setName(String name) {
    this.mName = name;
  }

  public String getAddress() {
    return mAddress;
  }

  public void setAddress(String address) {
    this.mAddress = address;
  }

  public String getMail() {
    return mMail;
  }

  public void setMail(String mail) {
    this.mMail = mail;
  }

  public String getCityName() {
    return mCityName;
  }

  public void setCityName(String cityName) {
    this.mCityName = cityName;
  }

  public PhoneDTO getPhoneNumber() {
    return mPhoneNumber;
  }

  public void setPhoneNumber(PhoneDTO phoneNumber) {
    this.mPhoneNumber = phoneNumber;
  }

  public PhoneDTO getAlternativePhoneNumber() {
    return mAlternativePhoneNumber;
  }

  public void setAlternativePhoneNumber(PhoneDTO alternativePhoneNumber) {
    this.mAlternativePhoneNumber = alternativePhoneNumber;
  }

  public String getCpf() {
    return mCpf;
  }

  public void setCpf(String cpf) {
    this.mCpf = cpf;
  }

  public String getZipCode() {
    return mZipCode;
  }

  public void setZipCode(String zipCode) {
    this.mZipCode = zipCode;
  }

  public String getStateName() {
    return mStateName;
  }

  public void setStateName(String stateName) {
    this.mStateName = stateName;
  }
}
