package com.odigeo.app.android.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.navigator.MyTripDetailsNavigator;
import com.odigeo.data.entity.booking.Booking;

public class BookingStatusOdigeoNotificationBuilder extends OdigeoNotificationBuilder {

  private Booking booking;

  public BookingStatusOdigeoNotificationBuilder(Context context) {
    super();
    this.context = context;
  }

  public void setBooking(Booking booking) {
    this.booking = booking;
  }

  @Override public void buildNotificationBuilder() {
    NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
    builder.setSmallIcon(R.drawable.noticon);
    builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.icon));
    builder.setContentTitle(getNotificationTitle());
    builder.setContentText(getNotificationText());
    builder.setAutoCancel(true);
    odigeoNotification.setNotificationBuilder(builder);
  }

  @Override public void buildNotificationIntent() {
    Intent intent = MyTripDetailsNavigator.startIntent(context, booking);
    odigeoNotification.setNotificationIntent(intent);
  }

  @Override public void buildNotificationTaskStackBuilder() {
    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
    stackBuilder.addParentStack(MyTripDetailsNavigator.class);
    stackBuilder.addNextIntent(odigeoNotification.getNotificationIntent());
    odigeoNotification.setNotificationTaskStackBuilder(stackBuilder);
  }

  @Override public void buildPendingIntent() {
    PendingIntent contentIntent = odigeoNotification.getNotificationTaskStackBuilder().
        getPendingIntent((int) booking.getBookingId(), PendingIntent.FLAG_UPDATE_CURRENT);
    odigeoNotification.getNotificationBuilder().setContentIntent(contentIntent);
  }

  @Override public void sendNotification() {
    NotificationManager mNotificationManager =
        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    mNotificationManager.notify((int) booking.getBookingId(),
        odigeoNotification.getNotificationBuilder().build());
  }

  private String getNotificationTitle() {
    if (booking.getBookingStatus().equals(Booking.BOOKING_STATUS_CONTRACT)) {
      return LocalizablesFacade.getString(context, "checkbookingstatus_message_ok").toString();
    } else {
      return LocalizablesFacade.getString(context, "checkbookingstatus_message_error").toString();
    }
  }

  private String getNotificationText() {
    if (booking.getBookingStatus().equals(Booking.BOOKING_STATUS_CONTRACT)) {
      return LocalizablesFacade.getString(context, "checkbookingstatus_oknotification").toString();
    } else {
      return LocalizablesFacade.getString(context, "checkbookingstatus_konotification").toString();
    }
  }
}
