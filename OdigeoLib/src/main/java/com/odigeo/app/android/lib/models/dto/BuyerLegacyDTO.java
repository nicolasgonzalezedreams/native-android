package com.odigeo.app.android.lib.models.dto;

import android.support.annotation.Nullable;
import com.odigeo.app.android.lib.models.Passenger;
import java.io.Serializable;

/**
 * Created by ManuelOrtiz on 17/09/2014.
 *
 * @author Irving
 * @since 15/05/2015
 */
public class BuyerLegacyDTO extends Passenger implements Serializable {

  private String stringCountryPhoneNumber;
  private String identification;
  private String lastNames;
  private String buyerIdentificationTypeName;
  private String countryCode;

  @Nullable private String buyerCardNumber;

  public String getStringCountryPhoneNumber() {
    return stringCountryPhoneNumber;
  }

  public void setStringCountryPhoneNumber(String countryPhoneNumber) {
    this.stringCountryPhoneNumber = countryPhoneNumber;
  }

  public String getIdentification() {
    return identification;
  }

  public void setIdentification(String identification) {
    this.identification = identification;
  }

  public String getLastNames() {
    return lastNames;
  }

  public void setLastNames(String lastNames) {
    this.lastNames = lastNames;
  }

  public String getBuyerIdentificationTypeName() {
    return buyerIdentificationTypeName;
  }

  public void setBuyerIdentificationTypeName(String buyerIdentificationTypeName) {
    this.buyerIdentificationTypeName = buyerIdentificationTypeName;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  @Nullable public String getBuyerCardNumber() {

    return buyerCardNumber;
  }

  public void setBuyerCardNumber(@Nullable String buyerCardNumber) {

    this.buyerCardNumber = obfuscateCardNumber(buyerCardNumber);
  }

  private String obfuscateCardNumber(String buyerCardNumber) {
    int maxCharactersShowed = 4;
    StringBuilder builder = new StringBuilder();

    for (int i = 0; i < buyerCardNumber.length(); i++) {
      if (i < buyerCardNumber.length() - maxCharactersShowed) {
        builder.append('*');
      } else {
        builder.append(buyerCardNumber.charAt(i));
      }
    }
    return builder.toString();
  }
}
