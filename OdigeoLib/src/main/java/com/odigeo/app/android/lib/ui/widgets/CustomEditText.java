package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.EditText;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.LocalizablesFacade;

/**
 * Created by ManuelOrtiz on 03/10/2014.
 */
@Deprecated public class CustomEditText extends EditText {
  public CustomEditText(Context context) {
    super(context);
  }

  public CustomEditText(Context context, AttributeSet attrs) {
    super(context, attrs);
    if (!isInEditMode()) {
      parseAttributes(context, attrs);
    }
  }

  public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    if (!isInEditMode()) {
      parseAttributes(context, attrs);
    }
  }

  /**
   * Parse the attributes.
   *
   * @param context The Context the view is running in, through which it can access the current
   * theme,
   * resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   */
  private void parseAttributes(Context context, AttributeSet attrs) {

    if (!isInEditMode()) {
      TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.TextViewWithFont);

      String odigeoDbKey = values.getString(R.styleable.OdigeoTextAttrs_odigeoText);

      int typefaceValue = values.getInt(R.styleable.TextViewWithFont_typeface, 0);
      values.recycle();

      if (odigeoDbKey != null) {
        setText(LocalizablesFacade.getString(context, odigeoDbKey));
      }

      setTypeface(Configuration.getInstance().getFonts().get(typefaceValue));
    }
  }
}
