package com.odigeo.app.android.lib.models.dto;

import android.support.annotation.NonNull;
import java.util.LinkedList;
import java.util.List;

/**
 * Model to represent a list of BookingSummaries
 */
public final class CheckedBookingsDTO {

  /**
   * List of checked bookings
   */
  @NonNull List<CheckedBookingDTO> bookings;

  public CheckedBookingsDTO() {
    bookings = new LinkedList<>();
  }

  public CheckedBookingsDTO(@NonNull List<CheckedBookingDTO> bookings) {
    this.bookings = bookings;
  }

  /**
   * @return the bookings
   */
  @NonNull public List<CheckedBookingDTO> getBookings() {
    return bookings;
  }

  /**
   * @param bookings the bookings to set
   */
  public void setBookings(@NonNull List<CheckedBookingDTO> bookings) {
    this.bookings = bookings;
  }
}
