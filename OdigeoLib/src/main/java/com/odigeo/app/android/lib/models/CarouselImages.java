package com.odigeo.app.android.lib.models;

import java.io.Serializable;

/**
 * Created by arturo.padron on 12/02/2015.
 */
public class CarouselImages implements Serializable {

  private String imageURL;
  private String imageText;

  public CarouselImages(String imageUrl, String imageText) {
    this.imageURL = imageUrl;
    this.imageText = imageText;
  }

  public final String getImageURL() {

    return imageURL;
  }

  public final void setImageURL(String imageURL) {
    this.imageURL = imageURL;
  }

  public final String getImageText() {
    return imageText;
  }

  public final void setImageText(String imageText) {
    this.imageText = imageText;
  }
}
