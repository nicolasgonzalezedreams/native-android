package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class MarketingRevenueDataAdapterDTO {

  public static final String ABSOLUTE_VALUE = "ABSOLUTE";
  private String type;
  private BigDecimal value;

  /**
   * Gets the value of the type property.
   */
  public String getType() {
    return type;
  }

  /**
   * Sets the value of the type property.
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * Gets the value of the value property.
   */
  public BigDecimal getValue() {
    return value;
  }

  /**
   * Sets the value of the value property.
   */
  public void setValue(BigDecimal value) {
    this.value = value;
  }
}
