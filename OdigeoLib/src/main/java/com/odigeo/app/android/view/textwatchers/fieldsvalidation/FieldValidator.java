package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

public interface FieldValidator {
  boolean validateField(String fieldInformation);

  boolean validateCodification(String fieldInformation);
}
