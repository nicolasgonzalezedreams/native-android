package com.odigeo.app.android.lib.models.dto.v1;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>Java class for baggageFee complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="baggageFee">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="numBags" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="fee" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class BaggageFeeDTO implements Serializable {

  protected Integer numBags;
  protected BigDecimal fee;

  /**
   * Gets the value of the numBags property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getNumBags() {
    return numBags;
  }

  /**
   * Sets the value of the numBags property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setNumBags(Integer value) {
    this.numBags = value;
  }

  /**
   * Gets the value of the fee property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getFee() {
    return fee;
  }

  /**
   * Sets the value of the fee property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setFee(BigDecimal value) {
    this.fee = value;
  }
}
