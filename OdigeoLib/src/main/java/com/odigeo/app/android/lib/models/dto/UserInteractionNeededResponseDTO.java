package com.odigeo.app.android.lib.models.dto;

import java.util.Map;

@Deprecated public class UserInteractionNeededResponseDTO {

  protected String redirectUrl;
  protected FormSendTypeDTO formSendType;
  protected String htmlCode;
  protected Map<String, String> parameters;

  /**
   * Gets the value of the redirectUrl property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getRedirectUrl() {
    return redirectUrl;
  }

  /**
   * Sets the value of the redirectUrl property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setRedirectUrl(String value) {
    this.redirectUrl = value;
  }

  /**
   * Gets the value of the formSendType property.
   *
   * @return possible object is
   * {@link FormSendTypeDTO }
   */
  public FormSendTypeDTO getFormSendType() {
    return formSendType;
  }

  /**
   * Sets the value of the formSendType property.
   *
   * @param value allowed object is
   * {@link FormSendTypeDTO }
   */
  public void setFormSendType(FormSendTypeDTO value) {
    this.formSendType = value;
  }

  /**
   * Gets the value of the htmlCode property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getHtmlCode() {
    return htmlCode;
  }

  /**
   * Sets the value of the htmlCode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setHtmlCode(String value) {
    this.htmlCode = value;
  }

  /**
   * @return the parameters
   */
  public Map<String, String> getParameters() {
    return parameters;
  }

  /**
   * @param parameters the parameters to set
   */
  public void setParameters(Map<String, String> parameters) {
    this.parameters = parameters;
  }
}
