package com.odigeo.app.android.lib.utils.exceptions;

import com.google.gson.Gson;
import com.odigeo.data.entity.shoppingCart.BookingResponse;

public class BookingResponseException extends Exception {

  private BookingResponseException(String detailMessage) {
    super(detailMessage);
  }

  public static BookingResponseException newInstance(BookingResponse bookingResponse) {
    Gson gson = new Gson();
    String json = gson.toJson(bookingResponse);
    return new BookingResponseException(json);
  }
}
