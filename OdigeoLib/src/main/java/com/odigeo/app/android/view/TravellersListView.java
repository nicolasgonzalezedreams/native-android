package com.odigeo.app.android.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.navigator.BaseNavigator;
import com.odigeo.app.android.view.adapters.TravellersAdapter;
import com.odigeo.app.android.view.adapters.TravellersListeners;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.TravellersListPresenter;
import com.odigeo.presenter.contracts.navigators.TravellersListNavigatorInterface;
import com.odigeo.presenter.contracts.views.TravellersListViewInterface;

import static com.odigeo.dataodigeo.tracker.TrackerConstants.NEW_PAX_TRAVELLERS_TAP_EVENT;

/**
 * Created by carlos.navarrete on 28/08/15.
 */
public class TravellersListView extends BaseView<TravellersListPresenter>
    implements TravellersListViewInterface {

  public static TravellersListView newInstance() {
    Bundle args = new Bundle();
    TravellersListView fragment = new TravellersListView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = super.onCreateView(inflater, container, savedInstanceState);
    ((BaseNavigator) getActivity()).setNavigationTitle(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_TRAVELLERS).toString());
    ((BaseNavigator) getActivity()).setUpToolbarButton(0);
    return view;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override public void onResume() {
    super.onResume();
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_MYINFO_FREQUENT_TRAVELLERS_LIST);
  }

  protected TravellersListPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideTravellersListPresenter(this, (TravellersListNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.sso_travellers;
  }

  @Override protected void initComponent(View view) {
    RecyclerView.LayoutManager mTravellerLayoutManager =
        new LinearLayoutManager(getActivity().getApplicationContext());
    RecyclerView mTravellersRecycleView =
        (RecyclerView) view.findViewById(R.id.sso_travellers_recyclerview);
    TravellersAdapter mTravellersAdapter =
        new TravellersAdapter(mPresenter.getTravellersFromDB(), this.getActivity(),
            new TravellersListeners() {
              @Override public void onClick(boolean isDefaultTraveller) {
                mTracker.trackAppseeEvent(NEW_PAX_TRAVELLERS_TAP_EVENT);
                mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_DATA_PAX,
                    TrackerConstants.ACTION_PAX_SUMMARY,
                    TrackerConstants.LABEL_ADD_PASSENGER_CLICKS);
                getPresenter().getNavigatorInterface()
                    .showTravellerDetailView(OneCMSKeys.SSO_TRAVELLERS_ADD, -1, isDefaultTraveller);
              }

              @Override
              public void onClickTraveller(long userTravellerId, boolean isDefaultTraveller) {
                mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_DATA_PAX,
                    TrackerConstants.ACTION_PAX_SUMMARY, TrackerConstants.LABEL_OPEN_PAX_CLICKS);
                getPresenter().getNavigatorInterface()
                    .showTravellerDetailView(OneCMSKeys.SSO_TRAVELLERS_EDIT_TITLE, userTravellerId,
                        isDefaultTraveller);
              }
            });
    mTravellersRecycleView.setHasFixedSize(true);
    mTravellersRecycleView.setLayoutManager(mTravellerLayoutManager);
    mTravellersRecycleView.setAdapter(mTravellersAdapter);
  }

  @Override protected void setListeners() {
    // Nothing
  }

  @Override protected void initOneCMSText(View view) {
    // Nothing
  }
}
