package com.odigeo.app.android.lib;

import android.location.Location;
import com.globant.roboneck.common.NeckCookie;
import com.google.gson.annotations.Expose;
import com.odigeo.app.android.lib.models.OldMyShoppingCart;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;
import com.odigeo.app.android.lib.models.dto.ItinerarySortCriteriaDTO;
import com.odigeo.app.android.lib.models.dto.MembershipPerksDTO;
import com.odigeo.app.android.lib.models.dto.ShoppingCartCollectionOptionDTO;
import com.odigeo.app.android.lib.models.dto.responses.AvailableProductsResponse;
import com.odigeo.app.android.lib.models.dto.responses.FlightSearchResponse;
import com.odigeo.data.entity.geo.City;
import java.io.Serializable;
import java.util.List;

/**
 * Created by manuel on 28/11/14.
 */
@Deprecated public class OdigeoSession implements Serializable {

  //When the app starts, retrieve this information
  private List<City> citiesGeolocated;
  private String messageRestServiceResponse;
  private Location currentLocation;

  //Data get in the Flight Search Service response
  private List<NeckCookie> sessionBookingCookies;
  private String insuranceOptOut;
  private ItinerarySortCriteriaDTO sortCriteria;
  private boolean dataHasBeenLoaded;

  @Expose private FlightSearchResponse searchResponse;
  @Expose private List<FareItineraryDTO> itineraryResults;
  @Expose private List<FareItineraryDTO> itineraryResultsFiltered;

  //Data obtained when select a search result, in the create shopping cart service response
  @Expose private OldMyShoppingCart oldMyShoppingCart;

  //After create shopping cart, and retrieve the products
  @Expose private AvailableProductsResponse availableProductsResponse;
  private boolean skipInsurance;

  //Data obtained in the addPassengerInfo service response
  @Expose private List<ShoppingCartCollectionOptionDTO> paymentMethods;

  private MembershipPerksDTO membershipPerksDTO;

  public final Location getCurrentLocation() {
    return currentLocation;
  }

  public final List<NeckCookie> getSessionBookingCookies() {
    return sessionBookingCookies;
  }

  public final void setSessionBookingCookies(List<NeckCookie> sessionBookingCookies) {
    this.sessionBookingCookies = sessionBookingCookies;
  }

  public final String getInsuranceOptOut() {
    return insuranceOptOut;
  }

  public final void setInsuranceOptOut(String insuranceOptOut) {
    this.insuranceOptOut = insuranceOptOut;
  }

  public final ItinerarySortCriteriaDTO getSortCriteria() {
    return sortCriteria;
  }

  public final void setSortCriteria(ItinerarySortCriteriaDTO sortCriteria) {
    this.sortCriteria = sortCriteria;
  }

  public final List<FareItineraryDTO> getItineraryResults() {
    return itineraryResults;
  }

  public final void setItineraryResults(List<FareItineraryDTO> itineraryResults) {
    this.itineraryResults = itineraryResults;
  }

  public final List<FareItineraryDTO> getItineraryResultsFiltered() {
    return itineraryResultsFiltered;
  }

  public final void setItineraryResultsFiltered(List<FareItineraryDTO> itineraryResultsFiltered) {
    this.itineraryResultsFiltered = itineraryResultsFiltered;
  }

  public final OldMyShoppingCart getOldMyShoppingCart() {
    return oldMyShoppingCart;
  }

  public final void setOldMyShoppingCart(OldMyShoppingCart oldMyShoppingCart) {
    this.oldMyShoppingCart = oldMyShoppingCart;
  }

  public final AvailableProductsResponse getAvailableProductsResponse() {
    return availableProductsResponse;
  }

  public final void setAvailableProductsResponse(
      AvailableProductsResponse availableProductsResponse) {
    this.availableProductsResponse = availableProductsResponse;
  }

  public final boolean isSkipInsurance() {
    return skipInsurance;
  }

  public final void setSkipInsurance(boolean skipInsurance) {
    this.skipInsurance = skipInsurance;
  }

  public final List<ShoppingCartCollectionOptionDTO> getPaymentMethods() {
    return paymentMethods;
  }

  public final void setPaymentMethods(List<ShoppingCartCollectionOptionDTO> paymentMethods) {
    this.paymentMethods = paymentMethods;
  }

  public final FlightSearchResponse getSearchResponse() {
    return searchResponse;
  }

  public final void setSearchResponse(FlightSearchResponse searchResponse) {
    this.searchResponse = searchResponse;
  }

  public final boolean isDataHasBeenLoaded() {
    return dataHasBeenLoaded;
  }

  public final void setDataHasBeenLoaded(boolean dataHasBeenLoaded) {
    this.dataHasBeenLoaded = dataHasBeenLoaded;
  }

  public MembershipPerksDTO getMembershipPerksDTO() {
    return membershipPerksDTO;
  }

  public void setMembershipPerksDTO(MembershipPerksDTO membershipPerksDTO) {
    this.membershipPerksDTO = membershipPerksDTO;
  }
}
