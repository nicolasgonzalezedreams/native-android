package com.odigeo.app.android.lib.models.dto;

public enum MobileSectionResultType {
  SCALE, ARRIVAL, HUB;
}
