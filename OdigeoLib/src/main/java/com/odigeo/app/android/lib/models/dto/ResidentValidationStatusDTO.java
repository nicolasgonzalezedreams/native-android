package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public enum ResidentValidationStatusDTO implements Serializable {

  OK, KO, PENDING, DATA_ERROR, AVAILABILITY, NOT_RESPONSE;

  public static ResidentValidationStatusDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
