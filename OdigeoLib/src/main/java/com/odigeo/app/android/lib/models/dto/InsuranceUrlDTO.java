package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

/**
 * @author miguel
 */
public class InsuranceUrlDTO implements Serializable {

  protected InsuranceConditionsUrlTypeDTO type;
  protected String url;

  /**
   * Gets the value of the type property.
   *
   * @return possible object is {@link InsuranceConditionsUrlType }
   */
  public InsuranceConditionsUrlTypeDTO getType() {
    return type;
  }

  /**
   * Sets the value of the type property.
   *
   * @param value allowed object is {@link InsuranceConditionsUrlType }
   */
  public void setType(InsuranceConditionsUrlTypeDTO value) {
    this.type = value;
  }

  /**
   * Gets the value of the url property.
   *
   * @return possible object is {@link String }
   */
  public String getUrl() {
    return url;
  }

  /**
   * Sets the value of the url property.
   *
   * @param value allowed object is {@link String }
   */
  public void setUrl(String value) {
    this.url = value;
  }
}
