package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class MonitoringAlertBooking implements Serializable {

  private BigDecimal totalPrice;
  private BigDecimal providerPrice;
  private BookingStatusDTO bookingStatus;
  private MonitoringAlert monitoringAlert;
  private Phone phone;

  public final BigDecimal getProviderPrice() {
    return providerPrice;
  }

  public final void setProviderPrice(BigDecimal providerPrice) {
    this.providerPrice = providerPrice;
  }

  public final BookingStatusDTO getBookingStatus() {
    return bookingStatus;
  }

  public final void setBookingStatus(BookingStatusDTO bookingStatus) {
    this.bookingStatus = bookingStatus;
  }

  public final MonitoringAlert getMonitoringAlert() {
    return monitoringAlert;
  }

  public final void setMonitoringAlert(MonitoringAlert monitoringAlert) {
    this.monitoringAlert = monitoringAlert;
  }

  public final Phone getPhone() {
    return phone;
  }

  public final void setPhone(Phone phone) {
    this.phone = phone;
  }

  public final BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public final void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }
}
