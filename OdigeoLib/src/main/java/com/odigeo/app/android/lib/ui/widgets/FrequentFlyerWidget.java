package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.CarrierSpinnerAdapter;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.FrequentFlyerCardCodeDTO;
import com.odigeo.app.android.lib.ui.widgets.base.OdigeoSpinner;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Irving on 16/11/2014.
 */
@Deprecated public class FrequentFlyerWidget extends LinearLayout implements View.OnClickListener {

  private FrequentFlyerListener listener;

  private OdigeoSpinner carrierSpinner;
  private EditTextForm flyerCodeEdit;
  private FormOdigeoLayoutWithIcon odigeoLayout;

  private FrequentFlyerCardCodeDTO flyerCardCodeDTO;
  private List<CarrierDTO> carriersList;

  public FrequentFlyerWidget(Context context) {

    super(context);
    inflateLayout();
    init();
  }

  public FrequentFlyerWidget(Context context, AttributeSet attrs) {

    super(context, attrs);
    inflateLayout();
    init();
  }

  private void inflateLayout() {

    inflate(getContext(), R.layout.layout_frequent_flyer_widget, this);
    odigeoLayout = (FormOdigeoLayoutWithIcon) findViewById(R.id.layout_frequent_flyer_widget_form);
    carrierSpinner = (OdigeoSpinner) findViewById(R.id.spinner_frequent_flyer_carrier);
    flyerCodeEdit = (EditTextForm) findViewById(R.id.widget__frequent_flyer_code);
  }

  private void init() {

    flyerCardCodeDTO = new FrequentFlyerCardCodeDTO();
    carriersList = new ArrayList<>();
    odigeoLayout.setOnClickIconListener(this);
  }

  public final void setCarrierList(List<CarrierDTO> carrierList) {

    this.carriersList = carrierList;
    carrierSpinner.setAdapter(new CarrierSpinnerAdapter(getContext(), this.carriersList));
  }

  public final void setFrequentFlyerListener(FrequentFlyerListener listener) {
    this.listener = listener;
  }

  public final void hideDelete() {
    odigeoLayout.hideIcon();
  }

  public final FrequentFlyerCardCodeDTO getFlyerCardCodeDTO() {
    return this.flyerCardCodeDTO;
  }

  public final void setFlyerCardCodeDTO(FrequentFlyerCardCodeDTO flyerCardCodeDTO) {

    this.flyerCardCodeDTO = flyerCardCodeDTO;
    if (flyerCardCodeDTO != null) {
      carrierSpinner.setSelection(getCarrierByCode(flyerCardCodeDTO.getCarrierCode()));
      flyerCodeEdit.setText(flyerCardCodeDTO.getPassengerCardNumber());
    }
  }

  public final boolean isValid() {
    if (carrierSpinner.getSelectedItem() != null && flyerCodeEdit.isValid()) {
      flyerCardCodeDTO.setCarrierCode(((CarrierDTO) carrierSpinner.getSelectedItem()).getCode());
      flyerCardCodeDTO.setPassengerCardNumber(flyerCodeEdit.getText());
      return true;
    }
    return false;
  }

  private CarrierDTO getCarrierByCode(String carrierCode) {

    CarrierDTO carrierDTO = new CarrierDTO();
    carrierDTO.setCode(carrierCode);
    int index = carriersList.indexOf(carrierDTO);
    if (index >= 0) {
      carrierDTO = carriersList.get(index);
    }
    return carrierDTO;
  }

  @Override public final void onClick(View v) {
    if (listener != null) {
      listener.onClicKDeleteFlyerWidget(this);
    }
  }

  public interface FrequentFlyerListener {
    void onClicKDeleteFlyerWidget(FrequentFlyerWidget flyerWidget);
  }
}
