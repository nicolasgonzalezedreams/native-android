package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author miguel
 */
public class HotelSearchResultsDTO implements Serializable {

  protected List<AccommodationDealDTO> accommodationResults;
  protected long searchId;

  /**
   * @return the searchId
   */
  public long getSearchId() {
    return searchId;
  }

  /**
   * @param searchId the searchId to set
   */
  public void setSearchId(long searchId) {
    this.searchId = searchId;
  }

  /**
   * @return the accommodationResults
   */
  public List<AccommodationDealDTO> getAccommodationResults() {
    return accommodationResults;
  }

  /**
   * @param accommodationResults the accommodationResults to set
   */
  public void setAccommodationResults(List<AccommodationDealDTO> accommodationResults) {
    this.accommodationResults = accommodationResults;
  }
}
