package com.odigeo.app.android.navigator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoCountriesActivity;
import com.odigeo.app.android.lib.activities.OdigeoNoConnectionActivity;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.model.BookingInfoViewModel;
import com.odigeo.app.android.view.PassengerView;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.wrappers.CarrierWrapper;
import com.odigeo.app.android.view.wrappers.UserFrequentFlyersWrapper;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.shoppingCart.AvailableProductsResponse;
import com.odigeo.data.entity.shoppingCart.Carrier;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.FlowConfigurationResponse;
import com.odigeo.data.entity.shoppingCart.PassengerConflictDetails;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.contracts.navigators.PassengerNavigatorInterface;
import java.util.ArrayList;
import java.util.List;

public class PassengerNavigator extends BaseNavigator implements PassengerNavigatorInterface {

  private static final int INSURANCE_LAUNCH_CODE = 2;
  private static final int PAYMENT_LAUNCH_CODE = 3;

  private PassengerView mPassengerView;
  private int mWidgetPositionCallback;

  private CreateShoppingCartResponse mCreateShoppingCartResponse;
  private AvailableProductsResponse mAvailableProductsResponse;
  private FlowConfigurationResponse mFlowConfigurationResponse;
  private boolean mIsFullTransparency;
  private double mLastTicketsPrice;
  private SearchOptions mSearchOptions;
  private BookingInfoViewModel bookingInfo;

  public Fragment getInstancePassengerView() {
    if (mPassengerView == null) {
      mPassengerView =
          PassengerView.newInstance(mCreateShoppingCartResponse, mAvailableProductsResponse,
              mIsFullTransparency, mLastTicketsPrice, mSearchOptions, bookingInfo);
      mPassengerView.setArguments(getIntent().getExtras());
    }
    return mPassengerView;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_passenger);

    mCreateShoppingCartResponse = (CreateShoppingCartResponse) getIntent().getSerializableExtra(
        Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE);
    mAvailableProductsResponse = (AvailableProductsResponse) getIntent().getSerializableExtra(
        Constants.EXTRA_AVAILABLE_PRODUCTS_RESPONSE);
    mFlowConfigurationResponse = (FlowConfigurationResponse) getIntent().getSerializableExtra(
        Constants.EXTRA_FLOW_CONFIGURATION_RESPONSE);
    mIsFullTransparency = getIntent().getBooleanExtra(Constants.EXTRA_FULL_TRANSPARENCY, false);
    mLastTicketsPrice = getIntent().getDoubleExtra(Constants.EXTRA_REPRICING_TICKETS_PRICES, 0);
    mSearchOptions =
        (SearchOptions) getIntent().getSerializableExtra(Constants.EXTRA_SEARCH_OPTIONS);
    bookingInfo = (BookingInfoViewModel) getIntent().getSerializableExtra(
        Constants.EXTRA_BOOKING_INFO);

    replaceFragment(R.id.fl_container, getInstancePassengerView());
    setNavigationTitle(
        LocalizablesFacade.getString(this, OneCMSKeys.SSO_PASSENGERS_TITLE).toString());
  }

  @Override public final void onActivityResult(final int requestCode, final int resultCode,
      final Intent data) {
    if (resultCode == Activity.RESULT_OK) {
      if (requestCode == Constants.REQUEST_CODE_FREQUENT_FLYER_CODES) {
        if (data != null) {
          UserFrequentFlyersWrapper listWrapper =
              ((UserFrequentFlyersWrapper) data.getSerializableExtra(
                  Constants.RESPONSE_LIST_FREQUENT_FLYER));
          mPassengerView.onSetPassengerFrequentFlyer(listWrapper.getFlyerCardCodes(),
              mWidgetPositionCallback);
        }
      } else if (requestCode == Constants.REQUEST_CODE_DO_LOGIN) {
        mPassengerView.onActivityResult(requestCode, resultCode, data);
      } else if (requestCode == INSURANCE_LAUNCH_CODE || requestCode == PAYMENT_LAUNCH_CODE) {
        mCreateShoppingCartResponse = (CreateShoppingCartResponse) data.getSerializableExtra(
            Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE);
        mPassengerView.updateCreateShoppingCartResponse(mCreateShoppingCartResponse);
      } else if (data != null) {
        Country countrySelected = (Country) data.getSerializableExtra(Constants.EXTRA_COUNTRY);
        if (countrySelected != null) {
          switch (requestCode) {
            case Constants.SELECTING_TRAVELLER_NATIONALITY:
              mPassengerView.onSetNationalityCountry(countrySelected, mWidgetPositionCallback);
              break;
            case Constants.SELECTING_TRAVELLER_COUNTRY_OF_RESIDENCE:
              mPassengerView.onSetResidentCountry(countrySelected, mWidgetPositionCallback);
              break;
            case Constants.SELECTING_TRAVELLER_IDENTIFICATION_COUNTRY:
              mPassengerView.onSetIdentificationCountry(countrySelected, mWidgetPositionCallback);
              break;
            case Constants.SELECTING_COUNTRY_PHONE_PREFIX:
              mPassengerView.onSetContactPhonePrefix(countrySelected);
              break;
          }
        }
      }
    }
  }

  @Override public void onBackPressed() {
    Intent resultIntent = new Intent();
    resultIntent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE,
        mCreateShoppingCartResponse);
    resultIntent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, mSearchOptions);
    setResult(Activity.RESULT_OK, resultIntent);
    super.onBackPressed();
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_goto_summary, menu);
    MenuItem item = menu.findItem(R.id.menu_item_goto_summary);
    item.setTitle(LocalizablesFacade.getString(this, OneCMSKeys.COMMON_TRIP_DETAILS).toString());
    return super.onCreateOptionsMenu(menu);
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {
    int i = item.getItemId();
    if (i == R.id.menu_item_goto_summary) {
      tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAX,
          TrackerConstants.ACTION_FLIGHT_SUMMARY, TrackerConstants.LABEL_OPEN_FLIGHT_SUMMARY);
      navigateToSummary();
      return true;
    } else if (i == android.R.id.home) {
      tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAX,
          TrackerConstants.ACTION_NAVIGATION_ELEMENTS, TrackerConstants.LABEL_GO_BACK);
      onBackPressed();
      return true;
    } else {
      return super.onOptionsItemSelected(item);
    }
  }

  private void navigateToSummary() {
    OdigeoApp odigeoApp = (OdigeoApp) getApplication();
    Intent intent = new Intent(this, odigeoApp.getSummaryActivityClass());
    intent.putExtra(Constants.EXTRA_STEP_TO_SUMMARY, Step.PASSENGER);
    intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, mSearchOptions);
    intent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE, mCreateShoppingCartResponse);
    intent.putExtra(Constants.EXTRA_FULL_TRANSPARENCY, mIsFullTransparency);
    intent.putExtra(Constants.EXTRA_BOOKING_INFO, bookingInfo);
    startActivity(intent);
  }

  @Override public void navigateToCountryResident(int widgetPosition) {
    mWidgetPositionCallback = widgetPosition;
    Intent intent = new Intent(getApplicationContext(), OdigeoCountriesActivity.class);
    intent.putExtra(Constants.EXTRA_COUNTRY_MODE, OdigeoCountriesActivity.SELECTION_MODE_RESIDENCE);
    startActivityForResult(intent, Constants.SELECTING_TRAVELLER_COUNTRY_OF_RESIDENCE);
  }

  @Override public void navigateToCountryNationality(int widgetPosition) {
    mWidgetPositionCallback = widgetPosition;
    Intent intent = new Intent(getApplicationContext(), OdigeoCountriesActivity.class);
    intent.putExtra(Constants.EXTRA_COUNTRY_MODE, OdigeoCountriesActivity.SELECTION_MODE_COUNTRY);
    startActivityForResult(intent, Constants.SELECTING_TRAVELLER_NATIONALITY);
  }

  @Override public void navigateToCountryIdentification(int widgetPosition) {
    mWidgetPositionCallback = widgetPosition;
    Intent intent = new Intent(getApplicationContext(), OdigeoCountriesActivity.class);
    intent.putExtra(Constants.EXTRA_COUNTRY_MODE,
        OdigeoCountriesActivity.SELECTION_MODE_IDENTIFICATION);
    startActivityForResult(intent, Constants.SELECTING_TRAVELLER_IDENTIFICATION_COUNTRY);
  }

  @Override public void navigateToPhoneCode() {
    Intent intent = new Intent(getApplicationContext(), OdigeoCountriesActivity.class);
    intent.putExtra(Constants.EXTRA_COUNTRY_MODE,
        OdigeoCountriesActivity.SELECTION_MODE_PHONE_PREFIX);
    startActivityForResult(intent, Constants.SELECTING_COUNTRY_PHONE_PREFIX);
  }

  @Override public void navigateToFrequentFlyers(List<UserFrequentFlyer> frequentFlyers,
      List<Carrier> carriers, int passengerWidgetPosition, List<Carrier> filteredCarriers) {
    mWidgetPositionCallback = passengerWidgetPosition;
    Intent intent = new Intent(this, FrequentFlyerCodesNavigator.class);
    UserFrequentFlyersWrapper codesWrapper = new UserFrequentFlyersWrapper(frequentFlyers);
    intent.putExtra(Constants.EXTRA_FREQUENT_FLYER_LIST, codesWrapper);
    CarrierWrapper carriersWrapper = new CarrierWrapper(filteredCarriers);
    intent.putExtra(Constants.EXTRA_FREQUENT_FLYER_CARRIERS, carriersWrapper);
    startActivityForResult(intent, Constants.REQUEST_CODE_FREQUENT_FLYER_CODES);
  }

  @Override
  public void navigateToDuplicateBooking(PassengerConflictDetails passengerConflictDetails,
      List<String> clientBookingPnrs) {
    Intent intent =
        new Intent(this, ((OdigeoApp) getApplication()).getBookingDuplicatedActivityClass());
    intent.putExtra(Constants.EXTRA_BOOKING_STATUS, passengerConflictDetails.getBookingStatus());
    intent.putStringArrayListExtra(Constants.EXTRA_BOOKING_PNRS,
        (ArrayList<String>) clientBookingPnrs);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  @Override public void navigateToInsurance(CreateShoppingCartResponse createShoppingCartResponse,
      int numBaggages) {
    mCreateShoppingCartResponse.setPricingBreakdown(
        createShoppingCartResponse.getPricingBreakdown());
    mCreateShoppingCartResponse.getShoppingCart()
        .setTotalPrice(createShoppingCartResponse.getShoppingCart().getTotalPrice());
    mCreateShoppingCartResponse.getShoppingCart()
        .setBuyer(createShoppingCartResponse.getShoppingCart().getBuyer());
    mCreateShoppingCartResponse.getShoppingCart()
        .setTravellers(createShoppingCartResponse.getShoppingCart().getTravellers());
    mCreateShoppingCartResponse.getShoppingCart()
        .setBaggageTotalFee(createShoppingCartResponse.getShoppingCart().getBaggageTotalFee());
    mCreateShoppingCartResponse.getShoppingCart()
        .setCollectionOptions(createShoppingCartResponse.getShoppingCart().getCollectionOptions());
    mCreateShoppingCartResponse.setSortCriteria(createShoppingCartResponse.getSortCriteria());

    mSearchOptions.setNumBaggages(numBaggages);

    Intent intent = new Intent(this, InsurancesNavigator.class);
    intent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE, mCreateShoppingCartResponse);
    intent.putExtra(Constants.EXTRA_AVAILABLE_PRODUCTS_RESPONSE, mAvailableProductsResponse);
    intent.putExtra(Constants.EXTRA_FLOW_CONFIGURATION_RESPONSE, mFlowConfigurationResponse);
    intent.putExtra(Constants.EXTRA_FULL_TRANSPARENCY, mIsFullTransparency);
    intent.putExtra(Constants.EXTRA_REPRICING_TICKETS_PRICES, mLastTicketsPrice);
    intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, mSearchOptions);
    intent.putExtra(Constants.EXTRA_BOOKING_INFO, bookingInfo);
    startActivityForResult(intent, INSURANCE_LAUNCH_CODE);
  }

  @Override public void navigateToPayment(CreateShoppingCartResponse createShoppingCartResponse,
      int numBaggages) {
    mCreateShoppingCartResponse.setPricingBreakdown(
        createShoppingCartResponse.getPricingBreakdown());
    mCreateShoppingCartResponse.getShoppingCart()
        .setTotalPrice(createShoppingCartResponse.getShoppingCart().getTotalPrice());
    mCreateShoppingCartResponse.getShoppingCart()
        .setBuyer(createShoppingCartResponse.getShoppingCart().getBuyer());
    mCreateShoppingCartResponse.getShoppingCart()
        .setTravellers(createShoppingCartResponse.getShoppingCart().getTravellers());
    mCreateShoppingCartResponse.getShoppingCart()
        .setBaggageTotalFee(createShoppingCartResponse.getShoppingCart().getBaggageTotalFee());
    mCreateShoppingCartResponse.getShoppingCart()
        .setCollectionOptions(createShoppingCartResponse.getShoppingCart().getCollectionOptions());

    mSearchOptions.setNumBaggages(numBaggages);

    Intent intent = new Intent(this, PaymentNavigator.class);
    intent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE, mCreateShoppingCartResponse);
    intent.putExtra(Constants.EXTRA_FULL_TRANSPARENCY, mIsFullTransparency);
    intent.putExtra(Constants.EXTRA_REPRICING_TICKETS_PRICES, mLastTicketsPrice);
    intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, mSearchOptions);
    intent.putExtra(Constants.EXTRA_BOOKING_INFO, bookingInfo);
    startActivityForResult(intent, PAYMENT_LAUNCH_CODE);
  }

  @Override public void navigateToHome() {
    Intent intent = new Intent(this, NavigationDrawerNavigator.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    startActivity(intent);
  }

  @Override public void navigateToNoConnectionActivity() {
    OdigeoNoConnectionActivity.launch(this, Constants.REQUEST_CODE_ADDPASSENGERLISTENER);
  }

  @Override public void navigateToLogin() {
    Intent intent = new Intent(getApplicationContext(), LoginNavigator.class);
    startActivityForResult(intent, Constants.REQUEST_CODE_DO_LOGIN);
  }

  public void navigateToSearch() {
    OdigeoApp odigeoApp = (OdigeoApp) getApplication();
    Intent intent = new Intent(this, odigeoApp.getSearchFlightsActivityClass());
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }
}