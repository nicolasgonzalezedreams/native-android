package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.List;

@Deprecated public class ItineraryDTO implements Serializable {

  protected ItinerariesLegendDTO legend;
  protected List<FareComponentDTO> fareComponents;
  protected List<Integer> segments;

  protected Integer itinerary;

  /**
   * Gets the value of the legend property.
   *
   * @return possible object is
   * {@link ItinerariesLegend }
   */
  public ItinerariesLegendDTO getLegend() {
    return legend;
  }

  /**
   * Sets the value of the legend property.
   *
   * @param value allowed object is
   * {@link ItinerariesLegend }
   */
  public void setLegend(ItinerariesLegendDTO value) {
    this.legend = value;
  }

  /**
   * Gets the value of the itinerary property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getItinerary() {
    return itinerary;
  }

  /**
   * Sets the value of the itinerary property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setItinerary(Integer value) {
    this.itinerary = value;
  }

  public List<Integer> getSegments() {
    return segments;
  }

  public void setSegments(List<Integer> segments) {
    this.segments = segments;
  }

  public List<FareComponentDTO> getFareComponents() {
    return fareComponents;
  }

  public void setFareComponents(List<FareComponentDTO> fareComponents) {
    this.fareComponents = fareComponents;
  }
}
