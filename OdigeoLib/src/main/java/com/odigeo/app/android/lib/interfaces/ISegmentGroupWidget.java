package com.odigeo.app.android.lib.interfaces;

import com.odigeo.app.android.lib.ui.widgets.SegmentGroupWidget;

/**
 * Created by ManuelOrtiz on 15/09/2014.
 */
public interface ISegmentGroupWidget {
  void onSegmentSelected(SegmentGroupWidget segmentGroupWidget);

  void onItinerarySelect(int itineraryIndex);

  void onSegmentGroupUnselected(SegmentGroupWidget segmentGroupWidget);
}
