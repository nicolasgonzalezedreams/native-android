package com.odigeo.app.android.lib.models.dto;

import java.util.List;

public class CollectionEstimationFeesDTO {

  protected List<CollectionMethodKeyPriceDTO> collectionMethodFees;
  protected int id;

  public List<CollectionMethodKeyPriceDTO> getCollectionMethodFees() {
    return collectionMethodFees;
  }

  public void setCollectionMethodFees(List<CollectionMethodKeyPriceDTO> collectionMethodFees) {
    this.collectionMethodFees = collectionMethodFees;
  }

  /**
   * Gets the value of the id property.
   */
  public int getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   */
  public void setId(int value) {
    this.id = value;
  }
}
