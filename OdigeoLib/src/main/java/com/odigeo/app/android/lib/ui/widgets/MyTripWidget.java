package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.Section;
import com.odigeo.app.android.lib.models.dto.BookingSummaryStatus;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.data.entity.TravelType;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MyTripWidget extends LinearLayout implements View.OnClickListener {
  private static final int MAX_LENGTH_TITLE = 45;
  private CustomTextView txtDestination;

  private MyTripScheduleWidget departure;
  private MyTripScheduleWidget arrival;

  private ImageView imageView;
  private ImageView btnClose;
  private ImageView iconStatus;

  private TextView textStatus;

  private View separatorVertical;

  private SearchOptions searchOptions;
  private MyTripWidgetListener listener;

  private boolean pastTrip;
  private boolean imageSet;

  private long bookingId;

  private OdigeoImageLoader<ImageView> mImageLoader;

  public MyTripWidget(Context context, AttributeSet attrs) {
    super(context, attrs);

    inflateLayout(context);
  }

  public MyTripWidget(Context context, SearchOptions options, long bookingId) {
    super(context);

    this.bookingId = bookingId;
    searchOptions = options;

    inflateLayout(context);

    setValues();
  }

  public MyTripWidget(Context context, AttributeSet attrs, SearchOptions options) {
    super(context, attrs);

    searchOptions = options;

    inflateLayout(context);

    setValues();
  }

  private void inflateLayout(Context context) {
    inflate(context, R.layout.layout_mytrip_widget, this);

    this.setClickable(true);

    txtDestination = (CustomTextView) findViewById(R.id.txt_location);

    imageView = (ImageView) findViewById(R.id.imgMyTripBackground);
    btnClose = (ImageView) findViewById(R.id.imgMyTripWidgetClose);
    iconStatus = (ImageView) findViewById(R.id.mytrip_widget_booking_status_icon);
    textStatus = (TextView) findViewById(R.id.mytrip_widget_booking_status_text);
    departure = (MyTripScheduleWidget) findViewById(R.id.wdgtDeparture);
    arrival = (MyTripScheduleWidget) findViewById(R.id.wdgtArrival);
    separatorVertical = findViewById(R.id.separatorVertical);

    if (isInEditMode()) {
      setDestination("Paris");
    }

    btnClose.setOnClickListener(this);
    mImageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();
  }

  public void setListener(MyTripWidgetListener listener) {
    this.listener = listener;
  }

  public long getBookingId() {
    return bookingId;
  }

  public void setBookingId(long bookingId) {
    this.bookingId = bookingId;
  }

  public void setSearchOptions(SearchOptions searchOptions) {
    this.searchOptions = searchOptions;
    setValues();
  }

  public SearchOptions getSearchOptions() {
    return this.searchOptions;
  }

  private void setValues() {

    List<FlightSegment> segmentList = searchOptions.getFlightSegments();

    FlightSegment firstSegment = searchOptions.getFlightSegments().get(0);
    FlightSegment lastSegment = segmentList.get(segmentList.size() - 1);

    if (searchOptions.getTravelType() == TravelType.MULTIDESTINATION) {
      StringBuilder builder = new StringBuilder();
      for (int i = 0; i < segmentList.size(); i++) {
        FlightSegment segment = segmentList.get(i);
        builder.append(segment.getArrivalCity().getCityName());
        if (i != segmentList.size() - 1) {
          builder.append(", ");
        }
      }
      String stringDestination = builder.toString();
      //Checks if the stringDestination length is too big.
      if (stringDestination.length() > MAX_LENGTH_TITLE) {
        int ellipsisUnicode = 0x2026;
        String ellipsis = Character.toString((char) ellipsisUnicode);
        stringDestination =
            stringDestination.substring(stringDestination.length() - MAX_LENGTH_TITLE,
                stringDestination.length());
        stringDestination = ellipsis + stringDestination;
      }
      txtDestination.setText(HtmlUtils.formatHtml(stringDestination));
    } else {
      txtDestination.setText(HtmlUtils.formatHtml(firstSegment.getArrivalCity().getCityName()));
    }

    String keyDeparture = firstSegment.getDepartureCity().getCityName()
        + firstSegment.getArrivalCity().getCityName()
        + searchOptions.getId();
    String keyReturn = lastSegment.getDepartureCity().getCityName()
        + lastSegment.getArrivalCity().getCityName()
        + searchOptions.getId();
    long dateDepartureSaved = PreferencesManager.readSaveTimes(this.getContext(), keyDeparture);
    long dateReturnSaved = PreferencesManager.readSaveTimes(this.getContext(), keyReturn);

    if (dateDepartureSaved == 0 && dateReturnSaved == 0) {
      PreferencesManager.saveTimes(getContext(), keyDeparture, firstSegment.getDate());
      PreferencesManager.saveTimes(getContext(), keyReturn, lastSegment.getDate());
    }

    //Set departure information
    departure.setHour(getTimePartFormatted(
        dateDepartureSaved == 0 ? OdigeoDateUtils.createDate(firstSegment.getDate())
            : OdigeoDateUtils.createDate(dateDepartureSaved)));
    //departure.setHour(hourDeparture);
    departure.setDate(getDatePartFormatted(
        dateDepartureSaved == 0 ? OdigeoDateUtils.createDate(firstSegment.getDate())
            : OdigeoDateUtils.createDate(dateDepartureSaved)));
    //departure.setDate(dateDeparture);

    //There are more than one segment
    if (!firstSegment.equals(lastSegment)) {
      //Set return information
      arrival.setHour(getTimePartFormatted(
          dateReturnSaved == 0 ? OdigeoDateUtils.createDate(lastSegment.getDate())
              : OdigeoDateUtils.createDate(dateReturnSaved)));
      //arrival.setHour(hourArrival);
      arrival.setDate(getDatePartFormatted(
          dateReturnSaved == 0 ? OdigeoDateUtils.createDate(lastSegment.getDate())
              : OdigeoDateUtils.createDate(dateReturnSaved)));
      //arrival.setDate(dateArrival);
    } else { //Simple trip
      separatorVertical.setVisibility(GONE);
      arrival.setVisibility(GONE);
    }

    //Set the titles
    if (searchOptions.getTravelType() == TravelType.MULTIDESTINATION) {
      departure.setTitle(
          LocalizablesFacade.getString(getContext(), "mytripsviewcontroller_leg1").toString());
      arrival.setTitle(
          LocalizablesFacade.getString(getContext(), "mytripsviewcontroller_legfinal").toString());
    } else {
      departure.setTitle(
          LocalizablesFacade.getString(getContext(), "mytripsviewcontroller_departure").toString());
      arrival.setTitle(
          LocalizablesFacade.getString(getContext(), "mytripsviewcontroller_return").toString());
    }
  }

  public void deleteTimeTrips() {
    List<FlightSegment> segmentList = searchOptions.getFlightSegments();
    for (int i = 0; i < segmentList.size(); i++) {
      if (i == 0 || i == segmentList.size() - 1) {
        deleteTravel(segmentList.get(i));
      }
    }
  }

  private void deleteTravel(FlightSegment flightSegment) {
    String key = flightSegment.getDepartureCity().getCityName() + flightSegment.getArrivalCity()
        .getCityName() + searchOptions.getId();
    PreferencesManager.saveTimes(getContext(), key, 0);
  }

  public String setOffSetHour(String hour, int offSet) {
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
    Calendar cal = Calendar.getInstance();
    Date date = new Date();
    try {
      date = sdf.parse(hour);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    cal.setTime(date);
    cal.add(Calendar.HOUR, offSet);
    String timeCl = cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE);
    SimpleDateFormat result = new SimpleDateFormat("HH:mm");
    SimpleDateFormat resultDate = new SimpleDateFormat("EEE d MMM");
    //result.setTimeZone(TimeZone.getTimeZone("UTC"));
    String algo = resultDate.format(cal.getTime());
    return result.format(cal.getTime());
  }

  private String getDatePartFormatted(Date date) {
    if (date == null) {
      return "";
    }

    return OdigeoDateUtils.getGmtDateFormat(getResources().getString(R.string.templates__datelong3))
        .format(date);
  }

  private String getTimePartFormatted(Date date) {
    if (date == null) {
      return "";
    }
    //TODO: use format from class LocaleUtils
    return OdigeoDateUtils.getGmtDateFormat(
        Configuration.getInstance().getCurrentMarket().getTimeFormat()).format(date);
  }

  private void setLegDetails(MyTripScheduleWidget myTripScheduleWidget, Date date, String title) {
    myTripScheduleWidget.setTitle(title);

    String textDate =
        OdigeoDateUtils.getGmtDateFormat(getResources().getString(R.string.templates__datelong3))
            .format(date);
    myTripScheduleWidget.setDate(textDate);
  }

  /**
   * When the widget is loaded in UI, this method is called for set a destination image
   */
  public void setImage() {
    /**
     * If the image was already inserted, it is not necessary to continue.
     */
    if (imageSet) {
      return;
    }
    imageSet = true;
    //Set the image default with round corners
    String urlImageFileName = "";
    if (searchOptions.getTravelType() == TravelType.MULTIDESTINATION) {
      String fileName =
          String.format(Configuration.getInstance().getImagesSources().getMultidestinationFormat(),
              searchOptions.getMultiDestinationIndex());

      urlImageFileName =
          String.format("%s%s%s", Configuration.getInstance().getImagesSources().getUrlMyTrips(),
              fileName, Configuration.getInstance().getImagesSources().getImageFileType());
    } else {
      FlightSegment firstSegment = searchOptions.getFlightSegments().get(0);
      Section lastSection = firstSegment.getSections().get(firstSegment.getSections().size() - 1);

      urlImageFileName =
          String.format("%s%s%s", Configuration.getInstance().getImagesSources().getUrlMyTrips(),
              lastSection.getLocationTo().getIataCode(),
              Configuration.getInstance().getImagesSources().getImageFileType());
    }

    /**
     * Set default image with rounded corners
     */
    Bitmap defaultBackground =
        BitmapFactory.decodeResource(getResources(), R.drawable.trips_loading_placeholder);

    if (pastTrip) {
      defaultBackground = ViewUtils.toGrayscale(defaultBackground);
    }
    ViewUtils.setRoundedTopCornersToBitMap(defaultBackground, imageView);

    if (pastTrip) {
      ColorMatrix matrix = new ColorMatrix();
      matrix.setSaturation(0);
      ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
      imageView.setColorFilter(filter);
    }

    mImageLoader.load(imageView, urlImageFileName, R.drawable.trip_default_placeholder_image);
  }

  @Override public void onClick(View v) {
    if (v.getId() == R.id.imgMyTripWidgetClose) {
      listener.onClickDelete(this);
    }
  }

  public void disableCloseButton() {
    btnClose.setVisibility(GONE);
    btnClose.setClickable(false);
  }

  /**
   * Set the correct icon according to the status of the booking.
   *
   * @param newStatus Current status of the booking.
   */
  public void updateStatus(BookingSummaryStatus newStatus) {
    if (newStatus == BookingSummaryStatus.OK) {
      iconStatus.setImageResource(R.drawable.iconcontract);
      textStatus.setText(LocalizablesFacade.getString(getContext(), "checkbookingstatus_okmytrip"));
    } else if (newStatus == BookingSummaryStatus.PENDING) {
      iconStatus.setImageResource(R.drawable.iconpending);
      textStatus.setText(
          LocalizablesFacade.getString(getContext(), "checkbookingstatus_pendingmytrip"));
    } else {
      iconStatus.setImageResource(R.drawable.iconreject);
      textStatus.setText(LocalizablesFacade.getString(getContext(), "checkbookingstatus_komytrip"));
    }
  }

  public String getDestination() {
    return txtDestination.getText().toString();
  }

  public final void setDestination(String destination) {
    this.txtDestination.setText(destination);
  }

  public String getDepartureDate() {
    return departure.getDate();
  }

  public void setDepartureDate(String date) {
    departure.setDate(date);
  }

  public String getArrivalDate() {
    return arrival.getDate();
  }

  public void setPastTrip(boolean pastTrip) {
    this.pastTrip = pastTrip;
  }

  public interface MyTripWidgetListener {

    void onClickDelete(MyTripWidget widget);
  }
}
