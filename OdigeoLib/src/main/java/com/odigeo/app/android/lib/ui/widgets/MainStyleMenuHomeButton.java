package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.HomeOptionsButtons;

/**
 * Created by Irving on 25/08/2014.
 */
public class MainStyleMenuHomeButton extends ButtonMenuHomeWidget {
  public TextView subTitleTextView;

  public MainStyleMenuHomeButton(Context context, HomeOptionsButtons optionsButton) {
    super(context, R.layout.layout_home_menu_button_style, optionsButton);
    subTitleTextView = (TextView) super.rootView.findViewById(R.id.subtitle_menuHomeWidget);
  }

  public MainStyleMenuHomeButton(Context context, AttributeSet attrs) {
    super(context, attrs, R.layout.layout_home_menu_button_style);
    subTitleTextView = (TextView) super.rootView.findViewById(R.id.subtitle_menuHomeWidget);
  }

  public final void setSubTitle(CharSequence subTitle) {
    subTitleTextView.setText(subTitle);
  }
}
