package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Toast;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.interfaces.OdigeoInterfaces.PassengerPickerFragmentListener;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Irving Lóp on 01/09/2014.
 */
public class PassengerDialogPickerFragment extends android.support.v4.app.DialogFragment {
  private final Configuration config = Configuration.getInstance();
  private PassengerPickerFragmentListener callback;
  private NumberPicker adultsPicker;
  private NumberPicker kidsPicker;
  private NumberPicker babiesPicker;
  private int numAdults;
  private int numBabies;
  private int numKids;
  private Logger logger;

  @Override public void onAttach(Activity activity) {
    super.onAttach(activity);
    // Verify that the host activity implements the callback interface
    try {
      // Instantiate the NoticeDialogListener so we can send events to the host
      callback = (PassengerPickerFragmentListener) activity;
    } catch (ClassCastException e) {
      Log.e(Constants.TAG_LOG, activity.toString() + " must implement NoticeDialogListener");
      logger.log(Level.SEVERE, e.getMessage(), e);
    }
  }

  @Override public Dialog onCreateDialog(Bundle savedInstamnceState) {
    View rootView =
        getActivity().getLayoutInflater().inflate(R.layout.passenger_dialog_picker, null);
    adultsPicker = (NumberPicker) rootView.findViewById(R.id.numberPickerAdults);
    kidsPicker = (NumberPicker) rootView.findViewById(R.id.numberPickerKids);
    babiesPicker = (NumberPicker) rootView.findViewById(R.id.numberPickerBabies);

    adultsPicker.setMaxValue(9);
    kidsPicker.setMaxValue(9);
    babiesPicker.setMaxValue(9);

    adultsPicker.setMinValue(1);
    kidsPicker.setMinValue(0);
    babiesPicker.setMinValue(0);

    adultsPicker.setWrapSelectorWheel(false);
    kidsPicker.setWrapSelectorWheel(false);
    babiesPicker.setWrapSelectorWheel(false);

    adultsPicker.setValue(numAdults);
    kidsPicker.setValue(numKids);
    babiesPicker.setValue(numBabies);

    final android.support.v4.app.DialogFragment dThis = this;

    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setView(rootView)
        .setPositiveButton(
            LocalizablesFacade.getString(getActivity(), "passengerpicker_okbtn_titlelabel_text"),
            null)
        .setNegativeButton(LocalizablesFacade.getString(getActivity(), "common_cancel"),
            new DialogInterface.OnClickListener() {
              @Override public void onClick(DialogInterface dialog, int which) {
                // Empty.
              }
            });

    final AlertDialog dialog = builder.create();
    //Change the default behavior of the positive button to not to hide the dialog automatically after this button is pressed
    dialog.setOnShowListener(new DialogInterface.OnShowListener() {

      @Override public void onShow(DialogInterface dialogInterface) {

        Button b = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        b.setOnClickListener(new View.OnClickListener() {

          @Override public void onClick(View view) {
            //If there are errors in the validation
            if (!validatePassengers(adultsPicker.getValue(), kidsPicker.getValue(),
                babiesPicker.getValue())) {

              Toast toastErrorMessage = Toast.makeText(dThis.getActivity(),
                  LocalizablesFacade.getString(getActivity(),
                      "passengerpicker_passengerselectionerror"), Toast.LENGTH_LONG);

              toastErrorMessage.show();

              Log.i("ODIGO APP", "Passengers was shown");
            } else {
              //Everything was OK
              Log.i("ODIGEO APP", "Passengers validation passed");

              callback.onDialogPositiveClick(adultsPicker.getValue(), kidsPicker.getValue(),
                  babiesPicker.getValue());

              //Dismiss once everything is OK.
              dialog.dismiss();
            }
          }
        });
      }
    });

    return dialog;
  }

  /**
   * Validate if there are the admitted number of passengers
   *
   * @param numAdults Number of adults selected
   * @param numKids Number of infants selected
   * @param numBabies Number of babies selected
   * @return True if there are enough passengers allowed
   */
  private boolean validatePassengers(int numAdults, int numKids, int numBabies) {
    boolean isValid = true;

    if (numAdults + numKids + numBabies > config.getGeneralConstants()
        .getMaxPassengers()) { // If there are more passengers than the admitted
      isValid = false;
    } else if (numKids
        > config.getGeneralConstants().getMaxKidsPerAdult()
        * numAdults) { //If there are more infants than the admitted
      isValid = false;
    } else if (numBabies
        > config.getGeneralConstants().getMaxInfantPerAdult()
        * numAdults) { //If there are more babies than the admitted
      isValid = false;
    }

    Log.i("ODIGEO APP", "Pasajeros Validos? = " + isValid);

    return isValid;
  }

  public int getAdults() {
    return adultsPicker.getValue();
  }

  public int getChildren() {
    return kidsPicker.getValue();
  }

  public int getBabies() {
    return babiesPicker.getValue();
  }

  public int getNumAdults() {
    return numAdults;
  }

  public void setNumAdults(int numAdults) {
    this.numAdults = numAdults;
  }

  public int getNumBabies() {
    return numBabies;
  }

  public void setNumBabies(int numBabies) {
    this.numBabies = numBabies;
  }

  public int getNumKids() {
    return numKids;
  }

  public void setNumKids(int numKids) {
    this.numKids = numKids;
  }
}
