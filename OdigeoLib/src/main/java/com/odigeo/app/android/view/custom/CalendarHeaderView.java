package com.odigeo.app.android.view.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.view.BaseCustomWidget;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DateHelper;
import com.odigeo.presenter.CalendarHeaderPresenter;
import com.odigeo.presenter.contracts.views.holder.CalendarHeader;
import java.util.Date;

public class CalendarHeaderView extends BaseCustomWidget<CalendarHeaderPresenter>
    implements CalendarHeader {

  private final String EDIT_MODE_LABEL_DEPARTURE = "departure";
  private final String EDIT_MODE_LABEL_RETURN = "return";
  private final String EDIT_MODE_LABEL_SELECT_DATE = "Select date";

  private TextView tvDepartureLabel;
  private TextView tvDepartureDates;
  private LinearLayout linearLayoutDepartureMarker;
  private TextView tvReturnLabel;
  private TextView tvReturnDates;
  private LinearLayout linearLayoutReturnMarker;

  private DateHelper dateHelper;
  private float alpha;

  public CalendarHeaderView(Context context) {
    super(context, null);
  }

  public CalendarHeaderView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected CalendarHeaderPresenter setPresenter() {
    return new CalendarHeaderPresenter(this);
  }

  @Override public void initComponent() {
    tvDepartureLabel = (TextView) findViewById(R.id.departure_label);
    tvDepartureDates = (TextView) findViewById(R.id.departure_date_selected);
    linearLayoutDepartureMarker = (LinearLayout) findViewById(R.id.departure_marker);
    tvReturnLabel = (TextView) findViewById(R.id.return_label);
    tvReturnDates = (TextView) findViewById(R.id.return_date_selected);
    linearLayoutReturnMarker = (LinearLayout) findViewById(R.id.return_marker);

    initAlpha();
    dateHelper = new DateHelper();

    if (isInEditMode()) {
      highlightDepartureLabel();
      shadeReturnLabel();
    }
  }

  private void initAlpha() {
    TypedValue outValue = new TypedValue();
    getResources().getValue(R.dimen.selected_dates_alpha, outValue, true);
    alpha = outValue.getFloat();
  }

  @Override public int getComponentLayout() {
    return R.layout.calendar_header;
  }

  @Override public void initOneCMSText() {
    if (isInEditMode()) {
      tvDepartureLabel.setText(EDIT_MODE_LABEL_DEPARTURE);
      tvDepartureDates.setText(EDIT_MODE_LABEL_SELECT_DATE);
      tvReturnLabel.setText(EDIT_MODE_LABEL_RETURN);
    } else {
      tvDepartureLabel.setText(
          localizables.getString(OneCMSKeys.MY_TRIPS_FLIGHT_CARD_TITLE_OUTBOUND));
      tvReturnLabel.setText(localizables.getString(OneCMSKeys.MY_TRIPS_FLIGHT_CARD_TITLE_INBOUND));
    }
  }

  @Override public void setDepartureDateLabel(Date departureDate) {
    String outbound =
        dateHelper.getGmtDateFormat(getResources().getString(R.string.templates__datelong1))
            .format(departureDate);
    tvDepartureDates.setText(outbound);
  }

  @Override public void setDepartureDate(Date departureDate) {
    presenter.setDepartureDate(departureDate);
  }

  @Override public void setSelectDateOnDeparture() {
    tvDepartureDates.setText(localizables.getString(OneCMSKeys.CALENDAR_SELECT_DATE));
  }

  @Override public void resetDepartureDateLabel() {
    tvDepartureDates.setText("");
  }

  public void resetDepartureDate() {
    presenter.resetDepartureDate();
  }

  @Override public void normalizeDepartureLabel() {
    tvDepartureDates.setAlpha(1);
    tvDepartureLabel.setAlpha(1);
    linearLayoutDepartureMarker.setVisibility(GONE);
  }

  @Override public void highlightDepartureLabel() {
    tvDepartureDates.setAlpha(1);
    tvDepartureLabel.setAlpha(1);
    linearLayoutDepartureMarker.setVisibility(VISIBLE);
  }

  @Override public void shadeDepartureLabel() {
    tvDepartureLabel.setAlpha(alpha);
    tvDepartureDates.setAlpha(alpha);
    linearLayoutDepartureMarker.setVisibility(GONE);
  }

  @Override public void setReturnDateLabel(Date returnDate) {
    String inbound =
        dateHelper.getGmtDateFormat(getResources().getString(R.string.templates__datelong1))
            .format(returnDate);
    tvReturnDates.setText(inbound);
  }

  @Override public void setSelectDateOnReturn() {
    tvReturnDates.setText(localizables.getString(OneCMSKeys.CALENDAR_SELECT_DATE));
  }

  @Override public void setReturnDate(Date returnDate) {
    presenter.setReturnDate(returnDate);
  }

  @Override public void resetReturnDateLabel() {
    tvReturnDates.setText("");
  }

  @Override public void normalizeReturnLabel() {
    tvReturnDates.setAlpha(1);
    tvReturnLabel.setAlpha(1);
    linearLayoutReturnMarker.setVisibility(GONE);
  }

  @Override public void highlightReturnLabel() {
    tvReturnDates.setAlpha(1);
    tvReturnLabel.setAlpha(1);
    linearLayoutReturnMarker.setVisibility(VISIBLE);
  }

  @Override public void shadeReturnLabel() {
    tvReturnDates.setAlpha(alpha);
    tvReturnLabel.setAlpha(alpha);
    linearLayoutReturnMarker.setVisibility(GONE);
  }

  public void resetReturnDate() {
    presenter.resetReturnDate();
  }

  public void openingView() {
    presenter.openingView();
  }
}
