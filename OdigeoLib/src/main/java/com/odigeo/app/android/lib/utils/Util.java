package com.odigeo.app.android.lib.utils;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import com.globant.roboneck.common.NeckCookie;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.DistanceUnits;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.fragments.MSLErrorUtilsDialogFragment;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.app.android.lib.models.dto.ItineraryProviderBookingDTO;
import com.odigeo.app.android.lib.models.dto.custom.Code;
import com.odigeo.app.android.lib.models.dto.custom.MslError;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.shoppingCart.PricingMode;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.shoppingCart.StepsConfiguration;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jsoup.helper.StringUtil;

/**
 * Created by manuel on 31/08/14.
 */
public final class Util {

  public static final String YEAR = "year";
  public static final String SECOND = "second";
  public static final String MINUTE = "minute";
  public static final String HOUR_OF_DAY = "hourOfDay";
  public static final String DAY_OF_MONTH = "dayOfMonth";
  public static final String MONTH = "month";
  public static final String DATE_OF_BIRTH = "dateOfBirth";
  public static final String ID_EXPIRATION_DATE = "identificationExpirationDate";
  private static final float KILOMETERS_DIVIDER = 1000f;
  private static final float MILES_DIVIDER = 1609f;
  private static final int BUFFER_SIZE = 256;
  private static final float DECIMAL_RESIZE = 0.53f;
  private static final int ERROR_CODE = 4005;
  private static final String CYBER_SOURCE_MID = "CYBERSOURCE_MID";
  private static final String NON_NUMERIC_CHARS = "[^\\d]";
  private static final char DOT = '.';
  private static final char COMMA = ',';
  private static final int NO_DECIMAL_SEPARATOR = -1;
  private static final String EMPTY = "";

  private Util() {

  }

  /**
   * Transform the values that contains representation of dates or calendars setted into the
   * preferenceString to long.
   *
   * @param preferenceString the json string of the object stored in the preference file.
   * @return a new json string with the values modified.
   */
  public static String updateTripAtPreferences(String preferenceString) {
    JsonElement jsonElement = new JsonParser().parse(preferenceString);
    JsonArray root = jsonElement.getAsJsonArray();
    for (int i = 0; i < root.size(); i++) {
      JsonObject elementObj = root.get(i).getAsJsonObject();

      //parse date to millis
      updateField(elementObj, "searchDate");
      JsonElement flightSegments = elementObj.getAsJsonObject().get("flightSegments");
      JsonArray flights = flightSegments.getAsJsonArray();
      for (int j = 0; j < flights.size(); j++) {
        JsonObject flightElement = flights.get(j).getAsJsonObject();
        updateField(flightElement, "arrivalDate");
        updateField(flightElement, "date");
        if (flightElement.has("sections")) {
          JsonArray sectionsArray = flightElement.getAsJsonArray("sections");
          for (int s = 0; s < sectionsArray.size(); s++) {
            JsonObject sectionObject = sectionsArray.get(s).getAsJsonObject();
            updateFieldCalendarToLong(sectionObject, "arrivalDate");
            updateFieldCalendarToLong(sectionObject, "departureDate");
          }
        }
      }
    }
    return jsonElement.toString();
  }

  /**
   * Transform the values that contains representation of dates or calendars setted into the
   * strShoppingCart to long.
   *
   * @param strShoppingCart the json string of the object stored in the preference file.
   * @return a new json string with the values modified.
   */
  public static String updateShoppingCart(String strShoppingCart) {
    JsonElement jsonElement = new JsonParser().parse(strShoppingCart);
    JsonObject jsonObject = jsonElement.getAsJsonObject().get("mSearchOptions").getAsJsonObject();
    updateSearchOptions(jsonObject);
    JsonArray segmentWrapperArray =
        jsonElement.getAsJsonObject().get("segmentsWrapper").getAsJsonArray();
    for (int i = 0; i < segmentWrapperArray.size(); i++) {
      JsonObject segmentWrapperObject = segmentWrapperArray.get(i).getAsJsonObject();
      JsonArray sectionsArray = segmentWrapperObject.getAsJsonArray("sectionsObjects");
      for (int s = 0; s < sectionsArray.size(); s++) {
        JsonObject sectionObject = sectionsArray.get(s).getAsJsonObject();
        updateFieldCalendarToLong(sectionObject, "arrivalDate");
        updateFieldCalendarToLong(sectionObject, "departureDate");
      }
    }
    JsonArray travellersArray = jsonElement.getAsJsonObject().get("travellers").getAsJsonArray();
    for (int i = 0; i < travellersArray.size(); i++) {
      JsonObject travellerObject = travellersArray.get(i).getAsJsonObject();
      if (travellerObject.has(DATE_OF_BIRTH)) {
        updateFieldCalendarToLong(travellerObject, DATE_OF_BIRTH);
      }
      if (travellerObject.has(ID_EXPIRATION_DATE)) {
        updateFieldCalendarToLong(travellerObject, ID_EXPIRATION_DATE);
      }
    }
    return jsonElement.toString();
  }

  /**
   * Transform the values that contains representation of dates or calendars setted into the
   * strPassengers to long.
   *
   * @param historyString the json string of the object stored in the preference file.
   * @return a new json string with the values modified.
   */
  public static String updateHistoryJSON(String historyString) {
    JsonElement jsonElement = new JsonParser().parse(historyString);
    JsonArray root = jsonElement.getAsJsonArray();
    for (int i = 0; i < root.size(); i++) {
      JsonObject search = root.get(i).getAsJsonObject();
      updateSearchOptions(search);
    }
    return jsonElement.toString();
  }

  /**
   * Transform the values that contains representation of dates or calendars setted into the
   * jsonObject to long.
   *
   * @param objectJSON the json string of the object stored in the preference file.
   */
  private static void updateSearchOptions(JsonObject objectJSON) {
    updateField(objectJSON, "searchDate");
    JsonElement flightSegments = objectJSON.getAsJsonObject().get("flightSegments");
    JsonArray flights = flightSegments.getAsJsonArray();
    for (int j = 0; j < flights.size(); j++) {
      JsonObject flightElement = flights.get(j).getAsJsonObject();
      updateField(flightElement, "arrivalDate");
      updateField(flightElement, "date");
      if (flightElement.has("sections")) {
        JsonArray sectionsArray = flightElement.getAsJsonArray("sections");
        for (int s = 0; s < sectionsArray.size(); s++) {
          JsonObject sectionObject = sectionsArray.get(s).getAsJsonObject();
          updateFieldCalendarToLong(sectionObject, "arrivalDate");
          updateFieldCalendarToLong(sectionObject, "departureDate");
        }
      }
    }
  }

  /**
   * Transform a the tag that is contianed into the JSON Object with a representation of a Date.
   *
   * @param object Json Object that will be modified.
   * @param tagField Tag to be modified.
   */
  private static void updateField(JsonObject object, String tagField) {
    if (object.has(tagField)) {
      String value = object.getAsJsonObject().get(tagField).getAsString();
      long date = OdigeoDateUtils.parseDate(value);
      if (date > 0) {
        object.remove(tagField);
        object.add(tagField, new JsonPrimitive(date));
      }
    }
  }

  /**
   * Transform a the tag that is contianed into the JSON Object with a representation of a Calendar.
   *
   * @param sectionObject Json Object that will be modified.
   * @param tagField Tag to be modified.
   */
  private static void updateFieldCalendarToLong(JsonObject sectionObject, String tagField) {
    JsonObject calendarObject = null;
    try {
      calendarObject = sectionObject.getAsJsonObject(tagField);
    } catch (ClassCastException exception) {
      Log.d("Util", tagField + ": already migrated");
    }
    if (calendarObject != null) {
      int year = calendarObject.get(YEAR).getAsInt();
      int month = calendarObject.get(MONTH).getAsInt();
      int dayOfMonth = calendarObject.get(DAY_OF_MONTH).getAsInt();
      int hourOfDay = calendarObject.get(HOUR_OF_DAY).getAsInt();
      int minute = calendarObject.get(MINUTE).getAsInt();
      int second = calendarObject.get(SECOND).getAsInt();
      Calendar tmpCalendar = Calendar.getInstance();
      tmpCalendar.set(year, month, dayOfMonth, hourOfDay, minute, second);
      sectionObject.remove(tagField);
      sectionObject.add(tagField, new JsonPrimitive(tmpCalendar.getTimeInMillis()));
    }
  }

  public static float calculateDistance(Location locationFrom, Location locationTo,
      DistanceUnits distanceUnit) {

    float distanceInMeters = locationFrom.distanceTo(locationTo);

    if (distanceUnit == DistanceUnits.KILOMETERS) {
      return distanceInMeters / KILOMETERS_DIVIDER;
    } else if (distanceUnit == DistanceUnits.MILES) {
      return distanceInMeters / MILES_DIVIDER;
    } else {
      return distanceInMeters;
    }
  }

  /**
   * Decompress gzip data
   *
   * @param data Byte array containing the data compressed in gzip
   * @param charset Charset of the current data
   * @return String decompressed
   */
  public static String decompress(byte[] data, Charset charset) {
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    ByteArrayInputStream in = new ByteArrayInputStream(data);
    try {
      InputStream inflater = new GZIPInputStream(in);
      byte[] bbuf = new byte[BUFFER_SIZE];
      while (true) {
        int r = inflater.read(bbuf);
        if (r < 0) {
          break;
        }
        buffer.write(bbuf, 0, r);
      }
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
    return new String(buffer.toByteArray(), charset);
  }

  @Nullable public static NeckCookie getCookieById(List<NeckCookie> cookies, String cookieName) {
    NeckCookie neckCookie = null;

    if (cookies != null) {
      for (NeckCookie currentCookie : cookies) {
        if (currentCookie.getValue().startsWith(cookieName + "=")) {
          neckCookie = currentCookie;
        }
      }
    }

    return neckCookie;
  }

  /**
   * This method retrieve the index position of the
   *
   * @param stringPrice String with the currency value
   * @return Index of where be the decimal separator, if can't find it return -1.
   */
  static int getIndexDecimalSeparator(String stringPrice) {
    int indexDot = stringPrice.indexOf(DOT);
    int indexComma = stringPrice.indexOf(COMMA);
    int decimalIndex = Math.max(indexComma, indexDot);
    boolean isNotDecimalIndex = decimalIndex == NO_DECIMAL_SEPARATOR
        || stringPrice.substring(decimalIndex).replaceAll(NON_NUMERIC_CHARS, EMPTY).length() > 2;
    if (isNotDecimalIndex) {
      return NO_DECIMAL_SEPARATOR;
    }
    return decimalIndex;
  }

  public static Spannable getTextStylePrice(String stringPrice) {
    int index = stringPrice.length();
    if (isDecimalSupporterMarket()) {
      index = getIndexDecimalSeparator(stringPrice);
    }
    Spannable spannable = new SpannableString(stringPrice);
    if (isValidIndexPosition(stringPrice, index) && isNotJapaneseMarket()) {
      spannable = resizeDecimalBlock(stringPrice, index, spannable);
      spannable = resizeCurrencySymbol(stringPrice, spannable);
    }

    return spannable;
  }

  /**
   * Return a title for widgets
   *
   * @param context context from de activity
   * @param travelType Direc, Round o Multiple
   * @param index index of the widget in the container
   * @return string with title
   */
  public static String getTitleVolHeader(Context context, TravelType travelType, int index) {
    if (travelType == TravelType.SIMPLE) {
      return LocalizablesFacade.getString(context, "timefilterviewcontroller_labeloutbound")
          .toString();
    } else if (travelType == TravelType.ROUND) {
      if (index == 0) {
        return LocalizablesFacade.getString(context, "timefilterviewcontroller_labeloutbound")
            .toString();
      } else {
        return LocalizablesFacade.getString(context, "timefilterviewcontroller_labelinbound")
            .toString();
      }
    }
    return String.format("%s %s", CommonLocalizables.getInstance().getCommonLeg(context),
        index + 1);
  }

  public static String getTitleHeader(Context context, TravelType travelType, int index) {
    if (travelType == TravelType.SIMPLE) {
      return CommonLocalizables.getInstance().getCommonOutbound(context).toString();
    } else if (travelType == TravelType.ROUND) {
      if (index == 0) {
        return CommonLocalizables.getInstance().getCommonOutbound(context).toString();
      } else {
        return CommonLocalizables.getInstance().getCommonInbound(context).toString();
      }
    }
    return String.format("%s %s", CommonLocalizables.getInstance().getCommonLeg(context),
        index + 1);
  }

  /**
   * Return de correect display mode of the total price in the PricingBreakDow,n depending on the
   * Activity
   *
   * @param stepFlowConfiguration Activity o step currently
   * @param stepsList This list is obtained from the Activity Results
   */
  public static PricingMode getPriceTotalMode(Step stepFlowConfiguration,
      List<StepsConfiguration> stepsList) {
    if (stepsList != null) {
      for (StepsConfiguration step : stepsList) {
        if (step != null && (step.getStep() == stepFlowConfiguration
            || step.getStep() == Step.DEFAULT)) {
          return step.getTotalPriceMode();
        }
      }
    }

    return PricingMode.TOTAL;
  }

  /**
   * Returned id resource string from CabinCalssDTO
   *
   * @param cabinClassDTO cabinClass of the Section
   * @return the resource in strings.xml file
   */
  public static String getResourceCabinClass(CabinClassDTO cabinClassDTO) {
    // code copied from iOS code, -(NSString *)localizedCabinClass:(NSString *)cabinClass
    if (cabinClassDTO != null) {
      if (cabinClassDTO == CabinClassDTO.BUSINESS) {
        return "sectioncell_cabinclassbusiness";
      } else if (cabinClassDTO == CabinClassDTO.FIRST) {
        return "sectioncell_cabinclassfirst";
      } else if (cabinClassDTO == CabinClassDTO.PREMIUM_ECONOMY) {
        return "sectioncell_cabinclasspremiumeconomy";
      } else if (cabinClassDTO == CabinClassDTO.ECONOMIC_DISCOUNTED) {
        return "sectioncell_cabinclasseconomicdiscounted";
      } else if (cabinClassDTO == CabinClassDTO.TOURIST) {
        return "sectioncell_cabinclasstourist";
      }
    }
    return "sectioncell_cabinclasstourist";
  }

  public static String getDistanceUnitName(DistanceUnits distanceUnit, Context context) {
    String distanceUnitName = null;

    if (distanceUnit == DistanceUnits.KILOMETERS) {
      distanceUnitName = LocalizablesFacade.getString(context, "common_units_km").toString();
    } else if (distanceUnit == DistanceUnits.MILES) {
      distanceUnitName = LocalizablesFacade.getString(context, "common_units_miles").toString();
    }
    return distanceUnitName;
  }

  public static boolean isSpiceTimeoutException(Object exception) {
    boolean isTimeout = false;

    if (exception instanceof SpiceException) {
      SpiceException spiceException = (SpiceException) exception;
      if (spiceException.getCause() instanceof SocketTimeoutException) {
        isTimeout = true;
      }
    }
    return isTimeout;
  }

  //TODO Review what does this function do?
  public static void showTimeoutException(android.support.v4.app.FragmentManager fragmentManager,
      Step currentStep) {
    MslError error = new MslError();
    error.setCode(ERROR_CODE);
    error.setCodeString(Code.CONNECTION_TIMEOUT);
    if (error.getCode() != null) {
      MSLErrorUtilsDialogFragment fragment = MSLErrorUtilsDialogFragment.newInstance(null, error);
      fragment.setParentScreen(currentStep.name());
      fragment.show(fragmentManager, error.getCodeString().name());
    }
  }

  public static void sendEmailFeedback(String to, String subject, Activity activity) {

    String footerText = String.format(
        "<br/><br/><br/><font size=\"6\" color=\"lightgray\">%s%s v%s - %s<br/>%s</font>",
        activity.getResources().getString(R.string.copyright),
        Configuration.getInstance().getBrandVisualName(),
        DeviceUtils.getApplicationVersionName(activity),
        Configuration.getInstance().getCurrentMarket().getKey(), Build.MODEL);

    Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", to, null));
    sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
    sendIntent.putExtra(Intent.EXTRA_TEXT, HtmlUtils.formatHtml(footerText));
    activity.startActivity(Intent.createChooser(sendIntent,
        LocalizablesFacade.getRawString(activity, "aboutoptionsmodule_about_option_send_email")));
  }

  /**
   * Redirects to home activity
   */
  public static void sendToHome(Activity activity, OdigeoApp odigeoApp) {
    Intent intent = new Intent(activity, odigeoApp.getHomeActivity());
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    activity.startActivity(intent);
    activity.finish();
  }

  //TODO: use format from class LocaleUtils (delete this method and use class)
  public static String validationTimeFormatEU(String format) {
    if (Configuration.getCurrentLocale().getCountry().equals("US") && Configuration.getInstance()
        .getBrandPrefix()
        .equals("ED")) {
      return format.replace("HH:mm", "hh:mm a");
    } else {
      return format;
    }
  }

  /**
   * Prevents a string can be null
   *
   * @param text String to check if it is null
   * @return If the String is null then returns empty string
   */
  public static String notNullableString(String text) {
    if (text == null) {
      return "";
    }

    return text;
  }

  /**
   * This method sends a broadcast to indicate to the widget needs update.
   *
   * @param context Context where be called the update.
   * @param widgetClass Class reference to the widget implementation of {@link
   * com.odigeo.app.android.lib.widget.OdigeoWidget}.
   */
  public static void triggerWidgetUpdate(@NonNull Context context, @NonNull Class widgetClass) {
    Intent intent = new Intent(context, widgetClass);
    intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
    int[] ids = AppWidgetManager.getInstance(context)
        .getAppWidgetIds(new ComponentName(context, widgetClass));
    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
    context.sendBroadcast(intent);
  }

  /**
   * Generate of a list with the bookings Id for to show in SummaryActivity
   *
   * @param clientBookingId Client identifier
   * @param bookingDTOs bookings id for the flight
   * @return The correct data structure for to show in SummaryActivity
   */
  @Nullable public static List<String> makeClientBookingIds(String clientBookingId,
      @Nullable List<ItineraryProviderBookingDTO> bookingDTOs) {
    if (bookingDTOs != null) {
      Set<String> set = new HashSet<>();
      List<String> bookingsIds = new ArrayList<>();
      for (ItineraryProviderBookingDTO bookingDTO : bookingDTOs) {
        set.add(bookingDTO.getPnr());
      }

      for (String pnr : set.toArray(new String[set.size()])) {
        bookingsIds.add(clientBookingId + " - " + pnr);
      }
      return bookingsIds;
    }
    return null;
  }

  public static String getAdultsLabel(int noAdults, Context context) {
    if (noAdults == 1) {
      return CommonLocalizables.getInstance().getCommonAdult(context).toString();
    } else {
      return CommonLocalizables.getInstance().getCommonAdults(context).toString();
    }
  }

  public static String getKidsLabel(int noKids, Context context) {
    if (noKids == 1) {
      return CommonLocalizables.getInstance().getCommonChild(context).toString();
    } else {
      return CommonLocalizables.getInstance().getCommonChildren(context).toString();
    }
  }

  public static String getBabiesLabel(int noBabies, Context context) {
    if (noBabies == 1) {
      return CommonLocalizables.getInstance().getCommonInfant(context).toString();
    } else {
      return CommonLocalizables.getInstance().getCommonInfants(context).toString();
    }
  }

  /**
   * Get a dimen resource in DPs, to do this we get the current dimen value and divide it by the
   * current display density
   *
   * @param context Context to get te dimens and metrics values
   * @param dimen Dimen resource to get
   * @return The current dimen value in DPs
   */
  public static float getDimensionInDP(@NonNull Context context, @DimenRes int dimen) {
    return context.getResources().getDimension(dimen) / context.getResources()
        .getDisplayMetrics().density;
  }

  private static boolean isDecimalSupporterMarket() {
    Locale isoLocale = LocaleUtils.getCurrentLocale();
    return !LocaleUtils.JA_JP_KEY.equals(LocaleUtils.localeToString(isoLocale))
        && !LocaleUtils.IN_ID_KEY.equals(LocaleUtils.localeToString(isoLocale))
        && !LocaleUtils.ES_CO_KEY.equals(LocaleUtils.localeToString(isoLocale));
  }

  private static boolean isValidIndexPosition(String stringPrice, int index) {
    return index != stringPrice.length() && index > 0;
  }

  private static boolean isNotJapaneseMarket() {
    return !Configuration.getInstance()
        .getCurrentMarket()
        .getKey()
        .equals(Constants.JAPANESE_MARKET);
  }

  private static Spannable resizeDecimalBlock(String stringPrice, int index, Spannable spannable) {
    spannable.setSpan(new RelativeSizeSpan(DECIMAL_RESIZE), index, stringPrice.length(),
        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    return spannable;
  }

  private static Spannable resizeCurrencySymbol(String stringPrice, Spannable spannable) {
    int index = getFirstIndexOfNumber(stringPrice);
    if(index > 0) {
      spannable.setSpan(new RelativeSizeSpan(DECIMAL_RESIZE), 0, index, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    return spannable;
  }

  private static int getFirstIndexOfNumber(String stringPrice) {
    Matcher matcher = Pattern.compile("\\d+").matcher(stringPrice);
    matcher.find();
    int i = Integer.valueOf(matcher.start());
    return i;
  }
}
