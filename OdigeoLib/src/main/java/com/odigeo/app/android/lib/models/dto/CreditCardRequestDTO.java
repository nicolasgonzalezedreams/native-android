package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

/**
 * Created by ManuelOrtiz on 08/10/2014.
 */
public class CreditCardRequestDTO implements Serializable {
  private String cardTypeCode;
  private String cardNumber;
  private String cardOwner;
  private String cardExpirationYear;
  private String cardExpirationMonth;
  private String cardSecurityNumber;

  public String getCardOwner() {
    return cardOwner;
  }

  public void setCardOwner(String cardOwner) {
    this.cardOwner = cardOwner;
  }

  public String getCardTypeCode() {
    return cardTypeCode;
  }

  public void setCardTypeCode(String cardTypeCode) {
    this.cardTypeCode = cardTypeCode;
  }

  public String getCardSecurityNumber() {
    return cardSecurityNumber;
  }

  public void setCardSecurityNumber(String cardSecurityNumber) {
    this.cardSecurityNumber = cardSecurityNumber;
  }

  public String getCardExpirationYear() {
    return cardExpirationYear;
  }

  public void setCardExpirationYear(String cardExpirationYear) {
    this.cardExpirationYear = cardExpirationYear;
  }

  public String getCardExpirationMonth() {
    return cardExpirationMonth;
  }

  public void setCardExpirationMonth(String cardExpirationMonth) {
    this.cardExpirationMonth = cardExpirationMonth;
  }

  public String getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
  }
}
