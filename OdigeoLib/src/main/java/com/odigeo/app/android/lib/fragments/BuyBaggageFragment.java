package com.odigeo.app.android.lib.fragments;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.BuyBaggageAdapter;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.BuyBaggageItemModel;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.dto.BaggageApplicationLevelDTO;
import com.odigeo.app.android.lib.models.dto.BaggageConditionsDTO;
import com.odigeo.app.android.lib.models.dto.BaggageDescriptorDTO;
import com.odigeo.app.android.lib.models.dto.BaggageSelectionDTO;
import com.odigeo.app.android.lib.models.dto.SegmentTypeIndexDTO;
import com.odigeo.app.android.lib.models.dto.SelectableBaggageDescriptorDTO;
import com.odigeo.app.android.lib.utils.BaggageUtils;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Custom Fragment for showing baggage options using a list of flight segments
 *
 * @author M. en C. Javier Silva Pérez
 * @since 2/6/2015.
 */
public class BuyBaggageFragment extends Fragment {

  private List<FlightSegment> flightSegments = new LinkedList<FlightSegment>();
  private List<BaggageConditionsDTO> baggageConditions = new LinkedList<>();
  private Map<SegmentTypeIndexDTO, SelectableBaggageDescriptorDTO> baggageSelectionMap;
  private final AdapterView.OnItemSelectedListener generalSpinnerItemSelectedListener =
      new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
          BuyBaggageItemModel itemModel = (BuyBaggageItemModel) parent.getSelectedItem();
          Log.d(Constants.TAG_LOG, "Index: " + itemModel.getSegmentTypeIndexDTO());
          //Depending on selection, add the baggage selection or remove it from the map
          SelectableBaggageDescriptorDTO selectableBaggageDescriptor =
              itemModel.getSelectableBaggageDescriptor();
          if (position == 0
              || selectableBaggageDescriptor.getBaggageDescriptor().getPieces() == 0) {
            //If selected position is 0, remove the segment baggage option
            baggageSelectionMap.remove(itemModel.getSegmentTypeIndexDTO());
          } else {
            //Add the new baggage selection

            baggageSelectionMap.put(itemModel.getSegmentTypeIndexDTO(),
                selectableBaggageDescriptor);
          }
        }

        @Override public void onNothingSelected(AdapterView<?> parent) { /*Nothing to do here*/ }
      };

  /**
   * Creates a new instance for this fragment using the list of flight segments
   *
   * @param flightSegments List of available flight segments used in this fragment
   * @param conditions Baggage conditions to show in this fragment
   * @return A new instance of {@link com.odigeo.app.android.lib.fragments.BuyBaggageFragment}
   */
  public static BuyBaggageFragment newInstance(List<FlightSegment> flightSegments,
      List<BaggageConditionsDTO> conditions) {
    BuyBaggageFragment buyBaggageFragment = new BuyBaggageFragment();
    buyBaggageFragment.setFlightSegments(flightSegments);
    buyBaggageFragment.setBaggageConditions(conditions);
    return buyBaggageFragment;
  }

  public final void setBaggageConditions(List<BaggageConditionsDTO> baggageConditions) {
    this.baggageConditions = baggageConditions;
  }

  public final void setFlightSegments(List<FlightSegment> flightSegments) {
    this.flightSegments = flightSegments;
  }

  @Override public final View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View root = inflater.inflate(R.layout.fragment_baggage_info, container, false);
    LinearLayout lytBaggageContainer = (LinearLayout) root.findViewById(R.id.lyt_baggage_options);
    baggageSelectionMap = new HashMap<>();
    //Check if the list is empty
    if (baggageConditions == null || baggageConditions.isEmpty()) {
      //If the list is empty its impossible to check baggage conditions, so do not add any row
      addInformationCell(inflater, lytBaggageContainer);
    } else {
      Collections.sort(baggageConditions, new BaggageSortCollection());
      //Check all scenarios and add the corresponding views to the baggage container
      checkScenarios(inflater, lytBaggageContainer);
    }
    return root;
  }

  /**
   * Check all the baggage scenarios and add the corresponding rows to the baggage layout, in this
   * method we first check all the general scenarios (1, 5 and 6) and then the specific scenarios
   * (2, 3 and 4), scenarios to check: <ol> <li>Fare includes the same baggage allowance in all
   * segments of the itinerary. (If the same bag is included per segment and no more bags can be
   * added)</li> <li>Fare includes baggage allowance, but not for all segments of the itinerary.
   * (If one of the bags is included, but the user still can add more baggage, "add baggage"
   * button is shown, that brings up a dropdown with the number of bags available, weight (if
   * present) and its price)</li> <li>Non of segment include free baggage (If none of the bags is
   * included for free in the ticket fare, but the user can add baggages, "add baggage" button is
   * shown for every item, that brings up a dropdown with the number of bags available, weight (if
   * present) and its price)</li> <li>Can't add baggage in one or more segments (If one of the
   * segments does not allow baggage allowance adding.)</li> <li>Can't add baggage in any segment
   * (If the fare does not include free baggage and we can not offer it).</li> <li>Can add baggage
   * only by itinerary (If user can add baggage but only for the whole itinerary.Functionality is
   * the same as the other cases).</li> </ol>
   *
   * @param inflater LayoutInflater to inflate the rows layouts
   * @param lytBaggageContainer LinearLayout to add all the rows
   */
  private void checkScenarios(LayoutInflater inflater, LinearLayout lytBaggageContainer) {
    BaggageConditionsDTO firstBaggageConditions = baggageConditions.get(0);
    //Fist check the scenario 6 - "Can add baggage only by itinerary"
    if (checkCanAddItineraryScenario()) {
      //If the baggage information corresponds to this scenario, add a row
      addStandardCell(inflater, lytBaggageContainer, firstBaggageConditions,
          LocalizablesFacade.getString(getActivity(),
              "travellersviewcontroller_baggageconditions_addbaggage").toString());
      return;
    }

    //Check scenarios 1 and 5
    BaggageScenario cellType = checkAllSegmentsAllowance();
    if (cellType == BaggageScenario.SAME_BAGGAGE_INCLUDED) {
      //Get initial conditions
      addBaggageIncludedCell(inflater, lytBaggageContainer, firstBaggageConditions);
    } else if (cellType == BaggageScenario.CANT_ADD_BAGGAGE_TO_ITINERARY) {
      addInformationCell(inflater, lytBaggageContainer);
    } else {
      //Check baggage conditions one by one and add the corresponding rows depending the scenario
      int counter = 0;
      for (BaggageConditionsDTO conditions : baggageConditions) {
        //If the conditions includes baggage in ticket fare, scenario 2
        Boolean includeBaggage =
            !isBaggageOptionEmpty(conditions.getBaggageDescriptorIncludedInPrice());
        FlightSegment segment =
            BaggageUtils.getFlightSegmentByIndex(flightSegments, conditions.getSegmentTypeIndex());
        if (includeBaggage) {
          addStandardCellNotEditable(inflater, lytBaggageContainer, conditions, segment);
        }

        //Check if the user can add baggage, scenario 2,3 and 4
        if (canAddBaggage(conditions)) {
          //Add StandardCell
          addDestinationCell(inflater, lytBaggageContainer, conditions, segment);
        } else if (!includeBaggage) {
          //If we cant add baggage, show leg information and fare does not include
          addLegInformationCell(inflater, lytBaggageContainer, segment);
        }

        counter++;
      }
    }
  }

  /**
   * Adds information cell, this cell shows a message that indicates that no baggage is included
   * and we cant add more
   *
   * @param inflater LayoutInflater to inflate the row layout
   * @param lytBaggageContainer LinearLayout to add the row
   */
  private void addInformationCell(LayoutInflater inflater, LinearLayout lytBaggageContainer) {
    String phone =
        LocalizablesFacade.getString(getContext(), OneCMSKeys.MARKET_SPECIFIC_CALLCENTER_PHONE)
            .toString();
    if (phone.isEmpty()) {
      //If the phone list is empty, choose a default empty string to show
      phone = getString(R.string.empty_text_default);
    }
    //Add row to layout
    lytBaggageContainer.addView(newTextCell(inflater, lytBaggageContainer,
        BaggageUtils.getInformationContent(getActivity(), phone),
        R.drawable.confirmation_baggage_prohibido));
  }

  /**
   * Adds information cell, this cell shows a message that indicates that no baggage is included
   * and we cant add more
   *
   * @param inflater LayoutInflater to inflate the row layout
   * @param lytBaggageContainer LinearLayout to add the row
   * @param segment FlightSegment to show in this cell
   */
  private void addLegInformationCell(LayoutInflater inflater, LinearLayout lytBaggageContainer,
      FlightSegment segment) {
    //Get the first call center phone of the list
    String phone =
        LocalizablesFacade.getString(getContext(), OneCMSKeys.MARKET_SPECIFIC_CALLCENTER_PHONE)
            .toString();
    if (phone.isEmpty()) {
      //If the phone list is empty, choose a default empty string to show
      phone = getString(R.string.empty_text_default);
    }
    //Add row to layout
    lytBaggageContainer.addView(newTextWithHintCell(inflater, lytBaggageContainer,
        BaggageUtils.getLegInformationContent(getActivity(), phone),
        BaggageUtils.getSegmentName(getActivity(), segment)));
  }

  /**
   * Add baggage included cell, this cell shows a message indicating the included baggage
   * conditions for the complete itinerary
   *
   * @param inflater LayoutInflater to inflate the row layout
   * @param lytBaggageContainer LinearLayout to add the row
   * @param baggageConditions Baggage conditions to add
   */
  private void addBaggageIncludedCell(LayoutInflater inflater, LinearLayout lytBaggageContainer,
      BaggageConditionsDTO baggageConditions) {
    lytBaggageContainer.addView(newTextCell(inflater, lytBaggageContainer,
        BaggageUtils.getBaggageIncludedContent(getActivity(),
            baggageConditions.getBaggageDescriptorIncludedInPrice().getPieces(),
            baggageConditions.getBaggageDescriptorIncludedInPrice().getKilos()),
        R.drawable.confirmation_baggage));
  }

  /**
   * Adds a standard cell to the layout, this cell c
   *
   * @param inflater LayoutInflater to inflate the rows layouts
   * @param lytBaggageContainer LinearLayout to add the row
   * @param baggageConditions Baggage conditions to add
   * @param legText Text to show as hint of the spinner
   */
  private void addStandardCell(LayoutInflater inflater, LinearLayout lytBaggageContainer,
      BaggageConditionsDTO baggageConditions, String legText) {
    BuyBaggageAdapter adapter = createAdapter(baggageConditions);
    adapter.setDropDownViewResource(R.layout.layout_baggage_item);
    lytBaggageContainer.addView(
        newSpinnerWithHintCell(inflater, lytBaggageContainer, legText, adapter,
            generalSpinnerItemSelectedListener));
  }

  /**
   * Add ceel with info to Citites in the travel
   *
   * @param inflater LayoutInflater to inflate the rows layouts
   * @param lytBaggageContainer LinearLayout to add the row
   * @param baggageConditions Baggage conditions to add
   * @param segment Cities to show
   */
  private void addDestinationCell(LayoutInflater inflater, LinearLayout lytBaggageContainer,
      BaggageConditionsDTO baggageConditions, FlightSegment segment) {
    BuyBaggageAdapter adapter = createAdapter(baggageConditions);
    adapter.setDropDownViewResource(R.layout.layout_baggage_item);
    lytBaggageContainer.addView(
        newSpinnerCitiesWithHintCell(inflater, lytBaggageContainer, segment, adapter,
            generalSpinnerItemSelectedListener));
  }

  /**
   * Adds a standard cell to the layout, this cell has a text that indicates the included bags
   *
   * @param inflater LayoutInflater to inflate the rows layouts
   * @param lytBaggageContainer LinearLayout to add the row
   * @param conditionsDTO Baggage Condition to show
   * @param segment FlightSegment to show in this cell
   */
  private void addStandardCellNotEditable(LayoutInflater inflater, LinearLayout lytBaggageContainer,
      BaggageConditionsDTO conditionsDTO, FlightSegment segment) {
    String baggageIncludedText = BaggageUtils.getBaggageIncludedText(getActivity(),
        conditionsDTO.getBaggageDescriptorIncludedInPrice().getPieces(),
        LocalizablesFacade.getRawString(getActivity(),
            "travellersviewcontroller_baggageconditions_legbaggageincluded"),
        conditionsDTO.getBaggageDescriptorIncludedInPrice().getKilos(),
        LocalizablesFacade.getRawString(getActivity(),
            "travellersviewcontroller_baggageconditions_maxbagweight"));
    lytBaggageContainer.addView(
        newTextWithHintCell(inflater, lytBaggageContainer, baggageIncludedText,
            BaggageUtils.getSegmentName(getActivity(), segment)));
  }

  /**
   * Check if the list of baggage estimation fees corresponds to "Can add baggage only for
   * itinerary" scenario, which states: If user can add baggage but only for the whole itinerary.
   *
   * @return TRUE if the baggage estimation fees has only one item that states that the user can
   * add baggage only for the hole itinerary and not per leg
   */
  private Boolean checkCanAddItineraryScenario() {
    //Get the first estimation fee object and get its conditions
    BaggageConditionsDTO conditionsDTO = baggageConditions.get(0);
    //Check if the condition has Itinerary application level and can add baggage to it
    return conditionsDTO.getBaggageApplicationLevel() == BaggageApplicationLevelDTO.ITINERARY
        && canAddBaggage(conditionsDTO);
  }

  /**
   * Check the entire list, looking for any of the following scenarios: <ul><li>Scenario 1: Fare
   * includes the same baggage allowance in all segments of the itinerary. (If the same bag is
   * included per segment and no more bags can be added)</li> <li>Scenario 5: Can't add baggage in
   * any segment (If the fare does not include free baggage and we can not offer it).</li> </ul>
   *
   * @return {@link com.odigeo.app.android.lib.fragments.BuyBaggageFragment.BaggageScenario#SAME_BAGGAGE_INCLUDED}
   * if the first scenario is full filled, {@link com.odigeo.app.android.lib.fragments.BuyBaggageFragment.BaggageScenario#CANT_ADD_BAGGAGE_TO_ITINERARY}
   * if the second scenario is, or {@link com.odigeo.app.android.lib.fragments.BuyBaggageFragment.BaggageScenario#NON_FREE_BAGGAGE}
   * in other case
   */
  private BaggageScenario checkAllSegmentsAllowance() {
    //Check if we can add baggage at least in one leg of the itinerary, if so, this value will be TRUE
    Boolean canAddBaggage = Boolean.FALSE;
    //Check if at least one segment of the itinerary includes free baggage
    Boolean includeFreeBaggage = Boolean.FALSE;
    //Check if all the segments in the itinerary has the same included baggage options
    Boolean hasSameIncludedBaggage = Boolean.TRUE;
    //Get the first baggage conditions to use it to compare the rest of the list
    BaggageConditionsDTO currentConditions = baggageConditions.get(0);
    for (BaggageConditionsDTO conditionsDTO : baggageConditions) {
      //Check if we can add baggage to the current segment
      canAddBaggage = canAddBaggage || canAddBaggage(conditionsDTO);
      //Check if the segment includes free baggage
      includeFreeBaggage = includeFreeBaggage || !isBaggageOptionEmpty(
          conditionsDTO.getBaggageDescriptorIncludedInPrice());

      //If all the segments include free baggage, compare the current segment with the first one,
      // if not means that at least one previous segment does not include free baggage so no
      // case comparing
      if (includeFreeBaggage) {
        //Check if the included baggage is the same that the first one
        hasSameIncludedBaggage = hasSameIncludedBaggage && compareBaggageDescriptor(
            currentConditions.getBaggageDescriptorIncludedInPrice(),
            conditionsDTO.getBaggageDescriptorIncludedInPrice());
      } else {
        hasSameIncludedBaggage = Boolean.FALSE;
      }
    }

    //If in any segment of the itinerary we can add baggage and does not include free
    // baggage is scenario 5
    if (!canAddBaggage && !includeFreeBaggage) {
      return BaggageScenario.CANT_ADD_BAGGAGE_TO_ITINERARY;
    }

    //If we cant add baggage to any segment and all the segments has the same options
    // is scenario 1
    if (!canAddBaggage && hasSameIncludedBaggage) {
      return BaggageScenario.SAME_BAGGAGE_INCLUDED;
    }

    return BaggageScenario.NON_FREE_BAGGAGE;
  }

  /**
   * Compares two {@link com.odigeo.app.android.lib.models.dto.BaggageDescriptorDTO} objects, two
   * baggage descriptions are the same when both has the same pieces and kilos
   *
   * @param left First object to compare
   * @param right Second object to compare
   * @return TRUE if both object has the same price, pieces and price
   */
  private Boolean compareBaggageDescriptor(BaggageDescriptorDTO left, BaggageDescriptorDTO right) {
    return left.getPieces() == right.getPieces() && left.getKilos() == right.getKilos();
  }

  /**
   * Creates an adapter based on a list of selectable baggage descriptions
   *
   * @param baggageConditionsDTO Baggage conditions to check
   * @return An adapter filled out with the corresponding baggage options
   */
  private BuyBaggageAdapter createAdapter(@NonNull BaggageConditionsDTO baggageConditionsDTO) {
    //Create the item model list and add the default item
    List<BuyBaggageItemModel> itemModels = new LinkedList<BuyBaggageItemModel>();
    //If the conditions does not include baggage, add a default item
    //        itemModels.add(new BuyBaggageItemModel(null, baggageConditionsDTO.getSegmentTypeIndex()));
    //Go over selectable baggage descriptor and add a new item to the list
    for (SelectableBaggageDescriptorDTO baggageDescriptorDTO : baggageConditionsDTO.getSelectableBaggageDescriptor()) {
      itemModels.add(
          getStandardCellItem(baggageDescriptorDTO, baggageConditionsDTO.getSegmentTypeIndex()));
    }

    return new BuyBaggageAdapter(getActivity(), itemModels);
  }

  /**
   * Check if a baggage descriptor corresponds to an empty baggage cell
   *
   * @param includedBaggage Baggage descriptor
   * @return TRUE the descriptor is null or the included pieces is 0 or less
   */
  private Boolean isBaggageOptionEmpty(BaggageDescriptorDTO includedBaggage) {
    return includedBaggage == null || includedBaggage.getPieces() <= 0;
  }

  /**
   * Check if we can add baggage to the specified conditions
   *
   * @param baggageConditionsDTO Baggage conditions to check
   * @return TRUE if the selectable baggage options are not null and not empty
   */
  private Boolean canAddBaggage(BaggageConditionsDTO baggageConditionsDTO) {
    List<SelectableBaggageDescriptorDTO> selectableBaggageDescriptorDTOs =
        baggageConditionsDTO.getSelectableBaggageDescriptor();
    return selectableBaggageDescriptorDTOs != null && !selectableBaggageDescriptorDTOs.isEmpty();
  }

  /**
   * Creates a new view with a simple text view this cell can be used for: <ul><li>{@link
   * com.odigeo.app.android.lib.fragments.BuyBaggageFragment.BaggageScenario#BAGGAGE_INCLUDED}</li>
   * <li>{@link com.odigeo.app.android.lib.fragments.BuyBaggageFragment.BaggageScenario#CANT_ADD_BAGGAGE_TO_ITINERARY}</li></ul>
   *
   * @param inflater LayoutInflater to inflate the row layout
   * @param container Container of the new cell, the cell view will not be added by default
   * @param message Message to set as cell content
   * @return The view to add
   */
  private View newTextCell(LayoutInflater inflater, ViewGroup container, String message,
      @DrawableRes int indicator) {
    View root = inflater.inflate(R.layout.fragment_baggage_text_row, container, false);
    View hint = root.findViewById(R.id.txt_row_hint);
    //Hide view hint
    hint.setVisibility(View.GONE);
    //Set the message to content text view
    TextView content = (TextView) root.findViewById(R.id.txt_row_content);
    content.setCompoundDrawablesWithIntrinsicBounds(indicator, 0, 0, 0);
    content.setText(Html.fromHtml(message));
    return root;
  }

  /**
   * Creates a new view with a single text view as content and an other as hint
   *
   * @param inflater LayoutInflater to inflate the row layout
   * @param container Container of the new cell, the cell view will not be added by default
   * @param message Message to set as cell content
   * @param hint Hint message to show
   * @return The view to add
   */
  private View newTextWithHintCell(LayoutInflater inflater, ViewGroup container, String message,
      String hint) {
    View root = inflater.inflate(R.layout.fragment_baggage_text_row, container, false);
    //Set the message and hint values
    TextView content = (TextView) root.findViewById(R.id.txt_row_content);
    content.setText(Html.fromHtml(message));
    TextView txtHint = (TextView) root.findViewById(R.id.txt_row_hint);
    txtHint.setText(hint);
    return root;
  }

  /**
   * Creates a new view with the spinner to select baggage options
   *
   * @param inflater LayoutInflater to inflate the row layout
   * @param container Container of the new cell, the cell view will not be added by default
   * @param hint Hint message to show
   * @param adapter Adapter to set to the spinner, this should include the options to set
   * @param itemSelectedListener Item click lister for the spinner in the cell
   * @return The view to add
   */
  private View newSpinnerWithHintCell(LayoutInflater inflater, ViewGroup container, String hint,
      SpinnerAdapter adapter, AdapterView.OnItemSelectedListener itemSelectedListener) {
    View root = inflater.inflate(R.layout.fragment_baggage_spinner_row, container, false);
    //Set the message and hint values
    TextView txtHint = (TextView) root.findViewById(R.id.txt_row_hint);
    txtHint.setText(hint);
    Spinner txtSpinner = (Spinner) root.findViewById(R.id.txt_row_spinner);
    txtSpinner.setAdapter(adapter);
    txtSpinner.setOnItemSelectedListener(itemSelectedListener);
    return root;
  }

  /**
   * Creates a new view with the spinner to select baggage options
   *
   * @param inflater LayoutInflater to inflate the row layout
   * @param container Container of the new cell, the cell view will not be added by default
   * @param segment Cities to show
   * @param adapter Adapter to set to the spinner, this should include the options to set
   * @param itemSelectedListener Item click lister for the spinner in the cell
   * @return The view to add
   */
  private View newSpinnerCitiesWithHintCell(LayoutInflater inflater, ViewGroup container,
      FlightSegment segment, SpinnerAdapter adapter,
      AdapterView.OnItemSelectedListener itemSelectedListener) {
    View root =
        inflater.inflate(R.layout.fragment_baggage_spinner_row_destination, container, false);
    //Set the message and hint values
    TextView txtHintDeparture = (TextView) root.findViewById(R.id.txt_row_hint_departure);
    TextView txtHintArrival = (TextView) root.findViewById(R.id.txt_row_hint_destination);
    txtHintDeparture.setText(HtmlUtils.formatHtml(segment.getDepartureCity().getCityName()));
    txtHintArrival.setText(HtmlUtils.formatHtml(segment.getArrivalCity().getCityName()));
    Spinner txtSpinner = (Spinner) root.findViewById(R.id.txt_row_spinner);
    txtSpinner.setAdapter(adapter);
    txtSpinner.setOnItemSelectedListener(itemSelectedListener);
    return root;
  }

  /**
   * Get standard cell item using the selectable baggage descriptor to create the item option
   * text
   *
   * @param selectableBaggageDescriptorDTO Descriptor to use to filled out the baggage option
   * @param segmentTypeIndexDTO Segment to assign the new item
   * @return A {@link com.odigeo.app.android.lib.models.BuyBaggageItemModel} with the values to
   * show in the adapter view
   */
  private BuyBaggageItemModel getStandardCellItem(
      SelectableBaggageDescriptorDTO selectableBaggageDescriptorDTO,
      SegmentTypeIndexDTO segmentTypeIndexDTO) {
    BuyBaggageItemModel buyBaggageItemModel = new BuyBaggageItemModel();

    //Set buy baggage parameters
    buyBaggageItemModel.setSelectableBaggageDescriptor(selectableBaggageDescriptorDTO);
    buyBaggageItemModel.setSegmentTypeIndexDTO(segmentTypeIndexDTO);
    return buyBaggageItemModel;
  }

  /**
   * Go over the baggage options and return the list of the baggage selected by the user
   *
   * @return a list of {@link com.odigeo.app.android.lib.models.dto.BaggageSelectionDTO} filled up
   * with the selected baggage options selected by the user, this list will only include the
   * baggage options not included in the current fare
   */
  public final List<BaggageSelectionDTO> getAdditionalBaggageSelection() {
    List<BaggageSelectionDTO> baggageSelectionList = new LinkedList<BaggageSelectionDTO>();
    //Go over the selection map and create a new baggageSelectionDTO for each entry
    for (Map.Entry<SegmentTypeIndexDTO, SelectableBaggageDescriptorDTO> entry : baggageSelectionMap.entrySet()) {
      BaggageDescriptorDTO baggageDescriptor = entry.getValue().getBaggageDescriptor();
      addBaggageSelection(baggageSelectionList, entry.getKey(), baggageDescriptor);
    }
    return baggageSelectionList;
  }

  /**
   * Add the necessary baggage selections to the list, the objects to add will depend on the
   * segment index, if its null this method will add one baggage selection per flight segment
   * because it considers that the baggage selections its for the full itinerary
   *
   * @param baggageSelectionList List to add the baggage selections
   * @param segmentTypeIndexDTO The segment to which correspond the new baggage selection,
   * @param baggageDescriptor Baggage descriptor to add
   */
  private void addBaggageSelection(List<BaggageSelectionDTO> baggageSelectionList,
      SegmentTypeIndexDTO segmentTypeIndexDTO, BaggageDescriptorDTO baggageDescriptor) {
    if (segmentTypeIndexDTO == null) {
      //If the Segment index is null, means that the baggage condition is per itinerary, so we need to add one selection option per segment
      for (int i = 0; i < flightSegments.size(); i++) {
        baggageSelectionList.add(
            new BaggageSelectionDTO(baggageDescriptor, SegmentTypeIndexDTO.fromIndex(i)));
      }
    } else {
      baggageSelectionList.add(new BaggageSelectionDTO(baggageDescriptor, segmentTypeIndexDTO));
    }
  }

  /**
   * Gets a list for all the included baggage no mather if its included in ticket fare or if it
   * has been added by the user
   *
   * @return a list of {@link com.odigeo.app.android.lib.models.dto.BaggageSelectionDTO} with the
   * baggage included in ticket fare
   */
  public final List<BaggageSelectionDTO> getIncludedBaggage() {
    List<BaggageSelectionDTO> baggageSelectionList = new LinkedList<BaggageSelectionDTO>();
    for (BaggageConditionsDTO baggageCondition : baggageConditions) {
      BaggageDescriptorDTO baggageDescriptor =
          baggageCondition.getBaggageDescriptorIncludedInPrice();
      //If the conditions has included baggage adds it to the final list
      if (!isBaggageOptionEmpty(baggageDescriptor)) {
        addBaggageSelection(baggageSelectionList, baggageCondition.getSegmentTypeIndex(),
            baggageDescriptor);
      }
    }
    return baggageSelectionList;
  }

  /**
   * Enum that stores the baggage types that can have each leg, depending on this type the row can
   * change its layout
   */
  public enum BaggageScenario {
    /**
     * Scenario 1: Fare includes the same baggage allowance in all segments of the itinerary.
     * (If the same bag is included per segment and no more bags can be added)
     */
    SAME_BAGGAGE_INCLUDED, /**
     * Scenario 2: Fare includes baggage allowance, but not for all segments of the itinerary.
     * (If one of the bags is included, but the user still can add more baggage
     */
    BAGGAGE_INCLUDED, /**
     * Scenario 3: Non of segment include free baggage (If none of the bags is included for free
     * in the ticket fare, but the user can add baggage
     */
    NON_FREE_BAGGAGE, /**
     * Scenario 4: Can't add baggage in one or more segments (If one of the segments does not
     * allow baggage allowance adding.)
     */
    CANT_ADD_BAGGAGE_TO_SEGMENT, /**
     * Scenario 5: Can't add baggage in any segment (If the fare does not include free baggage
     * and we can not offer it).
     */
    CANT_ADD_BAGGAGE_TO_ITINERARY, /**
     * Scenario 6: Can add baggage only by itinerary (If user can add baggage but only for the
     * whole itinerary
     */
    CAN_ADD_BAGGAGE_TO_ITINERARY
  }

  /**
   * Custom comparator to order baggage conditions list using the segment index
   */
  static class BaggageSortCollection implements Comparator<BaggageConditionsDTO> {

    @Override public int compare(BaggageConditionsDTO lhs, BaggageConditionsDTO rhs) {
      return lhs.getSegmentTypeIndex().compareTo(rhs.getSegmentTypeIndex());
    }
  }
}
