package com.odigeo.app.android.view.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.Fragment;
import com.odigeo.app.android.view.constants.OneCMSKeys;

/**
 * Created by gastonmira on 14/10/16.
 */

public class CallTelephoneHelper {

  public static void callPhoneNumberInFragment(Context context, Fragment fragment,
      String phoneNumber) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (PermissionsHelper.askForPermissionIfNeededInFragment(Manifest.permission.CALL_PHONE,
          fragment, OneCMSKeys.PERMISSION_PHONE_CONTACTUS_MESSAGE,
          PermissionsHelper.PHONE_REQUEST_CODE)) {
        intentCall(phoneNumber, context);
      }
    } else {
      intentCall(phoneNumber, context);
    }
  }

  private static void intentCall(CharSequence phoneNumber, Context context) {
    Intent intent = new Intent(Intent.ACTION_CALL);
    intent.setData(Uri.parse("tel:" + phoneNumber));
    context.startActivity(intent);
  }

  public static void callPhoneNumberInActivity(Activity activity, String phoneNumber) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (PermissionsHelper.askForPermissionIfNeeded(Manifest.permission.CALL_PHONE, activity,
          OneCMSKeys.PERMISSION_PHONE_CONTACTUS_MESSAGE, PermissionsHelper.PHONE_REQUEST_CODE)) {
        intentCall(phoneNumber, activity);
      }
    } else {
      intentCall(phoneNumber, activity);
    }
  }
}
