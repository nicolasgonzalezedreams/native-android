package com.odigeo.app.android.lib.models.dto;

import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import com.odigeo.data.entity.BaseSpinnerItem;

/**
 * <p>Java class for residentGroup.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;simpleType name="residentGroup">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CANARIAS"/>
 *     &lt;enumeration value="BALEARES"/>
 *     &lt;enumeration value="CEUTA"/>
 *     &lt;enumeration value="MELILLA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
public enum ResidentGroupDTO implements BaseSpinnerItem {

  CANARIAS("mslresidentgroupcontainer_residentgroupnamecanaryislands"), BALEARES(
      "mslresidentgroupcontainer_residentgroupnamebalearicislands"), CEUTA(
      "mslresidentgroupcontainer_residentgroupnameceuta"), MELILLA(
      "mslresidentgroupcontainer_residentgroupnamemelilla");

  @StringRes private final String key;

  ResidentGroupDTO(String key) {
    this.key = key;
  }

  public static ResidentGroupDTO fromValue(String v) {
    return valueOf(v);
  }

  @Override public String getShownTextKey() {
    return key;
  }

  @Override public int getImageId() {
    return EMPTY_RESOURCE;
  }

  @Nullable @Override public String getShownText() {
    return null;
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public Object getData() {
    return null;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }
}
