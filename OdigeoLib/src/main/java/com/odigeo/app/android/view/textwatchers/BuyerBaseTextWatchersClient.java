package com.odigeo.app.android.view.textwatchers;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.textwatchers.OnFocusChange.BaseOnFocusChange;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.AddressValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.CPFValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.CityValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.MailValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.NameValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.PhoneNumberValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.StateValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.SurnameValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.ZipValidator;
import java.util.ArrayList;
import java.util.List;

public class BuyerBaseTextWatchersClient {

  private Context mContext;
  private TextInputLayout mTilName;
  private TextInputLayout mTilLastName;
  private TextInputLayout mTilIdentificatioNumber;
  private TextInputLayout mTilCpf;
  private TextInputLayout mTilAddress;
  private TextInputLayout mTilCityName;
  private TextInputLayout mTilStateName;
  private TextInputLayout mTilZipCode;
  private TextInputLayout mTilPhoneNumber;
  private TextInputLayout mTilAlternativePhoneNumber;
  private TextInputLayout mTilMail;
  private TextInputLayout mTilPhoneCode;
  private TextInputLayout mTilCountryCode;
  private List<TextInputLayout> mTilsBuyer = new ArrayList<>();

  public BuyerBaseTextWatchersClient(Context context, TextInputLayout name,
      TextInputLayout lastName, TextInputLayout identificationNumber, TextInputLayout cpf,
      TextInputLayout address, TextInputLayout cityName, TextInputLayout state,
      TextInputLayout zipcode, TextInputLayout phone, TextInputLayout alternativePhone,
      TextInputLayout mail, TextInputLayout phoneCode, TextInputLayout countryCode) {
    mContext = context;
    mTilName = name;
    mTilLastName = lastName;
    mTilIdentificatioNumber = identificationNumber;
    mTilCpf = cpf;
    mTilAddress = address;
    mTilCityName = cityName;
    mTilStateName = state;
    mTilZipCode = zipcode;
    mTilPhoneNumber = phone;
    mTilAlternativePhoneNumber = alternativePhone;
    mTilMail = mail;
    mTilPhoneCode = phoneCode;
    mTilCountryCode = countryCode;
    addTilsToList();
  }

  private void addTilsToList() {
    mTilsBuyer.add(mTilName);
    mTilsBuyer.add(mTilLastName);
    mTilsBuyer.add(mTilIdentificatioNumber);
    mTilsBuyer.add(mTilCpf);
    mTilsBuyer.add(mTilAddress);
    mTilsBuyer.add(mTilCityName);
    mTilsBuyer.add(mTilStateName);
    mTilsBuyer.add(mTilZipCode);
    mTilsBuyer.add(mTilPhoneNumber);
    mTilsBuyer.add(mTilAlternativePhoneNumber);
    mTilsBuyer.add(mTilMail);
    mTilsBuyer.add(mTilPhoneCode);
    mTilsBuyer.add(mTilCountryCode);
  }

  public void initBuyerOnFocusChange() {
    setNameOnFocusChange();
    setLastNameOnFocusChange();
    setCPFOnFocusChange();
    setAddressOnFocusChange();
    setZipOnFocusChange();
    setCityOnFocusChange();
    setStateOnFocusChange();
    setMailOnFocusChange();
    setPhoneOnFocusChange();
    setAlternativePhoneNumberOnFocusChange();
    setPhoneCodeTextWatcher();
    setCountryCodeTextWatcher();
  }

  private void setNameOnFocusChange() {
    if (mTilName.getEditText() != null) {
      mTilName.getEditText()
          .setOnFocusChangeListener(new BaseOnFocusChange(mContext, mTilName, new NameValidator(),
              OneCMSKeys.VALIDATION_ERROR_NAME));
      mTilName.getEditText().addTextChangedListener(new BaseTextWatcher(mTilName));
    }
  }

  private void setLastNameOnFocusChange() {
    if (mTilLastName.getEditText() != null) {
      mTilLastName.getEditText()
          .setOnFocusChangeListener(
              new BaseOnFocusChange(mContext, mTilLastName, new SurnameValidator(),
                  OneCMSKeys.VALIDATION_ERROR_SURNAME));
      mTilLastName.getEditText().addTextChangedListener(new BaseTextWatcher(mTilLastName));
    }
  }

  private void setCPFOnFocusChange() {
    if (mTilCpf.getEditText() != null) {
      mTilCpf.getEditText()
          .setOnFocusChangeListener(new BaseOnFocusChange(mContext, mTilCpf, new CPFValidator(),
              OneCMSKeys.VALIDATION_ERROR_CPF));
      mTilCpf.getEditText().addTextChangedListener(new BaseTextWatcher(mTilCpf));
    }
  }

  private void setAddressOnFocusChange() {
    if (mTilAddress.getEditText() != null) {
      mTilAddress.getEditText()
          .setOnFocusChangeListener(
              new BaseOnFocusChange(mContext, mTilAddress, new AddressValidator(),
                  OneCMSKeys.VALIDATION_ERROR_ADDRESS));
      mTilAddress.getEditText().addTextChangedListener(new BaseTextWatcher(mTilAddress));
    }
  }

  private void setCityOnFocusChange() {
    if (mTilCityName.getEditText() != null) {
      mTilCityName.getEditText()
          .setOnFocusChangeListener(
              new BaseOnFocusChange(mContext, mTilCityName, new CityValidator(),
                  OneCMSKeys.VALIDATION_ERROR_CITY_NAME));
      mTilCityName.getEditText().addTextChangedListener(new BaseTextWatcher(mTilCityName));
    }
  }

  private void setStateOnFocusChange() {
    if (mTilStateName.getEditText() != null) {
      mTilStateName.getEditText()
          .setOnFocusChangeListener(
              new BaseOnFocusChange(mContext, mTilStateName, new StateValidator(),
                  OneCMSKeys.VALIDATION_ERROR_STATE_NAME));
      mTilStateName.getEditText().addTextChangedListener(new BaseTextWatcher(mTilStateName));
    }
  }

  private void setZipOnFocusChange() {
    if (mTilZipCode.getEditText() != null) {
      mTilZipCode.getEditText()
          .setOnFocusChangeListener(new BaseOnFocusChange(mContext, mTilZipCode, new ZipValidator(),
              OneCMSKeys.VALIDATION_ERROR_POSTAL_CODE));
      mTilZipCode.getEditText().addTextChangedListener(new BaseTextWatcher(mTilZipCode));
    }
  }

  private void setMailOnFocusChange() {
    if (mTilMail.getEditText() != null) {
      mTilMail.getEditText()
          .setOnFocusChangeListener(new BaseOnFocusChange(mContext, mTilMail, new MailValidator(),
              OneCMSKeys.VALIDATION_ERROR_MAIL));
      mTilMail.getEditText().addTextChangedListener(new BaseTextWatcher(mTilMail));
    }
  }

  private void setPhoneOnFocusChange() {
    if (mTilPhoneNumber.getEditText() != null) {
      mTilPhoneNumber.getEditText()
          .setOnFocusChangeListener(
              new BaseOnFocusChange(mContext, mTilPhoneNumber, new PhoneNumberValidator(),
                  OneCMSKeys.VALIDATION_ERROR_PHONE_NUMBER));
      mTilPhoneNumber.getEditText().addTextChangedListener(new BaseTextWatcher(mTilPhoneNumber));
    }
  }

  private void setAlternativePhoneNumberOnFocusChange() {
    if (mTilAlternativePhoneNumber.getEditText() != null) {
      mTilAlternativePhoneNumber.getEditText()
          .setOnFocusChangeListener(new BaseOnFocusChange(mContext, mTilAlternativePhoneNumber,
              new PhoneNumberValidator(), OneCMSKeys.VALIDATION_ERROR_PHONE_NUMBER));
      mTilAlternativePhoneNumber.getEditText()
          .addTextChangedListener(new BaseTextWatcher(mTilAlternativePhoneNumber));
    }
  }

  private void setPhoneCodeTextWatcher() {
    if (mTilPhoneCode.getEditText() != null) {
      mTilPhoneCode.getEditText().addTextChangedListener(new BaseTextWatcher(mTilPhoneCode));
    }
  }

  private void setCountryCodeTextWatcher() {
    if (mTilCountryCode.getEditText() != null) {
      mTilCountryCode.getEditText().addTextChangedListener(new BaseTextWatcher(mTilCountryCode));
    }
  }

  public void setNullErrorInTextWatchers() {
    mTilName.setError(null);
    mTilLastName.setError(null);
    mTilIdentificatioNumber.setError(null);
    mTilCpf.setError(null);
    mTilAddress.setError(null);
    mTilCityName.setError(null);
    mTilStateName.setError(null);
    mTilZipCode.setError(null);
    mTilPhoneNumber.setError(null);
    mTilAlternativePhoneNumber.setError(null);
    mTilMail.setError(null);

    mTilName.setErrorEnabled(false);
    mTilLastName.setErrorEnabled(false);
    mTilIdentificatioNumber.setErrorEnabled(false);
    mTilCpf.setErrorEnabled(false);
    mTilAddress.setErrorEnabled(false);
    mTilCityName.setErrorEnabled(false);
    mTilStateName.setErrorEnabled(false);
    mTilZipCode.setErrorEnabled(false);
    mTilPhoneNumber.setErrorEnabled(false);
    mTilAlternativePhoneNumber.setErrorEnabled(false);
    mTilMail.setErrorEnabled(false);
  }

  @Nullable public TextInputLayout getTextInputLayoutWithError() {
    validateIdentificationInCaseSpinnerChangeAndHaventFocusOut();
    for (TextInputLayout textInputLayout : mTilsBuyer) {
      if (isThereErrorInTextInputLayout(textInputLayout)) {
        haveToSetErrorInUnfocousableTextInputLayout(textInputLayout);
        return textInputLayout;
      }
    }
    return null;
  }

  private boolean isThereErrorInTextInputLayout(TextInputLayout textInputLayout) {
    return ((textInputLayout.getEditText() != null) && (textInputLayout.getVisibility()
        == View.VISIBLE) && ((!TextUtils.isEmpty(textInputLayout.getError()))
        || (textInputLayout.getEditText().getText().toString().isEmpty())));
  }

  public void updateTextInputLayoutError(TextInputLayout textInputLayoutFocused) {
    for (TextInputLayout textinputLayout : mTilsBuyer) {
      if ((textinputLayout.equals(textInputLayoutFocused)) && (textInputLayoutFocused.getEditText()
          != null)) {
        validateField(textInputLayoutFocused);
      }
    }
  }

  private void haveToSetErrorInUnfocousableTextInputLayout(TextInputLayout textInputLayout) {
    if (textInputLayout.equals(mTilPhoneCode)) {
      setPhoneCodeError();
    } else if (textInputLayout.equals(mTilCountryCode)) {
      setCountryCodeError();
    } else if ((textInputLayout.getEditText() != null) && (!textInputLayout.equals(
        mTilIdentificatioNumber))) {
      validateField(textInputLayout);
    }
  }

  private void setPhoneCodeError() {
    if (mTilPhoneCode.getEditText() != null) {
      String oneCMSError = LocalizablesFacade.getString(mTilPhoneCode.getContext(),
          OneCMSKeys.VALIDATION_ERROR_PHONE_CODE).toString();
      mTilPhoneCode.setError(oneCMSError);
      mTilPhoneCode.setErrorEnabled(true);
    }
  }

  private void setCountryCodeError() {
    if (mTilCountryCode.getEditText() != null) {
      String oneCMSError = LocalizablesFacade.getString(mTilPhoneCode.getContext(),
          OneCMSKeys.VALIDATION_ERROR_COUNTRY_CODE).toString();
      mTilCountryCode.setError(oneCMSError);
      mTilCountryCode.setErrorEnabled(true);
    }
  }

  public void updateIdentificationAfterChangeInSpinner(TextInputLayout textInputLayout) {
    int identificationIndext = mTilsBuyer.indexOf(mTilIdentificatioNumber);
    if (identificationIndext != -1) {
      mTilsBuyer.set(identificationIndext, textInputLayout);
      mTilIdentificatioNumber = textInputLayout;
    }
  }

  private void validateField(TextInputLayout textInputLayout) {
    if (textInputLayout.getEditText() != null) {
      BaseOnFocusChange baseOnFocusChange =
          (BaseOnFocusChange) textInputLayout.getEditText().getOnFocusChangeListener();
      baseOnFocusChange.validateField(textInputLayout.getEditText().getEditableText());
    }
  }

  private void validateIdentificationInCaseSpinnerChangeAndHaventFocusOut() {
    if (mTilIdentificatioNumber.getVisibility() == View.VISIBLE) {
      validateField(mTilIdentificatioNumber);
    }
  }
}
