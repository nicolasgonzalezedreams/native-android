package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;
import java.util.List;

public class BazaarvoiceReviewDTO {

  protected List<BazaarvoiceProductReviewDTO> productReviews;
  protected List<BazaarvoiceRatingValueReviewDTO> ratingValueReviews;
  protected int totalReviewCount;
  protected BigDecimal averageOverallRating;

  /**
   * Gets the value of the totalReviewCount property.
   */
  public int getTotalReviewCount() {
    return totalReviewCount;
  }

  /**
   * Sets the value of the totalReviewCount property.
   */
  public void setTotalReviewCount(int value) {
    this.totalReviewCount = value;
  }

  /**
   * Gets the value of the averageOverallRating property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getAverageOverallRating() {
    return averageOverallRating;
  }

  /**
   * Sets the value of the averageOverallRating property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setAverageOverallRating(BigDecimal value) {
    this.averageOverallRating = value;
  }

  /**
   * @return the productReviews
   */
  public List<BazaarvoiceProductReviewDTO> getProductReviews() {
    return productReviews;
  }

  /**
   * @param productReviews the productReviews to set
   */
  public void setProductReviews(List<BazaarvoiceProductReviewDTO> productReviews) {
    this.productReviews = productReviews;
  }

  /**
   * @return the ratingValueReviews
   */
  public List<BazaarvoiceRatingValueReviewDTO> getRatingValueReviews() {
    return ratingValueReviews;
  }

  /**
   * @param ratingValueReviews the ratingValueReviews to set
   */
  public void setRatingValueReviews(List<BazaarvoiceRatingValueReviewDTO> ratingValueReviews) {
    this.ratingValueReviews = ratingValueReviews;
  }
}
