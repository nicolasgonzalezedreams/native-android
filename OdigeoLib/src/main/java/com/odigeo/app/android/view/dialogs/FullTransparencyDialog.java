package com.odigeo.app.android.view.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.fragments.OdigeoResultsFragment;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.tracker.TrackerControllerInterface;

@Deprecated public class FullTransparencyDialog extends DialogFragment {

  private static final String TITLE = "title";
  private static final String MESSAGE = "message";
  private static final String BUTTON = "button";
  private String mTitle;
  private String mMessage;
  private String mButton;
  private PreferencesControllerInterface mPreferencesController;
  private TrackerControllerInterface mTrackerController;
  private NoticeDialogListener mListener;
  private Button mButtonClose;
  private static final String FULL_TRANSPARENCY_DIALOG = "full_transparency_dialog";

  private static FullTransparencyDialog newInstance(String title, String message, String button) {
    Bundle args = new Bundle();
    args.putString(TITLE, title);
    args.putString(MESSAGE, message);
    args.putString(BUTTON, button);
    FullTransparencyDialog fragment = new FullTransparencyDialog();
    fragment.setArguments(args);
    return fragment;
  }

  @Nullable @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View root = inflater.inflate(R.layout.dialog_full_transparency, container, false);
    getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    extractArguments();
    initComponent(root);
    setListeners();
    return root;
  }

  private void initComponent(View root) {
    mPreferencesController = AndroidDependencyInjector.getInstance().providePreferencesController();
    mTrackerController = AndroidDependencyInjector.getInstance().provideTrackerController();
    TextView title = ((TextView) root.findViewById(R.id.title_full_transparency));
    TextView message = ((TextView) root.findViewById(R.id.message_full_transparency));
    mButtonClose = (Button) root.findViewById(R.id.button_full_transparency);

    mButtonClose.setText(mButton);
    setCancelable(true);
    title.setText(mTitle);
    message.setText(mMessage);
  }

  private void setListeners() {
    mButtonClose.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPreferencesController.saveBooleanValue(OdigeoResultsFragment.SHOW_FULL_TRANSPARENCY_DIALOG,
            false);
        mListener.onDialogPositiveClick(FullTransparencyDialog.this);
        mTrackerController.trackFullTransparencyDialogAppearance(false);
        dismiss();
      }
    });
  }

  private void extractArguments() {
    Bundle bundle = getArguments();
    mTitle = bundle.getString(TITLE);
    mMessage = bundle.getString(MESSAGE);
    mButton = bundle.getString(BUTTON);
  }

  public void setListener(NoticeDialogListener listener) {
    mListener = listener;
  }

  public interface NoticeDialogListener {

    void onDialogPositiveClick(DialogFragment dialog);
  }

  public static class Builder {

    private FragmentManager mFragmentManager;
    private String mTitle;
    private String mMessage;
    private String mButton;

    public Builder(FragmentManager fragmentManager) {
      this.mFragmentManager = fragmentManager;
    }

    public Builder setTitleResource(String title) {
      mTitle = title;
      return this;
    }

    public Builder setMessageResource(String message) {
      mMessage = message;
      return this;
    }

    public Builder setButtonResource(String button) {
      mButton = button;
      return this;
    }

    public FullTransparencyDialog build(NoticeDialogListener listener) {
      FullTransparencyDialog dialog = newInstance(mTitle, mMessage, mButton);
      dialog.setListener(listener);
      dialog.show(mFragmentManager, FULL_TRANSPARENCY_DIALOG);
      return dialog;
    }
  }
}
