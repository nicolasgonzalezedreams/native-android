package com.odigeo.app.android.lib.models.dto;

public enum AdditionalProductTypeDTO {

  HOTEL, SMS, INSURANCE, SERVICE_PACK, PARKING, OTHER;

  public static AdditionalProductTypeDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
