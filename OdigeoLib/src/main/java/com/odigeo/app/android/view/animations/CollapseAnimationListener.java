package com.odigeo.app.android.view.animations;

import android.view.View;
import android.view.animation.Animation;
import android.widget.ScrollView;

public class CollapseAnimationListener implements Animation.AnimationListener {

  private static long DELAY_TIME = 100;

  View mViewGoToChild;
  View mViewGoToFocus;
  ScrollView mScroll;

  public CollapseAnimationListener(View viewGoToChild, View goToViewFocus, ScrollView scroll) {
    mViewGoToChild = viewGoToChild;
    mViewGoToFocus = goToViewFocus;
    mScroll = scroll;
  }

  @Override public void onAnimationStart(Animation animation) {

  }

  @Override public void onAnimationEnd(Animation animation) {
    mScroll.postDelayed(new Runnable() {
      @Override public void run() {
        mScroll.requestChildFocus(mViewGoToChild, mViewGoToFocus);
      }
    }, DELAY_TIME);
  }

  @Override public void onAnimationRepeat(Animation animation) {

  }
}
