package com.odigeo.app.android.view;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.base.BlackDialog;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.MyTripsManualImportPresenter;
import com.odigeo.presenter.contracts.navigators.MyTripsNavigatorInterface;
import com.odigeo.presenter.contracts.views.MyTripsManualImportViewInterface;

import static com.odigeo.dataodigeo.tracker.TrackerConstants.IMPORT_TRIP_EVENT;

public class MyTripsManualImportView extends BaseView<MyTripsManualImportPresenter>
    implements MyTripsManualImportViewInterface {

  public static final int BLACK_DIALOG_LIFETIME = 2500;

  private EditText mEtEmail;
  private EditText mEtBookingId;

  private TextInputLayout mTilEmail;
  private TextInputLayout mTilBookingId;

  private Button mBtnAdd;

  private TextWatcher mOnTextChanged;
  private BlackDialog mFlightsDialog;
  private TextView mTvTitle;
  private TextView mTvDescription;

  public static MyTripsManualImportView newInstance() {
    return new MyTripsManualImportView();
  }

  @Override protected MyTripsManualImportPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideMyTripsManualImportPresenter(this, (MyTripsNavigatorInterface) getActivity());
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    menu.clear();
  }

  @Override protected int getFragmentLayout() {
    return R.layout.activity_import_trip;
  }

  @Override protected void initComponent(View view) {

    mTvTitle = (TextView) view.findViewById(R.id.tvTitle);
    mTvDescription = (TextView) view.findViewById(R.id.tvDescription);

    mEtEmail = (EditText) view.findViewById(R.id.importTripEmail);
    mEtBookingId = (EditText) view.findViewById(R.id.importTripBookingId);

    mTilEmail = (TextInputLayout) view.findViewById(R.id.tilEmail);
    mTilBookingId = (TextInputLayout) view.findViewById(R.id.tilBookingId);

    mBtnAdd = (Button) view.findViewById(R.id.button_add_to_my_trip);

    mOnTextChanged = new TextWatcher() {
      @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
        mPresenter.validateEmailAndBookingIdFormat(mEtEmail.getText().toString(),
            mEtBookingId.getText().toString());
      }

      @Override public void afterTextChanged(Editable s) {

      }
    };
  }

  @Override protected void setListeners() {

    mBtnAdd.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mTracker.trackMyTripsManualImportViewPage(IMPORT_TRIP_EVENT,
            TrackerConstants.CATEGORY_MY_TRIPS, TrackerConstants.ACTION_ADD_TRIP,
            TrackerConstants.LABEL_ADD_BOOKING_TO_MY_TRIPS_TAPS);

        final String email = mEtEmail.getText().toString().trim();
        final String bookingId = mEtBookingId.getText().toString().trim();
        mPresenter.importTrip(email, bookingId);
      }
    });

    mEtEmail.addTextChangedListener(mOnTextChanged);
    mEtBookingId.addTextChangedListener(mOnTextChanged);

    mEtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
          mPresenter.checkErrorEmail(mEtEmail.getText().toString());
        } else {
          mTilEmail.setErrorEnabled(false);
        }
      }
    });

    mEtBookingId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
          mEtBookingId.setText(mEtBookingId.getText().toString().toUpperCase());
          mPresenter.checkErrorBookingId(mEtBookingId.getText().toString());
        } else {
          mTilBookingId.setErrorEnabled(false);
        }
      }
    });
  }

  @Override public void onResume() {
    super.onResume();

    mPresenter.init();
  }

  @Override protected void initOneCMSText(View view) {
    mTvTitle.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.MY_TRIPS_IMPORT_MANUAL_TITLE));
    mTvDescription.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.MY_TRIPS_IMPORT_MANUAL_DESCRIPTION,
            Configuration.getInstance().getBrandVisualName()));
    mTilEmail.setHint(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.MY_TRIPS_IMPORT_MANUAL_EMAIL_HINT));
    mTilBookingId.setHint(LocalizablesFacade.getString(getContext(),
        OneCMSKeys.MY_TRIPS_IMPORT_MANUAL_BOOKING_ID_HINT));
    mBtnAdd.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.MY_TRIPS_IMPORT_MANUAL_BUTTON)

    );
  }

  @Override public void enableAddBookingButton(boolean isEnabled) {
    mBtnAdd.setEnabled(isEnabled);
  }

  @Override public void showEmailError(boolean showError) {
    if (showError) {
      mTilEmail.setError(LocalizablesFacade.getString(getContext(),
          OneCMSKeys.MY_TRIPS_IMPORT_ERROR_EMAIL_VALIDATION));
    }

    mTilEmail.setErrorEnabled(showError);
  }

  @Override public void showBookingIdError(boolean showError) {
    if (showError) {
      mTilBookingId.setError(LocalizablesFacade.getString(getContext(),
          OneCMSKeys.MY_TRIPS_IMPORT_ERROR_BOOKING_ID_VALIDATION));
    }

    mTilBookingId.setErrorEnabled(showError);
  }

  @Override public void showImportFail() {
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS,
        TrackerConstants.ACTION_ADD_TRIP, TrackerConstants.LABEL_ADD_BOOKING_TO_MY_TRIPS_FAILURE);

    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setCancelable(true);
    builder.setTitle(android.R.string.dialog_alert_title)
        .setMessage(
            LocalizablesFacade.getString(getContext(), OneCMSKeys.MY_TRIPS_IMPORT_ERROR).toString())
        .setPositiveButton(LocalizablesFacade.getString(getContext(), OneCMSKeys.COMMON_OK), null);
    builder.create().show();
  }

  @Override public void showImportSuccess(final Booking booking) {

    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS,
        TrackerConstants.ACTION_ADD_TRIP, TrackerConstants.LABEL_ADD_BOOKING_TO_MY_TRIPS_SUCCESS);

    showBlackDialog(OneCMSKeys.IMPORT_TRIP_OK, booking.getArrivalCityName(0));
    goBack();
  }

  @Override public void showAlreadyImported(final Booking booking) {

    showBlackDialog(OneCMSKeys.IMPORT_TRIP_ALREADY, booking.getArrivalCityName(0));
    goBack();
  }

  private void goBack() {
    if (isResumed()) {
      getActivity().onBackPressed();
    }
  }

  private void showBlackDialog(String title, String text) {
    if (isResumed()) {
      BlackDialog successDialog = new BlackDialog(getActivity(), R.drawable.trip_imported);
      successDialog.show(LocalizablesFacade.getString(getContext(), title, text),
          BLACK_DIALOG_LIFETIME);
    }
  }

  @Override public void showLoading() {
    mFlightsDialog = new BlackDialog(getActivity(), true);
    mFlightsDialog.show(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.MY_TRIPS_IMPORT_FINFING_BOOKING)
            .toString());
  }

  @Override public void hideLoading() {
    if (mFlightsDialog != null && mFlightsDialog.isShowing()) {
      mFlightsDialog.dismiss();
    }
  }

  @Override public boolean isViewVisible() {
    return isResumed();
  }
}
