package com.odigeo.app.android.lib.ui.widgets.base;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ScrollView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;

/**
 * Created by manuel on 02/10/14.
 */
@Deprecated public abstract class CustomField extends PressableWidget {

  private boolean mandatory;
  private String errorText;
  private int validationFormat;
  private CustomFieldListener customFieldListener;

  public CustomField(Context context) {
    super(context);
  }

  public CustomField(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray aAttrs = context.obtainStyledAttributes(attrs, R.styleable.CustomField, 0, 0);
    mandatory = aAttrs.getBoolean(R.styleable.CustomField_isMandatory, false);
    String errorKey = aAttrs.getString(R.styleable.CustomField_errorMessage);
    errorText = errorKey != null ? LocalizablesFacade.getString(context, errorKey).toString() : "";
    setValidationFormat(aAttrs.getInteger(R.styleable.CustomField_fieldValidationFormat, 0));

    aAttrs.recycle();
  }

  public abstract boolean validate();

  public abstract boolean isValid();

  public abstract void clearValidation();

  public final boolean isMandatory() {
    return mandatory;
  }

  public final void setMandatory(boolean mandatory) {
    this.mandatory = mandatory;
  }

  public final CharSequence getErrorText() {
    return errorText;
  }

  public final void setErrorText(String errorText) {
    this.errorText = errorText;
  }

  /**
   * Shows an error on the custom field, this method should be override if the custom field can
   * show an inline error
   */
  public void showError() {
    //If the field can show an error override this method
  }

  /**
   * Hide the error on the custom field, if the method can show an error it should have a method
   * to hide it, so override this method for hiding this error
   */
  public void hideError() {
    //If the field can show an error override this method for hide it
  }

  public final CustomFieldListener getCustomFieldListener() {
    return customFieldListener;
  }

  public final void setCustomFieldListener(CustomFieldListener listener) {
    this.customFieldListener = listener;
  }

  public final int getValidationFormat() {
    return validationFormat;
  }

  public final void setValidationFormat(int validationFormat) {
    this.validationFormat = validationFormat;
  }

  /*
      Pos[0]; x
      Pos[1]; y
   */
  public final int[] getCoordinates() {

    int[] coords = new int[2];

    try {
      this.getLocationOnScreen(coords);
    } catch (Exception error) {

      Log.d(Constants.TAG_LOG, "ERROR ON getCoordinates - CustomField:" + error.getMessage());
      return null;
    }

    return coords;
  }

  public final void setFocusOnItem(ScrollView scroll) {

    int[] coords = getCoordinates();

    if (coords != null) {
      scroll.scrollTo(coords[0], coords[1]);
    }
  }

  public interface CustomFieldListener {
    void onFieldValueChanged(CustomField field, Object newValue);
  }
}
