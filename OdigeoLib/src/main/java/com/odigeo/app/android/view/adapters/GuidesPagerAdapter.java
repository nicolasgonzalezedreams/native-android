package com.odigeo.app.android.view.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.view.TouchImageView;

/**
 * Created by matias.dirusso on 15/12/2015.
 */
public class GuidesPagerAdapter extends PagerAdapter {

  private PdfRenderer mRenderer;
  private Fragment mFragment;
  private int mMaxPdfs;

  public GuidesPagerAdapter(PdfRenderer renderer, Fragment fragment, int maxPdfs) {
    mRenderer = renderer;
    mFragment = fragment;
    mMaxPdfs = maxPdfs;
  }

  @Override public int getCount() {
    return mMaxPdfs;
  }

  @Override public boolean isViewFromObject(View view, Object object) {
    return view == object;
  }

  @Override public Object instantiateItem(ViewGroup container, int position) {
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

      LayoutInflater inflater = (LayoutInflater) mFragment.getActivity()
          .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
      View view = inflater.inflate(R.layout.my_trip_arrival_guide_screen, container, false);

      TouchImageView imageView = (TouchImageView) view.findViewById(R.id.pdfImage);

      PdfRenderer.Page pdFRendererPage = mRenderer.openPage(position);

      Bitmap bitmap =
          Bitmap.createBitmap(2 * pdFRendererPage.getWidth(), 2 * pdFRendererPage.getHeight(),
              Bitmap.Config.ARGB_8888);

      pdFRendererPage.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
      imageView.setImageBitmap(bitmap);
      imageView.invalidate();

      pdFRendererPage.close();
      container.addView(view);
      return view;
    }
    return null;
  }

  @Override public void destroyItem(ViewGroup container, int position, Object object) {
    container.removeView((LinearLayout) object);
  }
}
