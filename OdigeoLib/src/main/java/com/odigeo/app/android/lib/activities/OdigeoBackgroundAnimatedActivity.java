package com.odigeo.app.android.lib.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.tracker.TuneTrackerInterface;

/**
 * Main Activity for the Odigeo application, implements the Animated background and the repricing
 * message.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 2.2.3
 * @since 18/11/2014
 */
public abstract class OdigeoBackgroundAnimatedActivity extends OdigeoForegroundBaseActivity {
  private static final long REPRICING_TIME = 6000;
  private final Handler handlerRepricing = new Handler();
  protected TuneTrackerInterface mTuneTracker;
  /**
   * Price of Tickets, before any transaction in a last activity, this value is recovered from a
   * Intent
   */
  protected double lastTicketsPrice;
  /**
   * Price of Insurances, before any transaction in a last activity, this value is recovered from a
   * Intent
   */
  protected double lastInsurancesPrice;
  private View widening;
  private final Runnable runnableRepricing = new Runnable() {
    @Override public void run() {
      widening.setVisibility(View.GONE);
    }
  };
  private TextView mTvWidening;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    boolean wasAppInstalled = PreferencesManager.wasAppInstalled(this);
    Log.d(Constants.TAG_LOG, "wasAppInstalled=" + wasAppInstalled);
    PreferencesManager.setWasAppInstalled(this, true);
    mTuneTracker = AndroidDependencyInjector.getInstance().provideTuneTracker();
    mTuneTracker.setExistingUser(wasAppInstalled);
  }

  @Override public void onResume() {
    super.onResume();
    if (getActivityTitle() != null) {
      getSupportActionBar().setTitle(getActivityTitle());
    }
  }

  @Override protected void onPause() {
    super.onPause();
  }

  @Override public void onStart() {
    super.onStart();
    if (((OdigeoApp) getApplication()).hasAnimatedBackground()) {
      ViewUtils.loadAnimate(findViewById(R.id.image_nube_top), R.anim.translate_nube_top);
      ViewUtils.loadAnimate(findViewById(R.id.image_nube_botton), R.anim.translate_nube_bottom);
    }
  }

  @Override public void setContentView(int layoutResID) {
    //Inflate the default layout.
    super.setContentView(R.layout.activity_odigeo);
    //Get the container for all the rest views.
    FrameLayout root = ((FrameLayout) findViewById(R.id.frl_root));
    widening = findViewById(R.id.widening);
    mTvWidening = ((TextView) findViewById(R.id.tvWideningLeyend));
    hideRepricingMessage();
    //Add the others views.
    getLayoutInflater().inflate(layoutResID, root);
  }

  @Override public void onStop() {
    super.onStop();
  }

  @Override protected void onDestroy() {
    handlerRepricing.removeCallbacks(runnableRepricing);
    super.onDestroy();
  }

  public String getActivityTitle() {
    return null;
  }

  public boolean hasRepricingTickets(double currentTicketsPrices, String shoppingCartLocale) {
    lastTicketsPrice = getIntent().
        getDoubleExtra(Constants.EXTRA_REPRICING_TICKETS_PRICES, currentTicketsPrices);
    double diff = currentTicketsPrices - lastTicketsPrice;
    if (diff != 0) {
      showRepricingMessage(diff, shoppingCartLocale);
      lastTicketsPrice = currentTicketsPrices;
      return true;
    }
    return false;
  }

  public boolean hasRepricingInsurances(double currentInsurancesPrices, String shoppingCartLocale) {
    if (getIntent().hasExtra(Constants.EXTRA_REPRICING_INSURANCES)) {
      lastInsurancesPrice =
          getIntent().getDoubleExtra(Constants.EXTRA_REPRICING_INSURANCES, currentInsurancesPrices);
      double diff = currentInsurancesPrices - lastInsurancesPrice;
      if (diff != 0) {
        showRepricingInsuranceMessage(diff, shoppingCartLocale);
      }
    }
    return false;
  }

  public void showRepricingMessage(double difference, @NonNull String locale) {
    CharSequence stringFormatted = "";
    String message;
    if (difference < 0) {
      message = LocaleUtils.getLocalizedCurrencyValue(-difference, locale);
      stringFormatted = LocalizablesFacade.getString(this,
          OneCMSKeys.REPRICINGWIDENING_FLIGHTREPRICING_MESSAGELOWER, message);
    } else if (difference > 0) {
      message = LocaleUtils.getLocalizedCurrencyValue(difference, locale);
      stringFormatted = LocalizablesFacade.getString(this,
          OneCMSKeys.REPRICINGWIDENING_FLIGHTREPRICING_MESSAGEHIGHER, message);
    }
    mTvWidening.setText(stringFormatted);
    widening.setVisibility(View.VISIBLE);
    handlerRepricing.postDelayed(runnableRepricing, REPRICING_TIME);
  }

  public void showRepricingInsuranceMessage(double difference, @NonNull String locale) {
    String message = LocaleUtils.getLocalizedCurrencyValue(difference, locale);
    mTvWidening.setText(
        LocalizablesFacade.getString(this, OneCMSKeys.REPRICINGWIDENING_BAGGAGEREPRICING_MESSAGE,
            message));
    widening.setVisibility(View.VISIBLE);
    handlerRepricing.postDelayed(runnableRepricing, REPRICING_TIME);
  }

  public void showWideningMessage(@NonNull String message) {
    mTvWidening.setText(message);
    widening.setVisibility(View.VISIBLE);
    handlerRepricing.postDelayed(runnableRepricing, REPRICING_TIME);
  }

  public void hideRepricingMessage() {
    widening.setVisibility(View.GONE);
  }
}
