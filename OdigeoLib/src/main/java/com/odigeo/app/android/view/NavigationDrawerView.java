package com.odigeo.app.android.view;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.app.android.navigator.NavigationDrawerNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.interfaces.ListenerUpdateCarousel;
import com.odigeo.dataodigeo.net.controllers.FacebookController;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.NavigationDrawerPresenter;
import com.odigeo.presenter.contracts.navigators.NavigationDrawerNavigatorInterface;
import com.odigeo.presenter.contracts.views.NavigationDrawerViewInterface;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LOGOUT_TAP_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.MY_DATA_HOME_TAP_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.MY_TRIPS_HOME_EVENT;

public class NavigationDrawerView extends BaseView<NavigationDrawerPresenter>
    implements NavigationDrawerViewInterface {

  private static final int DRAWER_CLOSE_DELAY_MS = 250;
  private static final int POSITION_ITEM_LOGIN_REGISTER = 0;
  private static final int POSITION_ITEM_LOGOUT = 1;
  private final Handler mDrawerActionHandler = new Handler();
  private NavigationView mNavigationView;
  private View mDrawerHeader;
  private DrawerLayout mDrawer;
  private TextView mUserNameTextView;
  private TextView mUserEmailTextView;
  private ImageView mUserProfileImageView;
  private int mIdBackground;
  private int mIdUserImageProfile;
  private String mUrlUserImageProfile;
  private boolean mDefaultMenuPresent = true;
  private ImageView mArrowImageView;
  private TextView mEmailToValidate;
  private RelativeLayout mTappingLayout;
  private ListenerUpdateCarousel mCallbackUpdateCarousel;
  private OdigeoImageLoader imageLoader;
  private AlertDialog mLogoutConfirmationDialog;
  private NavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
      new NavigationView.OnNavigationItemSelectedListener() {

        @Override public boolean onNavigationItemSelected(final MenuItem menuItemNavigationDrawer) {
          mDrawerActionHandler.postDelayed(new Runnable() {
            @Override public void run() {
              handleItemSelected(menuItemNavigationDrawer.getItemId());
            }
          }, DRAWER_CLOSE_DELAY_MS);
          return true;
        }
      };

  public static NavigationDrawerView newInstance() {
    return new NavigationDrawerView();
  }

  @Override public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    setUpDrawerLayout();
  }

  @Override public void onResume() {
    super.onResume();
    mPresenter.checkUserSession();
    mPresenter.checkLogoutWasMade();
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    mCallbackUpdateCarousel = (ListenerUpdateCarousel) context;
  }

  @Override public void onStop() {
    super.onStop();
    if (mLogoutConfirmationDialog != null) {
      mLogoutConfirmationDialog.dismiss();
    }
  }

  @Override public void onDetach() {
    mCallbackUpdateCarousel = null;
    super.onDetach();
  }

  @Override public void refreshMenu() {
    mDefaultMenuPresent = !mDefaultMenuPresent;
    if (!mDefaultMenuPresent) {
      loadMenuOptionsAccountPreferences();
      return;
    }
    loadMenuUserLogged();
  }

  @Override
  public void refreshNavigationDrawer(int resourceDrawable, String firstText, String secondText) {
    mPresenter.refreshUserNameTextView(firstText);
    mUserEmailTextView.setText(secondText);
    if (resourceDrawable != 0) {
      mArrowImageView.setImageResource(resourceDrawable);
      mArrowImageView.setVisibility(VISIBLE);
    } else {
      mArrowImageView.setVisibility(GONE);
    }

    changeNavigationDrawerBackgroundHeader();
  }

  @Override public void refreshNavigationDrawerUserLogged() {
    String firstText = mPresenter.getUserName();
    String secondText = mPresenter.getUserEmailFromCredentials();

    mIdBackground = R.drawable.navigation_header_background;
    mIdUserImageProfile = R.drawable.ic_account;
    mUrlUserImageProfile = mPresenter.getUrlUserImageProfile();

    loadMenuUserLogged();

    refreshNavigationDrawer(R.drawable.ic_arrow_drop_down, firstText, secondText);
    updateEmailToValidate(false, "");

    mUserNameTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
        getResources().getDimensionPixelSize(R.dimen.size_font_L));
    mUserNameTextView.setTypeface(null, Typeface.BOLD);
    mUserEmailTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
        getResources().getDimensionPixelSize(R.dimen.size_font_M));

    loadCircularUserProfileImage();
  }

  @Override public void refreshNavigationDrawerUserUnLogged() {
    if (getActivity() != null) {
      String firstText =
          LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ND_WHY_TO_LOGIN).toString();
      String secondText =
          LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ND_SAVE_RECENT_SEARCH)
              .toString();
      mIdBackground = R.drawable.navigation_header_background;
      mIdUserImageProfile = R.drawable.ic_restore;
      mUrlUserImageProfile = null;
      loadMenuNotSigned();
      updateEmailToValidate(false, "");
      refreshNavigationDrawer(0, firstText, secondText);
      mNavigationView.getMenu().getItem(POSITION_ITEM_LOGIN_REGISTER).setChecked(true);
      mUserNameTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
          getResources().getDimensionPixelSize(R.dimen.size_font_L));
      mUserNameTextView.setTypeface(null, Typeface.NORMAL);
      mUserEmailTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
          getResources().getDimensionPixelSize(R.dimen.size_font_M));
      mUserEmailTextView.setTypeface(null, Typeface.BOLD);
      loadCircularUserProfileImage();

      //TODO: Esto es una chapuza, es necesario refactorizar el login con redes sociales
      FacebookController facebookController =
          AndroidDependencyInjector.getInstance().provideFacebookHelper(this, null);
      facebookController.logout();
    }
  }

  @Override public void refreshNavigationDrawerUserInactive(String userEmail) {

    mIdBackground = R.drawable.navigation_header_background;
    mIdUserImageProfile = R.drawable.drawer_icon_latest;
    mUrlUserImageProfile = null;

    loadMenuNotSigned();

    updateEmailToValidate(true, userEmail);

    String firstText = LocalizablesFacade.getString(getActivity(),
        OneCMSKeys.SSO_MYINFO_VALIDATE_YOUR_ACCOUNT_TITLE).toString();
    String secondText =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_MYINFO_VALIDATE_ACCOUNT_MSG,
            Configuration.getInstance().getBrandVisualName()).toString();

    refreshNavigationDrawer(0, firstText, secondText);

    mNavigationView.getMenu().getItem(POSITION_ITEM_LOGIN_REGISTER).setChecked(true);
    mUserNameTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
        getResources().getDimensionPixelOffset(R.dimen.size_font_L));
    mUserNameTextView.setTypeface(null, Typeface.NORMAL);
    mUserEmailTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
        getResources().getDimensionPixelOffset(R.dimen.size_font_M));
    mUserEmailTextView.setTypeface(null, Typeface.BOLD);

    loadUserProfileImage();
  }

  private void updateEmailToValidate(boolean visible, String emailToValidateString) {
    mEmailToValidate.setVisibility(visible ? VISIBLE : GONE);
    mEmailToValidate.setText(emailToValidateString);
  }

  @Override public void showLogOutConfirmationAlert() {
    AlertDialog.Builder logoutConfirmationDialog = new AlertDialog.Builder(getActivity());
    logoutConfirmationDialog.setTitle(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_LOGOUT_TITLE));
    logoutConfirmationDialog.setMessage(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ACCOUNTPREF_LOGOUT_CHECK));
    logoutConfirmationDialog.setPositiveButton(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_LOGOUT_BUTTON),
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            loadMenuNotSigned();
            mPresenter.logOut();
            //TODO CHANGE THIS
            PreferencesManager.clearSearchOptionsAndShoppingCart(getContext());
            mTracker.updateDimensionOnUserLogin(false);
            mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_DATA_ACC_DETAILS,
                TrackerConstants.ACTION_ACCOUNT_SUMMARY, TrackerConstants.LABEL_LOG_OUT_CLICKS);
          }
        });
    logoutConfirmationDialog.setNegativeButton(android.R.string.cancel,
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
          }
        });
    mLogoutConfirmationDialog = logoutConfirmationDialog.show();
  }

  @Override protected NavigationDrawerPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideNavigationDrawerPresenter(this, (NavigationDrawerNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.navigation_drawer_view;
  }

  @Override protected void initComponent(View view) {
    mNavigationView = (NavigationView) view.findViewById(R.id.navigationDrawer);
    mDrawerHeader = mNavigationView.inflateHeaderView(R.layout.navigation_drawer_header);
    mUserEmailTextView = (TextView) mDrawerHeader.findViewById(R.id.userEmailNavigationDrawer);
    mUserNameTextView = (TextView) mDrawerHeader.findViewById(R.id.userNameNavigationDrawer);
    mUserProfileImageView =
        (ImageView) mDrawerHeader.findViewById(R.id.userImageProfileNavigationDrawer);
    mArrowImageView = (ImageView) mDrawerHeader.findViewById(R.id.ivOptionsArrow);
    mEmailToValidate = (TextView) mDrawerHeader.findViewById(R.id.email_to_validate_textView);
    mTappingLayout = (RelativeLayout) mDrawerHeader.findViewById(R.id.tapping_layout);
  }

  @Override protected void setListeners() {
    mNavigationView.setNavigationItemSelectedListener(navigationItemSelectedListener);
    mTappingLayout.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.changeMenu();
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_AREA, getAction(),
            TrackerConstants.LABEL_ACCOUNT_MENU_OPEN);
      }
    });
  }

  @Override protected void initOneCMSText(View view) {

  }

  private void loadMenuUserLogged() {
    mArrowImageView.setImageResource(R.drawable.ic_arrow_drop_down);
    mArrowImageView.setVisibility(VISIBLE);
    Menu menu = loadMenuInNavigationDrawer(R.menu.menu_navigation_drawer_user_logged);

    menu.findItem(R.id.drawer_item_travellers)
        .setTitle(LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ND_TRAVELLERS));
    menu.findItem(R.id.drawer_item_trips)
        .setTitle(LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ND_TRIPS));

    menu.findItem(R.id.drawer_item_settings)
        .setTitle(LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ND_SETTINGS));
    menu.findItem(R.id.drawer_item_about)
        .setTitle(LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ND_ABOUT));
  }

  private void loadMenuOptionsAccountPreferences() {
    int idMenu = R.menu.menu_navigation_drawer_account_options;
    int arrowDrawable = R.drawable.ic_arrow_drop_up;
    mArrowImageView.setImageResource(arrowDrawable);
    mArrowImageView.setVisibility(VISIBLE);
    Menu menu = loadMenuInNavigationDrawer(idMenu);
    mNavigationView.getMenu().getItem(POSITION_ITEM_LOGOUT).setChecked(true);

    menu.findItem(R.id.acountPreferencesItem)
        .setTitle(
            LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ND_ACCOUNT_PREFERENCES));
    menu.findItem(R.id.logOutItem)
        .setTitle(LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_LOGOUT_BUTTON));
  }

  private void loadMenuNotSigned() {
    Menu menu = loadMenuInNavigationDrawer(R.menu.menu_navigation_drawer);

    menu.findItem(R.id.drawer_item_login_register)
        .setTitle(LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ND_LOGIN_OR_REGISTER));

    menu.findItem(R.id.drawer_item_travellers)
        .setTitle(LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ND_TRAVELLERS));
    menu.findItem(R.id.drawer_item_trips)
        .setTitle(LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ND_TRIPS));

    menu.findItem(R.id.drawer_item_settings)
        .setTitle(LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ND_SETTINGS));
    menu.findItem(R.id.drawer_item_about)
        .setTitle(LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ND_ABOUT));
  }

  @Override public void hideUserName() {
    mUserNameTextView.setVisibility(GONE);
  }

  @Override public void showUserName(String name) {
    mUserNameTextView.setVisibility(VISIBLE);
    mUserNameTextView.setText(name);
  }

  private Menu loadMenuInNavigationDrawer(int idMenu) {
    Menu menu = mNavigationView.getMenu();
    menu.clear();
    mNavigationView.inflateMenu(idMenu);
    return menu;
  }

  private void setUpDrawerLayout() {
    Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
    ((NavigationDrawerNavigator) getActivity()).setSupportActionBar(toolbar);

    imageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();

    mDrawer = (DrawerLayout) getActivity().findViewById(R.id.drawerLayout);
    ActionBarDrawerToggle toggle =
        new ActionBarDrawerToggle(getActivity(), mDrawer, toolbar, R.string.navigation_drawer_open,
            R.string.navigation_drawer_close) {
          @Override public void onDrawerClosed(View drawerView) {
            if (!mDefaultMenuPresent) {
              refreshMenu();
            }
          }

          @Override public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
            mPresenter.trackOpenMenu();
          }
        };
    mDrawer.setDrawerListener(toggle);
    toggle.syncState();
  }

  private void loadUserProfileImage() {
    imageLoader.load(mUserProfileImageView, mUrlUserImageProfile, mIdUserImageProfile);
  }

  private void loadCircularUserProfileImage() {
    imageLoader.loadCircleTransformation(mUserProfileImageView, mUrlUserImageProfile,
        mIdUserImageProfile);
  }

  private void changeNavigationDrawerBackgroundHeader() {
    mDrawerHeader.setBackgroundResource(mIdBackground);
  }

  private void handleItemSelected(final int menuItemIdSelected) {
    String label = null;
    String action = null;
    String category = TrackerConstants.CATEGORY_MY_AREA;

    if (menuItemIdSelected == R.id.drawer_item_login_register) {
      mPresenter.goToJoinUs();
      label = TrackerConstants.LABEL_JOIN_NOW_CLICKS;
      action = getAction();
    } else if (menuItemIdSelected == R.id.drawer_item_travellers) {
      mPresenter.goToTravellers();
      label = TrackerConstants.LABEL_USUAL_TRAVELLERS_OPEN;
      action = getAction();

      mTracker.trackAppseeEvent(MY_DATA_HOME_TAP_EVENT);
    } else if (menuItemIdSelected == R.id.drawer_item_trips) {
      mPresenter.goToTrips();
      label = TrackerConstants.LABEL_MY_TRIPS_OPEN;
      action = getAction();

      mTracker.trackAppseeEvent(MY_TRIPS_HOME_EVENT);
    } else if (menuItemIdSelected == R.id.drawer_item_settings) {
      mPresenter.goToSettings();
      label = TrackerConstants.LABEL_APP_CONFIGURATION_CLICKS;
      action = getAction();
    } else if (menuItemIdSelected == R.id.drawer_item_about) {
      mPresenter.goToAbout();
      label = TrackerConstants.LABEL_INFORMATION_OPEN;
      action = getAction();
    } else if (menuItemIdSelected == R.id.acountPreferencesItem) {
      mPresenter.goToAccountPreferences();
      label = TrackerConstants.LABEL_ACCOUNT_PREFERENCES_OPEN;
      action = TrackerConstants.ACTION_ACCOUNT_MENU;
      category = TrackerConstants.CATEGORY_MY_DATA_ACC;
    } else if (menuItemIdSelected == R.id.logOutItem) {
      mTracker.trackAppseeEvent(LOGOUT_TAP_EVENT);
      showLogOutConfirmationAlert();
      label = TrackerConstants.LABEL_LOG_OUT_CLICKS;
      action = TrackerConstants.ACTION_ACCOUNT_MENU;
      category = TrackerConstants.CATEGORY_MY_DATA_ACC;
    }

    mTracker.trackAnalyticsEvent(category, action, label);
    closeDrawer();
  }

  private String getAction() {
    String action;
    if (mPresenter.isUserInactive()) {
      action = TrackerConstants.ACTION_DATA_PENDING_VALIDATION;
    } else if (mPresenter.isUserLoggedIn()) {
      action = TrackerConstants.ACTION_DATA_LOGGED;
    } else if (!mPresenter.hasLocalData()) {
      action = TrackerConstants.ACTION_NO_DATA;
    } else {
      action = TrackerConstants.ACTION_DATA_LOCAL;
    }
    return action;
  }

  @Override public void closeDrawer() {
    if (mDrawer != null) {
      mDrawer.closeDrawers();
    }
  }

  @Override public boolean drawerIsOpened() {
    return mDrawer.isDrawerOpen(GravityCompat.START);
  }

  @Override public void updateCarousel() {
    if (mCallbackUpdateCarousel != null) {
      mCallbackUpdateCarousel.onUpdateCarousel();
    }
  }
}