package com.odigeo.app.android.lib.models.dto;

public class CreditCardDTO {

  protected String creditCardNumber;
  protected String expiryMonth;
  protected String expiryYear;

  /**
   * Gets the value of the creditCardNumber property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCreditCardNumber() {
    return creditCardNumber;
  }

  /**
   * Sets the value of the creditCardNumber property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCreditCardNumber(String value) {
    this.creditCardNumber = value;
  }

  /**
   * Gets the value of the expiryMonth property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getExpiryMonth() {
    return expiryMonth;
  }

  /**
   * Sets the value of the expiryMonth property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setExpiryMonth(String value) {
    this.expiryMonth = value;
  }

  /**
   * Gets the value of the expiryYear property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getExpiryYear() {
    return expiryYear;
  }

  /**
   * Sets the value of the expiryYear property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setExpiryYear(String value) {
    this.expiryYear = value;
  }
}
