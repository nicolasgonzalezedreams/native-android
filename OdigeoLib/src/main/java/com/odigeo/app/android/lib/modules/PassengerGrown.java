package com.odigeo.app.android.lib.modules;

import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.dto.TravellerTypeDTO;
import com.odigeo.app.android.lib.ui.widgets.ButtonDateForm;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.dialogs.DateDialog;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ManuelOrtiz on 29/10/2014.
 */
public final class PassengerGrown {

  private PassengerGrown() {

  }

  public static void limitBirthDatePicker(TravellerTypeDTO travellerTypeDTO,
      ButtonDateForm buttonDateForm) {
    Calendar min = Calendar.getInstance();
    Calendar max = Calendar.getInstance();

    Date currentDate = buttonDateForm.getDate();
    Date minDate = null;

    if (travellerTypeDTO == TravellerTypeDTO.INFANT) {
      min.add(Calendar.YEAR, -Constants.MAX_AGE_INFANT);
      max.add(Calendar.DATE, -1);
      minDate = min.getTime();
    } else if (travellerTypeDTO == TravellerTypeDTO.CHILD) {
      min.add(Calendar.YEAR, -Constants.MAX_AGE_CHILD);
      max.add(Calendar.YEAR, -Constants.MAX_AGE_INFANT);
      minDate = min.getTime();
    } else if (travellerTypeDTO == TravellerTypeDTO.ADULT) {
      max.add(Calendar.YEAR, -Constants.MAX_AGE_CHILD);
    }

    buttonDateForm.setDate(currentDate);
    buttonDateForm.setMinDate(minDate);
    buttonDateForm.setMaxDate(max.getTime());
  }

  public static void limitBirthDatePicker(String key, DateDialog dateDialog) {
    Calendar min = Calendar.getInstance();
    Calendar max = Calendar.getInstance();

    //Date currentDate = dateDialog.getDate();
    Date minDate = null;

    if (key == OneCMSKeys.PASSENGER_TYPE_INFANT) {
      min.add(Calendar.YEAR, -Constants.MAX_AGE_INFANT);
      max.add(Calendar.DATE, -1);
      minDate = min.getTime();
    } else if (key == OneCMSKeys.PASSENGER_TYPE_CHILD) {
      min.add(Calendar.YEAR, -Constants.MAX_AGE_CHILD);
      max.add(Calendar.YEAR, -Constants.MAX_AGE_INFANT);
      minDate = min.getTime();
    } else if (key == OneCMSKeys.PASSENGER_TYPE_ADULT) {
      max.add(Calendar.YEAR, -Constants.MAX_AGE_CHILD);
    }

    ///dateDialog.setDate(currentDate);
    dateDialog.setMinDate(minDate);
    dateDialog.setMaxDate(max.getTime());
  }
}
