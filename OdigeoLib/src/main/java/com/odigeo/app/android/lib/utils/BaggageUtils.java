/**
 * Utility class for baggage common methods, like string generation texts Created by M. en C. Javier
 * Silva Perez on 3/2/2015.
 */

package com.odigeo.app.android.lib.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.dto.SegmentTypeIndexDTO;
import java.util.List;

/**
 * Utility class for baggage common methods, like string generation texts
 *
 * @author M. en C. Javier Silva Perez
 * @version 1.0
 * @since 3/2/2015
 */
public final class BaggageUtils {

  private BaggageUtils() {

  }

  /**
   * Creates the full baggage option text that shows the number of bags included and the max
   * weight for each one using custom strings to construct the full text
   *
   * @param numberBaggage Total number of baggage included on the leg
   * @param maxWeight Max weight per baggage could be 0 or null
   * @param maxWeightString String resource reference for max weight text
   * @return The string to show on the cell
   */
  @NonNull public static String getBaggageOptionText(Context context, Integer numberBaggage,
      Integer maxWeight, String maxWeightString) {
    StringBuffer strBaggage;

    String resourceString =
        LocalizablesFacade.getString(context, "travellersviewcontroller_baggageconditions_bag")
            .toString();

    if (numberBaggage > 1) {
      resourceString =
          LocalizablesFacade.getString(context, "travellersviewcontroller_baggageconditions_bags")
              .toString();
    }

    strBaggage = new StringBuffer(String.format("%s " + resourceString, numberBaggage));

    if (maxWeight != null && maxWeight > 0) {
      strBaggage.append(' ' + String.format(maxWeightString, maxWeight));
    }
    return strBaggage.toString();
  }

  /**
   * Creates the full baggage option text that shows the number of bags included and the max
   * weight for each one using custom strings to construct the full text
   *
   * @param numberBaggage Total number of baggage included on the leg
   * @param baggageNumberString String resource to be formatted using the baggage number using %1$d
   * format
   * @param maxWeight Max weight per baggage could be 0 or null
   * @param maxWeightString String resource reference for max weight text
   * @return The string to show on the cell
   */
  @NonNull public static String getBaggageIncludedText(Context context, Integer numberBaggage,
      String baggageNumberString, Integer maxWeight, String maxWeightString) {
    String temp = String.format(baggageNumberString, numberBaggage);
    StringBuilder res = new StringBuilder(temp);
    if (maxWeight != null && maxWeight > 0) {
      String formatted = " " + String.format(maxWeightString, maxWeight);
      res.append(formatted);
    }
    return res.toString();
  }

  /**
   * Creates the text to show on {@link com.odigeo.app.android.lib.fragments.BuyBaggageFragment.BaggageScenario#BAGGAGE_INCLUDED}
   * cell using the number og baggage included and the maxWeight per baggage if the maxWeight is
   * not specified do not add it to the text
   *
   * @param numberBaggage Total number of baggage included on the leg
   * @param maxWeight Max weight per baggage could be 0 or null
   * @return The string to show on the cell
   */
  @NonNull public static String getBaggageIncludedContent(Context context, Integer numberBaggage,
      Integer maxWeight) {

    return getBaggageIncludedText(context, numberBaggage, LocalizablesFacade.getRawString(context,
        "travellersviewcontroller_baggageconditions_tripbaggageincluded"), maxWeight,
        LocalizablesFacade.getRawString(context,
            "travellersviewcontroller_baggageconditions_maxbagweight"));
  }

  /**
   * Creates the text for {@link com.odigeo.app.android.lib.fragments.BuyBaggageFragment.BaggageScenario#CANT_ADD_BAGGAGE_TO_SEGMENT}
   * cell using a custom phone number
   *
   * @param phoneNumber Phone number to add at the end of the text
   * @return The string to show on the cell
   */
  public static String getLegInformationContent(Context context, String phoneNumber) {
    return LocalizablesFacade.getString(context,
        "travellersviewcontroller_baggageconditions_legbaggagenotincludednotpurchasable",
        phoneNumber).toString();
  }

  /**
   * Creates the text for {@link com.odigeo.app.android.lib.fragments.BuyBaggageFragment.BaggageScenario#CANT_ADD_BAGGAGE_TO_ITINERARY}
   * cell using a custom phone number
   *
   * @param phoneNumber Phone number to add at the end of the text
   * @return The string to show on the cell
   */
  public static String getInformationContent(Context context, String phoneNumber) {
    return String.format(LocalizablesFacade.getRawString(context,
        "travellersviewcontroller_baggageconditions_tripbaggagenotincludednotpurchasable"),
        phoneNumber);
  }

  /**
   * Based on the {@link com.odigeo.app.android.lib.models.dto.SegmentTypeIndexDTO} compared to
   * the selected index
   *
   * @param flightSegmentList List of flight segments to search
   * @param segmentTypeIndexDTO Segment to search in flight segments list
   * @return The estimation fee object that contains the index using {@link
   * com.odigeo.app.android.lib.models.dto.SegmentTypeIndexDTO#index} value
   */
  public static FlightSegment getFlightSegmentByIndex(List<FlightSegment> flightSegmentList,
      SegmentTypeIndexDTO segmentTypeIndexDTO) {
    int index = segmentTypeIndexDTO.getIndex();
    if (index < flightSegmentList.size()) {
      return flightSegmentList.get(index);
    }
    return new FlightSegment();
  }

  /**
   * Using segment departure and arrival city names return a string like: "DepartureCity to
   * ArrivalCity"
   *
   * @param segment Segment to get the arrival and depature city names
   * @return A string of type "DepartureCity to ArrivalCity"
   */
  public static String getSegmentName(Context context, FlightSegment segment) {
    String name = context.getString(R.string.format_segment_description);
    return String.format(name, segment.getDepartureCity().getCityName(),
        segment.getArrivalCity().getCityName());
  }
}
