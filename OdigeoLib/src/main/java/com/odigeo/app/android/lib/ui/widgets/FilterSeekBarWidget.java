package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class FilterSeekBarWidget extends LinearLayout implements PropertyChangeListener {

  private static final int DEFVALUE = 100;
  private final List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();
  private final AttributeSet attributes;
  private TextView titleTextView;
  private TextView subTitleTextView;
  private SeekBar seekBar;
  private String textTitle;
  private String textCity;
  private int maxValueSeekBar;
  private int selectedValue;
  private int parentNumberWidget;
  private PropertyChangeListener propertyChangeListener;

  public FilterSeekBarWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
    attributes = attrs;
    init();
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.layout_filter_trip_duration_widget, this);
    titleTextView = (TextView) findViewById(R.id.text_filter_trip_duration_title);
    subTitleTextView = (TextView) findViewById(R.id.text_filter_trip_duration_subtittle);
    seekBar = (SeekBar) findViewById(R.id.seekBar_trip_duration);

    //        seekBar.setPropertyChangeListener(this);
    seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        selectedValue = progress;
      }

      @Override public void onStartTrackingTouch(SeekBar seekBar) {
        //TODO add action
      }

      @Override public void onStopTrackingTouch(SeekBar seekBar) {
        notifyListener(Constants.SEEKERBAR_DURATION, "", String.valueOf(seekBar.getProgress()));
      }
    });
  }

  private void setAttributes() {
    TypedArray typedArray =
        getContext().obtainStyledAttributes(attributes, R.styleable.FilterSeekBarWidget);

    textTitle = typedArray.getString(R.styleable.FilterSeekBarWidget_textTitle);
    textCity = typedArray.getString(R.styleable.FilterSeekBarWidget_textSubtitle);
    maxValueSeekBar = typedArray.getInt(R.styleable.FilterSeekBarWidget_maxValue, DEFVALUE);
    typedArray.recycle();

    setMaxValueSeekBar(maxValueSeekBar);
  }

  private void init() {
    inflateLayout();
    setAttributes();
    selectedValue = maxValueSeekBar;
    drawValues();
  }

  private void drawValues() {
    if (textTitle != null) {
      titleTextView.setText(LocalizablesFacade.getString(getContext(), textTitle));
    }
    if (textCity != null) {
      subTitleTextView.setText(LocalizablesFacade.getString(getContext(), textCity));
    }
    setSelectedValue(selectedValue);
  }

  public final void setMaxValue(int value) {
    setMaxValueSeekBar(value);
  }

  public final int getMaxValueSeekBar() {
    return maxValueSeekBar;
  }

  private void setMaxValueSeekBar(int value) {
    seekBar.setMax(value);
    this.maxValueSeekBar = value;
  }

  public final void setValueSelected(int selectedValue) {
    setSelectedValue(selectedValue);
  }

  public final int getSelectedValue() {
    return this.selectedValue;
  }

  private void setSelectedValue(int selectedValue) {
    seekBar.setProgress(selectedValue);
    this.selectedValue = selectedValue;
  }

  private void setTextTitle(String title) {
    titleTextView.setText(title);
  }

  public final void setSubTitle(String subTitle) {
    setTextSubTitle(subTitle);
  }

  public final void setSubTitleText(String subTitleText) {
    setTextSubTitle(subTitleText);
  }

  private void setTextSubTitle(String subTitleValue) {
    subTitleTextView.setText(subTitleValue);
  }

  public final void setTypeFaceTitle(Typeface type) {
    titleTextView.setTypeface(type);
  }

  public final void setTypeFaceSubTitle(Typeface type) {
    subTitleTextView.setTypeface(type);
  }

  public final int getParentNumberWidget() {
    return parentNumberWidget;
  }

  public final void setParentNumberWidget(int parentNumberWidget) {
    this.parentNumberWidget = parentNumberWidget;
  }

  private void notifyListener(PropertyChangeEvent event) {
    this.propertyChangeListener.propertyChange(event);
  }

  private void notifyListener(String property, String oldValue, String newValue) {
    if (this.propertyChangeListener != null) {
      this.propertyChangeListener.propertyChange(
          new PropertyChangeEvent(this, property, oldValue, newValue));
    }
  }

  public final void addChangeListener(PropertyChangeListener newListener) {
    listener.add(newListener);
  }

  public final void setPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
    this.propertyChangeListener = propertyChangeListener;
  }

  @Override public final void propertyChange(PropertyChangeEvent event) {
    notifyListener(event);
  }
}
