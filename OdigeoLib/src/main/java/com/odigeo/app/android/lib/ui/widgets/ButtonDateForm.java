package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.DatePicker;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.base.ButtonWithAndWithoutHint;
import com.odigeo.app.android.lib.utils.AppLocaleDatePickerDialog;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.Validations;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by ManuelOrtiz on 11/09/2014.
 */
public class ButtonDateForm extends ButtonWithAndWithoutHint implements View.OnClickListener {
  public static final String TIMEZONE_GMT = "GMT";
  //UTC Timezone
  protected Date date;
  protected boolean showDays;
  protected boolean showMonths;
  protected boolean showYears;
  protected String dateFormat;
  //UTC Timezone
  protected Date minDate;
  //UTC Timezone
  protected Date maxDate;
  protected AppLocaleDatePickerDialog datePickerDialog;
  //Labels
  private String okLabel;
  private String cancelLabel;

  public ButtonDateForm(Context context, AttributeSet attrs) {
    super(context, attrs);

    //Get Design time params
    TypedArray aAttrs = context.obtainStyledAttributes(attrs, R.styleable.customDatePicker, 0, 0);
    showDays = aAttrs.getBoolean(R.styleable.customDatePicker_showDays, true);
    showMonths = aAttrs.getBoolean(R.styleable.customDatePicker_showMonths, true);
    showYears = aAttrs.getBoolean(R.styleable.customDatePicker_showYears, true);
    dateFormat = aAttrs.getString(R.styleable.customDatePicker_dateFormat);

    if (dateFormat == null || dateFormat.trim().equals("")) {
      dateFormat = getResources().getString(R.string.templates__date1);
    }

    aAttrs.recycle();

    this.setOnClickListener(this);
  }

  @Override public final int getEmptyLayout() {

    return R.layout.layout_form_text_field_empty;
  }

  @Override public final int getLayout() {

    return R.layout.layout_form_text_field;
  }

  //It has UTC time zone
  public final Date getDate() {
    return date;
  }

  //Date should have UTC time zone
  public void setDate(Date date) {
    this.date = date;

    if (this.date != null) {

      //date = Util.resetTimeToDate(date);
      this.date = resetTimeToDate(this.date, TimeZoneType.UTC);

      SimpleDateFormat simpleDateFormat = OdigeoDateUtils.getGmtDateFormat(dateFormat);

      this.setText(simpleDateFormat.format(this.date));
    } else {
      this.clear();
    }

    if (getCustomFieldListener() != null) {
      getCustomFieldListener().onFieldValueChanged(this, this.date);
    }
  }

  //It has Default time zone
  public final Date getDateInDefaultTimeZone() {

    if (date == null) {
      return null;
    }

    Calendar calendarGmtTimezone = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE_GMT));
    calendarGmtTimezone.setTime(date);

    Calendar calendarDefaultTimezone = Calendar.getInstance();

    calendarDefaultTimezone.set(Calendar.YEAR,
        calendarGmtTimezone.get(Calendar.YEAR));            // set hour to midnight
    calendarDefaultTimezone.set(Calendar.MONTH,
        calendarGmtTimezone.get(Calendar.MONTH));                 // set minute in hour
    calendarDefaultTimezone.set(Calendar.DAY_OF_MONTH,
        calendarGmtTimezone.get(Calendar.DAY_OF_MONTH));
    calendarDefaultTimezone.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
    calendarDefaultTimezone.set(Calendar.MINUTE, 0);                 // set minute in hour
    calendarDefaultTimezone.set(Calendar.SECOND, 0);                 // set second in minute
    calendarDefaultTimezone.set(Calendar.MILLISECOND, 0);            // set millis in second

    return calendarDefaultTimezone.getTime();
  }

  @Override public void onClick(View v) {
    Calendar calendar;
    if (date == null) {
      //Get Today's date
      calendar = Calendar.getInstance();
    } else {
      //Create calendar, with UTC timezone
      calendar = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE_GMT));
      calendar.setTime(date);
    }

    createDatePicker(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH));
  }

  private void createDatePicker(int year, int month, int day) {
    datePickerDialog = new AppLocaleDatePickerDialog(getContext(), null, year, month, day);

    final DatePicker datePicker = datePickerDialog.getDatePicker();

    if (maxDate != null) {
      datePicker.setMaxDate(maxDate.getTime());
    }

    if (minDate != null) {
      datePicker.setMinDate(minDate.getTime());
    }

    if (!showDays) {
      findAndHideField(datePicker, "mDaySpinner");
    }

    if (!showMonths) {
      findAndHideField(datePicker, "mMonthSpinner");
    }

    if (!showYears) {
      findAndHideField(datePicker, "mYearSpinner");
    }

    datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getCancelLabel(),
        (DialogInterface.OnClickListener) null);

    datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, getOKLabel(),
        new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialog, int which) {

            //Calendar newCalendar = Calendar.getInstance();
            Calendar newCalendar = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE_GMT));

            newCalendar.set(Calendar.YEAR, datePicker.getYear());
            newCalendar.set(Calendar.MONTH, datePicker.getMonth());
            newCalendar.set(Calendar.DATE, datePicker.getDayOfMonth());

            setDate(newCalendar.getTime());
          }
        });

    datePickerDialog.show();
  }

  protected String getOKLabel() {
    if (!TextUtils.isEmpty(okLabel)) {
      return okLabel;
    }
    return LocalizablesFacade.getString(getContext(), "passengerpicker_okbtn_titlelabel_text")
        .toString();
  }

  public void setOKLabel(String label) {
    okLabel = label;
  }

  protected String getCancelLabel() {
    if (!TextUtils.isEmpty(cancelLabel)) {
      return cancelLabel;
    }
    return LocalizablesFacade.getString(getContext(), "common_cancel").toString();
  }

  public void setCancelLabel(String label) {
    cancelLabel = label;
  }

  protected void findAndHideField(DatePicker datepicker, String name) {
    try {
      Field field = DatePicker.class.getDeclaredField(name);
      field.setAccessible(true);
      View fieldInstance = (View) field.get(datepicker);
      fieldInstance.setVisibility(View.GONE);
    } catch (Exception e) {
      Logger.getLogger(ButtonDateForm.class.getName()).log(Level.SEVERE, Constants.TAG_LOG, e);
    }
  }

  @Override public boolean isValid() {
    if (this.getVisibility() == GONE) {
      return true;
    }

    if (super.isEmpty() && !isMandatory()) {
      return true;
    }

    if (super.isEmpty() && isMandatory()) {
      return false;
    }

    return Validations.validateDateTypes(getValidationFormat(), getDate());
  }

  public Date getMinDate() {
    return minDate;
  }

  //This date is set with the Default Timezone
  public void setMinDate(Date minDate) {
    this.minDate = startOfTheDay(minDate, TimeZoneType.LOCAL);

    if (date != null && minDate != null) {
      if (getDateInDefaultTimeZone().before(minDate)) {
        setDate(minDate);
      }
    }
  }

  public Date getMaxDate() {
    return maxDate;
  }

  //Set the max date accepted by the picker, but set the time to the last time of the day
  //This date is set with the Default Timezone
  public void setMaxDate(Date maxDate) {
    this.maxDate = endOfTheDay(maxDate, TimeZoneType.LOCAL);

    if (date != null && maxDate != null) {
      if (getDateInDefaultTimeZone().after(maxDate)) {
        setDate(maxDate);
      }
    }
  }

  //Set the min date accepted by the picker, but set the time to the first time of the day
  public Date startOfTheDay(Date date, TimeZoneType timeZoneType) {
    if (date == null) {
      return null;
    }

    return resetTimeToDate(date, timeZoneType);
    //return Util.resetTimeToDate(date);
  }

  public Date endOfTheDay(Date date, TimeZoneType timeZoneType) {
    if (date == null) {
      return date;
    }

    //Calendar calendar = Calendar.getInstance();
    Calendar calendar = null;

    if (timeZoneType == TimeZoneType.LOCAL) {
      calendar = Calendar.getInstance();
    } else if (timeZoneType == TimeZoneType.UTC) {
      calendar = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE_GMT));
    }

    //calendar.setTime(Util.resetTimeToDate(date));
    calendar.setTime(resetTimeToDate(date, timeZoneType));
    calendar.add(Calendar.DATE, 1);
    calendar.add(Calendar.SECOND, -1);

    return calendar.getTime();
  }

  //TODO Modify Util method to parameterized the timezone
  private Date resetTimeToDate(Date date, TimeZoneType timezone) {
    if (date == null) {
      return null;
    }

    //final Calendar dateWithoutTime = Calendar.getInstance();

    Calendar dateWithoutTime = null;

    if (timezone == TimeZoneType.LOCAL) {
      dateWithoutTime = Calendar.getInstance();
    } else if (timezone == TimeZoneType.UTC) {
      dateWithoutTime = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE_GMT));
    }

    dateWithoutTime.setTime(date);
    dateWithoutTime.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
    dateWithoutTime.set(Calendar.MINUTE, 0);                 // set minute in hour
    dateWithoutTime.set(Calendar.SECOND, 0);                 // set second in minute
    dateWithoutTime.set(Calendar.MILLISECOND, 0);            // set millis in second

    return dateWithoutTime.getTime();
  }

  enum TimeZoneType {
    LOCAL, UTC
  }
}
