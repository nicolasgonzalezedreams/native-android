package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.models.dto.FrequentFlyerCardCodeDTO;
import com.odigeo.app.android.lib.ui.widgets.EmptyViewWidget;
import java.util.ArrayList;

/**
 * Created by Irving Lóp on 15/11/2014.
 */
public class FrequentFlyerEmptyFragment extends Fragment
    implements EmptyViewWidget.EmptyViewWidgetListener {

  @Override public final void onAttach(Activity activity) {
    super.onAttach(activity);
    setHasOptionsMenu(true);
  }

  @Override public final View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View view = inflater.inflate(R.layout.layout_frequent_flyer_empty_fragment, container, false);
    ((EmptyViewWidget) view.findViewById(R.id.widget_frequent_flyer_emptyview)).setEmptyListener(
        this);
    return view;
  }

  @Override public final void clickEmptyViewButton() {
    FrequentFlyerFragment fragment =
        FrequentFlyerFragment.newInstance(new ArrayList<FrequentFlyerCardCodeDTO>());
    getActivity().getFragmentManager()
        .beginTransaction()
        .replace(android.R.id.content, fragment)
        .commit();
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {
    int itemId = item.getItemId();
    if (itemId == android.R.id.home) {
      getActivity().finish();
    }
    return super.onOptionsItemSelected(item);
  }
}
