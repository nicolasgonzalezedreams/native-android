package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.List;

public class AirlineGroupDTO implements Serializable {

  protected List<CarrierDTO> carriers;

  public List<CarrierDTO> getCarriers() {
    return carriers;
  }

  public void setCarriers(List<CarrierDTO> carriers) {
    this.carriers = carriers;
  }
}
