package com.odigeo.app.android.lib.models;

import android.text.Spanned;
import com.odigeo.app.android.lib.interfaces.ShareTextsInterface;

/**
 * Created by gaston.mira on 11/23/16.
 */

public abstract class BaseShareModel implements ShareTextsInterface {
  private String mTwitterMessage;
  private String mFbMessage;
  private String mFbMessengerMessage;
  private String mWhatsAppMessage;
  private String mCopyToClipboardMessage;
  private Spanned mEmailBodyMessage;
  private String mEmailSubject;
  private String mSmsMessage;

  public String getTwitterMessage() {
    return mTwitterMessage;
  }

  public void setTwitterMessage(String twitterMessage) {
    mTwitterMessage = twitterMessage;
  }

  public String getFbMessage() {
    return mFbMessage;
  }

  public void setFbMessage(String fbMessage) {
    mFbMessage = fbMessage;
  }

  public String getFbMessengerMessage() {
    return mFbMessengerMessage;
  }

  public void setFbMessengerMessage(String fbMessengerMessage) {
    mFbMessengerMessage = fbMessengerMessage;
  }

  public String getWhatsAppMessage() {
    return mWhatsAppMessage;
  }

  public void setWhatsAppMessage(String whatsAppMessage) {
    mWhatsAppMessage = whatsAppMessage;
  }

  public String getCopyToClipboardMessage() {
    return mCopyToClipboardMessage;
  }

  public void setCopyToClipboardMessage(String copyToClipboardMessage) {
    mCopyToClipboardMessage = copyToClipboardMessage;
  }

  public Spanned getEmailBodyMessage() {
    return mEmailBodyMessage;
  }

  public void setEmailBodyMessage(Spanned emailBodyMessage) {
    mEmailBodyMessage = emailBodyMessage;
  }

  public String getEmailSubject() {
    return mEmailSubject;
  }

  public void setEmailSubject(String emailSubject) {
    mEmailSubject = emailSubject;
  }

  public String getSmsMessage() {
    return mSmsMessage;
  }

  public void setSmsMessage(String smsMessage) {
    mSmsMessage = smsMessage;
  }
}
