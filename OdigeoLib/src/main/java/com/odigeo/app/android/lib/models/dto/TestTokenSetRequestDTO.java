package com.odigeo.app.android.lib.models.dto;

import java.util.ArrayList;
import java.util.List;

public class TestTokenSetRequestDTO {

  protected List<PartitionRequestDTO> partitionRequest;
  protected TestTokenSetCookieDTO testTokenSet;
  protected String webSite;

  /**
   * Gets the value of the partitionRequest property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the partitionRequest property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getPartitionRequest().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list
   * {@link PartitionRequestDTO }
   */
  public List<PartitionRequestDTO> getPartitionRequest() {
    if (partitionRequest == null) {
      partitionRequest = new ArrayList<PartitionRequestDTO>();
    }
    return this.partitionRequest;
  }

  /**
   * Gets the value of the testTokenSet property.
   *
   * @return possible object is
   * {@link TestTokenSetCookie }
   */
  public TestTokenSetCookieDTO getTestTokenSet() {
    return testTokenSet;
  }

  /**
   * Sets the value of the testTokenSet property.
   *
   * @param value allowed object is
   * {@link TestTokenSetCookie }
   */
  public void setTestTokenSet(TestTokenSetCookieDTO value) {
    this.testTokenSet = value;
  }

  /**
   * Gets the value of the webSite property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getWebSite() {
    return webSite;
  }

  /**
   * Sets the value of the webSite property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setWebSite(String value) {
    this.webSite = value;
  }
}
