package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;

/**
 * Created by Crux on 30/09/2014.
 */
public class SwitchWidget extends LinearLayout {
  private final String text;
  private final Drawable background;
  private TextView txtLabel;
  private SwitchCompat switchPassenger;
  private SwitchListener listener;

  public SwitchWidget(Context context, AttributeSet attrs) {

    super(context, attrs);

    TypedArray aAttrs = context.obtainStyledAttributes(attrs, R.styleable.SwitchWidget, 0, 0);

    text = aAttrs.getString(R.styleable.SwitchWidget_odigeoText);
    background = aAttrs.getDrawable(R.styleable.SwitchWidget_android_background);

    initialize(context);

    switchPassenger.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (listener != null && buttonView.isPressed()) {
          if (isChecked) {
            listener.onSwitchOn();
          } else {
            listener.onSwitchOff();
          }
        }
      }
    });
  }

  private void initialize(Context context) {
    inflate(context, R.layout.widget_switch_labelled, this);

    txtLabel = (TextView) findViewById(R.id.txtSwitchPassengerLabel);
    switchPassenger = (SwitchCompat) findViewById(R.id.switchPassengerPayment);

    if (text != null) {
      txtLabel.setText(LocalizablesFacade.getString(getContext(), text));
    }

    //this.setBackgroundDrawable();
    this.setBackgroundDrawable(background);
        /*
        if (background >= 0) {
            this.setBackgroundResource(background);
        }*/
  }

  public String getLabel() {

    return txtLabel.getText().toString();
  }

  public void setLabel(CharSequence label) {

    this.txtLabel.setText(label);
  }

  public boolean isChecked() {
    return getSwitchPassenger().isChecked();
  }

  public void setChecked(boolean isChecked) {
    getSwitchPassenger().setChecked(isChecked);
  }

  public SwitchCompat getSwitchPassenger() {
    return switchPassenger;
  }

  public SwitchListener getListener() {
    return listener;
  }

  public void setListener(SwitchListener listener) {
    this.listener = listener;
  }

  public interface SwitchListener {
    void onSwitchOn();

    void onSwitchOff();
  }
}
