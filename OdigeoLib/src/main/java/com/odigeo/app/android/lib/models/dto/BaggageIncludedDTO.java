package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public enum BaggageIncludedDTO implements Serializable {

  INCLUDED, NOT_INCLUDED, UNKNOWN, INCONSISTENT;

  public static BaggageIncludedDTO getValue(String value) {
    return valueOf(value);
  }

  public String value() {
    return name();
  }
}
