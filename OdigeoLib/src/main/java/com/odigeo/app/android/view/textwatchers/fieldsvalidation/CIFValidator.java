package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

public class CIFValidator extends BaseValidator implements FieldValidator {

  private static final String REGEX_CIF = "^[ABCDEFGHJKLMNPQS]\\d{7}[0-9,A-J]";

  public CIFValidator() {
    super();
  }

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_CIF);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
