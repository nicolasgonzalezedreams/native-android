package com.odigeo.app.android.view.dialogs;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.DatePicker;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.utils.AppLocaleDatePickerDialog;
import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by carlos.navarrete on 11/09/15.
 */
@Deprecated public class DateDialog {

  public static final String TIMEZONE_GMT = "GMT";
  //UTC Timezone
  protected Date date;
  protected boolean showDays;
  protected boolean showMonths;
  protected boolean showYears;
  protected String dateFormat;
  //UTC Timezone
  protected Date minDate;
  //UTC Timezone
  protected Date maxDate;
  AppLocaleDatePickerDialog datePickerDialog;
  DatePickerDialog mDatePickerDialog;
  //Labels
  private String okLabel;
  private String cancelLabel;
  private Context mContext;
  private String mStringOk;
  private String mStringCancel;
  /**
   * Builds an object of DateDialog
   *
   * @param mContext Uses the Context of the Activity to set context where the dialog will dislpay
   * @param okLabelString The value of label of confirmation button
   * @param cancelLabelString The value of the label of  cancel button
   */
  public DateDialog(Context mContext, String okLabelString, String cancelLabelString) {
    this.mContext = mContext;
    mStringCancel = cancelLabelString;
    mStringOk = okLabelString;
  }

  /**
   * Creates and retrieves an object AppLocalePickerDialog
   *
   * @param year The year of the date
   * @param month The month of the date
   * @param day the day of the date
   */
  public AppLocaleDatePickerDialog createDateDialog(int year, int month, int day) {

    datePickerDialog = new AppLocaleDatePickerDialog(mContext, null, year, month, day);
    final DatePicker datePicker = datePickerDialog.getDatePicker();

    if (maxDate != null) {
      datePicker.setMaxDate(maxDate.getTime());
    }

    if (minDate != null) {
      datePicker.setMinDate(minDate.getTime());
    }

        /*if (!showDays) {
            findAndHideField(datePicker, "mDaySpinner");
        }

        if (!showMonths) {
            findAndHideField(datePicker, "mMonthSpinner");
        }

        if (!showYears) {
            findAndHideField(datePicker, "mYearSpinner");
        }*/

    //        datePickerDialog.setButton(
    //            DialogInterface.BUTTON_NEGATIVE,
    //            getCancelLabel(),
    //            (DialogInterface.OnClickListener) null);
    ////
    //        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, getOKLabel(), new DialogInterface.OnClickListener() {
    //                                       @Override
    //                                       public void onClick(DialogInterface dialog, int which) {
    //
    //                                           //Calendar newCalendar = Calendar.getInstance();
    //                                           Calendar newCalendar = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE_GMT));
    //
    //                                           newCalendar.set(Calendar.YEAR, datePicker.getYear());
    //                                           newCalendar.set(Calendar.MONTH, datePicker.getMonth());
    //                                           newCalendar.set(Calendar.DATE, datePicker.getDayOfMonth());
    //
    //                                           setDate(newCalendar.getTime());
    //                                       }
    //                                   });

    return datePickerDialog;
  }

  public String getOKLabel() {
    return mStringOk;
  }

  public void setOKLabel(String label) {
    okLabel = label;
  }

  public String getCancelLabel() {
    return mStringCancel;
  }

  //Date should have UTC time zone
  public void setCancelLabel(String label) {
    cancelLabel = label;
  }

  protected void findAndHideField(DatePicker datepicker, String name) {
    try {
      Field field = DatePicker.class.getDeclaredField(name);
      field.setAccessible(true);
      View fieldInstance = (View) field.get(datepicker);
      fieldInstance.setVisibility(View.GONE);
    } catch (Exception e) {
      Logger.getLogger(DateDialog.class.getName()).log(Level.SEVERE, Constants.TAG_LOG, e);
    }
  }

  public Date getMinDate() {
    return minDate;
  }

  //This date is set with the Default Timezone
  public void setMinDate(Date minDate) {
    this.minDate = startOfTheDay(minDate, TimeZoneType.LOCAL);
  }

  public Date getMaxDate() {
    return maxDate;
  }

  //Set the max date accepted by the picker, but set the time to the last time of the day
  //This date is set with the Default Timezone
  public void setMaxDate(Date maxDate) {
    this.maxDate = endOfTheDay(maxDate, TimeZoneType.LOCAL);
  }

  //Set the min date accepted by the picker, but set the time to the first time of the day
  public Date startOfTheDay(Date date, TimeZoneType timeZoneType) {
    if (date == null) {
      return null;
    }

    return resetTimeToDate(date, timeZoneType);
    //return Util.resetTimeToDate(date);
  }

  public Date endOfTheDay(Date date, TimeZoneType timeZoneType) {
    if (date == null) {
      return date;
    }

    //Calendar calendar = Calendar.getInstance();
    Calendar calendar = null;

    if (timeZoneType == TimeZoneType.LOCAL) {
      calendar = Calendar.getInstance();
    } else if (timeZoneType == TimeZoneType.UTC) {
      calendar = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE_GMT));
    }

    //calendar.setTime(Util.resetTimeToDate(date));
    calendar.setTime(resetTimeToDate(date, timeZoneType));
    calendar.add(Calendar.DATE, 1);
    calendar.add(Calendar.SECOND, -1);

    return calendar.getTime();
  }

  //TODO Modify Util method to parameterized the timezone
  private Date resetTimeToDate(Date date, TimeZoneType timezone) {
    if (date == null) {
      return null;
    }

    //final Calendar dateWithoutTime = Calendar.getInstance();

    Calendar dateWithoutTime = null;

    if (timezone == TimeZoneType.LOCAL) {
      dateWithoutTime = Calendar.getInstance();
    } else if (timezone == TimeZoneType.UTC) {
      dateWithoutTime = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE_GMT));
    }

    dateWithoutTime.setTime(date);
    dateWithoutTime.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
    dateWithoutTime.set(Calendar.MINUTE, 0);                 // set minute in hour
    dateWithoutTime.set(Calendar.SECOND, 0);                 // set second in minute
    dateWithoutTime.set(Calendar.MILLISECOND, 0);            // set millis in second

    return dateWithoutTime.getTime();
  }

  //It has Default time zone
  public final Date getDateInDefaultTimeZone() {

    if (date == null) {
      return null;
    }

    Calendar calendarGmtTimezone = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE_GMT));
    calendarGmtTimezone.setTime(date);

    Calendar calendarDefaultTimezone = Calendar.getInstance();

    calendarDefaultTimezone.set(Calendar.YEAR,
        calendarGmtTimezone.get(Calendar.YEAR));            // set hour to midnight
    calendarDefaultTimezone.set(Calendar.MONTH,
        calendarGmtTimezone.get(Calendar.MONTH));                 // set minute in hour
    calendarDefaultTimezone.set(Calendar.DAY_OF_MONTH,
        calendarGmtTimezone.get(Calendar.DAY_OF_MONTH));
    calendarDefaultTimezone.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
    calendarDefaultTimezone.set(Calendar.MINUTE, 0);                 // set minute in hour
    calendarDefaultTimezone.set(Calendar.SECOND, 0);                 // set second in minute
    calendarDefaultTimezone.set(Calendar.MILLISECOND, 0);            // set millis in second

    return calendarDefaultTimezone.getTime();
  }

  public AppLocaleDatePickerDialog showDialog(final OnGetDateSelectedListener listener) {
    Calendar calendar;
    if (date == null) {
      //Get Today's date
      calendar = Calendar.getInstance();
    } else {
      //Create calendar, with UTC timezone
      calendar = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE_GMT));
      calendar.setTime(date);
    }

    datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getCancelLabel(),
        (DialogInterface.OnClickListener) null);
    datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, getOKLabel(),
        new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialog, int which) {

            //Calendar newCalendar = Calendar.getInstance();
            Calendar newCalendar = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE_GMT));

            newCalendar.set(Calendar.YEAR, datePickerDialog.getDatePicker().getYear());
            newCalendar.set(Calendar.MONTH, datePickerDialog.getDatePicker().getMonth());
            newCalendar.set(Calendar.DATE, datePickerDialog.getDatePicker().getDayOfMonth());

            listener.onGetDate(newCalendar.getTime());
            //setDate(newCalendar.getTime());
          }
        });
    return datePickerDialog;
  }

  enum TimeZoneType {
    LOCAL, UTC
  }
}
