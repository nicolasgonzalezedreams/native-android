package com.odigeo.app.android.lib.utils.events;

/**
 * Created by emiliano.desantis on 22/10/2014.
 */
public class GATrackingEvent {
  private String action;
  private String label;
  private String category;

  public GATrackingEvent(String category, String action, String label) {
    this.action = action;
    this.label = label;
    this.category = category;
  }

  public final String getAction() {
    return action;
  }

  public final void setAction(String action) {
    this.action = action;
  }

  public final String getLabel() {
    return label;
  }

  public final void setLabel(String label) {
    this.label = label;
  }

  public final String getCategory() {
    return category;
  }

  public final void setCategory(String category) {
    this.category = category;
  }
}
