package com.odigeo.app.android.view.helpers;

import android.content.Context;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.tools.DateHelperInterface;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static java.util.Calendar.DATE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

public class DateHelper implements DateHelperInterface {

  public static final String yyyy_MM_dd_format = "yyyy-MM-dd";
  private static final long MILLISECONDS_IN_A_MINUTE = 60000;
  private static final String GMT_TIME_ZONE = "GMT";
  private static final int N_MONTHS = 12;
  private static final int OCTOBER = 10;

  @Override public String millisecondsToDate(long dateInMilliseconds, String dateFormat) {
    String dateResult = "";

    if (dateInMilliseconds != 0) {
      Date date = new Date(dateInMilliseconds);
      SimpleDateFormat format = new SimpleDateFormat(dateFormat, LocaleUtils.getCurrentLocale());
      dateResult = format.format(date);
    }
    return dateResult;
  }

  @Override public String millisecondsToDateGMT(long dateInMilliseconds, String dateFormat) {
    String dateResult = "";

    if (dateInMilliseconds != 0) {
      Date date = new Date(dateInMilliseconds);
      SimpleDateFormat format = new SimpleDateFormat(dateFormat, LocaleUtils.getCurrentLocale());
      format.setTimeZone(TimeZone.getTimeZone(GMT_TIME_ZONE));
      dateResult = format.format(date);
    }
    return dateResult;
  }

  @Override public long millisecondsLeastOffset(long date) {
    return date - TimeZone.getDefault().getOffset(date);
  }

  public String getTime(long date, Context context) {
    return millisecondsToDate(date, context.getResources().getString(R.string.templates_card_time));
  }

  public String getTimeGMT(long date, Context context) {
    return millisecondsToDateGMT(date, context.getResources().getString(R.string.templates__time1));
  }

  public String getDayAndMonth(long date, Context context) {
    return millisecondsToDate(date, context.getResources().getString(R.string.card_date_format));
  }

  public String getDayAndMonthGMT(long date, Context context) {
    return millisecondsToDateGMT(date, context.getResources().getString(R.string.card_date_format));
  }

  public boolean isSameDay(long date1, long date2, Context context) {
    String day1, day2;
    day1 = getDayAndMonth(date1, context);
    day2 = getDayAndMonth(date2, context);
    return day1.equals(day2);
  }

  @Override public long minutesToMiliseconds(long date) {
    return date * MILLISECONDS_IN_A_MINUTE;
  }

  @Override public long getTimeStampFromDayMonthYear(int year, int month, int day) {
    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(GMT_TIME_ZONE));
    calendar.set(year, month - 1, day, 0, 0);
    return calendar.getTimeInMillis();
  }

  @Override public List<Integer> getDayMonthYearFromTimeStamp(long date) {
    Date dateToTransform = new Date(date);
    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(GMT_TIME_ZONE));
    cal.setTime(dateToTransform);

    List<Integer> dayMonthYear = new ArrayList<>();
    dayMonthYear.add(cal.get(Calendar.DAY_OF_MONTH));
    dayMonthYear.add(cal.get(MONTH));
    dayMonthYear.add(cal.get(YEAR));
    return dayMonthYear;
  }

  @Override public int getDay(long date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(date);
    return calendar.get(Calendar.DAY_OF_MONTH);
  }

  @Override public int getMonth(long date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(date);
    return calendar.get(MONTH) + 1;
  }

  @Override public int getYear(long date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(date);
    return calendar.get(YEAR);
  }

  @Override public int getHour(long date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(date);
    return calendar.get(Calendar.HOUR_OF_DAY);
  }

  @Override public int getMinute(long date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(date);
    return calendar.get(Calendar.MINUTE);
  }

  @Override public int getAge(long birthdate) {
    Calendar currentDate = Calendar.getInstance();

    Calendar birthdateCalendar = Calendar.getInstance();
    birthdateCalendar.setTimeInMillis(birthdate);

    int age = currentDate.get(YEAR) - birthdateCalendar.get(YEAR);
    if (birthdateCalendar.get(MONTH) > currentDate.get(MONTH) || (currentDate.get(MONTH)
        == birthdateCalendar.get(MONTH) && birthdateCalendar.get(DATE) > currentDate.get(DATE))) {
      age--;
    }
    return age;
  }

  @Override public long dateToMiliseconds(String date, String dateFormat) {
    SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.getDefault());
    format.setTimeZone(TimeZone.getTimeZone(GMT_TIME_ZONE));
    try {
      Date formattedDate = format.parse(date);
      return formattedDate.getTime();
    } catch (ParseException e) {
      e.printStackTrace();
      return 0;
    }
  }

  @Override public long getCurrentSystemMillis() {
    return System.currentTimeMillis();
  }

  public int getCurrentYear() {
    return Calendar.getInstance().get(YEAR);
  }

  @Override public int getCurrentYearLastTwoCharacters() {
    String year = String.valueOf(Calendar.getInstance().get(YEAR));
    int yearLastTwoChar = Integer.valueOf(year.substring(year.length() - 2));
    return yearLastTwoChar;
  }

  @Override public int getCurrentMonth() {
    return Calendar.getInstance().get(MONTH) + 1;
  }

  public List<String> getAllMonth() {
    List<String> months = new ArrayList<>();
    for (int i = 1; i < (N_MONTHS + 1); i++) {
      if (i < OCTOBER) {
        months.add(0 + String.valueOf(i));
      } else {
        months.add(String.valueOf(i));
      }
    }
    return months;
  }

  public List<String> getFollowingYearsFromNow(int howManyFollowingYears) {
    List<String> years = new ArrayList<>();
    int currentYear = Calendar.getInstance().get(Calendar.YEAR);
    String tempCurrentYear = String.valueOf(currentYear);
    currentYear = Integer.valueOf(tempCurrentYear.substring(tempCurrentYear.length() - 2));
    for (int i = 0; i < howManyFollowingYears; i++) {
      years.add(String.valueOf(currentYear + i));
    }
    return years;
  }

  public SimpleDateFormat getGmtDateFormat(String format) {
    SimpleDateFormat result = new SimpleDateFormat(format, LocaleUtils.getCurrentLocale());
    result.setTimeZone(TimeZone.getTimeZone(GMT_TIME_ZONE));
    return result;
  }
}