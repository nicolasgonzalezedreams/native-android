package com.odigeo.app.android.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.interactors.CountriesInteractor;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 16/02/16.
 */
public class CountriesUpdateService extends Service
    implements CountriesInteractor.OnUpdateListener {

  private final CountriesInteractor mCountriesInteractor =
      AndroidDependencyInjector.getInstance().provideCountriesInteractor();

  @Nullable @Override public IBinder onBind(Intent intent) {
    return null;
  }

  @Override public int onStartCommand(Intent intent, int flags, int startId) {
    mCountriesInteractor.updateCountries(this);
    return START_NOT_STICKY;
  }

  @Override public void onFinish() {
    stopSelf();
  }
}
