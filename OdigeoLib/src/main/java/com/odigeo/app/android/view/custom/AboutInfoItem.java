package com.odigeo.app.android.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.helpers.DrawableUtils;

public class AboutInfoItem extends LinearLayout {

  private String mText;
  private int mImageId;
  private TextView mTextView;
  private ImageView mImageView;

  public AboutInfoItem(Context context) {
    super(context);
    inflateLayout();
  }

  public AboutInfoItem(Context context, AttributeSet attrs) {
    super(context, attrs);
    inflateLayout();
    setAttributs(attrs);
  }

  private void setAttributs(AttributeSet attributs) {
    TypedArray typedArray =
        getContext().obtainStyledAttributes(attributs, R.styleable.AboutInfoItem);

    mText = typedArray.getString(R.styleable.AboutInfoItem_text);
    mImageId =
        typedArray.getResourceId(R.styleable.AboutInfoItem_src, R.drawable.confirmation_info);

    typedArray.recycle();

    if (mText != null) {
      mTextView.setText(LocalizablesFacade.getString(getContext(), mText));
    }

    Drawable imageTinted =
        DrawableUtils.getTintedResource(mImageId, R.color.primary_brand, getContext());
    mImageView.setImageDrawable(imageTinted);
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.layout_about_info_item, this);
    mTextView = (TextView) findViewById(R.id.text_info_item);
    mImageView = (ImageView) findViewById(R.id.icon_info_item);
  }

  public String getText() {
    return mText;
  }

  public void setText(String text) {
    mText = text;
    mTextView.setText(mText);
  }

  public int getImageId() {
    return mImageId;
  }

  public void setImageId(int imageId) {
    mImageId = imageId;
    mImageView.setImageResource(mImageId);
  }
}
