package com.odigeo.app.android.notification;

import android.content.Context;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.interactors.notification.OdigeoNotificationManagerInterface;

public class OdigeoNotificationManager implements OdigeoNotificationManagerInterface {

  private OdigeoNotificationDirector odigeoNotificationDirector;
  private Context context;

  public OdigeoNotificationManager(Context context) {
    this.context = context;
    odigeoNotificationDirector = new OdigeoNotificationDirector();
  }

  @Override public void sendBookingStatusNotification(Booking booking) {
    BookingStatusOdigeoNotificationBuilder bookingStatusNotificationBuilder =
        new BookingStatusOdigeoNotificationBuilder(context);
    bookingStatusNotificationBuilder.setBooking(booking);
    odigeoNotificationDirector.setOdigeoNotificationBuilder(bookingStatusNotificationBuilder);
    odigeoNotificationDirector.createNotification();
  }
}
