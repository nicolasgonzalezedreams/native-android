package com.odigeo.app.android.view;

import com.odigeo.dataodigeo.tracker.TrackerConstants;

public class PaypalExternalPaymentView extends ExternalPaymentView {

  private static final String MAIN_PAGE_PAYPAL = "/cgi-bin/webscr?cmd=";

  @Override public void trackGoBack() {
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_PAYPAL_PAGE,
        TrackerConstants.ACTION_NAVIGATION_ELEMENTS, TrackerConstants.LABEL_GO_BACK);
  }

  private boolean isUserInPaypalPaymentForm() {
    return getCurrentUrl() != null && !getCurrentUrl().contains(MAIN_PAGE_PAYPAL);
  }

  @Override protected boolean shouldInterceptPageChange(String url) {
    return true;
  }

  @Override public void onBackPressed() {
    if (!isUserInPaypalPaymentForm()) {
      clickOnCancelWebViewButton();
    }
    super.onBackPressed();
  }
}
