package com.odigeo.app.android.view;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.view.adapters.BaseAdaptersFactory;
import com.odigeo.app.android.view.animations.CollapseAndExpandAnimationUtil;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.custom.BaggageCollectionView;
import com.odigeo.app.android.view.custom.ClearableEditText;
import com.odigeo.app.android.view.custom.SpinnerWithTitle;
import com.odigeo.app.android.view.dialogs.DatePickerDialogView;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.app.android.view.helpers.StringFormatHelper;
import com.odigeo.app.android.view.interfaces.BaggageCollectionListener;
import com.odigeo.app.android.view.interfaces.ListenerPassengerNavigator;
import com.odigeo.app.android.view.interfaces.ListenerPassengerWidget;
import com.odigeo.app.android.view.textwatchers.IdentificationBaseTextWatcherClient;
import com.odigeo.app.android.view.textwatchers.PassengerBaseTextWatchersClient;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.RequestValidationHandler;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.shoppingCart.Carrier;
import com.odigeo.data.entity.shoppingCart.Itinerary;
import com.odigeo.data.entity.shoppingCart.ItinerarySortCriteria;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.data.entity.shoppingCart.TravellerIdentificationType;
import com.odigeo.data.entity.shoppingCart.TravellerRequiredFields;
import com.odigeo.data.entity.shoppingCart.TravellerType;
import com.odigeo.data.entity.shoppingCart.request.BaggageSelectionRequest;
import com.odigeo.data.entity.shoppingCart.request.TravellerRequest;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.presenter.PassengerWidgetPresenter;
import com.odigeo.presenter.contracts.views.PassengerWidget;
import com.odigeo.tools.DateHelperInterface;
import com.odigeo.validations.BirthdateValidator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.text.Html.fromHtml;
import static com.odigeo.app.android.lib.consts.Emojis.DIRECT_HIT;
import static com.odigeo.app.android.view.constants.OneCMSKeys.PASSENGER_TYPE_PREMIUM;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_CUSTOMER_DETAILS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_PAX_BAGGAGE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_FLIGHTS_PAX;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_PARTIAL_PAX_PAYS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SAME_BAGGAGE_AUTO_UNCHECKED;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SAME_BAGGAGE_CHECKED;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SAME_BAGGAGE_UNCHECKED;

public class PassengerWidgetView extends BaseCustomWidget<PassengerWidgetPresenter>
    implements PassengerWidget, BaggageCollectionListener {

  private static final int NO_MEMBERSHIP = -1;
  private final Itinerary itinerary;
  DialogFragment mDialogBirthDateFragment;
  DialogFragment mIdentificationExpirationDateFragment;
  private TextView mTvPassengerWidgetTitle;
  private RelativeLayout mRlHeaderPassengerItem;
  private RelativeLayout mRlResidentGroupContainer;
  private ImageView mCnivPassengerImageProfile, ivMembershipStar;
  private Spinner mSpwtPassengers;
  private TextView titleHeader;
  private SpinnerWithTitle mSpwtResidentGroup, mSpwtResidentLocality;
  private TextView mTvPassengers;
  private TextInputLayout mTilName;
  private TextInputLayout mTilMiddleName;
  private TextInputLayout mTilFirstLastName;
  private TextInputLayout mTilSecondLastName;
  private TextInputLayout mTilBirthdate;
  private TextInputLayout mTilCountryOfResidence;
  private TextInputLayout mTilNationality;
  private RelativeLayout mRlIdentificationContainer;
  private SpinnerWithTitle mSpwtIdentificationType;
  private TextInputLayout mTilIdentification;
  private TextInputLayout mTilIdentificationExpirationDate;
  private TextInputLayout mTilIdentificationCountry;
  private Button mBtnOpenOrCloseDetails;
  private ClearableEditText mCetFrequentFlyer;
  private int mPassengerWidgetPosition;
  private int mTravellerPreviousItemSelected = -1;
  private PassengerBaseTextWatchersClient mPassengerBaseTextWatcherClient;
  private TravellerRequiredFields mPassengerInformation;
  private List<UserFrequentFlyer> mFrequentFlyerSelectedList = new ArrayList<>();
  private List<UserTraveller> mTravellersListForWidget;
  private IdentificationBaseTextWatcherClient mIdentificationBaseTextWatcherClient;
  private android.support.v4.app.FragmentManager mFragmentManager;
  private DateHelperInterface mDateHelperInterface;
  private ListenerPassengerNavigator mListenerPassengerNavigator;
  private ListenerPassengerWidget mListenerPassengerWidget;
  private String mPassengerTypeTitleAdult, mPassengerTypeTitleChild, mPassengerTypeTitleInfant;
  private String mPassengerCardTitle;
  private String mEditCollapse, mCloseCollapse;
  private String mNewPassenger;
  private String errorChildOrBabyBirthdateTitle, errorChildBirthdateBody, errorBabyBirthdateBody,
      errorNewSearch;
  private FrameLayout mFlBaggageInformationContainer;
  private BaggageCollectionView baggageCollectionView;
  private RelativeLayout mRlFieldsContainer;
  private Country mResidentCountry;
  private int passengerPositionInSpinner;
  private UserTraveller currentUserTravellerInWidget;
  private List<Traveller> mTravellers;
  private String commonCancel, commonContinue;
  private LinearLayout applyBaggageSelection;
  private Switch applyBaggageSelectionSwitch;
  private TextView applyBaggageSelectionEmoji;
  private boolean hasMoreNonChildPassengers;
  private String passengerGender;
  private RelativeLayout titleMrClickableArea, titleMrsClickableArea;
  private TextView titleMrLabel, titleMrsLabel;

  public PassengerWidgetView(Context context, FragmentManager fragmentManager,
      int passengerWidgetPosition, TravellerRequiredFields passengerInformation,
      DateHelperInterface dateHelperInterface,
      ListenerPassengerNavigator listenerPassengerNavigator,
      List<UserTraveller> travellersListForWidget, ListenerPassengerWidget listenerPassengerWidget,
      Itinerary itinerary, List<Traveller> travellers, TravelType travelType) {
    super(context);
    mPassengerWidgetPosition = passengerWidgetPosition;
    mPassengerInformation = passengerInformation;
    this.itinerary = itinerary;
    mFragmentManager = fragmentManager;
    mDateHelperInterface = dateHelperInterface;
    mListenerPassengerNavigator = listenerPassengerNavigator;
    mTravellersListForWidget = travellersListForWidget;
    mListenerPassengerWidget = listenerPassengerWidget;
    mTravellers = travellers;
    initPresenter();
    initPassengerTextInputLayoutClient();
    initIdentificationTextInputLayoutClient();
    initAdapters();
    setListeners();
    configureFields();
    addBaggage(itinerary, travelType);
  }

  private void initPresenter() {
    presenter.setPassengerInformation(mPassengerInformation,
        new BirthdateValidator(mDateHelperInterface, mPassengerInformation));
  }

  private void addBaggage(Itinerary itinerary, TravelType travelType) {
    baggageCollectionView =
        new BaggageCollectionView(getContext(), mPassengerInformation.getBaggageConditions(),
            itinerary, mTravellers, mPassengerWidgetPosition, travelType, this);
    mFlBaggageInformationContainer.addView(baggageCollectionView);
  }

  @Override public void initComponent() {
    mTvPassengerWidgetTitle = (TextView) findViewById(R.id.tvPassengerItemTitle);
    mTvPassengers = (TextView) findViewById(R.id.tvPassengers);
    mRlHeaderPassengerItem = (RelativeLayout) findViewById(R.id.rlHeaderPassengerItem);
    mCnivPassengerImageProfile = (ImageView) findViewById(R.id.cnivPassengerImageProfile);
    ivMembershipStar = (ImageView) findViewById(R.id.ivMembershipStar);
    mSpwtPassengers = (Spinner) findViewById(R.id.spwtPassengers);
    titleHeader = (TextView) findViewById(R.id.titleHeader);
    mTilName = (TextInputLayout) findViewById(R.id.tilName);
    mTilMiddleName = (TextInputLayout) findViewById(R.id.tilMiddleName);
    mTilFirstLastName = (TextInputLayout) findViewById(R.id.tilFirstLastName);
    mTilSecondLastName = (TextInputLayout) findViewById(R.id.tilSecondLastName);
    mTilBirthdate = (TextInputLayout) findViewById(R.id.tilBirthdate);
    mTilCountryOfResidence = (TextInputLayout) findViewById(R.id.tilCountryOfResident);
    mTilNationality = (TextInputLayout) findViewById(R.id.tilNationality);
    mRlIdentificationContainer = (RelativeLayout) findViewById(R.id.rlIdentificationContainer);
    mSpwtIdentificationType = (SpinnerWithTitle) findViewById(R.id.spwtIdentificationType);
    mTilIdentification = (TextInputLayout) findViewById(R.id.tilIdentification);
    mTilIdentificationExpirationDate =
        (TextInputLayout) findViewById(R.id.tilIdentificationExpirationDate);
    mTilIdentificationCountry = (TextInputLayout) findViewById(R.id.tilIdentificationCountry);
    mCetFrequentFlyer = (ClearableEditText) findViewById(R.id.cetFrequentFlyer);
    mBtnOpenOrCloseDetails = (Button) findViewById(R.id.btnOpenOrCloseDetails);
    mFlBaggageInformationContainer = (FrameLayout) findViewById(R.id.flBaggageInformationContainer);
    mCetFrequentFlyer.getEditText().setFocusable(false);
    mCetFrequentFlyer.getEditText().setCursorVisible(false);
    mRlFieldsContainer = (RelativeLayout) findViewById(R.id.rlFieldsContainer);
    mRlResidentGroupContainer = (RelativeLayout) findViewById(R.id.rlResidentGroupContainer);
    mSpwtResidentGroup = (SpinnerWithTitle) findViewById(R.id.spwtResidentGroup);
    mSpwtResidentLocality = (SpinnerWithTitle) findViewById(R.id.spwtResidentLocality);
    applyBaggageSelection = (LinearLayout) findViewById(R.id.applyBaggageSelection);
    applyBaggageSelectionSwitch = (Switch) findViewById(R.id.applyBaggageSelectionSwitch);
    applyBaggageSelectionEmoji = (TextView) findViewById(R.id.applyBaggageSelectionEmoji);
    titleMrClickableArea = (RelativeLayout) findViewById(R.id.titleMrClickableArea);
    titleMrsClickableArea = (RelativeLayout) findViewById(R.id.titleMrsClickableArea);
    titleMrLabel = (TextView) findViewById(R.id.titleMrLabel);
    titleMrsLabel = (TextView) findViewById(R.id.titleMrsLabel);
  }

  @Override public int getComponentLayout() {
    return R.layout.passenger_item;
  }

  @Override public void initOneCMSText() {
    mNewPassenger = localizables.getString(OneCMSKeys.PASSENGER_TYPE_NEW);
    mEditCollapse = localizables.getString(OneCMSKeys.PASSENGER_COLLAPSE_EDIT);
    mCloseCollapse = localizables.getString(OneCMSKeys.PASSENGER_COLLAPSE_CLOSE);
    errorChildOrBabyBirthdateTitle =
        localizables.getString(OneCMSKeys.PASSENGER_CHILD_INFANT_BIRTHDATE_ERROR_TITTLE);
    errorChildBirthdateBody =
        localizables.getString(OneCMSKeys.PASSENGER_CHILD_BIRTHDATE_ERROR_BODY);
    errorBabyBirthdateBody =
        localizables.getString(OneCMSKeys.PASSENGER_INFANT_BIRTHDATE_ERROR_BODY);
    mPassengerTypeTitleAdult = localizables.getString(OneCMSKeys.PASSENGER_TYPE_ADULT);
    mPassengerTypeTitleChild = localizables.getString(OneCMSKeys.PASSENGER_TYPE_CHILD);
    mPassengerTypeTitleInfant = localizables.getString(OneCMSKeys.PASSENGER_TYPE_INFANT);
    mPassengerCardTitle = localizables.getString(OneCMSKeys.TITLE_CARD_PASSENGER);
    mTvPassengers.setText(localizables.getString(OneCMSKeys.SSO_TRAVELLERS_CONTACT_DETAIL));
    titleHeader.setText(localizables.getString(OneCMSKeys.PASSENGER_TITTLE));
    mTilName.setHint(localizables.getString(OneCMSKeys.HINT_NAME));
    mTilMiddleName.setHint(localizables.getString(OneCMSKeys.HINT_MIDDLE_NAME));
    mTilFirstLastName.setHint(localizables.getString(OneCMSKeys.HINT_FIRST_LASTNAME));
    mTilSecondLastName.setHint(localizables.getString(OneCMSKeys.HINT_SECOND_LASTNAME));
    mTilBirthdate.setHint(localizables.getString(OneCMSKeys.HINT_BIRTH_DATE));
    mTilCountryOfResidence.setHint(localizables.getString(OneCMSKeys.HINT_COUNTRY_RESIDENCE));
    mTilNationality.setHint(localizables.getString(OneCMSKeys.HINT_NATIONALITY));
    mSpwtIdentificationType.setText(localizables.getString(OneCMSKeys.HINT_IDENTIFICATION_SECTION));
    mTilIdentification.setHint(localizables.getString(OneCMSKeys.HINT_IDENTIFICATION));
    mTilIdentificationExpirationDate.setHint(
        localizables.getString(OneCMSKeys.HINT_IDENTIFICATION_EXPIRATION_DATE));
    mTilIdentificationCountry.setHint(
        localizables.getString(OneCMSKeys.HINT_IDENTIFICATION_COUNTRY));
    mCetFrequentFlyer.setHint(localizables.getString(OneCMSKeys.FREQUENT_FLYER_OPTIONAL));
    mSpwtResidentGroup.setText(localizables.getString(OneCMSKeys.HINT_RESIDENT_TITLE));
    errorNewSearch = localizables.getString(OneCMSKeys.BOOKING_OUTDATED_NEW_SEARCH);
    commonCancel = localizables.getString(OneCMSKeys.COMMON_CANCEL);
    commonContinue = localizables.getString(OneCMSKeys.COMMON_BUTTONCONTINUE);
    applyBaggageSelectionSwitch.setText(
        fromHtml(localizables.getRawString(OneCMSKeys.PASSENGER_BAGGAGE_APPLY_BAGGAGE_SELECTION)));
    applyBaggageSelectionEmoji.setText(DIRECT_HIT);
    passengerGender = localizables.getString(OneCMSKeys.PASSENGER_TITLE_MR);
    titleMrLabel.setText(localizables.getString(OneCMSKeys.PASSENGER_TITLE_MR));
    titleMrsLabel.setText(localizables.getString(OneCMSKeys.PASSENGER_TITLE_MRS));
  }

  @Override protected PassengerWidgetPresenter setPresenter() {
    return dependencyInjector.providePassengerWidgetPresenter(this);
  }

  private void initIdentificationTextInputLayoutClient() {
    mIdentificationBaseTextWatcherClient =
        new IdentificationBaseTextWatcherClient(getContext(), mTilIdentification,
            mPassengerBaseTextWatcherClient);
  }

  private void initAdapters() {
    initPassengerNamesAdapter();
    initTypeOfIdentificationAdapter();
  }

  private void initPassengerNamesAdapter() {
    List<UserTraveller> localTravellersByGroup = new ArrayList<>();

    if ((!mTravellersListForWidget.isEmpty())) {
      UserTraveller defaultUserTraveller =
          presenter.getEmptyUserTraveller(mPassengerWidgetPosition + 1, mNewPassenger);
      localTravellersByGroup.add(defaultUserTraveller);
    }

    localTravellersByGroup.addAll(presenter.removeNullUserTravellers(mTravellersListForWidget));

    List<String> travellersName =
        presenter.formatPassengersNameToShowOnSpinner(localTravellersByGroup);
    ArrayAdapter<String> passengersAdapter =
        BaseAdaptersFactory.createStringAdapter(getContext(), travellersName);

    mSpwtPassengers.setAdapter(passengersAdapter);

    presenter.setUserTravellers(localTravellersByGroup);
    presenter.setInitialSpinnerTraveller();
  }

  private void initTypeOfIdentificationAdapter() {
    String identification;
    List<String> identificationSpinnerList = new ArrayList<>();

    removeBirthdayFromIdentificationPicker();
    for (int i = 0; i < mPassengerInformation.getIdentificationTypes().size(); i++) {
      identification = mPassengerInformation.getIdentificationTypes().get(i).value();
      identificationSpinnerList.add(identification);
    }

    ArrayAdapter<String> identificationTraveller =
        BaseAdaptersFactory.createStringAdapter(getContext(), identificationSpinnerList);
    mSpwtIdentificationType.setAdapter(identificationTraveller);
  }

  @Override public void initResidentGroupAdapter(List<String> residentGroup) {
    ArrayAdapter<String> residentGroupAdapter =
        BaseAdaptersFactory.createStringAdapter(getContext(), residentGroup);
    mSpwtResidentGroup.setAdapter(residentGroupAdapter);
  }

  @Override public void initResidentLocalitiesAdapter(List<String> residentLocalities) {
    ArrayAdapter<String> residentLocalitiesAdapter =
        BaseAdaptersFactory.createStringAdapter(getContext(), residentLocalities);
    mSpwtResidentLocality.setAdapter(residentLocalitiesAdapter);
  }

  private void removeBirthdayFromIdentificationPicker() {
    for (TravellerIdentificationType option : mPassengerInformation.getIdentificationTypes()) {
      if (option.equals(TravellerIdentificationType.BIRTH_DATE)) {
        mPassengerInformation.getIdentificationTypes().remove(option);
        break;
      }
    }
  }

  private void setListeners() {
    setSpinnerTravellerListener();
    setGenderListeners();
    setSpinnerIdenticationTypeListener();
    setTextInputDateOfBirthListener();
    setTextInputFrequentFlyerListener();
    setTextInputCountryResidentListener();
    setTextInputCountryNationalityListener();
    setTextInputIdentificationCountryListener();
    setTextInputIdentificationExpirationDate();
    setOpenOrCloseDetailsListener();
  }

  public void setPassengerAsDefaultPassenger(boolean hasMoreNonChildPassengers) {
    getUserTravellerAndPositionToFillContactWidget();
    mListenerPassengerWidget.onSetDefaultPassengerInformation(currentUserTravellerInWidget,
        passengerPositionInSpinner);
    String label = LABEL_PARTIAL_PAX_PAYS + (mPassengerWidgetPosition + 1);
    trackerController.trackAnalyticsEvent(CATEGORY_FLIGHTS_PAX, ACTION_CUSTOMER_DETAILS, label);
    this.hasMoreNonChildPassengers = hasMoreNonChildPassengers;
  }

  private void getUserTravellerAndPositionToFillContactWidget() {
    currentUserTravellerInWidget = presenter.getSelectedSpinnerTraveller();
    passengerPositionInSpinner = mSpwtPassengers.getSelectedItemPosition();
    if (comesFromInsurances(currentUserTravellerInWidget)) {
      presenter.refreshUserTravellers();
      currentUserTravellerInWidget = presenter.getSelectedSpinnerTraveller();
      passengerPositionInSpinner = getWidgetPosition();
    } else {
      passengerPositionInSpinner = mSpwtPassengers.getSelectedItemPosition();
      if (thereIsNoSpinner()) {
        passengerPositionInSpinner = getWidgetPosition();
      }
    }
  }

  private boolean comesFromInsurances(UserTraveller currentUserTravellerInWidget) {
    return ((currentUserTravellerInWidget == null));
  }

  private boolean thereIsNoSpinner() {
    return ((passengerPositionInSpinner == -1));
  }

  private void setSpinnerTravellerListener() {
    mSpwtPassengers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String travellerName = (String) mSpwtPassengers.getSelectedItem();
        if (travellerName.equals(mNewPassenger)) {
          expandFieldsContainer();
          presenter.cleanPassengerFields();
          presenter.setSelectedSpinnerTraveller(0);
          checkIfHaveToLoadInformationInContactWidget();
        } else if (isPassengerSelectedInAnotherTravellerSpinner(travellerName)) {
          selectPreviousPassenger();
        } else {
          checkIfHaveToLoadInformationInContactWidget();
          mTravellerPreviousItemSelected = position;
          presenter.loadInformationInUI(position);
        }
        presenter.onChangeMembership(travellerName);
      }

      @Override public void onNothingSelected(AdapterView<?> parent) {

      }
    });
  }

  private void selectPreviousPassenger() {
    mSpwtPassengers.setSelection(mTravellerPreviousItemSelected);
  }

  @Override public boolean isPassengerSelectedInAnotherTravellerSpinner(String travellerName) {
    return (mListenerPassengerWidget.IsPassengerSelectedInAnotherWidgetSpinner(
        mPassengerWidgetPosition, travellerName, getPassengerWidgetType()));
  }

  private void checkIfHaveToLoadInformationInContactWidget() {
    if ((mPassengerWidgetPosition == 0) && (mTravellerPreviousItemSelected != -1)) {
      int passengerPosition = mSpwtPassengers.getSelectedItemPosition();
      mListenerPassengerWidget.setNewDefaultPassengerSpinner(passengerPosition);
    }
  }

  private void setGenderListeners() {
    titleMrClickableArea.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        passengerGender = titleMrLabel.getText().toString();
        decorateGenderLabel(titleMrLabel, R.drawable.passenger_title_selected, R.color.mono00);
        decorateGenderLabel(titleMrsLabel, R.drawable.passenger_title_unselected,
            R.color.basic_light);
      }
    });

    titleMrsClickableArea.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        passengerGender = titleMrsLabel.getText().toString();
        decorateGenderLabel(titleMrLabel, R.drawable.passenger_title_unselected,
            R.color.basic_light);
        decorateGenderLabel(titleMrsLabel, R.drawable.passenger_title_selected, R.color.mono00);
      }
    });
  }

  private void decorateGenderLabel(TextView genderLabel, @DrawableRes int background,
      @ColorRes int textColor) {
    genderLabel.setBackgroundResource(background);
    genderLabel.setTextColor(ContextCompat.getColor(getContext(), textColor));
  }

  private void setSpinnerIdenticationTypeListener() {
    mSpwtIdentificationType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (mRlIdentificationContainer.getVisibility() == View.VISIBLE) {
          String identificationTypeSelected = (String) mSpwtIdentificationType.getSelectedItem();
          TravellerIdentificationType travellerIdentificationType =
              TravellerIdentificationType.fromValue(identificationTypeSelected);
          mIdentificationBaseTextWatcherClient.setTravellerBehaviour(travellerIdentificationType);
        }
      }

      @Override public void onNothingSelected(AdapterView<?> parent) {

      }
    });
  }

  private void setOpenOrCloseDetailsListener() {
    mBtnOpenOrCloseDetails.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        showOrHideFields();
      }
    });
  }

  private void setTextInputDateOfBirthListener() {
    if (mTilBirthdate.getEditText() != null) {
      mTilBirthdate.getEditText().setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
          mDialogBirthDateFragment.show(mFragmentManager, "DialogDateFragment");
        }
      });
    }
  }

  private void setTextInputFrequentFlyerListener() {
    mCetFrequentFlyer.setOnClearClickListener(new ClearableEditText.OnClearClickListener() {
      @Override public void onClick() {
        mCetFrequentFlyer.setText(null);
        mFrequentFlyerSelectedList.clear();
      }
    });

    mCetFrequentFlyer.setOnEditTextClickListener(new ClearableEditText.OnEditTextClickListener() {
      @Override public void onClick(EditText editText) {
        List<Carrier> passengerCarriers = presenter.getCarriersFromPassenger();
        mListenerPassengerNavigator.navigateToFrequentFlyerPassenger(mFrequentFlyerSelectedList,
            passengerCarriers, mPassengerWidgetPosition);
      }
    });
  }

  private void setTextInputCountryResidentListener() {
    if (mTilCountryOfResidence.getEditText() != null) {
      mTilCountryOfResidence.getEditText().setOnClickListener(new OnClickListener() {
        @Override public void onClick(View v) {
          mListenerPassengerNavigator.navigateToCountryResidentPassenger(mPassengerWidgetPosition);
        }
      });
    }
  }

  private void setTextInputCountryNationalityListener() {
    if (mTilNationality.getEditText() != null) {
      mTilNationality.getEditText().setOnClickListener(new OnClickListener() {
        @Override public void onClick(View v) {
          mListenerPassengerNavigator.navigateToCountryNationalityPassenger(
              mPassengerWidgetPosition);
        }
      });
    }
  }

  private void setTextInputIdentificationCountryListener() {
    if (mTilIdentificationCountry.getEditText() != null) {
      mTilIdentificationCountry.getEditText().setOnClickListener(new OnClickListener() {
        @Override public void onClick(View v) {
          mListenerPassengerNavigator.navigateToCountryIdentificationPassenger(
              mPassengerWidgetPosition);
        }
      });
    }
  }

  private void setTextInputIdentificationExpirationDate() {
    if (mTilIdentificationExpirationDate.getEditText() != null) {
      mTilIdentificationExpirationDate.getEditText().setOnClickListener(new OnClickListener() {
        @Override public void onClick(View v) {
          mIdentificationExpirationDateFragment.show(mFragmentManager, "DialogDateFragment");
        }
      });
    }
  }

  private void initPassengerTextInputLayoutClient() {

    RequestValidationHandler requestValidationHandler =
        AndroidDependencyInjector.getInstance().provideIdentificationRequestValidationHandler();

    mPassengerBaseTextWatcherClient =
        new PassengerBaseTextWatchersClient(getContext(), mSpwtIdentificationType, mTilName,
            mTilMiddleName, mTilFirstLastName, mTilSecondLastName, mTilIdentification,
            mTilNationality, mTilCountryOfResidence, mTilIdentificationCountry,
            mTilIdentificationExpirationDate, mTilBirthdate,
            presenter.getSeconLastNameConditionRule(), requestValidationHandler);
  }

  public void initPassengerOnFocusChangeCustomKeyboard() {
    mPassengerBaseTextWatcherClient.initPassengerOnFocusChange();
  }

  private void configureFields() {
    presenter.configureWidgetFields();
    presenter.configureCollapse();
  }

  @Override public void setExpirationIdentificationCalendarMaxAndMin() {
    long maxCalendarDateByDefault = 0;
    long minCalendarDate = Calendar.getInstance().getTime().getTime();
    mIdentificationExpirationDateFragment =
        DatePickerDialogView.newInstance(maxCalendarDateByDefault, minCalendarDate,
            new DatePickerDialogView.onClickDatePickerListener() {
              @Override public void onDateDateSelected(int year, int month, int day) {
                long expirationIdentificationDate =
                    mDateHelperInterface.getTimeStampFromDayMonthYear(year, month, day);
                setExpirationIdentificationDate(expirationIdentificationDate);
              }
            });
  }

  private void setExpirationIdentificationDate(long date) {
    if (mTilIdentificationExpirationDate.getEditText() != null) {
      Date expirationIdentificationDate = new Date(date);
      String templateDate = getResources().getString(R.string.templates__date2);
      SimpleDateFormat simpleDateFormat = OdigeoDateUtils.getGmtDateFormat(templateDate);
      mTilIdentificationExpirationDate.getEditText()
          .setText(simpleDateFormat.format(expirationIdentificationDate));
    }
  }

  @Override public void setBirthDateCalendarMaxAndMin(Date minDate, Date maxDate) {
    long maxCalendarDate = maxDate.getTime();
    long minCalendarDate = minDate.getTime();
    mDialogBirthDateFragment = DatePickerDialogView.newInstance(maxCalendarDate, minCalendarDate,
        new DatePickerDialogView.onClickDatePickerListener() {
          @Override public void onDateDateSelected(int year, int month, int day) {
            presenter.validateBirthdate(day, month + 1, year, itinerary);
            long passengerBirthdate =
                mDateHelperInterface.getTimeStampFromDayMonthYear(year, month, day);
            setBirthDate(passengerBirthdate);
          }
        });
  }

  private void setBirthDate(long date) {
    if (mTilBirthdate.getEditText() != null) {
      Date birthDate = new Date(date);
      String templateDate = getResources().getString(R.string.templates__date2);
      SimpleDateFormat simpleDateFormat = OdigeoDateUtils.getGmtDateFormat(templateDate);
      mTilBirthdate.getEditText().setText(simpleDateFormat.format(birthDate));
    }
  }

  @Override public void setIdentificationTypePosition(int position) {
    if (mSpwtIdentificationType.isEnabled() && mSpwtIdentificationType.getCount() > 0) {
      mSpwtIdentificationType.setSelection(position);
    }
  }

  public void setFrequentFlyer(List<UserFrequentFlyer> frequentFlyerCards) {
    mFrequentFlyerSelectedList = frequentFlyerCards;
    presenter.formartFrequentFlyerInformation(frequentFlyerCards);
  }

  @Override public void showCardTitleAdult() {
    String passengerWidgetTitle =
        StringFormatHelper.formatPassengerTittle(mPassengerCardTitle, mPassengerWidgetPosition + 1,
            mPassengerTypeTitleAdult);
    mTvPassengerWidgetTitle.setText(passengerWidgetTitle);
  }

  @Override public void showCardTitleChild() {
    String passengerWidgetTitle =
        StringFormatHelper.formatPassengerTittle(mPassengerCardTitle, mPassengerWidgetPosition + 1,
            mPassengerTypeTitleChild);
    mTvPassengerWidgetTitle.setText(passengerWidgetTitle);
  }

  @Override public void showCardTitleInfant() {
    String passengerWidgetTitle =
        StringFormatHelper.formatPassengerTittle(mPassengerCardTitle, mPassengerWidgetPosition + 1,
            mPassengerTypeTitleInfant);
    mTvPassengerWidgetTitle.setText(passengerWidgetTitle);
  }

  @Override public void showImagePassengerProfile() {
    Drawable tintedDrawable =
        DrawableUtils.getTintedResource(R.drawable.ic_contact, R.color.basic_light, getContext());
    mCnivPassengerImageProfile.setImageDrawable(tintedDrawable);
  }

  @Override public void showImageMembershipProfile() {
    Drawable tintedDrawable =
        DrawableUtils.getTintedResource(R.drawable.ic_star_black, R.color.membership_star,
            getContext());
    ivMembershipStar.setImageDrawable(tintedDrawable);
    ivMembershipStar.setVisibility(VISIBLE);
  }

  @Override public void showSpinnerMembershipTitle() {
    String membershipTitle = localizables.getString(PASSENGER_TYPE_PREMIUM);
    mTvPassengers.setText(membershipTitle);
  }

  @Override public void showSpinnerNonMembershipTitle() {
    mTvPassengers.setText(localizables.getString(OneCMSKeys.SSO_TRAVELLERS_CONTACT_DETAIL));
  }

  @Override public void showSpinnerName(int position) {
    mSpwtPassengers.setSelection(position);
  }

  @Override public void showName(String name) {
    if (mTilName.getEditText() != null) {
      mTilName.getEditText().setText(name);
    }
  }

  @Override public void showLastName(String lastName) {
    if (mTilFirstLastName.getEditText() != null) {
      mTilFirstLastName.getEditText().setText(lastName);
    }
  }

  @Override public void showMiddleName(String middleName) {
    if (mTilMiddleName.getEditText() != null) {
      mTilMiddleName.getEditText().setText(middleName);
    }
  }

  @Override public void showSecondLastname(String secondLastName) {
    if (mTilSecondLastName.getEditText() != null) {
      mTilSecondLastName.getEditText().setText(secondLastName);
    }
  }

  @Override public void showBirthdate(String birthdate) {
    if (mTilBirthdate.getEditText() != null) {
      mTilBirthdate.getEditText().setText(birthdate);
    }
  }

  @Override public void showCountryOfResident(String countryOfResident) {
    if (mTilCountryOfResidence.getEditText() != null) {
      mTilCountryOfResidence.getEditText().setText(countryOfResident);
    }
  }

  @Override public void showNationality(String nationality) {
    if (mTilNationality.getEditText() != null) {
      mTilNationality.getEditText().setText(nationality);
    }
  }

  public void showCountryNationality(Country nationalityCountry) {
    if (mTilNationality.getEditText() != null) {
      mTilNationality.getEditText().setText(nationalityCountry.getCountryCode());
    }
  }

  public void showCountryIdentification(Country identificationCountry) {
    if (mTilIdentificationCountry.getEditText() != null) {
      mTilIdentificationCountry.getEditText().setText(identificationCountry.getCountryCode());
    }
  }

  @Override public void showIdentificationNumber(String identificationNumber) {
    if (mTilIdentification.getEditText() != null) {
      mTilIdentification.getEditText().setText(identificationNumber);
    }
  }

  @Override public void showIdentificationExpiration(String expirationDate) {
    if (mTilIdentificationExpirationDate.getEditText() != null) {
      mTilIdentificationExpirationDate.getEditText().setText(expirationDate);
    }
  }

  @Override public void showIdentificationCountry(String identificationCountry) {
    if (mTilIdentificationCountry.getEditText() != null) {
      mTilIdentificationCountry.getEditText().setText(identificationCountry);
    }
  }

  @Override public void showFrequentFlyer(String frequentFlyerInformation) {
    mCetFrequentFlyer.setText(frequentFlyerInformation);
  }

  @Override public void showInfantBirthdateEror() {
    showBirthdateErrorAlertDialog(errorBabyBirthdateBody);
  }

  @Override public void showChildBirthdateError() {
    showBirthdateErrorAlertDialog(errorChildBirthdateBody);
  }

  private void showBirthdateErrorAlertDialog(String body) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
    builder.setCancelable(true);
    builder.setTitle(errorChildOrBabyBirthdateTitle);
    builder.setPositiveButton(errorNewSearch, new DialogInterface.OnClickListener() {
      @Override public void onClick(DialogInterface dialog, int which) {
        mListenerPassengerNavigator.navigateToSearch();
      }
    });
    builder.setNegativeButton(commonCancel, new DialogInterface.OnClickListener() {
      @Override public void onClick(DialogInterface dialog, int which) {

      }
    });
    builder.setMessage(body);
    builder.create();
    builder.show();
  }

  @Override public void showCollapseEditText() {
    mBtnOpenOrCloseDetails.setText(mEditCollapse);
  }

  @Override public void showCollapseCloseText() {
    mBtnOpenOrCloseDetails.setText(mCloseCollapse);
  }

  @Override public void hideFieldsContainer() {
    mRlFieldsContainer.setVisibility(GONE);
  }

  @Override public void hidePassengerHeaderSpinner() {
    mRlHeaderPassengerItem.setVisibility(GONE);
  }

  public void hideButtonOpenOrCloseDetails() {
    mBtnOpenOrCloseDetails.setVisibility(GONE);
  }

  @Override public void hideBirthdate() {
    mTilBirthdate.setVisibility(GONE);
  }

  @Override public void hideNameEditable() {
    mTilName.setVisibility(GONE);
  }

  @Override public void hideMiddleNameEditable() {
    mTilMiddleName.setVisibility(GONE);
  }

  @Override public void hideLastNameEditable() {
    mTilFirstLastName.setVisibility(GONE);
  }

  @Override public void hideSecondLastNameEditable() {
    mTilSecondLastName.setVisibility(GONE);
  }

  @Override public void hideIdentification() {
    mRlIdentificationContainer.setVisibility(GONE);
    mTilIdentification.setVisibility(GONE);
  }

  @Override public void hideImageMembershipProfile() {
    ivMembershipStar.setVisibility(GONE);
  }

  @Override public void hideTitle() {
    titleHeader.setVisibility(GONE);
  }

  @Override public void hideNationality() {
    mTilNationality.setVisibility(GONE);
  }

  @Override public void hideIdentificationExpirationDate() {
    mTilIdentificationExpirationDate.setVisibility(GONE);
  }

  @Override public void hideCountryOfResidence() {
    mTilCountryOfResidence.setVisibility(GONE);
  }

  @Override public void hideIdentificationCountry() {
    mTilIdentificationCountry.setVisibility(GONE);
  }

  @Override public void hideFrequentFlyer() {
    mCetFrequentFlyer.setVisibility(GONE);
  }

  @Override public void hideResidentWidgetGroup() {
    mRlResidentGroupContainer.setVisibility(GONE);
  }

  @Override public void enableIdentificationTypeMultipleOptions() {
    mSpwtIdentificationType.setEnabled(true);
  }

  @Override public void enableIdentificationTypeUniqueOption() {
    mSpwtIdentificationType.setEnabled(false);
  }

  @Override public int getWidgetPosition() {
    return mPassengerWidgetPosition;
  }

  public TravellerType getPassengerWidgetType() {
    return mPassengerInformation.getTravellerType();
  }

  public String getPassengerSelectedItemSpinner() {
    return (String) mSpwtPassengers.getSelectedItem();
  }

  @Override public String getPassengerType() {
    return mPassengerInformation.getTravellerType().value();
  }

  @Override public String getPassengerTitleName() {
    return presenter.transformMarketTittleToGeneric(passengerGender,
        localizables.getString(OneCMSKeys.PASSENGER_TITLE_MR));
  }

  @Override public String getPassengerName() {
    if ((mTilName.getEditText() != null) && (mTilName.getVisibility() == VISIBLE)) {
      return mTilName.getEditText().getText().toString();
    }
    return (String) mSpwtPassengers.getSelectedItem();
  }

  @Override public int getPassengerSelected() {
    if (mSpwtPassengers.getVisibility() == VISIBLE
        && mSpwtPassengers.getSelectedItemPosition() > 0) {
      return mSpwtPassengers.getSelectedItemPosition();
    } else {
      return -1;
    }
  }

  @Override @Nullable public String getPassengerMiddleName() {
    if ((mTilMiddleName.getEditText() != null) && (mTilMiddleName.getVisibility() == VISIBLE)) {
      return mTilMiddleName.getEditText().getText().toString();
    }
    return null;
  }

  @Override public String getPassengerFirstLastName() {
    if ((mTilFirstLastName.getEditText() != null) && (mTilFirstLastName.getVisibility()
        == VISIBLE)) {
      return mTilFirstLastName.getEditText().getText().toString();
    }
    return (String) mSpwtPassengers.getSelectedItem();
  }

  @Override @Nullable public String getPassengerSecondLastName() {
    if ((mTilSecondLastName.getEditText() != null)
        && (mTilSecondLastName.getVisibility()
        == VISIBLE)
        && (mTilSecondLastName.getEditText().getText() != null)
        && (!mTilSecondLastName.getEditText().getText().toString().isEmpty())) {
      return mTilSecondLastName.getEditText().getText().toString();
    } else if (presenter.getSecondLastNameFromPassenger() != null) {
      return presenter.getSecondLastNameFromPassenger();
    }
    return null;
  }

  @Override @Nullable public String getPassengerDayOfBirth() {
    if (mTilBirthdate.getEditText() != null) {
      return mTilBirthdate.getEditText().getText().toString();
    }
    return null;
  }

  @Override @Nullable public String getPassengerNationalityCountryCode() {
    if (mTilNationality.getEditText() != null) {
      return mTilNationality.getEditText().getText().toString();
    }
    return null;
  }

  @Override @Nullable public String getPassengerResidentCountryCode() {
    if (mTilCountryOfResidence.getEditText() != null) {
      return mResidentCountry.getCountryCode();
    }
    return null;
  }

  @Override @Nullable public String getPassengerIdentification() {
    if (mTilIdentification.getEditText() != null) {
      return mTilIdentification.getEditText().getText().toString();
    }
    return null;
  }

  @Override public String getPassengerIdentificationType() {
    return (String) mSpwtIdentificationType.getSelectedItem();
  }

  @Override @Nullable public String getPassengerIdentificationExpiration() {
    if (mTilIdentificationExpirationDate.getEditText() != null) {
      return mTilIdentificationExpirationDate.getEditText().getText().toString();
    }
    return null;
  }

  @Override @Nullable public String getPassengerIdentificationCountryCode() {
    if (mTilIdentificationCountry.getEditText() != null) {
      return mTilIdentificationCountry.getEditText().getText().toString();
    }
    return null;
  }

  @Override @Nullable public String getLocalityResidentCode() {
    if (mRlResidentGroupContainer.getVisibility() == VISIBLE) {
      String residentLocality = (String) mSpwtResidentLocality.getSelectedItem();
      return presenter.getResidentLocalityByCode(residentLocality);
    }
    return null;
  }

  @Override public void showMembershipAlert() {
    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
    builder.setCancelable(true);
    builder.setPositiveButton(commonContinue, new DialogInterface.OnClickListener() {
      @Override public void onClick(DialogInterface dialog, int which) {
        onMembershipDialogConfirmed();
      }
    });
    builder.setNegativeButton(commonCancel, new DialogInterface.OnClickListener() {
      @Override public void onClick(DialogInterface dialog, int which) {
        onMembershipDialogCancelled();
      }
    });
    builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override public void onCancel(DialogInterface dialogInterface) {
        onMembershipDialogCancelled();
      }
    });
    String passengerNotPremiumMesagge =
        localizables.getString(OneCMSKeys.PASSENGER_NOT_PREMIUM_MEMBER);
    builder.setMessage(passengerNotPremiumMesagge);
    builder.create();
    builder.show();
  }

  private void onMembershipDialogConfirmed() {
    presenter.membershipWasNotSelected();
    showSpinnerNonMembershipTitle();
    hideImageMembershipProfile();
    mListenerPassengerWidget.showPricesWithoutMembershipPerks();
  }

  private void onMembershipDialogCancelled() {
    int membershipPositionInSpinner = presenter.getMembershipPosition();
    if (membershipPositionInSpinner != NO_MEMBERSHIP) {
      mSpwtPassengers.setSelection(membershipPositionInSpinner);
    }
  }

  @Override public List<BaggageSelectionRequest> getPassengerBaggage() {
    return baggageCollectionView.getBaggageSelections();
  }

  public TravellerRequest getTraveller() {
    return presenter.getTraveller();
  }

  public UserTraveller getUserTraveller() {
    return presenter.getUserTraveller();
  }

  @Override public List<UserFrequentFlyer> getPassengerFrequentFlyerCode() {
    return mFrequentFlyerSelectedList;
  }

  public void showCountryResident(Country residentCountry) {
    if (mTilCountryOfResidence.getEditText() != null) {
      mResidentCountry = residentCountry;
      mTilCountryOfResidence.getEditText().setText(residentCountry.getName());
    }
  }

  @Override public void setNullTextWatchersError() {
    mPassengerBaseTextWatcherClient.setNullErrorInTextWatchers();
  }

  public boolean haveToFocusOnPassengerTextInputError() {
    final TextInputLayout textInputLayout =
        mPassengerBaseTextWatcherClient.getTextInputLayoutWithError();
    if (textInputLayout != null) {
      mRlFieldsContainer.setVisibility(VISIBLE);
      mRlFieldsContainer.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
      textInputLayout.requestFocus();
      scrollToTextInputLayoutWithError(textInputLayout);
      return true;
    }
    return false;
  }

  private void scrollToTextInputLayoutWithError(final TextInputLayout textInputLayout) {
    final ScrollView svPassenger = mListenerPassengerWidget.getScrollView();
    svPassenger.post(new Runnable() {
      @Override public void run() {

        int aditionalOffsetForIdentification = 0;
        if (isIdentificationField(textInputLayout)) {
          View identificationParent = (View) textInputLayout.getParent();
          aditionalOffsetForIdentification = identificationParent.getTop();
        }
        View parent = (View) textInputLayout.getParent().getParent();
        View grandParent = (View) parent.getParent().getParent();
        int inputHeight = textInputLayout.getHeight();
        int top = textInputLayout.getTop()
            + parent.getTop()
            + grandParent.getTop()
            + inputHeight
            + aditionalOffsetForIdentification;
        int height = svPassenger.getHeight();
        svPassenger.smoothScrollTo(0, top - height / 2);
      }
    });
  }

  private boolean isIdentificationField(TextInputLayout textInputLayout) {
    return (textInputLayout.equals(mTilIdentification) || textInputLayout.equals(
        mTilIdentificationCountry) || textInputLayout.equals(mTilIdentificationExpirationDate));
  }

  private void showOrHideFields() {
    if (mRlFieldsContainer.getVisibility() == VISIBLE) {
      CollapseAndExpandAnimationUtil.collapse(mRlFieldsContainer, true,
          mListenerPassengerWidget.getScrollView(),
          (View) mRlFieldsContainer.getParent().getParent());
      showCollapseEditText();
    } else {
      CollapseAndExpandAnimationUtil.expand(mRlFieldsContainer);
      showCollapseCloseText();
    }
  }

  @Nullable public TextInputLayout getLastFocused() {
    View lastFocusedView = mRlFieldsContainer.getFocusedChild();
    if (lastFocusedView instanceof RelativeLayout) {
      RelativeLayout mRlIdentification = (RelativeLayout) lastFocusedView;
      return (TextInputLayout) mRlIdentification.getFocusedChild();
    } else if (lastFocusedView instanceof TextInputLayout) {
      return (TextInputLayout) mRlFieldsContainer.getFocusedChild();
    }
    return null;
  }

  public void updateTextInputLayoutError(TextInputLayout textInputLayout) {
    mPassengerBaseTextWatcherClient.updateTextInputLayoutError(textInputLayout);
  }

  private void expandFieldsContainer() {
    if (mRlFieldsContainer.getVisibility() == View.GONE) {
      mRlFieldsContainer.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
      mRlFieldsContainer.setVisibility(View.VISIBLE);
      showCollapseCloseText();
    }
  }

  @Override public boolean isThereAnyTextInputLayoutEmpty() {
    TextInputLayout textInputLayout = mPassengerBaseTextWatcherClient.getTextInputLayoutEmpty();
    return (textInputLayout != null);
  }

  @Override
  public void updateBaggageSelection(List<BaggageSelectionRequest> baggageSelectionRequestList) {
    baggageCollectionView.updateBaggageSelections(baggageSelectionRequestList);
  }

  @Override public void disableApplyBaggageSelection() {
    if (applyBaggageSelectionSwitch.isChecked()) {
      applyBaggageSelectionSwitch.setChecked(false);
      trackerController.trackAnalyticsEvent(CATEGORY_FLIGHTS_PAX, ACTION_PAX_BAGGAGE,
          LABEL_SAME_BAGGAGE_AUTO_UNCHECKED);
    }
  }

  @Override public void onBaggageSelectionChange(boolean isDefaultBaggageSelection) {
    if (isDefaultBaggageSelection) {
      mListenerPassengerWidget.applyDefaultBaggageSelection(
          baggageCollectionView.getBaggageSelections());
    } else if (hasMoreNonChildPassengers) {
      if (applyBaggageSelection.getVisibility() != VISIBLE) {
        showApplyBaggageSelectionWidget();
      }
    } else {
      mListenerPassengerWidget.disableApplyDefaultBaggageSelection();
    }
  }

  private void showApplyBaggageSelectionWidget() {
    CollapseAndExpandAnimationUtil.expand(applyBaggageSelection);
    applyBaggageSelectionSwitch.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        String label;
        if (applyBaggageSelectionSwitch.isChecked()) {
          label = LABEL_SAME_BAGGAGE_CHECKED;
        } else {
          label = LABEL_SAME_BAGGAGE_UNCHECKED;
        }
        trackerController.trackAnalyticsEvent(CATEGORY_FLIGHTS_PAX, ACTION_PAX_BAGGAGE, label);
      }
    });
    applyBaggageSelectionSwitch.setOnCheckedChangeListener(
        new CompoundButton.OnCheckedChangeListener() {
          @Override public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            baggageCollectionView.setDefaultBaggageSelectionToAllPassengers(isChecked);
            if (isChecked) {
              mListenerPassengerWidget.applyDefaultBaggageSelection(
                  baggageCollectionView.getBaggageSelections());
            }
          }
        });
  }

  @Override
  public void propagateItinerarySortCriteria(ItinerarySortCriteria itinerarySortCriteria) {
    mListenerPassengerWidget.propagateItinerarySortCriteria(itinerarySortCriteria);
  }

  @Override public void showMembershipPrices() {
    mListenerPassengerWidget.showMembershipPrices();
  }

  @Override public void selectTitleMR() {
    titleMrClickableArea.performClick();
  }

  @Override public void selectTitleMRS() {
    titleMrsClickableArea.performClick();
  }
}