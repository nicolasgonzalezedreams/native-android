package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BaggageConditionsDTO implements Serializable {

  protected BaggageDescriptorDTO baggageDescriptorIncludedInPrice;

  protected List<SelectableBaggageDescriptorDTO> selectableBaggageDescriptor;

  protected BaggageApplicationLevelDTO baggageApplicationLevel;

  protected SegmentTypeIndexDTO segmentTypeIndex;

  /**
   * Gets the value of the baggageDescriptorIncludedInPrice property.
   */
  public BaggageDescriptorDTO getBaggageDescriptorIncludedInPrice() {
    return baggageDescriptorIncludedInPrice;
  }

  /**
   * -     * Sets the value of the baggageDescriptorIncludedInPrice property.
   * -
   */
  public void setBaggageDescriptorIncludedInPrice(
      BaggageDescriptorDTO baggageDescriptorIncludedInPrice) {
    this.baggageDescriptorIncludedInPrice = baggageDescriptorIncludedInPrice;
  }

  /**
   * Gets the value of the selectableBaggageDescriptor property.
   */
  public List<SelectableBaggageDescriptorDTO> getSelectableBaggageDescriptor() {
    if (selectableBaggageDescriptor == null) {
      selectableBaggageDescriptor = new ArrayList<>();
    }
    return this.selectableBaggageDescriptor;
  }

  /**
   * -     * Sets the value of the selectableBaggageDescriptor property.
   * -
   */
  public void setSelectableBaggageDescriptor(
      List<SelectableBaggageDescriptorDTO> selectableBaggageDescriptor) {
    this.selectableBaggageDescriptor = selectableBaggageDescriptor;
  }

  /**
   * Gets the value of the baggageApplicationLevel property.
   */
  public BaggageApplicationLevelDTO getBaggageApplicationLevel() {
    return baggageApplicationLevel;
  }

  /**
   * -     * Gets the value of the baggageIncludedInPrice property.
   * -
   */
  public void setBaggageApplicationLevel(BaggageApplicationLevelDTO baggageApplicationLevel) {
    this.baggageApplicationLevel = baggageApplicationLevel;
  }

  /**
   * Sets the value of the baggageIncludedInPrice property.
   */
  public SegmentTypeIndexDTO getSegmentTypeIndex() {
    return segmentTypeIndex;
  }

  /**
   * -     * Sets the value of the segmentTypeIndex property.
   * -
   */
  public void setSegmentTypeIndex(SegmentTypeIndexDTO segmentTypeIndex) {
    this.segmentTypeIndex = segmentTypeIndex;
  }
}
