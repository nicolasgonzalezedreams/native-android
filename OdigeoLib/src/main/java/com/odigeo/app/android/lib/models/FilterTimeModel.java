package com.odigeo.app.android.lib.models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Irving Lóp on 16/10/2014.
 * this model is for FilterTimeWidget
 */
public class FilterTimeModel implements Serializable {
  private Date departureMinDateSelected;
  private Date arrivalMinDateSelected;
  private Date departureMaxDateSelected;
  private Date arrivalMaxDateSelected;
  private int maxFlightHoursSelected;

  public final int getMaxFlightHoursSelected() {
    return maxFlightHoursSelected;
  }

  public final void setMaxFlightHoursSelected(int maxFlightHoursSelected) {
    this.maxFlightHoursSelected = maxFlightHoursSelected;
  }

  @Override public final boolean equals(Object object) {
    if (object == null) {
      return false;
    }

    if (!(object instanceof FilterTimeModel)) {
      return false;
    }

    FilterTimeModel model2 = (FilterTimeModel) object;

    if (!getArrivalMaxDateSelected().equals(model2.getArrivalMaxDateSelected())) {
      return false;
    }

    if (!getArrivalMinDateSelected().equals(model2.getArrivalMinDateSelected())) {
      return false;
    }

    if (!getDepartureMaxDateSelected().equals(model2.getDepartureMaxDateSelected())) {
      return false;
    }

    if (!getDepartureMinDateSelected().equals(model2.getDepartureMinDateSelected())) {
      return false;
    }

    if (maxFlightHoursSelected != model2.getMaxFlightHoursSelected()) {
      return false;
    }
    return true;
  }

  public final Date getDepartureMinDateSelected() {
    return departureMinDateSelected;
  }

  public final void setDepartureMinDateSelected(Date departureMinDateSelected) {
    this.departureMinDateSelected = departureMinDateSelected;
  }

  public final Date getArrivalMinDateSelected() {
    return arrivalMinDateSelected;
  }

  public final void setArrivalMinDateSelected(Date arrivalMinDateSelected) {
    this.arrivalMinDateSelected = arrivalMinDateSelected;
  }

  public final Date getDepartureMaxDateSelected() {
    return departureMaxDateSelected;
  }

  public final void setDepartureMaxDateSelected(Date departureMaxDateSelected) {
    this.departureMaxDateSelected = departureMaxDateSelected;
  }

  public final Date getArrivalMaxDateSelected() {
    return arrivalMaxDateSelected;
  }

  public final void setArrivalMaxDateSelected(Date arrivalMaxDateSelected) {
    this.arrivalMaxDateSelected = arrivalMaxDateSelected;
  }
}
