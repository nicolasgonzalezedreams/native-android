package com.odigeo.app.android.lib.widget;

import android.content.Intent;
import android.widget.RemoteViewsService;

/**
 * Service to bind the list views with the data.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 11/03/15
 */
public class WidgetService extends RemoteViewsService {

  @Override public RemoteViewsFactory onGetViewFactory(Intent intent) {
    return new SearchWidgetProvider(this.getApplicationContext());
  }
}
