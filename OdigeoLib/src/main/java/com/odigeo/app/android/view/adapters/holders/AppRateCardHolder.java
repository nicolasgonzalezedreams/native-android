package com.odigeo.app.android.view.adapters.holders;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.navigator.AppRateFeedBackNavigator;
import com.odigeo.app.android.navigator.AppRateNavigator;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.presenter.contracts.views.holder.AppRateCardHolderInterface;
import java.util.List;

import static com.odigeo.app.android.view.constants.OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_BUTTON_BAD;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_BUTTON_GOOD;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_SUBTITLE;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_TITLE;

public class AppRateCardHolder extends BaseRecyclerViewHolder<String>
    implements AppRateCardHolderInterface, DividerProviderViewHolder {

  private TextView title;
  private TextView subtitle;
  private Button buttonLike;
  private Button buttonNotLike;
  private TrackerControllerInterface mTracker;

  public static AppRateCardHolder newInstance(ViewGroup container) {
    View view = LayoutInflater.from(container.getContext())
        .inflate(R.layout.apprate_mytrips_card, container, false);
    return new AppRateCardHolder(view);
  }

  public AppRateCardHolder(View itemView) {
    super(itemView);
    title = (TextView) itemView.findViewById(R.id.tv_apprate_mytrips_title);
    subtitle = (TextView) itemView.findViewById(R.id.tv_apprate_mytrips_subtitle);
    buttonLike = (Button) itemView.findViewById(R.id.btn_like);
    buttonNotLike = (Button) itemView.findViewById(R.id.btn_not_like);

    mTracker = dependencyInjector.provideTrackerController();
  }

  @Override public void bind(int position, String item) {
    title.setText(localizableProvider.getString(MYTRIPSVIEWCONTROLLER_RATE_TITLE));
    subtitle.setText(localizableProvider.getString(MYTRIPSVIEWCONTROLLER_RATE_SUBTITLE));
    buttonLike.setText(localizableProvider.getString(MYTRIPSVIEWCONTROLLER_RATE_BUTTON_GOOD));
    buttonNotLike.setText(localizableProvider.getString(MYTRIPSVIEWCONTROLLER_RATE_BUTTON_BAD));
    setListeners();
  }

  private void setListeners() {
    buttonLike.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        Intent intent =
            new Intent(getContext().getApplicationContext(), AppRateFeedBackNavigator.class);
        intent.putExtra(AppRateNavigator.DESTINATION, AppRateNavigator.DESTINATION_THANKS);
        getContext().startActivity(intent);
        mTracker.trackMyTripsRateAppLikeItButton();
      }
    });

    buttonNotLike.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        Intent intent =
            new Intent(getContext().getApplicationContext(), AppRateFeedBackNavigator.class);
        intent.putExtra(AppRateNavigator.DESTINATION, AppRateNavigator.DESTINATION_HELP_IMPROVE);
        getContext().startActivity(intent);
        mTracker.trackMyTripsRateAppNotLikeItButton();
      }
    });
  }

  @Override public int getDividerDrawable() {
    return R.drawable.card_list_divider;
  }
}
