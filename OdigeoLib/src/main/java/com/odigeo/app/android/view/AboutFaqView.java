package com.odigeo.app.android.view;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.navigator.AboutUsNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.custom.AboutInfoItem;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.AboutFaqPresenter;
import com.odigeo.presenter.contracts.views.AboutFaqViewInterface;

public class AboutFaqView extends BaseView<AboutFaqPresenter> implements AboutFaqViewInterface {

  private AboutInfoItem mBtnAboutFaqQuestion;
  private AboutInfoItem mBtnAboutFaqAboutGo;
  private AboutInfoItem mBtnAboutFaqAbout;
  private AboutInfoItem mBtnAboutFaqSecure;
  private TextView mTvFooterLegal;
  private TextView mTvAboutFaqTitle;

  @Override public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    mPresenter.setActionBarTitle(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.ABOUTOPTIONSMODULE_ABOUT_OPTION_FAQ)
            .toString());
  }

  @Override protected AboutFaqPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideAboutFaqPresenter(this, (AboutUsNavigator) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_about_faq_fragment;
  }

  @Override protected void initComponent(View view) {
    mTvAboutFaqTitle = (TextView) view.findViewById(R.id.tvAboutFaqTitle);
    mTvFooterLegal = (TextView) view.findViewById(R.id.tvFooterLegal);
    mBtnAboutFaqQuestion = (AboutInfoItem) view.findViewById(R.id.wdgtAboutFaqQuestionNameAndTitle);
    mBtnAboutFaqAboutGo = (AboutInfoItem) view.findViewById(R.id.wdgtAboutFaqAboutGoNameAndTitle);
    mBtnAboutFaqAbout =
        (AboutInfoItem) view.findViewById(R.id.wdgtAboutFaqAboutThisAppNameAndTitle);
    mBtnAboutFaqSecure = (AboutInfoItem) view.findViewById(R.id.wdgtAboutFaqSecureNameAndTitle);
  }

  @Override protected void setListeners() {
    mBtnAboutFaqQuestion.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        trackEventAboutFaqOption(GAnalyticsNames.LABEL_QUESTIONS_ABOUT_FLIGHTS);
        mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_FAQ);

        mPresenter.openWebView(OneCMSKeys.FAQ_URL_FLIGHT,
            LocalizablesFacade.getString(getContext(), OneCMSKeys.QUESTIONS_ABOUT_FLIGHTS_URL)
                .toString());
      }
    });

    mBtnAboutFaqAboutGo.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        trackEventAboutFaqOption(GAnalyticsNames.LABEL_ABOUT_GOVOYAGES);
        mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_ABOUT_BRAND);

        mPresenter.openWebView(OneCMSKeys.FAQ_URL_ABOUT_BRAND,
            LocalizablesFacade.getString(getContext(), OneCMSKeys.ABOUT_BRAND_URL).toString());
      }
    });

    mBtnAboutFaqAbout.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        trackEventAboutFaqOption(GAnalyticsNames.LABEL_ABOUT_APP);
        mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_ABOUT_APP);

        mPresenter.openWebView(OneCMSKeys.FAQ_URL_ABOUT_APP,
            LocalizablesFacade.getString(getContext(), OneCMSKeys.ABOUT_APP_URL).toString());
      }
    });

    mBtnAboutFaqSecure.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        trackEventAboutFaqOption(GAnalyticsNames.LABEL_ABOUT_SECURE_SHOPPING);
        mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_SECURE_SHOPPING);

        mPresenter.openWebView(OneCMSKeys.FAQ_URL_SECURE_SHOPPING,
            LocalizablesFacade.getString(getContext(), OneCMSKeys.SECURE_SHOPPING_URL).toString());
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    mTvAboutFaqTitle.setText(LocalizablesFacade.getString(getActivity().getApplicationContext(),
        OneCMSKeys.FAQVIEWCONTROLLER_HEADERTITLE));
    mTvFooterLegal.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.FAQ_FOOTER_LEGAL,
        Configuration.getInstance().getBrandVisualName()));

    mBtnAboutFaqQuestion.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.FAQ_BUTTON_FLIGHT).toString());
    mBtnAboutFaqAboutGo.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.FAQ_BUTTON_BRAND,
            Configuration.getInstance().getBrandVisualName()).toString());
    mBtnAboutFaqAbout.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.FAQ_BUTTON_APP).toString());
    mBtnAboutFaqSecure.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.FAQ_BUTTON_SECURE).toString());
  }

  private void trackEventAboutFaqOption(String label) {
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_ABOUT_US,
        TrackerConstants.ACTION_ABOUT_US, label);
  }

  @Override public void onDestroy() {
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_ABOUT_US_FAQ,
        TrackerConstants.ACTION_ABOUT_US, TrackerConstants.LABEL_GO_BACK);
    super.onDestroy();
  }
}
