package com.odigeo.app.android.lib.models;

import com.odigeo.app.android.lib.consts.DistanceUnits;
import java.io.Serializable;

/**
 * Created by Irving Lóp on 13/11/2014.
 */
public class AppSettings implements Serializable {
  private String marketKey;
  private int defaultAdults;
  private int defaultKids;
  private int defaultBabies;
  private DistanceUnits distanceUnit;

  public final String getMarketKey() {
    return marketKey;
  }

  public final void setMarketKey(String marketKey) {
    this.marketKey = marketKey;
  }

  public final int getDefaultAdults() {
    return defaultAdults;
  }

  public final void setDefaultAdults(int defaultAdults) {
    this.defaultAdults = defaultAdults;
  }

  public final int getDefaultKids() {
    return defaultKids;
  }

  public final void setDefaultKids(int defaultKids) {
    this.defaultKids = defaultKids;
  }

  public final int getDefaultBabies() {
    return defaultBabies;
  }

  public final void setDefaultBabies(int defaultBabies) {
    this.defaultBabies = defaultBabies;
  }

  public final DistanceUnits getDistanceUnit() {
    return distanceUnit;
  }

  public final void setDistanceUnit(DistanceUnits distanceUnit) {
    this.distanceUnit = distanceUnit;
  }
}
