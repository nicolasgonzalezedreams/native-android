package com.odigeo.app.android.lib.config;

/**
 * Created by ManuelOrtiz on 11/09/2014.
 */
public class ImagesSources {
  private String urlAirlineLogos;
  private String urlMyTrips;
  private String urlCountries;
  private String imageFileType;
  private String multidestinationFormat;

  public final String getUrlAirlineLogos() {
    return urlAirlineLogos;
  }

  public final void setUrlAirlineLogos(String urlAirlineLogos) {
    this.urlAirlineLogos = urlAirlineLogos;
  }

  public final String getUrlMyTrips() {
    return urlMyTrips;
  }

  public final void setUrlMyTrips(String urlMyTrips) {
    this.urlMyTrips = urlMyTrips;
  }

  public final String getUrlCountries() {
    return urlCountries;
  }

  public final void setUrlCountries(String urlCountries) {
    this.urlCountries = urlCountries;
  }

  public final String getImageFileType() {
    return imageFileType;
  }

  public final void setImageFileType(String imageFileType) {
    this.imageFileType = imageFileType;
  }

  public final String getMultidestinationFormat() {
    return multidestinationFormat;
  }

  public final void setMultidestinationFormat(String multidestinationFormat) {
    this.multidestinationFormat = multidestinationFormat;
  }
}
