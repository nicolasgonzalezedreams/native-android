package com.odigeo.app.android.lib.pojos.banktransfer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.odigeo.app.android.lib.R;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @author Oscar Álvarez
 * @version 1.0
 * @since 30/04/15
 */
public class BanktransferWarning extends IncompleteBookingBean {
  @Override public View inflate(@NonNull Context context, @NonNull ViewGroup parent) {
    View view = LayoutInflater.from(context)
        .inflate(R.layout.warning_incomplete_booking_item, parent, false);
    return view;
  }
}
