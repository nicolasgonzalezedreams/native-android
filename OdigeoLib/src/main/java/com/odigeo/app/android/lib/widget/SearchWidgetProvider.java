package com.odigeo.app.android.lib.widget;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.geo.City;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 11/03/15
 */
public class SearchWidgetProvider implements RemoteViewsService.RemoteViewsFactory {
  private static final int MAX_LIST_ITEMS = 6;
  private final Context context;
  private List<SearchOptions> searchOptionsList;

  public SearchWidgetProvider(@NonNull Context context) {
    this.context = context;

    searchOptionsList = populateList(context);
  }

  /**
   * This method populate the list with all the items
   */
  public static List<SearchOptions> populateList(Context context) {
    List<SearchOptions> roundSearch =
        PreferencesManager.readFlights(context, Constants.PREFERENCE_HISTORY_SEARCH);
    List<SearchOptions> auxList =
        PreferencesManager.readFlights(context, Constants.PREFERENCE_HISTORY_SEARCH_MD);
    Date currentDate = OdigeoDateUtils.resetTimeToDate(new Date());
    //To mix lists for simple and round flights.
    int cont = 0;
    FlightSegment flightSegment;
    Date flightDate;
    //To mix lists for simple and round flights.
    for (SearchOptions searchOptions : auxList) {
      roundSearch.add(searchOptions);
    }

    //Order the list using search date
    Collections.sort(roundSearch, new Comparator<SearchOptions>() {
      @Override public int compare(SearchOptions lhs, SearchOptions rhs) {
        if (lhs.getSearchDate() == rhs.getSearchDate()) {
          return 0;
        } else if (lhs.getSearchDate() > rhs.getSearchDate()) {
          return 1;
        } else {
          return -1;
        }
      }
    });

    List<SearchOptions> searchOptionsList = new LinkedList<>();
    SearchOptions searchOptions;
    for (int i = 0; cont < MAX_LIST_ITEMS && i < roundSearch.size(); i++) {
      //Setup of the item
      searchOptions = roundSearch.get(i);
      flightSegment = searchOptions.getFlightSegments().get(0);
      flightDate = OdigeoDateUtils.resetTimeToDate(
          OdigeoDateUtils.createCalendar(flightSegment.getDate()).getTime());
      if (flightDate.compareTo(currentDate) >= 0) {
        cont++;
        searchOptionsList.add(searchOptions);
      }
    }

    return searchOptionsList;
  }

  @Override public boolean hasStableIds() {
    return true;
  }

  @Override public void onCreate() {
    //Nothing to do here
  }

  @Override public void onDestroy() {
    //Nothing to do here
  }

  @Override public void onDataSetChanged() {
    searchOptionsList = populateList(context);
  }

  @Override public int getViewTypeCount() {
    return 1;
  }

  @Override public RemoteViews getLoadingView() {
    return null;
  }

  @Override public int getCount() {
    return searchOptionsList.size();
  }

  @Override public long getItemId(int position) {
    return position;
  }

  @Override public RemoteViews getViewAt(int position) {
    //Creation of the row.
    RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_list_item);
    SearchOptions searchOptions = searchOptionsList.get(position);
    remoteViews.removeAllViews(R.id.lnlPlaces);
    //Setup of the item
    Date currentDate = new Date();
    FlightSegment flightSegment = searchOptions.getFlightSegments().get(0);
    SimpleDateFormat dateFormat =
        new SimpleDateFormat(context.getString(R.string.widget_item_date_format),
            LocaleUtils.getCurrentLocale());
    dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

    if (flightSegment.getDate() >= currentDate.getTime()) {
      StringBuffer dates = new StringBuffer();
      String defaultValue = " - ";
      if (searchOptions.getTravelType() != TravelType.MULTIDESTINATION) {
        addSegment(searchOptions, remoteViews,
            searchOptions.getFlightSegments().get(0).getDepartureCity(),
            searchOptions.getFlightSegments().get(0).getArrivalCity(), 0);
        dates.append(dateFormat.format(flightSegment.getDate()));
        if (searchOptions.getTravelType() == TravelType.ROUND) {
          dates.append(defaultValue);
          dates.append(dateFormat.format(searchOptions.getFlightSegments().get(1).getDate()));
        }
      } else {
        int i = 0;
        for (FlightSegment segment : searchOptions.getFlightSegments()) {
          addSegment(searchOptions, remoteViews, segment.getDepartureCity(),
              segment.getArrivalCity(), i++);
          if (i > 1) {
            dates.append(defaultValue);
          }
          dates.append(dateFormat.format(segment.getDate()));
        }
      }
      remoteViews.setTextViewText(R.id.lblDates, dates);
      //Get total of passengers
      int passengers = searchOptions.getTotalPassengers();
      remoteViews.setTextViewText(R.id.lblPassengers, " " + passengers);
      if (passengers > 1) {
        remoteViews.setImageViewResource(R.id.imgPassengers, R.drawable.search_multi_pax);
      }

      //Set onClick event
      Intent intent = new Intent();
      intent.putExtra(Constants.EXTRA_WIDGET_SEARCH_OPTIONS, searchOptions);
      remoteViews.setOnClickFillInIntent(R.id.rllRow, intent);
    }
    return remoteViews;
  }

  private void addSegment(SearchOptions searchOptions, RemoteViews remoteViews, City departure,
      City arrival, int segmentIndex) {
    if (departure == null || arrival == null) {
      return;
    }

    String cityReturnLabel =
        searchOptions.getTravelType() == TravelType.MULTIDESTINATION ? arrival.getIataCode()
            : arrival.getCityName();

    //If it is the first of the row
    if (segmentIndex == 0) {
      String cityDepartureLabel =
          searchOptions.getTravelType() == TravelType.MULTIDESTINATION ? departure.getIataCode()
              : departure.getCityName();

      RemoteViews departureCity =
          new RemoteViews(context.getPackageName(), R.layout.remote_views_label);
      departureCity.setTextViewText(R.id.lblCity, cityDepartureLabel);
      remoteViews.addView(R.id.lnlPlaces, departureCity);
    }

    RemoteViews plane = new RemoteViews(context.getPackageName(), R.layout.remote_views_image);
    remoteViews.addView(R.id.lnlPlaces, plane);

    RemoteViews arrivalCity =
        new RemoteViews(context.getPackageName(), R.layout.remote_views_label);
    arrivalCity.setTextViewText(R.id.lblCity, cityReturnLabel);
    remoteViews.addView(R.id.lnlPlaces, arrivalCity);
  }
}
