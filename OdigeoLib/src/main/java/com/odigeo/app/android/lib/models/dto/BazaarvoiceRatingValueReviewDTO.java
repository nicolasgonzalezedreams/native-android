package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class BazaarvoiceRatingValueReviewDTO {

  protected String key;
  protected BigDecimal rating;

  /**
   * Gets the value of the key property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getKey() {
    return key;
  }

  /**
   * Sets the value of the key property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setKey(String value) {
    this.key = value;
  }

  /**
   * Gets the value of the rating property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getRating() {
    return rating;
  }

  /**
   * Sets the value of the rating property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setRating(BigDecimal value) {
    this.rating = value;
  }
}
