package com.odigeo.app.android.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.navigator.BaseNavigator;
import com.odigeo.app.android.navigator.IdentificationsNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.dialogs.DateDialog;
import com.odigeo.app.android.view.dialogs.OnGetDateSelectedListener;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.presenter.IdentificationsPresenter;
import com.odigeo.presenter.contracts.navigators.IdentificationsNavigatorInterface;
import com.odigeo.presenter.contracts.views.IdentificationsViewInterface;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by carlos.navarrete on 10/09/15.
 */
public class IdentificationView extends BaseView<IdentificationsPresenter>
    implements IdentificationsViewInterface {

  public static final String TIMEZONE_GMT = "GMT";

  public static final String TITLE = "title";
  public static final String EXTRA_TYPE = "type";

  private String mTitle;
  private EditText mIdentificationNumber;
  private EditText mExpiryDate;
  private EditText mCountryIssue;
  private TextInputLayout mInputIdentificationDocumentNumber;
  private TextInputLayout mInputIdentificationExpireDate;
  private TextInputLayout mInputIdentificationCountry;
  private Button mDeleteIdentification;
  private DateDialog mDateDialog;

  private boolean errorRaised;

  private long mDateAsLong;
  private Country mCountry;
  private String mIdNumber;

  private UserIdentification.IdentificationType mIdentificationType;

  /**
   * Retrieves a new instance of the fragment Identification view
   *
   * @param identificationType Sets the title depending on the constant received, also sets the
   * corresponding validation of the document
   * @return a fragment of IdentificationView
   */
  public static IdentificationView newInstance(
      UserIdentification.IdentificationType identificationType, String numberId, long expireDate,
      String countryCode) {

    IdentificationView mIdentificationView = new IdentificationView();
    Bundle args = new Bundle();
    args.putSerializable(EXTRA_TYPE, identificationType);
    args.putString(IdentificationsNavigator.IDENTIFICATION_ID_NUMBER, numberId);
    args.putLong(IdentificationsNavigator.IDENTIFICATION_DATE, expireDate);
    args.putString(IdentificationsNavigator.IDENTIFICATION_COUNTRY, countryCode);
    mIdentificationView.setArguments(args);
    return mIdentificationView;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle bundle = getArguments();
    mIdentificationType =
        (UserIdentification.IdentificationType) (bundle != null ? bundle.getSerializable(EXTRA_TYPE)
            : "");

    if (bundle != null
        && bundle.getString(IdentificationsNavigator.IDENTIFICATION_ID_NUMBER) != null) {
      mDateAsLong = bundle.getLong(IdentificationsNavigator.IDENTIFICATION_DATE);
      String countryCode = bundle.getString(IdentificationsNavigator.IDENTIFICATION_COUNTRY);
      mCountry = mPresenter.getCountryByCC(LocaleUtils.getCurrentLanguageIso(), countryCode);
      mIdNumber = bundle.getString(IdentificationsNavigator.IDENTIFICATION_ID_NUMBER);
    }
    if (mIdentificationType.equals(UserIdentification.IdentificationType.NATIONAL_ID_CARD)) {
      mTitle =
          LocalizablesFacade.getString(getActivity(), OneCMSKeys.HEADER_NATIONAL_ID).toString();
    } else if (mIdentificationType.equals(UserIdentification.IdentificationType.PASSPORT)) {
      mTitle = LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_PASSPORT).toString();
    } else if (mIdentificationType.equals(UserIdentification.IdentificationType.NIF)) {
      mTitle = LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_NIF).toString();
    } else if (mIdentificationType.equals(UserIdentification.IdentificationType.NIE)) {
      mTitle = LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_NIE).toString();
    }
    setHasOptionsMenu(true);
    getActivity().getSupportFragmentManager().getBackStackEntryCount();
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = super.onCreateView(inflater, container, savedInstanceState);
    ((BaseNavigator) getActivity()).setNavigationTitle(mTitle);
    ((BaseNavigator) getActivity()).setUpToolbarButton(0);
    return view;
  }

  @Override protected IdentificationsPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideIdentificationsPresenter(this, (IdentificationsNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.sso_identificacion_information_view;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int itemId = item.getItemId();
    if (itemId == android.R.id.home) {
      mPresenter.getNavigatorInterface().showBackView();
      return true;
    } else if (itemId == R.id.sso_traveller_confirmation_identification) {
      if (!errorRaised) {
        if (mPresenter.validateFields(mIdentificationNumber.getText().toString(),
            mExpiryDate.getText().toString(), mCountryIssue.getText().toString())) {
          getPresenter().getNavigatorInterface()
              .returnIdentifications(mIdentificationNumber.getText().toString(), mDateAsLong,
                  mCountry, mIdentificationType);
        }
      }
    }
    return super.onOptionsItemSelected(item);
  }

  @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.sso_menu_identification_traveller, menu);
    MenuItem item = menu.findItem(R.id.sso_traveller_confirmation_identification);
    item.setTitle(LocalizablesFacade.getString(this.getActivity(), OneCMSKeys.DATA_STORAGE_SAVE));
    item.setIcon(
        DrawableUtils.getTintedResource(R.drawable.ic_done, R.color.actionbar_confirm_tint,
            getActivity()));
  }

  @Override protected void initComponent(View view) {
    mDateDialog = new DateDialog(getActivity(),
        LocalizablesFacade.getString(getContext(), OneCMSKeys.COMMON_OK).toString(),
        LocalizablesFacade.getString(getContext(), OneCMSKeys.COMMON_CANCEL).toString());
    mDateDialog.setMinDate(Calendar.getInstance().getTime());
    Typeface regularTypeFace = Configuration.getInstance().getFonts().getRegular();
    mInputIdentificationDocumentNumber =
        (TextInputLayout) view.findViewById(R.id.input_identification_number);
    mIdentificationNumber = (EditText) view.findViewById(R.id.edit_text_identification_number);
    mInputIdentificationExpireDate =
        (TextInputLayout) view.findViewById(R.id.input_expiration_date);
    mExpiryDate = (EditText) view.findViewById(R.id.edit_text_expiration_date);
    mInputIdentificationCountry = (TextInputLayout) view.findViewById(R.id.input_country_issue);
    mCountryIssue = (EditText) view.findViewById(R.id.edit_text_country_issue);
    mDeleteIdentification = (Button) view.findViewById(R.id.button_delete_identification);

    mInputIdentificationDocumentNumber.setTypeface(regularTypeFace);
    mIdentificationNumber.setTypeface(regularTypeFace);
    mInputIdentificationExpireDate.setTypeface(regularTypeFace);
    mExpiryDate.setTypeface(regularTypeFace);
    mInputIdentificationCountry.setTypeface(regularTypeFace);
    mCountryIssue.setTypeface(regularTypeFace);
    mDeleteIdentification.setTypeface(regularTypeFace);

    mDeleteIdentification.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_IDENTIFICATION_DELETE));
    mIdentificationNumber.setText(mIdNumber);
    if (mCountry != null && !StringUtils.isEmpty(mCountry.getCountryCode())) {
      mCountryIssue.setText(mCountry.getName());
      mDeleteIdentification.setVisibility(View.VISIBLE);
    }
    if (mDateAsLong != 0) {
      setExpirationDate(mDateAsLong);
    }

    if (mIdentificationType.equals(UserIdentification.IdentificationType.PASSPORT)) {
      mInputIdentificationDocumentNumber.getEditText()
          .setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
    }
  }

  @Override protected void setListeners() {
    mCountryIssue.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.getNavigatorInterface()
            .navigateToSelectCountry(IdentificationsNavigator.TAG_IDENTIFICATIONS,
                Constants.SELECTING_TRAVELLER_NATIONALITY);
      }
    });

    mExpiryDate.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mIdentificationNumber.clearFocus();
        showDialog(mDateDialog);
      }
    });
    //TODO when refactor user BaseOnFocusChange class.
    mIdentificationNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override public void onFocusChange(View view, boolean hasFocus) {
        if (!hasFocus) {
          if (mIdentificationType.equals(UserIdentification.IdentificationType.NATIONAL_ID_CARD)) {
            //Sets the validation for ID CARD
            if (!mPresenter.validateDNI(mIdentificationNumber.getText().toString())) {
              showOnNumberIdentificationError();
              errorRaised = true;
            } else {
              mInputIdentificationDocumentNumber.setError(null);
              errorRaised = false;
            }
          } else if (mIdentificationType.equals(UserIdentification.IdentificationType.PASSPORT)) {
            //Sets the validation for PASSPORT
            if (!mPresenter.validatePassport(mIdentificationNumber.getText().toString())) {
              showOnNumberIdentificationError();
              errorRaised = true;
            } else {
              mInputIdentificationDocumentNumber.setError(null);
              errorRaised = false;
            }
          } else if (mIdentificationType.equals(UserIdentification.IdentificationType.NIF)) {
            //Sets the validation for DNI
            if (!mPresenter.validateDNI(mIdentificationNumber.getText().toString())) {
              showOnNumberIdentificationError();
              errorRaised = true;
            } else {
              mInputIdentificationDocumentNumber.setError(null);
              errorRaised = false;
            }
          } else if (mIdentificationType.equals(UserIdentification.IdentificationType.NIE)) {
            //Sets the validation for NIE
            if (!mPresenter.validateNIE(mIdentificationNumber.getText().toString())) {
              showOnNumberIdentificationError();
              errorRaised = true;
            } else {
              mInputIdentificationDocumentNumber.setError(null);
              errorRaised = false;
            }
          }
        }
      }
    });

    mDeleteIdentification.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mExpiryDate.setText("");
        mIdentificationNumber.setText("");
        mCountryIssue.setText("");
        mPresenter.getNavigatorInterface()
            .returnIdentifications(null, -1, null, mIdentificationType);
      }
    });
  }

  public void setCountryIssueSelected(Country country) {
    mCountry = country;
    mCountryIssue.setText(country.getName());
  }

  @Override protected void initOneCMSText(View view) {
    mInputIdentificationDocumentNumber.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_IDENTIFICATION));
    mInputIdentificationExpireDate.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.CREDIT_CARD_EXPIRATION_DATE));
    mInputIdentificationCountry.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.COUNTRY_SELECTED_ERROR));
  }

  private void showDialog(DateDialog dateDialog) {
    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE_GMT));
    if (mDateAsLong != 0) {
      calendar.setTimeInMillis(mDateAsLong);
    }
    dateDialog.createDateDialog(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH));
    dateDialog.showDialog(new OnGetDateSelectedListener() {
      @Override public void onGetDate(Date date) {
        if (date != null) {
          setExpirationDate(date.getTime());
          mDateAsLong = date.getTime();
        }
      }
    }).show();
  }

  private void setExpirationDate(long dateInMilliseconds) {
    Date date = new Date();
    date.setTime(dateInMilliseconds);
    String presentationDate = getResources().getString(R.string.templates__datelong1);
    SimpleDateFormat simpleDateFormat = OdigeoDateUtils.getGmtDateFormat(presentationDate);
    mExpiryDate.setText(simpleDateFormat.format(date));
  }

  @Override public void showOnNumberIdentificationError() {
    mInputIdentificationDocumentNumber.setError(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.VALIDATION_ERROR_IDENTIFICATION));
  }

  @Override public void showOnExpireDateError() {
    mInputIdentificationExpireDate.setError(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.CREDIT_CARD_EXPIRATION_DATE));
  }

  @Override public void showOnCountrySelectedError() {
    mInputIdentificationCountry.setError(LocalizablesFacade.getString(getActivity(),
        OneCMSKeys.VALIDATION_ERROR_SELECTING_COUNTRIES));
  }
}
