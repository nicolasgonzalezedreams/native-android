/**
 * Created by Ing. Jesús Fernando Sierra Pastrana on 10/02/2015.
 */

package com.odigeo.app.android.lib.utils;

import android.support.annotation.NonNull;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.utils.BrandUtils;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.odigeo.app.android.utils.BrandUtils.LOCALE_KEY_FI;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * This class wraps all the behavior and utils necessary to work with different {@link
 * java.util.Locale} objects and retrieve the correct values and characters.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 10/02/2015
 */
public final class LocaleUtils {

  /**
   * Default length of the string for the destination search.
   */
  public static final int DEFAULT_SEARCH_LENGTH = 3;
  public static final String EN_GB_KEY = "en_GB";
  public static final String JA_JP_KEY = "ja_JP";
  public static final String ES_CO_KEY = "es_CO";
  public static final String IN_ID_KEY = "in_ID";
  private static final String EURO_COIN = "€";
  private static final String DOLLAR_COIN = "$";
  private static final String POUND = "£";
  private static final String CURRENCY_FORMAT = "%s";
  private static final String STRING_SPACE = " ";
  private static final String DIRHAM_UAE_COIN = "درهم";
  /**
   * Here gonna be placed the currency formats.
   */
  private static final Map<String, String> CURRENCIES_FORMAT_MAP = new HashMap<>();
  /**
   * Here gonna be placed the locales who needs perform a conversion to work correctly. When use a
   * some util method and return a RuntimeException.
   */
  private static final Map<String, String> LOCATIONS_MAP = new HashMap<>();
  /**
   * Here gonna be placed the string length for every locale supported, the key it's the correct
   * locale string retrieved for {@link #getISOLocale(String)}.
   */
  private static final Map<String, Integer> SEARCH_LENGTH_MAP = new HashMap<>();

  static {
    LOCATIONS_MAP.put("en", "en_IE");
    LOCATIONS_MAP.put("en_IE", "en_IE");
    LOCATIONS_MAP.put("en_CA", "en_CA");
    LOCATIONS_MAP.put("fr_CA", "fr_CA");
    LOCATIONS_MAP.put("en_US", "en_US");
    LOCATIONS_MAP.put("es_US", "es_US");
    LOCATIONS_MAP.put(EN_GB_KEY, EN_GB_KEY);
    LOCATIONS_MAP.put("en_UK", EN_GB_KEY);
    LOCATIONS_MAP.put("el_GR", "el_GR");
    LOCATIONS_MAP.put("ru_RU", "ru_RU");
    LOCATIONS_MAP.put("fr_CH", "fr_CH");
    LOCATIONS_MAP.put("de_CH", "de_CH");
    LOCATIONS_MAP.put("it_CH", "it_CH");
    LOCATIONS_MAP.put("nl_NL", "nl_NL");
    LOCATIONS_MAP.put("tr_TR", "tr_TR");
    LOCATIONS_MAP.put("en_TR", "tr_TR");
    LOCATIONS_MAP.put("hi_IN", "hi_IN");
    LOCATIONS_MAP.put("en_ID", "in_ID");
    LOCATIONS_MAP.put("en_PH", "tg_PH");
    LOCATIONS_MAP.put("zh_CN", "zh_CN");
    LOCATIONS_MAP.put("en_SG", "en_SG");
    LOCATIONS_MAP.put("en_HK", "zh_HK");
    LOCATIONS_MAP.put("en_TH", "en_TH");
    LOCATIONS_MAP.put(JA_JP_KEY, "ja_JP");
    LOCATIONS_MAP.put("en_AU", "en_AU");
    LOCATIONS_MAP.put("en_NZ", "en_NZ");
    LOCATIONS_MAP.put("en_ZA", "en_ZA");
    LOCATIONS_MAP.put("en_AE", "en_AE");
    LOCATIONS_MAP.put("fr_MA", "fr_MA");
    LOCATIONS_MAP.put("en_EG", "en_EG");
    LOCATIONS_MAP.put("pt_PT", "pt_PT");
    LOCATIONS_MAP.put("pt_BR", "pt_BR");
    LOCATIONS_MAP.put("es_AR", "es_AR");
    LOCATIONS_MAP.put("es_CL", "es_CL");
    LOCATIONS_MAP.put(ES_CO_KEY, ES_CO_KEY);
    LOCATIONS_MAP.put("es_MX", "es_MX");
    LOCATIONS_MAP.put("es_PE", "es_PE");
    LOCATIONS_MAP.put("es_VE", "es_VE");
    LOCATIONS_MAP.put("es_ES", "es_ES");
    LOCATIONS_MAP.put("fr_FR", "fr_FR");
    LOCATIONS_MAP.put("it_IT", "it_IT");
    LOCATIONS_MAP.put("de_DE", "de_DE");
    LOCATIONS_MAP.put("da_DK", "da_DK");
    LOCATIONS_MAP.put("fi_FI", "fi_FI");
    LOCATIONS_MAP.put("no_NO", "no_NO");
    LOCATIONS_MAP.put("sv_SE", "sv_SE");
    LOCATIONS_MAP.put("ko_KR", "ko_KR");
    LOCATIONS_MAP.put("zh_TW", "zh_TW");
    LOCATIONS_MAP.put("vi_VN", "vi_VN");
    LOCATIONS_MAP.put("ro_RO", "ro_RO");
    LOCATIONS_MAP.put("cs_CZ", "cs_CZ");
    LOCATIONS_MAP.put("hu_HU", "hu_HU");
    LOCATIONS_MAP.put("is_IS", "is_IS");
    LOCATIONS_MAP.put("pl_PL", "pl_PL");

    SEARCH_LENGTH_MAP.put(JA_JP_KEY, 1);

    CURRENCIES_FORMAT_MAP.put("en_CA", DOLLAR_COIN + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("fr_CA", CURRENCY_FORMAT + " " + DOLLAR_COIN);
    CURRENCIES_FORMAT_MAP.put("en_US", DOLLAR_COIN + " " + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("es_US", DOLLAR_COIN + " " + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put(EN_GB_KEY, "£ " + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("en_IE", EURO_COIN + " " + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("el_GR", CURRENCY_FORMAT + " " + EURO_COIN);
    CURRENCIES_FORMAT_MAP.put("ru_RU", CURRENCY_FORMAT + " руб");
    CURRENCIES_FORMAT_MAP.put("fr_CH", CURRENCY_FORMAT + " CHF");
    CURRENCIES_FORMAT_MAP.put("de_CH", "CHF " + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("it_CH", CURRENCY_FORMAT + " CHF");
    CURRENCIES_FORMAT_MAP.put("nl_NL", EURO_COIN + " " + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("tr_TR", CURRENCY_FORMAT + " TL");
    CURRENCIES_FORMAT_MAP.put("hi_IN", "₹ " + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("in_ID", "Rp" + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("tg_PH", "₱" + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("zh_CN", "￥" + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("en_SG", "S" + DOLLAR_COIN + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("zh_HK", "HK" + DOLLAR_COIN + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("en_TH", "฿" + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put(JA_JP_KEY, "￥" + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("en_AU", "AU" + DOLLAR_COIN + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("en_NZ", "NZ" + DOLLAR_COIN + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("en_ZA", "R" + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("en_AE", DIRHAM_UAE_COIN + STRING_SPACE + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("fr_MA", "DH " + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("en_EG", "E" + POUND + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("pt_PT", CURRENCY_FORMAT + " " + EURO_COIN);
    CURRENCIES_FORMAT_MAP.put("pt_BR", "R" + DOLLAR_COIN + " " + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("es_AR", DOLLAR_COIN + " " + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("es_CL", DOLLAR_COIN + " " + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put(ES_CO_KEY, DOLLAR_COIN + " " + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("es_MX", DOLLAR_COIN + " " + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("es_PE", "S/." + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("es_VE", "Bs.F " + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("es_ES", CURRENCY_FORMAT + " " + EURO_COIN);
    CURRENCIES_FORMAT_MAP.put("fr_FR", CURRENCY_FORMAT + " " + EURO_COIN);
    CURRENCIES_FORMAT_MAP.put("it_IT", EURO_COIN + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("de_DE", CURRENCY_FORMAT + " " + EURO_COIN);
    CURRENCIES_FORMAT_MAP.put("da_DK", CURRENCY_FORMAT + " DKK");
    CURRENCIES_FORMAT_MAP.put("fi_FI", CURRENCY_FORMAT + " " + EURO_COIN);
    CURRENCIES_FORMAT_MAP.put("no_NO", CURRENCY_FORMAT + " NOK");
    CURRENCIES_FORMAT_MAP.put("sv_SE", CURRENCY_FORMAT + " SEK");
    CURRENCIES_FORMAT_MAP.put("ko_KR", "￦ " + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("zh_TW", "NTD" + CURRENCY_FORMAT);
    CURRENCIES_FORMAT_MAP.put("vi_VN", CURRENCY_FORMAT + " ₫");
    CURRENCIES_FORMAT_MAP.put("ro_RO", CURRENCY_FORMAT + " RON");
    CURRENCIES_FORMAT_MAP.put("cs_CZ", CURRENCY_FORMAT + " Kč");
    CURRENCIES_FORMAT_MAP.put("hu_HU", CURRENCY_FORMAT + " Ft");
    CURRENCIES_FORMAT_MAP.put("is_IS", CURRENCY_FORMAT + " kr.");
    CURRENCIES_FORMAT_MAP.put("pl_PL", CURRENCY_FORMAT + " PLN");
  }

  /**
   * To prevent the instantiation.
   */
  private LocaleUtils() {
  }

  /**
   * This method takes a string representation of {@link java.util.Locale} and return the correct
   * Locale object.
   *
   * @param locale String representation of the locate (Example. "en_US").
   * @return A correct Locale with the correct language and country.
   */
  private static Locale getISOLocale(String locale) {
    String isoLocale = LOCATIONS_MAP.get(locale);
    String[] values = isoLocale.split("_");
    Locale value = null;

    if (LOCATIONS_MAP.containsKey(locale)) {
      if (values.length > 1) {
        try {
          value = new Locale(values[0], values[1]);
        } catch (RuntimeException e) {
          System.out.println("Can't be converted the map value into Locale");
        }
      }
    } else {
      throw new IllegalArgumentException("The locale " + locale + " isn't in the map.");
    }
    return value;
  }

  /**
   * Retrieves the correct and current {@link java.util.Locale}
   *
   * @return Correct Locale.
   */
  public static Locale getCurrentLocale() {
    return getISOLocale(localeToString(Configuration.getCurrentLocale()));
  }

  public static String getCurrentLanguageIso() {
    return Configuration.getCurrentLocale().getLanguage();
  }

  /**
   * Retrieves the current ISO Locale code in String ("en_US").
   *
   * @return ISO Locale code.
   */
  public static String getCurrentIsoLocale() {
    String locale = localeToString(Configuration.getCurrentLocale());
    if (LOCATIONS_MAP.containsKey(locale)) {
      return LOCATIONS_MAP.get(locale);
    } else {
      throw new IllegalArgumentException("The locale " + locale + " isn't in the map.");
    }
  }

  /**
   * Takes a {@link Locale} object and returns the String representation in the "en_US" format.
   *
   * @param locale Locale to be converted.
   * @return String representation in format "en_US"
   */
  public static String localeToString(Locale locale) {
    if (isEmpty(locale.getCountry())) {
      return locale.getLanguage();
    } else {
      return locale.getLanguage() + '_' + locale.getCountry();
    }
  }

  /**
   * Retrieves the symbol of the currency for the {@link java.util.Locale} passed in string
   * representation in format "en_US".
   *
   * @param locale String representation of the Locale where be obtained the symbol.
   * @return Correct symbol of the currency.
   */
  public static String getCurrencySymbol(String locale) {
    Locale isoLocale = getISOLocale(locale);
    return Currency.getInstance(isoLocale).getSymbol(isoLocale);
  }

  /**
   * Retrieves the symbol of the currency for the {@link java.util.Locale} in {@link
   * com.odigeo.app.android.lib.config.Configuration#getCurrentLocale()}
   *
   * @return Correct symbol of the currency.
   */
  public static String getCurrencySymbol() {
    return getCurrencySymbol(localeToString(Configuration.getCurrentLocale()));
  }

  /**
   * Retrieves the currency code defined in the ISO 4217 for the {@link java.util.Locale} in
   * string representation format "en_US".
   *
   * @param locale String representation of the Locale where be obtained the currency code.
   * @return Correct currency code.
   */
  public static String getCurrencyCode(String locale) {
    Locale isoLocale = getISOLocale(locale);
    return Currency.getInstance(isoLocale).getCurrencyCode();
  }

  /**
   * Retrieves the currency code defined in the ISO 4217 for the {@link java.util.Locale} in
   * {@link com.odigeo.app.android.lib.config.Configuration#getCurrentLocale()}
   *
   * @return Correct currency code.
   */
  public static String getCurrencyCode() {
    return getCurrencyCode(localeToString(Configuration.getCurrentLocale()));
  }

  /**
   * use {@link com.odigeo.interactors.provider.MarketProviderInterface}
   */
  @Deprecated public static String getLocalizedCurrencyValue(double value, String locale) {
    Locale isoLocale = getISOLocale(locale);
    NumberFormat numberFormat;
    //For arabic languages let the Locale do all the job
    String localeString = localeToString(isoLocale);
    if ("ar_AE".equals(localeString)) {
      numberFormat = NumberFormat.getCurrencyInstance(isoLocale);
    } else {
      //For all the others only format numbers
      numberFormat = NumberFormat.getNumberInstance(isoLocale);
      //Special case for Japan, don't use fractions of coins
      if (JA_JP_KEY.equals(localeString) || IN_ID_KEY.equals(localeString) || ES_CO_KEY.equals(
          localeString)) {
        numberFormat.setMaximumFractionDigits(0);
        numberFormat.setMinimumFractionDigits(0);
      } else {
        //For all the others currencies show 2 fractions digits
        numberFormat.setMinimumFractionDigits(2);
        numberFormat.setMaximumFractionDigits(2);
      }
    }
    return String.format(isoLocale, CURRENCIES_FORMAT_MAP.get(localeString),
        numberFormat.format(value));
  }

  private static String getLocalizedCurrencyValue(double value, String locale,
      boolean validateDecimals) {
    Locale isoLocale = getISOLocale(locale);
    NumberFormat numberFormat;
    //For arabic languages let the Locale do all the job
    String localeString = localeToString(isoLocale);
    if ("ar_AE".equals(localeString)) {
      numberFormat = NumberFormat.getCurrencyInstance(isoLocale);
    } else {
      //For all the others only format numbers
      numberFormat = NumberFormat.getNumberInstance(isoLocale);
      if (validateDecimals && showPriceWithoutDecimal(locale)) {
        numberFormat.setMaximumFractionDigits(0);
        numberFormat.setMinimumFractionDigits(0);
      } else {
        //For all the others currencies show 2 fractions digits
        numberFormat.setMinimumFractionDigits(2);
        numberFormat.setMaximumFractionDigits(2);
      }
    }
    return String.format(isoLocale, CURRENCIES_FORMAT_MAP.get(localeString),
        numberFormat.format(value));
  }

  private static boolean showPriceWithoutDecimal(String locale) {
    return JA_JP_KEY.equals(locale)
        || IN_ID_KEY.equals(locale)
        || ES_CO_KEY.equals(locale)
        || showNordicPriceWithoutDecimals(locale);
  }

  private static boolean showNordicPriceWithoutDecimals(String locale) {
    return BrandUtils.isNordicCountryByLocale(locale) && !LOCALE_KEY_FI.equalsIgnoreCase(locale);
  }

  /**
   * Retrieves a value correctly formatted depending of the locale in {@link
   * com.odigeo.app.android.lib.config.Configuration#getCurrentLocale()}.
   *
   * @param value Value to be converted.
   * @return Correct format of currency for the value and the locale passed.
   */
  public static String getLocalizedCurrencyValue(double value) {
    return getLocalizedCurrencyValue(value, localeToString(Configuration.getCurrentLocale()),
        false);
  }

  public static String getLocalizedCurrencyValue(double value, Locale locale,
      boolean validateDecimals) {
    if (validateDecimals) {
      return getLocalizedCurrencyValue(value, localeToString(locale), true);
    }
    return getLocalizedCurrencyValue(value);
  }

  /**
   * Retrieves a integer value correctly localized.
   *
   * @param value Value to be localized.
   * @param locale String representation of the locate (Example. "en_US") where be obtained the
   * number
   * format.
   * @return String representation of the value correctly localized.
   */
  public static String getLocalizedNumber(int value, String locale) {
    NumberFormat numberFormat = NumberFormat.getNumberInstance(getISOLocale(locale));
    return numberFormat.format(value);
  }

  /**
   * Retrieves a integer value correctly localized with the Locale obtained from {@link
   * com.odigeo.app.android.lib.config.Configuration#getCurrentLocale()}.
   *
   * @param value Value to be localized.
   * @return String representation of the value correctly localized.
   */
  public static String getLocalizedNumber(int value) {
    return getLocalizedNumber(value, localeToString(Configuration.getCurrentLocale()));
  }

  /**
   * Retrieves a integer value correctly localized.
   *
   * @param value Value to be localized.
   * @param locale String representation of the locate (Example. "en_US") where be obtained the
   * number
   * format.
   * @return String representation of the value correctly localized.
   */
  public static String getLocalizedNumber(double value, String locale) {
    NumberFormat numberFormat = NumberFormat.getNumberInstance(getISOLocale(locale));
    return numberFormat.format(value);
  }

  /**
   * Retrieves a integer value correctly localized with the Locale obtained from {@link
   * com.odigeo.app.android.lib.config.Configuration#getCurrentLocale()}.
   *
   * @param value Value to be localized.
   * @return String representation of the value correctly localized.
   */
  public static String getLocalizedNumber(double value) {
    return getLocalizedNumber(value, localeToString(Configuration.getCurrentLocale()));
  }

  /**
   * Retrieves the correct length of the search criteria for destination. If not exists a specific
   * rule for the current locale in {@link com.odigeo.app.android.lib.config.Configuration#getCurrentLocale()},
   * then retrieves the {@link #DEFAULT_SEARCH_LENGTH} value.
   *
   * @return The corresponding value for the minimum string length search for the current locale.
   */
  public static int getLocalizedSearchLength() {
    String currentLocale = localeToString(Configuration.getCurrentLocale());
    if (SEARCH_LENGTH_MAP.containsKey(currentLocale)) {
      return SEARCH_LENGTH_MAP.get(currentLocale);
    } else {
      return DEFAULT_SEARCH_LENGTH;
    }
  }

  public static boolean isJapaneseCharacter(String query) {
    return query.matches("[\\p{Hiragana}\\p{Katakana}\\p{Han}]*+");
  }

  /**
   * use {@link com.odigeo.interactors.provider.MarketProviderInterface}
   */
  @Deprecated public static Locale strToLocale(String strLocale) {
    String[] strLocaleParts;

    strLocaleParts = strLocale.split("_");

    if (strLocaleParts.length < 2) {
      strLocaleParts = strLocale.split("-");
    }

    String language = null;
    String country = null;

    if (strLocaleParts.length > 0) {
      language = strLocaleParts[0].toLowerCase();
    }

    if (strLocaleParts.length > 1) {
      country = strLocaleParts[1].toUpperCase();
    }

    Locale locale;

    if (country == null) {
      locale = new Locale(language);
    } else {
      locale = new Locale(language, country);
    }

    return locale;
  }

  @NonNull public static String getLocalizedCurrencyFeeValue(Double fee) {
    String locale = localeToString(Configuration.getCurrentLocale());
    return " (+ " + getLocalizedCurrencyValue(fee, locale) + ")";
  }
}
