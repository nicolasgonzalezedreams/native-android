package com.odigeo.app.android.lib.data;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import com.localytics.android.Localytics;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.database.daos.LocalizableDao;
import com.odigeo.app.android.lib.data.database.model.Localizable;
import com.odigeo.dataodigeo.tracker.RootUtil;
import java.util.HashMap;
import java.util.Map;

/**
 * Read Odigeo Strings from Database.
 *
 * @author Cristian Fanjul
 * @since 11/05/2015
 */
public class LocalizablesFacade {

  public static final String NOT_FOUND = "(%s not found)";
  public static final String NOT_FOUND_TEXT = "not found)";
  public static final String ELEMENT_TO_REPLACE = "%s";
  private static final String TRACKING_CONTENT_NOT_FOUND_ATTRIBUTE_KEY = "Content Not Found";
  private static final String TRACKING_CONTENT_NOT_FOUND_EVENT = "contentKey";

  /**
   * This is the basic method to obtain a String by its key.
   *
   * @param key The key value for a localizable.
   */
  public static CharSequence getString(@NonNull Context context, @NonNull String key) {
    return getString(context, key, (String) null);
  }

  /**
   * Gets a String by its key, and allows to replace elements inside it.
   *
   * @param context A context that invokes the database.
   * @param key The key value for a localizable.
   * @param params Every string object for each %s to replace.
   * @return CharSequence.
   */
  public static CharSequence getString(@NonNull Context context, @NonNull String key,
      @Nullable String... params) {
    Localizable localizable = LocalizableDao.getLocalizableByKey(context, parseDash(key));

    if (localizable.getValue() != null) {
      if (localizable.getValue().contains(ELEMENT_TO_REPLACE)) {
        try {
          return Html.fromHtml(String.format(localizable.getValue(), (Object[]) params));
        } catch (Exception ex) {
          return localizable.getValue();
        }
      } else {
        return Html.fromHtml(localizable.getValue());
      }
    }
    trackNotFound(key);
    return String.format(NOT_FOUND, key);
  }

  private static void trackNotFound(String key) {
    if (Constants.isTestEnvironment) {
      Map<String, String> attributes = new HashMap<>();
      attributes.put(TRACKING_CONTENT_NOT_FOUND_ATTRIBUTE_KEY, key);
      if (!RootUtil.isDeviceRooted()) {
        Localytics.tagEvent(TRACKING_CONTENT_NOT_FOUND_EVENT, attributes);
      }
    }
  }

  /**
   * This method should be used to get a String exactly as it is stored in the database. It may
   * include html tags or elements that should
   * be replaced (%s).
   *
   * @param key The key value for a localizable.
   */
  public static String getRawString(@NonNull Context context, @NonNull String key) {
    Localizable localizable = LocalizableDao.getLocalizableByKey(context, parseDash(key));

    if (localizable.getValue() != null) {
      return localizable.getValue();
    }

    return String.format(NOT_FOUND, key);
  }

  public static String getRawString(@NonNull Context context, @NonNull String key,
      @Nullable String... params) {
    Localizable localizable = LocalizableDao.getLocalizableByKey(context, parseDash(key));

    if (localizable.getValue() != null) {

      if (localizable.getValue().contains(ELEMENT_TO_REPLACE)) {
        try {
          return String.format(localizable.getValue(), (Object[]) params);
        } catch (Exception ex) {
          return localizable.getValue();
        }
      } else {
        return localizable.getValue();
      }
    }

    return String.format(NOT_FOUND, key);
  }

  private static String parseDash(String key) {
    if (key != null) {
      return key.replace("-", "]");
    } else {
      return null;
    }
  }
}
