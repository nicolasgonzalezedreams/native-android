package com.odigeo.app.android.view;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.fragments.MSLErrorUtilsDialogFragment;
import com.odigeo.app.android.lib.models.dto.custom.Code;
import com.odigeo.app.android.lib.models.dto.custom.MslError;
import com.odigeo.app.android.lib.ui.widgets.TimeoutCounterWidget;
import com.odigeo.app.android.lib.ui.widgets.TopBriefWidget;
import com.odigeo.app.android.lib.ui.widgets.base.BlackDialog;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.exceptions.ShoppingCartResponseException;
import com.odigeo.app.android.model.BookingInfoViewModel;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.dialogs.BookingOutdatedDialog;
import com.odigeo.app.android.view.helpers.BookingFlowTimeoutHelper;
import com.odigeo.data.entity.shoppingCart.AvailableProductsResponse;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.FlowConfigurationResponse;
import com.odigeo.data.entity.shoppingCart.Insurance;
import com.odigeo.data.entity.shoppingCart.InsuranceOffer;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.helper.CrashlyticsUtil;
import com.odigeo.presenter.InsurancesPresenter;
import com.odigeo.presenter.contracts.navigators.InsurancesNavigatorInterface;
import com.odigeo.presenter.contracts.views.InsurancesViewInterface;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class InsurancesView extends BaseView<InsurancesPresenter>
    implements InsurancesViewInterface, InsuranceWidget.InsuranceCardListener,
    InsuranceDeclineWidget.InsuranceDeclineWidgetListener,
    BookingFlowTimeoutHelper.TimeoutListener {

  private static final long REPRICING_TIME = 8000;
  private static final long DELAY_TIME = 500;
  private final String mLocaleMarket = LocaleUtils.localeToString(Configuration.getCurrentLocale());

  private List<InsuranceWidget> mInsurancesWidgetList;
  private InsuranceDeclineWidget mInsuranceDeclineWidget;
  private LinearLayout mLlContainerInsurances;
  private List<InsuranceOffer> insuranceOffersSelected;
  private ScrollView mSvInsurances;
  private CheckBox selectedOption;
  private BlackDialog mLoadingDialog;
  private String mLoadingDialogText;
  private Button mBtnContinue;
  private TextView mTvInsuranceInfoHeader, mTvConditionsAcceptanceOnContinue;
  private LinearLayout mLlWidening;
  private TextView mTvWidening;
  private Handler mHandlerRepricing = new Handler();
  private Runnable mRunnableRepricing;
  private TimeoutCounterWidget mTimeoutCounterWidget;
  private BookingFlowTimeoutHelper mTimerHelper;
  private boolean mHasToShowTimeoutDialog = true;
  private InsuranceWidget mRecommendedInsuranceWidget;
  private CreateShoppingCartResponse mCreateShoppingCartResponse;
  private AvailableProductsResponse mAvailableProductsResponse;
  private FlowConfigurationResponse mFlowConfigurationResponse;
  private boolean mIsFullTransparency;
  private double mLastTicketsPrice;
  private SearchOptions mSearchOptions;
  private View mView;
  private boolean mHasMandatoryWidget;
  private BookingInfoViewModel bookingInfo;

  public static InsurancesView newInstance(CreateShoppingCartResponse createShoppingCartResponse,
      AvailableProductsResponse availableProductsResponse,
      FlowConfigurationResponse flowConfigurationResponse, boolean isFullTransparency,
      double lastTicketsPrice, SearchOptions searchOptions, BookingInfoViewModel bookingInfo) {
    InsurancesView insurancesView = new InsurancesView();
    Bundle bundle = new Bundle();
    bundle.putSerializable(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE,
        createShoppingCartResponse);
    bundle.putSerializable(Constants.EXTRA_AVAILABLE_PRODUCTS_RESPONSE, availableProductsResponse);
    bundle.putSerializable(Constants.EXTRA_FLOW_CONFIGURATION_RESPONSE, flowConfigurationResponse);
    bundle.putBoolean(Constants.EXTRA_FULL_TRANSPARENCY, isFullTransparency);
    bundle.putDouble(Constants.EXTRA_REPRICING_TICKETS_PRICES, lastTicketsPrice);
    bundle.putSerializable(Constants.EXTRA_SEARCH_OPTIONS, searchOptions);
    bundle.putSerializable(Constants.EXTRA_BOOKING_INFO, bookingInfo);
    insurancesView.setArguments(bundle);
    return insurancesView;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mCreateShoppingCartResponse = (CreateShoppingCartResponse) getArguments().getSerializable(
        Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE);
    mAvailableProductsResponse = (AvailableProductsResponse) getArguments().getSerializable(
        Constants.EXTRA_AVAILABLE_PRODUCTS_RESPONSE);
    mFlowConfigurationResponse = (FlowConfigurationResponse) getArguments().getSerializable(
        Constants.EXTRA_FLOW_CONFIGURATION_RESPONSE);
    mIsFullTransparency = getArguments().getBoolean(Constants.EXTRA_FULL_TRANSPARENCY);
    mLastTicketsPrice = getArguments().getDouble(Constants.EXTRA_REPRICING_TICKETS_PRICES);
    mSearchOptions = (SearchOptions) getArguments().getSerializable(Constants.EXTRA_SEARCH_OPTIONS);
    bookingInfo = (BookingInfoViewModel) getArguments().getSerializable(Constants
        .EXTRA_BOOKING_INFO);
    mHasMandatoryWidget = false;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    initPresenter();
    initTopBrief();
  }

  @Override protected InsurancesPresenter getPresenter() {
    return dependencyInjector
        .provideInsurancesPresenter(this, (InsurancesNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.insurance_view;
  }

  @Override protected void initComponent(View view) {
    mView = view;
    mSvInsurances = (ScrollView) view.findViewById(R.id.svInsurances);
    mTvInsuranceInfoHeader = (TextView) view.findViewById(R.id.tvInsuranceInfoHeader);
    mLlContainerInsurances = (LinearLayout) view.findViewById(R.id.llContainerInsurance);
    mBtnContinue = (Button) view.findViewById(R.id.btnContinue);
    mTvConditionsAcceptanceOnContinue =
        (TextView) view.findViewById(R.id.tvConditionsAcceptanceOnContinue);
    insuranceOffersSelected = new ArrayList<>();
    mLlWidening = (LinearLayout) view.findViewById(R.id.widening);
    mTvWidening = (TextView) mLlWidening.findViewById(R.id.tvWideningLeyend);
    mTimeoutCounterWidget = (TimeoutCounterWidget) view.findViewById(R.id.timeoutCounterWidget);
    initRunnableRepricing();
    initTimeoutBookingFlow();
  }

  private void initTopBrief() {
    TopBriefWidget topBriefWidget = ((TopBriefWidget) mView.findViewById(R.id.topbrief_insurances));
    topBriefWidget.addData(mSearchOptions);
    if (mIsFullTransparency || mPresenter.isMemberForCurrentMarket()) {
      topBriefWidget.setMembershipApplied(mPresenter.shouldApplyMembershipPerks());
      topBriefWidget.updatePrice(mPresenter.getTotalPrice());
    }
  }

  private void initRunnableRepricing() {
    mRunnableRepricing = new Runnable() {
      @Override public void run() {
        mLlWidening.setVisibility(View.GONE);
      }
    };
  }

  private void initTimeoutBookingFlow() {
    mTimerHelper = new BookingFlowTimeoutHelper(this);
    mTimerHelper.checkTimeoutConditions(getArguments());
  }

  private void initPresenter() {
    BigDecimal collectionMethodWithPriceValue = BigDecimal.ZERO;
    if(bookingInfo.getCollectionMethodWithPrice() != null) {
      collectionMethodWithPriceValue = bookingInfo.getCollectionMethodWithPrice().getPrice();
    }
    mPresenter.initializePresenter(mAvailableProductsResponse, mCreateShoppingCartResponse,
        mFlowConfigurationResponse, mLastTicketsPrice, Configuration.getCurrentLocale(),
        collectionMethodWithPriceValue);
  }

  public void updateCreateShoppingCartResponse(
      CreateShoppingCartResponse createShoppingCartResponse) {
    mCreateShoppingCartResponse = createShoppingCartResponse;
    initTopBrief();
    mPresenter.updateCreateShoppingCartResponse(mCreateShoppingCartResponse);
  }

  @Override public void onResume() {
    super.onResume();
    mHasToShowTimeoutDialog = true;
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_INSURANCES);
  }

  @Override public void onStop() {
    super.onStop();
    mHasToShowTimeoutDialog = false;
  }

  @Override protected void setListeners() {
    setTimerListener();

    mBtnContinue.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        onAddInsurancesButtonClick();
      }
    });
  }

  private void setTimerListener() {
    mTimeoutCounterWidget.setListener(mTimerHelper.getTimeoutWidgetListener());
  }

  @Override protected void initOneCMSText(View view) {
    mTvInsuranceInfoHeader.setText(LocalizablesFacade.getString(getActivity(),
        OneCMSKeys.INSURANCESVIEWCONTROLLER_INFORMATIVEMODULE_TEXT));
    mBtnContinue.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.COMMON_BUTTONCONTINUE));
    mTvConditionsAcceptanceOnContinue.setText(LocalizablesFacade.getString(getActivity(),
        OneCMSKeys.INSURANCESVIEWCONTROLLER_CONDITIONSMODULE_TEXT));
    mLoadingDialogText =
        LocalizablesFacade.getString(getContext(), OneCMSKeys.INSURANCE_ADD_INSURANCE_MESSAGE)
            .toString();
  }

  @Override public void updateInsurancesList(AvailableProductsResponse availableProductsResponse,
      int totalPassengers) {
    mInsurancesWidgetList = new ArrayList<>();
    for (InsuranceOffer insuranceOffer : availableProductsResponse.getInsuranceOffers()) {
      InsuranceWidget insuranceWidget =
          new InsuranceWidget(getActivity(), mSvInsurances, insuranceOffer, totalPassengers, this);
      mInsurancesWidgetList.add(insuranceWidget);
      mLlContainerInsurances.addView(insuranceWidget);
    }
    mInsuranceDeclineWidget =
        new InsuranceDeclineWidget(getActivity().getApplicationContext(), mSvInsurances, this);
    mLlContainerInsurances.addView(mInsuranceDeclineWidget);
    mSvInsurances.post(new Runnable() {
      @Override public void run() {
        mSvInsurances.smoothScrollTo(0, 0);
      }
    });
  }

  @Override public void addMandatoryWidget(Insurance insurance) {
    mLlContainerInsurances.addView(new InsuranceMandatoryWidget(getActivity(), insurance));
    mHasMandatoryWidget = true;
    mBtnContinue.setEnabled(true);
  }

  @Override public void setInsuranceSelected(String insuranceSelected) {
    for (InsuranceWidget insuranceWidget : mInsurancesWidgetList) {
      if (insuranceWidget.getInsuranceOffer().getId().equals(insuranceSelected)) {
        insuranceWidget.setChecked(true);
        onInsuranceSelected(insuranceWidget);
      }
    }
  }

  @Override public void setRecommendedInsurance(String insuranceRecommended) {
    for (InsuranceWidget insuranceWidget : mInsurancesWidgetList) {
      if (insuranceWidget.getInsuranceOffer().getId().equals(insuranceRecommended)) {
        mRecommendedInsuranceWidget = insuranceWidget;
      }
    }
  }

  @Override public void showRecommendedInsurance() {
    Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
      @Override public void run() {
        if (mRecommendedInsuranceWidget != null) {
          mRecommendedInsuranceWidget.setRecommended();
        }
      }
    }, DELAY_TIME);
  }

  @Override public void onInsuranceSelected(InsuranceWidget selectedInsuranceWidget) {
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_INSURANCE,
        TrackerConstants.ACTION_INSURANCE_INFORMATION, TrackerConstants.LABEL_INSURANCE_SELECT);
    mBtnContinue.setEnabled(true);
    insuranceOffersSelected.clear();
    insuranceOffersSelected.add(selectedInsuranceWidget.getInsuranceOffer());
    if (selectedOption != null) {
      selectedOption.setChecked(false);
    }
    selectedOption = selectedInsuranceWidget.getCheckbox();
    mInsuranceDeclineWidget.collapseRejectContent(selectedInsuranceWidget);
    if (Configuration.getInstance().getCurrentMarket().isInsuranceEuropeanConditions()) {
      mTvConditionsAcceptanceOnContinue.setVisibility(View.VISIBLE);
    }
  }

  @Override public void onInsuranceUnselected(InsuranceWidget unselectedInsuranceWidget) {
    if (!mHasMandatoryWidget) {
      mBtnContinue.setEnabled(false);
    }
    insuranceOffersSelected.remove(unselectedInsuranceWidget.getInsuranceOffer());
    selectedOption = null;
    mTvConditionsAcceptanceOnContinue.setVisibility(View.GONE);
  }

  @Override public void onInsuranceDeclineSelected(InsuranceDeclineWidget insuranceDeclineWidget) {
    mBtnContinue.setEnabled(true);
    insuranceOffersSelected.clear();
    if (selectedOption != null) {
      selectedOption.setChecked(false);
    }
    selectedOption = insuranceDeclineWidget.getCheckbox();
    mTvConditionsAcceptanceOnContinue.setVisibility(View.GONE);
  }

  @Override public void onInsuranceDeclineUnselected() {
    if (!mHasMandatoryWidget) {
      mBtnContinue.setEnabled(false);
    }
    selectedOption = null;
  }

  void onAddInsurancesButtonClick() {
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_INSURANCE,
        TrackerConstants.ACTION_INSURANCE_INFORMATION, TrackerConstants.LABEL_CLICK_CONTINUE);
    mPresenter.processInsurancesSelection(insuranceOffersSelected);
  }

  @Override public void hideLoadingDialog() {
    if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
      mLoadingDialog.dismiss();
    }
  }

  @Override public void showLoadingDialog() {
    mLoadingDialog = new BlackDialog(getActivity(), true);
    mLoadingDialog.show(mLoadingDialogText);
  }

  @Override public void showIncreaseTicketRepricingMessage(double difference) {
    String price = LocaleUtils.getLocalizedCurrencyValue(difference, mLocaleMarket);
    String increaseRepricingMessage =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.INCREASE_REPRICING_TICKET, price)
            .toString();
    showRepricingWidening(increaseRepricingMessage);
  }

  @Override public void showDecreaseTicketRepricingMessage(double difference) {
    String price = LocaleUtils.getLocalizedCurrencyValue(difference, mLocaleMarket);
    String decreaseRepricingMessage =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.DECREASE_REPRICING_TICKET, price)
            .toString();
    showRepricingWidening(decreaseRepricingMessage);
  }

  @Override public void showOutdatedBookingId() {
    BookingOutdatedDialog bookingOutdatedDialog = new BookingOutdatedDialog(getActivity());
    bookingOutdatedDialog.show();
  }

  @Override public void showGeneralError() {
    MslError error = new MslError();
    error.setCode(Code.GENERAL_ERROR.getErrorCode());
    error.setCodeString(Code.GENERAL_ERROR);
    MSLErrorUtilsDialogFragment fragmentErrorDialog =
        MSLErrorUtilsDialogFragment.newInstance(null, error);
    if (fragmentErrorDialog != null) {
      fragmentErrorDialog.setParentScreen(Step.INSURANCE.toString());
      fragmentErrorDialog.setCancelable(false);
      fragmentErrorDialog.show(getChildFragmentManager(), "");
    }
  }

  private void showRepricingWidening(String message) {
    mTvWidening.setText(message);
    mLlWidening.setVisibility(View.VISIBLE);
    mHandlerRepricing.postDelayed(mRunnableRepricing, REPRICING_TIME);
  }

  @Override public void startTimer(int time) {
    mTimeoutCounterWidget.startTimer(time);
  }

  @Override public void showTimeoutWidget(int seconds, int minutes) {
    mTimeoutCounterWidget.setVisibility(View.VISIBLE);
    mTimeoutCounterWidget.setMinutes(seconds, minutes);
    mTimeoutCounterWidget.bringToFront();
  }

  @Override public void setScrollParameters() {
    mTimeoutCounterWidget.setVisibility(View.VISIBLE);
  }

  @Override
  public void trackShoppingCartResponseException(CreateShoppingCartResponse shoppingCartResponse) {
    ((CrashlyticsUtil) getActivity().getApplication()).trackNonFatal(
        ShoppingCartResponseException.newInstance(shoppingCartResponse));
  }

  @Override public void showTimeOutErrorMessage() {
    if (mHasToShowTimeoutDialog) {
      MslError error = new MslError();
      error.setCode(Code.SESSION_TIMEOUT.getErrorCode());
      error.setCodeString(Code.SESSION_TIMEOUT);
      MSLErrorUtilsDialogFragment fragmentErrorDialog =
          MSLErrorUtilsDialogFragment.newInstance(null, error);
      if (fragmentErrorDialog != null && getActivity() != null) {
        fragmentErrorDialog.setParentScreen(Step.INSURANCE.toString());
        fragmentErrorDialog.setCancelable(false);
        fragmentErrorDialog.show(getChildFragmentManager(), "");
      }
    }
  }
}