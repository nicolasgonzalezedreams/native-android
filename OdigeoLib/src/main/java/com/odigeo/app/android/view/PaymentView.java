package com.odigeo.app.android.view;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.globant.roboneck.common.Utils;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoNoConnectionActivity;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.fragments.MSLErrorUtilsDialogFragment;
import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import com.odigeo.app.android.lib.models.dto.custom.Code;
import com.odigeo.app.android.lib.models.dto.custom.MslError;
import com.odigeo.app.android.lib.ui.widgets.TimeoutCounterWidget;
import com.odigeo.app.android.lib.ui.widgets.TopBriefWidget;
import com.odigeo.app.android.lib.ui.widgets.base.BlackDialog;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.exceptions.BookingResponseException;
import com.odigeo.app.android.model.BookingInfoViewModel;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.dialogs.BookingOutdatedDialog;
import com.odigeo.app.android.view.helpers.BookingFlowTimeoutHelper;
import com.odigeo.app.android.view.helpers.CallTelephoneHelper;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.entity.booking.Traveller;
import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.data.entity.shoppingCart.BookingStatus;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.ResumeDataRequest;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.helper.CrashlyticsUtil;
import com.odigeo.presenter.PaymentPresenter;
import com.odigeo.presenter.contracts.navigators.PaymentNavigatorInterface;
import com.odigeo.presenter.contracts.views.PaymentViewInterface;
import com.odigeo.presenter.listeners.PaymentWidgetFormListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.view.View.GONE;
import static com.odigeo.data.entity.TravelType.MULTIDESTINATION;
import static com.odigeo.data.entity.TravelType.ROUND;
import static com.odigeo.data.entity.TravelType.SIMPLE;
import static com.odigeo.data.entity.booking.Booking.TRIP_TYPE_MULTI_SEGMENT;
import static com.odigeo.data.entity.booking.Booking.TRIP_TYPE_ONE_WAY;
import static com.odigeo.data.entity.booking.Booking.TRIP_TYPE_ROUND_TRIP;

public class PaymentView extends BaseView<PaymentPresenter>
    implements PaymentViewInterface, BookingFlowTimeoutHelper.TimeoutListener, View.OnClickListener,
    PaymentWidgetFormListener {

  private static final long REPRICING_TIME = 8000;
  protected AlertDialog alertDialog;
  private View mView;
  private LinearLayout widening;
  private TextView wideningInfo;
  private ScrollView scrollView;
  private TextView paymentInfoHeader, paymentSecureInfo, paymentSecuredBy;
  private LinearLayout paymentContainer;
  private LinearLayout mockPaymentButtons;
  private Button mockConfirmedButton, mockPendingButton, mockRejectedButton;
  private TopBriefWidget topBriefWidget;
  private TimeoutCounterWidget timeoutCounterWidget;
  private BookingFlowTimeoutHelper timerHelper;
  private PaymentFormWidgetView paymentFormWidget;
  private PromoCodeWidgetView promoCodeWidget;
  private PriceBreakdownWidgetView priceBreakdownWidget;
  private PaymentPurchaseWidget paymentPurchaseWidget;
  private Handler handlerRepricing = new Handler();
  private Runnable runnableRepricing;
  private BlackDialog loadingDialog;
  private boolean hasToShowTimeoutDialog;

  private CreateShoppingCartResponse createShoppingCartResponse;
  private boolean isFullTransparency;
  private double lastTicketsPrice;
  private double lastInsurancePrice;
  private SearchOptions searchOptions;
  private CollectionMethodWithPrice collectionMethodWithPrice;

  private Map<TravelType, String> tripTypes = new HashMap<>();
  private BookingInfoViewModel bookingInfo;

  public static PaymentView newInstance(CreateShoppingCartResponse createShoppingCartResponse,
      boolean isFullTransparency, double lastTicketsPrice, double lastInsurancePrice,
      SearchOptions searchOptions, CollectionMethodWithPrice collectionMethodWithPrice,
      BookingInfoViewModel bookingInfo) {
    PaymentView paymentView = new PaymentView();
    Bundle bundle = new Bundle();
    bundle.putSerializable(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE,
        createShoppingCartResponse);
    bundle.putBoolean(Constants.EXTRA_FULL_TRANSPARENCY, isFullTransparency);
    bundle.putDouble(Constants.EXTRA_REPRICING_TICKETS_PRICES, lastTicketsPrice);
    bundle.putDouble(Constants.EXTRA_REPRICING_INSURANCES, lastInsurancePrice);
    bundle.putSerializable(Constants.EXTRA_SEARCH_OPTIONS, searchOptions);
    bundle.putSerializable(Constants.COLLECTION_METHOD_WITH_PRICE, collectionMethodWithPrice);
    bundle.putSerializable(Constants.EXTRA_BOOKING_INFO, bookingInfo);
    paymentView.setArguments(bundle);
    return paymentView;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    createShoppingCartResponse = (CreateShoppingCartResponse) getArguments().getSerializable(
        Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE);
    isFullTransparency = getArguments().getBoolean(Constants.EXTRA_FULL_TRANSPARENCY);
    lastTicketsPrice = getArguments().getDouble(Constants.EXTRA_REPRICING_TICKETS_PRICES);
    lastInsurancePrice = getArguments().getDouble(Constants.EXTRA_REPRICING_INSURANCES);
    searchOptions = (SearchOptions) getArguments().getSerializable(Constants.EXTRA_SEARCH_OPTIONS);
    collectionMethodWithPrice = (CollectionMethodWithPrice) getArguments().getSerializable(
        Constants.COLLECTION_METHOD_WITH_PRICE);
    bookingInfo = (BookingInfoViewModel) getArguments().getSerializable(Constants
        .EXTRA_BOOKING_INFO);
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    initPresenter();
    initTopBrief();
  }

  @Override public void onResume() {
    super.onResume();
    hasToShowTimeoutDialog = true;
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_PAYMENT);
    mPresenter.trackLoggedUserHasPaymentMethods();
  }

  @Override public void onStop() {
    super.onStop();
    hasToShowTimeoutDialog = false;
  }

  @Override protected PaymentPresenter getPresenter() {
    return dependencyInjector.providePaymentPresenter(this,
        (PaymentNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.payment_view;
  }

  @Override protected void initComponent(View view) {
    mView = view;
    timeoutCounterWidget = (TimeoutCounterWidget) view.findViewById(R.id.timeoutCounterWidget);
    widening = (LinearLayout) view.findViewById(R.id.widening);
    wideningInfo = (TextView) widening.findViewById(R.id.tvWideningLeyend);
    scrollView = (ScrollView) view.findViewById(R.id.svPayment);
    paymentInfoHeader = (TextView) view.findViewById(R.id.tvPaymentInfoHeader);
    paymentContainer = (LinearLayout) view.findViewById(R.id.llContainerPayment);
    paymentSecureInfo = (TextView) view.findViewById(R.id.tvPaymentSecureInfo);
    paymentSecuredBy = (TextView) view.findViewById(R.id.tvPaymentSecuredBy);
    mockPaymentButtons = (LinearLayout) view.findViewById(R.id.layout_payment_mockup_buttons);
    mockConfirmedButton = (Button) view.findViewById(R.id.button_confirmed_mockup);
    mockPendingButton = (Button) view.findViewById(R.id.button_pending_mockup);
    mockRejectedButton = (Button) view.findViewById(R.id.button_error_mockup);

    initTimeoutBookingFlow();
    initPriceBreakdownWidget();
    initPaymentCardDetailsWidget();
    initPromoCodeWidget();
    initPurchaseWidget();
    initMockButtons();
    initRunnableRepricing();
    mPresenter.setPresenterWidgets(paymentFormWidget.getPresenter(),
        priceBreakdownWidget.getPresenter(), promoCodeWidget.getPresenter(),
        paymentPurchaseWidget.getPresenter());
    addContent();
  }

  private void initTimeoutBookingFlow() {
    timerHelper = new BookingFlowTimeoutHelper(this);
    timerHelper.checkTimeoutConditions(getArguments());
  }

  private void initPriceBreakdownWidget() {
    priceBreakdownWidget = new PriceBreakdownWidgetView(getContext());
    priceBreakdownWidget.addData(scrollView, searchOptions,
        createShoppingCartResponse.getPricingBreakdown(), Step.PAYMENT, collectionMethodWithPrice,
        createShoppingCartResponse.getShoppingCart());
    priceBreakdownWidget.showPricesBreakdown();
    priceBreakdownWidget.checkTaxRefundableInfo();
  }

  private void initPaymentCardDetailsWidget() {
    paymentFormWidget = new PaymentFormWidgetView(this, getContext(),
        createShoppingCartResponse.getShoppingCart().getCollectionOptions(), scrollView);
  }

  @Override public void updatePaymentCardDetailsWidget() {
    for (int i = 0; i < paymentContainer.getChildCount(); i++) {
      View view = paymentContainer.getChildAt(i);
      if (view == paymentFormWidget) {
        paymentContainer.removeView(view);

        paymentFormWidget = new PaymentFormWidgetView(this, getContext(),
            createShoppingCartResponse.getShoppingCart().getCollectionOptions(), scrollView);

        paymentContainer.addView(paymentFormWidget, i);
      }
    }
  }

  private void initPromoCodeWidget() {
    promoCodeWidget = new PromoCodeWidgetView(getContext(), createShoppingCartResponse);
  }

  private void initPurchaseWidget() {
    paymentPurchaseWidget = new PaymentPurchaseWidget(getContext(),
        Configuration.getInstance().getCurrentMarket().getNeedsExplicitAcceptance());
  }

  private void initMockButtons() {
    if (!Constants.isTestEnvironment) {
      mockPaymentButtons.setVisibility(GONE);
    }
  }

  private void initRunnableRepricing() {
    runnableRepricing = new Runnable() {
      @Override public void run() {
        widening.setVisibility(GONE);
      }
    };
  }

  private void addContent() {
    paymentContainer.addView(paymentFormWidget);
    paymentContainer.addView(promoCodeWidget);
    paymentContainer.addView(priceBreakdownWidget);
    paymentContainer.addView(paymentPurchaseWidget);
  }

  @Override protected void setListeners() {
    timeoutCounterWidget.setListener(timerHelper.getTimeoutWidgetListener());
    mockConfirmedButton.setOnClickListener(this);
    mockPendingButton.setOnClickListener(this);
    mockRejectedButton.setOnClickListener(this);
  }

  @Override protected void initOneCMSText(View view) {
    paymentInfoHeader.setText(
        localizables.getString(OneCMSKeys.PAYMENTVIEWCONTROLLER_INFORMATIVEMODULE_TEXT));
    paymentSecureInfo.setText(localizables.getString(OneCMSKeys.SECURE_SHOPPING_FOOTER_TITLE));
    paymentSecuredBy.setText(localizables.getString(OneCMSKeys.SECURE_SHOPPING_FOOTER_SECURED_BY));
  }

  @Override public void initTopBrief() {
    topBriefWidget = ((TopBriefWidget) mView.findViewById(R.id.tbPayment));
    topBriefWidget.addData(searchOptions);
    if (isFullTransparency || mPresenter.isMembershipForCurrentMarket()) {
      updateTopBrief();
    }
  }

  @Override public void updateTopBrief() {
    topBriefWidget.setMembershipApplied(mPresenter.shouldApplyMembershipPerks());
    topBriefWidget.updatePrice(mPresenter.getTotalPrice());
  }

  private void initPresenter() {
    BigDecimal collectionMethodWithPriceValue = BigDecimal.ZERO;
    if(bookingInfo.getCollectionMethodWithPrice() != null) {
      collectionMethodWithPriceValue = bookingInfo.getCollectionMethodWithPrice().getPrice();
    }
    mPresenter.initializePresenter(createShoppingCartResponse, isFullTransparency, lastTicketsPrice,
        lastInsurancePrice, collectionMethodWithPriceValue);
  }

  @Override public void showLoadingDialog() {
    loadingDialog = new BlackDialog(getActivity(), true);
    loadingDialog.show(localizables.getString(OneCMSKeys.LOADING_MESSAGE_PAYING));
  }

  @Override public void showLoadingResumeBooking() {
    loadingDialog = new BlackDialog(getActivity(), true);
    loadingDialog.show(localizables.getString(OneCMSKeys.LOADINGVIEWCONTROLLER_MESSAGE_LOADING));
  }

  @Override public void hideLoadingDialog() {
    if (loadingDialog != null && loadingDialog.isShowing()) {
      loadingDialog.dismiss();
    }
  }

  @Override public void startTimer(int time) {
    timeoutCounterWidget.startTimer(time);
  }

  @Override public void showTimeoutWidget(int seconds, int minutes) {
    timeoutCounterWidget.setVisibility(View.VISIBLE);
    timeoutCounterWidget.setMinutes(seconds, minutes);
    timeoutCounterWidget.bringToFront();
  }

  @Override public void setScrollParameters() {
    timeoutCounterWidget.setVisibility(View.VISIBLE);
  }

  @Override public void showTimeOutErrorMessage() {
    if (hasToShowTimeoutDialog) {
      MslError error = new MslError();
      error.setCode(Code.SESSION_TIMEOUT.getErrorCode());
      error.setCodeString(Code.SESSION_TIMEOUT);
      MSLErrorUtilsDialogFragment fragmentErrorDialog =
          MSLErrorUtilsDialogFragment.newInstance(null, error);
      if (fragmentErrorDialog != null && getActivity() != null) {
        fragmentErrorDialog.setParentScreen(Step.PAYMENT.toString());
        fragmentErrorDialog.setCancelable(false);
        fragmentErrorDialog.show(getChildFragmentManager(), "");
      }
    }
  }

  @Override public void onClick(View v) {
    int vId = v.getId();
    if (vId == R.id.button_confirmed_mockup) {
      mPresenter.processMockedResponse(BookingStatus.CONTRACT);
    } else if (vId == R.id.button_pending_mockup) {
      mPresenter.processMockedResponse(BookingStatus.PENDING);
    } else if (vId == R.id.button_error_mockup) {
      mPresenter.processMockedResponse(BookingStatus.REJECTED);
    }
  }

  @Override public void showRepricingTicketsMessage(double repricingAmount) {
    CharSequence stringFormatted = "";
    String message;
    if (repricingAmount < 0) {
      message = LocaleUtils.getLocalizedCurrencyValue(-repricingAmount,
          LocaleUtils.localeToString(Configuration.getCurrentLocale()));
      stringFormatted =
          localizables.getString(OneCMSKeys.REPRICINGWIDENING_FLIGHTREPRICING_MESSAGELOWER,
              message);
    } else if (repricingAmount > 0) {
      message = LocaleUtils.getLocalizedCurrencyValue(repricingAmount,
          LocaleUtils.localeToString(Configuration.getCurrentLocale()));
      stringFormatted =
          localizables.getString(OneCMSKeys.REPRICINGWIDENING_FLIGHTREPRICING_MESSAGEHIGHER,
              message);
    }
    wideningInfo.setText(stringFormatted);
    widening.setVisibility(View.VISIBLE);
    handlerRepricing.postDelayed(runnableRepricing, REPRICING_TIME);
  }

  @Override public void showRepricingDialog(double repricingAmount) {
    String message = localizables.getString(OneCMSKeys.REPRICING_ALERT_MESSAGE,
        LocaleUtils.getLocalizedCurrencyValue(repricingAmount));

    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_ERROR_REPRICING);

    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
    builder.setCancelable(true);
    builder.setTitle(localizables.getString(OneCMSKeys.REPRICING_ALERT_TITLE));

    DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        showLoadingDialog();
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
            TrackerConstants.ACTION_REPRICING, TrackerConstants.LABEL_REPRICING_CONTINUE);
        mPresenter.onContinueButtonClick();
      }
    };
    DialogInterface.OnClickListener negativeClickListener = new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        OdigeoApp odigeoApp = (OdigeoApp) getActivity().getApplication();
        Utils.sendToHome(getActivity(), odigeoApp);
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
            TrackerConstants.ACTION_REPRICING, TrackerConstants.LABEL_REPRICING_GO_BACK);
      }
    };
    builder.setMessage(message)
        .setPositiveButton(localizables.getString(OneCMSKeys.ALERT_BUY_BUTTON_TITLE),
            onClickListener)
        .setNegativeButton(localizables.getString(OneCMSKeys.COMMON_CANCEL), negativeClickListener);

    builder.create();
    builder.show();
  }

  @Override public void showRepricingInsuranceMessage(double repricingAmount) {
    String message = LocaleUtils.getLocalizedCurrencyValue(repricingAmount,
        LocaleUtils.localeToString(Configuration.getCurrentLocale()));
    wideningInfo.setText(
        localizables.getString(OneCMSKeys.REPRICINGWIDENING_BAGGAGEREPRICING_MESSAGE, message));
    widening.setVisibility(View.VISIBLE);
    handlerRepricing.postDelayed(runnableRepricing, REPRICING_TIME);
  }

  @Override public void showTimeoutException() {
    Util.showTimeoutException(getActivity().getSupportFragmentManager(), Step.PAYMENT);
  }

  @Override public void showNoConnectionActivity() {
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_ERROR_NO_CONNECTION);
    OdigeoNoConnectionActivity.launch(getActivity(),
        Constants.REQUEST_CODE_BOOKINGCONFIRMATIONLISTENER);
  }

  @Override public void showOutdatedBookingId() {
    BookingOutdatedDialog bookingOutdatedDialog = new BookingOutdatedDialog(getActivity());
    bookingOutdatedDialog.show();
  }

  @Override public void showGeneralMslError() {
    MslError error = new MslError();
    error.setCode(Code.GENERAL_ERROR.getErrorCode());
    error.setCodeString(Code.GENERAL_ERROR);
    MSLErrorUtilsDialogFragment fragmentErrorDialog =
        MSLErrorUtilsDialogFragment.newInstance(null, error);
    if (fragmentErrorDialog != null) {
      fragmentErrorDialog.setParentScreen(Step.PAYMENT.toString());
      fragmentErrorDialog.setCancelable(false);
      fragmentErrorDialog.show(getChildFragmentManager(), "");
    }
  }

  @Override public void showVelocityError(String description) {
    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
    DialogInterface.OnClickListener positiveClickListener = new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.cancel();
      }
    };
    alertDialogBuilder.setMessage(description)
        .setCancelable(true)
        .setPositiveButton(localizables.getString(OneCMSKeys.COMMON_OK), positiveClickListener);

    AlertDialog alertDialog = alertDialogBuilder.create();
    alertDialog.show();
  }

  @Override public void showPaymentRetry() {
    alertDialog = new AlertDialog.Builder(getContext()).create();
    alertDialog.setCancelable(true);
    LayoutInflater inflater =
        (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View layout = inflater.inflate(R.layout.dialog_session_expired_payment,
        (ViewGroup) getActivity().findViewById(R.id.dialog_expired_linear_layout));
    alertDialog.setView(layout);

    fillDialogInformation(alertDialog, layout);

    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
        TrackerConstants.ACTION_PAYMENT_RETRY, TrackerConstants.LABEL_PAYMENT_RETRY_APPEARANCES);
    alertDialog.show();
  }

  private void fillDialogInformation(final AlertDialog alertDialog, View layout) {
    TextView dialogTitle = (TextView) layout.findViewById(R.id.dialog_expired_title);
    TextView dialogSubtitle = (TextView) layout.findViewById(R.id.dialog_expired_subtitle);
    Button callButton = (Button) layout.findViewById(R.id.btn_call);
    Button checkPaymentButton = (Button) layout.findViewById(R.id.btn_second_dialog_expired);

    final CharSequence phoneNumber =
        localizables.getString(OneCMSKeys.PAYMENTVIEWCONTROLLER_PAYMENTRETRY_PHONE_NUMBER);
    if (!phoneNumber.toString().isEmpty()) {
      callButton.setVisibility(View.VISIBLE);
      callButton.setText(localizables.getString(OneCMSKeys.ERROR_SESSION_EXPIRED_PHONE_BUTTON,
          phoneNumber.toString()));
      callButton.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {

          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
              TrackerConstants.ACTION_PAYMENT_RETRY, TrackerConstants.LABEL_CALL_NOW_CLICKS);
          CallTelephoneHelper.callPhoneNumberInActivity(getActivity(), phoneNumber.toString());
        }
      });
    }

    dialogTitle.setText(localizables.getString(OneCMSKeys.ERROR_PAYMENT_TITLE));

    final CharSequence subtitleText =
        localizables.getString(OneCMSKeys.PAYMENT_METHOD_PHONE_PRICING_INFO);
    dialogSubtitle.setText(
        localizables.getString(OneCMSKeys.ERROR_PAYMENT_SUBTITLE, subtitleText.toString()));

    checkPaymentButton.setText(localizables.getString(OneCMSKeys.COMMON_CHECK));
    checkPaymentButton.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
            TrackerConstants.ACTION_PAYMENT_RETRY, TrackerConstants.LABEL_CHECK_PAYMENT_DETAILS);
        alertDialog.dismiss();
      }
    });
  }

  @Override public void trackSuccessfulPurchase() {
    mTracker.trackLocalyticsEvent(TrackerConstants.PURCHASE_SUCCESSFULL);
  }

  @Override public void trackPaymentMethodSelected(String collectionOptionType) {
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
        TrackerConstants.ACTION_PAYMENT_DETAILS,
        TrackerConstants.LABEL_PAYMENT_METHOD_SELECTED + collectionOptionType);
  }

  @Override public void trackBookingResponseException(BookingResponse bookingResponse) {
    ((CrashlyticsUtil) getActivity().getApplication()).trackNonFatal(
        BookingResponseException.newInstance(bookingResponse));
  }

  public void onExternalPaymentFinished(ResumeDataRequest resumeDataRequest) {
    mPresenter.onExternalPaymentFinished(resumeDataRequest, false);
  }

  @Override public void onCollectionOptionSelected(CollectionMethodType collectionMethodType) {
    mPresenter.onCollectionOptionSelected(collectionMethodType);
  }

  @Override public void onPaymentMethodSelected(String paymentMethod) {
    mPresenter.onPaymentMethodSelected(paymentMethod, false);
  }

  @Override public void onPaymentWidgetValidation(boolean areFieldsCorrect) {
    mPresenter.onPaymentWidgetValidation(areFieldsCorrect);
  }

  @Override public void onSavedPaymentMethodSelected(String paymentMethod) {
    mPresenter.onPaymentMethodSelected(paymentMethod, true);
  }

  @Override public Booking getMockedBooking() {
    initialiseTripTypes();

    Booking booking = new Booking();
    List<Segment> segments = new ArrayList<>();

    Segment firstSegment = getFirstSegmentFromSearchOptions();
    segments.add(firstSegment);

    Segment lastSegment = getLastSegmentFromSearchOptions();
    if (lastSegment != null) {
      segments.add(lastSegment);
    }
    booking.setSegments(segments);

    String bookingTripType = getTripTypeFromSearchOptions();
    booking.setTripType(bookingTripType);

    List<Traveller> travellers = getTravellersFromSearchOptions();
    booking.setTravellers(travellers);

    String arrivalAirportCode = searchOptions.getFirstSegment().getArrivalCity().getIataCode();
    booking.setArrivalAirportCode(arrivalAirportCode);

    return booking;
  }

  private void initialiseTripTypes() {
    tripTypes.put(SIMPLE, TRIP_TYPE_ONE_WAY);
    tripTypes.put(ROUND, TRIP_TYPE_ROUND_TRIP);
    tripTypes.put(MULTIDESTINATION, TRIP_TYPE_MULTI_SEGMENT);
  }

  private Segment getFirstSegmentFromSearchOptions() {
    Segment firstSegment = new Segment();

    LocationBooking arrivalLocation = new LocationBooking();
    String arrivalCityName = searchOptions.getFirstSegment().getArrivalCity().getCityName();
    arrivalLocation.setCityName(arrivalCityName);

    List<Section> sectionList = new ArrayList<>();
    Section firstSection = new Section();
    firstSection.setTo(arrivalLocation);
    firstSection.setArrivalDate(searchOptions.getFirstSegment().getLastSection().getArrivalDate());

    sectionList.add(firstSection);
    firstSegment.setSectionsList(sectionList);

    return firstSegment;
  }

  private Segment getLastSegmentFromSearchOptions() {
    TravelType travelType = searchOptions.getTravelType();
    String tripType = tripTypes.get(travelType);

    if (!tripType.equals(TRIP_TYPE_ROUND_TRIP)) return null;

    Segment lastSegment = new Segment();
    List<Section> sectionListLastSegment = new ArrayList<>();
    Section section = new Section();
    section.setDepartureDate(searchOptions.getLastSegment().getFirstSection().getDepartureDate());

    sectionListLastSegment.add(section);
    lastSegment.setSectionsList(sectionListLastSegment);

    return lastSegment;
  }

  private String getTripTypeFromSearchOptions() {
    TravelType travelType = searchOptions.getTravelType();
    return tripTypes.get(travelType);
  }

  private List<Traveller> getTravellersFromSearchOptions() {
    List<Traveller> travellers = new ArrayList<>();
    for (int i = 0; i < searchOptions.getTotalPassengers(); i++) {
      travellers.add(new Traveller());
    }
    return travellers;
  }
}