package com.odigeo.app.android.lib.models.dto;

public class InitSessionDTO {

  protected TestTokenSetRequestDTO testTokenSetRequest;

  /**
   * Gets the value of the testTokenSetRequest property.
   *
   * @return possible object is
   * {@link TestTokenSetRequest }
   */
  public TestTokenSetRequestDTO getTestTokenSetRequest() {
    return testTokenSetRequest;
  }

  /**
   * Sets the value of the testTokenSetRequest property.
   *
   * @param value allowed object is
   * {@link TestTokenSetRequest }
   */
  public void setTestTokenSetRequest(TestTokenSetRequestDTO value) {
    this.testTokenSetRequest = value;
  }
}
