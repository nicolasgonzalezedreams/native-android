package com.odigeo.app.android.view.adapters;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.ViewGroup;
import com.odigeo.app.android.view.adapters.diffutil.MyTripsDiffCallback;
import com.odigeo.app.android.view.adapters.holders.AppRateCardHolder;
import com.odigeo.app.android.view.adapters.holders.BaseRecyclerViewHolder;
import com.odigeo.app.android.view.adapters.holders.BookingCardHolder;
import com.odigeo.app.android.view.adapters.holders.FooterHolder;
import com.odigeo.app.android.view.adapters.holders.HeaderHolder;
import com.odigeo.app.android.view.adapters.holders.PastBookingCardHolder;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.interactors.provider.MyTripsProvider;
import com.odigeo.presenter.MyTripsPresenter;
import com.odigeo.tools.DateHelperInterface;
import java.util.ArrayList;
import java.util.List;

public class BookingAdapter extends Adapter<BaseRecyclerViewHolder> {

  public static final int TYPE_UPCOMING_TRIP = 1;
  public static final int TYPE_RATE_APP = 2;
  static final int TYPE_PAST_TRIP = 3;
  static final int TYPE_HEADER = 0;
  static final int TYPE_FOOTER = 4;

  private static final String RATE_APP_TAG = "RATE_APP";

  private MyTripsProvider mDataSet;
  private final DateHelperInterface mDateHelper;
  private final MyTripsPresenter myTripsPresenter;
  private OnBookingClickListener mBookingClickListener;

  public interface OnBookingClickListener {

    void onClick(int position, Booking booking);
  }

  public BookingAdapter(MyTripsProvider dataSet, DateHelperInterface dateHelper,
      MyTripsPresenter myTripsPresenter) {
    this.mDataSet = dataSet;
    this.mDateHelper = dateHelper;
    this.myTripsPresenter = myTripsPresenter;
  }

  public void setBookingClickListener(OnBookingClickListener bookingClickListener) {
    this.mBookingClickListener = bookingClickListener;
  }

  @Override public int getItemViewType(int position) {
    if (isPositionBooking(position)) {
      if (getItem(position).isPastBooking()) {
        return TYPE_PAST_TRIP;
      } else {
        return TYPE_UPCOMING_TRIP;
      }
    } else {
      if (position == 0 && myTripsPresenter.checkShouldShowAppRate()) {
        return TYPE_RATE_APP;
      } else if (isLastPosition(position)) {
        return TYPE_FOOTER;
      } else {
        return TYPE_HEADER;
      }
    }
  }

  @Override public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
    switch (viewType) {
      case TYPE_UPCOMING_TRIP:
        return BookingCardHolder.newInstance(viewGroup, mDateHelper, mDataSet.getCurrentSize(),
            mBookingClickListener);
      case TYPE_PAST_TRIP:
        return PastBookingCardHolder.newInstance(viewGroup, mDateHelper, mBookingClickListener);
      case TYPE_RATE_APP:
        return AppRateCardHolder.newInstance(viewGroup);
      case TYPE_FOOTER:
        return FooterHolder.newInstance(viewGroup);
      case TYPE_HEADER:
      default:
        return HeaderHolder.newInstance(viewGroup);
    }
  }

  @Override public void onBindViewHolder(BaseRecyclerViewHolder viewHolder, int position) {
    int itemViewType = getItemViewType(position);
    Object argument;
    switch (itemViewType) {
      case TYPE_UPCOMING_TRIP:
      case TYPE_PAST_TRIP:
        argument = getItem(position);
        break;
      case TYPE_RATE_APP:
        argument = RATE_APP_TAG;
        break;
      case TYPE_HEADER:
        // 26/07/17 This is provisional, only waiting for having a common type of card, sorry
        argument = true;
        break;
      case TYPE_FOOTER:
      default:
        argument = getItem(position);
    }
    viewHolder.bind(position, argument);
  }

  @Override public int getItemCount() {
    return mDataSet.size();
  }

  private boolean isPositionBooking(int position) {
    return getItem(position) != null;
  }

  @VisibleForTesting public Booking getItem(int position) {
    return mDataSet.get(position);
  }

  public void setBookings(@NonNull final MyTripsProvider bookings, boolean shouldShowRateApp) {
    MyTripsProvider oldDataset = new MyTripsProvider();
    List<Booking> mDataSetBookings = new ArrayList<>();
    mDataSetBookings.addAll(mDataSet.getBookingList());
    mDataSetBookings.addAll(mDataSet.getOldBookingList());

    oldDataset.addAllBookings(mDataSetBookings, shouldShowRateApp);
    final DiffUtil.DiffResult diffResult =
        DiffUtil.calculateDiff(new MyTripsDiffCallback(oldDataset, bookings));

    Handler handler = new Handler(Looper.getMainLooper());
    handler.post(new Runnable() {
      @Override public void run() {
        mDataSet = bookings;
        diffResult.dispatchUpdatesTo(BookingAdapter.this);
      }
    });
  }

  private boolean isLastPosition(int position) {
    return position == mDataSet.size() - 1;
  }
}
