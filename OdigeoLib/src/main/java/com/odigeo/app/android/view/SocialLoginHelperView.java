package com.odigeo.app.android.view;

import android.content.Intent;
import android.os.Bundle;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.services.SyncService;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DialogHelper;
import com.odigeo.data.net.error.MslError;
import com.odigeo.dataodigeo.net.controllers.FacebookController;
import com.odigeo.dataodigeo.net.controllers.GooglePlusController;
import com.odigeo.presenter.SocialLoginPresenter;
import com.odigeo.presenter.contracts.views.LoginSocialInterface;

import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_SIGN_IN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_SSO_LOGIN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SSO_SUCCESSFUL_LOGIN_FACEBOOK;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SSO_SUCCESSFUL_LOGIN_GOOGLE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SSO_SUCCESSFUL_LOGIN_ODIGEO;

public abstract class SocialLoginHelperView extends BaseView<SocialLoginPresenter>
    implements LoginSocialInterface {

  protected DialogHelper dialogHelper;
  private FacebookController facebookController;
  private GooglePlusController googlePlusController;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    facebookController =
        AndroidDependencyInjector.getInstance().provideFacebookHelper(this, getPresenter());
    googlePlusController =
        AndroidDependencyInjector.getInstance().provideGooglePlusHelper(this, getPresenter());
    dialogHelper = new DialogHelper(getActivity());
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    facebookController.onActivityResult(requestCode, resultCode, data);
    googlePlusController.onActivityResult(requestCode, data);
  }

  protected void loginGoogle() {
    googlePlusController.loginGoogle();
  }

  protected void loginFacebook() {
    facebookController.login();
  }

  @Override public void loginSuccess(String source, String email) {
    if (getActivity() != null) {
      dialogHelper.showDoneDialog(getActivity(),
          LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_INITSESSION_WELCOME, email)
              .toString(), R.drawable.ok_process, new DialogHelper.ProcessDoneListener() {
            @Override public void onDismiss() {
              mPresenter.onLoginSuccessDialogClosed();
            }
          });

      Intent intent = new Intent(getActivity(), SyncService.class);
      getActivity().startService(intent);
    }

    mTracker.updateDimensionOnUserLogin(true);

    switch (source) {
      case FACEBOOK_SOURCE:
        mTracker.trackAnalyticsEvent(getSSOTrackingCategory(), ACTION_SIGN_IN,
            LABEL_SSO_SUCCESSFUL_LOGIN_FACEBOOK);
        break;
      case GOOGLE_SOURCE:
        mTracker.trackAnalyticsEvent(getSSOTrackingCategory(), ACTION_SIGN_IN,
            LABEL_SSO_SUCCESSFUL_LOGIN_GOOGLE);
        break;
      case PASSWORD_SOURCE:
        mTracker.trackAnalyticsEvent(getSSOTrackingCategory(), ACTION_SIGN_IN,
            LABEL_SSO_SUCCESSFUL_LOGIN_ODIGEO);
        break;
    }
  }

  @Override public void onStop() {
    super.onStop();
    dialogHelper.hideDialog();
  }

  protected void showProgress() {
    dialogHelper.showDialog(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_INITSESSION_LOGINGIN)
            .toString());
  }

  @Override public void hideProgress() {
    dialogHelper.hideDialog();
  }

  @Override public void showFacebookAuthError() {
    String errorMessage = String.format(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_FB_GOOGLE_ERROR_CREATING_ACCOUNT)
            .toString(), FACEBOOK_SOURCE);
    showErrorDialog(errorMessage);
  }

  @Override public void showFacebookPermissionError() {
    String errorMessage =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_FACEBOOK_ERROR_PERMISSIONS)
            .toString();
    showErrorDialog(errorMessage);
  }

  @Override public void showUnknownLoginError() {
    String errorMessage =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ERROR_WRONG).toString();
    showErrorDialog(errorMessage);
  }

  @Override public void showGoogleAuthError() {
    String errorMessage =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_FB_GOOGLE_ERROR_CREATING_ACCOUNT,
            GOOGLE_SOURCE).toString();
    showErrorDialog(errorMessage);
  }

  @Override public String getSSOTrackingCategory() {
    if (mPresenter != null) {
      return mPresenter.getSSOTrackingCategory();
    } else {
      return CATEGORY_SSO_LOGIN;
    }
  }

  private void showErrorDialog(String errorMessage) {
    dialogHelper.showErrorDialog(getActivity(), errorMessage);
  }

  @Override public void showAuthError() {
    //nothing
  }

  @Override public void onLogoutError(MslError error, String message) {
    //nothing
  }
}
