package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.booking.Carrier;
import com.odigeo.data.entity.booking.FlightStats;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import com.odigeo.tools.DurationFormatter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class SummaryTravelWidgetNew extends LinearLayout {

  private static final int MINUTES_BY_HOUR = 60;
  private static final String SPACE = " ";
  private static final String GMT_TIMEZONE = "GMT";
  private static final String LINE = " - ";
  private static final int MIN_ON_MILLIS = 60000;
  private LinearLayout mListFlights;
  private TextView mTextStops;
  private Fonts mFonts;
  private TravelType mTypeTravel;

  private OdigeoImageLoader imageLoader;
  private DurationFormatter durationFormatter;

  public SummaryTravelWidgetNew(Context context, AttributeSet attrs) {
    super(context, attrs);
    initView();
  }

  public SummaryTravelWidgetNew(Context context, Segment segment, TravelType typeTravel,
      int position, DurationFormatter durationFormatter) {
    super(context);
    this.mTypeTravel = typeTravel;
    this.durationFormatter = durationFormatter;
    initView();
    initHeader(segment, position);
    drawTravels(segment.getSectionsList());
  }

  private void initView() {
    inflate(getContext(), R.layout.layout_summary_travel, this);
    mFonts = Configuration.getInstance().getFonts();
    mListFlights = (LinearLayout) findViewById(R.id.layout_travelList_travel_summary);
    mTextStops = (TextView) findViewById(R.id.tvStopsNumber);
    mTextStops.setTypeface(mFonts.getRegular());

    imageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();
  }

  private void initHeader(Segment segment, int position) {
    TextView textViewTitle = (TextView) findViewById(R.id.text_title_travel_summary);
    textViewTitle.setText(getSegmentTitle(position));
    textViewTitle.setTypeface(mFonts.getBold());

    TextView textFlight = (TextView) findViewById(R.id.text_subtitle_travel_summary);
    textFlight.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.TRIP_DETAILS_DURATION));
    textFlight.setTypeface(mFonts.getBold());

    TextView textTimeFlight = (TextView) findViewById(R.id.text_time_travel_summary);
    textTimeFlight.setText(durationFormatter.format(segment.getDuration()));
    textTimeFlight.setTypeface(mFonts.getRegular());
  }

  private String getSegmentTitle(int position) {
    if (mTypeTravel == TravelType.SIMPLE) {
      return CommonLocalizables.getInstance().getCommonOutbound(getContext()).toString();
    } else if (mTypeTravel == TravelType.ROUND) {
      if (position == 0) {
        return CommonLocalizables.getInstance().getCommonOutbound(getContext()).toString();
      } else {
        return CommonLocalizables.getInstance().getCommonInbound(getContext()).toString();
      }
    } else {
      return CommonLocalizables.getInstance().getCommonLeg(getContext()) + SPACE + (position + 1);
    }
  }

  private void setStops(List<Section> sections) {
    if (sections.size() > 1) {
      int stopsNumber = sections.size() - 1;
      StringBuilder stringStopsInfo =
          new StringBuilder(SPACE + getContext().getString(R.string.point_middle) + SPACE);
      if (stopsNumber == 1) {
        stringStopsInfo.append(LocalizablesFacade.getString(getContext(),
            OneCMSKeys.SCALES_FILTER_VIEW_CONTROLLER_1_SCALES));
      } else {
        stringStopsInfo.append(stopsNumber)
            .append(SPACE)
            .append(LocalizablesFacade.getString(getContext(), OneCMSKeys.COMMON_STOPS));
      }
      mTextStops.setText(stringStopsInfo);
    }
  }

  private void drawTravels(List<Section> sections) {
    if (sections != null) {
      int size = sections.size();
      Section section;
      Section next;
      setStops(sections);
      for (int flight = 0; flight < size; flight++) {
        section = sections.get(flight);
        if (section != null) {
          //Draw the section
          addSection(section, size, flight, getStatus(section));
          //If it's have a next section, then draw the scale information
          if (flight < size - 1) {
            next = sections.get(flight + 1);
            addScale(OdigeoDateUtils.createCalendar(section.getArrivalDate()),
                OdigeoDateUtils.createCalendar(next.getDepartureDate()));
          }
        }
      }
    }
  }

  private String getStatus(Section section) {
    String flightStatus;
    flightStatus =
        (section.getFlightStats() == null) ? null : section.getFlightStats().getFlightStatus();
    if (flightStatus != null) { //If the Stats are available
      return flightStatus;
    } else { //Normal scenario
      return null;
    }
  }

  private void addSection(Section section, int totalSections, int flight, String status) {
    long departureMillis = section.getDepartureDate();
    long arrivalMillis = section.getArrivalDate();
    if (status != null) {
      if (status.equalsIgnoreCase(FlightStats.STATUS_DELAYED)) {
        int minutes;
        if (section.getFlightStats().getDepartureTime() > 0) {
          departureMillis = section.getFlightStats().getDepartureTime();
        } else {
          String departureTimeDelay = section.getFlightStats().getDepartureTimeDelay();
          try {
            minutes = Integer.parseInt(departureTimeDelay);
          } catch (NumberFormatException e) {
            minutes =
                Integer.parseInt(departureTimeDelay.substring(0, departureTimeDelay.length() - 1));
          }
          departureMillis += (minutes * MIN_ON_MILLIS);
        }
        if (section.getFlightStats().getArrivalTime() > 0) {
          arrivalMillis = section.getFlightStats().getArrivalTime();
        } else {
          String arrivalTimeDelay = section.getFlightStats().getArrivalTimeDelay();
          try {
            minutes = Integer.parseInt(arrivalTimeDelay);
          } catch (NumberFormatException e) {
            minutes =
                Integer.parseInt(arrivalTimeDelay.substring(0, arrivalTimeDelay.length() - 1));
          }
          arrivalMillis += (minutes * MIN_ON_MILLIS);
        }
      }
      addStatus(status, flight == 0, section.getFlightStats().getDepartureTimeDelay());
    }
    addFlight(OdigeoDateUtils.createCalendar(departureMillis), section.getFrom(),
        section.getDepartureTerminal(), R.layout.layout_summary_travel_item_flight_departure, false,
        status, false);
    addInformation(section, status);
    boolean isLast = (flight == totalSections - 1);
    addFlight(OdigeoDateUtils.createCalendar(arrivalMillis), section.getTo(),
        section.getArrivalTerminal(), R.layout.layout_summary_travel_item_flight_arrival, isLast,
        status, true);
  }

  private void addFlight(Calendar dateFlight, LocationBooking location, String terminal,
      int idLayout, boolean isLast, String status, boolean isArrival) {
    if (location != null) {
      View itemFlight = LayoutInflater.from(getContext()).inflate(idLayout, mListFlights, false);

      View line = itemFlight.findViewById(R.id.line_flight);
      line.setLayerType(LAYER_TYPE_SOFTWARE, null);
      ImageView circle = ((ImageView) itemFlight.findViewById(R.id.circle_flight));

      TextView time = (TextView) itemFlight.findViewById(R.id.text_time_departure_summaryWidget);
      TextView date = (TextView) itemFlight.findViewById(R.id.text_date_departure_summaryWidget);
      TextView airport = (TextView) itemFlight.findViewById(R.id.text_namePlace_summaryWidget);
      TextView terminalTV = (TextView) itemFlight.findViewById(R.id.text_terminal_summarywidget);
      TextView city = (TextView) itemFlight.findViewById(R.id.text_cityName_summarywidget);

      time.setTypeface(mFonts.getBold());
      date.setTypeface(mFonts.getRegular());
      airport.setTypeface(mFonts.getBold());
      terminalTV.setTypeface(mFonts.getBold());
      city.setTypeface(mFonts.getRegular());

      if (status != null) {
        if (status.equalsIgnoreCase(FlightStats.STATUS_DELAYED)) {
          if (!isArrival) {
            ViewUtils.tintBackground(line, R.color.semantic_negative_non_blocker);
          }
          time.setTextColor(
              ContextCompat.getColor(getContext(), R.color.semantic_negative_non_blocker));
          ViewUtils.tintImageViewSrc(circle, R.color.semantic_negative_non_blocker);
        } else if (status.equalsIgnoreCase(FlightStats.STATUS_CANCELLED)) {
          if (!isArrival) {
            ViewUtils.tintBackground(line, R.color.semantic_negative_blocker);
          }
          ViewUtils.tintImageViewSrc(circle, R.color.semantic_negative_blocker);
        }
      }

      SimpleDateFormat format = OdigeoDateUtils.getGmtDateFormat(
          Configuration.getInstance().getCurrentMarket().getTimeFormat());
      time.setText(
          String.format(format.format(dateFlight.getTime()), LocaleUtils.getCurrentLocale()));

      format = OdigeoDateUtils.getGmtDateFormat(getContext().getString(R.string.templates__time4));
      date.setText(format.format(dateFlight.getTime()));

      String airportText = location.getLocationName() + " (" + location.getLocationCode() + ")";
      airport.setText(airportText);

      if (terminal == null) {
        terminalTV.setVisibility(View.GONE);
      } else {
        String terminalText =
            LocalizablesFacade.getString(getContext(), OneCMSKeys.CARDS_TERMINAL) + " " + terminal;
        terminalTV.setText(terminalText);
      }

      city.setText(location.getCityName());

      if (isLast) {
        itemFlight.findViewById(R.id.line_flight).setVisibility(GONE);
      }
      mListFlights.addView(itemFlight);
    }
  }

  private void addStatus(String status, boolean isFirst, String delay) {
    View view = LayoutInflater.from(getContext())
        .inflate(R.layout.flight_status_change, mListFlights, false);
    view.findViewById(R.id.line_flight).setLayerType(LAYER_TYPE_SOFTWARE, null);
    TextView title = ((TextView) view.findViewById(R.id.status_title));
    TextView message = ((TextView) view.findViewById(R.id.status_message));
    if (isFirst) {
      view.findViewById(R.id.status_padding).setVisibility(GONE);
    }
    if (status.equalsIgnoreCase(FlightStats.STATUS_DELAYED)) {
      message.setVisibility(GONE);
      title.setTextColor(
          ContextCompat.getColor(getContext(), R.color.semantic_negative_non_blocker));
      title.setText(buildDelayMessage(delay));
    } else if (status.equalsIgnoreCase(FlightStats.STATUS_CANCELLED)) {
      title.setText(LocalizablesFacade.getString(getContext(), OneCMSKeys.CARDS_STATUS_CANCELLED));
      message.setText(
          LocalizablesFacade.getString(getContext(), OneCMSKeys.CARDS_STATUS_CANCELLED_MSG));
    } else if (status.equalsIgnoreCase(FlightStats.STATUS_DIVERTED)) {
      title.setText(
          LocalizablesFacade.getString(getContext(), OneCMSKeys.CARDS_STATUS_DIVERTED_TITLE));
      message.setText(
          LocalizablesFacade.getString(getContext(), OneCMSKeys.CARDS_STATUS_DIVERTED_MSG));
    } else {
      title.setVisibility(GONE);
      message.setVisibility(GONE);
    }
    mListFlights.addView(view);
  }

  private CharSequence buildDelayMessage(String delay) {
    return LocalizablesFacade.getString(getContext(), OneCMSKeys.CARDS_STATUS_DELAYED,
        durationFormatter.format(Integer.parseInt(delay)));
  }

  private void addInformation(Section section, String status) {
    View card = LayoutInflater.from(getContext())
        .inflate(R.layout.layout_summary_travel_item_info, mListFlights, false);

    View line = card.findViewById(R.id.line_flight);
    line.setLayerType(LAYER_TYPE_SOFTWARE, null);
    if (status != null) {
      if (status.equalsIgnoreCase(FlightStats.STATUS_DELAYED)) {
        ViewUtils.tintBackground(line, R.color.semantic_negative_non_blocker);
      } else if (status.equalsIgnoreCase(FlightStats.STATUS_CANCELLED)) {
        ViewUtils.tintBackground(line, R.color.semantic_negative_blocker);
      }
    }

    ImageView airlineIcon = (ImageView) card.findViewById(R.id.image_airline_icon);
    TextView airlineName = (TextView) card.findViewById(R.id.text_airline_info);
    TextView classFlight = (TextView) card.findViewById(R.id.text_type_class);
    TextView timeFlight = (TextView) card.findViewById(R.id.text_time_flight_summaryWidget);
    TextView operated = (TextView) card.findViewById(R.id.text_legend_operated);
    operated.setText(CommonLocalizables.getInstance().getCommonOperatedbytext(getContext()));

    airlineName.setTypeface(mFonts.getRegular());
    classFlight.setTypeface(mFonts.getRegular());
    timeFlight.setTypeface(mFonts.getRegular());

    Carrier operationCarrier = section.getCarrier();
    String airline = null;
    if (operationCarrier != null) {
      String url = Configuration.getInstance().getImagesSources().getUrlAirlineLogos()
          + operationCarrier.getCode()
          + ".png";
      imageLoader.load(airlineIcon, url);

      airline = operationCarrier.getName()
          + LINE
          + operationCarrier.getCode()
          + SPACE
          + section.getSectionId();
    }
    airlineName.setText(airline);
    if (section.getCabinClass() != null && !section.getCabinClass().isEmpty()) {
      classFlight.setText(LocalizablesFacade.getString(getContext(),
          Util.getResourceCabinClass(CabinClassDTO.valueOf(section.getCabinClass()))));
    }

    timeFlight.setText(durationFormatter.format(section.getDuration()));
    if (section.getAircraft() != null && !section.getAircraft().isEmpty()) {
      card.findViewById(R.id.icon_aircraft_itemInfo).setVisibility(VISIBLE);
      TextView aircraftCode = (TextView) card.findViewById(R.id.text_aircraft_summaryWidget);
      aircraftCode.setText(section.getAircraft());
      aircraftCode.setTypeface(mFonts.getRegular());
    }
    mListFlights.addView(card);
  }

  private void addScale(Calendar arrivalTime, Calendar departureTime) {
    long timeScale = departureTime.getTimeInMillis() - arrivalTime.getTimeInMillis();

    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(timeScale);
    calendar.setTimeZone(TimeZone.getTimeZone(GMT_TIMEZONE));

    int hour = calendar.get(Calendar.HOUR_OF_DAY);
    int minutes = calendar.get(Calendar.MINUTE);

    View card = LayoutInflater.from(getContext())
        .inflate(R.layout.layout_summary_travel_item_scale, mListFlights, false);
    card.findViewById(R.id.top_line).setLayerType(LAYER_TYPE_SOFTWARE, null);
    card.findViewById(R.id.bottom_line).setLayerType(LAYER_TYPE_SOFTWARE, null);

    ((TextView) card.findViewById(R.id.text_time_scale_summaryWidget)).setText(
        durationFormatter.format(hour, minutes));
    ((TextView) card.findViewById(R.id.text_time_scale_summaryWidget)).setTypeface(
        mFonts.getBold());
    TextView tvScaleTitle = (TextView) card.findViewById(R.id.text_title_scale);
    tvScaleTitle.setTypeface(mFonts.getBold());
    tvScaleTitle.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.CELLSTOP_LABEL_CHANGEPLANE));
    TextView textTimeFlight = ((TextView) card.findViewById(R.id.text_time_flight_summaryWidget));
    textTimeFlight.setTypeface(mFonts.getBold());
    textTimeFlight.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.STOPCELL_STOP_DURATION));

    if (Configuration.getInstance()
        .getCurrentMarket()
        .getLanguage()
        .toUpperCase()
        .equals(Constants.GERMANY_MARKET)) {
      textTimeFlight.setWidth(
          getResources().getDimensionPixelSize(R.dimen.text_time_flight_summaryWidget_width));
    }

    TextView tvLegendScale = (TextView) card.findViewById(R.id.text_legend_scale);
    tvLegendScale.setTypeface(mFonts.getRegular());
    tvLegendScale.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.CELLSTOP_LABEL_WARNIG));
    mListFlights.addView(card);
  }
}
