package com.odigeo.app.android.lib.consts;

import android.support.annotation.Nullable;
import com.odigeo.data.entity.BaseSpinnerItem;

/**
 * Created by ManuelOrtiz on 09/09/2014.
 */
public enum DistanceUnits implements BaseSpinnerItem {
  METERS(BaseSpinnerItem.EMPTY_STRING), KILOMETERS("common_units_km"), MILES("common_units_miles");

  String stringKey;

  DistanceUnits(String stringKey) {
    this.stringKey = stringKey;
  }

  public static DistanceUnits fromValue(String value) {
    return valueOf(value);
  }

  public String value() {
    return name();
  }

  @Override public String getShownTextKey() {
    return stringKey;
  }

  @Override public int getImageId() {
    return EMPTY_RESOURCE;
  }

  @Nullable @Override public String getShownText() {
    return null;
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public Object getData() {
    return null;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }
}
