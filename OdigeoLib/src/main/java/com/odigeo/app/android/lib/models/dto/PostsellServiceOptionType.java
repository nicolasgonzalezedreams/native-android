package com.odigeo.app.android.lib.models.dto;

public enum PostsellServiceOptionType {
  BASIC, STANDARD, PREMIUM;
}
