package com.odigeo.app.android.lib.models.dto;

/**
 * @author ruben
 */
public class HotelRoomsResponseDTO extends BaseResponseDTO {

  protected SearchRoomResponseDTO searchRoomResponse;

  public SearchRoomResponseDTO getSearchRoomResponse() {
    return searchRoomResponse;
  }

  public void setSearchRoomResponse(SearchRoomResponseDTO searchRoomResponse) {
    this.searchRoomResponse = searchRoomResponse;
  }
}
