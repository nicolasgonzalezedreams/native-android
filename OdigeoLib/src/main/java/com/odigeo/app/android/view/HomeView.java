package com.odigeo.app.android.view;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.helper.CrossCarUrlHandler;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.HotelsUrlBuilder;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.services.SyncService;
import com.odigeo.app.android.utils.BrandUtils;
import com.odigeo.app.android.utils.deeplinking.DeepLinkingUtil;
import com.odigeo.app.android.view.adapters.CardAdapter;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DialogHelper;
import com.odigeo.app.android.view.interfaces.AuthDialogActionInterface;
import com.odigeo.app.android.view.interfaces.ListenerUpdateCarousel;
import com.odigeo.data.entity.carrousel.BaggageBeltCard;
import com.odigeo.data.entity.carrousel.BoardingGateCard;
import com.odigeo.data.entity.carrousel.CampaignCard;
import com.odigeo.data.entity.carrousel.CancelledSectionCard;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.Carousel;
import com.odigeo.data.entity.carrousel.DelayedSectionCard;
import com.odigeo.data.entity.carrousel.DivertedSectionCard;
import com.odigeo.data.entity.carrousel.SearchDropOffCard;
import com.odigeo.data.entity.carrousel.SectionCard;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.dataodigeo.net.controllers.FacebookController;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.dataodigeo.welcome.WelcomeProvider;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.presenter.HomePresenter;
import com.odigeo.presenter.contracts.navigators.NavigationDrawerNavigatorInterface;
import com.odigeo.presenter.contracts.views.CardViewInterface;
import com.odigeo.presenter.contracts.views.HomeViewInterface;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.List;

public class HomeView extends BaseView<HomePresenter>
    implements HomeViewInterface, ListenerUpdateCarousel {

  public static final String HOME_SCREEN_LABEL_NAME = "navtab";

  protected LinearLayout mButtonsContainer;
  protected RelativeLayout mRlFlight;
  protected RelativeLayout mRlHotel;
  protected RelativeLayout mRlCar;
  protected TextView mTvTitleFlight;
  protected TextView mTvSubtitleFlight;
  protected TextView mTvTitleHotel;
  protected TextView mTvSubtitleHotel;
  protected TextView mTvTitleCar;
  protected TextView mTvSubtitleCar;
  private TextView mEmptyText;
  private TextView mEmptyMainText;
  private TextView mEmptySubText;
  private View mHomeHeader;
  private ViewPager mCardsPager;
  private PagerAdapter mCardsPagerAdapter;
  private List<Fragment> mCards = new ArrayList<>();
  private float mPosition = 0f;
  private SyncService mService;
  private boolean mBound;
  private TextView mWelcomeMessage;
  private DialogHelper mDialogHelper;
  private MarketProviderInterface mMarketProvider;
  /**
   * Defines callbacks for service binding, passed to bindService()
   */
  private ServiceConnection mConnection = new ServiceConnection() {

    @Override public void onServiceConnected(ComponentName className, IBinder service) {
      // We've bound to LocalService, cast the IBinder and get LocalService instance
      SyncService.SyncServiceBinder binder = (SyncService.SyncServiceBinder) service;
      mService = binder.getService();
      mBound = true;
      mPresenter.synchronizeData();
    }

    @Override public void onServiceDisconnected(ComponentName arg0) {
      mBound = false;
    }
  };

  public static HomeView newInstance() {
    return new HomeView();
  }

  @Override protected HomePresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideHomePresenter(this, (NavigationDrawerNavigatorInterface) getActivity());
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mDialogHelper = new DialogHelper(getActivity());
    mMarketProvider = AndroidDependencyInjector.getInstance().provideMarketProvider();
  }

  @Override public void onResume() {
    super.onResume();
    hideButtons();

    mPresenter.subscribeToUpdateBooking();
    Intent intent = new Intent(getActivity(), SyncService.class);
    getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_HOME);

    if (shouldShowDefaultBackground(false)) {
      setDefaultBackgroundImage();
    }
    mPresenter.updateCities();
    mPresenter.getCachedCarousel();
  }

  @Override public void onPause() {
    super.onPause();
    if (mPresenter.isUpdateBookingEventActive()) {
      mPresenter.unSubscribeToUpdateBooking();
    }
    if (mBound) {
      getActivity().unbindService(mConnection);
      mBound = false;
    }
  }

  @Override public void synchronizeData(OnAuthRequestDataListener<Void> listener) {
    mService.synchronizeData(listener);
  }

  @Override public void showAuthErrorMessage() {
    if (getActivity() != null && !getActivity().isFinishing()) {
      mDialogHelper.showAuthErrorDialog(this, new AuthDialogActionInterface() {
        @Override public void OnAuthDialogOK() {
          mPresenter.onAuthOkClicked();
        }
      });
      localLogout();
    }
  }

  @Override public boolean isActive() {
    return isAdded();
  }

  private void localLogout() {
    //TODO: Cambiar eso cuando se refactorice login social
    FacebookController facebookController =
        AndroidDependencyInjector.getInstance().provideFacebookHelper(this, null);
    facebookController.logout();
  }

  @Override public void onDestroy() {
    super.onDestroy();
    mPresenter.unSubscribeToUpdateBooking();
    mPresenter.clearLocalyticsKeys();
  }

  @Override protected int getFragmentLayout() {
    return R.layout.odigeo_home_view;
  }

  @Override protected void initComponent(View view) {
    mButtonsContainer = (LinearLayout) view.findViewById(R.id.llButtonsContainer);
    LayoutInflater.from(getActivity())
        .inflate(R.layout.layout_home_menu_buttons, mButtonsContainer);
    mRlFlight = (RelativeLayout) view.findViewById(R.id.rlFlight);
    mRlHotel = (RelativeLayout) view.findViewById(R.id.rlHotel);
    mRlCar = (RelativeLayout) view.findViewById(R.id.rlCar);
    mTvTitleFlight = (TextView) view.findViewById(R.id.tvTitleFlight);
    mTvSubtitleFlight = (TextView) view.findViewById(R.id.tvSubtitleFlight);
    mTvTitleHotel = (TextView) view.findViewById(R.id.tvTitleHotel);
    mTvSubtitleHotel = (TextView) view.findViewById(R.id.tvSubtitleHotel);
    mTvTitleCar = (TextView) view.findViewById(R.id.tvTitleCar);
    mTvSubtitleCar = (TextView) view.findViewById(R.id.tvSubtitleCar);
    mHomeHeader = view.findViewById(R.id.home_header);

    mEmptyText = (TextView) view.findViewById(R.id.tvCarrousel);
    mEmptyMainText = (TextView) view.findViewById(R.id.tvEmptyTitle);
    mEmptySubText = (TextView) view.findViewById(R.id.tvEmptySubTitle);

    int pagerOffScreenPageLimit = 2;
    mCardsPager = (ViewPager) view.findViewById(R.id.vpCards);
    mCardsPager.setClipToPadding(false);
    mCardsPager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.viewpager_margin));
    mCardsPager.setOffscreenPageLimit(pagerOffScreenPageLimit);

    mWelcomeMessage = (TextView) view.findViewById(R.id.tv_welcome_message);
    mCardsPagerAdapter = new CardAdapter(getChildFragmentManager(), mCards);
    mCardsPager.setAdapter(mCardsPagerAdapter);
  }

  private void setViewPagerListeners() {
    mCardsPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mCards.size() > 0) {
          mPresenter.translateHomeBackgroundPosition(-mPosition);
          mPosition = position + positionOffset;
        }
      }

      @Override public void onPageSelected(int position) {
      }

      @Override public void onPageScrollStateChanged(int state) {
      }
    });
  }

  private void handleMTTCardTracking(Fragment cardView, int position) {
    String cardLabel = "";
    if (cardView instanceof NextTripDelayedView) {
      cardLabel = TrackerConstants.LABEL_CARD_DELAYED;
    } else if (cardView instanceof NextTripView) {
      cardLabel = TrackerConstants.LABEL_CARD_NEXT_FLIGHT_OK;
    } else if (cardView instanceof CancelledTripView) {
      cardLabel = TrackerConstants.LABEL_CARD_CANCELLED;
    } else if (cardView instanceof DivertedTripView) {
      cardLabel = TrackerConstants.LABEL_CARD_DIVERTED;
    } else if (cardView instanceof BoardingGateCardView) {
      cardLabel = TrackerConstants.LABEL_CARD_BOARDING_GATE;
    } else if (cardView instanceof BaggageBeltCardView) {
      cardLabel = TrackerConstants.LABEL_CARD_BAGGAGE_BELT;
    } else if (cardView instanceof SearchDropOffCardView) {
      cardLabel = TrackerConstants.LABEL_CARD_SEARCH;
    }
    trackCardShown(cardLabel, position);
  }

  private void handleCampaignCardTracking(String url, int position) {
    String cardLabel = "";
    DeepLinkingUtil.DeepLinkType deepLinkType =
        DeepLinkingUtil.getCampaignCardType(getContext(), Uri.parse(url));
    switch (deepLinkType) {
      case CARS:
        cardLabel = TrackerConstants.LABEL_CARD_CARS;
        break;
      case SEARCH:
        cardLabel = TrackerConstants.LABEL_CARD_PROMO;
        break;
      case HOTELS:
        cardLabel = TrackerConstants.LABEL_CARD_HOTEL;
        break;
      case SSO:
        cardLabel = TrackerConstants.LABEL_CARD_SSO;
        break;
    }
    trackCardShown(cardLabel, position);
  }

  private void trackCardShown(String cardLabel, int position) {
    if (!cardLabel.isEmpty()) {
      String trackingLabel =
          TrackerConstants.LABEL_CARD_SHOWN_FORMAT.replace(TrackerConstants.CARD_TYPE_IDENTIFIER,
              cardLabel)
              .replace(TrackerConstants.CARD_POSITION_IDENTIFIER, String.valueOf(position));
      mTracker.trackMTTCardEvent(trackingLabel);
    }
  }

  public void hideButtons() {
    if (Configuration.getInstance().getCurrentMarket().getKey().equals(Constants.GREEK_MARKET)) {
      mRlCar.setVisibility(View.GONE);
    }
  }

  @Override protected void setListeners() {
    mRlFlight.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_HOME,
            TrackerConstants.ACTION_PRODUCT_SELECTOR,
            TrackerConstants.LABEL_PRODUCT_FLIGHTS_CLICKS);
        mPresenter.showSearchView();
      }
    });

    mRlHotel.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_HOME,
            TrackerConstants.ACTION_PRODUCT_SELECTOR, TrackerConstants.LABEL_PRODUCT_HOTELS_CLICKS);
        mTracker.trackLocalyticsEvent(TrackerConstants.CLICKED_CROSS_SELL_HOTELS);

        mPresenter.showHotelsView(buildHotelsUrl());
      }
    });

    mRlCar.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_HOME,
            TrackerConstants.ACTION_PRODUCT_SELECTOR, TrackerConstants.LABEL_PRODUCT_CARS_CLICKS);
        mTracker.trackLocalyticsEvent(TrackerConstants.CLICKED_CROSS_SELL_CARS);

        String url = LocalizablesFacade.getString(getActivity(),
            OneCMSKeys.WEBVIEWCONTROLLER_CARS_HOME_URL_ANDROID).toString();

        if (url.contains(LocalizablesFacade.NOT_FOUND_TEXT)) {
          url = "";
        } else {
          url = url.replace("[" + CrossCarUrlHandler.PREF_CURRENCY + "]",
              mMarketProvider.getCurrencyKey());
        }

        mPresenter.showCarsView(url);
      }
    });
  }

  private String buildHotelsUrl() {
    String url;
    if (BrandUtils.isTravellinkBrand(mMarketProvider.getBrand()) || BrandUtils.isOpodoNordicsBrand(
        mMarketProvider.getBrand(), mMarketProvider.getCrossSellingMarketKey())) {

      url = new HotelsUrlBuilder(false, HOME_SCREEN_LABEL_NAME, mMarketProvider).getUrl();
    } else {
      url = String.format(mMarketProvider.getWebViewHotelsURL(), mMarketProvider.getAid(),
          mMarketProvider.getLanguage(), mMarketProvider.getCurrencyKey(),
          mMarketProvider.getWebsite().toLowerCase());
    }
    return url;
  }

  @Override protected void initOneCMSText(View view) {
    mTvTitleFlight.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.FLIGHTS_TITLE));
    mTvSubtitleFlight.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.FLIGHTS_SUBTITLE));
    mTvTitleHotel.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.HOTELS_TITLE));
    mTvSubtitleHotel.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.HOTELS_SUBTITLE));
    mTvTitleCar.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARS_TITLE));
    mTvSubtitleCar.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARS_SUBTITLE));

    mEmptyText.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.NO_TRIPS_MESSAGE,
        Configuration.getInstance().getBrandVisualName()));
    if (mEmptyMainText.getVisibility() != View.GONE) {
      mEmptyMainText.setText(
          LocalizablesFacade.getString(getActivity(), OneCMSKeys.NO_TRIPS_MAIN_MESSAGE,
              Configuration.getInstance().getBrandVisualName()));
    }
    if (mEmptySubText.getVisibility() != View.GONE) {
      mEmptySubText.setText(
          LocalizablesFacade.getString(getActivity(), OneCMSKeys.NO_TRIPS_SECONDARY_MESSAGE,
              Configuration.getInstance().getBrandVisualName()));
    }
  }

  @Override public void onUpdateCarousel() {
    mPresenter.synchronizeData();
    mPresenter.getCachedCarousel();
  }

  @Override public void showCards(Carousel carousel) {

    long lastUpdate = mPresenter.getLastCarouselUpdate();
    mCards.clear();
    List<Card> cardList = carousel.getCarrouselCards();

    changeBackgroundElementsVisibility(isCardListEmpty(cardList));

    addCardsToPager(cardList, lastUpdate);

    initializeBackground(carousel);

    initializeWelcomeMessage(!isCardListEmpty(cardList));
  }

  private void initializeWelcomeMessage(final boolean shouldDisplayMessage) {

    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override public void run() {
        int visibility = View.GONE;
        if (!isSmallScreen() && shouldDisplayMessage) {
          WelcomeProvider welcomeHelper =
              AndroidDependencyInjector.getInstance().provideWelcomeProvider();
          String templateMessage = LocalizablesFacade.getRawString(getContext(),
              OneCMSKeys.HOME_VIEM_CONTROLLER_HI_NAME);
          String messageWithName = welcomeHelper.getWelcomeMessage(templateMessage);
          if (messageWithName != null) {
            visibility = View.VISIBLE;
            mWelcomeMessage.setText(messageWithName);
          }
        }
        mWelcomeMessage.setVisibility(visibility);
      }
    });
  }

  private boolean isSmallScreen() {
    if (getActivity() != null && isAdded()) {
      DisplayMetrics metrics = new DisplayMetrics();
      getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
      switch (metrics.densityDpi) {
        case DisplayMetrics.DENSITY_LOW:
        case DisplayMetrics.DENSITY_MEDIUM:
          return true;
        default:
          return false;
      }
    } else {
      return true;
    }
  }

  protected void changeBackgroundElementsVisibility(boolean makeVisible) {
    if (makeVisible) {
      mHomeHeader.setVisibility(View.VISIBLE);
    } else {
      mHomeHeader.setVisibility(View.GONE);
    }
  }

  private boolean isCardListEmpty(List<Card> cardList) {
    return cardList == null || cardList.size() == 0 || (cardList.size() == 1
        && cardList.get(0).getType() == Card.CardType.HOME_CARD);
  }

  private void addCardsToPager(List<Card> cardList, long lastUpdate) {
    if (isCardListEmpty(cardList) && shouldShowPromotionText()) {
      mEmptyText.setVisibility(View.VISIBLE);
    }

    boolean shouldUpdateIndex = false;
    for (Card card : cardList) {
      Fragment cardToAdd = getCardByType(card, lastUpdate);
      if (cardToAdd != null && cardToAdd instanceof CardViewInterface) {
        ((CardViewInterface) cardToAdd).setCardPosition(mCards.size());

        if (card.getType().equals(Card.CardType.PROMOTION_CARD)) {
          addPromotionCard(cardToAdd, ((CampaignCard) card).getSubType(),
              ((CampaignCard) card).getUrl());
        } else {
          addMTTCard(cardToAdd);
        }
        if (shouldUpdateIndex(card)) {
          shouldUpdateIndex = true;
        }
      }
    }

    mCardsPagerAdapter.notifyDataSetChanged();
    if (shouldUpdateIndex) {
      mCardsPager.setCurrentItem(mCards.size() - 1);
    }
  }

  private boolean shouldUpdateIndex(Card card) {
    return mPresenter.cameFromLocalyticsPush()
        && card instanceof CampaignCard
        && ((CampaignCard) card).getCampaignCardKey() != null
        && ((CampaignCard) card).getCampaignCardKey().equals(mPresenter.getLocalyticsPushContent());
  }

  private void addPromotionCard(Fragment cardToAdd, CampaignCard.CardSubType subType, String url) {
    if (shouldAddPromotionCard(subType)) {
      mCards.add(cardToAdd);
      handleCampaignCardTracking(url, mCards.size());
      ((CardViewInterface) cardToAdd).setCardPosition(mCards.size());
    }
  }

  private boolean shouldAddPromotionCard(CampaignCard.CardSubType subType) {
    return subType == null
        || (subType.equals(CampaignCard.CardSubType.LOGGED)
        && getPresenter().userIsLoggedIn())
        || (subType.equals(CampaignCard.CardSubType.NOT_LOGGED)
        && !getPresenter().userIsLoggedIn())
        || subType.equals(CampaignCard.CardSubType.PROMOCODE)
        || subType.equals(CampaignCard.CardSubType.OTHER)
        || subType.equals(CampaignCard.CardSubType.DISCOUNT)
        || subType.equals(CampaignCard.CardSubType.FEATURES)
        || subType.equals(CampaignCard.CardSubType.NEWS)
        || subType.equals(CampaignCard.CardSubType.INBOX);
  }

  private void addMTTCard(Fragment cardToAdd) {
    mCards.add(cardToAdd);
    handleMTTCardTracking(cardToAdd, mCards.size());
    ((CardViewInterface) cardToAdd).setCardPosition(mCards.size());
  }

  private Fragment getCardByType(Card card, long lastUpdate) {
    switch (card.getType()) {
      case PROMOTION_CARD:
        return CampaignCardView.newInstance((CampaignCard) card);
      case CANCELLED_SECTION_CARD:
        return CancelledTripView.newInstance((CancelledSectionCard) card, lastUpdate);
      case BAGGAGE_BELT_CARD:
        return BaggageBeltCardView.newInstance((BaggageBeltCard) card, lastUpdate);
      case DIVERTED_SECTION_CARD:
        return DivertedTripView.newInstance((DivertedSectionCard) card, lastUpdate);
      case DELAYED_SECTION_CARD:
        return NextTripDelayedView.newInstance((DelayedSectionCard) card, lastUpdate);
      case SECTION_CARD:
        return NextTripView.newInstance((SectionCard) card, lastUpdate);
      case BOARDING_GATE_CARD:
        return BoardingGateCardView.newInstance((BoardingGateCard) card, lastUpdate);
      case DROP_OFF_CARD:
        return SearchDropOffCardView.newInstance((SearchDropOffCard) card);
      default:
        return null;
    }
  }

  private void initializeBackground(Carousel carousel) {

    if (mCards.size() > 0) {
      setViewPagerListeners();
      Card currentCard = carousel.getCarrouselCards().get(mCardsPager.getCurrentItem());
      if (!currentCard.getType().equals(Card.CardType.PROMOTION_CARD)) {
        mPresenter.setCityBackgroundImage(currentCard.getImage());
      } else if (hasCarouselOnlyCampaignCards(carousel.getCarrouselCards())) {
        mPresenter.setCampaignBackgroundImage();
      }
    } else if (shouldShowDefaultBackground(isCardListEmpty(carousel.getCarrouselCards()))) {
      setDefaultBackgroundImage();
    }
  }

  //TODO: Use the generic tracker
  @Subscribe public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getActivity().getApplication());
  }

  //TODO: Use the generic tracker
  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getActivity().getApplication());
  }

  @Override public void setDefaultBackgroundImage() {

  }

  @Override public void setCampaignBackgroundImage() {

  }

  @Override public void setCityBackgroundImage(String imageUrl) {

  }

  @Override public void translateBackgroundPosition(float position) {

  }

  protected boolean shouldShowPromotionText() {
    return false;
  }

  private boolean shouldShowDefaultBackground(boolean isCarouselEmpty) {
    boolean isFragmentAttached = isAdded();
    boolean isActivityNull = getActivity() == null;
    boolean isCardPagerNull = mCardsPager == null;
    boolean isCardPagerAdapterNull = mCardsPager.getAdapter() == null;
    boolean isCardPagerEmpty = false;
    if (!isCardPagerAdapterNull) {
      isCardPagerEmpty = mCardsPager.getAdapter().getCount() == 0;
    }
    boolean isCardPagerHide = mCardsPager.getVisibility() == View.GONE;

    return (isFragmentAttached && !isActivityNull) && (isCardPagerNull
        || isCardPagerAdapterNull
        || isCardPagerEmpty
        || isCardPagerHide
        || isCarouselEmpty);
  }

  private boolean hasCarouselOnlyCampaignCards(List<Card> cardList) {
    boolean hasOnlyCampaignCards = true;
    for (Card card : cardList) {
      if (!card.getType().equals(Card.CardType.PROMOTION_CARD) && !card.getType()
          .equals(Card.CardType.HOME_CARD)) {
        hasOnlyCampaignCards = false;
      }
    }
    return hasOnlyCampaignCards;
  }
}
