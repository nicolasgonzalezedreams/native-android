package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.odigeo.app.android.lib.activities.OdigeoSearchActivity;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.fragments.base.TripFragmentBase;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.data.entity.TravelType;
import java.util.List;

/**
 * Created by Irving on 01/09/2014.
 */
public class SimpleTripFragment extends TripFragmentBase {
  private FlightSegment segmentTo;

  /**
   * Creates a new instance of this fragment using search options and flight segments
   *
   * @param searchOptions Search options to loadWidgetImage
   * @param flightSegments Flight segments to show
   * @return A new instance of this fragment
   */
  public static SimpleTripFragment newInstance(SearchOptions searchOptions,
      List<FlightSegment> flightSegments) {
    SimpleTripFragment simpleTripFragment = new SimpleTripFragment();
    simpleTripFragment.setSearchOptions(searchOptions);
    simpleTripFragment.setFlightSegments(flightSegments);
    return simpleTripFragment;
  }

  @Override public void onAttach(Activity activity) {
    super.onAttach(activity);

    setSegments();
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = super.onCreateView(inflater, container, savedInstanceState);

    if (isShouldLaunchSearch()) {
      setShouldLaunchSearch(false);
      launchSearch(false);
    }

    searchTripWidget.hideReturnDate();

    return rootView;
  }

  @Override public void setSegments() {
    if (getActivity() != null
        && ((OdigeoSearchActivity) getActivity()).getFlightSegmentsSimple() != null) {
      segmentTo = ((OdigeoSearchActivity) getActivity()).getFlightSegmentsSimple().get(0);
    }
  }

  @Override public void setControlsValues() {
    super.setControlsValues();

    //Set Departure City
    getSearchTripWidget().setCityFrom(segmentTo.getDepartureCity());

    //Set Arrival City
    getSearchTripWidget().setCityTo(segmentTo.getArrivalCity());

    //Set the departure Date
    getSearchTripWidget().setDepartureDate(OdigeoDateUtils.createDate(segmentTo.getDate()));

    showResidentWidget(getSearchOptions().getResident());
  }

  @Override public boolean isDataValid() {
    return segmentTo.getDate() != 0
        && segmentTo.getDepartureCity() != null
        && segmentTo.getArrivalCity() != null
        && !segmentTo.getDepartureCity()
        .getIataCode()
        .equalsIgnoreCase(segmentTo.getArrivalCity().getIataCode());
  }

  @Override public TravelType getTravelType() {
    return TravelType.SIMPLE;
  }

  @Override public boolean isShownInThisFragment(SearchOptions searchOptions) {
    return searchOptions.getTravelType() != TravelType.MULTIDESTINATION;
  }
}
