package com.odigeo.app.android.view.dialogs;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;

public class BookingOutdatedDialog extends AlertDialog.Builder {

  private Activity mActivity;

  public BookingOutdatedDialog(Activity activity) {
    super(activity);
    mActivity = activity;
    setCancelable(false);
    setTitle(LocalizablesFacade.getString(mActivity, OneCMSKeys.BOOKING_OUTDATED_TITLE));
    setMessage(LocalizablesFacade.getString(mActivity, OneCMSKeys.BOOKING_OUTDATED_MESSAGE));
    setPositiveButton(LocalizablesFacade.getString(mActivity, OneCMSKeys.BOOKING_OUTDATED_RELAUNCH),
        new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialogInterface, int i) {
            OdigeoApp odigeoApp = (OdigeoApp) mActivity.getApplication();
            Intent intent = new Intent(mActivity, odigeoApp.getSearchFlightsActivityClass());
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(Constants.EXTRA_ACTION_RESEARCH, true);
            mActivity.startActivity(intent);
          }
        });
    setNegativeButton(
        LocalizablesFacade.getString(mActivity, OneCMSKeys.BOOKING_OUTDATED_NEW_SEARCH),
        new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialogInterface, int i) {
            OdigeoApp odigeoApp = (OdigeoApp) mActivity.getApplication();
            Intent intent = new Intent(mActivity, odigeoApp.getSearchFlightsActivityClass());
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mActivity.startActivity(intent);
          }
        });
  }
}