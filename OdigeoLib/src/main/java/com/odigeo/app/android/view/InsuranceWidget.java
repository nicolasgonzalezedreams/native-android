package com.odigeo.app.android.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.view.animations.CollapseAndExpandAnimationUtil;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.custom.BubbleLayout;
import com.odigeo.data.entity.shoppingCart.Insurance;
import com.odigeo.data.entity.shoppingCart.InsuranceOffer;
import com.odigeo.data.entity.shoppingCart.InsuranceShoppingItem;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import java.math.BigDecimal;

public class InsuranceWidget extends LinearLayout implements View.OnClickListener {

  private TrackerControllerInterface mTracker;

  private BubbleLayout mBlRecommendedInsurance;
  private CheckBox mCbInsurance;
  private TextView mTvBubbleText, mTvInsuranceTitle, mTvInsurancePrice,
      mTvInsurancePricePerPassenger, mTvTAC, mTvConditionsDocument, mTvMoreInfo;
  private LinearLayout mLlInsuranceDescriptionShort, mLlInsuranceDeclineExpandedContent,
      mLlInsuranceDescription;

  private InsuranceCardListener mListener;

  private InsuranceOffer mInsuranceOffer;
  private Insurance mInsurance;
  private BigDecimal mInsuranceTotalPrice;
  private int mTotalPassengers;

  private Activity mActivity;
  private Context mContext;
  private ScrollView mScrollView;

  public InsuranceWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public InsuranceWidget(Activity activity, ScrollView scrollView, InsuranceOffer insuranceOffer,
      int totalPassengers, InsuranceCardListener insuranceCardListener) {
    super(activity.getApplicationContext());
    inflate(activity.getApplicationContext(), R.layout.insurance_widget, this);
    mActivity = activity;
    mContext = activity.getApplicationContext();
    mScrollView = scrollView;
    mListener = insuranceCardListener;
    mInsuranceOffer = insuranceOffer;
    mInsurance = mInsuranceOffer.getInsurances().get(0);
    mInsuranceTotalPrice = mInsuranceOffer.getTotalPrice();
    mTotalPassengers = totalPassengers;
    setOnClickListener(this);
    bindViews();
    addContent(mInsurance);
    mTracker = AndroidDependencyInjector.getInstance().provideTrackerController();
  }

  public InsuranceWidget(Activity activity, ScrollView scrollView,
      InsuranceShoppingItem insuranceShoppingItem, int totalPassengers) {
    super(activity.getApplicationContext());
    inflate(activity.getApplicationContext(), R.layout.insurance_widget, this);
    mActivity = activity;
    mContext = activity.getApplicationContext();
    mScrollView = scrollView;
    mListener = null;
    mInsurance = insuranceShoppingItem.getInsurance();
    mInsuranceTotalPrice = insuranceShoppingItem.getTotalPrice();
    mTotalPassengers = totalPassengers;
    setOnClickListener(this);
    bindViews();
    mCbInsurance.setVisibility(GONE);
    addContent(mInsurance);
    mTracker = AndroidDependencyInjector.getInstance().provideTrackerController();
  }

  public InsuranceOffer getInsuranceOffer() {
    if (mInsuranceOffer != null) {
      return mInsuranceOffer;
    } else {
      return null;
    }
  }

  public CheckBox getCheckbox() {
    return mCbInsurance;
  }

  public void setChecked(boolean value) {
    mCbInsurance.setChecked(value);
  }

  void bindViews() {
    mBlRecommendedInsurance = (BubbleLayout) findViewById(R.id.blRecommendedInsurance);
    mTvBubbleText = (TextView) findViewById(R.id.tvBubbleText);
    mCbInsurance = (CheckBox) findViewById(R.id.cbInsurance);
    mTvInsuranceTitle = (TextView) findViewById(R.id.tvInsuranceTitle);
    mTvInsurancePrice = (TextView) findViewById(R.id.tvInsurancePrice);
    mTvInsurancePricePerPassenger = (TextView) findViewById(R.id.tvInsurancePricePerPassenger);
    mLlInsuranceDescriptionShort = (LinearLayout) findViewById(R.id.llInsuranceDescription_short);
    mLlInsuranceDeclineExpandedContent =
        (LinearLayout) findViewById(R.id.llInsuranceDeclineExpandedContent);
    mLlInsuranceDescription = (LinearLayout) findViewById(R.id.llInsuranceDescription);
    mTvTAC = (TextView) findViewById(R.id.tvTAC);
    mTvTAC.setOnClickListener(this);
    mTvConditionsDocument = (TextView) findViewById(R.id.tvConditionsDocument);
    mTvMoreInfo = (TextView) findViewById(R.id.tvMoreInfo);
    mTvMoreInfo.setOnClickListener(this);
  }

  private void addContent(final Insurance insurance) {
    mTvBubbleText.setText(LocalizablesFacade.getString(mContext,
        OneCMSKeys.INSURANCE_WIDGET_RECOMMENDED_BUBBLE_TEXT));

    String insurancePolicy =
        OneCMSKeys.INSURANCE_VIEW_CONTROLLER + insurance.getPolicy().toLowerCase();

    mTvInsuranceTitle.setText(
        LocalizablesFacade.getString(mContext, insurancePolicy + OneCMSKeys.INSURANCE_TITLE));

    String pricePerPassenger =
        LocaleUtils.getLocalizedCurrencyValue(mInsuranceTotalPrice.doubleValue() / mTotalPassengers,
            LocaleUtils.getCurrentLocale().toString());
    mTvInsurancePrice.setText(String.valueOf(pricePerPassenger));

    mTvInsurancePricePerPassenger.setText(
        LocalizablesFacade.getString(mContext, OneCMSKeys.INSURANCE_PER_PASSENGER_MESSAGE));

    String insuranceDescriptionShort = LocalizablesFacade.getString(mContext,
        insurancePolicy + OneCMSKeys.INSURANCE_DESCRIPTION_SHORT).toString();
    String[] shortDescriptionItems = insuranceDescriptionShort.split("\n");
    for (String descriptionItem : shortDescriptionItems) {
      LayoutInflater inflater =
          (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      View rowView =
          inflater.inflate(R.layout.insurance_row_description, mLlInsuranceDescriptionShort, false);
      mLlInsuranceDescriptionShort.addView(rowView);
      TextView txtDescription = (TextView) rowView.findViewById(R.id.tvInsuranceDescription);
      txtDescription.setText(descriptionItem.trim());
    }

    String insuranceDescription =
        LocalizablesFacade.getString(mContext, insurancePolicy + OneCMSKeys.INSURANCE_DESCRIPTION)
            .toString();
    String[] descriptionItems = insuranceDescription.split("\n");
    for (String descriptionItem : descriptionItems) {
      LayoutInflater inflater =
          (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      View rowView =
          inflater.inflate(R.layout.insurance_row_description, mLlInsuranceDescriptionShort, false);
      mLlInsuranceDescription.addView(rowView);
      TextView txtDescription = (TextView) rowView.findViewById(R.id.tvInsuranceDescription);
      txtDescription.setText(descriptionItem.trim());
    }

    if (insurance.getConditionsUrls() != null) {
      if (insurance.getConditionsUrls().size() >= 1) {
        mTvTAC.setText(
            LocalizablesFacade.getRawString(mContext, OneCMSKeys.INSURANCE_CONDITIONS_ACCEPTANCE));
        OdigeoApp app = (OdigeoApp) mActivity.getApplication();
        HtmlUtils.setActivityLinkTextClickable(mTvTAC, mActivity,
            app.getInsurancesConditionsActivityClass(),
            createTACContent(insurancePolicy, insurance.getConditionsUrls().get(0).getUrl()));
      }

      if (insurance.getConditionsUrls().size() >= 2) {
        mTvConditionsDocument.setText(
            LocalizablesFacade.getRawString(mContext, OneCMSKeys.INSURANCE_CONDITIONS_PDF));
        HtmlUtils.setDownloadPdfLinkTextClickable(mActivity, mTvConditionsDocument,
            mTvInsuranceTitle.getText().toString(), insurance.getConditionsUrls().get(1).getUrl());
      }
    }

    mTvMoreInfo.setText(LocalizablesFacade.getString(mContext, OneCMSKeys.COMMON_SHOW_MORE)
        .toString()
        .toUpperCase());
  }

  private Bundle createTACContent(String insurancePolicy, String conditionsURl) {
    Bundle bundle = new Bundle();
    bundle.putString(Constants.EXTRA_TITLE_ICF,
        LocalizablesFacade.getRawString(mContext, insurancePolicy + OneCMSKeys.INSURANCE_TITLE));
    bundle.putString(Constants.EXTRA_TEXT_LINK_ICF, LocalizablesFacade.getRawString(mContext,
        insurancePolicy + OneCMSKeys.INSURANCE_DOCUMENT_LINK));
    bundle.putString(Constants.EXTRA_LINK_ICF, conditionsURl);
    bundle.putString(Constants.EXTRA_PDF_NAME_ICF, Constants.NAME_INSURANCES_CONDITIONS_BASE);
    bundle.putString(Constants.EXTRA_CONTENT_ICF, LocalizablesFacade.getRawString(mContext,
        insurancePolicy + OneCMSKeys.INSURANCE_TEXT_CONDITIONS));
    bundle.putString(Constants.EXTRA_PRICE_ICF, LocalizablesFacade.getRawString(mContext,
        insurancePolicy + OneCMSKeys.INSURANCE_TOTAL_PRICE));
    bundle.putString(Constants.EXTRA_PRICE_DETAIL_ICF, LocalizablesFacade.getRawString(mContext,
        insurancePolicy + OneCMSKeys.INSURANCE_TOTAL_PRICE_DETAIL));
    return bundle;
  }

  @Override public void onClick(View view) {
    if (view.getId() == R.id.tvTAC) {
      mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_INSURANCES_T_CS);
      mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_INSURANCE,
          TrackerConstants.ACTION_INSURANCE_INFORMATION,
          TrackerConstants.LABEL_INSURANCE_CONDITIONS);
    } else if (view.getId() == R.id.tvMoreInfo) {
      if (mTvMoreInfo.getText()
          .equals(LocalizablesFacade.getString(mContext, OneCMSKeys.COMMON_SHOW_MORE)
              .toString()
              .toUpperCase())) {
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_INSURANCE,
            TrackerConstants.ACTION_NAVIGATION_ELEMENTS,
            TrackerConstants.LABEL_OPEN_INSURANCE_INFO);
        expandDescriptionContent();
        mTvMoreInfo.setText(LocalizablesFacade.getString(mContext, OneCMSKeys.COMMON_SHOW_LESS)
            .toString()
            .toUpperCase());
      } else {
        collapseDescriptionContent();
        mTvMoreInfo.setText(LocalizablesFacade.getString(mContext, OneCMSKeys.COMMON_SHOW_MORE)
            .toString()
            .toUpperCase());
      }
    } else if (mListener != null) {
      toggleInsuranceSelected();
    }
  }

  private void toggleInsuranceSelected() {
    if (mCbInsurance.isChecked()) {
      setChecked(false);
      mListener.onInsuranceUnselected(this);
    } else {
      setChecked(true);
      mListener.onInsuranceSelected(this);
    }
  }

  public void setRecommended() {
    Animation fadeInAnimation = AnimationUtils.loadAnimation(mContext, R.anim.bubble_animation);
    mBlRecommendedInsurance.startAnimation(fadeInAnimation);
  }

  public void expandDescriptionContent() {
    CollapseAndExpandAnimationUtil.expand(mLlInsuranceDeclineExpandedContent);
  }

  public void collapseDescriptionContent() {
    CollapseAndExpandAnimationUtil.collapse(mLlInsuranceDeclineExpandedContent, true, mScrollView,
        (View) mLlInsuranceDeclineExpandedContent.getParent().getParent());
  }

  interface InsuranceCardListener {
    void onInsuranceSelected(InsuranceWidget selectedInsuranceWidget);

    void onInsuranceUnselected(InsuranceWidget unselectedInsuranceWidget);
  }
}