package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class CreditCardDetails implements Serializable {
  private String owner;
  private String creditCardType;
  private String creditCardNumber;
  private String cvv;
  private String creditCardExpirationDate;

  public final String getOwner() {
    return owner;
  }

  public final void setOwner(String owner) {
    this.owner = owner;
  }

  public final String getCreditCardType() {
    return creditCardType;
  }

  public final void setCreditCardType(String creditCardType) {
    this.creditCardType = creditCardType;
  }

  public final String getCreditCardNumber() {
    return creditCardNumber;
  }

  public final void setCreditCardNumber(String creditCardNumber) {
    this.creditCardNumber = creditCardNumber;
  }

  public final String getCvv() {
    return cvv;
  }

  public final void setCvv(String cvv) {
    this.cvv = cvv;
  }

  public final String getCreditCardExpirationDate() {
    return creditCardExpirationDate;
  }

  public final void setCreditCardExpirationDate(String creditCardExpirationDate) {
    this.creditCardExpirationDate = creditCardExpirationDate;
  }
}
