package com.odigeo.app.android.view.textwatchers;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;

public class CVVTextWatcher extends BaseTextWatcher {

  private FieldValidator mFieldValidator;
  private boolean mIsFieldCorrect = false;
  private TextInputLayout mTilCVV;
  private PaymentMethodBaseTextWatchersClient mPaymentMethodBaseTextWatchersClient;

  public CVVTextWatcher(TextInputLayout textInputLayout, FieldValidator fieldValidator,
      PaymentMethodBaseTextWatchersClient paymentMethodBaseTextWatchersClient) {
    super(textInputLayout);
    mFieldValidator = fieldValidator;
    mTilCVV = textInputLayout;
    mPaymentMethodBaseTextWatchersClient = paymentMethodBaseTextWatchersClient;
  }

  @Override public void afterTextChanged(Editable editableText) {
    super.afterTextChanged(editableText);
    mIsFieldCorrect = mFieldValidator.validateField(editableText.toString());
    mPaymentMethodBaseTextWatchersClient.onCharacterWriteOnField(mIsFieldCorrect, mTilCVV);
  }
}
