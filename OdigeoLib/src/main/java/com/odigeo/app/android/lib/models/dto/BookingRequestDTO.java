package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

/**
 * Created by ManuelOrtiz on 08/10/2014.
 */
public class BookingRequestDTO implements Serializable {
  private String collectionMethodType;
  private CreditCardRequestDTO creditCardRequest;

  public String getCollectionMethodType() {
    return collectionMethodType;
  }

  public void setCollectionMethodType(String collectionMethodType) {
    this.collectionMethodType = collectionMethodType;
  }

  public CreditCardRequestDTO getCreditCardRequest() {
    return creditCardRequest;
  }

  public void setCreditCardRequest(CreditCardRequestDTO creditCardRequest) {
    this.creditCardRequest = creditCardRequest;
  }
}
