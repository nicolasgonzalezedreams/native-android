package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.Passenger;
import com.odigeo.app.android.lib.models.dto.TravellerTypeDTO;
import com.odigeo.app.android.lib.ui.widgets.base.CustomField;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import java.util.Date;

/**
 * Created by manuel on 27/10/14.
 */
public class KidsGrownUpErrorWidget extends CustomField implements View.OnClickListener {

  private final CustomTextView txtDescription;
  private final Button btnModifyKidGrown;
  private Passenger passenger;
  private KidsGrownListener listener;

  public KidsGrownUpErrorWidget(Context context, AttributeSet attrs) {

    super(context, attrs);

    inflate(getContext(), R.layout.layout_kid_grown_up, this);

    txtDescription = (CustomTextView) findViewById(R.id.txtKidGrownDescription);
    btnModifyKidGrown = (Button) findViewById(R.id.btnModifyKidGrown);
    btnModifyKidGrown.setOnClickListener(this);
    btnModifyKidGrown.setText(
        LocalizablesFacade.getString(getContext(), "kidsgrewupcell_button_label").toString());
  }

  public final void setPassenger(Passenger passenger, Date returnDate) {
    this.passenger = passenger;

    this.setVisibility(GONE);

    if (passenger.getTravellerType() != TravellerTypeDTO.ADULT) {

      boolean isGrowing = isGrowing(returnDate);

      if (isGrowing) {

        this.setVisibility(VISIBLE);

        if (passenger.getTravellerType() == TravellerTypeDTO.INFANT) {
          txtDescription.setText(
              LocalizablesFacade.getString(getContext(), "kidsgrewupcell_description_infant")
                  .toString());
        } else if (passenger.getTravellerType() == TravellerTypeDTO.CHILD) {
          txtDescription.setText(
              LocalizablesFacade.getString(getContext(), "kidsgrewupcell_description_child")
                  .toString());
        }
      }
    }
  }

  private boolean isGrowing(Date lastSegmentDate) {
    if (passenger == null || passenger.getBirthDate() == null || lastSegmentDate == null) {
      return false;
    }

    int age = OdigeoDateUtils.getAgeInDate(passenger.getBirthDate(), lastSegmentDate);

    int maximumAge =
        passenger.getTravellerType() == TravellerTypeDTO.INFANT ? Constants.MAX_AGE_INFANT
            : Constants.MAX_AGE_CHILD;

    return age >= maximumAge;
  }

  @Override public final boolean validate() {
    return false;
  }

  @Override public final boolean isValid() {
    return false;
  }

  @Override public void clearValidation() {
    // TODO: clear validation if necessary.
  }

  public final KidsGrownListener getListener() {
    return listener;
  }

  public final void setListener(KidsGrownListener listener) {
    this.listener = listener;
  }

  @Override public final void onClick(View v) {
    if (v.getId() == btnModifyKidGrown.getId() && listener != null) {

      if (passenger.getTravellerType() == TravellerTypeDTO.INFANT) {
        listener.onKidsGrownClick(1, 0);
      } else {
        listener.onKidsGrownClick(0, 1);
      }
    }
  }

  interface KidsGrownListener {
    void onKidsGrownClick(int noBabies, int noKids);
  }
}
