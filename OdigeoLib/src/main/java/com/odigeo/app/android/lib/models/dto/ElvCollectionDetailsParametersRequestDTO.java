package com.odigeo.app.android.lib.models.dto;

public class ElvCollectionDetailsParametersRequestDTO {

  protected String bankAccountNumber;
  protected String bankLocationId;
  protected String ownerName;
  protected String ownerLastName;
  protected String street;
  protected String streetNumber;
  protected String zipCode;
  protected String city;
  protected String countryCode;

  /**
   * Gets the value of the bankAccountNumber property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getBankAccountNumber() {
    return bankAccountNumber;
  }

  /**
   * Sets the value of the bankAccountNumber property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setBankAccountNumber(String value) {
    this.bankAccountNumber = value;
  }

  /**
   * Gets the value of the bankLocationId property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getBankLocationId() {
    return bankLocationId;
  }

  /**
   * Sets the value of the bankLocationId property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setBankLocationId(String value) {
    this.bankLocationId = value;
  }

  /**
   * Gets the value of the ownerName property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getOwnerName() {
    return ownerName;
  }

  /**
   * Sets the value of the ownerName property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setOwnerName(String value) {
    this.ownerName = value;
  }

  /**
   * Gets the value of the ownerLastName property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getOwnerLastName() {
    return ownerLastName;
  }

  /**
   * Sets the value of the ownerLastName property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setOwnerLastName(String value) {
    this.ownerLastName = value;
  }

  /**
   * Gets the value of the street property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getStreet() {
    return street;
  }

  /**
   * Sets the value of the street property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setStreet(String value) {
    this.street = value;
  }

  /**
   * Gets the value of the streetNumber property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getStreetNumber() {
    return streetNumber;
  }

  /**
   * Sets the value of the streetNumber property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setStreetNumber(String value) {
    this.streetNumber = value;
  }

  /**
   * Gets the value of the zipCode property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getZipCode() {
    return zipCode;
  }

  /**
   * Sets the value of the zipCode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setZipCode(String value) {
    this.zipCode = value;
  }

  /**
   * Gets the value of the city property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCity() {
    return city;
  }

  /**
   * Sets the value of the city property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCity(String value) {
    this.city = value;
  }

  /**
   * Gets the value of the countryCode property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCountryCode() {
    return countryCode;
  }

  /**
   * Sets the value of the countryCode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCountryCode(String value) {
    this.countryCode = value;
  }
}
