package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class AccommodationBookingRoom implements Serializable {

  protected String roomDescription;
  protected String cancelPolicy;
  protected String infoPolicy;
  protected String taxesPolicy;
  protected Integer roomNumber;
  protected int adultsNumber;
  protected int infantsNumber;
  protected int childsNumber;

  /**
   * Gets the value of the roomDescription property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getRoomDescription() {
    return roomDescription;
  }

  /**
   * Sets the value of the roomDescription property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setRoomDescription(String value) {
    this.roomDescription = value;
  }

  /**
   * Gets the value of the cancelPolicy property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getCancelPolicy() {
    return cancelPolicy;
  }

  /**
   * Sets the value of the cancelPolicy property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setCancelPolicy(String value) {
    this.cancelPolicy = value;
  }

  /**
   * Gets the value of the infoPolicy property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getInfoPolicy() {
    return infoPolicy;
  }

  /**
   * Sets the value of the infoPolicy property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setInfoPolicy(String value) {
    this.infoPolicy = value;
  }

  /**
   * Gets the value of the taxesPolicy property.
   *
   * @return possible object is
   * {@link String }
   */
  public final String getTaxesPolicy() {
    return taxesPolicy;
  }

  /**
   * Sets the value of the taxesPolicy property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public final void setTaxesPolicy(String value) {
    this.taxesPolicy = value;
  }

  /**
   * Gets the value of the roomNumber property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public final Integer getRoomNumber() {
    return roomNumber;
  }

  /**
   * Sets the value of the roomNumber property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public final void setRoomNumber(Integer value) {
    this.roomNumber = value;
  }

  /**
   * Gets the value of the adultsNumber property.
   */
  public final int getAdultsNumber() {
    return adultsNumber;
  }

  /**
   * Sets the value of the adultsNumber property.
   */
  public final void setAdultsNumber(int value) {
    this.adultsNumber = value;
  }

  /**
   * Gets the value of the infantsNumber property.
   */
  public final int getInfantsNumber() {
    return infantsNumber;
  }

  /**
   * Sets the value of the infantsNumber property.
   */
  public final void setInfantsNumber(int value) {
    this.infantsNumber = value;
  }

  /**
   * Gets the value of the childsNumber property.
   */
  public final int getChildsNumber() {
    return childsNumber;
  }

  /**
   * Sets the value of the childsNumber property.
   */
  public final void setChildsNumber(int value) {
    this.childsNumber = value;
  }
}
