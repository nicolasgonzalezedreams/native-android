package com.odigeo.app.android.lib.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.squareup.otto.Subscribe;

/**
 * Created by Irving on 28/01/2015.
 */
public class OdigeoInsurancesConditionsActivity extends OdigeoForegroundBaseActivity {

  private static final int LINK_LAST_CHARACTER = 3;
  TextView priceDetailTextView;
  String priceDetail;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_insurances_conditions);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    //        FormOdigeoLayout odigeoLayout = (FormOdigeoLayout) findViewById(
    //            R.id.formlayout_insurances_conditions);
    TextView tvInsuranceConditionsTitle = (TextView) findViewById(R.id.tvInsuranceConditionsTitle);
    TextView linkTextView = (TextView) findViewById(R.id.text_link_insurances_conditions);
    TextView contentTextView = (TextView) findViewById(R.id.text_content_insurances_conditions);
    TextView priceTextView = (TextView) findViewById(R.id.text_price_insurances_conditions);
    priceDetailTextView = (TextView) findViewById(R.id.text_price_insurances_detail);

    Intent i = getIntent();

    if (i != null) {
      String title = i.getStringExtra(Constants.EXTRA_TITLE_ICF);
      String textLink = i.getStringExtra(Constants.EXTRA_TEXT_LINK_ICF).replace("<br/>", "\n");
      String link = i.getStringExtra(Constants.EXTRA_LINK_ICF);
      String namePDF = i.getStringExtra(Constants.EXTRA_PDF_NAME_ICF);
      String content = i.getStringExtra(Constants.EXTRA_CONTENT_ICF);
      String price = i.getStringExtra(Constants.EXTRA_PRICE_ICF).replaceAll("\n", "<p>");
      priceDetail = i.getStringExtra(Constants.EXTRA_PRICE_DETAIL_ICF).replace("<br/>", "\n");

      tvInsuranceConditionsTitle.setText(title);
      linkTextView.setText(textLink);
      convertLinkToListener(priceTextView, price);

      HtmlUtils.setDownloadPdfLinkTextClickable(this, linkTextView, namePDF, link);
      Spanned contentText = HtmlUtils.formatHtml(content);
      contentTextView.setText(contentText.toString());
    }
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {
    int i = item.getItemId();
    if (i == android.R.id.home) {
      finish();
    }
    return super.onOptionsItemSelected(item);
  }

  /**
   * A click listener is added to a link in order to display more text when this is tapped.
   */
  private void convertLinkToListener(TextView textView, String text) {
    String newText = text.replace("<a>", "{a}").replace("</a>", "{/a}");
    Spanned htmlText = HtmlUtils.formatHtml(newText);
    newText = htmlText.toString();
    textView.setText(htmlText);

    int startIndex = newText.indexOf("{a}");
    int endIndex = newText.indexOf("{/a}") - LINK_LAST_CHARACTER;

    if (endIndex > startIndex) {
      newText = newText.replace("{a}", "").replace("{/a}", "");

      Spannable spannable = Spannable.Factory.getInstance().newSpannable(newText);
      spannable.setSpan(new MyClickableSpan(), startIndex, endIndex,
          Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

      textView.setText(spannable);
      textView.setMovementMethod(LinkMovementMethod.getInstance());
    }
  }

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getApplication());
  }

  /**
   * Shows the extra text: price details.
   */
  private class MyClickableSpan extends ClickableSpan {

    public MyClickableSpan() {
      priceDetailTextView.setText(priceDetail);
    }

    @Override public void onClick(View widget) {
      priceDetailTextView.setVisibility(View.VISIBLE);
    }

    @Override public void updateDrawState(TextPaint ds) {
      ds.setColor(getApplicationContext().getResources().getColor(R.color.primary_brand));
      ds.setUnderlineText(true);
    }
  }
}
