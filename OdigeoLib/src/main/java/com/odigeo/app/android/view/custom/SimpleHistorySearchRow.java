package com.odigeo.app.android.view.custom;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.AttributeSet;
import android.widget.TextView;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.interfaces.HistorySearchRowListener;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;

public class SimpleHistorySearchRow extends DefaultHistorySearchRow {

  public SimpleHistorySearchRow(Context context, SearchOptions searchOptions,
      HistorySearchRowListener listener) {
    super(context, searchOptions, listener);
  }

  public SimpleHistorySearchRow(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public void drawHistorySearch() {
    String departureCity = getCityName(searchOptions.getFlightSegments().get(0).getDepartureCity());
    String arrivalCity = getCityName(searchOptions.getFlightSegments().get(0).getArrivalCity());

    long searchDate = searchOptions.getFlightSegments().get(0).getDate();
    int totalPassengers = searchOptions.getTotalPassengers();

    SpannableStringBuilder ssb = new SpannableStringBuilder();
    ssb.append(departureCity);
    ssb.append("   ");
    ssb.setSpan(getArrowImageSpan(false), ssb.length() - 2, ssb.length() - 1,
        Spanned.SPAN_INCLUSIVE_INCLUSIVE);
    ssb.append(arrivalCity);

    mRowCities.setText(ssb, TextView.BufferType.SPANNABLE);

    drawDate(OdigeoDateUtils.createDate(searchDate));
    drawNumberOfPassengers(totalPassengers);
  }

  @Override public FlightSegment getCurrentFlightSegment() {
    return searchOptions.getLastSegment();
  }
}
