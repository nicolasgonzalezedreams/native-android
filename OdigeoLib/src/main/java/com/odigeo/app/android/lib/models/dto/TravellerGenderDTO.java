package com.odigeo.app.android.lib.models.dto;

import android.support.annotation.Nullable;
import com.odigeo.data.entity.BaseSpinnerItem;
import java.io.Serializable;

/**
 * <p>Java class for travellerGender.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;simpleType name="travellerGender">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MALE"/>
 *     &lt;enumeration value="FEMALE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
public enum TravellerGenderDTO implements Serializable, BaseSpinnerItem {

  MALE("common_male"), FEMALE("common_female");

  String stringKey;

  TravellerGenderDTO(String stringKey) {
    this.stringKey = stringKey;
  }

  public static TravellerGenderDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

  @Override public String getShownTextKey() {
    return stringKey;
  }

  @Override public int getImageId() {
    return EMPTY_RESOURCE;
  }

  @Nullable @Override public String getShownText() {
    return null;
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public Object getData() {
    return null;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }
}
