package com.odigeo.app.android.lib.models;

import android.support.annotation.DrawableRes;

/**
 * Model for walkthrough pages that are static, this means that the text and the image are included
 * in the app resources
 *
 * @author M.Sc. Javier Silva Perez
 * @since 06/04/15.
 */
public class WalkthroughPageStatic extends WalkthroughPage {

  @DrawableRes private int imageRes;

  /**
   * Creates a new page using an static image instead of a url based image
   *
   * @param imageRes Drawable resource to show
   * @param title Page title to show
   * @param description Description text to show
   */
  public WalkthroughPageStatic(@DrawableRes int imageRes, String title, String description) {
    super(title, description);
    this.imageRes = imageRes;
  }

  @DrawableRes public int getImageRes() {
    return imageRes;
  }

  public void setImageRes(@DrawableRes int imageRes) {
    this.imageRes = imageRes;
  }
}
