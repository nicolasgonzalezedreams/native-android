package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class PostsellServiceOptionBooking implements Serializable {

  private BigDecimal totalPrice;
  private BigDecimal providerPrice;
  private BookingStatusDTO bookingStatus;
  private PostsellServiceOption postsellServiceOption;
  private List<InsuranceProviderBookingDTO> insuranceProviderBookings;
  private List<AdditionalProductProviderBookingDTO> additionalProductProviderBookings;
  private List<MonitoringAlertBooking> monitoringAlertProviderBookings;
  private List<SmsNotificationBooking> smsNotificationProviderBookings;

  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  public void setProviderPrice(BigDecimal providerPrice) {
    this.providerPrice = providerPrice;
  }

  public BookingStatusDTO getBookingStatus() {
    return bookingStatus;
  }

  public void setBookingStatus(BookingStatusDTO bookingStatus) {
    this.bookingStatus = bookingStatus;
  }

  public PostsellServiceOption getPostsellServiceOption() {
    return postsellServiceOption;
  }

  public void setPostsellServiceOption(PostsellServiceOption postsellServiceOption) {
    this.postsellServiceOption = postsellServiceOption;
  }

  public List<InsuranceProviderBookingDTO> getInsuranceProviderBookings() {
    return insuranceProviderBookings;
  }

  public void setInsuranceProviderBookings(
      List<InsuranceProviderBookingDTO> insuranceProviderBookings) {
    this.insuranceProviderBookings = insuranceProviderBookings;
  }

  public List<AdditionalProductProviderBookingDTO> getAdditionalProductProviderBookings() {
    return additionalProductProviderBookings;
  }

  public void setAdditionalProductProviderBookings(
      List<AdditionalProductProviderBookingDTO> additionalProductProviderBookings) {
    this.additionalProductProviderBookings = additionalProductProviderBookings;
  }

  public List<MonitoringAlertBooking> getMonitoringAlertProviderBookings() {
    return monitoringAlertProviderBookings;
  }

  public void setMonitoringAlertProviderBookings(
      List<MonitoringAlertBooking> monitoringAlertProviderBookings) {
    this.monitoringAlertProviderBookings = monitoringAlertProviderBookings;
  }

  public List<SmsNotificationBooking> getSmsNotificationProviderBookings() {
    return smsNotificationProviderBookings;
  }

  public void setSmsNotificationProviderBookings(
      List<SmsNotificationBooking> smsNotificationProviderBookings) {
    this.smsNotificationProviderBookings = smsNotificationProviderBookings;
  }

  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }
}
