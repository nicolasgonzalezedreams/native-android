package com.odigeo.app.android.lib.models.dto;

public enum MovementActionDTO {

  DIRECTY_PAYMENT, AUTHORIZE, REQUEST, CONFIRM, QUERY, REFUND, CANCEL, VERIFY;

  public static MovementActionDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
