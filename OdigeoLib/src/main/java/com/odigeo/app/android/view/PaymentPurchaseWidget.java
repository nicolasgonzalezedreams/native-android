package com.odigeo.app.android.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoWebViewActivity;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.PdfDownloader;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.presenter.PaymentPurchasePresenter;
import com.odigeo.presenter.contracts.views.PaymentPurchaseWidgetInterface;

import static com.facebook.FacebookSdk.getApplicationContext;

public class PaymentPurchaseWidget extends LinearLayout implements PaymentPurchaseWidgetInterface {

  private static final String PDF_EXTENSION = ".pdf";

  private Context context;

  private LinearLayout llContainer;
  private Button btnContinue;
  private CheckBox cbTACAcceptance;
  private TextView termsAndConditions;
  private PaymentPurchasePresenter paymentPurchasePresenter;

  private boolean needsExplicitAcceptance, isExplicitAcceptanceChecked;

  public PaymentPurchaseWidget(Context context) {
    super(context);
    this.context = context;
  }

  public PaymentPurchaseWidget(Context context, boolean needsExplicitAcceptance) {
    super(context);
    this.context = context;
    paymentPurchasePresenter =
        AndroidDependencyInjector.getInstance().providePaymentPurchasePresenter(this);
    initViews(needsExplicitAcceptance);
  }

  public PaymentPurchasePresenter getPresenter() {
    return paymentPurchasePresenter;
  }

  private void initViews(boolean needsExplicitAcceptance) {
    inflate(getContext(), R.layout.payment_purchase_widget, this);
    this.needsExplicitAcceptance = needsExplicitAcceptance;
    isExplicitAcceptanceChecked = false;

    if (needsExplicitAcceptance) {
      initExplicitViews();
    } else {
      initNonExplicitViews();
    }

    setListeners();
  }

  private void initExplicitViews() {
    llContainer = (LinearLayout) findViewById(R.id.llExplicitAcceptanceContainer);
    llContainer.setVisibility(VISIBLE);

    TextView purchaseWidgetTitle = (TextView) findViewById(R.id.purchaseWidgetTitle);
    purchaseWidgetTitle.setText(
        LocalizablesFacade.getString(context, OneCMSKeys.PAYMENT_PURCHASE_WIDGET_TITLE));

    cbTACAcceptance = (CheckBox) findViewById(R.id.cbTACAAcceptance);
    cbTACAcceptance.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        isExplicitAcceptanceChecked = cbTACAcceptance.isChecked();
        paymentPurchasePresenter.onExplicitAcceptanceChecked(cbTACAcceptance.isChecked());
      }
    });

    termsAndConditions = (TextView) findViewById(R.id.conditionsExplicitAcceptance);
    termsAndConditions.setText(
        LocalizablesFacade.getString(context, OneCMSKeys.PAYMENY_CONDITIONS_EXPLICIT_ACCEPTANCE));

    btnContinue = (Button) findViewById(R.id.btnExplicitAcceptanceContinue);
    btnContinue.setText(LocalizablesFacade.getString(context, OneCMSKeys.COMMON_BUTTONCONTINUE));
  }

  private void initNonExplicitViews() {
    llContainer = (LinearLayout) findViewById(R.id.llNonExplicitAcceptanceContainer);
    llContainer.setVisibility(VISIBLE);

    termsAndConditions = (TextView) findViewById(R.id.tvConditionsAcceptanceOnContinue);
    termsAndConditions.setText(
        LocalizablesFacade.getString(context, OneCMSKeys.PAYMENT_CONDITIONS_IMPLICIT_ACCEPTANCE,
            Configuration.getInstance().getBrandVisualName()));

    btnContinue = (Button) findViewById(R.id.btnNonExplicitAcceptanceContinue);
    btnContinue.setText(LocalizablesFacade.getString(context, OneCMSKeys.COMMON_BUTTONCONTINUE));
  }

  private void setListeners() {
    termsAndConditions.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        paymentPurchasePresenter.onTACClick();
      }
    });

    btnContinue.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        paymentPurchasePresenter.onContinueButtonClick();
      }
    });
  }

  @Override public void openConditionsDialog() {
    AlertDialog.Builder builder = new AlertDialog.Builder(context);
    LayoutInflater inflater = ((Activity) context).getLayoutInflater();
    View layout = inflater.inflate(R.layout.dialog_terms_and_conditions, this, false);
    builder.setView(layout);

    final String paymentConditionsTitle =
        LocalizablesFacade.getString(context, OneCMSKeys.TERMS_AND_CONDITIONS).toString();
    final String airlineConditionsTitle =
        LocalizablesFacade.getString(context, OneCMSKeys.AIRLINE_CONDITIONS).toString();

    Button paymentConditions = (Button) layout.findViewById(R.id.paymentConditions);
    paymentConditions.setText(paymentConditionsTitle);
    Button airlineConditions = (Button) layout.findViewById(R.id.airlineConditions);
    airlineConditions.setText(airlineConditionsTitle);

    final AlertDialog alertDialog = builder.show();
    paymentConditions.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        alertDialog.dismiss();
        openUrl(LocalizablesFacade.getString(getApplicationContext(),
            OneCMSKeys.ABOUTOPTIONSMODULE_ABOUT_OPTION_TERMS_URL).toString(),
            paymentConditionsTitle);
      }
    });
    airlineConditions.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        alertDialog.dismiss();
        openUrl(
            LocalizablesFacade.getString(getApplicationContext(), OneCMSKeys.ABOUT_TC_AIRLINE_URL)
                .toString(), airlineConditionsTitle);
      }
    });
  }

  private void openUrl(String url, String title) {
    if (url.contains(PDF_EXTENSION)) {
      PdfDownloader task = new PdfDownloader(context);
      task.execute(url);
    } else {
      Intent intent = new Intent(getApplicationContext(), OdigeoWebViewActivity.class);
      intent.putExtra(Constants.EXTRA_URL_WEBVIEW, url);
      intent.putExtra(Constants.TITLE_WEB_ACTIVITY, title);
      intent.putExtra(Constants.SHOW_HOME_ICON, false);
      context.startActivity(intent);
    }
  }

  @Override public void setContinueButtonEnable(boolean enable) {
    btnContinue.setEnabled(enable && (!needsExplicitAcceptance || isExplicitAcceptanceChecked));
  }
}