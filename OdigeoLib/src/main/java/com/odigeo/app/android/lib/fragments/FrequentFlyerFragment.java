package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.FrequentFlyerCardCodeDTO;
import com.odigeo.app.android.lib.ui.widgets.FrequentFlyerWidget;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Irving Lóp on 15/11/2014.
 */
public class FrequentFlyerFragment extends Fragment
    implements View.OnClickListener, FrequentFlyerWidget.FrequentFlyerListener {

  private List<FrequentFlyerWidget> frequentFlyerWidgets;

  private LinearLayout containerLayout;
  private Button anotherFlyCodeButton;

  public static FrequentFlyerFragment newInstance(List<FrequentFlyerCardCodeDTO> codeList) {
    FrequentFlyerFragment fragment = new FrequentFlyerFragment();
    Bundle bundle = new Bundle();
    bundle.putSerializable(Constants.EXTRA_FREQUENT_FLYER_LIST, (ArrayList) codeList);
    fragment.setArguments(bundle);
    return fragment;
  }

  @Override public void onAttach(Activity activity) {
    super.onAttach(activity);
    setHasOptionsMenu(true);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    frequentFlyerWidgets = new ArrayList<FrequentFlyerWidget>();

    View view = inflater.inflate(R.layout.layout_frequent_flyer_fragment, container, false);
    containerLayout = (LinearLayout) view.findViewById(R.id.layout_frequent_flyer_container);
    anotherFlyCodeButton = (Button) view.findViewById(R.id.button_frequent_flyer_another_flyer);
    anotherFlyCodeButton.setText(LocalizablesFacade.getString(getActivity(),
        "frequentflyerviewcontroller_button_anothercode").toString());
    FrequentFlyerWidget flyerWidget =
        (FrequentFlyerWidget) view.findViewById(R.id.view_frequent_flyer_widget_1);

    flyerWidget.setCarrierList(getCarriers());
    flyerWidget.hideDelete();
    frequentFlyerWidgets.add(flyerWidget);

    anotherFlyCodeButton.setOnClickListener(this);
    anotherFlyCodeButton.setTypeface(Configuration.getInstance().getFonts().getBold());
    return view;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    List<FrequentFlyerCardCodeDTO> frequentFlyerCodeList =
        //PreferencesManager.readFrequentFlyerCodes(getActivity()
        // .getApplicationContext());
        (ArrayList<FrequentFlyerCardCodeDTO>) getArguments().getSerializable(
            Constants.EXTRA_FREQUENT_FLYER_LIST);
    if (frequentFlyerCodeList != null) {
      int index = 0;
      for (FrequentFlyerCardCodeDTO flyerCardCode : frequentFlyerCodeList) {
        if (index == frequentFlyerWidgets.size()) {

          addFlyerWidget();
        }
        frequentFlyerWidgets.get(index).setFlyerCardCodeDTO(flyerCardCode);
        index++;
      }
    }
  }

  @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(R.menu.menu_frequent_flyer, menu);
    MenuItem item = menu.findItem(R.id.menu_item_frequent_flyer_saved);
    item.setTitle(
        LocalizablesFacade.getString(getActivity(), "mydatadetailsviewcontroller_save").toString());
    super.onCreateOptionsMenu(menu, inflater);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int itemId = item.getItemId();
    if (itemId == android.R.id.home) {
      showMessage();
    } else {
      if (itemId == R.id.menu_item_frequent_flyer_saved && validCodes()) {
        savedCodes();
        getActivity().finish();
      }
    }
    return super.onOptionsItemSelected(item);
  }

  private void showMessage() {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

    builder.setTitle(
        LocalizablesFacade.getString(getActivity(), "mydatadetailsviewcontroller_save").toString())
        .setMessage(LocalizablesFacade.getString(getActivity(),
            "mydatapassengersviewcontroller_discardactionsheet_title"))
        .setPositiveButton(LocalizablesFacade.getString(getActivity(), "common_buttoncontinue"),
            new DialogInterface.OnClickListener() {
              @Override public void onClick(DialogInterface dialog, int which) {
                getActivity().onBackPressed();
              }
            });
    builder.setNegativeButton(LocalizablesFacade.getString(getActivity(), "common_cancel"), null);
    builder.create().show();
  }

  private boolean validCodes() {

    for (FrequentFlyerWidget flyerWidget : frequentFlyerWidgets) {
      if (!flyerWidget.isValid()) {
        return false;
      }
    }
    return true;
  }

  private void savedCodes() {

    ArrayList<FrequentFlyerCardCodeDTO> flyerCodes = new ArrayList<FrequentFlyerCardCodeDTO>();
    for (FrequentFlyerWidget flyerWidget : frequentFlyerWidgets) {
      if (!flyerCodes.contains(flyerWidget.getFlyerCardCodeDTO())) {
        flyerCodes.add(flyerWidget.getFlyerCardCodeDTO());
      }
    }
    //PreferencesManager.saveFrequentFlyerCodes(getActivity().getApplicationContext(), flyerCodes);
    Intent intent = new Intent();
    intent.putExtra(Constants.EXTRA_FREQUENT_FLYER_LIST, flyerCodes);
    getActivity().setResult(Activity.RESULT_OK, intent);
  }

  private void addFlyerWidget() {
    FrequentFlyerWidget flyerWidget =
        new FrequentFlyerWidget(getActivity().getApplicationContext());
    flyerWidget.setCarrierList(getCarriers());
    flyerWidget.setFrequentFlyerListener(this);
    frequentFlyerWidgets.add(flyerWidget);
    containerLayout.addView(flyerWidget);
  }

  @Override public void onClick(View v) {
    if (v.equals(anotherFlyCodeButton)) {
      addFlyerWidget();
    }
  }

  private List<CarrierDTO> getCarriers() {

    return Configuration.getInstance().getCarriers();
  }

  @Override public void onClicKDeleteFlyerWidget(FrequentFlyerWidget flyerWidget) {
    containerLayout.removeView(flyerWidget);
    frequentFlyerWidgets.remove(flyerWidget);
  }
}
