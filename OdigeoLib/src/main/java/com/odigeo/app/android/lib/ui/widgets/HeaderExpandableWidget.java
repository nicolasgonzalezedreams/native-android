package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;

/**
 * Created by emiliano.desantis on 06/10/2014.
 */
public class HeaderExpandableWidget extends RelativeLayout implements View.OnClickListener {
  private boolean isOpen;
  private String labelShowMore;
  private String labelShowLess;
  private String labelSubtitle;

  private ImageView expandableImageView;
  private TextView expandableTextView;

  private View dynamicLayout;

  private HeaderExpandableWidgetListener listener;

  public HeaderExpandableWidget(Context context, View dynamicLayout) {
    super(context);
    this.dynamicLayout = dynamicLayout;
    inflateLayout();
  }

  public HeaderExpandableWidget(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray typedArray =
        context.obtainStyledAttributes(attrs, R.styleable.HeaderExpandableWidget, 0, 0);
    labelShowMore = typedArray.getString(R.styleable.HeaderExpandableWidget_labelShowMore);
    labelShowLess = typedArray.getString(R.styleable.HeaderExpandableWidget_labelShowLess);
    labelSubtitle = typedArray.getString(R.styleable.HeaderExpandableWidget_textSubtitle);
    inflateLayout();
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.layout_widget_header_expandable, this);
    expandableTextView = (CustomTextView) findViewById(R.id.text_header_expandable);
    expandableImageView = (ImageView) findViewById(R.id.image_header_expandable);
    TextView expandableSubTitleTextView =
        (TextView) findViewById(R.id.text_header_expandable_subtitle);

    isOpen = false;
    this.setOnClickListener(this);
    if (labelShowMore != null) {
      expandableTextView.setText(LocalizablesFacade.getString(getContext(), labelShowMore));
    }

    if (labelSubtitle != null) {
      expandableSubTitleTextView.setVisibility(VISIBLE);
      expandableSubTitleTextView.setText(LocalizablesFacade.getString(getContext(), labelSubtitle));
    }
    hideDynamicLayout();
  }

  @Override public final void onClick(View v) {
    isOpen = !isOpen;
    if (isOpen) {
      showDynamicLayout();
    } else {
      hideDynamicLayout();
    }
  }

  public final void hideLayoutDinamically() {
    hideDynamicLayout();
  }

  private void hideDynamicLayout() {
    if (dynamicLayout != null) {
      isOpen = false;
      dynamicLayout.setVisibility(GONE);
      expandableImageView.setImageResource(R.drawable.more_info);
      expandableTextView.setText(LocalizablesFacade.getString(getContext(), labelShowMore));
    }

    if (listener != null) {
      listener.onCollapsed();
    }
  }

  public final void showDynamicLayout() {
    if (dynamicLayout != null) {
      isOpen = true;
      dynamicLayout.setVisibility(VISIBLE);
      expandableImageView.setImageResource(R.drawable.less_info);
      expandableTextView.setText(LocalizablesFacade.getString(getContext(), labelShowLess));
    }

    if (listener != null) {
      listener.onExpanded();
    }
  }

  public final void setLabelShowMore(String labelShowMore) {
    this.labelShowMore = labelShowMore;
  }

  public final void setLabelShowLess(String labelShowLess) {
    this.labelShowLess = labelShowLess;
  }

  public final void setDynamicLayout(View dynamicLayout) {
    this.dynamicLayout = dynamicLayout;
    if (isOpen) {
      showDynamicLayout();
    } else {
      hideDynamicLayout();
    }
  }

  public final HeaderExpandableWidgetListener getListener() {
    return listener;
  }

  public final void setListener(HeaderExpandableWidgetListener listener) {
    this.listener = listener;
  }

  public interface HeaderExpandableWidgetListener {
    void onExpanded();

    void onCollapsed();
  }
}
