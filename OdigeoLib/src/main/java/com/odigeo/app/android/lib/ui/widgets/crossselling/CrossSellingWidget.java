package com.odigeo.app.android.lib.ui.widgets.crossselling;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.Section;
import com.odigeo.app.android.lib.ui.widgets.CustomTextView;
import com.odigeo.app.android.lib.ui.widgets.FormOdigeoLayout;
import com.odigeo.app.android.lib.utils.MyAsyncTask;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.data.entity.TravelType;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Cross selling widget.
 *
 * @author cristian.fanjul
 * @since 24/02/2015
 */
public class CrossSellingWidget extends LinearLayout {

  private static final float IMAGE_RADIUS = 4;
  private CustomTextView txtDestination;
  private CustomTextView fromDateToDate;
  private ImageView imageView;
  private SearchOptions searchOptions;
  private Bitmap sourceBitmap;
  private URL url;

  public CrossSellingWidget(Context context, AttributeSet attrs) {
    super(context, attrs);

    inflateLayout(context);
  }

  public CrossSellingWidget(Context context, AttributeSet attrs, SearchOptions options) {
    super(context, attrs);

    searchOptions = options;

    inflateLayout(context);

    setValues();
  }

  private void inflateLayout(Context context) {
    inflate(context, R.layout.layout_cross_selling_widget, this);
    this.setClickable(true);

    txtDestination = (CustomTextView) findViewById(R.id.txt_location);

    imageView = (ImageView) findViewById(R.id.img_cross_selling_background);
    fromDateToDate = (CustomTextView) findViewById(R.id.txt_from_date_to_date);

    hideTitleAndSubtitle();
  }

  /**
   * Is not necessary to show title and subtitle in this form.
   */
  private void hideTitleAndSubtitle() {
    FormOdigeoLayout formOdigeoLayout =
        (FormOdigeoLayout) findViewById(R.id.form_odigeo_layout_cross_selling);
    formOdigeoLayout.getTitleContainerLayout().setVisibility(View.GONE);
  }

  public final CrossSellingButton getBookHotelButton() {
    CrossSellingButton bookHotelButton =
        (CrossSellingButton) findViewById(R.id.btn_cross_selling_book_hotel);
    return bookHotelButton;
  }

  public final CrossSellingButton getRentCarButton() {
    CrossSellingButton rentCarButton =
        (CrossSellingButton) findViewById(R.id.btn_cross_selling_rent_car);
    return rentCarButton;
  }

  public final SearchOptions getSearchOptions() {
    return this.searchOptions;
  }

  public final void setSearchOptions(SearchOptions searchOptions) {
    this.searchOptions = searchOptions;
    setValues();
  }

  /**
   * Sets the values that the widget must show: city and dates.
   */
  private void setValues() {
    FlightSegment firstSegment = searchOptions.getFirstSegment();
    FlightSegment lastSegment = searchOptions.getLastSegment();

    txtDestination.setText(firstSegment.getArrivalCity().getCityName());

    if (searchOptions.getTravelType() == TravelType.MULTIDESTINATION) {
      // TODO: update when defined.
      setFlightDate(firstSegment, lastSegment);
    } else if (searchOptions.getTravelType() == TravelType.SIMPLE) {
      setSingleFlightDate(firstSegment);
    } else if (searchOptions.getTravelType() == TravelType.ROUND) {
      setFlightDate(firstSegment, lastSegment);
    }

    setImage();
  }

  /**
   * Set from date to date.
   */
  private void setFlightDate(FlightSegment firstSegment, FlightSegment lastSegment) {
    String fromDate = getDatePartFormatted(OdigeoDateUtils.createDate(firstSegment.getDate()));
    String toDate = getDatePartFormatted(OdigeoDateUtils.createDate(lastSegment.getDate()));
    fromDateToDate.setText(fromDate + " - " + toDate);
  }

  private void setSingleFlightDate(FlightSegment flightSegment) {
    String date = getDatePartFormatted(OdigeoDateUtils.createDate(flightSegment.getDate()));
    fromDateToDate.setText(date);
  }

  private String getDatePartFormatted(Date date) {
    if (date == null) {
      return "";
    }

    return OdigeoDateUtils.getGmtDateFormat(getResources().getString(R.string.templates__datelong1))
        .format(date);
  }

  private String getTimePartFormatted(Date date) {
    if (date == null) {
      return "";
    }
    //TODO: use format from class LocaleUtils
    return OdigeoDateUtils.getGmtDateFormat(
        Configuration.getInstance().getCurrentMarket().getTimeFormat()).format(date);
  }

  /**
   * An image for an iata is brought and set to the ImageView.
   */
  public final void setImage() {
    String urlImageFileName = "";

    FlightSegment firstSegment = searchOptions.getFlightSegments().get(0);
    Section lastSection = firstSegment.getSections().get(firstSegment.getSections().size() - 1);

    urlImageFileName =
        String.format("%s%s%s", Configuration.getInstance().getImagesSources().getUrlMyTrips(),
            lastSection.getLocationTo().getIataCode(),
            Configuration.getInstance().getImagesSources().getImageFileType());

    try {
      url = new URL(urlImageFileName);

      MyAsyncTask asyncTask = new MyAsyncTask();

      MyAsyncTask.DoInBackground doInBackground = new MyAsyncTask.DoInBackground() {
        @Override public void executeInBackground() {
          try {
            sourceBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
          } catch (IOException e) {
            Logger.getLogger(CrossSellingWidget.class.getName())
                .log(Level.SEVERE, Constants.TAG_LOG, e);
          }
        }
      };

      MyAsyncTask.DoOnPostExecute doOnPostExecute = new MyAsyncTask.DoOnPostExecute() {
        @Override public void postExecute() {
          if (sourceBitmap != null) {
            if (Configuration.getInstance().getBrand().equals("GoVoyages")) {
              ViewUtils.setRoundedTopCornersToBitMap(sourceBitmap, imageView, IMAGE_RADIUS);
            } else {
              imageView.setImageBitmap(sourceBitmap);
            }
          }
        }
      };
      asyncTask.setDoOnPostExecute(doOnPostExecute);
      asyncTask.execute(doInBackground);
    } catch (Exception e) {
      Logger.getLogger(CrossSellingWidget.class.getName()).log(Level.SEVERE, Constants.TAG_LOG, e);
    }
  }

  public final String getDestination() {
    return txtDestination.getText().toString();
  }

  public final void setDestination(String destination) {
    this.txtDestination.setText(destination);
  }
}
