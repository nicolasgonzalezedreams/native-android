package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

/**
 * @author miguel
 */
public class TechnicalStopDTO implements Serializable {

  protected int location;
  protected long arrivalDate;
  protected long departureDate;
  protected String equipmentType;

  /**
   * Gets the value of the location property.
   */
  public int getLocation() {
    return location;
  }

  /**
   * Sets the value of the location property.
   */
  public void setLocation(int value) {
    this.location = value;
  }

  /**
   * Gets the value of the arrivalDate property.
   *
   * @return possible object is {@link long }
   */
  public long getArrivalDate() {
    return arrivalDate;
  }

  /**
   * Sets the value of the arrivalDate property.
   *
   * @param value allowed object is {@link long }
   */
  public void setArrivalDate(long value) {
    this.arrivalDate = value;
  }

  /**
   * Gets the value of the departureDate property.
   *
   * @return possible object is {@link long }
   */
  public long getDepartureDate() {
    return departureDate;
  }

  /**
   * Sets the value of the departureDate property.
   *
   * @param value allowed object is {@link long }
   */
  public void setDepartureDate(long value) {
    this.departureDate = value;
  }

  /**
   * Gets the value of the equipmentType property.
   *
   * @return possible object is {@link String }
   */
  public String getEquipmentType() {
    return equipmentType;
  }

  /**
   * Sets the value of the equipmentType property.
   *
   * @param value allowed object is {@link String }
   */
  public void setEquipmentType(String value) {
    this.equipmentType = value;
  }
}