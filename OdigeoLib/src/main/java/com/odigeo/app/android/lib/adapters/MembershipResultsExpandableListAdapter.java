package com.odigeo.app.android.lib.adapters;

import android.app.Activity;
import android.graphics.Paint;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.interfaces.AdapterExpandableListListener;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;
import com.odigeo.app.android.lib.utils.ResultsPriceCalculator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.TravelType;
import java.util.List;

public class MembershipResultsExpandableListAdapter extends ResultsActivityExpandableListAdapter {

  private static final float TEXT_RESIZING_70 = 0.53f;

  public MembershipResultsExpandableListAdapter(Activity context,
      List<FareItineraryDTO> itineraryResults, TravelType travelType, int divider, int numSegments,
      String locale, AdapterExpandableListListener adapterExpandableListListener) {
    super(context, itineraryResults, travelType, divider, numSegments, locale,
        adapterExpandableListListener);
  }

  @Override public View inflateHeader(int groupPosition, boolean isExpanded, View convertView,
      ViewGroup parent) {
    return inflateGroupViewMembers(groupPosition, isExpanded, convertView, parent);
  }

  private View inflateGroupViewMembers(int groupPosition, boolean isExpanded, View convertView,
      ViewGroup parent) {
    View view =
        inflater.inflate(R.layout.layout_expandablelist_header_price_members, parent, false);

    itineraryContainers[groupPosition] = view;

    setItineraryPrice(groupPosition);
    setItinerarySlashedPrice(groupPosition);
    setItineraryMemberLabel(groupPosition);
    ((ExpandableListView) parent).expandGroup(groupPosition);
    return view;
  }

  private void setItineraryMemberLabel(int groupPosition) {
    View convertView = itineraryContainers[groupPosition];

    if (convertView != null) {
      TextView memberLabel = (TextView) convertView.findViewById(R.id.member_label);
      memberLabel.setText(LocalizablesFacade.getString(context,
          OneCMSKeys.RESULTSVIEWCONTROLLER_PREMIUM_PRICE_PER_PAX));
    }
  }

  private void setItinerarySlashedPrice(int groupPosition) {
    View convertView = itineraryContainers[groupPosition];
    FareItineraryDTO fareItineraryDTO = (FareItineraryDTO) getGroup(groupPosition);

    if (convertView != null && fareItineraryDTO.getMembershipPerks() != null) {
      String slashedPrice =
          ResultsPriceCalculator.calculateSlashedPrice(fareItineraryDTO, selectedMethod, divider,
              locale);

      TextView textViewSlashedPrice =
          (TextView) convertView.findViewById(R.id.flights_slashed_price);

      Spannable spannable = new SpannableString(slashedPrice);
      spannable.setSpan(new RelativeSizeSpan(TEXT_RESIZING_70), 0, slashedPrice.length(),
          Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
      textViewSlashedPrice.setText(spannable);
      textViewSlashedPrice.setPaintFlags(
          textViewSlashedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }
  }
}
