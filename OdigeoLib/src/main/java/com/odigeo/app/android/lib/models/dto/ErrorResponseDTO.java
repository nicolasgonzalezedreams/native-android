package com.odigeo.app.android.lib.models.dto;

import com.odigeo.app.android.lib.models.dto.custom.MslError;
import java.io.Serializable;

/**
 * @author miguel
 */
@Deprecated public class ErrorResponseDTO implements Serializable {

  private MslError error;

  public ErrorResponseDTO() {
  }

  public ErrorResponseDTO(MslError error) {
    this.error = error;
  }

  /**
   * @return the error
   */
  public MslError getError() {
    return error;
  }

  /**
   * @param error the error to set
   */
  public void setError(MslError error) {
    this.error = error;
  }
}
