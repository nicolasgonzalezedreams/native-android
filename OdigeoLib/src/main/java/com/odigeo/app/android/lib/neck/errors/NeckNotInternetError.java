package com.odigeo.app.android.lib.neck.errors;

import com.globant.roboneck.requests.BaseNeckRequestException;

/**
 * Created by ManuelOrtiz on 10/09/2014.
 */
public class NeckNotInternetError implements BaseNeckRequestException.Error {
  private final String message;
  private final int errorCode;
  private final int status;

  public NeckNotInternetError(String message, int errorCode, int status) {
    this.message = message;
    this.errorCode = errorCode;
    this.status = status;
  }

  @Override public final String getMessage() {
    return message;
  }

  @Override public final int getErrorCode() {
    return errorCode;
  }

  @Override public final int getStatus() {
    return status;
  }
}
