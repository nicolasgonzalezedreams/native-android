package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class SmartHubItinerary implements Serializable {

  protected Integer id;
  protected Integer itinerary;
  protected Integer rule;
  protected Integer outboundHubDefinition;
  protected Integer inboundHubDefinition;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getInboundHubDefinition() {
    return inboundHubDefinition;
  }

  public void setInboundHubDefinition(Integer inboundHubDefinition) {
    this.inboundHubDefinition = inboundHubDefinition;
  }

  public Integer getItinerary() {
    return itinerary;
  }

  public void setItinerary(Integer itinerary) {
    this.itinerary = itinerary;
  }

  public Integer getOutboundHubDefinition() {
    return outboundHubDefinition;
  }

  public void setOutboundHubDefinition(Integer outboundHubDefinition) {
    this.outboundHubDefinition = outboundHubDefinition;
  }

  public Integer getRule() {
    return rule;
  }

  public void setRule(Integer rule) {
    this.rule = rule;
  }
}
