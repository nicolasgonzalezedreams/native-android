package com.odigeo.app.android.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.navigator.AppRateFeedBackNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.presenter.AppRateThanksPresenter;
import com.odigeo.presenter.contracts.views.AppRateThanksViewInterface;

public class AppRateThanksView extends BaseView<AppRateThanksPresenter>
    implements AppRateThanksViewInterface {

  private TextView mTvAppRateThanksTitle;
  private TextView mTvAppRateThanksText;
  private TextView mBtnThanks;

  public static AppRateThanksView newInstance() {
    Bundle args = new Bundle();
    AppRateThanksView fragment = new AppRateThanksView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override protected AppRateThanksPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideAppRateThanksPresenter(this, (AppRateFeedBackNavigator) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_apprate_thanks;
  }

  @Override protected void initComponent(View view) {
    mTvAppRateThanksTitle = (TextView) view.findViewById(R.id.tv_apprate_thanks_title);
    mTvAppRateThanksText = (TextView) view.findViewById(R.id.tv_apprate_thanks_text);
    mBtnThanks = (Button) view.findViewById(R.id.btnThanks);
  }

  @Override protected void setListeners() {
    mBtnThanks.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.navigateToPlayStore();
        mPresenter.closeAppRateThanksNavigator();
        mTracker.trackAppRateThanksView();
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    mTvAppRateThanksTitle.setText(localizables.getString(OneCMSKeys.LEAVEFEEDBACK_THANKS_TITLE));
    mTvAppRateThanksText.setText(localizables.getString(OneCMSKeys.LEAVEFEEDBACK_THANKS_SUBTITLE));
    mBtnThanks.setText(localizables.getString(OneCMSKeys.LEAVEFEEDBACK_THANKS_BUTTON));
  }
}
