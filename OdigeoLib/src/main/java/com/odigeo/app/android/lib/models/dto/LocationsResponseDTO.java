package com.odigeo.app.android.lib.models.dto;

import java.util.ArrayList;
import java.util.List;

public class LocationsResponseDTO {

  protected List<LocationDescriptionDTO> cities = new ArrayList<LocationDescriptionDTO>();

  public LocationsResponseDTO() {
  }

  public List<LocationDescriptionDTO> getCities() {
    return cities;
  }

  public void setCities(List<LocationDescriptionDTO> cities) {
    this.cities = cities;
  }
}
