package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;
import com.odigeo.app.android.lib.interfaces.IInsuranceRefusalWidgetListener;

/**
 * Created by emiliano.desantis on 10/09/2014.
 */
public class InsuranceRefusalWidget extends LinearLayout implements View.OnClickListener {

  private final AppCompatCheckBox chkInsuranceRefuse;
  private IInsuranceRefusalWidgetListener listener;

  public InsuranceRefusalWidget(Context context, AttributeSet attrs) {
    super(context, attrs);

    LayoutInflater.from(context).inflate(R.layout.widget_insurance_refuse, this);

    TextView descriptionTextView = (TextView) findViewById(R.id.txtDescription);
    TextView hintTextView = (TextView) findViewById(R.id.txtHint);
    chkInsuranceRefuse = (AppCompatCheckBox) findViewById(R.id.chkInsuranceRefuse);

    Fonts fonts = Configuration.getInstance().getFonts();

    descriptionTextView.setClickable(false);
    hintTextView.setClickable(false);
    descriptionTextView.setTypeface(fonts.getBold());
    hintTextView.setTypeface(fonts.getRegular());

    chkInsuranceRefuse.setClickable(false);

    this.setClickable(true);

    findViewById(R.id.layout_insurance_refuse).setOnClickListener(this);
  }

  public final void setListener(IInsuranceRefusalWidgetListener listener) {
    this.listener = listener;
  }

  public final boolean isChecked() {
    return chkInsuranceRefuse.isChecked();
  }

  public final void setChecked(boolean checked) {
    chkInsuranceRefuse.setChecked(checked);
  }

  @Override public final void onClick(View v) {

    setChecked(!isChecked());
    if (listener != null) {
      if (isChecked()) {
        listener.onInsuranceRefusalSelected();
      } else {
        listener.onInsuranceRefusalUnselected();
      }
    }
  }
}
