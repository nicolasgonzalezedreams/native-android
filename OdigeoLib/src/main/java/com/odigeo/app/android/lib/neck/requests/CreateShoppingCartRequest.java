package com.odigeo.app.android.lib.neck.requests;

import android.util.Log;
import com.globant.roboneck.common.NeckCookie;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.models.dto.responses.CreateShoppingCartResponse;
import com.odigeo.app.android.lib.neck.requests.base.BaseOdigeoRequests;
import com.odigeo.data.net.helper.DomainHelperInterface;
import java.util.List;

/**
 * Created by ManuelOrtiz on 10/09/2014.
 */
@Deprecated //Don't use this class
public class CreateShoppingCartRequest
    extends BaseOdigeoRequests<CreateShoppingCartResponse, CreateShoppingCartResponse, Object> {

  private static final String PATH = "createShoppingCart";
  private DomainHelperInterface mDomainHelper;

  public CreateShoppingCartRequest(Class<CreateShoppingCartResponse> clazz, Object requestModel,
      List<NeckCookie> cookies, OdigeoSession odigeoSession, String deviceId,
      DomainHelperInterface domainHelper) {
    super(clazz, requestModel, cookies, odigeoSession, deviceId);
    mDomainHelper = domainHelper;
  }

  @Override protected final String getUrl() {
    String url = String.format("%s%s", mDomainHelper.getUrl(), PATH);

    return url;
  }

  @Override protected final CreateShoppingCartResponse processContent(String responseBody) {

    CreateShoppingCartResponse response = super.processContent(responseBody);

    Log.i(Constants.TAG_LOG, "CreateShoppingCart Rest Services returned " + responseBody);

    response.setCookies(getCookiesReceived());

    return response;
  }
}



