package com.odigeo.app.android.lib.config;

import java.io.Serializable;

/**
 * Created by emiliano.gudino on 20/11/2014.
 */
public class ResidentItineraryPlace implements Serializable {

  private String code;
  private boolean isCountry;

  public ResidentItineraryPlace(String code, boolean isCountry) {
    this.code = code;
    this.isCountry = isCountry;
  }

  public final boolean isCountry() {
    return isCountry;
  }

  public final void setCountry(boolean isCountry) {
    this.isCountry = isCountry;
  }

  public final String getCode() {
    return code;
  }

  public final void setCode(String code) {
    this.code = code;
  }
}
