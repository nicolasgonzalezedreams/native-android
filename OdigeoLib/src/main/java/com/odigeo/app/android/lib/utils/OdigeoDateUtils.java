package com.odigeo.app.android.lib.utils;

import android.support.annotation.NonNull;
import android.util.Log;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.data.entity.TravelType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * This class manages date manipulations.
 *
 * @deprecated use {@link #DateHelper} instead
 */
@Deprecated public final class OdigeoDateUtils {

  public static final String TIME_ZONE_GMT = "GMT";
  public static final Date ZERO_DATE = new Date(Long.MIN_VALUE);
  public static final int YEAR_IN_DAYS = 365;
  private static final String DATE_FORMAT = "HH:mm:ss MM/dd/yyyy";
  private static final String DATE_WEEK_DAY_FORMAT = "EEEE";
  private static final String DATE_TRACKER_FORMAT = "ddd-MM-yyyy";
  private static final int MINUTES = 60;

  private OdigeoDateUtils() {

  }

  /**
   * This method gets the minutes from a given date
   *
   * @param date Date to analyze
   * @return amount of minutes of the date
   */
  public static int getMinute(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    return c.get(Calendar.MINUTE);
  }

  /**
   * This method gets the hours from a given date
   *
   * @param date Date to analyze
   * @return amount of hours of the date
   */
  public static int getHour(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    return c.get(Calendar.HOUR_OF_DAY);
  }

  /**
   * This method gets the day of the month from a given date
   *
   * @param date Date to analyze
   * @return day of the month of the date
   */
  public static int getDayOfMonth(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    return c.get(Calendar.DAY_OF_MONTH);
  }

  /**
   * This method gets the month from a given date
   *
   * @param date Date to analyze
   * @return month of the date
   */
  public static int getMonth(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    return c.get(Calendar.MONTH) + 1;
  }

  /**
   * This method gets the year from a given date
   *
   * @param date Date to analyze
   * @return year of the date
   */
  public static int getYear(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    return c.get(Calendar.YEAR);
  }

  /**
   * This method adds an amount of days to a date
   *
   * @param date date where the days will be added
   * @param days amount of days to add
   * @return date with the added days
   */
  public static Date addDaysToDate(Date date, int days) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.add(Calendar.DATE, days);
    return (Date) cal.getTime();
  }

  /**
   * Eliminate the time part to a Date object
   *
   * @param dateMilliseconds The time in milliseconds to create a date
   * @return Return the Date object, for those milliseconds
   */
  public static Calendar createCalendar(long dateMilliseconds) {

    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(dateMilliseconds);
    return calendar;
  }

  /**
   * Creates a Date instance from given milliseconds
   *
   * @param dateMilliseconds given time in milliseconds value
   * @return date
   */
  public static Date createDate(long dateMilliseconds) {
    if (dateMilliseconds != 0) {
      Calendar calendar = Calendar.getInstance();
      calendar.setTimeInMillis(dateMilliseconds);
      return calendar.getTime();
    } else {
      return null;
    }
  }

  /**
   * Parse a given date to milliseconds, uses pattern MMM dd, yyyy HH:mm:ss a
   *
   * @param dateString is the date saved with the pattern MMM dd, yyyy HH:mm:ss a
   * @return time in milliseconds
   */

  public static long parseDate(String dateString) {
    SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss aa", Locale.US);
    SimpleDateFormat format24 = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss", Locale.US);
    Date dateParsed = null;
    if ("0".equals(dateString)) {
      return 0L;
    }
    try {
      dateParsed = format.parse(dateString);
      return dateParsed.getTime();
    } catch (ParseException exceptionAMPM) {
      try {
        dateParsed = format24.parse(dateString);
        return dateParsed.getTime();
      } catch (ParseException exception24H) {
        Log.e("OdigeoDateUtils", exception24H.getMessage());
        return 0L;
      }
    }
  }

  /**
   * Eliminate the time part to a Date object
   *
   * @param date The date to reset its time
   * @return Return the Date object, without time, only the date part
   */
  @NonNull public static Date resetTimeToDate(@NonNull Date date) {
    final Calendar dateWithoutTime = Calendar.getInstance();

    dateWithoutTime.setTime(date);
    dateWithoutTime.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
    dateWithoutTime.set(Calendar.MINUTE, 0);                 // set minute in hour
    dateWithoutTime.set(Calendar.SECOND, 0);                 // set second in minute
    dateWithoutTime.set(Calendar.MILLISECOND, 0);            // set millis in second

    return dateWithoutTime.getTime();
  }

  /**
   * Eliminate the time part to a Calendar object
   *
   * @param date The date to reset its time
   * @return Return the Calendar object, without time, only the date part
   */
  public static Calendar resetTimeToDate(Calendar date) {
    Date dateWithoutTime = resetTimeToDate(date.getTime());

    Calendar newCalendar = Calendar.getInstance();
    newCalendar.setTime(dateWithoutTime);

    return newCalendar;
  }

  /**
   * Execute a compare dates, but ignoring the time
   *
   * @return 0 if the argument Date1 is equal to Date2; a value less than 0 if this Date1 is before
   * the Date2; and a value greater than 0
   * if Date1 is after the Date2.
   */
  public static int compareDatesWithoutTime(Date date1, Date date2) {
    Date dateWithoutTime1 = resetTimeToDate(date1);
    Date dateWithoutTime2 = resetTimeToDate(date2);

    return dateWithoutTime1.compareTo(dateWithoutTime2);
  }

  /**
   * This method returns a Date object given the day, month and year
   *
   * @param day Day for the date
   * @param month Month for the date
   * @param year Year for the date
   * @return Date object with the previous parameters
   */
  public static Date getDateFromNumbers(int day, int month, int year) {
    if (day > 0 && month > 0 && year > 0) {
      Calendar calendar = Calendar.getInstance();
      calendar.set(Calendar.DATE, day);
      calendar.set(Calendar.MONTH, month - 1);
      calendar.set(Calendar.YEAR, year);

      return calendar.getTime();
    }

    return null;
  }

  /**
   * This method calculates the age of a person with the date of birth and the target date
   *
   * @param dateOfBirth Date of birth of the passenger
   * @param dateTarget Target date to calculate the age
   * @return Age of the passenger
   */
  public static int getAgeInDate(Calendar dateOfBirth, Calendar dateTarget) {
    if (dateOfBirth.after(dateTarget)) {
      throw new IllegalArgumentException("Can't be born in the future");
    }

    int year1 = dateTarget.get(Calendar.YEAR);
    int year2 = dateOfBirth.get(Calendar.YEAR);

    int age = year1 - year2;

    int month1 = dateTarget.get(Calendar.MONTH);
    int month2 = dateOfBirth.get(Calendar.MONTH);

    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = dateTarget.get(Calendar.DAY_OF_MONTH);
      int day2 = dateOfBirth.get(Calendar.DAY_OF_MONTH);
      if (day2 > day1) {
        age--;
      }
    }

    return age;
  }

  /**
   * This method calculates the age of a person with the date of birth and the target date
   *
   * @param dateOfBirth Date of birth of the passenger
   * @param dateTarget Target date to calculate the age
   * @return Age of the passenger
   */
  public static int getAgeInDate(Date dateOfBirth, Date dateTarget) {
    Calendar calendarDateOfBirth = Calendar.getInstance();
    calendarDateOfBirth.setTime(dateOfBirth);

    Calendar calendarDateTarget = Calendar.getInstance();
    calendarDateTarget.setTime(dateTarget);

    return getAgeInDate(calendarDateOfBirth, calendarDateTarget);
  }

  /**
   * This method calculates the age of a person with the date of birth and the target date
   *
   * @param dateOfBirth Date of birth of the passenger
   * @param dateTarget Target date to calculate the age
   * @return Age of the passenger
   */
  public static int getAgeInDate(Date dateOfBirth, Calendar dateTarget) {
    Calendar calendarDateOfBirth = Calendar.getInstance();
    calendarDateOfBirth.setTime(dateOfBirth);

    return getAgeInDate(calendarDateOfBirth, dateTarget);
  }

  /**
   * This method calculates the age of a person with the date of birth and the target date
   *
   * @param dateOfBirth Date of birth of the passenger
   * @param dateTarget Target date to calculate the age
   * @return Age of the passenger
   */
  public static int getAgeInDate(Calendar dateOfBirth, Date dateTarget) {
    Calendar calendarDateTarget = Calendar.getInstance();
    calendarDateTarget.setTime(dateTarget);

    return getAgeInDate(dateOfBirth, calendarDateTarget);
  }

  /**
   * Get the simpleDateFormat object, with the current Market locale, and TIME_ZONE_GMT time zone
   *
   * @param format Format to apply to a date
   * @return The SimpleDateFormat object, with the current market locale set
   */
  @Deprecated public static SimpleDateFormat getGmtDateFormat(String format) {
    SimpleDateFormat result = new SimpleDateFormat(format, LocaleUtils.getCurrentLocale());
    result.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_GMT));
    return result;
  }

  public static SimpleDateFormat getGmtDateFormat(String format, String timeZone) {
    SimpleDateFormat result = new SimpleDateFormat(format);
    result.setTimeZone(TimeZone.getTimeZone(timeZone));
    return result;
  }

  /**
   * This method returns the formatted time given an amount of minutes
   *
   * @param totalMinuts minutes to be calculated
   * @return Formatted time
   */
  public static String getTimeFormat(int totalMinuts) {
    int hours = totalMinuts / MINUTES;
    int minuts = totalMinuts % MINUTES;
    return String.format("%02dh%02d'", hours, minuts);
  }

  public static int calculateTripDuration(long departureDate, long arrivalDate) {
    long diff = arrivalDate - departureDate;
    return (int) diff / (24 * 60 * 60 * 1000);
  }

  public static int getDifferenceDays(long departure, long arrival) {
    Calendar departureDate = Calendar.getInstance();
    Calendar arrivalDate = Calendar.getInstance();
    departureDate.setTimeInMillis(departure);
    arrivalDate.setTimeInMillis(arrival);
    departureDate.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_GMT));
    arrivalDate.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_GMT));
    if (arrivalDate.get(Calendar.DAY_OF_YEAR) >= departureDate.get(Calendar.DAY_OF_YEAR)) {
      return arrivalDate.get(Calendar.DAY_OF_YEAR) - departureDate.get(Calendar.DAY_OF_YEAR);
    } else {
      return (arrivalDate.get(Calendar.DAY_OF_YEAR) + YEAR_IN_DAYS) - departureDate.get(
          Calendar.DAY_OF_YEAR);
    }
  }

  /**
   * This method checks if the trip starts and ends on the same day
   *
   * @param searchOptions Search options with the information about the search, including trip
   * dates
   * @return true if the trip starts and ends on the same day
   */
  public static boolean isOneDayTrip(SearchOptions searchOptions) {
    if (searchOptions.getTravelType().equals(TravelType.SIMPLE)) {
      return false;
    }
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    Date departureDate = OdigeoDateUtils.createDate(searchOptions.getFirstSegment().getDate());
    Date arrivalDate = OdigeoDateUtils.createDate(searchOptions.getLastSegment().getDate());
    String departureDay = dateFormat.format(departureDate);
    String arrivalDay = dateFormat.format(arrivalDate);
    return departureDay.equals(arrivalDay);
  }
}
