package com.odigeo.app.android.view.textwatchers.OnFocusChange;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.LinearLayout;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.tracker.TrackerConstants;

import static android.view.View.GONE;

public class CVVOnFocusChange extends BaseOnFocusChange {

  private TextInputLayout mTilCVV;
  private TrackerControllerInterface mTracker;
  private boolean mThereWasFocusAtLeastOnce = false;
  private LinearLayout mLlCVVTooltip;

  public CVVOnFocusChange(Context context, TextInputLayout textInputLayout,
      FieldValidator fieldValidator, String oneCMSKey,
      TrackerControllerInterface trackerControllerInterface, LinearLayout llCVVTooltip) {
    super(context, textInputLayout, fieldValidator, oneCMSKey);
    mTilCVV = textInputLayout;
    mTracker = trackerControllerInterface;
    mLlCVVTooltip = llCVVTooltip;
  }

  @Override public void onFocusChange(View v, boolean hasFocus) {
    super.onFocusChange(v, hasFocus);
    boolean isFieldCorrect = super.getIsFieldCorrect();
    if (!hasFocus) mLlCVVTooltip.setVisibility(GONE);

    if (!isFieldCorrect && mTilCVV.getEditText() != null) {
      String fieldInformation = mTilCVV.getEditText().getText().toString();
      if (fieldInformation.isEmpty()) {
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
            TrackerConstants.ACTION_PAYMENT_DETAILS_ERROR, TrackerConstants.LABEL_CVV_MISSING);
      } else {
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
            TrackerConstants.ACTION_PAYMENT_DETAILS_ERROR, TrackerConstants.LABEL_CVV_INVALID);
      }
    }
    if (hasFocus) mThereWasFocusAtLeastOnce = true;
  }

  public boolean getThereWasFocusAtLeastOnce() {
    return mThereWasFocusAtLeastOnce;
  }
}
