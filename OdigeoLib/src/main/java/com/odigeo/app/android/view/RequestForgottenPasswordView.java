package com.odigeo.app.android.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.navigator.RequestForgottenPasswordNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DialogHelper;
import com.odigeo.app.android.view.interfaces.AuthDialogActionInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.RequestForgottenPasswordPresenter;
import com.odigeo.presenter.contracts.views.RequestForgottenPasswordViewInterface;

public class RequestForgottenPasswordView extends BaseView<RequestForgottenPasswordPresenter>
    implements RequestForgottenPasswordViewInterface {

  private static final String USER_EMAIL = "USER_EMAIL";
  private Button mButtonRecoverForgottenPassword;
  private EditText mTxtRecoverMail;
  TextWatcher onTextChanged = new TextWatcher() {
    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
      mPresenter.validateUsernameFormat(mTxtRecoverMail.getText().toString());
    }

    @Override public void afterTextChanged(Editable s) {

    }
  };
  private DialogHelper mDialogHelper;
  private TextView tvInstructions;
  private String mErrorUsernameIncorrect;
  private String mErrorUsernameEmpty;
  private TextInputLayout mTilRecoverMail;
  private String mErrorText;

  public static RequestForgottenPasswordView newInstance(String email) {
    RequestForgottenPasswordView view = new RequestForgottenPasswordView();
    Bundle args = new Bundle();
    args.putString(USER_EMAIL, email);
    view.setArguments(args);

    return view;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override public void onResume() {
    super.onResume();
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_LOGIN_PASSWORD_RECOVERY);
  }

  @Override protected RequestForgottenPasswordPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideRequestForgottenPasswordPresenter(this,
            (RequestForgottenPasswordNavigator) getActivity());
  }

  private String getEmail() {
    Bundle args = this.getArguments();
    String email = "";
    if (args != null) {
      email = args.getString(USER_EMAIL);
    }
    return email;
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_password_recovery;
  }

  @Override protected void initComponent(View view) {
    mButtonRecoverForgottenPassword = (Button) view.findViewById(R.id.btnRecoverPassword);
    mTxtRecoverMail = (EditText) view.findViewById(R.id.txtRecoverMail);
    mTilRecoverMail = (TextInputLayout) view.findViewById(R.id.tilRecoverMail);
    tvInstructions = (TextView) view.findViewById(R.id.txt_vw_reset_pass_paragraph1);
    mTxtRecoverMail.setText(getEmail());
    mDialogHelper = new DialogHelper(getActivity());
    mPresenter.validateUsernameFormat(mTxtRecoverMail.getText().toString());
  }

  @Override protected void setListeners() {
    mButtonRecoverForgottenPassword.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        String email = mTxtRecoverMail.getText().toString();
        mPresenter.requestForgottenPassword(email);
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_SSO_PWD_RECOVERY,
            TrackerConstants.ACTION_PWD_RECOVERY, TrackerConstants.LABEL_SEND_INSTRUCTIONS);
      }
    });
    mTxtRecoverMail.addTextChangedListener(onTextChanged);
  }

  @Override protected void initOneCMSText(View view) {
    mTilRecoverMail.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_FORM_SHADOWTEXT_EMAIL));
    tvInstructions.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_FORM_INSTRUCTIONSTEXT_EMAIL));
    mButtonRecoverForgottenPassword.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_PASSWORDRECOVERY_FORGET_BUTTON));
    mErrorUsernameIncorrect =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ERROR_USERNAME_INCORRECT)
            .toString();
    mErrorUsernameEmpty =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ERROR_USERNAME_EMPTY).toString();
    mErrorText =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.VALIDATION_ERROR_MAIL).toString();
  }

  @Override public void enableButton(boolean isEnabled) {
    mButtonRecoverForgottenPassword.setEnabled(isEnabled);
  }

  @Override public void setUsernameError() {
    mTxtRecoverMail.setError(mErrorUsernameIncorrect);
  }

  @Override public void setUsernameEmpty() {
    mTxtRecoverMail.setError(mErrorUsernameEmpty);
  }

  @Override public void showProgress(String email) {
    CharSequence charSequence = LocalizablesFacade.getString(getActivity(),
        OneCMSKeys.SSO_RECOVERPASSWORD_SENDING_INSTRUCTIONS, email);
    mDialogHelper.showDialog(charSequence);
  }

  public void showAnError(String error) {
    mDialogHelper.showErrorDialog(getActivity(), error);
  }

  @Override public void showUserDoesNotExistError() {
    String userDoesNotExistError = LocalizablesFacade.getString(getActivity(),
        OneCMSKeys.SSO_ERROR_EMAIL_NOT_REGISTER_OR_ACTIVE).toString();
    showAnError(userDoesNotExistError);
  }

  @Override public void showUnknownError() {
    String userDoesNotExistError =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ERROR_WRONG).toString();
    showAnError(userDoesNotExistError);
  }

  @Override public void showConnectionErrorTryFacebookOrGooglePlus(String email) {
    mDialogHelper.showErrorDialog(getActivity(), LocalizablesFacade.getString(getActivity(),
        OneCMSKeys.SSO_ERROR_RECOVERY_PASSWORD_FAIL_TRY_FACEBOOK_GOOGLE, email).toString());
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_SSO_PWD_RECOVERY,
        TrackerConstants.ACTION_PWD_RECOVERY_EMAIL,
        TrackerConstants.LABEL_SSO_NOT_ODIGEO_ACCOUNT_ERROR);
  }

  @Override public void hideProgress() {
    mDialogHelper.hideDialog();
  }

  @Override public void showAccountInactiveError(String email) {
    AlertDialog.Builder pendingMailDialog = new AlertDialog.Builder(getActivity());
    pendingMailDialog.setTitle(LocalizablesFacade.getString(getActivity(),
        OneCMSKeys.SSO_ERROR_ACCOUNT_NOT_ACTIVATED_TITLE));
    pendingMailDialog.setMessage(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ACCOUNT_NOT_ACTIVATED_MSG, email)
            .toString());
    pendingMailDialog.setPositiveButton(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_GO_TO_MAIL),
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_APP_EMAIL);
            startActivity(Intent.createChooser(intent,
                LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_GO_TO_MAIL)));
          }
        });
    pendingMailDialog.setNegativeButton(android.R.string.cancel,
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
          }
        });
    pendingMailDialog.show();
  }

  @Override public void showEmailFormatError(boolean isValid) {
    if (!isValid) {
      mTilRecoverMail.setError(mErrorText);
    } else {
      mTilRecoverMail.setError(null);
    }
  }

  @Override public void showAuthError() {
    mDialogHelper.showAuthErrorDialog(this, new AuthDialogActionInterface() {
      @Override public void OnAuthDialogOK() {
        mPresenter.onAuthOkClicked();
      }
    });
  }

  @Override public void onLogoutError(MslError error, String message) {
    Log.e(error.name(), message);
  }
}
