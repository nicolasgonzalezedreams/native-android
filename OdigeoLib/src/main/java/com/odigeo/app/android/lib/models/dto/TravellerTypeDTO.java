package com.odigeo.app.android.lib.models.dto;

import android.support.annotation.Nullable;
import com.odigeo.data.entity.BaseSpinnerItem;
import java.io.Serializable;

public enum TravellerTypeDTO implements Serializable, BaseSpinnerItem {

  ADULT("mydatapassengercell_type_adult"), CHILD("mydatapassengercell_type_child"), INFANT(
      "mydatapassengercell_type_infant");

  private final String stringKey;

  TravellerTypeDTO(String resourceId) {
    this.stringKey = resourceId;
  }

  public static TravellerTypeDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

  public String getStringKey() {
    return stringKey;
  }

  @Override public String getShownTextKey() {
    return stringKey;
  }

  @Override public int getImageId() {
    return BaseSpinnerItem.EMPTY_RESOURCE;
  }

  @Nullable @Override public String getShownText() {
    return null;
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public Object getData() {
    return null;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }
}
