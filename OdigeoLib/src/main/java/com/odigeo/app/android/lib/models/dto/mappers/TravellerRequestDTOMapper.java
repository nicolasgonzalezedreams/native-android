package com.odigeo.app.android.lib.models.dto.mappers;

import com.odigeo.app.android.lib.models.dto.FrequentFlyerCardCodeDTO;
import com.odigeo.app.android.lib.models.dto.RequiredFieldDTO;
import com.odigeo.app.android.lib.models.dto.TravellerIdentificationTypeDTO;
import com.odigeo.app.android.lib.models.dto.TravellerInformationDescriptionDTO;
import com.odigeo.app.android.lib.models.dto.TravellerRequestDTO;
import com.odigeo.app.android.lib.models.dto.TravellerTitleDTO;
import com.odigeo.app.android.lib.models.dto.TravellerTypeDTO;
import com.odigeo.data.entity.extensions.UIUserTraveller;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Julio Kun on 29/09/2015.
 */
@Deprecated public class TravellerRequestDTOMapper {

  public TravellerRequestDTO getTraveller(UIUserTraveller userTraveller,
      TravellerInformationDescriptionDTO travellerInformationDescriptionDTO,
      UIUserTraveller buyer) {
    TravellerRequestDTO travellerRequestDTO = generateTravellerRequestDTO(userTraveller, buyer);
    if (travellerInformationDescriptionDTO.getNeedsIdentification()
        == RequiredFieldDTO.NOT_REQUIRED) {
      travellerRequestDTO.setIdentificationTypeName(null);
      travellerRequestDTO.setIdentification(null);
    }
    return travellerRequestDTO;
  }

  private TravellerRequestDTO generateTravellerRequestDTO(UIUserTraveller userTraveller,
      UIUserTraveller buyer) {

    TravellerRequestDTO travellerRequestDTO = new TravellerRequestDTO();
    travellerRequestDTO.setTravellerTypeName(
        convertTravellerType(userTraveller.getTypeOfTraveller()).value());
    travellerRequestDTO.setFrequentFlyerAirlineCodes(getFrequentFlyerAirlineCodes(userTraveller));

    UserProfile userProfile = userTraveller.getUserProfile();

    if (userProfile != null) {

      travellerRequestDTO.setTitleName(getTitle(userProfile.getTitle()).name());
      travellerRequestDTO.setName(userProfile.getName());
      travellerRequestDTO.setFirstLastName(userProfile.getFirstLastName());
      travellerRequestDTO.setSecondLastName(userProfile.getSecondLastName());
      travellerRequestDTO.setGender(userProfile.getGender());

      travellerRequestDTO.setCountryCodeOfResidence(userProfile.getNationalityCountryCode());
      travellerRequestDTO.setNationalityCountryCode(userProfile.getNationalityCountryCode());

      Calendar dateOfBirth = Calendar.getInstance();
      dateOfBirth.setTime(new Date(userProfile.getBirthDate()));
      travellerRequestDTO.setYearOfBirth(dateOfBirth.get(Calendar.YEAR));
      travellerRequestDTO.setMonthOfBirth(dateOfBirth.get(Calendar.MONTH) + 1);
      travellerRequestDTO.setDayOfBirth(dateOfBirth.get(Calendar.DATE));

      //Todo: must be changed with the information provided in the UIUserTraveller as agreed.
      travellerRequestDTO.setIdentification(userTraveller.getIdentification());
      if (userTraveller.getTravellerIdentificationType() != null
          && userTraveller.getIdentification() != null
          && !userTraveller.getIdentification().isEmpty()) {

        travellerRequestDTO.setIdentificationTypeName(
            userTraveller.getTravellerIdentificationType().value());

        // TODO: temporal fix, we have to see how to get this data
        if (userProfile.getNationalityCountryCode() != null
            && !userProfile.getNationalityCountryCode().isEmpty()) {
          travellerRequestDTO.setIdentificationIssueContryCode(
              userProfile.getNationalityCountryCode());
        } else if (buyer != null
            && buyer.getUserProfile() != null
            && buyer.getUserProfile().getUserAddress() != null
            && buyer.getUserProfile().getUserAddress().getCountry() != null) {

          travellerRequestDTO.setCountryCodeOfResidence(
              buyer.getUserProfile().getUserAddress().getCountry());
          travellerRequestDTO.setNationalityCountryCode(
              buyer.getUserProfile().getUserAddress().getCountry());
          travellerRequestDTO.setIdentificationIssueContryCode(
              buyer.getUserProfile().getUserAddress().getCountry());
        }

        travellerRequestDTO.setIdentificationExpirationDay(1);
        travellerRequestDTO.setIdentificationExpirationMonth(1);
        travellerRequestDTO.setIdentificationExpirationYear(2020);
      }
    }
    travellerRequestDTO.setBaggageSelections(userTraveller.getBaggageList());

    return travellerRequestDTO;
  }

  private TravellerTitleDTO getTitle(UserProfile.Title title) {
    if (UserProfile.Title.MR.equals(title)) {
      return TravellerTitleDTO.MR;
    }
    if (UserProfile.Title.MRS.equals(title)) {
      return TravellerTitleDTO.MRS;
    }
    if (UserProfile.Title.MS.equals(title)) {
      return TravellerTitleDTO.MS;
    }

    return TravellerTitleDTO.MR;
  }

  private TravellerTypeDTO convertTravellerType(UserTraveller.TypeOfTraveller typeOfTraveller) {
    if (typeOfTraveller.equals(UserTraveller.TypeOfTraveller.CHILD)) {
      return TravellerTypeDTO.CHILD;
    }
    if (typeOfTraveller.equals(UserTraveller.TypeOfTraveller.ADULT)) {
      return TravellerTypeDTO.ADULT;
    }
    if (typeOfTraveller.equals(UserTraveller.TypeOfTraveller.INFANT)) {
      return TravellerTypeDTO.INFANT;
    }
    return TravellerTypeDTO.ADULT;
  }

  private List<FrequentFlyerCardCodeDTO> getFrequentFlyerAirlineCodes(UserTraveller userTraveller) {
    List<FrequentFlyerCardCodeDTO> list = new ArrayList<>();
    if (userTraveller.getUserFrequentFlyers() != null) {
      for (UserFrequentFlyer frequentFlyer : userTraveller.getUserFrequentFlyers()) {
        FrequentFlyerCardCodeDTO frequentFlyerCardCodeDTO = new FrequentFlyerCardCodeDTO();
        frequentFlyerCardCodeDTO.setCarrierCode(frequentFlyer.getAirlineCode());
        frequentFlyerCardCodeDTO.setPassengerCardNumber(frequentFlyer.getFrequentFlyerNumber());
        list.add(frequentFlyerCardCodeDTO);
      }
    }
    return list;
  }

  private TravellerIdentificationTypeDTO convertUserIdentification(
      UserIdentification.IdentificationType userType) {

    if (userType.equals(UserIdentification.IdentificationType.BIRTH_DATE)) {
      return TravellerIdentificationTypeDTO.BIRTH_DATE;
    }
    if (userType.equals(UserIdentification.IdentificationType.PASSPORT)) {
      return TravellerIdentificationTypeDTO.PASSPORT;
    }
    if (userType.equals(UserIdentification.IdentificationType.NIE)) {
      return TravellerIdentificationTypeDTO.NIE;
    }
    if (userType.equals(UserIdentification.IdentificationType.NIF)) {
      return TravellerIdentificationTypeDTO.NIF;
    }
    if (userType.equals(UserIdentification.IdentificationType.NATIONAL_ID_CARD)) {
      return TravellerIdentificationTypeDTO.NATIONAL_ID_CARD;
    }

    return TravellerIdentificationTypeDTO.EMPTY;
  }
}
