package com.odigeo.app.android.lib.pojos.banktransfer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.CustomTextView;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.StyledBoldString;
import com.odigeo.data.entity.shoppingCart.BankTransferData;
import java.lang.ref.WeakReference;
import java.math.BigDecimal;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @author Oscar Álvarez
 * @version 1.0
 * @since 30/04/15
 */
public class BanktransferFirstStep extends IncompleteBookingBean {
  private final BankTransferData bankTransferData;
  private final BigDecimal totalValue;

  public BanktransferFirstStep(@NonNull BankTransferData bankTransferData,
      @NonNull BigDecimal totalValue) {
    this.bankTransferData = bankTransferData;
    this.totalValue = totalValue;
  }

  @Override public View inflate(@NonNull Context context, @NonNull ViewGroup parent) {
    View view = LayoutInflater.from(context)
        .inflate(R.layout.first_step_banktransfer_booking, parent, false);

    CustomTextView header =
        ((CustomTextView) view.findViewById(R.id.txt_step1_booking_banktransfer_header));

    String rawText = LocalizablesFacade.getRawString(context,
        "confirmation_howtocompletebooking_deposit_dothetransfer");

    CharSequence headerText =
        String.format(rawText, LocaleUtils.getLocalizedCurrencyValue(totalValue.doubleValue()));

    Spannable spannable = new StyledBoldString(new WeakReference<>(context), headerText.toString(),
        R.style.bankTransferBlueBoldText).getSpannable();
    header.setText(spannable, TextView.BufferType.SPANNABLE);

    CustomTextView bank =
        ((CustomTextView) view.findViewById(R.id.txt_step1_booking_banktransfer_bank));
    bank.setText(bankTransferData.getBankName());

    CustomTextView accountNumber =
        ((CustomTextView) view.findViewById(R.id.txt_step1_booking_banktransfer_account_number));
    accountNumber.setText(bankTransferData.getAccountNumber());

    CustomTextView iban =
        ((CustomTextView) view.findViewById(R.id.txt_step1_booking_banktransfer_iban));
    iban.setText(bankTransferData.getIban());

    CustomTextView swift =
        ((CustomTextView) view.findViewById(R.id.txt_step1_booking_banktransfer_swift));
    swift.setText(bankTransferData.getSwiftCode());

    CustomTextView holder =
        ((CustomTextView) view.findViewById(R.id.txt_step1_booking_banktransfer_holder));
    holder.setText(bankTransferData.getAccountHolder());

    CustomTextView reason =
        ((CustomTextView) view.findViewById(R.id.txt_step1_booking_banktransfer_reason));

    CharSequence orderId = LocalizablesFacade.getString(context,
        "confirmation_howtocompletebooking_deposit_bookingreason", bankTransferData.getOrderId());
    reason.setText(orderId);
    return view;
  }
}
