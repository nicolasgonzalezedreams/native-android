package com.odigeo.app.android.view.components;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.navigator.FlightDetailsNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.FontHelper;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.tools.DateHelperInterface;
import com.odigeo.tools.DurationFormatter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class NextFlightProvider {

  private static final int TIME_TO_SHOW_BOOKING_AFTER_LANDING = 30;

  private final Activity activity;
  private final TrackerControllerInterface tracker;
  private final Booking booking;
  private final DateHelperInterface dateHelper;
  private final DurationFormatter durationFormatter;
  private final LocalizableProvider localizableProvider;
  private View mView;
  private Button mBtnSeeFlightDetails;
  private TextView mTvNextFlightTitle;
  private TextView mTvBookingDeparture;
  private TextView mTvBookingDepartureDate;
  private TextView mTvBookingArrival;
  private TextView mTvBookingArrivalDate;
  private TextView mTvDuration;
  private TextView mTvDepartureTitle;
  private TextView mTvArrivalTitle;

  public NextFlightProvider(Activity activity, Booking booking, TrackerControllerInterface tracker,
      DateHelperInterface dateHelper, DurationFormatter durationFormatter,
      LocalizableProvider localizableProvider) {
    this.activity = activity;
    this.booking = booking;
    this.dateHelper = dateHelper;
    this.tracker = tracker;
    this.durationFormatter = durationFormatter;
    this.localizableProvider = localizableProvider;
  }

  private void initView(ViewGroup parent) {
    mView = activity.getLayoutInflater().inflate(R.layout.next_flight, parent, false);
    mBtnSeeFlightDetails = (Button) mView.findViewById(R.id.btnSeeFlightDetails);
    mTvNextFlightTitle = (TextView) mView.findViewById(R.id.tvNextFlightTitle);
    mTvBookingDeparture = (TextView) mView.findViewById(R.id.tvBookingDeparture);
    mTvBookingDepartureDate = (TextView) mView.findViewById(R.id.tvBookingDepartureDate);
    mTvBookingArrival = (TextView) mView.findViewById(R.id.tvBookingArrival);
    mTvBookingArrivalDate = (TextView) mView.findViewById(R.id.tvBookingArrivalDate);
    mTvDuration = (TextView) mView.findViewById(R.id.tvDuration);
    mTvDepartureTitle = (TextView) mView.findViewById(R.id.tvDepartureTitle);
    mTvArrivalTitle = (TextView) mView.findViewById(R.id.tvArrivalTitle);

    initOneCMSText();
  }

  public View getNextFlight(ViewGroup parent) {
    initView(parent);
    setNextBooking();
    setListeners();
    return mView;
  }

  private void setListeners() {
    mBtnSeeFlightDetails.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_TRIPS_TRIP_INFO,
            TrackerConstants.ACTION_TRIP_DETAILS, TrackerConstants.LABEL_OPEN_FLIGHT_SUMMARY);

        Intent intent = new Intent(activity.getApplicationContext(), FlightDetailsNavigator.class);
        intent.putExtra(FlightDetailsNavigator.EXTRA_BOOKING, booking);

        activity.startActivity(intent);
      }
    });
  }

  private void setNextBooking() {
    long departureDate;
    String departureAiport;

    long arrivalDate;
    String arrivalAiport;

    long flightDuration;
    String flightType;
    long offsetAfterLanding = TimeUnit.MINUTES.toMillis(TIME_TO_SHOW_BOOKING_AFTER_LANDING);
    String flightCardOneScale =
        localizableProvider.getString(OneCMSKeys.SCALES_FILTER_VIEW_CONTROLLER_1_SCALES);
    String flightCardScale = localizableProvider.getString(OneCMSKeys.MY_TRIPS_FLIGHT_CARD_SCALES);
    String flightCardDirect = localizableProvider.getString(OneCMSKeys.MY_TRIPS_FLIGHT_CARD_DIRECT);

    int index = 0;

    Section fromSectionToShow, toSectionToShow;

    do {
      fromSectionToShow = booking.getSegments().get(index).getSectionsList().get(0);
      departureDate = getDepartureDateFromSection(fromSectionToShow);

      int sections = booking.getSegments().get(index).getSectionsList().size();
      toSectionToShow = booking.getSegments().get(index).getSectionsList().get(sections - 1);
      arrivalDate = getArrivalDateFromSection(toSectionToShow);

      departureAiport = fromSectionToShow.getFrom().getLocationCode();
      arrivalAiport = booking.getSegments()
          .get(index)
          .getSectionsList()
          .get(sections - 1)
          .getTo()
          .getLocationCode();

      flightDuration = booking.getSegments().get(index).getDuration();

      //show text either DIRECT / 1 SCALE / X SCALES
      if (sections > 2) {
        flightType = (sections - 1) + " " + flightCardScale;
      } else if (sections == 2) {
        flightType = flightCardOneScale;
      } else {
        flightType = flightCardDirect;
      }

      index++;
    } while ((arrivalDate + offsetAfterLanding) < Calendar.getInstance().getTimeInMillis()
        && index < booking.getSegments().size());
    addNextFlight(departureDate, departureAiport, arrivalDate, arrivalAiport, flightDuration,
        flightType);
  }

  private long getDepartureDateFromSection(Section section) {
    long departureDate = section.getDepartureDate();

    if (section.getFlightStats() != null && section.getFlightStats().getDepartureTime() != 0) {
      try {
        departureDate = section.getFlightStats().getDepartureTime();
      } catch (NumberFormatException e) {
        //Nothing
        Log.e(this.getClass().getName(), e.getMessage());
      }
    }

    return departureDate;
  }

  private long getArrivalDateFromSection(Section section) {
    long arrivalDate = section.getArrivalDate();

    if (section.getFlightStats() != null && section.getFlightStats().getArrivalTime() != 0) {
      try {
        arrivalDate = section.getFlightStats().getArrivalTime();
      } catch (NumberFormatException e) {
        //Nothing
        Log.e(this.getClass().getName(), e.getMessage());
      }
    }

    return arrivalDate;
  }

  private String formatDatetime(long date) {
    return String.format("<b>%s</b> %s", dateHelper.millisecondsToDateGMT(date,
        activity.getResources().getString(R.string.templates__time1)),
        dateHelper.millisecondsToDateGMT(date,
            activity.getResources().getString(R.string.templates__datelong1)));
  }

  private void addNextFlight(long departureDate, String departureAirport, long arrivalDate,
      String arrivalAirport, long flightDuration, String flightType) {

    String oneCMSKey = (booking.getTripType().equals(Booking.TRIP_TYPE_ROUND_TRIP)
        && !booking.getArrivalAirportCode().equals(arrivalAirport))
        ? OneCMSKeys.MY_TRIPS_FLIGHT_CARD_TITLE_INBOUND
        : OneCMSKeys.MY_TRIPS_FLIGHT_CARD_TITLE_OUTBOUND;

    mTvNextFlightTitle.setText(localizableProvider.getString(oneCMSKey));

    String departureDateStr = formatDatetime(departureDate);
    String arrivalDateStr = formatDatetime(arrivalDate);

    mTvBookingDeparture.setText(departureAirport);
    mTvBookingDepartureDate.setText(HtmlUtils.formatHtml(departureDateStr));
    mTvBookingArrival.setText(arrivalAirport);
    mTvBookingArrivalDate.setText(HtmlUtils.formatHtml(arrivalDateStr));
    mTvDuration.setText(String.format("%s %s %s", durationFormatter.format(flightDuration),
        FontHelper.ICON_MY_TRIP_DETAILS_MIDDLE_DOT, flightType));
  }

  private void initOneCMSText() {
    mTvDepartureTitle.setText(
        localizableProvider.getString(OneCMSKeys.MY_TRIPS_FLIGHT_CARD_DEPARTURE));

    mTvArrivalTitle.setText(localizableProvider.getString(OneCMSKeys.MY_TRIPS_FLIGHT_CARD_ARRIVAL));

    mBtnSeeFlightDetails.setText(
        localizableProvider.getString(OneCMSKeys.MY_TRIPS_FLIGHT_CARD_BTN_DETAILS));
  }
}