package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for agreementsType.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;simpleType name="agreementsType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ETICKET"/>
 *     &lt;enumeration value="PAPER"/>
 *     &lt;enumeration value="NO_AGREEMENT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
public enum AgreementsTypeDTO {

  ETICKET, PAPER, NO_AGREEMENT;

  public static AgreementsTypeDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
