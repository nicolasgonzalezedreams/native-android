package com.odigeo.app.android.navigator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.localytics.android.Localytics;
import com.odigeo.DependencyInjector;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.helper.CrossCarUrlHandler;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoWalkthroughActivity;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Market;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.modules.GAWidgetEvents;
import com.odigeo.app.android.lib.modules.NewAppVersion;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.lib.widget.OdigeoWidget;
import com.odigeo.app.android.utils.deeplinking.AppIndexingUtil;
import com.odigeo.app.android.utils.deeplinking.DeepLinkingUtil;
import com.odigeo.app.android.utils.deeplinking.exception.DeepLinkingException;
import com.odigeo.app.android.view.HomeView;
import com.odigeo.app.android.view.NavigationDrawerView;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.interfaces.ListenerUpdateCarousel;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.exceptions.FacebookTokenAuthException;
import com.odigeo.exceptions.GoogleTokenAuthException;
import com.odigeo.exceptions.SessionCredentialsAuthException;
import com.odigeo.presenter.contracts.navigators.NavigationDrawerNavigatorInterface;
import com.squareup.otto.Subscribe;
import java.io.Serializable;

public abstract class NavigationDrawerNavigator extends BaseNavigator
    implements NavigationDrawerNavigatorInterface, ListenerUpdateCarousel {

  protected static final String TAG_HOME_VIEW = "TAG_HOME_VIEW";
  protected HomeView mHomeView;
  private NavigationDrawerView mNavigationDrawerView;
  private OdigeoApp mOdigeoApp;
  private CrossCarUrlHandler mCrossCarUrlHandler;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view);

    mNavigationDrawerView = NavigationDrawerView.newInstance();

    replaceFragment(R.id.fl_container, mNavigationDrawerView);

    mOdigeoApp = (OdigeoApp) getApplication();
    mCrossCarUrlHandler =
        new CrossCarUrlHandler(AndroidDependencyInjector.getInstance().provideMarketProvider(),
            AndroidDependencyInjector.getInstance().provideDateHelper());

    showHome();

    //TODO HOME REFACTOR: esto no sabemos muy bien que hace
    OdigeoSession odigeoSession = mOdigeoApp.getOdigeoSession();
    odigeoSession.setDataHasBeenLoaded(true);

    readWidgetIntents();
    readAppIndexingIntent();
    readDeepLinkIntent();
    showNewVersionDialog();

        /*
        Tracking Widget first activation from here,
        it's not working from OdigeoWidget class because it requires to register the BusProvider from Activity
         */
    trackFromWidget();

    startAppseeSession();

    readJumpToSearchIntent();
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.settings_whats_new) {
      navigateToWalkthrough();
      return true;
    } else if (item.getItemId() == R.id.qa_mode_homeActivity
        && mOdigeoApp.getQAModeActivityClass() != null) {
      navigateToQAMode();
    } else if (item.getItemId() == R.id.one_cms_options) {
      navigateToOneCMSOptions();
      return true;
    } else if (item.getItemId() == R.id.qa_mode_member) {
      navigateToMembershipInfo();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override public void onBackPressed() {
    if (mNavigationDrawerView.drawerIsOpened()) {
      mNavigationDrawerView.closeDrawer();
    } else {
      super.onBackPressed();
    }
  }

  @Override public void onResume() {
    super.onResume();
    Util.triggerWidgetUpdate(this, mOdigeoApp.getWidgetClass());
    if (mOdigeoApp.hasMarketChanged()) {
      DependencyInjector dependencyInjector = AndroidDependencyInjector.getInstance();
      dependencyInjector.provideEventsNotifier()
          .fireEvent(dependencyInjector.provideLoadCarouselEvent());
    }
  }

  @Override protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
    Localytics.onNewIntent(this, intent);
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == Constants.REQUEST_CODE_QAMODE) {
      Constants.domain = PreferencesManager.getQAModeURL(getApplicationContext());
    }
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_home_activity, menu);
    setMenuTitles(menu);
    return super.onCreateOptionsMenu(menu);
  }

  private void showNewVersionDialog() {
    if (PreferencesManager.isNewVersion(this)) {
      NewAppVersion.checkForNewVersion(this);
    }
  }

  private void trackFromWidget() {
    SharedPreferences preferences =
        getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE);
    if (preferences.getBoolean(OdigeoWidget.FIRST_UPDATE, false) && !preferences.getBoolean(
        OdigeoWidget.WIDGET_ACTIVATION_TRACKED, false)) {
      GAWidgetEvents.postFirstUpdate(this);
      SharedPreferences.Editor editor = preferences.edit();
      editor.putBoolean(OdigeoWidget.WIDGET_ACTIVATION_TRACKED, true);
      editor.apply();
    }
  }

  private void readAppIndexingIntent() {
    String action = getIntent().getAction();
    Uri uri = getIntent().getData();

    if (Intent.ACTION_VIEW.equals(action)
        && uri != null
        && uri.getScheme() != null
        && uri.getScheme().equals(getString(R.string.app_indexing_scheme_search))) {
      SearchOptions searchOptions =
          AppIndexingUtil.getSearchOptionsFromAppIndexingIntent(getIntent());
      if (searchOptions != null) {
        navigateToSearchWithSearchOptions(searchOptions);
      }
    }
  }

  private void readDeepLinkIntent() {
    String action = getIntent().getAction();
    Uri uri = getIntent().getData();

    if (Intent.ACTION_VIEW.equals(action)
        && uri != null
        && uri.getScheme() != null
        && uri.getScheme().equals(getString(R.string.deeplink_scheme_app_identifier))) {
      DeepLinkingUtil.DeepLinkType deepLinkType =
          DeepLinkingUtil.getCampaignCardType(getApplicationContext(), uri);
      Market market = Configuration.getInstance().getCurrentMarket();
      switch (deepLinkType) {
        case CARS:
          navigateToCars(mCrossCarUrlHandler.getCarUrl(CrossCarUrlHandler.CAMPAIGN_HEADER), false);
          finish();
          break;
        case HOTELS:
          navigateToHotels(String.format(Configuration.getInstance().getWebViewHotelsURL(),
              Configuration.getInstance().getAid(), market.getLanguage(), market.getCurrencyKey(),
              market.getWebsite().toLowerCase()));
          finish();
          break;
        case SEARCH:
          try {
            SearchOptions searchOptions =
                DeepLinkingUtil.getSearchOptionsFromDeepLinkingIntent(getIntent());
            if (searchOptions != null) {
              searchOptions.setIsResident(false);
              navigateToSearchWithSearchOptions(searchOptions);
            } else {
              navigateToSearch();
              finish();
            }
          } catch (DeepLinkingException deepLinkException) {
            mOdigeoApp.trackNonFatal(deepLinkException);
          }
          break;
      }
    }
  }

  private void readJumpToSearchIntent() {
    if (getIntent().getExtras() != null) {
      if (getIntent().getExtras().getBoolean(Constants.EXTRA_JUMP_TO_SEARCH)) {
        navigateToSearch();
      }
    }
  }

  private void setMenuTitles(Menu menu) {
    MenuItem itemWhatsNew = menu.findItem(R.id.settings_whats_new);
    itemWhatsNew.setTitle(
        LocalizablesFacade.getString(this, OneCMSKeys.WALKTHROUGH_TITLE).toString());
    itemWhatsNew.setVisible(Constants.isTestEnvironment);

    MenuItem itemQaMode = menu.findItem(R.id.qa_mode_homeActivity);
    itemQaMode.setVisible(Constants.isTestEnvironment);
    itemQaMode.setTitle(getString(R.string.qa_mode));

    MenuItem itemExtractDatabase = menu.findItem(R.id.one_cms_options);
    itemExtractDatabase.setVisible(Constants.isTestEnvironment);

    MenuItem itemMembership = menu.findItem(R.id.qa_mode_member);
    itemMembership.setVisible(Constants.isTestEnvironment);
  }

  private void navigateToQAMode() {
    Intent miIntent = new Intent(this, mOdigeoApp.getQAModeActivityClass());
    startActivityForResult(miIntent, Constants.REQUEST_CODE_QAMODE);
  }

  private void navigateToOneCMSOptions() {
    startActivity(new Intent(this, LocalizablesUpdaterNavigator.class).setFlags(
        Intent.FLAG_ACTIVITY_CLEAR_TASK));
  }

  protected void showHome() {
    mHomeView = HomeView.newInstance();
    replaceFragment(R.id.flHome, mHomeView, TAG_HOME_VIEW);
  }

  private void navigateToMembershipInfo() {
    Intent intent = new Intent(this, MembershipQANavigator.class);
    startActivity(intent);
  }

  private void readWidgetIntents() {
    if (getIntent().getBooleanExtra(Constants.EXTRA_ACTION_GOTO_MYTRIPS, false)) {

      if (getIntent().getExtras() != null) {

        if (mOdigeoApp.getMyTripDetailsActivityClass() != null) {
          AndroidDependencyInjector.getInstance()
              .provideTrackerController()
              .trackAnalyticsEvent(TrackerConstants.CATEGORY_HOME,
                  TrackerConstants.ACTION_NAVIGATION_ELEMENTS,
                  TrackerConstants.LABEL_APP_MY_TRIPS_CLICKS);
        }

        // Hit the event of GA
        GAWidgetEvents.postBookedTap(this);

        Intent intent = new Intent(this, mOdigeoApp.getMyTripsActivityClass());
        startActivity(intent);
      }
    } else if (getIntent().getBooleanExtra(Constants.EXTRA_WIDGET_EMPTY, false)) {
      // Trigger the hit of GA
      GAWidgetEvents.postEmptyTap(this);

      Intent intent = new Intent(this, mOdigeoApp.getSearchFlightsActivityClass());
      startActivity(intent);
      finish();
    } else if (getIntent().getBooleanExtra(Constants.EXTRA_WIDGET_SEARCH, false)) {
      // Trigger the hit of GA
      GAWidgetEvents.postSearchTap(GAWidgetEvents.NEW_TAP, this);

      Intent intent = new Intent(this, mOdigeoApp.getSearchFlightsActivityClass());
      startActivity(intent);
      finish();
    }

    //Check the intent from the widget
    SearchOptions options =
        (SearchOptions) getIntent().getSerializableExtra(Constants.EXTRA_WIDGET_SEARCH_OPTIONS);

    if (options != null) {
      //Trigger the hit of GA
      GAWidgetEvents.postSearchTap(GAWidgetEvents.RECENT_TAP, this);

      Intent intent = new Intent(this, mOdigeoApp.getSearchFlightsActivityClass());
      intent.putExtra(Constants.EXTRA_WIDGET_SEARCH_OPTIONS, options);
      startActivity(intent);
      finish();
    }
  }

  //TODO HOME REFACTOR: ¿Para que se utiliza esto? Queremos quitar Otto.
  @Subscribe public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getApplication());
  }

  //TODO HOME REFACTOR: ¿Para que se utiliza esto? Queremos quitar Otto.
  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getApplication());
  }

  @Override public void navigateToJoinUs() {
    startActivity(new Intent(getApplicationContext(), JoinUsNavigator.class));
  }

  @Override public void navigateToLogin() {
    Intent intent = new Intent(getApplicationContext(), LoginNavigator.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(intent);
    finish();
  }

  @Override public void navigateToTravellers() {
    startActivity(new Intent(getApplicationContext(), TravellersNavigator.class));
  }

  @Override public void navigateToMyTrips() {
    startActivity(new Intent(getApplicationContext(), MyTripsNavigator.class));
  }

  @Override public void navigateToSettings() {
    if (mOdigeoApp.getSettingsActivityClass() != null) {
      Intent miIntent = new Intent(this, mOdigeoApp.getSettingsActivityClass());
      startActivity(miIntent);
      finish();
    }
  }

  @Override public void navigateToAbout() {
    startActivity(new Intent(getApplicationContext(), AboutUsNavigator.class));
  }

  @Override public void navigateToAccountPreferences() {
    startActivity(new Intent(getApplicationContext(), AccountPreferencesNavigator.class));
  }

  @Override public void navigateToWalkthrough() {
    Intent intent = new Intent(this, ((OdigeoApp) getApplication()).getWalkthroughActivity());
    intent.putExtra(OdigeoWalkthroughActivity.EXTRA_WALKTHROUGH_CONTENT,
        getIntent().getSerializableExtra(OdigeoWalkthroughActivity.EXTRA_WALKTHROUGH_CONTENT));
    startActivity(intent);
  }

  @Override public void navigateToSearch() {
    startActivity(new Intent(this, mOdigeoApp.getSearchFlightsActivityClass()));
  }

  @Override public void navigateToSearchWithSearchOptions(Serializable searchOptions) {
    Intent intent = new Intent(this, mOdigeoApp.getSearchFlightsActivityClass());
    intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, searchOptions);
    startActivity(intent);
    finish();
  }

  @Override public void navigateToHotels(String url) {
    Intent intent = new Intent(this, mOdigeoApp.getWebViewActivityClass());
    intent.putExtra(Constants.EXTRA_URL_WEBVIEW, url);
    intent.putExtra(Constants.TITLE_WEB_ACTIVITY,
        LocalizablesFacade.getString(this, OneCMSKeys.HOTELS_TITLE).toString());
    intent.putExtra(Constants.WEBVIEW_TYPE, Constants.WEBVIEW_HOTELS);
    intent.putExtra(Constants.SHOW_HOME_ICON, true);
    tracker.trackHotels();
    startActivity(intent);
  }

  @Override public void navigateToCars(String url, boolean showVoucher) {
    Intent intent = new Intent(this, mOdigeoApp.getWebViewActivityClass());
    intent.putExtra(Constants.EXTRA_URL_WEBVIEW, url);
    intent.putExtra(Constants.TITLE_WEB_ACTIVITY,
        LocalizablesFacade.getString(this, OneCMSKeys.CARS_TITLE).toString());
    intent.putExtra(Constants.WEBVIEW_TYPE, Constants.WEBVIEW_CARS);
    intent.putExtra(Constants.EXTRA_SHOW_VOUCHER_IN_CAR, showVoucher);
    intent.putExtra(Constants.SHOW_HOME_ICON, true);
    tracker.trackCars();
    startActivity(intent);
  }

  @Override public void navigateToTripDetail(Booking booking) {
    Intent intent = MyTripDetailsNavigator.startIntent(this, booking);
    startActivity(intent);
  }

  @Override public void navigateToLastMinute() {
    Intent intent = new Intent(this, mOdigeoApp.getPixelWebViewActivityClass());
    intent.putExtra(Constants.INTENT_URL_WEBVIEW, Configuration.getInstance().getLastMinuteURL());
    startActivity(intent);
  }

  @Override public void navigateToPacks() {
    Intent intent = new Intent(this, mOdigeoApp.getPixelWebViewActivityClass());
    intent.putExtra(Constants.INTENT_URL_WEBVIEW, Configuration.getInstance().getPackageURL());
    startActivity(intent);
  }

  @Override public void onAuthError(@Nullable String source) {
    mNavigationDrawerView.refreshNavigationDrawerUserUnLogged();
    mNavigationDrawerView.updateCarousel();

    if (source != null) {
      mOdigeoApp.trackNonFatal(source.equals("facebook") ? new FacebookTokenAuthException()
          : new GoogleTokenAuthException());
    } else {
      mOdigeoApp.trackNonFatal(new SessionCredentialsAuthException());
    }
  }

  @Override public void onUpdateCarousel() {
    mHomeView.onUpdateCarousel();
  }

  @Override public boolean hasChangedMarket() {
    if (mOdigeoApp.hasMarketChanged()) {
      mOdigeoApp.setHasMarketChanged(false);
      return true;
    } else {
      return false;
    }
  }
}
