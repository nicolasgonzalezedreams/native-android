package com.odigeo.app.android.lib.models.dto;

public enum BookingStatusDTO {

  INIT, REQUEST, CONTRACT, HOLD, DIDNOTBUY, REJECTED, PENDING, RETAINED, UNKNOWN, FINAL_RET;

  public static BookingStatusDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
