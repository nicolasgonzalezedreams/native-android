package com.odigeo.app.android.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.odigeo.DependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.presenter.BasePresenter;
import com.odigeo.presenter.contracts.views.BaseViewInterface;

public abstract class BaseView<P extends BasePresenter> extends Fragment
    implements BaseViewInterface {

  protected P mPresenter;
  protected DependencyInjector dependencyInjector;
  protected LocalizableProvider localizables;
  protected TrackerControllerInterface mTracker;
  protected CrashlyticsController crashlytics;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setRetainInstance(true);
    dependencyInjector = ((OdigeoApp) getContext().getApplicationContext()).getDependencyInjector();
    localizables = dependencyInjector.provideLocalizables();
    mTracker = dependencyInjector.provideTrackerController();
    mPresenter = getPresenter();
    crashlytics = dependencyInjector.provideCrashlyticsController();
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(getFragmentLayout(), container, false);
    initComponent(view);
    initOneCMSText(view);
    setListeners();
    return view;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
        /* Make whatever we want when the fragment is created */
  }

  /**
   * Get presenter
   *
   * @return {@link #mPresenter}
   */
  protected abstract P getPresenter();

  /**
   * Get fragment layout
   *
   * @return Layout id
   */
  protected abstract int getFragmentLayout();

  /**
   * Init all component of the class
   *
   * @param view Container view of the components
   */
  protected abstract void initComponent(View view);

  /**
   * Init all listeners
   */
  protected abstract void setListeners();

  /**
   * Init texts in the view with OneCMS. This method will be called on the {@link
   * #onCreateView(LayoutInflater, ViewGroup, Bundle)} after the {@link #initComponent(View)}
   * method.
   *
   * @param view Reference to the root view.
   */
  protected abstract void initOneCMSText(View view);

  @Override public boolean isActive() {
    return isAdded();
  }
}
