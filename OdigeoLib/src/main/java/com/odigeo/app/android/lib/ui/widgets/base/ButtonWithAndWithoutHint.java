package com.odigeo.app.android.lib.ui.widgets.base;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ManuelOrtiz on 11/09/2014.
 */
public abstract class ButtonWithAndWithoutHint extends ButtonWithHint {
  private final List<PropertyChangeListener> propertyChangeListeners =
      new ArrayList<PropertyChangeListener>();
  protected boolean isEmpty;
  private int textEmptySize;
  private ColorStateList textEmptyColor;
  private String tag;

  public ButtonWithAndWithoutHint(Context context, AttributeSet attrs) {
    super(context, attrs);

    //Get Design time params
    TypedArray aAttrs = context.obtainStyledAttributes(attrs, R.styleable.ButtonTextAndHint, 0, 0);

    String textEmptyKey = aAttrs.getString(R.styleable.ButtonTextAndHint_textEmpty);
    textEmptySize = aAttrs.getDimensionPixelSize(R.styleable.ButtonTextAndHint_textEmptySize,
        DEFAULT_TEXT_SIZE);
    textEmptyColor = aAttrs.getColorStateList(R.styleable.ButtonTextAndHint_textEmptyColor);

    aAttrs.recycle();

    //If there are not values assigned
    if (getText() == null || getText().equals("")) {
      inflateEmptyLayout();
      setEmptyTextFromKey(textEmptyKey);
    } else {
      isEmpty = true;
      inflateLayout();
      getTxtVwText().setText(LocalizablesFacade.getString(getContext(), getText()));
      getTxtHint().setText(LocalizablesFacade.getString(getContext(), getHintKey()));
    }
  }

  public abstract int getEmptyLayout();

  public final void inflateEmptyLayout() {
    if (!isEmpty()) {
      isEmpty = true;
      this.removeAllViewsInLayout();
      inflate(getContext(), getEmptyLayout(), this);
      txtVwText = (TextView) findViewById(R.id.txtButtonWithHint_text);

      setTextViewSize(txtVwText, textEmptySize);
      setTextViewColor(getTxtVwText(), textEmptyColor);
      if (txtVwText != null) {
        txtVwText.setText(this.emptyText);
      }
    }
  }

  public final void setEmptyTextFromKey(String textEmptyKey) {
    if (textEmptyKey != null) {
      CharSequence charSequence = LocalizablesFacade.getString(getContext(), textEmptyKey);
      this.emptyText = charSequence;
      getTxtVwText().setText(charSequence);
    }
  }

  private void inflateLayout() {
    if (isEmpty()) {
      isEmpty = false;

      this.removeAllViewsInLayout();

      inflate(this.getContext(), getLayout(), this);

      txtVwText = (TextView) findViewById(R.id.txtButtonWithHint_text);
      txtVwHint = (TextView) findViewById(R.id.txtButtonWithHint_hint);

      if (getTxtVwText() != null) {
        setTextViewSize(getTxtVwText(), getTextSize());
        setTextViewColor(getTxtVwText(), getTextColor());
      }

      if (getTxtHint() != null) {
        setTextViewSize(getTxtHint(), getHintSize());
        setTextViewColor(getTxtHint(), getTextHintColor());
      }
    }
  }

  public final void clear() {
    inflateEmptyLayout();
  }

  @Override public final void setText(String text) {
    super.setText(text);

    if (text == null || text.equals("")) {
      inflateEmptyLayout();
    } else {
      inflateLayout();
      getTxtVwText().setText(text);
      getTxtHint().setText(LocalizablesFacade.getString(getContext(), getHintKey()));
    }
  }

  protected final ColorStateList getTextEmptyColor() {
    return textEmptyColor;
  }

  protected final void setTextEmptyColor(ColorStateList textEmptyColor) {
    this.textEmptyColor = textEmptyColor;
  }

  public final int getTextEmptySize() {
    return textEmptySize;
  }

  public final void setTextEmptySize(int textEmptySize) {
    this.textEmptySize = textEmptySize;
  }

  @Override public final boolean validate() {
    if (!isValid()) {
      notifyListeners(getTag(), "", "");

      requestFocus();
      getTxtVwText().setError(getErrorText());

      return false;
    }

    return true;
  }

  @Override

  public final void clearValidation() {
    getTxtVwText().setError(null);
  }

  public final boolean isEmpty() {
    return isEmpty;
  }

  private void notifyListeners(String property, String oldValue, String newValue) {
    for (PropertyChangeListener name : propertyChangeListeners) {
      name.propertyChange(new PropertyChangeEvent(this, property, oldValue, newValue));
    }
  }

  public final void addChangeListener(PropertyChangeListener newListener) {
    propertyChangeListeners.add(newListener);
  }

  @Override public final String getTag() {
    return tag;
  }

  public final void setTag(String tag) {
    this.tag = tag;
  }

  public void setEmptyTextAndUpdate(String text) {
    inflateEmptyLayout();
    getTxtVwText().setText(text);
  }
}

