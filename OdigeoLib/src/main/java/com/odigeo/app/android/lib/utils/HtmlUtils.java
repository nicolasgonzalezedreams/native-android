package com.odigeo.app.android.lib.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.StyleSpan;
import android.util.Log;
import android.widget.TextView;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.utils.events.ClickableUrlLinkSpan;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ximena.perez on 19/03/2015.
 */
public final class HtmlUtils {

  private static final int CLICKABLE_PHONE_CHARACTERS = 5;
  private static final int LINK_LAST_CHARACTER = 3;

  private HtmlUtils() {

  }

  /**
   * This method sets the phone number on the text clickable
   *
   * @param textView TextView displaying the text
   * @param text Text being displayed including the phone number
   * @param context Application context
   */
  public static void setPhoneTextClickable(TextView textView, String text, Context context) {

    Pattern p = Pattern.compile("(\\+)?[0-9 ]{5,}");
    Matcher m = p.matcher(text);

    int indStart = 0;
    int indEnd = 0;

    if (m.find()) {

      indStart = m.start();
      indEnd = m.end();
      Log.d("EL REGEXP ENCONTRADO", "IS " + m.group());

      if (indStart != indEnd && indEnd - indStart >= CLICKABLE_PHONE_CHARACTERS) {
        ClickablePhoneNumberSpan newClickable = new ClickablePhoneNumberSpan(context);
        newClickable.setPhoneNumber(m.group());

        Spannable span = Spannable.Factory.getInstance().newSpannable(text);
        span.setSpan(newClickable, indStart, indEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(span);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
      }
    } else {
      textView.setText(text);
    }
  }

  /**
   * This method sets the Download PDF text a clickable link
   *
   * @param activity Activity displaying the text
   * @param textView TextView displaying the text
   * @param pdfName The name of the PDF
   * @param link The text displayed as a link
   */
  public static void setDownloadPdfLinkTextClickable(Activity activity, TextView textView,
      String pdfName, String link) {

    String originalText = textView.getText().toString();

    String textLink = tagHtmlContent("a", originalText);
    String boldText = tagHtmlContent("b", originalText);

    if (textLink != null) {
      originalText = originalText.replaceAll("<a>", "").replaceAll("</a>", "");
    }
    if (boldText != null) {
      originalText = originalText.replaceAll("<b>", "").replaceAll("</b>", "");
    }

    Spannable spannable = Spannable.Factory.getInstance().newSpannable(originalText);
    if (textLink != null) {
      int newStartLinkIndex = originalText.indexOf(textLink);
      if (newStartLinkIndex >= 0) {
        ClickableDownloadPdfLinkSpan clickable =
            new ClickableDownloadPdfLinkSpan(activity, pdfName, link);
        spannable.setSpan(clickable, newStartLinkIndex, newStartLinkIndex + textLink.length(),
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
      }
    }
    if (boldText != null) {
      int newStartBoldIndex = originalText.indexOf(boldText);
      if (newStartBoldIndex >= 0) {

        StyleSpan bold = new StyleSpan(Typeface.BOLD);
        spannable.setSpan(bold, newStartBoldIndex, newStartBoldIndex + boldText.length(),
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
      }
    }
    textView.setText(spannable);
    textView.setMovementMethod(LinkMovementMethod.getInstance());
  }

  /**
   * This method sets a text link clickable
   *
   * @param textView TextView that contains the text link
   * @param fromActivity Activity displaying the text
   * @param newActivity Activity that will be accessed
   * @param bundle Bundle to be sent to the next activity
   */
  public static void setActivityLinkTextClickable(TextView textView, Activity fromActivity,
      Class newActivity, Bundle bundle) {

    String original = textView.getText().toString();

    int startIndex = original.indexOf("<b>");
    int endIndex = original.indexOf("</b>") - LINK_LAST_CHARACTER;

    if (endIndex > startIndex) {
      original = original.replace("<b>", "").replace("</b>", "");

      ClickableActivityLinkSpan clickable =
          new ClickableActivityLinkSpan(fromActivity, newActivity);
      clickable.setBundle(bundle);
      Spannable spannable = Spannable.Factory.getInstance().newSpannable(original);
      spannable.setSpan(clickable, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

      textView.setText(spannable);
      textView.setMovementMethod(LinkMovementMethod.getInstance());
    }
  }

  /**
   * Enable click in a specific text to open a Web View
   *
   * @param textView TextView that contains the text link
   * @param originalText Text with html tags
   * @param activity Activity that launches the Web View //TODO: add @NotNull annotation in
   * originalText param
   */
  public static void setLinkTextClickable(TextView textView, String originalText,
      Activity activity) {

    String http = "http://";

    //TODO: Search more than one link
    String original =
        originalText.replace("<a>", "{a}").replace("</a>", "{/a}").replaceAll("\n", "<p>");
    CharSequence htmlText = Html.fromHtml(original);
    textView.setText(htmlText);

    String htmlString = textView.getText().toString();

    int startIndex = htmlString.indexOf("{a}");
    int endIndex = htmlString.indexOf("{/a}") - LINK_LAST_CHARACTER;

    if (endIndex > startIndex) {
      htmlString = htmlString.replace("{a}", "").replace("{/a}", "");

      StringBuffer url = new StringBuffer();
      url.append(htmlString.substring(startIndex, endIndex));
      if (!url.toString().startsWith(http)) {
        url.append(http);
        url.append(url);
      }
      ClickableUrlLinkSpan clickable = new ClickableUrlLinkSpan(activity, url.toString());
      Spannable spannable = Spannable.Factory.getInstance().newSpannable(htmlString);
      spannable.setSpan(clickable, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
      textView.setText(spannable);
      textView.setMovementMethod(LinkMovementMethod.getInstance());
    }
  }

  /**
   * This method adds html tags with a specific tag
   *
   * @param tagHtml tag to be added
   * @param pivotString content to be tagged
   * @return The content with the specific tags added
   */
  public static String tagHtmlContent(String tagHtml, String pivotString) {

    int startIndex = pivotString.indexOf("<" + tagHtml + ">") + LINK_LAST_CHARACTER;
    int endIndex = pivotString.indexOf("</" + tagHtml + ">");

    if (endIndex > startIndex) {
      return pivotString.substring(startIndex, endIndex);
    }
    return null;
  }

  /**
   * This method launches an intent with a specific URL
   *
   * @param url URL to be launched
   * @param activity Activity from where the URL will be launched
   */
  public static void launchUrl(String url, Activity activity) {

    if (HtmlUtils.checkUrlIsPdf(url)) {
      PdfDownloader task = new PdfDownloader(activity);
      task.execute(url);
    } else {
      OdigeoApp odigeoApp = (OdigeoApp) activity.getApplication();

      Intent intent = new Intent(activity, odigeoApp.getWebViewActivityClass());
      Bundle extras = new Bundle();
      extras.putString("URL", url);
      intent.putExtras(extras);
      activity.startActivity(intent);
    }
  }

  /**
   * This method checks if the URL is a pdf
   *
   * @param url URL to be checked
   * @return true if it's a pdf, false if it's not
   */
  public static boolean checkUrlIsPdf(String url) {

    return url.contains(".pdf");
  }

  /**
   * This method launches a new web view
   *
   * @param url URL for the new web view
   * @param activity The current activity
   */
  public static void launchNewWebView(String url, Activity activity) {

    OdigeoApp odigeoApp = (OdigeoApp) activity.getApplicationContext();
    if (checkUrlIsPdf(url)) {
      PdfDownloader task = new PdfDownloader(activity);
      task.execute(url);
    } else {

      Intent intent = new Intent(activity, odigeoApp.getWebViewActivityClass());
      Bundle extras = new Bundle();
      extras.putString(Constants.EXTRA_URL_WEBVIEW, url);
      intent.putExtras(extras);
      activity.startActivity(intent);
    }
  }

  /**
   * This method formats a given text to HTML
   *
   * @param text Text to format to HTML
   * @return Formatted text
   */
  public static Spanned formatHtml(String text) {
    if (text != null) {
      return Html.fromHtml(text);
    }
    return null;
  }

  /**
   * Transform an string with b tag with a b and href to see it underlined
   * @param imput string to change
   * @return string changes
   */
  public static String underlineUsingHref(String imput) {
    return imput.replaceAll("\\<b>", "\\<b><a href>").replaceAll("\\</b>", "\\<a href><b>");
  }

}
