package com.odigeo.app.android.lib.models.dto.requests;

import com.odigeo.app.android.lib.models.dto.BaseRequestDTO;
import com.odigeo.app.android.lib.models.dto.ItinerarySortCriteriaDTO;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.shoppingCart.request.PromotionalCodeRequest;
import java.io.Serializable;
import java.util.List;

/**
 * Created by ManuelOrtiz on 17/09/2014.
 */
public class AddOrRemoveProductRequestModel extends BaseRequestDTO implements Serializable {
  protected List<String> insuranceOffers;
  protected List<PromotionalCodeRequest> promotionalCodes;
  protected long bookingId;
  protected ItinerarySortCriteriaDTO sortCriteria;
  protected Step step;

  /**
   * @return the insuranceOffers
   */
  public final List<String> getInsuranceOffers() {
    return insuranceOffers;
  }

  /**
   * @param insuranceOffers the insuranceOffers to set
   */
  public final void setInsuranceOffers(List<String> insuranceOffers) {
    this.insuranceOffers = insuranceOffers;
  }

  /**
   * @return the promotionalCodes
   */
  public final List<PromotionalCodeRequest> getPromotionalCodes() {
    return promotionalCodes;
  }

  /**
   * @param promotionalCodes the promotionalCodes to set
   */
  public final void setPromotionalCodes(List<PromotionalCodeRequest> promotionalCodes) {
    this.promotionalCodes = promotionalCodes;
  }

  /**
   * @return the bookingId
   */
  public final long getBookingId() {
    return bookingId;
  }

  /**
   * @param bookingId the bookingId to set
   */
  public final void setBookingId(long bookingId) {
    this.bookingId = bookingId;
  }

  /**
   * Gets the value of the sortCriteria property.
   */
  public final ItinerarySortCriteriaDTO getSortCriteria() {
    return sortCriteria;
  }

  /**
   * Sets the value of the sortCriteria property.
   */
  public final void setSortCriteria(ItinerarySortCriteriaDTO sortCriteria) {
    this.sortCriteria = sortCriteria;
  }

  /**
   * Gets the value of the step property.
   */
  public final Step getStep() {
    return step;
  }

  /**
   * Sets the value of the step property.
   */
  public final void setStep(Step step) {
    this.step = step;
  }
}
