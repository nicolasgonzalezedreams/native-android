package com.odigeo.app.android.view;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.OdigeoSpinnerAdapter;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Market;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.base.BlackDialog;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.navigator.TravellersNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.dialogs.DateDialog;
import com.odigeo.app.android.view.dialogs.OnGetDateSelectedListener;
import com.odigeo.app.android.view.helpers.DialogHelper;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.app.android.view.interfaces.AuthDialogActionInterface;
import com.odigeo.data.entity.BaseSpinnerItem;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.extensions.UITitleOfTraveller;
import com.odigeo.data.entity.extensions.UITypeOfTraveller;
import com.odigeo.data.entity.userData.UserAddress;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.data.net.error.MslError;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.interactors.CountriesInteractor;
import com.odigeo.presenter.TravellerDetailPresenter;
import com.odigeo.presenter.contracts.navigators.TravellersListNavigatorInterface;
import com.odigeo.presenter.contracts.views.TravellerDetailViewInterface;
import com.odigeo.tools.CheckerTool;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import org.apache.commons.lang3.StringUtils;

import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_PAX_DETAILS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_MY_DATA_PAX;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_DELETE_PAX_CLICKS;

/**
 * Created by carlos.navarrete on 02/09/15.
 */
public class TravellerDetailView extends BaseView<TravellerDetailPresenter>
    implements TravellerDetailViewInterface, View.OnClickListener {

  public static final String TIMEZONE_GMT = "GMT";
  public static final String TITLE = "title";
  public static final String EXTRA_USER_TRAVELLER_ID = "extUserTraId";
  public static final String EXTRA_IS_DEFAULT_TRAVELLER = "extIsDefaultTraveller";
  private static final SparseIntArray VALIDATIONS_MAP = new SparseIntArray();
  private static final SparseArray<String> ERRORS_MAP = new SparseArray<>();
  private static final String MARKET_SPAIN = "es_ES";

  static {
    VALIDATIONS_MAP.put(R.id.sso_add_passenger_name, CheckerTool.TYPE_NAME);
    VALIDATIONS_MAP.put(R.id.sso_add_passenger_surname, CheckerTool.TYPE_NAME);
    VALIDATIONS_MAP.put(R.id.sso_add_passenger_second_surname, CheckerTool.TYPE_NAME);
    VALIDATIONS_MAP.put(R.id.sso_add_passenger_email, CheckerTool.TYPE_EMAIL);
    VALIDATIONS_MAP.put(R.id.sso_add_passenger_state, CheckerTool.TYPE_NAME);
    VALIDATIONS_MAP.put(R.id.sso_add_passenger_city, CheckerTool.TYPE_NAME);
    VALIDATIONS_MAP.put(R.id.sso_add_passenger_address, CheckerTool.TYPE_ADDRESS);
    VALIDATIONS_MAP.put(R.id.sso_add_passenger_zip_code, CheckerTool.TYPE_ZIP_CODE);
    VALIDATIONS_MAP.put(R.id.sso_add_passenger_phone_code, CheckerTool.TYPE_PHONE_CODE);
    VALIDATIONS_MAP.put(R.id.sso_add_passenger_country_residence, CheckerTool.TYPE_COUNTRY);
    VALIDATIONS_MAP.put(R.id.sso_add_passenger_cell_phone, CheckerTool.TYPE_PHONE);
    //        validationsMap.put(R.id.sso_add_passenger_national_id_card);
    //        validationsMap.put(R.id.sso_add_passenger_passport);

    ERRORS_MAP.put(R.id.sso_add_passenger_name, OneCMSKeys.VALIDATION_ERROR_NAME);
    ERRORS_MAP.put(R.id.sso_add_passenger_surname, OneCMSKeys.VALIDATION_ERROR_SURNAME);
    ERRORS_MAP.put(R.id.sso_add_passenger_second_surname,
        OneCMSKeys.VALIDATION_ERROR_SECOND_SURNAME);
    ERRORS_MAP.put(R.id.sso_add_passenger_email, OneCMSKeys.VALIDATION_ERROR_MAIL);
    ERRORS_MAP.put(R.id.sso_add_passenger_state, OneCMSKeys.VALIDATION_ERROR_STATE_NAME);
    ERRORS_MAP.put(R.id.sso_add_passenger_city, OneCMSKeys.VALIDATION_ERROR_CITY_NAME);
    ERRORS_MAP.put(R.id.sso_add_passenger_address, OneCMSKeys.VALIDATION_ERROR_ADDRESS);
    ERRORS_MAP.put(R.id.sso_add_passenger_zip_code, OneCMSKeys.VALIDATION_ERROR_POSTAL_CODE);
    ERRORS_MAP.put(R.id.sso_add_passenger_phone_code, OneCMSKeys.VALIDATION_ERROR_PHONE_CODE);
    ERRORS_MAP.put(R.id.sso_add_passenger_country_residence,
        OneCMSKeys.VALIDATION_ERROR_COUNTRY_CODE);
    ERRORS_MAP.put(R.id.sso_add_passenger_cell_phone, OneCMSKeys.VALIDATION_ERROR_PHONE_NUMBER);
  }

  private final CountriesInteractor mCountriesInteractor =
      AndroidDependencyInjector.getInstance().provideCountriesInteractor();
  private final String mLanguageIsoCode = LocaleUtils.getCurrentLanguageIso();
  private ScrollView mScrollView;
  private final View.OnFocusChangeListener onFocusChangeListener =
      new View.OnFocusChangeListener() {
        @Override public void onFocusChange(View view, boolean hasFocus) {
          if (view instanceof TextInputEditText) {
            TextInputEditText textInputEditText = (TextInputEditText) view;
            if (!hasFocus) {
              validateEditText((TextInputLayout) textInputEditText.getParent().getParent(), false,
                  false);
            }
          }
        }
      };
  private BlackDialog mLoadingDialog;
  private TextView mTextViewTypePassenger;
  private TextView mTexViewTitlePassenger;
  private TextView mTextViewDefaultTraveller;
  private TextView mHeaderBasicInfo;
  private TextView mHeaderDocumentation;
  private TextView mHeaderContactDetail;
  private Spinner mSpinnerTypePassenger;
  private Spinner mSpinnerTitlePassenger;
  private SwitchCompat mSwitchDefault;
  private TextInputEditText mEditTextName;
  private TextInputEditText mEditTextSurname;
  private TextInputEditText mEditTextSecondSurname;
  private TextInputEditText mEditTextBirthDate;
  private TextInputEditText mEditTextNationality;
  private TextInputEditText mEditTextIdCard;
  private TextInputEditText mEditTextPassport;
  private TextInputEditText mEditTextEmail;
  private TextInputEditText mEditTextState;
  private TextInputEditText mEditTextCity;
  private TextInputEditText mEditTextAddress;
  private TextInputEditText mEditTextZipCode;
  private TextInputEditText mEditTextCountryResidence;
  private TextInputEditText mEditTextPhonePrefix;
  private TextInputEditText mEditTextCellphone;
  private TextInputEditText mEditTextFrequentFlyer;
  private Button mButtonDeleteTraveller;
  private TextInputLayout mInputName;
  private TextInputLayout mInputSurname;
  private TextInputLayout mInputSecondSurname;
  private TextInputLayout mInputBirthDate;
  private TextInputLayout mInputNationality;
  private TextInputLayout mInputIdCard;
  private TextInputLayout mInputPassport;
  private TextInputLayout mInputEmail;
  private TextInputLayout mInputState;
  private TextInputLayout mInputCity;
  private TextInputLayout mInputAddress;
  private TextInputLayout mInputZipCode;
  private TextInputLayout mInputCountryResidence;
  private TextInputLayout mInputPhonePrefix;
  private TextInputLayout mInputCellphone;
  private TextInputLayout mInputFrequentFlyer;
  private DateDialog mDateDialog;
  private DialogHelper mDialogHelper;
  private HashMap<UserIdentification.IdentificationType, UserIdentification>
      mUserIdentificationsMap;
  private List<UserFrequentFlyer> mUserFrequentFlyerList = new ArrayList<>();
  private String mTitle;
  private String mNationalityCountryCode;
  private String mAddressCountryCode;
  private String mPhonePrefixCountryCode;
  private long mMinValidDate;
  private long mBirthDate;
  private long mUserTravellerId = -1;
  private boolean mIsDefaultTraveller;

  public static TravellerDetailView newInstance(String title, long userTravellerId,
      boolean isDefaultTraveller) {
    TravellerDetailView travellerDetailViewFragment = new TravellerDetailView();
    Bundle args = new Bundle();
    args.putString(TITLE, title);
    args.putLong(EXTRA_USER_TRAVELLER_ID, userTravellerId);
    args.putBoolean(EXTRA_IS_DEFAULT_TRAVELLER, isDefaultTraveller);
    travellerDetailViewFragment.setArguments(args);
    return travellerDetailViewFragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      mTitle = getArguments().getString(TITLE, "");
      mUserTravellerId = getArguments().getLong(EXTRA_USER_TRAVELLER_ID, -1);
      mIsDefaultTraveller = getArguments().getBoolean(EXTRA_IS_DEFAULT_TRAVELLER, false);
    }
    setHasOptionsMenu(true);
    mUserIdentificationsMap = new HashMap<>();
  }

  @Override public void onResume() {
    super.onResume();
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_MYINFO_FREQUENT_TRAVELLERS_ACTION);
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mPresenter.initActionBar(mTitle, 0);
    mLoadingDialog = new BlackDialog(getActivity(), true);
    mMinValidDate = mPresenter.getMinValidDate(UserTraveller.TypeOfTraveller.ADULT).getTime();
    mBirthDate = mMinValidDate;
    if (mUserTravellerId != -1) {
      drawValues(mPresenter.getUserTraveller(mUserTravellerId));
    } else {
      dateDialogSettings(UserTraveller.TypeOfTraveller.ADULT);
    }
    if (mIsDefaultTraveller) {
      mSwitchDefault.setChecked(true);
      view.findViewById(R.id.layout_default_traveller).setVisibility(View.GONE);
    }
  }

  @Override protected TravellerDetailPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideTravellerDetailPresenter(this, (TravellersListNavigatorInterface) getActivity());
  }

  @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(R.menu.sso_menu_add_traveller, menu);
    MenuItem item = menu.findItem(R.id.sso_confirmation_add_traveller);
    item.setTitle(LocalizablesFacade.getString(this.getActivity(), OneCMSKeys.DATA_STORAGE_SAVE));
    item.setIcon(
        DrawableUtils.getTintedResource(R.drawable.ic_done, R.color.actionbar_confirm_tint,
            getActivity()));
  }

  @Override protected int getFragmentLayout() {
    return R.layout.sso_add_passenger_view;
  }

  private boolean validateForm(TextInputEditText... textInputEditTexts) {
    boolean result = true;
    for (TextInputEditText textInputEditText : textInputEditTexts) {
      result &= validateEditText((TextInputLayout) textInputEditText.getParent().getParent(), false,
          result);
    }
    return result;
  }

  private boolean validateAddressForm(TextInputEditText... textInputEditTexts) {
    boolean isEmpty = true;
    boolean result = true;
    for (TextInputEditText textInputEditText : textInputEditTexts) {
      isEmpty &= textInputEditText.getText().toString().trim().isEmpty();
    }
    if (!isEmpty) {
      for (TextInputEditText textInputEditText : textInputEditTexts) {
        result &=
            validateEditText((TextInputLayout) textInputEditText.getParent().getParent(), true,
                result);
      }
    }
    return result;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    ViewUtils.hideKeyboard(getActivity());
    int itemId = item.getItemId();
    if (itemId == R.id.sso_confirmation_add_traveller) {
      UserTraveller.TypeOfTraveller typeOfTraveller =
          (UserTraveller.TypeOfTraveller) ((BaseSpinnerItem) mSpinnerTypePassenger.getSelectedItem())
              .getData();
      if (validateForm(mEditTextName, mEditTextSurname, mEditTextSecondSurname, mEditTextEmail,
          mEditTextCellphone) && validateAddressForm(mEditTextCity, mEditTextAddress,
          mEditTextZipCode, mEditTextCountryResidence) && mPresenter.validateBirthDate(
          typeOfTraveller, mBirthDate, mEditTextBirthDate.getText().toString())) {

        UserProfile.Title title =
            (UserProfile.Title) ((BaseSpinnerItem) mSpinnerTitlePassenger.getSelectedItem()).getData();

        mPresenter.saveTraveller(mUserTravellerId, mEditTextName.getText().toString().trim(), null,
            mEditTextSurname.getText().toString().trim(),
            mEditTextSecondSurname.getText().toString().trim(), typeOfTraveller, title, mBirthDate,
            mNationalityCountryCode, false, mEditTextEmail.getText().toString().trim(), null,
            mSwitchDefault.isChecked(), null, null, null,
            mEditTextState.getText().toString().trim(), mEditTextCity.getText().toString().trim(),
            mEditTextAddress.getText().toString().trim(),
            mEditTextZipCode.getText().toString().trim(), mAddressCountryCode, null, false, null,
            mPhonePrefixCountryCode, mEditTextCellphone.getText().toString().trim(), null, null,
            null, mUserIdentificationsMap, mUserFrequentFlyerList);
      }
    }
    return super.onOptionsItemSelected(item);
  }

  @Override protected void initComponent(View view) {

    mScrollView = (ScrollView) view.findViewById(R.id.sso_add_passenger_scroll_view);
    mDialogHelper = new DialogHelper(getActivity());
    mDateDialog = new DateDialog(getActivity(),
        LocalizablesFacade.getString(getContext(), OneCMSKeys.COMMON_OK).toString(),
        LocalizablesFacade.getString(getContext(), OneCMSKeys.COMMON_CANCEL).toString());

    Typeface regularTypeFace = Configuration.getInstance().getFonts().getRegular();
    Typeface boldTypeface = Configuration.getInstance().getFonts().getBold();

    mHeaderBasicInfo =
        (TextView) view.findViewById(R.id.sso_add_passenger_view_title_card_basic_info);
    mHeaderDocumentation =
        (TextView) view.findViewById(R.id.sso_add_passenger_view_title_card_documentation);
    mHeaderContactDetail =
        (TextView) view.findViewById(R.id.sso_add_passenger_view_title_card_contact_detail);
    mTextViewTypePassenger = (TextView) view.findViewById(R.id.sso_add_passenger_type_passenger);
    mTexViewTitlePassenger = (TextView) view.findViewById(R.id.add_passenger_title_passenger);
    mTextViewDefaultTraveller =
        (TextView) view.findViewById(R.id.sso_add_passenger_body_default_passenger);
    mTextViewDefaultTraveller =
        (TextView) view.findViewById(R.id.sso_add_passenger_body_default_passenger);

    mSwitchDefault = (SwitchCompat) view.findViewById(R.id.switch_default_traveller);
    //Spinner configuration
    mSpinnerTypePassenger = (Spinner) view.findViewById(R.id.spinner_add_passenger_type_passenger);
    OdigeoSpinnerAdapter<UITypeOfTraveller> typeOfTravellerAdapter =
        new OdigeoSpinnerAdapter<>(getActivity(), UITypeOfTraveller.getList());
    mSpinnerTypePassenger.setAdapter(typeOfTravellerAdapter);

    //Spinner configuration
    mSpinnerTitlePassenger =
        (Spinner) view.findViewById(R.id.sso_spinner_add_passenger_title_passenger);
    OdigeoSpinnerAdapter<UITitleOfTraveller> titleOfTravellerAdapter =
        new OdigeoSpinnerAdapter<>(getActivity(), UITitleOfTraveller.getList());
    mSpinnerTitlePassenger.setAdapter(titleOfTravellerAdapter);

    //EditText configuration along with the TextInputLayouts
    mInputName = (TextInputLayout) view.findViewById(R.id.sso_add_passenger_name);
    mEditTextName = (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_name);
    mInputSurname = (TextInputLayout) view.findViewById(R.id.sso_add_passenger_surname);
    mEditTextSurname = (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_surname);
    mInputSecondSurname =
        (TextInputLayout) view.findViewById(R.id.sso_add_passenger_second_surname);
    mEditTextSecondSurname =
        (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_second_surname);
    mInputBirthDate = (TextInputLayout) view.findViewById(R.id.sso_add_passenger_birthdate);
    mEditTextBirthDate =
        (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_birthdate);
    mInputNationality = (TextInputLayout) view.findViewById(R.id.sso_add_passenger_nationality);
    mEditTextNationality =
        (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_nationality);
    mInputIdCard = (TextInputLayout) view.findViewById(R.id.sso_add_passenger_national_id_card);
    mEditTextIdCard =
        (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_national_id_card);
    mInputPassport = (TextInputLayout) view.findViewById(R.id.sso_add_passenger_passport);
    mEditTextPassport =
        (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_passport);
    mInputEmail = (TextInputLayout) view.findViewById(R.id.sso_add_passenger_email);
    mEditTextEmail = (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_email);
    mInputState = (TextInputLayout) view.findViewById(R.id.sso_add_passenger_state);
    mEditTextState = (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_state);
    mInputCity = (TextInputLayout) view.findViewById(R.id.sso_add_passenger_city);
    mEditTextCity = (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_city);
    mInputAddress = (TextInputLayout) view.findViewById(R.id.sso_add_passenger_address);
    mEditTextAddress = (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_address);
    mInputZipCode = (TextInputLayout) view.findViewById(R.id.sso_add_passenger_zip_code);
    mEditTextZipCode = (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_zip_code);
    mInputPhonePrefix = (TextInputLayout) view.findViewById(R.id.sso_add_passenger_phone_code);
    mEditTextPhonePrefix =
        (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_phone_code);
    mInputCountryResidence =
        (TextInputLayout) view.findViewById(R.id.sso_add_passenger_country_residence);
    mEditTextCountryResidence =
        (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_country_residence);
    mInputCellphone = (TextInputLayout) view.findViewById(R.id.sso_add_passenger_cell_phone);
    mEditTextCellphone =
        (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_cell_phone);
    mInputFrequentFlyer =
        (TextInputLayout) view.findViewById(R.id.sso_add_passenger_frequent_flyer);
    mEditTextFrequentFlyer =
        (TextInputEditText) view.findViewById(R.id.edit_text_add_passenger_frequent_flyer);

    mButtonDeleteTraveller = (Button) view.findViewById(R.id.button_add_traveller_reset_password);

    mEditTextName.setTypeface(regularTypeFace);
    mEditTextSurname.setTypeface(regularTypeFace);
    mEditTextSecondSurname.setTypeface(regularTypeFace);
    mEditTextBirthDate.setTypeface(regularTypeFace);
    mEditTextNationality.setTypeface(regularTypeFace);
    mEditTextIdCard.setTypeface(regularTypeFace);
    mEditTextPassport.setTypeface(regularTypeFace);
    mEditTextEmail.setTypeface(regularTypeFace);
    mEditTextState.setTypeface(regularTypeFace);
    mEditTextCity.setTypeface(regularTypeFace);
    mEditTextAddress.setTypeface(regularTypeFace);
    mEditTextZipCode.setTypeface(regularTypeFace);
    mEditTextCountryResidence.setTypeface(regularTypeFace);
    mEditTextPhonePrefix.setTypeface(regularTypeFace);
    mEditTextCellphone.setTypeface(regularTypeFace);
    mEditTextFrequentFlyer.setTypeface(regularTypeFace);

    mTextViewTypePassenger.setTypeface(regularTypeFace);
    mTexViewTitlePassenger.setTypeface(regularTypeFace);
    mTextViewDefaultTraveller.setTypeface(regularTypeFace);
    mHeaderBasicInfo.setTypeface(boldTypeface);
    mHeaderDocumentation.setTypeface(boldTypeface);
    mHeaderContactDetail.setTypeface(boldTypeface);

    mInputName.setTypeface(regularTypeFace);
    mInputSurname.setTypeface(regularTypeFace);
    mInputSecondSurname.setTypeface(regularTypeFace);
    mInputBirthDate.setTypeface(regularTypeFace);
    mInputNationality.setTypeface(regularTypeFace);
    mInputIdCard.setTypeface(regularTypeFace);
    mInputPassport.setTypeface(regularTypeFace);

    mInputEmail.setTypeface(regularTypeFace);
    mInputState.setTypeface(regularTypeFace);
    mInputCity.setTypeface(regularTypeFace);
    mInputAddress.setTypeface(regularTypeFace);
    mInputZipCode.setTypeface(regularTypeFace);
    mInputCountryResidence.setTypeface(regularTypeFace);
    mInputPhonePrefix.setTypeface(regularTypeFace);
    mInputCellphone.setTypeface(regularTypeFace);
    mInputFrequentFlyer.setTypeface(regularTypeFace);

    mButtonDeleteTraveller.setTypeface(regularTypeFace);

    mPresenter.showSecondTextView();
  }

  @Override protected void setListeners() {

    mEditTextNationality.setOnClickListener(this);
    mEditTextCountryResidence.setOnClickListener(this);
    mEditTextPhonePrefix.setOnClickListener(this);
    mEditTextIdCard.setOnClickListener(this);
    mEditTextPassport.setOnClickListener(this);
    mEditTextBirthDate.setOnClickListener(this);
    mEditTextFrequentFlyer.setOnClickListener(this);
    mButtonDeleteTraveller.setOnClickListener(this);

    mEditTextName.setOnFocusChangeListener(onFocusChangeListener);
    mEditTextSurname.setOnFocusChangeListener(onFocusChangeListener);
    mEditTextSecondSurname.setOnFocusChangeListener(onFocusChangeListener);
    mEditTextEmail.setOnFocusChangeListener(onFocusChangeListener);
    mEditTextAddress.setOnFocusChangeListener(onFocusChangeListener);
    mEditTextState.setOnFocusChangeListener(onFocusChangeListener);
    mEditTextCity.setOnFocusChangeListener(onFocusChangeListener);
    mEditTextZipCode.setOnFocusChangeListener(onFocusChangeListener);
    mEditTextCellphone.setOnFocusChangeListener(onFocusChangeListener);

    mSpinnerTypePassenger.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        UserTraveller.TypeOfTraveller typeOfTraveller =
            (UserTraveller.TypeOfTraveller) ((BaseSpinnerItem) mSpinnerTypePassenger.getSelectedItem())
                .getData();
        dateDialogSettings(typeOfTraveller);
        if (position != 0) {
          mSwitchDefault.setEnabled(false);
          mSwitchDefault.setChecked(false);
        } else {
          mSwitchDefault.setEnabled(true);
        }
      }

      @Override public void onNothingSelected(AdapterView<?> parent) {
      }
    });
  }

  @Override protected void initOneCMSText(View view) {
    mHeaderBasicInfo.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_TRAVELLERS_BASIC_INFO));
    mHeaderDocumentation.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_TRAVELLERS_DOCUMENTATION));
    mHeaderContactDetail.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_TRAVELLERS_CONTACT_DETAIL));

    mInputName.setHint(LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_NAME));
    mInputSurname.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_FIRST_LASTNAME));
    mInputSecondSurname.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_SECOND_LASTNAME));
    mTextViewTypePassenger.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.LABEL_TITLE_TYPE_PASSENGER));
    mTexViewTitlePassenger.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.LABEL_TITLE_TITLE_PASSENGER));
    mInputNationality.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_NATIONALITY));
    mInputBirthDate.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_BIRTH_DATE));
    mTextViewDefaultTraveller.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.LABEL_TITLE_DEFAULT_TRAVELLER));

    mInputIdCard.setHint(LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_ID_CARD));
    mInputPassport.setHint(LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_PASSPORT));

    mInputEmail.setHint(LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_EMAIL));
    mInputState.setHint(LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_STATE));
    mInputCity.setHint(LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_CITY));
    mInputAddress.setHint(LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_ADDRESS));
    mInputZipCode.setHint(LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_ZIP_CODE));
    mInputCountryResidence.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_COUNTRY_RESIDENCE));
    mInputPhonePrefix.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_PHONE_PREFIX));
    mInputCellphone.setHint(LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_PHONE));
    mInputFrequentFlyer.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.HINT_FREQUENT_FLYER_CODE));
    mButtonDeleteTraveller.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_TRAVELLERS_DELETE));

    if (mUserTravellerId < 1) {
      mButtonDeleteTraveller.setVisibility(View.GONE);
    }
  }

  private void drawValues(UserTraveller userTraveller) {
    if (userTraveller != null) {
      int typeOfTravellerOrdinal = userTraveller.getTypeOfTraveller().ordinal();
      if (typeOfTravellerOrdinal < UITypeOfTraveller.getList().size()) {
        mSpinnerTypePassenger.setSelection(typeOfTravellerOrdinal);
      }
      mEditTextEmail.setText(userTraveller.getEmail());
      UserProfile profile = userTraveller.getUserProfile();
      if (profile != null) {
        mEditTextName.setText(profile.getName());
        mEditTextSurname.setText(profile.getFirstLastName());
        mEditTextSecondSurname.setText(profile.getSecondLastName());
        int titlePassengerOrdinal = profile.getTitle().ordinal();

        if ((profile.getNationalityCountryCode() != null) && (!profile.getNationalityCountryCode()
            .isEmpty())) {
          mNationalityCountryCode = profile.getNationalityCountryCode();
          Country country = mCountriesInteractor.getCountryByCountryCode(mLanguageIsoCode,
              profile.getNationalityCountryCode());
          if (country != null) {
            mEditTextNationality.setText(country.getName());
          }
        }

        if (titlePassengerOrdinal < UITitleOfTraveller.getList().size()) {
          mSpinnerTitlePassenger.setSelection(titlePassengerOrdinal);
        }

        setBirthDate(profile.getBirthDate());
        if (profile.getUserIdentificationList() != null && !profile.getUserIdentificationList()
            .isEmpty()) {
          for (UserIdentification uIdentification : profile.getUserIdentificationList()) {
            setIdentification(uIdentification);
          }
        }

        UserAddress userAddress = profile.getUserAddress();
        if (userAddress != null) {
          mEditTextState.setText(userAddress.getState());
          mEditTextCity.setText(userAddress.getCity());
          mEditTextAddress.setText(userAddress.getAddress());
          mEditTextZipCode.setText(userAddress.getPostalCode());
          if ((userAddress.getCountry() != null) && (!TextUtils.isEmpty(
              userAddress.getCountry()))) {
            Country countryResident = mCountriesInteractor.getCountryByCountryCode(mLanguageIsoCode,
                userAddress.getCountry());
            if (countryResident != null) {
              setCountryResidenceSelected(countryResident);
            }
          }
        }

        if ((profile.getPrefixPhoneNumber() != null) && (!TextUtils.isEmpty(
            profile.getPrefixPhoneNumber()))) {
          Country countryPhoneNumber =
              mCountriesInteractor.getCountryByCountryCode(mLanguageIsoCode,
                  profile.getPrefixPhoneNumber());
          if (countryPhoneNumber != null) {
            setPhonePrefixSelected(countryPhoneNumber);
          }
        }

        mEditTextCellphone.setText(profile.getPhoneNumber());
      }
      if (userTraveller.getUserFrequentFlyers() != null && !userTraveller.getUserFrequentFlyers()
          .isEmpty()) {
        mUserFrequentFlyerList = userTraveller.getUserFrequentFlyers();
      }
      mSwitchDefault.setChecked(mIsDefaultTraveller);
    }
  }

  private void dateDialogSettings(UserTraveller.TypeOfTraveller keyTravellerType) {
    mDateDialog.setMinDate(mPresenter.getMinValidDate(keyTravellerType));
    mDateDialog.setMaxDate(mPresenter.getMaxValidDate(keyTravellerType));
  }

  @Override public void setUserFrequentFlyerList(List<UserFrequentFlyer> frequentFlyerList) {
    this.mUserFrequentFlyerList = frequentFlyerList;
  }

  @Override public List<UserFrequentFlyer> getUserFrequentFlyer() {
    return mUserFrequentFlyerList;
  }

  @Override public void showDateError() {
    mInputBirthDate.setError(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.VALIDATION_ERROR_BIRTH_DATE));
    focusOnView(mInputBirthDate);
  }

  @Override public void showAuthError() {
    mDialogHelper.showAuthErrorDialog(this, new AuthDialogActionInterface() {
      @Override public void OnAuthDialogOK() {
        mPresenter.onAuthOkClicked();
      }
    });
  }

  @Override public void onLogoutError(MslError error, String message) {
    Log.e(error.name(), message);
  }

  @Override public void showSecondSurnameView() {
    Market market = Configuration.getInstance().getCurrentMarket();
    if (market.getLocale().equals(MARKET_SPAIN)) {
      mInputSecondSurname.setVisibility(View.VISIBLE);
    }
  }

  @Override public void showLoadingDialog() {
    mLoadingDialog.show();
  }

  @Override public void hideLoadingDialog() {
    if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
      mLoadingDialog.dismiss();
    }
  }

  @Override public void showSavedErrorDialog() {
    mDialogHelper.showErrorDialog(getActivity(),
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_ERROR_WRONG).toString());
  }

  private void showCalendarDialog(DateDialog dateDialog) {
    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE_GMT));
    UserTraveller.TypeOfTraveller typeOfTraveller =
        (UserTraveller.TypeOfTraveller) ((BaseSpinnerItem) mSpinnerTypePassenger.getSelectedItem()).getData();
    if (mPresenter.validateBirthDate(typeOfTraveller, mBirthDate,
        mEditTextBirthDate.getText().toString()) && mBirthDate > mMinValidDate) {
      calendar.setTimeInMillis(mBirthDate);
    } else {
      calendar.setTimeInMillis(mPresenter.getMaxValidDate(typeOfTraveller).getTime());
    }
    dateDialog.createDateDialog(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH));
    dateDialog.showDialog(new OnGetDateSelectedListener() {
      @Override public void onGetDate(Date date) {
        setBirthDate(date);
      }
    }).show();
  }

  private void setBirthDate(Date date) {
    if (date != null) {
      String presentationDate = getResources().getString(R.string.templates__date2);
      SimpleDateFormat simpleDateFormat = OdigeoDateUtils.getGmtDateFormat(presentationDate);
      mEditTextBirthDate.setText(simpleDateFormat.format(date));
      mInputBirthDate.setError(null);
      mBirthDate = date.getTime();
    }
  }

  private void setBirthDate(long dateLong) {
    if (dateLong == UserProfile.UNKNOWN_BIRTHDATE || dateLong == 0) {
      mEditTextBirthDate.setText("");
      mInputBirthDate.setError(null);
      mBirthDate = UserProfile.UNKNOWN_BIRTHDATE;
    } else if (dateLong > mMinValidDate) {
      Date date = new Date(dateLong);
      String presentationDate = getResources().getString(R.string.templates__date2);
      SimpleDateFormat simpleDateFormat = OdigeoDateUtils.getGmtDateFormat(presentationDate);
      mEditTextBirthDate.setText(simpleDateFormat.format(date));
      mInputBirthDate.setError(null);
      mBirthDate = date.getTime();
    }
  }

  public void setNationalitySelected(Country country) {
    mEditTextNationality.setText(country.getName());
    mNationalityCountryCode = country.getCountryCode();
  }

  public void setIdentification(UserIdentification identification) {
    if (identification != null) {
      setIdentification(identification.getIdentificationId(),
          identification.getIdentificationExpirationDate(),
          identification.getIdentificationCountryCode(), identification.getIdentificationType());
    }
  }

  public void setIdentification(String numberIdentification, long dateExpiration,
      String identificationCountryCode, UserIdentification.IdentificationType identificationType) {
    UserIdentification userIdentification;

    //Set here all because we need compatibility with legacy users.
    if (identificationType == UserIdentification.IdentificationType.NATIONAL_ID_CARD
        || identificationType == UserIdentification.IdentificationType.NIF
        || identificationType == UserIdentification.IdentificationType.NIE) {
      if (numberIdentification != null && !numberIdentification.isEmpty()) {
        userIdentification =
            new UserIdentification(-1, -1, numberIdentification, identificationCountryCode,
                dateExpiration, UserIdentification.IdentificationType.NATIONAL_ID_CARD, 0);
        mUserIdentificationsMap.put(UserIdentification.IdentificationType.NATIONAL_ID_CARD,
            userIdentification);
        mEditTextIdCard.setText(numberIdentification);
      } else {
        mUserIdentificationsMap.put(UserIdentification.IdentificationType.NATIONAL_ID_CARD, null);
        mEditTextIdCard.setText("");
      }
    } else if (identificationType == UserIdentification.IdentificationType.PASSPORT) {
      if (!StringUtils.isEmpty(numberIdentification)) {
        userIdentification =
            new UserIdentification(-1, -1, numberIdentification, identificationCountryCode,
                dateExpiration, UserIdentification.IdentificationType.PASSPORT, 0);
        mUserIdentificationsMap.put(UserIdentification.IdentificationType.PASSPORT,
            userIdentification);
        mEditTextPassport.setText(numberIdentification);
      } else {
        mUserIdentificationsMap.put(UserIdentification.IdentificationType.PASSPORT, null);
        mEditTextPassport.setText("");
      }
    }
  }

  public void setCountryResidenceSelected(Country country) {
    mEditTextCountryResidence.setText(country.getName());
    mAddressCountryCode = country.getCountryCode();
    mInputCountryResidence.setError(null);
  }

  public void setPhonePrefixSelected(Country country) {
    String phonePrefix = country.getPhonePrefix();
    mEditTextPhonePrefix.setText(String.format("(%s) %s", phonePrefix, country.getName()));
    mPhonePrefixCountryCode = country.getCountryCode();
  }

  @Override public void onClick(View view) {
    int viewId = view.getId();
    if (viewId == mEditTextNationality.getId()) {
      mPresenter.getNavigatorInterface()
          .navigateToSelectCountry(TravellersNavigator.TAG_TRAVELLER_DETAIL,
              Constants.SELECTING_TRAVELLER_NATIONALITY);
    } else if (viewId == mEditTextCountryResidence.getId()) {
      mPresenter.getNavigatorInterface()
          .navigateToSelectCountry(TravellersNavigator.TAG_TRAVELLER_DETAIL,
              Constants.SELECTING_BUYER_COUNTRY_OF_RESIDENCE);
    } else if (viewId == mEditTextPhonePrefix.getId()) {
      mPresenter.getNavigatorInterface().navigateToCountryCode();
    } else if (viewId == mEditTextIdCard.getId()) {
      //Constants.REQUEST_CODE_CARTE is the request code for National ID card til further information.
      mPresenter.getNavigatorInterface()
          .navigateToIdentifications(UserIdentification.IdentificationType.NATIONAL_ID_CARD,
              Constants.REQUEST_CODE_CARTE,
              getUserIdentificationAtList(UserIdentification.IdentificationType.NATIONAL_ID_CARD));
    } else if (viewId == mEditTextPassport.getId()) {
      mPresenter.getNavigatorInterface()
          .navigateToIdentifications(UserIdentification.IdentificationType.PASSPORT,
              Constants.REQUEST_CODE_PASSPORT,
              getUserIdentificationAtList(UserIdentification.IdentificationType.PASSPORT));
    } else if (viewId == mEditTextBirthDate.getId()) {
      showCalendarDialog(mDateDialog);
    } else if (viewId == mEditTextFrequentFlyer.getId()) {
      ((TravellersNavigator) getActivity()).navigateToFrequentFlyerCodes();
    } else if (viewId == mButtonDeleteTraveller.getId()) {
      mPresenter.deleteUserTraveller(mUserTravellerId);
      mTracker.trackAnalyticsEvent(CATEGORY_MY_DATA_PAX, ACTION_PAX_DETAILS,
          LABEL_DELETE_PAX_CLICKS);
    }
  }

  private UserIdentification getUserIdentificationAtList(
      UserIdentification.IdentificationType type) {
    if (!mUserIdentificationsMap.isEmpty()) {
      return mUserIdentificationsMap.get(type);
    }
    return null;
  }

  /**
   * Check if the field is valid.
   *
   * @param idView Id of the view to be checked.
   * @param value Value to be verified.
   * @return If the value is correct.
   */
  private boolean isValid(int idView, String value, boolean validateIfEmpty) {
    boolean result = true;
    if (validateIfEmpty
        || !value.isEmpty()
        || idView == R.id.sso_add_passenger_name
        || idView == R.id.sso_add_passenger_surname) {
      switch (VALIDATIONS_MAP.get(idView)) {
        case CheckerTool.TYPE_NAME:
          result = CheckerTool.checkTravellerName(value);
          break;
        case CheckerTool.TYPE_COUNTRY:
          result = CheckerTool.checkCountry(value);
          break;
        case CheckerTool.TYPE_EMAIL:
          result = CheckerTool.checkEmail(value);
          break;
        case CheckerTool.TYPE_ADDRESS:
          result = CheckerTool.checkTravellerAddress(value);
          break;
        case CheckerTool.TYPE_PHONE:
          result = CheckerTool.checkTravellerPhoneNumberNumbers(value);
          break;
        case CheckerTool.TYPE_ZIP_CODE:
          result = CheckerTool.checkTravellerZipCode(value);
          break;
        case CheckerTool.TYPE_PHONE_CODE:
          result = !TextUtils.isEmpty(value);
          break;
        case CheckerTool.TYPE_NIE:
          result = CheckerTool.checkTravellerNIECharacters(value);
          break;
        case CheckerTool.TYPE_NIF:
          result = CheckerTool.checkTravellerDNICharacters(value);
          break;
        case CheckerTool.TYPE_PASSPORT:
          result = CheckerTool.checkTravellerPassport(value);
          break;
        default:
          result = true;
          break;
      }
    }
    return result;
  }

  private boolean validateEditText(TextInputLayout textInputLayout, boolean validateIfEmpty,
      boolean setFocus) {
    if (textInputLayout.getVisibility() == View.VISIBLE) {
      String text = textInputLayout.getEditText().getText().toString().trim();
      boolean result = isValid(textInputLayout.getId(), text, validateIfEmpty);
      if (result) {
        textInputLayout.setError(null);
        textInputLayout.getEditText().setText(text);
      } else {
        String string =
            LocalizablesFacade.getRawString(getContext(), ERRORS_MAP.get(textInputLayout.getId()));
        textInputLayout.setError(string);
        focusInvalidField(textInputLayout, setFocus);
      }
      return result;
    }
    return true;
  }

  private void focusOnView(final TextInputLayout textInputLayout) {
    new Handler().post(new Runnable() {
      @Override public void run() {
        View parent = (View) textInputLayout.getParent().getParent();
        int top = textInputLayout.getTop() + parent.getTop();
        int inputHeight = textInputLayout.getHeight();
        int height = mScrollView.getHeight();
        mScrollView.smoothScrollTo(0, top - height / 2 + inputHeight);
      }
    });
  }

  private void focusInvalidField(TextInputLayout textInputLayout, boolean setFocus) {
    if (setFocus) {
      if (textInputLayout.getEditText().isFocusable()) {
        textInputLayout.requestFocus();
        // Show keyboard
        ((InputMethodManager) getActivity().getSystemService(
            Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED,
            InputMethodManager.HIDE_NOT_ALWAYS);
      }
      focusOnView(textInputLayout);
    }
  }

  @Override public void onStop() {
    //clears inputTextLayout error to fix editTexts underline bug
    mInputName.setError(null);
    super.onStop();
  }
}
