package com.odigeo.app.android.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.localytics.android.Localytics;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import java.util.Calendar;
import java.util.Map;

/**
 * A service that extends FirebaseMessagingService.
 * This is required if you want to do any message handling beyond receiving notifications on apps in
 * the background.
 * To receive notifications in foregrounded apps, to receive data payload, to send upstream
 * messages, and so on, you must extend this service.
 */

public class OdigeoFirebaseMessagingService extends FirebaseMessagingService {

  private static final String LOCALYTICS_PUSH_FILTER = "ll";
  private static final String MTT_PUSH_FILTER = "text";

  @Override public void onMessageReceived(RemoteMessage remoteMessage) {
    Map<String, String> data = remoteMessage.getData();
    RemoteMessage.Notification notification = remoteMessage.getNotification();

    // Integration with Localytics v4 through Firebase
    if (data != null && data.containsKey(LOCALYTICS_PUSH_FILTER)) {
      // Localytics notification
      Localytics.displayPushNotification(convertMap(data));
    } else if (data != null && data.containsKey(MTT_PUSH_FILTER)) {
      // MTT notifications
      String message = data.get(MTT_PUSH_FILTER);
      showNotification(message);
    } else if (notification != null && !TextUtils.isEmpty(notification.getBody())) {
      // Firebase dashboard notification
      showNotification(notification.getBody());
    }
  }

  private void showNotification(String message) {
    OdigeoApp odigeoApp = (OdigeoApp) getApplicationContext();

    Intent mainIntent = new Intent(this, odigeoApp.getNavigationDrawerNavigator());
    PendingIntent launchIntent =
        PendingIntent.getActivity(this, 1, mainIntent, PendingIntent.FLAG_UPDATE_CURRENT);

    NotificationCompat.Builder builder =
        new NotificationCompat.Builder(this).setSmallIcon(getSmallIcon())
            .setContentTitle(Configuration.getInstance().getBrandVisualName())
            .setContentText(message)
            .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
            .setContentIntent(launchIntent)
            .setDefaults(Notification.DEFAULT_ALL)
            .setAutoCancel(true)
            .setColor(ContextCompat.getColor(this, R.color.accent_color));

    NotificationManager notificationManager =
        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    notificationManager.notify((int) Calendar.getInstance().getTimeInMillis(), builder.build());
  }

  private int getSmallIcon() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      return R.drawable.noticon;
    } else {
      return R.drawable.icon;
    }
  }

  private Bundle convertMap(Map<String, String> map) {
    Bundle bundle = new Bundle(map != null ? map.size() : 0);
    if (map != null) {
      for (Map.Entry<String, String> entry : map.entrySet()) {
        bundle.putString(entry.getKey(), entry.getValue());
      }
    }

    return bundle;
  }
}
