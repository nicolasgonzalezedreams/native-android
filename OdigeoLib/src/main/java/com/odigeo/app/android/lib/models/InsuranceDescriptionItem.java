package com.odigeo.app.android.lib.models;

import java.io.Serializable;

/**
 * Created by ManuelOrtiz on 11/09/2014.
 */
public class InsuranceDescriptionItem implements Serializable {
  private String description;
  private boolean isSubtitle;

  public InsuranceDescriptionItem(String description, boolean isSubtitle) {
    this.description = description;
    this.isSubtitle = isSubtitle;
  }

  public final String getDescription() {
    return description;
  }

  public final void setDescription(String description) {
    this.description = description;
  }

  public final boolean isSubtitle() {
    return isSubtitle;
  }

  public final void setSubtitle(boolean isSubtitle) {
    this.isSubtitle = isSubtitle;
  }
}
