package com.odigeo.app.android.lib.pojos.banktransfer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.CustomTextView;
import com.odigeo.app.android.lib.utils.StyledBoldString;
import java.lang.ref.WeakReference;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 27/04/15
 */
public class BanktransferSimple extends IncompleteBookingBean {
  @Override public View inflate(@NonNull Context context, @NonNull ViewGroup parent) {
    View view =
        LayoutInflater.from(context).inflate(R.layout.incomplete_booking_item, parent, false);
    CustomTextView tv = ((CustomTextView) view.findViewById(R.id.text));
    String boldString = LocalizablesFacade.getRawString(context,
        "confirmation_howtocompletebooking_deposit_bankimportant");
    Spannable spannable = new StyledBoldString(new WeakReference<>(context), boldString,
        R.style.bankTransferBlueBoldText).getSpannable();
    tv.setText(spannable, TextView.BufferType.SPANNABLE);
    return view;
  }
}
