package com.odigeo.app.android.lib.config;

import com.odigeo.app.android.lib.consts.DistanceUnits;
import com.odigeo.app.android.lib.models.SpinnerItemModel;
import com.odigeo.app.android.lib.models.dto.BuyerIdentificationTypeDTO;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.app.android.lib.models.dto.ResidentGroupDTO;
import com.odigeo.app.android.lib.models.dto.TravellerGenderDTO;
import com.odigeo.app.android.lib.models.dto.TravellerIdentificationTypeDTO;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by manuel on 25/10/14.
 */
public final class Lists {

  private static Lists instance;
  private final List<DistanceUnits> distanceUnits;
  private List<CabinClassDTO> cabinClasses;
  //    @Deprecated
  //    private final List<SpinnerItemModel<TravellerTypeDTO>> travellerTypes;
  //    @Deprecated
  //    private final List<SpinnerItemModel<TravellerTitleDTO>> travellerTitles;
  @Deprecated private List<SpinnerItemModel<TravellerGenderDTO>> genders;

  private Lists() {
    //        travellerTypes = new ArrayList<SpinnerItemModel<TravellerTypeDTO>>();
    //        getTravellerTypes()
    //            .add(new SpinnerItemModel<TravellerTypeDTO>(TravellerTypeDTO.ADULT, "common_adult"));
    //        getTravellerTypes()
    //            .add(new SpinnerItemModel<TravellerTypeDTO>(TravellerTypeDTO.CHILD, "common_child"));
    //        getTravellerTypes()
    //            .add(new SpinnerItemModel<TravellerTypeDTO>(TravellerTypeDTO.INFANT, "common_infant"));
    //
    //        travellerTitles = new ArrayList<SpinnerItemModel<TravellerTitleDTO>>();
    //        getTravellerTitles()
    //            .add(new SpinnerItemModel<TravellerTitleDTO>(TravellerTitleDTO.MR, "common_mr"));
    //        getTravellerTitles()
    //            .add(new SpinnerItemModel<TravellerTitleDTO>(TravellerTitleDTO.MRS, "common_mrs"));
    //        getTravellerTitles()
    //            .add(new SpinnerItemModel<TravellerTitleDTO>(TravellerTitleDTO.MS, "common_ms"));

    setGenders(new ArrayList<SpinnerItemModel<TravellerGenderDTO>>());
    getGenders().add(
        new SpinnerItemModel<TravellerGenderDTO>(TravellerGenderDTO.MALE, "common_male"));
    getGenders().add(
        new SpinnerItemModel<TravellerGenderDTO>(TravellerGenderDTO.FEMALE, "common_female"));
    List<SpinnerItemModel<TravellerIdentificationTypeDTO>> identificationTypes =
        new ArrayList<SpinnerItemModel<TravellerIdentificationTypeDTO>>();
    identificationTypes.add(new SpinnerItemModel<TravellerIdentificationTypeDTO>(
        TravellerIdentificationTypeDTO.PASSPORT,
        "msltravellerinformationdescription_identificationtypepassport"));

    identificationTypes.add(
        new SpinnerItemModel<TravellerIdentificationTypeDTO>(TravellerIdentificationTypeDTO.NIE,
            "msltravellerinformationdescription_identificationtypenie"));

    identificationTypes.add(
        new SpinnerItemModel<TravellerIdentificationTypeDTO>(TravellerIdentificationTypeDTO.NIF,
            "msltravellerinformationdescription_identificationtypenif"));

    identificationTypes.add(new SpinnerItemModel<TravellerIdentificationTypeDTO>(
        TravellerIdentificationTypeDTO.NATIONAL_ID_CARD,
        "msltravellerinformationdescription_identificationtypenational_id_card"));

    identificationTypes.add(new SpinnerItemModel<TravellerIdentificationTypeDTO>(
        TravellerIdentificationTypeDTO.BIRTH_DATE, "datepicker_birthdate"));

    //identificationTypes.add(new SpinnerItemModel<TravellerIdentificationTypeDTO>(
    //        TravellerIdentificationTypeDTO.¿?,
    //        "msltravellerinformationdescription_identificationcif"));

    distanceUnits = new ArrayList<>();
    distanceUnits.add(DistanceUnits.KILOMETERS);
    distanceUnits.add(DistanceUnits.MILES);
  }

  public static synchronized Lists getInstance() {
    if (instance == null) {
      instance = new Lists();
    }
    return instance;
  }

  public List<CabinClassDTO> getCabinClasses() {
    return cabinClasses;
  }

  public void setCabinClasses(List<CabinClassDTO> cabinClasses) {
    this.cabinClasses = cabinClasses;
  }

  //    @Deprecated
  //    public List<SpinnerItemModel<TravellerTypeDTO>> getTravellerTypes() {
  //        return travellerTypes;
  //    }

  //    @Deprecated
  //    public List<SpinnerItemModel<TravellerTitleDTO>> getTravellerTitles() {
  //        return travellerTitles;
  //    }

  @Deprecated public List<SpinnerItemModel<TravellerGenderDTO>> getGenders() {
    return genders;
  }

  @Deprecated public void setGenders(List<SpinnerItemModel<TravellerGenderDTO>> genders) {
    this.genders = genders;
  }

  public List<DistanceUnits> getDistanceUnits() {
    return distanceUnits;
  }

  public void setTravellerIdentificationStringId(
      SpinnerItemModel<TravellerIdentificationTypeDTO> identificationTypeDTO) {
    if (identificationTypeDTO != null && identificationTypeDTO.getItem() != null) {

      if (identificationTypeDTO.getItem() == TravellerIdentificationTypeDTO.BIRTH_DATE) {
        identificationTypeDTO.setStringKey("datepicker_birthdate");
      } else if (identificationTypeDTO.getItem() == TravellerIdentificationTypeDTO.NIE) {
        identificationTypeDTO.setStringKey(
            "msltravellerinformationdescription_identificationtypenie");
      } else if (identificationTypeDTO.getItem() == TravellerIdentificationTypeDTO.NIF) {
        identificationTypeDTO.setStringKey(
            "msltravellerinformationdescription_identificationtypenif");
      } else if (identificationTypeDTO.getItem()
          == TravellerIdentificationTypeDTO.NATIONAL_ID_CARD) {
        identificationTypeDTO.setStringKey(
            "msltravellerinformationdescription_identificationtypenational_id_card");
      } else if (identificationTypeDTO.getItem() == TravellerIdentificationTypeDTO.PASSPORT) {
        identificationTypeDTO.setStringKey(
            "msltravellerinformationdescription_identificationtypepassport");
      }
    }
  }

  public void setResidentGroupStringId(SpinnerItemModel<ResidentGroupDTO> residentGroup) {

    if (residentGroup != null && residentGroup.getItem() != null) {
      if (residentGroup.getItem() == ResidentGroupDTO.CANARIAS) {
        residentGroup.setStringKey("mslresidentgroupcontainer_residentgroupnamecanaryislands");
      } else if (residentGroup.getItem() == ResidentGroupDTO.BALEARES) {
        residentGroup.setStringKey("mslresidentgroupcontainer_residentgroupnamebalearicislands");
      } else if (residentGroup.getItem() == ResidentGroupDTO.CEUTA) {
        residentGroup.setStringKey("mslresidentgroupcontainer_residentgroupnameceuta");
      } else if (residentGroup.getItem() == ResidentGroupDTO.MELILLA) {
        residentGroup.setStringKey("mslresidentgroupcontainer_residentgroupnamemelilla");
      }
    }
  }

  @Deprecated public void setBuyerIdentificationStringId(
      SpinnerItemModel<BuyerIdentificationTypeDTO> identificationTypeDTO) {
    if (identificationTypeDTO != null && identificationTypeDTO.getItem() != null) {

      if (identificationTypeDTO.getItem() == BuyerIdentificationTypeDTO.BIRTH_DATE) {
        identificationTypeDTO.setStringKey("datepicker_birthdate");
      } else if (identificationTypeDTO.getItem() == BuyerIdentificationTypeDTO.CIF) {
        identificationTypeDTO.setStringKey("msltravellerinformationdescription_identificationcif");
      } else if (identificationTypeDTO.getItem() == BuyerIdentificationTypeDTO.NIE) {
        identificationTypeDTO.setStringKey(
            "msltravellerinformationdescription_identificationtypenie");
      } else if (identificationTypeDTO.getItem() == BuyerIdentificationTypeDTO.NIF) {
        identificationTypeDTO.setStringKey(
            "msltravellerinformationdescription_identificationtypenif");
      } else if (identificationTypeDTO.getItem() == BuyerIdentificationTypeDTO.NATIONAL_ID_CARD) {
        identificationTypeDTO.setStringKey(
            "msltravellerinformationdescription_identificationtypenational_id_card");
      } else if (identificationTypeDTO.getItem() == BuyerIdentificationTypeDTO.PASSPORT) {
        identificationTypeDTO.setStringKey(
            "msltravellerinformationdescription_identificationtypepassport");
      }
    }
  }
}
