package com.odigeo.app.android.view.helpers;

public interface PermissionListener {

  void permissionAccepted();
}
