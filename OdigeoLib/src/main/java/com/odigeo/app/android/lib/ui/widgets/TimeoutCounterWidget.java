package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.interfaces.ITimeoutWidget;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.dataodigeo.net.helper.semaphore.UIAutomationSemaphore;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ximena.perez on 20/02/2015.
 */

public class TimeoutCounterWidget extends LinearLayout {

  private static final int SECONDS = 60;
  private static final int MILLISECONDS = 1000;
  TextView timeTextView;
  TextView titleTextView;
  CountDownTimer timer;
  Context context;
  int time;
  private ITimeoutWidget listener;

  public TimeoutCounterWidget(final Context context, AttributeSet attrs) {
    super(context, attrs);
    this.context = context;
    time = 0;
    View viewParent =
        LayoutInflater.from(context).inflate(R.layout.layout_timeout_counter_widget, this);
    timeTextView = (TextView) viewParent.findViewById(R.id.text_timeout_counter_time);
    titleTextView = (TextView) viewParent.findViewById(R.id.text_timeout_counter_title);
    ImageView cross = (ImageView) viewParent.findViewById(R.id.icon_cross);
    if (context instanceof ITimeoutWidget) {
      this.listener = (ITimeoutWidget) context;
    }
    cross.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        TimeoutCounterWidget.this.setVisibility(GONE);

        if (listener != null) {
          listener.setWidgetClosed();
        }
      }
    });
  }

  /**
   * This method sets the minutes on the timer and initializes the timer
   *
   * @param minutes Represents the total minutes to be applied on the timer countdown
   * Represents the total minutes to be shown on the widget
   */
  public final void setMinutes(int seconds, int minutes) {
    String message =
        LocalizablesFacade.getString(getContext(), "odgtimeoutcounterview_messagelabel_text",
            minutes + ":00").toString();
    CharSequence styledMessage = HtmlUtils.formatHtml(message);
    timeTextView.setText((seconds / 60) + ":" + (seconds % 60));
    titleTextView.setText(styledMessage);

    startTimer(seconds);
  }

  /**
   * This method starts the timer without displaying the widget
   *
   * @param seconds Seconds from which the timer will be started
   */
  public void startTimer(int seconds) {
    UIAutomationSemaphore.getInstance().idleUp();
    if (timer == null) {
      timer = new CountDownTimer(seconds * 1000, 1000) {
        public void onTick(long millisUntilFinished) {
          if (timeTextView.getVisibility() == View.VISIBLE) {
            Date date = new Date(millisUntilFinished);
            DateFormat formatter = new SimpleDateFormat("m:ss", Locale.getDefault());
            String dateFormatted = formatter.format(date);
            timeTextView.setText(dateFormatted);
          }
          if (millisUntilFinished != 0) {
            time = (int) millisUntilFinished / 1000;
          } else {
            time = 0;
          }
        }

        public void onFinish() {
          if (timeTextView.getVisibility() == View.VISIBLE) {
            timeTextView.setText("0:00");
            TimeoutCounterWidget.this.setVisibility(GONE);
          }
          time = 0;
          if (listener != null) {
            listener.showErrorDialog();
          }
          UIAutomationSemaphore.getInstance().idleDown();
        }
      }.start();
    }
  }

  /**
   * This method cancels the timer, in cases such as the user canceling the widget, or pressing
   * back
   */
  public final void cancelTimer() {
    if (timer != null) {
      timer.cancel();
    }
  }

  @Override protected final void onVisibilityChanged(View changedView, int visibility) {

    if (changedView.equals(this) && visibility == VISIBLE) {
      Animation bottomUp = AnimationUtils.loadAnimation(TimeoutCounterWidget.this.getContext(),
          R.anim.timeout_counter_widget_up);

      this.startAnimation(bottomUp);
    }

    if (changedView.equals(this) && visibility == GONE) {
      Animation bottomDown = AnimationUtils.loadAnimation(TimeoutCounterWidget.this.getContext(),
          R.anim.timeout_counter_widget_down);
      this.startAnimation(bottomDown);
    }

    super.onVisibilityChanged(changedView, visibility);
  }

  public int getTime() {
    return time;
  }

  public void setListener(ITimeoutWidget listener) {
    this.listener = listener;
  }
}
