package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public enum BaggageApplicationLevelDTO implements Serializable {

  SECTION, SEGMENT, ITINERARY;

  public static BaggageApplicationLevelDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }
}
