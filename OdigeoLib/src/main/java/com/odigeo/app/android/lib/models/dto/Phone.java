package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class Phone implements Serializable {
  protected String countryCode;
  protected String number;

  public final String getCountryCode() {
    return countryCode;
  }

  public final void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public final String getNumber() {
    return number;
  }

  public final void setNumber(String number) {
    this.number = number;
  }
}
