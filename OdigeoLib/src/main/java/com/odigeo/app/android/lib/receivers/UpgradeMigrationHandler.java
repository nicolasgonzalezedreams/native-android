package com.odigeo.app.android.lib.receivers;

import android.content.Context;
import android.util.Log;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.database.LocalizablesDBManager;
import com.odigeo.app.android.lib.mappers.StoredSearchOptionsMapper;
import com.odigeo.app.android.lib.utils.DeviceUtils;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.data.db.dao.StoredSearchDBDAOInterface;
import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import java.util.List;

public class UpgradeMigrationHandler {
  private static final int LAST_SEARCH_HISTORY_WITHOUT_DB_VERSION = 3604;

  //TODO: We include this code in v2.5.0. Delete when all user has been migrated
  public static void upgradeFromOldVersions(Context context) {
    int previousVersionCode = PreferencesManager.readLastAppVersionCode(context);
    int currentVersionCode = Integer.parseInt(DeviceUtils.getApplicationVersionCode(context));

    upgradeLocalizablesContent(context, previousVersionCode, currentVersionCode);
    upgradeSearchHistory(context, previousVersionCode);
  }

  private static void upgradeLocalizablesContent(Context context, int previousVersionCode,
      int currentVersionCode) {
    Log.d(Constants.TAG_LOG_UPGRADE, "upgradeLocalizablesContent="
        + previousVersionCode
        + " currentVersionCode="
        + currentVersionCode);
    if (currentVersionCode > previousVersionCode) {
      Log.d(Constants.TAG_LOG_UPGRADE, "Updating localizables.db");
      LocalizablesDBManager localizablesDBManager = new LocalizablesDBManager(context);
      localizablesDBManager.updateDatabase();
    }
  }

  private static void upgradeSearchHistory(Context context, int previousVersionCode) {
    Log.d(Constants.TAG_LOG_UPGRADE,
        "migrating history search from previousVersionCode=" + previousVersionCode + " to DB");
    PreferencesControllerInterface preferencesController =
        AndroidDependencyInjector.getInstance().providePreferencesController();
    StoredSearchOptionsMapper storedSearchOptionsMapper =
        AndroidDependencyInjector.getInstance().provideStoredSearchOptionsMapper();

    if (!preferencesController.getBooleanValue(
        PreferencesControllerInterface.RECENT_SEARCHES_MIGRATED)
        && previousVersionCode <= LAST_SEARCH_HISTORY_WITHOUT_DB_VERSION) {

      SearchesHandlerInterface searchesHandler =
          AndroidDependencyInjector.getInstance().provideSearchesHandler();
      //init DB in order to trigger OnUpgrade
      StoredSearchDBDAOInterface storedSearchDBDAO =
          AndroidDependencyInjector.getInstance().provideStoredSearchesDBDao();
      ((OdigeoSQLiteOpenHelper) storedSearchDBDAO).getOdigeoDatabase();

      List<SearchOptions> searchOptions =
          PreferencesManager.readFlights(context, Constants.PREFERENCE_HISTORY_SEARCH);
      searchOptions.addAll(
          PreferencesManager.readFlights(context, Constants.PREFERENCE_HISTORY_SEARCH_MD));

      for (SearchOptions searchOption : searchOptions) {
        try {
          StoredSearch convertedStoredSearch =
              storedSearchOptionsMapper.searchOptionToStoredSearchMapper(searchOption);
          searchesHandler.saveStoredSearch(convertedStoredSearch);
        } catch (NullPointerException ex) {
          ex.printStackTrace();
        }
      }

      preferencesController.clearHistorySearch();
      preferencesController.saveBooleanValue(
          PreferencesControllerInterface.RECENT_SEARCHES_MIGRATED, true);
    }
  }
}
