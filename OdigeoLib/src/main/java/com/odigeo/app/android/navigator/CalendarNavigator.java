package com.odigeo.app.android.navigator;

import android.content.Intent;
import android.os.Bundle;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.view.CalendarView;
import com.odigeo.data.entity.FlightsDirection;
import com.odigeo.data.entity.TravelType;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.contracts.navigators.CalendarNavigatorInterface;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CalendarNavigator extends BaseNavigator implements CalendarNavigatorInterface {

  private CalendarView calendarView;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view);

    FlightsDirection flightsDirection =
        (FlightsDirection) getIntent().getSerializableExtra(Constants.EXTRA_FLIGHT_DIRECTION);

    calendarView =
        CalendarView.newInstance(getIntent().getIntExtra(Constants.EXTRA_FLIGHT_SEGMENT_NUMBER, 0),
            (TravelType) getIntent().getSerializableExtra(Constants.EXTRA_TRAVEL_TYPE),
            flightsDirection,
            (ArrayList<Date>) getIntent().getSerializableExtra(Constants.EXTRA_SELECTED_DATES));

    replaceFragment(R.id.fl_container, calendarView);

    getSupportActionBar().hide();
  }

  @Override public void setResultAndFinish(int segmentNumber, TravelType travelType,
      List<Date> selectedDates) {
    Intent returnIntent = new Intent();
    returnIntent.putExtra(Constants.EXTRA_FLIGHT_SEGMENT_NUMBER, segmentNumber);
    returnIntent.putExtra(Constants.EXTRA_SELECTED_DATES, (ArrayList) selectedDates);
    returnIntent.putExtra(Constants.EXTRA_TRAVEL_TYPE, travelType);

    setResult(RESULT_OK, returnIntent);
    finish();
  }

  @Override public void onBackPressed() {
    super.onBackPressed();
    tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHT_SEARCH,
        TrackerConstants.ACTION_SEARCHER_FLIGHTS, TrackerConstants.LABEL_CALENDAR_BACK);
  }
}
