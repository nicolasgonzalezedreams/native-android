package com.odigeo.app.android.lib.models.dto;

public class ShoppingCartBookingRequestDTO {

  protected CreditCardCollectionDetailsParametersRequestDTO creditCardRequest;
  protected UserInteractionNeededRequestDTO userInteractionNeededRequest;
  protected CollectionMethodTypeDTO collectionMethodType;
  protected String bookingUserComment;

  /**
   * Gets the value of the creditCardRequest property.
   *
   * @return possible object is {@link com.odigeo.app.android.lib.models.dto.CreditCardCollectionDetailsParametersRequestDTO
   * }
   */
  public CreditCardCollectionDetailsParametersRequestDTO getCreditCardRequest() {
    return creditCardRequest;
  }

  /**
   * Sets the value of the creditCardRequest property.
   *
   * @param value allowed object is {@link com.odigeo.app.android.lib.models.dto.CreditCardCollectionDetailsParametersRequestDTO
   * }
   */
  public void setCreditCardRequest(CreditCardCollectionDetailsParametersRequestDTO value) {
    this.creditCardRequest = value;
  }

  /**
   * Gets the value of the userInteractionNeededRequest property.
   *
   * @return possible object is {@link com.odigeo.app.android.lib.models.dto.UserInteractionNeededRequestDTO
   * }
   */
  public UserInteractionNeededRequestDTO getUserInteractionNeededRequest() {
    return userInteractionNeededRequest;
  }

  /**
   * Sets the value of the userInteractionNeededRequest property.
   *
   * @param value allowed object is {@link com.odigeo.app.android.lib.models.dto.UserInteractionNeededRequestDTO
   * }
   */
  public void setUserInteractionNeededRequest(UserInteractionNeededRequestDTO value) {
    this.userInteractionNeededRequest = value;
  }

  /**
   * Gets the value of the collectionMethodType property.
   *
   * @return possible object is {@link com.odigeo.app.android.lib.models.dto.CollectionMethodTypeDTO
   * }
   */
  public CollectionMethodTypeDTO getCollectionMethodType() {
    return collectionMethodType;
  }

  /**
   * Sets the value of the collectionMethodType property.
   *
   * @param value allowed object is {@link com.odigeo.app.android.lib.models.dto.CollectionMethodTypeDTO
   * }
   */
  public void setCollectionMethodType(CollectionMethodTypeDTO value) {
    this.collectionMethodType = value;
  }

  /**
   * Gets the value of the bookingUserComment property.
   *
   * @return possible object is {@link String }
   */
  public String getBookingUserComment() {
    return bookingUserComment;
  }

  /**
   * Sets the value of the bookingUserComment property.
   *
   * @param value allowed object is {@link String }
   */
  public void setBookingUserComment(String value) {
    this.bookingUserComment = value;
  }
}
