package com.odigeo.app.android.view.interfaces;

import android.support.design.widget.TextInputLayout;

public interface TextWatcherValidationsListener {
  void onCharacterWriteOnField(boolean informationIsCorrectboolean,
      TextInputLayout textInputLayout);
}
