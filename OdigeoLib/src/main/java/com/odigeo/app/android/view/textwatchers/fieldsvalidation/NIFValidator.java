package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

public class NIFValidator extends BaseValidator implements FieldValidator {

  public static final String REGEX_NIF = "(\\d{8})([-]?)([A-Za-z]{1})";

  public NIFValidator() {
    super();
  }

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_NIF);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
