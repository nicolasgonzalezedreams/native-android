package com.odigeo.app.android.lib.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.ForegroundChecker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.data.tracker.TrackerControllerInterface;

import static com.odigeo.dataodigeo.tracker.TrackerConstants.BACK_EVENT;

/**
 * Base activity to save the state of the app and be possible to determine if the app is in the
 * foreground or not.
 *
 * @author Julio Kun
 * @since 24/06/15
 */
public abstract class OdigeoForegroundBaseActivity extends AppCompatActivity {

  protected TrackerControllerInterface mTracker;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mTracker = AndroidDependencyInjector.getInstance().provideTrackerController();
  }

  @Override protected void onResume() {
    super.onResume();
    setOnForeground(true);
    BusProvider.getInstance().register(this);
  }

  @Override protected void onPause() {
    super.onPause();
    setOnForeground(false);
    BusProvider.getInstance().unregister(this);
  }

  @Override public void onBackPressed() {
    super.onBackPressed();
    mTracker.trackAppseeEvent(BACK_EVENT);
  }

  private void setOnForeground(boolean onForeground) {
    ForegroundChecker foregroundChecker = ForegroundChecker.getInstance();
    foregroundChecker.setOnForeground(onForeground, this);
  }

  public abstract void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent);

  protected void startAppseeSession() {
    mTracker.startSession();
  }
}
