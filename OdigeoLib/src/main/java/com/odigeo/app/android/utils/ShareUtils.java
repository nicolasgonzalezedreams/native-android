package com.odigeo.app.android.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Parcelable;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.BaseShareModel;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gaston.mira on 7/25/16.
 */
public class ShareUtils {

  private static final String APP_PACKAGE_FACEBOOK = "com.facebook.katana";
  private static final String APP_PACKAGE_TWITTER = "twitter";
  private static final String APP_PACKAGE_FB_MESSENGER = "com.facebook.orca";
  private static final String APP_PACKAGE_COPY_TO_CLIPBOARD = "com.google.android.apps.docs";
  private static final String APP_PACKAGE_WHATSAPP = "com.whatsapp";
  private static final String APP_PACKAGE_SMS = "android.mms";
  private static final String APP_ACTIVITY_TWITTER =
      "com.twitter.android.composer.ComposerActivity";
  private static final String APP_ACTIVITY_COPY_CLIPBOARD =
      "com.google.android.apps.docs.drive.clipboard.SendTextToClipboardActivity";
  private static final String TEXT_PLAIN = "text/plain";
  private static final String TYPE_MESSAGE_RFC = "message/rfc822";
  private static final String DOUBLE_CARRIAGE_RETURN = "\n\n";
  private static ShareUtils instance = null;

  public static ShareUtils getInstance() {
    if (instance == null) {
      instance = new ShareUtils();
    }
    return instance;
  }

  public void share(Context context, BaseShareModel shareModel) {
    CharSequence shareDialogTitle =
        LocalizablesFacade.getString(context, OneCMSKeys.SHARE_BOOKING_DIALOG_TITLE);
    CharSequence message = "";
    String appName = Configuration.getInstance().getBrandVisualName();
    String storeUrl = Configuration.getInstance().getAppStoreURL();
    List<Intent> targetedShareIntents = new ArrayList<Intent>();
    Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
    shareIntent.setType(TEXT_PLAIN);
    Intent twitterIntent = new Intent(android.content.Intent.ACTION_SEND);
    twitterIntent.setType("application/twitter");
    List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(shareIntent, 0);

    if (!resInfo.isEmpty()) {
      for (ResolveInfo resolveInfo : resInfo) {
        String packageName = resolveInfo.activityInfo.packageName;
        /**
         * Just show the only apps I need to share
         */
        if (packageName.contains(APP_PACKAGE_FACEBOOK)
            || packageName.contains(APP_PACKAGE_FB_MESSENGER)
            || packageName.contains(APP_PACKAGE_SMS)
            || packageName.contains(APP_PACKAGE_WHATSAPP)) {

          Intent targetedShareIntent = new Intent(android.content.Intent.ACTION_SEND);
          targetedShareIntent.setType(TEXT_PLAIN);

          if (packageName.equals(APP_PACKAGE_FACEBOOK)) {
            message = storeUrl;
          } else if (packageName.contains(APP_PACKAGE_WHATSAPP)) {
            message = shareModel.getWhatsAppMessage();
          } else if (packageName.contains(APP_PACKAGE_SMS)) {
            message = shareModel.getSmsMessage();
          } else if (packageName.equals(APP_PACKAGE_FB_MESSENGER)) {
            message = shareModel.getFbMessengerMessage();
          }

          targetedShareIntent.putExtra(Intent.EXTRA_TEXT, message);
          targetedShareIntent.setPackage(packageName);
          targetedShareIntents.add(targetedShareIntent);
        } else if (packageName.contains(APP_PACKAGE_TWITTER)) {
          if (resolveInfo.activityInfo.name.equals(APP_ACTIVITY_TWITTER)) {
            message = shareModel.getTwitterMessage();
            twitterIntent.setPackage(packageName);
            twitterIntent.putExtra(Intent.EXTRA_TEXT, message);
            twitterIntent.setClassName(resolveInfo.activityInfo.packageName,
                resolveInfo.activityInfo.name);
            targetedShareIntents.add(twitterIntent);
          }
        } else if (packageName.contains(APP_PACKAGE_COPY_TO_CLIPBOARD)) {
          if (resolveInfo.activityInfo.name.equals(APP_ACTIVITY_COPY_CLIPBOARD)) {
            message = shareModel.getCopyToClipboardMessage();
            Intent copyIntent = new Intent(android.content.Intent.ACTION_SEND);
            copyIntent.setPackage(packageName);
            copyIntent.setType(TEXT_PLAIN);
            copyIntent.putExtra(Intent.EXTRA_TEXT, message);
            copyIntent.setClassName(resolveInfo.activityInfo.packageName,
                resolveInfo.activityInfo.name);
            targetedShareIntents.add(copyIntent);
          }
        }
      }

      /**
       * Now we add the apps that can share sending emails.
       */
      Intent emailIntent = new Intent(Intent.ACTION_SEND);
      emailIntent.setData(Uri.parse("mailto:"));
      String emailSubject = shareModel.getEmailSubject();
      emailIntent.putExtra(Intent.EXTRA_SUBJECT, emailSubject);
      emailIntent.putExtra(Intent.EXTRA_TEXT, shareModel.getEmailBodyMessage());
      emailIntent.setType(TYPE_MESSAGE_RFC);

      Intent chooserIntent = Intent.createChooser(emailIntent, shareDialogTitle);
      chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
          targetedShareIntents.toArray(new Parcelable[] {}));
      chooserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      context.startActivity(chooserIntent);
    }
  }
}
