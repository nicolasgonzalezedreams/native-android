package com.odigeo.app.android.lib.data;

import android.content.Context;
import java.util.HashMap;

/**
 * This class stores some strings brought from the database in a hash map. It avoids calling the
 * database many times for the same value. This is useful if we need to improve the performance in
 * certain sections or screens.
 *
 * @author Cristian Fanjul
 * @since 03/07/2015
 */
public class CommonLocalizables {

  private static final String COMMON_ADULT = "common_adult";
  private static final String COMMON_ADULTS = "common_adults";
  private static final String COMMON_CHILD = "common_child";
  private static final String COMMON_CHILDREN = "common_children";
  private static final String COMMON_INFANT = "common_infant";
  private static final String COMMON_INFANTS = "common_infants";

  private static final String COMMON_OUTBOUND = "common_outbound";
  private static final String COMMON_INBOUND = "common_inbound";
  private static final String COMMON_LEG = "common_leg";

  private static final String FULLPRICE_RESULTS_SELECTED = "fullprice_results_selected";
  private static final String FULLPRICE_RESULTS_DESCRIPTION = "fullprice_results_description";

  private static final String SEARCHVIEWCONTROLLER_PICKFROMLABEL_TEXT =
      "searchviewcontroller_pickfromlabel_text";
  private static final String SEARCHVIEWCONTROLLER_PICKTOLABEL_TEXT =
      "searchviewcontroller_picktolabel_text";
  private static final String SEARCHVIEWCONTROLLER_LABELDATEGO_TEXT =
      "searchviewcontroller_labeldatego_text";
  private static final String SEARCHVIEWCONTROLLER_LABELDATERETURN_TEXT =
      "searchviewcontroller_labeldatereturn_text";

  private static final String SCALESFILTERVIEWCONTROLLER_1_SCALES_TEXT =
      "scalesfilterviewcontroller_1scales_text";
  private static final String SCALESFILTERVIEWCONTROLLER_MORESCALES_TEXT =
      "scalesfilterviewcontroller_morescales_text";
  private static final String SCALESFILTERVIEWCONTROLLER_NOSCALES_TEXT =
      "scalesfilterviewcontroller_noscales_text";
  private static final String NORESULTSVIEWCONTROLLER_LABELSEATSLEFT =
      "noresultsviewcontroller_labelseatsleft";
  private static final String COMMON_OPERATEDBYTEXT = "common_operatedbytext";
  private static final String EXTRA_SCALES = "extra_scales";

  private static CommonLocalizables instance = new CommonLocalizables();
  private HashMap<String, CharSequence> map = new HashMap<>();

  public static CommonLocalizables getInstance() {
    return instance;
  }

  public CharSequence getCommonAdult(Context context) {
    if (map.get(COMMON_ADULT) == null) {
      map.put(COMMON_ADULT, LocalizablesFacade.getString(context, COMMON_ADULT));
    }
    return map.get(COMMON_ADULT);
  }

  public CharSequence getCommonAdults(Context context) {
    if (map.get(COMMON_ADULTS) == null) {
      map.put(COMMON_ADULTS, LocalizablesFacade.getString(context, COMMON_ADULTS));
    }
    return map.get(COMMON_ADULTS);
  }

  public CharSequence getCommonChild(Context context) {
    if (map.get(COMMON_CHILD) == null) {
      map.put(COMMON_CHILD, LocalizablesFacade.getString(context, COMMON_CHILD));
    }
    return map.get(COMMON_CHILD);
  }

  public CharSequence getCommonChildren(Context context) {
    if (map.get(COMMON_CHILDREN) == null) {
      map.put(COMMON_CHILDREN, LocalizablesFacade.getString(context, COMMON_CHILDREN));
    }
    return map.get(COMMON_CHILDREN);
  }

  public CharSequence getCommonInfant(Context context) {
    if (map.get(COMMON_INFANT) == null) {
      map.put(COMMON_INFANT, LocalizablesFacade.getString(context, COMMON_INFANT));
    }
    return map.get(COMMON_INFANT);
  }

  public CharSequence getCommonInfants(Context context) {
    if (map.get(COMMON_INFANTS) == null) {
      map.put(COMMON_INFANTS, LocalizablesFacade.getString(context, COMMON_INFANTS));
    }
    return map.get(COMMON_INFANTS);
  }

  public CharSequence getCommonOutbound(Context context) {
    if (map.get(COMMON_OUTBOUND) == null) {
      map.put(COMMON_OUTBOUND, LocalizablesFacade.getString(context, COMMON_OUTBOUND));
    }
    return map.get(COMMON_OUTBOUND);
  }

  public CharSequence getCommonInbound(Context context) {
    if (map.get(COMMON_INBOUND) == null) {
      map.put(COMMON_INBOUND, LocalizablesFacade.getString(context, COMMON_INBOUND));
    }
    return map.get(COMMON_INBOUND);
  }

  public CharSequence getCommonLeg(Context context) {
    if (map.get(COMMON_LEG) == null) {
      map.put(COMMON_LEG, LocalizablesFacade.getString(context, COMMON_LEG));
    }
    return map.get(COMMON_LEG);
  }

  public CharSequence getFullpriceResultsSelected(Context context) {
    if (map.get(FULLPRICE_RESULTS_SELECTED) == null) {
      map.put(FULLPRICE_RESULTS_SELECTED,
          LocalizablesFacade.getString(context, FULLPRICE_RESULTS_SELECTED));
    }
    return map.get(FULLPRICE_RESULTS_SELECTED);
  }

  public CharSequence getFullpriceResultsDescription(Context context) {
    if (map.get(FULLPRICE_RESULTS_DESCRIPTION) == null) {
      map.put(FULLPRICE_RESULTS_DESCRIPTION,
          LocalizablesFacade.getRawString(context, FULLPRICE_RESULTS_DESCRIPTION));
    }
    return map.get(FULLPRICE_RESULTS_DESCRIPTION);
  }

  public CharSequence getSearchviewcontrollerPickfromlabelText(Context context) {
    if (map.get(SEARCHVIEWCONTROLLER_PICKFROMLABEL_TEXT) == null) {
      map.put(SEARCHVIEWCONTROLLER_PICKFROMLABEL_TEXT,
          LocalizablesFacade.getString(context, SEARCHVIEWCONTROLLER_PICKFROMLABEL_TEXT));
    }
    return map.get(SEARCHVIEWCONTROLLER_PICKFROMLABEL_TEXT);
  }

  public CharSequence getSearchviewcontrollerPicktolabelText(Context context) {
    if (map.get(SEARCHVIEWCONTROLLER_PICKTOLABEL_TEXT) == null) {
      map.put(SEARCHVIEWCONTROLLER_PICKTOLABEL_TEXT,
          LocalizablesFacade.getString(context, SEARCHVIEWCONTROLLER_PICKTOLABEL_TEXT));
    }
    return map.get(SEARCHVIEWCONTROLLER_PICKTOLABEL_TEXT);
  }

  public CharSequence getSearchviewcontrollerLabeldategoText(Context context) {
    if (map.get(SEARCHVIEWCONTROLLER_LABELDATEGO_TEXT) == null) {
      map.put(SEARCHVIEWCONTROLLER_LABELDATEGO_TEXT,
          LocalizablesFacade.getString(context, SEARCHVIEWCONTROLLER_LABELDATEGO_TEXT));
    }
    return map.get(SEARCHVIEWCONTROLLER_LABELDATEGO_TEXT);
  }

  public CharSequence getSearchviewcontrollerLabeldatereturnText(Context context) {
    if (map.get(SEARCHVIEWCONTROLLER_LABELDATERETURN_TEXT) == null) {
      map.put(SEARCHVIEWCONTROLLER_LABELDATERETURN_TEXT,
          LocalizablesFacade.getString(context, SEARCHVIEWCONTROLLER_LABELDATERETURN_TEXT));
    }
    return map.get(SEARCHVIEWCONTROLLER_LABELDATERETURN_TEXT);
  }

  public CharSequence getCommonOperatedbytext(Context context) {
    if (map.get(COMMON_OPERATEDBYTEXT) == null) {
      map.put(COMMON_OPERATEDBYTEXT, LocalizablesFacade.getString(context, COMMON_OPERATEDBYTEXT));
    }
    return map.get(COMMON_OPERATEDBYTEXT);
  }

  public CharSequence getScalesfilterviewcontroller1scalesText(Context context) {
    if (map.get(SCALESFILTERVIEWCONTROLLER_1_SCALES_TEXT) == null) {
      map.put(SCALESFILTERVIEWCONTROLLER_1_SCALES_TEXT,
          LocalizablesFacade.getString(context, SCALESFILTERVIEWCONTROLLER_1_SCALES_TEXT));
    }
    return map.get(SCALESFILTERVIEWCONTROLLER_1_SCALES_TEXT);
  }

  public CharSequence getScalesfilterviewcontrollerMorescalesText(Context context) {
    if (map.get(SCALESFILTERVIEWCONTROLLER_MORESCALES_TEXT) == null) {
      map.put(SCALESFILTERVIEWCONTROLLER_MORESCALES_TEXT,
          LocalizablesFacade.getString(context, SCALESFILTERVIEWCONTROLLER_MORESCALES_TEXT));
    }
    return map.get(SCALESFILTERVIEWCONTROLLER_MORESCALES_TEXT);
  }

  public CharSequence getExtraScales(Context context) {
    if (map.get(EXTRA_SCALES) == null) {
      map.put(EXTRA_SCALES, LocalizablesFacade.getString(context, EXTRA_SCALES));
    }
    return map.get(EXTRA_SCALES);
  }

  public CharSequence getNoresultsviewcontrollerTabelseatsleft(Context context) {
    if (map.get(NORESULTSVIEWCONTROLLER_LABELSEATSLEFT) == null) {
      map.put(NORESULTSVIEWCONTROLLER_LABELSEATSLEFT,
          LocalizablesFacade.getRawString(context, NORESULTSVIEWCONTROLLER_LABELSEATSLEFT));
    }
    return map.get(NORESULTSVIEWCONTROLLER_LABELSEATSLEFT);
  }

  public CharSequence getScalesfilterviewcontrollerNoscalesText(Context context) {
    if (map.get(SCALESFILTERVIEWCONTROLLER_NOSCALES_TEXT) == null) {
      map.put(SCALESFILTERVIEWCONTROLLER_NOSCALES_TEXT,
          LocalizablesFacade.getString(context, SCALESFILTERVIEWCONTROLLER_NOSCALES_TEXT));
    }
    return map.get(SCALESFILTERVIEWCONTROLLER_NOSCALES_TEXT);
  }

  public void clearAll() {
    map.clear();
  }
}