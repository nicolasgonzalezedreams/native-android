package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.Util;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by luisalfonsobejaranosanchez on 22/09/14.
 */
public class FilterRangeBarDateWidget extends LinearLayout {
  //15 minutes
  // TODO requested by Manuel - validate this constant!
  private static final long INCREMENT = 900000;
  private final List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();
  protected TextView titleTextView;
  protected TextView subtitleTextView;
  private TextView textStartDate;
  private TextView textEndDate;
  private Date minDate;
  private Date maxDate;
  private Date startDateSelected;
  private Date endDateSelected;
  private RangeSeekBar<Long> seekBar;
  private int parentNumberWidget;
  private String travelMode;
  private boolean loading;

  public FilterRangeBarDateWidget(Context context) {
    super(context);
    inflateLayout();
  }

  public FilterRangeBarDateWidget(Context context, AttributeSet attrs) {
    super(context, attrs);

    setIsLoading(true);

    inflateLayout();
    setAttributes(attrs);
    initSeekBar();

    setIsLoading(false);
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.layout_filter_date_widget, this);
    titleTextView = (TextView) findViewById(R.id.text_filter_date_widget_title);
    subtitleTextView = (TextView) findViewById(R.id.text_filter_date_widget_subtittle);
    textStartDate = (TextView) findViewById(R.id.text_filter_date_widget_startDate);
    textEndDate = (TextView) findViewById(R.id.text_filter_date_widget_endDate);

    minDate = new Date();
    maxDate = new Date();

    startDateSelected = new Date();
    endDateSelected = new Date();
  }

  private void setAttributes(AttributeSet attributes) {
    if (attributes != null) {
      TypedArray typedArray =
          getContext().obtainStyledAttributes(attributes, R.styleable.FilterRangeBarDateWidget);
      String textTitle = typedArray.getString(R.styleable.FilterRangeBarDateWidget_textTitle);
      String textCity = typedArray.getString(R.styleable.FilterRangeBarDateWidget_textSubtitle);
      typedArray.recycle();

      titleTextView.setText(LocalizablesFacade.getString(getContext(), textTitle));
      setTextSubtitle(textCity);
    }
  }

  private void initSeekBar() {
    seekBar = new RangeSeekBar<Long>(getMinDate().getTime(), getMaxDate().getTime(), getContext());
    seekBar.setNotifyWhileDragging(true);

    seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Long>() {
      @Override
      public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Long minValue, Long maxValue) {
        long difMin = minValue - getMinDate().getTime();
        long difMax = maxValue - getMaxDate().getTime();
        long contMin = difMin / INCREMENT;
        long contMax = difMax / INCREMENT;
        startDateSelected = new Date(getMinDate().getTime() + (contMin * INCREMENT));
        setStringDate(textStartDate, startDateSelected);
        endDateSelected = new Date(getMaxDate().getTime() + (contMax * INCREMENT));
        setStringDate(textEndDate, endDateSelected);
      }
    });

    LinearLayout layoutForSeekBar =
        (LinearLayout) findViewById(R.id.layout_filter_date_widget_seekBar);
    layoutForSeekBar.removeAllViewsInLayout();
    layoutForSeekBar.addView(seekBar);
  }

  public final void setSubtitleText(String subtitleText) {
    setTextSubtitle(subtitleText);
  }

  private void setTextSubtitle(String subtitle) {
    subtitleTextView.setText(HtmlUtils.formatHtml(subtitle));
  }

  private void setStringDate(TextView textView, Date date) {
    //TODO: use format from class LocaleUtils
    SimpleDateFormat format =
        OdigeoDateUtils.getGmtDateFormat(Util.validationTimeFormatEU("HH:mm (dd MMM)"));
    String newDate = format.format(date);

    notifyListeners(this.getTravelMode(), textView.getText().toString(), newDate);
    textView.setText(newDate);
  }

  public final Date getMinDate() {
    return minDate;
  }

  private void setMinDate(Date minDate) {
    this.minDate = minDate;
    initSeekBar();
    setStartDateSelected(minDate);
  }

  public final void setDateMin(Date minDate) {
    setMinDate(minDate);
  }

  public final Date getMaxDate() {
    return maxDate;
  }

  private void setMaxDate(Date maxDate) {
    this.maxDate = maxDate;
    initSeekBar();
    setEndDateSelected(maxDate);
  }

  public final void setDateMax(Date maxDate) {
    setMaxDate(maxDate);
  }

  public final Date getStartDateSelected() {
    return startDateSelected;
  }

  private void setStartDateSelected(Date startDateSelected) {
    this.startDateSelected = startDateSelected;

    seekBar.setSelectedMinValue(startDateSelected.getTime());

    setStringDate(textStartDate, startDateSelected);
  }

  public final void setDateStartSelected(Date startDateSelected) {
    setStartDateSelected(startDateSelected);
  }

  public final Date getEndDateSelected() {
    return endDateSelected;
  }

  private void setEndDateSelected(Date endDateSelected) {
    this.endDateSelected = endDateSelected;

    seekBar.setSelectedMaxValue(endDateSelected.getTime());

    setStringDate(textEndDate, endDateSelected);
  }

  public final void setDateEndSelected(Date endDateSelected) {
    setEndDateSelected(endDateSelected);
  }

  public final int getParentNumberWidget() {
    return parentNumberWidget;
  }

  public final void setParentNumberWidget(int parentNumberWidget) {
    this.parentNumberWidget = parentNumberWidget;
  }

  public final String getTravelMode() {
    return travelMode;
  }

  public final void setTravelMode(String travelMode) {
    this.travelMode = travelMode;
  }

  public final boolean isLoading() {
    return loading;
  }

  private void setIsLoading(boolean loading) {
    this.loading = loading;
  }

  private void notifyListeners(String property, String oldValue, String newValue) {
    for (PropertyChangeListener name : listener) {
      name.propertyChange(new PropertyChangeEvent(this, property, oldValue, newValue));
    }
  }

  public final void addChangeListener(PropertyChangeListener newListener) {
    listener.add(newListener);
  }
}
