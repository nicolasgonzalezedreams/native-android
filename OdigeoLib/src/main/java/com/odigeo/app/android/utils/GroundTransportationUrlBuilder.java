package com.odigeo.app.android.utils;

import android.support.annotation.Nullable;
import com.odigeo.app.android.view.custom.GroundTransportationViewBase;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.tools.DateUtils;
import java.util.Calendar;

import static com.odigeo.tools.DateUtils.DATE_TIME_FORMAT;

public class GroundTransportationUrlBuilder {

  private final Booking booking;

  public GroundTransportationUrlBuilder(Booking booking) {
    this.booking = booking;
  }

  private String getStartAddress() {
    return booking.getArrivalAirportCode();
  }

  @Nullable private String getMode() {
    switch (booking.getTripType()) {
      case Booking.TRIP_TYPE_ONE_WAY:
        return GroundTransportationViewBase.TRIP_TYPE_OW;
      case Booking.TRIP_TYPE_ROUND_TRIP:
        return GroundTransportationViewBase.TRIP_TYPE_RT;
      case Booking.TRIP_TYPE_MULTI_SEGMENT:
        return GroundTransportationViewBase.TRIP_TYPE_MS;
      default:
        return null;
    }
  }

  private String getPickupDatetime() {
    return DateUtils.getDateFormatWithDiff(DATE_TIME_FORMAT, booking.getArrivalDate(0),
        Calendar.MINUTE, 30);
  }

  private String getPickUpReturnDatetime() {
    return DateUtils.getDateFormatWithDiff(DATE_TIME_FORMAT,
        booking.getLastSegment().getFirstSection().getDepartureDate(), Calendar.HOUR, -3);
  }

  private int getNumPassengers() {
    return booking.getTravellers().size();
  }

  public String build(String rawUrl) {
    return String.format(rawUrl, getMode(), getNumPassengers(), getStartAddress(),
        getPickupDatetime(), getPickUpReturnDatetime());
  }
}
