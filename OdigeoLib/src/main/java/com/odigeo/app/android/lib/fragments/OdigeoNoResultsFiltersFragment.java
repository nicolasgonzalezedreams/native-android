package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoSearchResultsActivity;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.interfaces.OdigeoInterfaces;
import com.odigeo.app.android.lib.interfaces.SearchTripListener;
import com.odigeo.app.android.lib.models.FiltersSelected;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.app.android.lib.ui.widgets.SearchTripWidget;
import com.odigeo.app.android.lib.ui.widgets.TopBriefWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.TravelType;

/**
 * Created by Irving on 26/10/2014.
 */
public class OdigeoNoResultsFiltersFragment extends android.support.v4.app.Fragment
    implements SearchTripListener, View.OnClickListener {

  private OdigeoApp odigeoApp;
  private OdigeoSession odigeoSession;

  private OdigeoSearchResultsActivity activity;
  private SearchTripWidget searchTripWidget;
  private SearchOptions searchOptions;
  private OdigeoInterfaces.SearchListener listener;

  private Button buttonRemoveFilters;

  @Override public void onAttach(Activity activity) {
    super.onAttach(activity);
    this.activity = (OdigeoSearchResultsActivity) activity;
    odigeoApp = (OdigeoApp) activity.getApplication();
    odigeoSession = odigeoApp.getOdigeoSession();
    postGAEventFilterNoResults();
  }

  @Override public void onCreate(Bundle savedInstaceSate) {
    super.onCreate(savedInstaceSate);
    searchOptions = ((OdigeoSearchResultsActivity) getActivity()).getSearchOptions();
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.layout_search_noresults_filter, container, false);

    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {
      searchTripWidget =
          (SearchTripWidget) view.findViewById(R.id.searchTripWidget_noresults_filter);
      buttonRemoveFilters = (Button) view.findViewById(R.id.button_noresults_filter_remove_filters);

      buttonRemoveFilters.setTypeface(Configuration.getInstance().getFonts().getRegular());
      buttonRemoveFilters.setText(LocalizablesFacade.getString(getActivity(),
          "noresultssearchorfilterscell_buttonresetfilters_text").toString());

      searchTripWidget.setSegments(searchOptions.getFlightSegments());
      searchTripWidget.setTravelType(searchOptions.getTravelType());
      searchTripWidget.setListener(this);

      buttonRemoveFilters.setOnClickListener(this);

      ((TopBriefWidget) view.findViewById(R.id.topbrief_noresults_filter)).addData(searchOptions);

      TextView tvResetFilter = (TextView) view.findViewById(R.id.tvResetFilter);
      tvResetFilter.setText(LocalizablesFacade.getString(getContext(),
          OneCMSKeys.NORESULTSSEARCHORFILTERSCELL_LABELRESETFILTERS_TEXT));

      TextView tvResetSearch = (TextView) view.findViewById(R.id.tvResetSearch);
      tvResetSearch.setText(LocalizablesFacade.getString(getContext(),
          OneCMSKeys.NORESULTSSEARCHORFILTERSCELL_LABELRESETSEARCH_TEXT));
    }
    return view;
  }

  @Override public void onStart() {
    super.onStart();
    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {
      if (searchOptions.getTravelType() == TravelType.SIMPLE) {
        searchTripWidget.hideReturnDate();
      }
    }
  }

  @Override public void onResume() {
    super.onResume();
    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {
      setControlsValues();
    }
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {

    super.onViewCreated(view, savedInstanceState);
    if (odigeoSession == null || !odigeoSession.isDataHasBeenLoaded()) {
      Util.sendToHome(activity, odigeoApp);
    }
  }

  public void setListener(OdigeoInterfaces.SearchListener listener) {
    this.listener = listener;
  }

  /**
   * Sets the values of the controls chosen by the user
   */
  public void setControlsValues() {
    FlightSegment departureSegment = searchOptions.getFlightSegments().get(0);

    if (searchOptions.getFlightSegments().size() > 1) {
      searchOptions.getFlightSegments().get(1);
    }

    //Set Departure City
    if (departureSegment.getDepartureCity() != null) {
      searchTripWidget.setCityFrom(departureSegment.getDepartureCity());
    }

    //Set Arrival City
    if (departureSegment.getArrivalCity() != null) {
      searchTripWidget.setCityTo(departureSegment.getArrivalCity());
    }

    //Set the departure Date
    if (departureSegment.getDate() != 0) {
      searchTripWidget.setDepartureDate(OdigeoDateUtils.createDate(departureSegment.getDate()));
    }

    //Set if it is a direct flight
    searchTripWidget.getChkDirectFlight().setChecked(searchOptions.isDirectFlight());

    //Set the cabin class selected
    searchTripWidget.getSpinnerClass().setSelection(searchOptions.getCabinClass());

    //Set the number of passengers selected
    searchTripWidget.setPassengers(this.searchOptions.getNumberOfAdults(),
        this.searchOptions.getNumberOfKids(), this.searchOptions.getNumberOfBabies());
    //Set the departure Date
    if (searchTripWidget.getTravelType() == TravelType.ROUND
        && searchOptions.getFlightSegments().get(1).getDate() != 0) {
      searchTripWidget.setReturnDate(
          OdigeoDateUtils.createDate(searchOptions.getFlightSegments().get(1).getDate()));
    }

    //Should be enabled the Search Flights Buttons?
    searchTripWidget.getBtnSearchFlight().setEnabled(isDataValid());
  }

  private boolean isDataValid() {
    FlightSegment segmentDeparture = searchOptions.getFlightSegments().get(0);
    if (searchTripWidget.getTravelType() == TravelType.SIMPLE && isFlightSegmentValid(
        segmentDeparture)) {
      return true;
    } else if (searchTripWidget.getTravelType() == TravelType.ROUND) {
      FlightSegment segmentReturn = searchOptions.getFlightSegments().get(1);

      if (segmentReturn.getDate() != 0 && isFlightSegmentValid(segmentReturn)) {
        return true;
      }
    }
    return false;
  }

  private boolean isFlightSegmentValid(FlightSegment segment) {
    if (segment.getDate() != 0
        && segment.getDepartureCity() != null
        && segment.getArrivalCity() != null
        && !segment.getDepartureCity()
        .getIataCode()
        .equalsIgnoreCase(segment.getArrivalCity().getIataCode())) {
      return true;
    }
    return false;
  }

  @Override public void onClickSearch(SearchTripWidget searchTripWidget, boolean isResident) {
    if (listener != null) {
      postGAEventNewSearch();
      activity.setFiltersSelected(new FiltersSelected());
      listener.onClickSearch(searchTripWidget, false);
    }
  }

  @Override public void onClickDirectFlight(boolean checked) {
    //just for implements
  }

  @Override public void onClickButtonPassenger() {
    //just for implements
  }

  @Override public void onCabinClassSelected(CabinClassDTO cabinClassSelected) {
    //just for implements
  }

  @Override public void onResidentSwitchClicked(boolean checked) {
    //just for implements
  }

  @Override public void onClick(View v) {
    if (v.getId() == buttonRemoveFilters.getId()) {
      resetFilters();
    }
  }

  /**
   * Resets the filters and shows the results screen.
   */
  private void resetFilters() {
    postGAEventNoResultsRemoveFilters();

    OdigeoSession odigeoSession = ((OdigeoApp) getActivity().getApplication()).getOdigeoSession();

    activity.setFiltersSelected(new FiltersSelected());
    odigeoSession.setItineraryResultsFiltered(odigeoSession.getItineraryResults());
    activity.recreateFragmentWithCollectionMethods();
  }

  private void postGAEventNoResultsRemoveFilters() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_RESULTS,
            GAnalyticsNames.ACTION_RESULTS_LIST, GAnalyticsNames.LABEL_NO_RESULTS_REMOVE_FILTERS));
  }

  private void postGAEventFilterNoResults() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_RESULTS,
            GAnalyticsNames.ACTION_RESULTS_LIST, GAnalyticsNames.LABEL_FILTER_NO_RESULTS));
  }

  private void postGAEventNewSearch() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_RESULTS,
            GAnalyticsNames.ACTION_RESULTS_LIST, GAnalyticsNames.LABEL_NO_RESULTS_NEW_SEARCH));
  }
}
