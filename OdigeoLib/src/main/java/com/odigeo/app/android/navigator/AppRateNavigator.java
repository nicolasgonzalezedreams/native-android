package com.odigeo.app.android.navigator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.view.AppRateView;
import com.odigeo.presenter.contracts.navigators.AppRateNavigatorInterface;

public class AppRateNavigator extends BaseNavigator implements AppRateNavigatorInterface {

  public static final String DESTINATION = "destination";
  public static final String DESTINATION_THANKS = "thanks";
  public static final String DESTINATION_HELP_IMPROVE = "help_improve";

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view);
    setUpToolbarButton(0);
    showAppRateView(savedInstanceState != null);
  }

  @Override public void navigateToThanksScreen() {
    Intent intent = new Intent(getApplicationContext(), AppRateFeedBackNavigator.class);
    intent.putExtra(DESTINATION, DESTINATION_THANKS);
    startActivity(intent);
  }

  @Override public void navigateToHelpImproveScreen() {
    Intent intent = new Intent(getApplicationContext(), AppRateFeedBackNavigator.class);
    intent.putExtra(DESTINATION, DESTINATION_HELP_IMPROVE);
    startActivity(intent);
  }

  @Override public void showAppRateView(boolean isRestoringState) {
    if (!isRestoringState) {
      getSupportFragmentManager().beginTransaction()
          .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
              android.R.anim.fade_in, android.R.anim.fade_out)
          .add(R.id.fl_container, AppRateView.newInstance())
          .commit();
    }
  }
}
