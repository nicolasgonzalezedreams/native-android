package com.odigeo.app.android.view.interfaces;

public interface MttCardBackground {

  String getBackgroundImage();
}
