package com.odigeo.app.android.utils.deeplinking.extractors;

import com.odigeo.app.android.lib.config.SearchOptions;

public class ExecuteSearchExtractor implements DeepLinkingExtractor {
  @Override public void extractParameter(SearchOptions.Builder builder, String key, String value) {
    builder.setExecuteSearch(Boolean.parseBoolean(value));
  }
}
