package com.odigeo.app.android.lib.models.dto;

import android.support.annotation.Nullable;
import com.odigeo.data.entity.BaseSpinnerItem;
import java.io.Serializable;

/**
 * <p>Java class for travellerTitle.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;simpleType name="travellerTitle">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MR"/>
 *     &lt;enumeration value="MRS"/>
 *     &lt;enumeration value="MS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
public enum TravellerTitleDTO implements Serializable, BaseSpinnerItem {
  MR("common_mr"), MRS("common_mrs"), MS("common_ms");

  private final String stringKey;

  /**
   * Constructor
   *
   * @param key String key that brings text from database.
   */
  TravellerTitleDTO(String key) {
    this.stringKey = key;
  }

  public static TravellerTitleDTO fromValue(String v) {
    return valueOf(v);
  }

  public String getStringKey() {
    return stringKey;
  }

  public String value() {
    return name();
  }

  @Override public String getShownTextKey() {
    return stringKey;
  }

  @Override public int getImageId() {
    return BaseSpinnerItem.EMPTY_RESOURCE;
  }

  @Override public String getShownText() {
    return null;
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public Object getData() {
    return null;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }
}
