package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.models.SpinnerItemModel;
import com.odigeo.app.android.lib.models.dto.TravellerTypeDTO;
import java.util.List;

/**
 * Created by manuel on 12/10/14.
 *
 * @deprecated Use {@link OdigeoSpinnerAdapter} instead
 */
@Deprecated public class SpinnerTravellerTypeAdapter
    extends ArrayAdapter<SpinnerItemModel<TravellerTypeDTO>> {
  private final List<SpinnerItemModel<TravellerTypeDTO>> travellerTypes;
  private final Context context;

  public SpinnerTravellerTypeAdapter(Context context,
      List<SpinnerItemModel<TravellerTypeDTO>> travellerTypes) {
    super(context, R.layout.layout_spinner_simple_text, travellerTypes);
    this.context = context;
    this.travellerTypes = travellerTypes;
  }

  @Override public int getCount() {
    return travellerTypes.size();
  }

  @Override public SpinnerItemModel<TravellerTypeDTO> getItem(int position) {
    return travellerTypes.get(position);
  }

  @Override public long getItemId(int position) {
    return position;
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {
    View view = LayoutInflater.from(context).inflate(R.layout.layout_spinner_simple_text, null);
    TextView textView = (TextView) view.findViewById(R.id.textView_card_name_and_icon);
    textView.setTypeface(Configuration.getInstance().getFonts().getRegular());
    textView.setText(travellerTypes.get(position).getText());
    return view;
  }

  @Override public View getDropDownView(int position, View convertView, ViewGroup parent) {
    View view =
        LayoutInflater.from(context).inflate(R.layout.layout_spinner_drop_simple_text, null);
    TextView textView = (TextView) view.findViewById(R.id.textView_card_name_and_icon);
    textView.setTypeface(Configuration.getInstance().getFonts().getRegular());
    textView.setText(travellerTypes.get(position).getText());
    return view;
  }
}
