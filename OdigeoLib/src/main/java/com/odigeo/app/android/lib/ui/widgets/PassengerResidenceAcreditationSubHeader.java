package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.utils.HtmlUtils;

/**
 * Created by Oscar Álvarez on 23/01/2015.
 */
public class PassengerResidenceAcreditationSubHeader extends LinearLayout {
  public PassengerResidenceAcreditationSubHeader(Context context, AttributeSet attrs) {
    super(context, attrs);
    initialize(context);
  }

  private void initialize(Context context) {
    inflate(context, R.layout.layout_passenger_residence_acreditation_subheader, this);
  }

  public final void setTitleType(String title) {
    TextView textView = (TextView) findViewById(R.id.txt_passenger_acreditation_subheader_type);
    textView.setText(title);
  }

  public final void setTitleTravelLine(String title) {
    TextView textView =
        (TextView) findViewById(R.id.txt_passenger_acreditation_subheader_travel_line);
    textView.setText(HtmlUtils.formatHtml(title));
  }
}
