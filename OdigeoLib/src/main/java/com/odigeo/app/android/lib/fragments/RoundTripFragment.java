package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.odigeo.app.android.lib.activities.OdigeoSearchActivity;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.fragments.base.TripFragmentBase;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.data.entity.TravelType;
import java.util.List;

/**
 * Created by Irving on 01/09/2014.
 */
public class RoundTripFragment extends TripFragmentBase {
  private FlightSegment segmentTo;
  private FlightSegment segmentReturn;

  /**
   * Creates a new instance of this fragment using search options and flight segments
   *
   * @param searchOptions Search options to loadWidgetImage
   * @param flightSegments Flight segments to show
   * @return A new instance of this fragment
   */
  public static RoundTripFragment newInstance(SearchOptions searchOptions,
      List<FlightSegment> flightSegments) {
    RoundTripFragment roundTripFragment = new RoundTripFragment();
    roundTripFragment.setSearchOptions(searchOptions);
    roundTripFragment.setFlightSegments(flightSegments);
    return roundTripFragment;
  }

  @Override public void onAttach(Activity activity) {
    super.onAttach(activity);
    setSegments();
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = super.onCreateView(inflater, container, savedInstanceState);

    if (isShouldLaunchSearch()) {
      setShouldLaunchSearch(false);
      launchSearch(false);
    }

    return rootView;
  }

  @Override public void setSegments() {
    if (getActivity() != null) {
      if (((OdigeoSearchActivity) getActivity()).getFlightSegmentsRound() != null) {
        segmentTo = ((OdigeoSearchActivity) getActivity()).getFlightSegmentsRound().get(0);
      }

      if (((OdigeoSearchActivity) getActivity()).getFlightSegmentsRound() != null) {
        segmentReturn = ((OdigeoSearchActivity) getActivity()).getFlightSegmentsRound().get(1);
      }
    }
  }

  @Override public void setControlsValues() {
    super.setControlsValues();

    //Set Departure City
    getSearchTripWidget().setCityFrom(segmentTo.getDepartureCity());

    //Set Arrival City
    getSearchTripWidget().setCityTo(segmentTo.getArrivalCity());

    //Set the departure Date
    getSearchTripWidget().setDepartureDate(OdigeoDateUtils.createDate(segmentTo.getDate()));

    this.getSearchTripWidget().setReturnDate(OdigeoDateUtils.createDate(segmentReturn.getDate()));

    showResidentWidget(getSearchOptions().getResident());
  }

  @Override public boolean isDataValid() {
    return segmentTo.getDate() != 0
        && segmentReturn.getDate() != 0
        && segmentTo.getDepartureCity() != null
        && segmentTo.getArrivalCity() != null
        && !segmentTo.getDepartureCity()
        .getIataCode()
        .equalsIgnoreCase(segmentTo.getArrivalCity().getIataCode());
  }

  @Override public TravelType getTravelType() {
    return TravelType.ROUND;
  }

  @Override public boolean isShownInThisFragment(SearchOptions searchOptions) {
    return searchOptions.getTravelType() != TravelType.MULTIDESTINATION;
  }
}
