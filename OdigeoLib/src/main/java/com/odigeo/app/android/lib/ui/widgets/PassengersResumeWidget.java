package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.utils.BaggageUtils;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.shoppingCart.BaggageDescriptor;
import com.odigeo.data.entity.shoppingCart.BaggageSelectionResponse;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.data.entity.shoppingCart.TravellerType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Custom Widget that shows passenger details, like name and baggage options
 *
 * @author M. en C. Javier Silva Pérez
 * @since 02/26/2015.
 */
public class PassengersResumeWidget extends LinearLayout {

  private LinearLayout mLlContainerPassengers;
  private TextView mTvPassengerResumeTitle;

  public PassengersResumeWidget(Context context, AttributeSet attrs) {

    super(context, attrs);

    initialize(context);
  }

  private void initialize(Context context) {
    inflate(context, R.layout.layout_passengers_resume_widget, this);

    mLlContainerPassengers = (LinearLayout) findViewById(R.id.llContainerPassengers);
    mTvPassengerResumeTitle = (TextView) findViewById(R.id.tvPassengerResumeTitle);
    mTvPassengerResumeTitle.setText(LocalizablesFacade.getString(getContext(),
        OneCMSKeys.PASSENGERDETAILMODULE_HEADER_LABELTITLE));
  }

  public void setPassengers(List<Traveller> passengers, List<FlightSegment> flightSegments) {

    mLlContainerPassengers.removeAllViews();

    List<Traveller> passengersTemp = getPassengerListReordered(passengers);
    for (int i = 0; i < passengersTemp.size(); i++) {

      Traveller passenger = passengersTemp.get(i);
      PassengerResumeRow passengerResumeRow = new PassengerResumeRow(getContext(), passenger);
      mLlContainerPassengers.addView(passengerResumeRow);
      addBaggageOptions(flightSegments, passenger);
    }
  }

  /**
   * Reorder the list of passengers. Set the infant passengers at the end of the list
   *
   * @param passengers: list of passengers
   * @return the same list with the infant passengers at the end
   */
  private List<Traveller> getPassengerListReordered(List<Traveller> passengers) {

    List<Traveller> passengerRemoved = new ArrayList<>();
    for (Traveller passenger : passengers) {
      if (passenger.getTravellerType() == TravellerType.INFANT) {
        passengerRemoved.add(passenger);
      }
    }

    for (Traveller passenger : passengerRemoved) {
      passengers.remove(passenger);
      passengers.add(passenger);
    }
    return passengers;
  }

  /**
   * Adds the corresponding baggage options for each passenger
   *
   * @param flightSegments Flight segments
   * @param passenger Passenger to add the baggage
   */
  private void addBaggageOptions(List<FlightSegment> flightSegments, Traveller passenger) {
    Collections.sort(passenger.getBaggageSelections(), new BaggageSelectionSortCollection());

    boolean isTheLasItem;
    for (int j = 0; j < flightSegments.size(); j++) {
      BaggageSelectionResponse selection =
          searchBaggageSelection(passenger.getBaggageSelections(), j);

      isTheLasItem = j == flightSegments.size() - 1;

      //If the segment includes additional baggage, add it
      addBaggageRow(selection, flightSegments.get(j), isTheLasItem);
    }
  }

  /**
   * Add the baggage selection row to the view
   *
   * @param selection {@link BaggageSelectionResponse} to be added
   * @param flightSegment {@link com.odigeo.app.android.lib.models.FlightSegment} to which
   * corresponds to
   * @param isTheLast TRUE if the widget is the last item
   */
  private void addBaggageRow(BaggageSelectionResponse selection, FlightSegment flightSegment,
      boolean isTheLast) {
    if (selection != null
        && selection.getBaggageDescriptor() != null
        && selection.getBaggageDescriptor().getPieces() > 0) {
      View baggageRow = createBaggageRow(selection, flightSegment, Boolean.FALSE, isTheLast);
      mLlContainerPassengers.addView(baggageRow);
    }

    if (selection != null
        && selection.getBaggageDescriptorIncludedInPrice() != null
        && selection.getBaggageDescriptorIncludedInPrice().getPieces() > 0) {
      View baggageRow = createBaggageRow(selection, flightSegment, Boolean.TRUE, isTheLast);
      mLlContainerPassengers.addView(baggageRow);
    }
  }

  /**
   * Search an the baggage selection in the corresponding segment index
   *
   * @param baggageSelections Baggage list to iterate
   * @param index Segment index to search
   * @return The {@link BaggageSelectionResponse} for the
   * specified segment index
   */
  @Nullable private BaggageSelectionResponse searchBaggageSelection(
      @Nullable List<BaggageSelectionResponse> baggageSelections, int index) {
    if (baggageSelections != null) {
      for (BaggageSelectionResponse baggageSelection : baggageSelections) {
        if (baggageSelection.getSegmentTypeIndex() != null
            && baggageSelection.getSegmentTypeIndex().getIndex() == index) {
          return baggageSelection;
        }
      }
    }
    return null;
  }

  /**
   * Create a row view for additional baggage, includes baggage pieces and max weight
   *
   * @param baggageSelectionResponse Baggage selection to compare
   * @param segment Segment to which the baggage corresponds to
   * @param included TRUE if the baggageSelection option is included in ticket fare, FALSE if its
   * additional
   * @param isTheLast TRUE if the widget is the last item
   * @return A new view that shows the selected baggage option and the segment name
   */
  private View createBaggageRow(BaggageSelectionResponse baggageSelectionResponse,
      FlightSegment segment, Boolean included, boolean isTheLast) {
    View root = inflate(getContext(), R.layout.layout_confirmation_item_row, null);
    //Get needed padding based on the current layout margins for the image
    int paddingLeft =
        getResources().getDimensionPixelSize(R.dimen.widget_confirm_whatsnext_icon_size)
            + getResources().getDimensionPixelSize(R.dimen.widget_confirm_whatsnext_icon_margin);
    //Set the padding to the content layout
    View content = root.findViewById(R.id.lyt_row_content);
    content.setPadding(paddingLeft, 0, 0, 0);
    TextView txtHint = (TextView) root.findViewById(R.id.txtButtonWithHint_hint);
    txtHint.setText(HtmlUtils.formatHtml(BaggageUtils.getSegmentName(getContext(), segment)));
    TextView txtContent = (TextView) root.findViewById(R.id.txtButtonWithHint_text);
    //Set flight segment baggage option
    BaggageDescriptor baggageDescriptor;
    String baggageText;
    if (included) {
      baggageDescriptor = baggageSelectionResponse.getBaggageDescriptorIncludedInPrice();
      baggageText = BaggageUtils.getBaggageIncludedText(getContext(), baggageDescriptor.getPieces(),
          LocalizablesFacade.getRawString(getContext(),
              "travellersviewcontroller_baggageconditions_legbaggageincluded"),
          baggageDescriptor.getKilos(), LocalizablesFacade.getRawString(getContext(),
              "travellersviewcontroller_baggageconditions_maxbagweight"));
    } else {
      baggageDescriptor = baggageSelectionResponse.getBaggageDescriptor();
      baggageText = BaggageUtils.getBaggageOptionText(getContext(), baggageDescriptor.getPieces(),
          baggageDescriptor.getKilos(), LocalizablesFacade.getRawString(getContext(),
              "travellersviewcontroller_baggageconditions_maxbagweight"));
    }
    txtContent.setText(HtmlUtils.formatHtml(baggageText));
    ImageView imgIcon = (ImageView) root.findViewById(R.id.imgTravellerType);
    imgIcon.setImageResource(R.drawable.confirmation_baggage);
    if (isTheLast) {
      root.findViewById(R.id.confirmation_item_row_line_separator).setVisibility(GONE);
    }
    return root;
  }

  /**
   * Custom comparator to order baggage conditions list using the segment index
   */
  static class BaggageSelectionSortCollection implements Comparator<BaggageSelectionResponse> {
    @Override public int compare(BaggageSelectionResponse lhs, BaggageSelectionResponse rhs) {
      return lhs.getSegmentTypeIndex().compareTo(rhs.getSegmentTypeIndex());
    }
  }
}
