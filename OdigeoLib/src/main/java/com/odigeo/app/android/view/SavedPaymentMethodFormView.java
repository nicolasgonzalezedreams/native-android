package com.odigeo.app.android.view;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.utils.DrawablesFactory;
import com.odigeo.app.android.view.animations.CollapseAndExpandAnimationUtil;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.textwatchers.BaseTextWatcher;
import com.odigeo.app.android.view.textwatchers.OnFocusChange.BaseOnFocusChange;
import com.odigeo.app.android.view.textwatchers.OnFocusChange.CVVOnFocusChange;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.CVVAmexValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.CVVCardValidator;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.userData.CreditCard;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.interactors.ObfuscateCreditCardInteractor;
import com.odigeo.presenter.SavedPaymentMethodFormPresenter;
import com.odigeo.presenter.contracts.views.SavedPaymentMethodFormViewInterface;
import com.odigeo.presenter.listeners.OnClickSavedPaymentMethodRowListener;
import com.odigeo.tools.DateHelperInterface;
import java.util.List;
import java.util.Map;

public class SavedPaymentMethodFormView extends LinearLayout
    implements SavedPaymentMethodFormViewInterface {

  private static final float DISABLE_OPACITY = 0.4f;
  private static final float ENABLE_OPACITY = 1.0f;
  private static final int AMEX_CVV_LENGTH = 4;
  private static final int NORMAL_CVV_LENGHT = 3;
  private static Map<String, Integer> paymentMethodResources;

  private CreditCard creditCard;
  private Context context;
  private ScrollView svPaymentView;
  private RadioButton rbtnSavePaymentMethod;
  private View vLineSeparator;
  private ImageView ivCreditCard, IvCVVInfoTooltip;
  private TextView tvErrorMessage;
  private TextInputLayout tilCvv;
  private LinearLayout llCvv, llCVVTooltip;
  private RelativeLayout rlCVVTooltipAMEX, rlCVVTooltipNormal;
  private TextView tvCardDescriptionTooltip, tvAMEXCardDescriptionTooltip;
  private View vCvvTooltipSeparator;
  private List<ShoppingCartCollectionOption> shoppingCartCollectionOption;
  private SavedPaymentMethodFormPresenter presenter;
  private String expiredPaymentMethod, notAcceptedPaymentMethod, creditCardName;
  @DrawableRes private int cardImage;

  private TrackerControllerInterface mTracker;
  private OnClickSavedPaymentMethodRowListener onClickSavedPaymentMethodRowListener;

  public SavedPaymentMethodFormView(Context context) {
    super(context);
  }

  public SavedPaymentMethodFormView(Context context,
      OnClickSavedPaymentMethodRowListener onClickSavedPaymentMethodRowListener,
      CreditCard creditCard, List<ShoppingCartCollectionOption> shoppingCartCollectionOption,
      ScrollView scrollView) {
    super(context);
    this.context = context;
    this.onClickSavedPaymentMethodRowListener = onClickSavedPaymentMethodRowListener;
    this.creditCard = creditCard;
    this.shoppingCartCollectionOption = shoppingCartCollectionOption;
    this.svPaymentView = scrollView;
    loadTracker();
    bindViews();
    initContent();
    initPresenter();
    buildRow();
    initListeners();
  }

  private void loadTracker() {
    mTracker = AndroidDependencyInjector.getInstance().provideTrackerController();
  }

  private void initPresenter() {
    DateHelperInterface dateHelper = AndroidDependencyInjector.getInstance().provideDateHelper();
    ObfuscateCreditCardInteractor obfuscateCreditCardInteractor =
        AndroidDependencyInjector.getInstance().provideObfuscateCreditCardInteractor();
    presenter = new SavedPaymentMethodFormPresenter(this, onClickSavedPaymentMethodRowListener,
        obfuscateCreditCardInteractor, dateHelper, creditCard, shoppingCartCollectionOption);
  }

  private void bindViews() {
    LayoutInflater inflater =
        (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    inflater.inflate(R.layout.layout_payment_save_method, this, true);
    rbtnSavePaymentMethod = (RadioButton) findViewById(R.id.rbtnPaymentMethod);
    ivCreditCard = (ImageView) findViewById(R.id.saveCreditCardImage);
    tilCvv = (TextInputLayout) findViewById(R.id.tilSavedPaymentMethodCVV);
    tvErrorMessage = (TextView) findViewById(R.id.tvSavedPaymentMethodError);
    llCvv = (LinearLayout) findViewById(R.id.llSavePaymentMethodsBody);
    IvCVVInfoTooltip = (ImageView) findViewById(R.id.ivCVVInfoTooltip);
    llCVVTooltip = (LinearLayout) findViewById(R.id.llCVVTooltip);
    rlCVVTooltipAMEX = (RelativeLayout) findViewById(R.id.rlAMEXCardCVV);
    rlCVVTooltipNormal = (RelativeLayout) findViewById(R.id.rlNormalCardCVV);
    vCvvTooltipSeparator = findViewById(R.id.vCVVToltipSeparator);
    tvCardDescriptionTooltip = (TextView) findViewById(R.id.tvCVVCardDescription);
    tvAMEXCardDescriptionTooltip = (TextView) findViewById(R.id.tvCVVAMEXCardDescription);
    vLineSeparator = findViewById(R.id.vSaveMethodSeparator);
  }

  private void initContent() {
    tilCvv.setHint(LocalizablesFacade.getString(context, OneCMSKeys.HINT_CREDIT_CARD_CVV));
    tvCardDescriptionTooltip.setText(HtmlUtils.formatHtml(
        LocalizablesFacade.getRawString(getContext(), OneCMSKeys.PAYMENT_CVV_TOOLTIP)));
    tvAMEXCardDescriptionTooltip.setText(HtmlUtils.formatHtml(
        LocalizablesFacade.getRawString(getContext(), OneCMSKeys.PAYMENT_CVV_TOOLTIP_AMEX)));

    String creditCardNameKey =
        OneCMSKeys.SAVED_PAYMENT_METHOD_CARD + creditCard.getCreditCardTypeId().toLowerCase();
    creditCardName = LocalizablesFacade.getString(context, creditCardNameKey).toString();
    expiredPaymentMethod =
        LocalizablesFacade.getString(context, OneCMSKeys.SAVED_PAYMENT_METHOD_EXPIRED).toString();
    notAcceptedPaymentMethod =
        LocalizablesFacade.getString(context, OneCMSKeys.SAVED_PAYMENT_METHOD_NOT_ACCEPTED)
            .toString();
  }

  private void buildRow() {
    paymentMethodResources = DrawablesFactory.createCreditCardResourcesMap();
    presenter.buildRow();
  }

  @Override public void initCVVAmexValidator() {
    if (tilCvv.getEditText() != null) {
      tilCvv.getEditText().addTextChangedListener(new BaseTextWatcher(tilCvv));
      tilCvv.getEditText()
          .setOnFocusChangeListener(
              new CVVOnFocusChange(getContext(), tilCvv, new CVVAmexValidator(),
                  OneCMSKeys.HINT_VALIDATION_ERROR_CVV, mTracker, llCVVTooltip));
      setMaxLengthCVV(AMEX_CVV_LENGTH);
    }
  }

  @Override public void initCVVNormalValidator() {
    if (tilCvv.getEditText() != null) {
      tilCvv.getEditText().addTextChangedListener(new BaseTextWatcher(tilCvv));
      tilCvv.getEditText()
          .setOnFocusChangeListener(
              new CVVOnFocusChange(getContext(), tilCvv, new CVVCardValidator(),
                  OneCMSKeys.HINT_VALIDATION_ERROR_CVV, mTracker, llCVVTooltip));
      setMaxLengthCVV(NORMAL_CVV_LENGHT);
    }
  }

  private void initListeners() {
    IvCVVInfoTooltip.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        presenter.onClickCVVInfoTooltip(llCVVTooltip.getVisibility());
      }
    });

    rbtnSavePaymentMethod.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        boolean isRadioButtonChecked = rbtnSavePaymentMethod.isChecked();
        presenter.onClickRadioButton(isRadioButtonChecked, getTag());
      }
    });
  }

  private void setMaxLengthCVV(int maxLength) {
    if (tilCvv.getEditText() != null) {
      InputFilter[] filterArray = new InputFilter[1];
      filterArray[0] = new InputFilter.LengthFilter(maxLength);
      tilCvv.getEditText().setFilters(filterArray);
    }
  }

  @Override public void hideCVVToolTip() {
    CollapseAndExpandAnimationUtil.collapse(llCVVTooltip, false, null, llCvv);
  }

  @Override public void showCVVAMEXToolTip() {
    rlCVVTooltipAMEX.setVisibility(VISIBLE);
    CollapseAndExpandAnimationUtil.expand(llCVVTooltip);
  }

  @Override public void showCVVNormalToolTip() {
    rlCVVTooltipNormal.setVisibility(VISIBLE);
    CollapseAndExpandAnimationUtil.expand(llCVVTooltip);
  }

  @Override public void showRadioButtonText(String creditcardObfuscate) {
    rbtnSavePaymentMethod.setText(creditCardName + " " + creditcardObfuscate);
  }

  @Override public void showCreditCardImage(String paymentMethod) {
    cardImage = paymentMethodResources.get(paymentMethod);
    ivCreditCard.setImageResource(cardImage);
  }

  @Override public void hideCVV() {
    llCvv.setVisibility(GONE);
  }

  @Override public void showCVV() {
    llCvv.post(new Runnable() {
      @Override public void run() {
        CollapseAndExpandAnimationUtil.expand(llCvv);
      }
    });
    vLineSeparator.setVisibility(GONE);
  }

  @Override public void expandCVV() {
    CollapseAndExpandAnimationUtil.expand(llCvv);
    vLineSeparator.setVisibility(GONE);
  }

  @Override public void collapseCVV() {
    CollapseAndExpandAnimationUtil.collapse(llCvv, false, null, llCvv);
    vLineSeparator.setVisibility(VISIBLE);
  }

  @Override public void hideCVVSeparatorToolTip() {
    vCvvTooltipSeparator.setVisibility(GONE);
  }

  @Override public void hideCVVAMEXToolTip() {
    rlCVVTooltipAMEX.setVisibility(GONE);
  }

  @Override public void hideCVVNormalToolTip() {
    rlCVVTooltipNormal.setVisibility(GONE);
  }

  @Override public void reduceCardImageOpacity() {
    ivCreditCard.setAlpha(DISABLE_OPACITY);
  }

  @Override public void increaseCardImageOpacity() {
    ivCreditCard.setAlpha(ENABLE_OPACITY);
  }

  @Override public void showExpiryDateError() {
    tvErrorMessage.setVisibility(VISIBLE);
    tvErrorMessage.setText(expiredPaymentMethod);
  }

  @Override public void showCreditCardNotAccepted() {
    tvErrorMessage.setVisibility(VISIBLE);
    tvErrorMessage.setText(notAcceptedPaymentMethod);
  }

  @Override public void hideCreditCardError() {
    tvErrorMessage.setVisibility(GONE);
  }

  @Override public void disableRadioButton() {
    rbtnSavePaymentMethod.setEnabled(false);
  }

  @Override public void enableRadioButton() {
    rbtnSavePaymentMethod.setEnabled(true);
  }

  public boolean isRadioButtonChecked() {
    return rbtnSavePaymentMethod.isChecked();
  }

  @Override public void checkRadioButton() {
    rbtnSavePaymentMethod.setChecked(true);
  }

  @Override public void unCheckRadioButton() {
    rbtnSavePaymentMethod.setChecked(false);
    collapseCVV();
  }

  @Override public void clearCVV() {
    if (tilCvv.getEditText() != null) {
      tilCvv.getEditText().setText("");
    }
  }

  @Override @Nullable public String getCVV() {
    if (tilCvv.getEditText() != null) {
      return tilCvv.getEditText().getText().toString();
    }
    return null;
  }

  @Override public void scrollToCVVWithError() {
    svPaymentView.post(new Runnable() {
      @Override public void run() {
        tilCvv.requestFocus();
        View parent = (View) tilCvv.getParent().getParent();
        View grandParent = (View) parent.getParent().getParent();
        int tilCVVHeight = tilCvv.getHeight();
        int topHeight = tilCvv.getTop() + parent.getTop() + grandParent.getTop() + tilCVVHeight;
        int scrollHeight = svPaymentView.getHeight();
        svPaymentView.smoothScrollTo(0, topHeight - scrollHeight / 2);
      }
    });
  }

  @Override public boolean isTilCVVInfoValid() {
    if (tilCvv.getEditText() != null) {
      BaseOnFocusChange baseOnFocusChange =
          (BaseOnFocusChange) tilCvv.getEditText().getOnFocusChangeListener();
      baseOnFocusChange.validateField(tilCvv.getEditText().getEditableText());
      return (TextUtils.isEmpty(tilCvv.getError()));
    }
    return false;
  }

  public SavedPaymentMethodFormPresenter getPresenter() {
    return presenter;
  }
}
