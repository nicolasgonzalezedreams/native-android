package com.odigeo.app.android.lib.models.dto;

import java.util.ArrayList;
import java.util.List;

public class SearchRoomResponseDTO {

  protected AccommodationDealInformationDTO accommodationDealInformation;
  protected List<RoomGroupDealDTO> roomGroupDeals;

  protected List<RoomDealDTO> roomDealLegend;
  protected long searchId;

  public AccommodationDealInformationDTO getAccommodationDealInformation() {
    return accommodationDealInformation;
  }

  public void setAccommodationDealInformation(AccommodationDealInformationDTO value) {
    this.accommodationDealInformation = value;
  }

  public List<RoomGroupDealDTO> getRoomGroupDeals() {
    if (roomGroupDeals == null) {
      roomGroupDeals = new ArrayList<RoomGroupDealDTO>();
    }
    return this.roomGroupDeals;
  }

  public void setRoomGroupDeals(List<RoomGroupDealDTO> roomGroupDeals) {
    this.roomGroupDeals = roomGroupDeals;
  }

  public List<RoomDealDTO> getRoomDealLegend() {
    if (roomDealLegend == null) {
      roomDealLegend = new ArrayList<RoomDealDTO>();
    }
    return this.roomDealLegend;
  }

  public void setRoomDealLegend(List<RoomDealDTO> roomDealLegend) {
    this.roomDealLegend = roomDealLegend;
  }

  public long getSearchId() {
    return searchId;
  }

  public void setSearchId(long value) {
    this.searchId = value;
  }
}
