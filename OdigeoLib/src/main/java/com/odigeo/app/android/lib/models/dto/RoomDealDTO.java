package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RoomDealDTO {

  protected String description;
  protected List<String> bedsDescriptions;
  protected String cancelPolicy;
  protected String key;
  protected String roomType;
  protected String fareDescription;
  protected RoomSmokingPreferenceDTO smokingPreference;
  protected BigDecimal providerPrice;
  protected BigDecimal providerTaxes;
  protected String providerCurrency;
  protected Integer roomsLeft;
  protected boolean isCancellationFree;
  protected BoardTypeDTO boardType;

  public BoardTypeDTO getBoardType() {
    return boardType;
  }

  public void setBoardType(BoardTypeDTO boardType) {
    this.boardType = boardType;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String value) {
    this.description = value;
  }

  public List<String> getBedsDescriptions() {
    if (bedsDescriptions == null) {
      bedsDescriptions = new ArrayList<String>();
    }
    return this.bedsDescriptions;
  }

  public void setBedsDescriptions(List<String> bedsDescriptions) {
    this.bedsDescriptions = bedsDescriptions;
  }

  public String getCancelPolicy() {
    return cancelPolicy;
  }

  public void setCancelPolicy(String value) {
    this.cancelPolicy = value;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String value) {
    this.key = value;
  }

  public String getRoomType() {
    return roomType;
  }

  public void setRoomType(String value) {
    this.roomType = value;
  }

  public String getFareDescription() {
    return fareDescription;
  }

  public void setFareDescription(String value) {
    this.fareDescription = value;
  }

  public RoomSmokingPreferenceDTO getSmokingPreference() {
    return smokingPreference;
  }

  public void setSmokingPreference(RoomSmokingPreferenceDTO value) {
    this.smokingPreference = value;
  }

  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  public void setProviderPrice(BigDecimal value) {
    this.providerPrice = value;
  }

  public BigDecimal getProviderTaxes() {
    return providerTaxes;
  }

  public void setProviderTaxes(BigDecimal value) {
    this.providerTaxes = value;
  }

  public String getProviderCurrency() {
    return providerCurrency;
  }

  public void setProviderCurrency(String value) {
    this.providerCurrency = value;
  }

  public Integer getRoomsLeft() {
    return roomsLeft;
  }

  public void setRoomsLeft(Integer value) {
    this.roomsLeft = value;
  }

  public boolean isIsCancellationFree() {
    return isCancellationFree;
  }

  public void setIsCancellationFree(boolean value) {
    this.isCancellationFree = value;
  }
}
