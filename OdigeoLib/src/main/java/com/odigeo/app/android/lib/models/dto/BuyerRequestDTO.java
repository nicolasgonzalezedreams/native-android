package com.odigeo.app.android.lib.models.dto;

@Deprecated public class BuyerRequestDTO {

  protected String name;
  protected String lastNames;
  protected String mail;
  protected String buyerIdentificationTypeName;
  protected String identification;
  protected int dayOfBirth;
  protected int monthOfBirth;
  protected int yearOfBirth;
  protected String cpf;
  protected String address;
  protected String cityName;
  protected String stateName;
  protected String zipCode;
  protected String phoneNumber1;
  protected String phoneNumber2;
  protected String countryPhoneNumber1;
  protected String countryPhoneNumber2;
  protected String countryCode;

  /**
   * Gets the value of the name property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the value of the name property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setName(String value) {
    this.name = value;
  }

  /**
   * Gets the value of the lastNames property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getLastNames() {
    return lastNames;
  }

  /**
   * Sets the value of the lastNames property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setLastNames(String value) {
    this.lastNames = value;
  }

  /**
   * Gets the value of the mail property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getMail() {
    return mail;
  }

  /**
   * Sets the value of the mail property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setMail(String value) {
    this.mail = value;
  }

  /**
   * Gets the value of the buyerIdentificationTypeName property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getBuyerIdentificationTypeName() {
    return buyerIdentificationTypeName;
  }

  /**
   * Sets the value of the buyerIdentificationTypeName property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setBuyerIdentificationTypeName(String value) {
    this.buyerIdentificationTypeName = value;
  }

  /**
   * Gets the value of the identification property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getIdentification() {
    return identification;
  }

  /**
   * Sets the value of the identification property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setIdentification(String value) {
    this.identification = value;
  }

  /**
   * Gets the value of the dayOfBirth property.
   */
  public int getDayOfBirth() {
    return dayOfBirth;
  }

  /**
   * Sets the value of the dayOfBirth property.
   */
  public void setDayOfBirth(int value) {
    this.dayOfBirth = value;
  }

  /**
   * Gets the value of the monthOfBirth property.
   */
  public int getMonthOfBirth() {
    return monthOfBirth;
  }

  /**
   * Sets the value of the monthOfBirth property.
   */
  public void setMonthOfBirth(int value) {
    this.monthOfBirth = value;
  }

  /**
   * Gets the value of the yearOfBirth property.
   */
  public int getYearOfBirth() {
    return yearOfBirth;
  }

  /**
   * Sets the value of the yearOfBirth property.
   */
  public void setYearOfBirth(int value) {
    this.yearOfBirth = value;
  }

  /**
   * Gets the value of the cpf property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCpf() {
    return cpf;
  }

  /**
   * Sets the value of the cpf property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCpf(String value) {
    this.cpf = value;
  }

  /**
   * Gets the value of the address property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getAddress() {
    return address;
  }

  /**
   * Sets the value of the address property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setAddress(String value) {
    this.address = value;
  }

  /**
   * Gets the value of the cityName property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCityName() {
    return cityName;
  }

  /**
   * Sets the value of the cityName property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCityName(String value) {
    this.cityName = value;
  }

  /**
   * Gets the value of the stateName property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getStateName() {
    return stateName;
  }

  /**
   * Sets the value of the stateName property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setStateName(String value) {
    this.stateName = value;
  }

  /**
   * Gets the value of the zipCode property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getZipCode() {
    return zipCode;
  }

  /**
   * Sets the value of the zipCode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setZipCode(String value) {
    this.zipCode = value;
  }

  /**
   * Gets the value of the phoneNumber1 property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getPhoneNumber1() {
    return phoneNumber1;
  }

  /**
   * Sets the value of the phoneNumber1 property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setPhoneNumber1(String value) {
    this.phoneNumber1 = value;
  }

  /**
   * Gets the value of the phoneNumber2 property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getPhoneNumber2() {
    return phoneNumber2;
  }

  /**
   * Sets the value of the phoneNumber2 property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setPhoneNumber2(String value) {
    this.phoneNumber2 = value;
  }

  /**
   * Gets the value of the countryPhoneNumber1 property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCountryPhoneNumber1() {
    return countryPhoneNumber1;
  }

  /**
   * Sets the value of the countryPhoneNumber1 property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCountryPhoneNumber1(String value) {
    this.countryPhoneNumber1 = value;
  }

  /**
   * Gets the value of the countryPhoneNumber2 property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCountryPhoneNumber2() {
    return countryPhoneNumber2;
  }

  /**
   * Sets the value of the countryPhoneNumber2 property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCountryPhoneNumber2(String value) {
    this.countryPhoneNumber2 = value;
  }

  /**
   * Gets the value of the countryCode property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCountryCode() {
    return countryCode;
  }

  /**
   * Sets the value of the countryCode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCountryCode(String value) {
    this.countryCode = value;
  }
}
