package com.odigeo.app.android.lib.receivers;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.services.OneCMSUpdaterService;
import com.odigeo.data.localizable.OneCMSAlarmsController;

public class OneCMSUpdaterReceiver extends WakefulBroadcastReceiver {

  private OneCMSAlarmsController oneCMSAlarmsController;

  public OneCMSUpdaterReceiver() {
    oneCMSAlarmsController =
        AndroidDependencyInjector.getInstance().provideOneCMSAlarmsController();
  }

  @Override public void onReceive(Context context, Intent intent) {
    if (intent.getAction() != null && intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
      oneCMSAlarmsController.setOneCMSUpdateAlarms();
      completeWakefulIntent(intent);
    } else {
      ComponentName comp =
          new ComponentName(context.getPackageName(), OneCMSUpdaterService.class.getName());
      Intent newIntent = new Intent(context, OneCMSUpdaterService.class);
      context.startService((newIntent.setComponent(comp)));
    }
  }
}
