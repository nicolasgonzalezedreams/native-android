package com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators;

import com.odigeo.app.android.view.textwatchers.fieldsvalidation.BaseValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;

public class MasterCardValidator extends BaseValidator implements FieldValidator {

  public static final String REGEX_MASTERCARD = "(?:^(5[1-5]{1}[0-9]{14}))$";

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_MASTERCARD);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
