package com.odigeo.app.android.lib.models;

/**
 * Created by manuel on 06/10/14.
 */
public class IdentificationType {
  private static final int HASH_CODE = 19;
  private static final int HASH_CODE_MULTIPLIER = 31;
  private String id;
  private String text;

  public IdentificationType() {

  }

  public IdentificationType(String id) {
    this.setId(id);
  }

  public IdentificationType(String id, String text) {
    this.setId(id);
    this.setText(text);
  }

  public final String getId() {
    return id;
  }

  public final void setId(String id) {
    this.id = id;
  }

  public final String getText() {
    return text;
  }

  public final void setText(String text) {
    this.text = text;
  }

  public final boolean equals(Object otherObject) {
    if (otherObject == null) {
      return false;
    }

    if (!(otherObject instanceof IdentificationType)) {
      return false;
    }

    return ((IdentificationType) otherObject).getId().equals(id);
  }

  public final int hashCode() {
    int hashCode = HASH_CODE * HASH_CODE_MULTIPLIER + id.hashCode();
    return hashCode;
  }
}
