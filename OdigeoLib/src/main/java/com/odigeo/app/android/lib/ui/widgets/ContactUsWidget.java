package com.odigeo.app.android.lib.ui.widgets;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.DeviceUtils;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.PermissionsHelper;

import static android.Manifest.permission.CALL_PHONE;
import static com.odigeo.app.android.view.helpers.PermissionsHelper.PHONE_REQUEST_CODE;

public class ContactUsWidget extends LinearLayout {

  public static final String SEE_MORE_INDICATOR = "*";
  private final View viewRoot;
  private TextView txtSchedule;
  private TextView txtPhoneText;
  private TextView txtPhoneAbroadText;
  private TextView txtForeignContact;
  private Button btnCallPhone;

  public ContactUsWidget(final Context context, AttributeSet attrs) {
    super(context, attrs);

    viewRoot = inflate(this.getContext(), R.layout.layout_contact_us_widget, this);
    findViews();

    if (!isInEditMode()) {

      setScheduleInformation();

      btnCallPhone.setTypeface(Configuration.getInstance().getFonts().getSemiBold());

      btnCallPhone.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          String phone = btnCallPhone.getText().toString().replace(SEE_MORE_INDICATOR, "");

          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (PermissionsHelper.askForPermissionIfNeeded(CALL_PHONE, (Activity) context,
                OneCMSKeys.PERMISSION_PHONE_CONTACTUS_MESSAGE, PHONE_REQUEST_CODE)) {
              DeviceUtils.callPhoneNumber(phone, getContext());
            }
          } else {
            DeviceUtils.callPhoneNumber(phone, getContext());
          }

          postGAEventAboutContactUs(GAnalyticsNames.LABEL_ABOUT_PHONE_TOP_CLICKS);
        }
      });

      setPhoneInformation();
      setForeignPhoneInformation();
      setPhoneFeesInformation();
    }
  }

  private void findViews() {
    txtForeignContact = (TextView) viewRoot.findViewById(R.id.txtForeignContact);
    txtSchedule = (TextView) viewRoot.findViewById(R.id.txtSchedule);

    btnCallPhone = ((Button) viewRoot.findViewById(R.id.button_phone_about_contact));

    txtPhoneText = (TextView) viewRoot.findViewById(R.id.txtPhoneText);
    txtPhoneAbroadText = (TextView) viewRoot.findViewById(R.id.txtPhoneAbroadText);
  }

  private void setScheduleInformation() {
    String schedule = LocalizablesFacade.getString(getContext(),
        OneCMSKeys.MARKETSPECIFIC_CALLCENTERSCHEDULE_PERIOD0).toString() + "\n" + LocalizablesFacade
        .getString(getContext(), OneCMSKeys.MARKETSPECIFIC_CALLCENTERSCHEDULE_PERIOD1)
        .toString() + "\n" + LocalizablesFacade.getString(getContext(),
        OneCMSKeys.MARKETSPECIFIC_CALLCENTERSCHEDULE_PERIOD2).toString();

    txtSchedule.setText(schedule);
  }

  private void setPhoneFeesInformation() {
    String phoneFees = LocalizablesFacade.getString(getContext(),
        OneCMSKeys.MARKETSPECIFIC_CALLCENTERPHONE_PHONEFEES).toString();
    txtPhoneText.setVisibility(View.VISIBLE);
    txtPhoneText.setText(phoneFees);
    HtmlUtils.setPhoneTextClickable(txtPhoneText, notNullString(phoneFees), getContext());
  }

  private void setForeignPhoneInformation() {
    String foreignPhone = LocalizablesFacade.getString(getContext(),
        OneCMSKeys.MARKETSPECIFIC_CALLCENTERPHONE_PHONEABROAD).toString();
    if (!TextUtils.isEmpty(foreignPhone)) {
      HtmlUtils.setPhoneTextClickable(txtForeignContact, LocalizablesFacade.getString(getContext(),
          OneCMSKeys.CONFIRMATION_WHATSNEXT_CALLCENTERABROAD, notNullString(foreignPhone))
          .toString(), getContext());
      txtPhoneAbroadText.setText(LocalizablesFacade.getString(getContext(),
          OneCMSKeys.MARKETSPECIFIC_CALLCENTERPHONE_PHONEABROADFEES).toString());
    } else {
      txtForeignContact.setVisibility(GONE);
      txtPhoneAbroadText.setVisibility(GONE);
    }
  }

  private void setPhoneInformation() {
    String phone =
        LocalizablesFacade.getString(getContext(), OneCMSKeys.MARKET_SPECIFIC_CALLCENTER_PHONE)
            .toString();
    btnCallPhone.setText(phone);
    if (!btnCallPhone.getText().toString().contains(SEE_MORE_INDICATOR)) {
      btnCallPhone.setText(btnCallPhone.getText().toString().concat(SEE_MORE_INDICATOR));
    }
  }

  private String notNullString(String value) {
    if (value == null) {
      return "";
    }
    return value;
  }

  private void postGAEventAboutContactUs(String label) {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_ABOUT_US_CONTACT,
            GAnalyticsNames.ACTION_ABOUT_US_CONTACT, label));
  }
}
