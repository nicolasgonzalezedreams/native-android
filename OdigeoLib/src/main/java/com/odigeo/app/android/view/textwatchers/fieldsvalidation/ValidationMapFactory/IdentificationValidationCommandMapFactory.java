package com.odigeo.app.android.view.textwatchers.fieldsvalidation.ValidationMapFactory;

import com.odigeo.app.android.view.textwatchers.fieldsvalidation.CIFValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.NIEValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.NIFValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.NationalIdCardValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.PassportValidator;
import java.util.HashMap;
import java.util.Map;

public class IdentificationValidationCommandMapFactory {

  private static final String PASSPORT = "PASSPORT";
  private static final String NIE = "NIE";
  private static final String NIF = "NIF";
  private static final String NATIONAL_ID_CARD = "NATIONAL_ID_CARD";
  private static final String CIF = "CIF";

  public static Map<String, FieldValidator> getMap() {
    Map<String, FieldValidator> commandMap = new HashMap<>();
    commandMap.put(PASSPORT, new PassportValidator());
    commandMap.put(NIE, new NIEValidator());
    commandMap.put(NIF, new NIFValidator());
    commandMap.put(NATIONAL_ID_CARD, new NationalIdCardValidator());
    commandMap.put(CIF, new CIFValidator());
    return commandMap;
  }
}
