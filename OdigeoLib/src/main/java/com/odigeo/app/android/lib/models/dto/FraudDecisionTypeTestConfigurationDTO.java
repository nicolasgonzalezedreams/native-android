package com.odigeo.app.android.lib.models.dto;

public enum FraudDecisionTypeTestConfigurationDTO {

  ACCEPT_NO_RISK, ACCEPT_LOW_RISK, STOP, BANK_TRANSFER, FAX, IN_RANGE, IN_RANGE_WITH_SH, MANUAL_FRAUD;

  public static FraudDecisionTypeTestConfigurationDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
