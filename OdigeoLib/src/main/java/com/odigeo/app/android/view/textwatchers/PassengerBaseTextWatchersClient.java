package com.odigeo.app.android.view.textwatchers;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.custom.SpinnerWithTitle;
import com.odigeo.app.android.view.textwatchers.OnFocusChange.BaseOnFocusChange;
import com.odigeo.app.android.view.textwatchers.OnFocusChange.SecondLastNameOnFocusChange;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.NameValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.RequestValidationHandler;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.SurnameValidator;
import java.util.ArrayList;
import java.util.List;

public class PassengerBaseTextWatchersClient {

  private TextInputLayout mTilName;
  private TextInputLayout mTilMiddleName;
  private TextInputLayout mTilSurname;
  private TextInputLayout mTilSecondSurname;
  private TextInputLayout mTilIdentification;
  private TextInputLayout mTilNationality;
  private TextInputLayout mTilCountryOfResidence;
  private TextInputLayout mTilIdentificationCountry;
  private TextInputLayout mTilIdentificationExpirationDate;
  private TextInputLayout mTilBirthdate;
  private SpinnerWithTitle identificationSpinner;
  private List<TextInputLayout> mTilsPassenger = new ArrayList<>();
  private Context mContext;
  private boolean mIsSecondLastNameConditionRuleMandatory;
  private RequestValidationHandler requestValidationHandler;

  public PassengerBaseTextWatchersClient(Context context, SpinnerWithTitle identificationSpinner,
      TextInputLayout name, TextInputLayout middleName, TextInputLayout surname,
      TextInputLayout secondSurname, TextInputLayout identification, TextInputLayout nationality,
      TextInputLayout countryOfResident, TextInputLayout identificationCountry,
      TextInputLayout identificationExpirationDate, TextInputLayout birthdate,
      boolean isSecondLastNameConditionRuleMandatory,
      RequestValidationHandler requestValidationHandler) {
    mContext = context;
    mTilName = name;
    mTilMiddleName = middleName;
    mTilSurname = surname;
    mTilSecondSurname = secondSurname;
    mTilIdentification = identification;
    mTilNationality = nationality;
    mTilCountryOfResidence = countryOfResident;
    mTilIdentificationCountry = identificationCountry;
    mTilIdentificationExpirationDate = identificationExpirationDate;
    mTilBirthdate = birthdate;
    mIsSecondLastNameConditionRuleMandatory = isSecondLastNameConditionRuleMandatory;
    this.identificationSpinner = identificationSpinner;
    this.requestValidationHandler = requestValidationHandler;
    addTilsToList();
  }

  public void initPassengerOnFocusChange() {
    initNameOnFocusChange();
    initMiddleNameOnFocusChange();
    initSurnameOnFocusChange();
    initSecondSurnameOnFocusChange();
    initBirthdateTextWatcher();
    initCountryOfResidentTextWatcher();
    initNationalityTextWatcher();
    initIdentificationExpirationDateTextWatcher();
    initIdentificationCountryTextWatcher();
  }

  private void addTilsToList() {
    mTilsPassenger.add(mTilName);
    mTilsPassenger.add(mTilMiddleName);
    mTilsPassenger.add(mTilSurname);
    mTilsPassenger.add(mTilSecondSurname);
    mTilsPassenger.add(mTilBirthdate);
    mTilsPassenger.add(mTilCountryOfResidence);
    mTilsPassenger.add(mTilNationality);
    mTilsPassenger.add(mTilIdentification);
    mTilsPassenger.add(mTilIdentificationExpirationDate);
    mTilsPassenger.add(mTilIdentificationCountry);
  }

  private void initNameOnFocusChange() {
    if (mTilName.getEditText() != null) {
      mTilName.getEditText()
          .setOnFocusChangeListener(new BaseOnFocusChange(mContext, mTilName, new NameValidator(),
              OneCMSKeys.VALIDATION_ERROR_NAME));
      mTilName.getEditText().addTextChangedListener(new BaseTextWatcher(mTilName));
    }
  }

  private void initMiddleNameOnFocusChange() {
    if (mTilMiddleName.getEditText() != null) {
      mTilMiddleName.getEditText()
          .setOnFocusChangeListener(
              new BaseOnFocusChange(mContext, mTilMiddleName, new NameValidator(),
                  OneCMSKeys.VALIDATION_ERROR_NAME));
      mTilMiddleName.getEditText().addTextChangedListener(new BaseTextWatcher(mTilMiddleName));
    }
  }

  private void initSurnameOnFocusChange() {
    if (mTilSurname.getEditText() != null) {
      mTilSurname.getEditText()
          .setOnFocusChangeListener(
              new BaseOnFocusChange(mContext, mTilSurname, new SurnameValidator(),
                  OneCMSKeys.VALIDATION_ERROR_SURNAME));
      mTilSurname.getEditText().addTextChangedListener(new BaseTextWatcher(mTilSurname));
    }
  }

  private void initSecondSurnameOnFocusChange() {
    if (mTilSecondSurname.getEditText() != null) {
      mTilSecondSurname.getEditText()
          .setOnFocusChangeListener(
              new SecondLastNameOnFocusChange(mContext, mTilSecondSurname, new SurnameValidator(),
                  OneCMSKeys.VALIDATION_ERROR_SECOND_SURNAME,
                  mIsSecondLastNameConditionRuleMandatory));
      mTilSecondSurname.getEditText()
          .addTextChangedListener(new BaseTextWatcher(mTilSecondSurname));
    }
  }

  private void initBirthdateTextWatcher() {
    if (mTilBirthdate.getEditText() != null) {
      mTilBirthdate.getEditText().addTextChangedListener(new BaseTextWatcher(mTilBirthdate));
    }
  }

  private void initCountryOfResidentTextWatcher() {
    if (mTilCountryOfResidence.getEditText() != null) {
      mTilCountryOfResidence.getEditText()
          .addTextChangedListener(new BaseTextWatcher(mTilCountryOfResidence));
    }
  }

  private void initNationalityTextWatcher() {
    if (mTilNationality.getEditText() != null) {
      mTilNationality.getEditText().addTextChangedListener(new BaseTextWatcher(mTilNationality));
    }
  }

  private void initIdentificationExpirationDateTextWatcher() {
    if (mTilIdentificationExpirationDate.getEditText() != null) {
      mTilIdentificationExpirationDate.getEditText()
          .addTextChangedListener(new BaseTextWatcher(mTilIdentificationExpirationDate));
    }
  }

  private void initIdentificationCountryTextWatcher() {
    if (mTilIdentificationCountry.getEditText() != null) {
      mTilIdentificationCountry.getEditText()
          .addTextChangedListener(new BaseTextWatcher(mTilIdentificationCountry));
    }
  }

  @Nullable public TextInputLayout getTextInputLayoutWithError() {
    validateIdentificationInCaseSpinnerChangeAndHaventFocusOut();
    for (TextInputLayout textInputLayout : mTilsPassenger) {
      if (isThereErrorInTextInputLayout(textInputLayout)) {
        haveToSetErrorInTextInputLayout(textInputLayout);
        return textInputLayout;
      }
    }
    return null;
  }

  @Nullable public TextInputLayout getTextInputLayoutEmpty() {
    for (TextInputLayout textInputLayout : mTilsPassenger) {
      if ((textInputLayout.getEditText() != null)
          && (textInputLayout.getVisibility()
          == View.VISIBLE)
          && (isNotSecondLastNameOrIsSecondLastNameMandatory(textInputLayout))
          && ((textInputLayout.getEditText().getText().toString().isEmpty()))) {
        return textInputLayout;
      }
    }
    return null;
  }

  private void haveToSetErrorInTextInputLayout(TextInputLayout textInputLayout) {
    if (textInputLayout.equals(mTilNationality)) {
      setNationalityError();
    } else if (textInputLayout.equals(mTilCountryOfResidence)) {
      setCountryOfResidentError();
    } else if (textInputLayout.equals(mTilIdentificationCountry)) {
      setIdentificationCountryError();
    } else if (textInputLayout.equals(mTilIdentificationExpirationDate)) {
      setIdentificationExpirationDateError();
    } else if (textInputLayout.equals(mTilBirthdate)) {
      setBirthdateError();
    } else if (isNotSecondLastNameOrIsSecondLastNameMandatory(textInputLayout)) {
      validateField(textInputLayout);
    }
  }

  private void setNationalityError() {
    String oneCMSError = LocalizablesFacade.getString(mTilNationality.getContext(),
        OneCMSKeys.VALIDATION_ERROR_NATIONALITY).toString();
    mTilNationality.setError(oneCMSError);
    mTilNationality.setErrorEnabled(true);
  }

  private void setCountryOfResidentError() {
    String oneCMSError = LocalizablesFacade.getString(mTilCountryOfResidence.getContext(),
        OneCMSKeys.VALIDATION_ERROR_COUNTRY_CODE).toString();
    mTilCountryOfResidence.setError(oneCMSError);
    mTilCountryOfResidence.setErrorEnabled(true);
  }

  private void setIdentificationCountryError() {
    String oneCMSError = LocalizablesFacade.getString(mTilIdentificationCountry.getContext(),
        OneCMSKeys.VALIDATION_ERROR_IDENTIFICATION_COUNTRY).toString();
    mTilIdentificationCountry.setError(oneCMSError);
    mTilIdentificationCountry.setErrorEnabled(true);
  }

  private void setIdentificationExpirationDateError() {
    String oneCMSError = LocalizablesFacade.getString(mTilIdentificationExpirationDate.getContext(),
        OneCMSKeys.VALIDATION_ERROR_IDENTIFICATION_EXPIRATION).toString();
    mTilIdentificationExpirationDate.setError(oneCMSError);
    mTilIdentificationExpirationDate.setErrorEnabled(true);
  }

  private void setBirthdateError() {
    String oneCMSError = LocalizablesFacade.getString(mTilBirthdate.getContext(),
        OneCMSKeys.VALIDATION_ERROR_BIRTH_DATE).toString();
    mTilBirthdate.setError(oneCMSError);
    mTilBirthdate.setErrorEnabled(true);
  }

  private void setIdentificationError() {
    String oneCMSError = LocalizablesFacade.getString(mTilIdentification.getContext(),
        OneCMSKeys.VALIDATION_ERROR_IDENTIFICATION).toString();
    mTilIdentification.setError(oneCMSError);
    mTilIdentification.setErrorEnabled(true);
  }

  public void setNullErrorInTextWatchers() {
    mTilName.setError(null);
    mTilMiddleName.setError(null);
    mTilSurname.setError(null);
    mTilSecondSurname.setError(null);
    mTilIdentification.setError(null);
    mTilNationality.setError(null);
    mTilCountryOfResidence.setError(null);
    mTilIdentificationCountry.setError(null);
    mTilIdentificationExpirationDate.setError(null);
    mTilBirthdate.setError(null);

    mTilName.setErrorEnabled(false);
    mTilMiddleName.setErrorEnabled(false);
    mTilSurname.setErrorEnabled(false);
    mTilSecondSurname.setErrorEnabled(false);
    mTilIdentification.setErrorEnabled(false);
    mTilNationality.setErrorEnabled(false);
    mTilCountryOfResidence.setErrorEnabled(false);
    mTilIdentificationCountry.setErrorEnabled(false);
    mTilIdentificationExpirationDate.setErrorEnabled(false);
    mTilBirthdate.setErrorEnabled(false);
  }

  private boolean isThereErrorInTextInputLayout(TextInputLayout textInputLayout) {
    return ((textInputLayout.getEditText() != null) && (textInputLayout.getVisibility()
        == View.VISIBLE) && ((!TextUtils.isEmpty(textInputLayout.getError()))
        || (((textInputLayout.getEditText().getText().toString().isEmpty()))
        && (isNotSecondLastNameOrIsSecondLastNameMandatory(textInputLayout)))));
  }

  public void updateTextInputLayoutError(TextInputLayout textInputLayoutFocused) {
    for (TextInputLayout textinputLayout : mTilsPassenger) {
      if ((textinputLayout.equals(textInputLayoutFocused)) && (textInputLayoutFocused.getEditText()
          != null) && (isNotSecondLastNameOrIsSecondLastNameMandatory(textInputLayoutFocused))) {
        validateField(textInputLayoutFocused);
      }
    }
  }

  private boolean isNotSecondLastNameOrIsSecondLastNameMandatory(TextInputLayout textInputLayout) {
    return (!textInputLayout.equals(mTilSecondSurname) || (textInputLayout.equals(mTilSecondSurname)
        && (mIsSecondLastNameConditionRuleMandatory)));
  }

  public void updateIdentificationAfterChangeInSpinner(TextInputLayout textInputLayout) {
    int identificationIndex = mTilsPassenger.indexOf(mTilIdentification);
    if (identificationIndex != -1) {
      mTilsPassenger.set(identificationIndex, textInputLayout);
      mTilIdentification = textInputLayout;
    }
  }

  private void validateField(TextInputLayout textInputLayout) {
    if (textInputLayout.getEditText() != null) {
      BaseOnFocusChange baseOnFocusChange =
          (BaseOnFocusChange) textInputLayout.getEditText().getOnFocusChangeListener();
      baseOnFocusChange.validateField(textInputLayout.getEditText().getEditableText());
    }
  }

  private void validateIdentificationInCaseSpinnerChangeAndHaventFocusOut() {
    if (mTilIdentification.getVisibility() == View.VISIBLE
        && mTilIdentification.getEditText() != null) {
      String userIdentification = mTilIdentification.getEditText().getText().toString();
      String identificationType = (String) identificationSpinner.getSelectedItem();
      boolean isValid =
          requestValidationHandler.handleRequest(identificationType, userIdentification);
      if (!isValid) setIdentificationError();
    }
  }
}
