package com.odigeo.app.android.lib.models.dto;

import java.util.ArrayList;
import java.util.List;

public class ItineraryProviderBookingsDTO {

  protected List<ItineraryProviderBookingDTO> bookings;

  protected MobileSegmentResult segments;
  /**
   * NOTE: In the DTO´s for Import Trip, the class has this value
   */
  protected SeatPreferencesSelectionSet seatPreferencesSelectionSet;
  /**
   * NOTE: In the DTO´s for Import Trip, the class does not has this value, but the JSON has this
   * value
   */
  protected List<ItinerarySegment> itinerarySegments;
  protected BookingItinerary bookingItinerary;
  private ItinerariesLegendDTO legend;

  /**
   * Gets the value of the bookings property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the bookings property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    completeBookingData().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list
   * {@link com.odigeo.app.android.lib.models.dto.ItineraryProviderBookingsDTO }
   */
  public List<ItineraryProviderBookingDTO> getBookings() {
    if (bookings == null) {
      bookings = new ArrayList<ItineraryProviderBookingDTO>();
    }
    return this.bookings;
  }

  public void setBookings(List<ItineraryProviderBookingDTO> bookings) {
    this.bookings = bookings;
  }

  public MobileSegmentResult getSegments() {
    if (segments == null) {
      segments = new MobileSegmentResult();
    }
    return this.segments;
  }

  public void setSegments(MobileSegmentResult segments) {
    this.segments = segments;
  }

  public ItinerariesLegendDTO getLegend() {
    return legend;
  }

  public void setLegend(ItinerariesLegendDTO legend) {
    this.legend = legend;
  }

  public SeatPreferencesSelectionSet getSeatPreferencesSelectionSet() {
    return seatPreferencesSelectionSet;
  }

  public void setSeatPreferencesSelectionSet(
      SeatPreferencesSelectionSet seatPreferencesSelectionSet) {
    this.seatPreferencesSelectionSet = seatPreferencesSelectionSet;
  }

  public List<ItinerarySegment> getItinerarySegments() {
    return itinerarySegments;
  }

  public void setItinerarySegments(List<ItinerarySegment> itinerarySegments) {
    this.itinerarySegments = itinerarySegments;
  }

  public BookingItinerary getBookingItinerary() {
    return bookingItinerary;
  }

  public void setBookingItinerary(BookingItinerary bookingItinerary) {
    this.bookingItinerary = bookingItinerary;
  }
}
