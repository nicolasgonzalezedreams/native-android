package com.odigeo.app.android.lib.interfaces;

/**
 * Created by emiliano.desantis on 05/09/2014.
 */
public interface ClickButtonSearch {
  void onClickButtonSearch();
}
