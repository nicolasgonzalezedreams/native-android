package com.odigeo.app.android.lib.utils.exceptions;

import com.google.gson.Gson;
import com.odigeo.data.entity.booking.Booking;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 8/06/16
 */

public class BookingException extends Exception {

  private BookingException(String detailMessage, Throwable throwable) {
    super(detailMessage, throwable);
  }

  public static BookingException newInstance(Booking booking, Throwable throwable) {
    Gson gson = new Gson();
    String json = gson.toJson(booking);
    return new BookingException(json, throwable);
  }
}
