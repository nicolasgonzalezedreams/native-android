package com.odigeo.app.android.lib.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoConfirmationActivity;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.navigator.AboutUsNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.PermissionsHelper;

import static android.Manifest.permission.CALL_PHONE;
import static com.odigeo.app.android.view.helpers.PermissionsHelper.PHONE_REQUEST_CODE;

/**
 * Created by manuel on 17/12/14.
 */
public class ClickablePhoneNumberSpan extends ClickableSpan {

  private final Context context;
  private String phoneNumber;

  public ClickablePhoneNumberSpan(Context context) {
    this.context = context;
  }

  @Override public final void onClick(View v) {

    postGAEventAboutContactUs(GAnalyticsNames.LABEL_ABOUT_PHONE_BOTTOM_CLICKS);

    if (!(context instanceof OdigeoConfirmationActivity)) {
      AboutUsNavigator activity = (AboutUsNavigator) context;
      activity.setPhoneCallNumber(phoneNumber);
    }

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (PermissionsHelper.askForPermissionIfNeeded(CALL_PHONE, (Activity) context,
          OneCMSKeys.PERMISSION_PHONE_CONTACTUS_MESSAGE, PHONE_REQUEST_CODE)) {
        DeviceUtils.callPhoneNumber(phoneNumber.trim(), context);
      }
    } else {
      DeviceUtils.callPhoneNumber(phoneNumber.trim(), context);
    }
  }

  @Override public final void updateDrawState(TextPaint ds) {
    ds.setColor(context.getResources().getColor(R.color.basic_light));
    ds.setUnderlineText(true);
  }

  public final void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  private void postGAEventAboutContactUs(String label) {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_ABOUT_US_CONTACT,
            GAnalyticsNames.ACTION_ABOUT_US_CONTACT, label));
  }
}
