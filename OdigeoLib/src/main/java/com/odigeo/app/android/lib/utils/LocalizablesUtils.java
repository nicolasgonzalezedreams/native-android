package com.odigeo.app.android.lib.utils;

import android.content.Context;
import android.util.Log;
import com.odigeo.app.android.lib.data.database.daos.LocalizableDao;
import com.odigeo.app.android.lib.data.database.model.Localizable;
import com.odigeo.data.entity.localizables.OdigeoLocalizable;
import com.odigeo.presenter.listeners.LocalizablesUtilsListener;
import java.util.List;

/**
 * TODO: Important
 * We need to remove this class in the second iteration of the refactoring of localizables. Once
 * we moved the LocalizableDAO to Data layer we can move the code to LocalizablesInteractor and
 * delete this class.
 */
public class LocalizablesUtils implements LocalizablesUtilsListener {
  private static String TAG = "LocalizablesUtils";
  private Context context;

  public LocalizablesUtils(Context context) {
    this.context = context;
  }

  @Override public void updateLocalizables(final String oneCMSTable,
      final List<OdigeoLocalizable> localizables) {

    LocalizableDao localizableDao = new LocalizableDao(context, oneCMSTable);

    for (OdigeoLocalizable newLocalizable : localizables) {

      Localizable localizable =
          new Localizable(newLocalizable.getLocalizableKey(), newLocalizable.getLocalizableValue());
      localizableDao.insertOrUpdate(localizable);

      Log.i(TAG, "Processing String "
          + oneCMSTable
          + " : "
          + newLocalizable.getLocalizableKey()
          + " - "
          + newLocalizable.getLocalizableValue());
    }
  }
}
