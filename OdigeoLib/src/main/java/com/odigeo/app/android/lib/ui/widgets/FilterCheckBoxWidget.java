package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.base.CustomField;

/**
 * Created by Irving on 15/10/2014.
 */
public class FilterCheckBoxWidget extends CustomField {
  private TextView checkBoxText;
  private ImageView indicatorImage;

  private String textCheck;

  private String extra;

  private boolean isCheck;

  public FilterCheckBoxWidget(Context context, String textCheck) {
    super(context);
    inflateLayout();

    this.textCheck = textCheck;
    isCheck = false;
    init();
  }

  public FilterCheckBoxWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
    inflateLayout();
    setAttributes(attrs);
    init();
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.layout_filter_checkbox, this);
    indicatorImage = (ImageView) findViewById(R.id.filter_checkbox_indicator);
    checkBoxText = (TextView) findViewById(R.id.filter_checkbox_text);
  }

  private void setAttributes(AttributeSet attributes) {
    TypedArray array =
        getContext().obtainStyledAttributes(attributes, R.styleable.FilterCheckBoxWidget);
    String localizableKey = array.getString(R.styleable.FilterCheckBoxWidget_odigeoText);
    if (localizableKey != null) {
      textCheck = LocalizablesFacade.getString(getContext(), localizableKey).toString();
    }
    isCheck = array.getBoolean(R.styleable.FilterCheckBoxWidget_isCheck, true);
    array.recycle();
  }

  private void init() {
    if (textCheck != null) {
      setCheckBoxText();
    }
    setCheck();
  }

  private void setCheckBoxText() {
    checkBoxText.setText(textCheck);
  }

  public final void setCheckBoxText(String string) {
    if (string != null) {
      textCheck = string;
      setCheckBoxText();
    }
  }

  private void setCheck() {
    if (isCheck) {
      indicatorImage.setVisibility(VISIBLE);
    } else {
      indicatorImage.setVisibility(GONE);
    }
    setSelected(isCheck);
  }

  public final boolean isCheck() {
    return isCheck;
  }

  public final void setCheck(boolean isCheck) {
    this.isCheck = isCheck;
    setCheck();
  }

  public final String getTextCheck() {
    return textCheck;
  }

  @Override public final boolean validate() {
    return false;
  }

  @Override public final boolean isValid() {
    return false;
  }

  @Override public void clearValidation() {
    //TODO add action
  }

  public final String getExtra() {
    return extra;
  }

  public final void setExtra(String extra) {
    this.extra = extra;
  }
}
