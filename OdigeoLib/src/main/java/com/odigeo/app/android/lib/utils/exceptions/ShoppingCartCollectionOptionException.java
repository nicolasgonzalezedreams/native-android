package com.odigeo.app.android.lib.utils.exceptions;

import com.google.gson.Gson;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;

public class ShoppingCartCollectionOptionException extends Exception {

  private ShoppingCartCollectionOptionException(String detailMessage) {
    super(detailMessage);
  }

  public static ShoppingCartCollectionOptionException newInstance(
      CollectionMethodType collectionMethodType) {
    Gson gson = new Gson();
    String json = gson.toJson(collectionMethodType);
    return new ShoppingCartCollectionOptionException(json);
  }
}
