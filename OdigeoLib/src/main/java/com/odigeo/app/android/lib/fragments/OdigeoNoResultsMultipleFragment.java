package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.TopBriefWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.view.constants.OneCMSKeys;

/**
 * Created by manuel on 24/09/14.
 */
public class OdigeoNoResultsMultipleFragment extends android.support.v4.app.Fragment
    implements View.OnClickListener {

  private OdigeoApp odigeoApp;
  private OdigeoSession odigeoSession;
  private Button btnBooking;

  public static OdigeoNoResultsMultipleFragment newInstance(SearchOptions searchOptions) {
    OdigeoNoResultsMultipleFragment newFragment = new OdigeoNoResultsMultipleFragment();

    Bundle bundleArgs = new Bundle();
    bundleArgs.putSerializable(Constants.EXTRA_SEARCH_OPTIONS, searchOptions);

    newFragment.setArguments(bundleArgs);

    return newFragment;
  }

  @Override public final void onAttach(Activity activity) {
    super.onAttach(activity);
    // GAnalytics screen tracking -
    postGAScreenTrackingNoResultsMulti();
    odigeoApp = (OdigeoApp) activity.getApplication();
    odigeoSession = odigeoApp.getOdigeoSession();
  }

  @Override public final View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View viewRoot = inflater.inflate(R.layout.layout_search_noresults_multiple, container, false);

    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {
      TextView tvResetSearch = (TextView) viewRoot.findViewById(R.id.tvResetSearch);
      tvResetSearch.setText(LocalizablesFacade.getString(getContext(),
          OneCMSKeys.NORESULTSSEARCHORFILTERSCELL_LABELRESETSEARCH_TEXT));

      btnBooking = (Button) viewRoot.findViewById(R.id.button_booking_noresults);
      btnBooking.setOnClickListener(this);
      btnBooking.setTypeface(Configuration.getInstance().getFonts().getBold());
      btnBooking.setText(
          LocalizablesFacade.getString(getContext(), "searchviewcontroller_searchbtn_text")
              .toString());

      SearchOptions searchOptions =
          (SearchOptions) getArguments().getSerializable(Constants.EXTRA_SEARCH_OPTIONS);

      TopBriefWidget topBriefWidget =
          ((TopBriefWidget) viewRoot.findViewById(R.id.topbrief_results));
      topBriefWidget.addData(searchOptions);
    }
    return viewRoot;
  }

  @Override public final void onViewCreated(View view, Bundle savedInstanceState) {

    super.onViewCreated(view, savedInstanceState);
    if (odigeoSession == null || !odigeoSession.isDataHasBeenLoaded()) {
      Util.sendToHome(this.getActivity(), odigeoApp);
    }
  }

  @Override public final void onClick(View v) {
    if (v.getId() == btnBooking.getId()) {
      getActivity().finish();
    }
  }

  private void postGAScreenTrackingNoResultsMulti() {
    BusProvider.getInstance()
        .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_NO_RESULTS_MULTI));
  }
}
