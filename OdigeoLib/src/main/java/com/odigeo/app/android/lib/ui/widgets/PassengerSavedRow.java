package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.models.Passenger;
import com.odigeo.app.android.lib.models.dto.TravellerTypeDTO;
import com.odigeo.app.android.lib.ui.widgets.base.ButtonWithHint;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.events.LongClickPassengerEvent;

/**
 * Created by emiliano.desantis on 09/10/2014.
 */
public class PassengerSavedRow extends ButtonWithHint
    implements View.OnClickListener, View.OnLongClickListener {
  private int imageSource;
  private Passenger passenger;

  public PassengerSavedRow(Context context, Passenger passenger) {
    super(context);
    this.setPassenger(passenger);

    initialize();
  }

  public PassengerSavedRow(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray aAttrs =
        context.obtainStyledAttributes(attrs, R.styleable.ImageWithTitleAndDescription, 0, 0);
    imageSource = aAttrs.getResourceId(R.styleable.ImageWithTitleAndDescription_android_src, 0);

    aAttrs.recycle();

    initialize();
  }

  private void initialize() {
    this.setClickable(true);
    this.setOnClickListener(this);
    this.setOnLongClickListener(this);

    ImageView icon = (ImageView) findViewById(R.id.imgTravellerType);
    ImageView iconDefault = (ImageView) findViewById(R.id.imgTravellerTypeDefault);

    if (getPassenger() != null) {
      if (getPassenger().getTravellerType() == TravellerTypeDTO.ADULT) {
        icon.setImageResource(R.drawable.confirmation_adult);
        setHint(CommonLocalizables.getInstance().getCommonAdult(getContext()));
      } else if (getPassenger().getTravellerType() == TravellerTypeDTO.INFANT) {
        icon.setImageResource(R.drawable.confirmation_infant);
        setHint(CommonLocalizables.getInstance().getCommonInfant(getContext()));
      } else if (getPassenger().getTravellerType() == TravellerTypeDTO.CHILD) {
        icon.setImageResource(R.drawable.confirmation_child);
        setHint(CommonLocalizables.getInstance().getCommonChild(getContext()));
      }

      setText(getPassenger().getName() + " " + getPassenger().getFirstLastName());

      if (getPassenger().isPassengerDefault()) {
        iconDefault.setVisibility(View.VISIBLE);
      }
    } else {
      if (icon != null) {
        icon.setImageResource(imageSource);
      }
    }
  }

  @Override public int getLayout() {
    return R.layout.layout_passenger_saved_row_widget;
  }

  @Override public void onClick(View v) {
    BusProvider.getInstance().post(this);
  }

  @Override public boolean onLongClick(View view) {
    BusProvider.getInstance().post(new LongClickPassengerEvent(this));
    return true;
  }

  public final Passenger getPassenger() {
    return passenger;
  }

  public final void setPassenger(Passenger passenger) {
    this.passenger = passenger;
  }
}
