package com.odigeo.app.android.lib.models.dto;

import android.content.Context;
import android.support.annotation.Nullable;
import java.io.Serializable;

public class CollectionMethodDTO implements Serializable {

  private static final String BANK_TRANSFER_CODE = "bank_transfer";
  protected CreditCardTypeDTO creditCardType;
  protected CollectionMethodTypeDTO type;

  /**
   * NOTE: In the DTO's for Import Trip, the class has this properties
   */
  protected int id;
  //    protected String type;

  /**
   * Gets the value of the creditCardType property.
   *
   * @return possible object is {@link com.odigeo.app.android.lib.models.dto.CreditCardTypeDTO }
   */
  public CreditCardTypeDTO getCreditCardType() {
    return creditCardType;
  }

  /**
   * Sets the value of the creditCardType property.
   *
   * @param value allowed object is {@link com.odigeo.app.android.lib.models.dto.CreditCardTypeDTO
   * }
   */
  public void setCreditCardType(CreditCardTypeDTO value) {
    this.creditCardType = value;
  }

  /**
   * Gets the value of the type property.
   *
   * @return possible object is {@link com.odigeo.app.android.lib.models.dto.CollectionMethodTypeDTO
   * }
   */
  public CollectionMethodTypeDTO getType() {
    return type;
  }

  /**
   * Sets the value of the type property.
   *
   * @param value allowed object is {@link com.odigeo.app.android.lib.models.dto.CollectionMethodTypeDTO
   * }
   */
  public void setType(CollectionMethodTypeDTO value) {
    this.type = value;
  }

  public boolean equals(Object collectionMethodDTO) {
    if (!(collectionMethodDTO instanceof CollectionMethodDTO)) {
      return false;
    }

    CollectionMethodDTO otherObject = (CollectionMethodDTO) collectionMethodDTO;

    return otherObject.getType() == getType() && otherObject.getCreditCardType()
        .equals(getCreditCardType());
  }

  /**
   * Depending on the method type, get the corresponding code, this will only check {@link
   * com.odigeo.app.android.lib.models.dto.CollectionMethodTypeDTO#CREDITCARD} and {@link
   * return null
   *
   * @return Gets the method type code, if the type is different from {@link
   * com.odigeo.app.android.lib.models.dto.CollectionMethodTypeDTO#CREDITCARD} or {@link
   */
  @Nullable public String getCode() {
    if (type == CollectionMethodTypeDTO.CREDITCARD) {
      return creditCardType.getCode();
    } else if (type == CollectionMethodTypeDTO.BANKTRANSFER) {
      return BANK_TRANSFER_CODE;
    } else {
      return null;
    }
  }

  public String getName(Context context) {
    if (type == CollectionMethodTypeDTO.CREDITCARD) {
      return creditCardType.getName();
    } else {
      return type.value();
    }
  }
}
