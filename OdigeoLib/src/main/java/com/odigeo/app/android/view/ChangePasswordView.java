package com.odigeo.app.android.view;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.navigator.BaseNavigator;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.DialogHelper;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.app.android.view.interfaces.AuthDialogActionInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.ChangePasswordPresenter;
import com.odigeo.presenter.contracts.navigators.AccountPreferencesNavigatorInterface;
import com.odigeo.presenter.contracts.views.ChangePasswordViewInterface;

public class ChangePasswordView extends BaseView<ChangePasswordPresenter>
    implements ChangePasswordViewInterface {

  private EditText mEditTextCurrentPassword;
  private EditText mEditTextNewPassword;
  private EditText mEditTextRepeatPassword;
  private TextInputLayout mInputCurrent;
  private TextInputLayout mInputNew;
  private TextInputLayout mInputRepeat;
  private CheckBox mCheckSwap;
  private Button mBtnReset;
  private DialogHelper mMakingActionDialog;

  public static ChangePasswordView newInstance() {
    return new ChangePasswordView();
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = super.onCreateView(inflater, container, savedInstanceState);

    String title =
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_CHANGEPASSWORD_TITLE);
    ((BaseNavigator) getActivity()).setNavigationTitle(title);
    ((BaseNavigator) getActivity()).setUpToolbarButton(
        DrawableUtils.getTintedResource(R.drawable.ic_clear, R.color.navigation_bar_title,
            getActivity()));

    mPresenter.checkAccountType();

    return view;
  }

  @Override public void onResume() {
    super.onResume();
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_MYINFO_ACCOUNT_PASSWORD);
  }

  @Override protected ChangePasswordPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideChangePasswordPresenter(this, (AccountPreferencesNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.sso_change_password;
  }

  @Override protected void initComponent(View view) {
    mEditTextCurrentPassword =
        (EditText) view.findViewById(R.id.edit_text_password_current_password);
    mEditTextNewPassword = (EditText) view.findViewById(R.id.edit_text_password_new_password);
    mEditTextRepeatPassword = (EditText) view.findViewById(R.id.edit_text_password_repeat_password);
    mCheckSwap = (CheckBox) view.findViewById(R.id.checkbox_change_password);
    mBtnReset = (Button) view.findViewById(R.id.button_reset_password);
    mInputCurrent = ((TextInputLayout) view.findViewById(R.id.input_current_password));
    mInputNew = ((TextInputLayout) view.findViewById(R.id.input_new_password));
    mInputRepeat = ((TextInputLayout) view.findViewById(R.id.input_repeat_password));
    mMakingActionDialog = new DialogHelper(getActivity());
  }

  @Override protected void initOneCMSText(View view) {
    mInputCurrent.setHint(
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_CHANGEPASSWORD_CURRENT));
    mInputNew.setHint(
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_CHANGEPASSWORD_NEW));
    mInputRepeat.setHint(
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_CHANGEPASSWORD_REPEAT));
    mBtnReset.setText(
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_CHANGEPASSWORD_RESET));
    mCheckSwap.setText(
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_SHOW_PASSWORD));
  }

  @Override protected void setListeners() {
    mBtnReset.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.changePasswordAction(mEditTextCurrentPassword.getText().toString(),
            mEditTextNewPassword.getText().toString(),
            mEditTextRepeatPassword.getText().toString());
        mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_DATA_ACC_DETAILS,
            TrackerConstants.ACTION_ACCOUNT_SUMMARY, TrackerConstants.LABEL_CHANGE_PASSWORD_CLICKS);
      }
    });

    mCheckSwap.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int typeText;
        if (isChecked) {
          typeText = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD;
        } else {
          typeText = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD;
        }
        changeTypeEditText(typeText);
      }
    });

    mEditTextCurrentPassword.addTextChangedListener(createPasswordTextWatcher());
    mEditTextNewPassword.addTextChangedListener(createPasswordTextWatcher());
    mEditTextRepeatPassword.addTextChangedListener(createPasswordTextWatcher());
  }

  private TextWatcher createPasswordTextWatcher() {
    return new TextWatcher() {
      @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (validatePasswords()) {
          resetErrors();
          mBtnReset.setEnabled(true);
        } else {
          mBtnReset.setEnabled(false);
          mBtnReset.setTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
        }
      }

      @Override public void afterTextChanged(Editable s) {
      }
    };
  }

  private void resetErrors() {
    mInputCurrent.setError(null);
    mInputNew.setError(null);
    mInputRepeat.setError(null);
  }

  @Override public void invalidFormatCurrentPassword() {
    mInputCurrent.setError(
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_FORM_ERROR_PASSWORD_FORMAT));
  }

  @Override public void invalidFormatNewPassword() {
    mInputNew.setError(
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_FORM_ERROR_PASSWORD_FORMAT));
  }

  @Override public void invalidFormatRepeatPassword() {
    mInputRepeat.setError(
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_FORM_ERROR_PASSWORD_FORMAT));
  }

  @Override public void invalidConcurrentPassword() {
    mInputCurrent.setError(
        LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_PASSWORD_WRONG));
  }

  @Override public void newPasswordMatch(boolean isMatching) {
    mInputRepeat.setError(isMatching ? null
        : LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_REPEAT_PASSWORD_ERROR));
  }

  @Override public void showChangingPasswordDialog() {
    mMakingActionDialog.showDialog(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_CHANGEPASSWORD_RESET));
  }

  @Override public void newPasswordIsTheSame(boolean isTheSame) {
    if (isTheSame) {
      mInputNew.setError(
          LocalizablesFacade.getRawString(getActivity(), OneCMSKeys.SSO_NEW_PASSWORD_EQUAL_ERROR));
      //removes the error to avoid multiple warnings
      mInputCurrent.setError(null);
    } else {
      mInputNew.setError(null);
      mInputRepeat.setError(null);
    }
  }

  @Override public void changeCurrentPasswordVisibility(boolean isVisible) {
    mInputCurrent.setVisibility((isVisible) ? View.VISIBLE : View.GONE);
  }

  @Override public void passwordChanged(boolean wasPasswordChanged) {
    String message = wasPasswordChanged ? LocalizablesFacade.getString(getActivity(),
        OneCMSKeys.SSO_PASSWORD_CHANGE_SUCCESS).toString()
        : LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_PASSWORD_CHANGE_FAIL)
            .toString();
    if (wasPasswordChanged) {
      mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_DATA_ACC_DETAILS,
          TrackerConstants.ACTION_ACCOUNT_SUMMARY,
          TrackerConstants.LABEL_CHANGE_PASSWORD_SUCCESFUL);
    } else {
      mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_MY_DATA_ACC_DETAILS,
          TrackerConstants.ACTION_ACCOUNT_SUMMARY, TrackerConstants.LABEL_CHANGE_PASSWORD_FAILURE);
    }
    int imgResource = wasPasswordChanged ? R.drawable.ok_process : R.drawable.hud_wrong_info;
    mMakingActionDialog.showDoneDialog(getActivity(), message, imgResource,
        new DialogHelper.ProcessDoneListener() {
          @Override public void onDismiss() {

          }
        });
  }

  private boolean validatePasswords() {
    return mPresenter.validatePasswords(mEditTextCurrentPassword.getText().toString(),
        mEditTextNewPassword.getText().toString(), mEditTextRepeatPassword.getText().toString());
  }

  /**
   * Changes the current input type of all the EditText.
   *
   * @param type Type of input to be set.
   */
  private void changeTypeEditText(int type) {
    mEditTextCurrentPassword.setInputType(type);
    mEditTextNewPassword.setInputType(type);
    mEditTextRepeatPassword.setInputType(type);
  }

  @Override public void showAuthError() {
    mMakingActionDialog.showAuthErrorDialog(this, new AuthDialogActionInterface() {
      @Override public void OnAuthDialogOK() {
        mPresenter.onAuthOkClicked();
      }
    });
  }

  @Override public void onLogoutError(MslError error, String message) {
    Log.e(error.name(), message);
  }
}
