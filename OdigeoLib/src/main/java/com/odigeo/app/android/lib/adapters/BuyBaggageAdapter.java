package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.BuyBaggageItemModel;
import com.odigeo.app.android.lib.models.dto.SelectableBaggageDescriptorDTO;
import com.odigeo.app.android.lib.utils.BaggageUtils;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import java.util.List;

/**
 * Custom adapter for buy baggage spinner
 *
 * @author M. en C. Javier Silva Perez
 * @version 1.0
 * @since 02/10/2015
 */
public class BuyBaggageAdapter extends ArrayAdapter<BuyBaggageItemModel> {
  /**
   * Default constructor
   *
   * @param context Context to initialize the adapter
   * @param objects List to initialize the adapter content
   */
  public BuyBaggageAdapter(Context context, List<BuyBaggageItemModel> objects) {
    super(context, 0, objects);
  }

  @Override public final View getView(int position, View convertView, ViewGroup parent) {
    return getCustomView(position, parent, R.layout.layout_baggage_item,
        LocalizablesFacade.getString(getContext(),
            "travellersviewcontroller_baggageconditions_addbaggage").toString());
  }

  @Override public final View getDropDownView(int position, View convertView, ViewGroup parent) {
    return getCustomView(position, parent, R.layout.layout_baggage_item_drop,
        LocalizablesFacade.getString(getContext(),
            "travellersviewcontroller_baggageconditions_nobaggageselected").toString());
  }

  /**
   * Create a common custom view for get view and dropdown view
   *
   * @param position Item position to show
   * @param parent Parent for the new view
   * @param idLayout Layout id to inflate
   * @param idStringDefault String to show when item 0 is selected
   * @return The view filled out
   */

  public final View getCustomView(int position, ViewGroup parent, int idLayout,
      String idStringDefault) { //@StringRes int idStringDefault) {
    LayoutInflater inflater =
        (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View root = inflater.inflate(idLayout, parent, false);

    TextView txtBaggageOption = (TextView) root.findViewById(R.id.txt_baggage_option);
    TextView txtPrice = (TextView) root.findViewById(R.id.txt_price);

    BuyBaggageItemModel buyBaggageItemModel = getItem(position);
    SelectableBaggageDescriptorDTO selectableBaggageDescriptor =
        buyBaggageItemModel.getSelectableBaggageDescriptor();
    String text;
    //If selectable baggage descriptor is null, show default values
    if (selectableBaggageDescriptor == null) {
      text = LocalizablesFacade.getString(getContext(),
          "travellersviewcontroller_baggageconditions_addbaggage").toString();
      //If no price is set, hide price text view
      txtPrice.setVisibility(View.GONE);
    } else {
      int pieces = selectableBaggageDescriptor.getBaggageDescriptor().getPieces();
      if (pieces == 0) {
        text = idStringDefault;
        txtPrice.setVisibility(View.GONE);
      } else {
        text = BaggageUtils.getBaggageOptionText(getContext(), pieces,
            selectableBaggageDescriptor.getBaggageDescriptor().getKilos(),
            LocalizablesFacade.getRawString(getContext(),
                "travellersviewcontroller_baggageconditions_maxbagweight"));

        txtPrice.setText("+" + LocaleUtils.getLocalizedCurrencyValue(
            selectableBaggageDescriptor.getPrice().doubleValue()));
      }
    }

    txtBaggageOption.setText(HtmlUtils.formatHtml(text));

    return root;
  }

  /**
   * Create a common custom view for get view and dropdown view
   *
   * @param position Item position to show
   * @param parent Parent for the new view
   * @param idLayout Layout id to inflate
   * @return The view filled out
   */
  public final View getCustomView(int position, ViewGroup parent, int idLayout) {
    LayoutInflater inflater =
        (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View root = inflater.inflate(idLayout, parent, false);

    TextView txtBaggageOption = (TextView) root.findViewById(R.id.txt_baggage_option);
    TextView txtPrice = (TextView) root.findViewById(R.id.txt_price);

    BuyBaggageItemModel buyBaggageItemModel = getItem(position);
    SelectableBaggageDescriptorDTO selectableBaggageDescriptor =
        buyBaggageItemModel.getSelectableBaggageDescriptor();
    String text;
    //If selectable baggage descriptor is null, show default values
    if (selectableBaggageDescriptor == null) {
      text = LocalizablesFacade.getString(getContext(),
          "travellersviewcontroller_baggageconditions_addbaggage").toString();
      //If no price is set, hide price text view
      txtPrice.setVisibility(View.GONE);
    } else {
      int pieces = selectableBaggageDescriptor.getBaggageDescriptor().getPieces();
      if (pieces == 0) {
        text = LocalizablesFacade.getString(getContext(),
            "travellersviewcontroller_baggageconditions_nobaggageselected").toString();
        txtPrice.setVisibility(View.GONE);
      } else {
        text = BaggageUtils.getBaggageOptionText(getContext(), pieces,
            selectableBaggageDescriptor.getBaggageDescriptor().getKilos(),
            LocalizablesFacade.getRawString(getContext(),
                "travellersviewcontroller_baggageconditions_maxbagweight"));

        txtPrice.setText("+" + LocaleUtils.getLocalizedCurrencyValue(
            selectableBaggageDescriptor.getPrice().doubleValue()));
      }
    }

    txtBaggageOption.setText(HtmlUtils.formatHtml(text));

    return root;
  }
}
