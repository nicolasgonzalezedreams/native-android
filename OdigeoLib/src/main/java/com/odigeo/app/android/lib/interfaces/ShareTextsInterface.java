package com.odigeo.app.android.lib.interfaces;

/**
 * Created by gaston.mira on 11/23/16.
 */

public interface ShareTextsInterface {

  void createTwitterShareText(String oneCmsKey, String storeUrl);

  void createFacebookShareText(String storeUrl);

  void createWhatsappShareText(String oneCmsKey, String storeUrl);

  void createSmsShareText(String oneCmsKey, String storeUrl);

  void createCopyToClipboardShareText(String oneCmsKey, String storeUrl);

  void createFbMessengerShareText(String oneCmsKey, String storeUrl);

  void createEmailSubject(String oneCmsKey);

  void createEmailBody(String oneCmsKey, String storeUrl);
}
