package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

/**
 * Created by ximena.perez on 27/01/2015.
 */
public class BaggageDescriptorDTO implements Serializable {

  private int pieces;
  private int kilos;

  /**
   * @return
   */
  public int getPieces() {
    return pieces;
  }

  /**
   * @param pieces
   */
  public void setPieces(int pieces) {
    this.pieces = pieces;
  }

  /**
   * @return
   */
  public int getKilos() {
    return kilos;
  }

  /**
   * -     * @param kilos
   * -
   */
  public void setKilos(int kilos) {
    this.kilos = kilos;
  }
}

