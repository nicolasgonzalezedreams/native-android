package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for formOfIdentificationType.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;simpleType name="formOfIdentificationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NATIONAL_ID_CARD"/>
 *     &lt;enumeration value="PASSPORT"/>
 *     &lt;enumeration value="CREDIT_CARD"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
public enum FormOfIdentificationTypeDTO {

  NATIONAL_ID_CARD, PASSPORT, CREDIT_CARD;

  public static FormOfIdentificationTypeDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
