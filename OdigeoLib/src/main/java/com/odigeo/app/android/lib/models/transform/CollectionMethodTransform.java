package com.odigeo.app.android.lib.models.transform;

import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import com.odigeo.app.android.lib.models.dto.CollectionMethodDTO;
import com.odigeo.app.android.lib.models.dto.CreditCardTypeDTO;

/**
 * Created by manuel on 20/10/14.
 */
public final class CollectionMethodTransform {

  private CollectionMethodTransform() {

  }

  public static CollectionMethodWithPrice clone(
      CollectionMethodWithPrice collectionMethodWithPrice) {
    CollectionMethodWithPrice newObject = new CollectionMethodWithPrice();

    newObject.setCollectionMethodKey(collectionMethodWithPrice.getCollectionMethodKey());
    newObject.setCollectionMethod(clone(collectionMethodWithPrice.getCollectionMethod()));
    newObject.setCheapest(collectionMethodWithPrice.isCheapest());
    newObject.setPrice(collectionMethodWithPrice.getPrice());

    return newObject;
  }

  public static CollectionMethodDTO clone(CollectionMethodDTO method) {
    CollectionMethodDTO newMethod = new CollectionMethodDTO();

    newMethod.setType(method.getType());
    newMethod.setCreditCardType(clone(method.getCreditCardType()));

    return newMethod;
  }

  public static CreditCardTypeDTO clone(CreditCardTypeDTO creditCardTypeDTO) {
    if (creditCardTypeDTO == null) {
      return null;
    }

    CreditCardTypeDTO newObject = new CreditCardTypeDTO();
    newObject.setName(creditCardTypeDTO.getName());
    newObject.setCode(creditCardTypeDTO.getCode());

    return newObject;
  }
}
