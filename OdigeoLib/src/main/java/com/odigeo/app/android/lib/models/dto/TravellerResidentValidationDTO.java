package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class TravellerResidentValidationDTO implements Serializable {

  protected int numPassenger;
  protected boolean allowResidentValidationRetry;
  protected ResidentValidationStatusDTO residentValidationType;

  /**
   * @return the numPassenger
   */
  public int getNumPassenger() {
    return numPassenger;
  }

  /**
   * @param numPassenger the numPassenger to set
   */
  public void setNumPassenger(int numPassenger) {
    this.numPassenger = numPassenger;
  }

  /**
   * @return the status
   */
  public ResidentValidationStatusDTO getResidentValidationType() {
    return residentValidationType;
  }

  /**
   * @param residentValidationType the status to set
   */
  public void setResidentValidationType(ResidentValidationStatusDTO residentValidationType) {
    this.residentValidationType = residentValidationType;
  }

  public boolean getAllowResidentValidationRetry() {
    return allowResidentValidationRetry;
  }

  public void setAllowResidentValidationRetry(boolean allowResidentValidationRetry) {
    this.allowResidentValidationRetry = allowResidentValidationRetry;
  }
}
