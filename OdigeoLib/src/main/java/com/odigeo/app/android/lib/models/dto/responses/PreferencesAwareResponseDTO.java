package com.odigeo.app.android.lib.models.dto.responses;

import com.odigeo.app.android.lib.models.dto.PreferencesDTO;
import com.odigeo.app.android.lib.models.dto.responses.base.BaseResponseDTO;

/**
 * @author miguel
 */
@Deprecated public class PreferencesAwareResponseDTO extends BaseResponseDTO {

  protected PreferencesDTO preferences;

  public PreferencesAwareResponseDTO() {
  }

  /**
   * Gets the value of the preferences property.
   *
   * @return possible object is {@link Preferences }
   */
  public PreferencesDTO getPreferences() {
    return preferences;
  }

  /**
   * Sets the value of the preferences property.
   *
   * @param value allowed object is {@link Preferences }
   */
  public void setPreferences(PreferencesDTO value) {
    this.preferences = value;
  }
}
