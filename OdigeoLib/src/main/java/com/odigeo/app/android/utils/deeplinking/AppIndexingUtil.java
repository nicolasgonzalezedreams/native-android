package com.odigeo.app.android.utils.deeplinking;

import android.content.Intent;
import android.net.Uri;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.data.entity.geo.City;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 6/07/16
 */

public class AppIndexingUtil {
  private static final String CITY_SEPARATOR = "::";
  private static final int IATA_CODE = 0;
  private static final int NAME = 1;
  private static final int COUNTRY = 2;
  private static final String ITEM_SEPARATOR = "/";
  private static final int ORIGIN = 0;
  private static final int DESTINY = 1;
  private static final int AIRLINE = 2;
  private static SearchOptions.Builder sBuilder;

  public static SearchOptions getSearchOptionsFromAppIndexingIntent(Intent intent) {
    sBuilder = new SearchOptions.Builder();
    Uri uri = intent.getData();
    if (uri == null) {
      return null;
    }
    String path = uri.getAuthority() + uri.getPath();
    if (path.isEmpty()) {
      return sBuilder.build();
    }
    processParameters(path);
    return sBuilder.build();
  }

  private static void processParameters(String path) {
    String[] parameters = path.split(ITEM_SEPARATOR);
    for (int index = 0; index < parameters.length; index++) {
      String parameter = parameters[index];
      switch (index) {
        case ORIGIN:
          extractCity(parameter, true);
          break;
        case DESTINY:
          extractCity(parameter, false);
          break;
        case AIRLINE:
          sBuilder.addAirline(parameter);
          break;
      }
    }
  }

  private static void extractCity(String parameter, boolean isOrigin) {
    City city = new City();
    String[] cityElements = parameter.split(CITY_SEPARATOR);
    for (int index = 0; index < cityElements.length; index++) {
      String value = cityElements[index];
      switch (index) {
        case IATA_CODE:
          city.setIataCode(value);
          break;
        case NAME:
          city.setCityName(value);
          break;
        case COUNTRY:
          city.setCountryName(value);
          break;
      }
    }
    if (isOrigin) {
      sBuilder.addDeparture(0, city);
    } else {
      sBuilder.addArrival(0, city);
    }
  }
}
