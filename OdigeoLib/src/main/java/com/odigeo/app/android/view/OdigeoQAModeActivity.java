package com.odigeo.app.android.view;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.QAModeAdapter;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.ui.widgets.EditTextForm;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.navigator.BaseNavigator;
import com.odigeo.presenter.QAModePresenter;
import com.odigeo.presenter.contracts.navigators.QAModeNavigator;
import com.odigeo.presenter.contracts.views.QAModeView;
import com.odigeo.presenter.model.QAModeUrlModel;
import com.squareup.otto.Subscribe;
import java.util.Collections;
import java.util.List;

public class OdigeoQAModeActivity extends BaseNavigator implements QAModeView, QAModeNavigator {

  private QAModePresenter presenter;
  private EditTextForm newUrlEditText;
  private ListView qaModeUrlsList;
  private QAModeAdapter qaModeAdapter;
  private boolean allowUrlRemoval = true;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    presenter = dependencyInjector.provideQAModePresenter(this, this);
    setContentView(R.layout.activity_qa_mode);

    setupViews();

    presenter.getQAModeUrls();
  }

  private void setupViews() {
    final ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setTitle(R.string.qa_mode);
      actionBar.setDisplayHomeAsUpEnabled(true);
    }

    newUrlEditText = (EditTextForm) findViewById(R.id.qa_mode_new_url);
    newUrlEditText.setHint(getString(R.string.qa_mode_hint));

    qaModeUrlsList = (ListView) findViewById(R.id.qa_mode_list);
    qaModeUrlsList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    qaModeUrlsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        presenter.onSelectQAModeUrl(qaModeAdapter.getItem(position));
      }
    });
    registerForContextMenu(qaModeUrlsList);
    qaModeAdapter = new QAModeAdapter(this, Collections.<QAModeUrlModel>emptyList());
    qaModeUrlsList.setAdapter(qaModeAdapter);

    final Button mAddButton = (Button) findViewById(R.id.qa_mode_button);
    mAddButton.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        final String url = newUrlEditText.getText();
        presenter.onAddQAModeUrl(url);
      }
    });
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      this.onBackPressed();
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    super.onCreateContextMenu(menu, v, menuInfo);
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_qamode, menu);

    menu.findItem(R.id.qa_remove).setVisible(allowUrlRemoval);
  }

  @Override public boolean onContextItemSelected(MenuItem item) {
    final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
    final int index = info.position;
    final int id = item.getItemId();
    if (id == R.id.qa_edit) {
      editListViewItem(index, qaModeAdapter.getItem(index));
      return true;
    } else if (id == R.id.qa_remove) {
      presenter.onRemoveQAModeUrl(qaModeAdapter.getItem(index));
      return true;
    }
    return super.onContextItemSelected(item);
  }

  private void editListViewItem(final int position, QAModeUrlModel qaModeUrlModel) {
    final Dialog dialog = new Dialog(this);
    dialog.setContentView(R.layout.dialog_qa_mode);
    dialog.setTitle(getResources().getString(R.string.qa_mode_edit_url));

    final EditText txtMode = (EditText) dialog.findViewById(R.id.qa_mode_edittext);
    Button btnSave = (Button) dialog.findViewById(R.id.qa_mode_button);

    txtMode.setText(qaModeUrlModel.getUrl());
    btnSave.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        final String newUrl = txtMode.getText().toString();
        presenter.onEditQAModeUrl(qaModeAdapter.getItem(position), newUrl);
        dialog.dismiss();
      }
    });
    dialog.show();
  }

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getApplication());
  }

  @Override public boolean isActive() {
    return ViewCompat.isAttachedToWindow(qaModeUrlsList);
  }

  @Override public void showQAModeUrls(final List<QAModeUrlModel> qaModeUrls) {
    qaModeAdapter.updateList(qaModeUrls);
  }

  @Override public void showEmptyUrlMessage() {
    newUrlEditText.setErrorText("");
    newUrlEditText.showError();
  }

  @Override public void showAddQAModeUrlSuccess(List<QAModeUrlModel> qaModeUrlModels) {
    qaModeAdapter.updateList(qaModeUrlModels);
    newUrlEditText.setText("");
  }

  @Override public void showQAModeUrlSelected(List<QAModeUrlModel> qaModeUrls,
      QAModeUrlModel selectedQAQaModeUrl) {
    qaModeAdapter.updateList(qaModeUrls);
  }

  @Override public void disableQAModeUrlDeletion() {
    allowUrlRemoval = false;
  }

  @Override public void enableQAModeUrlDeletion() {
    allowUrlRemoval = true;
  }

  @Override public void navigateBackWithSelectedQAModeUrl(QAModeUrlModel selectedQAQaModeUrl) {
    Intent data = new Intent();
    data.putExtra(Constants.EXTRA_QA_MODE, selectedQAQaModeUrl);
    setResult(RESULT_OK, data);
    finish();
  }
}
