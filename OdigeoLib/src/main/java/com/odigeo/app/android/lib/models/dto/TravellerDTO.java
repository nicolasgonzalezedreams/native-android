package com.odigeo.app.android.lib.models.dto;

import android.support.annotation.NonNull;
import com.google.gson.annotations.Until;
import com.odigeo.app.android.lib.consts.Constants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Java class for Traveller complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="Traveller">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="travellerType" type="{http://api.edreams.com/booking/next}travellerType"
 * />
 *       &lt;attribute name="title" type="{http://api.edreams.com/booking/next}travellerTitle" />
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="firstLastName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="secondLastName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="travellerGender" type="{http://api.edreams.com/booking/next}travellerGender"
 * />
 *       &lt;attribute name="dateOfBirth" type="{http://www.w3.org/2001/XMLSchema}date" />
 *       &lt;attribute name="nationalityCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"
 * />
 *       &lt;attribute name="countryCodeOfResidence" type="{http://www.w3.org/2001/XMLSchema}string"
 * />
 *       &lt;attribute name="identification" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="identificationType" type="{http://api.edreams.com/booking/next}travellerIdentificationType"
 * />
 *       &lt;attribute name="identificationExpirationDate" type="{http://www.w3.org/2001/XMLSchema}date"
 * />
 *       &lt;attribute name="identificationIssueCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"
 * />
 *       &lt;attribute name="localityCodeOfResidence" type="{http://www.w3.org/2001/XMLSchema}string"
 * />
 *       &lt;attribute name="numBags" use="required" type="{http://www.w3.org/2001/XMLSchema}int"
 * />
 *       &lt;attribute name="middleName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="meal" type="{http://api.edreams.com/booking/next}travellerMeal" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@Deprecated public class TravellerDTO implements Serializable {

  protected TravellerTypeDTO travellerType;
  protected TravellerTitleDTO title;
  protected String name;
  protected String firstLastName;
  protected String secondLastName;
  protected TravellerGenderDTO travellerGender;
  protected long dateOfBirth;
  protected String nationalityCountryCode;
  protected String countryCodeOfResidence;
  protected String identification;
  protected TravellerIdentificationTypeDTO identificationType;
  protected long identificationExpirationDate;
  protected String identificationIssueCountryCode;
  protected String localityCodeOfResidence;
  protected List<BaggageSelectionDTO> baggageSelections;
  protected List<BaggageSelectionDTO> includedBaggage;
  protected String middleName;
  protected TravellerMealDTO meal;
  protected int numPassenger;

  @Deprecated @Until(Constants.DTOVersion.DEFAULT) protected int numBags;

  @Deprecated public int getNumBags() {
    return numBags;
  }

  @Deprecated public void setNumBags(int numBags) {
    this.numBags = numBags;
  }

  /**
   * Gets the value of the travellerType property.
   *
   * @return possible object is {@link com.odigeo.app.android.lib.models.dto.TravellerTypeDTO }
   */
  public TravellerTypeDTO getTravellerType() {
    return travellerType;
  }

  /**
   * Sets the value of the travellerType property.
   *
   * @param value allowed object is {@link com.odigeo.app.android.lib.models.dto.TravellerTypeDTO }
   */
  public void setTravellerType(TravellerTypeDTO value) {
    this.travellerType = value;
  }

  /**
   * Gets the value of the title property.
   *
   * @return possible object is {@link com.odigeo.app.android.lib.models.dto.TravellerTitleDTO }
   */
  public TravellerTitleDTO getTitle() {
    return title;
  }

  /**
   * Sets the value of the title property.
   *
   * @param value allowed object is {@link com.odigeo.app.android.lib.models.dto.TravellerTitleDTO
   * }
   */
  public void setTitle(TravellerTitleDTO value) {
    this.title = value;
  }

  /**
   * Gets the value of the name property.
   *
   * @return possible object is {@link String }
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the value of the name property.
   *
   * @param value allowed object is {@link String }
   */
  public void setName(String value) {
    this.name = value;
  }

  /**
   * Gets the value of the firstLastName property.
   *
   * @return possible object is {@link String }
   */
  public String getFirstLastName() {
    return firstLastName;
  }

  /**
   * Sets the value of the firstLastName property.
   *
   * @param value allowed object is {@link String }
   */
  public void setFirstLastName(String value) {
    this.firstLastName = value;
  }

  /**
   * Gets the value of the secondLastName property.
   *
   * @return possible object is {@link String }
   */
  public String getSecondLastName() {
    return secondLastName;
  }

  /**
   * Sets the value of the secondLastName property.
   *
   * @param value allowed object is {@link String }
   */
  public void setSecondLastName(String value) {
    this.secondLastName = value;
  }

  /**
   * Gets the value of the travellerGender property.
   *
   * @return possible object is {@link com.odigeo.app.android.lib.models.dto.TravellerGenderDTO }
   */
  public TravellerGenderDTO getTravellerGender() {
    return travellerGender;
  }

  /**
   * Sets the value of the travellerGender property.
   *
   * @param value allowed object is {@link com.odigeo.app.android.lib.models.dto.TravellerGenderDTO
   * }
   */
  public void setTravellerGender(TravellerGenderDTO value) {
    this.travellerGender = value;
  }

  /**
   * Gets the value of the dateOfBirth property.
   *
   * @return possible object is {@link long }
   */
  public long getDateOfBirth() {
    return dateOfBirth;
  }

  /**
   * Sets the value of the dateOfBirth property.
   *
   * @param value allowed object is {@link long }
   */
  public void setDateOfBirth(long value) {
    this.dateOfBirth = value;
  }

  /**
   * Gets the value of the nationalityCountryCode property.
   *
   * @return possible object is {@link String }
   */
  public String getNationalityCountryCode() {
    return nationalityCountryCode;
  }

  /**
   * Sets the value of the nationalityCountryCode property.
   *
   * @param value allowed object is {@link String }
   */
  public void setNationalityCountryCode(String value) {
    this.nationalityCountryCode = value;
  }

  /**
   * Gets the value of the countryCodeOfResidence property.
   *
   * @return possible object is {@link String }
   */
  public String getCountryCodeOfResidence() {
    return countryCodeOfResidence;
  }

  /**
   * Sets the value of the countryCodeOfResidence property.
   *
   * @param value allowed object is {@link String }
   */
  public void setCountryCodeOfResidence(String value) {
    this.countryCodeOfResidence = value;
  }

  /**
   * Gets the value of the identification property.
   *
   * @return possible object is {@link String }
   */
  public String getIdentification() {
    return identification;
  }

  /**
   * Sets the value of the identification property.
   *
   * @param value allowed object is {@link String }
   */
  public void setIdentification(String value) {
    this.identification = value;
  }

  /**
   * Gets the value of the identificationType property.
   *
   * @return possible object is {@link com.odigeo.app.android.lib.models.dto.TravellerIdentificationTypeDTO
   * }
   */
  public TravellerIdentificationTypeDTO getIdentificationType() {
    return identificationType;
  }

  /**
   * Sets the value of the identificationType property.
   *
   * @param value allowed object is {@link com.odigeo.app.android.lib.models.dto.TravellerIdentificationTypeDTO
   * }
   */
  public void setIdentificationType(TravellerIdentificationTypeDTO value) {
    this.identificationType = value;
  }

  /**
   * Gets the value of the identificationExpirationDate property.
   *
   * @return possible object is {@link long }
   */
  public long getIdentificationExpirationDate() {
    return identificationExpirationDate;
  }

  /**
   * Sets the value of the identificationExpirationDate property.
   *
   * @param value allowed object is {@link long }
   */
  public void setIdentificationExpirationDate(long value) {
    this.identificationExpirationDate = value;
  }

  /**
   * Gets the value of the identificationIssueCountryCode property.
   *
   * @return possible object is {@link String }
   */
  public String getIdentificationIssueCountryCode() {
    return identificationIssueCountryCode;
  }

  /**
   * Sets the value of the identificationIssueCountryCode property.
   *
   * @param value allowed object is {@link String }
   */
  public void setIdentificationIssueCountryCode(String value) {
    this.identificationIssueCountryCode = value;
  }

  /**
   * Gets the value of the localityCodeOfResidence property.
   *
   * @return possible object is {@link String }
   */
  public String getLocalityCodeOfResidence() {
    return localityCodeOfResidence;
  }

  /**
   * Sets the value of the localityCodeOfResidence property.
   *
   * @param value allowed object is {@link String }
   */
  public void setLocalityCodeOfResidence(String value) {
    this.localityCodeOfResidence = value;
  }

  /**
   * Gets the value of the middleName property.
   *
   * @return possible object is {@link String }
   */
  public String getMiddleName() {
    return middleName;
  }

  /**
   * Sets the value of the middleName property.
   *
   * @param value allowed object is {@link String }
   */
  public void setMiddleName(String value) {
    this.middleName = value;
  }

  /**
   * Gets the value of the meal property.
   *
   * @return possible object is {@link com.odigeo.app.android.lib.models.dto.TravellerMealDTO }
   */
  public TravellerMealDTO getMeal() {
    return meal;
  }

  /**
   * Sets the value of the meal property.
   *
   * @param value allowed object is {@link com.odigeo.app.android.lib.models.dto.TravellerMealDTO }
   */
  public void setMeal(TravellerMealDTO value) {
    this.meal = value;
  }

  /**
   * Gets the value of baggage selection
   *
   * @return possible object is {@link com.odigeo.app.android.lib.models.dto.BaggageSelectionDTO }
   */
  @NonNull public List<BaggageSelectionDTO> getBaggageSelections() {
    if (baggageSelections == null) {
      baggageSelections = new ArrayList<>();
    }
    return this.baggageSelections;
  }

  /**
   * Sets the value of the meal property.
   *
   * @param value allowed object is {@link com.odigeo.app.android.lib.models.dto.BaggageSelectionDTO
   * }
   */
  public void setBaggageSelections(List<BaggageSelectionDTO> value) {
    this.baggageSelections = value;
  }

  @NonNull public List<BaggageSelectionDTO> getIncludedBaggage() {
    if (includedBaggage == null) {
      includedBaggage = new ArrayList<>();
    }
    return includedBaggage;
  }

  public void setIncludedBaggage(List<BaggageSelectionDTO> includedBaggage) {
    this.includedBaggage = includedBaggage;
  }

  public int getNumPassenger() {
    return numPassenger;
  }

  public void setNumPassenger(int numPassenger) {
    this.numPassenger = numPassenger;
  }
}
