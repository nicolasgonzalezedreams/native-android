package com.odigeo.app.android.lib.models.dto.responses;

import com.globant.roboneck.common.NeckCookie;
import com.odigeo.app.android.lib.models.dto.ItinerarySortCriteriaDTO;
import com.odigeo.app.android.lib.models.dto.ResidentValidationRetryDTO;
import com.odigeo.app.android.lib.models.dto.ShoppingCartDTO;
import com.odigeo.app.android.lib.models.dto.custom.PricingBreakdown;
import java.io.Serializable;
import java.util.List;

@Deprecated public class CreateShoppingCartResponse extends PreferencesAwareResponseDTO
    implements Serializable {

  private ShoppingCartDTO shoppingCart;
  private ItinerarySortCriteriaDTO sortCriteria;
  private PricingBreakdown pricingBreakdown;
  private boolean userCommentRequired;
  private ResidentValidationRetryDTO residentValidationRetry;

  private List<NeckCookie> cookies;

  public final ShoppingCartDTO getShoppingCart() {
    return shoppingCart;
  }

  public final void setShoppingCart(ShoppingCartDTO shoppingCart) {
    this.shoppingCart = shoppingCart;
  }

  public final ItinerarySortCriteriaDTO getSortCriteria() {
    return sortCriteria;
  }

  public final void setSortCriteria(ItinerarySortCriteriaDTO sortCriteria) {
    this.sortCriteria = sortCriteria;
  }

  public final boolean getUserCommentRequired() {
    return userCommentRequired;
  }

  public final void setUserCommentRequired(boolean userCommentRequired) {
    this.userCommentRequired = userCommentRequired;
  }

  public final ResidentValidationRetryDTO getResidentValidationRetry() {
    return residentValidationRetry;
  }

  public final void setResidentValidationRetry(ResidentValidationRetryDTO residentValidationRetry) {
    this.residentValidationRetry = residentValidationRetry;
  }

  public final PricingBreakdown getPricingBreakdown() {
    return pricingBreakdown;
  }

  public final void setPricingBreakdown(PricingBreakdown pricingBreakdown) {
    this.pricingBreakdown = pricingBreakdown;
  }

  public final List<NeckCookie> getCookies() {
    return cookies;
  }

  public final void setCookies(List<NeckCookie> cookies) {
    this.cookies = cookies;
  }
}
