package com.odigeo.app.android.lib.models.dto;

import java.util.List;

/**
 * @author miguel
 */
public class ItinerariesLegendDTO {

  protected List<CarrierDTO> carriers;
  protected List<LocationDTO> locations;
  protected List<SectionResultDTO> sectionResults;
  protected List<SegmentResultDTO> segmentResults;
  protected List<FlightFareInformationPassengerResultDTO> fareInfoPaxResults;
  protected List<CollectionEstimationFeesDTO> collectionEstimationFees;
  protected List<CollectionMethodLegendDTO> collectionMethods;
  protected List<BaggageEstimationFeesDTO> baggageEstimationFees;
  protected ItinerariesDTO itineraries;
  protected ResidentsValidationDTO residentsValidation;

  /**
   * @return the carriers
   */
  public List<CarrierDTO> getCarriers() {
    return carriers;
  }

  /**
   * @param carriers the carriers to set
   */
  public void setCarriers(List<CarrierDTO> carriers) {
    this.carriers = carriers;
  }

  /**
   * @return the locations
   */
  public List<LocationDTO> getLocations() {
    return locations;
  }

  /**
   * @param locations the locations to set
   */
  public void setLocations(List<LocationDTO> locations) {
    this.locations = locations;
  }

  /**
   * @return the sectionResults
   */
  public List<SectionResultDTO> getSectionResults() {
    return sectionResults;
  }

  /**
   * @param sectionResults the sectionResults to set
   */
  public void setSectionResults(List<SectionResultDTO> sectionResults) {
    this.sectionResults = sectionResults;
  }

  /**
   * @return the segmentResults
   */
  public List<SegmentResultDTO> getSegmentResults() {
    return segmentResults;
  }

  /**
   * @param segmentResults the segmentResults to set
   */
  public void setSegmentResults(List<SegmentResultDTO> segmentResults) {
    this.segmentResults = segmentResults;
  }

  /**
   * @return the fareInfoPaxResults
   */
  public List<FlightFareInformationPassengerResultDTO> getFareInfoPaxResults() {
    return fareInfoPaxResults;
  }

  /**
   * @param fareInfoPaxResults the fareInfoPaxResults to set
   */
  public void setFareInfoPaxResults(
      List<FlightFareInformationPassengerResultDTO> fareInfoPaxResults) {
    this.fareInfoPaxResults = fareInfoPaxResults;
  }

  /**
   * @return the collectionEstimationFees
   */
  public List<CollectionEstimationFeesDTO> getCollectionEstimationFees() {
    return collectionEstimationFees;
  }

  /**
   * @param collectionEstimationFees the collectionEstimationFees to set
   */
  public void setCollectionEstimationFees(
      List<CollectionEstimationFeesDTO> collectionEstimationFees) {
    this.collectionEstimationFees = collectionEstimationFees;
  }

  /**
   * @return the collectionMethods
   */
  public List<CollectionMethodLegendDTO> getCollectionMethods() {
    return collectionMethods;
  }

  /**
   * @param collectionMethods the collectionMethods to set
   */
  public void setCollectionMethods(List<CollectionMethodLegendDTO> collectionMethods) {
    this.collectionMethods = collectionMethods;
  }

  /**
   * @return the baggageEstimationFees
   */
  public List<BaggageEstimationFeesDTO> getBaggageEstimationFees() {
    return baggageEstimationFees;
  }

  /**
   * @param baggageEstimationFees the baggageEstimationFees to set
   */
  public void setBaggageEstimationFees(List<BaggageEstimationFeesDTO> baggageEstimationFees) {
    this.baggageEstimationFees = baggageEstimationFees;
  }

  /**
   * @return the itineraries
   */
  public ItinerariesDTO getItineraries() {
    return itineraries;
  }

  /**
   * @param itineraries the itineraries to set
   */
  public void setItineraries(ItinerariesDTO itineraries) {
    this.itineraries = itineraries;
  }

  /**
   * @return the residentsValidation
   */
  public ResidentsValidationDTO getResidentsValidation() {
    return residentsValidation;
  }

  /**
   * @param residentsValidation the residentsValidation to set
   */
  public void setResidentsValidation(ResidentsValidationDTO residentsValidation) {
    this.residentsValidation = residentsValidation;
  }
}
