package com.odigeo.app.android.lib.ui.widgets.base;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.Validations;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by manuel on 02/10/14.
 */
public abstract class EditWithHint extends CustomField {

  protected static final int DEFAULT_TEXT_SIZE = 0;
  private final int inputType;
  private final List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();
  protected EditWithHint thisView = this;
  protected ValueAnimator mFocusToUnfocusAnimation;
  protected ValueAnimator mUnfocusToFocusAnimation;
  private String text;
  private String hint;
  private String warningMessage;
  private int textSize;
  private int hintSize;
  private int mFocusedColor;
  private int mUnFocusedColor;
  private EditText editText;
  private TextView txtHint;
  private final TextWatcher textWatcher = new TextWatcher() {

    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      //Nothing to do here
    }

    @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
      if (getCustomFieldListener() != null) {
        getCustomFieldListener().onFieldValueChanged(thisView, s.toString());
      }
    }

    @Override public void afterTextChanged(Editable s) {
      if (TextUtils.isEmpty(s)) {
        if (txtHint.getVisibility() == VISIBLE) {
          hideFloatingLabel();
        }
      } else {
        if (txtHint.getVisibility() == INVISIBLE) {
          showFloatingLabel();
        }

        if (editText.getError() != null) {
          clearValidation();
        }
      }
    }
  };
  private boolean changingFocus;
  private PopupWindow warningLatinCharacters;
  private ColorStateList textColor;
  private ColorStateList basicLight;
  private String tag;
  private final OnFocusChangeListener onFocusChangeListener = new OnFocusChangeListener() {

    @Override public void onFocusChange(View v, boolean hasFocus) {
      ValueAnimator lColorAnimation;

      if (hasFocus) {
        lColorAnimation = getFocusToUnFocusAnimation();
        if (warningMessage != null && editText.getError() == null) {
          showWarning();
        }
      } else {
        lColorAnimation = getUnFocusToFocusAnimation();
        if (warningMessage != null) {
          hideWarning();
        }
      }

      lColorAnimation.setDuration(700);
      lColorAnimation.start();

      if (!hasFocus && !changingFocus && !TextUtils.isEmpty(editText.getText())) {
        validate();
        editText.clearFocus();
      }
    }

    private ValueAnimator getFocusToUnFocusAnimation() {
      if (mFocusToUnfocusAnimation == null) {
        mFocusToUnfocusAnimation = getFocusAnimation(mUnFocusedColor, mFocusedColor);
      }
      return mFocusToUnfocusAnimation;
    }

    private ValueAnimator getUnFocusToFocusAnimation() {
      if (mUnfocusToFocusAnimation == null) {
        mUnfocusToFocusAnimation = getFocusAnimation(mFocusedColor, mUnFocusedColor);
      }
      return mUnfocusToFocusAnimation;
    }
  };
  private CharSequence hintString;

  public EditWithHint(Context context) {
    super(context);

    textSize = DEFAULT_TEXT_SIZE;
    hintSize = DEFAULT_TEXT_SIZE;
    mFocusedColor = android.R.color.black;
    mUnFocusedColor = android.R.color.darker_gray;
    inputType = EditorInfo.TYPE_CLASS_TEXT;

    inflateLayout();
  }

  public EditWithHint(Context context, AttributeSet attrs) {
    super(context, attrs);

    //Get Design time params
    TypedArray aAttrs = context.obtainStyledAttributes(attrs, R.styleable.FloatLabelEditText, 0, 0);
    text = aAttrs.getString(R.styleable.FloatLabelEditText_text);
    hint = aAttrs.getString(R.styleable.FloatLabelEditText_hint);
    textSize =
        aAttrs.getDimensionPixelSize(R.styleable.FloatLabelEditText_textSize, DEFAULT_TEXT_SIZE);
    hintSize =
        aAttrs.getDimensionPixelSize(R.styleable.FloatLabelEditText_hintSize, DEFAULT_TEXT_SIZE);
    textColor = aAttrs.getColorStateList(R.styleable.FloatLabelEditText_textColor);
    basicLight = aAttrs.getColorStateList(R.styleable.FloatLabelEditText_hintColor);
    mFocusedColor =
        aAttrs.getColor(R.styleable.FloatLabelEditText_textColorHintFocused, android.R.color.black);
    mUnFocusedColor = aAttrs.getColor(R.styleable.FloatLabelEditText_textColorHintUnFocused,
        android.R.color.darker_gray);
    inputType =
        aAttrs.getInt(R.styleable.FloatLabelEditText_android_inputType, EditorInfo.TYPE_CLASS_TEXT);

    aAttrs.recycle();

    inflateLayout();
  }

  public abstract int getLayout();

  private void inflateLayout() {
    inflate(this.getContext(), getLayout(), this);

    setEditText((EditText) findViewById(R.id.editTextEdiWithHint_text));
    setTxtHint((TextView) findViewById(R.id.editTextEditWithHint_hint));

    getEditText().setOnFocusChangeListener(onFocusChangeListener);

    hintString = LocalizablesFacade.getString(getContext(), hint);

    getEditText().setHint(hintString);
    getEditText().setInputType(inputType);
    getEditText().setTypeface(Configuration.getInstance().getFonts().getRegular());

    setViewSize(getEditText(), textSize);
    setViewSize(getTxtHint(), hintSize);

    setViewColor(getEditText(), textColor);

    if (basicLight != null) {
      getEditText().setHintTextColor(basicLight);
    }

    if (getEditText() != null) {
      getEditText().setText(text);
    }

    if (getTxtHint() != null) {
      getTxtHint().setText(hintString);
      getTxtHint().setVisibility(INVISIBLE);
    }

    if (getText() != null) {
      showFloatingLabel();
    }

    getEditText().addTextChangedListener(textWatcher);

    setClickable(true);

    this.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        editText.requestFocus();

        InputMethodManager imm =
            (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
      }
    });
  }

  @Override public OnFocusChangeListener getOnFocusChangeListener() {
    //Return the edit text focus change listener, in order to let other classes add custom actions to it
    return editText.getOnFocusChangeListener();
  }

  @Override public void setOnFocusChangeListener(OnFocusChangeListener l) {
    //Let the editText handle focus change listeners
    editText.setOnFocusChangeListener(l);
  }

  private void showFloatingLabel() {
    txtHint.setVisibility(VISIBLE);
    txtHint.startAnimation(AnimationUtils.loadAnimation(getContext(),
        R.anim.weddingparty_floatlabel_slide_from_bottom));
  }

  private void hideFloatingLabel() {
    txtHint.setVisibility(INVISIBLE);
    txtHint.startAnimation(
        AnimationUtils.loadAnimation(getContext(), R.anim.weddingparty_floatlabel_slide_to_bottom));
  }

  /**
   * Gets the content form the edit text, but if its empty, we must return null in order to
   * prevent MSL errors
   *
   * @return The content from the edit text, could return null if the content is empty
   */
  @Nullable public final String getText() {
    if (getEditTextString().toString().trim().isEmpty()) {
      return null;
    } else {
      return getEditTextString().toString();
    }
  }

  public void setText(String text) {
    this.text = text;

    if (getEditText() != null) {
      getEditText().setText(text);
    }
  }

  private Editable getEditTextString() {
    return editText.getText();
  }

  protected final void setViewSize(TextView textViewItem, int textSizeValue) {
    if (textSizeValue > 0) {
      textViewItem.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeValue);
    }
  }

  protected final void setViewColor(TextView textViewItem, ColorStateList colorValue) {
    if (colorValue != null) {
      textViewItem.setTextColor(colorValue);
    }
  }

  public String getHint() {
    return hint;
  }

  public void setHint(String hint) {
    this.hint = hint;

    if (getTxtHint() != null) {
      getTxtHint().setText(hint);
    }

    if (getEditText() != null) {
      getEditText().setHint(hint);
    }
  }

  protected int getTextSize() {
    return textSize;
  }

  protected void setTextSize(int textSize) {
    this.textSize = textSize;
  }

  protected int getHintSize() {
    return hintSize;
  }

  protected void setHintSize(int hintSize) {
    this.hintSize = hintSize;
  }

  protected ColorStateList getTextColor() {
    return textColor;
  }

  public void setTextColor(ColorStateList textColor) {
    this.textColor = textColor;
  }

  protected ColorStateList getTextbasicLight() {
    return basicLight;
  }

  public void setTextbasicLight(ColorStateList textbasicLight) {
    this.basicLight = textbasicLight;
  }

  protected final TextView getTxtHint() {
    return txtHint;
  }

  protected final void setTxtHint(TextView txtHint) {
    this.txtHint = txtHint;
  }

  public final EditText getEditText() {
    return editText;
  }

  public final void setEditText(EditText editText) {
    this.editText = editText;
  }

  private ValueAnimator getFocusAnimation(int fromColor, int toColor) {
    ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), fromColor, toColor);
    colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

      @Override public void onAnimationUpdate(ValueAnimator animator) {
        txtHint.setTextColor((Integer) animator.getAnimatedValue());
      }
    });
    return colorAnimation;
  }

  /**
   * check if the format is correct and add the corresponding error message
   *
   * @return if the format is correct
   */
  @Override public boolean validate() {

    if (!isValid()) {

      notifyListeners(getTag(), "", "");

      changingFocus = true;
      showError();
      changingFocus = false;

      return false;
    }

    return true;
  }

  public boolean validate(ScrollView scroll) {

    if (!isValid()) {

      changingFocus = true;

      showError();
      editText.requestFocus();

      setFocusOnItem(scroll);

      changingFocus = false;

      return false;
    }

    return true;
  }

  @Override public void showError() {
    editText.setError(getErrorText());
  }

  @Override public void hideError() {
    editText.setError(null);
  }

  /**
   * check if the format is correct
   *
   * @return if the format is correct
   */
  @Override public boolean isValid() {

    if (this.getVisibility() == GONE) {
      return true;
    }
    String value = "";
    if (getText() != null) {
      value = getText().trim();
    }

    return !isMandatory() && TextUtils.isEmpty(value)
        || !TextUtils.isEmpty(value) && Validations.validate(getValidationFormat(), value);
  }

  @Override public void clearValidation() {
    editText.setError(null);
  }

  private void notifyListeners(String property, String oldValue, String newValue) {
    for (PropertyChangeListener name : listener) {
      name.propertyChange(new PropertyChangeEvent(this, property, oldValue, newValue));
    }
  }

  public void addChangeListener(PropertyChangeListener newListener) {
    listener.add(newListener);
  }

  @Override public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public void setbasicLightFocused(int mFocusedColor) {
    this.mFocusedColor = mFocusedColor;

    mUnfocusToFocusAnimation = getFocusAnimation(mFocusedColor, mUnFocusedColor);
    mFocusToUnfocusAnimation = getFocusAnimation(mUnFocusedColor, mFocusedColor);
  }

  public void setbasicLightUnFocused(int mUnFocusedColor) {
    this.mUnFocusedColor = mUnFocusedColor;

    mUnfocusToFocusAnimation = getFocusAnimation(mFocusedColor, mUnFocusedColor);
    mFocusToUnfocusAnimation = getFocusAnimation(mUnFocusedColor, mFocusedColor);
  }

  public void setWarningMessage(String warningMessage) {
    this.warningMessage = warningMessage;
  }

  public void showWarning() {

    if (warningLatinCharacters == null) {

      View warningView = LayoutInflater.from(getContext())
          .inflate(R.layout.layout_warning_latin_characters_popup, null);

      float scale = getResources().getDisplayMetrics().density;
      int width = (int) (200 * scale + 0.5f);
      int height = (int) (50 * scale + 0.5f);
      warningLatinCharacters = new PopupWindow(warningView, width, height);
      warningLatinCharacters.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);

      warningLatinCharacters.setFocusable(false);
      warningLatinCharacters.setOutsideTouchable(true);
      warningLatinCharacters.setBackgroundDrawable(new BitmapDrawable());
    }
    if (warningMessage == null) {
      warningMessage =
          LocalizablesFacade.getString(getContext(), "passengerpicker_screen_latin_characters")
              .toString();
    }

    View warningView = warningLatinCharacters.getContentView();
    TextView textWarningView = (TextView) warningView.findViewById(R.id.text_warning_view);
    textWarningView.setText(warningMessage);
    chooseSize(warningLatinCharacters, warningMessage, textWarningView);

    warningLatinCharacters.showAsDropDown(editText, 100, 0);
  }

  public void hideWarning() {
    if (warningLatinCharacters != null && warningLatinCharacters.isShowing()) {
      warningLatinCharacters.dismiss();
    }
  }

  private void chooseSize(PopupWindow popup, CharSequence message, TextView tv) {
    int wid = tv.getPaddingLeft() + tv.getPaddingRight();
    int ht = tv.getPaddingTop() + tv.getPaddingBottom();

        /*
         * Figure out how big the text would be if we laid it out to the
         * full width of this view minus the border.
         * */
    int cap = editText.getWidth() - wid;
    if (cap < 0) {
      cap = 200; // We must not be measured yet -- setFrame() will fix it.
    }
    Layout l =
        new StaticLayout(message, tv.getPaint(), cap, Layout.Alignment.ALIGN_NORMAL, 1, 0, true);
    float max = 0;
    for (int i = 0; i < l.getLineCount(); i++) {
      max = Math.max(max, l.getLineWidth(i));
    }
    popup.setWidth(wid + (int) Math.ceil(max));
    popup.setHeight(ht + l.getHeight());
  }

  public final void setMaxLenght(int maxLength) {
    InputFilter[] existingFiltersArray = editText.getFilters();
    int filterSize = existingFiltersArray.length + 1;
    InputFilter[] fArray = new InputFilter[filterSize];
    System.arraycopy(existingFiltersArray, 0, fArray, 0, existingFiltersArray.length);

    fArray[filterSize - 1] = new InputFilter.LengthFilter(maxLength);
    editText.setFilters(fArray);
  }
}
