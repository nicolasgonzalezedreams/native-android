package com.odigeo.app.android.view;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.View;
import com.localytics.android.Localytics;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.tracker.TuneTrackerInterface;
import com.odigeo.interactors.CheckUserCredentialsInteractor;
import com.odigeo.presenter.SplashPresenter;
import com.odigeo.presenter.contracts.navigators.SplashNavigatorInterface;
import com.odigeo.presenter.contracts.views.SplashViewInterface;
import com.squareup.otto.Subscribe;

public class SplashView extends BaseView<SplashPresenter> implements SplashViewInterface {

  private static final int SPLASH_TIME_MILS = 3000;
  private TuneTrackerInterface mTuneTracker;

  private Handler handler = new Handler();
  private Runnable runnable = new Runnable() {
    @Override public void run() {
      mPresenter.goToNextActivity();
    }
  };

  public static Fragment getInstance() {
    return new SplashView();
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    boolean wasAppInstalled = PreferencesManager.wasAppInstalled(getActivity());
    PreferencesManager.setWasAppInstalled(getActivity(), true);
    mTuneTracker = AndroidDependencyInjector.getInstance().provideTuneTracker();
    mTuneTracker.setExistingUser(wasAppInstalled);
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setQaModeDomain();
    registerPush();

    updateUserLoginTrackerDimension();
    trackUserLaunchedApp();

    Configuration.getInstance().updateUserAgent(getActivity());
    PreferencesManager.setNewVersion(getActivity());

    mPresenter.startOneCMSTimer();

    String numOfDays =
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.DROP_OFF_NUMBER_OF_DAYS).toString();
    mPresenter.updateLastAppOpening(numOfDays);

    initNextActivityStarterHandler();

    mPresenter.initABManager();
    mPresenter.checkActiveBookingsOnLaunch();
    mPresenter.initRemoteConfig();
  }

  @Override public void onStart() {
    super.onStart();
    if (((OdigeoApp) getActivity().getApplication()).hasAnimatedBackground()) {
      ViewUtils.loadAnimate(getView().findViewById(R.id.image_nube_top), R.anim.translate_nube_top);
      ViewUtils.loadAnimate(getView().findViewById(R.id.image_nube_botton),
          R.anim.translate_nube_bottom);
    }
  }

  @Override public void onStop() {
    super.onStop();
    handler.removeCallbacks(runnable);
  }

  @Override protected SplashPresenter getPresenter() {
    return AndroidDependencyInjector.getInstance()
        .provideSplashPresenter(this, (SplashNavigatorInterface) getActivity());
  }

  @Override protected int getFragmentLayout() {
    return R.layout.activity_splash;
  }

  @Override protected void initComponent(View view) {

  }

  @Override protected void setListeners() {

  }

  @Override protected void initOneCMSText(View view) {

  }

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getActivity().getApplication());
  }

  private void initNextActivityStarterHandler() {
    handler.postDelayed(runnable, SPLASH_TIME_MILS);
  }

  /**
   * Sets the QA url selected to the domain if the environment is testing. This fix the problem that
   * the QA MODE URL selected is not
   * active when the app is restarted.
   */
  private void setQaModeDomain() {
    // set the environment on debug mode
    if (Constants.isTestEnvironment) {
      final String mslUrl = dependencyInjector.providePreferencesController().getQAModeUrl();
      dependencyInjector.provideDomainHelper().putUrl(mslUrl);
      Constants.domain = mslUrl;
    }
  }

  //  Register for push notifications
  private void registerPush() {
    // Used while testing
    Localytics.setLoggingEnabled(false);

    mPresenter.startFirebaseService();

    AndroidDependencyInjector.getInstance().provideTrackerController().trackDirectAppLaunch();

    //Create an asycn task to check if the market custom dim is set
    AsyncTask<Void, Void, String> call = new AsyncTask<Void, Void, String>() {
      @Override protected String doInBackground(Void[] params) {
        //Check the value of the custom dim, can't be done in the main thread
        return Localytics.getCustomDimension(Constants.LOCALYTICS_MARKET_CUSTOM_INDEX);
      }

      @Override protected void onPostExecute(String s) {
        //If the custom dim is null, means that has not been set, so update it
        if (s == null) {
          Localytics.setCustomDimension(Constants.LOCALYTICS_MARKET_CUSTOM_INDEX,
              Configuration.getInstance().getCurrentMarket().getName());
        }
      }
    };
    call.execute();
  }

  private void trackUserLaunchedApp() {
    mTracker.trackAppLaunched();
  }

  private void updateUserLoginTrackerDimension() {
    CheckUserCredentialsInteractor checkUserCredentialsInteractor =
        AndroidDependencyInjector.getInstance().provideCheckUserCredentialsInteractor();
    mTracker.updateDimensionOnUserLogin(checkUserCredentialsInteractor.isUserLogin());
  }
}
