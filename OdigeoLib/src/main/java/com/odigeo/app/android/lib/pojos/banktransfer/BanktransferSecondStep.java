package com.odigeo.app.android.lib.pojos.banktransfer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.ui.widgets.CustomTextView;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @author Oscar Álvarez
 * @version 1.0
 * @since 30/04/15
 */
public class BanktransferSecondStep extends IncompleteBookingBean {
  private final String bookingId;
  private final String fax;

  public BanktransferSecondStep(@NonNull String bookingId, @NonNull String fax) {
    this.bookingId = bookingId;
    this.fax = fax;
  }

  @Override public View inflate(@NonNull Context context, @NonNull ViewGroup parent) {
    View view = LayoutInflater.from(context)
        .inflate(R.layout.second_step_banktransfer_booking, parent, false);

    CustomTextView faxNumber =
        ((CustomTextView) view.findViewById(R.id.txt_step2_booking_banktransfer_fax_number));
    faxNumber.setText(fax);

    CustomTextView reference =
        ((CustomTextView) view.findViewById(R.id.txt_step2_booking_banktransfer_reference));

    CharSequence orderId = LocalizablesFacade.getString(context,
        "confirmation_howtocompletebooking_deposit_bookingreason", bookingId);

    reference.setText(orderId);
    return view;
  }
}
