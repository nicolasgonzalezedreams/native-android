package com.odigeo.app.android.view;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.dataodigeo.tracker.TrackerConstants;

public class UserAlreadyRegisteredView extends LoginView {

  public static final String USER_EMAIL = "USER_EMAIL";

  public static UserAlreadyRegisteredView newInstance(String email) {
    UserAlreadyRegisteredView view = new UserAlreadyRegisteredView();
    Bundle args = new Bundle();
    args.putString(USER_EMAIL, email);
    view.setArguments(args);
    return view;
  }

  @Override public void initComponent(View view) {
    super.initComponent(view);
    mUsername.setText(getEmail());
    mIsMailFormatCorrect = true;
  }

  @Override protected int getFragmentLayout() {
    return R.layout.layout_register_existing_email_screen;
  }

  @Override protected void setListeners() {
    setLoginLegacyListeners();
  }

  private String getEmail() {
    Bundle args = this.getArguments();
    String email = "";
    if (args != null && args.containsKey(USER_EMAIL)) {
      email = args.getString(USER_EMAIL);
    }
    return email;
  }

  @Override protected void initOneCMSText(View view) {
    TextInputLayout tilUsername = (TextInputLayout) view.findViewById(R.id.etEmailLayout);
    TextInputLayout tilPassword = (TextInputLayout) view.findViewById(R.id.etPasswordLayout);
    tilUsername.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_FORM_SHADOWTEXT_EMAIL));
    tilPassword.setHint(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_FORM_SHADOWTEXT_PASSWORD));
    mBtnLogin.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_LOGIN_BUTTON));
    mCbShowPassword.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_SHOW_PASSWORD));
    TextView tvAlreadyRegistered = (TextView) view.findViewById(R.id.tvAlreadyRegistered);
    tvAlreadyRegistered.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SOO_REGISTERINFO_ALREADY_REGISTERED,
            getEmail()));
  }

  @Override public void onResume() {
    super.onResume();
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_REGISTER_EMAIL_ALREADY_USED);
  }
}
