package com.odigeo.app.android.lib.models.dto;

/**
 * @author carlos.aller@odigeo.com
 * @since 14/06/13
 */
public enum ProductTypeDTO {
  FLIGHT, TRAIN, HOTEL, INSURANCE, SMS, MONITORING_ALERT, POSTSELL_SERVICE_OPTION, ADDITIONAL_PRODUCT
}
