package com.odigeo.app.android.lib.utils;

import com.odigeo.app.android.lib.models.CollectionEstimationFees;
import com.odigeo.app.android.lib.models.CollectionMethodWithPrice;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;

import static com.odigeo.app.android.lib.utils.LocaleUtils.strToLocale;

public class ResultsPriceCalculator {

  public static String calculatePrice(FareItineraryDTO fareItineraryDTO,
      CollectionMethodWithPrice selectedCollectionMethod, int totalNumberOfPassengers,
      String locale) {
    double pricePerPassenger =
        calculatePricePerPassenger(fareItineraryDTO, selectedCollectionMethod,
            totalNumberOfPassengers);

    return LocaleUtils.getLocalizedCurrencyValue(pricePerPassenger, strToLocale(locale), true);
  }

  public static double calculatePricePerPassenger(FareItineraryDTO fareItineraryDTO,
      CollectionMethodWithPrice selectedCollectionMethod, int totalNumberOfPassengers) {
    double priceTotal = fareItineraryDTO.getPrice().getSortPrice().doubleValue();

    CollectionMethodWithPrice cheapestCollectionMethodFee =
        getCheapestCollectionMethodFee(fareItineraryDTO.getCollectionMethodFeesObject());

    //Remove cheapest collection method fee in order to show price without fees
    if (cheapestCollectionMethodFee != null) {
      priceTotal -= cheapestCollectionMethodFee.getPrice().doubleValue();
    }

    CollectionMethodWithPrice selectedCollectionMethodFee =
        getSelectedCollectionMethodFee(fareItineraryDTO.getCollectionMethodFeesObject(),
            selectedCollectionMethod);

    //If is FT user is suposed to have selected a collection method fee, if not, cheapest one
    // is set as default. We add that value to the price
    if (selectedCollectionMethodFee != null) {
      priceTotal += selectedCollectionMethodFee.getPrice().doubleValue();
    }

    return priceTotal / totalNumberOfPassengers;
  }

  public static CollectionMethodWithPrice getCheapestCollectionMethodFee(
      CollectionEstimationFees collectionEstimationFees) {

    CollectionMethodWithPrice cheapestPrice = null;

    if (collectionEstimationFees != null
        && collectionEstimationFees.getCollectionMethodFees() != null
        && !collectionEstimationFees.getCollectionMethodFees().isEmpty()) {
      cheapestPrice = collectionEstimationFees.getCheapest();
    }

    return cheapestPrice;
  }

  public static CollectionMethodWithPrice getSelectedCollectionMethodFee(
      CollectionEstimationFees collectionEstimationFees,
      CollectionMethodWithPrice selectedCollectionMethod) {

    CollectionMethodWithPrice selectedPrice = null;

    if (selectedCollectionMethod != null
        && collectionEstimationFees != null
        && collectionEstimationFees.getCollectionMethodFees() != null
        && !collectionEstimationFees.getCollectionMethodFees().isEmpty()) {

      selectedPrice = collectionEstimationFees.getCollectionMethodFees().
          get(selectedCollectionMethod.getCollectionMethod().getCreditCardType().getCode());
    }

    return selectedPrice;
  }

  public static String calculateSlashedPrice(FareItineraryDTO fareItineraryDTO,
      CollectionMethodWithPrice selectedCollectionMethod, int totalNumberOfPassengers, String locale) {
    double pricePerPassenger =
        calculatePricePerPassenger(fareItineraryDTO, selectedCollectionMethod,
            totalNumberOfPassengers);

    double membershipPerksTotal = fareItineraryDTO.getMembershipPerks().getFee().doubleValue();

    double membershipPerksPerPassenger = membershipPerksTotal/totalNumberOfPassengers;

    pricePerPassenger -= membershipPerksPerPassenger;

    return LocaleUtils.getLocalizedCurrencyValue(pricePerPassenger, strToLocale(locale), true);
  }
}
