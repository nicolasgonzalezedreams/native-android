package com.odigeo.app.android.lib.models.dto;

import java.util.List;

public class FraudBookingResultContainerDTO {
  protected List<FraudTrackingItemDTO> fraudTrackingItems;
  protected FraudDecisionTypeDTO fraudBookingDecision;

  public List<FraudTrackingItemDTO> getFraudTrackingItems() {
    return fraudTrackingItems;
  }

  public void setFraudTrackingItems(List<FraudTrackingItemDTO> fraudTrackingItems) {
    this.fraudTrackingItems = fraudTrackingItems;
  }

  /**
   * Gets the value of the fraudBookingDecision property.
   *
   * @return possible object is
   * {@link FraudDecisionTypeDTO }
   */
  public FraudDecisionTypeDTO getFraudBookingDecision() {
    return fraudBookingDecision;
  }

  /**
   * Sets the value of the fraudBookingDecision property.
   *
   * @param value allowed object is
   * {@link FraudDecisionTypeDTO }
   */
  public void setFraudBookingDecision(FraudDecisionTypeDTO value) {
    this.fraudBookingDecision = value;
  }
}
