package com.odigeo.app.android.view;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.utils.deeplinking.DeepLinkingUtil;
import com.odigeo.app.android.view.interfaces.MttCardBackground;
import com.odigeo.data.entity.carrousel.CampaignCard;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.extensions.UIPromocard;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.CampaignCardViewPresenter;
import com.odigeo.presenter.contracts.views.CampaignCardViewInterface;

/**
 * @author José Eduardo Cabrera Rivera
 * @version 1.0
 * @since 11/11/2015.
 */
public class CampaignCardView extends BaseView<CampaignCardViewPresenter>
    implements CampaignCardViewInterface, MttCardBackground {

  private static final String KEY_PROMO_CARD = "KEY_PROMO_CARD";
  protected CampaignCard mCard;
  private int MAX_LENGTH;
  private int LEFT_LIMIT;
  private int RIGHT_LIMIT;
  private TextView mTvCampaignTitle;
  private TextView mTvCampaignBody;
  private TextView mTvCampaignActionButtonOne;
  private TextView mTvCampaignActionButtonTwo;
  private ImageView mIvCampaignImage;
  private int mCardPosition;

  private OdigeoImageLoader imageLoader;
  private OdigeoApp application;

  public static CampaignCardView newInstance(CampaignCard card) {
    Bundle args = new Bundle();
    args.putParcelable(KEY_PROMO_CARD, new UIPromocard(card));
    CampaignCardView fragment = new CampaignCardView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    MAX_LENGTH = getResources().getInteger(R.integer.card_button_max_length);
    LEFT_LIMIT = getResources().getInteger(R.integer.card_button_left_limit);
    RIGHT_LIMIT = getResources().getInteger(R.integer.card_button_right_limit);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    getCardFromArguments();
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override public void onResume() {
    super.onResume();
    application = (OdigeoApp) getActivity().getApplication();
  }

  @Override protected CampaignCardViewPresenter getPresenter() {
    return null;
  }

  @Override protected int getFragmentLayout() {
    return R.layout.campaign_card;
  }

  @Override protected void initComponent(View view) {
    imageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();

    mTvCampaignTitle = (TextView) view.findViewById(R.id.TvCampaignTitle);
    mTvCampaignBody = (TextView) view.findViewById(R.id.TvCampaignBody);
    mTvCampaignActionButtonOne = (TextView) view.findViewById(R.id.TvCampaignActionButtonOne);
    mTvCampaignActionButtonTwo = (TextView) view.findViewById(R.id.TvCampaignActionButtonTwo);
    mIvCampaignImage = (ImageView) view.findViewById(R.id.IvCampaignImage);
    initDataNextTripCard();
    loadCardImage();
  }

  /**
   * This returns empty because the Campaign cards doesn't change the background image of the Home.
   * Also, the image attribute for this card type is used to loadWidgetImage the image inside the
   * card.
   */
  @Override public String getBackgroundImage() {
    return "";
  }

  private void getCardFromArguments() {
    Bundle bundle = getArguments();

    if (bundle.containsKey(KEY_PROMO_CARD)) {
      mCard = (UIPromocard) bundle.getParcelable(KEY_PROMO_CARD);
    }

    if (bundle.containsKey(Card.KEY_CARD_POSITION)) {
      mCardPosition = bundle.getInt(Card.KEY_CARD_POSITION);
    } else {
      mCardPosition = 1;
    }
  }

  private void initDataNextTripCard() {
    try {
      mTvCampaignTitle.setText(mCard.getTitle());
      mTvCampaignBody.setText(mCard.getSubtitle());

      if (!TextUtils.isEmpty(mCard.getUrlText2())) {
        mTvCampaignActionButtonOne.setFilters(
            new InputFilter[] { new InputFilter.LengthFilter(12) });
        mTvCampaignActionButtonTwo.setVisibility(View.VISIBLE);
        String urlText = checkTextLength(mCard.getUrlText());
        String urlText2 = checkTextLength(mCard.getUrlText2());
        mTvCampaignActionButtonOne.setText(urlText);
        mTvCampaignActionButtonTwo.setText(urlText2);
      } else {
        mTvCampaignActionButtonOne.setText(mCard.getUrlText());
      }
    } catch (Exception exception) {
      logIntoCrashlytics(exception);
    }
  }

  private String checkTextLength(String text) {
    if (text.length() > MAX_LENGTH) {
      text = text.substring(LEFT_LIMIT, RIGHT_LIMIT) + "...";
    }

    return text;
  }

  private void loadCardImage() {
    imageLoader.load(mIvCampaignImage, mCard.getImage(), R.drawable.campaign_card_placeholder);
  }

  @Override protected void setListeners() {
    mTvCampaignActionButtonOne.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        try {
          if (mCard.getUrl() != null) {
            openDeeplinkInActionButton(mCard.getUrl());
          }
        } catch (Exception exception) {
          logIntoCrashlytics(exception);
        }
      }
    });

    mTvCampaignActionButtonTwo.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        try {
          if (mCard.getUrl2() != null) {
            openDeeplinkInActionButton(mCard.getUrl2());
          }
        } catch (Exception exception) {
          logIntoCrashlytics(exception);
        }
      }
    });
  }

  private void openDeeplinkInActionButton(String url) {
    try {
      trackCardSeeInfo();
      Intent deepLinkIntent = new Intent(Intent.ACTION_VIEW);
      deepLinkIntent.setData(Uri.parse(url));
      startActivity(deepLinkIntent);
    } catch (ActivityNotFoundException e) {
      Log.e(getTag(), e.getMessage(), e);
      logIntoCrashlytics(e);
    }
  }

  @Override protected void initOneCMSText(View view) {
  }

  public void trackCardSeeInfo() {
    String cardLabel = "";
    DeepLinkingUtil.DeepLinkType deepLinkType =
        DeepLinkingUtil.getCampaignCardType(getContext(), Uri.parse(mCard.getUrl()));
    switch (deepLinkType) {
      case CARS:
        cardLabel = TrackerConstants.LABEL_CARD_CARS;
        break;
      case SEARCH:
        cardLabel = TrackerConstants.LABEL_CARD_PROMO;
        break;
      case HOTELS:
        cardLabel = TrackerConstants.LABEL_CARD_HOTEL;
        break;
      case SSO:
        cardLabel = TrackerConstants.LABEL_CARD_SSO;
        break;
    }
    if (!cardLabel.isEmpty()) {
      String trackingLabel =
          TrackerConstants.LABEL_CARD_SEE_INFO_FORMAT.replace(TrackerConstants.CARD_TYPE_IDENTIFIER,
              cardLabel)
              .replace(TrackerConstants.CARD_POSITION_IDENTIFIER, String.valueOf(mCardPosition));
      mTracker.trackMTTCardEvent(trackingLabel);
    }
  }

  @Override public void setCardPosition(int position) {
    Bundle bundle = getArguments();
    bundle.putInt(Card.KEY_CARD_POSITION, position);
    this.setArguments(bundle);
  }

  private void logIntoCrashlytics(Throwable throwable) {
    if (application != null) {
      application.trackNonFatal(throwable);
      application.setBool("Card is null", mCard == null);
      application.setBool("Card is added", isAdded());
      application.setBool("Card is detached", isDetached());
      application.setBool("Card is hidden", isHidden());
      application.setBool("Card is in layout", isInLayout());
      application.setBool("Card is removing", isRemoving());
      application.setBool("Card is resumed", isResumed());
      application.setBool("Card is visible", isVisible());
      application.setBool("Arguments are null", getArguments() == null);
    }
  }
}
