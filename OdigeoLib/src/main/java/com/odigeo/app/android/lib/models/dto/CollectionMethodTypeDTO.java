package com.odigeo.app.android.lib.models.dto;

@Deprecated public enum CollectionMethodTypeDTO {

  CREDITCARD("CREDITCARD"), ELV("ELV"), PAYPAL("PAYPAL"), SECURE_3_D("SECURE3D"), BANKTRANSFER(
      "BANKTRANSFER"), INSTALLMENT("INSTALLMENT"), SOFORT("SOFORT"), GIROPAY("GIROPAY"), PREPAY(
      "PREPAY"), TRUSTLY("TRUSTLY"), KLARNA("KLARNA");

  private final String value;

  CollectionMethodTypeDTO(String v) {
    value = v;
  }

  public static CollectionMethodTypeDTO fromValue(String v) {
    for (CollectionMethodTypeDTO c : CollectionMethodTypeDTO.values()) {
      if (c.value.equals(v)) {
        return c;
      }
    }
    throw new IllegalArgumentException(v);
  }

  public String value() {
    return value;
  }

}
