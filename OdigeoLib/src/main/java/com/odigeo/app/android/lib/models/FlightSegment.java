package com.odigeo.app.android.lib.models;

import com.odigeo.data.entity.geo.City;
import java.io.Serializable;
import java.util.List;

public class FlightSegment implements Serializable {

  private City departureCity;
  private City arrivalCity;
  private long date;
  private long arrivalDate;
  private List<Section> sections;

  public City getDepartureCity() {
    return departureCity;
  }

  public void setDepartureCity(City departureCity) {
    this.departureCity = departureCity;
  }

  public City getArrivalCity() {
    return arrivalCity;
  }

  public void setArrivalCity(City arrivalCity) {
    this.arrivalCity = arrivalCity;
  }

  public long getDate() {
    return date;
  }

  public void setDate(long date) {
    this.date = date;
  }

  public long getArrivalDate() {
    return arrivalDate;
  }

  public void setArrivalDate(long arrivalDate) {
    this.arrivalDate = arrivalDate;
  }

  public List<Section> getSections() {
    return sections;
  }

  public void setSections(List<Section> sections) {
    this.sections = sections;
  }

  public Section getFirstSection() {
    return sections.get(0);
  }

  public Section getLastSection() {
    return sections.get(sections.size() - 1);
  }
}
