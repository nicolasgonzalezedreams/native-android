package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.ui.widgets.base.TwoTextWithImage;
import com.odigeo.data.entity.booking.Country;

/**
 * Created by Irving Lóp on 13/11/2014.
 */
public class CountryButtonWithImage extends TwoTextWithImage {
  public CountryButtonWithImage(Context context) {
    super(context);
  }

  public CountryButtonWithImage(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public final void setCountry(Country country) {
    super.setText(country.getName());

    String countryCode = country.getCountryCode().split(":")[0];

    String url = String.format("%s%s@2x.png",
        Configuration.getInstance().getImagesSources().getUrlCountries(), countryCode);

    super.setUrlImage(url);
  }

  @Override protected final int getIdSecondTextView() {
    return R.id.text_countrybutton_header;
  }

  @Override protected final int getLayout() {
    return R.layout.layout_country_button_with_image;
  }

  @Override protected final int getIdTextView() {
    return R.id.text_countrybutton_name;
  }

  @Override protected final int getIdImageView() {
    return R.id.image_countrybutton_flag;
  }
}
