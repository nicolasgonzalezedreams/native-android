package com.odigeo.app.android.navigator;

import android.os.Bundle;
import android.support.annotation.Nullable;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.view.LocalizablesUpdaterView;
import com.odigeo.presenter.contracts.navigators.LocalizablesUpdaterNavigatorInterface;

public class LocalizablesUpdaterNavigator extends BaseNavigator
    implements LocalizablesUpdaterNavigatorInterface {

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setTitle(R.string.onecms_options);
    setContentView(R.layout.activity_single_view);
    replaceFragment(R.id.fl_container, LocalizablesUpdaterView.newInstance());
  }
}
