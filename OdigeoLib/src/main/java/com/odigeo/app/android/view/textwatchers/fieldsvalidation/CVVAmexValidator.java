package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

public class CVVAmexValidator extends BaseValidator implements FieldValidator {

  private static final String REGEX_CPF = "[0-9]{4}";

  public CVVAmexValidator() {
    super();
  }

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_CPF);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
