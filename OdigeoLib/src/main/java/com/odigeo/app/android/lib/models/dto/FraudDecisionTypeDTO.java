package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for fraudDecisionType.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;simpleType name="fraudDecisionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ACCEPT_NO_RISK"/>
 *     &lt;enumeration value="ACCEPT_LOW_RISK"/>
 *     &lt;enumeration value="STOP"/>
 *     &lt;enumeration value="BANK_TRANSFER"/>
 *     &lt;enumeration value="FAX"/>
 *     &lt;enumeration value="IN_RANGE"/>
 *     &lt;enumeration value="IN_RANGE_WITH_SH"/>
 *     &lt;enumeration value="MANUAL_FRAUD"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
public enum FraudDecisionTypeDTO {

  ACCEPT_NO_RISK, ACCEPT_LOW_RISK, STOP, BANK_TRANSFER, FAX, IN_RANGE, IN_RANGE_WITH_SH, MANUAL_FRAUD;

  public static FraudDecisionTypeDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
