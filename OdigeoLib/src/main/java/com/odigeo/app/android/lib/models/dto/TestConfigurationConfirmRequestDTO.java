package com.odigeo.app.android.lib.models.dto;

public class TestConfigurationConfirmRequestDTO {

  protected BookingErrorRequestTestConfigurationDTO bookingErrorRequest;
  protected FraudDecisionTypeTestConfigurationDTO fraudDecisionType;
  protected CollectionStatusTestConfigurationDTO collectionStatus;
  protected BookingResponseStatusTestConfigurationDTO bookingResponseStatus;

  /**
   * Gets the value of the bookingErrorRequest property.
   *
   * @return possible object is
   * {@link BookingErrorRequestTestConfiguration }
   */
  public BookingErrorRequestTestConfigurationDTO getBookingErrorRequest() {
    return bookingErrorRequest;
  }

  /**
   * Sets the value of the bookingErrorRequest property.
   *
   * @param value allowed object is
   * {@link BookingErrorRequestTestConfiguration }
   */
  public void setBookingErrorRequest(BookingErrorRequestTestConfigurationDTO value) {
    this.bookingErrorRequest = value;
  }

  /**
   * Gets the value of the fraudDecisionType property.
   *
   * @return possible object is
   * {@link FraudDecisionTypeTestConfiguration }
   */
  public FraudDecisionTypeTestConfigurationDTO getFraudDecisionType() {
    return fraudDecisionType;
  }

  /**
   * Sets the value of the fraudDecisionType property.
   *
   * @param value allowed object is
   * {@link FraudDecisionTypeTestConfiguration }
   */
  public void setFraudDecisionType(FraudDecisionTypeTestConfigurationDTO value) {
    this.fraudDecisionType = value;
  }

  /**
   * Gets the value of the collectionStatus property.
   *
   * @return possible object is
   * {@link CollectionStatusTestConfiguration }
   */
  public CollectionStatusTestConfigurationDTO getCollectionStatus() {
    return collectionStatus;
  }

  /**
   * Sets the value of the collectionStatus property.
   *
   * @param value allowed object is
   * {@link CollectionStatusTestConfiguration }
   */
  public void setCollectionStatus(CollectionStatusTestConfigurationDTO value) {
    this.collectionStatus = value;
  }

  /**
   * Gets the value of the bookingResponseStatus property.
   *
   * @return possible object is
   * {@link BookingResponseStatusTestConfiguration }
   */
  public BookingResponseStatusTestConfigurationDTO getBookingResponseStatus() {
    return bookingResponseStatus;
  }

  /**
   * Sets the value of the bookingResponseStatus property.
   *
   * @param value allowed object is
   * {@link BookingResponseStatusTestConfiguration }
   */
  public void setBookingResponseStatus(BookingResponseStatusTestConfigurationDTO value) {
    this.bookingResponseStatus = value;
  }
}
