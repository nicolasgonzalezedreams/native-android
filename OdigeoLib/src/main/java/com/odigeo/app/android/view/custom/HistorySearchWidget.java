package com.odigeo.app.android.view.custom;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.interfaces.HistorySearchRowListener;
import com.odigeo.app.android.lib.interfaces.IHistorySearchListener;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import java.util.List;

/**
 * Created by Irving Lóp on 13/10/2014.
 */
public class HistorySearchWidget extends LinearLayout implements HistorySearchRowListener {
  private static final int TOP_SEARCHES = 4;

  private IHistorySearchListener listener;

  private List<SearchOptions> historyList;
  private LinearLayout container;
  private boolean isExpanded;
  private TextView mTvSearchHistoryTitle;
  private Button mIbtnClearHistory;
  private LinearLayout footerContainer;

  private TrackerControllerInterface mTracker;

  public HistorySearchWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
    inflateLayout();
    isExpanded = false;
    mTracker = AndroidDependencyInjector.getInstance().provideTrackerController();
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.layout_history_search_widget, this);
    container = (LinearLayout) findViewById(R.id.layout_container_history_search);
    mTvSearchHistoryTitle = (TextView) findViewById(R.id.tvSearchHistoryTitle);
    mIbtnClearHistory = (Button) findViewById(R.id.ibtnClearHistory);
    footerContainer = (LinearLayout) findViewById(R.id.layout_footer_history_search);

    mIbtnClearHistory.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        postGAEventCleanRecentSearches();
        listener.onDeleteHistorySearchW();
      }
    });
    mIbtnClearHistory.setText(LocalizablesFacade.getString(getContext(),
        OneCMSKeys.SEARCHVIEWCONTROLLER_HISTORY_DELETE_ALL));
    mTvSearchHistoryTitle.setText(LocalizablesFacade.getString(getContext(),
        OneCMSKeys.SEARCHVIEWCONTROLLER_LABELHEADER_TXT));
  }

  public final void setListener(IHistorySearchListener listener) {
    this.listener = listener;
  }

  public final void setHistoryList(List<SearchOptions> searchOptionsList) {
    historyList = searchOptionsList;
  }

  public final void clearItems() {
    container.removeAllViewsInLayout();
  }

  public final void drawFirstHistorySearches() {
    if (historyList != null && !historyList.isEmpty()) {
      clearItems();
      drawAllHistoryViews();

      if (historyList.size() > TOP_SEARCHES && !isExpanded) {
        initFooter();
        setContainerBackground(R.drawable.background_card_last_search_with_footer);
      } else if (historyList.size() > TOP_SEARCHES && isExpanded) {
        drawBottomSearches();
      }
    }
  }

  private void setContainerBackground(@DrawableRes int drawableResource) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
      container.setBackground(getBackgroundDrawable(drawableResource));
    } else {
      container.setBackgroundDrawable(getBackgroundDrawable(drawableResource));
    }
  }

  private Drawable getBackgroundDrawable(@DrawableRes int drawableResource) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      return getResources().getDrawable(drawableResource, getContext().getTheme());
    } else {
      return getResources().getDrawable(drawableResource);
    }
  }

  private void initFooter() {
    footerContainer.setVisibility(VISIBLE);
    footerContainer.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        expandFooter();
      }
    });
  }

  private void expandFooter() {
    isExpanded = true;
    drawBottomSearches();
    footerContainer.setVisibility(GONE);
    setContainerBackground(R.drawable.background_card);
    postGAEventRecentSearchViewMore();
  }

  /**
   * Iterates historyList and adds all the views to the container.
   */
  private void drawAllHistoryViews() {
    for (int i = 0; i < TOP_SEARCHES && i < historyList.size(); i++) {
      addViewToContainer(historyList.get(i));
    }
  }

  private void drawBottomSearches() {
    for (int i = TOP_SEARCHES; i < historyList.size(); i++) {
      addViewToContainer(historyList.get(i));
    }
  }

  private void addViewToContainer(SearchOptions searchOptions) {
    switch (searchOptions.getTravelType()) {
      case SIMPLE:
        container.addView(new SimpleHistorySearchRow(getContext(), searchOptions, this));
        break;
      case ROUND:
        container.addView(new RoundHistorySearchRow(getContext(), searchOptions, this));
        break;
      case MULTIDESTINATION:
        container.addView(new MultiHistorySearchRow(getContext(), searchOptions, this));
    }
  }

  @Override public final void onClickRowSearch(SearchOptions searchOptions) {
    if (listener != null) {
      int index = historyList.indexOf(searchOptions);
      postGATrackingEventRedoRecentSearch(++index); //Needs to be 1...N not 0...N-1
      listener.onClickHistorySearchW(searchOptions);
    }
  }

  private void postGATrackingEventRedoRecentSearch(int index) {
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHT_SEARCH,
        TrackerConstants.ACTION_RECENT_SEARCH,
        TrackerConstants.LABEL_PARTIAL_RECENT_SEARCH + index);
  }

  private void postGAEventCleanRecentSearches() {
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHT_SEARCH,
        TrackerConstants.ACTION_RECENT_SEARCH, TrackerConstants.LABEL_RECENT_SEARCH_CLEAN);
  }

  private void postGAEventRecentSearchViewMore() {
    mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHT_SEARCH,
        TrackerConstants.ACTION_RECENT_SEARCH, TrackerConstants.LABEL_RECENT_SEARCH_VIEW_MORE);
  }
}
