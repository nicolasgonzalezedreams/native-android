package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ItineraryProviderBookingDTO {

  protected ItinerariesLegendDTO legend;
  protected List<BookingAdditionalParameterDTO> additionalParameters;
  protected int numAdults;
  protected int numChildren;
  protected int numInfants;
  protected boolean resident;
  protected String pnr;
  protected BookingStatusDTO bookingStatus;
  protected List<Integer> segments;
  protected List<Integer> itineraryProviders;
  protected boolean merchant;
  protected ProviderPaymentMethodTypeDTO paymentMethodType;
  protected String corporateCreditCard;
  protected List<ProviderBookingItineraryDTO> providerBookingItinerary;
  protected BigDecimal totalPrice;
  protected int baggageProviderPrice;

  /**
   * NOTE: In the DTO´s for Import Trip, the class has this values
   */
  protected String corporateCreditCardExDate;
  protected CreditCardDetails creditCardDetails;
  protected Map<Integer, String> residentValidations;

  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }

  /**
   * Gets the value of the legend property.
   *
   * @return possible object is
   * {@link com.odigeo.app.android.lib.models.dto.ItinerariesLegendDTO }
   */
  public ItinerariesLegendDTO getLegend() {
    return legend;
  }

  /**
   * Sets the value of the legend property.
   *
   * @param value allowed object is
   * {@link com.odigeo.app.android.lib.models.dto.ItinerariesLegendDTO }
   */
  public void setLegend(ItinerariesLegendDTO value) {
    this.legend = value;
  }

  /**
   * Gets the value of the additionalParameters property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the additionalParameters property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getAdditionalParameters().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list
   * {@link com.odigeo.app.android.lib.models.dto.BookingAdditionalParameterDTO }
   */
  public List<BookingAdditionalParameterDTO> getAdditionalParameters() {
    if (additionalParameters == null) {
      additionalParameters = new ArrayList<BookingAdditionalParameterDTO>();
    }
    return this.additionalParameters;
  }

  public void setAdditionalParameters(List<BookingAdditionalParameterDTO> additionalParameters) {
    this.additionalParameters = additionalParameters;
  }

  /**
   * Gets the value of the numAdults property.
   */
  public int getNumAdults() {
    return numAdults;
  }

  /**
   * Sets the value of the numAdults property.
   */
  public void setNumAdults(int value) {
    this.numAdults = value;
  }

  /**
   * Gets the value of the numChildren property.
   */
  public int getNumChildren() {
    return numChildren;
  }

  /**
   * Sets the value of the numChildren property.
   */
  public void setNumChildren(int value) {
    this.numChildren = value;
  }

  /**
   * Gets the value of the numInfants property.
   */
  public int getNumInfants() {
    return numInfants;
  }

  /**
   * Sets the value of the numInfants property.
   */
  public void setNumInfants(int value) {
    this.numInfants = value;
  }

  /**
   * Gets the value of the resident property.
   */
  public boolean isResident() {
    return resident;
  }

  /**
   * Sets the value of the resident property.
   */
  public void setResident(boolean value) {
    this.resident = value;
  }

  /**
   * Gets the value of the pnr property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getPnr() {
    return pnr;
  }

  /**
   * Sets the value of the pnr property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setPnr(String value) {
    this.pnr = value;
  }

  /**
   * Gets the value of the bookingStatus property.
   *
   * @return possible object is
   * {@link com.odigeo.app.android.lib.models.dto.BookingStatusDTO }
   */
  public BookingStatusDTO getBookingStatus() {
    return bookingStatus;
  }

  /**
   * Sets the value of the bookingStatus property.
   *
   * @param value allowed object is
   * {@link com.odigeo.app.android.lib.models.dto.BookingStatusDTO }
   */
  public void setBookingStatus(BookingStatusDTO value) {
    this.bookingStatus = value;
  }

  /**
   * Gets the value of the segments property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the segments property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getSegments().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list
   * {@link Integer }
   */
  public List<Integer> getSegments() {
    if (segments == null) {
      segments = new ArrayList<Integer>();
    }
    return this.segments;
  }

  /**
   * Sets the value of the segments' list.
   */
  public void setSegments(List<Integer> segments) {
    this.segments = segments;
  }

  /**
   * Gets the value of the itineraryProviders property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the itineraryProviders property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getItineraryProviders().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list
   * {@link Integer }
   */
  public List<Integer> getItineraryProviders() {
    if (itineraryProviders == null) {
      itineraryProviders = new ArrayList<Integer>();
    }
    return this.itineraryProviders;
  }

  /**
   * Sets the value of the itinerary providers' list.
   */
  public void setItineraryProviders(List<Integer> itineraryProviders) {
    this.itineraryProviders = itineraryProviders;
  }

  /**
   * Gets the value of the merchant property.
   */
  public boolean isMerchant() {
    return merchant;
  }

  /**
   * Sets the value of the merchant property.
   */
  public void setMerchant(boolean value) {
    this.merchant = value;
  }

  /**
   * Gets the value of the paymentMethodType property.
   *
   * @return possible object is
   * {@link com.odigeo.app.android.lib.models.dto.ProviderPaymentMethodTypeDTO }
   */
  public ProviderPaymentMethodTypeDTO getPaymentMethodType() {
    return paymentMethodType;
  }

  /**
   * Sets the value of the paymentMethodType property.
   *
   * @param value allowed object is
   * {@link com.odigeo.app.android.lib.models.dto.ProviderPaymentMethodTypeDTO }
   */
  public void setPaymentMethodType(ProviderPaymentMethodTypeDTO value) {
    this.paymentMethodType = value;
  }

  /**
   * Gets the value of the corporateCreditCard property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCorporateCreditCard() {
    return corporateCreditCard;
  }

  /**
   * Sets the value of the corporateCreditCard property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCorporateCreditCard(String value) {
    this.corporateCreditCard = value;
  }

  /**
   * @return the providerBookingItinerary
   */
  public List<ProviderBookingItineraryDTO> getProviderBookingItinerary() {
    return providerBookingItinerary;
  }

  /**
   * @param providerBookingItinerary the providerBookingItinerary to set
   */
  public void setProviderBookingItinerary(
      List<ProviderBookingItineraryDTO> providerBookingItinerary) {
    this.providerBookingItinerary = providerBookingItinerary;
  }

  public String getCorporateCreditCardExDate() {
    return corporateCreditCardExDate;
  }

  public void setCorporateCreditCardExDate(String corporateCreditCardExDate) {
    this.corporateCreditCardExDate = corporateCreditCardExDate;
  }

  public CreditCardDetails getCreditCardDetails() {
    return creditCardDetails;
  }

  public void setCreditCardDetails(CreditCardDetails creditCardDetails) {
    this.creditCardDetails = creditCardDetails;
  }

  public Map<Integer, String> getResidentValidations() {
    return residentValidations;
  }

  public void setResidentValidations(Map<Integer, String> residentValidations) {
    this.residentValidations = residentValidations;
  }

  public int getBaggageProviderPrice() {
    return baggageProviderPrice;
  }

  public void setBaggageProviderPrice(int baggageProviderPrice) {
    this.baggageProviderPrice = baggageProviderPrice;
  }
}
