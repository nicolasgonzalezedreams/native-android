package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.DistanceUnits;
import com.odigeo.app.android.lib.models.SpinnerItemModel;
import java.util.List;

/**
 * Created by Irving Lóp on 13/11/2014.
 *
 * @deprecated Use {@link OdigeoSpinnerAdapter} instead
 */
@Deprecated public class DistanceUnitSpinnerAdapter
    extends ArrayAdapter<SpinnerItemModel<DistanceUnits>> {
  private final List<SpinnerItemModel<DistanceUnits>> distanceUnits;

  public DistanceUnitSpinnerAdapter(Context context,
      List<SpinnerItemModel<DistanceUnits>> objects) {
    super(context, R.layout.layout_spinner_simple_text, objects);
    this.distanceUnits = objects;
  }

  @Override public final View getView(int position, View convertView, ViewGroup parent) {
    View view =
        LayoutInflater.from(getContext()).inflate(R.layout.layout_spinner_simple_text, null);
    TextView textView = (TextView) view.findViewById(R.id.textView_card_name_and_icon);
    textView.setTypeface(Configuration.getInstance().getFonts().getBold());
    textView.setText(distanceUnits.get(position).getText());
    return view;
  }

  @Override public final View getDropDownView(int position, View convertView, ViewGroup parent) {
    View view =
        LayoutInflater.from(getContext()).inflate(R.layout.layout_spinner_drop_simple_text, null);
    TextView textView = (TextView) view.findViewById(R.id.textView_card_name_and_icon);
    textView.setTypeface(Configuration.getInstance().getFonts().getBold());
    textView.setText(distanceUnits.get(position).getText());
    return view;
  }

  @Override public final int getCount() {
    return distanceUnits.size();
  }

  @Override public final SpinnerItemModel<DistanceUnits> getItem(int position) {
    return distanceUnits.get(position);
  }

  @Override public final long getItemId(int position) {
    return position;
  }
}
