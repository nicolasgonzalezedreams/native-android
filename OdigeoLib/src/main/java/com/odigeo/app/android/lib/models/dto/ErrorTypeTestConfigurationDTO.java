package com.odigeo.app.android.lib.models.dto;

public enum ErrorTypeTestConfigurationDTO {

  UNKNOWN, NO_ERROR, CONNECTION_TIMEOUT, CARD_DENIED;

  public static ErrorTypeTestConfigurationDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
