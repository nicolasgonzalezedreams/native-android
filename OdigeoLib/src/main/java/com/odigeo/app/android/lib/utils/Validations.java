package com.odigeo.app.android.lib.utils;

import android.text.TextUtils;
import android.util.Log;
import com.odigeo.app.android.lib.consts.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by ManuelOrtiz on 10/10/2014.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.2
 * @since 28/01/15
 */
@Deprecated public final class Validations {
  public static final int VALIDATION_NONE = 0;
  public static final int VALIDATION_NAME = 1;
  public static final int VALIDATION_ADDRESS = 2;
  public static final int VALIDATION_MAIL = 3;
  public static final int VALIDATION_BIRTH_DATE = 4;
  public static final int VALIDATION_EXPIRATION_DATE = 5;
  public static final int VALIDATION_EXPIRATION_MONTH_YEAR = 6;
  public static final int VALIDATION_CODE = 7;
  public static final int VALIDATION_TYPE = 8;
  public static final int VALIDATION_NIF = 9;
  public static final int VALIDATION_NIE = 10;
  public static final int VALIDATION_PASSPORT = 11;
  public static final int VALIDATION_NATIONAL_ID_CARD = 12;
  public static final int VALIDATION_CIF = 13;
  public static final int VALIDATION_POSTAL_CODE = 14;
  public static final int VALIDATION_NUMBER = 15;
  public static final int VALIDATION_PHONE_NUMBER = 16;
  public static final int VALIDATION_CC_TYPE = 17;
  public static final int VALIDATION_CC_NUMBER = 18;
  public static final int VALIDATION_CC_PLACEHOLDER = 19;
  public static final int VALIDATION_CCV = 20;
  public static final int VALIDATION_CPF = 21;
  public static final int VALIDATION_BOOKING_ID = 23;
  public static final int MIN_CHARACTERS_EMAIL = 6;
  public static final int MIN_PASSWORD_LENGTH = 7;
  public static final String CHARACTERS_NOT_ALLOWED = "~!@#$%^&*+=`|(){}[]:;\"<>?";
  public static final String PASSWORD_EXPRESSION = "^(?=.*[a-z]*)(?=.*[A-Z]*)(?=.*\\d).+$";
  private static final int MAX_CHARACTERS = 30;
  private static final int MAX_CHARACTERS_PASSPORT = 15;
  private static final int MAX_CHARACTERS_BOOKING_ID = 12;
  private static final int MIN_CHARACTERS_BOOKING_ID = 3;
  private static final int MAX_CHARACTERS_ZIP_CODE = 10;
  private static final int MAX_CHARACTERS_EMAIL = 64;
  private static final int MAX_CHARACTERS_ID = 9;
  private static final int ID_LETTER = 8;
  private static final int MIN_CHARACTERS_NIE_DOCUMENT = 8;
  private static final int MAX_CHARACTERS_NIE_DOCUMENT = 9;
  private static final int CIF_VALIDATION_DIGIT = 10;
  private static final int MAX_CHARACTERS_CIF = 9;
  private static final int LAST_VALUE_CIF = 8;
  private static final int CCV_LENGTH = 3;
  private static final int CCV_LENGTH_AMERICAN_EXPRESS = 4;
  private static final int CPF_LENGTH = 11;
  private static final int MAX_CHARACTERS_CARD_NUMBER = 19;
  private static final int MIN_CHARACTERS_CARD_NUMBER = 12;
  private static final int MASTERCARD_CARD_TYPE = 16;
  private static final int VISA_CARD_TYPE = 13;
  private static final int AMERICAN_EXPRESS_CARD_TYPE = 15;
  private static final int DINERS_CARD_TYPE = 14;
  private static final int MA_CARD_TYPE = 16;
  private static final int CREDIT_CARD_NUMBER_DIVIDER = 10;
  private static final String REGEX = "[%s]+";
  private static final String NAME_ALLOWED_CHARS =
      "A-Za-zÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜäëïöüşŞıçÇ'ŒœßØøÅåÆæÞþÐ ";
  private static final String ADDRESS_ALLOWED_CHARS = NAME_ALLOWED_CHARS + "0-9.,_";
  private static final String EMAIL_ALLOWED_CHARS =
      "[A-Z0-9a-z_%+-]+(\\.[A-Z0-9a-z_%+-]+)*@[A-Za-z0-9]+([\\-]*[A-Za-z0-9])*(\\.[A-Za-z0-9]+([\\-]*[A-Za-z0-9])*)*\\.[A-Za-z]{2,}";
  private static final String PHONE_ALLOWED_CHARS = "[0-9]{7,15}";

  /**
   * To prevent the instantiation.
   */
  private Validations() {
    // Nothing
  }

  public static boolean validateDateTypes(int validationType, Date value) {
    boolean isValid = false;

    switch (validationType) {
      case VALIDATION_BIRTH_DATE:
        isValid = validateDate(value);
        break;
      case VALIDATION_EXPIRATION_DATE:
        isValid = validateDate(value);
        break;
      case VALIDATION_EXPIRATION_MONTH_YEAR:
        Calendar calendarDate = Calendar.getInstance();
        calendarDate.setTime(value);

        isValid = validateMonth(String.valueOf(calendarDate.get(Calendar.MONTH) + 1),
            String.valueOf(calendarDate.get(Calendar.YEAR)));
        break;
      default:
    }
    return isValid;
  }

  /**
   * Perform a validation of a input value
   *
   * @param validationType Kind of validation to be performed
   * @param value Value to be validated
   * @return If the validation its ok.
   */
  public static boolean validate(int validationType, String value) {
    return validatePersonValue(validationType, value)
        || validateCodesValue(validationType, value)
        || validateOthersValue(validationType, value);
  }

  /**
   * Perform validations for others value
   *
   * @param validationType Kind of validation to be performed
   * @param value Value to be validated
   * @return If the validation its ok.
   */
  public static boolean validateOthersValue(int validationType, String value) {
    boolean isValid = false;
    switch (validationType) {
      case VALIDATION_NONE:
        isValid = true;
        break;
      case VALIDATION_CC_TYPE:
        isValid = false;
        break;
      case VALIDATION_CC_NUMBER:
        isValid = validateFormatCreditCardNumber(value);
        break;
      case VALIDATION_CC_PLACEHOLDER:
        isValid = validateCreditCardHolder(value);
        break;
      case VALIDATION_CPF:
        isValid = validateCPF(value);
        break;
      default:
    }
    return isValid;
  }

  /**
   * Perform validations for some Codes value
   *
   * @param validationType Kind of validation to be performed
   * @param value Value to be validated
   * @return If the validation its ok.
   */
  public static boolean validateCodesValue(int validationType, String value) {
    boolean isValid = false;
    switch (validationType) {
      case VALIDATION_CODE:
        isValid = validateString(value);
        break;
      case VALIDATION_TYPE:
        isValid = validateString(value);
        break;
      case VALIDATION_NIF:
        isValid = validateID(value);
        break;
      case VALIDATION_NIE:
        isValid = validateNIE(value);
        break;
      case VALIDATION_CIF:
        isValid = validateCIF(value);
        break;
      case VALIDATION_BOOKING_ID:
        isValid = validateBookingId(value);
      default:
    }
    return isValid;
  }

  /**
   * Perform validations for some Person value
   *
   * @param validationType Kind of validation to be performed
   * @param value Value to be validated
   * @return If the validation its ok.
   */
  public static boolean validatePersonValue(int validationType, String value) {
    boolean isValid = false;
    switch (validationType) {
      case VALIDATION_NAME:
        isValid = validateName(value, 2, MAX_CHARACTERS);
        break;
      case VALIDATION_ADDRESS:
        isValid = validateAddress(value, 2, MAX_CHARACTERS);
        break;
      case VALIDATION_MAIL:
        isValid = validEmail(value);
        break;
      case VALIDATION_PASSPORT:
        isValid = validatePassport(value);
        break;
      case VALIDATION_NATIONAL_ID_CARD:
        isValid = validateString(value);
        break;
      case VALIDATION_POSTAL_CODE:
        isValid = validateAddress(value, 1, MAX_CHARACTERS_ZIP_CODE);
        break;
      case VALIDATION_NUMBER:
        isValid = validateNumberFields(value);
        break;
      case VALIDATION_PHONE_NUMBER:
        isValid = validatePhoneNumber(value);
        break;
      default:
    }
    return isValid;
  }

  public static boolean validateString(String value) {
    return !TextUtils.isEmpty(value);
  }

  public static boolean validateName(String name, int minCharacters, int maxCharacters) {
    return validateLengthField(name, minCharacters, maxCharacters) && name != null && name.matches(
        String.format(REGEX, NAME_ALLOWED_CHARS));
  }

  public static boolean validateAddress(String address, int minCharacters, int maxCharacters) {
    return validateLengthField(address, minCharacters, maxCharacters)
        && address != null
        && address.matches(String.format(REGEX, ADDRESS_ALLOWED_CHARS));
  }

  public static boolean validatePhoneNumber(String phoneNumber) {
    return !(phoneNumber == null || phoneNumber.trim().equals("")) && phoneNumber.matches(
        PHONE_ALLOWED_CHARS);
  }

  public static boolean validateLengthField(String value, int minCharacters, int maxCharacters) {
    if (value == null) {
      return minCharacters == 0;
    }

    return value.length() >= minCharacters && value.length() <= maxCharacters;
  }

  public static boolean validateCharactersField(String value, String charactersNotAllowed) {
    if (value == null || charactersNotAllowed == null) {
      return true;
    }

    for (int i = 0; i < value.length(); i++) {
      if (charactersNotAllowed.toUpperCase(Locale.getDefault())
          .contains(String.valueOf(value.charAt(i)).toUpperCase())) {
        return false;
      }
    }

    return true;
  }

  public static boolean validateTextFields(String value, int minCharacters, int maxCharacters) {
    if (validateLengthField(value, minCharacters, maxCharacters) && (value == null
        || validateCharactersField(value, "~!@#$%^&*_+=`|(){}[]:;\"<>,.?0123456789"))) {
      return true;
    }

    return false;
  }

  /**
   * This method validates that the string contains only letters and numbers,
   * addition to complying with a length
   *
   * @param value string to analyze
   * @param minCharacters length minimum
   * @param maxCharacters length maximum
   * @return True if value has the correct format
   */
  public static boolean validateTextAndNumberFields(String value, int minCharacters,
      int maxCharacters) {
    return validateString(value)
        && validateLengthField(value, minCharacters, maxCharacters)
        && validateCharactersField(value, "~!@#$%^&*_-+=`|(){}[]:;\"<>,.?/");
  }

  public static boolean validatePassport(String value) {
    if (validateLengthField(value, 1, MAX_CHARACTERS_PASSPORT)) {
      return value.toUpperCase(Locale.getDefault()).matches("[A-Z0-9]+");
    }

    return false;
  }

  public static boolean validateNumberFields(String value) {
    if (value == null || value.trim().equals("")) {
      return false;
    }

    if (value.matches("[0-9]+")) {
      return true;
    }

    return false;
  }

  /**
   * Verify that an Booking Id has the correct format
   *
   * @param bookingId Booking Id to analyse
   * @return TRUE if Booking Id has the correct format
   */
  public static boolean validateBookingId(String bookingId) {
    return validateTextAndNumberFields(bookingId, MIN_CHARACTERS_BOOKING_ID,
        MAX_CHARACTERS_BOOKING_ID);
  }

  public static boolean validEmail(String value) {
    if (validateLengthField(value, MIN_CHARACTERS_EMAIL, MAX_CHARACTERS_EMAIL) && (value.matches(
        EMAIL_ALLOWED_CHARS))) {
      return true;
    }

    return false;
  }

  public static boolean validateMonth(String month, String year) {
    if (!validateNumberFields(month) || !validateNumberFields(year)) {
      return false;
    }

    Calendar thisMonthCalendar = Calendar.getInstance();
    thisMonthCalendar.set(Calendar.DATE, 1);

    //Convert the month and year parameter, to a Calendar object,
    // setting to the first day of the month, and without time
    int noMonth = Integer.parseInt(month);
    int noYear = Integer.parseInt(year);

    Calendar dateCalendar = Calendar.getInstance();
    dateCalendar.set(Calendar.MONTH, noMonth - 1);
    dateCalendar.set(Calendar.YEAR, noYear);
    thisMonthCalendar.set(Calendar.DATE, 1);

    //Return true if the date in the parameters is greater or equals than the first day of
    // the current month
    return OdigeoDateUtils.compareDatesWithoutTime(dateCalendar.getTime(),
        thisMonthCalendar.getTime()) >= 0;
  }

  public static boolean validateDate(Date date) {

    return date != null;
  }

  public static boolean validateID(String stringNumber) {
    StringBuffer number = new StringBuffer();
    String defaultValue = "0";
    number.append(stringNumber);
    boolean isValid = false;
    if (number == null || number.length() < 2) {
      return false;
    }
    while (number.length() < MAX_CHARACTERS_ID) {
            /* For Android its recommended use String and not StringBuffer, because Android perform
             * a optimization in build time.
             */
      number.append(defaultValue);
      number.append(number);
    }

    String idNumber = number.substring(0, ID_LETTER);
    String idLetter = number.substring(ID_LETTER);
    idLetter = idLetter.toUpperCase(LocaleUtils.getCurrentLocale());
    if (!idNumber.matches("[0-9]+")) {
      return false;
    }
    try {
      String chars = "TRWAGMYFPDXBNJZSQVHLCKE";
      int mod = Integer.parseInt(idNumber) % chars.length();

      String com = String.valueOf(chars.charAt(mod));
      isValid = idLetter.equals(com);
    } catch (Exception e) {
      Log.e(Constants.TAG_LOG, e.getMessage(), e);
    }
    return isValid;
  }

  public static boolean validateNIE(String stringNieDocument) {
    StringBuffer nieDocument = new StringBuffer();
    String defaultValue = "0";
    nieDocument.append(stringNieDocument);
    boolean response = false;
    if (nieDocument == null
        || nieDocument.length() < MIN_CHARACTERS_NIE_DOCUMENT
        || nieDocument.length() > MAX_CHARACTERS_NIE_DOCUMENT) {
      return false;
    }
    while (nieDocument.length() <= MAX_CHARACTERS_NIE_DOCUMENT) {
            /* For Android its recommended use String and not StringBuffer, because Android perform
             * a optimization in build time.
             */
      nieDocument.append(defaultValue);
      nieDocument.append(nieDocument);
    }

    //Get the idLetter
    String idLetter = nieDocument.substring(nieDocument.length() - 1);
    String xyz = String.valueOf(nieDocument.charAt(0)).toUpperCase();

    if ("0".equals(xyz)) {
      xyz = String.valueOf(nieDocument.charAt(1)).toUpperCase();
    }

    //Get idNumber
    String idNumber = nieDocument.substring(0, MAX_CHARACTERS_NIE_DOCUMENT);

    String chars = "TRWAGMYFPDXBNJZSQVHLCKE";

    if ("XYZ".contains(xyz)) {
      idNumber = idNumber.replaceAll("X", "0");
      idNumber = idNumber.replaceAll("Y", "1");
      idNumber = idNumber.replaceAll("Z", "2");

      try {
        int mod = Integer.parseInt(idNumber) % chars.length();
        String com = String.valueOf(chars.charAt(mod)).toUpperCase();
        response = idLetter.equals(com);
      } catch (Exception e) {
        Log.e(Constants.TAG_LOG, e.getMessage(), e);
      }
    }

    return response;
  }

  public static boolean validateCIF(String value) {
    if (checkCIF(value)) {
      int finalDigit = getFinalDigit(value);
      return ((CIF_VALIDATION_DIGIT - finalDigit) == 1);
    } else {
      return false;
    }
  }

  /**
   * Perform all the validation of CIF
   *
   * @param value Value tobe checked
   * @return If its ok
   */
  private static boolean checkCIF(String value) {
    if (TextUtils.isEmpty(value)) {
      return false;
    } else if (value.length() < MAX_CHARACTERS_CIF) {
      return false;
    }
    return checkFirstAndLastValue(value);
  }

  /**
   * Check if the first ans last value its ok in the CIF
   *
   * @param value Value tobe checked
   * @return If its ok
   */
  private static boolean checkFirstAndLastValue(String value) {
    List<String> lastValueReferencesLetter = Arrays.asList("K", "Q", "S");
    List<String> lastValueReferencesNumber = Arrays.asList("A", "B", "E", "H");
    List<String> characters =
        new ArrayList(Arrays.asList(value.toUpperCase(Locale.getDefault()).split("(?<!^)")));
    List<String> references =
        Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q",
            "R", "S", "V", "W");
    String firstValue = characters.get(0);
    String lastValue = characters.get(LAST_VALUE_CIF);
    boolean containsMustBeLetter = lastValueReferencesLetter.contains(firstValue);
    boolean containsMustBeNumber = lastValueReferencesNumber.contains(firstValue);
    if (containsMustBeLetter || containsMustBeNumber) {
      if (containsMustBeLetter && !isLetter(lastValue)) {
        return false;
      }
      if (containsMustBeNumber && !isDigit(lastValue)) {
        return false;
      }
    }
    return isLetter(firstValue) && references.contains(firstValue);
  }

  /**
   * Perform the operations necessary to get the finalDigit
   *
   * @param value Input value with what by performed the operations
   * @return The final digit resulting of all the operations
   */
  private static int getFinalDigit(String value) {
    char[] subStringToValidate = value.substring(1, LAST_VALUE_CIF).toCharArray();
    int pairSum = 0;
    int oddSum = 0;
    for (int i = 0; i < subStringToValidate.length; i++) {
      boolean isPairPos = ((i + 1) % 2) == 0;
      if (isPairPos) {
        pairSum += Character.getNumericValue(subStringToValidate[i]);
      }
      if (!isPairPos) {
        char[] multi =
            String.valueOf((Character.getNumericValue(subStringToValidate[i]) * 2)).toCharArray();
        int interSum = 0;
        for (char valueChar : multi) {
          interSum += Character.getNumericValue(valueChar);
        }
        oddSum += interSum;
      }
    }
    char[] finalSum = String.valueOf((pairSum + oddSum)).toCharArray();
    return Character.getNumericValue(finalSum[finalSum.length - 1]);
  }

  public static boolean isLetter(String value) {
    return value.matches("[a-zA-Z]+");
  }

  public static boolean isDigit(String value) {
    char c = value.charAt(0);
    return (c >= '0' && c <= '9');
  }

  public static boolean validateCCV(String number, String creditCardType) {
    if (number == null || !number.matches("[0-9]+")) {
      return false;
    }

    int len = CCV_LENGTH;
    if (Constants.CreditCard.TYPE_AMERICAN_EXPRESS.equals(creditCardType)) {
      len = CCV_LENGTH_AMERICAN_EXPRESS;
    }

    return number.length() == len;
  }

  public static boolean validateCreditCardHolder(String holder) {
    return !(holder == null || TextUtils.isEmpty(holder.trim())) && holder.trim()
        .matches("[0-9A-Z\\s-]+");
  }

  public static boolean validateCPF(String number) {

    if (validateLengthField(number, CPF_LENGTH, CPF_LENGTH) && (number.matches(
        "[0-9]+(\\.[0-9]+)?"))) {
      return true;
    }

    return false;
  }

  public static boolean validateCreditCardOfType(String stringCardType, String stringCardNumber) {

    if (TextUtils.isEmpty(stringCardType) || TextUtils.isEmpty(stringCardNumber)) {
      return false;
    }

    String cardType = stringCardType;
    String cardNumber = stringCardNumber.replaceAll("\\s", "");

    if (TextUtils.isEmpty(cardNumber) || TextUtils.isEmpty(cardType)) {
      return false;
    }
    if (cardNumber.length() < MIN_CHARACTERS_CARD_NUMBER
        || cardNumber.length() > MAX_CHARACTERS_CARD_NUMBER) {
      return false;
    }
    if (cardType.contains("-")) {
      cardType = cardType.substring(0, cardType.indexOf('-'));
    }
    return checkCardInfo(cardNumber, cardType);
  }

  /**
   * Check if the information of the credit card its ok
   *
   * @param cardNumber Number of the credit card
   * @param cardType Type of card
   * @return If its valid
   */
  private static boolean checkCardInfo(String cardNumber, String cardType) {
    return "4B".equals(cardType) || validateRules(cardType, cardNumber);
  }

  /**
   * Generate and check the rules for a credit card depending of this type
   *
   * @param cardType Type of card
   * @param cardNumber Number of the credit card
   * @return If pass the rules
   */
  private static boolean validateRules(String cardType, String cardNumber) {
    String[] rules = new String[] {};
    List<Integer> lengths = new ArrayList<Integer>();
    List<String> mastercard =
        Arrays.asList("CM", "CA", "RM", "MC", "MD", "M6", "M4", "MP", "EC", "MI");
    List<String> visa = Arrays.asList("E1", "VI", "RV", "VV", "VB", "3V", "EA", "VD", "DL", "VE");
    //If the credit card format will be checked using a regex this will have a value
    String validationRegex = null;
    if (mastercard.contains(cardType)) {
      lengths = Arrays.asList(MASTERCARD_CARD_TYPE);
      rules = new String[] { "51", "52", "53", "54", "55" };
    } else if (visa.contains(cardType)) {
      lengths = Arrays.asList(VISA_CARD_TYPE, MASTERCARD_CARD_TYPE);
      rules = new String[] { "4" };
    } else if (Constants.CreditCard.TYPE_AMERICAN_EXPRESS.equals(cardType)) {
      lengths = Arrays.asList(AMERICAN_EXPRESS_CARD_TYPE);
      rules = new String[] { "34", "37" };
    } else if (Constants.CreditCard.TYPE_DINERS_CLUB.equals(cardType)) {
      lengths = Arrays.asList(DINERS_CARD_TYPE);
      rules = new String[] { "30", "36", "38" };
    } else if ("MA".equals(cardType)) {
      lengths = Arrays.asList(MA_CARD_TYPE);
      rules = new String[] { "50", "51", "52", "53", "54", "55" };
    } else if (Constants.CreditCard.TYPE_JCB.equals(cardType)) {
      //Set the regex to validate the card
      validationRegex = Constants.CreditCard.VALIDATION_REGEX_JCB;
    }

    return validFormat(validationRegex, cardNumber, lengths, rules)
        && validateFormatCreditCardNumber(cardNumber);
  }

  private static boolean validFormat(String validationRegex, String cardNumber,
      List<Integer> lengths, String[] rules) {
    Boolean validFormat;
    //Check the credit card using either a regex or checking length
    if (validationRegex == null) {
      validFormat = checkLengths(cardNumber, lengths, rules);
    } else {
      validFormat = cardNumber.matches(validationRegex);
    }
    return validFormat;
  }

  /**
   * Check if the length its ok for the type of credit card
   *
   * @param cardNumber Number of the credit card
   * @param lengths Lengths supported fot the type of the credit card
   * @param rules Rules to be check
   * @return If pass the rules
   */
  private static boolean checkLengths(String cardNumber, List<Integer> lengths, String[] rules) {
    //The cardNumber has one of the lengths allowed, and the cardNumber starts with one of
    // the rules
    if (lengths.contains(cardNumber.length())) {
      for (String headDigits : rules) {
        if (cardNumber.startsWith(headDigits)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Checks credit card number using Luhn's Algorithm ()
   *
   * @param stringCardNumber Credit card number to check
   * @return TRUE if the credit card is valid according Luhn Algorithm
   * @see <a href="http://en.wikipedia.org/wiki/Luhn_algorithm">Luhn Algorithm at Wikipedia</a>
   */
  public static boolean validateFormatCreditCardNumber(String stringCardNumber) {
    String cardNumber = stringCardNumber.replaceAll("\\s", "");
    if (cardNumber == null) {
      return false;
    }
    int lastIndex = cardNumber.length() - 1;
    int luhn = 0;
    boolean isValid = false;
    try {
      for (int i = 0; i < cardNumber.length(); i++) {
        int doubled = Integer.parseInt(String.valueOf(cardNumber.charAt(lastIndex - i)));
        if (i % 2 == 1) {
          doubled *= 2;
        }
        String strDoubled = String.valueOf(doubled);
        if (strDoubled.length() > 1) {
          luhn = luhn + Integer.parseInt(String.valueOf(strDoubled.charAt(0))) + Integer.parseInt(
              String.valueOf(strDoubled.charAt(1)));
        } else {
          luhn += doubled;
        }
      }
      isValid = luhn % CREDIT_CARD_NUMBER_DIVIDER == 0;
    } catch (Exception e) {
      Log.e(Constants.TAG_LOG, e.getMessage(), e);
    }
    return isValid;
  }

  /**
   * This method check if a date it's valid for a search.
   *
   * @param original Date to be checked.
   * @return If the date it's ok.
   */
  public static boolean checkDateForSearch(Date original) {
    if (original == null || original.getTime() == new Date(0).getTime()) {
      return false;
    } else {
      Date minRange = new Date();
      Date date = new Date(original.getTime() + Constants.DAY_IN_MILLISECONDS);
      Date maxRange = new Date(minRange.getTime() + Constants.YEAR_IN_MILLISECONDS);
      maxRange.setTime(minRange.getTime() + Constants.YEAR_IN_MILLISECONDS);
      return (date.after(minRange) || date.equals(minRange)) && date.before(maxRange);
    }
  }

  /**
   * Validate if the password is correct.
   *
   * @param password Password to be validated.
   * @return If the password entered was correct.
   */
  public static boolean validatePassword(String password) {
    return password.length() >= MIN_PASSWORD_LENGTH && validateCharactersField(password,
        CHARACTERS_NOT_ALLOWED) && password.matches(PASSWORD_EXPRESSION);
  }
}
