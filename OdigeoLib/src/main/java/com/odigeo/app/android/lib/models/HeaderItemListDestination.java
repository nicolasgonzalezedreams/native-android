package com.odigeo.app.android.lib.models;

import com.odigeo.app.android.lib.interfaces.OdigeoInterfaces;

/**
 * Created by Irving Lóp on 02/09/2014.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 2.1.2
 * @since 01/03/2015
 */
public class HeaderItemListDestination implements OdigeoInterfaces.ItemListDestination {
  private int idIconHeader;
  private String titleHeader;

  public HeaderItemListDestination(int idIconHeader, String titleHeader) {
    this.idIconHeader = idIconHeader;
    this.titleHeader = titleHeader;
  }

  public final int getIdIconHeader() {
    return idIconHeader;
  }

  public final void setIdIconHeader(int idIconHeader) {
    this.idIconHeader = idIconHeader;
  }

  public final String getTitleHeader() {
    return titleHeader;
  }

  public final void setTitleHeader(String titleHeader) {
    this.titleHeader = titleHeader;
  }

  @Override public final boolean isSection() {
    return true;
  }
}
