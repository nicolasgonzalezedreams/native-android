package com.odigeo.app.android.lib.interfaces;

import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.fragments.base.TripFragmentBase;
import com.odigeo.app.android.lib.ui.widgets.SearchTripWidget;

/**
 * Created by Irving Lóp on 01/09/2014.
 */
public interface OdigeoInterfaces {

  interface SearchListener {
    void onClickSearch(SearchTripWidget searchTripWidget, boolean isResident);

    void onClickHistorySearch(SearchOptions searchOptions);

    void onDeleteHistorySearchW(TripFragmentBase fragment);
  }

  interface PassengerPickerFragmentListener {
    void onDialogPositiveClick(int numAdults, int numKids, int numBabies);
  }

  interface ItemListDestination {
    boolean isSection();
  }
}
