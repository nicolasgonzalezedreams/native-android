package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.ui.widgets.base.CustomField;

/**
 * Created by manuel on 16/11/14.
 */
public class FrequentFlyerCodesWidget extends CustomField {

  private ButtonForm buttonForm;

  public FrequentFlyerCodesWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
    inflateLayout(context);
  }

  private void inflateLayout(Context context) {

    inflate(context, R.layout.layout_frequent_flyer_codes_px_widget, this);
    buttonForm = (ButtonForm) findViewById(R.id.buttonFrequentCodes);
  }

  public final void setOnClickListener(OnClickListener l) {
    buttonForm.setOnClickListener(l);
  }

  @Override public final boolean validate() {
    return false;
  }

  @Override public final boolean isValid() {
    return false;
  }

  @Override public void clearValidation() {
    //TODO add action
  }
}
