package com.odigeo.app.android.lib.utils.exceptions;

import com.google.gson.Gson;
import com.odigeo.app.android.lib.models.FlightSegment;

public class FlightSegmentException extends Throwable {

  private FlightSegmentException(String json) {
    super(json);
  }

  public static FlightSegmentException newInstance(FlightSegment flightSegment) {
    String json = new Gson().toJson(flightSegment);
    return new FlightSegmentException(json);
  }
}
