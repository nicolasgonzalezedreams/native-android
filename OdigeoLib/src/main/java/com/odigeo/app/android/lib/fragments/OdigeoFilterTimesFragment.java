package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoFiltersActivity;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.models.FilterTimeModel;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.SegmentGroup;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;
import com.odigeo.app.android.lib.ui.widgets.FilterTimeWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.data.entity.TravelType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Irving Lóp on 14/10/2014.
 */
public class OdigeoFilterTimesFragment extends Fragment {

  private static final int MINUTES_BY_HOUR = 60;
  long departureMinTime, departureMaxTime, arrivalMinTime, arrivalMaxTime;
  private OdigeoFiltersActivity activity;
  private OdigeoApp odigeoApp;
  private OdigeoSession odigeoSession;
  private List<FilterTimeWidget> widgets;
  private LinearLayout layoutContainer;
  private List<FlightSegment> flightSegments;
  private TravelType travelType;
  private List<FareItineraryDTO> itineraryResults;

  @Override public void onAttach(Activity activity) {
    super.onAttach(activity);
    this.activity = (OdigeoFiltersActivity) activity;

    // GAnalytics screen tracking - Filter airlines fragment
    BusProvider.getInstance()
        .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_FILTERS_TIME
            //, getActivity().getApplication()
        ));
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    odigeoApp = (OdigeoApp) activity.getApplication();
    odigeoSession = odigeoApp.getOdigeoSession();

    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {

      flightSegments = activity.getSearchOptions().getFlightSegments();
      travelType = activity.getSearchOptions().getTravelType();
      itineraryResults = activity.getItineraryResults();

      widgets = new ArrayList<>();
    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View view = inflater.inflate(R.layout.layout_filter_time_fragment, container, false);
    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {
      layoutContainer =
          (LinearLayout) view.findViewById(R.id.layout_fragment_filter_time_container);
      drawWidgets();
    }
    return view;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {

    super.onViewCreated(view, savedInstanceState);
    if (odigeoSession == null || !odigeoSession.isDataHasBeenLoaded()) {
      Util.sendToHome(activity, odigeoApp);
    }
  }

  public List<FilterTimeModel> getFilterModels() {
    List<FilterTimeModel> timeModelList = new ArrayList<FilterTimeModel>();

    for (int i = 0; i < widgets.size(); i++) {
      FilterTimeWidget timeWidget = widgets.get(i);
      timeWidget.setNumberWidget(i);
      timeModelList.add(timeWidget.getTimeModel());
    }
    return timeModelList;
  }

  public void drawWidgets() {
    if (flightSegments != null && !flightSegments.isEmpty()) {
      int index = 0;
      for (FlightSegment segment : flightSegments) {
        departureMinTime = itineraryResults.get(0)
            .getSegmentGroups()
            .get(index)
            .getSegmentWrappers()
            .get(0)
            .getSectionsObjects()
            .get(0)
            .getDepartureDate();

        departureMaxTime = Long.MIN_VALUE;
        calcDepartureTime(index);

        arrivalMinTime = itineraryResults.get(0)
            .getSegmentGroups()
            .get(index)
            .getSegmentWrappers()
            .get(0)
            .getSectionsObjects()
            .get(0)
            .getArrivalDate();

        arrivalMaxTime = Long.MIN_VALUE;
        calcArrivalTime(index);

        long maxDuration = getMaxDuration(index);

        String title = Util.getTitleVolHeader(getActivity(), travelType, index);

        FilterTimeWidget widget = new FilterTimeWidget(getActivity());

        widgets.add(index, widget);

        layoutContainer.addView(widget);
        ViewUtils.setSeparator(widget, getActivity().getApplicationContext());
        widget.setTitle(title);
        widget.setDateFlight(OdigeoDateUtils.createDate(segment.getDate()));
        widget.setDepartureCity(segment.getDepartureCity());
        widget.setArrivalCity(segment.getArrivalCity());

        widget.setDepartureMinDate(OdigeoDateUtils.createDate(departureMinTime));
        widget.setDepartureMaxDate(OdigeoDateUtils.createDate(departureMaxTime));
        widget.setArrivalMinDate(OdigeoDateUtils.createDate(arrivalMinTime));
        widget.setArrivalMaxDate(OdigeoDateUtils.createDate(arrivalMaxTime));

        widget.setMaxFlightHours((int) maxDuration / MINUTES_BY_HOUR);

        if (activity.getFiltersSelected() != null
            && activity.getFiltersSelected().getTimes() != null
            && !activity.getFiltersSelected().getTimes().isEmpty()) {
          widget.setSelectedTimeModel(activity.getFiltersSelected().getTimes().get(index));
        }

        index++;
      }
    }
  }

  private void calcDepartureTime(int index) {
    for (FareItineraryDTO itineraryResult : itineraryResults) {
      SegmentGroup segmentGroup = itineraryResult.getSegmentGroups().get(index);

      for (SegmentWrapper segmentWrapper : segmentGroup.getSegmentWrappers()) {
        if (segmentWrapper.getSectionsObjects().get(0).getDepartureDate() < departureMinTime) {
          departureMinTime = segmentWrapper.getSectionsObjects().get(0).getDepartureDate();
        }

        if (segmentWrapper.getSectionsObjects().get(0).getDepartureDate() > departureMaxTime) {
          departureMaxTime = segmentWrapper.getSectionsObjects().get(0).getDepartureDate();
        }
      }
    }
  }

  private void calcArrivalTime(int index) {
    for (FareItineraryDTO itineraryResult : itineraryResults) {
      SegmentGroup segmentGroup = itineraryResult.getSegmentGroups().get(index);

      for (SegmentWrapper segmentWrapper : segmentGroup.getSegmentWrappers()) {
        if (segmentWrapper.getSectionsObjects()
            .get(segmentWrapper.getSectionsObjects().size() - 1)
            .getArrivalDate() < arrivalMinTime) {
          arrivalMinTime = segmentWrapper.getSectionsObjects()
              .get(segmentWrapper.getSectionsObjects().size() - 1)
              .getArrivalDate();
        }

        if (segmentWrapper.getSectionsObjects()
            .get(segmentWrapper.getSectionsObjects().size() - 1)
            .getArrivalDate() > arrivalMaxTime) {
          arrivalMaxTime = segmentWrapper.getSectionsObjects()
              .get(segmentWrapper.getSectionsObjects().size() - 1)
              .getArrivalDate();
        }
      }
    }
  }

  private long getMaxDuration(int index) {
    long maxDuration = 0;
    for (FareItineraryDTO itineraryResult : itineraryResults) {
      SegmentGroup segmentGroup = itineraryResult.getSegmentGroups().get(index);
      for (SegmentWrapper segmentWrapper : segmentGroup.getSegmentWrappers()) {
        if (segmentWrapper.getSegment().getDuration() > maxDuration) {
          maxDuration = segmentWrapper.getSegment().getDuration();
        }
      }
    }
    return maxDuration;
  }
}
