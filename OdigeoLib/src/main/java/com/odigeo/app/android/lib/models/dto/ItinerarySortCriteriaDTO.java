package com.odigeo.app.android.lib.models.dto;

@Deprecated public enum ItinerarySortCriteriaDTO {
  MINIMUM_PURCHASABLE_PRICE, MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE, APPARENT_PROVIDER_PRICE, APPARENT_PROVIDER_PRICE_WITH_DISCOUNTS, APPARENT_PROVIDER_PRICE_WITH_SEARCH_FEE;

  public static ItinerarySortCriteriaDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }
}
