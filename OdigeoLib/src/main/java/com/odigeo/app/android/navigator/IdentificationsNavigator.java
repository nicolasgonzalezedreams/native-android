package com.odigeo.app.android.navigator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoCountriesActivity;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.view.IdentificationView;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.presenter.contracts.navigators.IdentificationsNavigatorInterface;

/**
 * Created by carlos.navarrete on 21/09/15.
 */
public class IdentificationsNavigator extends BaseNavigator
    implements IdentificationsNavigatorInterface {

  public static final String TAG_IDENTIFICATIONS = "IDENTIFICATIONS";
  public static final String IDENTIFICATION_TYPE = "identification_type";
  public static final String IDENTIFICATION_ID_NUMBER = "ID_NUMBER";
  public static final String IDENTIFICATION_DATE = "EXPIRY_DATE";
  public static final String IDENTIFICATION_COUNTRY = "COUNTRY";

  private UserIdentification.IdentificationType mIdentificationType;

  private IdentificationView mIdentificationView;
  private String mNumberId;
  private long mExpireDate;
  private String mCountryCode;
  private String mfragTag;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Intent intent = getIntent();

    mIdentificationType =
        (UserIdentification.IdentificationType) intent.getSerializableExtra(IDENTIFICATION_TYPE);
    mNumberId = intent.getStringExtra(IDENTIFICATION_ID_NUMBER);
    mCountryCode = intent.getStringExtra(IDENTIFICATION_COUNTRY);
    mExpireDate = intent.getLongExtra(IDENTIFICATION_DATE, 0);
    setContentView(R.layout.activity_single_view_with_toolbar);
    showView();
  }

  protected void showView() {
    replaceFragment(R.id.fl_container, getIdentificationsView());
  }

  private IdentificationView getIdentificationsView() {
    if (mIdentificationView == null) {
      mfragTag = TAG_IDENTIFICATIONS;
      if (mNumberId != null && !mNumberId.isEmpty()) {
        mIdentificationView =
            IdentificationView.newInstance(mIdentificationType, mNumberId, mExpireDate,
                mCountryCode);
      } else {
        mIdentificationView = IdentificationView.newInstance(mIdentificationType, null, 0, null);
      }
    }
    return mIdentificationView;
  }

  @Override public void showBackView() {

    this.onBackPressed();
  }

  @Override public void setValuesActionBar(String title, int idIcon) {
    setNavigationTitle(title);
    setUpToolbarButton(idIcon);
  }

  @Override public void returnIdentifications(String numberIdentification, long dateExpiration,
      Country identificationCountry, UserIdentification.IdentificationType identificationType) {

    Intent data = new Intent();
    data.putExtra(IDENTIFICATION_ID_NUMBER, numberIdentification);
    data.putExtra(IDENTIFICATION_DATE, dateExpiration);
    if (identificationCountry != null) {
      data.putExtra(IDENTIFICATION_COUNTRY, identificationCountry.getCountryCode());
    } else {
      data.putExtra(IDENTIFICATION_COUNTRY, identificationCountry);
    }
    data.putExtra(IDENTIFICATION_TYPE, identificationType);
    setResult(RESULT_OK, data);
    finish();
  }

  @Override public void returnForResult() {

  }

  @Override public void navigateToSelectCountry(String fragmentTag, int constants) {
    OdigeoApp odigeoApp = (OdigeoApp) getApplicationContext();
    Intent intent = null;
    if (constants == Constants.SELECTING_TRAVELLER_NATIONALITY) {
      intent = new Intent(getApplicationContext(), OdigeoCountriesActivity.class);
      intent.putExtra(Constants.EXTRA_COUNTRY_MODE, OdigeoCountriesActivity.SELECTION_MODE_COUNTRY);
    }

    startActivityForResult(intent, constants);
  }

  @Override public final void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == Activity.RESULT_OK) {
      Country countrySelected = (Country) data.getSerializableExtra(Constants.EXTRA_COUNTRY);
      if (countrySelected != null) {
        if (requestCode == Constants.SELECTING_TRAVELLER_NATIONALITY) {
          if (mfragTag.equals(TAG_IDENTIFICATIONS)) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
            if (fragment != null && fragment instanceof IdentificationView) {
              IdentificationView identificationView =
                  (IdentificationView) getSupportFragmentManager().findFragmentById(
                      R.id.fl_container);
              identificationView.setCountryIssueSelected(countrySelected);
            }
          }
        }
      }
    }
  }
}
