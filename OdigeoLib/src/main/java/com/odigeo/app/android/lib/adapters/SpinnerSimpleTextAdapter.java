package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.models.SpinnerItemModel;
import java.util.List;

/**
 * General spinner adapter for enums this will show on
 *
 * @deprecated Use {@link OdigeoSpinnerAdapter} instead
 */
@Deprecated public class SpinnerSimpleTextAdapter<T extends Enum>
    extends ArrayAdapter<SpinnerItemModel<T>> {
  //TODO: Generalize this spinner so this do not work only for Enums
  public SpinnerSimpleTextAdapter(Context context, List<SpinnerItemModel<T>> genders) {
    super(context, R.layout.layout_spinner_simple_text, genders);
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {
    View view =
        LayoutInflater.from(getContext()).inflate(R.layout.layout_spinner_simple_text, null);
    TextView textView = (TextView) view.findViewById(R.id.textView_card_name_and_icon);
    textView.setTypeface(Configuration.getInstance().getFonts().getRegular());

    textView.setText(getItem(position).getText());
    return view;
  }

  @Override public View getDropDownView(int position, View convertView, ViewGroup parent) {
    View view = LayoutInflater.from(getContext())
        .inflate(R.layout.layout_spinner_drop_simple_text, parent, false);
    TextView textView = (TextView) view.findViewById(R.id.textView_card_name_and_icon);
    textView.setTypeface(Configuration.getInstance().getFonts().getRegular());
    textView.setText(getItem(position).getText());
    return view;
  }
}
