package com.odigeo.app.android.lib.modules;

import com.odigeo.app.android.lib.models.SegmentGroup;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;
import com.odigeo.app.android.lib.models.dto.SectionDTO;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arturo.padron on 19/05/2015.
 * <p/>
 * Used to filter module into the app.
 */
public class Filters {

  public static List<FareItineraryDTO> getFilterDepartureAtIndexSegmentGroup(
      String departureIataCode, List<FareItineraryDTO> itineraryResults, int indexSegmentGroup) {
    List<FareItineraryDTO> itineraryResultsFiltered = new ArrayList<FareItineraryDTO>();
    for (FareItineraryDTO itineraryResult : itineraryResults) {
      FareItineraryDTO newItineraryResult = null;

      int counterSegmentsGroup = 0;
      for (SegmentGroup segmentGroup : itineraryResult.getSegmentGroups()) {
        SegmentGroup newSegmentGroup = null;

        for (SegmentWrapper segmentWrapper : segmentGroup.getSegmentWrappers()) {

          if (counterSegmentsGroup != indexSegmentGroup || filterByDepartureAirport(
              departureIataCode, segmentWrapper)) {
            if (newItineraryResult == null) {
              newItineraryResult = getNewItineraryFrom(itineraryResult);
            }

            if (newSegmentGroup == null) {
              newSegmentGroup = getNewSegmentGroupFrom(segmentGroup);
              newSegmentGroup.setItineraryResultParent(newItineraryResult);
              newItineraryResult.getSegmentGroups().add(newSegmentGroup);
            }

            newSegmentGroup.getSegmentWrappers().add(segmentWrapper);
            newItineraryResult.getCarriers().add(segmentWrapper.getCarrier());
          }
        }
        counterSegmentsGroup++;
      }

      if(newItineraryResult != null) newItineraryResult.setMembershipPerks(itineraryResult
          .getMembershipPerks());

      //If the itinerary resulted has the number of segment groups should it has
      if (addResult(newItineraryResult, itineraryResult)) {
        itineraryResultsFiltered.add(newItineraryResult);
      }
    }
    return itineraryResultsFiltered;
  }

  public static List<FareItineraryDTO> getFilterArrivalAtIndexSegmentGroup(String arrivalIataCode,
      List<FareItineraryDTO> itineraryResults, int indexSegmentGroup) {
    List<FareItineraryDTO> itineraryResultsFiltered = new ArrayList<FareItineraryDTO>();
    for (FareItineraryDTO itineraryResult : itineraryResults) {
      FareItineraryDTO newItineraryResult = null;

      int counterSegmentsGroup = 0;
      for (SegmentGroup segmentGroup : itineraryResult.getSegmentGroups()) {
        SegmentGroup newSegmentGroup = null;

        for (SegmentWrapper segmentWrapper : segmentGroup.getSegmentWrappers()) {

          if (counterSegmentsGroup != indexSegmentGroup || filterByArrivalAirport(arrivalIataCode,
              segmentWrapper)) {
            if (newItineraryResult == null) {
              newItineraryResult = getNewItineraryFrom(itineraryResult);
            }

            if (newSegmentGroup == null) {
              newSegmentGroup = getNewSegmentGroupFrom(segmentGroup);
              newSegmentGroup.setItineraryResultParent(newItineraryResult);
              newItineraryResult.getSegmentGroups().add(newSegmentGroup);
            }

            newSegmentGroup.getSegmentWrappers().add(segmentWrapper);
            newItineraryResult.getCarriers().add(segmentWrapper.getCarrier());
          }
        }
        counterSegmentsGroup++;
      }

      if(newItineraryResult != null) newItineraryResult.setMembershipPerks(itineraryResult
          .getMembershipPerks());

      //If the itinerary resulted has the number of segment groups should it has
      if (addResult(newItineraryResult, itineraryResult)) {
        itineraryResultsFiltered.add(newItineraryResult);
      }
    }
    return itineraryResultsFiltered;
  }

  private static boolean filterByDepartureAirport(String departureIataCode,
      SegmentWrapper segmentWrapper) {
    List<SectionDTO> sectionsObjects = segmentWrapper.getSectionsObjects();
    SectionDTO departure = sectionsObjects.get(0);
    return departure.getLocationFrom().getIataCode() != null && departure.getLocationFrom()
        .getIataCode()
        .equals(departureIataCode);
  }

  private static boolean filterByArrivalAirport(String arrivalIataCode,
      SegmentWrapper segmentWrapper) {
    List<SectionDTO> sectionsObjects = segmentWrapper.getSectionsObjects();
    SectionDTO arrival = sectionsObjects.get(sectionsObjects.size() - 1);
    return arrival.getLocationTo().getIataCode() != null && arrival.getLocationTo()
        .getIataCode()
        .equals(arrivalIataCode);
  }

  /**
   * Create a new itinerary since an older list with necessary data of the flights.
   *
   * @param itineraryDTO Contain data about the flights.
   * @return Itinerary of flights.
   */
  private static FareItineraryDTO getNewItineraryFrom(FareItineraryDTO itineraryDTO) {
    FareItineraryDTO newItineraryResult = new FareItineraryDTO();
    newItineraryResult.setCollectionMethodFeesObject(itineraryDTO.getCollectionMethodFeesObject());
    newItineraryResult.setBaggageFees(itineraryDTO.getBaggageFees());
    newItineraryResult.setCollectionMethodFees(itineraryDTO.getCollectionMethodFees());
    newItineraryResult.setItinerary(itineraryDTO.getItinerary());
    newItineraryResult.setKey(itineraryDTO.getKey());
    newItineraryResult.setPrice(itineraryDTO.getPrice());
    newItineraryResult.setResident(itineraryDTO.getResident());
    newItineraryResult.setSegmentGroups(new ArrayList<SegmentGroup>());
    newItineraryResult.setMembershipPerks(itineraryDTO.getMembershipPerks());
    return newItineraryResult;
  }

  /**
   * Create a new segment group since an older list with necessary data of the flights.
   *
   * @param segmentGroup Contain data about the flights.
   * @return A new segment group.
   */

  private static SegmentGroup getNewSegmentGroupFrom(SegmentGroup segmentGroup) {
    SegmentGroup newSegmentGroup = new SegmentGroup();
    newSegmentGroup.setPrice(segmentGroup.getPrice());
    newSegmentGroup.setCarrier(segmentGroup.getCarrier());
    newSegmentGroup.setSegmentWrappers(new ArrayList<SegmentWrapper>());
    newSegmentGroup.setSegmentGroupIndex(segmentGroup.getSegmentGroupIndex());
    newSegmentGroup.setBaggageIncluded(segmentGroup.getBaggageIncluded());

    return newSegmentGroup;
  }

  /**
   * This method adds a result to the filtered list
   *
   * @param newItineraryResult new result to add
   * @param itineraryResult results
   */
  private static boolean addResult(FareItineraryDTO newItineraryResult,
      FareItineraryDTO itineraryResult) {
    if (newItineraryResult != null && newItineraryResult.getSegmentGroups() != null) {
      return (newItineraryResult.getSegmentGroups().size() == itineraryResult.getSegmentGroups()
          .size());
    }
    return false;
  }
}
