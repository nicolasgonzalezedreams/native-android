package com.odigeo.app.android.view.wrappers;

import com.odigeo.data.entity.shoppingCart.Carrier;
import java.io.Serializable;
import java.util.List;

/**
 * This class is used to wrap a list of {@link Carrier} and give us the capacity to pass the list
 * through Activities as a {@link Serializable} Extra.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 25/09/15
 */
public class CarrierWrapper implements Serializable {
  private final List<Carrier> carriers;

  /**
   * Constructor
   *
   * @param carriers The list of {@link Carrier} to be wrap.
   */
  public CarrierWrapper(List<Carrier> carriers) {
    this.carriers = carriers;
  }

  public List<Carrier> getCarriers() {
    return carriers;
  }
}
