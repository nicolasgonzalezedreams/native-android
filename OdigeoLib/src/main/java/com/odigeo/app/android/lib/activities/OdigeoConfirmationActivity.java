package com.odigeo.app.android.lib.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.helper.CrossCarUrlHandler;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.BookingMessages;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.OldMyShoppingCart;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.ShareTripModel;
import com.odigeo.app.android.lib.models.dto.BookingSummaryStatus;
import com.odigeo.app.android.lib.models.dto.SectionDTO;
import com.odigeo.app.android.lib.models.dto.custom.MessageCode;
import com.odigeo.app.android.lib.modules.Ecommerce;
import com.odigeo.app.android.lib.ui.widgets.ConfirmationBuyerInfoWidget;
import com.odigeo.app.android.lib.ui.widgets.ConfirmationHeader;
import com.odigeo.app.android.lib.ui.widgets.InsuranceMandatoryWidget;
import com.odigeo.app.android.lib.ui.widgets.PassengerResidenceAcreditationWidget;
import com.odigeo.app.android.lib.ui.widgets.PassengersResumeWidget;
import com.odigeo.app.android.lib.ui.widgets.SummaryTravelWidget;
import com.odigeo.app.android.lib.ui.widgets.UnfoldingBankTransferInfo;
import com.odigeo.app.android.lib.ui.widgets.VelocityInformationWidget;
import com.odigeo.app.android.lib.ui.widgets.WhatsNextWidget;
import com.odigeo.app.android.lib.ui.widgets.base.TwoTextsDetailsBreakdown;
import com.odigeo.app.android.lib.ui.widgets.crossselling.CrossSellingWidgetDrawer;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.lib.utils.exceptions.BookingDetailException;
import com.odigeo.app.android.lib.utils.exceptions.ShoppingCartCollectionOptionException;
import com.odigeo.app.android.lib.utils.exceptions.ShoppingCartResponseException;
import com.odigeo.app.android.model.BookingInfoViewModel;
import com.odigeo.app.android.utils.ShareUtils;
import com.odigeo.app.android.view.InsuranceWidget;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.custom.ConfirmationGroundTransportationView;
import com.odigeo.app.android.view.custom.GroundTransportationViewBase.GroundTransportationWidgetListener;
import com.odigeo.app.android.view.helpers.CallTelephoneHelper;
import com.odigeo.app.android.view.helpers.PermissionsHelper;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.shoppingCart.BankTransferData;
import com.odigeo.data.entity.shoppingCart.BankTransferResponse;
import com.odigeo.data.entity.shoppingCart.BookingDetail;
import com.odigeo.data.entity.shoppingCart.BookingStatus;
import com.odigeo.data.entity.shoppingCart.Buyer;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.InsuranceShoppingItem;
import com.odigeo.data.entity.shoppingCart.ItineraryProviderBooking;
import com.odigeo.data.entity.shoppingCart.MarketingRevenueDataAdapter;
import com.odigeo.data.entity.shoppingCart.Money;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.data.entity.tracker.Product;
import com.odigeo.data.entity.tracker.Transaction;
import com.odigeo.data.entity.userData.InsertCreditCardRequest;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.dataodigeo.tracker.TrackerController;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackHelper;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackerFlowSession;
import com.odigeo.dataodigeo.tracker.tune.TuneHelper;
import com.odigeo.helper.CrashlyticsUtil;
import com.odigeo.interactors.SavePaymentMethodInteractor;
import com.odigeo.tools.CalendarHelperInterface;
import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.odigeo.app.android.view.constants.OneCMSKeys.ADD_TO_CALENDAR;

public abstract class OdigeoConfirmationActivity extends OdigeoBackgroundAnimatedActivity
    implements View.OnClickListener, GroundTransportationWidgetListener {

  public static final String XSELL_SCREEN_LABEL_NAME = "confpage";

  private static final String FIRST_SEGMENT = "firstSegment";
  private static final String DD_MM_YYYY = "dd/MM/yyyy";
  private static final String HH_MM = "HH:mm";
  private OldMyShoppingCart oldMyShoppingCart;
  private OdigeoApp odigeoApp;
  private ConfirmationHeader confirmationHeader;
  private WhatsNextWidget newWidget;
  private Button addCalenderButton;
  private OdigeoSession odigeoSession;
  private ConfirmationBuyerInfoWidget confirmationBuyerInfoWidget;
  private ConfirmationGroundTransportationView confirmationGroundTransportationView;
  private LinearLayout groundTransportationContainer;
  private Button buttonAction;
  private List<BankTransferData> bankTransferData;
  private BankTransferResponse bankTransferResponse;
  private CardView mCardShareTrip;
  private String mCardNumberObfuscated;
  private SearchOptions mSearchOptions;
  private CreateShoppingCartResponse mCreateShoppingCartResponse;
  private ScrollView mScrollView;
  private RelativeLayout mPhoneCard;
  private SearchTrackHelper mSearchTrackHelper;
  private Booking mBooking;
  private CalendarHelperInterface mCalendarHelper;
  private ShoppingCartCollectionOption mShoppingCartCollectionOption;
  private InsertCreditCardRequest creditCardToSave;
  private SavePaymentMethodInteractor savePaymentMethodInteractor;
  private TrackerControllerInterface trackerController;
  private SessionController sessionController;
  private BookingInfoViewModel bookingInfo;

  private BookingDetail bookingDetail;
  private List<MarketingRevenueDataAdapter> marketingRevenueDataAdapterList;
  private String phoneNumber;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_confirmation);
    odigeoApp = (OdigeoApp) getApplication();
    odigeoSession = odigeoApp.getOdigeoSession();
    trackerController = AndroidDependencyInjector.getInstance().provideTrackerController();
    sessionController = AndroidDependencyInjector.getInstance().provideSessionController();
    sessionController = AndroidDependencyInjector.getInstance().provideSessionController();
    savePaymentMethodInteractor =
        AndroidDependencyInjector.getInstance().provideSavePaymentMethodInteractor();
    creditCardToSave = (InsertCreditCardRequest) getIntent().getSerializableExtra(
        Constants.EXTRA_SAVE_PAYMENT_METHOD);
    mSearchOptions =
        (SearchOptions) getIntent().getSerializableExtra(Constants.EXTRA_SEARCH_OPTIONS);
    mCreateShoppingCartResponse = (CreateShoppingCartResponse) getIntent().getSerializableExtra(
        Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE);
    mShoppingCartCollectionOption = (ShoppingCartCollectionOption) getIntent().getSerializableExtra(
        Constants.EXTRA_COLLECTION_OPTION_SELECTED);
    mBooking = (Booking) getIntent().getSerializableExtra(Constants.EXTRA_BOOKING);
    mSearchTrackHelper = SearchTrackerFlowSession.getInstance().getSearchTrackHelper();
    mCalendarHelper = AndroidDependencyInjector.getInstance().provideCalendarHelper(this);

    bookingDetail =
        (BookingDetail) getIntent().getSerializableExtra(Constants.EXTRA_BOOKING_DETAIL);
    bookingInfo = (BookingInfoViewModel) getIntent().getSerializableExtra(Constants.EXTRA_BOOKING_INFO);
    marketingRevenueDataAdapterList = (List<MarketingRevenueDataAdapter>) getIntent().getExtras()
        .getSerializable(Constants.EXTRA_MARKETING_REVENUE);

    if (chkSession()) {
      init();
      mCardNumberObfuscated = getIntent().getStringExtra(Constants.CREDIT_CARD_NUMBER_OBFUSCATED);
      bankTransferData = (List<BankTransferData>) getIntent().getExtras()
          .getSerializable(Constants.EXTRA_BANK_TRANSFER_DATA);
      bankTransferResponse = (BankTransferResponse) getIntent().getSerializableExtra(
          Constants.EXTRA_BANK_TRANSFER_RESPONSE);

      if (getStatusConfirmation() == BookingStatus.REJECTED) {
        findViewById(R.id.layout_container_confirmation).setVisibility(View.GONE);
        mCardShareTrip.setVisibility(View.GONE);
        confirmationHeader.setStatusConfirmation(getStatusConfirmation());
        buttonAction.setVisibility(View.GONE);
        if (!phoneNumber.isEmpty()) {
          mPhoneCard.setVisibility(View.VISIBLE);
        }
      } else {
        setViews(confirmationBuyerInfoWidget, buttonAction);

        Util.triggerWidgetUpdate(this, odigeoApp.getWidgetClass());

        if (mSearchOptions.getTravelType() != TravelType.MULTIDESTINATION
            && !OdigeoDateUtils.isOneDayTrip(mSearchOptions)) {
          updateCrossSellingInfo();

          new CrossSellingWidgetDrawer(new WeakReference<Activity>(this), oldMyShoppingCart,
              mSearchOptions, XSELL_SCREEN_LABEL_NAME,
              CrossCarUrlHandler.CAMPAIGN_CONFIRMATION_PAGE,
              mCreateShoppingCartResponse.getShoppingCart(),
              AndroidDependencyInjector.getInstance().provideMarketProvider());
          findViewById(R.id.myTripsInformationContainer).setVisibility(View.VISIBLE);
        }

        PreferencesManager.saveTimeMostRecentBooking(getApplicationContext(),
            oldMyShoppingCart.getCreationDate());

        trackTransaction(getMarketingRevenue(), bookingDetail.getPrice(),
            mShoppingCartCollectionOption);
      }
    } else {
      Util.sendToHome(this, odigeoApp);
    }

    trackWithLocalytics();
  }

  @Override public void onResume() {

    super.onResume();
    switch (getStatusConfirmation()) {
      case REJECTED:
        postGAScreenBookingRejected();
        break;
      case CONTRACT:
        drawVelocityInformationWidget();
        postGAScreenBookingConfirmed();

        if (creditCardToSave != null) {
          savePaymentMethodInteractor.savePaymentMethod(creditCardToSave);
        }

        trackSavedPaymentMethod();
        break;
      case PENDING:
        postGAScreenPaymentPending();
        break;
    }
  }

  private void trackSavedPaymentMethod() {
    if (creditCardToSave != null && userHasNoCreditCardsStored()) {
      trackerController.trackStorePaymentMethodIsUsed();
    } else if (userDoesNotWantToStoreCreditCard()) {
      trackerController.trackStorePaymentMethodNotUsed();
    }
  }

  private boolean userHasNoCreditCardsStored() {
    return sessionController.getCreditCards() == null || sessionController.getCreditCards()
        .isEmpty();
  }

  private boolean userDoesNotWantToStoreCreditCard() {
    return creditCardToSave == null
        && sessionController.getCredentials() != null
        && userHasNoCreditCardsStored();
  }

  @Override public final String getActivityTitle() {
    return LocalizablesFacade.getString(this, "confirmationviewcontroller_title").toString();
  }

  private void trackWithLocalytics() {
    //Get value per passenger
    String[] airlines = new String[3];
    String[] airportsDeparture = new String[3];
    String[] airportsArrival = new String[3];
    long[] date = new long[3];

    int pricePerPassenger =
        (int) mCreateShoppingCartResponse.getShoppingCart().getTotalPrice().longValue()
            / mSearchOptions.getTotalPassengers();

    long returnDate = bookingInfo.getSegmentsWrappers()
        .get(bookingInfo.getSegmentsWrappers().size() - 1)
        .getSectionsObjects()
        .get(0)
        .getDepartureDate();

    //Set segments data
    for (int i = 0; i < 3; i++) {
      if (i < bookingInfo.getSegmentsWrappers().size()) {
        airlines[i] = bookingInfo.getSegmentsWrappers().get(i).getCarrier().getName();
        airportsArrival[i] = bookingInfo.getSegmentsWrappers()
            .get(i)
            .getSectionsObjects()
            .get(0)
            .getLocationTo()
            .getIataCode();
        airportsDeparture[i] = bookingInfo.getSegmentsWrappers()
            .get(i)
            .getSectionsObjects()
            .get(0)
            .getLocationFrom()
            .getIataCode();
        date[i] = bookingInfo.getSegmentsWrappers()
            .get(i)
            .getSectionsObjects()
            .get(0)
            .getDepartureDate();
      } else {
        airlines[i] = TrackerConstants.NA;
        airportsArrival[i] = TrackerConstants.NA;
        airportsDeparture[i] = TrackerConstants.NA;
        date[i] = 0;
      }
    }

    //Set trips type
    String tripType = "";

    switch (mSearchOptions.getTravelType()) {
      case SIMPLE:
        tripType = TrackerConstants.TYPE_SIMPLE;
        break;
      case ROUND:
        tripType = TrackerConstants.TYPE_ROUND;
        break;
      case MULTIDESTINATION:
        tripType = TrackerConstants.TYPE_MULTIDESTINATION;
        break;
    }

    //Set insurances
    String insurances = "";

    List<InsuranceShoppingItem> insuranceShoppingItems =
        mCreateShoppingCartResponse.getShoppingCart()
            .getOtherProductsShoppingItems()
            .getInsuranceShoppingItems();
    for (int i = 0; i < insuranceShoppingItems.size(); i++) {
      insurances += insuranceShoppingItems.get(0).getInsurance().getTitle() + ", ";
    }

    if (insurances.length() > 1) {
      insurances = insurances.substring(0, insurances.length() - 2);
    }

    //Set purchase successful
    String localyticsBookingStatus;

    switch (getStatusConfirmation()) {
      case CONTRACT:
        localyticsBookingStatus = TrackerConstants.STATUS_OK;
        trackPaymentSuccessfull(mSearchTrackHelper);

        mTracker.trackActiveBookingsOnFlightConfirmed();
        break;
      case PENDING:
        localyticsBookingStatus = TrackerConstants.STATUS_PENDING;
        trackPaymentSuccessfull(mSearchTrackHelper);

        mTracker.trackActiveBookingsOnFlightPending();
        break;
      case REJECTED:
        localyticsBookingStatus = TrackerConstants.STATUS_KO;
        break;
      default:
        localyticsBookingStatus = TrackerConstants.NA;
        break;
    }

    //Send data to tracker
    AndroidDependencyInjector.getInstance()
        .provideTrackerController()
        .trackFlightBookingSummary(mSearchOptions.getNumberOfAdults(),
            mSearchOptions.getNumberOfKids(), mSearchOptions.getCabinClass().getShownText(),
            mCreateShoppingCartResponse.getShoppingCart().getTotalPrice(),
            mSearchOptions.getNumberOfBabies(), insurances, airlines[0], airportsArrival[0],
            airportsDeparture[0], date[0], airlines[1], airportsArrival[1], airportsDeparture[1],
            date[1], airlines[2], airportsArrival[2], airportsDeparture[2], date[2],
            localyticsBookingStatus, insuranceShoppingItems.size() > 0, returnDate, tripType,
            pricePerPassenger, (mSearchOptions.getIncludedBaggage() == null) ? 0
                : mSearchOptions.getIncludedBaggage().size());
  }

  private void trackPaymentSuccessfull(SearchTrackHelper mSearchTrackHelper) {
    if(mSearchTrackHelper != null) {
      mTracker.trackPaymentSuccessful(getMarketingRevenue(),
          Currency.getInstance(Configuration.getInstance().getCurrentMarket().getCurrencyKey()),
          mSearchTrackHelper.getFlightType(), mSearchTrackHelper.getAdults(), mSearchTrackHelper.getKids(), mSearchTrackHelper.getInfants(),
          mSearchTrackHelper.getOrigins(), mSearchTrackHelper.getDestinations(), mSearchTrackHelper.getDates(), mSearchTrackHelper.getAirlines(),
          mSearchTrackHelper.getDepartureCountries(), mSearchTrackHelper.getArrivalCountries());
    }else {
      CrashlyticsController crashlyticsController = AndroidDependencyInjector.getInstance().provideCrashlyticsController();
      crashlyticsController.trackNonFatal(BookingDetailException.newInstance(bookingDetail));
    }
  }

  private BigDecimal getMarketingRevenue() {

    if (marketingRevenueDataAdapterList == null) {
      return BigDecimal.ZERO;
    }

    for (MarketingRevenueDataAdapter marketingRevenueDataAdapter : marketingRevenueDataAdapterList) {
      if (marketingRevenueDataAdapter.getType()
          .equals(MarketingRevenueDataAdapter.ABSOLUTE_VALUE)) {
        return marketingRevenueDataAdapter.getValue();
      }
    }
    return BigDecimal.ZERO;
  }

  /**
   * The passenger name is set in the string that appears before de widget.
   */
  private void updateCrossSellingInfo() {

    TextView txtCompleteTripAddingHotelCar =
        (TextView) findViewById(R.id.txtCompleteTripAddingHotelCar);
    txtCompleteTripAddingHotelCar.setText(
        LocalizablesFacade.getString(this, "crosssellingmodule_informativetitle",
            mCreateShoppingCartResponse.getShoppingCart().getBuyer().getName()));
    txtCompleteTripAddingHotelCar.setVisibility(View.VISIBLE);
  }

  /**
   * Initialize variables
   */
  private void init() {

    oldMyShoppingCart = odigeoSession.getOldMyShoppingCart();
    mPhoneCard = (RelativeLayout) findViewById(R.id.cardPhone);
    confirmationHeader = (ConfirmationHeader) findViewById(R.id.header_confirmation);
    newWidget = (WhatsNextWidget) findViewById(R.id.whatsnew_confirmation);
    confirmationBuyerInfoWidget =
        (ConfirmationBuyerInfoWidget) findViewById(R.id.confirmationBuyerInfoWidget);
    groundTransportationContainer = (LinearLayout) findViewById(R.id.groundTransportationLayout);
    buttonAction = (Button) findViewById(R.id.btnGoToMyTrips);
    buttonAction.setTypeface(Configuration.getInstance().getFonts().getBold());
    buttonAction.setOnClickListener(this);
    buttonAction.setText(
        LocalizablesFacade.getString(this, "confirmationviewcontroller_gotomytripsbutton")
            .toString());
    mScrollView = (ScrollView) findViewById(R.id.scroll_confirmation);
    TextView priceBreakdownTitle = (TextView) findViewById(R.id.tvPriceBreakdownTitle);
    priceBreakdownTitle.setText(LocalizablesFacade.getString(getApplicationContext(),
        OneCMSKeys.PRICINGBREAKDOWN_PRICEBREAKDOWN));
    mCardShareTrip = (CardView) findViewById(R.id.cardShareTrip);
    TextView shareTitle = (TextView) findViewById(R.id.tvShareBookingTitle);
    shareTitle.setText(LocalizablesFacade.getString(this, OneCMSKeys.SHARE_BOOKING_TITLE));
    TextView shareSubtitle = (TextView) findViewById(R.id.tvShareBookingSubtitle);
    shareSubtitle.setText(LocalizablesFacade.getString(this, OneCMSKeys.SHARE_BOOKING_SUBTITLE));
    TextView shareTrip = (TextView) findViewById(R.id.tv_share_booking);
    shareTrip.setText(LocalizablesFacade.getString(this, OneCMSKeys.SHARE_BOOKING_BUTTON));
    shareTrip.setOnClickListener(this);
    TextView txtMyTripsInformation = (TextView) findViewById(R.id.txtMyTripsInformation);
    txtMyTripsInformation.setText(LocalizablesFacade.getString(getApplicationContext(),
        OneCMSKeys.CROSSSELLINGMODULE_MYTRIPS_INFORMATION));
    phoneNumber = LocalizablesFacade.getString(getApplicationContext(),
        OneCMSKeys.CONFIRMATIONVIEWCONTROLLER_PHONEPAYMENT_PHONE_NUMBER).toString();

    TextView rejectTitle = (TextView) findViewById(R.id.tvPhoneTitle);
    rejectTitle.setText(LocalizablesFacade.getString(getApplicationContext(),
        OneCMSKeys.CONFIRMATIONVIEWCONTROLLER_PHONEPAYMENT_TITLE));

    TextView rejectSubtitle = (TextView) findViewById(R.id.tvPhoneSubtitle);
    rejectSubtitle.setText(LocalizablesFacade.getString(getApplicationContext(),
        OneCMSKeys.CONFIRMATIONVIEWCONTROLLER_PHONEPAYMENT_SUBTITLE));
    Button rejectCallButton = (Button) findViewById(R.id.btnPhoneCall);
    rejectCallButton.setText(LocalizablesFacade.getString(getApplicationContext(),
        OneCMSKeys.CONFIRMATIONVIEWCONTROLLER_PHONEPAYMENT_BUTTON, phoneNumber));
    TextView rejectCallDescription = (TextView) findViewById(R.id.tvPhoneDescription);
    rejectCallDescription.setText(LocalizablesFacade.getString(getApplicationContext(),
        OneCMSKeys.CONFIRMATIONVIEWCONTROLLER_PHONEPAYMENT_BUTTON_SUBTITLE));
    rejectCallButton.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {

        CallTelephoneHelper.callPhoneNumberInActivity(OdigeoConfirmationActivity.this, phoneNumber);
        TrackerController.newInstance(getApplication(),
            Configuration.getInstance().getCurrentMarket().getGACustomDim(),
            AndroidDependencyInjector.getInstance().provideABTestHelper(),
            AndroidDependencyInjector.getInstance().provideAnalyticsCustomDimensionFormatter())
            .trackConfirmationRejectedPhoneCall();
      }
    });
  }

  /**
   * Validate the session
   *
   * @return true is validate, false is not validate
   */
  private boolean chkSession() {

    return odigeoSession != null && odigeoSession.isDataHasBeenLoaded();
  }

  /**
   * Set view
   *
   * @param confirmationBuyerInfoWidget set widget to show confirmation widget
   * @param buttonAction set the button to use
   */

  private void setViews(ConfirmationBuyerInfoWidget confirmationBuyerInfoWidget,
      Button buttonAction) {

    String cardName = "";

    if (mShoppingCartCollectionOption != null) {
      if (mShoppingCartCollectionOption.getMethod().getType() == CollectionMethodType.CREDITCARD) {
        cardName = mShoppingCartCollectionOption.getMethod().getCreditCardType().getName();
      } else {
        cardName = mShoppingCartCollectionOption.getMethod().getType().value();
      }
    } else {
      ((CrashlyticsUtil) getApplication()).trackNonFatal(
          ShoppingCartCollectionOptionException.newInstance(null));
    }

    Buyer buyer = mCreateShoppingCartResponse.getShoppingCart().getBuyer();
    newWidget.setStatus(getStatusConfirmation());
    newWidget.setEmailConfirmation(buyer.getMail());

    drawFlightsWidgets();

    addCalenderButton = (Button) findViewById(R.id.btnConfirmationAddToCalendar);

    addCalenderButton.setTypeface(Configuration.getInstance().getFonts().getBold());
    addCalenderButton.setOnClickListener(this);

    addCalenderButton.setText(
        LocalizablesFacade.getString(this, ADD_TO_CALENDAR)
            .toString());

    if (mBooking == null) {
      addCalenderButton.setVisibility(View.GONE);
    }

    drawPassengersWidget();

    confirmationBuyerInfoWidget.setBuyerPayment(buyer, cardName, mCardNumberObfuscated);

    drawInsurances();

    drawPricingBreakdownWidget();
    drawResidenceAcreditationWidget();

    showStatus();

    drawIncompleteBooking();

    buttonAction.setOnClickListener(this);
    postGAEventGoToMyTrips();

    showGroundTransportationWidget();
  }

  private void showGroundTransportationWidget() {
    String active =
        LocalizablesFacade.getString(this, OneCMSKeys.CONFIRMATION_GROUND_TRANSPORTATION_ACTIVE)
            .toString();
    boolean shouldShowGroundTransportation = Boolean.parseBoolean(active);

    if (shouldShowGroundTransportation && mBooking != null) {
      confirmationGroundTransportationView = new ConfirmationGroundTransportationView(this, mBooking, this);
      confirmationGroundTransportationView.setId(R.id.ground_transportation_widget);
      groundTransportationContainer.addView(confirmationGroundTransportationView);
      groundTransportationContainer.setVisibility(View.VISIBLE);
      mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_CONFIRMATION_PAGE_CONFIRMED,
          TrackerConstants.ACTION_XSELL_WIDGET, TrackerConstants.LABEL_GT_APPEARANCES);
    } else {
      groundTransportationContainer.setVisibility(View.GONE);
    }
  }

  /**
   * Show the current status
   */
  private void showStatus() {
    if (getStatusConfirmation() == BookingStatus.CONTRACT) {
      showContract();
    } else if (getStatusConfirmation() == BookingStatus.PENDING) {
      showPending();
    }
  }

  /**
   * Show contract
   */
  private void showContract() {

    drawVelocityInformationWidget();
    findViewById(R.id.layout_aditional_info_confirmation).setVisibility(View.GONE);
    confirmationHeader.setStatusConfirmation(BookingStatus.CONTRACT);
    newWidget.setStatus(BookingStatus.CONTRACT);
    newWidget.setBookingReference(getClientBookingIds());
    newWidget.setEmailConfirmation(
        mCreateShoppingCartResponse.getShoppingCart().getBuyer().getMail());
    // Set Status of the booking
    oldMyShoppingCart.setBookingSummaryStatus(BookingSummaryStatus.OK);
  }

  /**
   * Show pending
   */
  private void showPending() {

    confirmationHeader.setStatusConfirmation(BookingStatus.PENDING);
    findViewById(R.id.layout_aditional_info_confirmation).setVisibility(View.VISIBLE);
    ((TextView) findViewById(R.id.tvAditionalInformationTitle)).setText(
        LocalizablesFacade.getString(getApplicationContext(),
            OneCMSKeys.ADDITIONALINFOMODULE_HEADER_LABELTITLE));
    addCalenderButton.setVisibility(View.GONE);
    newWidget.setStatus(BookingStatus.PENDING);
    newWidget.setEmailConfirmation(
        mCreateShoppingCartResponse.getShoppingCart().getBuyer().getMail());
    newWidget.setBookingReference(getClientBookingIds());
    // Set Status of the booking
    oldMyShoppingCart.setBookingSummaryStatus(BookingSummaryStatus.PENDING);
  }

  /**
   * Show calendar
   */

  private void addToCalendar() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (PermissionsHelper.askForPermissionIfNeeded(Manifest.permission.WRITE_CALENDAR, this,
          OneCMSKeys.PERMISSION_CALENDAR_BOOKING_MESSAGE,
          PermissionsHelper.CALENDAR_REQUEST_CODE)) {
        mCalendarHelper.addBookingToCalendar(mBooking);
      }
    } else {
      mCalendarHelper.addBookingToCalendar(mBooking);
    }
    postGAEventAddToCalendar();
  }

  @Override public void onRequestPermissionsResult(int requestCode, String[] permissions,
      int[] grantResults) {

    switch (requestCode) {
      case PermissionsHelper.CALENDAR_REQUEST_CODE:
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT,
              TrackerConstants.LABEL_CALENDAR_ACCESS_ALLOW);
          addToCalendar();
        } else {
          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT,
              TrackerConstants.LABEL_CALENDAR_ACCESS_DENY);
        }
        break;
      case PermissionsHelper.PHONE_REQUEST_CODE:
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT, TrackerConstants.LABEL_PHONE_ACCESS_ALLOW);
          Intent callIntent = new Intent(Intent.ACTION_CALL);
          callIntent.setData(Uri.parse("tel:" + phoneNumber));
          startActivity(callIntent);
        } else {
          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT, TrackerConstants.LABEL_PHONE_ACCESS_DENY);
        }
        break;
    }
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }

  @Override public void navigateToGroundTransportation(String url) {

    Intent intent = new Intent(this, odigeoApp.getWebViewActivityClass());
    intent.putExtra(Constants.EXTRA_URL_WEBVIEW, url);
    intent.putExtra(Constants.TITLE_WEB_ACTIVITY,
        LocalizablesFacade.getString(this, OneCMSKeys.WEBVIEWCONTROLLER_GROUND_TRANSPORTATION_TITLE)
            .toString());
    intent.putExtra(Constants.SHOW_HOME_ICON, true);
    startActivity(intent);
  }

  /**
   * Show cards for travels
   */
  private void drawFlightsWidgets() {

    List<SegmentWrapper> segments = bookingInfo.getSegmentsWrappers();
    if (segments != null && !segments.isEmpty()) {
      LinearLayout layoutForFlights =
          (LinearLayout) findViewById(R.id.layout_for_flights_confirmation);
      /// ** STAR: draw cards for travels
      int index = 0;
      for (SegmentWrapper segmentWrapper : segments) {
        SummaryTravelWidget widget =
            new SummaryTravelWidget(this, segmentWrapper, mSearchOptions.getTravelType(), index++);
        setSeparator(widget);
        layoutForFlights.addView(widget);
      }
      /// ** END
    }
  }

  /**
   * Show price into widget
   */
  private void drawPricingBreakdownWidget() {

    double price;
    if (bookingDetail.getPrice() != null) {
      price = bookingDetail.getPrice().getAmount().doubleValue();
    } else {
      price = mCreateShoppingCartResponse.getShoppingCart().getTotalPrice().doubleValue();
    }

    TwoTextsDetailsBreakdown details =
        (TwoTextsDetailsBreakdown) findViewById(R.id.text_confirmation_total_price);
    details.setPricePrincipal(price, LocaleUtils.localeToString(Configuration.getCurrentLocale()));
  }

  /**
   * Show insurances
   */
  private void drawInsurances() {

    List<InsuranceShoppingItem> insurances = mCreateShoppingCartResponse.getShoppingCart()
        .getOtherProductsShoppingItems()
        .getInsuranceShoppingItems();

    if (insurances != null && !insurances.isEmpty()) {
      for (InsuranceShoppingItem insurance : insurances) {
        if (insurance.isSelectable()) {
          drawInsurancesWidget(insurance);
        }
      }
      for (InsuranceShoppingItem insurance : insurances) {
        //Is it mandatory?
        if (!insurance.isSelectable()) {
          drawInsurancesMandatory(insurance);
        }
      }
    }
  }

  /**
   * Show velocity into widget
   */
  private void drawVelocityInformationWidget() {

    BookingMessages serializableMessages =
        (BookingMessages) getIntent().getSerializableExtra(Constants.EXTRA_BOOKING_MESSAGES);
    if (serializableMessages != null && serializableMessages.getMessages() != null) {

      BookingMessages bookingMessages = new BookingMessages();
      bookingMessages.setMessages(serializableMessages.getMessages());
      String description = "";
      boolean foundedCode = false;

      if (bookingMessages.getMessages() != null && !bookingMessages.getMessages().isEmpty()) {
        for (int i = 0; i < bookingMessages.getMessages().size(); i++) {
          if (bookingMessages.getMessages()
              .get(i)
              .getCode()
              .equalsIgnoreCase(MessageCode.VEL_001.getMessageCode())
              || bookingMessages.getMessages()
              .get(i)
              .getCode()
              .equalsIgnoreCase(MessageCode.VEL_002.getMessageCode())) {
            foundedCode = true;
            description = bookingMessages.getMessages().get(i).getDescription();
          }
        }
      }

      if (foundedCode) {
        VelocityInformationWidget velocityInformationWidget =
            (VelocityInformationWidget) findViewById(R.id.confirmationVelocityInformationWidget);
        velocityInformationWidget.setVisibility(View.VISIBLE);
        velocityInformationWidget.setText(description);
      }
    }
  }

  /**
   * Show insurances into widget
   */
  private void drawInsurancesWidget(InsuranceShoppingItem insuranceShoppingItem) {

    InsuranceWidget insuranceWidget = new InsuranceWidget(this, mScrollView, insuranceShoppingItem,
        mCreateShoppingCartResponse.getShoppingCart().getTravellers().size());
    setSeparator(insuranceWidget);
    LinearLayout container = (LinearLayout) findViewById(R.id.layout_for_insurance_confirmation);
    container.addView(insuranceWidget);
  }

  /**
   * Show residence acreditation
   */

  private void drawResidenceAcreditationWidget() {
    if (mSearchOptions.getResident() != null) {
      PassengerResidenceAcreditationWidget widget =
          (PassengerResidenceAcreditationWidget) findViewById(
              R.id.residence_acreditation_confirmation);
      widget.setVisibility(View.VISIBLE);
      widget.setShoppingCart(bookingInfo.getSegmentsWrappers(), mSearchOptions,
          bookingDetail.getItineraryBookings().getLegend().getResidentsValidation(),
          bookingDetail.getItineraryBookings().getLegend().getSectionResults(),
          mCreateShoppingCartResponse.getShoppingCart().getTravellers());
    }
  }

  /**
   * Show insurance mandatory
   *
   * @param insuranceShoppingItem receive data from DTO
   */
  private void drawInsurancesMandatory(InsuranceShoppingItem insuranceShoppingItem) {

    InsuranceMandatoryWidget widget = new InsuranceMandatoryWidget(this, insuranceShoppingItem);
    setSeparator(widget);
    LinearLayout container = (LinearLayout) findViewById(R.id.layout_for_insurance_confirmation);
    container.addView(widget);
  }

  /**
   * Show passenger into widget
   */
  private void drawPassengersWidget() {

    List<Traveller> travellers = mCreateShoppingCartResponse.getShoppingCart().getTravellers();
    List<FlightSegment> flightSegments = mSearchOptions.getFlightSegments();
    if (travellers != null && !travellers.isEmpty()) {
      PassengersResumeWidget widget =
          (PassengersResumeWidget) findViewById(R.id.passengers_widget_confirmation);
      widget.setPassengers(travellers, flightSegments);
      widget.setVisibility(View.VISIBLE);
    }
  }

  @Override public final boolean onCreateOptionsMenu(Menu menu) {

    getMenuInflater().inflate(R.menu.menu_goto_home, menu);
    MenuItem item = menu.findItem(R.id.menu_item_goto_home_activity);
    item.setTitle(LocalizablesFacade.getString(this, "slidehome_notrip_mainmessage").toString());
    return super.onCreateOptionsMenu(menu);
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {

    int i = item.getItemId();
    if (i == android.R.id.home) {
      finish();
      return true;
    } else if (i == R.id.menu_item_goto_home_activity) {
      postGAEventGoHome();
      gotoHome();
      return true;
    } else {
      return super.onOptionsItemSelected(item);
    }
  }

  /**
   * Go to home
   */
  private void gotoHome() {

    Intent intent = new Intent(this, odigeoApp.getHomeActivity());
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  /**
   * Line separator
   *
   * @param view view to add separator
   */
  private void setSeparator(View view) {
    // add Divider between each widget
    int paddingTop = getResources().getDimensionPixelSize(R.dimen.separator_general_widgets);
    view.setPadding(0, paddingTop, 0, 0);
  }

  @Override public final void onBackPressed() {

    super.onBackPressed();
    gotoHome();
  }

  @Override public final void onClick(View view) {

    int idView = view.getId();
    if (idView == R.id.btnGoToMyTrips) {
      Intent intent = new Intent(OdigeoConfirmationActivity.this, odigeoApp.getHomeActivity());
      intent.putExtra(Constants.EXTRA_ACTION_GOTO_MYTRIPS, true);
      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      startActivity(intent);
    } else if (idView == R.id.btnConfirmationAddToCalendar) {
      addToCalendar();
    } else if (idView == R.id.tv_share_booking) {
      postGAEventShareTrip();
      ShareTripModel shareTripModel = prepareTripToShare();
      if (shareTripModel != null) {
        ShareUtils.getInstance().share(getApplicationContext(), shareTripModel);
      }
    }
  }

  /**
   * This method is to create the SharetripModel to use it for the email text
   *
   * @return shareTripModel
   */
  @Nullable private ShareTripModel prepareTripToShare() {

    ShareTripModel shareTripModel = null;
    String storeUrl = Configuration.getInstance().getAppStoreURL();
    SimpleDateFormat simpleDateFormat = OdigeoDateUtils.getGmtDateFormat(DD_MM_YYYY);
    SimpleDateFormat simpleHourFormat = OdigeoDateUtils.getGmtDateFormat(HH_MM);

    List<SegmentWrapper> segments = bookingInfo.getSegmentsWrappers();

    if (segments != null && !segments.isEmpty()) {

      List<SectionDTO> sectionDTOs = segments.get(0).getSectionsObjects();
      SectionDTO sectionDepartureDTO = sectionDTOs.get(0);
      SectionDTO sectionArrivalDto = sectionDTOs.get(sectionDTOs.size() - 1);
      Calendar date = OdigeoDateUtils.createCalendar(sectionDepartureDTO.getDepartureDate());

      String fromCity = sectionDepartureDTO.getLocationFrom().getCityName();
      String toCity = sectionArrivalDto.getLocationTo().getCityName();
      String price = String.valueOf(
          mCreateShoppingCartResponse.getShoppingCart().getTotalPrice().doubleValue());
      String passengers = String.valueOf(mSearchOptions.getTotalPassengers());
      String dateDeparting = simpleDateFormat.format(date.getTime());
      String hourDeparting = simpleHourFormat.format(date.getTime());
      String dateArriving = simpleDateFormat.format(sectionArrivalDto.getArrivalDate());
      String hourArriving = simpleHourFormat.format(sectionArrivalDto.getArrivalDate());
      String carrier = segments.get(0).getCarrier().getName();
      String travelType = mSearchOptions.getTravelType().name();

      if (mSearchOptions.getTravelType() != TravelType.SIMPLE) {
        List<SectionDTO> sectionDTOsBack = segments.get(segments.size() - 1).getSectionsObjects();
        SectionDTO sectionDepartureDTOBack = sectionDTOsBack.get(0);
        SectionDTO sectionArrivalDtoBack = sectionDTOsBack.get(sectionDTOsBack.size() - 1);
        Calendar dateBack =
            OdigeoDateUtils.createCalendar(sectionDepartureDTOBack.getDepartureDate());
        String fromCitySecond = sectionDepartureDTOBack.getLocationFrom().getCityName();
        String toCitySecond = sectionArrivalDtoBack.getLocationTo().getCityName();
        String passengersBack = String.valueOf(mSearchOptions.getTotalPassengers());
        String dateDepartingSecond = simpleDateFormat.format(dateBack.getTime());
        String hourDepartingSecond = simpleHourFormat.format(dateBack.getTime());
        String dateArrivingSecond = simpleDateFormat.format(sectionArrivalDtoBack.getArrivalDate());
        String hourArrivingSecond = simpleHourFormat.format(sectionArrivalDtoBack.getArrivalDate());
        String carrierSecond = segments.get(segments.size() - 1).getCarrier().getName();
        shareTripModel =
            new ShareTripModel(getApplicationContext(), fromCity, toCity, price, passengers,
                dateDeparting, hourDeparting, dateArriving, hourArriving, carrier, fromCitySecond,
                toCitySecond, dateDepartingSecond, hourDepartingSecond, dateArrivingSecond,
                hourArrivingSecond, carrierSecond, passengersBack, travelType);
        shareTripModel.createEmailBody(OneCMSKeys.SHARE_BOOKING_EMAIL_BODY, storeUrl);
      } else {
        shareTripModel =
            new ShareTripModel(getApplicationContext(), fromCity, toCity, price, passengers,
                dateDeparting, hourDeparting, dateArriving, hourArriving, carrier, "", "", "", "",
                "", "", "", "", travelType);
        shareTripModel.createEmailBody(OneCMSKeys.SHARE_BOOKING_EMAIL_BODY_ONE_WAY, storeUrl);
      }
      shareTripModel.createEmailSubject(OneCMSKeys.SHARE_BOOKING_EMAIL_SUBJECT);
      shareTripModel.createTwitterShareText(OneCMSKeys.SHARE_BOOKING_TWITTER_DESCRIPTION, storeUrl);
      shareTripModel.createWhatsappShareText(OneCMSKeys.SHARE_BOOKING_WHATSAPP_DESCRIPTION,
          storeUrl);
      shareTripModel.createCopyToClipboardShareText(OneCMSKeys.SHARE_BOOKING_CLIPBOARD_DESCRIPTION,
          storeUrl);
      shareTripModel.createFacebookShareText(storeUrl);
      shareTripModel.createFbMessengerShareText(OneCMSKeys.SHARE_BOOKING_FB_MESSENGER_DESCRIPTION,
          storeUrl);
      shareTripModel.createSmsShareText(OneCMSKeys.SHARE_BOOKING_SMS_DESCRIPTION, storeUrl);
    }
    return shareTripModel;
  }

  private void postGAEventShareTrip() {

    TrackerController.newInstance(getApplication(),
        Configuration.getInstance().getCurrentMarket().getGACustomDim(),
        AndroidDependencyInjector.getInstance().provideABTestHelper(),
        AndroidDependencyInjector.getInstance().provideAnalyticsCustomDimensionFormatter())
        .trackAnalyticsEvent(TrackerConstants.CATEGORY_CONFIRMATION_PAGE_CONFIRMED,
            TrackerConstants.ACTION_TRIP_DETAILS, TrackerConstants.LABEL_CONFIRMATION_SHARE_TRIP);
  }

  public final BookingStatus getStatusConfirmation() {

    return bookingDetail.getBookingStatus();
  }

  /**
   * Post event go home
   */
  private void postGAEventGoHome() {

    if (getStatusConfirmation() == BookingStatus.CONTRACT) {
      BusProvider.getInstance()
          .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_CONFIRMATION_PAGE_CONFIRMED,
              GAnalyticsNames.ACTION_NAVIGATION, GAnalyticsNames.LABEL_HOME_CLICKS));
    }
    if (getStatusConfirmation() == BookingStatus.REJECTED) {
      BusProvider.getInstance()
          .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_CONFIRMATION_PAGE_REJECTED,
              GAnalyticsNames.ACTION_BOOKING_REJECTED, GAnalyticsNames.LABEL_HOME_CLICKS));
    }
    if (getStatusConfirmation() == BookingStatus.PENDING) {
      BusProvider.getInstance()
          .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_CONFIRMATION_PAGE_PENDING,
              GAnalyticsNames.ACTION_BOOKING_PENDING, GAnalyticsNames.LABEL_HOME_CLICKS));
    }
  }

  /**
   * Post event calendar
   */
  private void postGAEventAddToCalendar() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_CONFIRMATION_PAGE_CONFIRMED,
            GAnalyticsNames.ACTION_BOOKING_CONFIRMED, GAnalyticsNames.LABEL_CALENDAR_ADD_CLICKS));
  }

  /**
   * Post event my trips
   */
  private void postGAEventGoToMyTrips() {

    if (getStatusConfirmation() == BookingStatus.CONTRACT) {
      BusProvider.getInstance()
          .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_CONFIRMATION_PAGE_CONFIRMED,
              GAnalyticsNames.ACTION_BOOKING_CONFIRMED, GAnalyticsNames.LABEL_MY_TRIPS_CLICKS));
    } else if (getStatusConfirmation() == BookingStatus.PENDING) {
      BusProvider.getInstance()
          .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_CONFIRMATION_PAGE_PENDING,
              GAnalyticsNames.ACTION_BOOKING_PENDING, GAnalyticsNames.LABEL_MY_TRIPS_CLICKS));
    }
  }

  /**
   * Post booking rejected
   */
  private void postGAScreenBookingRejected() {
    BusProvider.getInstance()
        .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_CONFIRMATION_REJECTED));
  }

  /**
   * Post booking confirmed
   */
  private void postGAScreenBookingConfirmed() {
    // GAnalytics screen tracking - Booking confirmed
    BusProvider.getInstance()
        .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_CONFIRMATION_CONTRACT));
  }

  /**
   * Post payments pending
   */
  private void postGAScreenPaymentPending() {
    // GAnalytics screen tracking - Payment pending
    BusProvider.getInstance()
        .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_CONFIRMATION_PENDING));
  }

  /**
   * Abstract perform tracking
   */
  public void performGATracking() {

    if (getStatusConfirmation() == BookingStatus.REJECTED) {
      // GAnalytics screen tracking - Payment rejected
      BusProvider.getInstance()
          .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_CONFIRMATION_REJECTED));
    } else if (getStatusConfirmation() == BookingStatus.CONTRACT) {

      // GAnalytics screen tracking - Booking confirmed
      BusProvider.getInstance()
          .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_CONFIRMATION_CONTRACT));
    } else if (getStatusConfirmation() == BookingStatus.PENDING) {
      // GAnalytics screen tracking - Payment pending
      BusProvider.getInstance()
          .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_CONFIRMATION_PENDING));
    }
  }

  /**
   * Here we link and setup the information for the incomplete booking
   */
  private void drawIncompleteBooking() {
    if (bankTransferData == null || bankTransferResponse == null) {
      findViewById(R.id.form_incomplete_booking).setVisibility(View.GONE);
      confirmationBuyerInfoWidget.setVisibility(View.VISIBLE);
    } else {
      findViewById(R.id.form_incomplete_booking).setVisibility(View.VISIBLE);
      confirmationBuyerInfoWidget.setVisibility(View.GONE);
      UnfoldingBankTransferInfo bankTransferInfo =
          ((UnfoldingBankTransferInfo) findViewById(R.id.banktransfer_wrapper));
      bankTransferInfo.setData(bankTransferData.get(0), bankTransferResponse,
          mCreateShoppingCartResponse.getShoppingCart().getTotalPrice());
    }
  }

  public abstract void onGAEventTriggered(GATrackingEvent event);

  private List<String> getClientBookingIds() {

    Set<String> set = new HashSet<>();
    if (bookingDetail.getItineraryBookings() != null) {
      for (ItineraryProviderBooking itineraryProviderBooking : bookingDetail.getItineraryBookings()
          .getBookings()) {
        set.add(itineraryProviderBooking.getPnr());
      }
    }

    List<String> identifications = new ArrayList<>();
    for (String pnr : set.toArray(new String[set.size()])) {
      String identification = String.valueOf(bookingDetail.getBookingBasicInfo().getId());
      if (pnr != null && !pnr.equals("null")) {
        identification = identification + " - " + pnr;
      }
      identifications.add(identification);
    }
    return identifications;
  }

  private void trackTransaction(BigDecimal marketingRevenue, Money currency,
      ShoppingCartCollectionOption shoppingCartCollectionOption) {
    if (currency != null) {
      trackTuneRevenue(marketingRevenue, currency.getCurrency());

      if (shoppingCartCollectionOption != null) {
        trackGARevenue(marketingRevenue, currency.getCurrency(), shoppingCartCollectionOption);
      } else {
        AndroidDependencyInjector.getInstance()
            .provideCrashlyticsController()
            .trackNonFatal(ShoppingCartResponseException.newInstance(mCreateShoppingCartResponse));
      }
    }
  }

  private void trackTuneRevenue(BigDecimal marketingRevenue, String currency) {

    TuneHelper.setBookingId(
        String.valueOf(mCreateShoppingCartResponse.getShoppingCart().getBookingId()));
    TuneHelper.setRevenueMarketing(marketingRevenue.doubleValue());
    TuneHelper.setCurrencyCode(currency);

    mTuneTracker.trackTransaction();
  }

  private void trackGARevenue(BigDecimal marketingRevenue, String currency,
      ShoppingCartCollectionOption shoppingCartCollectionOption) {
    Transaction transaction =
        Ecommerce.createGATransaction(mSearchOptions, mCreateShoppingCartResponse.getShoppingCart(),
            mCreateShoppingCartResponse.getShoppingCart()
                .getOtherProductsShoppingItems()
                .getInsuranceShoppingItems(), marketingRevenue, currency,
            shoppingCartCollectionOption);

    mTracker.trackEcommerceTransaction(transaction,
        Configuration.getInstance().getCurrentMarket().getGACustomDim());

    if (transaction.getProductList() != null) {
      for (Product product : transaction.getProductList()) {
        mTracker.trackEcommerceProduct(product,
            Configuration.getInstance().getCurrentMarket().getGACustomDim());
      }
    }
  }
}
