package com.odigeo.app.android.lib.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.holders.BaseHolder;
import com.odigeo.data.entity.BaseSpinnerItem;
import java.util.List;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 12/01/16
 */
public abstract class BaseOdigeoAdapter<T extends BaseSpinnerItem> extends ArrayAdapter<T> {

  /**
   * Constructor
   *
   * @param context Context where be called the adapter.
   * @param objects List of the object to be displayed.
   */
  public BaseOdigeoAdapter(Context context, List<T> objects) {
    super(context, R.layout.spinner_item, objects);
  }

  @Override public View getDropDownView(int position, View convertView, ViewGroup parent) {
    return inflateView(getLayout(), position, convertView, parent);
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {
    return inflateView(getLayout(), position, convertView, parent);
  }

  /**
   * Retrieves the layout to be inflated on each item.
   *
   * @return Layout Resource Id to be inflated.
   */
  protected int getLayout() {
    return R.layout.spinner_item;
  }

  /**
   * Method to the common inflate of the items.
   *
   * @param position Position to be inflated.
   * @param convertView View corresponding to the position.
   * @param parent Parent of the item to be inflated.
   * @return A non empty and set view.
   */
  protected final View inflateView(@LayoutRes int layout, int position, @Nullable View convertView,
      @NonNull ViewGroup parent) {
    T item = getItem(position);
    convertView = buildViewAndHolder(convertView, parent, layout);
    BaseHolder holder = (BaseHolder) convertView.getTag();
    drawItem(holder, item);
    return convertView;
  }

  /**
   * Inflates the view and creates the holder.
   *
   * @param convertView View for the item.
   * @param parent Parent where will be attached the convertView.
   * @param layout Layout to be inflated.
   * @return A non null view.
   */
  protected abstract View buildViewAndHolder(View convertView, ViewGroup parent, int layout);

  /**
   * Draws all the elements of the item.
   *
   * @param holder View Holder where are stored the views' references.
   * @param item Item to be drew.
   */
  protected abstract void drawItem(BaseHolder holder, BaseSpinnerItem item);
}
