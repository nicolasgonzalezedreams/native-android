package com.odigeo.app.android.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.PdfDownloader;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.shoppingCart.Insurance;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.tracker.TrackerConstants;

public class InsuranceMandatoryWidget extends RelativeLayout {

  private Activity mActivity;
  private Insurance mInsurance;
  private TrackerControllerInterface mTracker;

  public InsuranceMandatoryWidget(Context context, AttributeSet attrs) {

    super(context, attrs);

    initialize();
  }

  public InsuranceMandatoryWidget(Activity context, Insurance insurance) {
    super(context);

    this.mInsurance = insurance;
    this.mActivity = context;
    mTracker = AndroidDependencyInjector.getInstance().provideTrackerController();
    initialize();
  }

  private void initialize() {
    inflate(getContext(), R.layout.widget_insurance_mandatory, this);

    getRootView().setOnClickListener(new OnClickListener() {

      @Override public void onClick(View v) {
        if (mInsurance != null
            && mInsurance.getConditionsUrls() != null
            && !mInsurance.getConditionsUrls().isEmpty()) {
          String urlConditions = mInsurance.getConditionsUrls().get(0).getUrl();
          mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_INSURANCES_T_CS);
          PdfDownloader task2 = new PdfDownloader(mActivity);
          task2.execute(urlConditions);
        }
      }
    });

    TextView insuranceTitleTextView = (TextView) findViewById(R.id.mandatory_insurance_title);
    TextView insuranceTextTextView = (TextView) findViewById(R.id.mandatory_insurance_text);

    insuranceTitleTextView.setTypeface(Configuration.getInstance().getFonts().getRegular());
    insuranceTextTextView.setTypeface(Configuration.getInstance().getFonts().getBold());

    insuranceTitleTextView.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.MANDATORY_INSURANCE_TITLE));
    if (mInsurance != null) {
      insuranceTextTextView.setText(mInsurance.getTitle());
    }
    this.setClickable(true);
  }
}