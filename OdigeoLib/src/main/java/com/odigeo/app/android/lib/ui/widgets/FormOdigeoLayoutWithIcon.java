package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;

/**
 * Created by Irving Lóp on 13/10/2014.
 */

// si se intenta heredar de el o construirlo por código
// no se obtendra el comportamiento esperado
public class FormOdigeoLayoutWithIcon extends LinearLayout {

  private ImageView headerImageView;
  private TextView headerTextView;

  public FormOdigeoLayoutWithIcon(Context context, AttributeSet attrs) {
    super(context, attrs, 0);

    TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.FormOdigeoLayoutWithIcon);
    String title = array.getString(R.styleable.FormOdigeoLayoutWithIcon_textTitle);
    int idIcon =
        array.getResourceId(R.styleable.FormOdigeoLayoutWithIcon_iconDelete, R.drawable.close);
    array.recycle();

    inflateLayout();
    headerTextView.setText(LocalizablesFacade.getString(getContext(), title));
    setIdImage(idIcon);
  }

  public FormOdigeoLayoutWithIcon(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);

    TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.FormOdigeoLayoutWithIcon);
    String title = array.getString(R.styleable.FormOdigeoLayoutWithIcon_textTitle);
    int idIcon =
        array.getResourceId(R.styleable.FormOdigeoLayoutWithIcon_iconDelete, R.drawable.close);
    array.recycle();

    inflateLayout();
    headerTextView.setText(LocalizablesFacade.getString(getContext(), title));
    setIdImage(idIcon);
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.layout_form_odigeo_with_icon, this);
    setBackgroundResource(R.drawable.background_card);
    headerImageView = (ImageView) findViewById(R.id.button_delete_form_odigeo);
    headerTextView = (TextView) findViewById(R.id.text_title_form_odigeo);
  }

  private void setTitleText(String title) {
    headerTextView.setText(title);
  }

  public final String getTitle() {
    return headerTextView.getText().toString();
  }

  public final void setTitle(String title) {
    setTitleText(title);
  }

  public final void setIdImageResource(int idImageResource) {
    setIdImage(idImageResource);
  }

  private void setIdImage(int idImageResource) {
    headerImageView.setImageResource(idImageResource);
  }

  public final void setOnClickIconListener(OnClickListener clickListener) {
    headerImageView.setOnClickListener(clickListener);
  }

  public final void hideIcon() {
    headerImageView.setVisibility(GONE);
  }
}
