package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

/**
 * <p>Java class for promotionalCodeUsageResponse complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="promotionalCodeUsageResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class PromotionalCodeUsageResponseDTO {

  protected String code;
  protected BigDecimal amount;

  /**
   * Gets the value of the code property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCode() {
    return code;
  }

  /**
   * Sets the value of the code property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCode(String value) {
    this.code = value;
  }

  /**
   * Gets the value of the amount property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getAmount() {
    return amount;
  }

  /**
   * Sets the value of the amount property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setAmount(BigDecimal value) {
    this.amount = value;
  }
}
