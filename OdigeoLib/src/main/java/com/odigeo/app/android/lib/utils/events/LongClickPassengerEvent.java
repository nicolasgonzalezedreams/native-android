package com.odigeo.app.android.lib.utils.events;

import com.odigeo.app.android.lib.ui.widgets.PassengerSavedRow;

public class LongClickPassengerEvent {
  private PassengerSavedRow row;

  public LongClickPassengerEvent(PassengerSavedRow row) {
    this.row = row;
  }

  public final PassengerSavedRow getRow() {
    return row;
  }

  public final void setRow(PassengerSavedRow row) {
    this.row = row;
  }
}
