package com.odigeo.app.android.lib.ui.widgets.base;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created by Irving Lóp on 09/10/2014.
 */
public abstract class TextWithImage extends PressableWidget {
  private String text;
  private int idImage;
  private ColorStateList textColor;

  private TextView textView;
  private ImageView imageView;

  private PropertyChangeListener listener;
  private OdigeoImageLoader mImageLoader;

  public TextWithImage(Context context) {
    super(context);
    inflateLayout();
    mImageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();
  }

  public TextWithImage(Context context, AttributeSet attrs) {
    super(context, attrs);
    inflateLayout();
    setAttributs(attrs);
    mImageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();
  }

  private void setAttributs(AttributeSet attributs) {
    TypedArray typedArray =
        getContext().obtainStyledAttributes(attributs, R.styleable.TextWithImage);
    text = typedArray.getString(R.styleable.TextWithImage_odigeoText);
    idImage = typedArray.getResourceId(R.styleable.TextWithImage_src, R.drawable.confirmation_info);
    textColor = typedArray.getColorStateList(R.styleable.TextWithImage_textColor);
    typedArray.recycle();

    if (text != null) {
      textView.setText(LocalizablesFacade.getString(getContext(), text));
    }
    if (textColor != null) {
      textView.setTextColor(textColor);
    }

    imageView.setImageResource(idImage);
  }

  private void inflateLayout() {
    inflate(getContext(), getLayout(), this);
    textView = (TextView) findViewById(getIdTextView());
    imageView = (ImageView) findViewById(getIdImageView());
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    notifyListener(this, this.text, this.text, text);
    this.text = text;
    textView.setText(text);
  }

  public int getIdImage() {
    return idImage;
  }

  public void setIdImage(int idImage) {
    this.idImage = idImage;
    imageView.setImageResource(idImage);
  }

  public void setUrlImage(String urlImage) {
    mImageLoader.load(imageView, urlImage);
  }

  public ColorStateList getTextColor() {
    return textColor;
  }

  public void setTextColor(ColorStateList textColor) {
    this.textColor = textColor;
    textView.setTextColor(textColor);
  }

  protected abstract int getLayout();

  protected abstract int getIdTextView();

  protected abstract int getIdImageView();

  private void notifyListener(Object object, String property, String oldValue, String newValue) {
    if (listener != null) {
      listener.propertyChange(new PropertyChangeEvent(object, property, oldValue, newValue));
    }
  }

  public void setChangeListener(PropertyChangeListener newListener) {
    this.listener = newListener;
  }
}
