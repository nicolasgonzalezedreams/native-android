package com.odigeo.app.android.lib.models.dto;

import com.odigeo.app.android.lib.models.dto.custom.FlowConfiguration;

/**
 * @author miguel
 */
public class SearchStatusResponseDTO extends PreferencesAwareResponseDTO {

  protected SearchResultsPageDTO itineraryResultsPage;
  protected HotelSearchResultsDTO hotelSearchResults;
  protected boolean requiresDisambiguation;
  protected boolean requiresResidentConfirmation;

  // Custom
  protected String currency;
  protected String locale;
  protected String webSite;
  protected String mkportal;
  protected FlowConfiguration flowConfiguration;

  public SearchStatusResponseDTO() {
  }

  /**
   * Gets the value of the itineraryResultsPage property.
   *
   * @return possible object is {@link SearchResultsPage }
   */
  public SearchResultsPageDTO getItineraryResultsPage() {
    return itineraryResultsPage;
  }

  /**
   * Sets the value of the itineraryResultsPage property.
   *
   * @param value allowed object is {@link SearchResultsPage }
   */
  public void setItineraryResultsPage(SearchResultsPageDTO value) {
    this.itineraryResultsPage = value;
  }

  /**
   * Gets the value of the requiresDisambiguation property.
   */
  public boolean isRequiresDisambiguation() {
    return requiresDisambiguation;
  }

  /**
   * Sets the value of the requiresDisambiguation property.
   */
  public void setRequiresDisambiguation(boolean value) {
    this.requiresDisambiguation = value;
  }

  /**
   * Gets the value of the requiresResidentConfirmation property.
   */
  public boolean isRequiresResidentConfirmation() {
    return requiresResidentConfirmation;
  }

  /**
   * Sets the value of the requiresResidentConfirmation property.
   */
  public void setRequiresResidentConfirmation(boolean value) {
    this.requiresResidentConfirmation = value;
  }

  /**
   * @return the currency
   */
  public String getCurrency() {
    return currency;
  }

  /**
   * @param currency the currency to set
   */
  public void setCurrency(String currency) {
    this.currency = currency;
  }

  /**
   * @return the locale
   */
  public String getLocale() {
    return locale;
  }

  /**
   * @param locale the locale to set
   */
  public void setLocale(String locale) {
    this.locale = locale;
  }

  /**
   * @return the webSite
   */
  public String getWebSite() {
    return webSite;
  }

  /**
   * @param webSite the webSite to set
   */
  public void setWebSite(String webSite) {
    this.webSite = webSite;
  }

  /**
   * @return the mkportal
   */
  public String getMkportal() {
    return mkportal;
  }

  /**
   * @param mkportal the mkportal to set
   */
  public void setMkportal(String mkportal) {
    this.mkportal = mkportal;
  }

  /**
   * @return the flowConfiguration
   */
  public FlowConfiguration getFlowConfiguration() {
    return flowConfiguration;
  }

  /**
   * @param flowConfiguration the flowConfiguration to set
   */
  public void setFlowConfiguration(FlowConfiguration flowConfiguration) {
    this.flowConfiguration = flowConfiguration;
  }

  /**
   * @return the hotelResults
   */
  public HotelSearchResultsDTO getHotelResults() {
    return hotelSearchResults;
  }

  /**
   * @param hotelResults the hotelResults to set
   */
  public void setHotelResults(HotelSearchResultsDTO hotelResults) {
    this.hotelSearchResults = hotelResults;
  }
}
