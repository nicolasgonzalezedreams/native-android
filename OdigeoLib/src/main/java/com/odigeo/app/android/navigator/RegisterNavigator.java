package com.odigeo.app.android.navigator;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.FacebookAlreadyRegisteredView;
import com.odigeo.app.android.view.GoogleAlreadyRegisteredView;
import com.odigeo.app.android.view.RegisterSuccessfulView;
import com.odigeo.app.android.view.RegisterView;
import com.odigeo.app.android.view.UserAlreadyRegisteredView;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.PermissionsHelper;
import com.odigeo.dataodigeo.net.controllers.GooglePlusController;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.exceptions.SocialLoginTimeOutException;
import com.odigeo.presenter.contracts.navigators.RegisterNavigatorInterface;

public class RegisterNavigator extends BaseNavigator implements RegisterNavigatorInterface {

  private RegisterView mRegisterView;
  private GoogleAlreadyRegisteredView mGoogleRegisteredView;
  private FacebookAlreadyRegisteredView mFacebookRegisteredView;
  private OdigeoApp odigeoApp;
  private boolean mComesFromJoinUs;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view);
    Intent intent = getIntent();
    mComesFromJoinUs = intent.hasExtra(Constants.EXTRA_COMES_FROM_JOIN_US);

    odigeoApp = (OdigeoApp) getApplication();
    showRegisterView();
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    if (requestCode == GooglePlusController.REQUEST_RESOLVE_ERROR) {
      Fragment f = getSupportFragmentManager().findFragmentById(R.id.fl_container);
      f.onActivityResult(requestCode, resultCode, data);
    } else {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }

  @Override public void registerSuccess(String email) {
    setTitle(LocalizablesFacade.getString(this, OneCMSKeys.SSO_REGISTER_SUCCESS));
    setUpToolbarButton(R.drawable.abc_ic_clear_material);
    replaceFragment(R.id.fl_container, getRegisterSuccessfulView(email));
    tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_SSO_REGISTER,
        TrackerConstants.ACTION_REGISTER_FORM,
        TrackerConstants.LABEL_SSO_SUCCESSFUL_REGISTER_ODIGEO);
    tracker.trackSignUp();
  }

  @Override public void navigateToLogIn(String email) {
    Bundle bundle = new Bundle();
    bundle.putString(UserAlreadyRegisteredView.USER_EMAIL, email);
    Intent intent = new Intent(getApplicationContext(), LoginNavigator.class);
    intent.putExtras(bundle);
    startActivity(intent);
    tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_SSO_REGISTER,
        TrackerConstants.ACTION_REGISTER_FORM,
        TrackerConstants.LABEL_SSO_EMAIL_ALREADY_USED_ODIGEO_ERROR);
    finish();
  }

  @Override public void showUserRegisteredFacebook(String email) {
    Bundle bundle = new Bundle();
    bundle.putString("parentClass", "Register");
    setTitle(LocalizablesFacade.getString(this, OneCMSKeys.SSO_ERROR_ALREADY_USED));
    setUpToolbarButton(R.drawable.abc_ic_clear_material);
    replaceFragment(R.id.fl_container, getFacebookRegisteredView(email));
    tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_SSO_REGISTER,
        TrackerConstants.ACTION_REGISTER_FORM,
        TrackerConstants.LABEL_SSO_EMAIL_ALREADY_USED_FACEBOOK_ERROR);
  }

  @Override public void showUserRegisteredGoogle(String email) {
    Bundle bundle = new Bundle();
    bundle.putString("parentClass", "Register");
    setTitle(LocalizablesFacade.getString(this, OneCMSKeys.SSO_ERROR_ALREADY_USED));
    setUpToolbarButton(R.drawable.abc_ic_clear_material);
    replaceFragment(R.id.fl_container, getGoogleRegisteredView(email));
    tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_SSO_REGISTER,
        TrackerConstants.ACTION_REGISTER_FORM,
        TrackerConstants.LABEL_SSO_EMAIL_ALREADY_USED_GOOGLE_ERROR);
  }

  private void showRegisterView() {
    replaceFragment(R.id.fl_container, getRegisterView());
    setTitle(LocalizablesFacade.getString(this, OneCMSKeys.SSO_REGISTER_TITLE).toString());
    setUpToolbarButton(0);
  }

  /**
   * Get register fragment instance
   *
   * @return {@link RegisterView}
   */
  private RegisterView getRegisterView() {
    if (mRegisterView == null) {
      mRegisterView = RegisterView.newInstance();
    }

    return mRegisterView;
  }

  /**
   * Get google registered view fragment instance
   *
   * @return {@link GoogleAlreadyRegisteredView}
   */
  private GoogleAlreadyRegisteredView getGoogleRegisteredView(String email) {
    if (mGoogleRegisteredView == null) {
      mGoogleRegisteredView = GoogleAlreadyRegisteredView.newInstance(email);
    }

    return mGoogleRegisteredView;
  }

  /**
   * Get facebook registered view fragment instance
   *
   * @return {@link FacebookAlreadyRegisteredView}
   */
  private FacebookAlreadyRegisteredView getFacebookRegisteredView(String email) {
    if (mFacebookRegisteredView == null) {
      mFacebookRegisteredView = FacebookAlreadyRegisteredView.newInstance(email);
    }

    return mFacebookRegisteredView;
  }

  @Override public void navigateToHome() {
    startActivity(new Intent(this, odigeoApp.getHomeActivity()));
    finish();
  }

  @Override public void navigateToLogin() {
    Intent intent = new Intent(getApplicationContext(), LoginNavigator.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(intent);
    finish();
  }

  @Override public void onTimeOutError() {
    odigeoApp.trackNonFatal(new SocialLoginTimeOutException());
  }

  private RegisterSuccessfulView getRegisterSuccessfulView(String email) {
    return RegisterSuccessfulView.newInstance(email);
  }

  public void setUpToolbarButton(int resourceId) {
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      if (resourceId != 0) {
        actionBar.setHomeAsUpIndicator(resourceId);
      } else {
        actionBar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_material);
      }
      actionBar.setDisplayHomeAsUpEnabled(true);
    }
  }

  @Override public void onBackPressed() {
    navigateBack();
  }

  @Override protected void onHomeUpButtonPressed() {
    navigateBack();
  }

  private void navigateBack() {
    android.support.v4.app.Fragment currentFragment =
        getSupportFragmentManager().findFragmentById(R.id.fl_container);
    if (currentFragment != null) {
      if (currentFragment.getClass().equals(RegisterSuccessfulView.class)) {
        navigateToHome();
        finish();
      } else if (!currentFragment.getClass().equals(RegisterView.class)) {
        showRegisterView();
      } else {
        if (!mComesFromJoinUs) {
          navigateToHome();
        }
        finish();
      }
    } else {
      super.onBackPressed();
    }
  }

  @Override public void onRequestPermissionsResult(int requestCode, String[] permissions,
      int[] grantResults) {
    switch (requestCode) {
      case PermissionsHelper.ACCOUNTS_FACEBOOK_REQUEST_CODE:
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT,
              TrackerConstants.LABEL_ACCOUNT_ACCESS_ALLOW);
          mRegisterView.facebookRegister();
        } else {
          tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT,
              TrackerConstants.LABEL_ACCOUNT_ACCESS_DENY);
        }
        break;

      case PermissionsHelper.ACCOUNTS_GOOGLE_REQUEST_CODE:
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT,
              TrackerConstants.LABEL_ACCOUNT_ACCESS_ALLOW);
          mRegisterView.googlePlusRegister();
        } else {
          tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT,
              TrackerConstants.LABEL_ACCOUNT_ACCESS_DENY);
        }
        break;
    }
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }

  @Override public String getSSOTrackingCategory() {
    return TrackerConstants.CATEGORY_SSO_REGISTER;
  }
}