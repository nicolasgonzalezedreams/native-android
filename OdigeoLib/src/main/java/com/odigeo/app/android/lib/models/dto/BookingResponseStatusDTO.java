package com.odigeo.app.android.lib.models.dto;

public enum BookingResponseStatusDTO {

  BOOKING_ERROR, BOOKING_PAYMENT_RETRY, BOOKING_CONFIRMED, BOOKING_REPRICING, BROKEN_FLOW, USER_INTERACTION_NEEDED, BOOKING_STOP, ON_HOLD;

  public static BookingResponseStatusDTO fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
