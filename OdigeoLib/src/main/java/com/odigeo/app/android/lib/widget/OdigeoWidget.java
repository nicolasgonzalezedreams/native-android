package com.odigeo.app.android.lib.widget;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.DimenRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.TypedValue;
import android.widget.RemoteViews;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import com.odigeo.interactors.MyTripsInteractor;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 04/03/15
 */
public abstract class OdigeoWidget extends AppWidgetProvider {

  public static final String FIRST_UPDATE = "first_update";
  public static final String WIDGET_ACTIVATION_TRACKED = "widget_activation_tracked";
  public static final String GMT_TIMEZONE = "GMT";
  private OdigeoImageLoader mImageLoader;
  private Booking lastBooking;

  /**
   * This method generate the url of the destination image.
   *
   * @param iataCode of arrival city.
   * @return The url of the image.
   */
  @Nullable public static String getDestinationImageUrl(@NonNull String iataCode) {
    return Configuration.getInstance().getImagesSources().getUrlMyTrips() + iataCode + Configuration
        .getInstance()
        .getImagesSources()
        .getImageFileType();
  }

  /**
   * This method generate the image url of a MD flight .
   *
   * @param index fo image to download
   * @return The url of the image.
   */
  @Nullable public static String getMultiDestinationImageUrl(int index) {
    return Configuration.getInstance().getImagesSources().getUrlMyTrips() + String.format(
        Configuration.getInstance().getImagesSources().
            getMultidestinationFormat(), index) + Configuration.getInstance()
        .getImagesSources()
        .getImageFileType();
  }

  /**
   * In this method must returns the correct reference to the HomeActivity of each app.
   *
   * @return Class reference to the activity.
   */
  public abstract Class getHomeActivity();

  /**
   * In this method must returns the correct reference to the WidgetService of each app.
   *
   * @return Class reference to the service.
   */
  public abstract Class getWidgetServiceClass();

  /**
   * Called when its necessary to adjust the view to fit on the smallest widget size
   *
   * @param context Context to use
   * @param remoteViews Remote views that needs to be updated
   */
  @TargetApi(Build.VERSION_CODES.JELLY_BEAN) protected void adjustSmallBookingWidget(
      Context context, RemoteViews remoteViews) {
    updateViewParams(context, remoteViews, R.dimen.size_font_XXS, R.dimen.size_font_XL,
        R.dimen.size_font_XS, R.dimen.odigeo_widget_section_margin_small,
        R.dimen.odigeo_widget_footer_padding_vertical_small);
  }

  /**
   * Called when its necessary to adjust the view to fit on the normal widget size
   *
   * @param context Context to use
   * @param remoteViews Remote views that needs to be updated
   */
  @TargetApi(Build.VERSION_CODES.JELLY_BEAN) protected void adjustNormalBookingWidget(
      Context context, RemoteViews remoteViews) {
    updateViewParams(context, remoteViews, R.dimen.size_font_S, R.dimen.size_font_2XL,
        R.dimen.size_font_S, R.dimen.odigeo_widget_section_margin,
        R.dimen.odigeo_widget_footer_padding_vertical);
  }

  /**
   * Update view params for eDreams or Opodo booking layout
   *
   * @param context Context to get the resources
   * @param remoteViews RemoteViews to be updated
   * @param sizeFontLabel Font size resource for label fields
   * @param sizeFontIATA Font size resource for iata fields
   * @param sizeFontContent Font size resource for content fields
   * @param odigeoWidgetSectionMarginSmall Margin resource for sections
   * @param paddingVerticalSmall Margin size resource for the small paddings used in the layout
   */
  private void updateViewParams(Context context, RemoteViews remoteViews,
      @DimenRes int sizeFontLabel, @DimenRes int sizeFontIATA, @DimenRes int sizeFontContent,
      @DimenRes int odigeoWidgetSectionMarginSmall, @DimenRes int paddingVerticalSmall) {
    float labelSize = Util.getDimensionInDP(context, sizeFontLabel);
    float iataSize = Util.getDimensionInDP(context, sizeFontIATA);
    float contentSize = Util.getDimensionInDP(context, sizeFontContent);
    updateFontSize(remoteViews, labelSize, iataSize, contentSize);

    int sectionPadding = (int) Util.getDimensionInDP(context, odigeoWidgetSectionMarginSmall);
    int separatorPadding = (int) Util.getDimensionInDP(context, paddingVerticalSmall);
    updateSectionPadding(remoteViews, sectionPadding, separatorPadding);
  }

  /**
   * Update all the necessary remote views font size using the specified values
   *
   * @param remoteViews Remote view object to update
   * @param labelSize Size to be used for label fields
   * @param iataSize Size to be used for IATA fields
   * @param contentSize Size to be used for content fields fields
   */
  @TargetApi(Build.VERSION_CODES.JELLY_BEAN) private void updateFontSize(RemoteViews remoteViews,
      float labelSize, float iataSize, float contentSize) {
    remoteViews.setTextViewTextSize(R.id.lblDepartureHeader, TypedValue.COMPLEX_UNIT_SP, labelSize);
    remoteViews.setTextViewTextSize(R.id.lblDepartureLabel, TypedValue.COMPLEX_UNIT_SP, labelSize);
    remoteViews.setTextViewTextSize(R.id.lblArrivalHeader, TypedValue.COMPLEX_UNIT_SP, labelSize);
    remoteViews.setTextViewTextSize(R.id.lblArrivalLabel, TypedValue.COMPLEX_UNIT_SP, labelSize);
    remoteViews.setTextViewTextSize(R.id.lblDepartureIata, TypedValue.COMPLEX_UNIT_SP, iataSize);
    remoteViews.setTextViewTextSize(R.id.lblArrivalIata, TypedValue.COMPLEX_UNIT_SP, iataSize);
    remoteViews.setTextViewTextSize(R.id.lblDepartureDate, TypedValue.COMPLEX_UNIT_SP, contentSize);
    remoteViews.setTextViewTextSize(R.id.lblArrivalDate, TypedValue.COMPLEX_UNIT_SP, contentSize);
    remoteViews.setTextViewTextSize(R.id.lblDepartureTime, TypedValue.COMPLEX_UNIT_SP, contentSize);
    remoteViews.setTextViewTextSize(R.id.lblArrivalTime, TypedValue.COMPLEX_UNIT_SP, contentSize);
    remoteViews.setTextViewTextSize(R.id.lblFlightNumber, TypedValue.COMPLEX_UNIT_SP, labelSize);
  }

  /**
   * Update the paddings of the sections for eDreams and Opodo layouts
   *
   * @param remoteViews Remote views to update
   * @param sectionPadding Section padding to set between each content section
   * @param separatorPadding Padding to set to the footer line
   */
  @TargetApi(Build.VERSION_CODES.JELLY_BEAN) private void updateSectionPadding(
      RemoteViews remoteViews, int sectionPadding, int separatorPadding) {
    remoteViews.setViewPadding(R.id.lblDepartureHeader, 0, sectionPadding, 0, 0);
    remoteViews.setViewPadding(R.id.lblArrivalHeader, 0, sectionPadding, 0, 0);
    remoteViews.setViewPadding(R.id.lblDepartureLabel, 0, sectionPadding, 0, 0);
    remoteViews.setViewPadding(R.id.lblArrivalLabel, 0, sectionPadding, 0, 0);
    remoteViews.setViewPadding(R.id.lblDepartureTime, 0, 0, 0, separatorPadding);
    remoteViews.setViewPadding(R.id.lblArrivalTime, 0, 0, 0, separatorPadding);
  }

  @Override
  public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
    super.onUpdate(context, appWidgetManager, appWidgetIds);
    update(context, appWidgetManager, appWidgetIds);
  }

  @Override
  public void onAppWidgetOptionsChanged(Context context, AppWidgetManager appWidgetManager,
      int appWidgetId, Bundle newOptions) {
    super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions);
    //Rearrange widget layout if the sdk is at least 16
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
      if (getWidgetType(context) == WidgetType.BOOKING) {
        RemoteViews remoteViews =
            new RemoteViews(context.getPackageName(), R.layout.odigeo_booking_widget);
        //For small widget use small font size and padding
        if (hasMinSize(context, newOptions)) {
          adjustSmallBookingWidget(context, remoteViews);
        } else {
          //Return to normal values
          adjustNormalBookingWidget(context, remoteViews);
        }
        // Tell the AppWidgetManager to perform an update on the current app widget
        appWidgetManager.partiallyUpdateAppWidget(appWidgetId, remoteViews);
      }
    }
  }

  /**
   * Determine if the widget size is the same as the specified min size (2 squares)
   *
   * @param context Context to get the resources
   * @param newOptions Bundle options to get the widget size
   * @return TRUE if the widget size is the same as the specified min size, FALSE otherwise
   */
  @TargetApi(Build.VERSION_CODES.JELLY_BEAN) private boolean hasMinSize(Context context,
      Bundle newOptions) {
    //Get widget size in portrait
    int minHeight = newOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_HEIGHT);
    //Get the size in squares according the formula specified at
    // http://developer.android.com/guide/practices/ui_guidelines/widget_design.html
    int squareMin = (minHeight + 30) / 70;
    //Get specified default square size, using min dim and current display metrics density
    float def =
        context.getResources().getDimension(R.dimen.widget_rows_min) / context.getResources()
            .getDisplayMetrics().density;
    int squareDef = (int) Math.floor((def + 30) / 70);
    //if the current square size is less or equals to the specified min, rearrange widget layout
    return squareMin <= squareDef;
  }

  /**
   * Here would be checked and selected the correct widget to shown.
   *
   * @param context Context where be called the widget.
   * @param appWidgetManager For the manage of the widget.
   * @param appWidgetIds array of the id widget attached to the provider.
   */
  private void update(@NonNull Context context, @NonNull AppWidgetManager appWidgetManager,
      @IdRes int[] appWidgetIds) {
    mImageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();
    //Get the latest booking
    MyTripsInteractor interactor =
        AndroidDependencyInjector.getInstance().provideMyTripsInteractor();
    lastBooking = interactor.getNextBooking();
    //Change Locale.
    android.content.res.Configuration configuration = new android.content.res.Configuration();
    configuration.locale = Configuration.getCurrentLocale();
    Locale.setDefault(Configuration.getCurrentLocale());
    context.getResources()
        .updateConfiguration(configuration, context.getResources().getDisplayMetrics());
    // Check if it's the first update of the widget
    SharedPreferences preferences =
        context.getSharedPreferences(Constants.SHARED_PREFERENCES_ODIGEO, Activity.MODE_PRIVATE);
    if (!preferences.getBoolean(FIRST_UPDATE, false)) {
      // Trigger the hit of GA
      SharedPreferences.Editor editor = preferences.edit();
      editor.putBoolean(FIRST_UPDATE, true);
      editor.apply();
    }
    // Perform this loop procedure for each App Widget that belongs to this provider
    for (int appWidgetId : appWidgetIds) {
      RemoteViews remoteViews = getRemoteViews(context, appWidgetManager, appWidgetId);
      //If the widget is small, update font size and view paddings
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && hasMinSize(context,
          appWidgetManager.getAppWidgetOptions(appWidgetId))) {
        adjustSmallBookingWidget(context, remoteViews);
      }
      // Tell the AppWidgetManager to perform an update on the current app widget
      appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
    }
  }

  /**
   * According the search options get the widget type
   *
   * @param context Context to use
   * @return The widget type that should be used according the current search options
   */
  private WidgetType getWidgetType(Context context) {
    WidgetType widgetType = WidgetType.BOOKING;
    if (lastBooking == null) {
      //Check if has previous flights search.
      if (SearchWidgetProvider.populateList(context).isEmpty()) {
        widgetType = WidgetType.EMPTY;
      } else {
        widgetType = WidgetType.SEARCH;
      }
    }
    return widgetType;
  }

  /**
   * Depending on the search options select the corresponding widget layout and get its remote views
   *
   * @param context context to use for inflating and getting resources
   * @param appWidgetManager Appwidget manage used when creating the remote views
   * @param appWidgetId Id of the widget to update
   * @return Remote views used to create the widget layout
   */
  private RemoteViews getRemoteViews(@NonNull Context context,
      @NonNull AppWidgetManager appWidgetManager, int appWidgetId) {
    RemoteViews remoteViews;
    WidgetType widgetType = getWidgetType(context);
    //Check if hasn't search history or a booking.
    if (widgetType == WidgetType.BOOKING) {
      remoteViews = createBookingWidget(context, appWidgetId);
    } else if (widgetType == WidgetType.SEARCH) {
      remoteViews = createSearchWidget(context, appWidgetManager, appWidgetId);
    } else {
      remoteViews = createEmptyWidget(context);
    }
    return remoteViews;
  }

  /**
   * Here gonna be inflated, linked and set the actions and views to the case when the user don't
   * have previous search or a booking.
   *
   * @param context Context where be called the widget.
   * @return The remote views created.
   */
  private RemoteViews createEmptyWidget(@NonNull Context context) {
    // Create an Intent to launch SplashActivity
    Intent intent = new Intent(context, getHomeActivity());
    intent.putExtra(Constants.EXTRA_WIDGET_EMPTY, true);
    PendingIntent pendingIntent =
        PendingIntent.getActivity(context, WidgetType.EMPTY.ordinal(), intent,
            PendingIntent.FLAG_UPDATE_CURRENT);
    RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.odigeo_empty_widget);
    views.setTextViewText(R.id.lblBrand, Configuration.getInstance().getBrand());
    views.setTextViewText(R.id.lblEmpty,
        LocalizablesFacade.getString(context, OneCMSKeys.ODGWIDGETVIRGINSIMPLEVIEW_TITLE)
            .toString());
    views.setTextViewText(R.id.btnSearchNow,
        LocalizablesFacade.getString(context, OneCMSKeys.ODGWIDGETVIRGINSIMPLEVIEW_SEARCH)
            .toString());
    views.setOnClickPendingIntent(R.id.btnSearchNow, pendingIntent);
    return views;
  }

  /**
   * Here gonna be inflated, linked and set the actions and views to the case when the user have a
   * flight booked.
   *
   * @param context Context where be called the widget.
   * @return The remote views created.
   */
  private RemoteViews createBookingWidget(@NonNull Context context, int appWidgetId) {
    final RemoteViews views =
        new RemoteViews(context.getPackageName(), R.layout.odigeo_booking_widget);
    Date currentDate = new Date();
    SimpleDateFormat dateFormat =
        new SimpleDateFormat(context.getString(R.string.widget_date_format),
            LocaleUtils.getCurrentLocale());
    dateFormat.setTimeZone(TimeZone.getTimeZone(GMT_TIMEZONE));
    views.setTextViewText(R.id.lblBrand, Configuration.getInstance().getBrand());
    for (Segment segment : lastBooking.getSegments()) {
      if (segment.getFirstSection().getDepartureDate() >= currentDate.getTime()) {
        setBookingScales(segment, views);
        LocationBooking locationFrom = segment.getFirstSection().getFrom();
        LocationBooking locationTo = segment.getLastSection().getTo();
        views.setTextViewText(R.id.lblDepartureIata, locationFrom.getCityIATACode());
        views.setTextViewText(R.id.lblDeparture, Html.fromHtml(locationFrom.getCityName()));
        views.setTextViewText(R.id.lblDepartureDate,
            dateFormat.format(OdigeoDateUtils.createDate(segment.getDepartureDate())));
        views.setTextViewText(R.id.lblDepartureTime,
            getTimePartFormatted(OdigeoDateUtils.createDate(segment.getDepartureDate())));
        views.setTextViewText(R.id.lblArrivalIata, locationTo.getCityIATACode());
        views.setTextViewText(R.id.lblArrival, Html.fromHtml(locationTo.getCityName()));
        views.setTextViewText(R.id.lblArrivalDate,
            dateFormat.format(OdigeoDateUtils.createDate(segment.getArrivalDate())));
        views.setTextViewText(R.id.lblArrivalTime,
            getTimePartFormatted(OdigeoDateUtils.createDate(segment.getArrivalDate())));
        String flightId = LocalizablesFacade.getString(context, OneCMSKeys.WIDGET_FLIGHT_NUMBER)
            + segment.getFirstSection().getCarrier().getCode().toUpperCase()
            + segment.getFirstSection().getId();
        views.setTextViewText(R.id.lblFlightNumber, flightId);
        views.setTextViewText(R.id.lblDepartureLabel,
            LocalizablesFacade.getString(context, OneCMSKeys.EVENTCALENDAR_TAKEOFF));
        views.setTextViewText(R.id.lblArrivalLabel,
            LocalizablesFacade.getString(context, OneCMSKeys.EVENTCALENDAR_LANDINGAT));
        views.setTextViewText(R.id.lblDepartureHeader,
            LocalizablesFacade.getString(context, OneCMSKeys.MYTRIPSVIEWCONTROLLER_DEPARTURE));
        views.setTextViewText(R.id.lblArrivalHeader,
            LocalizablesFacade.getString(context, OneCMSKeys.EVENTCALENDAR_ARRIVAL));
        String urlImage;
        if (lastBooking.getTripType().equals(Booking.TRIP_TYPE_MULTI_SEGMENT)) {
          urlImage = getMultiDestinationImageUrl(lastBooking.getMultiDestinyIndex());
        } else {
          urlImage = getDestinationImageUrl(locationTo.getCityIATACode());
        }

        int width = context.getResources().getDimensionPixelSize(R.dimen.widget_columns);
        int height = context.getResources().getDimensionPixelSize(R.dimen.widget_rows);
        mImageLoader.loadWidgetImage(views, urlImage, R.id.imgDestination, width, height,
            appWidgetId, R.drawable.trips_loading_placeholder);

        break;
      }
    }
    Intent intent = new Intent(context, getHomeActivity());
    intent.putExtra(Constants.EXTRA_ACTION_GOTO_MYTRIPS, true);
    intent.putExtra(Constants.EXTRA_BOOKING, lastBooking);
    PendingIntent pendingIntent =
        PendingIntent.getActivity(context, WidgetType.BOOKING.ordinal(), intent,
            PendingIntent.FLAG_UPDATE_CURRENT);
    views.setOnClickPendingIntent(R.id.lytWidget, pendingIntent);
    return views;
  }

  /**
   * Create the third scenario for the widget.
   *
   * @param context Context where be called the widget.
   * @param appWidgetId Id of the actual widget.
   * @return The remote views created.
   */
  public RemoteViews createSearchWidget(@NonNull Context context,
      @NonNull AppWidgetManager appWidgetManager, int appWidgetId) {
    RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.odigeo_search_widget);
    views.setTextViewText(R.id.lblBrand, Configuration.getInstance().getBrand());
    views.setTextViewText(R.id.lblHeaderList,
        LocalizablesFacade.getString(context, OneCMSKeys.LATESTSEARCHES));
    views.setTextViewText(R.id.btnDiscover,
        LocalizablesFacade.getString(context, OneCMSKeys.DISCOVER));
    Intent intent = new Intent(context, getWidgetServiceClass());
    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
    views.setRemoteAdapter(R.id.listSearch, intent);

    Intent intentApp = new Intent(context, getHomeActivity());
    intentApp.putExtra(Constants.EXTRA_WIDGET_SEARCH, true);
    PendingIntent pendingIntent =
        PendingIntent.getActivity(context, WidgetType.EMPTY.ordinal(), intentApp,
            PendingIntent.FLAG_UPDATE_CURRENT);
    views.setOnClickPendingIntent(R.id.btnDiscover, pendingIntent);

    //Template intent for the list items click
    Intent intentList = new Intent(context, getHomeActivity());
    PendingIntent pendingIntentList =
        PendingIntent.getActivity(context, WidgetType.SEARCH.ordinal(), intentList,
            PendingIntent.FLAG_UPDATE_CURRENT);
    views.setPendingIntentTemplate(R.id.listSearch, pendingIntentList);
    //To refresh the list
    appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.listSearch);
    return views;
  }

  private String getTimePartFormatted(Date date) {
    if (date == null) {
      return "";
    }
    //TODO: use format from class LocaleUtils
    return OdigeoDateUtils.getGmtDateFormat(
        Configuration.getInstance().getCurrentMarket().getTimeFormat()).format(date);
  }

  /**
   * Sets the scales or flight icon for the flight segment.
   *
   * @param remoteViews reference to view.
   */
  protected abstract void setBookingScales(Segment segment, RemoteViews remoteViews);

  private enum WidgetType {
    SEARCH, EMPTY, BOOKING
  }
}
