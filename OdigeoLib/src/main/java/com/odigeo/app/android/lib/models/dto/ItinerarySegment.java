package com.odigeo.app.android.lib.models.dto;

import java.util.List;

/**
 * @author Irving
 * @version 1.0
 * @since 18/03/2015.
 */
public class ItinerarySegment {

  private List<SectionImportTrip> sections;
  private long duration;

  public final List<SectionImportTrip> getSections() {

    return sections;
  }

  public final void setSections(List<SectionImportTrip> sections) {
    this.sections = sections;
  }

  public long getDuration() {
    return duration;
  }

  public void setDuration(long duration) {
    this.duration = duration;
  }
}
