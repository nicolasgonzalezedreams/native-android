package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

/**
 * <p>Java class for InstallmentInformationDescription complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="InstallmentInformationDescription">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="installmentsNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}int"
 * />
 *       &lt;attribute name="firstInstallmentPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"
 * />
 *       &lt;attribute name="installmentPriceExceptFirst" type="{http://www.w3.org/2001/XMLSchema}decimal"
 * />
 *       &lt;attribute name="commissionPercent" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class InstallmentInformationDescriptionDTO {

  protected int installmentsNumber;
  protected BigDecimal firstInstallmentPrice;
  protected BigDecimal installmentPriceExceptFirst;
  protected BigDecimal commissionPercent;

  /**
   * Gets the value of the installmentsNumber property.
   */
  public int getInstallmentsNumber() {
    return installmentsNumber;
  }

  /**
   * Sets the value of the installmentsNumber property.
   */
  public void setInstallmentsNumber(int value) {
    this.installmentsNumber = value;
  }

  /**
   * Gets the value of the firstInstallmentPrice property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getFirstInstallmentPrice() {
    return firstInstallmentPrice;
  }

  /**
   * Sets the value of the firstInstallmentPrice property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setFirstInstallmentPrice(BigDecimal value) {
    this.firstInstallmentPrice = value;
  }

  /**
   * Gets the value of the installmentPriceExceptFirst property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getInstallmentPriceExceptFirst() {
    return installmentPriceExceptFirst;
  }

  /**
   * Sets the value of the installmentPriceExceptFirst property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setInstallmentPriceExceptFirst(BigDecimal value) {
    this.installmentPriceExceptFirst = value;
  }

  /**
   * Gets the value of the commissionPercent property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getCommissionPercent() {
    return commissionPercent;
  }

  /**
   * Sets the value of the commissionPercent property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setCommissionPercent(BigDecimal value) {
    this.commissionPercent = value;
  }
}
