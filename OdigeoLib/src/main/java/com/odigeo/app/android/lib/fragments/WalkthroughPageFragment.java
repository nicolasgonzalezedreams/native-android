package com.odigeo.app.android.lib.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.models.DefaultWalkthroughPage;
import com.odigeo.app.android.lib.models.WalkthroughPage;
import com.odigeo.app.android.lib.models.WalkthroughPageStatic;
import com.odigeo.app.android.lib.models.WalkthroughPageWS;
import com.odigeo.app.android.lib.ui.widgets.CustomTextView;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;

/**
 * Fragment that represents a page on the walkthrough, this fragment should include a header image
 * and text content
 *
 * @author M. en C. Javier Silva Perez
 * @version 1.0
 * @since 03/05/2015
 */
public class WalkthroughPageFragment extends Fragment {

  WalkthroughPage walkthroughPage;

  private OdigeoImageLoader<ImageView> mImageLoader;

  /**
   * Creates a new instance of this fragment using
   *
   * @param walkthroughPage Object with the values to show in this page
   * @return A new instance of {@link com.odigeo.app.android.lib.fragments.WalkthroughPageFragment}
   */
  public static WalkthroughPageFragment newInstance(WalkthroughPage walkthroughPage) {
    WalkthroughPageFragment fragment = new WalkthroughPageFragment();
    fragment.setWalkthroughPage(walkthroughPage);
    return fragment;
  }

  public void setWalkthroughPage(WalkthroughPage walkthroughPage) {
    this.walkthroughPage = walkthroughPage;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //Preserves fragment instance
    setRetainInstance(true);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View root = null;

    mImageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();

    //Default walkthrough
    if (walkthroughPage != null && walkthroughPage instanceof DefaultWalkthroughPage) {
      root = inflater.inflate(R.layout.fragment_walkthrough_default, container, false);
    } else if (walkthroughPage != null) {
      root = inflater.inflate(R.layout.fragment_walkthrough_page, container, false);
      ImageView imgWalkthrough = (ImageView) root.findViewById(R.id.img_walkthrough);
      CustomTextView txtHeader = (CustomTextView) root.findViewById(R.id.txt_walkthrough_header);
      CustomTextView txtContent = (CustomTextView) root.findViewById(R.id.txt_walkthrough_content);
      if (walkthroughPage instanceof WalkthroughPageWS) {
        WalkthroughPageWS wsPage = (WalkthroughPageWS) walkthroughPage;
        mImageLoader.load(imgWalkthrough, wsPage.getImageUrl());
      } else {
        WalkthroughPageStatic staticPage = (WalkthroughPageStatic) walkthroughPage;
        imgWalkthrough.setImageResource(staticPage.getImageRes());
      }
      txtHeader.setText(walkthroughPage.getTitle());
      txtContent.setText(walkthroughPage.getDescription());
    }

    return root;
  }
}
