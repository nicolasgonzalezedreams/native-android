package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for HidingItinerary complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="HidingItinerary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="itinerary" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class HidingItineraryDTO {

  protected Integer id;
  protected Integer itinerary;

  /**
   * Gets the value of the id property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setId(Integer value) {
    this.id = value;
  }

  /**
   * Gets the value of the itinerary property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getItinerary() {
    return itinerary;
  }

  /**
   * Sets the value of the itinerary property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setItinerary(Integer value) {
    this.itinerary = value;
  }
}
