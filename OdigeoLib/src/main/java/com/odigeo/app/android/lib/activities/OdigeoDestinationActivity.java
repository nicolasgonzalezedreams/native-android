package com.odigeo.app.android.lib.activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.globant.roboneck.common.NeckSpiceManager;
import com.globant.roboneck.requests.BaseNeckRequestException;
import com.globant.roboneck.requests.BaseNeckRequestListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.localytics.android.Localytics;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.odigeo.DependencyInjector;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.DestinationAdapter;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.interfaces.OdigeoInterfaces.ItemListDestination;
import com.odigeo.app.android.lib.interfaces.SoftKeyboardListener;
import com.odigeo.app.android.lib.models.CityItemListDestination;
import com.odigeo.app.android.lib.models.HeaderItemListDestination;
import com.odigeo.app.android.lib.neck.requests.DestinationSearchRequest;
import com.odigeo.app.android.lib.ui.widgets.DestinationErrorWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.helpers.PermissionsHelper;
import com.odigeo.data.entity.FlightsDirection;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.entity.geo.LocationDescriptionType;
import com.odigeo.dataodigeo.location.LocationController;
import com.odigeo.dataodigeo.location.LocationControllerListener;
import com.odigeo.dataodigeo.location.TemporalDestination;
import com.odigeo.dataodigeo.net.helper.semaphore.UIAutomationSemaphore;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Irving López on 01/09/2014.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.2
 * @since 05/02/2015
 */
public abstract class OdigeoDestinationActivity extends OdigeoBackgroundAnimatedActivity
    implements SearchView.OnQueryTextListener, View.OnClickListener, SoftKeyboardListener,
    LocationControllerListener {

  private static final int SAVED_CITIES_MAX = 11;
  private static final int QUERY_MIN_CHARACTERS = 3;
  private static List<City> lstCitiesFound = null;
  protected NeckSpiceManager spiceManager = new NeckSpiceManager();
  private LinearLayout buttonGeoLocation;
  private DestinationsViewTypes currentView = DestinationsViewTypes.RECENT;
  private FlightsDirection flightDirection;
  private int noSegment;
  private String query;
  private DestinationErrorWidget widgetErrorDestination;
  private boolean isSearching;
  private DestinationAdapter adapter;
  private LocationController locationController;

  @Override public final void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    locationController = AndroidDependencyInjector.getInstance().provideLocationController();
    setContentView(R.layout.activity_destination);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    widgetErrorDestination =
        (DestinationErrorWidget) this.findViewById(R.id.widgetErrorDestination);
    flightDirection =
        (FlightsDirection) this.getIntent().getSerializableExtra(Constants.EXTRA_FLIGHT_DIRECTION);
    noSegment = this.getIntent().getIntExtra(Constants.EXTRA_FLIGHT_SEGMENT_NUMBER, 0);
    buttonGeoLocation = (LinearLayout) findViewById(R.id.layout_geo_location);
    TextView titleHeaderGeoLocation = (TextView) findViewById(R.id.header_geo_list_destination);
    ListView listViewLocation = (ListView) findViewById(R.id.listView_destinationActivity);
    adapter = new DestinationAdapter(this, new ArrayList<ItemListDestination>());
    listViewLocation.setAdapter(adapter);
    titleHeaderGeoLocation.setTypeface(Configuration.getInstance().getFonts().getBold());
    buttonGeoLocation.setOnClickListener(this);
    listViewLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (adapter.getItemViewType(position) != DestinationAdapter.HEADER) {
          City city = (City) adapter.getItem(position);
          savedCityInPreference(city, getFilePreferences(flightDirection));
          if (flightDirection.equals(
              FlightsDirection.DEPARTURE)) { //Check and launching of the GA events.
            triggerDepartureGAEvents(city);
          } else if (flightDirection.equals(FlightsDirection.RETURN)) {
            triggerReturnGAEvents(city);
          }
          Intent returnIntent = new Intent(); //Setup the intent with the return data.
          returnIntent.putExtra(Constants.EXTRA_FLIGHT_DIRECTION, flightDirection);
          returnIntent.putExtra(Constants.EXTRA_FLIGHT_SEGMENT_NUMBER, noSegment);
          returnIntent.putExtra(Constants.EXTRA_DESTINATION, city);
          setResult(RESULT_OK, returnIntent);
          finish();
        }
      }
    });
    //// show the history Cities
    showCitiesStored(flightDirection);
  }

  /**
   * Method to wrap the launch of the Departure GA events.
   *
   * @param city City where be extracted the data.
   */
  private void triggerDepartureGAEvents(City city) {
    if (currentView == DestinationsViewTypes.SEARCH) {
      if (city.getType() == LocationDescriptionType.CITY) {
        postGATrackingEventAutocompleteDepartureClosestCities();
      } else {
        postGATrackingEventAutocompleteDepartureClosestAirports();
      }
    } else if (currentView == DestinationsViewTypes.RECENT) {
      postGATrackingEventAutocompleteDepartureRecent();
    } else if (currentView == DestinationsViewTypes.GEOLOCATION) {
      postGATrackingEventAutocompleteDepartureGeolocated();
    }
  }

  /**
   * Method to wrap the launch of the Return GA events.
   *
   * @param city City where be extracted the data.
   */
  private void triggerReturnGAEvents(City city) {
    if (currentView == DestinationsViewTypes.SEARCH) {
      if (city.getType() == LocationDescriptionType.CITY) {
        postGATrackingEventAutocompleteArrivalClosestCities();
      } else {
        postGATrackingEventAutocompleteArrivalClosestAirports();
      }
    } else if (currentView == DestinationsViewTypes.RECENT) {
      postGATrackingEventAutocompleteArrivalRecent();
    } else if (currentView == DestinationsViewTypes.GEOLOCATION) {
      postGATrackingEventAutocompleteArrivalGeolocated();
    }
  }

  private void showCitiesStored(FlightsDirection direction) {
    List<City> historyCities = getListCitiesStored(getFilePreferences(direction));
    if (historyCities != null) {
      setResults(historyCities, true, direction);
    }
  }

  private boolean savedCityInPreference(City city, String fileNamePreferences) {
    SharedPreferences preferences = getSharedPreferences(fileNamePreferences, MODE_PRIVATE);
    List<String> listCities;
    String cityString = new Gson().toJson(city);
    if (preferences.contains(Constants.PREFERENCE_LIST_CITIES)) {
      listCities = getCitiesStringStored(preferences);
      if (listCities != null && listCities.contains(cityString)) {
        listCities.remove(cityString);
      }
    } else {
      listCities = new ArrayList<>();
    }
    if (listCities != null) {
      listCities.add(0, cityString);
      if (listCities.size() == SAVED_CITIES_MAX) {
        listCities.remove(SAVED_CITIES_MAX - 1);
      }
      return savedCities(listCities, preferences);
    }
    return false;
  }

  private boolean savedCities(List<String> cities, SharedPreferences preferences) {
    SharedPreferences.Editor editor = preferences.edit();
    editor.putString(Constants.PREFERENCE_LIST_CITIES, new Gson().toJson(cities));
    return editor.commit();
  }

  private List<String> getCitiesStringStored(SharedPreferences preferences) {
    String stringArrayList = preferences.getString(Constants.PREFERENCE_LIST_CITIES, null);
    TypeToken typeToken = new TypeToken<ArrayList<String>>() {
    };
    Type typeList = typeToken.getType();
    return new Gson().fromJson(stringArrayList, typeList);
  }

  private List<City> getListCitiesStored(String fileNamePreferences) {
    SharedPreferences preferences = getSharedPreferences(fileNamePreferences, MODE_PRIVATE);
    if (preferences.contains(Constants.PREFERENCE_LIST_CITIES)) {
      List<String> citiesStrings = getCitiesStringStored(preferences);
      if (citiesStrings != null) {
        ArrayList<City> listCities = new ArrayList<>();
        Gson gson = new Gson();
        for (String stringCity : citiesStrings) {
          City cityStored = gson.fromJson(stringCity, City.class);
          if (!listCities.contains(cityStored)) {
            listCities.add(cityStored);
          }
        }
        return listCities;
      }
    }
    return null;
  }

  @Override public final void onStart() {
    super.onStart();
    spiceManager.start(this);
  }

  @Override public final String getActivityTitle() {
    return null;
  }

  @Override public final void onStop() {
    super.onStop();
    spiceManager.shouldStop();
  }

  @Override protected final void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
    Localytics.onNewIntent(this, intent);
  }

  //Sets the Cities List corresponding to the results list
  public final void setResults(List<City> citiesFiltered) {
    setResults(citiesFiltered, false, null);
  }

  public final void setResults(List<City> citiesFiltered, boolean showPreviousSearches,
      FlightsDirection direction) {
    if (citiesFiltered == null || citiesFiltered.isEmpty()) {
      //Show error widget
      showNotFoundRecordsErrorMessage();
    } else {
      //Disable error widget
      widgetErrorDestination.setVisibility(View.GONE);
      List<ItemListDestination> lstDestinationsItems = new ArrayList<>();
      if (showPreviousSearches) {
        String recentHeaders;
        if (direction == FlightsDirection.DEPARTURE) {
          recentHeaders =
              LocalizablesFacade.getString(this, OneCMSKeys.LOCATIONS_SEARCH_BAR_RECENT_ORIGINS)
                  .toString();
        } else {
          recentHeaders = LocalizablesFacade.getString(this,
              OneCMSKeys.LOCATIONS_SEARCH_BAR_RECENT_DESTINATIONS).toString();
        }
        lstDestinationsItems.add(
            new HeaderItemListDestination(R.drawable.search_buildings, recentHeaders));
        for (City city : citiesFiltered) {
          lstDestinationsItems.add(CityItemListDestination.fromCity(city, query));
        }
      } else {
        lstDestinationsItems = CityItemListDestination.expandList(citiesFiltered, query);
      }
      if (currentView == DestinationsViewTypes.SEARCH
          || currentView == DestinationsViewTypes.RECENT) {
        adapter.setListDestinations(lstDestinationsItems);
        adapter.setQuery(query);
      } else {
        adapter.setListDestinations(separateAirportsAndCities(lstDestinationsItems));
        adapter.setQuery(null);
      }
      adapter.notifyDataSetChanged();
    }
  }

  private String getFilePreferences(FlightsDirection direction) {
    if (direction == FlightsDirection.DEPARTURE) {
      return Constants.SHARED_PREFERENCE_FILE_DEPARTURE_CITIES;
    } else {
      return Constants.SHARED_PREFERENCE_FILE_RETOUR_CITIES;
    }
  }

  private List<ItemListDestination> separateAirportsAndCities(
      List<ItemListDestination> lstDestinationsItems) {
    List<ItemListDestination> lstAirports = new ArrayList<>();
    List<ItemListDestination> lstCities = new ArrayList<>();
    for (ItemListDestination itemDestination : lstDestinationsItems) {
      if (((CityItemListDestination) itemDestination).getType()
          .equals(LocationDescriptionType.CITY)) {
        lstCities.add(itemDestination);
      } else {
        lstAirports.add(itemDestination);
      }
    }
    ArrayList<ItemListDestination> lstNewDestinationsItems = new ArrayList<>();
    if (!lstAirports.isEmpty()) {
      lstNewDestinationsItems.add(new HeaderItemListDestination(R.drawable.search_plane,
          LocalizablesFacade.getString(this,
              "locationsviewcontroller_searchbar_closestairports_placeholder").toString()));
      for (ItemListDestination item : lstAirports) {
        lstNewDestinationsItems.add(item);
      }
    }
    if (!lstCities.isEmpty()) {
      lstNewDestinationsItems.add(new HeaderItemListDestination(R.drawable.search_buildings,
          LocalizablesFacade.getString(this,
              "locationsviewcontroller_searchbar_closestcities_placeholder").toString()));
      for (ItemListDestination item : lstCities) {
        lstNewDestinationsItems.add(item);
      }
    }
    return lstNewDestinationsItems;
  }

  /**
   * Filter the places obtained by the rest service, those who start with the query given
   *
   * @param query Query to search places
   * @return Return a Cities List of those cities that apply the criteria
   */
  private List<City> filterFoundPlaces(String query) {
    List<City> listFiltered = new ArrayList<>();
    if (lstCitiesFound != null) {
      Log.i(Constants.TAG_LOG, "Filtering rows " + lstCitiesFound.size());
    } else {
      Log.i(Constants.TAG_LOG, "Filtering null");
    }
    for (City city : lstCitiesFound) {
      if (city.getType() == LocationDescriptionType.IATA_CODE) {
        if (city.getIataCode()
            .toLowerCase(LocaleUtils.getCurrentLocale())
            .startsWith(query.toLowerCase(LocaleUtils.getCurrentLocale()))) {
          listFiltered.add(city);
        }
      } else if (city.getName()
          .toLowerCase(LocaleUtils.getCurrentLocale())
          .startsWith(query.toLowerCase(LocaleUtils.getCurrentLocale())) || city.getCityName()
          .toLowerCase(LocaleUtils.getCurrentLocale())
          .startsWith(query.toLowerCase(LocaleUtils.getCurrentLocale())) || containsLocation(city,
          query)) {
        listFiltered.add(city);
      }
    }
    return listFiltered;
  }

  private boolean containsLocation(City city, String query) {
    for (String location : city.getLocationNames()) {
      if (location.toLowerCase(LocaleUtils.getCurrentLocale())
          .startsWith(query.toLowerCase(LocaleUtils.getCurrentLocale()))) {
        return true;
      }
    }
    return false;
  }

  @Override public final void onSoftKeyboardShown(boolean isShowing) {
    if (isShowing) {
      InputMethodManager inputMethodManager =
          (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }
  }

  @Override public final void onClick(View v) {
    if (v.getId() == buttonGeoLocation.getId()) {

      boolean isPermissionAllowed =
          PermissionsHelper.askForPermissionIfNeeded(Manifest.permission.ACCESS_FINE_LOCATION, this,
              OneCMSKeys.PERMISSION_LOCATION_AIRPORT_MESSAGE,
              PermissionsHelper.LOCATION_REQUEST_CODE);

      if (isPermissionAllowed) {
        fetchNearestLocations(v);
      }
    }
  }

  private void fetchNearestLocations(View v) {
    List<City> nearestPlacesList = locationController.getNearestPlacesList();
    if (nearestPlacesList != null && !nearestPlacesList.isEmpty()) {
      ViewUtils.hideKeyboard(getApplicationContext(), v);
      currentView = DestinationsViewTypes.GEOLOCATION;
      BusProvider.getInstance()
          .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHT_SEARCH,
              GAnalyticsNames.ACTION_SEARCHER_FLIGHTS, GAnalyticsNames.LABEL_GEOLOCATION_CLICKS));
      setResults(nearestPlacesList);
    } else {
      locationController.subscribe(this);
      locationController.getNearestCitiesRequest();
      showNotFoundRecordsErrorMessage();
    }
  }

  @Override public void onRequestPermissionsResult(int requestCode, String[] permissions,
      int[] grantResults) {
    switch (requestCode) {
      case PermissionsHelper.LOCATION_REQUEST_CODE:
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT,
              TrackerConstants.LABEL_LOCATION_ACCESS_ALLOW);
          currentView = DestinationsViewTypes.GEOLOCATION;
          BusProvider.getInstance()
              .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHT_SEARCH,
                  GAnalyticsNames.ACTION_SEARCHER_FLIGHTS,
                  GAnalyticsNames.LABEL_GEOLOCATION_CLICKS));
          setResults(locationController.getNearestPlacesList());
        } else {
          mTracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_APP_PERMISSIONS,
              TrackerConstants.ACTION_PERMISSIONS_ALERT,
              TrackerConstants.LABEL_LOCATION_ACCESS_DENY);
        }
    }
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }

  @Override public final boolean onQueryTextSubmit(String query) {
    return false;
  }

  @Override public final boolean onQueryTextChange(String query) {
    Log.i(Constants.TAG_LOG, "Querying " + query);
    this.query = query;
    currentView = DestinationsViewTypes.SEARCH;
    widgetErrorDestination.setVisibility(View.GONE);

    if ((query.trim().length() >= LocaleUtils.getLocalizedSearchLength()
        && (!Configuration.getCurrentLocale().getLanguage().equals("ja"))) || (((query.trim()
        .length() >= LocaleUtils.getLocalizedSearchLength() && (query.trim().length()
        >= QUERY_MIN_CHARACTERS) || (query.trim().length() >= LocaleUtils.getLocalizedSearchLength()
        && query.trim().length() <= QUERY_MIN_CHARACTERS
        && LocaleUtils.isJapaneseCharacter(query))) && Configuration.getCurrentLocale()
        .getLanguage()
        .equals("ja")))) {

      checkDestinationToJapanese();
    } else {

      checkDestination();
    }

    return true;
  }

  /**
   * Check destination
   */
  private void checkDestination() {

    Log.i(Constants.TAG_LOG, "lstCitiesFound is null");
    currentView = DestinationsViewTypes.RECENT;
    if (lstCitiesFound != null) {
      lstCitiesFound.clear();
    }
    adapter.setListDestinations(new ArrayList<ItemListDestination>());
    adapter.setQuery(null);
    adapter.notifyDataSetChanged();
    showCitiesStored(flightDirection);
  }

  /**
   * Check destination to JA
   */
  private void checkDestinationToJapanese() {

    if (lstCitiesFound == null || lstCitiesFound.isEmpty()) {
      Log.i(Constants.TAG_LOG, "DOING SEARCH");
      doSearch();
    } else {
      Log.i(Constants.TAG_LOG, "ENTERING TO setResults");
      setResults(filterFoundPlaces(query));
    }
  }

  private synchronized void doSearch() {
    if (!isSearching) {

      isSearching = true;

      DependencyInjector dependencyInjector = AndroidDependencyInjector.getInstance();
      DestinationSearchRequest requestLocation =
          new DestinationSearchRequest(Constants.FLIGHTS_PRODUCT,
              Configuration.getInstance().getCurrentMarket().getLocale(),
              Configuration.getInstance().getCurrentMarket().getWebsite(), "DEPARTURE", query,
              dependencyInjector.provideDomainHelper(), dependencyInjector.provideHeaderHelper());

      Log.d(this.toString(), "setIdlestate up " + query);
      UIAutomationSemaphore.getInstance().idleUp();
      this.spiceManager.executeCacheRequest(requestLocation, new ResponseLocationListener());
    }
  }

  @Override public final boolean onOptionsItemSelected(MenuItem item) {

    if (item.getItemId() == android.R.id.home) {
      // GATracker - Choose origin/destination - event 1
      BusProvider.getInstance()
          .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHT_SEARCH,
              GAnalyticsNames.ACTION_SEARCHER_FLIGHTS, GAnalyticsNames.LABEL_CANCEL_INPUT));
      this.onBackPressed();
    }

    return super.onOptionsItemSelected(item);
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_destinations_activity, menu);
    MenuItem searchItem = menu.findItem(R.id.btn_search_destination);
    SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
    searchView.setIconifiedByDefault(false);
    searchView.setIconified(false);
    searchView.setOnQueryTextListener(this);
    return true;
  }

  public final void showErrorMessage(String title, String description) {
    Log.i(Constants.TAG_LOG, "Removing footer");
    ////Remove footer if there exist
    adapter.setListDestinations(new ArrayList<ItemListDestination>());
    adapter.setQuery(null);
    adapter.notifyDataSetChanged();
    widgetErrorDestination.setTitle(title);
    widgetErrorDestination.setDescription(description);
    widgetErrorDestination.setVisibility(View.VISIBLE);
    Log.i(Constants.TAG_LOG, "Widget has been showed");
  }

  /**
   * Shows an error message when there are not records matching with the search criteria
   */
  public final void showNotFoundRecordsErrorMessage() {
    Log.i(Constants.TAG_LOG, "Showing not found records");
    showErrorMessage(
        LocalizablesFacade.getString(this, "locationsviewcontroller_noresultstitle_text")
            .toString(),
        LocalizablesFacade.getString(this, "locationsviewcontroller_noresultssubtitle_text")
            .toString());
  }

  /**
   * Shows an error message when there are no internet connection to obtain the search results
   */
  public final void showNotInternetConnectionErrorMessage() {
    Log.i(Constants.TAG_LOG, "Showing not internet connection");
    if (lstCitiesFound != null) {
      lstCitiesFound.clear();
    }
    showErrorMessage(
        LocalizablesFacade.getString(this, "noconnectioninformationview_title").toString(),
        LocalizablesFacade.getString(this, "noconnectioninformationview_subtitle").toString());
  }

  private void postGATrackingEventAutocompleteArrivalGeolocated() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHT_SEARCH,
            GAnalyticsNames.ACTION_SEARCHER_FLIGHTS,
            GAnalyticsNames.LABEL_AUTOCOMPLETE_ARRIVAL_GEOLOCATED));
  }

  private void postGATrackingEventAutocompleteArrivalRecent() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHT_SEARCH,
            GAnalyticsNames.ACTION_SEARCHER_FLIGHTS,
            GAnalyticsNames.LABEL_AUTOCOMPLETE_ARRIVAL_RECENT));
  }

  private void postGATrackingEventAutocompleteArrivalClosestAirports() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHT_SEARCH,
            GAnalyticsNames.ACTION_SEARCHER_FLIGHTS,
            GAnalyticsNames.LABEL_AUTOCOMPLETE_ARRIVAL_CLOSEST_AIRPORTS));
  }

  private void postGATrackingEventAutocompleteArrivalClosestCities() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHT_SEARCH,
            GAnalyticsNames.ACTION_SEARCHER_FLIGHTS,
            GAnalyticsNames.LABEL_AUTOCOMPLETE_ARRIVAL_CLOSEST_CITIES));
  }

  private void postGATrackingEventAutocompleteDepartureGeolocated() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHT_SEARCH,
            GAnalyticsNames.ACTION_SEARCHER_FLIGHTS,
            GAnalyticsNames.LABEL_AUTOCOMPLETE_DEPARTURE_GEOLOCATED));
  }

  private void postGATrackingEventAutocompleteDepartureRecent() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHT_SEARCH,
            GAnalyticsNames.ACTION_SEARCHER_FLIGHTS,
            GAnalyticsNames.LABEL_AUTOCOMPLETE_DEPARTURE_RECENT));
  }

  private void postGATrackingEventAutocompleteDepartureClosestAirports() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHT_SEARCH,
            GAnalyticsNames.ACTION_SEARCHER_FLIGHTS,
            GAnalyticsNames.LABEL_AUTOCOMPLETE_DEPARTURE_CLOSEST_AIRPORTS));
  }

  private void postGATrackingEventAutocompleteDepartureClosestCities() {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHT_SEARCH,
            GAnalyticsNames.ACTION_SEARCHER_FLIGHTS,
            GAnalyticsNames.LABEL_AUTOCOMPLETE_DEPARTURE_CLOSEST_CITIES));
  }

  public abstract void onGAEventTriggered(GATrackingEvent event);

  @Override public void onLocationReady() {
    currentView = DestinationsViewTypes.GEOLOCATION;

    List<City> nearestCities = locationController.getNearestPlacesList();
    if (nearestCities.isEmpty()) {
      Toast.makeText(this, LocalizablesFacade.getString(this, OneCMSKeys.CANNOT_FIND_LOCATIONS),
          Toast.LENGTH_LONG).show();
      Log.i(Constants.TAG_LOG, "Unable to obtain near locations");
    } else {
      setResults(locationController.getNearestPlacesList());
    }
  }

  public enum DestinationsViewTypes {
    GEOLOCATION, RECENT, SEARCH
  }

  class ResponseLocationListener extends BaseNeckRequestListener<TemporalDestination> {

    @Override public void onRequestSuccessful(TemporalDestination destinationsResponse) {
      Log.i(Constants.TAG_LOG, "onRequestSuccessful - ResponseLocationListener");
      lstCitiesFound = destinationsResponse.getCities();
      setResults(filterFoundPlaces(query));
      isSearching = false;

      UIAutomationSemaphore.getInstance().idleDown();
      Log.d(OdigeoDestinationActivity.this.toString(), "setIdlestate down " + query);
    }

    @Override public void onRequestError(BaseNeckRequestException.Error error) {
      Log.i(Constants.TAG_LOG, "onRequestError - ResponseLocationListener: " + error.getMessage());
      // GAnalytics screen tracking - Error case: no connection
      BusProvider.getInstance()
          .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_ERROR_NO_CONNECTION));
      showNotInternetConnectionErrorMessage();
      isSearching = false;

      UIAutomationSemaphore.getInstance().idleDown();
      Log.d(OdigeoDestinationActivity.this.toString(), "setIdlestate down error" + query);
    }

    @Override public void onRequestException(SpiceException exception) {
      Log.i(Constants.TAG_LOG,
          "onRequestError - ResponseLocationListener: " + exception.getMessage());
      showNotInternetConnectionErrorMessage();
      isSearching = false;

      UIAutomationSemaphore.getInstance().idleDown();
      Log.d(OdigeoDestinationActivity.this.toString(), "setIdlestate down exception" + query);
    }
  }
}
