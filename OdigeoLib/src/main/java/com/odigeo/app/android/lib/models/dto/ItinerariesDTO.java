package com.odigeo.app.android.lib.models.dto;

import java.util.Collection;
import java.util.List;

public class ItinerariesDTO {

  protected List<CrossFaringItineraryDTO> crossFarings;
  protected List<HubItineraryDTO> hubs;
  protected List<OwasrtItineraryDTO> owasrts;
  protected List<HidingItineraryDTO> hidings;
  protected List<ItineraryProviderDTO> simples;
  protected Collection<SmartHubItinerary> smartHubs;

  public List<CrossFaringItineraryDTO> getCrossFarings() {
    return crossFarings;
  }

  public void setCrossFarings(List<CrossFaringItineraryDTO> crossFarings) {
    this.crossFarings = crossFarings;
  }

  public List<HubItineraryDTO> getHubs() {
    return hubs;
  }

  public void setHubs(List<HubItineraryDTO> hubs) {
    this.hubs = hubs;
  }

  public List<OwasrtItineraryDTO> getOwasrts() {
    return owasrts;
  }

  public void setOwasrts(List<OwasrtItineraryDTO> owasrts) {
    this.owasrts = owasrts;
  }

  public List<HidingItineraryDTO> getHidings() {
    return hidings;
  }

  public void setHidings(List<HidingItineraryDTO> hidings) {
    this.hidings = hidings;
  }

  public List<ItineraryProviderDTO> getSimples() {
    return simples;
  }

  public void setSimples(List<ItineraryProviderDTO> simples) {
    this.simples = simples;
  }

  public Collection<SmartHubItinerary> getSmartHubs() {
    return smartHubs;
  }

  public void setSmartHubs(Collection<SmartHubItinerary> smartHubs) {
    this.smartHubs = smartHubs;
  }
}
