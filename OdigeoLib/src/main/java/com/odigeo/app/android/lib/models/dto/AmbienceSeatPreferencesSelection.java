package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.List;

public class AmbienceSeatPreferencesSelection implements Serializable {
  private List<Integer> sections;
  private String ambiencePreferenceCode;
  private MoneyDTO providerPrice;
  private MoneyDTO totalPrice;

  public final List<Integer> getSections() {
    return sections;
  }

  public final void setSections(List<Integer> sections) {
    this.sections = sections;
  }

  public final String getAmbiencePreferenceCode() {
    return ambiencePreferenceCode;
  }

  public final void setAmbiencePreferenceCode(String ambiencePreferenceCode) {
    this.ambiencePreferenceCode = ambiencePreferenceCode;
  }

  public final MoneyDTO getProviderPrice() {
    return providerPrice;
  }

  public final void setProviderPrice(MoneyDTO providerPrice) {
    this.providerPrice = providerPrice;
  }

  public final MoneyDTO getTotalPrice() {
    return totalPrice;
  }

  public final void setTotalPrice(MoneyDTO totalPrice) {
    this.totalPrice = totalPrice;
  }
}
