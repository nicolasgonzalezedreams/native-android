package com.odigeo.app.android.navigator;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.view.AppRateHelpImproveView;
import com.odigeo.app.android.view.AppRateThanksView;
import com.odigeo.app.android.view.AppRateView;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.presenter.contracts.navigators.AppRateFeedbackNavigatorInterface;
import java.util.List;

public class AppRateFeedBackNavigator extends BaseNavigator
    implements AppRateFeedbackNavigatorInterface {

  public static final String MARKET_URI = "market://details?id=";

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_view);
    setUpToolbarButton(0);
    setTitle(localizables.getString(OneCMSKeys.LEAVEFEEDBACK_HEADER));
    if (getIntent().getStringExtra(AppRateNavigator.DESTINATION)
        .equals(AppRateNavigator.DESTINATION_THANKS)) {
      showThanksScreen(savedInstanceState != null);
    } else if (getIntent().getStringExtra(AppRateNavigator.DESTINATION)
        .equals(AppRateNavigator.DESTINATION_HELP_IMPROVE)) {
      showHelpImproveScreen(savedInstanceState != null);
    }
  }

  @Override public void showThanksScreen(boolean isRestoringState) {
    if (!isRestoringState) {
      getSupportFragmentManager().beginTransaction()
          .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
              android.R.anim.fade_in, android.R.anim.fade_out)
          .add(R.id.fl_container, AppRateThanksView.newInstance())
          .commit();
    }
  }

  @Override public void showHelpImproveScreen(boolean isRestoringState) {
    if (!isRestoringState) {
      getSupportFragmentManager().beginTransaction()
          .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
              android.R.anim.fade_in, android.R.anim.fade_out)
          .add(R.id.fl_container, AppRateHelpImproveView.newInstance())
          .commit();
    }
  }

  @Override public void navigateToPlayStore() {
    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(MARKET_URI + getPackageName()));
    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    if (isCallable(i)) {
      startActivity(i);
    }
  }

  @Override public void navigateToHome() {
    Intent intent = new Intent(this, ((OdigeoApp) getApplication()).getHomeActivity());
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
    finish();
  }

  @Override public void closeAppRateThanksNavigator() {
    finish();
  }

  private boolean isCallable(Intent intent) {
    List<ResolveInfo> list =
        getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
    return !list.isEmpty();
  }
}
