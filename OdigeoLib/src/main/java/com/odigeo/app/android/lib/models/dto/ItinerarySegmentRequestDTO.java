package com.odigeo.app.android.lib.models.dto;

/**
 * @author miguel
 */
public class ItinerarySegmentRequestDTO {

  protected LocationRequestDTO departure;
  protected LocationRequestDTO destination;
  protected String dateString;
  protected String time;
  protected Integer timeWindow;

  public ItinerarySegmentRequestDTO(String dateString, LocationRequestDTO destination,
      LocationRequestDTO departure) {
    this.dateString = dateString;
    this.destination = destination;
    this.departure = departure;
  }

  /**
   * Gets the value of the departure property.
   *
   * @return possible object is
   * {@link ItineraryLocationRequest }
   */
  public LocationRequestDTO getDeparture() {
    return departure;
  }

  /**
   * Sets the value of the departure property.
   *
   * @param value allowed object is
   * {@link ItineraryLocationRequest }
   */
  public void setDeparture(LocationRequestDTO value) {
    this.departure = value;
  }

  /**
   * Gets the value of the destination property.
   *
   * @return possible object is
   * {@link ItineraryLocationRequest }
   */
  public LocationRequestDTO getDestination() {
    return destination;
  }

  /**
   * Sets the value of the destination property.
   *
   * @param value allowed object is
   * {@link ItineraryLocationRequest }
   */
  public void setDestination(LocationRequestDTO value) {
    this.destination = value;
  }

  /**
   * Gets the value of the time property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getTime() {
    return time;
  }

  /**
   * Sets the value of the time property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setTime(String value) {
    this.time = value;
  }

  /**
   * Gets the value of the timeWindow property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getTimeWindow() {
    return timeWindow;
  }

  /**
   * Sets the value of the timeWindow property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setTimeWindow(Integer value) {
    this.timeWindow = value;
  }

  /**
   * @return the dateString
   */
  public String getDateString() {
    return dateString;
  }

  /**
   * @param dateString the dateString to set
   */
  public void setDateString(String dateString) {
    this.dateString = dateString;
  }
}
