package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoFiltersActivity;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.models.FilterStopModel;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.SegmentGroup;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;
import com.odigeo.app.android.lib.models.dto.SectionDTO;
import com.odigeo.app.android.lib.ui.widgets.FilterStopWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.data.entity.TravelType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Irving Lóp on 14/10/2014.
 */

public class OdigeoFilterStopsFragment extends Fragment {

  private Context context;
  private OdigeoFiltersActivity activity;
  private OdigeoApp odigeoApp;
  private OdigeoSession odigeoSession;

  private LinearLayout layoutContainer;

  private TravelType travelType;
  private List<FareItineraryDTO> itineraryResults;
  private List<FilterStopWidget> filterStopWidgets;

  @Override public void onAttach(Activity activity) {
    super.onAttach(activity);
    this.activity = (OdigeoFiltersActivity) activity;
    this.context = activity.getApplicationContext();

    // GAnalytics screen tracking - Filter stops fragment
    BusProvider.getInstance().post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_FILTERS_STOPS));
  }

  @Override public void onCreate(Bundle saveInstance) {
    super.onCreate(saveInstance);

    odigeoApp = (OdigeoApp) activity.getApplication();
    odigeoSession = odigeoApp.getOdigeoSession();

    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {

      filterStopWidgets = new ArrayList<FilterStopWidget>();
      itineraryResults = activity.getItineraryResults();
    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.layout_filter_scales_fragment, container, false);
    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {

      layoutContainer = (LinearLayout) view.findViewById(R.id.layout_filter_scales_container);
      drawWidgets();
    }
    return view;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {

    super.onViewCreated(view, savedInstanceState);
    if (odigeoSession == null || !odigeoSession.isDataHasBeenLoaded()) {
      Util.sendToHome(activity, odigeoApp);
    }
  }

  private void drawWidgets() {
    List<FlightSegment> flightSegments = activity.getSearchOptions().getFlightSegments();
    travelType = activity.getSearchOptions().getTravelType();
    if (flightSegments != null && !flightSegments.isEmpty()) {
      int index = 0;
      for (FlightSegment segment : flightSegments) {
        FilterStopWidget widget = new FilterStopWidget(context, segment,
            Util.getTitleVolHeader(context, travelType, index));
        layoutContainer.addView(widget);
        ViewUtils.setSeparator(widget, getActivity().getApplicationContext());

        long maxDuration = getMaxScaleDuration(index);

        int maxDurationInHours = 0;
        if (maxDuration > 0) {
          maxDurationInHours = (int) (maxDuration / 60);
        }

        widget.setMaxValueSeekBar(maxDurationInHours);

        if (activity.getFiltersSelected() != null
            && activity.getFiltersSelected().getStops() != null) {

          FilterStopModel model = activity.getFiltersSelected().getStops().get(index);
          widget.setDataModel(model);
        }

        filterStopWidgets.add(widget);
        widget.setWidgetNumber(filterStopWidgets.size());

        index++;
      }
    }
  }

  private long getMaxScaleDuration(int index) {
    long maxDuration = 0;
    for (FareItineraryDTO itineraryResult : itineraryResults) {
      SegmentGroup segmentGroup = itineraryResult.getSegmentGroups().get(index);

      for (SegmentWrapper segmentWrapper : segmentGroup.getSegmentWrappers()) {
        for (SectionDTO section : segmentWrapper.getSectionsObjects()) {
          if (section.getDuration() > maxDuration) {
            maxDuration = section.getDuration();
          }
        }
      }
    }
    return maxDuration;
  }

  public List<FilterStopModel> getFilterStopModels() {
    List<FilterStopModel> stopModels = new ArrayList<FilterStopModel>();

    for (FilterStopWidget widget : filterStopWidgets) {
      stopModels.add(widget.getStopModel());
    }

    return stopModels;
  }
}
