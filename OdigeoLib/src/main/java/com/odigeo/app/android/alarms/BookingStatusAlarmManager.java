package com.odigeo.app.android.alarms;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.receivers.BookingStatusAlarmReceiver;
import com.odigeo.app.android.receivers.BootReceiver;
import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.interactors.managers.BookingStatusAlarmManagerInterface;

public class BookingStatusAlarmManager implements BookingStatusAlarmManagerInterface {

  public static final String ALARM_BOOKING_STATUS_ACTION = "ALARM_BOOKING_STATUS_ACTION";
  private static final long TIME_48H_TO_MILLIS = 172800000;
  private static final long TIME_1H_TO_MILLIS = 3600000;
  private Context context;
  private BookingDBDAOInterface bookingDBDAO;

  public BookingStatusAlarmManager(Context context, BookingDBDAOInterface bookingDBDAO) {
    this.context = context;
    this.bookingDBDAO = bookingDBDAO;
  }

  @Override public void initializeAlarm() {
    Intent alarmIntent = new Intent(context, BookingStatusAlarmReceiver.class);
    alarmIntent.setAction(ALARM_BOOKING_STATUS_ACTION);
    PendingIntent pendingIntent =
        PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);

    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calculateWhenTriggerAlarm(),
        calculateIntervalOfTheAlarm(), pendingIntent);

    ComponentName receiver = new ComponentName(context, BootReceiver.class);
    PackageManager pm = context.getPackageManager();
    pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
        PackageManager.DONT_KILL_APP);
  }

  @Override public void updateAlarm() {
    if (existAlarm()) {
      cancelAlarm();
    }
    initializeAlarm();
  }

  @Override public void cancelAlarm() {

    Intent alarmIntent = new Intent(context, BookingStatusAlarmReceiver.class);
    PendingIntent pendingIntent =
        PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);

    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    if (alarmManager != null) {
      alarmManager.cancel(pendingIntent);
    }

    ComponentName receiver = new ComponentName(context, BootReceiver.class);
    PackageManager pm = context.getPackageManager();
    pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
        PackageManager.DONT_KILL_APP);
  }

  private boolean existAlarm() {
    Intent alarmIntent = new Intent(context, BookingStatusAlarmReceiver.class);
    PendingIntent pendingIntent =
        PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_NO_CREATE);
    return pendingIntent != null;
  }

  private long calculateWhenTriggerAlarm() {
    return System.currentTimeMillis() + TIME_1H_TO_MILLIS;
  }

  private long calculateIntervalOfTheAlarm() {
    if (hasAddedMoreThanTwoDaysAgo(bookingDBDAO.getLatestPendingBookingAddedTime())) {
      return context.getResources().getInteger(R.integer.booking_status_twelve_hours_interval);
    } else {
      return context.getResources().getInteger(R.integer.booking_status_six_hours_interval);
    }
  }

  private boolean hasAddedMoreThanTwoDaysAgo(long timeLastPendingBookingAdded) {
    long currentTime = System.currentTimeMillis();
    if (currentTime > timeLastPendingBookingAdded + TIME_48H_TO_MILLIS) {
      return true;
    } else {
      return false;
    }
  }
}
