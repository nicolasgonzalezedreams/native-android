package com.odigeo.app.android.lib.models.dto.requests;

import com.odigeo.app.android.lib.models.dto.AccommodationSearchRequestDTO;
import com.odigeo.app.android.lib.models.dto.ItinerarySearchRequestDTO;
import java.io.Serializable;

public class FlightsRequestModel implements Serializable {

  protected ItinerarySearchRequestDTO itinerarySearchRequest;
  protected AccommodationSearchRequestDTO accommodationSearchRequest;

  public FlightsRequestModel(ItinerarySearchRequestDTO itinerarySR) {
    itinerarySearchRequest = itinerarySR;
  }

  public FlightsRequestModel() {
    itinerarySearchRequest = new ItinerarySearchRequestDTO();
  }

  /**
   * Gets the value of the itinerarySearchRequest property.
   *
   * @return possible object is {@link ItinerarySearchRequestDTO }
   */
  public final ItinerarySearchRequestDTO getItinerarySearchRequest() {
    return itinerarySearchRequest;
  }

  /**
   * Sets the value of the itinerarySearchRequest property.
   *
   * @param value allowed object is {@link ItinerarySearchRequestDTO }
   */
  public final void setItinerarySearchRequest(ItinerarySearchRequestDTO value) {
    this.itinerarySearchRequest = value;
  }

  public final AccommodationSearchRequestDTO getAccommodationSearchRequest() {
    return accommodationSearchRequest;
  }

  public final void setAccommodationSearchRequest(
      AccommodationSearchRequestDTO accommodationSearchRequest) {
    this.accommodationSearchRequest = accommodationSearchRequest;
  }
}
