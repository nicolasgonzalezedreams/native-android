package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.LocationDTO;
import com.odigeo.app.android.lib.models.dto.SectionDTO;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.TravelType;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import com.odigeo.tools.DurationFormatter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Irving on 14/09/2014.
 */
public class SummaryTravelWidget extends LinearLayout {

  private final Context context;
  private final LinearLayout listFlights;
  private final Fonts fonts;
  private CarrierDTO carrier;

  private TravelType typeTravel;

  private OdigeoImageLoader imageLoader;
  private DurationFormatter durationFormatter;

  public SummaryTravelWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
    this.context = context;

    imageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();
    inflate(context, R.layout.layout_summary_travel, this);
    listFlights = (LinearLayout) findViewById(R.id.layout_travelList_travel_summary);
    fonts = Configuration.getInstance().getFonts();
  }

  public SummaryTravelWidget(Context context, SegmentWrapper segmentWrapper, TravelType typeTravel,
      int posiiton) {
    super(context);

    this.context = context;
    this.typeTravel = typeTravel;

    imageLoader = AndroidDependencyInjector.getInstance().provideImageLoader();
    durationFormatter = AndroidDependencyInjector.getInstance().provideDurationFormatter();

    inflate(context, R.layout.layout_summary_travel, this);
    fonts = Configuration.getInstance().getFonts();
    this.carrier = segmentWrapper.getCarrier();

    listFlights = (LinearLayout) findViewById(R.id.layout_travelList_travel_summary);

    TextView textViewTitle = (TextView) findViewById(R.id.text_title_travel_summary);
    textViewTitle.setText(getTitle(posiiton));
    textViewTitle.setTypeface(fonts.getBold());

    TextView textFligt = (TextView) findViewById(R.id.text_subtitle_travel_summary);
    textFligt.setTypeface(fonts.getBold());

    TextView textTimeFlight = (TextView) findViewById(R.id.text_time_travel_summary);
    textTimeFlight.setText(durationFormatter.format(segmentWrapper.getSegment().getDuration()));
    textTimeFlight.setTypeface(fonts.getRegular());

    drawTravels(segmentWrapper.getSectionsObjects());
  }

  private String getTitle(int position) {
    if (typeTravel == TravelType.SIMPLE) {
      return CommonLocalizables.getInstance().getCommonOutbound(getContext()).toString();
    } else if (typeTravel == TravelType.ROUND) {
      if (position == 0) {
        return CommonLocalizables.getInstance().getCommonOutbound(getContext()).toString();
      } else {
        return CommonLocalizables.getInstance().getCommonInbound(getContext()).toString();
      }
    } else {
      return CommonLocalizables.getInstance().getCommonLeg(getContext()) + " " + (position + 1);
    }
  }

  private void drawTravels(List<SectionDTO> sections) {
    if (sections != null) {
      for (int flight = 0; flight < sections.size(); flight++) {
        SectionDTO section = sections.get(flight);
        if (section != null) {
          addFlight(OdigeoDateUtils.createCalendar(section.getDepartureDate()),
              section.getLocationFrom(), section.getDepartureTerminal(),
              R.layout.layout_summary_travel_item_flight_departure, false);
          addCardInfo(section);
          boolean isLast = (flight == sections.size() - 1);
          addFlight(OdigeoDateUtils.createCalendar(section.getArrivalDate()),
              section.getLocationTo(), section.getArrivalTerminal(),
              R.layout.layout_summary_travel_item_flight_arrival, isLast);
          if (flight < sections.size() - 1) {
            addCardScale(OdigeoDateUtils.createCalendar(section.getArrivalDate()),
                OdigeoDateUtils.createCalendar(sections.get(flight + 1).getDepartureDate()));
          }
        }
      }
    }
  }

  private void addFlight(Calendar dateFlight, LocationDTO location, String terminal, int idLayout,
      boolean isLast) {
    if (location != null) {

      View itemFlight = LayoutInflater.from(context).inflate(idLayout, this, false);
      TextView time = (TextView) itemFlight.findViewById(R.id.text_time_departure_summaryWidget);
      TextView date = (TextView) itemFlight.findViewById(R.id.text_date_departure_summaryWidget);
      TextView airport = (TextView) itemFlight.findViewById(R.id.text_namePlace_summaryWidget);
      TextView terminalTV = (TextView) itemFlight.findViewById(R.id.text_terminal_summarywidget);
      TextView city = (TextView) itemFlight.findViewById(R.id.text_cityName_summarywidget);

      time.setTypeface(fonts.getBold());
      date.setTypeface(fonts.getRegular());
      airport.setTypeface(fonts.getBold());
      terminalTV.setTypeface(fonts.getBold());
      city.setTypeface(fonts.getRegular());

      //TODO: use format from class LocaleUtils
      SimpleDateFormat format = OdigeoDateUtils.getGmtDateFormat(
          Configuration.getInstance().getCurrentMarket().getTimeFormat());
      time.setText(String.format(format.format(dateFlight.getTime())));

      format = OdigeoDateUtils.getGmtDateFormat(getContext().getString(R.string.templates__time4));
      date.setText(format.format(dateFlight.getTime()));

      airport.setText(location.getName() + " (" + location.getIataCode() + ")");

      if (terminal == null) {
        terminalTV.setVisibility(View.GONE);
      } else {
        String terminalText =
            LocalizablesFacade.getString(getContext(), OneCMSKeys.CARDS_TERMINAL) + " " + terminal;
        terminalTV.setText(terminalText);
      }

      city.setText(location.getCityName());

      if (isLast) {
        itemFlight.findViewById(R.id.line_flight).setVisibility(GONE);
      }
      listFlights.addView(itemFlight);
    }
  }

  private void addCardInfo(SectionDTO section) {

    View card =
        LayoutInflater.from(context).inflate(R.layout.layout_summary_travel_item_info, this, false);
    ImageView airlineIcon = (ImageView) card.findViewById(R.id.image_airline_icon);
    TextView airlineName = (TextView) card.findViewById(R.id.text_airline_info);
    TextView classFlight = (TextView) card.findViewById(R.id.text_type_class);
    TextView timeFlight = (TextView) card.findViewById(R.id.text_time_flight_summaryWidget);

    airlineName.setTypeface(fonts.getRegular());
    classFlight.setTypeface(fonts.getRegular());
    timeFlight.setTypeface(fonts.getRegular());

    if (carrier.getCode() != null) {
      String url = ViewUtils.getCarrierLogo(carrier.getCode());
      imageLoader.load(airlineIcon, url);
    }

    airlineName.setText(carrier.getName() + " - " + section.getCarrier() + " " + section.getId());

    CarrierDTO operationCarrier = section.getOperatingCarrierObject();
    if (operationCarrier != null && !operationCarrier.getCode().equals(carrier.getCode())) {
      card.findViewById(R.id.layout_operated_by).setVisibility(VISIBLE);
      TextView operatedByLegend = (TextView) card.findViewById(R.id.text_legend_operated);
      operatedByLegend.setTypeface(fonts.getBold());
      operatedByLegend.setText(
          CommonLocalizables.getInstance().getCommonOperatedbytext(getContext()));
      TextView operatedBy = (TextView) card.findViewById(R.id.text_operated_by);
      operatedBy.setTypeface(fonts.getRegular());
      operatedBy.setText(" " + section.getOperatingCarrierObject().getName());
    }

    classFlight.setText(
        LocalizablesFacade.getString(context, Util.getResourceCabinClass(section.getCabinClass())));

    timeFlight.setText(durationFormatter.format(section.getDuration()));
    if (section.getAircraft() != null && !section.getAircraft().isEmpty()) {
      card.findViewById(R.id.icon_aircraft_itemInfo).setVisibility(VISIBLE);
      TextView aircraftCode = (TextView) card.findViewById(R.id.text_aircraft_summaryWidget);
      aircraftCode.setText(section.getAircraft());
      aircraftCode.setTypeface(fonts.getRegular());
    }

    listFlights.addView(card);
  }

  private void addCardScale(Calendar arrivalTime, Calendar departureTime) {
    long timeScale = departureTime.getTimeInMillis() - arrivalTime.getTimeInMillis();

    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(timeScale);
    calendar.setTimeZone(TimeZone.getTimeZone("GMT"));

    int hour = calendar.get(Calendar.HOUR_OF_DAY);
    int minuts = calendar.get(Calendar.MINUTE);

    View card = LayoutInflater.from(context)
        .inflate(R.layout.layout_summary_travel_item_scale, this, false);

    ((TextView) card.findViewById(R.id.text_time_scale_summaryWidget)).setText(
        durationFormatter.format(hour, minuts));
    ((TextView) card.findViewById(R.id.text_time_scale_summaryWidget)).setTypeface(fonts.getBold());
    TextView tvScaleTitle = (TextView) card.findViewById(R.id.text_title_scale);
    tvScaleTitle.setTypeface(fonts.getBold());
    tvScaleTitle.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.CELLSTOP_LABEL_CHANGEPLANE));
    TextView textTimeFlight = ((TextView) card.findViewById(R.id.text_time_flight_summaryWidget));
    textTimeFlight.setTypeface(fonts.getBold());
    textTimeFlight.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.STOPCELL_STOP_DURATION));
    textTimeFlight.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.STOPCELL_STOP_DURATION));

    if (Configuration.getInstance()
        .getCurrentMarket()
        .getLanguage()
        .toUpperCase()
        .equals(Constants.GERMANY_MARKET)) {
      textTimeFlight.setWidth(
          getResources().getDimensionPixelSize(R.dimen.text_time_flight_summaryWidget_width));
    }

    TextView tvLegendScale = (TextView) card.findViewById(R.id.text_legend_scale);
    tvLegendScale.setTypeface(fonts.getRegular());
    tvLegendScale.setText(
        LocalizablesFacade.getString(getContext(), OneCMSKeys.CELLSTOP_LABEL_WARNIG));
    listFlights.addView(card);
  }
}
