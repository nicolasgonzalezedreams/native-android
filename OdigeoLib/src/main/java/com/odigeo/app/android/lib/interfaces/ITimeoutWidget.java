package com.odigeo.app.android.lib.interfaces;

/**
 * Created by ximena.perez on 29/05/2015.
 */
public interface ITimeoutWidget {

  void showTimeoutWidget();

  void setScrollParameters();

  void setWidgetClosed();

  void showErrorDialog();
}
