package com.odigeo.app.android.lib.models.dto;

import java.util.List;

public class AccommodationDetailDTO {

  protected AccommodationDealInformationDTO accommodationDealInformation;
  protected String stateProvince;
  protected String country;
  protected String postalCode;
  protected List<String> mapUrls;
  protected String locationDescription;
  protected String infoZone;
  protected String checkInPolicy;
  protected String checkOutPolicy;
  protected String hotelPolicy;
  protected List<String> creditCardTypes;
  protected String phoneNumber;
  protected String mail;
  protected String web;
  protected String fax;
  protected List<ImageInformationDTO> images;
  protected List<AccommodationServiceDTO> availableServices;
  protected List<AccommodationServiceDTO> roomAvailableServices;
  protected BazaarvoiceReviewDTO usersReview;
  protected String numberOfRooms;
  protected String hotelType;

  /**
   * Gets the value of the accommodationDealInformation property.
   *
   * @return possible object is
   * {@link AccommodationDealInformation }
   */
  public AccommodationDealInformationDTO getAccommodationDealInformation() {
    return accommodationDealInformation;
  }

  /**
   * Sets the value of the accommodationDealInformation property.
   *
   * @param value allowed object is
   * {@link AccommodationDealInformation }
   */
  public void setAccommodationDealInformation(AccommodationDealInformationDTO value) {
    this.accommodationDealInformation = value;
  }

  /**
   * Gets the value of the stateProvince property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getStateProvince() {
    return stateProvince;
  }

  /**
   * Sets the value of the stateProvince property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setStateProvince(String value) {
    this.stateProvince = value;
  }

  /**
   * Gets the value of the country property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCountry() {
    return country;
  }

  /**
   * Sets the value of the country property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCountry(String value) {
    this.country = value;
  }

  /**
   * Gets the value of the postalCode property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getPostalCode() {
    return postalCode;
  }

  /**
   * Sets the value of the postalCode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setPostalCode(String value) {
    this.postalCode = value;
  }

  /**
   * Gets the value of the locationDescription property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getLocationDescription() {
    return locationDescription;
  }

  /**
   * Sets the value of the locationDescription property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setLocationDescription(String value) {
    this.locationDescription = value;
  }

  /**
   * Gets the value of the infoZone property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getInfoZone() {
    return infoZone;
  }

  /**
   * Sets the value of the infoZone property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setInfoZone(String value) {
    this.infoZone = value;
  }

  /**
   * Gets the value of the checkInPolicy property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCheckInPolicy() {
    return checkInPolicy;
  }

  /**
   * Sets the value of the checkInPolicy property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCheckInPolicy(String value) {
    this.checkInPolicy = value;
  }

  /**
   * Gets the value of the checkOutPolicy property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getCheckOutPolicy() {
    return checkOutPolicy;
  }

  /**
   * Sets the value of the checkOutPolicy property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setCheckOutPolicy(String value) {
    this.checkOutPolicy = value;
  }

  /**
   * Gets the value of the hotelPolicy property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getHotelPolicy() {
    return hotelPolicy;
  }

  /**
   * Sets the value of the hotelPolicy property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setHotelPolicy(String value) {
    this.hotelPolicy = value;
  }

  /**
   * Gets the value of the phoneNumber property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getPhoneNumber() {
    return phoneNumber;
  }

  /**
   * Sets the value of the phoneNumber property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setPhoneNumber(String value) {
    this.phoneNumber = value;
  }

  /**
   * Gets the value of the mail property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getMail() {
    return mail;
  }

  /**
   * Sets the value of the mail property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setMail(String value) {
    this.mail = value;
  }

  /**
   * Gets the value of the web property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getWeb() {
    return web;
  }

  /**
   * Sets the value of the web property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setWeb(String value) {
    this.web = value;
  }

  /**
   * Gets the value of the fax property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getFax() {
    return fax;
  }

  /**
   * Sets the value of the fax property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setFax(String value) {
    this.fax = value;
  }

  /**
   * Gets the value of the usersReview property.
   *
   * @return possible object is
   * {@link BazaarvoiceReview }
   */
  public BazaarvoiceReviewDTO getUsersReview() {
    return usersReview;
  }

  /**
   * Sets the value of the usersReview property.
   *
   * @param value allowed object is
   * {@link BazaarvoiceReview }
   */
  public void setUsersReview(BazaarvoiceReviewDTO value) {
    this.usersReview = value;
  }

  /**
   * Gets the value of the numberOfRooms property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getNumberOfRooms() {
    return numberOfRooms;
  }

  /**
   * Sets the value of the numberOfRooms property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setNumberOfRooms(String value) {
    this.numberOfRooms = value;
  }

  /**
   * Gets the value of the hotelType property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getHotelType() {
    return hotelType;
  }

  /**
   * Sets the value of the hotelType property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setHotelType(String value) {
    this.hotelType = value;
  }

  /**
   * @return the mapUrls
   */
  public List<String> getMapUrls() {
    return mapUrls;
  }

  /**
   * @param mapUrls the mapUrls to set
   */
  public void setMapUrls(List<String> mapUrls) {
    this.mapUrls = mapUrls;
  }

  /**
   * @return the creditCardTypes
   */
  public List<String> getCreditCardTypes() {
    return creditCardTypes;
  }

  /**
   * @param creditCardTypes the creditCardTypes to set
   */
  public void setCreditCardTypes(List<String> creditCardTypes) {
    this.creditCardTypes = creditCardTypes;
  }

  /**
   * @return the images
   */
  public List<ImageInformationDTO> getImages() {
    return images;
  }

  /**
   * @param images the images to set
   */
  public void setImages(List<ImageInformationDTO> images) {
    this.images = images;
  }

  /**
   * @return the availableServices
   */
  public List<AccommodationServiceDTO> getAvailableServices() {
    return availableServices;
  }

  /**
   * @param availableServices the availableServices to set
   */
  public void setAvailableServices(List<AccommodationServiceDTO> availableServices) {
    this.availableServices = availableServices;
  }

  /**
   * @return the roomAvailableServices
   */
  public List<AccommodationServiceDTO> getRoomAvailableServices() {
    return roomAvailableServices;
  }

  /**
   * @param roomAvailableServices the roomAvailableServices to set
   */
  public void setRoomAvailableServices(List<AccommodationServiceDTO> roomAvailableServices) {
    this.roomAvailableServices = roomAvailableServices;
  }
}
