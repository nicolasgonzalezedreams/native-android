package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author miguel
 */
@Deprecated public class InsuranceOfferDTO implements Serializable {

  private List<InsuranceDTO> insurances;
  private String id;
  private BigDecimal totalPrice;
  private BigDecimal providerPrice;

  public InsuranceOfferDTO() {

  }

  public InsuranceOfferDTO(String id) {
    this.id = id;
  }

  public InsuranceOfferDTO(BigDecimal providerPrice, BigDecimal totalPrice) {
    this.providerPrice = providerPrice;
    this.totalPrice = totalPrice;

    insurances = new ArrayList<InsuranceDTO>();
  }

  /**
   * Gets the value of the id property.
   *
   * @return possible object is {@link String }
   */
  public String getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   *
   * @param value allowed object is {@link String }
   */
  public void setId(String value) {
    this.id = value;
  }

  /**
   * Gets the value of the totalPrice property.
   *
   * @return possible object is {@link java.math.BigDecimal }
   */
  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  /**
   * Sets the value of the totalPrice property.
   *
   * @param value allowed object is {@link java.math.BigDecimal }
   */
  public void setTotalPrice(BigDecimal value) {
    this.totalPrice = value;
  }

  /**
   * Gets the value of the providerPrice property.
   *
   * @return possible object is {@link java.math.BigDecimal }
   */
  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  /**
   * Sets the value of the providerPrice property.
   *
   * @param value allowed object is {@link java.math.BigDecimal }
   */
  public void setProviderPrice(BigDecimal value) {
    this.providerPrice = value;
  }

  public List<InsuranceDTO> getInsurances() {
    return insurances;
  }

  public void setInsurances(List<InsuranceDTO> insurances) {
    this.insurances = insurances;
  }

  public boolean equals(Object object) {
    if (!(object instanceof InsuranceOfferDTO)) {
      return false;
    }

    return this.getId().equals(((InsuranceOfferDTO) object).getId());
  }
}
