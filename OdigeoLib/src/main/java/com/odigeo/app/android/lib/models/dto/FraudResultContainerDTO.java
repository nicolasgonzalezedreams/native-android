package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for FraudResultContainer complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="FraudResultContainer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="fraudDecisionType" type="{http://api.edreams.com/booking/next}fraudDecisionType"
 * />
 *       &lt;attribute name="fraudDecisionTypeStatus" type="{http://api.edreams.com/booking/next}fraudDecisionTypeStatus"
 * />
 *       &lt;attribute name="isWhiteListed" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="isBlackListed" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */

public class FraudResultContainerDTO {

  protected FraudDecisionTypeDTO fraudDecisionType;
  protected FraudDecisionTypeStatusDTO fraudDecisionTypeStatus;
  protected Boolean isWhiteListed;
  protected Boolean isBlackListed;

  /**
   * Gets the value of the fraudDecisionType property.
   *
   * @return possible object is {@link FraudDecisionTypeDTO }
   */
  public FraudDecisionTypeDTO getFraudDecisionType() {
    return fraudDecisionType;
  }

  /**
   * Sets the value of the fraudDecisionType property.
   *
   * @param value allowed object is {@link FraudDecisionTypeDTO }
   */
  public void setFraudDecisionType(FraudDecisionTypeDTO value) {
    this.fraudDecisionType = value;
  }

  /**
   * Gets the value of the fraudDecisionTypeStatus property.
   *
   * @return possible object is {@link FraudDecisionTypeStatusDTO }
   */
  public FraudDecisionTypeStatusDTO getFraudDecisionTypeStatus() {
    return fraudDecisionTypeStatus;
  }

  /**
   * Sets the value of the fraudDecisionTypeStatus property.
   *
   * @param value allowed object is {@link FraudDecisionTypeStatusDTO }
   */
  public void setFraudDecisionTypeStatus(FraudDecisionTypeStatusDTO value) {
    this.fraudDecisionTypeStatus = value;
  }

  /**
   * Gets the value of the isWhiteListed property.
   *
   * @return possible object is {@link Boolean }
   */
  public Boolean isIsWhiteListed() {
    return isWhiteListed;
  }

  /**
   * Sets the value of the isWhiteListed property.
   *
   * @param value allowed object is {@link Boolean }
   */
  public void setIsWhiteListed(Boolean value) {
    this.isWhiteListed = value;
  }

  /**
   * Gets the value of the isBlackListed property.
   *
   * @return possible object is {@link Boolean }
   */
  public Boolean isIsBlackListed() {
    return isBlackListed;
  }

  /**
   * Sets the value of the isBlackListed property.
   *
   * @param value allowed object is {@link Boolean }
   */
  public void setIsBlackListed(Boolean value) {
    this.isBlackListed = value;
  }
}
