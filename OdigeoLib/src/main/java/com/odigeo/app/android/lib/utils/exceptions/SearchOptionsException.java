package com.odigeo.app.android.lib.utils.exceptions;

import com.google.gson.Gson;
import com.odigeo.app.android.lib.config.SearchOptions;

public class SearchOptionsException extends Exception {

  private SearchOptionsException(String detailMessage, Throwable th) {
    super(detailMessage, th);
  }

  public static SearchOptionsException newInstance(SearchOptions searchOptions, Throwable th) {
    Gson gson = new Gson();
    String json = gson.toJson(searchOptions);
    return new SearchOptionsException(json, th);
  }
}
