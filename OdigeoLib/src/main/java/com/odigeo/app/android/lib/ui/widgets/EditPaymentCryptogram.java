package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.ui.widgets.base.EditWithHint;

/**
 * Created by ManuelOrtiz on 08/10/2014.
 */
public class EditPaymentCryptogram extends EditWithHint {

  private final ImageView imageView;
  private CryptogramLenght currentLength;

  public EditPaymentCryptogram(Context context, AttributeSet attrs) {
    super(context, attrs);
    imageView = (ImageView) findViewById(R.id.imgCard);
  }

  public final CryptogramLenght getCurrentLength() {
    return currentLength;
  }

  public final void setCurrentLength(CryptogramLenght currentLength) {
    this.currentLength = currentLength;

    if (currentLength == CryptogramLenght.LENGHT4) {
      imageView.setImageResource(R.drawable.payment_cryptogramme2);
    } else {
      imageView.setImageResource(R.drawable.payment_cryptogramme);
    }
  }

  @Override public final int getLayout() {
    return R.layout.layout_edit_payment_cryptogram;
  }

  public enum CryptogramLenght {
    LENGHT3, LENGHT4
  }
}
