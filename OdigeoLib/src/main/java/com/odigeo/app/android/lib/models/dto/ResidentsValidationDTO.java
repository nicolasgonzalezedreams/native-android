package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResidentsValidationDTO implements Serializable {

  protected List<TravellerResidentValidationDTO> residentValidations =
      new ArrayList<TravellerResidentValidationDTO>();

  public ResidentsValidationDTO() {
  }

  /**
   * @return the travellersResidentValidation
   */
  public List<TravellerResidentValidationDTO> getTravellersResidentValidation() {
    return residentValidations;
  }

  /**
   * @param residentValidations the travellersResidentValidation to set
   */
  public void setTravellersResidentValidation(
      List<TravellerResidentValidationDTO> residentValidations) {
    this.residentValidations = residentValidations;
  }
}
