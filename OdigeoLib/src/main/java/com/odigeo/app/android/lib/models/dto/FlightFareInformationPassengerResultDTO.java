package com.odigeo.app.android.lib.models.dto;

public class FlightFareInformationPassengerResultDTO {

  protected FlightFareInformationPassengerDTO fareInfoPax;
  protected int id;

  /**
   * Gets the value of the fareInfoPax property.
   *
   * @return possible object is
   * {@link FlightFareInformationPassenger }
   */
  public FlightFareInformationPassengerDTO getFareInfoPax() {
    return fareInfoPax;
  }

  /**
   * Sets the value of the fareInfoPax property.
   *
   * @param value allowed object is
   * {@link FlightFareInformationPassenger }
   */
  public void setFareInfoPax(FlightFareInformationPassengerDTO value) {
    this.fareInfoPax = value;
  }

  /**
   * Gets the value of the id property.
   */
  public int getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   */
  public void setId(int value) {
    this.id = value;
  }
}
