package com.odigeo.app.android.lib.consts;

import android.graphics.Bitmap;

public final class Constants {

  public static final int NOT_PREVIOUS_VERSION_FOUND = -1;
  public static final String CARROUSEL_IMAGES = "http://www.edreams.com/";
  public static final String FLIGHTS_PRODUCT = "FLIGHT";
  public static final String TAG_LOG = "ODIGEO APP";
  public static final String TAG_LOG_MSL = "MSL RESPONSE";
  public static final String TAG_LOG_FULLPRICE = "SortCriteriaFullPrice";
  public static final String TAG_SYNC = "OdigeoApplications";
  public static final String TAG_LOG_UPGRADE = "OnUpgradeReceiver";
  public static final String APP_VERSION_NAME_CRASHLYTICS_PROPERTY = "APP_VERSION_NAME";
  public static final String SHARED_PREFERENCE_FILE_SETTINGS = "prefFileSettings";
  public static final String PREFERENCE_SETTINGS = "appSettings";
  public static final String FIRST_TIME_OPENED = "firstTimeOpened";
  public static final String SHARED_PREFERENCE_FILE_DEPARTURE_CITIES = "historyDepartCities";
  public static final String SHARED_PREFERENCE_FILE_RETOUR_CITIES = "historyRetourCities";
  public static final String PREFERENCE_LIST_CITIES = "citiesNames";
  public static final String SHARED_PREFERENCE_FILE_PASSENGERS =
      "com.odigeo.app.android.lib.passengers";
  public static final String PREFERENCE_PASSENGERS = "passengers";
  public static final String SHARED_PREFERENCE_FILE_SEARCH_OPTIONS =
      "com.odigeo.app.android.lib.search_options";
  public static final String PREFERENCE_SEARCH_OPTIONS = "mSearchOptions";
  public static final String SHARED_PREFERENCE_FILE_SHOPPING_CART =
      "com.odigeo.app.android.lib.shopping_cart_";
  public static final String PREFERENCE_SHOPPING_CART = "shoppingcart";
  public static final String SHARED_PREFERENCE_FILE_HISTORY_SEARCH =
      "com.odigeo.app.android.lib.historysearch";
  public static final String PREFERENCE_HISTORY_SEARCH = "historySearch";
  public static final String PREFERENCE_HISTORY_SEARCH_MD = "historySearchMD";
  public static final String SHARED_PREFERENCES_ODIGEO =
      "com.odigeo.app.android.lib.shared_preferences";
  public static final String PREFERENCE_APP_VERSION_CODE = "appVersionCode";
  public static final String PREFERENCE_RATE_THE_APP = "rateTheApp";
  public static final String PREFERENCE_APP_VERSION_UPDATE_TO_IGNORE = "appVersionNameToIgnore";
  public static final String PREFERENCE_IS_FIRST_IN_HOME = "firstInHomeActivity";
  public static final String PREFERENCE_IS_FIRST_MY_TRIPS = "firstMyTrips";
  public static final String PREFERENCE_MULTIDESTINATION_INDEX = "multiDestiIndex";
  public static final String PREFERENCE_FREQUENT_FLYER_CODES = "FreqFlyerCodes";
  public static final String SHARED_PREFERENCE_FILE_QA_MODE_URLS =
      "com.odigeo.app.android.lib.qa_mode_urls";
  public static final String PREFERENCE_QA_MODE_URLS = "qaModeUrls";
  public static final String PREFERENCE_QA_MODE_URL = "qaModeUrl";
  public static final String PREFERENCE_SESSION = "Session";
  public static final String PREFERENCE_SESSION_RESULTS = "SearchResults";
  public static final String PREFERENCE_SESSION_FULLPRICE = "SearchResultsFullPriceMethods";
  public static final String PREFERENCE_SESSION_MY_SHOPPING_CART = "Session_MyShoppingCart";
  public static final String PREFERENCE_SESSION_AVAILABLE_PRODUCTS =
      "Session_availableProductsResponse";
  public static final String PREFERENCE_SESSION_ITINERARY_RESULTS = "Session_itineraryResults";
  public static final String PREFERENCE_SESSION_ITINERARY_RESULTS_FILTERED =
      "Session_itineraryResultsFiltered";
  public static final String PREFERENCE_SESSION_PASSENGERS = "SessionPassengers";
  public static final String PREFERENCE_DATES_HAS_BEEN_MIGRATED = "datesHasBeenMigrated";
  public static final String PREFERENCE_STATUS_NOTIFICATIONS = "status";
  public static final String PREFERENCE_STATUS_FLIGHTS_NOTIFICATIONS = "flights_notifications";
  public static final String SHARED_PREFERENCES_MOST_RECENT_BOOKING = "lastMostRecentBooking";
  public static final String SHARED_PREFERENCES_LAST_UPDATE_BOOKING_STATUS = "lastupdatebooking";
  public static final int ID_TAB_ROUNDTRIP = 0;
  public static final int ID_TAB_SIMPLETRIP = 1;
  public static final int ID_TAB_MULTITRIP = 2;
  public static final int ID_TIME_FRAGMENT = 0;
  public static final int ID_STOPS_FRAGMENT = 1;
  public static final int ID_AIRLINES_FRAGMENT = 2;
  public static final int ID_AIRPORTS_FRAGMENT = 3;
  public static final String PATTERN_XML_STRINGS = "%@";
  public static final String INTENT_URL_WEBVIEW = "URL_for_WEBVIEW";
  public static final String INTENT_SEARCH_OPTIONS_TO_FILTERS = "intToFilters";
  public static final String INTENT_URL_PREFIX = "com.opodo.flights.pixellback://showHTMLAdress/";
  public static final String EXTRA_CARRIERS_FIL_TO_RESULTS = "carriersToResults";
  public static final String INTENT_CARRIERS_TO_FILTER = "carriersList";
  public static final String POST_FLIGHT_REQUEST = "search/";
  public static final int GEOLOCATION_RADIUS_IN_KM = 100;
  public static final String EXTRA_STEP_TO_SUMMARY = "intentToSummary";
  public static final String CHANNEL = "ANDROID";
  // new Insurances
  public static final String NAME_INSURANCES_CONDITIONS_BASE = "SUMGVFRESBOT011196FRM3.pdf";
  public static final String NAME_INSURANCES_CONDITIONS_EXTEND = "TACGVFRESBOT011199FRM3V2.pdf";
  //public static final String CHANNEL = "IOS";
  public static final String EXTRA_TITLE_ICF = "titleICF";
  public static final String EXTRA_CONTENT_ICF = "contentICF";
  public static final String EXTRA_LINK_ICF = "linkICF";
  public static final String EXTRA_TEXT_LINK_ICF = "textLICF";
  public static final String EXTRA_PRICE_ICF = "priceICF";
  public static final String EXTRA_PRICE_DETAIL_ICF = "pDetailICF";
  public static final String EXTRA_PDF_NAME_ICF = "PDFICF";
  public static final String EXTRA_ALL_COUNTRY_LIST = "AllCountries";
  public static final String EXTRA_URL_WEBVIEW = "URL_web";
  public static final String EXTRA_SHOW_VOUCHER_IN_CAR = "extra_show_voucher_in_car";
  public static final String EXTRA_SEARCH_OPTIONS = "SEARCH_OPTIONS_DATA";
  public static final String EXTRA_SEARCH_TRACKER_HELPER = "SEARCH_TRACKER_DATA";
  public static final String EXTRA_SEARCH_RESPONSE = "SEARCH_SERVICE_RESPONSE";
  public static final String EXTRA_FLIGHT_SEGMENT_NUMBER = "FLIGHT_SEGMENT_NUMBER";
  public static final String EXTRA_DESTINATION = "DESTINATION_SELECTED";
  public static final String EXTRA_FLIGHT_DIRECTION = "FLIGHT_DIRECTION";
  public static final String EXTRA_SELECTED_DATES = "SELECTED_DATES";
  public static final String EXTRA_TRAVEL_TYPE = "TRAVEL_TYPE";
  public static final String EXTRA_COUNTRY_MODE = "COUNTRY_MODE";
  public static final String EXTRA_COUNTRY = "COUNTRY_SELECTED";
  public static final String EXTRA_PASSENGER = "PASSENGER_DATA";
  public static final String EXTRA_IDENTIFICATIONS = "PASSENGER_IDENTIFICATIONS_DATA";
  public static final String EXTRA_PASSENGER_INDEX = "PASSENGER_INDEX";
  public static final String EXTRA_SEARCH_OPTION_ID = "SEARCH_OPTION_ID";
  public static final String EXTRA_FILTERS_SELECTED = "FLIGHTS_FILTERS";
  public static final String EXTRA_AIRPORTS_BY_CITY = "AIRPORTS_BY_CITY";
  public static final String EXTRA_ACTION_RESEARCH = "actionReSearch";
  public static final String EXTRA_ACTION_GOTO_MYTRIPS = "actionGoToTrips";
  public static final String EXTRA_COLLECTION_STATE = "COLLE_STATE";
  public static final String EXTRA_BOOKING_RESPONSE = "BOOKING_RESPONSE";
  public static final String EXTRA_BOOKING_DETAIL = "BOOKING_DETAILS";
  public static final String EXTRA_BOOKING_STATUS = "BOOKING_STATUS";
  public static final String EXTRA_BOOKING_MESSAGES = "EXTRA_BOOKING_MESSAGES";
  public static final String EXTRA_COLLECTION_METHOD = "EXTRA_COLLECTION_METHOD";
  public static final String EXTRA_BOOKING = "booking";
  public static final String EXTRA_MARKETING_REVENUE = "MARKETING_REVENUE";
  public static final String EXTRA_CURRENCY_CODE = "CURRENCY_CODE";
  public static final String EXTRA_QA_MODE = "QA_MODE_SELECTED";
  public static final String EXTRA_BOOKING_PNRS = "bookingsids";
  public static final String EXTRA_AIRPORT_NAME = "airportWidening";
  public static final String EXTRA_CITY_NAME = "cityWidening";
  public static final String EXTRA_WIDENING_TYPE = "wideningType";
  public static final String EXTRA_IATA_CODE_DEPARTURE = "iataCodeDeparture";
  public static final String EXTRA_ARRIVAL_CITY_NAME = "arrivalCityWidening";
  public static final String EXTRA_ARRIVAL_AIRPORT_NAME = "arrivalAirWidening";
  public static final String EXTRA_FREQUENT_FLYER_LIST = "FreqFlyerCodes";
  public static final String EXTRA_FREQUENT_FLYER_CARRIERS = "groupsCarriersFFCPassenger";
  public static final String FREQUENT_FLYER_POSITION = "frequentFlyerPosition";
  public static final String EXTRA_COUNTRY_ACT_PARENT = "ACTIVITY_PARENT";
  public static final String EXTRA_COUNTRY_ACT_PARENT_SETTINGS = "Settings";
  public static final String EXTRA_OLD_SHOPPING_CART = "oldShoppingCart";
  public static final String EXTRA_FLOW_CONFIGURATION_RESPONSE = "flow_configuration_response";
  public static final String EXTRA_AVAILABLE_PRODUCTS_RESPONSE = "available_products_response";
  public static final String EXTRA_CREATE_SHOPPING_CART_RESPONSE = "create_shopping_cart_response";
  public static final String EXTRA_FULL_TRANSPARENCY = "full_transparency";
  public static final String EXTRA_BOOKING_INFO = "booking_info";
  public static final String COLLECTION_METHOD_WITH_PRICE = "collection_method_with_price";
  public static final String EXTRA_BANK_TRANSFER_RESPONSE = "bank_transfer_response";
  public static final String EXTRA_BANK_TRANSFER_DATA = "bank_transfer_data";
  public static final String EXTRA_COLLECTION_OPTION_SELECTED = "collection_option_selected";
  public static final String EXTRA_JUMP_TO_SEARCH = "go_to_search";
  public static final String EXTRA_RESUME_DATA_REQUEST = "resume_data_request";
  public static final String EXTRA_COMES_FROM_JOIN_US = "comes_from_join_us";
  public static final String EXTRA_MY_TRIPS_WAITING_ANIMATION_ENABLED = "waiting_animation_enabled";
  public static final String EXTRA_SAVE_PAYMENT_METHOD = "saved_payment_method";
  //WidgetTrip
  public static final String EXTRA_REPRICING_INSURANCES = "EXTRA_REPRICING_INSURANCES";
  public static final String EXTRA_REPRICING_TICKETS_PRICES = "EXTRA_REPRICING_TICKETS";
  //10 minutes delay for runnable
  public static final long TIMEOUT_COUNTER_WIDGET_DELAY = 600000;
  public static final int TIMEOUT_COUNTER_WIDGET_SUMMARY = 10;
  public static final int TIMEOUT_COUNTER_WIDGET_PASSENGERS = 10;
  public static final String TIMEOUT_WIDGET_PREVIOUS_TIME = "TIMEOUT_WIDGET_PREVIOUS_TIME";
  public static final String TIMEOUT_WIDGET_SHOWN = "TIMEOUT_WIDGET_SHOWN";
  //3 seconds delay in order to change the scrollview params to make the animation flow smoothly
  public static final int TIMEOUT_COUNTER_SCROLL_PARAMS_REFORMAT = 3000;
  public static final int SELECTING_DESTINATION = 100;
  public static final int SELECTING_DATE = 101;
  public static final int SELECTING_BUYER_COUNTRY_OF_RESIDENCE = 100;
  public static final int SELECTING_TRAVELLER_COUNTRY_OF_RESIDENCE = 102;
  public static final int SELECTING_TRAVELLER_IDENTIFICATION_COUNTRY = 104;
  public static final int SELECTING_TRAVELLER_NATIONALITY = 106;
  public static final int SELECTING_COUNTRY_PHONE_PREFIX = 200;
  public static final int REQUEST_CODE_LANGUAGE = 222;
  public static final int REQUEST_CODE_QAMODE = 234; //TODO put real code
  public static final int REQUEST_CODE_FLIGHTLISTENER = 311;
  public static final int REQUEST_CODE_SHOPPINGCARLISTENER = 312;
  public static final int REQUEST_CODE_AVAILABLEPRODUCTSLISTENER = 313;
  public static final int REQUEST_CODE_REMOVEPRODUCTLISTENER = 314;
  public static final int REQUEST_CODE_ADDPRODUCTLISTENER = 315;
  public static final int REQUEST_CODE_ADDPASSENGERLISTENER = 316;
  public static final int REQUEST_CODE_BOOKINGCONFIRMATIONLISTENER = 317;
  public static final int REQUEST_CODE_CARTE = 400;
  public static final int REQUEST_CODE_NIE = 402;
  public static final int REQUEST_CODE_NIF = 404;
  public static final int REQUEST_CODE_PASSPORT = 405;
  public static final int REQUEST_CODE_FREQUENT_FLYER_CODES = 407;
  public static final int REQUEST_CODE_DO_LOGIN = 408;
  public static final String RESPONSE_LIST_FREQUENT_FLYER = "response_list_frequent_flyer";
  public static final int REQUEST_CODE_SEARCH = 500;
  public static final String UTF8 = "UTF-8";
  public static final String BRAND_OPODO = "Opodo";
  public static final String BRAND_GOVOYAGES = "GoVoyages";
  public static final String BRAND_EDREAMS = "eDreams";
  public static final String BRAND_TRAVELLINK = "Travellink";
  public static final String GERMANY_MARKET = "DE";
  public static final String JAPANESE_MARKET = "JP";
  public static final String GREEK_MARKET = "GR";
  public static final int MAX_AGE_INFANT = 2;
  public static final int MAX_AGE_CHILD = 12;
  public static final int MAX_PAYMENT_RETRY_ATTEMPTS = 2;
  public static final String EXTRA_NUMBER_OF_BABIES = "NUMBER_OF_BABIES";
  public static final String EXTRA_NUMBER_OF_KIDS = "NUMBER_OF_KIDS";
  // CODE VELOCITY MESSAGES
  public static final String VEL_001 = "VEL-001";
  public static final String VEL_002 = "VEL-002";
  public static final String VEL_003 = "VEL-003";
  //public static final String GA_CUSTOM_DIMENSION = Configuration.getInstance()
  // .getCurrentMarket().getGACustomDim();
  public static final String PHONE = "Phone";
  public static final String PHONE_ABROAD = "PhoneAbroad";
  public static final int MAX_NUM_MULTI_IMAGES = 4;
  // REQUIRED FOR USE ON GATRACKING BY THE SEEKER BAR WIDGETS
  public static final String SEEKERBAR_DEPARTURE = "Departure";
  public static final String SEEKERBAR_ARRIVAL = "Arrival";
  public static final String SEEKERBAR_DURATION = "Duration";
  public static final int RATE_THE_APP_FIRST_SEARCHES_NUMBER = 5;
  public static final int RATE_THE_APP_SECOND_SEARCHES_NUMBER = 10;
  public static final int PROMOCODE_LENGTH = 2;
  public static final String PROMOCODE_KEY = "promocode_key";
  public static final int BLUR_FACTOR = 40;
  public static final String FRAGMENT_TAG_WAITING_RESULTS = "waitingResultsFrag";
  public static final String FRAGMENT_TAG_RESULTS = "ResultsFrag";
  public static final String WEBVIEW_TYPE = "webview_type";
  public static final String WEBVIEW_HOTELS = "webview_hotels";
  public static final String WEBVIEW_CARS = "webview_cars";
  public static final String FLIGHT_SEGMENTS_SIMPLE = "Flight Segments Simple";
  public static final String FLIGHT_SEGMENTS_ROUND = "Flight Segments Round";
  public static final String FLIGHT_SEGMENTS_MULTIPLE = "Flight Segments Multiple";
  public static final int ERROR_CODE_SPICE_TIMEOUT = 408;
  public static final int TIMEOUT_SEARCH = 180;
  public static final int TIMEOUT_AVAILABLE_PRODUCTS = 240;
  public static final int TIMEOUT_CONFIRM = 310;
  public static final int TIMEOUT_ADD_PERSONAL_INFO = 180;
  public static final int TIMEOUT_DEFAULT = 120;
  public static final String MAC_ADDRESS_EMPTY = "00:00:00:00:00:00";
  public static final String INSURANCES_WIDGET_V1 = "V1";
  public static final String INSURANCES_WIDGET_V2 = "V2";
  public static final long YEAR_IN_MILLISECONDS = 31556952000L;
  public static final long DAY_IN_MILLISECONDS = 86400000L;
  public static final String TIME_ZONE_GMT = "GMT+00:00";
  public static final String PREFERENCE_NEW_VERSION = "PREFERENCE_NEW_VERSION";
  public static final String WAS_APP_INSTALLED = "WAS_APP_INSTALLED";
  public static final String EXTRA_WIDGET_EMPTY = "EXTRA_WIDGET_EMPTY";
  public static final String EXTRA_WIDGET_SEARCH = "EXTRA_WIDGET_SEARCH";
  public static final String EXTRA_WIDGET_SEARCH_OPTIONS = "EXTRA_WIDGET_SEARCH_OPTIONS";
  public static final String TAG_NAME_BUYER_INFO = "Name";
  public static final String TAG_LASTNAME_BUYER_INFO = "LastName";
  public static final String TAG_EMAIL_BUYER_INFO = "Email";
  public static final String TAG_CITY_BUYER_INFO = "City";
  public static final String TAG_ADDRESS_BUYER_INFO = "Address";
  public static final String TAG_CP_BUYER_INFO = "PostalCode";
  public static final String TAG_PHONE_BUYER_INFO = "Phone";
  public static final String TAG_COUNTRY_BUYER_INFO = "Country";
  public static final String TAG_PHONEPREFIX_BUYER_INFO = "PhonePrefix";
  public static final int HTTP_OK = 200;
  public static final int HTTP_CONTINUE = 100;
  public static final String TITLE_WEB_ACTIVITY = "webViewTitle";
  public static final String SHOW_HOME_ICON = "showHomeIcon";
  /**
   * Index used by the market custom dimen for localytics, must be the same that the one used in
   * the web dashboard
   */
  public static final int LOCALYTICS_MARKET_CUSTOM_INDEX = 1;
  public static boolean isTestEnvironment = true;
  public static String domain;
  public static String CREDIT_CARD_NUMBER_OBFUSCATED = "CreditCardObfuscated";
  public static Bitmap cityBitmap = null;

  private Constants() {

  }

  /**
   * Sets if the application is un debug mode or release.
   *
   * @param isDebug boolean true or false.
   */
  public static void setIsTestEnvironment(boolean isDebug) {
    Constants.isTestEnvironment = isDebug;
  }

  public static final class CreditCard {

    //TODO: Update this type
    public static final String TYPE_JCB = "JC";
    public static final String TYPE_CARTOPODO = "CO";
    public static final String TYPE_VISA_CREDIT = "VI";
    public static final String TYPE_VISA_DEBIT = "VD";
    public static final String TYPE_MASTER_CARD_CREDIT = "CA";
    public static final String TYPE_MASTER_CARD_DEBIT = "MD";
    public static final String TYPE_AMERICAN_EXPRESS = "AX";
    public static final String TYPE_BLEUE_NATIONAL = "CB";
    public static final String TYPE_VISA_ENTROPAY = "E1";
    public static final String TYPE_DINERS_CLUB = "DC";

    public static final String VALIDATION_REGEX_JCB =
        "^(?:2131|2100|1800|(3088|3096|3112|3158|3337)[0-9]|35[0-9]{3})[0-9]{11}$";

    /**
     * Prevent instantiation
     */
    private CreditCard() {

    }
  }

  public static class ADX {
    public static final boolean IS_TRACKING = true;
    public static final String IS_UPDATE = "isUpdate";
    //The first version code, where ADX was released
    public static final int FIRST_VERSION_CODE = 13;
    public static final String EVENT_SEARCH = "Search";
    public static final String EVENT_SALE = "Sale";
    public static final String LAUNCH = "Launch";
    public static final int LOG_LEVEL = 0;
    public static final String ID = "ADXID";
    public static final String EVENT_LAUNCH_DEEP_LINK = "DeepLinkLaunch";
  }

  /**
   * Static class to store all the date formats used accross the applications
   */
  public static final class OdigeoDateFormat {

    /**
     * Date format for Google Analytics
     */
    public static final String GA_FORMAT = "ddMMyyyy";

    /**
     * Prevent instantiation
     */
    private OdigeoDateFormat() {

    }
  }

  public static final class DTOVersion {

    public static final int DEFAULT = 1;
    /**
     * DTOs where updated to match the V2 of the MSL
     */
    public static final int MSL_V2 = 2;
    public static final int BAGGAGE_DETAILS = 3;
    /**
     * After Spinners updates, Shopping car has a new cabinClass type
     */
    public static final int SPINNER_UPDATE = 4;
    public static final int CURRENT = SPINNER_UPDATE;

    /**
     * Prevent instantiation
     */
    private DTOVersion() {

    }
  }

  public class WalkthroughResponseCode {
    public static final int OK_RESPONSE = 1;
    public static final int EMPTY_RESPONSE = 0;
    public static final int ERROR_RESPONSE = 2;

    private WalkthroughResponseCode() {

    }
  }
}
