package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.interfaces.ISegmentWidget;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.BaggageIncludedDTO;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.LocationDTO;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.app.android.view.BaseCustomView;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.tools.DurationFormatter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class SegmentWidget extends BaseCustomView implements View.OnClickListener {

  //TODO SSO day text
  public static final int[] IS_THE_LAST_INFORM = { R.attr.isTheLastInForm };

  private boolean isChecked;

  private boolean lastInForm;

  private ImageView checkImageView;
  private TextView departureTimeTextView;
  private TextView arrivalTimeTextView;
  private TextView departurePlaceTextView;
  private TextView arrivalPlaceTextView;
  private TextView timeTravelTextView;
  private TextView travelTypeTextView;
  private ImageView imageTravelType;
  private TextView numberSeatsTextView;
  private SegmentWrapper segmentWrapper;
  private TextView operatingCarrierTextView;
  private LinearLayout mLlBaggageAllowance;
  private ImageView mIvBaggageAllowanceIcon;
  private TextView mTvBaggageAllowanceText;

  private ISegmentWidget listener;

  public SegmentWidget(Context context, SegmentWrapper segmentWrapper) {
    super(context);
    this.segmentWrapper = segmentWrapper;
  }

  public void setListener(ISegmentWidget listener) {
    this.listener = listener;
  }

  public DurationFormatter getDurationFormatter() {
    return dependencyInjector.provideDurationFormatter();
  }

  public boolean isChecked() {
    return isChecked;
  }

  public void setChecked(boolean isChecked) {
    this.isChecked = isChecked;

    getSegmentWrapper().getSegment().setSelected(isChecked);

    setImageChecked();
  }

  public void setDepartureTime(Date departureTime) {
    //SimpleDateFormat format = new SimpleDateFormat("HH:mm");
    //TODO: use format from class LocaleUtils
    SimpleDateFormat format = OdigeoDateUtils.getGmtDateFormat(
        Configuration.getInstance().getCurrentMarket().getTimeFormat());
    this.departureTimeTextView.setText(format.format(departureTime));
  }

  public void setArrivalTime(Date arrivalTime, int differenceDays) {
    //SimpleDateFormat format = new SimpleDateFormat("HH:mm");
    //TODO: use format from class LocaleUtils
    SimpleDateFormat format = OdigeoDateUtils.getGmtDateFormat(
        Configuration.getInstance().getCurrentMarket().getTimeFormat());
    String arrivalTimeText = differenceDays == 0 ? format.format(arrivalTime)
        : format.format(arrivalTime) + " (+" + differenceDays + ")";
    this.arrivalTimeTextView.setText(arrivalTimeText);
  }

  public void setDeparturePlace(LocationDTO departurePlace) {
    String departurePlaceString = departurePlace.getName()
        + " ("
        + departurePlace.getIataCode()
        + ") "
        + departurePlace.getCityName();
    this.departurePlaceTextView.setText(departurePlaceString);
  }

  public void setArrivalPlace(LocationDTO arrivalPlace) {
    String arrivalPlaceString = arrivalPlace.getName()
        + " ("
        + arrivalPlace.getIataCode()
        + ") "
        + arrivalPlace.getCityName();
    this.arrivalPlaceTextView.setText(arrivalPlaceString);
  }

  public void setTimeTravel(int timeTravel) {
    this.timeTravelTextView.setText(getDurationFormatter().format(timeTravel));
  }

  /**
   * Sets operating carrier from CarrierDTO object.
   */
  public void setOperatingCarrier(CarrierDTO operatingCarrier) {
    if (operatingCarrier != null) {
      operatingCarrierTextView.setText(HtmlUtils.formatHtml("<b>"
          + CommonLocalizables.getInstance().getCommonOperatedbytext(getContext())
          + "</b>"
          + " "
          + operatingCarrier.getName()));
      operatingCarrierTextView.setTypeface(Configuration.getInstance().getFonts().getRegular());

      operatingCarrierTextView.setVisibility(View.VISIBLE);
    } else {
      operatingCarrierTextView.setVisibility(View.GONE);
    }
  }

  public void setScales(int numScales) {
    if (numScales == 1) {
      travelTypeTextView.setText(
          CommonLocalizables.getInstance().getScalesfilterviewcontroller1scalesText(getContext()));
    } else if (numScales > 1) {
      travelTypeTextView.setText(CommonLocalizables.getInstance()
          .getScalesfilterviewcontrollerMorescalesText(getContext()));
    }
    imageTravelType.setImageResource(R.drawable.search_stops);
  }

  public void setExtraLines(String scale, long timeScale) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(timeScale);
    calendar.setTimeZone(TimeZone.getTimeZone("GMT"));
    int hour = calendar.get(Calendar.HOUR_OF_DAY);
    int minutes = calendar.get(Calendar.MINUTE);
    TextView scaleText = (TextView) findViewById(R.id.extra_linea1_resultswidget);

    scaleText.setText(HtmlUtils.formatHtml("<b>"
        + CommonLocalizables.getInstance().getExtraScales(getContext())
        + "</b>"
        + " "
        + scale
        + " ("
        + getDurationFormatter().format(hour, minutes)
        + ")"));

    scaleText.setTypeface(Configuration.getInstance().getFonts().getRegular());
    scaleText.setVisibility(VISIBLE);
  }

  public void setTextNumberSeats(String numberSeatsString) {
    this.numberSeatsTextView.setVisibility(VISIBLE);
    findViewById(R.id.icon_passengers).setVisibility(VISIBLE);
    String text = String.format(CommonLocalizables.getInstance()
        .getNoresultsviewcontrollerTabelseatsleft(getContext())
        .toString(), numberSeatsString);
    this.numberSeatsTextView.setText(text);
  }

  public void setBaggageAllowanceInfo(BaggageIncludedDTO baggageIncluded) {

    if (baggageIncluded == BaggageIncludedDTO.INCLUDED) {
      mLlBaggageAllowance.setVisibility(VISIBLE);
      mIvBaggageAllowanceIcon.setImageResource(R.drawable.checkin_baggage_included);
      mIvBaggageAllowanceIcon.setColorFilter(getResources().getColor(R.color.semantic_positive));
      mTvBaggageAllowanceText.setText(LocalizablesFacade.getString(getContext(),
          OneCMSKeys.RESULTSVIEWCONTROLLER_BAGGAGE_INCLUDED).toString());
      mTvBaggageAllowanceText.setTextColor(getResources().getColor(R.color.semantic_positive));
    } else if (baggageIncluded == BaggageIncludedDTO.NOT_INCLUDED) {
      mLlBaggageAllowance.setVisibility(VISIBLE);
      mIvBaggageAllowanceIcon.setImageResource(R.drawable.hand_luggage_included);
      mTvBaggageAllowanceText.setText(LocalizablesFacade.getString(getContext(),
          OneCMSKeys.RESULTSVIEWCONTROLLER_BAGGAGE_NOT_INCLUDED).toString());
      mTvBaggageAllowanceText.setTextColor(getResources().getColor(R.color.basic_light));
    } else {
      mLlBaggageAllowance.setVisibility(GONE);
    }
  }

  @Override public void onClick(View v) {
    setChecked(!isChecked());

    if (listener != null) {
      if (isChecked) {
        listener.onSegmentSelected(this);
      } else {
        listener.onSegmentUnselected(this);
      }
    }
  }

  private void setImageChecked() {
    if (checkImageView != null) {
      if (isChecked()) {
        checkImageView.setImageResource(R.drawable.checkbox_checked_clean);
      } else {
        checkImageView.setImageResource(R.drawable.checkbox_unchecked_clean);
      }
    }
  }

  public SegmentWrapper getSegmentWrapper() {
    return segmentWrapper;
  }

  public void setSegmentWrapper(SegmentWrapper segmentWrapper) {
    this.segmentWrapper = segmentWrapper;
  }

  public boolean isLastInForm() {
    return lastInForm;
  }

  public void setLastInForm(boolean lastInForm) {
    this.lastInForm = lastInForm;

    refreshDrawableState();
  }

  @Override protected int[] onCreateDrawableState(int extraSpace) {

    final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);

    if (isLastInForm()) {
      mergeDrawableStates(drawableState, IS_THE_LAST_INFORM);
    }

    return drawableState;
  }

  @Override public void initComponent() {

    checkImageView = (ImageView) findViewById(R.id.checkbox_resultswidget);
    departureTimeTextView = (TextView) findViewById(R.id.departure_hour);
    arrivalTimeTextView = (TextView) findViewById(R.id.arrival_hour);
    departurePlaceTextView = (TextView) findViewById(R.id.departure_place);
    arrivalPlaceTextView = (TextView) findViewById(R.id.arrival_place);
    timeTravelTextView = (TextView) findViewById(R.id.estimeted_time);
    travelTypeTextView = (TextView) findViewById(R.id.type_flight_info);
    travelTypeTextView.setText(
        CommonLocalizables.getInstance().getScalesfilterviewcontrollerNoscalesText(getContext()));
    operatingCarrierTextView = (TextView) findViewById(R.id.operating_carrier);
    numberSeatsTextView = (TextView) findViewById(R.id.number_passengers);
    imageTravelType = (ImageView) findViewById(R.id.type_flight_icon);
    mLlBaggageAllowance = (LinearLayout) findViewById(R.id.LlBaggageAllowance);
    mIvBaggageAllowanceIcon = (ImageView) findViewById(R.id.IvBaggageAllowanceIcon);
    mTvBaggageAllowanceText = (TextView) findViewById(R.id.TvBaggageAllowanceText);

    Fonts fonts = Configuration.getInstance().getFonts();

    departureTimeTextView.setTypeface(fonts.getBold());
    arrivalTimeTextView.setTypeface(fonts.getBold());

    departurePlaceTextView.setTypeface(fonts.getRegular());
    arrivalPlaceTextView.setTypeface(fonts.getRegular());
    timeTravelTextView.setTypeface(fonts.getRegular());
    travelTypeTextView.setTypeface(fonts.getRegular());
    numberSeatsTextView.setTypeface(fonts.getRegular());
    mTvBaggageAllowanceText.setTypeface(fonts.getRegular());

    this.setOnClickListener(this);
  }

  @Override public int getComponentLayout() {
    return R.layout.layout_resultswidget_travel;
  }

  @Override public void initOneCMSText() {

  }
}
