package com.odigeo.app.android.lib.models;

import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.SectionDTO;
import com.odigeo.app.android.lib.models.dto.SegmentDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by manuel on 09/10/14.
 */
public class SegmentWrapper implements Serializable {
  private String key;
  private SegmentDTO segment;

  private CarrierDTO carrier;

  //This information is not provided by the rest service
  private List<SectionDTO> sectionsObjects = new ArrayList<SectionDTO>();

  public SegmentWrapper() {
  }

  public SegmentWrapper(String key, SegmentDTO segment) {
    this.key = key;
    this.setSegment(segment);
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public SegmentDTO getSegment() {
    return segment;
  }

  public final void setSegment(SegmentDTO segment) {
    this.segment = segment;
  }

  public CarrierDTO getCarrier() {
    return carrier;
  }

  public void setCarrier(CarrierDTO carrier) {
    this.carrier = carrier;
  }

  public List<SectionDTO> getSectionsObjects() {
    return sectionsObjects;
  }

  public void setSectionsObjects(List<SectionDTO> sectionsObjects) {
    this.sectionsObjects = sectionsObjects;
  }
}
