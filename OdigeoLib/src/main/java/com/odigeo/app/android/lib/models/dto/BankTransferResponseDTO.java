package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.List;

public class BankTransferResponseDTO implements Serializable {

  protected List<BankAccountResponseDTO> bankAccountResponses;
  protected String notificationFax;
  protected String notificationEmail;

  public List<BankAccountResponseDTO> getBankAccountResponses() {
    return bankAccountResponses;
  }

  public void setBankAccountResponses(List<BankAccountResponseDTO> bankAccountResponses) {
    this.bankAccountResponses = bankAccountResponses;
  }

  /**
   * Gets the value of the notificationFax property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getNotificationFax() {
    return notificationFax;
  }

  /**
   * Sets the value of the notificationFax property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setNotificationFax(String value) {
    this.notificationFax = value;
  }

  /**
   * Gets the value of the notificationEmail property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getNotificationEmail() {
    return notificationEmail;
  }

  /**
   * Sets the value of the notificationEmail property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setNotificationEmail(String value) {
    this.notificationEmail = value;
  }
}
