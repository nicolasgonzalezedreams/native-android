package com.odigeo.app.android.lib.data.networking;

import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.utils.BaggageConditionsFieldNamingStrategy;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import org.apache.http.protocol.HTTP;

/**
 * Created by sky on 12/05/2015. Volley adapter for JSON requests that will be parsed into Java
 * objects by Gson.
 */
public class GsonRequest<T> extends JsonRequest<T> {
  private final Class<T> mClassType;
  private final Map<String, String> mHeaders;
  private final Response.Listener<T> mListener;

  public GsonRequest(int method, String url, Class<T> classType, Map<String, String> headers,
      Response.Listener<T> listener, Response.ErrorListener errorListener) {
    this(method, url, classType, headers, null, listener, errorListener);
  }

  public GsonRequest(int method, String url, Class<T> classType, Map<String, String> headers,
      String jsonRequest, Response.Listener<T> listener, Response.ErrorListener errorListener) {
    super(method, url, jsonRequest, listener, errorListener);
    mClassType = classType;
    mHeaders = headers;
    mListener = listener;
  }

  @Override public Map<String, String> getHeaders() throws AuthFailureError {
    return mHeaders != null ? mHeaders : super.getHeaders();
  }

  @Override protected void deliverResponse(T response) {
    mListener.onResponse(response);
  }

  @Override protected Response<T> parseNetworkResponse(NetworkResponse response) {
    try {

      final String encoding = response.headers.get(HTTP.CONTENT_ENCODING);
      String responseString = "";
      if (encoding != null && encoding.contains("gzip")) {
        responseString = decodeGZip(response.data);
      } else {
        responseString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
      }

      return Response.success(processContent(responseString),
          HttpHeaderParser.parseCacheHeaders(response));
    } catch (UnsupportedEncodingException e) {
      return Response.error(new ParseError(e));
    } catch (JsonSyntaxException e) {
      return Response.error(new ParseError(e));
    } catch (Exception e) {
      return Response.error(new ParseError(e));
    }
  }

  private String decodeGZip(byte[] data) {
    StringBuffer responseString = new StringBuffer("");

    ByteArrayInputStream bais = null;
    GZIPInputStream gzis = null;
    InputStreamReader reader = null;
    BufferedReader in = null;

    try {
      bais = new ByteArrayInputStream(data);
      gzis = new GZIPInputStream(bais);
      reader = new InputStreamReader(gzis);
      in = new BufferedReader(reader);

      String read = in.readLine();
      while (read != null) {
        responseString.append(read);
        responseString.append(System.getProperty("line.separator"));
        read = in.readLine();
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (bais != null) {
          bais.close();
        }
        if (gzis != null) {
          gzis.close();
        }
        if (reader != null) {
          reader.close();
        }
        if (in != null) {
          in.close();
        }
      } catch (Exception e) {
        Logger.getLogger(GsonRequest.class.getName()).log(Level.SEVERE, Constants.TAG_LOG, e);
      }
    }

    return responseString.toString();
  }

  protected T processContent(String responseBody) {

    Log.d(Constants.TAG_LOG, "MSL response:\n" + responseBody);

    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.setVersion(Constants.DTOVersion.CURRENT);
    gsonBuilder.setFieldNamingStrategy(
        new BaggageConditionsFieldNamingStrategy(Constants.DTOVersion.CURRENT));

    Gson gson = gsonBuilder.create();

    T gsonResponse = null;

    try {
      gsonResponse = (T) gson.fromJson(responseBody, mClassType);
    } catch (Exception e) {
      Log.e(Constants.TAG_LOG, e.getMessage());
    }

    return gsonResponse;
  }
}