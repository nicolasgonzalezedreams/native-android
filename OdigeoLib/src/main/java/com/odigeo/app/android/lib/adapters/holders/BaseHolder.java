package com.odigeo.app.android.lib.adapters.holders;

import android.view.View;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 12/01/16
 */
public class BaseHolder {

  /**
   * Constructor
   *
   * @param view View where will be extracted the view's references.
   */
  public BaseHolder(View view) {
    // Nothing
  }
}
