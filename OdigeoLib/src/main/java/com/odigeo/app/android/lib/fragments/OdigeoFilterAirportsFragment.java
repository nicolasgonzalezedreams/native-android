package com.odigeo.app.android.lib.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoFiltersActivity;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.models.Airports;
import com.odigeo.app.android.lib.models.dto.LocationDTO;
import com.odigeo.app.android.lib.ui.widgets.FilterListWidget;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Irving Lóp on 14/10/2014.
 */
public class OdigeoFilterAirportsFragment extends Fragment {

  private List<FilterListWidget> listWidgets;
  private LinearLayout layoutContainer;

  private OdigeoFiltersActivity parentActivity;
  private OdigeoApp odigeoApp;
  private OdigeoSession odigeoSession;

  @Override public final void onAttach(Activity activity) {
    super.onAttach(activity);
    parentActivity = (OdigeoFiltersActivity) activity;

    // GAnalytics screen tracking - Filter airlines fragment
    BusProvider.getInstance()
        .post(new GAScreenTrackingEvent(GAnalyticsNames.SCREEN_FILTERS_AIRPORTS));
  }

  @Override public final View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.layout_filter_aeroport_fragment, container, false);
    odigeoApp = (OdigeoApp) parentActivity.getApplication();
    odigeoSession = odigeoApp.getOdigeoSession();

    if (odigeoSession != null && odigeoSession.isDataHasBeenLoaded()) {

      layoutContainer =
          (LinearLayout) view.findViewById(R.id.layout_container_list_filter_aeroport);
      drawWidgets();
    }
    return view;
  }

  @Override public final void onViewCreated(View view, Bundle savedInstanceState) {

    super.onViewCreated(view, savedInstanceState);
    if (odigeoSession == null || !odigeoSession.isDataHasBeenLoaded()) {
      Util.sendToHome(parentActivity, odigeoApp);
    }
  }

  private void drawWidgets() {
    layoutContainer.removeAllViewsInLayout();
    listWidgets = new ArrayList<FilterListWidget>();

    for (Airports airport : parentActivity.getListAirports()) {
      FilterListWidget widget = new FilterListWidget(getActivity());
      widget.setUsedOnAirportFilter(true);
      listWidgets.add(widget);
      widget.setSegmentNumber(listWidgets.size());
      layoutContainer.addView(widget);
      ViewUtils.setSeparator(widget, getActivity().getApplicationContext());

      widget.setCityName(airport.getTitle());

      List<String> lstAirports = new ArrayList<String>();

      for (LocationDTO city : airport.getAirports()) {
        widget.setNoFormatCityName(airport.getTitle());
        lstAirports.add(city.getName());
      }

      //            widget.setValues(lstAirports);
      widget.setAirports(airport.getAirports());

      if (parentActivity.getFiltersSelected() != null
          && parentActivity.getFiltersSelected().getAirports() != null) {

        List<String> lstAirportsSelected = new ArrayList<String>();

        for (String airportName : lstAirports) {
          for (LocationDTO location : parentActivity.getFiltersSelected().getAirports()) {
            if (location.getName().equals(airportName)) {
              lstAirportsSelected.add(airportName);
              break;
            }
          }
        }

        widget.setSelectedNames(lstAirportsSelected);
      }
    }
  }

  public final List<LocationDTO> getAirportsSelected() {
    List<LocationDTO> airportsFiltered = new ArrayList<LocationDTO>();

    for (FilterListWidget listWidget : listWidgets) {

      List<String> listAirportsSelected = listWidget.getSelectedNames();

      List<LocationDTO> airportsAvailable = null;

      for (Airports airports : parentActivity.getListAirports()) {
        if (airports.getTitle().equals(listWidget.getCityName())) {
          airportsAvailable = airports.getAirports();
        }
      }

      for (String airportName : listAirportsSelected) {
        for (LocationDTO location : airportsAvailable) {
          if (airportName.equals(location.getName())) {
            airportsFiltered.add(location);
          }
        }
      }
    }

    return airportsFiltered;
  }

  public boolean isAirportsSelectedEmpty(int index) {
    FilterListWidget listWidget = listWidgets.get(index);
    List<String> listAirportsSelected = listWidget.getSelectedNames();
    return listAirportsSelected.isEmpty();
  }

  /**
   * This method evaluates if selection of Airports is valid. The position given by the index 1 is
   * valid to have it empty since it has the stops. TODO: Modify when the sort of the Airports
   * change
   *
   * @return true if the selection is valid, false when position 0 and 2 is empty
   */
  public boolean isAirportsSelectedValid() {
    int index = 0;
    for (FilterListWidget listWidget : listWidgets) {
      if (index != 1 && listWidget.getSelectedNames().isEmpty()) {
        return false;
      }
      index++;
    }
    return true;
  }
}
