package com.odigeo.app.android.lib.config;

import java.util.List;

/**
 * Created by ManuelOrtiz on 22/09/2014.
 */
public class Market {
  /**
   * WebSite of the market obtained from the XML configuration file
   */
  private String website;
  /**
   * Language of the market obtained from the XML configuration file
   */
  private String language;
  /**
   * Locale of the market obtained from the XML configuration file
   */
  private String locale;
  /**
   * Currency of the market obtained from the XML configuration file
   */
  private boolean hasCo2;
  private String co2Link;

  // One CMS Table.
  private String oneCmsTable;

  private String currencyKey;

  private String name;
  private String key;

  private String twitterUser;
  private List<ResidentItinerary> residentItineraries;
  private String gaCustomDim;
  private boolean needsExplicitAcceptance;
  private String insurancesVersion;
  private boolean forcingLatingKeyboard;
  private String timeFormat;

  private String crossSellingMarketKey;

  private String xSellingSearchResultsHotelsUrl;
  private String xSellingFilterHotelsUrl;

  private String xSellingSearchResultsCarsUrl;
  private String xSellingFilterCarsUrl;

  private boolean insuranceEuropeanConditions;

  public final String getWebsite() {
    return website;
  }

  public final void setWebsite(String website) {
    this.website = website;
  }

  public final String getLanguage() {
    return language;
  }

  public final void setLanguage(String language) {
    this.language = language;
  }

  public final String getLocale() {
    return locale;
  }

  public final void setLocale(String locale) {
    this.locale = locale;
  }

  public final String getOneCMSTable() {
    return this.oneCmsTable;
  }

  public final void setOneCMSTable(String oneCmsTable) {
    this.oneCmsTable = oneCmsTable;
  }

  public final boolean getHasCo2() {
    return hasCo2;
  }

  public final void setHasCo2(boolean hasCo2) {
    this.hasCo2 = hasCo2;
  }

  public final String getCo2Link() {
    return co2Link;
  }

  public final void setCo2Link(String co2Link) {
    this.co2Link = co2Link;
  }

  public final String getName() {
    return name;
  }

  public final void setName(String name) {
    this.name = name;
  }

  public final String getxSellingSearchResultsHotelsUrl() {
    return xSellingSearchResultsHotelsUrl;
  }

  public final String getxSellingFilterHotelsUrl() {
    return xSellingFilterHotelsUrl;
  }

  public final String getKey() {
    return key;
  }

  public final void setKey(String key) {
    this.key = key;
  }

  public final String getTwitterUser() {
    return twitterUser;
  }

  public final void setTwitterUser(String twitterUser) {
    this.twitterUser = twitterUser;
  }

  public final List<ResidentItinerary> getResidentItineraries() {
    return residentItineraries;
  }

  public final void setResidentItineraries(List<ResidentItinerary> residentItineraries) {
    this.residentItineraries = residentItineraries;
  }

  public final String getGACustomDim() {
    return this.gaCustomDim;
  }

  public final void setGACustomDim(String gaCustomDim) {
    this.gaCustomDim = gaCustomDim;
  }

  public final boolean getNeedsExplicitAcceptance() {
    return needsExplicitAcceptance;
  }

  public final void setNeedsExplicitAcceptance(boolean needsExplicitAcceptance) {
    this.needsExplicitAcceptance = needsExplicitAcceptance;
  }

  public final String getInsurancesVersion() {
    return insurancesVersion;
  }

  public final void setInsurancesVersion(String insurancesVersion) {
    this.insurancesVersion = insurancesVersion;
  }

  public final boolean getForcingLatingKeyboard() {
    return forcingLatingKeyboard;
  }

  public final void setForcingLatingKeyboard(boolean forcingLatingKeyboard) {
    this.forcingLatingKeyboard = forcingLatingKeyboard;
  }

  public final String getTimeFormat() {
    return timeFormat;
  }

  public final void setTimeFormat(String timeFormat) {
    this.timeFormat = timeFormat;
  }

  public final String getCrossSellingMarketKey() {
    return this.crossSellingMarketKey;
  }

  public final void setCrossSellingMarketKey(String crossSellingMarketKey) {
    this.crossSellingMarketKey = crossSellingMarketKey;
  }

  public final void setXSellingSearchResultsHotelsUrl(String xSellingSearchResultsHotelsUrl) {
    this.xSellingSearchResultsHotelsUrl = xSellingSearchResultsHotelsUrl;
  }

  public final void setXSellingFilterHotelsUrl(String xSellingFilterHotelsUrl) {
    this.xSellingFilterHotelsUrl = xSellingFilterHotelsUrl;
  }

  public final String getxSellingFilterCarsUrl() {
    return xSellingFilterCarsUrl;
  }

  public final void setxSellingFilterCarsUrl(String xSellingFilterCarsUrl) {
    this.xSellingFilterCarsUrl = xSellingFilterCarsUrl;
  }

  public final String getxSellingSearchResultsCarsUrl() {
    return xSellingSearchResultsCarsUrl;
  }

  public final void setxSellingSearchResultsCarsUrl(String xSellingSearchResultsCarsUrl) {
    this.xSellingSearchResultsCarsUrl = xSellingSearchResultsCarsUrl;
  }

  public final String getCurrencyKey() {
    return currencyKey;
  }

  public final void setCurrencyKey(String currencyKey) {
    this.currencyKey = currencyKey;
  }

  public final boolean isInsuranceEuropeanConditions() {
    return insuranceEuropeanConditions;
  }

  public final void setInsuranceEuropeanConditions(boolean insuranceEuropeanConditions) {
    this.insuranceEuropeanConditions = insuranceEuropeanConditions;
  }
}
