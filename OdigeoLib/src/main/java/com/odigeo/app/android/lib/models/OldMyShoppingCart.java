package com.odigeo.app.android.lib.models;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.models.dto.BankTransferDataDTO;
import com.odigeo.app.android.lib.models.dto.BankTransferResponseDTO;
import com.odigeo.app.android.lib.models.dto.BookingSummaryStatus;
import com.odigeo.app.android.lib.models.dto.BuyerDTO;
import com.odigeo.app.android.lib.models.dto.BuyerInformationDescriptionDTO;
import com.odigeo.app.android.lib.models.dto.InsuranceShoppingItemDTO;
import com.odigeo.app.android.lib.models.dto.MoneyDTO;
import com.odigeo.app.android.lib.models.dto.ResidentsValidationDTO;
import com.odigeo.app.android.lib.models.dto.SectionDTO;
import com.odigeo.app.android.lib.models.dto.SectionResultDTO;
import com.odigeo.app.android.lib.models.dto.ShoppingCartCollectionOptionDTO;
import com.odigeo.app.android.lib.models.dto.ShoppingCartDTO;
import com.odigeo.app.android.lib.models.dto.ShoppingCartPriceCalculatorDTO;
import com.odigeo.app.android.lib.models.dto.TravellerDTO;
import com.odigeo.app.android.lib.models.dto.TravellerInformationDescriptionDTO;
import com.odigeo.app.android.lib.models.dto.custom.PricingBreakdown;
import com.odigeo.data.entity.shoppingCart.StepsConfiguration;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


@Deprecated public class OldMyShoppingCart implements Serializable {
  private List<SegmentWrapper> segmentsWrapper;
  private List<InsuranceShoppingItemDTO> insurances;
  private SearchOptions searchOptions;
  private PricingBreakdown pricingBreakdown;
  private Long bookingId;
  private List<TravellerInformationDescriptionDTO> requiredTravellerInformations;
  private BuyerInformationDescriptionDTO requiredBuyerInformation;
  private BuyerDTO buyer;
  private List<TravellerDTO> travellers;
  private String clientBookingReferenceId;
  private List<String> clientBookingReferenceIds;
  private CollectionMethodWithPrice fullPriceMethodWithPriceSelected;
  private ShoppingCartCollectionOptionDTO paymentMethodWithPriceSelected;
  private List<StepsConfiguration> stepsFlowConfiguration;
  private MoneyDTO price;
  private BigDecimal totalPrice;
  private BigDecimal collectionTotalFee;
  private ShoppingCartPriceCalculatorDTO shoppingCartPrice;
  private ResidentsValidationDTO residentsValidation;
  private List<SectionResultDTO> sectionResults;
  private String locale;
  private Integer dtoVersion;
  private BankTransferResponseDTO bankTransferResponseDTO;
  private List<BankTransferDataDTO> bankTransferDataDTOs;
  private long creationDate;

  /**
   * Current booking status
   */
  private BookingSummaryStatus bookingSummaryStatus;

  public OldMyShoppingCart() {

    bookingSummaryStatus = BookingSummaryStatus.PENDING;
  }

  public BookingSummaryStatus getBookingSummaryStatus() {
    return bookingSummaryStatus;
  }

  public void setBookingSummaryStatus(BookingSummaryStatus bookingSummaryStatus) {
    this.bookingSummaryStatus = bookingSummaryStatus;
  }

  public final void setPrices(ShoppingCartDTO shoppingCart) {
    if (shoppingCart != null) {
      this.totalPrice = shoppingCart.getTotalPrice();
      this.collectionTotalFee = shoppingCart.getCollectionTotalFee();
      this.shoppingCartPrice = shoppingCart.getPrice();
    }
  }

  public final List<SegmentWrapper> getSegmentsWrapper() {
    return segmentsWrapper;
  }

  public final void setSegmentsWrapper(List<SegmentWrapper> segmentsWrapper) {
    this.segmentsWrapper = segmentsWrapper;
  }

  public final List<InsuranceShoppingItemDTO> getInsurances() {
    return insurances;
  }

  public final void setInsurances(List<InsuranceShoppingItemDTO> insurances) {
    this.insurances = insurances;
  }

  public final SearchOptions getSearchOptions() {
    return searchOptions;
  }

  public final void setSearchOptions(SearchOptions searchOptions) {
    this.searchOptions = searchOptions;
  }

  public final PricingBreakdown getPricingBreakdown() {
    return pricingBreakdown;
  }

  public final void setPricingBreakdown(PricingBreakdown pricingBreakdown) {
    this.pricingBreakdown = pricingBreakdown;
  }

  public final Long getBookingId() {
    return bookingId;
  }

  public final void setBookingId(Long bookingId) {
    this.bookingId = bookingId;
  }

  public final List<TravellerInformationDescriptionDTO> getRequiredTravellerInformations() {
    return requiredTravellerInformations;
  }

  public final void setRequiredTravellerInformations(
      List<TravellerInformationDescriptionDTO> requiredTravellerInformations) {
    this.requiredTravellerInformations = requiredTravellerInformations;
  }

  public final BuyerInformationDescriptionDTO getRequiredBuyerInformation() {
    return requiredBuyerInformation;
  }

  public final void setRequiredBuyerInformation(
      BuyerInformationDescriptionDTO requiredBuyerInformation) {
    this.requiredBuyerInformation = requiredBuyerInformation;
  }

  @NonNull public final BuyerDTO getBuyer() {
    return buyer;
  }

  public final void setBuyer(BuyerDTO buyer) {
    this.buyer = buyer;
  }

  public final List<TravellerDTO> getTravellers() {
    return travellers;
  }

  public final void setTravellers(List<TravellerDTO> travellers) {
    this.travellers = travellers;
  }

  public final String getClientBookingReferenceId() {
    return clientBookingReferenceId;
  }

  public final void setClientBookingReferenceId(String clientBookingReferenceId) {
    this.clientBookingReferenceId = clientBookingReferenceId;
  }

  public final List<String> getClientBookingReferenceIds() {
    return clientBookingReferenceIds;
  }

  public final void setClientBookingReferenceIds(List<String> clientBookingReferenceIds) {
    this.clientBookingReferenceIds = clientBookingReferenceIds;
  }

  public final CollectionMethodWithPrice getFullPriceMethodWithPriceSelected() {
    return fullPriceMethodWithPriceSelected;
  }

  public final void setFullPriceMethodWithPriceSelected(
      CollectionMethodWithPrice fullPriceMethodWithPriceSelected) {
    this.fullPriceMethodWithPriceSelected = fullPriceMethodWithPriceSelected;
  }

  public final List<StepsConfiguration> getStepsFlowConfiguration() {
    return stepsFlowConfiguration;
  }

  public final void setStepsFlowConfiguration(List<StepsConfiguration> stepsFlowConfiguration) {
    this.stepsFlowConfiguration = stepsFlowConfiguration;
  }

  public final MoneyDTO getPrice() {
    return price;
  }

  public final void setPrice(MoneyDTO price) {
    this.price = price;
  }

  public final BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public final void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }

  public final BigDecimal getCollectionTotalFee() {
    return collectionTotalFee;
  }

  public final void setCollectionTotalFee(BigDecimal collectionTotalFee) {
    this.collectionTotalFee = collectionTotalFee;
  }

  public final ShoppingCartPriceCalculatorDTO getShoppingCartPrice() {
    return shoppingCartPrice;
  }

  public final void setShoppingCartPrice(ShoppingCartPriceCalculatorDTO shoppingCartPrice) {
    this.shoppingCartPrice = shoppingCartPrice;
  }

  public final ShoppingCartCollectionOptionDTO getPaymentMethodWithPriceSelected() {
    return paymentMethodWithPriceSelected;
  }

  public final void setPaymentMethodWithPriceSelected(
      ShoppingCartCollectionOptionDTO paymentMethodWithPriceSelected) {
    this.paymentMethodWithPriceSelected = paymentMethodWithPriceSelected;
  }

  public final ResidentsValidationDTO getResidentsValidation() {
    return residentsValidation;
  }

  public final void setResidentsValidation(ResidentsValidationDTO residentsValidation) {
    this.residentsValidation = residentsValidation;
  }

  public final List<SectionResultDTO> getSectionsResults() {
    return this.sectionResults;
  }

  public final void setSectionsResults(List<SectionResultDTO> sectionResults) {
    this.sectionResults = sectionResults;
  }

  public final String getLocale() {
    if (locale == null || TextUtils.isEmpty(locale)) {
      return "en_US";
    } else {
      return locale;
    }
  }

  public final void setLocale(String locale) {
    this.locale = locale;
  }

  public final Integer getDtoVersion() {
    return dtoVersion;
  }

  public final void setDtoVersion(Integer dtoVersion) {
    this.dtoVersion = dtoVersion;
  }

  public BankTransferResponseDTO getBankTransferResponseDTO() {
    return bankTransferResponseDTO;
  }

  public void setBankTransferResponseDTO(BankTransferResponseDTO bankTransferResponseDTO) {
    this.bankTransferResponseDTO = bankTransferResponseDTO;
  }

  public List<BankTransferDataDTO> getBankTransferDataDTOs() {
    return bankTransferDataDTOs;
  }

  public void setBankTransferDataDTOs(List<BankTransferDataDTO> bankTransferDataDTOs) {
    this.bankTransferDataDTOs = bankTransferDataDTOs;
  }

  public long getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(long creationDate) {
    this.creationDate = creationDate;
  }

  public long getArrivalDate() {
    long arrivalDate = 0;

    SectionDTO section = segmentsWrapper.get(0)
        .getSectionsObjects()
        .get(segmentsWrapper.get(0).getSectionsObjects().size() - 1);

    if (section != null) {
      arrivalDate = section.getArrivalDate();
    }

    return arrivalDate;
  }

  public long getLastDepartureDate() {
    long departureDate = 0;

    SectionDTO section = segmentsWrapper.get(segmentsWrapper.size() - 1)
        .getSectionsObjects()
        .get(segmentsWrapper.get(segmentsWrapper.size() - 1).getSectionsObjects().size() - 1);

    if (section != null) {
      departureDate = section.getDepartureDate();
    }

    return departureDate;
  }

  public String getArrivalAirportCode() {
    String arrivalAirportCode = "";

    SectionDTO section = segmentsWrapper.get(0).getSectionsObjects().get(0);

    if (section != null) {
      arrivalAirportCode = section.getArrivalTerminal();
    }

    return arrivalAirportCode;
  }

  /**
   * Calculate the total value for tickets.
   *
   * @return The amount for insurances.
   */
  //    public double getTicketsTotal(PricingBreakdown pricingBreakdown) {
  //        double total = 0;
  //        int lastStep = pricingBreakdown.getPricingBreakdownSteps().size() - 1;
  //        PricingBreakdownStep step = pricingBreakdown.getPricingBreakdownSteps()
  //                .get(lastStep);
  //        for (PricingBreakdownItem item : step.getPricingBreakdownItems()) {
  //            switch (item.getPriceItemType()) {
  //            case ADULTS_PRICE:
  //            case CHILDREN_PRICE:
  //            case INFANTS_PRICE:
  //                total += item.getPriceItemAmount();
  //                break;
  //            }
  //        }
  //        return total;
  //    }

  /**
   * Calculate the total value for insurances.
   *
   * @return The amount for insurances.
   */
  //    public double getInsurancesTotal(PricingBreakdown pricingBreakdown) {
  //        double total = 0;
  //        int lastStep = pricingBreakdown.getPricingBreakdownSteps().size() - 1;
  //        PricingBreakdownStep step = pricingBreakdown.getPricingBreakdownSteps().get(lastStep);
  //        for (PricingBreakdownItem item : step.getPricingBreakdownItems()) {
  //            switch (item.getPriceItemType()) {
  //            case INSURANCE_PRICE:
  //                total += item.getPriceItemAmount();
  //                break;
  //            }
  //        }
  //        return total;
  //    }
}