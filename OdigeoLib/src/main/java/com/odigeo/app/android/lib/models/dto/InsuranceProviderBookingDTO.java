package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class InsuranceProviderBookingDTO {

  protected InsuranceDTO insurance;
  protected List<InsuranceBookingCoveragePassengerPriceDTO> passengerProviderPrice;
  protected BigDecimal totalPrice;
  protected BookingStatusDTO bookingStatus;
  protected BigDecimal providerPrice;
  protected boolean selectable;

  /**
   * Gets the value of the insurance property.
   *
   * @return possible object is
   * {@link com.odigeo.app.android.lib.models.dto.InsuranceDTO }
   */
  public InsuranceDTO getInsurance() {
    return insurance;
  }

  /**
   * Sets the value of the insurance property.
   *
   * @param value allowed object is
   * {@link com.odigeo.app.android.lib.models.dto.InsuranceDTO }
   */
  public void setInsurance(InsuranceDTO value) {
    this.insurance = value;
  }

  /**
   * Gets the value of the passengerProviderPrice property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the passengerProviderPrice property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getPassengerProviderPrice().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list
   * {@link com.odigeo.app.android.lib.models.dto.InsuranceBookingCoveragePassengerPriceDTO }
   */
  public List<InsuranceBookingCoveragePassengerPriceDTO> getPassengerProviderPrice() {
    if (passengerProviderPrice == null) {
      passengerProviderPrice = new ArrayList<InsuranceBookingCoveragePassengerPriceDTO>();
    }
    return this.passengerProviderPrice;
  }

  public void setPassengerProviderPrice(
      List<InsuranceBookingCoveragePassengerPriceDTO> passengerProviderPrice) {
    this.passengerProviderPrice = passengerProviderPrice;
  }

  /**
   * Gets the value of the totalPrice property.
   *
   * @return possible object is
   * {@link java.math.BigDecimal }
   */
  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  /**
   * Sets the value of the totalPrice property.
   *
   * @param value allowed object is
   * {@link java.math.BigDecimal }
   */
  public void setTotalPrice(BigDecimal value) {
    this.totalPrice = value;
  }

  /**
   * Gets the value of the bookingStatus property.
   *
   * @return possible object is
   * {@link com.odigeo.app.android.lib.models.dto.BookingStatusDTO }
   */
  public BookingStatusDTO getBookingStatus() {
    return bookingStatus;
  }

  /**
   * Sets the value of the bookingStatus property.
   *
   * @param value allowed object is
   * {@link com.odigeo.app.android.lib.models.dto.BookingStatusDTO }
   */
  public void setBookingStatus(BookingStatusDTO value) {
    this.bookingStatus = value;
  }

  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  public void setProviderPrice(BigDecimal providerPrice) {
    this.providerPrice = providerPrice;
  }

  public boolean isSelectable() {
    return selectable;
  }

  public void setSelectable(boolean selectable) {
    this.selectable = selectable;
  }
}

