package com.odigeo.app.android.lib.models;

import java.io.Serializable;

/**
 * Created by manuel on 06/10/14.
 */
public class Gender implements Serializable {
  private String id;
  private String text;
  private String stringId;

  public Gender(String id) {
    this.id = id;
  }

  public Gender(String id, String stringId) {
    this.id = id;
    this.stringId = stringId;
  }

  public final String getId() {
    return id;
  }

  public final void setId(String id) {
    this.id = id;
  }

  public final String getText() {
    return text;
  }

  public final void setText(String text) {
    this.text = text;
  }

  public final String getStringId() {
    return stringId;
  }

  public final void setStringId(String stringId) {
    this.stringId = stringId;
  }
}
