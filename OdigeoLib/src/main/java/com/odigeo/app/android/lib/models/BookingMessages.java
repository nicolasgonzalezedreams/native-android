package com.odigeo.app.android.lib.models;

import com.odigeo.app.android.lib.models.dto.MessageResponseDTO;
import java.io.Serializable;
import java.util.List;

public class BookingMessages implements Serializable {

  protected List<MessageResponseDTO> messages;

  /**
   * @return the messages
   */
  public final List<MessageResponseDTO> getMessages() {
    return messages;
  }

  /**
   * @param messages the messages to set
   */
  public final void setMessages(List<MessageResponseDTO> messages) {
    this.messages = messages;
  }

  /**
   * @return the extensions
   */
}
