package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class RepricingInfoResponseDTO {

  protected BigDecimal amount;
  protected boolean absorption;

  /**
   * Gets the value of the amount property.
   *
   * @return possible object is
   * {@link BigDecimal }
   */
  public BigDecimal getAmount() {
    return amount;
  }

  /**
   * Sets the value of the amount property.
   *
   * @param value allowed object is
   * {@link BigDecimal }
   */
  public void setAmount(BigDecimal value) {
    this.amount = value;
  }

  /**
   * Gets the value of the absorption property.
   */
  public boolean isAbsorption() {
    return absorption;
  }

  /**
   * Sets the value of the absorption property.
   */
  public void setAbsorption(boolean value) {
    this.absorption = value;
  }
}
