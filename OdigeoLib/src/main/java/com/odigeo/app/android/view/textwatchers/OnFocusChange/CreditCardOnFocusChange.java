package com.odigeo.app.android.view.textwatchers.OnFocusChange;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.LinearLayout;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.RequestValidationHandler;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.contracts.views.PaymentFormWidgetInterface;

public class CreditCardOnFocusChange extends BaseOnFocusChange {

  private final TextInputLayout tilCreditCard;
  private final TrackerControllerInterface tracker;
  private boolean mThereWasFocusAtLeastOnce = false;
  private LinearLayout llCVVTooltip;
  private PaymentFormWidgetInterface paymentFormWidget;
  private RequestValidationHandler requestValidationHandler;

  public CreditCardOnFocusChange(Context context, TextInputLayout textInputLayout,
      FieldValidator fieldValidator, String oneCMSKey, TrackerControllerInterface tracker) {
    super(context, textInputLayout, fieldValidator, oneCMSKey);
    this.tilCreditCard = textInputLayout;
    this.tracker = tracker;
  }

  public CreditCardOnFocusChange(Context context, TextInputLayout textInputLayout,
      FieldValidator fieldValidator, String oneCMSKey, TrackerControllerInterface tracker,
      LinearLayout llCVVTooltip, PaymentFormWidgetInterface paymentFormWidget,
      RequestValidationHandler requestValidationHandler) {
    super(context, textInputLayout, fieldValidator, oneCMSKey);
    this.tilCreditCard = textInputLayout;
    this.tracker = tracker;
    this.llCVVTooltip = llCVVTooltip;
    this.paymentFormWidget = paymentFormWidget;
    this.requestValidationHandler = requestValidationHandler;
  }

  @Override public void onFocusChange(View v, boolean hasFocus) {
    super.onFocusChange(v, hasFocus);

    if (llCVVTooltip != null && hasFocus) llCVVTooltip.setVisibility(View.GONE);
    if (tilCreditCard.getEditText() == null) return;

    boolean isCreditCardCorrect = super.getIsFieldCorrect();

    if (paymentFormWidget.getPaymentMethodSpinnerVisibility() == View.VISIBLE) {
      String creditCardCode = paymentFormWidget.getPaymentMethodCodeDetected();
      String creditCardNumber =
          tilCreditCard.getEditText().getText().toString().replaceAll("\\s", "");
      isCreditCardCorrect =
          isCreditCardCorrect && requestValidationHandler.handleRequest(creditCardCode,
              creditCardNumber);
      if (!isCreditCardCorrect) setError();
    }

    trackIfThereIsAnyError(isCreditCardCorrect);

    if (hasFocus) mThereWasFocusAtLeastOnce = true;
  }

  @SuppressWarnings("ConstantConditions")
  private void trackIfThereIsAnyError(boolean isFieldCorrect) {
    if (!isFieldCorrect) {
      String fieldInformation = tilCreditCard.getEditText().getText().toString();
      if (fieldInformation.isEmpty()) {
        trackEmptyField();
      } else {
        trackErrorField();
      }
    }
  }

  private void trackEmptyField() {
    tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
        TrackerConstants.ACTION_PAYMENT_DETAILS_ERROR, TrackerConstants.LABEL_CARD_NUMBER_MISSING);
  }

  private void trackErrorField() {
    tracker.trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
        TrackerConstants.ACTION_PAYMENT_DETAILS_ERROR, TrackerConstants.LABEL_CARD_NUMBER_INVALID);
  }

  public boolean getThereWasFocusAtLeastOnce() {
    return mThereWasFocusAtLeastOnce;
  }
}
