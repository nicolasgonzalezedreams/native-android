package com.odigeo.app.android.lib.ui.widgets;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.adapters.OdigeoSpinnerAdapter;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;
import com.odigeo.app.android.lib.config.Lists;
import com.odigeo.app.android.lib.config.ResidentItinerary;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.interfaces.ITripLegWidgetListener;
import com.odigeo.app.android.lib.interfaces.SearchTripListener;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.app.android.lib.ui.widgets.ResidentsWidget.OnResidentSwitchListener;
import com.odigeo.app.android.lib.ui.widgets.base.OdigeoSpinner;
import com.odigeo.app.android.lib.utils.OdigeoDateUtils;
import com.odigeo.data.entity.FlightsDirection;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.odigeo.dataodigeo.tracker.TrackerConstants.DESTINATION_TAP_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.DIRECT_FLIGHTS_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.INBOUND_TAP_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.OUTBOUND_TAP_EVENT;

public class SearchTripWidget extends LinearLayout
    implements View.OnClickListener, ITripLegWidgetListener, OnResidentSwitchListener {
  private final List<SearchTripListener> listeners = new ArrayList<SearchTripListener>();
  private final OdigeoApp odigeoApp;
  private ButtonSearchDestination btnDestinationFrom;
  private ButtonSearchDestination btnDestinationTo;
  private ButtonSearchDate btnDepartureDate;
  private ButtonSearchDate btnReturnDate;
  private ButtonPassengers cellPassengers;
  private OdigeoSpinner spinnerClass;
  private AppCompatCheckBox chkDirectFlight;
  private Button btnSearchFlight;
  private LinearLayout rowDestinations;
  private LinearLayout rowDates;
  private LinearLayout rowFlightType;
  private Button btnAddAnotherLeg;
  private LinearLayout searchTripLegsContainer;
  private List<FlightSegment> flightSegments;
  private TravelType travelType;
  private List<TripLegWidget> tripLegWidgets;
  private ResidentsWidget residentsWidget;
  private TrackerControllerInterface mTrackerController;

  public SearchTripWidget(Context context, AttributeSet attrs) {
    super(context, attrs);

    odigeoApp = (OdigeoApp) context.getApplicationContext();

    TypedArray aAttrs =
        context.obtainStyledAttributes(attrs, R.styleable.SearchTripWidgetStyleable, 0, 0);
    boolean rowFlighstVisible =
        aAttrs.getBoolean(R.styleable.SearchTripWidgetStyleable_showFlightsType, true);
    boolean rowPassengersVisible =
        aAttrs.getBoolean(R.styleable.SearchTripWidgetStyleable_showPassengers, true);
    boolean rowDateReturnVisible =
        aAttrs.getBoolean(R.styleable.SearchTripWidgetStyleable_showDateReturn, true);
    aAttrs.recycle();
    inflate(getContext(), R.layout.layout_search_trip_widget, this);

    mTrackerController = AndroidDependencyInjector.getInstance().provideTrackerController();

    initialize(rowFlighstVisible, rowPassengersVisible, rowDateReturnVisible);
  }

  private void initialize(boolean rowFlighstVisible, boolean rowPassengersVisible,
      boolean rowDateReturnVisible) {

    btnDestinationFrom = (ButtonSearchDestination) findViewById(R.id.cell_from);
    btnDestinationTo = (ButtonSearchDestination) findViewById(R.id.cell_to);
    btnDepartureDate = (ButtonSearchDate) findViewById(R.id.cell_dateGo);
    btnReturnDate = (ButtonSearchDate) findViewById(R.id.cell_dateReturn);
    cellPassengers = (ButtonPassengers) findViewById(R.id.cell_passengers);
    chkDirectFlight = (AppCompatCheckBox) findViewById(R.id.cehck_flightsDirect_only);
    spinnerClass = (OdigeoSpinner) findViewById(R.id.spnTravelerClass_tripWidget);
    btnSearchFlight = (Button) findViewById(R.id.button_acept_widgetSearchActivity);
    residentsWidget = (ResidentsWidget) findViewById(R.id.residents_widget);
    rowDestinations = (LinearLayout) findViewById(R.id.search_row_destinations);
    rowDates = (LinearLayout) findViewById(R.id.search_row_dates);
    rowFlightType = (LinearLayout) findViewById(R.id.search_row_flight_type);

    setTextValues();

    searchTripLegsContainer = (LinearLayout) findViewById(R.id.searchTripLegsContainer);
    btnAddAnotherLeg = (Button) findViewById(R.id.btnAddAnotherLeg);

    btnAddAnotherLeg.setText(
        LocalizablesFacade.getString(odigeoApp, "searchviewcontroller_labeladdanotherleg")
            .toString());

    btnSearchFlight.setText(
        LocalizablesFacade.getString(odigeoApp, "searchviewcontroller_searchbtn_text").toString());

    btnDestinationFrom.setOnClickListener(this);
    btnDestinationTo.setOnClickListener(this);
    btnDepartureDate.setOnClickListener(this);
    btnReturnDate.setOnClickListener(this);
    btnSearchFlight.setOnClickListener(this);
    chkDirectFlight.setOnClickListener(this);
    cellPassengers.setOnClickListener(this);
    btnAddAnotherLeg.setOnClickListener(this);
    residentsWidget.setOnResidentSwitchListener(this);

    Fonts fonts = Configuration.getInstance().getFonts();

    if (!rowFlighstVisible) {
      rowFlightType.setVisibility(GONE);
    }

    if (!rowPassengersVisible) {
      cellPassengers.setVisibility(GONE);
    }

    if (!rowDateReturnVisible) {
      btnReturnDate.setVisibility(GONE);
    }

    //Checkbox
    getChkDirectFlight().setTypeface(fonts.getBold());

    btnSearchFlight.setTypeface(fonts.getBold());
    btnAddAnotherLeg.setTypeface(fonts.getBold());

    spinnerClass.setAdapter(
        new OdigeoSpinnerAdapter<>(getContext(), Lists.getInstance().getCabinClasses()));
    spinnerClass.setSpinnerItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (listeners != null) {
          for (SearchTripListener listener : listeners) {
            listener.onCabinClassSelected(
                ((CabinClassDTO) spinnerClass.getAdapter().getItem(position)));
          }
        }
      }

      @Override public void onNothingSelected(AdapterView<?> parent) {

      }
    });
    residentsWidget.setVisibility(View.GONE);
  }

  public final void initializeMultidestination() {
    hideRowDestinations();
    hideRowDates();

    clearLegs();
    btnAddAnotherLeg.setVisibility(VISIBLE);

    //For each flightSegment, creates a Leg Widget
    setTripLegWidgets(new ArrayList<TripLegWidget>());

    int i = 0;
    if (flightSegments != null) {
      for (FlightSegment flightSegment : flightSegments) {
        TripLegWidget tripLegWidget = new TripLegWidget(getContext(), i++, flightSegment);

        searchTripLegsContainer.addView(tripLegWidget);
        getTripLegWidgets().add(tripLegWidget);

        tripLegWidget.setListener(this);
      }
    }
  }

  public final void clearLegs() {
    tripLegWidgets = new ArrayList<>();
    searchTripLegsContainer.removeAllViewsInLayout();
  }

  public final void setListener(SearchTripListener listener) {
    boolean exist = false;

    for (SearchTripListener currentListener : listeners) {
      if (listener.equals(currentListener)) {
        exist = true;
        break;
      }
    }

    if (!exist) {
      this.listeners.add(listener);
    }
  }

  public final void setSegments(List<FlightSegment> flightSegments) {
    this.setFlightSegments(flightSegments);
  }

  public City getCityFrom() {
    return btnDestinationFrom.getCity();
  }

  public final void setCityFrom(City city) {
    btnDestinationFrom.setCity(city);
  }

  public City getCityTo() {
    return btnDestinationTo.getCity();
  }

  public final void setCityTo(City city) {
    btnDestinationTo.setCity(city);
  }

  public final void setDepartureDate(Date dateGo) {
    btnDepartureDate.setDate(dateGo);
  }

  public final void setReturnDate(Date dateReturn) {
    btnReturnDate.setDate(dateReturn);
  }

  public final void setPassengers(int adults, int infants, int babies) {
    cellPassengers.setNoAdults(adults);
    cellPassengers.setNoKids(infants);
    cellPassengers.setNoBabies(babies);
    Log.i(Constants.TAG_LOG,
        String.format("Adults: %s - Infants: %s - Babies: %s", adults, infants, babies));
  }

  @Override public final void onClick(View v) {
    if (listeners != null) {
      checkFromListener(v);
    }
    Intent intentActivity = null;

    int noSegment = 0;
    int requestCode = 0;

    if (v.getId() == btnDestinationFrom.getId() || v.getId() == btnDestinationTo.getId()) {
      intentActivity = new Intent(getContext(), odigeoApp.getDestinationActivityClass());
      intentActivity.putExtra(Constants.EXTRA_FLIGHT_SEGMENT_NUMBER, noSegment);
      requestCode = Constants.SELECTING_DESTINATION;

      if (v.getId() == btnDestinationFrom.getId()) {
        mTrackerController.trackAppseeEvent(TrackerConstants.ORIGIN_TAP_EVENT);
        intentActivity.putExtra(Constants.EXTRA_FLIGHT_DIRECTION, FlightsDirection.DEPARTURE);
      } else {
        mTrackerController.trackAppseeEvent(DESTINATION_TAP_EVENT);
        intentActivity.putExtra(Constants.EXTRA_FLIGHT_DIRECTION, FlightsDirection.RETURN);
      }
    } else if (v.getId() == btnDepartureDate.getId() || v.getId() == btnReturnDate.getId()) {
      if (v.getId() == btnDepartureDate.getId()) {
        mTrackerController.trackAppseeEvent(OUTBOUND_TAP_EVENT);
      } else {
        mTrackerController.trackAppseeEvent(INBOUND_TAP_EVENT);
      }
      intentActivity = onClickDates(v, noSegment);
      requestCode = Constants.SELECTING_DATE;
    } else if (v.getId() == btnAddAnotherLeg.getId()) {
      addNewTripLeg();
    }

    if (intentActivity != null) {
      ((Activity) getContext()).startActivityForResult(intentActivity, requestCode);
    }
  }

  public final Intent onClickDates(View v, int noSegment) {

    Intent intentActivity = new Intent(getContext(), odigeoApp.getCalendarActivityClass());

    intentActivity.putExtra(Constants.EXTRA_FLIGHT_SEGMENT_NUMBER, noSegment);

    ArrayList<Date> tripDates = new ArrayList<Date>();

    tripDates.add(OdigeoDateUtils.createDate(getFlightSegments().get(0).getDate()));

    if (getFlightSegments().size() > 1) {
      long dateLong = getFlightSegments().get(1).getDate();
      if (dateLong == 0 || (getFlightSegments().get(0).getDate() > dateLong)) {
        tripDates.add(null);
      } else {
        tripDates.add(OdigeoDateUtils.createDate(getFlightSegments().get(1).getDate()));
      }
    } else {
      tripDates.add(null);
    }

    intentActivity.putExtra(Constants.EXTRA_SELECTED_DATES, tripDates);
    intentActivity.putExtra(Constants.EXTRA_TRAVEL_TYPE, getTravelType());

    if (v.getId() == btnDepartureDate.getId()) {
      intentActivity.putExtra(Constants.EXTRA_FLIGHT_DIRECTION, FlightsDirection.DEPARTURE);
    } else {
      intentActivity.putExtra(Constants.EXTRA_FLIGHT_DIRECTION, FlightsDirection.RETURN);
    }

    return intentActivity;
  }

  public final void checkFromListener(View v) {
    for (SearchTripListener listener : listeners) {
      if (v.getId() == chkDirectFlight.getId()) {
        Log.i(Constants.TAG_LOG, "Direct flight: " + chkDirectFlight.isChecked());
        if (chkDirectFlight.isChecked()) {
          mTrackerController.trackAppseeEvent(DIRECT_FLIGHTS_EVENT);
        }
        listener.onClickDirectFlight(chkDirectFlight.isChecked());
      } else if (v.getId() == cellPassengers.getId()) {
        listener.onClickButtonPassenger();
      } else if (v.getId() == btnSearchFlight.getId()) {
        listener.onClickSearch(this, getResidentsWidget().getSwitchWidget().isChecked());

        AndroidDependencyInjector.getInstance()
            .provideTrackerController()
            .trackLocalyticsEvent(TrackerConstants.CLICKED_SEARCH_FLIGHTS);
      }
    }
  }

  private void addNewTripLeg() {

    FlightSegment newFlightSegment = new FlightSegment();
    getFlightSegments().add(newFlightSegment);

    TripLegWidget tripLegWidget =
        new TripLegWidget(getContext(), getTripLegWidgets().size(), newFlightSegment);
    City newStartCity =
        getTripLegWidgets().get(tripLegWidgets.size() - 1).getFlightSegment().getArrivalCity();
    if (newStartCity != null) {
      newFlightSegment.setDepartureCity(newStartCity);
      tripLegWidget.setCityFrom(newStartCity);
    }

    searchTripLegsContainer.addView(tripLegWidget);
    getTripLegWidgets().add(tripLegWidget);
    tripLegWidget.setListener(this);

    updateViews();

    btnSearchFlight.setEnabled(isDataValid());
  }

  /**
   * This method updates the button for new legs and adds the remove button on each leg if
   * necessary
   */
  public void updateViews() {
    //It it reaches the maximum number of legs admitted, then disable the add more legs button
    if (getTripLegWidgets().size() == Configuration.getInstance()
        .getGeneralConstants()
        .getMaxNumberOfLegs()) {
      btnAddAnotherLeg.setEnabled(false);
    } else {
      btnAddAnotherLeg.setEnabled(true);
    }

    //If there are more legs than the minimum, then add a remove button to each widget
    if (getTripLegWidgets().size() > Configuration.getInstance()
        .getGeneralConstants()
        .getMinNumberOfLegs()) {
      for (TripLegWidget currentTripLegWidget : getTripLegWidgets()) {
        currentTripLegWidget.showRemoveButton();
      }
    }
  }

  @Override public final void onClickRemoveWidget(int noSegment) {
    searchTripLegsContainer.removeViewAt(noSegment);
    getTripLegWidgets().remove(noSegment);
    flightSegments.remove(noSegment);

    for (int i = 0; i < flightSegments.size(); i++) {
      getTripLegWidgets().get(i).setNoSegment(i);

      //If the number of legs is the minimun, then disable the remove button
      if (flightSegments.size() == Configuration.getInstance()
          .getGeneralConstants()
          .getMinNumberOfLegs()) {
        getTripLegWidgets().get(i).hideRemoveButton();
      }
    }

    for (TripLegWidget tripLegWidget : getTripLegWidgets()) {
      tripLegWidget.updateData();
    }

    if (getTripLegWidgets().size() < Configuration.getInstance()
        .getGeneralConstants()
        .getMaxNumberOfLegs()) {
      btnAddAnotherLeg.setEnabled(true);
    }

    btnSearchFlight.setEnabled(isDataValid());
  }

  @Override public final void onClickDestinationFrom(int noSegment) {

    int requestCode = Constants.SELECTING_DESTINATION;

    Intent intentActivity = new Intent(getContext(), odigeoApp.getDestinationActivityClass());
    intentActivity.putExtra(Constants.EXTRA_FLIGHT_SEGMENT_NUMBER, noSegment);
    intentActivity.putExtra(Constants.EXTRA_FLIGHT_DIRECTION, FlightsDirection.DEPARTURE);

    ((Activity) getContext()).startActivityForResult(intentActivity, requestCode);
  }

  @Override public final void onClickDestinationTo(int noSegment) {
    int requestCode = Constants.SELECTING_DESTINATION;

    Intent intentActivity = new Intent(getContext(), odigeoApp.getDestinationActivityClass());
    intentActivity.putExtra(Constants.EXTRA_FLIGHT_SEGMENT_NUMBER, noSegment);
    intentActivity.putExtra(Constants.EXTRA_FLIGHT_DIRECTION, FlightsDirection.RETURN);

    ((Activity) getContext()).startActivityForResult(intentActivity, requestCode);
  }

  @Override public final void onClickDepartureDate(int noSegment) {
    int requestCode = Constants.SELECTING_DATE;

    ArrayList<Date> tripDates = new ArrayList<Date>();

    for (FlightSegment flightSegment : flightSegments) {
      tripDates.add(OdigeoDateUtils.createDate(flightSegment.getDate()));
    }

    Intent intentActivity = new Intent(getContext(), odigeoApp.getCalendarActivityClass());

    intentActivity.putExtra(Constants.EXTRA_FLIGHT_SEGMENT_NUMBER, noSegment);
    intentActivity.putExtra(Constants.EXTRA_TRAVEL_TYPE, getTravelType());
    intentActivity.putExtra(Constants.EXTRA_SELECTED_DATES, tripDates);

    if (noSegment == 0) {
      intentActivity.putExtra(Constants.EXTRA_FLIGHT_DIRECTION, FlightsDirection.DEPARTURE);
    } else if (noSegment == 1) {
      intentActivity.putExtra(Constants.EXTRA_FLIGHT_DIRECTION, FlightsDirection.RETURN);
    }

    ((Activity) getContext()).startActivityForResult(intentActivity, requestCode);
  }

  public final boolean isDataValid() {

    boolean isValid = true;

    for (FlightSegment flightSegment : getFlightSegments()) {
      if (flightSegment.getDate() == 0
          || flightSegment.getDepartureCity() == null
          || flightSegment.getArrivalCity() == null) {
        isValid = false;
      }
    }

    return isValid;
  }

  public final TravelType getTravelType() {
    return travelType;
  }

  public final void setTravelType(TravelType travelType) {

    this.travelType = travelType;

    if (travelType == TravelType.MULTIDESTINATION) {
      initializeMultidestination();
    }
  }

  public final List<TripLegWidget> getTripLegWidgets() {
    return tripLegWidgets;
  }

  public final void setTripLegWidgets(List<TripLegWidget> tripLegWidgets) {
    this.tripLegWidgets = tripLegWidgets;
  }

  public final void showResidentsWidget(ResidentItinerary residentItinerary) {

    residentsWidget.setResidentItinerary(residentItinerary);

    if (residentItinerary != null) {
      residentsWidget.setVisibility(View.VISIBLE);
      //remove the separator in the texts of the view, setted in the view layout_residents
      residentsWidget.getSecondText().removeSeparator();
      residentsWidget.getFirstText().removeSeparator();
    } else {
      residentsWidget.setVisibility(View.GONE);
    }
  }

  public final OdigeoSpinner getSpinnerClass() {
    return spinnerClass;
  }

  public final CheckBox getChkDirectFlight() {
    return chkDirectFlight;
  }

  public final Button getBtnSearchFlight() {
    return btnSearchFlight;
  }

  public final void showRowDestinations() {
    rowDestinations.setVisibility(VISIBLE);
  }

  public final void hideRowDestinations() {
    rowDestinations.setVisibility(GONE);
  }

  public final void showRowDates() {
    rowDates.setVisibility(VISIBLE);
  }

  public final void hideRowDates() {
    rowDates.setVisibility(GONE);
  }

  public final void showRowPassengers() {
    cellPassengers.setVisibility(VISIBLE);
  }

  public final void hideRowPassengers() {
    cellPassengers.setVisibility(GONE);
  }

  public final void showRowFlightType() {
    rowFlightType.setVisibility(VISIBLE);
  }

  public final void hideRowFlightType() {
    rowFlightType.setVisibility(GONE);
  }

  public final void hideDestinationTo() {
    btnDestinationFrom.setVisibility(GONE);
  }

  public final void showDestinationTo() {
    btnDestinationFrom.setVisibility(VISIBLE);
  }

  public final void hideReturnDate() {
    btnReturnDate.setVisibility(GONE);
  }

  public final void showReturnDate() {
    btnReturnDate.setVisibility(VISIBLE);
  }

  public final List<FlightSegment> getFlightSegments() {
    return flightSegments;
  }

  public final void setFlightSegments(List<FlightSegment> flightSegments) {
    this.flightSegments = flightSegments;
  }

  public final ResidentsWidget getResidentsWidget() {
    return residentsWidget;
  }

  public final ButtonSearchDestination getBtnDestinationFrom() {
    return btnDestinationFrom;
  }

  public final ButtonSearchDestination getBtnDestinationTo() {
    return btnDestinationTo;
  }

  public final ButtonSearchDate getBtnDepartureDate() {
    return btnDepartureDate;
  }

  public final ButtonSearchDate getBtnReturnDate() {
    return btnReturnDate;
  }

  public ButtonPassengers getCellPassengers() {
    return cellPassengers;
  }

  /**
   * Add some text brought from the database that may be stored in a static variable.
   */
  private void setTextValues() {
    btnDestinationFrom.setEmptyTextAndUpdate(CommonLocalizables.getInstance()
        .getSearchviewcontrollerPickfromlabelText(getContext())
        .toString());
    btnDestinationTo.setEmptyTextAndUpdate(CommonLocalizables.getInstance()
        .getSearchviewcontrollerPicktolabelText(getContext())
        .toString());
    btnDepartureDate.setEmptyTextAndUpdate(CommonLocalizables.getInstance()
        .getSearchviewcontrollerLabeldategoText(getContext())
        .toString());
    btnReturnDate.setEmptyTextAndUpdate(CommonLocalizables.getInstance()
        .getSearchviewcontrollerLabeldatereturnText(getContext())
        .toString());
  }

  @Override public void onResidentSwitchClicked(boolean checked) {
    if (listeners != null) {
      for (SearchTripListener listener : listeners) {
        listener.onResidentSwitchClicked(checked);
      }
    }
  }
}
