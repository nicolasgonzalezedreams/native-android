package com.odigeo.app.android.view.adapters.holders;

import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.v4.view.ViewCompat;
import android.support.v7.content.res.AppCompatResources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.utils.BookingUtils;
import com.odigeo.app.android.view.adapters.BookingAdapter;
import com.odigeo.app.android.view.helpers.DrawableUtils;
import com.odigeo.app.android.view.imageutils.BookingImageUtil;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;
import com.odigeo.tools.DateHelperInterface;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.odigeo.app.android.utils.BookingUtils.DEFAULT_CITY_DELIMITER;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LIST_CARD_TITLE_TRIP;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LIST_STATUS_CANCEL;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LIST_STATUS_CONFIRM;
import static com.odigeo.app.android.view.constants.OneCMSKeys.MY_TRIPS_LIST_STATUS_PENDING;
import static com.odigeo.data.entity.booking.Booking.TRIP_TYPE_ONE_WAY;

public class BookingCardHolder extends BaseRecyclerViewHolder<Booking>
    implements DividerProviderViewHolder {

  private final DateHelperInterface mDateHelper;
  private final int mCurrentBookings;
  private final View card;
  private final View bookingStatusContainer;
  private final TextView bookingStatusTV;
  private final ImageView bookingStatusIconIV;
  private final TextView fromDateTV;
  private final TextView toDateTV;
  private final LinearLayout toDateContainer;
  private final ImageView bookingBackgroundIV;
  private final TextView bookingCityTV;
  private final TextView bookingTripToLabelTV;
  private final BookingAdapter.OnBookingClickListener bookingClickListener;

  private final OdigeoImageLoader<ImageView> mImageLoader;
  private final BookingImageUtil bookingImageUtil;

  public static BookingCardHolder newInstance(ViewGroup container, DateHelperInterface dateHelper,
      int currentBookings, BookingAdapter.OnBookingClickListener onBookingClickListener) {
    View view = LayoutInflater.from(container.getContext())
        .inflate(R.layout.my_booking_item, container, false);
    return new BookingCardHolder(view, dateHelper, currentBookings, onBookingClickListener);
  }

  private BookingCardHolder(View v, DateHelperInterface dataHelper, int currentBookings,
      BookingAdapter.OnBookingClickListener onBookingClickListener) {
    super(v);
    mDateHelper = dataHelper;
    mCurrentBookings = currentBookings;
    card = v.findViewById(R.id.card_container);
    fromDateTV = (TextView) v.findViewById(R.id.booking_from_date);
    toDateTV = (TextView) v.findViewById(R.id.booking_to_date);
    toDateContainer = (LinearLayout) v.findViewById(R.id.booking_to_date_container);
    bookingStatusContainer = v.findViewById(R.id.booking_status_container);
    bookingStatusTV = (TextView) v.findViewById(R.id.booking_status);
    bookingStatusIconIV = (ImageView) v.findViewById(R.id.booking_status_icon);
    bookingCityTV = (TextView) v.findViewById(R.id.booking_city);
    bookingBackgroundIV = (ImageView) v.findViewById(R.id.booking_background_image);
    bookingTripToLabelTV = (TextView) v.findViewById(R.id.trip_to_label);
    mImageLoader = dependencyInjector.provideImageLoader();
    bookingImageUtil = dependencyInjector.provideMyTripsImageUtil();
    bookingClickListener = onBookingClickListener;
    bookingTripToLabelTV.setText(localizableProvider.getString(MY_TRIPS_LIST_CARD_TITLE_TRIP));
  }

  @Override public void bind(int position, Booking item) {
    if (item.getFirstSegment() != null) {
      String arrivalName = BookingUtils.getArrivalCityName(item, DEFAULT_CITY_DELIMITER);
      String formattedFromDate =
          BookingUtils.formatSegmentDate(item.getFirstSegment(), mDateHelper, localizableProvider);
      String formattedToDate =
          BookingUtils.formatSegmentDate(item.getLastSegment(), mDateHelper, localizableProvider);

      drawBookingStatus(item);
      drawBookingImage(item);

      bookingCityTV.setText(arrivalName);
      fromDateTV.setText(HtmlUtils.formatHtml(formattedFromDate));
      if (item.getTripType().equals(TRIP_TYPE_ONE_WAY)) {
        toDateTV.setText(null);
        toDateContainer.setVisibility(GONE);
      } else {
        toDateTV.setText(HtmlUtils.formatHtml(formattedToDate));
        toDateContainer.setVisibility(VISIBLE);
      }

      setListeners(card, item);
    }
  }

  private void setListeners(View root, final Booking item) {
    root.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        if (bookingClickListener != null) {
          bookingClickListener.onClick(getAdapterPosition(), item);
        }
      }
    });
  }

  private void drawBookingImage(Booking booking) {
    bookingImageUtil.loadBookingImage(getContext(), mImageLoader, bookingBackgroundIV, booking, false);
  }

  private void drawBookingStatus(Booking booking) {
    ViewCompat.setBackground(bookingStatusContainer, null);
    bookingStatusIconIV.setImageDrawable(null);
    bookingStatusTV.setText(null);

    if (booking.isConfirmed()) {
      drawBookingStatusBadge(R.drawable.ic_checked, R.color.dark_sea_foam,
          localizableProvider.getString(MY_TRIPS_LIST_STATUS_CONFIRM));
    } else if (booking.isCancelled()) {
      drawBookingStatusBadge(R.drawable.ic_canceled, R.color.booking_status_cancelled,
          localizableProvider.getString(MY_TRIPS_LIST_STATUS_CANCEL));
    } else if (booking.isPending()) {
      drawBookingStatusBadge(R.drawable.ic_pending, R.color.booking_status_pending,
          localizableProvider.getString(MY_TRIPS_LIST_STATUS_PENDING));
    }
  }

  private void drawBookingStatusBadge(@DrawableRes int statusIcon,
      @ColorRes int statusBackgroundColor, String statusText) {
    Drawable drawable =
        DrawableUtils.getTintedDrawable(getContext(), R.drawable.booking_status_rounded_background,
            statusBackgroundColor);
    ViewCompat.setBackground(bookingStatusContainer, drawable);
    bookingStatusIconIV.setImageDrawable(AppCompatResources.getDrawable(getContext(), statusIcon));
    bookingStatusTV.setText(statusText);
    bookingStatusContainer.setVisibility(VISIBLE);
  }

  @Override public int getDividerDrawable() {
    return R.drawable.card_list_divider;
  }

  public View getCard() {
    return card;
  }
}
