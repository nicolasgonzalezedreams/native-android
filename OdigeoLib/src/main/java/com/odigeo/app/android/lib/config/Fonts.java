package com.odigeo.app.android.lib.config;

import android.graphics.Typeface;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ManuelOrtiz on 28/08/2014.
 */
public class Fonts {
  public static final int FONT_BLACK = 0;
  public static final int FONT_BLACK_ITALIC = 1;
  public static final int FONT_BOLD = 2;
  public static final int FONT_BOLD_ITALIC = 3;
  public static final int FONT_EXTRA_LIGHT = 4;
  public static final int FONT_EXTRA_LIGHT_ITALIC = 5;
  public static final int FONT_ITALIC = 6;
  public static final int FONT_LIGHT = 7;
  public static final int FONT_LIGHT_ITALIC = 8;
  public static final int FONT_REGULAR = 9;
  public static final int FONT_SEMIBOLD = 10;
  public static final int FONT_SEMIBOLD_ITALIC = 11;
  private static Map<Integer, Typeface> fontsMap;
  private Typeface black;
  private Typeface blackItalic;
  private Typeface bold;
  private Typeface boldItalic;
  private Typeface extraLight;
  private Typeface extraLightItalic;
  private Typeface italic;
  private Typeface light;
  private Typeface lightItalic;
  private Typeface regular;
  private Typeface semiBold;
  private Typeface semiBoldItalic;

  public final void pack() {
    fontsMap = new HashMap<Integer, Typeface>();
    fontsMap.put(FONT_BLACK, black);
    fontsMap.put(FONT_BLACK_ITALIC, blackItalic);
    fontsMap.put(FONT_BOLD, bold);
    fontsMap.put(FONT_BOLD_ITALIC, boldItalic);
    fontsMap.put(FONT_EXTRA_LIGHT, extraLight);
    fontsMap.put(FONT_EXTRA_LIGHT_ITALIC, extraLightItalic);
    fontsMap.put(FONT_ITALIC, italic);
    fontsMap.put(FONT_LIGHT, light);
    fontsMap.put(FONT_LIGHT_ITALIC, lightItalic);
    fontsMap.put(FONT_REGULAR, regular);
    fontsMap.put(FONT_SEMIBOLD, semiBold);
    fontsMap.put(FONT_SEMIBOLD_ITALIC, semiBoldItalic);
  }

  public final Typeface get(int fontId) {
    if (fontsMap.containsKey(fontId)) {
      return fontsMap.get(fontId);
    }
    return null;
  }

  public final Typeface getBlack() {
    return black;
  }

  public final void setBlack(Typeface black) {
    this.black = black;
  }

  public final Typeface getBlackItalic() {
    return blackItalic;
  }

  public final void setBlackItalic(Typeface blackItalic) {
    this.blackItalic = blackItalic;
  }

  public final Typeface getBold() {
    return bold;
  }

  public final void setBold(Typeface bold) {
    this.bold = bold;
  }

  public final Typeface getBoldItalic() {
    return boldItalic;
  }

  public final void setBoldItalic(Typeface boldItalic) {
    this.boldItalic = boldItalic;
  }

  public final Typeface getExtraLight() {
    return extraLight;
  }

  public final void setExtraLight(Typeface extraLight) {
    this.extraLight = extraLight;
  }

  public final Typeface getExtraLightItalic() {
    return extraLightItalic;
  }

  public final void setExtraLightItalic(Typeface extraLightItalic) {
    this.extraLightItalic = extraLightItalic;
  }

  public final Typeface getItalic() {
    return italic;
  }

  public final void setItalic(Typeface italic) {
    this.italic = italic;
  }

  public final Typeface getLight() {
    return light;
  }

  public final void setLight(Typeface light) {
    this.light = light;
  }

  public final Typeface getLightItalic() {
    return lightItalic;
  }

  public final void setLightItalic(Typeface lightItalic) {
    this.lightItalic = lightItalic;
  }

  public final Typeface getRegular() {
    return regular;
  }

  public final void setRegular(Typeface regular) {
    this.regular = regular;
  }

  public final Typeface getSemiBold() {
    return semiBold;
  }

  public final void setSemiBold(Typeface semiBold) {
    this.semiBold = semiBold;
  }

  public final Typeface getSemiBoldItalic() {
    return semiBoldItalic;
  }

  public final void setSemiBoldItalic(Typeface semiBoldItalic) {
    this.semiBoldItalic = semiBoldItalic;
  }
}
