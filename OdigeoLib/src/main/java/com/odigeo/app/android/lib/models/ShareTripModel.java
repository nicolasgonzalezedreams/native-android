package com.odigeo.app.android.lib.models;

import android.content.Context;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.TravelType;

public class ShareTripModel extends BaseShareModel {

  private static final String STORE_URL_IDENTIFIER = "{X}";
  private static final String LINK_VAL_IDENTIFIER = "{Y}";
  private static final String HYPERLINK_VAL =
      "<a href=\"" + STORE_URL_IDENTIFIER + "\">" + LINK_VAL_IDENTIFIER + "</a>";
  private String mFromCity;
  private String mToCity;
  private String mPrice;
  private String mPassengers;
  private String mDateDeparting;
  private String mHourDeparting;
  private String mDateArriving;
  private String mHourArriving;
  private String mCarrierFirst;
  private String mFromCitySecond;
  private String mToCitySecond;
  private String mDateDepartingSecond;
  private String mHourDepartingSecond;
  private String mDateArrivingSecond;
  private String mHourArrivingSecond;
  private String mCarrierSecond;
  private String mPassengersBack;
  private String mTravelType;
  private Context mContext;

  public ShareTripModel(Context context, String fromCity, String toCity, String price,
      String passengers, String dateDeparting, String hourDeparting, String dateArriving,
      String hourArriving, String carrierFirst, String fromCitySecond, String toCitySecond,
      String dateDepartingSecond, String hourDepartingSecond, String dateArrivingSecond,
      String hourArrivingSecond, String carrierSecond, String passengersBack, String travelType) {
    mContext = context;
    mFromCity = fromCity;
    mToCity = toCity;
    mPrice = price;
    mPassengers = passengers;
    mDateDeparting = dateDeparting;
    mHourDeparting = hourDeparting;
    mDateArriving = dateArriving;
    mHourArriving = hourArriving;
    mCarrierFirst = carrierFirst;
    mFromCitySecond = fromCitySecond;
    mToCitySecond = toCitySecond;
    mDateDepartingSecond = dateDepartingSecond;
    mHourDepartingSecond = hourDepartingSecond;
    mDateArrivingSecond = dateArrivingSecond;
    mHourArrivingSecond = hourArrivingSecond;
    mCarrierSecond = carrierSecond;
    mPassengersBack = passengersBack;
    mTravelType = travelType;
  }

  public String getFromCity() {
    return mFromCity;
  }

  public void setFromCity(String fromCity) {
    mFromCity = fromCity;
  }

  public String getToCity() {
    return mToCity;
  }

  public void setToCity(String toCity) {
    mToCity = toCity;
  }

  public String getPrice() {
    return mPrice;
  }

  public void setPrice(String price) {
    mPrice = price;
  }

  public String getPassengers() {
    return mPassengers;
  }

  public void setPassengers(String passengers) {
    mPassengers = passengers;
  }

  public String getDateDeparting() {
    return mDateDeparting;
  }

  public void setDateDeparting(String dateDeparting) {
    mDateDeparting = dateDeparting;
  }

  public String getHourDeparting() {
    return mHourDeparting;
  }

  public void setHourDeparting(String hourDeparting) {
    mHourDeparting = hourDeparting;
  }

  public String getDateArriving() {
    return mDateArriving;
  }

  public void setDateArriving(String dateArriving) {
    mDateArriving = dateArriving;
  }

  public String getHourArriving() {
    return mHourArriving;
  }

  public void setHourArriving(String hourArriving) {
    mHourArriving = hourArriving;
  }

  public String getCarrierFirst() {
    return mCarrierFirst;
  }

  public void setCarrierFirst(String carrierFirst) {
    mCarrierFirst = carrierFirst;
  }

  public String getFromCitySecond() {
    return mFromCitySecond;
  }

  public void setFromCitySecond(String fromCitySecond) {
    mFromCitySecond = fromCitySecond;
  }

  public String getToCitySecond() {
    return mToCitySecond;
  }

  public void setToCitySecond(String toCitySecond) {
    mToCitySecond = toCitySecond;
  }

  public String getDateDepartingSecond() {
    return mDateDepartingSecond;
  }

  public void setDateDepartingSecond(String dateDepartingSecond) {
    mDateDepartingSecond = dateDepartingSecond;
  }

  public String getHourDepartingSecond() {
    return mHourDepartingSecond;
  }

  public void setHourDepartingSecond(String hourDepartingSecond) {
    mHourDepartingSecond = hourDepartingSecond;
  }

  public String getDateArrivingSecond() {
    return mDateArrivingSecond;
  }

  public void setDateArrivingSecond(String dateArrivingSecond) {
    mDateArrivingSecond = dateArrivingSecond;
  }

  public String getHourArrivingSecond() {
    return mHourArrivingSecond;
  }

  public void setHourArrivingSecond(String hourArrivingSecond) {
    mHourArrivingSecond = hourArrivingSecond;
  }

  public String getCarrierSecond() {
    return mCarrierSecond;
  }

  public void setCarrierSecond(String carrierSecond) {
    mCarrierSecond = carrierSecond;
  }

  public String getPassengersBack() {
    return mPassengersBack;
  }

  public void setPassengersBack(String passengersBack) {
    mPassengersBack = passengersBack;
  }

  public String getTravelType() {
    return mTravelType;
  }

  public void setTravelType(String travelType) {
    mTravelType = travelType;
  }

  @Override public void createTwitterShareText(String oneCmsKey, String storeUrl) {
    setTwitterMessage(createShareText(oneCmsKey, mFromCity, mToCity, mPrice, storeUrl));
  }

  @Override public void createFacebookShareText(String storeUrl) {
    setFbMessage(storeUrl);
  }

  @Override public void createWhatsappShareText(String oneCmsKey, String storeUrl) {
    setWhatsAppMessage(createShareText(oneCmsKey, mFromCity, mToCity, mPrice, storeUrl));
  }

  @Override public void createSmsShareText(String oneCmsKey, String storeUrl) {
    setSmsMessage(createShareText(oneCmsKey, mFromCity, mToCity, mPrice, storeUrl));
  }

  @Override public void createCopyToClipboardShareText(String oneCmsKey, String storeUrl) {
    setCopyToClipboardMessage(createShareText(oneCmsKey, mFromCity, mToCity, mPrice, storeUrl));
  }

  @Override public void createFbMessengerShareText(String oneCmsKey, String storeUrl) {
    setFbMessengerMessage(createShareText(oneCmsKey, mFromCity, mToCity, mPrice, storeUrl));
  }

  @Override public void createEmailSubject(String oneCmsKey) {
    setEmailSubject(createSubjecText(oneCmsKey, mToCity, mPrice));
  }

  @Override public void createEmailBody(String oneCMSkey, String storeUrl) {
    String priceToShow = LocaleUtils.getLocalizedCurrencyValue(Double.valueOf(mPrice));
    CharSequence link_val =
        LocalizablesFacade.getString(mContext, OneCMSKeys.SHARE_BOOKING_DOWNLOAD_HYPERLINK_TEXT);
    String hyperlink = HYPERLINK_VAL.replace(STORE_URL_IDENTIFIER, storeUrl)
        .replace(LINK_VAL_IDENTIFIER, link_val);
    if (isSimpleOrMultidestination(mTravelType)) {
      setEmailBodyMessage(HtmlUtils.formatHtml(
          LocalizablesFacade.getRawString(mContext, oneCMSkey, mFromCity, mToCity, priceToShow,
              mFromCity, mToCity, mPassengers, mHourDeparting, mDateDeparting, mHourArriving,
              mDateArriving, mCarrierFirst, hyperlink)));
    } else {
      setEmailBodyMessage(HtmlUtils.formatHtml(
          LocalizablesFacade.getRawString(mContext, oneCMSkey, mFromCity, mFromCitySecond,
              priceToShow, mFromCity, mToCity, mPassengers, mHourDeparting, mDateDeparting,
              mHourArriving, mDateArriving, mCarrierFirst, mFromCitySecond, mToCitySecond,
              mPassengersBack, mHourDepartingSecond, mDateDepartingSecond, mHourArrivingSecond,
              mDateArrivingSecond, mCarrierSecond, hyperlink)));
    }
  }

  private String createShareText(String oneCmsKey, String fromCity, String toCity, String price,
      String storeUrl) {
    String priceToShow = LocaleUtils.getLocalizedCurrencyValue(Double.valueOf(price));
    return LocalizablesFacade.getString(mContext, oneCmsKey, fromCity, toCity, priceToShow,
        storeUrl).toString();
  }

  private Boolean isSimpleOrMultidestination(String travelType) {
    return travelType.equals(TravelType.SIMPLE.name()) || travelType.equals(
        TravelType.MULTIDESTINATION.name());
  }

  private String createSubjecText(String oneCmsKey, String toCity, String price) {
    String priceToShow = LocaleUtils.getLocalizedCurrencyValue(Double.valueOf(price));
    return LocalizablesFacade.getString(mContext, oneCmsKey, toCity, priceToShow).toString();
  }
}
