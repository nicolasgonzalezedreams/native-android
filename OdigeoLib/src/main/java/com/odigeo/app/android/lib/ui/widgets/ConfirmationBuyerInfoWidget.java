package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.models.dto.BuyerDTO;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.shoppingCart.Buyer;

import static com.odigeo.app.android.view.constants.OneCMSKeys.BUYERDETAILSMODULE_CONFIRMATION_MAIL_LABEL_TITLE;

public class ConfirmationBuyerInfoWidget extends LinearLayout {

  private static String PAYPAL = "PAYPAL";
  private static String TRUSTLY = "TRUSTLY";
  private static String KLARNA = "KLARNA";

  private ConfirmationInfoItem2Texts wdgtBuyerName;
  private ConfirmationInfoItem2Texts wdgtSelectedPaymentMethod;
  private ConfirmationInfoItem2Texts wdgtConfirmationMail;
  private TextView mBuyerTitle;

  public ConfirmationBuyerInfoWidget(Context context, AttributeSet attrs) {
    super(context, attrs);

    initialize(context);
  }

  private void initialize(Context context) {
    inflate(context, R.layout.layout_container_confirmation_buyerinfo, this);
    mBuyerTitle = (TextView) findViewById(R.id.tvBuyerTitle);
    mBuyerTitle.setText(
        LocalizablesFacade.getString(context, OneCMSKeys.BUYERDETAILSMODULE_HEADER_LABELTITLE));
    wdgtBuyerName = (ConfirmationInfoItem2Texts) findViewById(
        R.id.wdgtConfirmationBuyerInfoodigeoerNameAndTitle);
    wdgtSelectedPaymentMethod =
        (ConfirmationInfoItem2Texts) findViewById(R.id.wdgtConfirmationSelectedPaymentMethod);
    wdgtConfirmationMail = (ConfirmationInfoItem2Texts) findViewById(R.id.wdgtConfirmationMail);
    wdgtConfirmationMail.setText(
        LocalizablesFacade.getString(context, BUYERDETAILSMODULE_CONFIRMATION_MAIL_LABEL_TITLE)
            .toString());
  }

  public final void setBuyer(BuyerDTO buyer) {
    wdgtBuyerName.setText(buyer.getName() + " " + buyer.getLastNames());
    wdgtBuyerName.setSecondText(CommonLocalizables.getInstance().getCommonAdult(getContext()));
    wdgtConfirmationMail.setText(buyer.getMail());
  }

  public final void setBuyer(BuyerDTO buyer, String cardName) {
    this.setBuyer(buyer);
    wdgtSelectedPaymentMethod.setSecondText(cardName);
  }

  public void setBuyerPayment(Buyer buyer, String cardName, String cardNumberObfuscated) {
    wdgtBuyerName.setText(buyer.getName() + " " + buyer.getLastNames());
    wdgtBuyerName.setSecondText(CommonLocalizables.getInstance().getCommonAdult(getContext()));
    wdgtConfirmationMail.setText(buyer.getMail());

    if (cardName.equals(PAYPAL) || cardName.equals(TRUSTLY) || cardName.equals(KLARNA)) {
      wdgtSelectedPaymentMethod.setSecondText(LocalizablesFacade.getString(getContext(),
          OneCMSKeys.MY_TRIPS_DETAILS_TRIP_PAYMENT_METHOD));
      wdgtSelectedPaymentMethod.setText(cardName);
    } else {
      wdgtSelectedPaymentMethod.setSecondText(cardName);
      wdgtSelectedPaymentMethod.setText(cardNumberObfuscated);
    }
  }

  public final void showInfoCard(boolean show) {
    if (show) {
      wdgtSelectedPaymentMethod.setVisibility(VISIBLE);
    } else {
      wdgtSelectedPaymentMethod.setVisibility(GONE);
    }
  }
}
