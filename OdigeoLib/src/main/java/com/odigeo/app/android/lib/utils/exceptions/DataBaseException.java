package com.odigeo.app.android.lib.utils.exceptions;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 8/06/16
 */

public class DataBaseException extends RuntimeException {

  public DataBaseException(String detailMessage, Throwable throwable) {
    super(detailMessage, throwable);
  }
}
