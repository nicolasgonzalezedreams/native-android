package com.odigeo.app.android.view.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import java.util.List;

/**
 * @author José Eduardo Cabrera Rivera
 * @version 1.0
 * @since 11/11/2015.
 */
public class CardAdapter extends FragmentStatePagerAdapter {

  private final List<Fragment> mCards;

  public CardAdapter(FragmentManager fm, List<Fragment> cards) {
    super(fm);
    mCards = cards;
  }

  @Override public Fragment getItem(int position) {
    return mCards.get(position);
  }

  @Override public int getCount() {
    return mCards.size();
  }
}
