package com.odigeo.app.android.view.textwatchers.fieldsvalidation;

public class CVVCardValidator extends BaseValidator implements FieldValidator {

  private static final String REGEX_CPF = "[0-9]{3}";

  public CVVCardValidator() {
    super();
  }

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_CPF);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
