package com.odigeo.app.android.lib.consts;

/**
 * Created by ManuelOrtiz on 08/09/2014.
 */
@Deprecated public final class GAnalyticsNames {
  public static final String SCREEN_SEARCH = "/A_app/flights/home/";
  public static final String SCREEN_HELP_SECTION = "/A_app/about/home/help/";
  public static final String SCREEN_MY_INFO = "/A_app/flights/myinfo/";
  public static final String SCREEN_MY_INFO_NEW_PAX = "/A_app/flights/myinfo/new-pax/";
  public static final String SCREEN_MY_INFO_VIEW_PAX = "/A_app/flights/myinfo/pax-details/";
  public static final String SCREEN_MY_INFO_IDS = "/A_app/flights/myinfo/documents/";
  public static final String SCREEN_COUNTRY_LIST = "/A_app/country-list/";
  public static final String SCREEN_WAITING_PAGE_ONE = "/BF/A_app/flights/waiting/1/";
  public static final String SCREEN_NO_RESULTS = "/BF/A_app/flights/error/no-results/";
  public static final String SCREEN_NO_RESULTS_MULTI = "/BF/A_app/multistop/error/no-results/";
  public static final String SCREEN_NO_RESULTS_FILTERS =
      "/BF/A_app/flights/filters/error/no-results/";
  public static final String SCREEN_RESULTS = "/BF/A_app/flights/results/";
  public static final String SCREEN_FLIGHT_SUMMARY = "/BF/A_app/flights/summary/";
  public static final String SCREEN_PASSENGERS = "/BF/A_app/flights/details-extras/";
  public static final String SCREEN_TRIP_DETAILS = "/BF/A_app/flights/shopping-cart/";
  public static final String SCREEN_INSURANCES_T_CS = "/BF/A_app/flights/insurances/terms/";
  public static final String SCREEN_CONFIRMATION_CONTRACT =
      "/BF/A_app/flights/confirmation/contract/";
  public static final String SCREEN_CONFIRMATION_PENDING =
      "/BF/A_app/flights/confirmation/pending/";
  public static final String SCREEN_CONFIRMATION_REJECTED =
      "/BF/A_app/flights/confirmation/rejected/";
  public static final String SCREEN_ERROR_NO_CONNECTION = "/BF/A_app/flights/error/noconnection";
  public static final String SCREEN_FILTERS_TIME = "/BF/A_app/flights/filters/time/";
  public static final String SCREEN_FILTERS_STOPS = "/BF/A_app/flights/filters/stops/";
  public static final String SCREEN_FILTERS_AIRLINE = "/BF/A_app/flights/filters/airline/";
  public static final String SCREEN_FILTERS_AIRPORTS = "/BF/A_app/flights/filters/airports/";
  public static final String SCREEN_SETTINGS = "/BF/A_app/flights/filters/settings/";
  public static final String SCREEN_SETTINGS_COUNTRY_LIST =
      "/BF/A_app/flights/filters/settings/country-list/";

  public static final String CATEGORY_HOME = "home";

  public static final String ACTION_RECENT_SEARCH = "recent_search";
  public static final String ACTION_SEARCHER_FLIGHTS = "searcher_flights";
  public static final String ACTION_NAVIGATION = "navigation_elements";
  public static final String ACTION_PRODUCT_SELECTOR = "product_selector";
  public static final String ACTION_RESULTS_LIST = "results_list";
  public static final String ACTION_RESULTS_FILTERS = "results_filters";
  public static final String ACTION_BOOKING_CONFIRMED = "booking_confirmed";
  public static final String ACTION_BOOKING_REJECTED = "booking_rejected";
  public static final String ACTION_BOOKING_PENDING = "booking_pending";
  public static final String ACTION_ABOUT_US_CONTACT = "about_us_contact_us";
  public static final String ACTION_TRIP_DETAILS = "trip_details";
  public static final String ACTION_PAX_SUMMARY = "pax_summary";
  public static final String ACTION_PAX_DETAILS = "pax_details";
  public static final String ACTION_CONNECTION_LOST = "connection_lost";
  public static final String ACTION_APP_OPTIONS = "app_options";
  public static final String ACTION_WALKTHROUGH_NEW_UPDATES = "walkthrough_new_updates";
  public static final String ACTION_EMPTY_TRIPS = "empty_trips";
  public static final String ACTION_CROSS_SELL_WIDGET = "cross_sell_widget";
  public static final String ACTION_FULL_PRICE_SELECTOR = "full_price_selector";
  public static final String ACTION_CONFIRMATION_BANK_TRANSFER = "bank_transfer";

  public static final String CATEGORY_FLIGHT_SEARCH = "flights_search";
  public static final String CATEGORY_FLIGHTS_FILTERS = "flights_filters";
  public static final String CATEGORY_FLIGHTS_RESULTS = "flights_results";
  public static final String CATEGORY_FLIGHTS_SUMMARY = "flights_summary";
  public static final String CATEGORY_CONFIRMATION_PAGE_CONFIRMED =
      "flights_confirmation_page_confirmed";
  public static final String CATEGORY_PARTIAL_FLIGHTS_TRIP_DETAILS = "flights_trip_details_";
  public static final String CATEGORY_CONFIRMATION_PAGE_REJECTED =
      "flights_confirmation_page_rejected";
  public static final String CATEGORY_CONFIRMATION_PAGE_PENDING =
      "flights_confirmation_page_pending";
  public static final String CATEGORY_ABOUT_US = "about_us";
  public static final String CATEGORY_ABOUT_US_CONTACT = "about_us_contact_us";
  public static final String CATEGORY_MY_TRIPS = "my_trips";
  public static final String CATEGORY_MY_TRIPS_INFO = "my_trips_trip_information";
  public static final String CATEGORY_MY_DATA = "my_data";
  public static final String CATEGORY_CONNECTION_LOST = "connection_lost";
  public static final String CATEGORY_CONFIGURATION_SCREEN = "configuration_screen";
  public static final String CATEGORY_WALKTHROUGH_NEW_UPDATES = "walkthrough_new_updates";
  public static final String CATEGORY_FLIGHTS_CONFIRMATION_PAGE_CONFIRMED =
      "flights_confirmation_page_confirmed";

  public static final String LABEL_DIRECT_FLIGHTS_CHECKED = "direct_flights_checked";
  public static final String LABEL_AIRLINES_DESELECT_ALL = "airlines_deselect_all";
  public static final String LABEL_AIRLINES_SELECT_ALL = "airlines_select_all";
  public static final String LABEL_APP_MY_DATA_CLICKS = "app_my_data_clicks";
  public static final String LABEL_APP_MY_TRIPS_CLICKS = "app_my_trips_clicks";
  public static final String LABEL_APP_INFORMATION_CLICKS = "app_information_clicks";
  public static final String LABEL_APP_CONFIGURATION_CLICKS = "app_configuration_clicks";
  public static final String LABEL_AUTOCOMPLETE_DEPARTURE_GEOLOCATED =
      "autocomplete_departure_geolocated";
  public static final String LABEL_AUTOCOMPLETE_DEPARTURE_CLOSEST_AIRPORTS =
      "autocomplete_departure_closest_airports";
  public static final String LABEL_AUTOCOMPLETE_DEPARTURE_CLOSEST_CITIES =
      "autocomplete_departure_closest_cities";
  public static final String LABEL_AUTOCOMPLETE_DEPARTURE_RECENT =
      "autocomplete_departure_closest_recent";
  public static final String LABEL_AUTOCOMPLETE_ARRIVAL_GEOLOCATED =
      "autocomplete_arrival_geolocated";
  public static final String LABEL_AUTOCOMPLETE_ARRIVAL_CLOSEST_AIRPORTS =
      "autocomplete_arrival_closest_airports";
  public static final String LABEL_AUTOCOMPLETE_ARRIVAL_CLOSEST_CITIES =
      "autocomplete_arrival_closest_cities";
  public static final String LABEL_AUTOCOMPLETE_ARRIVAL_RECENT =
      "autocomplete_arrival_closest_recent";
  public static final String LABEL_CANCEL_INPUT = "cancel_input";
  public static final String LABEL_CONTINUE_CLICKS_BOTTOM = "continue_clicks_bottom";
  public static final String LABEL_FILTERS_APPLY = "filters_apply";
  public static final String LABEL_FILTERS_GO_BACK = "filters_go_back";
  public static final String LABEL_FILTERS_OPEN_CLICKS = "filters_open_clicks";
  public static final String LABEL_FILTERS_OPEN_TIMES = "filters_open_times";
  public static final String LABEL_FILTERS_OPEN_STOPS = "filters_open_stops";
  public static final String LABEL_FILTERS_OPEN_AIRLINES = "filters_open_airlines";
  public static final String LABEL_FILTERS_OPEN_AIRPORTS = "filters_open_airports";
  public static final String LABEL_FLIGHT_INFO_CLOSED_FROM_OPEN = "flight_info_closed_from_open";
  public static final String LABEL_FLIGHT_INFO_OPEN_FROM_CLOSED = "flight_info_open_from_closed";
  public static final String LABEL_GEOLOCATION_CLICKS = "geolocation_clicks";
  public static final String LABEL_GO_BACK = "go_back";
  public static final String LABEL_MULTI_DEST = "multiple_destinations";
  public static final String LABEL_ONE_WAY = "one_way";
  public static final String LABEL_PARTIAL_TIMES_ARRIVAL = "times_arrival_";
  public static final String LABEL_PARTIAL_TIMES_DEPARTURE = "times_departure_";
  public static final String LABEL_PARTIAL_TIMES_DURATION_MAX = "times_max_duration_";
  public static final String LABEL_PARTIAL_CHECKED = "checked";
  public static final String LABEL_PARTIAL_UNCHECKED = "unchecked";
  public static final String LABEL_PARTIAL_AIRLINE_CHECKED = "airlines_checked_";
  public static final String LABEL_PARTIAL_AIRLINE_UNCHECKED = "airlines_unchecked_";
  public static final String LABEL_PARTIAL_AIRPORT_CHECKED = "airports_checked_";
  public static final String LABEL_PARTIAL_SEGMENT = "_segment_";
  public static final String LABEL_PARTIAL_AIRPORTS_SELECT_ALL_SEGMENT =
      "airports_select_all_segment_";
  public static final String LABEL_PARTIAL_CABIN_CLASS = "cabin_class_";
  public static final String LABEL_PARTIAL_NUMBER_PAX = "number_pax_";
  public static final String LABEL_PARTIAL_RECENT_SEARCH = "recent_search_select_";
  public static final String LABEL_PARTIAL_SELECT_RESULT = "select_result_";
  public static final String LABEL_PARTIAL_STOPS = "stops_";
  public static final String LABEL_PRODUCT_FLIGHTS_CLICKS = "product_flights_clicks";
  public static final String LABEL_PRODUCT_HOTELS_CLICKS = "product_hotels_clicks";
  public static final String LABEL_PRODUCT_CARS_CLICKS = "product_cars_clicks";
  public static final String LABEL_RECENT_SEARCH_CLEAN = "recent_search_clean";
  public static final String LABEL_RECENT_SEARCH_VIEW_MORE = "recent_search_view_more";
  public static final String LABEL_ROUND_TRIP = "round_trip";
  public static final String LABEL_SEARCH_FLIGHTS = "search_flights";
  public static final String LABEL_PAYMENT_INFO_OPEN = "open_payment_information";
  public static final String LABEL_HOME_CLICKS = "home_clicks";
  public static final String LABEL_CALENDAR_ADD_CLICKS = "add_to_calendar_clicks";
  public static final String LABEL_MY_TRIPS_CLICKS = "my_trips_clicks";
  public static final String LABEL_ABOUT_FAQ = "faq_clicks";
  public static final String LABEL_ABOUT_CONTACT_US = "contact_us_clicks";
  public static final String LABEL_ABOUT_T_CS = "terms_and_conditions_clicks";
  public static final String LABEL_ABOUT_FEEDBACK = "give_feedback_clicks";
  public static final String LABEL_ABOUT_SHARE = "share_the_app_clicks";
  public static final String LABEL_ABOUT_RATE = "rate_the_app_clicks";
  public static final String LABEL_QUESTIONS_ABOUT_FLIGHTS = "questions_about_flights_clicks";
  public static final String LABEL_ABOUT_GOVOYAGES = "about_govoyages_clicks";
  public static final String LABEL_ABOUT_APP = "about_this_app_clicks";
  public static final String LABEL_ABOUT_SECURE_SHOPPING = "secure_shopping_clicks";
  public static final String LABEL_ABOUT_PHONE_TOP_CLICKS = "phone_top_clicks";
  public static final String LABEL_ABOUT_PHONE_BOTTOM_CLICKS = "phone_bottom_clicks";
  public static final String LABEL_PARTIAL_MYTRIPS_SHARE = "share";
  public static final String LABEL_ADD_PASSENGER_CLICKS = "add_passenger_clicks";
  public static final String LABEL_OPEN_PAX_CLICKS = "open_pax_details";
  public static final String LABEL_DELETE_PAX_CLICKS = "delete_pax_clicks";
  public static final String LABEL_CONNECTION_LOST_RETRY = "connection_lost_try_again_clicks";
  public static final String LABEL_CONNECTION_LOST_APPEARANCES = "connection_lost_apperances";
  public static final String LABEL_NO_RESULTS_REMOVE_FILTERS = "no_results_found_undo_filtering";
  public static final String LABEL_SEARCH_NO_RESULTS = "searching_no_results_found_error";
  public static final String LABEL_FILTER_NO_RESULTS = "filtering_no_results_found_error";
  public static final String LABEL_NO_RESULTS_NEW_SEARCH = "no_results_found_search";
  public static final String LABEL_PARTIAL_COUNTRY = "country_";
  public static final String LABEL_PARTIAL_DISTANCE_UNITS = "distance_units_";
  public static final String LABEL_LAUNCH_NEW_SEARCH_CLICKS = "launch_new_search_clicks";
  public static final String LABEL_WALKTHROUGH_SCREEN_X = "walkthrough_screen_";
  public static final String LABEL_SEARCH_FLIGHT = "search_flight";
  public static final String LABEL_ADD_BOOKING = "add_booking";
  public static final String LABEL_CROSS_SELL_HOTEL_CLICKS = "cross_sell_hotel_clicks";
  public static final String LABEL_CROSS_SELL_CAR_CLICKS = "cross_sell_car_clicks";
  public static final String LABEL_OPEN_FULL_PRICE_SELECTOR = "open_full_price_selector";
  public static final String LABEL_APPLY_PAYMENT = "apply_";
  public static final String LABEL_CONFIRMATION_BANKTRANSFER = "complete_by_bank_transfer_open";

  // Values for the Widget
  public static final String CATEGORY_WIDGET = "native_widget";

  public static final String ACTION_WIDGET_EMPTY = "user_has_not_searched";
  public static final String ACTION_WIDGET_SEARCH = "user_has_searched";
  public static final String ACTION_WIDGET_BOOKED = "user_has_booked";
  public static final String ACTION_WIDGET_FIRST_UPDATE = "widget_activation";

  public static final String LABEL_WIDGET_EMPTY = "notification_no_search_taps";
  public static final String LABEL_WIDGET_SEARCH = "notification_search_%s_taps";
  public static final String LABEL_WIDGET_BOOKED = "notification_booking_taps";
  public static final String LABEL_WIDGET_FIRST_UPDATE = "activated";

  /**
   * Constructor to prevent the instantiation.
   */
  private GAnalyticsNames() {
    // Nothing
  }
}
