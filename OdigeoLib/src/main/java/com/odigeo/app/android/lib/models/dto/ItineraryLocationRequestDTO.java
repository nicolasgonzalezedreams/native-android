package com.odigeo.app.android.lib.models.dto;

/**
 * <p>Java class for ItineraryLocationRequest complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="ItineraryLocationRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="iataCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="geoNodeId" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
public class ItineraryLocationRequestDTO {
  protected String name;
  protected String iataCode;
  protected Integer geoNodeId;

  public ItineraryLocationRequestDTO() {

  }

  public ItineraryLocationRequestDTO(String name, String iataCode) {
    this.name = name;
    this.iataCode = iataCode;
  }

  /**
   * Gets the value of the name property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the value of the name property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setName(String value) {
    this.name = value;
  }

  /**
   * Gets the value of the iataCode property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getIataCode() {
    return iataCode;
  }

  /**
   * Sets the value of the iataCode property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setIataCode(String value) {
    this.iataCode = value;
  }

  /**
   * Gets the value of the geoNodeId property.
   *
   * @return possible object is
   * {@link Integer }
   */
  public Integer getGeoNodeId() {
    return geoNodeId;
  }

  /**
   * Sets the value of the geoNodeId property.
   *
   * @param value allowed object is
   * {@link Integer }
   */
  public void setGeoNodeId(Integer value) {
    this.geoNodeId = value;
  }
}
