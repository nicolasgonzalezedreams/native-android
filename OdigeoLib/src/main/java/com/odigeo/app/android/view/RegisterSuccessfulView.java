package com.odigeo.app.android.view;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.dataodigeo.tracker.TrackerConstants;
import com.odigeo.presenter.RegisterViewPresenter;
import com.odigeo.presenter.contracts.views.BaseViewInterface;

/**
 * Created by julio.kun on 8/21/2015.
 */
public class RegisterSuccessfulView extends BaseView<RegisterViewPresenter>
    implements BaseViewInterface {

  private static final String USER_EMAIL = "USER_EMAIL";

  public static RegisterSuccessfulView newInstance(String email) {
    RegisterSuccessfulView view = new RegisterSuccessfulView();
    Bundle args = new Bundle();
    args.putString(USER_EMAIL, email);
    view.setArguments(args);
    return view;
  }

  private String getEmail() {
    Bundle args = this.getArguments();
    String email = "";
    if (args != null) {
      email = args.getString(USER_EMAIL);
    }
    return email;
  }

  @Override protected RegisterViewPresenter getPresenter() {
    return null;
  }

  @Override public void onResume() {
    super.onResume();
    mTracker.trackAnalyticsScreen(TrackerConstants.SCREEN_REGISTER_SUCCESS);
  }

  @Override protected int getFragmentLayout() {
    return R.layout.sso_signup_success;
  }

  @Override protected void initComponent(View view) {
    String email = getEmail();

    int lightColour = ContextCompat.getColor(getActivity(), R.color.basic_light);

    // Apply a different foreground color to the email.
    TextView tvConfirmationEmail = (TextView) view.findViewById(R.id.tvConfirmationEmail);
    tvConfirmationEmail.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_REGISTER_CHECKMAIL_MSG, email));

    TextView tvCheckEmail = (TextView) view.findViewById(R.id.tvCheckEmailSpam);
    tvCheckEmail.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.SSO_REGISTER_CANTFIND_MSG, email));

    ImageView check = (ImageView) view.findViewById(R.id.confirm_your_account_img);
    check.getDrawable().setColorFilter(lightColour, PorterDuff.Mode.MULTIPLY);
    ImageView mail = (ImageView) view.findViewById(R.id.check_your_email_img);
    mail.getDrawable().setColorFilter(lightColour, PorterDuff.Mode.MULTIPLY);
    ImageView info = (ImageView) view.findViewById(R.id.cant_find_email_img);
    info.getDrawable().setColorFilter(lightColour, PorterDuff.Mode.MULTIPLY);
  }

  @Override protected void setListeners() {

  }

  @Override protected void initOneCMSText(View view) {

  }
}