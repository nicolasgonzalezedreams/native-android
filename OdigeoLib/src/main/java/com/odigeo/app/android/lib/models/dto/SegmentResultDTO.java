package com.odigeo.app.android.lib.models.dto;

/**
 * @author miguel
 */
public class SegmentResultDTO {

  protected SegmentDTO segment;
  protected int id;

  /**
   * Gets the value of the segment property.
   *
   * @return possible object is {@link SegmentDTO }
   */
  public SegmentDTO getSegment() {
    return segment;
  }

  /**
   * Sets the value of the segment property.
   *
   * @param value allowed object is {@link SegmentDTO }
   */
  public void setSegment(SegmentDTO value) {
    this.segment = value;
  }

  /**
   * Gets the value of the id property.
   */
  public int getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   */
  public void setId(int value) {
    this.id = value;
  }
}
