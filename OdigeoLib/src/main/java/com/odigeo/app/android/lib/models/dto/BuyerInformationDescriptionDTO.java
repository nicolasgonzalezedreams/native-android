package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;
import java.util.List;

@Deprecated public class BuyerInformationDescriptionDTO implements Serializable {

  protected List<BuyerIdentificationTypeDTO> identificationTypes;
  protected RequiredFieldDTO needsName;
  protected RequiredFieldDTO needsLastNames;
  protected RequiredFieldDTO needsMail;
  protected RequiredFieldDTO needsIdentification;
  protected RequiredFieldDTO needsDateOfBirth;
  protected RequiredFieldDTO needsCpf;
  protected RequiredFieldDTO needsAddress;
  protected RequiredFieldDTO needsCityName;
  protected RequiredFieldDTO needsStateName;
  protected RequiredFieldDTO needsZipCode;
  protected RequiredFieldDTO needsPhoneNumber;
  protected RequiredFieldDTO needsAlternativePhoneNumber;
  protected RequiredFieldDTO needsCountryCode;

  public List<BuyerIdentificationTypeDTO> getIdentificationTypes() {
    return identificationTypes;
  }

  public void setIdentificationTypes(List<BuyerIdentificationTypeDTO> identificationTypes) {
    this.identificationTypes = identificationTypes;
  }

  /**
   * Gets the value of the needsName property.
   *
   * @return possible object is
   * {@link RequiredFieldDTO }
   */
  public RequiredFieldDTO getNeedsName() {
    return needsName;
  }

  /**
   * Sets the value of the needsName property.
   *
   * @param value allowed object is
   * {@link RequiredFieldDTO }
   */
  public void setNeedsName(RequiredFieldDTO value) {
    this.needsName = value;
  }

  /**
   * Gets the value of the needsLastNames property.
   *
   * @return possible object is
   * {@link RequiredFieldDTO }
   */
  public RequiredFieldDTO getNeedsLastNames() {
    return needsLastNames;
  }

  /**
   * Sets the value of the needsLastNames property.
   *
   * @param value allowed object is
   * {@link RequiredFieldDTO }
   */
  public void setNeedsLastNames(RequiredFieldDTO value) {
    this.needsLastNames = value;
  }

  /**
   * Gets the value of the needsMail property.
   *
   * @return possible object is
   * {@link RequiredFieldDTO }
   */
  public RequiredFieldDTO getNeedsMail() {
    return needsMail;
  }

  /**
   * Sets the value of the needsMail property.
   *
   * @param value allowed object is
   * {@link RequiredFieldDTO }
   */
  public void setNeedsMail(RequiredFieldDTO value) {
    this.needsMail = value;
  }

  /**
   * Gets the value of the needsIdentification property.
   *
   * @return possible object is
   * {@link RequiredFieldDTO }
   */
  public RequiredFieldDTO getNeedsIdentification() {
    return needsIdentification;
  }

  /**
   * Sets the value of the needsIdentification property.
   *
   * @param value allowed object is
   * {@link RequiredFieldDTO }
   */
  public void setNeedsIdentification(RequiredFieldDTO value) {
    this.needsIdentification = value;
  }

  /**
   * Gets the value of the needsDateOfBirth property.
   *
   * @return possible object is
   * {@link RequiredFieldDTO }
   */
  public RequiredFieldDTO getNeedsDateOfBirth() {
    return needsDateOfBirth;
  }

  /**
   * Sets the value of the needsDateOfBirth property.
   *
   * @param value allowed object is
   * {@link RequiredFieldDTO }
   */
  public void setNeedsDateOfBirth(RequiredFieldDTO value) {
    this.needsDateOfBirth = value;
  }

  /**
   * Gets the value of the needsCpf property.
   *
   * @return possible object is
   * {@link RequiredFieldDTO }
   */
  public RequiredFieldDTO getNeedsCpf() {
    return needsCpf;
  }

  /**
   * Sets the value of the needsCpf property.
   *
   * @param value allowed object is
   * {@link RequiredFieldDTO }
   */
  public void setNeedsCpf(RequiredFieldDTO value) {
    this.needsCpf = value;
  }

  /**
   * Gets the value of the needsAddress property.
   *
   * @return possible object is
   * {@link RequiredFieldDTO }
   */
  public RequiredFieldDTO getNeedsAddress() {
    return needsAddress;
  }

  /**
   * Sets the value of the needsAddress property.
   *
   * @param value allowed object is
   * {@link RequiredFieldDTO }
   */
  public void setNeedsAddress(RequiredFieldDTO value) {
    this.needsAddress = value;
  }

  /**
   * Gets the value of the needsCityName property.
   *
   * @return possible object is
   * {@link RequiredFieldDTO }
   */
  public RequiredFieldDTO getNeedsCityName() {
    return needsCityName;
  }

  /**
   * Sets the value of the needsCityName property.
   *
   * @param value allowed object is
   * {@link RequiredFieldDTO }
   */
  public void setNeedsCityName(RequiredFieldDTO value) {
    this.needsCityName = value;
  }

  /**
   * Gets the value of the needsStateName property.
   *
   * @return possible object is
   * {@link RequiredFieldDTO }
   */
  public RequiredFieldDTO getNeedsStateName() {
    return needsStateName;
  }

  /**
   * Sets the value of the needsStateName property.
   *
   * @param value allowed object is
   * {@link RequiredFieldDTO }
   */
  public void setNeedsStateName(RequiredFieldDTO value) {
    this.needsStateName = value;
  }

  /**
   * Gets the value of the needsZipCode property.
   *
   * @return possible object is
   * {@link RequiredFieldDTO }
   */
  public RequiredFieldDTO getNeedsZipCode() {
    return needsZipCode;
  }

  /**
   * Sets the value of the needsZipCode property.
   *
   * @param value allowed object is
   * {@link RequiredFieldDTO }
   */
  public void setNeedsZipCode(RequiredFieldDTO value) {
    this.needsZipCode = value;
  }

  /**
   * Gets the value of the needsPhoneNumber property.
   *
   * @return possible object is
   * {@link RequiredFieldDTO }
   */
  public RequiredFieldDTO getNeedsPhoneNumber() {
    return needsPhoneNumber;
  }

  /**
   * Sets the value of the needsPhoneNumber property.
   *
   * @param value allowed object is
   * {@link RequiredFieldDTO }
   */
  public void setNeedsPhoneNumber(RequiredFieldDTO value) {
    this.needsPhoneNumber = value;
  }

  /**
   * Gets the value of the needsAlternativePhoneNumber property.
   *
   * @return possible object is
   * {@link RequiredFieldDTO }
   */
  public RequiredFieldDTO getNeedsAlternativePhoneNumber() {
    return needsAlternativePhoneNumber;
  }

  /**
   * Sets the value of the needsAlternativePhoneNumber property.
   *
   * @param value allowed object is
   * {@link RequiredFieldDTO }
   */
  public void setNeedsAlternativePhoneNumber(RequiredFieldDTO value) {
    this.needsAlternativePhoneNumber = value;
  }

  /**
   * Gets the value of the needsCountryCode property.
   *
   * @return possible object is
   * {@link RequiredFieldDTO }
   */
  public RequiredFieldDTO getNeedsCountryCode() {
    return needsCountryCode;
  }

  /**
   * Sets the value of the needsCountryCode property.
   *
   * @param value allowed object is
   * {@link RequiredFieldDTO }
   */
  public void setNeedsCountryCode(RequiredFieldDTO value) {
    this.needsCountryCode = value;
  }
}
