package com.odigeo.app.android.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.BaggageUtils;
import com.odigeo.app.android.lib.utils.HtmlUtils;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.shoppingCart.SelectableBaggageDescriptor;
import java.util.List;

public class BaggageOptionsAdapter extends ArrayAdapter<SelectableBaggageDescriptor> {

  public BaggageOptionsAdapter(Context context,
      List<SelectableBaggageDescriptor> baggageDescriptorList) {
    super(context, R.layout.baggage_option_item, baggageDescriptorList);
  }

  @NonNull @Override
  public final View getView(int position, View convertView, @NonNull ViewGroup parent) {
    return getCustomView(position, parent);
  }

  private View getCustomView(int position, ViewGroup parent) {
    LayoutInflater inflater =
        (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View root = inflater.inflate(R.layout.baggage_option_item, parent, false);

    TextView baggageDescription = (TextView) root.findViewById(R.id.tvBaggageDescription);
    TextView baggagePrice = (TextView) root.findViewById(R.id.tvBaggagePrice);

    SelectableBaggageDescriptor selectableBaggageDescriptor = getItem(position);

    String description;

    if (selectableBaggageDescriptor == null) {
      description =
          LocalizablesFacade.getString(getContext(), OneCMSKeys.PASSENGER_BAGGAGE_SELECTION)
              .toString();
      baggagePrice.setVisibility(View.GONE);
    } else {
      int pieces = selectableBaggageDescriptor.getBaggageDescriptor().getPieces();

      if (pieces == 0) {
        description =
            LocalizablesFacade.getString(getContext(), OneCMSKeys.PASSENGER_NO_BAGGAGE_SELECTION)
                .toString();
        baggagePrice.setVisibility(View.GONE);
      } else {
        description = BaggageUtils.getBaggageOptionText(getContext(), pieces,
            selectableBaggageDescriptor.getBaggageDescriptor().getKilos(),
            LocalizablesFacade.getRawString(getContext(), OneCMSKeys.PASSENGER_BAGGAGE_MAX_WEIGHT));

        String price = "+" + LocaleUtils.getLocalizedCurrencyValue(
            selectableBaggageDescriptor.getPrice().doubleValue());
        baggagePrice.setText(price);
      }
    }

    baggageDescription.setText(HtmlUtils.formatHtml(description));

    return root;
  }
}