package com.odigeo.app.android.lib.models.dto;

import java.math.BigDecimal;

public class AdditionalProductRequestDTO {

  protected Long id;
  protected String reference;
  protected BigDecimal price;
  protected String description;
  protected AdditionalProductTypeDTO additionalProductType;
  protected String currency;

  /**
   * Gets the value of the id property.
   *
   * @return possible object is {@link Long }
   */
  public Long getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   *
   * @param value allowed object is {@link Long }
   */
  public void setId(Long value) {
    this.id = value;
  }

  /**
   * Gets the value of the reference property.
   *
   * @return possible object is {@link String }
   */
  public String getReference() {
    return reference;
  }

  /**
   * Sets the value of the reference property.
   *
   * @param value allowed object is {@link String }
   */
  public void setReference(String value) {
    this.reference = value;
  }

  /**
   * Gets the value of the price property.
   *
   * @return possible object is {@link java.math.BigDecimal }
   */
  public BigDecimal getPrice() {
    return price;
  }

  /**
   * Sets the value of the price property.
   *
   * @param value allowed object is {@link java.math.BigDecimal }
   */
  public void setPrice(BigDecimal value) {
    this.price = value;
  }

  /**
   * Gets the value of the description property.
   *
   * @return possible object is {@link String }
   */
  public String getDescription() {
    return description;
  }

  /**
   * Sets the value of the description property.
   *
   * @param value allowed object is {@link String }
   */
  public void setDescription(String value) {
    this.description = value;
  }

  /**
   * Gets the value of the additionalProductType property.
   *
   * @return possible object is {@link AdditionalProductType }
   */
  public AdditionalProductTypeDTO getAdditionalProductType() {
    return additionalProductType;
  }

  /**
   * Sets the value of the additionalProductType property.
   *
   * @param value allowed object is {@link AdditionalProductType }
   */
  public void setAdditionalProductType(AdditionalProductTypeDTO value) {
    this.additionalProductType = value;
  }

  /**
   * Gets the value of the currency property.
   *
   * @return possible object is {@link String }
   */
  public String getCurrency() {
    return currency;
  }

  /**
   * Sets the value of the currency property.
   *
   * @param value allowed object is {@link String }
   */
  public void setCurrency(String value) {
    this.currency = value;
  }
}
