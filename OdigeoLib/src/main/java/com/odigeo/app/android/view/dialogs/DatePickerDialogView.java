package com.odigeo.app.android.view.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import com.odigeo.app.android.lib.R;
import java.util.Calendar;
import java.util.Date;

public class DatePickerDialogView extends DialogFragment
    implements DatePickerDialog.OnDateSetListener {

  private static String MIN_DATE_BIRTHDATE = "min_date_birthdate";
  private static String MAX_DATE_BIRTHDATE = "max_date_birthdate";
  DatePickerDialog mDatePickerDialog;
  private long mMaxDate = 0, mMinDate = 0;
  private onClickDatePickerListener mOnDateSelectedListener;

  public static DatePickerDialogView newInstance(long maximumDate, long minimumDate,
      onClickDatePickerListener onDateSelectedListener) {
    Bundle args = new Bundle();
    args.putLong(MAX_DATE_BIRTHDATE, maximumDate);
    args.putLong(MIN_DATE_BIRTHDATE, minimumDate);

    DatePickerDialogView fragment = new DatePickerDialogView();
    fragment.setOnDateSelectedListener(onDateSelectedListener);
    fragment.setArguments(args);
    return fragment;
  }

  @Override public Dialog onCreateDialog(Bundle savedInstanceState) {
    Bundle args = getArguments();
    if (args.containsKey(MIN_DATE_BIRTHDATE) && args.containsKey(MAX_DATE_BIRTHDATE)) {
      mMaxDate = args.getLong(MAX_DATE_BIRTHDATE);
      mMinDate = args.getLong(MIN_DATE_BIRTHDATE);
    }

    final Calendar c = Calendar.getInstance();
    c.add(Calendar.DAY_OF_MONTH, -1);
    int year = c.get(Calendar.YEAR);
    int month = c.get(Calendar.MONTH);
    int day = c.get(Calendar.DAY_OF_MONTH);

    if (mDatePickerDialog == null) {
      mDatePickerDialog =
          new DatePickerDialog(getActivity(), R.style.MaterialTheme_DialogTheme, this, year, month,
              day);
    }

    if ((mMaxDate != 0) && (mMinDate != 0)) {
      mDatePickerDialog.getDatePicker().setMaxDate(mMaxDate);
      mDatePickerDialog.getDatePicker().setMinDate(mMinDate);
    } else if ((mMaxDate == 0) && (mMinDate != 0)) {
      mDatePickerDialog.getDatePicker().setMinDate(mMinDate);
    } else if (mMaxDate != 0) {
      mDatePickerDialog.getDatePicker().setMaxDate(mMaxDate);
    }

    mDatePickerDialog.setTitle("");

    return mDatePickerDialog;
  }

  public void setOnDateSelectedListener(onClickDatePickerListener onDateSelectedListener) {
    mOnDateSelectedListener = onDateSelectedListener;
  }

  @Override public void onDateSet(DatePicker view, int year, int month, int day) {
    mOnDateSelectedListener.onDateDateSelected(year, month, day);

    Calendar minimumCalendar = getMinimunDate();
    Calendar selectedCalendar = Calendar.getInstance();
    selectedCalendar.clear();
    selectedCalendar.set(Calendar.MONTH, month);
    selectedCalendar.set(Calendar.YEAR, year);
    selectedCalendar.set(Calendar.DAY_OF_MONTH, day);

    if (selectedCalendar.before(minimumCalendar)) {
      int minDay = getMinimunDate().get(Calendar.DAY_OF_MONTH);
      mOnDateSelectedListener.onDateDateSelected(year, month + 1, minDay);
    } else {
      mOnDateSelectedListener.onDateDateSelected(year, month + 1, day);
    }
  }

  private Calendar getMinimunDate() {
    Date minDate = new Date(mMinDate);
    Calendar selectedCalendar = Calendar.getInstance();
    selectedCalendar.clear();
    selectedCalendar.setTime(minDate);
    return selectedCalendar;
  }

  public interface onClickDatePickerListener {
    void onDateDateSelected(int year, int month, int day);
  }
}
