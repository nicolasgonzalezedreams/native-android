package com.odigeo.app.android.view.textwatchers.fieldsvalidation.creditcardvalidators;

import com.odigeo.app.android.view.textwatchers.fieldsvalidation.BaseValidator;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;

public class DinersClubValidator extends BaseValidator implements FieldValidator {

  public static final String REGEX_DINNERS = "(?:^(3[0689]{1}[0-9]{12}))$";

  @Override public boolean validateField(String fieldInformation) {
    return checkField(fieldInformation, REGEX_DINNERS);
  }

  @Override public boolean validateCodification(String fieldInformation) {
    return validateLatinCharset(fieldInformation);
  }
}
