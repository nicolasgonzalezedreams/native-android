package com.odigeo.app.android.lib.consts;

public final class Emojis {

  private Emojis() {
  }

  public static final String DIRECT_HIT = "\uD83C\uDFAF";
}
