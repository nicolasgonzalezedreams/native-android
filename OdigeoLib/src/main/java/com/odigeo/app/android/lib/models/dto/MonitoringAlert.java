package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class MonitoringAlert implements Serializable {
  private String provider;
  private String description;

  public final String getProvider() {
    return provider;
  }

  public final void setProvider(String provider) {
    this.provider = provider;
  }

  public final String getDescription() {
    return description;
  }

  public final void setDescription(String description) {
    this.description = description;
  }
}
