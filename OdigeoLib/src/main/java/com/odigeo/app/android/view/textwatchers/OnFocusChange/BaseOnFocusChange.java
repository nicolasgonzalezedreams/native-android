package com.odigeo.app.android.view.textwatchers.OnFocusChange;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.app.android.view.snackbars.CustomSnackbar;
import com.odigeo.app.android.view.textwatchers.fieldsvalidation.FieldValidator;
import com.odigeo.data.localizable.LocalizableProvider;

public class BaseOnFocusChange implements View.OnFocusChangeListener {

  private Context mContext;
  private TextInputLayout mTextInputlayout;
  private FieldValidator mFieldValidator;
  private String mOneCMSKey;
  private boolean mIsFieldCorrect;
  private String oneCMSError;
  private final LocalizableProvider localizableProvider;

  public BaseOnFocusChange(Context context, TextInputLayout textInputLayout,
      FieldValidator fieldValidator, String oneCMSKey) {
    mContext = context;
    mTextInputlayout = textInputLayout;
    mFieldValidator = fieldValidator;
    mOneCMSKey = oneCMSKey;
    localizableProvider =
        ((OdigeoApp) context.getApplicationContext()).getDependencyInjector().provideLocalizables();
    initContent();
  }

  private void initContent() {
    oneCMSError = localizableProvider.getString(mOneCMSKey);
  }

  @Override public void onFocusChange(View v, boolean hasFocus) {
    if (!hasFocus) {
      validateField(((EditText) v).getText());
    }
  }

  public void validateField(Editable editableField) {
    mIsFieldCorrect = mFieldValidator.validateField(editableField.toString());

    if (!mIsFieldCorrect) {
      if (!mFieldValidator.validateCodification(editableField.toString())) {
        oneCMSError = localizableProvider.getString(OneCMSKeys.ANDROID_ONLY_LATIN_CHARACTERS);
        showKeyboardErrorSnackbar();
      } else {
        oneCMSError = localizableProvider.getString(mOneCMSKey);
      }
      setError();
    } else {
      cleanError();
    }
  }

  protected void setError() {
    mTextInputlayout.setError(oneCMSError);
    mTextInputlayout.setErrorEnabled(true);
  }

  private void cleanError() {
    mTextInputlayout.setError(null);
    mTextInputlayout.setErrorEnabled(false);
  }

  private void showKeyboardErrorSnackbar() {
    String messageText =
        LocalizablesFacade.getString(mTextInputlayout.getContext(), OneCMSKeys.KEYBOARD_SELECT_MSG)
            .toString();
    String buttonText = localizableProvider.getString(OneCMSKeys.KEYBOARD_SELECT_BUTTON);

    new CustomSnackbar(mContext, mTextInputlayout, messageText, buttonText, Snackbar.LENGTH_LONG,
        new CustomSnackbar.CustomSnackbarListener() {
          @Override public void onSnackbarActionClick() {
            InputMethodManager mgr =
                (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (mgr != null) {
              mgr.showInputMethodPicker();
            }
          }
        }).show();
  }

  boolean getIsFieldCorrect() {
    return mIsFieldCorrect;
  }
}
