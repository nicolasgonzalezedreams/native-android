package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.consts.GAnalyticsNames;
import com.odigeo.app.android.lib.interfaces.IFilterTabs;
import com.odigeo.app.android.lib.utils.BusProvider;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;

/**
 * Created by Irving Lóp on 14/10/2014.
 */
public class FilterTabsButtonWidget extends RelativeLayout implements View.OnClickListener {
  private static final int NUM_TABS_FILTER = 4;

  private IFilterTabs listener;

  private FilterButtonWidget[] tabsButtons;

  private int currentlyIndex;

  public FilterTabsButtonWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
    inflateLayout();
    init();
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.layout_filter_tabs_button, this);
    tabsButtons = new FilterButtonWidget[NUM_TABS_FILTER];
    tabsButtons[Constants.ID_TIME_FRAGMENT] =
        (FilterButtonWidget) findViewById(R.id.button_filter_tab_time);
    tabsButtons[Constants.ID_TIME_FRAGMENT].setIdIndex(Constants.ID_TIME_FRAGMENT);
    tabsButtons[Constants.ID_STOPS_FRAGMENT] =
        (FilterButtonWidget) findViewById(R.id.button_filter_tab_stops);
    tabsButtons[Constants.ID_STOPS_FRAGMENT].setIdIndex(Constants.ID_STOPS_FRAGMENT);
    tabsButtons[Constants.ID_AIRPORTS_FRAGMENT] =
        (FilterButtonWidget) findViewById(R.id.button_filter_tab_airports);
    tabsButtons[Constants.ID_AIRPORTS_FRAGMENT].setIdIndex(Constants.ID_AIRPORTS_FRAGMENT);
    tabsButtons[Constants.ID_AIRLINES_FRAGMENT] =
        (FilterButtonWidget) findViewById(R.id.button_filter_tab_airlines);
    tabsButtons[Constants.ID_AIRLINES_FRAGMENT].setIdIndex(Constants.ID_AIRLINES_FRAGMENT);
  }

  private void init() {
    currentlyIndex = Constants.ID_TIME_FRAGMENT;
    setOnClickListenerThis();
  }

  private void setOnClickListenerThis() {
    if (tabsButtons != null) {
      for (FilterButtonWidget widget : tabsButtons) {
        widget.setOnClickListener(this);
      }
    }
  }

  public final void setListener(IFilterTabs listener) {
    this.listener = listener;
  }

  @Override public final void onClick(View view) {

    boolean tabChanged = false;
    FilterButtonWidget widget = (FilterButtonWidget) view;
    // if the current fragment is Airlines, which need to validate this so selected at least one
    if (currentlyIndex != widget.getIdIndex() && listener != null && listener.onClickFilterTab(
        widget.getIdIndex())) {
      tabChanged = true;
    }

    if (tabChanged) {
      tabsButtons[currentlyIndex].click();
      widget.click();

      currentlyIndex = widget.getIdIndex();
      switch (currentlyIndex) {
        case Constants.ID_TIME_FRAGMENT:
          postGAEventTrackFilterUsed(GAnalyticsNames.LABEL_FILTERS_OPEN_TIMES);
          break;
        case Constants.ID_STOPS_FRAGMENT:
          postGAEventTrackFilterUsed(GAnalyticsNames.LABEL_FILTERS_OPEN_STOPS);
          break;
        case Constants.ID_AIRPORTS_FRAGMENT:
          postGAEventTrackFilterUsed(GAnalyticsNames.LABEL_FILTERS_OPEN_AIRPORTS);
          break;
        case Constants.ID_AIRLINES_FRAGMENT:
          postGAEventTrackFilterUsed(GAnalyticsNames.LABEL_FILTERS_OPEN_AIRLINES);
          break;
        default:
          break;
      }
    }
  }

  private void postGAEventTrackFilterUsed(String label) {
    BusProvider.getInstance()
        .post(new GATrackingEvent(GAnalyticsNames.CATEGORY_FLIGHTS_FILTERS,
            GAnalyticsNames.ACTION_RESULTS_FILTERS, label));
  }
}
