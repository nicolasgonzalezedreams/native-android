package com.odigeo.app.android.lib.models.dto;

import java.util.ArrayList;
import java.util.List;

public class AccommodationSearchRequestDTO {

  protected LocationRequestDTO destination;
  protected List<AccommodationRoomRequestDTO> roomRequests;

  protected long checkInDate;
  protected long checkOutDate;

  /**
   * Gets the value of the destination property.
   *
   * @return possible object is
   * {@link LocationRequest }
   */
  public LocationRequestDTO getDestination() {
    return destination;
  }

  /**
   * Sets the value of the destination property.
   *
   * @param value allowed object is
   * {@link LocationRequest }
   */
  public void setDestination(LocationRequestDTO value) {
    this.destination = value;
  }

  /**
   * Gets the value of the roomRequests property.
   * <p/>
   * <p/>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the roomRequests property.
   * <p/>
   * <p/>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getRoomRequests().add(newItem);
   * </pre>
   * <p/>
   * <p/>
   * <p/>
   * Objects of the following type(s) are allowed in the list
   * {@link AccommodationRoomRequestDTO }
   */
  public List<AccommodationRoomRequestDTO> getRoomRequests() {
    if (roomRequests == null) {
      roomRequests = new ArrayList<AccommodationRoomRequestDTO>();
    }
    return this.roomRequests;
  }

  /**
   * Gets the value of the checkInDate property.
   *
   * @return possible object is
   * {@link long }
   */
  public long getCheckInDate() {
    return checkInDate;
  }

  /**
   * Sets the value of the checkInDate property.
   *
   * @param value allowed object is
   * {@link long }
   */
  public void setCheckInDate(long value) {
    this.checkInDate = value;
  }

  /**
   * Gets the value of the checkOutDate property.
   *
   * @return possible object is
   * {@link long }
   */
  public long getCheckOutDate() {
    return checkOutDate;
  }

  /**
   * Sets the value of the checkOutDate property.
   *
   * @param value allowed object is
   * {@link long }
   */
  public void setCheckOutDate(long value) {
    this.checkOutDate = value;
  }
}
