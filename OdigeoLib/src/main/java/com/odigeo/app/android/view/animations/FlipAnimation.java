package com.odigeo.app.android.view.animations;

import android.animation.Animator;
import android.support.annotation.DrawableRes;
import android.view.View;
import android.widget.ImageView;

public class FlipAnimation {

  private static int ROTATION_Y_180 = 180;
  private View mView;
  @DrawableRes private int lastDrawable;

  public FlipAnimation(View view, @DrawableRes int unknownDrawableCard) {
    mView = view;
    lastDrawable = unknownDrawableCard;
  }

  public void flipRight(final @DrawableRes int newImageToShow) {
    if (lastDrawable != newImageToShow) {
      mView.animate().rotationY(ROTATION_Y_180).setListener(new Animator.AnimatorListener() {
        @Override public void onAnimationStart(Animator animation) {

        }

        @Override public void onAnimationEnd(Animator animation) {
          if (mView instanceof ImageView) {
            lastDrawable = newImageToShow;
            ImageView imageView = (ImageView) mView;
            imageView.setImageResource(newImageToShow);
            mView.setRotationY(0);
          }
        }

        @Override public void onAnimationCancel(Animator animation) {

        }

        @Override public void onAnimationRepeat(Animator animation) {

        }
      });
    }
  }

  public void flipLeft(final @DrawableRes int newImageToShow) {
    if (lastDrawable != newImageToShow) {
      mView.animate().rotationY(-ROTATION_Y_180).setListener(new Animator.AnimatorListener() {
        @Override public void onAnimationStart(Animator animation) {

        }

        @Override public void onAnimationEnd(Animator animation) {
          if (mView instanceof ImageView) {
            lastDrawable = newImageToShow;
            ImageView imageView = (ImageView) mView;
            imageView.setImageResource(newImageToShow);
            mView.setRotationY(0);
          }
        }

        @Override public void onAnimationCancel(Animator animation) {

        }

        @Override public void onAnimationRepeat(Animator animation) {

        }
      });
    }
  }
}
