package com.odigeo.app.android.lib.ui.widgets.base;

import android.content.Context;
import android.support.annotation.IdRes;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;

/**
 * Created by emiliano.desantis on 29/09/2014.
 */
public abstract class UnfoldingRowsWidget extends CustomField {
  //    private View rootView;

  protected LinearLayout panelUnfolding;
  private TextView legend;
  private ImageView button;
  private LinearLayout panelMainRow;
  private View separatorLine;
  private boolean isExpandable;

  public UnfoldingRowsWidget(Context context, AttributeSet attrs) {

    super(context, attrs);

    initialize(context);
  }

  private void initialize(Context context) {
    inflate(context, getLayout(), this);

    legend = (TextView) getRootView().findViewById(R.id.textView);
    button = (ImageView) getRootView().findViewById(R.id.button);
    separatorLine = findViewById(getIdSeparatorLine());
    panelUnfolding = (LinearLayout) findViewById(R.id.panelUnfolding);

    panelMainRow = (LinearLayout) findViewById(R.id.panelMainRow);
    panelMainRow.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        toggleNoticePanel();
      }
    });
    separatorLine.setVisibility(GONE);
    panelUnfolding.setVisibility(GONE);
    button.setImageResource(R.drawable.more_info);
  }

  private void disableNoticePanel() {
    separatorLine.setVisibility(GONE);
    panelUnfolding.setVisibility(GONE);
    panelMainRow.setClickable(false);
    button.setVisibility(GONE);
  }

  private void enableNoticePanel() {
    separatorLine.setVisibility(VISIBLE);
    panelUnfolding.setVisibility(VISIBLE);
    panelMainRow.setClickable(true);
    button.setVisibility(VISIBLE);
  }

  protected void toggleNoticePanel() {
    if (panelUnfolding.getVisibility() == VISIBLE) {
      separatorLine.setVisibility(GONE);
      panelUnfolding.setVisibility(GONE);
      button.setImageResource(R.drawable.more_info);
    } else {
      separatorLine.setVisibility(VISIBLE);
      panelUnfolding.setVisibility(VISIBLE);
      button.setImageResource(R.drawable.less_info);
    }
  }

  public String getLegend() {

    return legend.getText().toString();
  }

  public void setLegend(String legend) {

    this.legend.setText(legend);
  }

  public boolean isExpandable() {
    return isExpandable;
  }

  public void setExpandable(boolean isExpandable) {
    this.isExpandable = isExpandable;

    if (isExpandable) {
      enableNoticePanel();
    } else {
      disableNoticePanel();
    }
  }

  public abstract int getLayout();

  @IdRes public abstract int getIdSeparatorLine();
}
