package com.odigeo.app.android.lib.models.dto;

import java.io.Serializable;

public class UserAgentInfo implements Serializable {
  protected String device;
  protected String os;
  protected String osVersion;

  /**
   * Gets the value of the device property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getDevice() {
    return device;
  }

  /**
   * Sets the value of the device property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setDevice(String value) {
    this.device = value;
  }

  /**
   * Gets the value of the os property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getOs() {
    return os;
  }

  /**
   * Sets the value of the os property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setOs(String value) {
    this.os = value;
  }

  /**
   * Gets the value of the osVersion property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getOsVersion() {
    return osVersion;
  }

  /**
   * Sets the value of the osVersion property.
   *
   * @param value allowed object is
   * {@link String }
   */
  public void setOsVersion(String value) {
    this.osVersion = value;
  }
}
