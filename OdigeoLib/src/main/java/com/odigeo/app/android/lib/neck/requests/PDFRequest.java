package com.odigeo.app.android.lib.neck.requests;

import android.os.Environment;
import android.util.Pair;
import com.globant.roboneck.common.NeckCookie;
import com.globant.roboneck.requests.BaseNeckHttpRequest;
import com.globant.roboneck.requests.BaseNeckRequestException;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.odigeo.app.android.lib.neck.errors.NeckNotInternetError;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by emiliano.desantis on 19/09/2014.
 */
public class PDFRequest extends BaseNeckHttpRequest<File, File> {
  private final String url;
  private Logger logger;

  public PDFRequest(String url) {
    super(File.class);
    this.url = url;
  }

  @Override protected String getUrl() {

    return this.url;
  }

  @Override protected String getBody() {

    return null;
  }

  @Override public Object getCachekey() {

    return getUrl();
  }

  @Override public long getCacheExpirationTime() {

    return DurationInMillis.ALWAYS_EXPIRED;
  }

  @Override protected Map<String, String> getHeaders() {

    return null;
  }

  @Override protected Method getMethod() {

    return Method.GET;
  }

  @Override protected List<Pair<String, String>> getQueryParameters() {
    return null;
  }

  @Override protected List<NeckCookie> getCookies() {

    return null;
  }

  @Override protected File getRequestModel(File response) {

    return response;
  }

  @Override protected boolean isLogicError(File response) {

    return response == null;
  }

  @Override
  protected BaseNeckRequestException.Error processError(int arg0, File arg1, String arg2) {

    return new NeckNotInternetError("Define error string here", 100, 1);
  }

  @Override protected File processContent(String responseBody) {

        /*
        Writer writer;
        File root = Environment.getExternalStorageDirectory();
        File outDir = new File(root.getAbsolutePath() + File.separator + "EZ_time_tracker");
        File outputFile;

        if (!outDir.isDirectory())
        {
            outDir.mkdir();
        }

        try
        {
            if (!outDir.isDirectory())
            {
                throw new IOException(
                        "Unable to create directory EZ_time_tracker. Maybe the SD card is mounted?"
                );
            }
            outputFile = new File(outDir, "target.pdf");
            writer = new BufferedWriter(new FileWriter(outputFile));
            writer.write(responseBody);
            writer.close();
            return outputFile;
        }
        catch (IOException e)
        {
            Log.w("eztt", e.getMessage(), e);
            return null;
        } */

    FileOutputStream fop = null;
    File file;

    try {
      String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
      File folder = new File(extStorageDirectory, "govoyages");
      folder.mkdir();

      //            file = new File(context.getFilesDir(), "target.pdf");
      file = new File(extStorageDirectory, "target.pdf");
      fop = new FileOutputStream(file);

      // if file doesnt exists, then create it
      //            if (!file.exists())
      //            {
      //                file.createNewFile();
      //            }

      // get the content in bytes
      byte[] contentInBytes = responseBody.getBytes();

      fop.write(contentInBytes);
      fop.flush();
      fop.close();

      return file;
    } catch (IOException e) {
      logger.log(Level.SEVERE, e.getMessage(), e);
      return null;
    } finally {
      try {
        if (fop != null) {
          fop.close();
        }
      } catch (IOException e) {
        logger.log(Level.SEVERE, e.getMessage(), e);
      }
    }
  }
}
