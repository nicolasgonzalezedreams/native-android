package com.odigeo.app.android.lib.models.dto;

/**
 * Extended SectionResult with Section Type
 */
public final class MobileSectionResult {

  /**
   *
   */
  //    private static final long serialVersionUID = 3385231093164971670L;

  /**
   * Indicates the type of this section to compose the itinerary
   */
  private MobileSectionResultType sectionType;

  /**
   *
   */
  private Long itineraryBookingId;

  private SectionDTO section;

  /**
   * @return the sectionType
   */
  public MobileSectionResultType getSectionType() {
    return sectionType;
  }

  /**
   * @param sectionType the sectionType to set
   */
  void setSectionType(MobileSectionResultType sectionType) {
    this.sectionType = sectionType;
  }

  /**
   * @return the itineraryBookingId
   */
  public Long getItineraryBookingId() {
    return itineraryBookingId;
  }

  /**
   * @param itineraryBookingId the itineraryBookingId to set
   */
  void setItineraryBookingId(Long itineraryBookingId) {
    this.itineraryBookingId = itineraryBookingId;
  }

  public SectionDTO getSection() {
    return section;
  }

  public void setSection(SectionDTO section) {
    this.section = section;
  }
}
