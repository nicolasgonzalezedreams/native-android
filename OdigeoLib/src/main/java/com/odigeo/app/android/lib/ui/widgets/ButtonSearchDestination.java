package com.odigeo.app.android.lib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.ui.widgets.base.ButtonWithAndWithoutHint;
import com.odigeo.data.entity.geo.City;

/**
 * Created by ManuelOrtiz on 11/09/2014.
 */
public class ButtonSearchDestination extends ButtonWithAndWithoutHint {

  private City city;

  public ButtonSearchDestination(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public final int getLayout() {
    return R.layout.layout_search_trip_destination_widget;
  }

  @Override public final int getEmptyLayout() {
    return R.layout.layout_search_trip_destination_empty_widget;
  }

  public final City getCity() {
    return city;
  }

  public final void setCity(City city) {
    this.city = city;

    if (city != null) {
      String text = " ";
      if (city.getCityName() != null) {
        text += city.getCityName();
        text += ", ";
      }
      if (city.getCountryName() != null) {
        text += city.getCountryName();
      }
      this.setText(text);
      this.setHint(city.getIataCode());
    } else {
      this.clear();
    }
  }
}

