/**
 * Created by M. en C. Javier Silva Perez on 2/10/2015.
 */

package com.odigeo.app.android.lib.models;

import android.support.annotation.Nullable;
import com.odigeo.app.android.lib.models.dto.SegmentTypeIndexDTO;
import com.odigeo.app.android.lib.models.dto.SelectableBaggageDescriptorDTO;

/**
 * Custom model for buy baggage items, this class will wrap up the baggage option text depending on
 * {@link com.odigeo.app.android.lib.models.dto.BaggageConditionsDTO} and {@link
 * com.odigeo.app.android.lib.fragments.BuyBaggageFragment.BaggageScenario}
 *
 * @author M. en C. Javier Silva Perez
 * @version 1.0
 * @since 2/10/2015
 */
public class BuyBaggageItemModel {

  @Nullable private SelectableBaggageDescriptorDTO selectableBaggageDescriptor;

  /**
   * Segment to which this model corresponds to
   */
  @Nullable private SegmentTypeIndexDTO segmentTypeIndexDTO;

  /**
   * Default constructor, does nothing
   */
  public BuyBaggageItemModel() {

  }

  /**
   * Constructor with parameters
   *
   * @param selectableBaggageDescriptor Baggage options that will be shown in the item
   * @param segmentTypeIndexDTO {@link com.odigeo.app.android.lib.models.dto.SegmentTypeIndexDTO}
   * that corresponds to
   * this item
   */
  public BuyBaggageItemModel(@Nullable SelectableBaggageDescriptorDTO selectableBaggageDescriptor,
      @Nullable SegmentTypeIndexDTO segmentTypeIndexDTO) {
    this.selectableBaggageDescriptor = selectableBaggageDescriptor;
    this.segmentTypeIndexDTO = segmentTypeIndexDTO;
  }

  /**
   * Get the segment to which the item corresponds to
   *
   * @return The {@link com.odigeo.app.android.lib.models.dto.SegmentTypeIndexDTO} for this item
   */
  @Nullable public final SegmentTypeIndexDTO getSegmentTypeIndexDTO() {
    return segmentTypeIndexDTO;
  }

  /**
   * Set the corresponding segment type index
   *
   * @param segmentTypeIndexDTO A {@link com.odigeo.app.android.lib.models.dto.SegmentTypeIndexDTO}
   * or null
   */
  public final void setSegmentTypeIndexDTO(@Nullable SegmentTypeIndexDTO segmentTypeIndexDTO) {
    this.segmentTypeIndexDTO = segmentTypeIndexDTO;
  }

  /**
   * Get the baggage item description with price, pieces and kilos
   *
   * @return the item baggage options
   */
  @Nullable public final SelectableBaggageDescriptorDTO getSelectableBaggageDescriptor() {
    return selectableBaggageDescriptor;
  }

  /**
   * Sets the item baggage description
   *
   * @param selectableBaggageDescriptor Baggage options that will be shown in the item
   */
  public final void setSelectableBaggageDescriptor(
      @Nullable SelectableBaggageDescriptorDTO selectableBaggageDescriptor) {
    this.selectableBaggageDescriptor = selectableBaggageDescriptor;
  }
}
