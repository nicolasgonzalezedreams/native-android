package com.odigeo.data.entity.extensions;

import android.os.Parcel;
import android.os.Parcelable;
import com.odigeo.data.entity.carrousel.BaggageBeltCard;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.CarouselLocation;

public class UIBaggageBeltCard extends BaggageBeltCard implements Parcelable {

  public static final Creator<UIBaggageBeltCard> CREATOR = new Creator<UIBaggageBeltCard>() {

    @Override public UIBaggageBeltCard createFromParcel(Parcel in) {
      return new UIBaggageBeltCard(in);
    }

    @Override public UIBaggageBeltCard[] newArray(int size) {
      return new UIBaggageBeltCard[size];
    }
  };

  public UIBaggageBeltCard(BaggageBeltCard card) {
    setId(card.getId());
    setType(card.getType());
    setTripToHeader(card.getTripToHeader());
    setCarrier(card.getCarrier());
    setCarrierName(card.getCarrierName());
    setFlightId(card.getFlightId());
    setFrom(card.getFrom());
    setTo(card.getTo());
    setPriority(card.getPriority());
    setImage(card.getImage());
    setBaggageBelt(card.getBaggageBelt());
    setHasToShowRefreshButton(card.hasToShowRefreshButton());
  }

  protected UIBaggageBeltCard(Parcel in) {
    setId(in.readLong());
    setType(Card.CardType.valueOf(in.readString()));
    setTripToHeader(in.readString());
    setCarrier(in.readString());
    setCarrierName(in.readString());
    setFlightId(in.readString());
    setFrom((CarouselLocation) in.readParcelable(UICarrouselLocation.class.getClassLoader()));
    setTo((CarouselLocation) in.readParcelable(UICarrouselLocation.class.getClassLoader()));
    setPriority(in.readInt());
    setImage(in.readString());
    setBaggageBelt(in.readString());
    setHasToShowRefreshButton(in.readByte() != 0);
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(getId());
    dest.writeString(getType().toString());
    dest.writeString(getTripToHeader());
    dest.writeString(getCarrier());
    dest.writeString(getCarrierName());
    dest.writeString(getFlightId());
    Parcelable from = new UICarrouselLocation(getFrom());
    dest.writeParcelable(from, flags);
    Parcelable to = new UICarrouselLocation(getTo());
    dest.writeParcelable(to, flags);
    dest.writeInt(getPriority());
    dest.writeString(getImage());
    dest.writeString(getBaggageBelt());
    dest.writeByte((byte) (hasToShowRefreshButton() ? 1 : 0));
  }
}
