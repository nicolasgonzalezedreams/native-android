package com.odigeo.data.entity.extensions;

import android.os.Parcel;
import android.os.Parcelable;
import com.odigeo.data.entity.booking.Section;

public class UISection extends Section implements Parcelable {

  public static final Creator<UISection> CREATOR = new Creator<UISection>() {
    @Override public UISection createFromParcel(Parcel in) {
      return new UISection(in);
    }

    @Override public UISection[] newArray(int size) {
      return new UISection[size];
    }
  };

  public UISection(Section section) {
    setId(section.getId());
    setSectionId(section.getSectionId());
    setAircraft(section.getAircraft());
    setArrivalDate(section.getArrivalDate());
    setArrivalTerminal(section.getArrivalTerminal());
    setBaggageAllowanceQuantity(section.getBaggageAllowanceQuantity());
    setBaggageAllowanceType(section.getBaggageAllowanceType());
    setCabinClass(section.getCabinClass());
    setDepartureDate(section.getDepartureDate());
    setDepartureTerminal(section.getDepartureTerminal());
    setDuration(section.getDuration());
    setFlightID(section.getFlightID());
    setSectionType(section.getSectionType());
    setFrom(section.getFrom());
    setTo(section.getTo());
  }

  private UISection(Parcel in) {
    setId(in.readLong());
    setSectionId(in.readString());
    setAircraft(in.readString());
    setArrivalDate(in.readLong());
    setArrivalTerminal(in.readString());
    setBaggageAllowanceQuantity(in.readString());
    setBaggageAllowanceType(in.readString());
    setCabinClass(in.readString());
    setDepartureDate(in.readLong());
    setDepartureTerminal(in.readString());
    setDuration(in.readLong());
    setFlightID(in.readString());
    setSectionType(in.readString());
    setFrom((UILocationBooking) in.readParcelable(UILocationBooking.class.getClassLoader()));
    setTo((UILocationBooking) in.readParcelable(UILocationBooking.class.getClassLoader()));
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(this.getId());
    dest.writeString(getSectionId());
    dest.writeString(getAircraft());
    dest.writeLong(getArrivalDate());
    dest.writeString(getArrivalTerminal());
    dest.writeString(getBaggageAllowanceQuantity());
    dest.writeString(getBaggageAllowanceType());
    dest.writeString(getCabinClass());
    dest.writeLong(getDepartureDate());
    dest.writeString(getDepartureTerminal());
    dest.writeLong(getDuration());
    dest.writeString(getFlightID());
    dest.writeString(getSectionType());
    Parcelable from = new UILocationBooking(getFrom());
    Parcelable to = new UILocationBooking(getTo());
    dest.writeParcelable(from, flags);
    dest.writeParcelable(to, flags);
  }

  @Override public int describeContents() {
    return 0;
  }
}
