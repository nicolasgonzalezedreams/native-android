package com.odigeo.data.entity.extensions;

import android.support.annotation.Nullable;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.BaseSpinnerItem;
import com.odigeo.data.entity.userData.UserProfile;
import java.util.ArrayList;
import java.util.List;

public class UITitleOfTraveller implements BaseSpinnerItem {

  private static String GERMAN_MARKET = "DE";

  private final UserProfile.Title title;
  private final String oneCmsKey;

  public UITitleOfTraveller(UserProfile.Title title, String oneCmsKey) {
    this.title = title;
    this.oneCmsKey = oneCmsKey;
  }

  public static List<UITitleOfTraveller> getList() {
    List<UITitleOfTraveller> list = new ArrayList<>();
    list.add(new UITitleOfTraveller(UserProfile.Title.MR, OneCMSKeys.PASSENGER_TITLE_MR));
    list.add(new UITitleOfTraveller(UserProfile.Title.MRS, OneCMSKeys.PASSENGER_TITLE_MRS));
    if (!Configuration.getInstance().getCurrentMarket().getKey().equals(GERMAN_MARKET)) {
      //German does not distinguish between two female addresses (such as miss/misses)
      list.add(new UITitleOfTraveller(UserProfile.Title.MS, OneCMSKeys.PASSENGER_TITLE_MS));
    }
    list.add(new UITitleOfTraveller(UserProfile.Title.UNKNOWN, ""));
    return list;
  }

  @Override public String getShownTextKey() {
    return oneCmsKey;
  }

  @Override public int getImageId() {
    return 0;
  }

  @Nullable @Override public String getShownText() {
    return null;
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public UserProfile.Title getData() {
    return title;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }
}
