package com.odigeo.data.entity.extensions;

import android.os.Parcel;
import android.os.Parcelable;
import com.odigeo.data.entity.carrousel.CampaignCard;
import com.odigeo.data.entity.carrousel.Card;

public class UIPromocard extends CampaignCard implements Parcelable {

  public static final Creator<UIPromocard> CREATOR = new Creator<UIPromocard>() {
    @Override public UIPromocard createFromParcel(Parcel in) {
      return new UIPromocard(in);
    }

    @Override public UIPromocard[] newArray(int size) {
      return new UIPromocard[size];
    }
  };

  public UIPromocard(CampaignCard card) {
    setId(card.getId());
    setType(card.getType());
    setTitle(card.getTitle());
    setSubtitle(card.getSubtitle());
    setPriority(card.getPriority());
    setImage(card.getImage());
    setUrl(card.getUrl());
    setUrlText(card.getUrlText());
    setUrl2(card.getUrl2());
    setUrlText2(card.getUrlText2());
  }

  private UIPromocard(Parcel in) {
    setId(in.readLong());
    setType(Card.CardType.valueOf(in.readString()));
    setTitle(in.readString());
    setSubtitle(in.readString());
    setPriority(in.readInt());
    setImage(in.readString());
    setUrl(in.readString());
    setUrlText(in.readString());
    setUrl2(in.readString());
    setUrlText2(in.readString());
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(getId());
    dest.writeString(getType().toString());
    dest.writeString(getTitle());
    dest.writeString(getSubtitle());
    dest.writeInt(getPriority());
    dest.writeString(getImage());
    dest.writeString(getUrl());
    dest.writeString(getUrlText());
    setUrl2(getUrl2());
    setUrlText2(getUrlText2());
  }

  @Override public int describeContents() {
    return 0;
  }
}
