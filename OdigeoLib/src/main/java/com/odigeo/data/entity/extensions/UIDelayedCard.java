package com.odigeo.data.entity.extensions;

import android.os.Parcel;
import android.os.Parcelable;
import com.odigeo.data.entity.carrousel.CarouselLocation;
import com.odigeo.data.entity.carrousel.DelayedSectionCard;

/**
 * Created by Javier Marsicano on 29/01/16.
 */
public class UIDelayedCard extends DelayedSectionCard implements Parcelable {

  public static final Creator<UIDelayedCard> CREATOR = new Creator<UIDelayedCard>() {
    @Override public UIDelayedCard createFromParcel(Parcel in) {
      return new UIDelayedCard(in);
    }

    @Override public UIDelayedCard[] newArray(int size) {
      return new UIDelayedCard[size];
    }
  };

  public UIDelayedCard(DelayedSectionCard card) {
    setId(card.getId());
    setType(card.getType());
    setTripToHeader(card.getTripToHeader());
    setCarrier(card.getCarrier());
    setCarrierName(card.getCarrierName());
    setFlightId(card.getFlightId());
    setFrom(card.getFrom());
    setTo(card.getTo());
    setPriority(card.getPriority());
    setImage(card.getImage());
    setArrivalDelay(card.getArrivalDelay());
    setDepartureDelay(card.getDepartureDelay());
    setHasToShowRefreshButton(card.hasToShowRefreshButton());
  }

  protected UIDelayedCard(Parcel in) {
    setId(in.readLong());
    setType(CardType.valueOf(in.readString()));
    setTripToHeader(in.readString());
    setCarrier(in.readString());
    setCarrierName(in.readString());
    setFlightId(in.readString());
    setFrom((CarouselLocation) in.readParcelable(UICarrouselLocation.class.getClassLoader()));
    setTo((CarouselLocation) in.readParcelable(UICarrouselLocation.class.getClassLoader()));
    setPriority(in.readInt());
    setImage(in.readString());
    setArrivalDelay((in.readString()));
    setDepartureDelay(in.readString());
    setHasToShowRefreshButton(in.readByte() != 0);
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(getId());
    dest.writeString(getType().toString());
    dest.writeString(getTripToHeader());
    dest.writeString(getCarrier());
    dest.writeString(getCarrierName());
    dest.writeString(getFlightId());
    Parcelable from = new UICarrouselLocation(getFrom());
    dest.writeParcelable(from, flags);
    Parcelable to = new UICarrouselLocation(getTo());
    dest.writeParcelable(to, flags);
    dest.writeInt(getPriority());
    dest.writeString(getImage());
    dest.writeString(getArrivalDelay());
    dest.writeString(getDepartureDelay());
    dest.writeByte((byte) (hasToShowRefreshButton() ? 1 : 0));
  }
}
