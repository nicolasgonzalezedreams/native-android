package com.odigeo.data.entity.extensions;

import android.os.Parcel;
import android.os.Parcelable;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.CarouselLocation;
import com.odigeo.data.entity.carrousel.DivertedSectionCard;

/**
 * Created by matia on 1/29/2016.
 */
public class UIDivertedCard extends DivertedSectionCard implements Parcelable {

  public static final Creator<UIDivertedCard> CREATOR = new Creator<UIDivertedCard>() {
    @Override public UIDivertedCard createFromParcel(Parcel in) {
      return new UIDivertedCard(in);
    }

    @Override public UIDivertedCard[] newArray(int size) {
      return new UIDivertedCard[size];
    }
  };

  public UIDivertedCard(DivertedSectionCard card) {
    setId(card.getId());
    setType(card.getType());
    setTripToHeader(card.getTripToHeader());
    setCarrier(card.getCarrier());
    setCarrierName(card.getCarrierName());
    setFlightId(card.getFlightId());
    setFrom(card.getFrom());
    setTo(card.getTo());
    setPriority(card.getPriority());
    setImage(card.getImage());
    setHasToShowRefreshButton(card.hasToShowRefreshButton());
  }

  public UIDivertedCard(Parcel in) {
    setId(in.readLong());
    setType(Card.CardType.valueOf(in.readString()));
    setTripToHeader(in.readString());
    setCarrier(in.readString());
    setCarrierName(in.readString());
    setFlightId(in.readString());
    setFrom((CarouselLocation) in.readParcelable(UICarrouselLocation.class.getClassLoader()));
    setTo((CarouselLocation) in.readParcelable(UICarrouselLocation.class.getClassLoader()));
    setPriority(in.readInt());
    setImage(in.readString());
    setHasToShowRefreshButton(in.readByte() != 0);
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(getId());
    dest.writeString(getType().toString());
    dest.writeString(getTripToHeader());
    dest.writeString(getCarrier());
    dest.writeString(getCarrierName());
    dest.writeString(getFlightId());
    Parcelable from = new UICarrouselLocation(getFrom());
    dest.writeParcelable(from, flags);
    Parcelable to = new UICarrouselLocation(getTo());
    dest.writeParcelable(to, flags);
    dest.writeInt(getPriority());
    dest.writeString(getImage());
    dest.writeByte((byte) (hasToShowRefreshButton() ? 1 : 0));
  }
}
