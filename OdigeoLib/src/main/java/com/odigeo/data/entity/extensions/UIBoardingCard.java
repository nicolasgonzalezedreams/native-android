package com.odigeo.data.entity.extensions;

import android.os.Parcel;
import android.os.Parcelable;
import com.odigeo.data.entity.carrousel.BoardingGateCard;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.CarouselLocation;

/**
 * Created by matia on 1/29/2016.
 */
public class UIBoardingCard extends BoardingGateCard implements Parcelable {

  public static final Creator<UIBoardingCard> CREATOR = new Creator<UIBoardingCard>() {
    @Override public UIBoardingCard createFromParcel(Parcel in) {
      return new UIBoardingCard(in);
    }

    @Override public UIBoardingCard[] newArray(int size) {
      return new UIBoardingCard[size];
    }
  };

  public UIBoardingCard(BoardingGateCard card) {
    setId(card.getId());
    setType(card.getType());
    setTripToHeader(card.getTripToHeader());
    setCarrier(card.getCarrier());
    setCarrierName(card.getCarrierName());
    setFlightId(card.getFlightId());
    setFrom(card.getFrom());
    setTo(card.getTo());
    setGate(card.getGate());
    setPriority(card.getPriority());
    setImage(card.getImage());
    setHasToShowRefreshButton(card.hasToShowRefreshButton());
  }

  public UIBoardingCard(Parcel in) {
    setId(in.readLong());
    setType(Card.CardType.valueOf(in.readString()));
    setTripToHeader(in.readString());
    setCarrier(in.readString());
    setCarrierName(in.readString());
    setFlightId(in.readString());
    setFrom((CarouselLocation) in.readParcelable(UICarrouselLocation.class.getClassLoader()));
    setTo((CarouselLocation) in.readParcelable(UICarrouselLocation.class.getClassLoader()));
    setGate(in.readString());
    setPriority(in.readInt());
    setImage(in.readString());
    setHasToShowRefreshButton(in.readByte() != 0);
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(getId());
    dest.writeString(getType().toString());
    dest.writeString(getTripToHeader());
    dest.writeString(getCarrier());
    dest.writeString(getCarrierName());
    dest.writeString(getFlightId());
    Parcelable from = new UICarrouselLocation(getFrom());
    dest.writeParcelable(from, flags);
    Parcelable to = new UICarrouselLocation(getTo());
    dest.writeParcelable(to, flags);
    dest.writeString(getGate());
    dest.writeInt(getPriority());
    dest.writeString(getImage());
    dest.writeByte((byte) (hasToShowRefreshButton() ? 1 : 0));
  }
}
