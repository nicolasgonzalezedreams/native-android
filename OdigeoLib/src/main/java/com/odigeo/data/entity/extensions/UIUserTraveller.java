package com.odigeo.data.entity.extensions;

import android.support.annotation.Nullable;
import com.odigeo.app.android.lib.models.dto.BaggageSelectionDTO;
import com.odigeo.app.android.lib.models.dto.TravellerIdentificationTypeDTO;
import com.odigeo.data.entity.BaseSpinnerItem;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.List;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author José Eduardo Cabrera Rivera
 * @version 1.0
 * @since 26/09/15
 */
@Deprecated public class UIUserTraveller extends UserTraveller implements BaseSpinnerItem {

  private List<BaggageSelectionDTO> baggageList;
  private TravellerIdentificationTypeDTO identificationType;
  private String identification;

  /**
   * Default constructor
   */
  public UIUserTraveller() {
  }

  /**
   * @param userTraveller
   */
  public UIUserTraveller(UserTraveller userTraveller) {
    super(userTraveller.getId(), userTraveller.getUserTravellerId(), userTraveller.getBuyer(),
        userTraveller.getEmail(), userTraveller.getTypeOfTraveller(), userTraveller.getMealType(),
        userTraveller.getUserFrequentFlyers(), userTraveller.getUserProfile(),
        userTraveller.getUserId());
  }

  @Override public String getShownTextKey() {
    return EMPTY_STRING;
  }

  @Override public int getImageId() {
    return EMPTY_RESOURCE;
  }

  @Override public String getShownText() {
    return getTravellerName();
  }

  private String getTravellerName() {
    if (getUserProfile().getFirstLastName() == null) {
      return getUserProfile().getName();
    }
    return getUserProfile().getName() + " " + getUserProfile().getFirstLastName();
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public Object getData() {
    return null;
  }

  @Override public String toString() {
    return new ToStringBuilder(this).
        append("id", this.getId()).
        append("userTravellerId", this.getUserTravellerId()).
        append("buyer", this.getBuyer()).
        append("email", this.getEmail()).
        append("userId", this.getUserId()).toString();
  }

  @Override public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }
    UIUserTraveller rhs = (UIUserTraveller) obj;
    return new EqualsBuilder().append(this.getId(), rhs.getId())
        .append(this.getUserTravellerId(), rhs.getUserTravellerId())
        .append(this.getBuyer(), rhs.getBuyer())
        .append(this.getEmail(), rhs.getEmail())
        .append(this.getUserId(), rhs.getUserId())
        .isEquals();
  }

  @Override public int hashCode() {
    return new HashCodeBuilder(17, 37).
        append(this.getId()).
        append(this.getUserTravellerId()).
        append(this.getBuyer()).
        append(this.getEmail()).
        append(this.getUserId()).
        append(this.getUserFrequentFlyers()).
        toHashCode();
  }

  public List<BaggageSelectionDTO> getBaggageList() {
    return this.baggageList;
  }

  public void setBaggageList(List<BaggageSelectionDTO> list) {
    this.baggageList = list;
  }

  public void setIdentificationType(TravellerIdentificationTypeDTO identificationType) {
    this.identificationType = identificationType;
  }

  public TravellerIdentificationTypeDTO getTravellerIdentificationType() {
    return this.identificationType;
  }

  public String getIdentification() {
    return this.identification;
  }

  public void setIdentification(String identification) {
    this.identification = identification;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }

  public int getBaggagePiecesCount() {
    int baggageCount = 0;
    for (BaggageSelectionDTO baggageSelection : baggageList) {
      baggageCount += baggageSelection.getPieces();
    }
    return baggageCount;
  }
}
