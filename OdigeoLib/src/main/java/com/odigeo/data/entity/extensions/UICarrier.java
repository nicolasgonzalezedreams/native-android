package com.odigeo.data.entity.extensions;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.data.entity.BaseSpinnerItem;
import com.odigeo.data.entity.shoppingCart.Carrier;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 25/09/15
 */
public class UICarrier extends Carrier implements BaseSpinnerItem, Comparable {

  /**
   * Constructor
   *
   * @param carrier Carrier to be converted to a UICarrier.
   */
  public UICarrier(Carrier carrier) {
    super(carrier.getCode(), carrier.getName());
  }

  @Override public String getShownTextKey() {
    return EMPTY_STRING;
  }

  @Override public int getImageId() {
    return EMPTY_RESOURCE;
  }

  @Nullable @Override public String getShownText() {
    return getName();
  }

  @Nullable @Override public String getImageUrl() {
    if (getCode() == null) {
      return null;
    } else {
      return Configuration.getInstance().getImagesSources().getUrlAirlineLogos()
          + getCode()
          + ".png";
    }
  }

  @Nullable @Override public Carrier getData() {
    return this;
  }

  @Override public int compareTo(@NonNull Object another) {
    if (another instanceof UICarrier) {
      return getName().compareTo(((UICarrier) another).getName());
    } else {
      return 0;
    }
  }

  @Override public int getPlaceHolder() {
    return R.drawable.ff_carrier_icon_placeholder;
  }

  @Override public boolean equals(Object o) {
    return o != null && o instanceof UICarrier && getCode().equals(((UICarrier) o).getCode());
  }
}
