package com.odigeo.data.entity.extensions;

import android.os.Parcel;
import android.os.Parcelable;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.CarouselLocation;
import com.odigeo.data.entity.carrousel.SectionCard;

/**
 * Created by Javier Marsicano on 01/02/16.
 */
public class UINextTrip extends SectionCard implements Parcelable {

  public static final Creator<UINextTrip> CREATOR = new Creator<UINextTrip>() {
    @Override public UINextTrip createFromParcel(Parcel in) {
      return new UINextTrip(in);
    }

    @Override public UINextTrip[] newArray(int size) {
      return new UINextTrip[size];
    }
  };

  public UINextTrip(SectionCard card) {
    setId(card.getId());
    setType(card.getType());
    setCarrier(card.getCarrier());
    setCarrierName(card.getCarrierName());
    setFlightId(card.getFlightId());
    setFrom(card.getFrom());
    setTo(card.getTo());
    setTripToHeader(card.getTripToHeader());
    setPriority(card.getPriority());
    setImage(card.getImage());
    setHasToShowRefreshButton(card.hasToShowRefreshButton());
  }

  protected UINextTrip(Parcel in) {
    setId(in.readLong());
    setType(Card.CardType.valueOf(in.readString()));
    setCarrier(in.readString());
    setCarrierName(in.readString());
    setFlightId(in.readString());
    setTripToHeader(in.readString());
    setFrom((CarouselLocation) in.readParcelable(UICarrouselLocation.class.getClassLoader()));
    setTo((CarouselLocation) in.readParcelable(UICarrouselLocation.class.getClassLoader()));
    setPriority(in.readInt());
    setImage(in.readString());
    setHasToShowRefreshButton(in.readByte() != 0);
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(getId());
    dest.writeString(getType().toString());
    dest.writeString(getCarrier());
    dest.writeString(getCarrierName());
    dest.writeString(getFlightId());
    dest.writeString(getTripToHeader());
    Parcelable from = new UICarrouselLocation(getFrom());
    dest.writeParcelable(from, flags);
    Parcelable to = new UICarrouselLocation(getTo());
    dest.writeParcelable(to, flags);
    dest.writeInt(getPriority());
    dest.writeString(getImage());
    dest.writeByte((byte) (hasToShowRefreshButton() ? 1 : 0));
  }
}
