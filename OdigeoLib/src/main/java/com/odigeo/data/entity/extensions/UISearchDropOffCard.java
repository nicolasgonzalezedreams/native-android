package com.odigeo.data.entity.extensions;

import android.os.Parcel;
import android.os.Parcelable;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.SearchDropOffCard;

public class UISearchDropOffCard extends SearchDropOffCard implements Parcelable {

  public static final Creator<UISearchDropOffCard> CREATOR = new Creator<UISearchDropOffCard>() {
    @Override public UISearchDropOffCard createFromParcel(Parcel in) {
      return new UISearchDropOffCard(in);
    }

    @Override public UISearchDropOffCard[] newArray(int size) {
      return new UISearchDropOffCard[size];
    }
  };

  public UISearchDropOffCard(SearchDropOffCard card) {
    setId(card.getId());
    setType(card.getType());
    setPriority(card.getPriority());
    setImage(card.getImage());
    setBookNowUrl(card.getBookNowUrl());
    setEditTripUrl(card.getEditTripUrl());
    setDepartureCityIATA(card.getDepartureCityIATA());
    setArrivalCityIATA(card.getArrivalCityIATA());
    setDepartureDate(card.getDepartureDate());
    setArrivalDate(card.getArrivalDate());
    setNumAdults(card.getNumAdults());
    setNumChildren(card.getNumChildren());
    setNumInfants(card.getNumInfants());
    setCabinClass(card.getCabinClass());
    setDirect(card.isDirect());
    setTripType(card.getTripType());
  }

  protected UISearchDropOffCard(Parcel in) {
    setId(in.readLong());
    setType(Card.CardType.valueOf(in.readString()));
    setPriority(in.readInt());
    setImage(in.readString());
    setBookNowUrl(in.readString());
    setEditTripUrl(in.readString());
    setDepartureCityIATA(in.readString());
    setArrivalCityIATA(in.readString());
    setDepartureDate(in.readLong());
    setArrivalDate(in.readLong());
    setNumAdults(in.readInt());
    setNumChildren(in.readInt());
    setNumInfants(in.readInt());
    setCabinClass(StoredSearch.CabinClass.valueOf(in.readString()));
    setDirect(in.readByte() != 0);
    setTripType(StoredSearch.TripType.valueOf(in.readString()));
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(getId());
    dest.writeString(getType().toString());
    dest.writeInt(getPriority());
    dest.writeString(getImage());
    dest.writeString(getBookNowUrl());
    dest.writeString(getEditTripUrl());
    dest.writeString(getDepartureCityIATA());
    dest.writeString(getArrivalCityIATA());
    dest.writeLong(getDepartureDate());
    dest.writeLong(getArrivalDate());
    dest.writeInt(getNumAdults());
    dest.writeInt(getNumChildren());
    dest.writeInt(getNumInfants());
    dest.writeString(getCabinClass().toString());
    dest.writeByte((byte) (isDirect() ? 1 : 0));
    dest.writeString(getTripType().toString());
  }
}
