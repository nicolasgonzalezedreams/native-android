package com.odigeo.data.entity.extensions;

import android.os.Parcel;
import android.os.Parcelable;
import com.odigeo.data.entity.carrousel.CarouselLocation;

/**
 * Created by matia on 1/29/2016.
 */
public class UICarrouselLocation extends CarouselLocation implements Parcelable {

  public static final Creator<UICarrouselLocation> CREATOR = new Creator<UICarrouselLocation>() {
    @Override public UICarrouselLocation createFromParcel(Parcel in) {
      return new UICarrouselLocation(in);
    }

    @Override public UICarrouselLocation[] newArray(int size) {
      return new UICarrouselLocation[size];
    }
  };

  public UICarrouselLocation(CarouselLocation location) {
    setIata(location.getIata());
    setDate(location.getDate());
    setTerminal(location.getTerminal());
    setDescription(location.getDescription());
  }

  private UICarrouselLocation(Parcel in) {
    setIata(in.readString());
    setDate(in.readLong());
    setTerminal(in.readString());
    setDescription(in.readString());
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(getIata());
    dest.writeLong(getDate());
    dest.writeString(getTerminal());
    dest.writeString(getDescription());
  }
}
