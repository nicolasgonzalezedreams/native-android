package com.odigeo.data.entity.extensions;

import android.os.Parcel;
import android.os.Parcelable;
import com.odigeo.data.entity.booking.LocationBooking;

public class UILocationBooking extends LocationBooking implements Parcelable {

  public static final Creator<UILocationBooking> CREATOR = new Creator<UILocationBooking>() {
    @Override public UILocationBooking createFromParcel(Parcel in) {
      return new UILocationBooking(in);
    }

    @Override public UILocationBooking[] newArray(int size) {
      return new UILocationBooking[size];
    }
  };

  public UILocationBooking() {

  }

  public UILocationBooking(LocationBooking locationBooking) {
    setCountryCode(locationBooking.getCountryCode());
    setCurrencyCode(locationBooking.getCurrencyCode());
    setCurrencyRate(locationBooking.getCurrencyRate());
    setCurrencyUpdatedDate(locationBooking.getCurrencyUpdatedDate());
    setTimezone(locationBooking.getTimezone());
    setLocationCode(locationBooking.getLocationCode());
  }

  private UILocationBooking(Parcel in) {
    setCountryCode(in.readString());
    setCurrencyCode(in.readString());
    setCurrencyRate(in.readLong());
    setCurrencyUpdatedDate(in.readLong());
    setTimezone(in.readString());
    setLocationCode(in.readString());
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(getCountryCode());
    dest.writeString(getCurrencyCode());
    dest.writeLong(getCurrencyRate());
    dest.writeLong(getCurrencyUpdatedDate());
    dest.writeString(getTimezone());
    dest.writeString(getLocationCode());
  }
}
