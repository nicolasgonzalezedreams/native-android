package com.odigeo.data.entity.extensions;

import android.support.annotation.Nullable;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.BaseSpinnerItem;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 26/09/15
 */
public class UITypeOfTraveller implements BaseSpinnerItem {
  private final UserTraveller.TypeOfTraveller typeOfTraveller;
  private final String oneCmsKey;

  /**
   * Constructor
   *
   * @param typeOfTraveller Traveller Type
   * @param oneCmsKey OneCMS key corresponding to the type.
   */
  public UITypeOfTraveller(UserTraveller.TypeOfTraveller typeOfTraveller, String oneCmsKey) {
    this.typeOfTraveller = typeOfTraveller;
    this.oneCmsKey = oneCmsKey;
  }

  /**
   * Retrieves the list of {@link UITypeOfTraveller} to display.
   *
   * @return A list filled with all the values possibles.
   */
  public static List<UITypeOfTraveller> getList() {
    List<UITypeOfTraveller> list = new ArrayList<>();
    list.add(new UITypeOfTraveller(UserTraveller.TypeOfTraveller.ADULT,
        OneCMSKeys.PASSENGER_TYPE_ADULT));
    list.add(new UITypeOfTraveller(UserTraveller.TypeOfTraveller.CHILD,
        OneCMSKeys.PASSENGER_TYPE_CHILD));
    list.add(new UITypeOfTraveller(UserTraveller.TypeOfTraveller.INFANT,
        OneCMSKeys.PASSENGER_TYPE_INFANT));
    return list;
  }

  @Override public String getShownTextKey() {
    return oneCmsKey;
  }

  @Override public int getImageId() {
    return EMPTY_RESOURCE;
  }

  @Nullable @Override public String getShownText() {
    return null;
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public UserTraveller.TypeOfTraveller getData() {
    return typeOfTraveller;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }
}
