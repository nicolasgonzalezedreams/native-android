package com.odigeo.data.entity.extensions;

import android.os.Parcel;
import android.os.Parcelable;
import com.odigeo.data.entity.carrousel.CancelledSectionCard;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.CarouselLocation;

/**
 * Created by matias.dirusso on 1/29/2016.
 */
public class UICancelledCard extends CancelledSectionCard implements Parcelable {

  public static final Creator<UICancelledCard> CREATOR = new Creator<UICancelledCard>() {
    @Override public UICancelledCard createFromParcel(Parcel in) {
      return new UICancelledCard(in);
    }

    @Override public UICancelledCard[] newArray(int size) {
      return new UICancelledCard[size];
    }
  };

  public UICancelledCard(CancelledSectionCard card) {
    setId(card.getId());
    setType(card.getType());
    setCarrier(card.getCarrier());
    setTripToHeader(card.getTripToHeader());
    setCarrierName(card.getCarrierName());
    setFlightId(card.getFlightId());
    setFrom(card.getFrom());
    setTo(card.getTo());
    setPriority(card.getPriority());
    setImage(card.getImage());
    setHasToShowRefreshButton(card.hasToShowRefreshButton());
  }

  public UICancelledCard(Parcel in) {
    setId(in.readLong());
    setType(Card.CardType.valueOf(in.readString()));
    setTripToHeader(in.readString());
    setCarrier(in.readString());
    setCarrierName(in.readString());
    setFlightId(in.readString());
    setFrom((CarouselLocation) in.readParcelable(UICarrouselLocation.class.getClassLoader()));
    setTo((CarouselLocation) in.readParcelable(UICarrouselLocation.class.getClassLoader()));
    setPriority(in.readInt());
    setImage(in.readString());
    setHasToShowRefreshButton(in.readByte() != 0);
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(getId());
    dest.writeString(getType().toString());
    dest.writeString(getTripToHeader());
    dest.writeString(getCarrier());
    dest.writeString(getCarrierName());
    dest.writeString(getFlightId());
    Parcelable from = new UICarrouselLocation(getFrom());
    dest.writeParcelable(from, flags);
    Parcelable to = new UICarrouselLocation(getTo());
    dest.writeParcelable(to, flags);
    dest.writeInt(getPriority());
    dest.writeString(getImage());
    dest.writeByte((byte) (hasToShowRefreshButton() ? 1 : 0));
  }
}
