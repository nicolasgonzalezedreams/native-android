package com.odigeo.data.entity.extensions;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.data.entity.BaseSpinnerItem;
import com.odigeo.data.entity.shoppingCart.CollectionMethod;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.InstallmentInformationDescription;
import java.math.BigDecimal;
import java.util.List;

public class ShoppingCartCollectionOptionSpinner implements BaseSpinnerItem {

  private static final String BANK_TRANSFER_CODE = "bank_transfer";

  private CollectionMethod method;
  private List<InstallmentInformationDescription> installments;
  private BigDecimal fee;
  private String bankTransferText;
  @NonNull private String text;
  @DrawableRes private int image;

  public CollectionMethod getMethod() {
    return method;
  }

  public void setMethod(CollectionMethod method) {
    this.method = method;
  }

  public List<InstallmentInformationDescription> getInstallments() {
    return installments;
  }

  public void setInstallments(List<InstallmentInformationDescription> installments) {
    this.installments = installments;
  }

  public BigDecimal getFee() {
    return fee;
  }

  public void setFee(BigDecimal fee) {
    this.fee = fee;
  }

  public String getBankTransferText() {
    return bankTransferText;
  }

  public void setBankTransferText(String bankTransferText) {
    this.bankTransferText = bankTransferText;
  }

  @Override public String getShownTextKey() {
    return EMPTY_STRING;
  }

  @Override public int getImageId() {
    return image;
  }

  @Nullable @Override public String getShownText() {
    return text;
  }

  @Nullable @Override public String getImageUrl() {
    return null;
  }

  @Nullable @Override public Object getData() {
    return null;
  }

  @Override public int getPlaceHolder() {
    return EMPTY_RESOURCE;
  }

  @Override public String toString() {
    return text;
  }

  public void generateResources(@NonNull Context context) {
    if (method == null) {
      text = EMPTY_STRING;
      image = EMPTY_RESOURCE;
    } else {
      text = getName(context);

      //Get the image resource.
      String code = getCode();
      String imageName = String.format("cc_%s", code).toLowerCase();
      image =
          ViewUtils.getResourceIdByName(context, imageName, "drawable", context.getPackageName());
      //If not found the resource, set the default.
      if (image == 0) {
        image = R.drawable.cc_cu;
      }
    }
  }

  public String getName(Context context) {
    if (method.getType() == CollectionMethodType.CREDITCARD) {
      return method.getCreditCardType().getName();
    } else {
      return method.getType().value();
    }
  }

  @Nullable public String getCode() {
    if (method.getType() == CollectionMethodType.CREDITCARD) {
      return method.getCreditCardType().getCode();
    } else if (method.getType() == CollectionMethodType.BANKTRANSFER) {
      return BANK_TRANSFER_CODE;
    } else {
      return null;
    }
  }
}
