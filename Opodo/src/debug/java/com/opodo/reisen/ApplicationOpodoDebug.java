package com.opodo.reisen;

import com.facebook.stetho.Stetho;

/**
 * Created by Javier Marsicano on 04/04/16.
 */
public class ApplicationOpodoDebug extends ApplicationOpodo {
  @Override public void onCreate() {
    super.onCreate();
    Stetho.initializeWithDefaults(this);
  }
}
