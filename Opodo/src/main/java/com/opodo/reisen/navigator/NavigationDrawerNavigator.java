package com.opodo.reisen.navigator;

import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.opodo.reisen.view.HomeView;
import com.squareup.otto.Subscribe;

/**
 * Created by ximena.perez on 18/11/2015.
 */
public class NavigationDrawerNavigator
    extends com.odigeo.app.android.navigator.NavigationDrawerNavigator {

  @Override public void showHome() {
    mHomeView = HomeView.newInstance();
    replaceFragment(com.odigeo.app.android.lib.R.id.flHome, mHomeView, TAG_HOME_VIEW);
  }

  @Subscribe public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getApplication());
  }

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getApplication());
  }
}