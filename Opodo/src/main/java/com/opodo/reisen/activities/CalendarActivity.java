package com.opodo.reisen.activities;

import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.navigator.CalendarNavigator;
import com.squareup.otto.Subscribe;

/**
 * Created by ManuelOrtiz on 28/08/2014.
 */
public class CalendarActivity extends CalendarNavigator {

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getApplication());
  }
}
