package com.opodo.reisen.activities;

import com.odigeo.app.android.lib.activities.OdigeoDestinationActivity;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.squareup.otto.Subscribe;

/**
 * Created by Irving Lóp on 01/09/2014.
 */
public class DestinationActivity extends OdigeoDestinationActivity {

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), this.getApplication()
        //screenTrackingEvent.getApplication()
    );
  }

  @Subscribe public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getApplication());
  }
}
