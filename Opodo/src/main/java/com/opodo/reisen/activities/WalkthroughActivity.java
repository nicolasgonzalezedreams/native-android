package com.opodo.reisen.activities;

import com.odigeo.app.android.lib.activities.OdigeoWalkthroughActivity;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.squareup.otto.Subscribe;

/**
 * Wrapper activity for walkthrough
 *
 * @author M. en C. Javier Silva Pérez
 * @version 1.0
 * @since 09/03/15
 */
public class WalkthroughActivity extends OdigeoWalkthroughActivity {

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), this.getApplication()
        //screenTrackingEvent.getApplication()
    );
  }

  @Subscribe public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getApplication());
  }
}
