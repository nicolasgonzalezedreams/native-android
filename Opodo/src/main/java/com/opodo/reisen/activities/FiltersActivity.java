package com.opodo.reisen.activities;

import com.odigeo.app.android.lib.activities.OdigeoFiltersActivity;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.squareup.otto.Subscribe;

/**
 * Created by Irving Lóp on 14/10/2014.
 */
public class FiltersActivity extends OdigeoFiltersActivity {

  @Subscribe public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getApplication());
  }

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getApplication());
  }
}
