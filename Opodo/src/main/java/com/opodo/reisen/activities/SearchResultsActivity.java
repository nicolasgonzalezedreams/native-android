package com.opodo.reisen.activities;

import android.support.v4.app.Fragment;
import com.odigeo.app.android.lib.activities.OdigeoSearchResultsActivity;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.fragments.OdigeoNoResultsRoundFragment;
import com.odigeo.app.android.lib.fragments.OdigeoNoResultsSimpleFragment;
import com.odigeo.app.android.lib.fragments.base.OdigeoNoResultsFragment;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.opodo.reisen.fragments.OpodoSearchWaitingResultsFragment;
import com.squareup.otto.Subscribe;

/**
 * Created by ManuelOrtiz on 09/09/2014.
 */
public class SearchResultsActivity extends OdigeoSearchResultsActivity {

  @Override protected void addWaitingFragment() {
    Fragment fragment = new OpodoSearchWaitingResultsFragment();
    fragment.setArguments(getIntent().getExtras());
    addFragment(fragment);
  }

  @Override public OdigeoNoResultsFragment getNoResultsSimpleFragment() {
    return OdigeoNoResultsSimpleFragment.newInstance(getSearchOptions());
  }

  @Override public OdigeoNoResultsFragment getNoResultsRoundFragment() {
    return OdigeoNoResultsRoundFragment.newInstance(getSearchOptions());
  }

  @Override public void onClickHistorySearch(SearchOptions searchOptions) {
    //TODO fill method
  }

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getApplication());
  }

  @Subscribe public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getApplication());
  }
}
