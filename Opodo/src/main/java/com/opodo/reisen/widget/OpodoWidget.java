package com.opodo.reisen.widget;
/**
 * Created by Ing. Jesús Fernando Sierra Pastrana on 05/03/15.
 */

import android.view.View;
import android.widget.RemoteViews;
import com.odigeo.app.android.lib.widget.OdigeoWidget;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.opodo.reisen.R;
import com.opodo.reisen.navigator.NavigationDrawerNavigator;
import java.util.List;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 05/03/15
 */
public class OpodoWidget extends OdigeoWidget {

  @Override public Class getHomeActivity() {
    return NavigationDrawerNavigator.class;
  }

  @Override public Class getWidgetServiceClass() {
    return OpodoWidgetService.class;
  }

  @Override protected void setBookingScales(Segment segment, RemoteViews remoteViews) {
    List<Section> sections = segment.getSectionsList();
    if (sections.size() > 1) {
      remoteViews.setViewVisibility(R.id.image_flight_icon_widget, View.INVISIBLE);
      StringBuilder text = new StringBuilder();
      for (int i = 1; i < sections.size(); i++) {
        text.append('\n');
        text.append(sections.get(i).getFrom().getCityIATACode());
      }
      remoteViews.setTextViewText(R.id.text_iata_codes_scales, text.toString());
    } else {
      remoteViews.setTextViewText(R.id.text_iata_codes_scales, "");
      remoteViews.setViewVisibility(R.id.image_flight_icon_widget, View.VISIBLE);
    }
  }
}
