package com.opodo.reisen.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.odigeo.app.android.lib.consts.HomeOptionsButtons;
import com.opodo.reisen.R;

/**
 * Created by Irving on 26/11/2014.
 */
public class OpodoMenuHomeButton extends LinearLayout {

  private LinearLayout imageView;
  private TextView textView;
  private HomeOptionsButtons optionsButton;

  public OpodoMenuHomeButton(Context context, HomeOptionsButtons optionsButton) {
    super(context);
    this.optionsButton = optionsButton;
    inflateLayout();
  }

  public OpodoMenuHomeButton(Context context, AttributeSet attrs) {
    super(context, attrs);
    inflateLayout();
  }

  private void inflateLayout() {
    inflate(getContext(), R.layout.layout_opodo_home_menu_button, this);
    imageView = (LinearLayout) findViewById(R.id.container_icon_menuHomeWidget);
    textView = (TextView) findViewById(R.id.title_menuHomeWidget);
  }

  public void setText(String text) {
    textView.setText(text);
  }

  public void setBackgroundClick(int idSelectorDrawable) {
    imageView.setBackgroundResource(idSelectorDrawable);
  }

  public HomeOptionsButtons getOptionsButton() {
    return optionsButton;
  }

  public void setOptionsButton(HomeOptionsButtons optionsButton) {
    this.optionsButton = optionsButton;
  }
}
