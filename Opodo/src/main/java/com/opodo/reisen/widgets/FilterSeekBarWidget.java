package com.opodo.reisen.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.ui.widgets.CustomSeekBar;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class FilterSeekBarWidget extends LinearLayout implements PropertyChangeListener {

  private final List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();
  private final AttributeSet attributes;
  private TextView titleTextView;
  private TextView subTitleTextView;
  private CustomSeekBar seekBar;
  private String textTitle;
  private String textCity;
  private int maxValueSeekBar;
  private int selectedValue;
  private int parentNumberWidget;
  private PropertyChangeListener propertyChangeListener;

  public FilterSeekBarWidget(Context context, AttributeSet attrs) {
    super(context, attrs);
    attributes = attrs;
  }

  protected void inflateLayout() {
    inflate(getContext(), R.layout.layout_filter_trip_duration_widget, this);
    titleTextView = (TextView) findViewById(R.id.text_filter_trip_duration_title);
    subTitleTextView = (TextView) findViewById(R.id.text_filter_trip_duration_subtittle);
    seekBar = (CustomSeekBar) findViewById(R.id.seekBar_trip_duration);

    //        seekBar.setPropertyChangeListener(this);
    seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        //TODO fill method
      }

      @Override public void onStartTrackingTouch(SeekBar seekBar) {
        //TODO fill method
      }

      @Override public void onStopTrackingTouch(SeekBar seekBar) {
        String empty = "";
        notifyListener(Constants.SEEKERBAR_DURATION, empty, String.valueOf(seekBar.getProgress()));
      }
    });
  }

  private void setAttributes() {
    TypedArray typedArray =
        getContext().obtainStyledAttributes(attributes, R.styleable.FilterSeekBarWidget);

    textTitle = typedArray.getString(R.styleable.FilterSeekBarWidget_textTitle);
    textCity = typedArray.getString(R.styleable.FilterSeekBarWidget_textSubtitle);
    maxValueSeekBar = typedArray.getInt(R.styleable.FilterSeekBarWidget_maxValue, 100);
    typedArray.recycle();

    setMaxValueSeekBar(maxValueSeekBar);
  }

  public void init() {
    inflateLayout();
    setAttributes();
    selectedValue = maxValueSeekBar;
    drawValues();
  }

  private void drawValues() {
    if (textTitle != null) {
      setTextTitle(textTitle);
    }
    if (textCity != null) {
      setTextSubTitle(textCity);
    }
    setSelectedValue(selectedValue);
  }

  public int getMaxValueSeekBar() {
    return maxValueSeekBar;
  }

  public void setMaxValueSeekBar(int value) {
    seekBar.setMax(value);
  }

  public int getSelectedValue() {
    return this.selectedValue;
  }

  public void setSelectedValue(int selectedValue) {
    seekBar.setProgress(selectedValue);
  }

  public void setTextTitle(String title) {
    titleTextView.setText(title);
  }

  public void setTextSubTitle(String subTitleValue) {
    subTitleTextView.setText(subTitleValue);
  }

  public void setTypeFaceTitle(Typeface type) {
    titleTextView.setTypeface(type);
  }

  public void setTypeFaceSubTitle(Typeface type) {
    subTitleTextView.setTypeface(type);
  }

  public int getParentNumberWidget() {
    return parentNumberWidget;
  }

  public void setParentNumberWidget(int parentNumberWidget) {
    this.parentNumberWidget = parentNumberWidget;
  }

  private void notifyListener(PropertyChangeEvent event) {
    this.propertyChangeListener.propertyChange(event);
  }

  private void notifyListener(String property, String oldValue, String newValue) {
    if (this.propertyChangeListener != null) {
      this.propertyChangeListener.propertyChange(
          new PropertyChangeEvent(this, property, oldValue, newValue));
    }
  }

  public void addChangeListener(PropertyChangeListener newListener) {
    listener.add(newListener);
  }

  public void setPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
    this.propertyChangeListener = propertyChangeListener;
  }

  @Override public void propertyChange(PropertyChangeEvent event) {
    notifyListener(event);
  }
}
