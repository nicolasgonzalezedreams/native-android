package com.opodo.reisen.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.volley.toolbox.ImageRequest;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.fragments.OdigeoSearchWaitingResultsFragment;
import com.odigeo.app.android.lib.utils.ViewUtils;
import com.odigeo.data.entity.TravelType;
import com.opodo.reisen.R;

/**
 * Fragment shown while flights search results are loading. This implements dynamic background
 * loading.
 *
 * @author ismaelmachado
 */
public class OpodoSearchWaitingResultsFragment extends OdigeoSearchWaitingResultsFragment {

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View root = super.onCreateView(inflater, container, savedInstanceState);
    SearchOptions searchOptions =
        (SearchOptions) getArguments().getSerializable(Constants.EXTRA_SEARCH_OPTIONS);
    if (searchOptions.getTravelType() != TravelType.MULTIDESTINATION) {
      OdigeoApp odigeoApp = (OdigeoApp) getActivity().getApplication();
      View view = root.findViewById(R.id.viewSearchingBackground);
      String iata = searchOptions.getFlightSegments().get(0).getArrivalCity().getIataCode();
      ImageRequest request = ViewUtils.filterRemoteImage(getContext(), view,
          ViewUtils.getSearchImage(getActivity(), iata), R.color.black_alpha_30, false);
      odigeoApp.addToRequestQueue(request);
    }
    return root;
  }
}
