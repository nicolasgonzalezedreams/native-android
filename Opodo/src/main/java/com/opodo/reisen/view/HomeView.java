package com.opodo.reisen.view;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageRequest;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import com.opodo.reisen.R;

public class HomeView extends com.odigeo.app.android.view.HomeView {

  protected RelativeLayout mTripBackground;
  private RelativeLayout mRlLastMinute;
  private RelativeLayout mRlPacks;
  private TextView mTvTitleLastMinute;
  private TextView mTvTitlePacks;
  private String mCurrentImageUrl;

  public static HomeView newInstance() {
    return new HomeView();
  }

  @Override protected void initComponent(View view) {
    super.initComponent(view);
    mRlLastMinute = (RelativeLayout) view.findViewById(R.id.rlLastMinute);
    mRlPacks = (RelativeLayout) view.findViewById(R.id.rlPacks);
    mTvTitleLastMinute = (TextView) view.findViewById(R.id.tvTitleLastMinute);
    mTvTitlePacks = (TextView) view.findViewById(R.id.tvTitlePacks);
    mTripBackground = (RelativeLayout) view.findViewById(R.id.rl_home_view);
  }

  @Override protected void initOneCMSText(View view) {
    mTvTitleFlight.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.FLIGHTS_TITLE));
    mTvTitleHotel.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.HOTELS_TITLE));
    mTvTitleCar.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.CARS_TITLE));
    mTvTitleLastMinute.setText(
        LocalizablesFacade.getString(getActivity(), OneCMSKeys.LAST_MINUTE_TITLE));
    mTvTitlePacks.setText(LocalizablesFacade.getString(getActivity(), OneCMSKeys.PACKS_TITLE));
  }

  @Override protected void setListeners() {
    super.setListeners();
    mRlLastMinute.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.openLastMinute();
      }
    });
    mRlPacks.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        mPresenter.openPacks();
      }
    });
  }

  @Override public void hideButtons() {
    super.hideButtons();

        /* TODO: We need to temp disable the LastMinut buttons for every market */
    //if (!mMarket.getKey().equals(Constants.GERMANY_MARKET)) {
    mRlLastMinute.setVisibility(View.GONE);
    mRlPacks.setVisibility(View.GONE);
    //}
  }

  private ImageRequest loadRemoteImage(final String url) {
    return new ImageRequest(url, new Response.Listener<Bitmap>() {

      @Override public void onResponse(Bitmap bitmap) {
        if (getActivity() != null && isAdded()) {
          Drawable drawableBackground = new BitmapDrawable(getResources(), bitmap);
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mTripBackground.setBackground(drawableBackground);
          } else {
            mTripBackground.setBackgroundDrawable(drawableBackground);
          }
        }
      }
    }, 0, 0, Bitmap.Config.ARGB_8888, null);
  }

  @Override public void setCampaignBackgroundImage() {
    setBackground(R.drawable.bgr);
  }

  @Override public void setCityBackgroundImage(String imageUrl) {
    if (mCurrentImageUrl == null || !mCurrentImageUrl.equals(imageUrl)) {
      ImageRequest request = loadRemoteImage(imageUrl);
      RequestHelper requestHelper = AndroidDependencyInjector.getInstance().provideRequestHelper();
      requestHelper.addImageRequest(request);
      mCurrentImageUrl = imageUrl;
    }
  }

  @Override public void setDefaultBackgroundImage() {
    setBackground(R.drawable.home_header_background);
  }

  private void setBackground(int imageId) {
    if (getActivity() != null && isAdded()) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
        mTripBackground.setBackground(getResources().getDrawable(imageId));
      } else {
        mTripBackground.setBackgroundDrawable(getResources().getDrawable(imageId));
      }
    }
  }

  @Override protected boolean shouldShowPromotionText() {
    return false;
  }
}
