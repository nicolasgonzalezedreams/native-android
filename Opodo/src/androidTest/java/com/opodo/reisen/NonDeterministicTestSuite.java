package com.opodo.reisen;

import com.opodo.reisen.tests.importTrip.ImportTripTest;
import com.opodo.reisen.tests.insurances.InsurancesTest;
import com.opodo.reisen.tests.passenger.PassengerTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    InsurancesTest.class, ImportTripTest.class, PassengerTest.class
}) public class NonDeterministicTestSuite {
}
