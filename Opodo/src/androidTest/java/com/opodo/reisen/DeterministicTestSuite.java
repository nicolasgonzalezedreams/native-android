package com.opodo.reisen;

import com.opodo.reisen.tests.calendar.CalendarHolidaysTest;
import com.opodo.reisen.tests.calendar.CalendarSelectionTest;
import com.opodo.reisen.tests.carousel.CarouselCTATest;
import com.opodo.reisen.tests.carousel.CarouselDropOffTest;
import com.opodo.reisen.tests.confirmation.ConfirmationTest;
import com.opodo.reisen.tests.logout.LogoutTest;
import com.opodo.reisen.tests.passenger.BaggageTest;
import com.opodo.reisen.tests.payment.PaymentFormTest;
import com.opodo.reisen.tests.payment.PaymentPurchaseTest;
import com.opodo.reisen.tests.payment.PromoCodeTest;
import com.opodo.reisen.tests.register.RegisterTest;
import com.opodo.reisen.tests.results.ResultsTest;
import com.opodo.reisen.tests.search.SearchTest;
import com.opodo.reisen.tests.settings.SettingsTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    CarouselCTATest.class, CarouselDropOffTest.class, ConfirmationTest.class, LogoutTest.class,
    BaggageTest.class, PaymentFormTest.class, PaymentPurchaseTest.class, PromoCodeTest.class,
    RegisterTest.class, ResultsTest.class, SearchTest.class, SettingsTest.class,
    CalendarHolidaysTest.class, CalendarSelectionTest.class
}) public class DeterministicTestSuite {
}
