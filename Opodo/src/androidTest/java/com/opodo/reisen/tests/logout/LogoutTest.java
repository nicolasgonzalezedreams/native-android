package com.opodo.reisen.tests.logout;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.logout.OdigeoLogoutTest;
import com.opodo.reisen.navigator.NavigationDrawerNavigator;
import com.pepino.annotations.FeatureOptions;

/**
 * Created by eleazarspak on 31/1/17.
 */

@FeatureOptions(feature = "logout.feature") public class LogoutTest extends OdigeoLogoutTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(NavigationDrawerNavigator.class);
  }
}
