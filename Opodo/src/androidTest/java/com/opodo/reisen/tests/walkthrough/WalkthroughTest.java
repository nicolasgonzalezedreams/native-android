package com.opodo.reisen.tests.walkthrough;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.walkthrough.OdigeoWalkthroughTest;
import com.opodo.reisen.activities.WalkthroughActivity;
import com.pepino.annotations.FeatureOptions;

/**
 * Created by daniel.morales on 3/2/17.
 */
@FeatureOptions(feature = "walkthrough.feature") public class WalkthroughTest
    extends OdigeoWalkthroughTest {

  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(WalkthroughActivity.class, false, false);
  }
}
