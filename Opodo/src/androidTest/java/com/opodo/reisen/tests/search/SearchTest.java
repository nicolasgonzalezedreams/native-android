package com.opodo.reisen.tests.search;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.search.OdigeoSearchTest;
import com.opodo.reisen.activities.SearchActivity;
import com.pepino.annotations.FeatureOptions;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 22/12/16
 */
@FeatureOptions(feature = "search.feature") public class SearchTest extends OdigeoSearchTest {

  @Override public ActivityTestRule getActivityTestRule() {
    //lazy launch to allow set history, logged users, etc
    return new ActivityTestRule<>(SearchActivity.class, false, false);
  }
}
