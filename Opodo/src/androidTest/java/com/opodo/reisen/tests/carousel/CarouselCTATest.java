package com.opodo.reisen.tests.carousel;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.robot.mockserver.MockServerConfigurator;
import com.odigeo.test.tests.carousel.OdigeoCarouselCTATest;
import com.opodo.reisen.navigator.NavigationDrawerNavigator;
import com.pepino.annotations.FeatureOptions;

@FeatureOptions(feature = "carousel_cta.feature") public class CarouselCTATest
    extends OdigeoCarouselCTATest {

  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(NavigationDrawerNavigator.class, false, false);
  }

  @Override public @MockServerConfigurator.Brand int getBrand() {
    return MockServerConfigurator.OPODO;
  }
}
