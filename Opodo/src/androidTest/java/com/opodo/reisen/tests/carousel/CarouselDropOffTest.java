package com.opodo.reisen.tests.carousel;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.robot.mockserver.MockServerConfigurator;
import com.odigeo.test.tests.carousel.OdigeoCarouselDropOffTest;
import com.opodo.reisen.navigator.NavigationDrawerNavigator;
import com.pepino.annotations.FeatureOptions;

@FeatureOptions(feature = "carousel_dropoff.feature") public class CarouselDropOffTest
    extends OdigeoCarouselDropOffTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(NavigationDrawerNavigator.class, false, false);
  }

  @Override public int getBrand() {
    return MockServerConfigurator.OPODO;
  }
}
