package com.opodo.reisen.tests.payment;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.app.android.navigator.PaymentNavigator;
import com.odigeo.test.tests.payment.OdigeoPaymentPurchaseTest;
import com.pepino.annotations.FeatureOptions;

@FeatureOptions(feature = "payment_purchase.feature") public class PaymentPurchaseTest
    extends OdigeoPaymentPurchaseTest {

  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(PaymentNavigator.class, false, false);
  }
}
