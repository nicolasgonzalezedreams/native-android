package com.opodo.reisen.tests.calendar;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.search.OdigeoCalendarSelectionTest;
import com.opodo.reisen.activities.SearchActivity;
import com.pepino.annotations.FeatureOptions;

@FeatureOptions(feature = "calendar_selection.feature") public class CalendarSelectionTest
    extends OdigeoCalendarSelectionTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(SearchActivity.class, false, false);
  }
}
