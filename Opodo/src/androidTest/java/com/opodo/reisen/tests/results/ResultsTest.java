package com.opodo.reisen.tests.results;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.results.OdigeoResultsTest;
import com.opodo.reisen.activities.SearchResultsActivity;
import com.pepino.annotations.FeatureOptions;

/**
 * Created by Javier Marsicano on 08/03/17.
 */

@FeatureOptions(feature = "results.feature") public class ResultsTest extends OdigeoResultsTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(SearchResultsActivity.class, false, false);
  }
}
