package com.opodo.reisen.tests.calendar;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.search.OdigeoCalendarHolidaysTest;
import com.opodo.reisen.activities.SearchActivity;
import com.pepino.annotations.FeatureOptions;

/**
 * Created by daniel.morales on 22/12/16.
 */
@FeatureOptions(feature = "calendar_holidays.feature") public class CalendarHolidaysTest
    extends OdigeoCalendarHolidaysTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(SearchActivity.class);
  }
}
