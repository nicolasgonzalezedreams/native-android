package com.opodo.reisen;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ DeterministicTestSuite.class, NonDeterministicTestSuite.class })
public class AllTestSuite {
}
