Feature: Confirmation page
  Scenario: Payment succesfull
    Given user did entered valid payment details
    When user is in confirmation page
    Then transaction has been successfull

  Scenario: Payment pending
    Given user did entered not trustful details
    When user is in confirmation page
    Then transaction is in pending status

  Scenario: Payment rejected
    Given user did entered invalid payment data
    When user is in confirmation page
    Then transaction was rejected

  Scenario: Ground Transportation Widget is shown
    Given user did entered valid payment details
    When user is in confirmation page
    And transaction has been successfull
    Then ground transportation widget is shown