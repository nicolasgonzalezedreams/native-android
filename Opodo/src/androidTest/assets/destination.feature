Feature: As a user I want to use button "Use current location" with GPS enable and disable.

  Scenario: Select current location in city selection with GPS turned on
    Given GPS turned on
    And City selection page
    When User selects current location
    Then Current city results are shown

  Scenario: Select current location in city selection with GPS turned off
    Given GPS turned off
    And City selection page
    When User selects current location
    Then We reached no city results page