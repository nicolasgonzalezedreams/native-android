Feature: Select dates in one flow
  Scenario: Select departure and skip return, it turns into a One Way Trip
    Given user is in Search page searching for a round trip
    When select a departure date
    And skip return selection
    Then user should have selected One Way trip
    And the departure date should be the selected one

  Scenario: Select departure and return, return is correctly selected
    Given user is in Search page searching for a round trip
    When user select departure and return dates
    Then the dates should be the selected ones

  Scenario: Select departure, date is correctly selected
    Given user is in Search page searching for a one way trip
    When select a departure date submitting the selection
    Then the departure date should be the selected one

  Scenario: Select departure and new departure, date is reset to new selection
    Given user is in Search page searching for a one way trip
    And user has selected a departure date
    When select a new departure date
    Then the departure date should be the latest selected one

  Scenario: Select departure, return and new departure, departure date is reset to new selection and return is cleared
    Given user is in Search page searching for a round trip
    And user select departure and return dates
    When user switch to one way tab
    Then return date should be empty

  Scenario: If departure is first day of the month, the previous one does not appear
    Given it’s the 1st day of the month
    When user open the calendar
    Then previous month is not visible