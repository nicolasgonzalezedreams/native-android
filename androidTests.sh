#!/bin/bash

#Enable multidex and disable proguard
#sed -i.bu 's/multiDexEnabled false/multiDexEnabled true/g' OdigeoLib/build.gradle
#sed -i.bu 's/multiDexEnabled false/multiDexEnabled true/g' eDreams/build.gradle
#sed -i.bu 's/minifyEnabled true/minifyEnabled false/g' eDreams/build.gradle


#Start the emulator
#emulator -avd Nexus_5X_API_23 -wipe-data &
#EMULATOR_PID=$!

# Wait for Android to finish booting
#WAIT_CMD="adb wait-for-device shell getprop init.svc.bootanim"
#until $WAIT_CMD | grep -m 1 stopped; do
#  echo "Waiting..."
#  sleep 1
#done

# Unlock the Lock Screen
adb shell input keyevent 26
adb shell input keyevent 82

# Clear and capture logcat
#adb logcat -c
#adb logcat > build/logcat.log &
#LOGCAT_PID=$!

# Generate apk
#./gradlew assembleDebug

## Install apk
#adb install ./eDreams/build/outputs/apk/eDreams-debug.apk
#
## Grant permissions
#adb shell pm grant com.edreams.travel.dev android.permission.ACCESS_COARSE_LOCATION
#adb shell pm grant com.edreams.travel.dev android.permission.ACCESS_FINE_LOCATION
#adb shell pm grant com.edreams.travel.dev android.permission.CALL_PHONE
#adb shell pm grant com.edreams.travel.dev android.permission.GET_ACCOUNTS
#adb shell pm grant com.edreams.travel.dev android.permission.READ_CALENDAR
#adb shell pm grant com.edreams.travel.dev android.permission.WRITE_CALENDAR
#adb shell pm grant com.edreams.travel.dev android.permission.READ_PHONE_STATE
#adb shell pm grant com.edreams.travel.dev android.permission.WRITE_EXTERNAL_STORAGE
#
./gradlew connectedAndroidTest -i

# Stop the background processes
#kill $LOGCAT_PID
#kill $EMULATOR_PID
