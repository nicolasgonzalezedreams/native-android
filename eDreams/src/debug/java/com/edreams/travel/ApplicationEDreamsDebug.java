package com.edreams.travel;

import com.facebook.stetho.Stetho;

/**
 * Created by Javier Marsicano on 04/04/16.
 */
public class ApplicationEDreamsDebug extends ApplicationEDreams {
  @Override public void onCreate() {
    super.onCreate();
    Stetho.initializeWithDefaults(this);
  }
}
