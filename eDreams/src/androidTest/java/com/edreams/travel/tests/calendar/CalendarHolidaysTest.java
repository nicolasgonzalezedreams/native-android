package com.edreams.travel.tests.calendar;

import android.support.test.rule.ActivityTestRule;

import com.edreams.travel.activities.SearchActivity;
import com.odigeo.test.tests.search.OdigeoCalendarHolidaysTest;
import com.pepino.annotations.FeatureOptions;

import org.junit.Ignore;
@FeatureOptions(feature = "calendar_holidays.feature")
public class CalendarHolidaysTest extends OdigeoCalendarHolidaysTest {
    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(SearchActivity.class);
    }
}
