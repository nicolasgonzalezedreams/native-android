package com.edreams.travel.tests.summary;


import android.support.test.rule.ActivityTestRule;

import com.edreams.travel.activities.SummaryActivity;
import com.odigeo.test.tests.summary.OdigeoSummaryTest;
import com.pepino.annotations.FeatureOptions;

@FeatureOptions(feature = "summary.feature")
public class SummaryTest extends OdigeoSummaryTest {

    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(SummaryActivity.class, false, false);
    }
}
