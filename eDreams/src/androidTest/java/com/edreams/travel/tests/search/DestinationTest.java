package com.edreams.travel.tests.search;

import android.support.test.rule.ActivityTestRule;

import com.edreams.travel.activities.DestinationActivity;
import com.odigeo.test.tests.search.OdigeoDestinationTest;
import com.pepino.annotations.FeatureOptions;

import org.junit.Ignore;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 23/01/17
 */
@FeatureOptions(feature = "destination.feature")
public class DestinationTest extends OdigeoDestinationTest {

    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(DestinationActivity.class, true, false);
    }
}
