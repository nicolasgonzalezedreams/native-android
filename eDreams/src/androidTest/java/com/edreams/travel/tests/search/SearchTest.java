package com.edreams.travel.tests.search;

import android.support.test.rule.ActivityTestRule;

import com.edreams.travel.activities.SearchActivity;
import com.odigeo.test.tests.search.OdigeoSearchTest;
import com.pepino.annotations.FeatureOptions;

@FeatureOptions(feature = "search.feature")
public class SearchTest extends OdigeoSearchTest {

    @Override
    public ActivityTestRule getActivityTestRule() {
        //lazy launch to allow set history, logged users, etc
        return new ActivityTestRule<>(SearchActivity.class, false, false);
    }
}
