package com.edreams.travel.tests.login;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.app.android.navigator.LoginNavigator;
import com.odigeo.test.tests.login.OdigeoLoginTest;
import com.pepino.annotations.FeatureOptions;

@FeatureOptions(feature = "login.feature")
public class LoginTest extends OdigeoLoginTest {
  @Override
  public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(LoginNavigator.class, false, false);
  }
}
