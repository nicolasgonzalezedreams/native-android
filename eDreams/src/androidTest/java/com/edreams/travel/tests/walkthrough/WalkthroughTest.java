package com.edreams.travel.tests.walkthrough;

import android.support.test.rule.ActivityTestRule;

import com.edreams.travel.activities.WalkthroughActivity;
import com.odigeo.test.tests.walkthrough.OdigeoWalkthroughTest;
import com.pepino.annotations.FeatureOptions;

import org.junit.Ignore;

/**
 * Created by daniel.morales on 3/2/17.
 */
@Ignore
@FeatureOptions(feature = "walkthrough.feature")
public class WalkthroughTest extends OdigeoWalkthroughTest {

    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(WalkthroughActivity.class, false, false);
    }
}
