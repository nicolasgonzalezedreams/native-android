package com.edreams.travel.tests.logout;

import android.support.test.rule.ActivityTestRule;

import com.edreams.travel.navigator.NavigationDrawerNavigator;
import com.odigeo.test.tests.logout.OdigeoLogoutTest;
import com.pepino.annotations.FeatureOptions;

import org.junit.Ignore;

/**
 * Created by eleazarspak on 31/1/17.
 */
@Ignore
@FeatureOptions(feature = "logout.feature")
public class LogoutTest extends OdigeoLogoutTest {
    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(NavigationDrawerNavigator.class);
    }
}
