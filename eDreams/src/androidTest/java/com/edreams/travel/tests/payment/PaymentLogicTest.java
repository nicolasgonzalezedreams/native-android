package com.edreams.travel.tests.payment;

import android.support.test.rule.ActivityTestRule;

import com.odigeo.app.android.navigator.PaymentNavigator;
import com.odigeo.test.tests.payment.OdigeoPaymentLogicTest;
import com.pepino.annotations.FeatureOptions;

@FeatureOptions(feature = "payment_logic.feature")
public class PaymentLogicTest extends OdigeoPaymentLogicTest {
    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(PaymentNavigator.class, false, false);
    }
}
