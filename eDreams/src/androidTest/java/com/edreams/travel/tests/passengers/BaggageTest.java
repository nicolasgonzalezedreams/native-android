package com.edreams.travel.tests.passengers;

import android.support.test.rule.ActivityTestRule;

import com.odigeo.app.android.navigator.PassengerNavigator;
import com.odigeo.test.tests.passengers.OdigeoBaggageTest;
import com.pepino.annotations.FeatureOptions;

import org.junit.Ignore;

@FeatureOptions(feature = "baggage.feature")
public class BaggageTest extends OdigeoBaggageTest {
    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(PassengerNavigator.class, false, false);
    }
}
