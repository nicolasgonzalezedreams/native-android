package com.edreams.travel.tests.calendar;

import android.support.test.rule.ActivityTestRule;

import com.edreams.travel.activities.SearchActivity;
import com.odigeo.test.tests.search.OdigeoCalendarSelectionTest;
import com.pepino.annotations.FeatureOptions;

import org.junit.Ignore;

@FeatureOptions(feature = "calendar_selection.feature")
public class CalendarSelectionTest extends OdigeoCalendarSelectionTest {
    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(SearchActivity.class, false, false);
    }
}
