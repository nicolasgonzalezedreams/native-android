package com.edreams.travel.tests.payment;

import android.support.test.rule.ActivityTestRule;

import com.odigeo.app.android.navigator.PaymentNavigator;
import com.odigeo.test.tests.payment.OdigeoPromoCodeTest;
import com.pepino.annotations.FeatureOptions;

@FeatureOptions(feature = "promocode.feature")
public class PromoCodeTest extends OdigeoPromoCodeTest {
    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(PaymentNavigator.class, false, false);
    }
}