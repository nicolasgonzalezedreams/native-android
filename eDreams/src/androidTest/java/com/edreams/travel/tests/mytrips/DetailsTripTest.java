package com.edreams.travel.tests.mytrips;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.app.android.navigator.MyTripsNavigator;
import com.odigeo.test.tests.importTrip.OdigeoImportTripTest;
import com.pepino.annotations.FeatureOptions;

@FeatureOptions(feature = "detailtrips.feature")
public class DetailsTripTest extends OdigeoImportTripTest {
    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(MyTripsNavigator.class, false, false);
    }
}
