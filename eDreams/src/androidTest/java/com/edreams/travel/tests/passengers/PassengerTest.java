package com.edreams.travel.tests.passengers;

import android.support.test.rule.ActivityTestRule;

import com.odigeo.app.android.navigator.PassengerNavigator;
import com.odigeo.test.tests.passengers.OdigeoPassengerTest;
import com.pepino.annotations.FeatureOptions;

import org.junit.Ignore;

@FeatureOptions(feature = "passenger.feature")
public class PassengerTest extends OdigeoPassengerTest {
    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(PassengerNavigator.class, true, false);
    }
}
