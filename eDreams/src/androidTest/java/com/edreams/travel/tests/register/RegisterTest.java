package com.edreams.travel.tests.register;

import android.support.test.rule.ActivityTestRule;

import com.odigeo.app.android.navigator.RegisterNavigator;
import com.odigeo.test.tests.register.OdigeoRegisterTest;
import com.pepino.annotations.FeatureOptions;


@FeatureOptions(feature = "register.feature")
public class RegisterTest extends OdigeoRegisterTest {
    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(RegisterNavigator.class);
    }
}
