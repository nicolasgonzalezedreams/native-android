package com.edreams.travel.tests.confirmation;

import android.support.test.rule.ActivityTestRule;

import com.edreams.travel.activities.ConfirmationActivity;
import com.odigeo.test.tests.confirmation.OdigeoConfirmationTest;
import com.pepino.annotations.FeatureOptions;

@FeatureOptions(feature = "confirmation.feature") public class ConfirmationTest
    extends OdigeoConfirmationTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(ConfirmationActivity.class, false, false);
  }
}
