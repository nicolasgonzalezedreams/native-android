package com.edreams.travel.tests.travellers;

import android.support.test.rule.ActivityTestRule;

import com.odigeo.app.android.navigator.TravellersNavigator;
import com.odigeo.test.tests.travellers.OdigeoTravellersTest;
import com.pepino.annotations.FeatureOptions;

@FeatureOptions(feature = "travellers.feature")
public class TravellersTest extends OdigeoTravellersTest {

  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(TravellersNavigator.class, false, false);
  }
}
