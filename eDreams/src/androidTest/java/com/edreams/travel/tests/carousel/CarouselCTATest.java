package com.edreams.travel.tests.carousel;

import android.support.test.rule.ActivityTestRule;

import com.edreams.travel.navigator.NavigationDrawerNavigator;
import com.odigeo.test.robot.mockserver.MockServerConfigurator;
import com.odigeo.test.tests.carousel.OdigeoCarouselCTATest;
import com.pepino.annotations.FeatureOptions;

import org.junit.Ignore;

@FeatureOptions(feature = "carousel_cta.feature")
public class CarouselCTATest extends OdigeoCarouselCTATest {

    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(NavigationDrawerNavigator.class, false, false);
    }

    @Override
    public
    @MockServerConfigurator.Brand
    int getBrand() {
        return MockServerConfigurator.EDREAMS;
    }
}
