package com.edreams.travel.tests.mytrips;

import android.support.test.rule.ActivityTestRule;

import com.odigeo.app.android.navigator.MyTripsNavigator;
import com.odigeo.test.tests.mytrips.OdigeoMyTripsTest;
import com.pepino.annotations.FeatureOptions;

@FeatureOptions(feature = "mytrips.feature")
public class MyTripsTest extends OdigeoMyTripsTest {
    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(MyTripsNavigator.class, false, false);
    }
}
