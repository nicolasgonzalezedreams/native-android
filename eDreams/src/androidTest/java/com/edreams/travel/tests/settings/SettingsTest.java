package com.edreams.travel.tests.settings;

import android.support.test.rule.ActivityTestRule;

import com.edreams.travel.activities.SettingsActivity;
import com.odigeo.test.tests.settings.OdigeoSettingsTest;
import com.pepino.annotations.FeatureOptions;

import org.junit.Ignore;

@FeatureOptions(feature = "settings.feature")
public class SettingsTest extends OdigeoSettingsTest {
    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(SettingsActivity.class);
    }
}
