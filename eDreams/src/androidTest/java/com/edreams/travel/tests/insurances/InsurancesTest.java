package com.edreams.travel.tests.insurances;

import android.support.test.rule.ActivityTestRule;

import com.odigeo.app.android.navigator.InsurancesNavigator;
import com.odigeo.test.tests.insurances.OdigeoInsurancesTest;
import com.pepino.annotations.FeatureOptions;

import org.junit.Ignore;

/**
 * Created by carlos.cordero on 1/3/17.
 */
@FeatureOptions(feature = "insurance.feature")
public class InsurancesTest extends OdigeoInsurancesTest{
    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(InsurancesNavigator.class, false, false);
    }
}
