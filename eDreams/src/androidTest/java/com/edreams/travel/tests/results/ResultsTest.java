package com.edreams.travel.tests.results;

import android.support.test.rule.ActivityTestRule;

import com.edreams.travel.activities.SearchResultsActivity;
import com.odigeo.test.tests.results.OdigeoResultsTest;
import com.pepino.annotations.FeatureOptions;

import org.junit.Ignore;

/**
 * Created by Javier Marsicano on 07/03/17.
 */

@FeatureOptions(feature = "results.feature")
public class ResultsTest extends OdigeoResultsTest {
    @Override
    public ActivityTestRule getActivityTestRule() {
        return new ActivityTestRule<>(SearchResultsActivity.class, false, false);
    }
}
