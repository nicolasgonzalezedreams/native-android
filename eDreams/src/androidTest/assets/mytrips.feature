Feature: As a user I want to open cars webviews in an imported trip

  Scenario: As a user show one way past trips
    Given the page My Trips with one way past trip
    Then one way past trip is properly shown


  Scenario: As a user show rounded past trips
    Given the page My Trips with rounded past trip
    Then rounded past trip is properly shown

  Scenario: As a user show multitrip past trips
    Given the page My Trips with multitrip past trip
    Then multitrip past trip is properly shown





