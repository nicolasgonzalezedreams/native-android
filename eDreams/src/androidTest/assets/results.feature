Feature: As a user I want to perform a flight search, so that I can select a flight.

  Scenario Outline: Baggage included info
  Given user is in Results page
  When search results <IsIncluded> baggage included
  Then flight baggage information <IsDisplayed>

  Examples:
    | IsIncluded | IsDisplayed |
    | true       | true        |
    | false      | false       |

  Scenario: Continue button not displayed if there is no flight selected
    Given user is in Results page
    Then continue button is not displayed

  Scenario: Show the widening message in results
    Given user made a flight search with results that not include one of the destinations selected
    When user is in Results page
    Then widening message appears

  Scenario: FT message displayed in FT markets
    Given the results page on a FT market
    When user select FT message
    Then open layer is displayed regarding FT

  Scenario: Only available to continue if selected a flight
    Given user is in Results page
    When user select a flight
    And select continue button
    Then user is in Flight details page

  Scenario: Members should see membership prices
    Given A user that is member in a market that allow membership discounts
    When user is in results page
    Then membership price should be displayed
    And non membership price should be slashed
    And membership text should be displayed