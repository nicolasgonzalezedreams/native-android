Feature: Registration of a user

  Scenario Outline: Register with email
    Given Register page
    When user register with email <email> and password <password>
    Then user is registered

    Examples:
    | email          | password |
    | test@email.com | password1 |