Feature: As I user I want to login with email

  Scenario: Valid login with email
    Given user is in login page
    When user login with valid details
    Then user should be logged in

  Scenario: Not able to login with invalid email
    Given user is in login page
    When user enter invalid email
    Then email not registered error should be displayed

  Scenario: Not able to login with invalid password
    Given user is in login page
    When user enter invalid password
    Then invalid password error should be displayed