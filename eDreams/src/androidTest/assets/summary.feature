Feature: As a user I want to check summary page

  Scenario: C02 message is displayed
    Given User is in summary page
    Then CO2 message is displayed

  Scenario: C02 message is clickable

    Given User is in summary page
    Then User click in CO2 message

  Scenario: Reach Passengers Page from Flight Details Page pressing Continue button
    Given User is in summary page
    When User click in continue button
    Then User reach Passengers page