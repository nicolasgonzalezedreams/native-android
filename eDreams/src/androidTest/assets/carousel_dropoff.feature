Feature: Drop off cards are showed correctly

  Scenario: Search page is opened if CTA edit button is clicked
    Given user have one drop-off card
    When user select edit search
    Then search page is displayed
    And search details are filled with the card information

#  Scenario: Search is performed if CTA search button is clicked
#    Given user have one drop-off card
#    When user select re-search
#    Then user is in results page

#  Scenario: Card disapear if interact with it
#    Given user have one drop-off card
#    When user select edit search
#    And go back to Home page
#    Then card should not be displayed

  Scenario: Card disapear if search date < current date
    Given user have one drop-off card with a past date
    When user open the app
    Then card should not be displayed

