Feature: Check all the possible scenarios given by the MSL and check the status of the booking after pay

  Scenario Outline: Payment is pending given the following conditions
    Given user entered a valid payment method
    When MSL is going to return <bookingStatus> <collectionState>
    And user purchase
    Then user should have a pending booking

    Examples:
      | bookingStatus | collectionState    |
      | REQUEST       | NOT_COLLECTED      |
      | HOLD          |                    |
      | RETAINED      |                    |
      | UNKNOWN       |                    |

  Scenario Outline: Payment is rejected given the following conditioLns
    Given user entered a valid payment method
    When MSL is going to return <status>
    And user purchase
    Then user should have a rejected booking

    Examples:
      | status        |
      | BOOKING_ERROR |
      | BROKEN_FLOW   |
      | BOOKING_STOP  |
      | HOLD          |


  Scenario Outline: Payment is rejected given the following conditions even if status is BOOKING_CONFIRMED
    Given user entered a valid payment method
    When MSL is going to return <bookingStatus> <collectionState>
    And user purchase
    Then user should have a rejected booking

    Examples:
      | bookingStatus | collectionState |
      | REJECTED      |                 |
      | FINAL_RET     |                 |


  Scenario Outline: Payment is confirmed given the following conditions
    Given user entered a valid payment method
    When MSL is going to return <bookingStatus> <collectionState>
    And user purchase
    Then user should have a confirmed booking

    Examples:
      | bookingStatus | collectionState |
      | REQUEST       | COLLECTED       |
      | CONTRACT      |                 |


  Scenario Outline: Payment is retry given the following conditions
    Given user entered a valid payment method
    When MSL is going to return BOOKING_PAYMENT_RETRY with <collectionOptions>
    And user purchase
    Then user should see payment retry message

    Examples:
      | collectionOptions              |
      | ONLY_PAY_PAL                   |
      | BANK_TRANSFER_AND_CREDIT_CARDS |

  Scenario: Payment is repricing given the following conditions
    Given user entered a valid payment method
    When MSL is going to return BOOKING_REPRICING
    And user purchase
    Then user should see repricing message

  Scenario Outline: Payment opens external payment view given the following conditions
    Given user select the following <payment_method>
    When MSL is going to return USER_INTERACTION_NEEDED with <payment_method>
    And user purchase
    Then user should see External payment view

    Examples:
      | payment_method |
      | PAYPAL         |
      | TRUSTLY        |
      | KLARNA         |