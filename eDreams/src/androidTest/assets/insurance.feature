Feature: As a user in insurance page, I cannot continue without selecting an insurance option

 #TODO review why is failing
 # Scenario Outline: Can continue the booking flow selecting one insurance
 #   Given User is in Insurance page
 #   When User select <insurance_type>
 #   And User select the continue
 #   Then User reached Payment page
 #
 #   Examples:
 #
 #     | insurance_type             |
 #     | cancelation                |
 #     | cancelation and assistance |

  #Scenario: Can not continue the booking flow if no insurance option is selected
  #  Given User is in Insurance page
  #  When all insurance options are unselected
  #  Then continue button is not clickable

  Scenario: Show detail travel page from insurance page
    Given User is in Insurance page
    When user select Flight details
    Then user reached Flight details page

  Scenario: Can continue the booking flow by selecting the no insurance option
    Given User is in Insurance page
    When User select no insurance option
    And User select the continue
    Then User reached Payment page


