Feature: As a user I want to use promo code options

  Scenario Outline: Add promo code
    Given We are in the Payment page
    When User select the button add promo code button
    And Insert a <promocode>
    And Select the button validate button
    And Select the button price break down see details
    Then The promo code is included in the price break down

    Examples:
      | promocode |
      | VALIDCODE |

  Scenario Outline: Remove promo code
    Given We are in the Payment page
    When User select the button add promo code button
    And Insert a <promocode>
    And Select the button validate button
    And Select the button remove promo code button
    And Select the button confirm remove promo code
    Then The promo code field is empty

    Examples:
      | promocode |
      | VALIDCODE      |

  Scenario Outline: Add promo code Price Breakdown
    Given We are in the Payment page
    When User select the button add promo code button
    And Insert a <promocode>
    And Select the button validate button
    Then Total price <result>

    Examples:
      | promocode   | result        |
      | VALIDCODE   | Decreased     |
      | INVALIDCODE | Remains equal |
