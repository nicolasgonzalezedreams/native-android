Feature: Confirmation page

  Scenario: Payment succesfull
    Given user did entered valid payment details
    When user is in confirmation page
    Then transaction has been successfull

  Scenario: Payment pending
    Given user did entered not trustful details
    When user is in confirmation page
    Then transaction is in pending status

  Scenario: Payment rejected
    Given user did entered invalid payment data
    When user is in confirmation page
    Then transaction was rejected

  Scenario: Ground Transportation Widget is shown
    Given user did entered valid payment details
    When user is in confirmation page
    And transaction has been successfull
    Then ground transportation widget is shown

  Scenario Outline: Ground transportation widget only appears when ONECMS key have true value and confirmation page has status Confirmed.
    Given onecms key of ground transportation value is
    When user is on confirmation page with booking status <bookingStatus>
    Then ground transportation widget <should> appear

  Examples:
  | bookingStatus  | should |
  | CONTRACT       | true   |
  | PENDING        | true   |
  | REJECTED       | false  |