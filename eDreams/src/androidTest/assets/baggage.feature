Feature: As a user I want to change baggage amount in passengers page

  Scenario Outline: Select baggage
    Given user is in Passenger page
    When user select <numberOfBaggages> baggages
    Then user has <numberOfBaggages> baggages selected

    Examples:
      | numberOfBaggages |
      | 0                |
      | 1                |

  Scenario Outline: Deselect baggage
    Given user is in Passenger page
    When user select <numberOfBaggages> baggages
    And user unselect baggage
    Then user has no baggage selected
    Examples:
      | numberOfBaggages |
      | 1                |

  Scenario Outline: Deselect baggage with confirmation dialog
    Given user is in Passenger page
    When user select <numberOfBaggages> baggages
    And remove baggage nag is active
    And user unselect baggage
    And user confirm remove baggage
    Then user has no baggage selected
    Examples:
      | numberOfBaggages |
      | 1                |

  Scenario Outline: Deselect baggage with confirmation dialog
    Given user is in Passenger page
    When user select <numberOfBaggages> baggages
    And remove baggage nag is active
    And user unselect baggage
    And user cancel remove baggage
    Then user has <numberOfBaggages> baggages selected
    Examples:
      | numberOfBaggages |
      | 1                |

  Scenario: user applies same baggage selection for all passengers
    Given user is in Passenger page with two passengers
    When user adds baggages for the default passenger
    And user select apply baggage to all passengers
    Then user should see the same selection to the rest of passengers

  Scenario: user applies same baggage remove selection for all passengers
    Given user is in Passenger page with two passengers
    When user adds baggages for the default passenger
    And user select apply baggage to all passengers
    And user remove baggage from default passengers
    Then any passenger should have baggage

  Scenario: user apply baggage to all travellers and remove non default traveller baggage then turns the switch off
    Given user is in Passenger page with two passengers
    When user adds baggages for the default passenger
    And user select apply baggage to all passengers
    And user remove baggage from second passenger
    Then switch toggle off
    And default passenger maintain it's selection