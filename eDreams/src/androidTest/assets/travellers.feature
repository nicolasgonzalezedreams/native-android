Feature: Travelers page interactions

  Scenario Outline: Edit existing traveller
    Given travellers page with one saved traveller
    When user edit existing traveller with the following data: <name> name, <surname> surname, <email> email, <address> address, <city> city, <postcode> postcode, <phoneNumber> phone, <countryCode> country
    Then travaller info should be updated with <name> name and <surname> surname

  Examples:
  | name     | surname     | email         | address           | city     | postcode | phoneNumber | countryCode |
  | TestName | TestSurname | test@test.com | Calle Test 45 1 2 | TestCity | 12345    | 666333444   | ES          |

  Scenario: Delete existing traveller
    Given travellers page with one saved traveller
    When user deletes existing traveller
    Then travaller should be deleted

  Scenario Outline: Add new traveler
    Given the Page MyData passengers without travelers added
    When User add new traveler with the following data: <name> name, <surname> surname, <email> email, <address> address, <city> city, <postcode> postcode, <phoneNumber> phone, <countryCode> country
    Then new traveler has been added with <name> name and <surname> surname

    Examples:
      | name     | surname     | email         | address           | city     | postcode | phoneNumber | countryCode |
      | TestName | TestSurname | test@test.com | Calle Test 45 1 2 | TestCity | 12345    | 666333444   | ES          |

    Scenario: Select main traveller in myData
      Given travellers page with more than one saved travellers
      When user set non main traveller as main
      Then selected traveller is setted as main
