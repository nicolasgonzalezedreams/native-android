package com.edreams.travel.activities;

import com.odigeo.app.android.lib.activities.OdigeoNoConnectionActivity;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.squareup.otto.Subscribe;

/**
 * Created by Irving Lóp on 24/10/2014.
 */
public class NoConnectionActivity extends OdigeoNoConnectionActivity {

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), this.getApplication());
  }

  @Subscribe public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getApplication());
  }
}
