package com.edreams.travel.view;

import android.view.View;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.odigeo.app.android.view.custom.HomeBackground;
import com.squareup.otto.Subscribe;

public class HomeView extends com.odigeo.app.android.view.HomeView {

  protected HomeBackground mTripBackground;

  public static HomeView newInstance() {
    return new HomeView();
  }

  @Override public void onResume() {
    super.onResume();
  }

  @Override protected void initComponent(View view) {
    super.initComponent(view);
    mTripBackground = (HomeBackground) view.findViewById(R.id.ivBackgroundImage);
  }

  //TODO: Use the generic tracker
  @Subscribe public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getActivity().getApplication());
  }

  //TODO: Use the generic tracker
  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getActivity().getApplication());
  }

  @Override public void setCampaignBackgroundImage() {
    mTripBackground.setCampaignBackground();
  }

  @Override public void setCityBackgroundImage(String imageUrl) {
    mTripBackground.setCityImageBackground(imageUrl);
  }

  @Override public void setDefaultBackgroundImage() {
    mTripBackground.setDefaultImage();
  }

  @Override public void translateBackgroundPosition(float position) {
    mTripBackground.translatePosition(position);
  }

  @Override protected boolean shouldShowPromotionText() {
    return true;
  }
}