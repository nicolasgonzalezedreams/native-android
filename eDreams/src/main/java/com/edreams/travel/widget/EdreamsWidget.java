package com.edreams.travel.widget;

import android.content.Context;
import android.widget.RemoteViews;
import com.edreams.travel.R;
import com.edreams.travel.navigator.NavigationDrawerNavigator;
import com.odigeo.app.android.lib.widget.OdigeoWidget;
import com.odigeo.data.entity.booking.Segment;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 04/03/15
 */
public class EdreamsWidget extends OdigeoWidget {

  @Override public Class getHomeActivity() {
    return NavigationDrawerNavigator.class;
  }

  @Override public Class getWidgetServiceClass() {
    return EdreamsWidgetService.class;
  }

  @Override protected void adjustSmallBookingWidget(Context context, RemoteViews remoteViews) {
    super.adjustSmallBookingWidget(context, remoteViews);
    remoteViews.setImageViewResource(R.id.imgPlane, R.drawable.widget_plane_small);
  }

  @Override protected void adjustNormalBookingWidget(Context context, RemoteViews remoteViews) {
    super.adjustNormalBookingWidget(context, remoteViews);
    remoteViews.setImageViewResource(R.id.imgPlane, R.drawable.widget_plane);
  }

  @Override protected void setBookingScales(Segment segment, RemoteViews remoteViews) {
    //nothing
  }
}
