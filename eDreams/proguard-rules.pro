-repackageclasses ''

-dontwarn org.apache.http.**
-dontwarn android.net.http.AndroidHttpClient
-dontwarn com.android.volley.toolbox.**
-dontwarn com.octo.android.**
-dontwarn com.squareup.okhttp.**
-dontwarn com.threatmetrix.**
-dontwarn com.android.dx.**
-dontwarn android.support.v7.**
-dontwarn com.google.android.gms.**
-dontwarn au.com.bytecode.opencsv.**

-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }
-keep class android.support.design.** { *; }
-keep interface android.support.design.** { *; }
-keep public class com.google.android.gms.** { public *; }
-keep class com.localytics.** { *; }
-keep class com.facebook.** { *; }
-keep class org.jsoup.** { *; }
-keep class com.squareup.** { *; }
-keep class au.com.bytecode.opencsv.** {*;}


##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.odigeo.** { *; }
-keep class odigeo.dataodigeo.** { *; }
-keep class com.edreams.travel.** { *; }

##---------------End: proguard configuration for Gson  ----------
-dontobfuscate

##--------------- Tune ---------------
-keep public class com.tune.** { public *; }
-keep public class com.google.android.gms.ads.identifier.** { *; }
-keep public class com.google.android.gms.gcm.** { *; }
-keep public class com.google.android.gms.common.** { *; }

-keepclassmembers class ** {
    public void onEvent(**);
}
##--------------- Tune ---------------
