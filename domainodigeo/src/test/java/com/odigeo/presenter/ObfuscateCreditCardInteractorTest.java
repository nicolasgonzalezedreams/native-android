package com.odigeo.presenter;

import com.odigeo.interactors.ObfuscateCreditCardInteractor;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;

public class ObfuscateCreditCardInteractorTest {

  private ObfuscateCreditCardInteractor obfuscateCreditCardInteractor;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    obfuscateCreditCardInteractor = new ObfuscateCreditCardInteractor();
  }

  @Test public void shouldObfuscatedNormalCard() {
    String creditcardVisa = "4507970183871148";
    String creditCardObfuscated = obfuscateCreditCardInteractor.obfuscate(creditcardVisa);
    assertEquals(creditCardObfuscated, "************1148");
  }

  @Test public void shouldOfuscatedAmericanExpress() {
    String creditcardAmex = "379575582693366";
    String creditCardObfuscated = obfuscateCreditCardInteractor.obfuscate(creditcardAmex);
    assertEquals(creditCardObfuscated, "***********3366");
  }

  @Test public void shouldNotObfuscateEmptyString() {
    String creditcardVisa = "";
    String creditCardObfuscated = obfuscateCreditCardInteractor.obfuscate(creditcardVisa);
    assertEquals(creditCardObfuscated, "");
  }

  @Test public void shouldNotObfuscateAndCrashWithNullString() {
    String creditCardObfuscated = obfuscateCreditCardInteractor.obfuscate(null);
    assertEquals(creditCardObfuscated, null);
  }

  @Test public void shouldNotObfuscateFourDigits() {
    String creditCardObfuscated = obfuscateCreditCardInteractor.obfuscate("1234");
    assertEquals(creditCardObfuscated, "1234");
  }
}
