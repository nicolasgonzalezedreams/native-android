package com.odigeo.presenter;

import com.odigeo.configuration.ABAlias;
import com.odigeo.configuration.ABPartition;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.shoppingCart.BankTransferData;
import com.odigeo.data.entity.shoppingCart.BankTransferResponse;
import com.odigeo.data.entity.shoppingCart.BookingDetail;
import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.MarketingRevenueDataAdapter;
import com.odigeo.data.entity.shoppingCart.PromotionalCodeUsageResponse;
import com.odigeo.data.entity.shoppingCart.ResumeBooking;
import com.odigeo.data.entity.shoppingCart.ResumeDataRequest;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.shoppingCart.request.BookingRequest;
import com.odigeo.data.entity.shoppingCart.request.CreditCardCollectionDetailsParametersRequest;
import com.odigeo.data.entity.userData.InsertCreditCardRequest;
import com.odigeo.data.net.controllers.AddProductsNetControllerInterface;
import com.odigeo.data.net.listener.OnSaveImportedTripListener;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.helper.ABTestHelper;
import com.odigeo.interactors.ConfirmBookingInteractor;
import com.odigeo.interactors.MembershipInteractor;
import com.odigeo.interactors.MyTripsManualImportInteractor;
import com.odigeo.interactors.ResumeBookingInteractor;
import com.odigeo.interactors.TotalPriceCalculatorInteractor;
import com.odigeo.presenter.contracts.navigators.PaymentNavigatorInterface;
import com.odigeo.presenter.contracts.views.PaymentViewInterface;
import com.odigeo.presenter.listeners.OnConfirmBookingListener;
import com.odigeo.presenter.listeners.OnResumeBookingListener;
import com.odigeo.tools.CreditCardRequestToStoreCreditCardRequestMapper;
import java.math.BigDecimal;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.odigeo.test.mock.MocksProvider.providesPaymentMocks;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PaymentPresenterTest {

  private static final String PROMO_CODE = "Code";
  private static final String PAYMENT_METHOD = "AE";

  @Mock private PaymentViewInterface mView;
  @Mock private PaymentNavigatorInterface mNavigator;
  @Mock private ConfirmBookingInteractor mConfirmBookingInteractor;
  @Mock private MyTripsManualImportInteractor mMyTripsManualImportInteractor;
  @Mock private AddProductsNetControllerInterface mAddProductsNetControllerInterface;
  @Mock private ResumeBookingInteractor mResumeBookingInteractor;
  @Mock private PaymentFormWidgetPresenter mPaymentFormWidgetPresenter;
  @Mock private PriceBreakdownWidgetPresenter mPriceBreakdownWidgetPresenter;
  @Mock private CreditCardCollectionDetailsParametersRequest
      mCreditCardCollectionDetailsParametersRequest;
  @Mock private PromoCodeWidgetPresenter mPromoCodeWidgetPresenter;
  @Mock private PaymentPurchasePresenter mPaymentPurchasePresenter;
  @Mock private ResumeDataRequest mResumeDataRequest;
  @Mock private CreditCardRequestToStoreCreditCardRequestMapper
      creditCardRequestToStoreCreditCardRequestMapper;
  @Mock private CrashlyticsController crashlyticsController;
  @Mock private ABTestHelper abTestHelper;
  @Mock private MembershipInteractor membershipInteractor;
  @Mock private TotalPriceCalculatorInteractor totalPriceCalculatorInteractor;

  @Captor private ArgumentCaptor<OnConfirmBookingListener> mOnConfirmBookingListener;
  @Captor private ArgumentCaptor<OnSaveImportedTripListener> mOnSaveImportedTripListener;
  @Captor private ArgumentCaptor<OnResumeBookingListener> mOnResumeBookingListener;

  private PaymentPresenter mPaymentPresenter;
  private CreateShoppingCartResponse mCreateShoppingCartResponse;
  private boolean mIsFullTransparency;
  private double mLastTicketsPrice;
  private double mLastInsurancePrice;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    mCreateShoppingCartResponse = providesPaymentMocks().provideCreateShoppingCartResponse();
    mIsFullTransparency = true;
    mLastTicketsPrice = 123d;
    mLastInsurancePrice = 123d;

    mPaymentPresenter = new PaymentPresenter(mView, mNavigator, mConfirmBookingInteractor,
        mMyTripsManualImportInteractor, mResumeBookingInteractor,
        creditCardRequestToStoreCreditCardRequestMapper, abTestHelper, membershipInteractor,
        totalPriceCalculatorInteractor);

    mPaymentPresenter.setPresenterWidgets(mPaymentFormWidgetPresenter,
        mPriceBreakdownWidgetPresenter, mPromoCodeWidgetPresenter, mPaymentPurchasePresenter);
  }

  @Test public void shouldShowInsuranceRepricing() {
    mIsFullTransparency = false;
    mLastInsurancePrice = 333d;

    when(mPriceBreakdownWidgetPresenter.getPricingBreakdownItems()).thenReturn(
        providesPaymentMocks().provideListPricingBreakdownItems());
    when(mPromoCodeWidgetPresenter.getExistingCode()).thenReturn(PROMO_CODE);

    mPaymentPresenter.initializePresenter(mCreateShoppingCartResponse, mIsFullTransparency,
        mLastTicketsPrice, mLastInsurancePrice, BigDecimal.ZERO);

    verify(mView).showRepricingInsuranceMessage(anyDouble());
    verify(mView, never()).showRepricingTicketsMessage(anyDouble());
  }

  @Test public void shouldUpdatePriceBreakdownWithSelectedPaymentMethod() {
    when(mPriceBreakdownWidgetPresenter.getPricingBreakdownItems()).thenReturn(
        providesPaymentMocks().provideListPricingBreakdownItems());
    when(mPromoCodeWidgetPresenter.getExistingCode()).thenReturn(PROMO_CODE);

    mPaymentPresenter.initializePresenter(mCreateShoppingCartResponse, mIsFullTransparency,
        mLastTicketsPrice, mLastInsurancePrice, BigDecimal.ZERO);

    mPaymentPresenter.onPaymentMethodSelected(PAYMENT_METHOD, false);

    verify(mPriceBreakdownWidgetPresenter).setShoppingCartCollectionOption(
        any(ShoppingCartCollectionOption.class));
  }

  @Test public void shouldShowPromoCodeAlreadyAdded() {
    when(mPriceBreakdownWidgetPresenter.getPricingBreakdownItems()).thenReturn(
        providesPaymentMocks().provideListPricingBreakdownItems());
    when(mPromoCodeWidgetPresenter.getExistingCode()).thenReturn(PROMO_CODE);

    mPaymentPresenter.initializePresenter(mCreateShoppingCartResponse, mIsFullTransparency,
        mLastTicketsPrice, mLastInsurancePrice, BigDecimal.ZERO);

    verify(mPromoCodeWidgetPresenter).setPromotionalCode(any(PromotionalCodeUsageResponse.class));
  }

  @Test public void shouldConfirmBookingWithBankTransfer() {
    when(mPriceBreakdownWidgetPresenter.getPricingBreakdownItems()).thenReturn(
        providesPaymentMocks().provideListPricingBreakdownItems());
    when(mPromoCodeWidgetPresenter.getExistingCode()).thenReturn(PROMO_CODE);

    mPaymentPresenter.initializePresenter(mCreateShoppingCartResponse, mIsFullTransparency,
        mLastTicketsPrice, mLastInsurancePrice, BigDecimal.ZERO);
    mPaymentPresenter.onCollectionOptionSelected(CollectionMethodType.BANKTRANSFER);
    mPaymentPresenter.onContinueButtonClickWithBankTransfer();

    verify(mConfirmBookingInteractor).confirmBooking(any(BookingRequest.class),
        mOnConfirmBookingListener.capture());
    mOnConfirmBookingListener.getValue()
        .onSuccess(providesPaymentMocks().provideBookingResponseContract());

    verify(mMyTripsManualImportInteractor).importTrip(mOnSaveImportedTripListener.capture(),
        anyString(), anyString());
    mOnSaveImportedTripListener.getValue()
        .onResponse(providesPaymentMocks().provideBooking(), true);

    verify(mNavigator).navigateToBookingConfirmed(any(InsertCreditCardRequest.class), anyString(),
        any(CreateShoppingCartResponse.class), any(BookingDetail.class),
        anyListOf(MarketingRevenueDataAdapter.class), any(BankTransferResponse.class),
        anyListOf(BankTransferData.class), any(Booking.class),
        any(ShoppingCartCollectionOption.class));
  }

  @Test public void shouldConfirmBookingWithCreditCard() {
    when(mPriceBreakdownWidgetPresenter.getPricingBreakdownItems()).thenReturn(
        providesPaymentMocks().provideListPricingBreakdownItems());
    when(mPromoCodeWidgetPresenter.getExistingCode()).thenReturn(PROMO_CODE);
    when(mPaymentFormWidgetPresenter.createCreditCardRequest()).thenReturn(
        givenNotNullCreditCardRequest());

    mPaymentPresenter.initializePresenter(mCreateShoppingCartResponse, mIsFullTransparency,
        mLastTicketsPrice, mLastInsurancePrice, BigDecimal.ZERO);
    mPaymentPresenter.onCollectionOptionSelected(CollectionMethodType.CREDITCARD);
    mPaymentPresenter.onContinueButtonClickWithCreditCard();

    verify(mConfirmBookingInteractor).confirmBooking(any(BookingRequest.class),
        mOnConfirmBookingListener.capture());
    mOnConfirmBookingListener.getValue()
        .onSuccess(providesPaymentMocks().provideBookingResponseContract());

    verify(mMyTripsManualImportInteractor).importTrip(mOnSaveImportedTripListener.capture(),
        anyString(), anyString());
    mOnSaveImportedTripListener.getValue()
        .onResponse(providesPaymentMocks().provideBooking(), true);

    verify(mNavigator).navigateToBookingConfirmed(any(InsertCreditCardRequest.class), anyString(),
        any(CreateShoppingCartResponse.class), any(BookingDetail.class),
        anyListOf(MarketingRevenueDataAdapter.class), any(BankTransferResponse.class),
        anyListOf(BankTransferData.class), any(Booking.class),
        any(ShoppingCartCollectionOption.class));
  }

  @Test public void shouldShowPaymentRetryWithCreditCardWithRemoveView() {
    when(mPriceBreakdownWidgetPresenter.getPricingBreakdownItems()).thenReturn(
        providesPaymentMocks().provideListPricingBreakdownItems());
    when(mPromoCodeWidgetPresenter.getExistingCode()).thenReturn(PROMO_CODE);
    when(mPaymentFormWidgetPresenter.createCreditCardRequest()).thenReturn(
        givenNotNullCreditCardRequest());
    when(abTestHelper.getPartition(ABAlias.BBU_BBUSTERS534, 2)).thenReturn(ABPartition.ONE);
    when(abTestHelper.getPartition(ABAlias.BBU_BBUSTERS530, 2)).thenReturn(ABPartition.ONE);

    mPaymentPresenter.initializePresenter(mCreateShoppingCartResponse, mIsFullTransparency,
        mLastTicketsPrice, mLastInsurancePrice, BigDecimal.ZERO);
    mPaymentPresenter.onCollectionOptionSelected(CollectionMethodType.CREDITCARD);
    mPaymentPresenter.onContinueButtonClickWithCreditCard();

    verify(mConfirmBookingInteractor).confirmBooking(any(BookingRequest.class),
        mOnConfirmBookingListener.capture());
    mOnConfirmBookingListener.getValue()
        .onSuccess(providesPaymentMocks().provideBookingResponseRetry());
    verify(mView).updatePaymentCardDetailsWidget();
    verify(mView).showPaymentRetry();
  }

  @Test public void shouldShowPaymentRetryWithCreditCardWithRefreshView() {
    BookingResponse bookingResponseWithPaymentRetry =
        providesPaymentMocks().provideBookingResponseRetry();

    when(mPriceBreakdownWidgetPresenter.getPricingBreakdownItems()).thenReturn(
        providesPaymentMocks().provideListPricingBreakdownItems());
    when(mPromoCodeWidgetPresenter.getExistingCode()).thenReturn(PROMO_CODE);
    when(mPaymentFormWidgetPresenter.createCreditCardRequest()).thenReturn(
        givenNotNullCreditCardRequest());
    when(abTestHelper.getPartition(ABAlias.BBU_BBUSTERS534, 2)).thenReturn(ABPartition.ONE);
    when(abTestHelper.getPartition(ABAlias.BBU_BBUSTERS530, 2)).thenReturn(ABPartition.TWO);

    mPaymentPresenter.initializePresenter(mCreateShoppingCartResponse, mIsFullTransparency,
        mLastTicketsPrice, mLastInsurancePrice, BigDecimal.ZERO);
    mPaymentPresenter.onCollectionOptionSelected(CollectionMethodType.CREDITCARD);
    mPaymentPresenter.onContinueButtonClickWithCreditCard();

    verify(mConfirmBookingInteractor).confirmBooking(any(BookingRequest.class),
        mOnConfirmBookingListener.capture());
    mOnConfirmBookingListener.getValue().onSuccess(bookingResponseWithPaymentRetry);
    verify(mPaymentFormWidgetPresenter).onRefreshPaymentFormWidgetView(
        bookingResponseWithPaymentRetry.getCollectionOptions());
    verify(mView).showPaymentRetry();
  }

  @Test public void shouldShowBookingRejectedWithCreditCard() {
    when(mPriceBreakdownWidgetPresenter.getPricingBreakdownItems()).thenReturn(
        providesPaymentMocks().provideListPricingBreakdownItems());
    when(mPromoCodeWidgetPresenter.getExistingCode()).thenReturn(PROMO_CODE);
    when(mPaymentFormWidgetPresenter.createCreditCardRequest()).thenReturn(
        givenNotNullCreditCardRequest());

    mPaymentPresenter.initializePresenter(mCreateShoppingCartResponse, mIsFullTransparency,
        mLastTicketsPrice, mLastInsurancePrice, BigDecimal.ZERO);
    mPaymentPresenter.onCollectionOptionSelected(CollectionMethodType.CREDITCARD);
    mPaymentPresenter.onContinueButtonClickWithCreditCard();

    verify(mConfirmBookingInteractor).confirmBooking(any(BookingRequest.class),
        mOnConfirmBookingListener.capture());
    mOnConfirmBookingListener.getValue()
        .onSuccess(providesPaymentMocks().provideBookingResponseStop());
    verify(mNavigator).navigateToBookingRejected(any(BookingDetail.class));
  }

  @Test public void shouldNotConfirmBookingWithCreditCard() {
    when(mPriceBreakdownWidgetPresenter.getPricingBreakdownItems()).thenReturn(
        providesPaymentMocks().provideListPricingBreakdownItems());
    when(mPromoCodeWidgetPresenter.getExistingCode()).thenReturn(PROMO_CODE);
    when(mPaymentFormWidgetPresenter.createCreditCardRequest()).thenReturn(null);

    mPaymentPresenter.initializePresenter(mCreateShoppingCartResponse, mIsFullTransparency,
        mLastTicketsPrice, mLastInsurancePrice, BigDecimal.ZERO);
    mPaymentPresenter.onCollectionOptionSelected(CollectionMethodType.CREDITCARD);
    mPaymentPresenter.onContinueButtonClickWithCreditCard();

    verify(mConfirmBookingInteractor, never()).confirmBooking(any(BookingRequest.class),
        mOnConfirmBookingListener.capture());

    verify(mMyTripsManualImportInteractor, never()).importTrip(
        mOnSaveImportedTripListener.capture(), anyString(), anyString());

    verify(mNavigator, never()).navigateToBookingConfirmed(any(InsertCreditCardRequest.class),
        anyString(), any(CreateShoppingCartResponse.class), any(BookingDetail.class),
        anyListOf(MarketingRevenueDataAdapter.class), any(BankTransferResponse.class),
        anyListOf(BankTransferData.class), any(Booking.class),
        any(ShoppingCartCollectionOption.class));
  }

  @Test public void shouldResumeBookingWithSuccessfulPayPal() {
    //This test is waiting for the bookingFlow Session FIX
    //There can be a workaround but will need to change quite a few things
    //in the class and booking flow session is crashing sometimes.
  }

  @Test public void shouldResumeBookingWithCancelPayPal() {
    when(mPriceBreakdownWidgetPresenter.getPricingBreakdownItems()).thenReturn(
        providesPaymentMocks().provideListPricingBreakdownItems());
    when(mPromoCodeWidgetPresenter.getExistingCode()).thenReturn(PROMO_CODE);
    when(mPaymentFormWidgetPresenter.createCreditCardRequest()).thenReturn(
        givenNotNullCreditCardRequest());

    mPaymentPresenter.initializePresenter(mCreateShoppingCartResponse, mIsFullTransparency,
        mLastTicketsPrice, mLastInsurancePrice, BigDecimal.ZERO);
    mPaymentPresenter.onCollectionOptionSelected(CollectionMethodType.CREDITCARD);

    mPaymentPresenter.onExternalPaymentFinished(mResumeDataRequest, true);
    verify(mView).showLoadingResumeBooking();
    verify(mResumeBookingInteractor).resumeBooking(any(ResumeBooking.class),
        mOnResumeBookingListener.capture());
    mOnResumeBookingListener.getValue()
        .onSuccess(providesPaymentMocks().provideBookingResponseRetry());
    verify(mView).hideLoadingDialog();
    verify(mMyTripsManualImportInteractor, never()).importTrip(
        mOnSaveImportedTripListener.capture(), anyString(), anyString());
  }

  @Test public void shouldNotResumeBookingBecauseMSLError() {

    when(mPriceBreakdownWidgetPresenter.getPricingBreakdownItems()).thenReturn(
        providesPaymentMocks().provideListPricingBreakdownItems());
    when(mPromoCodeWidgetPresenter.getExistingCode()).thenReturn(PROMO_CODE);
    when(mPaymentFormWidgetPresenter.createCreditCardRequest()).thenReturn(
        givenNotNullCreditCardRequest());

    mPaymentPresenter.initializePresenter(mCreateShoppingCartResponse, mIsFullTransparency,
        mLastTicketsPrice, mLastInsurancePrice, BigDecimal.ZERO);
    mPaymentPresenter.onCollectionOptionSelected(CollectionMethodType.CREDITCARD);

    mPaymentPresenter.onExternalPaymentFinished(mResumeDataRequest, true);
    verify(mView).showLoadingResumeBooking();
    verify(mResumeBookingInteractor).resumeBooking(any(ResumeBooking.class),
        mOnResumeBookingListener.capture());
    mOnResumeBookingListener.getValue().onNoConnectionError();
    verify(mView).hideLoadingDialog();
    verify(mMyTripsManualImportInteractor, never()).importTrip(
        mOnSaveImportedTripListener.capture(), anyString(), anyString());
  }

  private CreditCardCollectionDetailsParametersRequest givenNotNullCreditCardRequest() {
    CreditCardCollectionDetailsParametersRequest creditCardRequest =
        new CreditCardCollectionDetailsParametersRequest();

    creditCardRequest.setCardExpirationMonth("12");
    creditCardRequest.setCardExpirationYear("25");
    creditCardRequest.setCardNumber("1234123412341243");
    creditCardRequest.setCardOwner("jimy");
    creditCardRequest.setCardTypeCode("VI");
    return creditCardRequest;
  }
}