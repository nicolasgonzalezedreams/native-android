package com.odigeo.presenter;

import com.odigeo.data.ab.RemoteConfigControllerInterface;
import com.odigeo.data.localizable.OneCMSAlarmsController;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.session.CredentialsInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.MyTripsInteractor;
import com.odigeo.interactors.UpdateAppVersionCacheInteractor;
import com.odigeo.interactors.VisitsInteractor;
import com.odigeo.presenter.contracts.navigators.SplashNavigatorInterface;
import com.odigeo.presenter.contracts.views.SplashViewInterface;
import com.odigeo.presenter.listeners.VisitInteractorListener;
import java.util.Calendar;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SplashPresenterTest {

  private static final String okLastAppOpeningDays = "1";
  private static final String wrongLastAppOpeningDays = "5";
  private static final String lastOpeningDaysThreshold = "3";

  @Mock private SplashViewInterface splashViewInterface;
  @Mock private SplashNavigatorInterface splashNavigatorInterface;
  @Mock private PreferencesControllerInterface preferencesControllerInterface;
  @Mock private SessionController sessionController;
  @Mock private CredentialsInterface credentialsInterface;
  @Mock private VisitsInteractor visitsInteractor;
  @Mock private TrackerControllerInterface trackerControllerInterface;
  @Mock private MyTripsInteractor myTripsInteractor;
  @Mock private RemoteConfigControllerInterface remoteConfigControllerInterface;
  @Mock private UpdateAppVersionCacheInteractor updateAppVersionCacheInteractor;
  @Mock private OneCMSAlarmsController oneCMSAlarmsController;
  @Captor private ArgumentCaptor<VisitInteractorListener> visitInteractorListener;

  private SplashPresenter splashPresenter;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);

    splashPresenter = new SplashPresenter(splashViewInterface, splashNavigatorInterface,
        preferencesControllerInterface, sessionController, visitsInteractor,
        trackerControllerInterface, myTripsInteractor, updateAppVersionCacheInteractor,
        remoteConfigControllerInterface, oneCMSAlarmsController);
  }

  @Test public void shouldOpenWalkthroughActivity() {
    when(remoteConfigControllerInterface.isWalkthroughActive()).thenReturn(true);
    when(updateAppVersionCacheInteractor.isLastVersion()).thenReturn(false);
    splashPresenter.goToNextActivity();
    verify(splashNavigatorInterface).goToWalkThroughActivity();
  }

  @Test public void shouldOpenHomeActivityWhenNewAppVersionIsTheSameAsOld() {
    when(updateAppVersionCacheInteractor.isLastVersion()).thenReturn(true);
    splashPresenter.goToNextActivity();
    verify(splashNavigatorInterface).goToHomeActivity();
  }

  @Test public void shouldOpenWalkthroughActivityWhenUserIsLoggedAndDifferentAppVersion() {
    when(remoteConfigControllerInterface.isWalkthroughActive()).thenReturn(true);
    when(updateAppVersionCacheInteractor.isLastVersion()).thenReturn(false);
    when(sessionController.getCredentials()).thenReturn(credentialsInterface);
    splashPresenter.goToNextActivity();
    verify(splashNavigatorInterface).goToWalkThroughActivity();
  }

  @Test
  public void shouldOpenHomeAcivityWhenUserIsLoggedAndDefaultWalkthroughAndDifferentVersionCode() {
    when(updateAppVersionCacheInteractor.isLastVersion()).thenReturn(false);
    when(sessionController.getCredentials()).thenReturn(credentialsInterface);
    splashPresenter.goToNextActivity();
    verify(splashNavigatorInterface).goToHomeActivity();
  }

  @Test
  public void shouldOpenWalkthroughActivityWhenUserIsNotLoggedAndDefaultAndDifferentVersion() {
    when(remoteConfigControllerInterface.isWalkthroughActive()).thenReturn(true);
    when(updateAppVersionCacheInteractor.isLastVersion()).thenReturn(false);
    splashPresenter.goToNextActivity();
    verify(splashNavigatorInterface).goToWalkThroughActivity();
  }

  @Test public void shouldShowDropOffCard() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DAY_OF_MONTH, -Integer.valueOf(okLastAppOpeningDays));

    when(preferencesControllerInterface.containsValue(
        PreferencesControllerInterface.LAST_APP_OPENING)).thenReturn(true);
    when(preferencesControllerInterface.getLongValue(
        PreferencesControllerInterface.LAST_APP_OPENING)).thenReturn(calendar.getTimeInMillis());

    splashPresenter.updateLastAppOpening(lastOpeningDaysThreshold);

    verify(preferencesControllerInterface).saveBooleanValue(
        PreferencesControllerInterface.SHOULD_ADD_DROPOFF_CARD, true);
  }

  @Test public void shouldNotShowDropOffCard() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DAY_OF_MONTH, -Integer.valueOf(wrongLastAppOpeningDays));

    when(preferencesControllerInterface.containsValue(
        PreferencesControllerInterface.LAST_APP_OPENING)).thenReturn(true);
    when(preferencesControllerInterface.getLongValue(
        PreferencesControllerInterface.LAST_APP_OPENING)).thenReturn(calendar.getTimeInMillis());

    splashPresenter.updateLastAppOpening(lastOpeningDaysThreshold);

    verify(preferencesControllerInterface).saveBooleanValue(
        PreferencesControllerInterface.SHOULD_ADD_DROPOFF_CARD, false);
  }

  @Test public void shouldTrackActiveBookingsOnLaunchEvent() {

    when(myTripsInteractor.hasActiveBookings()).thenReturn(true);

    splashPresenter.checkActiveBookingsOnLaunch();

    InOrder inOrder = inOrder(myTripsInteractor, trackerControllerInterface);
    inOrder.verify(myTripsInteractor).hasActiveBookings();
    inOrder.verify(trackerControllerInterface).trackActiveBookingsOnLaunch();
    inOrder.verifyNoMoreInteractions();
  }

  @Test public void shouldUpdateDimensionsWhenFetchVisits() {
    splashPresenter.initABManager();
    verify(visitsInteractor, times(1)).fetch(visitInteractorListener.capture());
    visitInteractorListener.getValue().onSuccess();

    verify(trackerControllerInterface, times(1)).updateDimensionForABTest();
  }
}
