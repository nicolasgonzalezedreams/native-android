package com.odigeo.presenter;

import com.odigeo.data.calendar.controllers.CalendarController;
import com.odigeo.data.entity.TravelType;
import com.odigeo.presenter.contracts.navigators.CalendarNavigatorInterface;
import com.odigeo.presenter.contracts.views.CalendarViewInterface;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class CalendarPresenterTest {

  @Mock private CalendarViewInterface view;
  @Mock private CalendarNavigatorInterface navigator;
  @Mock private CalendarController calendarController;

  private CalendarPresenter calendarPresenter;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    calendarPresenter = new CalendarPresenter(view, navigator);
  }

  @Test public void calendarPresenterTest_NullSelectedDates() {
    initializeCalendarForOneWay(null);

    calendarPresenter.showSelectedDates();
    ArgumentCaptor<List> captorSelectedDates = ArgumentCaptor.forClass(List.class);
    ArgumentCaptor<Integer> captorCalendarMode = ArgumentCaptor.forClass(Integer.class);
    verify(view).setCalendarBounds(any(Date.class), any(Date.class), captorSelectedDates.capture(),
        captorCalendarMode.capture(), eq(false));

    assertNotNull(captorSelectedDates.getValue());
    assertEquals(2, captorSelectedDates.getValue().size());
    assertNull(captorSelectedDates.getValue().get(0));
    assertNull(captorSelectedDates.getValue().get(1));
  }

  @Test public void calendarPresenterTest_OWSelectedDates() {
    initializeCalendarForOneWay(getOWSelectedDates());

    calendarPresenter.showSelectedDates();
    ArgumentCaptor<List> captorSelectedDates = ArgumentCaptor.forClass(List.class);
    ArgumentCaptor<Integer> captorCalendarMode = ArgumentCaptor.forClass(Integer.class);
    verify(view).setCalendarBounds(any(Date.class), any(Date.class), captorSelectedDates.capture(),
        captorCalendarMode.capture(), eq(false));

    verify(view, times(1)).hideRoundTripHeader();
    assertNotNull(captorSelectedDates.getValue());
    assertEquals(1, captorSelectedDates.getValue().size());
    assertNotNull(captorSelectedDates.getValue().get(0));
  }

  @Test public void calendarPresenterTest_RTSelectedDatesOnlyDeparture() {
    initializeCalendarForRoundTrip(getRTSelectedDates(true));

    calendarPresenter.showSelectedDates();

    ArgumentCaptor<List> captorSelectedDates = ArgumentCaptor.forClass(List.class);
    ArgumentCaptor<Integer> captorCalendarMode = ArgumentCaptor.forClass(Integer.class);
    verify(view).setCalendarBounds(any(Date.class), any(Date.class), captorSelectedDates.capture(),
        captorCalendarMode.capture(), eq(false));

    verify(view, times(1)).showRoundTripHeader();
    verify(view, times(1)).showSkipReturnButton();
    assertEquals(2, captorSelectedDates.getValue().size());
    assertNotNull(captorSelectedDates.getValue().get(0));
    assertNull(captorSelectedDates.getValue().get(1));
  }

  @Test public void calendarPresenterTest_RTSelectedDates() {
    initializeCalendarForRoundTrip(getRTSelectedDates(false));

    calendarPresenter.showSelectedDates();

    ArgumentCaptor<List> captorSelectedDates = ArgumentCaptor.forClass(List.class);
    ArgumentCaptor<Integer> captorCalendarMode = ArgumentCaptor.forClass(Integer.class);
    verify(view).setCalendarBounds(any(Date.class), any(Date.class), captorSelectedDates.capture(),
        captorCalendarMode.capture(), eq(false));

    verify(view, times(1)).showRoundTripHeader();
    assertEquals(2, captorSelectedDates.getValue().size());
    assertNotNull(captorSelectedDates.getValue().get(0));
    assertNotNull(captorSelectedDates.getValue().get(1));
  }

  @Test public void calendarPresenterTest_MDSelectedDatesSegment0() {
    initializeCalendarForMultiStop(2);

    calendarPresenter.showSelectedDates();

    ArgumentCaptor<List> captorSelectedDates = ArgumentCaptor.forClass(List.class);
    ArgumentCaptor<Integer> captorCalendarMode = ArgumentCaptor.forClass(Integer.class);
    verify(view).setCalendarBounds(any(Date.class), any(Date.class), captorSelectedDates.capture(),
        captorCalendarMode.capture(), eq(false));

    verify(view, times(1)).hideRoundTripHeader();
    assertNotNull(captorSelectedDates.getValue());
    assertEquals(3, captorSelectedDates.getValue().size());
    assertNotNull(captorSelectedDates.getValue().get(0));
    assertNotNull(captorSelectedDates.getValue().get(1));
    assertNotNull(captorSelectedDates.getValue().get(2));
  }

  @Test public void calendarPresenterTest_OWSelectDate() {
    initializeCalendarForOneWay(null);

    Date selectedDate = getOWSelectedDates().get(0);
    calendarPresenter.onDateSelected(selectedDate);

    ArgumentCaptor<List> captorSelectedDates = ArgumentCaptor.forClass(List.class);
    ArgumentCaptor<Integer> captorCalendarMode = ArgumentCaptor.forClass(Integer.class);
    verify(view).setCalendarBounds(any(Date.class), any(Date.class), captorSelectedDates.capture(),
        captorCalendarMode.capture(), eq(true));

    verify(view, times(1)).setConfirmationButtonDate(any(Date.class));
    verify(view, times(1)).showConfirmationButton();
    assertNotNull(captorSelectedDates.getValue());
    assertEquals(2, captorSelectedDates.getValue().size());
    assertNotNull(captorSelectedDates.getValue().get(0));
    assertTrue(areSameDay(selectedDate, (Date) captorSelectedDates.getValue().get(0)));
    assertNull(captorSelectedDates.getValue().get(1));
  }

  @Test public void calendarPresenterTest_RTSelectDepartureDate() {
    initializeCalendarForRoundTrip(null);

    Date selectedDate = getRTSelectedDates(false).get(0);
    calendarPresenter.onDateSelected(selectedDate);

    ArgumentCaptor<List> captorSelectedDates = ArgumentCaptor.forClass(List.class);
    ArgumentCaptor<Integer> captorCalendarMode = ArgumentCaptor.forClass(Integer.class);
    verify(view).setCalendarBounds(any(Date.class), any(Date.class), captorSelectedDates.capture(),
        captorCalendarMode.capture(), eq(true));

    verify(view, atLeastOnce()).setDepartureDateHeader(any(Date.class));
    verify(view, times(1)).showSkipReturnButton();
    verify(view, atLeastOnce()).resetReturnDateHeader();
    assertNotNull(captorSelectedDates.getValue());
    assertEquals(2, captorSelectedDates.getValue().size());
    assertNotNull(captorSelectedDates.getValue().get(0));
    assertTrue(areSameDay(selectedDate, (Date) captorSelectedDates.getValue().get(0)));
    assertNull(captorSelectedDates.getValue().get(1));
  }

  @Test public void calendarPresenterTest_RTSelectReturnDate() {
    initializeCalendarForRoundTrip(null);

    Date selectedDate = getRTSelectedDepartureDates();
    calendarPresenter.onDateSelected(selectedDate);

    selectedDate = getRTSelectedReturnDates();
    calendarPresenter.onDateSelected(selectedDate);

    ArgumentCaptor<Date> captorSelectedDepartureDates = ArgumentCaptor.forClass(Date.class);
    ArgumentCaptor<Date> captorSelectedReturnDates = ArgumentCaptor.forClass(Date.class);
    verify(view).setConfirmationButtonDates(captorSelectedDepartureDates.capture(),
        captorSelectedReturnDates.capture());

    assertNotNull(captorSelectedDepartureDates.getValue());
    assertNotNull(captorSelectedReturnDates.getValue());
    verify(view, atLeastOnce()).setReturnDateHeader(any(Date.class));
    verify(view, times(1)).setConfirmationButtonDates(any(Date.class), any(Date.class));
    verify(view, times(1)).showConfirmationButton();
  }

  @Test public void calendarPresenterTest_MDSelectDatesSegment1() {
    initializeCalendarForMultiStop(1);
    Date selectedDate = new Date();

    calendarPresenter.showSelectedDates();
    calendarPresenter.onDateSelected(selectedDate);

    ArgumentCaptor<List> captorSelectedDates = ArgumentCaptor.forClass(List.class);
    ArgumentCaptor<Integer> captorCalendarMode = ArgumentCaptor.forClass(Integer.class);
    verify(view).setCalendarBounds(any(Date.class), any(Date.class), captorSelectedDates.capture(),
        captorCalendarMode.capture(), eq(false));

    verify(view, times(1)).hideRoundTripHeader();
    assertNotNull(captorSelectedDates.getValue());
    assertEquals(3, captorSelectedDates.getValue().size());
    assertNotNull(captorSelectedDates.getValue().get(0));
    assertNotNull(captorSelectedDates.getValue().get(1));
    assertNotNull(captorSelectedDates.getValue().get(2));
    assertTrue(areSameDay(selectedDate, (Date) captorSelectedDates.getValue().get(1)));
  }

  private void initializeCalendarForOneWay(ArrayList<Date> selectedDates) {
    if (selectedDates == null) {
      selectedDates = new ArrayList<>();
      selectedDates.add(null);
      selectedDates.add(null);
    }

    calendarPresenter.initializePresenter(0, TravelType.SIMPLE, selectedDates);
  }

  private void initializeCalendarForRoundTrip(ArrayList<Date> selectedDates) {
    if (selectedDates == null) {
      selectedDates = new ArrayList<>();
      selectedDates.add(null);
      selectedDates.add(null);
    }
    calendarPresenter.initializePresenter(0, TravelType.ROUND, selectedDates);
  }

  private void initializeCalendarForMultiStop(int segmentNumber) {
    calendarPresenter.initializePresenter(segmentNumber, TravelType.MULTIDESTINATION,
        getMDSelectedDates());
  }

  private ArrayList<Date> getOWSelectedDates() {
    ArrayList<Date> selectedDates = new ArrayList<>();
    selectedDates.add(new Date());
    return selectedDates;
  }

  private ArrayList<Date> getRTSelectedDates(boolean onlyDeparture) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());
    ArrayList<Date> selectedDates = new ArrayList<>();
    selectedDates.add(calendar.getTime());

    if (onlyDeparture) {
      selectedDates.add(null);
    } else {
      calendar.add(Calendar.DAY_OF_MONTH, 5);
      selectedDates.add(calendar.getTime());
    }

    return selectedDates;
  }

  private Date getRTSelectedDepartureDates() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());
    return calendar.getTime();
  }

  private Date getRTSelectedReturnDates() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());
    calendar.add(Calendar.DAY_OF_MONTH, 5);
    return calendar.getTime();
  }

  private ArrayList<Date> getMDSelectedDates() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());
    ArrayList<Date> selectedDates = new ArrayList<>();
    selectedDates.add(calendar.getTime());
    calendar.add(Calendar.DAY_OF_MONTH, 5);
    selectedDates.add(calendar.getTime());
    calendar.add(Calendar.DAY_OF_MONTH, 5);
    selectedDates.add(calendar.getTime());
    return selectedDates;
  }

  private boolean areSameDay(Date first, Date second) {
    Calendar cal1 = Calendar.getInstance();
    Calendar cal2 = Calendar.getInstance();
    cal1.setTime(first);
    cal2.setTime(second);
    return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
        && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
  }
}
