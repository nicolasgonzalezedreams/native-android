package com.odigeo.presenter;

import com.odigeo.presenter.contracts.views.holder.CalendarHeader;
import java.util.Calendar;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class CalendarHeaderPresenterTest {

  @Mock private CalendarHeader view;

  private CalendarHeaderPresenter presenter;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    presenter = new CalendarHeaderPresenter(view);
  }

  @Test public void departureLabelShadedAndReturnLabelHighlightedWhenDepartureDateSelected() {
    Date departureDate = getDepartureDate();

    presenter.setDepartureDate(departureDate);

    verify(view, times(1)).shadeDepartureLabel();
    verify(view, times(1)).highlightReturnLabel();
  }

  @Test public void departureLabelAndReturnLabelNormalizedWhenReturnDateSelected() {
    Date departureDate = getDepartureDate();
    Date returnDate = getReturnDate();
    presenter.setDepartureDate(departureDate);

    presenter.setReturnDate(returnDate);

    verify(view, times(1)).normalizeDepartureLabel();
    verify(view, atLeastOnce()).normalizeReturnLabel();
  }

  @Test public void departureLabelHighlightedAndReturnLabelShadedWhenOpening() {
    Date departureDate = getDepartureDate();
    Date returnDate = getReturnDate();
    presenter.openingView();

    presenter.setDepartureDate(departureDate);
    presenter.setReturnDate(returnDate);

    verify(view, times(1)).highlightDepartureLabel();
    verify(view, times(1)).shadeReturnLabel();
  }

  private Date getDepartureDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());
    calendar.add(Calendar.DAY_OF_MONTH, 1);

    return calendar.getTime();
  }

  private Date getReturnDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());
    calendar.add(Calendar.DAY_OF_MONTH, 3);

    return calendar.getTime();
  }
}
