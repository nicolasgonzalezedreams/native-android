package com.odigeo.presenter;

import com.odigeo.data.entity.contract.CreditCardType;
import com.odigeo.data.entity.shoppingCart.CollectionMethod;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.shoppingCart.request.CreditCardCollectionDetailsParametersRequest;
import com.odigeo.data.entity.userData.CreditCard;
import com.odigeo.interactors.ObfuscateCreditCardInteractor;
import com.odigeo.presenter.contracts.views.SavedPaymentMethodFormViewInterface;
import com.odigeo.presenter.listeners.OnClickSavedPaymentMethodRowListener;
import com.odigeo.tools.DateHelperInterface;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SavedPaymentMethodFormPresenterTest {

  private static final int VISIBILITY_GONE = 8;
  private static final int VISIBILITY_ON = 9;
  private static final int RANDOM_MONTH = 6;
  private static final int RANDOM_YEAR_SMALLER_THAN_CARD_EXPIRY = 18;
  private static final int RANDON_FUTURE_YEAR = 23;
  private static final String RANDOM_CVV = "123";

  @Mock private SavedPaymentMethodFormViewInterface savedPaymentMethodFormView;
  @Mock private OnClickSavedPaymentMethodRowListener onClickSavedPaymentMethodRowListener;
  @Mock private ObfuscateCreditCardInteractor obfuscateCreditCardInteractor;
  @Mock private DateHelperInterface dateHelper;
  private CreditCard creditCard;
  private List<ShoppingCartCollectionOption> shoppingCartCollectionOptions;
  private SavedPaymentMethodFormPresenter savedPaymentMethodFormPresenter;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    givenShoppingCart();
    initCreditCard();
    savedPaymentMethodFormPresenter =
        new SavedPaymentMethodFormPresenter(savedPaymentMethodFormView,
            onClickSavedPaymentMethodRowListener, obfuscateCreditCardInteractor, dateHelper,
            creditCard, shoppingCartCollectionOptions);
  }

  @Test public void shouldBuilRowVisaAndAccepted() {

    savedPaymentMethodFormPresenter.buildRow();

    verify(obfuscateCreditCardInteractor).obfuscate(anyString());
    verify(savedPaymentMethodFormView).showRadioButtonText(anyString());
    verify(savedPaymentMethodFormView).showCreditCardImage(anyString());
    verify(savedPaymentMethodFormView).initCVVNormalValidator();
    verify(savedPaymentMethodFormView).hideCVVAMEXToolTip();
    verify(savedPaymentMethodFormView).hideCVVSeparatorToolTip();
  }

  @Test public void shouldBuildRowAMEXAndAccepted() {

    when(dateHelper.getCurrentMonth()).thenReturn(RANDOM_MONTH);
    when(dateHelper.getCurrentYearLastTwoCharacters()).thenReturn(RANDON_FUTURE_YEAR);

    SavedPaymentMethodFormPresenter savedPaymentMethodFormPresenter =
        givenSavedPaymentMethodFormPresenterWithAmex();

    savedPaymentMethodFormPresenter.buildRow();

    verify(obfuscateCreditCardInteractor).obfuscate(anyString());
    verify(savedPaymentMethodFormView).showRadioButtonText(anyString());
    verify(savedPaymentMethodFormView).showCreditCardImage(anyString());
    verify(savedPaymentMethodFormView).initCVVAmexValidator();
    verify(savedPaymentMethodFormView).hideCVVNormalToolTip();
    verify(savedPaymentMethodFormView).hideCVVSeparatorToolTip();
  }

  @Test public void shouldBuildRowVisaAndNotAccepted() {

    when(dateHelper.getCurrentMonth()).thenReturn(RANDOM_MONTH);
    when(dateHelper.getCurrentYearLastTwoCharacters()).thenReturn(
        RANDOM_YEAR_SMALLER_THAN_CARD_EXPIRY);

    SavedPaymentMethodFormPresenter savedPaymentMethodFormPresenter =
        givenSavedPaymentMethodFormPresenterWithNoCreditCardVisaInShoppingCart();

    savedPaymentMethodFormPresenter.buildRow();

    verify(obfuscateCreditCardInteractor).obfuscate(anyString());
    verify(savedPaymentMethodFormView).showRadioButtonText(anyString());
    verify(savedPaymentMethodFormView).showCreditCardImage(anyString());
    verify(savedPaymentMethodFormView).showCreditCardNotAccepted();
    verify(savedPaymentMethodFormView).initCVVNormalValidator();
    verify(savedPaymentMethodFormView).hideCVVAMEXToolTip();
    verify(savedPaymentMethodFormView).disableRadioButton();
    verify(savedPaymentMethodFormView).hideCVV();
    verify(savedPaymentMethodFormView).reduceCardImageOpacity();
  }

  @Test public void shouldBuildRowAmexAndNotAccepted() {
    when(dateHelper.getCurrentMonth()).thenReturn(RANDOM_MONTH);
    when(dateHelper.getCurrentYearLastTwoCharacters()).thenReturn(
        RANDOM_YEAR_SMALLER_THAN_CARD_EXPIRY);

    SavedPaymentMethodFormPresenter savedPaymentMethodFormPresenter =
        givenSavedPaymentMethodFormPresenterWithNoAmexInShoppingCart();

    savedPaymentMethodFormPresenter.buildRow();

    verify(obfuscateCreditCardInteractor).obfuscate(anyString());
    verify(savedPaymentMethodFormView).showRadioButtonText(anyString());
    verify(savedPaymentMethodFormView).showCreditCardImage(anyString());
    verify(savedPaymentMethodFormView).showCreditCardNotAccepted();
    verify(savedPaymentMethodFormView).initCVVAmexValidator();
    verify(savedPaymentMethodFormView).hideCVVNormalToolTip();
    verify(savedPaymentMethodFormView).disableRadioButton();
    verify(savedPaymentMethodFormView).hideCVV();
    verify(savedPaymentMethodFormView).reduceCardImageOpacity();
  }

  @Test public void shouldBuildRowVisaExpired() {
    when(dateHelper.getCurrentMonth()).thenReturn(RANDOM_MONTH);
    when(dateHelper.getCurrentYearLastTwoCharacters()).thenReturn(RANDON_FUTURE_YEAR);

    SavedPaymentMethodFormPresenter savedPaymentMethodFormPresenter =
        givenSavedPaymentMethodFormPresenterWithExpiredCard();

    savedPaymentMethodFormPresenter.buildRow();

    verify(obfuscateCreditCardInteractor).obfuscate(anyString());
    verify(savedPaymentMethodFormView).showRadioButtonText(anyString());
    verify(savedPaymentMethodFormView).showCreditCardImage(anyString());
    verify(savedPaymentMethodFormView).initCVVNormalValidator();
    verify(savedPaymentMethodFormView).hideCVVAMEXToolTip();
    verify(savedPaymentMethodFormView).showExpiryDateError();
    verify(savedPaymentMethodFormView).disableRadioButton();
    verify(savedPaymentMethodFormView).hideCVV();
    verify(savedPaymentMethodFormView).reduceCardImageOpacity();
  }

  @Test public void shouldBuildRowAmexExpired() {
    when(dateHelper.getCurrentMonth()).thenReturn(RANDOM_MONTH);
    when(dateHelper.getCurrentYearLastTwoCharacters()).thenReturn(RANDON_FUTURE_YEAR);

    SavedPaymentMethodFormPresenter savedPaymentMethodFormPresenter =
        givenSavedPaymentMethodFormPresenterWithAmexExpired();

    savedPaymentMethodFormPresenter.buildRow();

    verify(obfuscateCreditCardInteractor).obfuscate(anyString());
    verify(savedPaymentMethodFormView).showRadioButtonText(anyString());
    verify(savedPaymentMethodFormView).showCreditCardImage(anyString());
    verify(savedPaymentMethodFormView).initCVVAmexValidator();
    verify(savedPaymentMethodFormView).hideCVVNormalToolTip();
    verify(savedPaymentMethodFormView).showExpiryDateError();
    verify(savedPaymentMethodFormView).disableRadioButton();
    verify(savedPaymentMethodFormView).hideCVV();
    verify(savedPaymentMethodFormView).reduceCardImageOpacity();
  }

  @Test public void shouldShowCVVTooltipNormal() {
    savedPaymentMethodFormPresenter.buildRow();
    savedPaymentMethodFormPresenter.onClickCVVInfoTooltip(VISIBILITY_GONE);
    verify(savedPaymentMethodFormView).showCVVNormalToolTip();
  }

  @Test public void shouldHideCVVTooltip() {
    savedPaymentMethodFormPresenter.buildRow();
    savedPaymentMethodFormPresenter.onClickCVVInfoTooltip(VISIBILITY_ON);
    verify(savedPaymentMethodFormView).hideCVVToolTip();
  }

  @Test public void shouldShowCVVTooltipAMEX() {
    SavedPaymentMethodFormPresenter savedPaymentMethodFormPresenter =
        givenSavedPaymentMethodFormPresenterWithAmexExpired();

    savedPaymentMethodFormPresenter.buildRow();
    savedPaymentMethodFormPresenter.onClickCVVInfoTooltip(VISIBILITY_GONE);
    verify(savedPaymentMethodFormView).showCVVAMEXToolTip();
  }

  @Test public void shouldScrollToCVVFieldErro() {
    savedPaymentMethodFormPresenter.scrollToCVVFieldWithError();
    verify(savedPaymentMethodFormView).scrollToCVVWithError();
  }

  @Test public void shouldCreateCreditCardRequest() {
    when(savedPaymentMethodFormView.getCVV()).thenReturn(RANDOM_CVV);

    CreditCardCollectionDetailsParametersRequest creditCardRequest =
        savedPaymentMethodFormPresenter.createCreditCardRequest();

    assertEquals(creditCard.getCreditCardNumber(), creditCardRequest.getCardNumber());
    assertEquals(creditCard.getCreditCardTypeId(), creditCardRequest.getCardTypeCode());
    assertEquals(creditCard.getExpiryMonth(), creditCardRequest.getCardExpirationMonth());
    assertEquals(creditCard.getExpiryYear(), creditCardRequest.getCardExpirationYear());
    assertEquals(creditCard.getOwner(), creditCardRequest.getCardOwner());
    assertEquals(creditCardRequest.getCardSecurityNumber(), RANDOM_CVV);
  }

  @Test public void shouldCheckIfPaymentMethodIsSelected() {
    savedPaymentMethodFormPresenter.isSelectedThisSavedPaymentMethod();
    verify(savedPaymentMethodFormView).isRadioButtonChecked();
  }

  @Test public void shouldCheckIFCVVInfoISChecked() {
    savedPaymentMethodFormPresenter.isCVVInfoValid();
    verify(savedPaymentMethodFormView).isTilCVVInfoValid();
  }

  @Test public void shouldClickRadioButton() {
    Object randomObject = new Object();
    savedPaymentMethodFormPresenter.onClickRadioButton(true, randomObject);
    verify(savedPaymentMethodFormView).expandCVV();
    verify(onClickSavedPaymentMethodRowListener).OnClickSavedPaymentMethodRadioButton(randomObject,
        creditCard.getCreditCardTypeId());
  }

  @Test public void shouldInitDefaultButton() {
    savedPaymentMethodFormPresenter.initDefaultRadioButton();
    verify(savedPaymentMethodFormView).checkRadioButton();
    verify(savedPaymentMethodFormView).showCVV();
  }

  private void givenShoppingCart() {
    shoppingCartCollectionOptions = new ArrayList<>();
    ShoppingCartCollectionOption creditCardCollectionOption = new ShoppingCartCollectionOption();
    CollectionMethod creditCardCollectionMethod = new CollectionMethod();
    CreditCardType creditCardType = new CreditCardType();
    creditCardType.setCode("VI");
    creditCardType.setName("VISA CREDIT");
    creditCardCollectionMethod.setCreditCardType(creditCardType);
    creditCardCollectionMethod.setType(CollectionMethodType.CREDITCARD);
    creditCardCollectionOption.setMethod(creditCardCollectionMethod);
    shoppingCartCollectionOptions.add(creditCardCollectionOption);
  }

  private List<ShoppingCartCollectionOption> givenShoppingCartWithNoCreditCards() {
    List<ShoppingCartCollectionOption> shoppingCartCollectionOptions = new ArrayList<>();
    ShoppingCartCollectionOption paypalCollectionOption = new ShoppingCartCollectionOption();
    paypalCollectionOption.setFee(new BigDecimal(3.00));
    CollectionMethod paypalCollectionMethod = new CollectionMethod();
    paypalCollectionMethod.setType(CollectionMethodType.PAYPAL);
    paypalCollectionOption.setMethod(paypalCollectionMethod);
    shoppingCartCollectionOptions.add(paypalCollectionOption);
    return shoppingCartCollectionOptions;
  }

  private SavedPaymentMethodFormPresenter givenSavedPaymentMethodFormPresenterWithNoCreditCardVisaInShoppingCart() {
    CreditCard creditCard = givenCreditCard();

    return new SavedPaymentMethodFormPresenter(savedPaymentMethodFormView,
        onClickSavedPaymentMethodRowListener, obfuscateCreditCardInteractor, dateHelper, creditCard,
        givenShoppingCartWithNoCreditCards());
  }

  private SavedPaymentMethodFormPresenter givenSavedPaymentMethodFormPresenterWithNoAmexInShoppingCart() {
    CreditCard creditCard = givenAmericanExpress();

    return new SavedPaymentMethodFormPresenter(savedPaymentMethodFormView,
        onClickSavedPaymentMethodRowListener, obfuscateCreditCardInteractor, dateHelper, creditCard,
        givenShoppingCartWithNoCreditCards());
  }

  private SavedPaymentMethodFormPresenter givenSavedPaymentMethodFormPresenterWithExpiredCard() {
    return new SavedPaymentMethodFormPresenter(savedPaymentMethodFormView,
        onClickSavedPaymentMethodRowListener, obfuscateCreditCardInteractor, dateHelper,
        givenCreditCardExpired(), shoppingCartCollectionOptions);
  }

  private SavedPaymentMethodFormPresenter givenSavedPaymentMethodFormPresenterWithAmexExpired() {
    return new SavedPaymentMethodFormPresenter(savedPaymentMethodFormView,
        onClickSavedPaymentMethodRowListener, obfuscateCreditCardInteractor, dateHelper,
        givenAmericanExpressExpired(), shoppingCartCollectionOptions);
  }

  private SavedPaymentMethodFormPresenter givenSavedPaymentMethodFormPresenterWithAmex() {
    return new SavedPaymentMethodFormPresenter(savedPaymentMethodFormView,
        onClickSavedPaymentMethodRowListener, obfuscateCreditCardInteractor, dateHelper,
        givenAmericanExpress(), shoppingCartCollectionOptions);
  }

  private CreditCard givenCreditCard() {
    return new CreditCard("1234515123", "12", "22", Long.valueOf("1"), "jimy", 1234, 1234, 1234,
        true, "VI");
  }

  private CreditCard givenCreditCardExpired() {
    return new CreditCard("1234515123", "12", "16", Long.valueOf("1"), "jimy", 1234, 1234, 1234,
        true, "VI");
  }

  private CreditCard givenAmericanExpressExpired() {
    return new CreditCard("1234515123", "12", "16", Long.valueOf("1"), "jimy", 1234, 1234, 1234,
        true, "AX");
  }

  private CreditCard givenAmericanExpress() {
    return new CreditCard("1234515123", "12", "53", Long.valueOf("1"), "jimy", 1234, 1234, 1234,
        true, "AX");
  }

  private void initCreditCard() {
    creditCard =
        new CreditCard("1234515123", "12", "22", Long.valueOf("1"), "jimy", 1234, 1234, 1234, true,
            "VI");
  }
}
