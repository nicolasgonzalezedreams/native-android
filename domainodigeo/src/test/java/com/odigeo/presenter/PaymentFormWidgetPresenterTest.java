package com.odigeo.presenter;

import com.odigeo.constants.PaymentMethodsKeys;
import com.odigeo.data.entity.shoppingCart.CollectionMethod;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.shoppingCart.request.CreditCardCollectionDetailsParametersRequest;
import com.odigeo.data.entity.userData.CreditCard;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.session.CredentialsInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.OrderCreditCardsByLastUsageInteractor;
import com.odigeo.presenter.contracts.views.PaymentFormWidgetInterface;
import com.odigeo.presenter.listeners.PaymentWidgetFormListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PaymentFormWidgetPresenterTest {

  private static final int VIEW_GONE = 8;
  private static String RANDOM_CREDITCARD = "1234";
  private static String RANDOM_CARDOWNER = "pepito griyo";
  private static String RANDOM_CVV = "102";
  private static String RANDOM_METHOD_CODE_DETECTED = "vi";
  private static String AMEX_METHOD_CODE = "AX";
  private static String MASTERCARD_METHOD_CODE = "MD";
  private static String UNKNOWN_METHOD_CODE = "??";
  private static String VISAELECTRON_METHOD_CODE = "VE";
  private static String RANDOM_EXPIRATION_MONTH = "5";
  private static String RANDOM_EXPIRATION_YEAR = "17";
  private static String REAL_CREDITCARD = "4556831390912531";
  private static String OBFUSCATED_CREDITCARD = "************2531";

  @Mock private PaymentFormWidgetInterface mPaymentFormWidgetInterface;
  @Mock private BinCheckPresenter mBinCheckPresenter;
  @Mock private PaymentWidgetFormListener mPaymentWidgetFormListener;
  @Mock private OrderCreditCardsByLastUsageInteractor mOrderCreditCardByLastUsageInteractor;
  @Mock private SessionController mSessionControler;
  @Mock private SavedPaymentMethodFormPresenter savedPaymentMethodFormPresenter1;
  @Mock private SavedPaymentMethodFormPresenter savedPaymentMethodFormPresenter2;
  @Mock private TrackerControllerInterface trackerControllerInterface;

  private PaymentFormWidgetPresenter mPaymentFormWidgetPresenter;
  private List<ShoppingCartCollectionOption> mShoppingCartCollectionOptions;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    mPaymentFormWidgetPresenter =
        new PaymentFormWidgetPresenter(mPaymentFormWidgetInterface, mBinCheckPresenter,
            mSessionControler, mOrderCreditCardByLastUsageInteractor, mPaymentWidgetFormListener,
            trackerControllerInterface);
    mPaymentFormWidgetPresenter.addSavedPaymentMethodPresenter(savedPaymentMethodFormPresenter1);
    mPaymentFormWidgetPresenter.addSavedPaymentMethodPresenter(savedPaymentMethodFormPresenter2);
  }

  @Test public void shouldShowOnlyCreditCardFields() {
    givenOnlyCreditCardCollection();

    when(mSessionControler.getCreditCards()).thenReturn(null);

    mPaymentFormWidgetPresenter.configurePaymentFormView(mShoppingCartCollectionOptions);

    verify(mPaymentFormWidgetInterface, never()).showBankTransferRadioButton();
    verify(mPaymentFormWidgetInterface).hideCreditCardRadioButton();
  }

  @Test public void shouldShowCreditCardAndPayPalFields() {
    givenCreditCardAndPaypalCollection();

    mPaymentFormWidgetPresenter.configurePaymentFormView(mShoppingCartCollectionOptions);

    verify(mPaymentFormWidgetInterface).showPaypalRadioButton();
    verify(mPaymentFormWidgetInterface).setPaypalFee(anyDouble());
    verify(mPaymentFormWidgetInterface, never()).showBankTransferRadioButton();
    verify(mPaymentFormWidgetInterface, never()).hideCreditCardRadioButton();
  }

  @Test public void shouldShowCreditCardAndPaypalAndBankTransferFields() {
    givenRandomCollectionWithPaypal();

    mPaymentFormWidgetPresenter.configurePaymentFormView(mShoppingCartCollectionOptions);

    verify(mPaymentFormWidgetInterface).showPaypalRadioButton();
    verify(mPaymentFormWidgetInterface).setPaypalFee(anyDouble());
    verify(mPaymentFormWidgetInterface).showBankTransferRadioButton();
    verify(mPaymentFormWidgetInterface, never()).hideCreditCardRadioButton();
  }

  @Test public void shouldShowCreditCardAndBankTransferFields() {
    givenCreditCardAndBanktransferCollection();

    mPaymentFormWidgetPresenter.configurePaymentFormView(mShoppingCartCollectionOptions);

    verify(mPaymentFormWidgetInterface).showBankTransferRadioButton();
    verify(mPaymentFormWidgetInterface, never()).hideCreditCardRadioButton();
  }

  @Test public void shouldShowCreditCardAndTrustlyFields() {
    givenCreditCardAndTrustlyCollection();

    mPaymentFormWidgetPresenter.configurePaymentFormView(mShoppingCartCollectionOptions);

    verify(mPaymentFormWidgetInterface).showTrustlyRadioButton();
    verify(mPaymentFormWidgetInterface).setTrustlyFee(anyDouble());
    verify(mPaymentFormWidgetInterface, never()).hideCreditCardRadioButton();
  }

  @Test public void shouldShowCreditCardAndKlarnaFields() {
    givenCreditCardAndKlarnaCollection();

    mPaymentFormWidgetPresenter.configurePaymentFormView(mShoppingCartCollectionOptions);

    verify(mPaymentFormWidgetInterface).showKlarnaRadioButton();
    verify(mPaymentFormWidgetInterface).setKlarnaFee(anyDouble());
    verify(mPaymentFormWidgetInterface, never()).hideCreditCardRadioButton();
  }

  @Test public void shouldShowCreditCardAndKlarnaAndTrustlyFields() {
    givenCreditCardAndKlarnaAndTrustlyCollection();

    mPaymentFormWidgetPresenter.configurePaymentFormView(mShoppingCartCollectionOptions);

    verify(mPaymentFormWidgetInterface).showTrustlyRadioButton();
    verify(mPaymentFormWidgetInterface).setTrustlyFee(anyDouble());
    verify(mPaymentFormWidgetInterface).showKlarnaRadioButton();
    verify(mPaymentFormWidgetInterface).setKlarnaFee(anyDouble());
    verify(mPaymentFormWidgetInterface, never()).hideCreditCardRadioButton();
  }

  @Test public void shouldShowAllPaymentMethodsInOrderFields() {
    givenAllPaymentMethods();

    mPaymentFormWidgetPresenter.configurePaymentFormView(mShoppingCartCollectionOptions);
    InOrder inOrder = inOrder(mPaymentFormWidgetInterface);

    inOrder.verify(mPaymentFormWidgetInterface).showPaypalRadioButton();
    inOrder.verify(mPaymentFormWidgetInterface).setPaypalFee(anyDouble());
    inOrder.verify(mPaymentFormWidgetInterface).showBankTransferRadioButton();
    inOrder.verify(mPaymentFormWidgetInterface).showTrustlyRadioButton();
    inOrder.verify(mPaymentFormWidgetInterface).setTrustlyFee(anyDouble());
    inOrder.verify(mPaymentFormWidgetInterface).showKlarnaRadioButton();
    inOrder.verify(mPaymentFormWidgetInterface).setKlarnaFee(anyDouble());
    inOrder.verify(mPaymentFormWidgetInterface, never()).showTrustlySeparator();
    inOrder.verify(mPaymentFormWidgetInterface, never()).hideCreditCardRadioButton();
  }

  @Test public void shouldFilerCollectionOptionsWithCreditCard() {
    givenRandomCollectionWithNoPaypal();

    mPaymentFormWidgetPresenter.configurePaymentFormView(mShoppingCartCollectionOptions);
    mShoppingCartCollectionOptions = mPaymentFormWidgetPresenter.createListForCreditCardSpinner();

    assertEquals(mShoppingCartCollectionOptions.size(), 1);
    assertEquals(mShoppingCartCollectionOptions.get(0).getMethod().getType(),
        CollectionMethodType.CREDITCARD);
  }

  @Test public void shouldCheckBinCreditCardNumber() {
    givenRandomCollectionWithNoPaypal();

    mPaymentFormWidgetPresenter.checkBin(RANDOM_CREDITCARD);

    verify(mBinCheckPresenter).checkBin(RANDOM_CREDITCARD);
    }

  @Test public void shouldSetCreditCardWhenSelectedItemSpinner() {
    mPaymentFormWidgetPresenter.onCreditCardSelectedItemSpinner(RANDOM_METHOD_CODE_DETECTED);
    verify(mPaymentFormWidgetInterface).trackBinCheckNoDetected();
    verify(mPaymentFormWidgetInterface).setCreditCardCode(RANDOM_METHOD_CODE_DETECTED);
  }

  @Test public void shouldNotSetCreditCardWhenSelectedItemSpinner() {
    mPaymentFormWidgetPresenter.onCreditCardSelectedItemSpinner(RANDOM_METHOD_CODE_DETECTED);
    verify(mPaymentFormWidgetInterface).trackBinCheckNoDetected();
    verify(mPaymentFormWidgetInterface).setCreditCardCode(RANDOM_METHOD_CODE_DETECTED);
  }

  @Test public void shouldUpdateCVVValidatorToAMEXAndTrackBinCheck() {
    mPaymentFormWidgetPresenter.updateCVVValidator(AMEX_METHOD_CODE, true);
    verify(mPaymentFormWidgetInterface).updateCVVAmexValidator();
    verify(mPaymentFormWidgetInterface).trackBinCheckSuccess();
  }

  @Test public void shouldUpdateCVVValidatorToAMEXAndNotTrackBinCheck() {
    mPaymentFormWidgetPresenter.updateCVVValidator(AMEX_METHOD_CODE, false);
    verify(mPaymentFormWidgetInterface).updateCVVAmexValidator();
    verify(mPaymentFormWidgetInterface, never()).trackBinCheckSuccess();
  }

  @Test public void shouldUpdateCVVValidatorToNonAmexAndTrackBinCheck() {
    mPaymentFormWidgetPresenter.updateCVVValidator(RANDOM_METHOD_CODE_DETECTED, true);
    verify(mPaymentFormWidgetInterface).updateCVVNormalValidator();
    verify(mPaymentFormWidgetInterface).trackBinCheckSuccess();
  }

  @Test public void shouldUpdateCVVValidatorToNonAmexAndNotTrackBinCheck() {
    mPaymentFormWidgetPresenter.updateCVVValidator(RANDOM_METHOD_CODE_DETECTED, false);
    verify(mPaymentFormWidgetInterface).updateCVVNormalValidator();
    verify(mPaymentFormWidgetInterface, never()).trackBinCheckSuccess();
  }

  @Test public void shouldNotUpdateUnknownMethodCVV() {
    mPaymentFormWidgetPresenter.updateCVVValidator(UNKNOWN_METHOD_CODE, true);
    verify(mPaymentFormWidgetInterface, never()).updateCVVAmexValidator();
    verify(mPaymentFormWidgetInterface, never()).updateCVVNormalValidator();
  }

  @Test public void shouldShowCVVTooltipAMEX() {
    mPaymentFormWidgetPresenter.onClickCVVInfoTooltip(VIEW_GONE, AMEX_METHOD_CODE);
    verify(mPaymentFormWidgetInterface).showCVVTooltipAMEX();
    verify(mPaymentFormWidgetInterface).showCVVTooltip();
  }

  @Test public void shouldShowCVVTooltipVisaOrMastercardWithMastercard() {
    mPaymentFormWidgetPresenter.onClickCVVInfoTooltip(VIEW_GONE, MASTERCARD_METHOD_CODE);
    verify(mPaymentFormWidgetInterface).showCVVTooltipVisaOrMastercard();
    verify(mPaymentFormWidgetInterface).trackCVVTooltip(anyString());
    verify(mPaymentFormWidgetInterface).showCVVTooltip();
  }

  @Test public void shouldShowCVVTooltipVisaOrMastercardWithVisa() {
    mPaymentFormWidgetPresenter.onClickCVVInfoTooltip(VIEW_GONE, VISAELECTRON_METHOD_CODE);
    verify(mPaymentFormWidgetInterface).showCVVTooltipVisaOrMastercard();
    verify(mPaymentFormWidgetInterface).trackCVVTooltip(anyString());
    verify(mPaymentFormWidgetInterface).showCVVTooltip();
  }

  @Test public void shouldShowCVVTooltipAllTypesFirstTime() {
    mPaymentFormWidgetPresenter.onClickCVVInfoTooltip(VIEW_GONE, null);
    verify(mPaymentFormWidgetInterface).showCVVTooltipAllTypes();
    verify(mPaymentFormWidgetInterface).trackCVVTooltip(anyString());
    verify(mPaymentFormWidgetInterface).showCVVTooltip();
  }

  @Test public void shouldObfuscatedCreditCardNumber() {
    when(mPaymentFormWidgetInterface.getCreditCardNumber()).thenReturn(REAL_CREDITCARD);

    String creditCardObfuscated = mPaymentFormWidgetPresenter.getCardNumberObfuscated();

    assertEquals(creditCardObfuscated, OBFUSCATED_CREDITCARD);
  }

  @Test public void shouldValidateLocalAmexAsAmex() {
    final String localAmex = "AX_LOCAL";
    assertEquals(mPaymentFormWidgetPresenter.isAmericanExpress(localAmex), true);
  }

  @Test public void shouldValidateAmex() {
    Assert.assertEquals(mPaymentFormWidgetPresenter.isAmericanExpress(AMEX_METHOD_CODE), true);
  }

  @Test public void shouldValidateNullAsIncorrectAmex() {
    Assert.assertEquals(mPaymentFormWidgetPresenter.isAmericanExpress(null), false);
  }

  @Test public void shouldValidateViAsVisaOrMastercard() {
    assertEquals(mPaymentFormWidgetPresenter.isMasterCardOrVisa(PaymentMethodsKeys.VI), true);
  }

  @Test public void shouldValidateVdAsVisaOrMastercard() {
    assertEquals(mPaymentFormWidgetPresenter.isMasterCardOrVisa(PaymentMethodsKeys.VD), true);
  }

  @Test public void shouldValidateVeAsVisaOrMastercard() {
    assertEquals(mPaymentFormWidgetPresenter.isMasterCardOrVisa(PaymentMethodsKeys.VE), true);
  }

  @Test public void shouldValidateE1AsVisaOrMastercard() {
    assertEquals(mPaymentFormWidgetPresenter.isMasterCardOrVisa(PaymentMethodsKeys.E1), true);
  }

  @Test public void shouldValidateMpAsVisaOrMastercard() {
    assertEquals(mPaymentFormWidgetPresenter.isMasterCardOrVisa(PaymentMethodsKeys.MP), true);
  }

  @Test public void shouldValidateMdAsVisaOrMastercard() {
    assertEquals(mPaymentFormWidgetPresenter.isMasterCardOrVisa(PaymentMethodsKeys.MD), true);
  }

  @Test public void shouldValidateCaAsVisaOrMastercard() {
    assertEquals(mPaymentFormWidgetPresenter.isMasterCardOrVisa(PaymentMethodsKeys.CA), true);
  }

  @Test public void shouldValidateMaAsVisaOrMastercard() {
    assertEquals(mPaymentFormWidgetPresenter.isMasterCardOrVisa(PaymentMethodsKeys.MA), true);
  }

  @Test public void shouldValidateNullAsIncorrectVisaOrMastercard() {
    assertEquals(mPaymentFormWidgetPresenter.isMasterCardOrVisa(null), false);
  }

  @Test public void shouldCreatedCreditCardRequest() {
    when(mPaymentFormWidgetInterface.getCreditCardNumber()).thenReturn(REAL_CREDITCARD);
    when(mPaymentFormWidgetInterface.getCreditCardOwner()).thenReturn(RANDOM_CARDOWNER);
    when(mPaymentFormWidgetInterface.getCVV()).thenReturn(RANDOM_CVV);
    when(mPaymentFormWidgetInterface.getPaymentMethodCodeDetected()).thenReturn(
        RANDOM_METHOD_CODE_DETECTED);
    when(mPaymentFormWidgetInterface.getExpirationMonth()).thenReturn(RANDOM_EXPIRATION_MONTH);
    when(mPaymentFormWidgetInterface.getExpirationYear()).thenReturn(RANDOM_EXPIRATION_YEAR);

    CreditCardCollectionDetailsParametersRequest creditCardRequest =
        mPaymentFormWidgetPresenter.createCreditCardRequest();

    assertEquals(creditCardRequest.getCardNumber(), REAL_CREDITCARD.replaceAll("\\s", ""));
    assertEquals(creditCardRequest.getCardOwner(), RANDOM_CARDOWNER);
    assertEquals(creditCardRequest.getCardSecurityNumber(), RANDOM_CVV);
    assertEquals(creditCardRequest.getCardTypeCode(), RANDOM_METHOD_CODE_DETECTED);
    assertEquals(creditCardRequest.getCardExpirationMonth(), RANDOM_EXPIRATION_MONTH);
    assertEquals(creditCardRequest.getCardExpirationYear(), RANDOM_EXPIRATION_YEAR);
  }

  @Test public void shouldCheckDefaultSavedPaymentMethodRadioButtonWithExpiredCardAndNotAccepted() {
    when(savedPaymentMethodFormPresenter1.isCreditCardExpired()).thenReturn(true);
    when(savedPaymentMethodFormPresenter1.isCreditCardAccepted()).thenReturn(false);

    mPaymentFormWidgetPresenter.checkDefaultSavedPaymentMethodRadioButton();

    verify(mPaymentFormWidgetInterface, never()).hideCreditCardWidget();
    verify(mPaymentFormWidgetInterface, never()).savedPaymentMethodWasSelected();
    verify(mPaymentFormWidgetInterface).setCreditCardChecked();
  }

  @Test public void shouldCheckDefaultSavedPaymentMethodRadioButtonWithNotExpiredCardAndAccepted() {

    when(savedPaymentMethodFormPresenter1.isCreditCardExpired()).thenReturn(false);
    when(savedPaymentMethodFormPresenter1.isCreditCardAccepted()).thenReturn(true);

    mPaymentFormWidgetPresenter.checkDefaultSavedPaymentMethodRadioButton();

    verify(savedPaymentMethodFormPresenter1).initDefaultRadioButton();
    verify(mPaymentFormWidgetInterface).hideCreditCardWidget();
    verify(mPaymentFormWidgetInterface).savedPaymentMethodWasSelected();
    verify(mPaymentWidgetFormListener).onSavedPaymentMethodSelected(anyString());
  }

  @Test public void shouldCheckDefaultSavePaymentMethodRadioButtonWithNotExpiredAndNotAccepted() {
    when(savedPaymentMethodFormPresenter1.isCreditCardExpired()).thenReturn(false);
    when(savedPaymentMethodFormPresenter1.isCreditCardAccepted()).thenReturn(false);

    mPaymentFormWidgetPresenter.checkDefaultSavedPaymentMethodRadioButton();

    verify(mPaymentFormWidgetInterface, never()).hideCreditCardWidget();
    verify(mPaymentFormWidgetInterface, never()).savedPaymentMethodWasSelected();
    verify(mPaymentFormWidgetInterface).setCreditCardChecked();
  }

  @Test public void shouldCheckDefaultSavePaymentMethodRadioButtonWithExpiredAnAccepted() {
    when(savedPaymentMethodFormPresenter1.isCreditCardExpired()).thenReturn(true);
    when(savedPaymentMethodFormPresenter1.isCreditCardAccepted()).thenReturn(true);

    mPaymentFormWidgetPresenter.checkDefaultSavedPaymentMethodRadioButton();

    verify(mPaymentFormWidgetInterface, never()).hideCreditCardWidget();
    verify(mPaymentFormWidgetInterface, never()).savedPaymentMethodWasSelected();
    verify(mPaymentFormWidgetInterface).setCreditCardChecked();
  }

  @Test public void shouldTrackUserLoggedWithPaymentMethods() {
    when(mSessionControler.getCreditCards()).thenReturn(null);
    when(mSessionControler.getCredentials()).thenReturn(getCredentials());
    mPaymentFormWidgetPresenter.trackLoggedUserWithPaymentMethods();
    verify(trackerControllerInterface).trackerUserLoggedWithoutPaymentMethods();
  }

  @Test public void shouldTrackUserLoggedWithoutPaymentMethods() {
    List<CreditCard> savedCreditCards = new ArrayList<>();
    CreditCard creditCard =
        new CreditCard("12345", "12", "13", 1L, "pepe", 1234, 12312, 12312, false, "AX");
    savedCreditCards.add(creditCard);
    when(mSessionControler.getCreditCards()).thenReturn(savedCreditCards);
    when(mSessionControler.getCredentials()).thenReturn(getCredentials());
    mPaymentFormWidgetPresenter.trackLoggedUserWithPaymentMethods();
    verify(trackerControllerInterface).trackUserLoggedWithPaymentMethods();
  }

  @Test public void shouldTrackPaymentMethodNotUsed() {
    when(mSessionControler.getCreditCards()).thenReturn(null);
    mPaymentFormWidgetPresenter.checkIfHaveToTrackStoreMethodNotUsed();
    verify(trackerControllerInterface, never()).trackStorePaymentMethodNotUsed();
  }

  @Test public void shouldRefreshPaymentFormWidgetWithCreditCardPreSelectedTest() {
    InOrder inOrder =
        inOrder(mPaymentFormWidgetInterface, mBinCheckPresenter, savedPaymentMethodFormPresenter1,
            savedPaymentMethodFormPresenter2, mPaymentWidgetFormListener);
    givenRandomCollectionWithPaypal();

    mPaymentFormWidgetPresenter.onRefreshPaymentFormWidgetView(mShoppingCartCollectionOptions);

    inOrder.verify(mPaymentFormWidgetInterface).clearCreditCardFields();
    inOrder.verify(mPaymentFormWidgetInterface).initCreditCardSpinner();
    inOrder.verify(mBinCheckPresenter).initPresenter(mShoppingCartCollectionOptions);
    inOrder.verify(mPaymentFormWidgetInterface).hideBankTransfer();
    inOrder.verify(mPaymentFormWidgetInterface).hideTrustlyWidget();
    inOrder.verify(mPaymentFormWidgetInterface).hidePaypalWidget();
    inOrder.verify(mPaymentFormWidgetInterface).hideKlarnaWidget();
    inOrder.verify(savedPaymentMethodFormPresenter1).refreshRow(mShoppingCartCollectionOptions);
    inOrder.verify(savedPaymentMethodFormPresenter2).refreshRow(mShoppingCartCollectionOptions);
    inOrder.verify(mPaymentFormWidgetInterface).setPaymentMethodsUnchecked();
    inOrder.verify(mPaymentFormWidgetInterface).performClickOnCreditCardRadioButton();
    inOrder.verify(mPaymentWidgetFormListener).onPaymentWidgetValidation(false);
  }

  @Test public void shouldRefreshPaymentFormWidgetWithSavePaymentMethodPresSelectedTest() {
    InOrder inOrder =
        inOrder(mPaymentFormWidgetInterface, mBinCheckPresenter, savedPaymentMethodFormPresenter1,
            savedPaymentMethodFormPresenter2);
    givenRandomCollectionWithPaypal();

    mPaymentFormWidgetPresenter.onRefreshPaymentFormWidgetView(mShoppingCartCollectionOptions);

    inOrder.verify(mPaymentFormWidgetInterface).clearCreditCardFields();
    inOrder.verify(mPaymentFormWidgetInterface).initCreditCardSpinner();
    inOrder.verify(mBinCheckPresenter).initPresenter(mShoppingCartCollectionOptions);
    inOrder.verify(mPaymentFormWidgetInterface).hideBankTransfer();
    inOrder.verify(mPaymentFormWidgetInterface).hideTrustlyWidget();
    inOrder.verify(mPaymentFormWidgetInterface).hidePaypalWidget();
    inOrder.verify(mPaymentFormWidgetInterface).hideKlarnaWidget();
    inOrder.verify(savedPaymentMethodFormPresenter1).refreshRow(mShoppingCartCollectionOptions);
    inOrder.verify(savedPaymentMethodFormPresenter2).refreshRow(mShoppingCartCollectionOptions);
    inOrder.verify(mPaymentFormWidgetInterface).setPaymentMethodsUnchecked();
  }

  private void givenRandomCollectionWithNoPaypal() {
    mShoppingCartCollectionOptions = new ArrayList<>();
    ShoppingCartCollectionOption creditCardCollectionOption = givenCreditCardCollection();
    mShoppingCartCollectionOptions.add(creditCardCollectionOption);

    ShoppingCartCollectionOption bankTransferCollectionOption = givenBankTransferCollection();
    mShoppingCartCollectionOptions.add(bankTransferCollectionOption);
  }

  private void givenRandomCollectionWithPaypal() {
    mShoppingCartCollectionOptions = new ArrayList<>();
    ShoppingCartCollectionOption creditCardCollectionOption = givenCreditCardCollection();
    mShoppingCartCollectionOptions.add(creditCardCollectionOption);

    ShoppingCartCollectionOption bankTransferCollectionOption = givenBankTransferCollection();
    mShoppingCartCollectionOptions.add(bankTransferCollectionOption);

    ShoppingCartCollectionOption paypalCollectionOption = givenPaypalCollection();
    mShoppingCartCollectionOptions.add(paypalCollectionOption);
  }

  private void givenOnlyCreditCardCollection() {
    mShoppingCartCollectionOptions = new ArrayList<>();
    ShoppingCartCollectionOption creditCardCollectionOption = givenCreditCardCollection();
    mShoppingCartCollectionOptions.add(creditCardCollectionOption);
  }

  private void givenCreditCardAndPaypalCollection() {
    mShoppingCartCollectionOptions = new ArrayList<>();
    ShoppingCartCollectionOption creditCardCollectionOption = givenCreditCardCollection();
    mShoppingCartCollectionOptions.add(creditCardCollectionOption);

    ShoppingCartCollectionOption paypalCollectionOption = givenPaypalCollection();
    mShoppingCartCollectionOptions.add(paypalCollectionOption);
  }

  private void givenCreditCardAndBanktransferCollection() {
    mShoppingCartCollectionOptions = new ArrayList<>();
    ShoppingCartCollectionOption creditCardCollectionOption = givenCreditCardCollection();
    mShoppingCartCollectionOptions.add(creditCardCollectionOption);

    ShoppingCartCollectionOption bankTransferCollectionOption = givenBankTransferCollection();
    mShoppingCartCollectionOptions.add(bankTransferCollectionOption);
  }

  private void givenCreditCardAndTrustlyCollection() {
    mShoppingCartCollectionOptions = new ArrayList<>();

    ShoppingCartCollectionOption trustlyCardCollectionOption = givenTrustlyCollection();
    mShoppingCartCollectionOptions.add(trustlyCardCollectionOption);

    ShoppingCartCollectionOption creditCardCollectionOption = givenCreditCardCollection();
    mShoppingCartCollectionOptions.add(creditCardCollectionOption);
  }

  private void givenCreditCardAndKlarnaCollection() {
    mShoppingCartCollectionOptions = new ArrayList<>();

    ShoppingCartCollectionOption klarnaCardCollectionOption = givenKlarnaCollection();
    mShoppingCartCollectionOptions.add(klarnaCardCollectionOption);

    ShoppingCartCollectionOption creditCardCollectionOption = givenCreditCardCollection();
    mShoppingCartCollectionOptions.add(creditCardCollectionOption);
  }

  private void givenCreditCardAndKlarnaAndTrustlyCollection() {
    mShoppingCartCollectionOptions = new ArrayList<>();

    ShoppingCartCollectionOption klarnaCardCollectionOption = givenKlarnaCollection();
    mShoppingCartCollectionOptions.add(klarnaCardCollectionOption);

    ShoppingCartCollectionOption creditCardCollectionOption = givenCreditCardCollection();
    mShoppingCartCollectionOptions.add(creditCardCollectionOption);

    ShoppingCartCollectionOption trustlyCardCollectionOption = givenTrustlyCollection();
    mShoppingCartCollectionOptions.add(trustlyCardCollectionOption);
  }

  private void givenAllPaymentMethods() {
    mShoppingCartCollectionOptions = new ArrayList<>();

    ShoppingCartCollectionOption bankTransferCollectionOption = givenBankTransferCollection();
    mShoppingCartCollectionOptions.add(bankTransferCollectionOption);

    ShoppingCartCollectionOption klarnaCardCollectionOption = givenKlarnaCollection();
    mShoppingCartCollectionOptions.add(klarnaCardCollectionOption);

    ShoppingCartCollectionOption trustlyCardCollectionOption = givenTrustlyCollection();
    mShoppingCartCollectionOptions.add(trustlyCardCollectionOption);

    ShoppingCartCollectionOption creditCardCollectionOption = givenCreditCardCollection();
    mShoppingCartCollectionOptions.add(creditCardCollectionOption);

    ShoppingCartCollectionOption paypalCollectionOption = givenPaypalCollection();
    mShoppingCartCollectionOptions.add(paypalCollectionOption);
  }

  private ShoppingCartCollectionOption givenCreditCardCollection() {
    ShoppingCartCollectionOption creditCardCollectionOption = new ShoppingCartCollectionOption();
    CollectionMethod creditCardCollectionMethod = new CollectionMethod();
    creditCardCollectionMethod.setType(CollectionMethodType.CREDITCARD);
    creditCardCollectionOption.setMethod(creditCardCollectionMethod);
    return creditCardCollectionOption;
  }

  private ShoppingCartCollectionOption givenBankTransferCollection() {
    ShoppingCartCollectionOption bankTransferCollectionOption = new ShoppingCartCollectionOption();
    CollectionMethod banktransferCollectionMethod = new CollectionMethod();
    banktransferCollectionMethod.setType(CollectionMethodType.BANKTRANSFER);
    bankTransferCollectionOption.setMethod(banktransferCollectionMethod);
    return bankTransferCollectionOption;
  }

  private ShoppingCartCollectionOption givenTrustlyCollection() {
    ShoppingCartCollectionOption trustlyCardCollectionOption = new ShoppingCartCollectionOption();
    CollectionMethod trustlyCardCollectionMethod = new CollectionMethod();
    trustlyCardCollectionMethod.setType(CollectionMethodType.TRUSTLY);
    trustlyCardCollectionOption.setFee(new BigDecimal(3.00));
    trustlyCardCollectionOption.setMethod(trustlyCardCollectionMethod);
    return trustlyCardCollectionOption;
  }

  private ShoppingCartCollectionOption givenKlarnaCollection() {
    ShoppingCartCollectionOption klarnaCardCollectionOption = new ShoppingCartCollectionOption();
    CollectionMethod klarnaCardCollectionMethod = new CollectionMethod();
    klarnaCardCollectionMethod.setType(CollectionMethodType.KLARNA);
    klarnaCardCollectionOption.setFee(new BigDecimal(3.00));
    klarnaCardCollectionOption.setMethod(klarnaCardCollectionMethod);
    return klarnaCardCollectionOption;
  }

  private ShoppingCartCollectionOption givenPaypalCollection() {
    ShoppingCartCollectionOption paypalCollectionOption = new ShoppingCartCollectionOption();
    paypalCollectionOption.setFee(new BigDecimal(3.00));
    CollectionMethod paypalCollectionMethod = new CollectionMethod();
    paypalCollectionMethod.setType(CollectionMethodType.PAYPAL);
    paypalCollectionOption.setMethod(paypalCollectionMethod);
    return paypalCollectionOption;
  }

  private CredentialsInterface getCredentials() {
    return new CredentialsInterface() {
      @Override public String getUser() {

        return null;
      }

      @Override public String getPassword() {

        return null;
      }

      @Override public void setPassword(String newPassword) {

      }

      @Override public CredentialsType getType() {

        return null;
      }

      @Override public String getCredentialTypeValue() {

        return null;
      }
    };
  }
}
