package com.odigeo.presenter;

import com.odigeo.data.db.helper.CitiesHandlerInterface;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.net.listener.OnRequestDataListListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.interactors.AutoCompleteInteractor;
import com.odigeo.presenter.contracts.navigators.NavigationDrawerNavigatorInterface;
import com.odigeo.presenter.contracts.views.SearchDropOffCardViewInterface;
import com.odigeo.test.mock.mocks.CityMocks;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class SearchDropOffCardViewPresenterTest {

  private static String mockedCityIata = "MAD";

  @Mock private SearchDropOffCardViewInterface view;
  @Mock private NavigationDrawerNavigatorInterface navigator;
  @Mock private AutoCompleteInteractor autoCompleteInteractor;
  @Mock private PreferencesControllerInterface preferencesControllerInterface;
  @Mock private CitiesHandlerInterface citiesHandlerInterface;

  private SearchDropOffCardViewPresenter presenter;

  @Captor private ArgumentCaptor<OnRequestDataListListener<City>> onRequestDataListener;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    presenter = new SearchDropOffCardViewPresenter(view, navigator, preferencesControllerInterface,
        citiesHandlerInterface);
  }

  @Test public void shouldReturnCityName() {
    when(citiesHandlerInterface.getCity(anyString())).thenReturn(
        CityMocks.provideCityWithIATA(mockedCityIata));

    String cityName = presenter.getCityName(mockedCityIata);

    assertNotEquals(cityName, mockedCityIata);
  }

  @Test public void shouldReturnCityIATA() {
    when(citiesHandlerInterface.getCity(anyString())).thenReturn(null);

    String cityName = presenter.getCityName(mockedCityIata);

    assertEquals(cityName, mockedCityIata);
  }
}
