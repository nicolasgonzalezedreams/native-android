package com.odigeo.presenter;

import com.odigeo.data.entity.contract.FormSendTypeDTO;
import com.odigeo.data.entity.contract.UserInteractionNeededResponse;
import com.odigeo.data.entity.shoppingCart.BookingAdditionalParameter;
import com.odigeo.data.entity.shoppingCart.ResumeDataRequest;
import com.odigeo.interactors.provider.AssetsProvider;
import com.odigeo.presenter.contracts.ExternalPaymentNavigatorInterface;
import com.odigeo.presenter.contracts.views.ExternalPaymentViewInterface;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.odigeo.presenter.PaymentPresenter.REDIRECT_URL_EXTERNAL_PAYMENT;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class ExternalPaymentPresenterTest {

  private static final String REDIRECT_URL = "http://odigeo.com";
  private static final String TOKEN_PARAM = "token";
  private static final String TOKEN_VALUE = "value";
  private static final String RESPONSE_PAYPAL_URL =
      "http://odigeo.com/?status=cancelled&token=EC-0VW14472JS962910J";
  private static final String STATUS = "status";
  private static final String STATUS_VALUE = "cancelled";
  private static final String TOKEN_RANDOM_VALUE = "EC-0VW14472JS962910J";

  private static final String MOCK_JS = "function serialized() {}";

  @Mock private ExternalPaymentNavigatorInterface mExternalPaymentNavigator;
  @Mock private ExternalPaymentViewInterface mExternalPaymentView;
  @Mock private AssetsProvider assetsProvider;

  private ExternalPaymentPresenter mExternalPaymentPresenter;

  @Before public void setUp() throws Exception {

    when(assetsProvider.loadAssetAsString(anyString())).thenReturn(MOCK_JS);

    mExternalPaymentPresenter =
        new ExternalPaymentPresenter(mExternalPaymentView, mExternalPaymentNavigator,
            assetsProvider);
  }

  @Test public void shouldNavigateUrl_onPaymentUrlChanged() {
    UserInteractionNeededResponse userInteractionNeededResponse =
        givenUserInteractionNeededResponsePaypal();

    mExternalPaymentPresenter.loadUrl(userInteractionNeededResponse);

    verify(mExternalPaymentView, times(1)).loadUrl(anyString());

    verifyNoMoreInteractions(mExternalPaymentView);
  }

  @Test public void shouldPostUrl_onPaymentUrlChanged() {
    UserInteractionNeededResponse userInteractionNeededResponse =
        givenUserInteractionNeededResponsePaypal();
    userInteractionNeededResponse.setFormSendType(FormSendTypeDTO.POST);

    mExternalPaymentPresenter.loadUrl(userInteractionNeededResponse);

    verify(mExternalPaymentView, times(1)).loadPostUrl(
        userInteractionNeededResponse.getRedirectUrl(),
        userInteractionNeededResponse.getParameters());

    verifyNoMoreInteractions(mExternalPaymentView);
  }

  @Test public void shouldNavigateBack_onPaymentFinishUrl()
      throws UnsupportedEncodingException, MalformedURLException {

    mExternalPaymentPresenter.onUrlChanged(PaymentPresenter.REDIRECT_URL_EXTERNAL_PAYMENT);

    verify(mExternalPaymentNavigator, times(1)).finishExternalPaymentNavigator(anyInt(),
        any(ResumeDataRequest.class));

    verifyNoMoreInteractions(mExternalPaymentNavigator);
  }

  @Test public void onParseFormData_shouldLoadSerializedJSAndInjectedJS() {
    mExternalPaymentPresenter.onParseFormData();

    String injectedJs = "var element = document.querySelector(\"form[action^='"
        + REDIRECT_URL_EXTERNAL_PAYMENT
        + "'][method='POST']\");";
    injectedJs += "if (element != undefined) { "
        + "element.submit = function(){};"
        + "android.parseFormData(serialize(element));"
        + "}";

    verify(mExternalPaymentView).loadUrl("javascript:" + MOCK_JS);
    verify(mExternalPaymentView).loadUrl("javascript:" + injectedJs);

    verifyNoMoreInteractions(mExternalPaymentView);
  }

  @Test public void onUrlFinished_hideLoadingView() {
    mExternalPaymentPresenter.onUrlLoadedFinished();

    verify(mExternalPaymentView).hideProgressBar();

    verifyNoMoreInteractions(mExternalPaymentView);
  }

  @Test public void shouldBuildResumeData()
      throws UnsupportedEncodingException, MalformedURLException {
    ResumeDataRequest resumeDataRequest =
        mExternalPaymentPresenter.buildResumeDataRequest(RESPONSE_PAYPAL_URL);
    List<BookingAdditionalParameter> bookingAdditionalParameters =
        resumeDataRequest.getAdditionalParameters();
    assertEquals(bookingAdditionalParameters.get(0).getName(), STATUS);
    assertEquals(bookingAdditionalParameters.get(0).getValue(), STATUS_VALUE);
    assertEquals(bookingAdditionalParameters.get(1).getName(), TOKEN_PARAM);
    assertEquals(bookingAdditionalParameters.get(1).getValue(), TOKEN_RANDOM_VALUE);
  }

  private UserInteractionNeededResponse givenUserInteractionNeededResponsePaypal() {
    UserInteractionNeededResponse userInteractionNeededResponsePaypal =
        new UserInteractionNeededResponse();
    userInteractionNeededResponsePaypal.setRedirectUrl(REDIRECT_URL);
    Map<String, String> parameters = new HashMap<>();
    parameters.put(TOKEN_PARAM, TOKEN_VALUE);
    userInteractionNeededResponsePaypal.setParameters(parameters);
    return userInteractionNeededResponsePaypal;
  }
}
