package com.odigeo.presenter;

import com.odigeo.interactors.LoginInteractor;
import com.odigeo.interactors.MyTripsInteractor;
import com.odigeo.interactors.TravellerDetailInteractor;
import com.odigeo.presenter.contracts.navigators.RegisterNavigatorInterface;
import com.odigeo.presenter.contracts.views.RegisterViewInterface;
import org.junit.Test;
import org.mockito.Mockito;

public class RegisterPresenterTest {

  private static final String USER_NAME = "testuser@test.com";
  private static final String PASSWORD = "password";
  private RegisterViewInterface view;
  //private RegisterUserPasswordInteractor registerUserPasswordInteractor;
  private TravellerDetailInteractor travellerDetailInteractor;
  private RegisterNavigatorInterface navigator;
  private LoginInteractor loginInteractor;
  private MyTripsInteractor myTripsInteractor;

  private void initializeDependencies() {
    view = Mockito.mock(RegisterViewInterface.class);
    //  registerUserPasswordInteractor = Mockito.mock(RegisterUserPasswordInteractor.class);
    travellerDetailInteractor = Mockito.mock(TravellerDetailInteractor.class);
    myTripsInteractor = Mockito.mock(MyTripsInteractor.class);
    navigator = Mockito.mock(RegisterNavigatorInterface.class);
    myTripsInteractor = Mockito.mock(MyTripsInteractor.class);
    loginInteractor = Mockito.mock(LoginInteractor.class);
  }

  @Test public void validateUserAndPasswordFormatTest() {

  }
/*    @Test
    public void validateUserAndPasswordFormatTest() {

        initializeDependencies();

        when(loginInteractor.validateUsernameAndPasswordFormat(USER_NAME, PASSWORD)).thenReturn(true);

        RegisterPresenter registerPresenter = new RegisterPresenter(view, registerUserPasswordInteractor, loginInteractor,
            travellerDetailInteractor, myTripsInteractor, navigator);
        registerPresenter.validateUsernameAndPasswordFormat(USER_NAME, PASSWORD);
        Mockito.verify(loginInteractor, times(1)).validateUsernameAndPasswordFormat(USER_NAME, PASSWORD);
        Mockito.verify(view, times(1)).enableSignUpButton(true);

    } */


/*    @Test
    public void validateUserNameFormatTest() {
        initializeDependencies();

        when(loginInteractor.validateUsernameFormat(USER_NAME)).thenReturn(true);

        RegisterPresenter registerPresenter = new RegisterPresenter(view, registerUserPasswordInteractor, loginInteractor,
            travellerDetailInteractor, myTripsInteractor, navigator);
        Assert.assertTrue(registerPresenter.validateUsernameFormat(USER_NAME));
        Mockito.verify(loginInteractor, times(1)).validateUsernameFormat(USER_NAME);
    } */

/*    @Test
    public void singUpPasswordWhenUserRegisterOkTest() {

        initializeDependencies();

        Answer answer = new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                UserRegisterPasswordListener callback = (UserRegisterPasswordListener) invocation.getArguments()[0];
                callback.onUserRegisterOk((String) invocation.getArguments()[1]);
                return null;
            }
        };

        Mockito.doAnswer(answer).when(registerUserPasswordInteractor)
            .registerUserPassword(any(UserRegisterPasswordListener.class), any(String.class), any(String.class), any(Boolean.class));

        RegisterPresenter registerPresenter = new RegisterPresenter(view, registerUserPasswordInteractor, loginInteractor,
            travellerDetailInteractor, myTripsInteractor, navigator);
        registerPresenter.signUpPassword(USER_NAME, PASSWORD, false);
        verify(view, times(1)).hideProgress();
        verify(navigator, times(1)).registerSuccess(USER_NAME);

    } */

/*    @Test
    public void singUpPasswordWhenOnUserRegisterFailsTest() {

        initializeDependencies();

        Answer answer = new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                UserRegisterPasswordListener callback = (UserRegisterPasswordListener) invocation.getArguments()[0];
                callback.onUserRegisterFail();
                return null;
            }
        };

        Mockito.doAnswer(answer).when(registerUserPasswordInteractor)
            .registerUserPassword(any(UserRegisterPasswordListener.class), any(String.class), any(String.class), any(Boolean.class));

        RegisterPresenter registerPresenter = new RegisterPresenter(view, registerUserPasswordInteractor, loginInteractor,
            travellerDetailInteractor, myTripsInteractor, navigator);
        registerPresenter.signUpPassword(USER_NAME, PASSWORD, false);
        verify(view, times(1)).hideProgress();
        verify(view, times(1)).showRegisterFail();

    } */

/*    @Test
    public void singUpPasswordWhenOnUserWrongTest() {

        initializeDependencies();

        Answer answer = new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                UserRegisterPasswordListener callback = (UserRegisterPasswordListener) invocation.getArguments()[0];
                callback.onUserWrong();
                return null;
            }
        };

        Mockito.doAnswer(answer).when(registerUserPasswordInteractor)
            .registerUserPassword(any(UserRegisterPasswordListener.class), any(String.class), any(String.class), any(Boolean.class));

        RegisterPresenter registerPresenter = new RegisterPresenter(view, registerUserPasswordInteractor, loginInteractor,
            travellerDetailInteractor, myTripsInteractor, navigator);
        registerPresenter.signUpPassword(USER_NAME, PASSWORD, false);
        verify(view, times(1)).hideProgress();
        verify(view, times(1)).setUsernameError();

    } */

/*    @Test
    public void singUpPasswordWhenOnPasswordWrongTest() {

        initializeDependencies();

        Answer answer = new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                UserRegisterPasswordListener callback = (UserRegisterPasswordListener) invocation.getArguments()[0];
                callback.onPasswordWrong();
                return null;
            }
        };

        Mockito.doAnswer(answer).when(registerUserPasswordInteractor)
            .registerUserPassword(any(UserRegisterPasswordListener.class), any(String.class), any(String.class), any(Boolean.class));

        RegisterPresenter registerPresenter = new RegisterPresenter(view, registerUserPasswordInteractor, loginInteractor,
            travellerDetailInteractor, myTripsInteractor, navigator);
        registerPresenter.signUpPassword(USER_NAME, PASSWORD, false);
        verify(view, times(1)).hideProgress();
        verify(view, times(1)).setPasswordError();

    } */

/*     @Test
    public void singUpPasswordWhenOnInvalidadCredentialForUserTest() {

        initializeDependencies();

        Answer answer = new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                UserRegisterPasswordListener callback = (UserRegisterPasswordListener) invocation.getArguments()[0];
                callback.onInvalidCredentialsForUser();
                return null;
            }
        };

        Mockito.doAnswer(answer).when(registerUserPasswordInteractor)
            .registerUserPassword(any(UserRegisterPasswordListener.class), any(String.class), any(String.class), any(Boolean.class));

        RegisterPresenter registerPresenter = new RegisterPresenter(view, registerUserPasswordInteractor, loginInteractor,
            travellerDetailInteractor, myTripsInteractor, navigator);
        registerPresenter.signUpPassword(USER_NAME, PASSWORD, false);
        verify(view, times(1)).hideProgress();
        verify(view, times(1)).showInvalidCredentials();

    } */

    /*@Test
    public void singUpPasswordWhenOnUserAlreadyExistTest() {

        initializeDependencies();

        Answer answer = new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                UserRegisterPasswordListener callback = (UserRegisterPasswordListener) invocation.getArguments()[0];
                callback.onUserAlreadyExist();
                return null;
            }
        };

        Mockito.doAnswer(answer).when(registerUserPasswordInteractor)
            .registerUserPassword(any(UserRegisterPasswordListener.class), any(String.class), any(String.class), any(Boolean.class));

        RegisterPresenter registerPresenter = new RegisterPresenter(view, registerUserPasswordInteractor, loginInteractor,
            travellerDetailInteractor, myTripsInteractor, navigator, userSessionInteractor);
        verify(view, times(1)).hideProgress();
        verify(view, times(1)).showUserAlreadyExist();

    }*/

/*    @Test
    public void singUpPasswordWhenOnUserRegisteredFacebookTest() {

        initializeDependencies();

        Answer answer = new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                UserRegisterPasswordListener callback = (UserRegisterPasswordListener) invocation.getArguments()[0];
                callback.onUserRegisteredFacebook((String) invocation.getArguments()[1]);
                return null;
            }
        };

        Mockito.doAnswer(answer).when(registerUserPasswordInteractor)
            .registerUserPassword(any(UserRegisterPasswordListener.class), any(String.class), any(String.class), any(Boolean.class));

        RegisterPresenter registerPresenter = new RegisterPresenter(view, registerUserPasswordInteractor, loginInteractor,
            travellerDetailInteractor, myTripsInteractor, navigator);
        registerPresenter.signUpPassword(USER_NAME, PASSWORD, false);
        verify(view, times(1)).hideProgress();
        verify(navigator, times(1)).showUserRegisteredFacebook(USER_NAME);

    } */

/*    @Test
    public void singUpPasswordWhenOnUserRegisteredGoogleTest() {

        initializeDependencies();

        Answer answer = new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                UserRegisterPasswordListener callback = (UserRegisterPasswordListener) invocation.getArguments()[0];
                callback.onUserRegisteredGoogle((String) invocation.getArguments()[1]);
                return null;
            }
        };

        Mockito.doAnswer(answer).when(registerUserPasswordInteractor)
            .registerUserPassword(any(UserRegisterPasswordListener.class), any(String.class), any(String.class), any(Boolean.class));

        RegisterPresenter registerPresenter = new RegisterPresenter(view, registerUserPasswordInteractor, loginInteractor,
            travellerDetailInteractor, myTripsInteractor, navigator);
        registerPresenter.signUpPassword(USER_NAME, PASSWORD, false);
        verify(view, times(1)).hideProgress();
        verify(navigator, times(1)).showUserRegisteredGoogle(USER_NAME);

    } */
}