package com.odigeo.presenter;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.MyTripsInteractor;
import com.odigeo.interactors.MyTripsManualImportInteractor;
import com.odigeo.presenter.contracts.navigators.MyTripsNavigatorInterface;
import com.odigeo.presenter.contracts.views.MyTripsManualImportViewInterface;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class MyTripsManualImportPresenterTest {

  @Mock private MyTripsManualImportViewInterface view;
  @Mock private MyTripsNavigatorInterface navigator;
  @Mock private MyTripsManualImportInteractor interactor;
  @Mock private SessionController sessionController;
  @Mock private TrackerControllerInterface trackerController;
  @Mock private MyTripsInteractor myTripsInteractor;

  private MyTripsManualImportPresenter presenter;

  private String emailTest;
  private String bookingIdTest;
  private Booking booking;

  @Before public void setUp() throws Exception {

    presenter = new MyTripsManualImportPresenter(view, navigator, interactor, trackerController,
        myTripsInteractor);

    doReturn(true).when(view).isViewVisible();
    emailTest = "test@odigeo.com";
    bookingIdTest = "432482764";
    booking = new Booking();
    booking.setTripType(Booking.TRIP_TYPE_ONE_WAY);
  }

  @Test public void shouldShowDialog_importTripSuccess_newBooking() {
    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        presenter.onSaveImportedTripListener.onResponse(booking, false);
        return null;
      }
    }).when(interactor).importTrip(presenter.onSaveImportedTripListener, emailTest, bookingIdTest);

    presenter.importTrip(emailTest, bookingIdTest);

    verify(view).showImportSuccess(booking);
  }

  @Test public void shouldShowDialog_importTripSuccess_alreadyImported() {
    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        presenter.onSaveImportedTripListener.onResponse(booking, true);
        return null;
      }
    }).when(interactor).importTrip(presenter.onSaveImportedTripListener, emailTest, bookingIdTest);

    presenter.importTrip(emailTest, bookingIdTest);

    verify(view).showAlreadyImported(booking);
  }

  @Test public void shouldShowDialog_importTripError() {
    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        presenter.onSaveImportedTripListener.onError(MslError.UNK_001, "Error");
        return null;
      }
    }).when(interactor).importTrip(presenter.onSaveImportedTripListener, emailTest, bookingIdTest);

    presenter.importTrip(emailTest, bookingIdTest);

    verify(view).showImportFail();
  }

  @Test public void shouldTrackEvent_importActiveBooking() {

    final Booking activeBooking = mock(Booking.class);
    when(activeBooking.isActive()).thenReturn(true);

    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        presenter.onSaveImportedTripListener.onResponse(activeBooking, false);
        return null;
      }
    }).when(interactor).importTrip(presenter.onSaveImportedTripListener, emailTest, bookingIdTest);

    when(myTripsInteractor.hasActiveBookings()).thenReturn(true);

    presenter.importTrip(emailTest, bookingIdTest);

    InOrder inOrder = inOrder(myTripsInteractor, trackerController);
    inOrder.verify(myTripsInteractor).hasActiveBookings();
    inOrder.verify(trackerController).trackActiveBookingsOnImportTrip();
    inOrder.verifyNoMoreInteractions();
  }

  @Test public void shouldNotTrackEvent_importInActiveBooking() {

    final Booking activeBooking = mock(Booking.class);
    when(activeBooking.isActive()).thenReturn(false);

    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        presenter.onSaveImportedTripListener.onResponse(activeBooking, false);
        return null;
      }
    }).when(interactor).importTrip(presenter.onSaveImportedTripListener, emailTest, bookingIdTest);

    when(myTripsInteractor.hasActiveBookings()).thenReturn(false);

    presenter.importTrip(emailTest, bookingIdTest);

    InOrder inOrder = inOrder(myTripsInteractor, trackerController);
    inOrder.verify(myTripsInteractor, never()).hasActiveBookings();
    inOrder.verify(trackerController, never()).trackActiveBookingsOnImportTrip();
    inOrder.verifyNoMoreInteractions();
  }
}
