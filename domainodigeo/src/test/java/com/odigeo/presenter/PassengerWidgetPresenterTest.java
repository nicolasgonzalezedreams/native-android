package com.odigeo.presenter;

import com.odigeo.data.entity.parser.TravellerRequestToUserTravellerParser;
import com.odigeo.data.entity.parser.UpdateUserTravellerWithTravellerRequestAndBuyerRequest;
import com.odigeo.data.entity.shoppingCart.Itinerary;
import com.odigeo.data.entity.shoppingCart.TravellerRequiredFields;
import com.odigeo.data.entity.shoppingCart.TravellerType;
import com.odigeo.data.entity.shoppingCart.request.TravellerRequest;
import com.odigeo.data.entity.userData.Membership;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.interactors.CheckUserBirthdayInteractor;
import com.odigeo.interactors.GetTravellersByTypeInteractor;
import com.odigeo.interactors.MembershipInteractor;
import com.odigeo.interactors.SortItinerarySectionsInteractor;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.presenter.contracts.views.PassengerWidget;
import com.odigeo.tools.ConditionRulesHelper;
import com.odigeo.tools.DateHelperInterface;
import com.odigeo.validations.BirthdateValidator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.odigeo.data.entity.shoppingCart.ItinerarySortCriteria.MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE;
import static com.odigeo.data.entity.shoppingCart.TravellerType.CHILD;
import static com.odigeo.data.entity.shoppingCart.TravellerType.INFANT;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class PassengerWidgetPresenterTest {

  private static final int ANY_INT_RANDOM = 123;

  private PassengerWidgetPresenter mPresenter;

  @Mock private PassengerWidget view;
  @Mock private CheckUserBirthdayInteractor checkUserBirthdayInteractor;
  @Mock private DateHelperInterface dateHelperInterface;
  @Mock private ConditionRulesHelper conditionRulesHelper;
  @Mock private UpdateUserTravellerWithTravellerRequestAndBuyerRequest
      updateUserTravellerWithTravellerRequestAndBuyerRequest;
  @Mock private TravellerRequestToUserTravellerParser travellerRequestToUserTravellerParser;
  @Mock private GetTravellersByTypeInteractor getTravellersByTypeInteractor;
  @Mock private List<UserTraveller> mTravellersGroupByWidgetType;
  @Mock private BirthdateValidator birthdateValidator;
  @Mock private SortItinerarySectionsInteractor sortItinerarySectionsInteractor;
  @Mock private MarketProviderInterface marketProvider;
  @Mock private MembershipInteractor membershipInteractor;
  private Itinerary itinerary = new Itinerary();

  @Before public void setup() {
    mPresenter =
        new PassengerWidgetPresenter(view, checkUserBirthdayInteractor, dateHelperInterface,
            conditionRulesHelper, updateUserTravellerWithTravellerRequestAndBuyerRequest,
            travellerRequestToUserTravellerParser, getTravellersByTypeInteractor,
            sortItinerarySectionsInteractor, marketProvider, membershipInteractor);

    mPresenter.setUserTravellers(Collections.<UserTraveller>emptyList());
    mPresenter.setPassengerInformation(new TravellerRequiredFields(), birthdateValidator);
  }

  @Test public void testGetUserTravellersShouldSetDefaultTraveller() {
    when(view.getPassengerSelected()).thenReturn(-1);

    UserTraveller userTraveller = new UserTraveller();
    final UserProfile userProfile = new UserProfile();
    userTraveller.setUserProfile(userProfile);
    when(travellerRequestToUserTravellerParser.travellerRequestToUserTraveller(
        any(TravellerRequest.class))).thenReturn(userTraveller);

    userTraveller = mPresenter.getUserTraveller();

    assertThat("Traveller was not set as default",
        userTraveller.getUserProfile().isDefaultTraveller(), is(true));
  }

  @Test public void shouldFindBuyerInUserTravellers() {
    List<UserTraveller> userTravellers = provideUserTravellers();
    when(membershipInteractor.getMembershipForCurrentMarket()).thenReturn(
        provideExpectedMembership("pepe", "lopez"));
    mPresenter.setUserTravellers(userTravellers);
    UserTraveller userTraveller = mPresenter.getBuyerMembershipTraveller();
    assertEquals(userTraveller, userTravellers.get(0));
  }

  @Test public void shouldNotFindBuyerInUserTravellers() {
    List<UserTraveller> userTravellers = provideUserTravellers();
    when(membershipInteractor.getMembershipForCurrentMarket()).thenReturn(
        provideExpectedMembership("juan", "lopez"));

    mPresenter.setUserTravellers(userTravellers);

    UserTraveller userTraveller = mPresenter.getBuyerMembershipTraveller();
    assertNotEquals(userTraveller, userTravellers.get(0));
  }

  @Test public void shouldNotShowAlertAndMembershipProfileWhenThereIsNoMember() {
    String randomTraveller = "randomTraveller";
    when(membershipInteractor.isMemberForCurrentMarket()).thenReturn(false);

    mPresenter.setUserTravellers(null);
    mPresenter.onChangeMembership(randomTraveller);

    verify(view, never()).showMembershipAlert();
    verify(view, never()).showSpinnerMembershipTitle();
    verify(view, never()).propagateItinerarySortCriteria(MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE);
  }

  @Test public void shouldShowAlertWhenMemberIsNotTravelling() {
    String randomTraveller = "randomTraveller";
    when(membershipInteractor.getMembershipForCurrentMarket()).thenReturn(
        provideExpectedMembership("pepe", "lopez"));
    when(membershipInteractor.isMemberForCurrentMarket()).thenReturn(true);
    when(view.isPassengerSelectedInAnotherTravellerSpinner(anyString())).thenReturn(false);

    mPresenter.membershipWasSelected();
    mPresenter.setUserTravellers(provideUserTravellers());
    mPresenter.onChangeMembership(randomTraveller);

    verify(view).showMembershipAlert();
  }

  @Test public void shouldShowMembershipProfileWhenMemberIsTravelling() {
    String expectedTraveller = "pepe lopez";
    when(membershipInteractor.getMembershipForCurrentMarket()).thenReturn(
        provideExpectedMembership("pepe", "lopez"));
    when(membershipInteractor.isMemberForCurrentMarket()).thenReturn(true);
    when(view.isPassengerSelectedInAnotherTravellerSpinner(anyString())).thenReturn(true);

    mPresenter.setUserTravellers(provideUserTravellers());
    mPresenter.onChangeMembership(expectedTraveller);

    verify(view).showSpinnerMembershipTitle();
    verify(view).showImageMembershipProfile();
    verify(view).propagateItinerarySortCriteria(MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE);
  }

  @Test public void shouldNotShowBirthdateErrorWhenInfantIsNotTurningIntoChildTrip() {
    TravellerRequiredFields travellerRequiredFields = new TravellerRequiredFields();
    travellerRequiredFields.setTravellerType(INFANT);
    mPresenter.setPassengerInformation(travellerRequiredFields, birthdateValidator);

    when(sortItinerarySectionsInteractor.sort(itinerary)).thenReturn(itinerary);
    when(birthdateValidator.isTravellerTypeTurningIntoAnotherTypeDuringTrip(any(int[].class),
        eq(itinerary), any(TravellerType.class), anyInt())).thenReturn(false);

    mPresenter.validateBirthdate(ANY_INT_RANDOM, ANY_INT_RANDOM, ANY_INT_RANDOM, itinerary);

    verify(view, never()).showInfantBirthdateEror();
    verify(view, never()).showChildBirthdateError();
  }

  @Test public void shouldShowBirthdateErrorWhenInfantIsTurningIntoChildDuringTrip() {
    TravellerRequiredFields travellerRequiredFields = new TravellerRequiredFields();
    travellerRequiredFields.setTravellerType(INFANT);
    mPresenter.setPassengerInformation(travellerRequiredFields, birthdateValidator);

    when(sortItinerarySectionsInteractor.sort(itinerary)).thenReturn(itinerary);
    when(birthdateValidator.isTravellerTypeTurningIntoAnotherTypeDuringTrip(any(int[].class),
        any(Itinerary.class), any(TravellerType.class), anyInt())).thenReturn(true);

    mPresenter.validateBirthdate(ANY_INT_RANDOM, ANY_INT_RANDOM, ANY_INT_RANDOM, itinerary);

    verify(view).showInfantBirthdateEror();
    verify(view, never()).showChildBirthdateError();
  }

  @Test public void shouldNotShowBirthdateErrorWhenChildIsNotTurningIntoAdultTrip() {
    TravellerRequiredFields travellerRequiredFields = new TravellerRequiredFields();
    travellerRequiredFields.setTravellerType(CHILD);
    mPresenter.setPassengerInformation(travellerRequiredFields, birthdateValidator);

    when(sortItinerarySectionsInteractor.sort(itinerary)).thenReturn(itinerary);
    when(birthdateValidator.isTravellerTypeTurningIntoAnotherTypeDuringTrip(any(int[].class),
        any(Itinerary.class), any(TravellerType.class), anyInt())).thenReturn(false);

    mPresenter.validateBirthdate(ANY_INT_RANDOM, ANY_INT_RANDOM, ANY_INT_RANDOM, itinerary);

    verify(view, never()).showInfantBirthdateEror();
    verify(view, never()).showChildBirthdateError();
  }

  @Test public void shouldShowBirthdateErrorWhenChildIsTurningIntoAdultDuringTrip() {
    TravellerRequiredFields travellerRequiredFields = new TravellerRequiredFields();
    travellerRequiredFields.setTravellerType(CHILD);
    mPresenter.setPassengerInformation(travellerRequiredFields, birthdateValidator);

    when(sortItinerarySectionsInteractor.sort(itinerary)).thenReturn(itinerary);
    when(birthdateValidator.isTravellerTypeTurningIntoAnotherTypeDuringTrip(any(int[].class),
        any(Itinerary.class), any(TravellerType.class), anyInt())).thenReturn(true);

    mPresenter.validateBirthdate(ANY_INT_RANDOM, ANY_INT_RANDOM, ANY_INT_RANDOM, itinerary);

    verify(view, never()).showInfantBirthdateEror();
    verify(view).showChildBirthdateError();
  }

  private Membership provideExpectedMembership(String name, String lastname) {
    Membership membership = new Membership();
    membership.setFirstName(name);
    membership.setLastNames(lastname);
    return membership;
  }

  private List<UserTraveller> provideUserTravellers() {
    List<UserTraveller> userTravellers = new ArrayList<>();
    UserTraveller userTraveller = new UserTraveller();
    UserProfile userProfile = new UserProfile();
    userProfile.setName("pepe");
    userProfile.setFirstLastName("lopez");
    userTraveller.setUserProfile(userProfile);
    userTravellers.add(userTraveller);
    return userTravellers;
  }
}
