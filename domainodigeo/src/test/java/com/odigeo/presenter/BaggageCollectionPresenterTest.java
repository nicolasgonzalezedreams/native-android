package com.odigeo.presenter;

import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.shoppingCart.BaggageApplicationLevel;
import com.odigeo.data.entity.shoppingCart.BaggageConditions;
import com.odigeo.data.entity.shoppingCart.BaggageDescriptor;
import com.odigeo.data.entity.shoppingCart.Itinerary;
import com.odigeo.data.entity.shoppingCart.SegmentTypeIndex;
import com.odigeo.data.entity.shoppingCart.SelectableBaggageDescriptor;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.presenter.contracts.views.BaggageCollection;
import com.odigeo.test.mock.MocksProvider;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.odigeo.test.mock.MocksProvider.providesItineraryMocks;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class BaggageCollectionPresenterTest {

  @Mock private BaggageCollection mView;
  private List<BaggageConditions> mBaggageConditionsList;
  @Mock private BaggageConditions mBaggageConditions;
  private Itinerary mItinerary;
  private TravelType travelType;

  @Before public void setUp() throws Exception {
    travelType = TravelType.ROUND;
    mItinerary = MocksProvider.providesItineraryMocks().getItinerary();
  }

  @Test public void buildBaggageSelectionItemsBaggageNotIncludedInAllSegmentTest() {
    Integer pieces = 0;
    Integer kilos = null;
    List<Traveller> travellers = new ArrayList<>();
    int passengerWidgetPosition = 0;

    mBaggageConditionsList = new ArrayList<>();
    mBaggageConditionsList.add(mBaggageConditions);

    when(mBaggageConditions.getSelectableBaggageDescriptor()).thenReturn(
        new ArrayList<SelectableBaggageDescriptor>());
    when(mBaggageConditions.getBaggageDescriptorIncludedInPrice()).thenReturn(
        new BaggageDescriptor(pieces, kilos));
    when(mBaggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.ITINERARY);

    BaggageCollectionPresenter baggageCollectionPresenter = new BaggageCollectionPresenter(mView);
    baggageCollectionPresenter.processBaggage(mBaggageConditionsList, mItinerary, travellers,
        passengerWidgetPosition, travelType);

    verify(mView, never()).showHeaderSubtitleInfo();
    assertEquals(baggageCollectionPresenter.checkIfAllBaggageConditionsNotIncludeBaggage(
        mBaggageConditionsList), true);
  }

  @Test public void buildBaggageSelectionItemsAllBaggageIncludedTheSameBaggageWithKilos() {
    Integer pieces = 2;
    Integer kilos = 10;
    List<Traveller> travellers = new ArrayList<>();
    int passengerWidgetPosition = 0;

    mBaggageConditionsList = new ArrayList<>();
    mBaggageConditionsList.add(mBaggageConditions);
    mBaggageConditionsList.add(mBaggageConditions);
    mBaggageConditionsList.add(mBaggageConditions);

    when(mBaggageConditions.getSelectableBaggageDescriptor()).thenReturn(
        new ArrayList<SelectableBaggageDescriptor>());
    when(mBaggageConditions.getBaggageDescriptorIncludedInPrice()).thenReturn(
        new BaggageDescriptor(pieces, kilos));
    when(mBaggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.ITINERARY);

    BaggageCollectionPresenter baggageCollectionPresenter = new BaggageCollectionPresenter(mView);
    baggageCollectionPresenter.processBaggage(mBaggageConditionsList, mItinerary, travellers,
        passengerWidgetPosition, travelType);

    verify(mView, atLeast(1)).showHeaderSubtitleInfo();
    verify(mView, never()).showPersuasiveMessage();
    assertEquals(baggageCollectionPresenter.checkIfAllBaggageConditionsIncludedTheSameBaggage(
        mBaggageConditionsList), true);
  }

  @Test public void buildBaggageSelectionItemsAllBaggageIncludedTheSameBaggageWithoutKilos() {
    Integer pieces = 2;
    Integer kilos = null;
    List<Traveller> travellers = new ArrayList<>();
    int passengerWidgetPosition = 0;

    mBaggageConditionsList = new ArrayList<>();
    mBaggageConditionsList.add(mBaggageConditions);
    mBaggageConditionsList.add(mBaggageConditions);
    mBaggageConditionsList.add(mBaggageConditions);

    when(mBaggageConditions.getSelectableBaggageDescriptor()).thenReturn(
        new ArrayList<SelectableBaggageDescriptor>());
    when(mBaggageConditions.getBaggageDescriptorIncludedInPrice()).thenReturn(
        new BaggageDescriptor(pieces, kilos));
    when(mBaggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.ITINERARY);

    BaggageCollectionPresenter baggageCollectionPresenter = new BaggageCollectionPresenter(mView);
    baggageCollectionPresenter.processBaggage(mBaggageConditionsList, mItinerary, travellers,
        passengerWidgetPosition, travelType);

    verify(mView, atLeast(1)).showHeaderSubtitleInfo();
    verify(mView, never()).showPersuasiveMessage();
    assertEquals(baggageCollectionPresenter.checkIfAllBaggageConditionsIncludedTheSameBaggage(
        mBaggageConditionsList), true);
  }

  @Test public void buildBaggageSelectionItemsApplicationLevelItinerary() {
    Integer pieces = 2;
    Integer kilos = null;
    List<Traveller> travellers = new ArrayList<>();
    int passengerWidgetPosition = 0;

    mBaggageConditionsList = new ArrayList<>();
    mBaggageConditionsList.add(mBaggageConditions);

    ArrayList<SelectableBaggageDescriptor> selectableBaggageDescriptorArrayList = new ArrayList<>();
    selectableBaggageDescriptorArrayList.add(
        new SelectableBaggageDescriptor(new BaggageDescriptor(pieces, kilos), new BigDecimal(10)));

    when(mBaggageConditions.getSelectableBaggageDescriptor()).thenReturn(
        selectableBaggageDescriptorArrayList);
    when(mBaggageConditions.getBaggageDescriptorIncludedInPrice()).thenReturn(
        new BaggageDescriptor(pieces, kilos));
    when(mBaggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.ITINERARY);

    BaggageCollectionPresenter baggageCollectionPresenter = new BaggageCollectionPresenter(mView);
    baggageCollectionPresenter.processBaggage(mBaggageConditionsList, mItinerary, travellers,
        passengerWidgetPosition, travelType);

    assertEquals(mBaggageConditions.getBaggageApplicationLevel(),
        BaggageApplicationLevel.ITINERARY);
  }

  @Test public void buildBaggageSelectionItemsApplicationLevelSegment() {
    Integer pieces = 2;
    Integer kilos = null;
    List<Traveller> travellers = new ArrayList<>();
    int passengerWidgetPosition = 0;

    mItinerary = providesItineraryMocks().getItinerary();
    mBaggageConditionsList = new ArrayList<>();
    mBaggageConditionsList.add(mBaggageConditions);

    ArrayList<SelectableBaggageDescriptor> selectableBaggageDescriptorArrayList = new ArrayList<>();
    selectableBaggageDescriptorArrayList.add(
        new SelectableBaggageDescriptor(new BaggageDescriptor(pieces, kilos), new BigDecimal(10)));

    when(mBaggageConditions.getSelectableBaggageDescriptor()).thenReturn(
        selectableBaggageDescriptorArrayList);
    when(mBaggageConditions.getBaggageDescriptorIncludedInPrice()).thenReturn(
        new BaggageDescriptor(pieces, kilos));
    when(mBaggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.SEGMENT);
    when(mBaggageConditions.getSegmentTypeIndex()).thenReturn(SegmentTypeIndex.FIRST);

    BaggageCollectionPresenter baggageCollectionPresenter = new BaggageCollectionPresenter(mView);
    baggageCollectionPresenter.processBaggage(mBaggageConditionsList, mItinerary, travellers,
        passengerWidgetPosition, travelType);

    assertEquals(mBaggageConditions.getBaggageApplicationLevel(), BaggageApplicationLevel.SEGMENT);
  }
}
