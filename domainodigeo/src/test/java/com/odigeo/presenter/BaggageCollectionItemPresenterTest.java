package com.odigeo.presenter;

import com.odigeo.configuration.ABAlias;
import com.odigeo.configuration.ABPartition;
import com.odigeo.data.entity.shoppingCart.BaggageApplicationLevel;
import com.odigeo.data.entity.shoppingCart.BaggageConditions;
import com.odigeo.data.entity.shoppingCart.BaggageDescriptor;
import com.odigeo.data.entity.shoppingCart.SegmentTypeIndex;
import com.odigeo.data.entity.shoppingCart.SelectableBaggageDescriptor;
import com.odigeo.data.entity.shoppingCart.request.BaggageSelectionRequest;
import com.odigeo.helper.ABTestHelper;
import com.odigeo.presenter.contracts.views.BaggageCollectionItem;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class BaggageCollectionItemPresenterTest {

  @Mock private BaggageCollectionItem view;
  @Mock private ABTestHelper abTestHelper;
  @Mock private BaggageConditions baggageConditions;

  @Test public void processBaggageWithItinerary() {
    Integer kilos = 0;

    when(baggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.ITINERARY);
    when(baggageConditions.getBaggageDescriptorIncludedInPrice()).thenReturn(
        new BaggageDescriptor(null, kilos));

    BaggageCollectionItemPresenter baggageCollectionItemPresenter =
        new BaggageCollectionItemPresenter(view, abTestHelper);
    baggageCollectionItemPresenter.processBaggage(baggageConditions, -1);

    verify(view, times(1)).showTravelInfo();
  }

  @Test public void processBaggageWithSegment() {
    Integer kilos = 0;

    when(baggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.SEGMENT);
    when(baggageConditions.getBaggageDescriptorIncludedInPrice()).thenReturn(
        new BaggageDescriptor(null, kilos));

    BaggageCollectionItemPresenter baggageCollectionItemPresenter =
        new BaggageCollectionItemPresenter(view, abTestHelper);
    baggageCollectionItemPresenter.processBaggage(baggageConditions, -1);

    verify(view, times(1)).showTravelInfo();
  }

  @Test public void processBaggageWithoutBaggage() {
    when(baggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.SEGMENT);

    BaggageCollectionItemPresenter baggageCollectionItemPresenter =
        new BaggageCollectionItemPresenter(view, abTestHelper);
    baggageCollectionItemPresenter.processBaggage(baggageConditions, -1);

    verify(view, times(1)).showBaggageNotIncluded();
  }

  @Test public void processBaggageIncludedInPriceWithoutKilos() {
    Integer pieces = 2;

    when(baggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.SEGMENT);
    when(baggageConditions.getBaggageDescriptorIncludedInPrice()).thenReturn(
        new BaggageDescriptor(pieces, null));

    BaggageCollectionItemPresenter baggageCollectionItemPresenter =
        new BaggageCollectionItemPresenter(view, abTestHelper);
    baggageCollectionItemPresenter.processBaggage(baggageConditions, -1);

    verify(view, times(1)).setBaggageIncludedWithoutKilos(pieces);
  }

  @Test public void processBaggageIncludedInPriceWithKilos() {
    Integer pieces = 2;
    Integer kilos = 4;

    when(baggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.SEGMENT);
    when(baggageConditions.getBaggageDescriptorIncludedInPrice()).thenReturn(
        new BaggageDescriptor(pieces, kilos));

    BaggageCollectionItemPresenter baggageCollectionItemPresenter =
        new BaggageCollectionItemPresenter(view, abTestHelper);
    baggageCollectionItemPresenter.processBaggage(baggageConditions, -1);

    verify(view, times(1)).setBaggageIncludedWithKilos(pieces, kilos);
  }

  @Test public void processBaggageWithSelectableBaggage() {
    Integer pieces = 2;
    Integer kilos = 4;

    List<SelectableBaggageDescriptor> selectableBaggageDescriptorList = new ArrayList<>();
    selectableBaggageDescriptorList.add(
        new SelectableBaggageDescriptor(new BaggageDescriptor(pieces, kilos), new BigDecimal(10)));

    when(baggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.SEGMENT);
    when(baggageConditions.getSelectableBaggageDescriptor()).thenReturn(
        selectableBaggageDescriptorList);

    BaggageCollectionItemPresenter baggageCollectionItemPresenter =
        new BaggageCollectionItemPresenter(view, abTestHelper);
    baggageCollectionItemPresenter.processBaggage(baggageConditions, -1);

    verify(view, times(1)).showBaggageOptions(selectableBaggageDescriptorList);
  }

  @Test public void processBaggageWithSelectableBaggageEmpty() {
    List<SelectableBaggageDescriptor> selectableBaggageDescriptorList = new ArrayList<>();

    when(baggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.SEGMENT);
    when(baggageConditions.getSelectableBaggageDescriptor()).thenReturn(
        selectableBaggageDescriptorList);

    BaggageCollectionItemPresenter baggageCollectionItemPresenter =
        new BaggageCollectionItemPresenter(view, abTestHelper);
    baggageCollectionItemPresenter.processBaggage(baggageConditions, -1);

    verify(view, times(1)).hideAddBaggageOption();
  }

  @Test public void processBaggageBuildBaggageSelectionRequestWithSelectedItem() {
    Integer pieces = 3;
    Integer kilos = 5;
    BigDecimal price = new BigDecimal(10);

    SegmentTypeIndex segmentTypeIndex = SegmentTypeIndex.FIRST;

    BaggageDescriptor baggageDescriptor = new BaggageDescriptor(pieces, kilos);
    SelectableBaggageDescriptor selectableBaggageDescriptor =
        new SelectableBaggageDescriptor(baggageDescriptor, price);

    List<SelectableBaggageDescriptor> selectableBaggageDescriptorList = new ArrayList<>();
    selectableBaggageDescriptorList.add(new SelectableBaggageDescriptor(baggageDescriptor, price));

    when(baggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.SEGMENT);
    when(baggageConditions.getSegmentTypeIndex()).thenReturn(segmentTypeIndex);
    when(baggageConditions.getSelectableBaggageDescriptor()).thenReturn(
        selectableBaggageDescriptorList);
    when(view.getSelectedItem()).thenReturn(selectableBaggageDescriptor);

    BaggageCollectionItemPresenter baggageCollectionItemPresenter =
        new BaggageCollectionItemPresenter(view, abTestHelper);
    baggageCollectionItemPresenter.processBaggage(baggageConditions, -1);

    BaggageSelectionRequest baggageSelectionRequest =
        baggageCollectionItemPresenter.buildBaggageSelectionRequest();

    assertEquals(baggageSelectionRequest.getBaggageDescriptor().getKilos(),
        baggageDescriptor.getKilos());
    assertEquals(baggageSelectionRequest.getBaggageDescriptor().getPieces(),
        baggageDescriptor.getPieces());
    assertEquals(baggageSelectionRequest.getSegmentTypeIndex(), segmentTypeIndex);
  }

  @Test public void processBaggageBuildBaggageSelectionRequestWithBaggageIncluded() {
    Integer pieces = 2;
    Integer kilos = 5;

    SegmentTypeIndex segmentTypeIndex = SegmentTypeIndex.FIRST;

    BaggageDescriptor baggageDescriptor = new BaggageDescriptor(pieces, kilos);

    when(baggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.SEGMENT);
    when(baggageConditions.getBaggageDescriptorIncludedInPrice()).thenReturn(baggageDescriptor);
    when(baggageConditions.getSegmentTypeIndex()).thenReturn(segmentTypeIndex);

    BaggageCollectionItemPresenter baggageCollectionItemPresenter =
        new BaggageCollectionItemPresenter(view, abTestHelper);
    baggageCollectionItemPresenter.processBaggage(baggageConditions, -1);

    BaggageSelectionRequest baggageSelectionRequest =
        baggageCollectionItemPresenter.buildBaggageSelectionRequest();

    assertEquals(baggageSelectionRequest.getBaggageDescriptor().getKilos(),
        baggageDescriptor.getKilos());
    assertEquals(baggageSelectionRequest.getBaggageDescriptor().getPieces(),
        baggageDescriptor.getPieces());
    assertEquals(baggageSelectionRequest.getSegmentTypeIndex(), segmentTypeIndex);
  }

  @Test public void shouldUpdateBaggageSelected() {
    Integer pieces = 2;
    Integer kilos = 4;

    BaggageDescriptor baggageDescriptor = new BaggageDescriptor(pieces, kilos);
    SelectableBaggageDescriptor selectableBaggageDescriptor =
        new SelectableBaggageDescriptor(baggageDescriptor, new BigDecimal(10));

    List<SelectableBaggageDescriptor> selectableBaggageDescriptorList = new ArrayList<>();
    selectableBaggageDescriptorList.add(selectableBaggageDescriptor);

    when(baggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.SEGMENT);
    when(baggageConditions.getSelectableBaggageDescriptor()).thenReturn(
        selectableBaggageDescriptorList);

    when(view.getSelectedItem()).thenReturn(selectableBaggageDescriptor);

    BaggageCollectionItemPresenter baggageCollectionItemPresenter =
        new BaggageCollectionItemPresenter(view, abTestHelper);
    baggageCollectionItemPresenter.processBaggage(baggageConditions, -1);

    verify(view, times(1)).showBaggageOptions(selectableBaggageDescriptorList);

    baggageCollectionItemPresenter.updateBaggageSelected(baggageDescriptor);

    verify(view).showBaggageSelected(selectableBaggageDescriptor);
  }

  @Test public void shouldNotUpdateBaggageSelected() {
    Integer pieces = 2;
    Integer kilos = 4;

    BaggageDescriptor baggageDescriptor = new BaggageDescriptor(pieces, kilos);
    SelectableBaggageDescriptor selectableBaggageDescriptor =
        new SelectableBaggageDescriptor(baggageDescriptor, new BigDecimal(10));

    List<SelectableBaggageDescriptor> selectableBaggageDescriptorList = new ArrayList<>();
    selectableBaggageDescriptorList.add(selectableBaggageDescriptor);

    when(baggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.SEGMENT);
    when(baggageConditions.getSelectableBaggageDescriptor()).thenReturn(
        selectableBaggageDescriptorList);

    when(view.getSelectedItem()).thenReturn(selectableBaggageDescriptor);

    BaggageCollectionItemPresenter baggageCollectionItemPresenter =
        new BaggageCollectionItemPresenter(view, abTestHelper);
    baggageCollectionItemPresenter.processBaggage(baggageConditions, -1);

    verify(view, times(1)).showBaggageOptions(selectableBaggageDescriptorList);

    BaggageDescriptor baggageDescriptorToUpdate = new BaggageDescriptor(1, 2);

    baggageCollectionItemPresenter.updateBaggageSelected(baggageDescriptorToUpdate);

    verify(view, never()).showBaggageSelected(any(SelectableBaggageDescriptor.class));
  }

  @Test public void shouldRemoveBaggageWithoutNag() {
    Integer pieces = 2;
    Integer kilos = 4;

    BaggageDescriptor baggageDescriptor = new BaggageDescriptor(pieces, kilos);
    SelectableBaggageDescriptor selectableBaggageDescriptor =
        new SelectableBaggageDescriptor(baggageDescriptor, new BigDecimal(10));

    List<SelectableBaggageDescriptor> selectableBaggageDescriptorList = new ArrayList<>();
    selectableBaggageDescriptorList.add(selectableBaggageDescriptor);

    when(baggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.SEGMENT);
    when(baggageConditions.getSelectableBaggageDescriptor()).thenReturn(
        selectableBaggageDescriptorList);

    when(view.getSelectedItem()).thenReturn(selectableBaggageDescriptor);

    BaggageCollectionItemPresenter baggageCollectionItemPresenter =
        new BaggageCollectionItemPresenter(view, abTestHelper);
    baggageCollectionItemPresenter.processBaggage(baggageConditions, 0);

    when(abTestHelper.getPartition(ABAlias.BBU_BBUSTERS430, 2)).thenReturn(ABPartition.ONE);
    baggageCollectionItemPresenter.onRemoveBaggageClick();

    verify(view).hideBaggageSelected();
  }

  @Test public void shouldRemoveBaggageWithNag() {
    Integer pieces = 2;
    Integer kilos = 4;

    BaggageDescriptor baggageDescriptor = new BaggageDescriptor(pieces, kilos);
    SelectableBaggageDescriptor selectableBaggageDescriptor =
        new SelectableBaggageDescriptor(baggageDescriptor, new BigDecimal(10));

    List<SelectableBaggageDescriptor> selectableBaggageDescriptorList = new ArrayList<>();
    selectableBaggageDescriptorList.add(selectableBaggageDescriptor);

    when(baggageConditions.getBaggageApplicationLevel()).thenReturn(
        BaggageApplicationLevel.SEGMENT);
    when(baggageConditions.getSelectableBaggageDescriptor()).thenReturn(
        selectableBaggageDescriptorList);

    when(view.getSelectedItem()).thenReturn(selectableBaggageDescriptor);

    BaggageCollectionItemPresenter baggageCollectionItemPresenter =
        new BaggageCollectionItemPresenter(view, abTestHelper);
    baggageCollectionItemPresenter.processBaggage(baggageConditions, 0);

    when(abTestHelper.getPartition(ABAlias.BBU_BBUSTERS430, 2)).thenReturn(ABPartition.TWO);
    baggageCollectionItemPresenter.onRemoveBaggageClick();

    verify(view).showConfirmRemoveBaggage();
  }
}
