package com.odigeo.presenter;

import com.odigeo.constants.PaymentMethodsKeys;
import com.odigeo.data.entity.CreditCardBinDetails;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.interactors.CheckBinFromLocalInteractor;
import com.odigeo.interactors.CheckBinFromNetworkInteractor;
import com.odigeo.presenter.contracts.views.BinCheckViewInterface;
import com.odigeo.presenter.listeners.OnCheckBinListener;
import com.odigeo.test.mock.mocks.PaymentMocks;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BinCheckPresenterTest {

  private final String sixDigitsCreditCardNumber = "123456";
  @Mock private BinCheckViewInterface mBinCheckViewInterface;
  @Mock private CheckBinFromLocalInteractor mCheckBinFromLocalInteractor;
  @Mock private CheckBinFromNetworkInteractor mCheckBinFromNetworkInteractor;
  @Mock private OnCheckBinListener mOnCheckBinListener;
  private List<ShoppingCartCollectionOption> mShoppingCartCollectionOptions;
  private BinCheckPresenter mPresenter;

  @Before public void setUp() throws Exception {

    MockitoAnnotations.initMocks(this);
    mPresenter = new BinCheckPresenter(mBinCheckViewInterface, mCheckBinFromLocalInteractor,
        mCheckBinFromNetworkInteractor);

    ShoppingCartCollectionOption shoppingCartCollectionOption =
        new PaymentMocks().provideShoppingCartCollectionOptionWithCreditCard("VI");
    mShoppingCartCollectionOptions = new ArrayList<>();
    mShoppingCartCollectionOptions.add(shoppingCartCollectionOption);

    mPresenter.initPresenter(mShoppingCartCollectionOptions);
  }

  @Test public void shouldCheckBinLocally_OneDigitCreditCard() {
    String creditCardNumber = "1";
    when(mCheckBinFromLocalInteractor.checkBinLocally(anyString(),
        anyListOf(ShoppingCartCollectionOption.class))).thenReturn(creditCardNumber);

    mPresenter.checkBin(creditCardNumber);

    verify(mCheckBinFromLocalInteractor).checkBinLocally(creditCardNumber,
        mShoppingCartCollectionOptions);
    verify(mBinCheckViewInterface).hidePaymentMethodSpinner();
    verify(mBinCheckViewInterface).flipCardInUI(anyString());
  }

  @Test public void shouldCheckBinLocally_FiveDigitsCreditCard() {
    String creditCardNumber = "12345";

    mPresenter.checkBin(creditCardNumber);

    verify(mCheckBinFromLocalInteractor).checkBinLocally(creditCardNumber,
        mShoppingCartCollectionOptions);
    verify(mBinCheckViewInterface).hidePaymentMethodSpinner();
    verify(mBinCheckViewInterface).flipCardInUI(anyString());
  }

  @Test public void shouldCheckBinMsl_SixDigitsCreditCard() {

    mPresenter.checkBin(sixDigitsCreditCardNumber);

    verify(mCheckBinFromNetworkInteractor).checkBinMSL(anyString(),
        Mockito.any(OnCheckBinListener.class));
  }

  @Test public void shouldShowCreditCardPaymentType_SixDigitsValidCreditCardInCache() {
    CreditCardBinDetails creditCardBinDetails = new PaymentMocks().provideCreditCardBinDetails();

    mPresenter.handleSuccessBinDetails(creditCardBinDetails, sixDigitsCreditCardNumber);

    verify(mBinCheckViewInterface).setCreditCardPaymentType();
    verify(mBinCheckViewInterface).hidePaymentMethodSpinner();
    verify(mBinCheckViewInterface).flipCardInUI(anyString());
  }

  @Test public void shouldShowPaymentMethodPicker_SixDigitsUnknownCreditCardInCache() {
    CreditCardBinDetails emptyCreditCardBinDetails = createEmptyCreditCardBinDetailsMock();

    mPresenter.handleSuccessBinDetails(emptyCreditCardBinDetails, sixDigitsCreditCardNumber);

    verify(mBinCheckViewInterface).showPaymentMethodSpinner();
    verify(mBinCheckViewInterface).flipCardInUI(PaymentMethodsKeys.UNKNOWN);
  }

  private CreditCardBinDetails createEmptyCreditCardBinDetailsMock() {
    return new CreditCardBinDetails();
  }
}
