package com.odigeo.presenter;

import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.presenter.contracts.views.PaymentPurchaseWidgetInterface;
import com.odigeo.presenter.listeners.PaymentPurchaseListener;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

public class PaymentPurchasePresenterTest {

  @Mock private PaymentPurchaseWidgetInterface mPaymentPurchaseWidgetView;
  @Mock private TrackerControllerInterface trackerController;
  @Mock private PaymentPurchaseListener mPaymentPurchaseListener;

  private PaymentPurchasePresenter mPaymentPurchasePresenter;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    mPaymentPurchasePresenter =
        new PaymentPurchasePresenter(mPaymentPurchaseWidgetView, trackerController);
  }

  @Test public void shouldCallOnPaymentPurchase() {
    mPaymentPurchasePresenter.setPaymentPurchaseListener(mPaymentPurchaseListener);
    mPaymentPurchasePresenter.onContinueButtonClick();

    verify(mPaymentPurchaseListener).onPaymentPurchaseContinue();
  }

  @Test public void shouldSetContinueButtonEnable() {
    mPaymentPurchasePresenter.onValidateOptionsChange(true);
    verify(mPaymentPurchaseWidgetView).setContinueButtonEnable(true);
  }
}