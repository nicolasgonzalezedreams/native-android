package com.odigeo.presenter;

import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.interactors.CheckUserCredentialsInteractor;
import com.odigeo.interactors.MyTripsInteractor;
import com.odigeo.interactors.UpdateBookingStatusInteractor;
import com.odigeo.interactors.UpdateBookingsFromNetworkInteractor;
import com.odigeo.interactors.provider.MyTripsProvider;
import com.odigeo.presenter.contracts.navigators.MyTripsNavigatorInterface;
import com.odigeo.presenter.contracts.views.MyTripsViewInterface;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.odigeo.interactors.UpdateBookingStatusInteractor.BookingStatusListener;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class MyTripsPresenterTest {

  @Mock private MyTripsViewInterface view;
  @Mock private MyTripsNavigatorInterface navigator;
  @Mock private MyTripsInteractor interactor;
  @Mock private UpdateBookingsFromNetworkInteractor updateBookingsInteractor;
  @Mock private UpdateBookingStatusInteractor updateBookingStatusInteractor;
  @Mock private PreferencesControllerInterface preferencesControllerInterface;
  @Mock private MyTripsProvider mockedTripsProvider;
  @Mock private TrackerControllerInterface trackerController;
  @Mock private CheckUserCredentialsInteractor checkUserCredentialsInteractor;

  @Captor private ArgumentCaptor<OnRequestDataListener<MyTripsProvider>> bookingsRequestCaptor;
  @Captor private ArgumentCaptor<OnRequestDataListener<Void>> updateBookingsRequestCaptor;
  @Captor private ArgumentCaptor<BookingStatusListener> bookingStatusListenerCaptor;
  private MyTripsPresenter mPresenter;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    mPresenter =
        new MyTripsPresenter(view, navigator, interactor, mockedTripsProvider, trackerController,
            updateBookingsInteractor, updateBookingStatusInteractor, preferencesControllerInterface,
            checkUserCredentialsInteractor);
  }

  @Test public void shouldInitializeBookingsOnUI() {
    mPresenter.initializeLocalBookings();

    verify(interactor).getBookings(bookingsRequestCaptor.capture());
    bookingsRequestCaptor.getValue().onResponse(mockedTripsProvider);

    verify(view).initializeBookings(any(MyTripsProvider.class), anyBoolean());
    verify(view).onRequestFinished(any(Boolean.class));
  }

  @Test public void shouldUpdateBookingsAndTripsWhenUserIsLogged() {
    when(checkUserCredentialsInteractor.isUserLogin()).thenReturn(true);

    mPresenter.triggerUpdate();

    verify(updateBookingsInteractor).updateMyTrips(updateBookingsRequestCaptor.capture());
    updateBookingsRequestCaptor.getValue().onResponse(any(Void.class));

    verify(interactor).getBookings(bookingsRequestCaptor.capture());
    bookingsRequestCaptor.getValue().onResponse(mockedTripsProvider);

    verify(view).notifyBookingsHasChanged(mockedTripsProvider, false);
    verify(view).onRequestFinished(any(Boolean.class));
  }

  @Test public void shouldUpdateBookingsButNotTripsWhenUserIsNotLogged() {
    when(checkUserCredentialsInteractor.isUserLogin()).thenReturn(false);

    mPresenter.triggerUpdate();

    verify(updateBookingStatusInteractor).updateBookingStatus(
        bookingStatusListenerCaptor.capture());

    bookingStatusListenerCaptor.getValue().onComplete(null);

    verify(interactor).getBookings(bookingsRequestCaptor.capture());
    bookingsRequestCaptor.getValue().onResponse(mockedTripsProvider);

    verify(view).notifyBookingsHasChanged(mockedTripsProvider, false);
    verify(view).onRequestFinished(any(Boolean.class));
  }

  @Test public void shouldDeleteAppRateCard() {
    when(checkUserCredentialsInteractor.isUserLogin()).thenReturn(true);
    when(mockedTripsProvider.getCurrentSize()).thenReturn(1);
    when(preferencesControllerInterface.getBooleanValue(anyString())).thenReturn(false);

    mPresenter.triggerUpdate();

    verify(updateBookingsInteractor).updateMyTrips(updateBookingsRequestCaptor.capture());
    updateBookingsRequestCaptor.getValue().onResponse(any(Void.class));

    verify(interactor).getBookings(bookingsRequestCaptor.capture());
    bookingsRequestCaptor.getValue().onResponse(mockedTripsProvider);

    verify(mockedTripsProvider).removeRateAppCard();
    verify(view).notifyBookingsHasChanged(mockedTripsProvider, false);
    verify(view).onRequestFinished(any(Boolean.class));
  }

  @Test public void testOpenLoginShouldTrackLocalyticsEvent() {
    mPresenter.openLogin();

    verify(trackerController).trackLocalyticsMyTripsClickedOnLogin();

    verifyNoMoreInteractions(trackerController);
  }

  @Test public void testOpenLoginShouldNavigateToLogin() {
    mPresenter.openLogin();

    verify(navigator).navigateToLogin();

    verifyNoMoreInteractions(navigator);
  }

  @Test public void testSetNavigationTitle() {
    final String title = "Title";
    mPresenter.setNavigationTitle(title);
    verify(navigator).setNavigationTitle(title);

    verifyNoMoreInteractions(navigator);
  }
}
