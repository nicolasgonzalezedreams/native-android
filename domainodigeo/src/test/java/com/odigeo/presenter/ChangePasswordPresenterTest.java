package com.odigeo.presenter;

import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.ChangePasswordInteractor;
import com.odigeo.interactors.LogoutInteractor;
import com.odigeo.presenter.contracts.navigators.AccountPreferencesNavigatorInterface;
import com.odigeo.presenter.contracts.views.ChangePasswordViewInterface;
import com.odigeo.presenter.listeners.OnChangePasswordListener;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ChangePasswordPresenterTest {

  private final String OLD_PASSWORD = "oldPassword1234"; // Current password properly formatted
  private final String OLD_PASSWORD_ERROR_FORMAT = "oldPassword";
  // Current password wrongly formatted
  private final String NEW_PASSWORD = "newPassword1234"; // New password properly formatted
  private final String NEW_PASSWORD_ERROR_FORMAT = "newPassword";
  // New password wrongly formatted
  private final String NEW_PASSWORD_ERROR = "errPassword1234";
  // Repeat password not matching new password

  @Mock private ChangePasswordViewInterface view;
  @Mock private AccountPreferencesNavigatorInterface navigator;
  @Mock private ChangePasswordInteractor changePasswordInteractor;
  @Mock private LogoutInteractor logoutInteractor;
  @Mock private SessionController sessionController;

  @Captor private ArgumentCaptor<OnChangePasswordListener> mOnChangePasswordListener;

  private ChangePasswordPresenter mPresenter;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    mPresenter =
        new ChangePasswordPresenter(view, navigator, changePasswordInteractor, logoutInteractor,
            sessionController);
  }

  @Test public void shouldShowChangingPasswordDialogAndCallInteractor() {
    prepareOnChangePasswordListener();

    verify(view, times(1)).showChangingPasswordDialog();
  }

  @Test public void shouldShowErrorWhenPasswordsAreTheSame() {
    boolean result = mPresenter.validatePasswords(OLD_PASSWORD, OLD_PASSWORD, OLD_PASSWORD);

    assertFalse(result);
    verify(view, times(1)).newPasswordIsTheSame(Boolean.TRUE);
  }

  @Test public void shouldShowErrorWhenNewPasswordsHasNotSameLength() {
    boolean result = mPresenter.validatePasswords(OLD_PASSWORD, NEW_PASSWORD, NEW_PASSWORD_ERROR);

    assertFalse(result);
  }

  @Test public void shouldShowErrorWhenNewPasswordsAreNotEquals() {
    boolean result = mPresenter.validatePasswords(OLD_PASSWORD, NEW_PASSWORD, NEW_PASSWORD_ERROR);

    assertFalse(result);
    verify(view, times(1)).newPasswordMatch(Boolean.FALSE);
  }

  @Test public void shouldShowErrorWhenCurrentPasswordErrorFormat() {
    when(changePasswordInteractor.isSocialNetworkAccount()).thenReturn(Boolean.FALSE);

    boolean result =
        mPresenter.validatePasswords(OLD_PASSWORD_ERROR_FORMAT, NEW_PASSWORD, NEW_PASSWORD);

    assertFalse(result);
    verify(view, times(1)).invalidFormatCurrentPassword();
  }

  @Test public void shouldShowErrorWhenNewPasswordErrorFormat() {
    boolean result = mPresenter.validatePasswords(OLD_PASSWORD, NEW_PASSWORD_ERROR_FORMAT,
        NEW_PASSWORD_ERROR_FORMAT);

    assertFalse(result);
    verify(view, times(1)).invalidFormatNewPassword();
  }

  @Test public void shouldChangePassword() {
    boolean result = mPresenter.validatePasswords(OLD_PASSWORD, NEW_PASSWORD, NEW_PASSWORD);

    assertTrue(result);
  }

  @Test public void shouldCallInteractorWhenCheckingAccountType() {
    when(changePasswordInteractor.isSocialNetworkAccount()).thenReturn(Boolean.FALSE);

    mPresenter.checkAccountType();

    verify(view, times(1)).changeCurrentPasswordVisibility(Boolean.TRUE);
  }

  @Test public void shouldNavigateToLoginWhenClickOnAuth() {
    mPresenter.onAuthOkClicked();

    verify(navigator, times(1)).navigateToLogin();
  }

  @Test public void shouldNavigateToAccountPreferencesOnSuccess() {
    prepareOnChangePasswordListener();

    mOnChangePasswordListener.getValue().onSuccess();

    verify(view, times(1)).passwordChanged(Boolean.TRUE);
    verify(navigator, times(1)).showAccountPreferencesView();
  }

  @Test public void shouldShowErrorChangePasswordOnError() {
    prepareOnChangePasswordListener();

    mOnChangePasswordListener.getValue().onError();

    verify(view, times(1)).passwordChanged(Boolean.FALSE);
  }

  @Test public void shouldShowErrorChangePasswordOnCurrentPasswordWrong() {
    prepareOnChangePasswordListener();

    mOnChangePasswordListener.getValue().onCurrentPasswordWrong();

    verify(view, times(1)).invalidConcurrentPassword();
    verify(view, times(1)).passwordChanged(Boolean.FALSE);
  }

  @Test public void shouldShowErrorChangePasswordOnPasswordDoesNotMatch() {
    prepareOnChangePasswordListener();

    mOnChangePasswordListener.getValue().onPasswordDoesNotMatch();

    verify(view, times(1)).newPasswordMatch(Boolean.FALSE);
    verify(view, times(1)).passwordChanged(Boolean.FALSE);
  }

  @Test public void shouldShowErrorChangePasswordOnCurrentPasswordInvalidFormat() {
    prepareOnChangePasswordListener();

    mOnChangePasswordListener.getValue().onCurrentPasswordInvalidFormat();

    verify(view, times(1)).invalidFormatCurrentPassword();
    verify(view, times(1)).passwordChanged(Boolean.FALSE);
  }

  @Test public void shouldShowErrorChangePasswordOnNewPasswordInvalidFormat() {
    prepareOnChangePasswordListener();

    mOnChangePasswordListener.getValue().onNewPasswordInvalidFormat();

    verify(view, times(1)).invalidFormatNewPassword();
    verify(view, times(1)).passwordChanged(Boolean.FALSE);
  }

  @Test public void shouldShowErrorChangePasswordOnRepeatPasswordInvalidFormat() {
    prepareOnChangePasswordListener();

    mOnChangePasswordListener.getValue().onRepeatPasswordInvalidFormat();

    verify(view, times(1)).invalidFormatRepeatPassword();
    verify(view, times(1)).passwordChanged(Boolean.FALSE);
  }

  @Test public void shouldMakeLogoutOnAuthError() {
    prepareOnChangePasswordListener();

    mOnChangePasswordListener.getValue().onAuthError();

    verify(logoutInteractor, times(1)).logout(any(OnRequestDataListener.class));
  }

  @Test public void shouldShowAuthErrorOnResponse() {
    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        ((OnRequestDataListener) invocation.getArguments()[0]).onResponse(Boolean.FALSE);
        return null;
      }
    }).when(logoutInteractor).logout(any(OnRequestDataListener.class));

    prepareOnChangePasswordListener();

    mOnChangePasswordListener.getValue().onAuthError();

    verify(view, times(1)).showAuthError();
    verify(sessionController, times(1)).saveLogoutWasMade(Boolean.TRUE);
  }

  @Test public void shouldShowErrorOnLogoutError() {
    final MslError mslError = MslError.AUTH_000;
    final String errorMessage = "errorMessage";
    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        ((OnRequestDataListener) invocation.getArguments()[0]).onError(mslError, errorMessage);
        return null;
      }
    }).when(logoutInteractor).logout(any(OnRequestDataListener.class));

    prepareOnChangePasswordListener();

    mOnChangePasswordListener.getValue().onAuthError();

    verify(view, times(1)).onLogoutError(mslError, errorMessage);
  }

  private void prepareOnChangePasswordListener() {
    mPresenter.changePasswordAction(OLD_PASSWORD, NEW_PASSWORD, NEW_PASSWORD);

    verify(changePasswordInteractor, times(1)).changePassword(eq(OLD_PASSWORD), eq(NEW_PASSWORD),
        eq(NEW_PASSWORD), mOnChangePasswordListener.capture());
  }
}
