package com.odigeo.presenter;

import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.PricingBreakdown;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItemType;
import com.odigeo.data.entity.shoppingCart.ShoppingCart;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.data.entity.userData.Membership;
import com.odigeo.interactors.MembershipInteractor;
import com.odigeo.interactors.TotalPriceCalculatorInteractor;
import com.odigeo.presenter.contracts.views.PriceBreakdownWidgetInterface;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.odigeo.test.mock.MocksProvider.provideMembershipMocks;
import static com.odigeo.test.mock.MocksProvider.provideTravellersMocks;
import static com.odigeo.test.mock.MocksProvider.providesInsurancesMocks;
import static com.odigeo.test.mock.MocksProvider.providesPaymentMocks;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class PriceBreakdownWidgetPresenterTest {

  @Mock private PriceBreakdownWidgetInterface priceBreakdownWidget;
  @Mock private ShoppingCart shoppingCart;
  @Mock private PricingBreakdown pricingBreakdown;
  @Mock private MembershipInteractor membershipInteractor;
  @Mock private TotalPriceCalculatorInteractor totalPriceCalculatorInteractor;

  private PriceBreakdownWidgetPresenter priceBreakdownWidgetPresenter;

  private static final String FIRST_NAME = "Name";
  private static final String WRONG_FIRST_NAME = "Nome";
  private static final String LAST_NAME = "Lastname";

  @Before public void setUp() throws Exception {
    priceBreakdownWidgetPresenter =
        new PriceBreakdownWidgetPresenter(priceBreakdownWidget, membershipInteractor,
            totalPriceCalculatorInteractor);
    priceBreakdownWidgetPresenter.configurePresenter(Step.PAYMENT, shoppingCart, pricingBreakdown);
  }

  @Test public void shouldReturnPricingBreakdownItems() {
    priceBreakdownWidgetPresenter.setPricingBreakdown(
        providesPaymentMocks().providePricingBreakdown());
    priceBreakdownWidgetPresenter.setCollectionMethodType(CollectionMethodType.CREDITCARD);
    priceBreakdownWidgetPresenter.setShoppingCartCollectionOption(
        providesPaymentMocks().provideCreditCardShoppingCartCollectionOption("AE"));
    Assert.assertNotEquals(priceBreakdownWidgetPresenter.getPricingBreakdownItems(), null);
  }

  @Test public void shouldReturnTotalPrice() {
    priceBreakdownWidgetPresenter.setPricingBreakdown(
        providesPaymentMocks().providePricingBreakdown());
    Assert.assertNotEquals(priceBreakdownWidgetPresenter.getTotalPrice(), null);
  }

  @Test public void shouldReturnPriceItemAmountWithType() {
    priceBreakdownWidgetPresenter.setPricingBreakdown(
        providesPaymentMocks().providePricingBreakdown());
    when(totalPriceCalculatorInteractor.getPricingBreakdownItems(any(PricingBreakdown.class), any
            (BigDecimal.class), any(Step.class))).thenReturn(providesPaymentMocks()
        .providePricingBreakdownItems());
    Assert.assertNotEquals(priceBreakdownWidgetPresenter.getPriceItemAmountWithType(
        PricingBreakdownItemType.PAYMENT_METHOD_PRICE_FULLPRICE, 0,
        providesInsurancesMocks().provideInsuranceShoppingItem()), null);
  }

  @Test public void shouldShowTaxRefundInfo() {
    when(pricingBreakdown.getTaxRefund()).thenReturn(56.4);
    priceBreakdownWidgetPresenter.onTaxRefundableInfoCheck();
    verify(priceBreakdownWidget).showTaxRefundableInfo(anyDouble());
  }

  @Test public void shouldNotShowTaxRefundInfo() {
    when(pricingBreakdown.getTaxRefund()).thenReturn(null);
    priceBreakdownWidgetPresenter.onTaxRefundableInfoCheck();
    verify(priceBreakdownWidget, never()).showTaxRefundableInfo(anyDouble());
  }

  @Test public void shouldApplyMembershipPerksWithNoTravellers() {
    when(shoppingCart.getTravellers()).thenReturn(new ArrayList<Traveller>());
    assertTrue(priceBreakdownWidgetPresenter.membershipIsApplied());
  }

  @Test public void shouldNotApplyMembershipPerksWithTravellerNotMember() {
    when(shoppingCart.getTravellers()).thenReturn(
        provideTravellersMocks().provideMockedTravellers(FIRST_NAME, LAST_NAME, ""));
    when(membershipInteractor.getMembershipForCurrentMarket()).thenReturn(
        provideMembershipMocks().provideMockedMembership(WRONG_FIRST_NAME, LAST_NAME));
    assertFalse(priceBreakdownWidgetPresenter.membershipIsApplied());
  }

  @Test public void shouldApplyMembershipPerksWithTravellerThatIsMember() {
    when(shoppingCart.getTravellers()).thenReturn(
        provideTravellersMocks().provideMockedTravellers(FIRST_NAME, LAST_NAME, ""));
    when(membershipInteractor.getMembershipForCurrentMarket()).thenReturn(
        provideMembershipMocks().provideMockedMembership(FIRST_NAME, LAST_NAME));
    assertTrue(priceBreakdownWidgetPresenter.membershipIsApplied());
  }
}