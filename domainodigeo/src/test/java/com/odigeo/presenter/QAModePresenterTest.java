package com.odigeo.presenter;

import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.presenter.contracts.navigators.QAModeNavigator;
import com.odigeo.presenter.contracts.views.QAModeView;
import com.odigeo.presenter.model.QAModeUrlModel;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class QAModePresenterTest {

  private static final String PROD_URL = "http://msl.com";
  private static final String QA_URL = "http://qa.msl.com";

  @Mock private QAModeView view;
  @Mock private QAModeNavigator navigator;
  @Mock private PreferencesControllerInterface preferences;

  private QAModePresenter presenter;

  private final List<QAModeUrlModel> qaModeUrlModels = new ArrayList<QAModeUrlModel>() {{
    add(new QAModeUrlModel(QA_URL, true));
    add(new QAModeUrlModel(PROD_URL, false));
  }};

  @Before public void setup() {
    presenter = new QAModePresenter(view, navigator, preferences);
    when(preferences.getQAModeUrls()).thenReturn(qaModeUrlModels);
    when(preferences.saveQAModeUrl(any(QAModeUrlModel.class)))
        .thenReturn(qaModeUrlModels);
    when(preferences.editQAModeUrl(any(QAModeUrlModel.class), any(QAModeUrlModel.class)))
        .thenReturn(
        qaModeUrlModels);
    when(preferences.deleteQAModeUrl(any(QAModeUrlModel.class)))
        .thenReturn(qaModeUrlModels);
  }

  @Test public void testWhenGetQaModeCallsShowOnView() {
    presenter.getQAModeUrls();

    verify(view).showQAModeUrls(qaModeUrlModels);
    verifyNoMoreInteractions(view);
  }

  @Test public void testWhenGetQaModeDisablesQAModeUrlDeletion() {
    final List<QAModeUrlModel> qaModeUrlModels = new ArrayList<QAModeUrlModel>() {{
      add(new QAModeUrlModel(QA_URL, true));
    }};

    when(preferences.getQAModeUrls()).thenReturn(qaModeUrlModels);

    presenter.getQAModeUrls();

    InOrder inOrder = inOrder(view);
    inOrder.verify(view).disableQAModeUrlDeletion();
    inOrder.verify(view).showQAModeUrls(qaModeUrlModels);
    inOrder.verifyNoMoreInteractions();
  }

  @Test public void testAddQAModeUrlEmptyUrlShowsMessage() {
    presenter.onAddQAModeUrl(null);

    verify(view).showEmptyUrlMessage();
    verifyNoMoreInteractions(view);
  }

  @Test public void testAddQAModeUrl() {
    when(preferences.saveQAModeUrl(any(QAModeUrlModel.class))).thenReturn(qaModeUrlModels);

    presenter.onAddQAModeUrl(PROD_URL);

    InOrder inOrder = inOrder(view, preferences);
    inOrder.verify(preferences).saveQAModeUrl(any(QAModeUrlModel.class));
    inOrder.verify(view).showAddQAModeUrlSuccess(qaModeUrlModels);
    inOrder.verify(view).enableQAModeUrlDeletion();
    inOrder.verifyNoMoreInteractions();
  }

  @Test public void testRemoveQAModeUrlCallsShowQAUrlsAndEnablesUrlDeletion() {
    final QAModeUrlModel toBeDeleted = mock(QAModeUrlModel.class);

    when(preferences.deleteQAModeUrl(any(QAModeUrlModel.class))).thenReturn(qaModeUrlModels);

    presenter.onRemoveQAModeUrl(toBeDeleted);

    InOrder inOrder = inOrder(view, preferences);
    inOrder.verify(preferences).deleteQAModeUrl(toBeDeleted);
    inOrder.verify(view).enableQAModeUrlDeletion();
    inOrder.verify(view).showQAModeUrls(qaModeUrlModels);
    inOrder.verifyNoMoreInteractions();
  }

  @Test public void testRemoveQAModeUrlCallsShowQAUrlsAndDisablesUrlDeletion() {
    final List<QAModeUrlModel> qaModeUrlModels = new ArrayList<QAModeUrlModel>() {{
      add(new QAModeUrlModel(QA_URL, true));
    }};
    final QAModeUrlModel toBeDeleted = mock(QAModeUrlModel.class);

    when(preferences.deleteQAModeUrl(any(QAModeUrlModel.class))).thenReturn(qaModeUrlModels);

    presenter.onRemoveQAModeUrl(toBeDeleted);

    InOrder inOrder = inOrder(view, preferences);
    inOrder.verify(preferences).deleteQAModeUrl(toBeDeleted);
    inOrder.verify(view).disableQAModeUrlDeletion();
    inOrder.verify(view).showQAModeUrls(qaModeUrlModels);
    inOrder.verifyNoMoreInteractions();
  }

  @Test public void testEditQAModeUrl() {
    final QAModeUrlModel qaModeUrlModel = mock(QAModeUrlModel.class);

    presenter.onEditQAModeUrl(qaModeUrlModel, QA_URL);

    InOrder inOrder = inOrder(view, preferences);
    inOrder.verify(preferences).editQAModeUrl(any(QAModeUrlModel.class), any(QAModeUrlModel.class));
    inOrder.verify(view).showQAModeUrls(qaModeUrlModels);
    inOrder.verifyNoMoreInteractions();
  }

  @Test public void testSelectQAModeUrl() {
    final QAModeUrlModel qaModeUrlModel = mock(QAModeUrlModel.class);
    presenter.onSelectQAModeUrl(qaModeUrlModel);

    InOrder inOrder = inOrder(view, preferences, navigator);
    inOrder.verify(preferences).editQAModeUrl(any(QAModeUrlModel.class), any(QAModeUrlModel.class));
    inOrder.verify(view).showQAModeUrlSelected(qaModeUrlModels, qaModeUrlModel);
    inOrder.verify(navigator).navigateBackWithSelectedQAModeUrl(qaModeUrlModel);
    inOrder.verifyNoMoreInteractions();
  }
}
