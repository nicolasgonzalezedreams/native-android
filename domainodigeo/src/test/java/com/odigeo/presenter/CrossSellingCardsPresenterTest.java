package com.odigeo.presenter;

import com.odigeo.data.download.DownloadController;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.xsell.CrossSelling;
import com.odigeo.interactors.ArrivalGuidesCardsInteractor;
import com.odigeo.interactors.GetCrossSellingForBookingInteractor;
import com.odigeo.presenter.contracts.mappers.xsell.CrossSellingCardMapper;
import com.odigeo.presenter.contracts.navigators.MyTripDetailsNavigatorInterface;
import com.odigeo.presenter.contracts.views.CrossSellingCardsView;
import com.odigeo.presenter.model.CrossSellingCard;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.odigeo.presenter.model.CrossSellingType.TYPE_CARS;
import static com.odigeo.presenter.model.CrossSellingType.TYPE_GROUND;
import static com.odigeo.presenter.model.CrossSellingType.TYPE_GUIDE;
import static com.odigeo.presenter.model.CrossSellingType.TYPE_HOTEL;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class CrossSellingCardsPresenterTest {

  private static final String GUIDE_URL = "http://edreams.com/guide.pdf";
  private static final String GROUND_URL = "http://edreams.com/ground";
  private static final String IATA = "IATA";

  @Mock private CrossSellingCardsView crossSellingCardsView;
  @Mock private GetCrossSellingForBookingInteractor getCrossSellingForBookingInteractor;
  @Mock private CrossSellingCardMapper crossSellingCardMapper;
  @Mock private DownloadController downloadController;
  @Mock private ArrivalGuidesCardsInteractor arrivalGuidesCardsInteractor;
  @Mock private MyTripDetailsNavigatorInterface navigator;
  @Mock private Booking booking;
  @Mock private CrossSellingCard crossSellingCard;

  private CrossSellingCardsPresenter presenter;

  @Before public void setup() {
    presenter =
        new CrossSellingCardsPresenter(crossSellingCardsView, getCrossSellingForBookingInteractor,
            crossSellingCardMapper, downloadController, arrivalGuidesCardsInteractor, navigator);
  }

  @Test public void testBuildXSellCardsForBooking() {
    List<CrossSelling> crossSellings = new ArrayList<CrossSelling>() {{
      add(new CrossSelling(booking, false, "Label", "Title", TYPE_CARS, "Subtitle"));
      add(new CrossSelling(booking, false, "Label", "Title", TYPE_GROUND, "Subtitle"));
      add(new CrossSelling(booking, false, "Label", "Title", TYPE_GUIDE, "Subtitle"));
      add(new CrossSelling(booking, false, "Label", "Title", TYPE_HOTEL, "Subtitle"));
    }};
    when(getCrossSellingForBookingInteractor.getCrossSellingForBooking(booking)).thenReturn(
        crossSellings);

    presenter.buildXsellCards(booking);

    verify(crossSellingCardMapper).transform(crossSellings);
    verifyNoMoreInteractions(crossSellingCardMapper);
  }

  @Test public void testBuildXSellCardsForBookingTracksGroundTransportationVisibility() {
    List<CrossSelling> crossSellings = new ArrayList<CrossSelling>() {{
      add(new CrossSelling(booking, false, "Label", "Title", TYPE_CARS, "Subtitle"));
      add(new CrossSelling(booking, false, "Label", "Title", TYPE_GROUND, "Subtitle"));
    }};
    when(getCrossSellingForBookingInteractor.getCrossSellingForBooking(booking)).thenReturn(
        crossSellings);

    presenter.buildXsellCards(booking);

    verify(crossSellingCardsView).trackGroundTransportationCardAppearance();
    verifyNoMoreInteractions(crossSellingCardsView);
  }

  @Test public void testOnHotelsCrossSellCardSelectedOpensHotels() {
    when(crossSellingCard.getType()).thenReturn(TYPE_HOTEL);
    when(crossSellingCard.getBooking()).thenReturn(booking);

    presenter.onCrossSellingCardSelected(crossSellingCard);

    verify(crossSellingCardsView).openHotels(booking);
    verifyNoMoreInteractions(crossSellingCardsView);
  }

  @Test public void testOnCarsCrossSellCardSelectedOpensCars() {
    when(crossSellingCard.getType()).thenReturn(TYPE_CARS);
    when(crossSellingCard.getBooking()).thenReturn(booking);

    presenter.onCrossSellingCardSelected(crossSellingCard);

    verify(crossSellingCardsView).openCars(booking);
    verifyNoMoreInteractions(crossSellingCardsView);
  }

  @Test public void testOnGroundTransportationCrossSellCardSelectedOpensGroundTransportation() {
    when(crossSellingCard.getType()).thenReturn(TYPE_GROUND);
    when(crossSellingCard.getBooking()).thenReturn(booking);

    presenter.onCrossSellingCardSelected(crossSellingCard);

    verify(crossSellingCardsView).openGroundTransportation(booking);
    verifyNoMoreInteractions(crossSellingCardsView);
  }

  @Test
  public void testOnGuideCrossSellCardSelectedOpensGuideIfPermissionAllowedAndIsAlreadyDownloaded() {
    when(booking.getArrivalIataCode()).thenReturn(IATA);
    when(crossSellingCard.getType()).thenReturn(TYPE_GUIDE);
    when(crossSellingCard.getBooking()).thenReturn(booking);
    when(crossSellingCardsView.checkStoragePermission()).thenReturn(true);
    when(arrivalGuidesCardsInteractor.isGuideDownloaded(IATA)).thenReturn(true);

    presenter.onCrossSellingCardSelected(crossSellingCard);

    InOrder inOrder = inOrder(crossSellingCardsView, downloadController);
    inOrder.verify(crossSellingCardsView).hideGuideDownloading();
    inOrder.verify(crossSellingCardsView).openCityGuide(booking);
    inOrder.verify(downloadController).unsubscribeDownloader();
    inOrder.verifyNoMoreInteractions();
  }

  @Test
  public void testOnGuideCrossSellCardSelectedDownloadsGuideIfPermissionAllowedAndDownloadInProgress() {
    when(booking.getArrivalIataCode()).thenReturn(IATA);
    when(crossSellingCard.getType()).thenReturn(TYPE_GUIDE);
    when(crossSellingCard.getBooking()).thenReturn(booking);
    when(crossSellingCardsView.checkStoragePermission()).thenReturn(true);
    when(arrivalGuidesCardsInteractor.isGuideDownloaded(IATA)).thenReturn(false);
    when(downloadController.isDownloadRunning()).thenReturn(true);

    presenter.onCrossSellingCardSelected(crossSellingCard);

    InOrder inOrder =
        inOrder(crossSellingCardsView, arrivalGuidesCardsInteractor, downloadController);
    inOrder.verify(crossSellingCardsView).checkStoragePermission();
    inOrder.verify(downloadController).subscribeDownloader(presenter);
    inOrder.verify(arrivalGuidesCardsInteractor).isGuideDownloaded(booking.getArrivalIataCode());
    inOrder.verify(downloadController).isDownloadRunning();
    inOrder.verify(crossSellingCardsView).showGuideDownloading();
    inOrder.verifyNoMoreInteractions();
  }

  @Test
  public void testOnGuideCrossSellCardSelectedDownloadsGuideIfPermissionAllowedAndNotDownloaded() {
    when(booking.getArrivalIataCode()).thenReturn(IATA);
    when(crossSellingCard.getType()).thenReturn(TYPE_GUIDE);
    when(crossSellingCard.getBooking()).thenReturn(booking);
    when(crossSellingCardsView.checkStoragePermission()).thenReturn(true);
    when(arrivalGuidesCardsInteractor.isGuideDownloaded(IATA)).thenReturn(false);
    when(arrivalGuidesCardsInteractor.getGuideUrl(anyLong())).thenReturn(GUIDE_URL);
    when(downloadController.isDownloadRunning()).thenReturn(false);

    presenter.onCrossSellingCardSelected(crossSellingCard);

    InOrder inOrder =
        inOrder(crossSellingCardsView, arrivalGuidesCardsInteractor, downloadController);
    inOrder.verify(crossSellingCardsView).checkStoragePermission();
    inOrder.verify(downloadController).subscribeDownloader(presenter);
    inOrder.verify(arrivalGuidesCardsInteractor).isGuideDownloaded(booking.getArrivalIataCode());
    inOrder.verify(downloadController).isDownloadRunning();
    inOrder.verify(arrivalGuidesCardsInteractor).getGuideUrl(booking.getArrivalGeoNodeId());
    inOrder.verify(downloadController).startDownload(GUIDE_URL, booking.getArrivalIataCode());
    inOrder.verify(crossSellingCardsView).showGuideDownloading();
    inOrder.verifyNoMoreInteractions();
  }

  @Test public void testDontDownloadGuideIfNoStoragePermissionAllowed() {
    when(booking.getArrivalIataCode()).thenReturn(IATA);
    when(crossSellingCard.getType()).thenReturn(TYPE_GUIDE);
    when(crossSellingCard.getBooking()).thenReturn(booking);
    when(crossSellingCardsView.checkStoragePermission()).thenReturn(false);

    presenter.onCrossSellingCardSelected(crossSellingCard);

    verify(crossSellingCardsView).checkStoragePermission();
    verifyNoMoreInteractions(crossSellingCardsView);
  }

  @Test public void testOnNoConnectionShowsNoConnectionDialog() {
    presenter.onNoConnectionError();

    verify(crossSellingCardsView).showNoConnectionDialog();
    verifyNoMoreInteractions(crossSellingCardsView);
  }

  @Test public void testOnDownloadFailedErrorShowsDownloadFailedDialog() {
    presenter.onDownloadFailedError();

    verify(crossSellingCardsView).showDownloadFailedDialog();
    verifyNoMoreInteractions(crossSellingCardsView);
  }

  @Test public void testOnInsufficientSpaceShowsInsufficientSpaceDialog() {
    presenter.onInsufficientSpaceError();

    verify(crossSellingCardsView).showInsufficientSpaceDialog();
    verifyNoMoreInteractions(crossSellingCardsView);
  }

  @Test public void testHasArrivalGuideCallsInteractor() {
    final long arrivalGeoNodeId = 1L;

    when(arrivalGuidesCardsInteractor.hasArrivalGuide(arrivalGeoNodeId)).thenReturn(true);

    final boolean hasArrivalGuide = presenter.hasArrivalGuide(arrivalGeoNodeId);

    assertThat(hasArrivalGuide, is(true));

    verify(arrivalGuidesCardsInteractor).hasArrivalGuide(arrivalGeoNodeId);
    verifyNoMoreInteractions(arrivalGuidesCardsInteractor);
  }

  @Test public void testNavigateToGroundTransportation() {
    presenter.navigateToGroundTransportation(GROUND_URL);

    verify(navigator).navigateToGroundTransportation(GROUND_URL);
    verifyNoMoreInteractions(arrivalGuidesCardsInteractor);
  }
}
