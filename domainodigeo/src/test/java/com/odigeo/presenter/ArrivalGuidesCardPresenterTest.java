package com.odigeo.presenter;

import com.odigeo.data.download.DownloadController;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.interactors.ArrivalGuidesCardsInteractor;
import com.odigeo.presenter.contracts.navigators.MyTripDetailsNavigatorInterface;
import com.odigeo.presenter.contracts.views.ArrivalGuidesProviderViewInterface;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class ArrivalGuidesCardPresenterTest {

  @Mock private ArrivalGuidesProviderViewInterface mView;
  @Mock private MyTripDetailsNavigatorInterface mNavigator;
  @Mock private ArrivalGuidesCardsInteractor mArrivalGuidesCardsInteractor;
  @Mock private DownloadController mDownloadManagerController;
  @Mock private TrackerControllerInterface mTracker;

  @Before public void setUp() {
    mView = Mockito.mock(ArrivalGuidesProviderViewInterface.class);
    mNavigator = Mockito.mock(MyTripDetailsNavigatorInterface.class);
    mArrivalGuidesCardsInteractor = Mockito.mock(ArrivalGuidesCardsInteractor.class);
    mDownloadManagerController = Mockito.mock(DownloadController.class);
    mTracker = Mockito.mock(TrackerControllerInterface.class);
  }

  @Test public void validateInitCardShowGuideDownloadedState() {

    when(mDownloadManagerController.isDownloadRunning()).thenReturn(false);
    when(mArrivalGuidesCardsInteractor.isGuideDownloaded("MAD")).thenReturn(true);

    ArrivalGuidesCardPresenter arrivalGuidesCardPresenter =
        new ArrivalGuidesCardPresenter(mView, mArrivalGuidesCardsInteractor, mNavigator,
            mDownloadManagerController, mTracker);
    arrivalGuidesCardPresenter.initCard(getMockedBooking(), "en");

    verify(mDownloadManagerController, times(1)).isDownloadRunning();
    verify(mArrivalGuidesCardsInteractor, times(1)).isGuideDownloaded("MAD");
    verify(mView, times(1)).showGuideDownloadedState();
  }

  @Test public void validateInitCardShowDownloadGuideState() {

    when(mDownloadManagerController.isDownloadRunning()).thenReturn(false);
    when(mArrivalGuidesCardsInteractor.isGuideDownloaded("MAD")).thenReturn(false);

    ArrivalGuidesCardPresenter arrivalGuidesCardPresenter =
        new ArrivalGuidesCardPresenter(mView, mArrivalGuidesCardsInteractor, mNavigator,
            mDownloadManagerController, mTracker);
    arrivalGuidesCardPresenter.initCard(getMockedBooking(), "en");

    verify(mDownloadManagerController, times(1)).isDownloadRunning();
    verify(mArrivalGuidesCardsInteractor, times(1)).isGuideDownloaded("MAD");
    verify(mView, times(1)).showDownloadGuideState();
  }

  @Test public void validateInitCardShowDownloadingGuideState() {

    when(mDownloadManagerController.isDownloadRunning()).thenReturn(true);
    when(mArrivalGuidesCardsInteractor.isGuideDownloaded("MAD")).thenReturn(true);

    ArrivalGuidesCardPresenter arrivalGuidesCardPresenter =
        new ArrivalGuidesCardPresenter(mView, mArrivalGuidesCardsInteractor, mNavigator,
            mDownloadManagerController, mTracker);
    arrivalGuidesCardPresenter.initCard(getMockedBooking(), "en");

    verify(mDownloadManagerController, times(1)).isDownloadRunning();
    verify(mArrivalGuidesCardsInteractor, times(1)).isGuideDownloaded("MAD");
    verify(mView, times(1)).showDownloadingGuideState();
  }

  @Test public void validateInitCardShowDownloadGuideStateTwo() {

    when(mDownloadManagerController.isDownloadRunning()).thenReturn(true);
    when(mArrivalGuidesCardsInteractor.isGuideDownloaded("MAD")).thenReturn(false);

    ArrivalGuidesCardPresenter arrivalGuidesCardPresenter =
        new ArrivalGuidesCardPresenter(mView, mArrivalGuidesCardsInteractor, mNavigator,
            mDownloadManagerController, mTracker);
    arrivalGuidesCardPresenter.initCard(getMockedBooking(), "en");

    verify(mDownloadManagerController, times(1)).isDownloadRunning();
    verify(mArrivalGuidesCardsInteractor, times(1)).isGuideDownloaded("MAD");
    verify(mView, times(1)).showDownloadGuideState();
  }

  @Test public void validateInitCardShowGuideInDifferentLanguageMessage() {

    when(mArrivalGuidesCardsInteractor.isSameGuideLanguage(1, "en")).thenReturn(false);

    ArrivalGuidesCardPresenter arrivalGuidesCardPresenter =
        new ArrivalGuidesCardPresenter(mView, mArrivalGuidesCardsInteractor, mNavigator,
            mDownloadManagerController, mTracker);
    arrivalGuidesCardPresenter.initCard(getMockedBooking(), "es");

    verify(mArrivalGuidesCardsInteractor, times(1)).isSameGuideLanguage(1, "es");
    verify(mView, times(1)).showGuideInDifferentLanguageMessage();
  }

  private Booking getMockedBooking() {
    LocationBooking to = new LocationBooking(1, "MAD", "", "", "", "", "", "", "", "");

    ArrayList<Section> sectionsList = new ArrayList<>();
    Section section =
        new Section(1, "", "", 1, "", "", "", "", 1, "", 1, "", "", null, null, null, null, to);
    sectionsList.add(section);

    ArrayList<Segment> segments = new ArrayList<>();
    Segment segment = new Segment(1, 1, 1, 1, 1, null, sectionsList);
    segments.add(segment);

    return new Booking("", 1, 1, "", "", 1, "", "", 1, "", 1, 1, "", null, null, null, segments,
        false, false, false);
  }
}
