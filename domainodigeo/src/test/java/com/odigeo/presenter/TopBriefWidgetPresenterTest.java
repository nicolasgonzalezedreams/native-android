package com.odigeo.presenter;

import com.odigeo.interactors.MembershipInteractor;
import com.odigeo.presenter.contracts.views.TopBriefViewInterface;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TopBriefWidgetPresenterTest {

  @Mock private MembershipInteractor membershipInteractor;
  @Mock private TopBriefViewInterface view;

  private TopBriefWidgetPresenter topBriefWidgetPresenter;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    topBriefWidgetPresenter = new TopBriefWidgetPresenter(view, membershipInteractor);
  }

  @Test public void shouldShowFullPrice() {
    when(membershipInteractor.isMemberForCurrentMarket()).thenReturn(false);
    when(view.getMembershipApplied()).thenReturn(false);

    topBriefWidgetPresenter.configureTotalPrice();

    verify(view).showFullPrice();
  }

  @Test public void shouldShowMembershipWithDiscount() {
    when(membershipInteractor.isMemberForCurrentMarket()).thenReturn(true);
    when(view.getMembershipApplied()).thenReturn(false);

    topBriefWidgetPresenter.configureTotalPrice();

    verify(view).showMembershipWithNoDiscount();
  }

  @Test public void shouldShowMembershipWithNoDiscount() {
    when(membershipInteractor.isMemberForCurrentMarket()).thenReturn(true);
    when(view.getMembershipApplied()).thenReturn(true);

    topBriefWidgetPresenter.configureTotalPrice();

    verify(view).showMembershipWithDiscount();
  }
}
