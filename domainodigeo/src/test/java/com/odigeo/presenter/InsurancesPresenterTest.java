package com.odigeo.presenter;

import com.odigeo.data.entity.shoppingCart.AvailableProductsResponse;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.FlowConfigurationResponse;
import com.odigeo.interactors.AddProductsToShoppingCartInteractor;
import com.odigeo.interactors.MembershipInteractor;
import com.odigeo.interactors.RemoveProductsFromShoppingCartInteractor;
import com.odigeo.interactors.TotalPriceCalculatorInteractor;
import com.odigeo.presenter.contracts.navigators.InsurancesNavigatorInterface;
import com.odigeo.presenter.contracts.views.InsurancesViewInterface;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.odigeo.test.mock.MocksProvider.providesInsurancesMocks;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class InsurancesPresenterTest {

  @Mock private InsurancesViewInterface mView;
  @Mock private InsurancesNavigatorInterface mNavigator;
  @Mock private AddProductsToShoppingCartInteractor mAddProductsToShoppingCartInteractor;
  @Mock private RemoveProductsFromShoppingCartInteractor mRemoveProductsFromShoppingCartInteractor;
  @Mock private MembershipInteractor membershipInteractor;
  @Mock private TotalPriceCalculatorInteractor totalPriceCalculatorInteractor;

  private InsurancesPresenter mPresenter;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    mPresenter = new InsurancesPresenter(mView, mNavigator, mAddProductsToShoppingCartInteractor,
        mRemoveProductsFromShoppingCartInteractor, membershipInteractor,
        totalPriceCalculatorInteractor);
  }

  @Test public void shouldShowInsurancesWithOutputSelected() {
    AvailableProductsResponse availableProductsResponse =
        providesInsurancesMocks().provideAvailableProductResponse();
    CreateShoppingCartResponse createShoppingCartResponse =
        providesInsurancesMocks().provideCreateShoppingCartResponseWithoutInsurancesShoppinItem();
    FlowConfigurationResponse flowConfigurationResponse =
        providesInsurancesMocks().provideFlowConfigurationResponse();

    mPresenter.initializePresenter(availableProductsResponse, createShoppingCartResponse,
        flowConfigurationResponse, 0.0, Locale.ENGLISH, null);

    verify(mView, atLeastOnce()).updateInsurancesList(availableProductsResponse,
        createShoppingCartResponse.getShoppingCart().getTravellers().size());
    verify(mView, atMost(1)).setInsuranceSelected(anyString());
    verify(mView, atMost(1)).setRecommendedInsurance(anyString());
  }

  @Test public void shouldShowInsurancesWithoutOutputSelected() {
    AvailableProductsResponse availableProductsResponse =
        providesInsurancesMocks().provideAvailableProductResponse();
    CreateShoppingCartResponse createShoppingCartResponse =
        providesInsurancesMocks().provideCreateShoppingCartResponseWithoutInsurancesShoppinItem();
    FlowConfigurationResponse flowConfigurationResponse =
        providesInsurancesMocks().provideNonOutputFLowConfigurationResponse();

    mPresenter.initializePresenter(availableProductsResponse, createShoppingCartResponse,
        flowConfigurationResponse, 0.0, Locale.ENGLISH, null);

    verify(mView, atLeastOnce()).updateInsurancesList(availableProductsResponse,
        createShoppingCartResponse.getShoppingCart().getTravellers().size());
    verify(mView, never()).setInsuranceSelected(anyString());
  }

  @Test public void shouldShowInsurancesWithShoppingItemStored() {
    AvailableProductsResponse availableProductsResponse =
        providesInsurancesMocks().provideAvailableProductResponse();
    CreateShoppingCartResponse createShoppingCartResponse =
        providesInsurancesMocks().provideCreateShoppingCartResponse();
    FlowConfigurationResponse flowConfigurationResponse =
        providesInsurancesMocks().provideNonOutputFLowConfigurationResponse();

    mPresenter.initializePresenter(availableProductsResponse, createShoppingCartResponse,
        flowConfigurationResponse, 0.0, Locale.ENGLISH, null);

    verify(mView, atLeastOnce()).updateInsurancesList(availableProductsResponse,
        createShoppingCartResponse.getShoppingCart().getTravellers().size());
    verify(mView, atMost(1)).setInsuranceSelected(anyString());
  }

  @Test public void shouldProcessInsuranceSelection() {
    AvailableProductsResponse availableProductsResponse =
        providesInsurancesMocks().provideAvailableProductResponse();
    CreateShoppingCartResponse createShoppingCartResponse =
        providesInsurancesMocks().provideCreateShoppingCartResponseWithoutInsurancesShoppinItem();
    FlowConfigurationResponse flowConfigurationResponse =
        providesInsurancesMocks().provideFlowConfigurationResponse();

    mPresenter.initializePresenter(availableProductsResponse, createShoppingCartResponse,
        flowConfigurationResponse, 0.0, Locale.ENGLISH, null);
    mPresenter.processInsurancesSelection(providesInsurancesMocks().provideListInsuranceOffer());

    verify(mView, times(1)).showLoadingDialog();
    verify(mView, never()).hideLoadingDialog();
  }
}