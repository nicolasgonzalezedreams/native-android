package com.odigeo.presenter;

import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.parser.UpdateUserTravellerWithTravellerRequestAndBuyerRequest;
import com.odigeo.data.entity.shoppingCart.BuyerRequiredFields;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.TravellerRequiredFields;
import com.odigeo.data.entity.shoppingCart.request.PersonalInfoRequest;
import com.odigeo.data.entity.shoppingCart.request.TravellerRequest;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.helper.MixBuyerAndTravellerHelper;
import com.odigeo.interactors.AddPassengerToShoppingCartInteractor;
import com.odigeo.interactors.CheckUserBirthdayInteractor;
import com.odigeo.interactors.CheckUserCredentialsInteractor;
import com.odigeo.interactors.CountriesInteractor;
import com.odigeo.interactors.FindBuyerInLocalPassengersInteractor;
import com.odigeo.interactors.GetTravellersByTypeInteractor;
import com.odigeo.interactors.MembershipInteractor;
import com.odigeo.interactors.SaveTravellersInteractor;
import com.odigeo.interactors.TotalPriceCalculatorInteractor;
import com.odigeo.interactors.VisitUserInteractor;
import com.odigeo.presenter.contracts.navigators.PassengerNavigatorInterface;
import com.odigeo.presenter.contracts.views.PassengerViewInterface;
import com.odigeo.presenter.listeners.OnAddPassengersToShoppingCartListener;
import com.odigeo.tools.DateHelperInterface;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.odigeo.test.mock.MocksProvider.providesCountryMocks;
import static com.odigeo.test.mock.MocksProvider.providesPassengerMocks;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PassengerPresenterTest {

  private static final int GET_FAKE_NON_EMPTY_USER = 1;
  private static String LANGUAGE_ISO_CODE = "es";
  private static String RANDOM_EMAIL = "RANDOM EMAIL";

  @Captor private ArgumentCaptor<OnAddPassengersToShoppingCartListener>
      addPassengerToshoppingCartListener;
  @Mock private PassengerViewInterface view;
  @Mock private PassengerNavigatorInterface navigator;
  @Mock private AddPassengerToShoppingCartInteractor addPassengerToShoppingCartInteractor;
  @Mock private CheckUserCredentialsInteractor checkUserCredentialsInteractor;
  @Mock private GetTravellersByTypeInteractor getTravellersByTypeInteractor;
  @Mock private CheckUserBirthdayInteractor checkUserBirthdayInteractor;
  @Mock private DateHelperInterface dateHelperInterface;
  @Mock private FindBuyerInLocalPassengersInteractor findBuyerInLocalPassengersInteractor;
  @Mock private CountriesInteractor countriesInteractor;
  @Mock private SaveTravellersInteractor saveTravellersInteractor;
  @Mock private UpdateUserTravellerWithTravellerRequestAndBuyerRequest
      updateUserTravellerWithTravellerRequestAndBuyerRequest;
  @Mock private MixBuyerAndTravellerHelper mixBuyerAndTravellerHelper;
  @Mock private VisitUserInteractor visitUserInteractor;
  @Mock private MembershipInteractor membershipInteractor;

  @Mock private List<TravellerRequest> travellerRequests;
  @Mock private CreateShoppingCartResponse createShoppingCartResponse;
  @Mock private OnAddPassengersToShoppingCartListener onAddPassengersToShoppingCartListener;
  @Mock private PersonalInfoRequest personalInfoRequest;
  @Mock private TotalPriceCalculatorInteractor totalPriceCalculatorInteractor;
  @Mock private SessionController sessionController;

  private PassengerPresenter presenter;
  private List<UserTraveller> userTravellers;
  private BuyerRequiredFields buyerRequiredFields;
  private List<TravellerRequiredFields> travellerRequiredFieldses;

  @Before public void setUp() throws Exception {

    MockitoAnnotations.initMocks(this);
    presenter = new PassengerPresenter(view, navigator, addPassengerToShoppingCartInteractor,
        checkUserCredentialsInteractor, getTravellersByTypeInteractor, checkUserBirthdayInteractor,
        dateHelperInterface, findBuyerInLocalPassengersInteractor, countriesInteractor,
        saveTravellersInteractor, updateUserTravellerWithTravellerRequestAndBuyerRequest,
        mixBuyerAndTravellerHelper,
        visitUserInteractor, membershipInteractor, totalPriceCalculatorInteractor,
        sessionController);
  }

  @Test public void shouldConfigureUIWithNoLocalDataAndNoLoggedUser() {

    givenEmptyLocalData();

    when(findBuyerInLocalPassengersInteractor.findBuyerInLocalPassengers(userTravellers)).thenReturn(0);
    when(checkUserCredentialsInteractor.isUserLogin()).thenReturn(false);

    presenter.initializePresenter(LANGUAGE_ISO_CODE, userTravellers, travellerRequiredFieldses,
        buyerRequiredFields, 0.0, 0.0, Locale.ENGLISH, false, BigDecimal.ZERO);
    presenter.configureContactFields();

    verify(view, atMost(2)).hideContactHeader();
    verify(view, atMost(2)).showContactImage();
    verify(view, atMost(2)).showContactSpinnerItem(anyInt());
    verify(view, never()).hideContactName();
    verify(view, never()).hideContactLastName();
    verify(view, atMost(2)).hideContactDateOfBirth();
    verify(view, atMost(2)).hideContactCPF();
    verify(view, never()).hideContactAddress();
    verify(view, never()).hideContactCity();
    verify(view, never()).hideContactState();
    verify(view, never()).hideContactZipCode();
    verify(view, never()).showPhoneCode(anyString(), any(Country.class));
    verify(view, never()).hideContactPhoneNumber();
    verify(view, never()).hideContactCountryCode();
    verify(view, atMost(2)).hideFooterMail();
    verify(view, never()).showContactEditableMail(anyString());
  }

  @Test public void shouldConfigureUIWithNoLocalDataAndLoggedUser() {

    givenEmptyLocalData();

    when(findBuyerInLocalPassengersInteractor.findBuyerInLocalPassengers(userTravellers)).thenReturn(0);
    when(checkUserCredentialsInteractor.isUserLogin()).thenReturn(true);
    when(checkUserCredentialsInteractor.getUserEmail()).thenReturn(RANDOM_EMAIL);

    presenter.initializePresenter(LANGUAGE_ISO_CODE, userTravellers, travellerRequiredFieldses,
        buyerRequiredFields, 0.0, 0.0, Locale.ENGLISH, false, BigDecimal.ZERO);
    presenter.configureContactFields();

    verify(view, atMost(2)).hideContactHeader();
    verify(view, atMost(2)).showContactImage();
    verify(view, atMost(2)).showContactSpinnerItem(anyInt());
    verify(view, never()).hideContactName();
    verify(view, never()).hideContactLastName();
    verify(view, atMost(2)).hideContactDateOfBirth();
    verify(view, atMost(2)).hideContactCPF();
    verify(view, never()).hideContactAddress();
    verify(view, never()).hideContactCity();
    verify(view, never()).hideContactState();
    verify(view, never()).hideContactZipCode();
    verify(view, never()).showPhoneCode(anyString(), any(Country.class));
    verify(view, never()).hideContactPhoneNumber();
    verify(view, never()).hideContactCountryCode();
    verify(view, atMost(2)).showContactEditableMail(RANDOM_EMAIL);
    verify(view, atMost(2)).hideContactEditableMail();
  }

  @Test public void shouldConfigureUIWithLocalDataAndNoLoggedUser() {

    givenLocalData();
    Country country = providesCountryMocks().provideCountryMock();

    when(findBuyerInLocalPassengersInteractor.findBuyerInLocalPassengers(userTravellers)).thenReturn(1);
    when(countriesInteractor.getCountryByCountryCode(anyString(), anyString())).thenReturn(
        country);
    when(checkUserCredentialsInteractor.isUserLogin()).thenReturn(false);

    presenter.initializePresenter(LANGUAGE_ISO_CODE, userTravellers, travellerRequiredFieldses,
        buyerRequiredFields, 0.0, 0.0, Locale.ENGLISH, false, BigDecimal.ZERO);
    presenter.configureContactFields();

    verify(view, atMost(2)).hideContactHeader();
    verify(view, atMost(2)).showContactImage();
    verify(view, atMost(2)).showContactSpinnerItem(anyInt());
    verify(view, atMost(2)).hideContactName();
    verify(view, atMost(2)).hideContactLastName();
    verify(view, atMost(2)).hideContactDateOfBirth();
    verify(view, atMost(2)).hideContactCPF();

    verify(view, never()).hideContactAddress();
    verify(view, atMost(2)).showContactAddress(anyString());
    verify(view, never()).hideContactCity();
    verify(view, atMost(2)).showContactCity(anyString());
    verify(view, never()).hideContactState();
    verify(view, atMost(2)).showContactState(anyString());
    verify(view, never()).hideContactZipCode();
    verify(view, atMost(2)).showContactZipCode(anyString());
    verify(view, never()).hideContactPhoneNumber();
    verify(view, atMost(2)).showContactPhoneNumber(anyString());
    verify(view, never()).hideContactCountryCode();
    verify(view, atMost(2)).showContactCountryCode(any(Country.class));

    verify(view, atMost(2)).hideFooterMail();
    verify(view, atMost(2)).showContactEditableMail(anyString());
  }

  @Test public void shouldConfigureUIWithLocalDataAndLoggedUser() {

    givenLocalData();
    Country country = providesCountryMocks().provideCountryMock();

    when(findBuyerInLocalPassengersInteractor.findBuyerInLocalPassengers(userTravellers)).thenReturn(1);
    when(countriesInteractor.getCountryByCountryCode(anyString(), anyString())).thenReturn(
        country);
    when(checkUserCredentialsInteractor.isUserLogin()).thenReturn(true);
    when(checkUserCredentialsInteractor.getUserEmail()).thenReturn(RANDOM_EMAIL);

    presenter.initializePresenter(LANGUAGE_ISO_CODE, userTravellers, travellerRequiredFieldses,
        buyerRequiredFields, 0.0, 0.0, Locale.ENGLISH, false, BigDecimal.ZERO);
    presenter.configureContactFields();

    verify(view, atMost(2)).hideContactHeader();
    verify(view, atMost(2)).showContactImage();
    verify(view, atMost(2)).showContactSpinnerItem(anyInt());
    verify(view, atMost(2)).hideContactName();
    verify(view, atMost(2)).hideContactLastName();
    verify(view, atMost(2)).hideContactDateOfBirth();
    verify(view, atMost(2)).hideContactCPF();

    verify(view, never()).hideContactAddress();
    verify(view, atMost(2)).showContactAddress(anyString());
    verify(view, never()).hideContactCity();
    verify(view, atMost(2)).showContactCity(anyString());
    verify(view, never()).hideContactState();
    verify(view, atMost(2)).showContactState(anyString());
    verify(view, never()).hideContactZipCode();
    verify(view, atMost(2)).showContactZipCode(anyString());
    verify(view, never()).hideContactPhoneNumber();
    verify(view, atMost(2)).showContactPhoneNumber(anyString());
    verify(view, never()).hideContactCountryCode();
    verify(view, atMost(2)).showContactCountryCode(any(Country.class));

    verify(view, never()).hideFooterMail();
    verify(view, atMost(2)).showUnEditableEmail(anyString());
    verify(view, atMost(2)).hideContactEditableMail();
  }

  @Test public void shouldFormatPassengerNameToShowOnSpinner() {

    List<UserTraveller> passengerNameToFormat = givenAPassengerNameToFormat();

    String nameAndSurnameConcatenated =
        presenter.formatPassengersNameToShowOnSpinner(passengerNameToFormat).get(0);

    assertEquals(nameAndSurnameConcatenated, "James Toca");
  }

  @Test public void shouldHaveMoreNonChildPassengers() {
    userTravellers = providesPassengerMocks().provideNonEmptyUserTravellerListMock();
    buyerRequiredFields = providesPassengerMocks().getBuyerRequireFieldsMock();
    travellerRequiredFieldses = providesPassengerMocks().getRequiredTravellerInformationTwoAdults();

    when(findBuyerInLocalPassengersInteractor.findBuyerInLocalPassengers(userTravellers)).thenReturn(0);
    when(checkUserCredentialsInteractor.isUserLogin()).thenReturn(false);

    presenter.initializePresenter(LANGUAGE_ISO_CODE, userTravellers, travellerRequiredFieldses,
        buyerRequiredFields, 0.0, 0.0, Locale.ENGLISH, false, BigDecimal.ZERO);

    assertEquals(presenter.hasMoreNonChildPassengers(), true);
  }

  @Test public void shouldNotHaveMoreNonChildPassengers() {
    userTravellers = providesPassengerMocks().provideNonEmptyUserTravellerListMock();
    buyerRequiredFields = providesPassengerMocks().getBuyerRequireFieldsMock();
    travellerRequiredFieldses = providesPassengerMocks().getRequiredTravellerInformationOneAdultAndOneChild();

    when(findBuyerInLocalPassengersInteractor.findBuyerInLocalPassengers(userTravellers)).thenReturn(0);
    when(checkUserCredentialsInteractor.isUserLogin()).thenReturn(false);

    presenter.initializePresenter(LANGUAGE_ISO_CODE, userTravellers, travellerRequiredFieldses,
        buyerRequiredFields, 0.0, 0.0, Locale.ENGLISH, false, BigDecimal.ZERO);

    assertEquals(presenter.hasMoreNonChildPassengers(), false);
  }

  private void givenEmptyLocalData() {

    userTravellers = providesPassengerMocks().provideEmptyUserTravellerListWithDefaultMock();
    buyerRequiredFields = providesPassengerMocks().getBuyerRequireFieldsMock();
    travellerRequiredFieldses = providesPassengerMocks().getRequiredTravellerInformationOneAdult();
  }

  private void givenLocalData() {

    userTravellers = providesPassengerMocks().provideNonEmptyUserTravellerListMock();
    buyerRequiredFields = providesPassengerMocks().getBuyerRequireFieldsMock();
    travellerRequiredFieldses = providesPassengerMocks().getRequiredTravellerInformationOneAdult();
  }

  private List<UserTraveller> givenAPassengerNameToFormat() {

    List<UserTraveller> mTravellersListToFormat = new ArrayList<>();
    mTravellersListToFormat.add(providesPassengerMocks().provideNonEmptyUserTravellerListMock()
        .get(GET_FAKE_NON_EMPTY_USER));
    return mTravellersListToFormat;
  }
}