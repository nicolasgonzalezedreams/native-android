package com.odigeo.presenter;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.request.CreditCardCollectionDetailsParametersRequest;
import com.odigeo.data.entity.shoppingCart.request.ModifyShoppingCartRequest;
import com.odigeo.data.net.controllers.AddProductsNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.interactors.AddProductsToShoppingCartInteractor;
import com.odigeo.interactors.RemoveProductsFromShoppingCartInteractor;
import com.odigeo.presenter.contracts.views.PromoCodeWidget;
import com.odigeo.presenter.listeners.OnAddProductsToShoppingCartListener;
import com.odigeo.presenter.listeners.OnRemoveProductsFromShoppingCartListener;
import com.odigeo.presenter.listeners.PromoCodeWidgetListener;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.odigeo.test.mock.MocksProvider.providesPaymentMocks;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;

public class PromoCodeWidgetPresenterTest {
  private static final String ERROR_ADDING_PROMOCDE = "COULD NOT ADD PROMOCODE";
  private MslError mMslRandomError = MslError.UNK_001;

  @Mock private PromoCodeWidget mView;
  @Mock private AddProductsToShoppingCartInteractor mAddProductsToShoppingCartInteractor;
  @Mock private RemoveProductsFromShoppingCartInteractor mRemoveProductsFromShoppingCartInteractor;
  @Mock private PreferencesControllerInterface mPreferencesControllerInterface;
  @Mock private PromoCodeWidgetListener mPromoCodeWidgetListener;

  @Mock private AddProductsNetControllerInterface mAddProductsNetControllerInterface;
  @Mock private CreditCardCollectionDetailsParametersRequest
      mCreditCardCollectionDetailsParametersRequest;
  @Captor private ArgumentCaptor<OnAddProductsToShoppingCartListener>
      mOnAddProductsToShoppingCartListener;
  @Captor private ArgumentCaptor<OnRemoveProductsFromShoppingCartListener>
      mOnRemoveProductsFromShoppingCartListener;

  private PromoCodeWidgetPresenter mPromoCodeWidgetPresenter;

  private CreateShoppingCartResponse mCreateShoppingCartResponse;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    mPromoCodeWidgetPresenter =
        new PromoCodeWidgetPresenter(mView, mAddProductsToShoppingCartInteractor,
            mRemoveProductsFromShoppingCartInteractor, mPreferencesControllerInterface);
  }

  @Test public void shouldAddNewPromoCode() {
    givenBookingFlowSession();
    mPromoCodeWidgetPresenter.configurePromoCodeWidgetPresenter(mCreateShoppingCartResponse);

    mPromoCodeWidgetPresenter.setPromoCodeWidgetListener(mPromoCodeWidgetListener);
    mPromoCodeWidgetPresenter.onValidatePromoCodeButtonClick();

    verify(mAddProductsToShoppingCartInteractor).addProducts(any(ModifyShoppingCartRequest.class),
        mOnAddProductsToShoppingCartListener.capture());
    mOnAddProductsToShoppingCartListener.getValue()
        .onSuccess(providesPaymentMocks().provideCreateShoppingCartResponseWithPromoCode());

    verify(mPromoCodeWidgetListener).onAddPromoCodeSuccessful(
        any(CreateShoppingCartResponse.class));
  }

  @Test public void shouldNotAddNewPromoCodeByOutdatedBookingId() {
    givenBookingFlowSession();
    mPromoCodeWidgetPresenter.configurePromoCodeWidgetPresenter(mCreateShoppingCartResponse);

    mPromoCodeWidgetPresenter.setPromoCodeWidgetListener(mPromoCodeWidgetListener);
    mPromoCodeWidgetPresenter.onValidatePromoCodeButtonClick();

    verify(mAddProductsToShoppingCartInteractor).addProducts(any(ModifyShoppingCartRequest.class),
        mOnAddProductsToShoppingCartListener.capture());
    mOnAddProductsToShoppingCartListener.getValue()
        .onSuccess(providesPaymentMocks().provideCreateShoppingCartResponseWithOutdatedBooking());

    verify(mPromoCodeWidgetListener).onOutdatedBookingId();
  }

  @Test public void shouldNotAddNewPromoCodeByPromoCodeErrors() {
    givenBookingFlowSession();
    mPromoCodeWidgetPresenter.configurePromoCodeWidgetPresenter(mCreateShoppingCartResponse);

    mPromoCodeWidgetPresenter.onValidatePromoCodeButtonClick();

    verify(mAddProductsToShoppingCartInteractor).addProducts(any(ModifyShoppingCartRequest.class),
        mOnAddProductsToShoppingCartListener.capture());
    mOnAddProductsToShoppingCartListener.getValue()
        .onSuccess(providesPaymentMocks().provideCreateShoppingCartResponseWithPromoCodeErrors());

    verify(mView).showPromoCodeError(anyString(), anyString());
  }

  @Test public void shouldNotAddNewPromoCodeByMslError() {
    givenBookingFlowSession();
    mPromoCodeWidgetPresenter.configurePromoCodeWidgetPresenter(mCreateShoppingCartResponse);

    mPromoCodeWidgetPresenter.onValidatePromoCodeButtonClick();

    verify(mAddProductsToShoppingCartInteractor).addProducts(any(ModifyShoppingCartRequest.class),
        mOnAddProductsToShoppingCartListener.capture());
    mOnAddProductsToShoppingCartListener.getValue().onError(mMslRandomError, ERROR_ADDING_PROMOCDE);

    verify(mView).hideValidateProgressBar();
    verify(mView).showValidateButton();
  }

  @Test public void shouldDeletePromoCode() {
    givenBookingFlowSession();
    mPromoCodeWidgetPresenter.configurePromoCodeWidgetPresenter(mCreateShoppingCartResponse);

    mPromoCodeWidgetPresenter.setPromoCodeWidgetListener(mPromoCodeWidgetListener);
    mPromoCodeWidgetPresenter.onDeletePromoCodeButtonClick();

    verify(mRemoveProductsFromShoppingCartInteractor).removeProducts(
        any(ModifyShoppingCartRequest.class), mOnRemoveProductsFromShoppingCartListener.capture());
    mOnRemoveProductsFromShoppingCartListener.getValue()
        .onSuccess(providesPaymentMocks().provideCreateShoppingCartResponseWithPromoCode());

    verify(mPromoCodeWidgetListener).onDeletePromoCodeSuccessful(
        any(CreateShoppingCartResponse.class));
  }

  @Test public void shouldNotDeletePromoCodeByOutdatedBookingId() {
    givenBookingFlowSession();
    mPromoCodeWidgetPresenter.configurePromoCodeWidgetPresenter(mCreateShoppingCartResponse);

    mPromoCodeWidgetPresenter.setPromoCodeWidgetListener(mPromoCodeWidgetListener);
    mPromoCodeWidgetPresenter.onDeletePromoCodeButtonClick();

    verify(mRemoveProductsFromShoppingCartInteractor).removeProducts(
        any(ModifyShoppingCartRequest.class), mOnRemoveProductsFromShoppingCartListener.capture());
    mOnRemoveProductsFromShoppingCartListener.getValue()
        .onSuccess(providesPaymentMocks().provideCreateShoppingCartResponseWithOutdatedBooking());

    verify(mPromoCodeWidgetListener).onOutdatedBookingId();
  }

  @Test public void shouldNotDeletePromoCodeByPromoCodeErrors() {
    givenBookingFlowSession();
    mPromoCodeWidgetPresenter.configurePromoCodeWidgetPresenter(mCreateShoppingCartResponse);

    mPromoCodeWidgetPresenter.onDeletePromoCodeButtonClick();

    verify(mRemoveProductsFromShoppingCartInteractor).removeProducts(
        any(ModifyShoppingCartRequest.class), mOnRemoveProductsFromShoppingCartListener.capture());
    mOnRemoveProductsFromShoppingCartListener.getValue()
        .onSuccess(providesPaymentMocks().provideCreateShoppingCartResponseWithPromoCodeErrors());

    verify(mView).showPromoCodeError(anyString(), anyString());
  }

  @Test public void shouldNotDeletePromoCodeByMslError() {
    givenBookingFlowSession();
    mPromoCodeWidgetPresenter.configurePromoCodeWidgetPresenter(mCreateShoppingCartResponse);

    mPromoCodeWidgetPresenter.onDeletePromoCodeButtonClick();

    verify(mRemoveProductsFromShoppingCartInteractor).removeProducts(
        any(ModifyShoppingCartRequest.class), mOnRemoveProductsFromShoppingCartListener.capture());
    mOnRemoveProductsFromShoppingCartListener.getValue()
        .onError(mMslRandomError, ERROR_ADDING_PROMOCDE);

    verify(mView).hideDeleteProgressBar();
    verify(mView).showDeletePromoCodeContainer();
  }

  private void givenBookingFlowSession() {
    mCreateShoppingCartResponse =
        providesPaymentMocks().provideCreateShoppingCartResponseWithPromoCode();
  }
}
