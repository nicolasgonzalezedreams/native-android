package com.odigeo.presenter;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.interactors.ArrivalGuidesCardsInteractor;
import com.odigeo.interactors.CheckUpcomingBookingInteractor;
import com.odigeo.interactors.CheckUserCredentialsInteractor;
import com.odigeo.interactors.UpdateBookingStatusInteractor;
import com.odigeo.presenter.contracts.navigators.MyTripDetailsNavigatorInterface;
import com.odigeo.presenter.contracts.views.MyTripDetailsViewInterface;
import java.awt.print.Book;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.verification.VerificationMode;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.booleanThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class MyTripDetailsPresenterTest {

  @Mock MyTripDetailsViewInterface view;
  @Mock MyTripDetailsNavigatorInterface navigator;
  @Mock CheckUpcomingBookingInteractor checkUpcomingBookingInteractor;
  @Mock ArrivalGuidesCardsInteractor arrivalGuidesCardsInteractor;
  @Mock CheckUserCredentialsInteractor checkUserCredentialsInteractor;
  @Mock UpdateBookingStatusInteractor updateBookingStatusInteractor;
  @Mock TrackerControllerInterface trackerController;

  @Captor ArgumentCaptor<UpdateBookingStatusInteractor.BookingStatusListener>
      updateBookingStatusListener;

  private MyTripDetailsPresenter presenter;

  @Before public void setup() {
    presenter = new MyTripDetailsPresenter(view, navigator, checkUpcomingBookingInteractor,
        arrivalGuidesCardsInteractor, updateBookingStatusInteractor, checkUserCredentialsInteractor,
        trackerController);

    when(view.isActive()).thenReturn(true);
  }

  @Test public void testHasArrivalGuideFetchesFromDao() {
    when(arrivalGuidesCardsInteractor.hasArrivalGuide(anyLong())).thenReturn(true);

    Booking booking = mock(Booking.class);
    when(booking.isMultiSegment()).thenReturn(false);
    when(booking.getArrivalGeoNodeId()).thenReturn(1L);

    boolean hasArrivalGuide = presenter.onCheckHasArrivalGuide(booking);

    assertThat(hasArrivalGuide, is(true));

    verify(arrivalGuidesCardsInteractor).hasArrivalGuide(1);
    verifyNoMoreInteractions(arrivalGuidesCardsInteractor);
  }

  @Test public void testOnAddToCalendarTracksAndCallsNavigator() {
    final Booking booking = new Booking();
    presenter.onAddToCalendar(booking);

    verify(trackerController).trackAddToCalendar();
    verify(navigator).addToCalendar(booking);
    verifyNoMoreInteractions(trackerController, navigator);
  }

  @Test public void testOnCheckUpcomingBooking() {
    final Booking booking = new Booking();

    when(checkUpcomingBookingInteractor.isUpcomingBooking(booking)).thenReturn(true);

    boolean isUpcomingBooking = presenter.onCheckUpcomingBooking(booking);

    assertThat(isUpcomingBooking, is(true));

    verify(checkUpcomingBookingInteractor).isUpcomingBooking(booking);
  }

  @Test public void testOnFinishDetailViewCallsNavigator() {
    presenter.finishDetailsView();

    verify(navigator).exitFromNavigator();
    verifyNoMoreInteractions(navigator);
  }

  @Test public void testOnCheckIfUserIsLoggedInAndIsUpcomingBookingSetsUpRefreshingViews() {
    when(checkUserCredentialsInteractor.isUserLogin()).thenReturn(true);

    final Booking booking = mock(Booking.class);
    when(booking.isPastBooking()).thenReturn(false);
    presenter.onCheckIfUserIsLoggedIn(booking);

    verify(checkUserCredentialsInteractor).isUserLogin();
    verify(view).setupRefreshBooking();
    verifyNoMoreInteractions(checkUpcomingBookingInteractor, view);
  }

  @Test public void testOnCheckIfIsPastBookingDisablesRefreshingViews() {
    final Booking booking = mock(Booking.class);
    when(booking.isPastBooking()).thenReturn(true);
    presenter.onCheckIfUserIsLoggedIn(booking);

    verify(view).disableRefreshBooking();
    verifyNoMoreInteractions(view);
  }

  @Test public void testOnRefreshBookingStatusUpdatesViewAndHidesRefresh() {
    final Booking booking = new Booking();
    final Booking updatedBooking = new Booking();
    presenter.onRefreshBookingStatus(booking);

    InOrder inOrder = inOrder(view, updateBookingStatusInteractor);
    inOrder.verify(updateBookingStatusInteractor)
        .updateBookingStatus(eq(booking), updateBookingStatusListener.capture());

    updateBookingStatusListener.getValue().onComplete(updatedBooking);

    inOrder.verify(view).isActive();
    inOrder.verify(view).updateBookingStatus(updatedBooking);
    inOrder.verify(view).hideRefreshing();
    inOrder.verifyNoMoreInteractions();
  }

  @Test public void testOnRefreshBookingStatusDontInteractWithViewIfNotActive() {
    when(view.isActive()).thenReturn(false);

    final Booking booking = new Booking();
    final Booking updatedBooking = new Booking();
    presenter.onRefreshBookingStatus(booking);

    InOrder inOrder = inOrder(view, updateBookingStatusInteractor);
    inOrder.verify(updateBookingStatusInteractor)
        .updateBookingStatus(eq(booking), updateBookingStatusListener.capture());

    updateBookingStatusListener.getValue().onComplete(updatedBooking);

    inOrder.verify(view).isActive();
    inOrder.verifyNoMoreInteractions();
  }
}
