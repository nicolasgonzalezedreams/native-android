package com.odigeo.interactors;

import com.odigeo.data.entity.localizables.LocalizablesResponse;
import com.odigeo.data.net.controllers.LocalizablesNetController;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.presenter.listeners.LocalizablesUtilsListener;
import com.odigeo.presenter.listeners.OnGetLocalizablesListener;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class) public class LocalizablesInteractorTest {

  @Mock private LocalizablesNetController localizablesNetController;
  @Mock private LocalizablesUtilsListener localizablesUtilsListener;
  @Mock private CrashlyticsController crashlyticsController;
  @Mock private OnGetLocalizablesListener onGetLocalizablesListener;
  @Mock private LocalizablesResponse localizablesResponse;
  @Captor private ArgumentCaptor<OnRequestDataListener<LocalizablesResponse>> onRequestDataListener;
  private String market = "es_ES";
  private LocalizablesInteractor localizablesInteractor;
  private MslError mslError = MslError.STA_001;
  private String message = "ERROR MESSAGE";

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    localizablesInteractor =
        new LocalizablesInteractor(localizablesNetController, localizablesUtilsListener,
            crashlyticsController);
  }

  @Test public void testGetLocalizablesError() {
    localizablesInteractor.getLocalizables(market, onGetLocalizablesListener);
    verify(localizablesNetController).getLocalizables(onRequestDataListener.capture());

    onRequestDataListener.getValue().onError(mslError, message);

    verify(onGetLocalizablesListener).onError(mslError, message);
  }
}
