package com.odigeo.interactors;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.ShoppingCart;
import com.odigeo.data.entity.shoppingCart.request.PersonalInfoRequest;
import com.odigeo.data.net.controllers.AddPassengerNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.trustdefender.TrustDefenderController;
import com.odigeo.presenter.listeners.OnAddPassengersToShoppingCartListener;
import com.odigeo.test.mock.MocksProvider;
import com.odigeo.test.mock.mocks.PaymentMocks;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.odigeo.data.preferences.PreferencesControllerInterface.JSESSION_COOKIE;
import static com.odigeo.test.mock.MocksProvider.providesPaymentMocks;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class AddPassengerToShoppingCartInteractorTest {

  private static String ERROR_ADDING_PASSENGER = "COULD NOT ADD PASSENGER";

  @Mock private AddPassengerNetControllerInterface mAddPassengerNetController;
  @Mock private TrustDefenderController trustDefenderController;
  @Mock private PreferencesControllerInterface preferencesController;
  @Mock private PersonalInfoRequest mPersonalInfoRequest;
  @Mock private OnAddPassengersToShoppingCartListener mOnAddPassengersToShoppingCartListener;
  @Mock private CreateShoppingCartResponse mCreateShoppingCartResponse;
  @Captor private ArgumentCaptor<OnRequestDataListener<CreateShoppingCartResponse>>
      mOnRequestDataListener;
  private AddPassengerToShoppingCartInteractor mAddPassengerToShoppingCartInteractor;
  private MslError mMslRandomError = MslError.UNK_001;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    mAddPassengerToShoppingCartInteractor =
        new AddPassengerToShoppingCartInteractor(mAddPassengerNetController,
            trustDefenderController, preferencesController);
  }

  @Test public void shouldAddPassengerToShoppingCart() {
    when(mCreateShoppingCartResponse.getShoppingCart()).thenReturn(
        providesPaymentMocks().provideShoppingCart());
    mAddPassengerToShoppingCartInteractor.addAddPassenger(mPersonalInfoRequest,
        mOnAddPassengersToShoppingCartListener);
    verify(mAddPassengerNetController).addPassengerToShoppingCart(mOnRequestDataListener.capture(),
        eq(mPersonalInfoRequest));
    mOnRequestDataListener.getValue().onResponse(mCreateShoppingCartResponse);
    verify(mOnAddPassengersToShoppingCartListener).onSuccess(mCreateShoppingCartResponse);
    verify(preferencesController).getStringValue(JSESSION_COOKIE);
    verify(trustDefenderController).sendFingerPrint(anyString(), anyString());
    verifyNoMoreInteractions(mAddPassengerNetController);
  }

  @Test public void shouldNotAddPassengerToShoppingCart() {
    mAddPassengerToShoppingCartInteractor.addAddPassenger(mPersonalInfoRequest,
        mOnAddPassengersToShoppingCartListener);
    verify(mAddPassengerNetController).addPassengerToShoppingCart(mOnRequestDataListener.capture(),
        eq(mPersonalInfoRequest));
    mOnRequestDataListener.getValue().onError(mMslRandomError, ERROR_ADDING_PASSENGER);
    verify(mOnAddPassengersToShoppingCartListener).onError(mMslRandomError, ERROR_ADDING_PASSENGER);
    verifyNoMoreInteractions(mAddPassengerNetController);
  }
}