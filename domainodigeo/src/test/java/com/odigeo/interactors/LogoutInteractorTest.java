package com.odigeo.interactors;

import com.odigeo.data.db.helper.BookingsHandlerInterface;
import com.odigeo.data.db.helper.MembershipHandlerInterface;
import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.net.controllers.NotificationNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.test.mock.mocks.UserMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class LogoutInteractorTest {

  @Mock private SessionController sessionController;
  @Mock private NotificationNetControllerInterface notificationsNetController;
  @Mock private BookingsHandlerInterface bookingsHandler;
  @Mock private SearchesHandlerInterface searchesHandler;
  @Mock private VisitUserInteractor visitUserInteractor;
  @Mock private MembershipHandlerInterface membershipHandler;
  @Mock private OnRequestDataListener<Boolean> onRequestDataListener;

  @Captor private ArgumentCaptor<OnAuthRequestDataListener<Boolean>> listenerArgumentCaptor;

  private LogoutInteractor logoutInteractor;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);

    logoutInteractor =
        new LogoutInteractor(sessionController, notificationsNetController, bookingsHandler,
            searchesHandler, visitUserInteractor, membershipHandler);
  }

  @Test public void logoutSuccess() {
    when(sessionController.getGcmToken()).thenReturn(UserMocks.ACCESS_TOKEN);
    logoutInteractor.logout(onRequestDataListener);

    verify(notificationsNetController).disableNotifications(listenerArgumentCaptor.capture(),
        anyString());
    listenerArgumentCaptor.getValue().onResponse(true);

    verify(visitUserInteractor, times(1)).logoutVisitUser();
    verify(searchesHandler, times(1)).removeSynchronizedStoredSearches();
    verify(sessionController, times(1)).removeAllData();
    verify(bookingsHandler, times(1)).deleteAllBookingData();
    verify(sessionController, times(1)).attachCreditCardsToSession(null);
    verify(membershipHandler, times(1)).clearMembership();
    verify(onRequestDataListener, times(1)).onResponse(true);
  }

  @Test public void logoutAuthError() {
    when(sessionController.getGcmToken()).thenReturn(UserMocks.ACCESS_TOKEN);
    logoutInteractor.logout(onRequestDataListener);

    verify(notificationsNetController).disableNotifications(listenerArgumentCaptor.capture(),
        anyString());
    listenerArgumentCaptor.getValue().onAuthError();

    verify(visitUserInteractor, times(1)).logoutVisitUser();
    verify(searchesHandler, times(1)).removeSynchronizedStoredSearches();
    verify(sessionController, times(1)).removeAllData();
    verify(bookingsHandler, times(1)).deleteAllBookingData();
    verify(sessionController, times(1)).attachCreditCardsToSession(null);
    verify(membershipHandler, times(1)).clearMembership();
    verify(onRequestDataListener, times(1)).onResponse(true);
  }

  @Test public void logoutMslErrorSTA000() {
    when(sessionController.getGcmToken()).thenReturn(UserMocks.ACCESS_TOKEN);
    logoutInteractor.logout(onRequestDataListener);

    verify(notificationsNetController).disableNotifications(listenerArgumentCaptor.capture(),
        anyString());
    listenerArgumentCaptor.getValue().onError(MslError.STA_000, "Message");

    verify(visitUserInteractor, times(1)).logoutVisitUser();
    verify(searchesHandler, times(1)).removeSynchronizedStoredSearches();
    verify(sessionController, times(1)).removeAllData();
    verify(bookingsHandler, times(1)).deleteAllBookingData();
    verify(sessionController, times(1)).attachCreditCardsToSession(null);
    verify(membershipHandler, times(1)).clearMembership();
    verify(onRequestDataListener, times(1)).onResponse(true);
  }

  @Test public void logoutMslErrorGeneric() {
    when(sessionController.getGcmToken()).thenReturn(UserMocks.ACCESS_TOKEN);
    logoutInteractor.logout(onRequestDataListener);

    verify(notificationsNetController).disableNotifications(listenerArgumentCaptor.capture(),
        anyString());
    listenerArgumentCaptor.getValue().onError(MslError.STA_001, "Message");

    verify(onRequestDataListener, times(1)).onResponse(false);
  }

  @Test public void logoutGCMTokenError() {
    when(sessionController.getGcmToken()).thenReturn(null);

    logoutInteractor.logout(onRequestDataListener);

    verifyZeroInteractions(notificationsNetController);
    verify(visitUserInteractor, times(1)).logoutVisitUser();
    verify(searchesHandler, times(1)).removeSynchronizedStoredSearches();
    verify(sessionController, times(1)).removeAllData();
    verify(bookingsHandler, times(1)).deleteAllBookingData();
    verify(sessionController, times(1)).attachCreditCardsToSession(null);
    verify(membershipHandler, times(1)).clearMembership();
    verify(onRequestDataListener, times(1)).onResponse(true);
  }
}
