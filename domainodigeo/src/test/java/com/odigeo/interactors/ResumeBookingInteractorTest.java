package com.odigeo.interactors;

import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.data.entity.shoppingCart.ResumeBooking;
import com.odigeo.data.net.controllers.ResumeBookingNetController;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.presenter.listeners.OnResumeBookingListener;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class ResumeBookingInteractorTest {

  private static String ERROR_RESUME_BOOKING = "COULD NOT RESUME BOOKING";

  @Mock private ResumeBookingNetController mResumeBookingNetController;
  @Mock private OnResumeBookingListener mOnResumeBookingListener;
  @Mock private ResumeBooking mResumeBooking;
  @Mock private BookingResponse mBookingResponse;
  @Captor private ArgumentCaptor<OnRequestDataListener<BookingResponse>> mOnRequestDataListener;
  private ResumeBookingInteractor mResumeBookingInteractorInteractor;
  private MslError mMslRandomError = MslError.UNK_001;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    mResumeBookingInteractorInteractor = new ResumeBookingInteractor(mResumeBookingNetController);
  }

  @Test public void shouldResumeBooking() {
    mResumeBookingInteractorInteractor.resumeBooking(mResumeBooking, mOnResumeBookingListener);
    verify(mResumeBookingNetController).resumeBooking(mOnRequestDataListener.capture(),
        eq(mResumeBooking));
    mOnRequestDataListener.getValue().onResponse(mBookingResponse);
    verify(mOnResumeBookingListener).onSuccess(mBookingResponse);
    verifyNoMoreInteractions(mResumeBookingNetController);
  }

    @Test
    public void shouldNotResumeBooking(){
        mResumeBookingInteractorInteractor.resumeBooking(mResumeBooking, mOnResumeBookingListener);
        verify(mResumeBookingNetController).resumeBooking(mOnRequestDataListener.capture(),eq(mResumeBooking));
        mOnRequestDataListener.getValue().onError(mMslRandomError, ERROR_RESUME_BOOKING);
        verify(mOnResumeBookingListener).onNoConnectionError();
        verifyNoMoreInteractions(mResumeBookingNetController);
    }
}
