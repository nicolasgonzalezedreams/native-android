package com.odigeo.interactors;

import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.net.controllers.SearchesNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.odigeo.test.mock.MocksProvider.providesStoredSearchMocks;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Created by Javier Marsicano on 12/08/16.
 */
public class SearchFlightInteractorTest {

  private static final MslError MSL_ERROR = MslError.UNK_001;
  private static final String REQUEST_ERROR_MESSAGE = "Could not add search";

  @Mock private SearchesNetControllerInterface mSearchesNetController;
  @Mock private SearchesHandlerInterface mSearchesHandler;
  @Mock private CheckUserCredentialsInteractor mUserSessionInteractor;
  @Mock private StoredSearch mStoredSearch;

  @Captor private ArgumentCaptor<OnRequestDataListener<StoredSearch>> mOnRequestDataListener;
  private SearchFlightInteractor mInteractor;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);

    mStoredSearch = providesStoredSearchMocks().provideStoredSearchNotSync();

    mInteractor = new SearchFlightInteractor(mSearchesNetController, mSearchesHandler,
        mUserSessionInteractor);
  }

  @Test public void addNewSearchTest_Success_userLoggedIn() {
    when(mUserSessionInteractor.isUserLogin()).thenReturn(true);
    mInteractor.addNewFlightSearch(mStoredSearch);

    verify(mSearchesNetController).postUserSearch(mOnRequestDataListener.capture(),
        Matchers.eq(mStoredSearch));
  }

  @Test public void addNewSearchTest_Error_userLoggedIn() {
    when(mUserSessionInteractor.isUserLogin()).thenReturn(true);
    mInteractor.addNewFlightSearch(mStoredSearch);

    verify(mSearchesNetController).postUserSearch(mOnRequestDataListener.capture(),
        Matchers.eq(mStoredSearch));
    mOnRequestDataListener.getValue().onError(MSL_ERROR, REQUEST_ERROR_MESSAGE);
    verify(mSearchesHandler).saveStoredSearch(mStoredSearch);
  }

  @Test public void addNewSearchUserNotLoggedInTest() {
    when(mUserSessionInteractor.isUserLogin()).thenReturn(false);
    mInteractor.addNewFlightSearch(mStoredSearch);
    verify(mSearchesHandler).saveStoredSearch(mStoredSearch);
  }

  @After public void tearDown() {
    verifyNoMoreInteractions(mSearchesNetController);
  }
}
