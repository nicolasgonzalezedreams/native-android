package com.odigeo.interactors;

import com.odigeo.data.db.dao.BaggageDBDAOInterface;
import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.db.dao.BuyerDBDAOInterface;
import com.odigeo.data.db.dao.InsuranceDBDAOInterface;
import com.odigeo.data.db.dao.SectionDBDAOInterface;
import com.odigeo.data.db.dao.SegmentDBDAOInterface;
import com.odigeo.data.db.dao.TravellerDBDAOInterface;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Buyer;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.presenter.listeners.OnRemoveBookingsListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class DeleteBookingsInteractorTest {

  private static final long BOOKING_ID_REJECTED = 1417272727L;

  private List<Long> rejectedBookingIdsExpected;
  private BookingDBDAOInterface mBookingDBDAOInterface;
  private TravellerDBDAOInterface mTravellerDBDAOInterface;
  private BuyerDBDAOInterface mBuyerDBDAOInterface;
  private InsuranceDBDAOInterface mInsuranceDBDAOInterface;
  private BaggageDBDAOInterface mBaggageDBDAOInterface;
  private SegmentDBDAOInterface mSegmentDBDAOInterface;
  private SectionDBDAOInterface mSectionDBDAOInterface;
  private Booking booking;
  private Segment segment;
  private List<Booking> bookingsToRemove;
  private List<Segment> segmentsToRemove;
  private OnRemoveBookingsListener onRemoveBookingsListener;
  private DeleteBookingsInteractor interactor;

  @Before public void setUp() {
    mBookingDBDAOInterface = Mockito.mock(BookingDBDAOInterface.class);
    mTravellerDBDAOInterface = Mockito.mock(TravellerDBDAOInterface.class);
    mBuyerDBDAOInterface = Mockito.mock(BuyerDBDAOInterface.class);
    mInsuranceDBDAOInterface = Mockito.mock(InsuranceDBDAOInterface.class);
    mBaggageDBDAOInterface = Mockito.mock(BaggageDBDAOInterface.class);
    mSegmentDBDAOInterface = Mockito.mock(SegmentDBDAOInterface.class);
    mSectionDBDAOInterface = Mockito.mock(SectionDBDAOInterface.class);
    onRemoveBookingsListener = Mockito.mock(OnRemoveBookingsListener.class);
    booking = Mockito.mock(Booking.class);
    segment = Mockito.mock(Segment.class);
    initBookingsToRemove();
    initBookingIdsRemoved();
    interactor = new DeleteBookingsInteractor(mBookingDBDAOInterface, mTravellerDBDAOInterface,
        mBuyerDBDAOInterface, mInsuranceDBDAOInterface, mBaggageDBDAOInterface,
        mSegmentDBDAOInterface, mSectionDBDAOInterface);
  }

  private void initBookingsToRemove() {
    bookingsToRemove = new ArrayList<>();
    segmentsToRemove = new ArrayList<>();
    segmentsToRemove.add(segment);
    booking.setSegments(segmentsToRemove);
    bookingsToRemove.add(booking);
  }

  private void initBookingIdsRemoved() {
    rejectedBookingIdsExpected = new ArrayList<>();
    rejectedBookingIdsExpected.add(BOOKING_ID_REJECTED);
  }

  @Test public void removeBookingsWhitoutSegmentsSuccessfully() {

    Mockito.when(booking.getSegments()).thenReturn(null);
    Mockito.when(booking.getTravellers()).thenReturn(Collections.EMPTY_LIST);
    Mockito.when(booking.getBuyer()).thenReturn(Mockito.mock(Buyer.class));
    Mockito.when(booking.getInsurances()).thenReturn(Collections.EMPTY_LIST);

    Mockito.when(mTravellerDBDAOInterface.deleteTraveller(Mockito.anyLong())).thenReturn(true);
    Mockito.when(mBuyerDBDAOInterface.deleteBuyer(Mockito.anyLong())).thenReturn(true);
    Mockito.when(mInsuranceDBDAOInterface.removeInsurance(Mockito.anyLong())).thenReturn(true);
    Mockito.when(mBookingDBDAOInterface.deleteBooking(Mockito.anyLong())).thenReturn(true);

    interactor.removeBookings(bookingsToRemove, onRemoveBookingsListener);

    Mockito.verify(mBookingDBDAOInterface).initTransaction();
    Mockito.verify(mBookingDBDAOInterface).setTransactionSuccessful();
    Mockito.verify(mBookingDBDAOInterface).endTransaction();
    Mockito.verify(onRemoveBookingsListener).onSuccess();
  }

  @Test public void removeBookingsWhitSegmentsSuccessfully() {

    Mockito.when(booking.getTravellers()).thenReturn(Collections.EMPTY_LIST);
    Mockito.when(booking.getBuyer()).thenReturn(Mockito.mock(Buyer.class));
    Mockito.when(booking.getInsurances()).thenReturn(Collections.EMPTY_LIST);
    Mockito.when(booking.getSegments()).thenReturn(segmentsToRemove);

    Mockito.when(segment.getBaggagesList()).thenReturn(Collections.EMPTY_LIST);
    Mockito.when(segment.getSectionsList()).thenReturn(Collections.EMPTY_LIST);

    Mockito.when(mTravellerDBDAOInterface.deleteTraveller(Mockito.anyLong())).thenReturn(true);
    Mockito.when(mBuyerDBDAOInterface.deleteBuyer(Mockito.anyLong())).thenReturn(true);
    Mockito.when(mInsuranceDBDAOInterface.removeInsurance(Mockito.anyLong())).thenReturn(true);
    Mockito.when(mBookingDBDAOInterface.deleteBooking(Mockito.anyLong())).thenReturn(true);
    Mockito.when(mBaggageDBDAOInterface.deleteBaggage(Mockito.anyLong())).thenReturn(true);
    Mockito.when(mSectionDBDAOInterface.deleteSection(Mockito.anyLong())).thenReturn(true);
    Mockito.when(mSegmentDBDAOInterface.deleteSegments(Mockito.anyLong())).thenReturn(true);

    interactor.removeBookings(bookingsToRemove, onRemoveBookingsListener);

    Mockito.verify(mBookingDBDAOInterface).initTransaction();
    Mockito.verify(mBookingDBDAOInterface).endTransaction();
    Mockito.verify(mBookingDBDAOInterface).setTransactionSuccessful();
    Mockito.verify(onRemoveBookingsListener).onSuccess();
  }


    /*@Test
    public void rejectedBookingsIdsWhitSegmentsTest() {

        Mockito.when(booking.getBookingId()).thenReturn(BOOKING_ID_REJECTED);
        Mockito.when(booking.getTravellers()).thenReturn(Collections.EMPTY_LIST);
        Mockito.when(booking.getBuyer()).thenReturn(Mockito.mock(Buyer.class));
        Mockito.when(booking.getInsurances()).thenReturn(Collections.EMPTY_LIST);
        Mockito.when(booking.getSegments()).thenReturn(segmentsToRemove);

        Mockito.when(segment.getBaggagesList()).thenReturn(Collections.EMPTY_LIST);
        Mockito.when(segment.getSectionsList()).thenReturn(Collections.EMPTY_LIST);

        Mockito.when(mTravellerDBDAOInterface.deleteTraveller(Mockito.anyLong())).thenReturn(true);
        Mockito.when(mBuyerDBDAOInterface.deleteBuyer(Mockito.anyLong())).thenReturn(true);
        Mockito.when(mInsuranceDBDAOInterface.removeInsurance(Mockito.anyLong())).thenReturn(true);
        Mockito.when(mBookingDBDAOInterface.deleteBooking(Mockito.anyLong())).thenReturn(true);
        Mockito.when(mBaggageDBDAOInterface.deleteBaggage(Mockito.anyLong())).thenReturn(true);
        Mockito.when(mSectionDBDAOInterface.deleteSection(Mockito.anyLong())).thenReturn(false);
        Mockito.when(mSegmentDBDAOInterface.deleteSegments(Mockito.anyLong())).thenReturn(true);

        List<Long> rejectedBookingsIdsActual = interactor.deleteBookingsCompletely(bookingsToRemove);
        Mockito.verify(mBookingDBDAOInterface).initTransaction();
        Mockito.verify(mBookingDBDAOInterface).endTransaction();
        Mockito.verify(mBookingDBDAOInterface, Mockito.never()).setTransactionSuccessful();
        Assert.assertEquals(rejectedBookingIdsExpected, rejectedBookingsIdsActual);

    }*/


    /*@Test
    public void rejectedBookingsIdsWhitoutSegmentsTest() {
        Mockito.when(booking.getBookingId()).thenReturn(BOOKING_ID_REJECTED);
        Mockito.when(booking.getSegments()).thenReturn(null);
        Mockito.when(booking.getTravellers()).thenReturn(Collections.EMPTY_LIST);
        Mockito.when(booking.getBuyer()).thenReturn(Mockito.mock(Buyer.class));
        Mockito.when(booking.getInsurances()).thenReturn(Collections.EMPTY_LIST);

        Mockito.when(mTravellerDBDAOInterface.deleteTraveller(Mockito.anyLong())).thenReturn(false);
        Mockito.when(mBuyerDBDAOInterface.deleteBuyer(Mockito.anyLong())).thenReturn(true);
        Mockito.when(mInsuranceDBDAOInterface.removeInsurance(Mockito.anyLong())).thenReturn(true);
        Mockito.when(mBookingDBDAOInterface.deleteBooking(Mockito.anyLong())).thenReturn(true);

        List<Long> rejectedBookingsIdsActual = interactor.deleteBookingsCompletely(bookingsToRemove);
        Mockito.verify(mBookingDBDAOInterface).initTransaction();
        Mockito.verify(mBookingDBDAOInterface).endTransaction();
        Mockito.verify(mBookingDBDAOInterface, Mockito.never()).setTransactionSuccessful();
        Assert.assertEquals(rejectedBookingIdsExpected, rejectedBookingsIdsActual);
    }*/

    /*@Test
    public void rejectedBookingsIdsWhitMultipleErrors() {
        Mockito.when(booking.getBookingId()).thenReturn(BOOKING_ID_REJECTED);
        Mockito.when(booking.getSegments()).thenReturn(null);
        Mockito.when(booking.getTravellers()).thenReturn(Collections.EMPTY_LIST);
        Mockito.when(booking.getBuyer()).thenReturn(Mockito.mock(Buyer.class));
        Mockito.when(booking.getInsurances()).thenReturn(Collections.EMPTY_LIST);

        Mockito.when(mTravellerDBDAOInterface.deleteTraveller(Mockito.anyLong())).thenReturn(false);
        Mockito.when(mBuyerDBDAOInterface.deleteBuyer(Mockito.anyLong())).thenReturn(false);
        Mockito.when(mInsuranceDBDAOInterface.removeInsurance(Mockito.anyLong())).thenReturn(false);
        Mockito.when(mBookingDBDAOInterface.deleteBooking(Mockito.anyLong())).thenReturn(false);

        List<Long> rejectedBookingsIdsActual = interactor.deleteBookingsCompletely(bookingsToRemove);
        Mockito.verify(mBookingDBDAOInterface).initTransaction();
        Mockito.verify(mBookingDBDAOInterface).endTransaction();
        Mockito.verify(mBookingDBDAOInterface, Mockito.never()).setTransactionSuccessful();
        Assert.assertEquals(rejectedBookingIdsExpected, rejectedBookingsIdsActual);
    }*/
}
