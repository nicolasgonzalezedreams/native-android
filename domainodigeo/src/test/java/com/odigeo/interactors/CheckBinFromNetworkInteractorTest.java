package com.odigeo.interactors;

import com.odigeo.data.entity.CreditCardBinDetails;
import com.odigeo.data.net.controllers.BinCheckNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.presenter.listeners.OnCheckBinListener;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class CheckBinFromNetworkInteractorTest {

  private static String ANY_CREDIT_CARD_NUMBER = "123456789";
  private static String BIN_CHECK_ERROR_MESSAGE = "Error with bincheck";
  private static MslError mMslRandomError = MslError.UNK_001;

  @Mock private BinCheckNetControllerInterface mBinCheckNetController;
  @Mock private CreditCardBinDetails mCreditCardBinDetails;
  @Mock private OnCheckBinListener mOnCheckBinListener;
  @Mock private OnRequestDataListener<CreditCardBinDetails> mOnRequestDataListenerMock;
  @Captor private ArgumentCaptor<OnRequestDataListener<CreditCardBinDetails>>
      mOnRequestDataListener;

  private CheckBinFromNetworkInteractor mCheckBinFromNetworkInteractor;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    mCheckBinFromNetworkInteractor = new CheckBinFromNetworkInteractor(mBinCheckNetController);
  }

  @Test public void shouldBinCheckMSLReturnSuccesfull() {

    mCheckBinFromNetworkInteractor.checkBinMSL(ANY_CREDIT_CARD_NUMBER, mOnCheckBinListener);

    verify(mBinCheckNetController).checkBin(mOnRequestDataListener.capture(),
        eq(ANY_CREDIT_CARD_NUMBER));
    mOnRequestDataListener.getValue().onResponse(mCreditCardBinDetails);
    verifyNoMoreInteractions(mBinCheckNetController);
  }

  @Test public void shouldBinCheckMSLReturnFailed() {

    mCheckBinFromNetworkInteractor.checkBinMSL(ANY_CREDIT_CARD_NUMBER, mOnCheckBinListener);

    verify(mBinCheckNetController).checkBin(mOnRequestDataListener.capture(),
        eq(ANY_CREDIT_CARD_NUMBER));
    mOnRequestDataListener.getValue().onError(mMslRandomError, BIN_CHECK_ERROR_MESSAGE);
    verifyNoMoreInteractions(mBinCheckNetController);
  }

  @Test public void shouldStoreCacheBinCheck_MSLReturnSuccesfull() {

    mCheckBinFromNetworkInteractor.checkBinMSL(ANY_CREDIT_CARD_NUMBER, mOnCheckBinListener);
    Map<String, CreditCardBinDetails> binCheckCache =
        mCheckBinFromNetworkInteractor.getBinCheckCache();

    verify(mBinCheckNetController).checkBin(mOnRequestDataListener.capture(),
        eq(ANY_CREDIT_CARD_NUMBER));
    mOnRequestDataListener.getValue().onResponse(mCreditCardBinDetails);
    Assert.assertTrue(binCheckCache.get(ANY_CREDIT_CARD_NUMBER) != null);
  }

  @Test public void shouldGetBinCheckFromCache_creditCardStoredInCache() {
    HashMap<String, CreditCardBinDetails> cache = new HashMap<>();
    cache.put(ANY_CREDIT_CARD_NUMBER, mCreditCardBinDetails);
    mCheckBinFromNetworkInteractor.setBinCheckCache(cache);

    mCheckBinFromNetworkInteractor.checkBinMSL(ANY_CREDIT_CARD_NUMBER, mOnCheckBinListener);

    verify(mBinCheckNetController, never()).checkBin(mOnRequestDataListenerMock,
        ANY_CREDIT_CARD_NUMBER);
  }

  @Test public void shouldGetBinCheckFromNetwork_creditCardNotInCache() {

    mCheckBinFromNetworkInteractor.checkBinMSL(ANY_CREDIT_CARD_NUMBER, mOnCheckBinListener);

    verify(mBinCheckNetController).checkBin(mOnRequestDataListener.capture(),
        eq(ANY_CREDIT_CARD_NUMBER));
  }
}