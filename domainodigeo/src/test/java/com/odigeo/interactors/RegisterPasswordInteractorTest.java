package com.odigeo.interactors;

import com.odigeo.data.entity.userData.User;
import com.odigeo.data.net.controllers.UserNetControllerInterface;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.session.SessionController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class RegisterPasswordInteractorTest {

  private static final String USER = "pablo.azana@odigeo.com";
  private static final String PASSWORD = "123456a";

  private UserNetControllerInterface userNetController;
  private SessionController sessionController;
  private OnAuthRequestDataListener<User> userRegisterPasswordListener;
  private RegisterPasswordInteractor registerPasswordInteractor;

  @Before public void setUp() {
    userNetController = Mockito.mock(UserNetControllerInterface.class);
    sessionController = Mockito.mock(SessionController.class);
    userRegisterPasswordListener = Mockito.mock(OnAuthRequestDataListener.class);
    registerPasswordInteractor =
        new RegisterPasswordInteractor(userNetController, sessionController);
  }

  @Test public void registerUserPasswordWhenUserOkTest() {
    registerPasswordInteractor.registerUserPassword(userRegisterPasswordListener, USER, PASSWORD,
        true);
    verify(userNetController, times(1)).registerUser(any(OnRequestDataListener.class),
        any(User.class));
  }
}
