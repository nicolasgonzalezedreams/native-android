package com.odigeo.interactors;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.xsell.CrossSelling;
import com.odigeo.data.repositories.CrossSellingRepository;
import com.odigeo.matchers.CrossSellingItemTypeMatcher;
import java.util.ArrayList;
import java.util.List;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.odigeo.data.entity.booking.Booking.TRIP_TYPE_MULTI_SEGMENT;
import static com.odigeo.data.entity.booking.Booking.TRIP_TYPE_ONE_WAY;
import static com.odigeo.presenter.model.CrossSellingType.TYPE_CARS;
import static com.odigeo.presenter.model.CrossSellingType.TYPE_GROUND;
import static com.odigeo.presenter.model.CrossSellingType.TYPE_GUIDE;
import static com.odigeo.presenter.model.CrossSellingType.TYPE_HOTEL;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class GetCrossSellingForBookingInteractorTest {

  @Mock private CrossSellingRepository crossSellingRepository;
  @Mock private Booking booking;

  private GetCrossSellingForBookingInteractor interactor;
  private List<CrossSelling> crossSellings;

  @Before public void setup() {
    interactor = new GetCrossSellingForBookingInteractor(crossSellingRepository);
    crossSellings = new ArrayList<CrossSelling>() {{
      add(new CrossSelling(booking, true, "Label", "Title", TYPE_CARS, "Subtitle"));
      add(new CrossSelling(booking, true, "Label", "Title", TYPE_HOTEL, "Subtitle"));
      add(new CrossSelling(booking, true, "Label", "Title", TYPE_GROUND, "Subtitle"));
      add(new CrossSelling(booking, true, "Label", "Title", TYPE_GUIDE, "Subtitle"));
    }};
    when(crossSellingRepository.getCrossSellingForBooking(booking)).thenReturn(crossSellings);
  }

  @Test public void testGetCrossSellingForBookingNoFiltersApplied() {
    when(crossSellingRepository.shouldShowGroundTransportation()).thenReturn(true);
    when(crossSellingRepository.getCrossSellingArrivalGuide(booking)).thenReturn(true);
    when(booking.getTripType()).thenReturn(TRIP_TYPE_ONE_WAY);

    List<CrossSelling> unfileteredCrossSellings = interactor.getCrossSellingForBooking(booking);

    assertThat(unfileteredCrossSellings.size(), is(equalTo(crossSellings.size())));

    verify(crossSellingRepository).getCrossSellingForBooking(booking);
  }

  @Test public void testGetCrossSellingForBookingDontDisplayGroundTransportation() {
    when(crossSellingRepository.shouldShowGroundTransportation()).thenReturn(false);
    when(crossSellingRepository.getCrossSellingArrivalGuide(booking)).thenReturn(true);
    when(booking.getTripType()).thenReturn(TRIP_TYPE_ONE_WAY);

    List<CrossSelling> unfileteredCrossSellings = interactor.getCrossSellingForBooking(booking);

    assertThat(unfileteredCrossSellings,
        not(hasItem(new CrossSellingItemTypeMatcher(TYPE_GROUND))));

    verify(crossSellingRepository).getCrossSellingForBooking(booking);
  }

  @Test public void testGetCrossSellingForBookingDontDisplayArrivalGuideIfNotAvailable() {
    when(crossSellingRepository.shouldShowGroundTransportation()).thenReturn(true);
    when(crossSellingRepository.getCrossSellingArrivalGuide(booking)).thenReturn(false);
    when(booking.getTripType()).thenReturn(TRIP_TYPE_ONE_WAY);

    List<CrossSelling> unfileteredCrossSellings = interactor.getCrossSellingForBooking(booking);

    assertThat(unfileteredCrossSellings, not(hasItem(new CrossSellingItemTypeMatcher(TYPE_GUIDE))));

    verify(crossSellingRepository).getCrossSellingForBooking(booking);
  }

  @Test public void testGetCrossSellingForBookingDontDisplayArrivalGuideIfMultiSegment() {
    when(crossSellingRepository.shouldShowGroundTransportation()).thenReturn(true);
    when(crossSellingRepository.getCrossSellingArrivalGuide(booking)).thenReturn(true);
    when(booking.getTripType()).thenReturn(TRIP_TYPE_MULTI_SEGMENT);

    List<CrossSelling> unfileteredCrossSellings = interactor.getCrossSellingForBooking(booking);

    assertThat(unfileteredCrossSellings, not(hasItem(new CrossSellingItemTypeMatcher(TYPE_GUIDE))));

    verify(crossSellingRepository).getCrossSellingForBooking(booking);
  }

  @Test public void testGetCrossSellingForBookingShouldAlwaysShowHotelsAndCars() {
    when(crossSellingRepository.shouldShowGroundTransportation()).thenReturn(false);
    when(crossSellingRepository.getCrossSellingArrivalGuide(booking)).thenReturn(false);
    when(booking.getTripType()).thenReturn(TRIP_TYPE_MULTI_SEGMENT);

    List<CrossSelling> unfileteredCrossSellings = interactor.getCrossSellingForBooking(booking);

    assertThat(unfileteredCrossSellings, allOf(hasItem(new CrossSellingItemTypeMatcher(TYPE_HOTEL)),
        hasItem(new CrossSellingItemTypeMatcher(TYPE_CARS))));

    verify(crossSellingRepository).getCrossSellingForBooking(booking);
  }

  @Test public void testGetCrossSellingForPastBookingReturnsEmpty() {
    when(booking.isPastBooking()).thenReturn(true);

    List<CrossSelling> crossSellings = interactor.getCrossSellingForBooking(booking);

    assertThat(crossSellings.size(), is(0));

  }
}
