package com.odigeo.interactors;

import com.odigeo.data.net.controllers.SendMailNetControllerInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.interactors.provider.MarketProviderInterface;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SendFeedbackInteractorTest {

  private static final String TEST_SUBJECT = "Feedback from the App";
  private static final String TEST_MESSAGE = "Test message";
  private static final String TEST_EMAIL = "email@test.com";

  private SendMailNetControllerInterface mSendMailNetController;
  private MarketProviderInterface mMarketProvider;
  private SendFeedbackInteractor mSendFeedbackInteractor;
  private OnRequestDataListener<Boolean> mSendFeedbackListener;
  private List<String> mScreenshots;

  @Before public void setUp() {
    mSendMailNetController = Mockito.mock(SendMailNetControllerInterface.class);
    mMarketProvider = Mockito.mock(MarketProviderInterface.class);
    mSendFeedbackInteractor = new SendFeedbackInteractor(mSendMailNetController, mMarketProvider);
    mSendFeedbackListener = Mockito.mock(OnRequestDataListener.class);
    mScreenshots = new ArrayList<>();
  }

  @Test public void sendFeedbackTest() {
    when(mMarketProvider.getFeedbackEmail()).thenReturn(TEST_EMAIL);
    mSendFeedbackInteractor.sendFeedback(mSendFeedbackListener, TEST_EMAIL, TEST_SUBJECT,
        TEST_MESSAGE, mScreenshots);
    verify(mSendMailNetController, times(1)).sendMail(any(OnRequestDataListener.class),
        any(String.class), any(String.class), any(String.class), any(String.class),
        any(List.class));
  }
}