package com.odigeo.interactors;

import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.db.helper.BookingsHandlerInterface;
import com.odigeo.data.entity.booking.Booking;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class BookingInteractorTest {

  private static long BOOKING_ID = 12345L;
  private BookingDBDAOInterface mBookingDBDAOInterface;
  private BookingsHandlerInterface mBookingsHandlerInterface;
  private Booking bookingExpected;

  @Before public void setUp() {
    mBookingDBDAOInterface = Mockito.mock(BookingDBDAOInterface.class);
    mBookingsHandlerInterface = Mockito.mock(BookingsHandlerInterface.class);
    bookingExpected = configureBookingExpected();
  }

  private Booking configureBookingExpected() {
    Booking booking = new Booking();
    booking.setBookingId(BOOKING_ID);
    return booking;
  }

  @Test public void getBookingsByIdSuccessfully() {
    Mockito.when(mBookingDBDAOInterface.getBooking(Mockito.anyLong())).thenReturn(bookingExpected);
    BookingInteractor bookingInteractor =
        new BookingInteractor(mBookingDBDAOInterface, mBookingsHandlerInterface);
    Booking bookingActual = bookingInteractor.getBookingById(BOOKING_ID);
    Assert.assertEquals(bookingExpected, bookingActual);
    Mockito.verify(mBookingsHandlerInterface).completeBooking(Mockito.any(Booking.class));
  }

  @Test public void getBookingsByIdUnSuccessfully() {
    Mockito.when(mBookingDBDAOInterface.getBooking(Mockito.anyLong())).thenReturn(null);
    BookingInteractor bookingInteractor =
        new BookingInteractor(mBookingDBDAOInterface, mBookingsHandlerInterface);
    Booking bookingActual = bookingInteractor.getBookingById(BOOKING_ID);
    Assert.assertNull(bookingActual);
    Mockito.verify(mBookingsHandlerInterface, Mockito.never())
        .completeBooking(Mockito.any(Booking.class));
  }
}
