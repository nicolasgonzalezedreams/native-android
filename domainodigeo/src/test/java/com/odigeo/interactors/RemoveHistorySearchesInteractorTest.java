package com.odigeo.interactors;

import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.net.controllers.SearchesNetControllerInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by matiasdirusso on 21/11/16.
 */
public class RemoveHistorySearchesInteractorTest {

  @Mock private RemoveHistorySearchesInteractor mRemoveHistorySearchesInteractor;
  @Mock private SearchesNetControllerInterface mSearchesNetController;
  @Mock private SearchesHandlerInterface mSearchesHandler;
  @Mock private CheckUserCredentialsInteractor mUserSessionInteractor;
  @Mock private OnRequestDataListener mOnRequestDataListener;
  @Captor private ArgumentCaptor<OnRequestDataListener<Boolean>> mRequestListener;
  @Mock private StoredSearch mStoredSearch;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    mRemoveHistorySearchesInteractor =
        new RemoveHistorySearchesInteractor(mSearchesNetController, mSearchesHandler,
            mUserSessionInteractor);
  }

  @Test public void removeAllSearchesUserLoggedIn() throws Exception {
    when(mUserSessionInteractor.isUserLogin()).thenReturn(true);
    when(mStoredSearch.isSynchronized()).thenReturn(true);
    when(mStoredSearch.getTripType()).thenReturn(StoredSearch.TripType.R);
    List<StoredSearch> storedSearchList = new ArrayList<>();
    storedSearchList.add(mStoredSearch);
    when(mSearchesHandler.getCompleteSearchesFromDB()).thenReturn(storedSearchList);
    mRemoveHistorySearchesInteractor.removeAllSearches(mOnRequestDataListener,
        StoredSearch.TripType.R);
    verify(mSearchesNetController).deleteUserSearch(mRequestListener.capture(), eq(mStoredSearch));
    verify(mSearchesHandler).removeAllSearchesLocally(StoredSearch.TripType.R);
  }

  @Test public void removeAllSearchesUserNotLoggedIn() throws Exception {
    when(mUserSessionInteractor.isUserLogin()).thenReturn(false);
    mRemoveHistorySearchesInteractor.removeAllSearches(mOnRequestDataListener,
        StoredSearch.TripType.R);
    verify(mSearchesHandler).removeAllSearchesLocally(StoredSearch.TripType.R);
  }
}