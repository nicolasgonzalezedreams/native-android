package com.odigeo.interactors;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.request.ModifyShoppingCartRequest;
import com.odigeo.data.net.controllers.RemoveProductsNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.trustdefender.TrustDefenderController;
import com.odigeo.presenter.listeners.OnRemoveProductsFromShoppingCartListener;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.odigeo.data.preferences.PreferencesControllerInterface.JSESSION_COOKIE;
import static com.odigeo.test.mock.MocksProvider.providesPaymentMocks;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class RemoveProductsFromShoppingCartInteractorTest {

  private static String ERROR_REMOVING_INSURANCE = "COULD NOT REMOVE INSURANCE";

  @Mock private RemoveProductsNetControllerInterface mRemoveProductsNetControllerInterface;
  @Mock private TrustDefenderController trustDefenderController;
  @Mock private PreferencesControllerInterface preferencesController;
  @Mock private ModifyShoppingCartRequest mModifyShoppingCartRequest;
  @Mock private OnRemoveProductsFromShoppingCartListener mOnRemoveProductsFromShoppingCartListener;
  @Mock private CreateShoppingCartResponse mCreateShoppingCartResponse;
  @Captor private ArgumentCaptor<OnRequestDataListener<CreateShoppingCartResponse>>
      mOnRequestDataListener;

  private RemoveProductsFromShoppingCartInteractor mRemoveProductsFromShoppingCartInteractor;
  private MslError mMslRandomError = MslError.UNK_001;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    mRemoveProductsFromShoppingCartInteractor =
        new RemoveProductsFromShoppingCartInteractor(mRemoveProductsNetControllerInterface,
            trustDefenderController, preferencesController);
  }

  @Test public void shouldRemoveInsuranceFromShoppingCart() {
    when(mCreateShoppingCartResponse.getShoppingCart()).thenReturn(
        providesPaymentMocks().provideShoppingCart());
    mRemoveProductsFromShoppingCartInteractor.removeProducts(mModifyShoppingCartRequest,
        mOnRemoveProductsFromShoppingCartListener);
    verify(mRemoveProductsNetControllerInterface).removeProductsFromShoppingCart(
        mOnRequestDataListener.capture(), eq(mModifyShoppingCartRequest));
    mOnRequestDataListener.getValue().onResponse(mCreateShoppingCartResponse);
    verify(preferencesController).getStringValue(JSESSION_COOKIE);
    verify(trustDefenderController).sendFingerPrint(anyString(), anyString());
    verifyNoMoreInteractions(mRemoveProductsNetControllerInterface);
  }

  @Test public void shouldNotRemoveInsuranceFromShoppingCart() {
    mRemoveProductsFromShoppingCartInteractor.removeProducts(mModifyShoppingCartRequest,
        mOnRemoveProductsFromShoppingCartListener);
    verify(mRemoveProductsNetControllerInterface).removeProductsFromShoppingCart(
        mOnRequestDataListener.capture(), eq(mModifyShoppingCartRequest));
    mOnRequestDataListener.getValue().onError(mMslRandomError, ERROR_REMOVING_INSURANCE);
    verifyNoMoreInteractions(mRemoveProductsNetControllerInterface);
  }
}
