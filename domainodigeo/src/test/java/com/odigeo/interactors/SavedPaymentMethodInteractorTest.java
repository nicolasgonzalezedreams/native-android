package com.odigeo.interactors;

import com.odigeo.data.entity.userData.InsertCreditCardRequest;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.net.controllers.SavePaymentMethodNetController;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.data.tracker.TrackerControllerInterface;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class SavedPaymentMethodInteractorTest {

  private static String ERROR_INSERTING_CREDIT_CARD = "COULD NOT ADD INSERT";

  @Mock private SavePaymentMethodNetController savePaymentMethodNetController;
  @Mock private TrackerControllerInterface trackerController;
  @Mock private CrashlyticsController crashlyticsController;
  @Captor private ArgumentCaptor<OnRequestDataListener<User>> onRequestDataListener;

  private MslError mMslRandomError = MslError.UNK_001;
  private SavePaymentMethodInteractor savePaymentMethodInteractor;
  private InsertCreditCardRequest randomCreditCard;
  private User randomUser;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    savePaymentMethodInteractor =
        new SavePaymentMethodInteractor(savePaymentMethodNetController, trackerController,
            crashlyticsController);
    randomCreditCard =
        new InsertCreditCardRequest("123456789", "12", "21", "jimy", 0, 0, 0, false, "VI");
  }

  @Test public void shouldInsertCreditCard() {
    savePaymentMethodInteractor.savePaymentMethod(randomCreditCard);
    verify(savePaymentMethodNetController).savePaymentMethod(onRequestDataListener.capture(),
        eq(randomCreditCard));
    onRequestDataListener.getValue().onResponse(randomUser);
    verifyNoMoreInteractions(savePaymentMethodNetController);
  }

  @Test public void shouldNotInsertCreditCard() {
    savePaymentMethodInteractor.savePaymentMethod(randomCreditCard);
    verify(savePaymentMethodNetController).savePaymentMethod(onRequestDataListener.capture(),
        eq(randomCreditCard));
    onRequestDataListener.getValue().onError(mMslRandomError, ERROR_INSERTING_CREDIT_CARD);
    verify(crashlyticsController).trackNonFatal(any(Exception.class));
    verify(crashlyticsController).setString(CrashlyticsController.INSERT_CREDITCARD_METHOD,
        CrashlyticsController.POST_METHOD);
    verifyNoMoreInteractions(savePaymentMethodNetController);
  }
}

