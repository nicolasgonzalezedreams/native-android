package com.odigeo.interactors;

import com.odigeo.data.net.controllers.VisitUserNetController;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.dataodigeo.session.CredentialsInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.dataodigeo.session.UserInfoInterface;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class VisitUserInteractorTest {

  private final long userId = 1;
  private VisitUserInteractor visitUserInteractor;
  @Mock private VisitUserNetController visitUserNetController;
  @Mock private SessionController sessionController;
  @Mock private CrashlyticsController crashlyticsController;
  @Mock private CredentialsInterface credentialsInterface;
  @Mock private UserInfoInterface userInfoInterface;
  @Captor private ArgumentCaptor<OnRequestDataListener<String>> onRequestDataListener;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    visitUserInteractor =
        new VisitUserInteractor(visitUserNetController, sessionController, crashlyticsController);
  }

  @Test public void shouldLoginVisitUser() {
    when(sessionController.getCredentials()).thenReturn(credentialsInterface);
    when(sessionController.getUserInfo()).thenReturn(userInfoInterface);
    when(userInfoInterface.getUserId()).thenReturn(userId);
    visitUserInteractor.loginVisitUser();
    verify(visitUserNetController).loginVisitUser(onRequestDataListener.capture(), anyLong());
    onRequestDataListener.getValue().onResponse("");
  }

  @Test public void shouldLogoutVisitUser() {
    when(sessionController.getUserInfo()).thenReturn(userInfoInterface);
    when(userInfoInterface.getUserId()).thenReturn(userId);
    visitUserInteractor.logoutVisitUser();
    verify(visitUserNetController).logoutVisitUser(onRequestDataListener.capture(), anyLong());
    onRequestDataListener.getValue().onResponse("");
  }

  @After public void tearDown() {
    verifyNoMoreInteractions(visitUserNetController);
  }
}
