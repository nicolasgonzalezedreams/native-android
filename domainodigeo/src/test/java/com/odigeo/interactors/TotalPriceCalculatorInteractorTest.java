package com.odigeo.interactors;

import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.PricingBreakdown;
import com.odigeo.data.entity.shoppingCart.ShoppingCart;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.test.mock.MocksProvider;
import java.math.BigDecimal;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

public class TotalPriceCalculatorInteractorTest {

  @Mock private ShoppingCart shoppingCart;
  @Mock private PricingBreakdown pricingBreakdown;
  @Mock private ShoppingCartCollectionOption shoppingCartCollectionOption;

  private TotalPriceCalculatorInteractor totalPriceCalculatorInteractor;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    totalPriceCalculatorInteractor = new TotalPriceCalculatorInteractor();
  }

  @Test public void
      shouldReturnCorrectTotalPriceWithoutCollectionMethodAndNoMembershipAndEmptyPriceBreakdown() {
    pricingBreakdown = null;
    when(shoppingCart.getTotalPrice()).thenReturn(BigDecimal.valueOf(75d));
    double totalPrice = totalPriceCalculatorInteractor.getTotalPrice(shoppingCart,
        pricingBreakdown, BigDecimal.ZERO, null);
    assertEquals(totalPrice, 75d);
  }

  @Test public void shouldReturnCorrectTotalPriceWithoutCollectionMethodAndNoMembership() {
    when(pricingBreakdown.getPricingBreakdownSteps()).thenReturn(MocksProvider
        .providesPaymentMocks().providePricingBreakdownStepsWithoutMembership());
    double totalPrice = totalPriceCalculatorInteractor.getTotalPrice(shoppingCart,
        pricingBreakdown, BigDecimal.ZERO, null);
    assertEquals(totalPrice, 123d);
  }

  @Test public void shouldReturnCorrectPriceWithWithoutCollectionMethodAndMembership() {
    when(pricingBreakdown.getPricingBreakdownSteps()).thenReturn(MocksProvider
        .providesPaymentMocks().providePricingBreakdownStepsWithMembership());

    double totalPrice = totalPriceCalculatorInteractor.getTotalPrice(shoppingCart,
        pricingBreakdown, BigDecimal.ZERO, null);

    double membershipPerkValue = MocksProvider.providesPaymentMocks()
        .provideMembershipPerksPricingBreakdownItem().getPriceItemAmount().doubleValue();

    assertEquals(totalPrice, 123 + membershipPerkValue);
  }

  @Test public void shouldReturnCorrectPriceWithDefaultCollectionMethod() {
    when(pricingBreakdown.getPricingBreakdownSteps()).thenReturn(MocksProvider
        .providesPaymentMocks().providePricingBreakdownStepsWithoutMembership());
    when(shoppingCartCollectionOption.getFee()).thenReturn(BigDecimal.valueOf(7d));
    totalPriceCalculatorInteractor.setCollectionMethodType(CollectionMethodType.CREDITCARD);
    totalPriceCalculatorInteractor.setShoppingCartCollectionOption(shoppingCartCollectionOption);

    double totalPrice = totalPriceCalculatorInteractor.getTotalPrice(shoppingCart,
        pricingBreakdown, BigDecimal.valueOf(7), null);

    assertEquals(totalPrice, 130d);
  }

  @Test public void shouldReturnCorrectPriceWithCollectionMethodWithPrice() {
    when(pricingBreakdown.getPricingBreakdownSteps()).thenReturn(MocksProvider
        .providesPaymentMocks().providePricingBreakdownStepsWithoutMembership());
    totalPriceCalculatorInteractor.setCollectionMethodType(CollectionMethodType.CREDITCARD);

    double totalPrice = totalPriceCalculatorInteractor.getTotalPrice(shoppingCart,
        pricingBreakdown, BigDecimal.valueOf(7), null);

    assertEquals(totalPrice, 130d);
  }
}
