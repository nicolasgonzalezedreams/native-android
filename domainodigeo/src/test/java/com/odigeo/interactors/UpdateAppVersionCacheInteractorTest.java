package com.odigeo.interactors;

import com.odigeo.data.preferences.PackageController;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class UpdateAppVersionCacheInteractorTest {

  @Mock private PreferencesControllerInterface preferencesController;
  @Mock private PackageController packageController;

  private UpdateAppVersionCacheInteractor updateAppVersionCacheInteractor;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    updateAppVersionCacheInteractor =
        new UpdateAppVersionCacheInteractor(preferencesController, packageController);
  }

  @Test public void shouldBeLastVersion() {
    when(preferencesController.getIntValue(anyString())).thenReturn(1);
    when(packageController.getApplicationVersionCode()).thenReturn(1);
    assertTrue(updateAppVersionCacheInteractor.isLastVersion());
  }

  @Test public void shouldNotBeLastVersion() {
    when(preferencesController.getIntValue(anyString())).thenReturn(1);
    when(packageController.getApplicationVersionCode()).thenReturn(2);
    assertFalse(updateAppVersionCacheInteractor.isLastVersion());
  }
}
