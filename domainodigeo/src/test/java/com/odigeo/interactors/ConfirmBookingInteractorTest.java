package com.odigeo.interactors;

import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.data.entity.shoppingCart.request.BookingRequest;
import com.odigeo.data.net.controllers.ConfirmBookingNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.presenter.listeners.OnConfirmBookingListener;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class ConfirmBookingInteractorTest {

  private static final String ERROR_CONFIRMING_BOOKING = "COULD CONFIRM BOOKING";

  @Mock private ConfirmBookingNetControllerInterface mConfirmBookingNetController;
  @Mock private BookingRequest mBookingRequest;
  @Mock private OnConfirmBookingListener mOnConfirmBookingListener;
  @Mock private BookingResponse mBookingResponse;
  @Captor private ArgumentCaptor<OnRequestDataListener<BookingResponse>> mOnRequestDataListener;

  private ConfirmBookingInteractor mConfirmBookingInteractor;
  private MslError mMslRandomError = MslError.UNK_001;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    mConfirmBookingInteractor = new ConfirmBookingInteractor(mConfirmBookingNetController);
  }

  @Test public void shouldConfirmBooking() {
    mConfirmBookingInteractor.confirmBooking(mBookingRequest, mOnConfirmBookingListener);
    verify(mConfirmBookingNetController).confirmBooking(mOnRequestDataListener.capture(),
        eq(mBookingRequest));
    mOnRequestDataListener.getValue().onResponse(mBookingResponse);
    verifyNoMoreInteractions(mConfirmBookingNetController);
  }

  @Test public void shouldNotConfirmBooking() {
    mConfirmBookingInteractor.confirmBooking(mBookingRequest, mOnConfirmBookingListener);
    verify(mConfirmBookingNetController).confirmBooking(mOnRequestDataListener.capture(),
        eq(mBookingRequest));
    mOnRequestDataListener.getValue().onError(mMslRandomError, ERROR_CONFIRMING_BOOKING);
    verifyNoMoreInteractions(mConfirmBookingNetController);
  }
}