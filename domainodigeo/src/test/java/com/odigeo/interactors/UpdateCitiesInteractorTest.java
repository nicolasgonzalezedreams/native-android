package com.odigeo.interactors;

import com.odigeo.data.db.helper.CitiesHandlerInterface;
import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.net.controllers.AutoCompleteNetControllerInterface;
import com.odigeo.data.net.listener.OnRequestDataListListener;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.test.mock.mocks.CityMocks;
import com.odigeo.test.mock.mocks.StoredSearchMocks;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class UpdateCitiesInteractorTest {

  private static String cityIATA1 = "PAR";
  private static String cityIATA2 = "MAD";

  private static String locale = "ES";
  private static String website = "Web";

  @Mock private AutoCompleteNetControllerInterface autoCompleteNetControllerInterface;
  @Mock private CitiesHandlerInterface citiesHandlerInterface;
  @Mock private MarketProviderInterface marketProviderInterface;
  @Mock private SearchesHandlerInterface searchesHandlerInterface;

  @Captor private ArgumentCaptor<OnRequestDataListListener<City>> onRequestDataListener;

  private StoredSearchMocks storedSearchMocks;

  private List<StoredSearch> storedSearches;

  private UpdateCitiesInteractor updateCitiesInteractor;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    storedSearchMocks = new StoredSearchMocks();
    storedSearches = new ArrayList<>();
    storedSearches.add(storedSearchMocks.provideStoredSearchWithSegment());
    updateCitiesInteractor =
        new UpdateCitiesInteractor(autoCompleteNetControllerInterface, citiesHandlerInterface,
            marketProviderInterface, searchesHandlerInterface);
  }

  @Test public void shouldNotRequestCitiesAlreadyStoredInTheDB() {
    when(searchesHandlerInterface.getCompleteSearchesFromDB()).thenReturn(storedSearches);
    when(citiesHandlerInterface.getCity(cityIATA1)).thenReturn(
        CityMocks.provideCityWithIATA(cityIATA1));
    when(citiesHandlerInterface.getCity(cityIATA2)).thenReturn(
        CityMocks.provideCityWithIATA(cityIATA2));

    updateCitiesInteractor.updateCities();

    verifyZeroInteractions(autoCompleteNetControllerInterface);
  }

  @Test public void shouldRequestCitiesNotStoredInTheDB() {
    when(searchesHandlerInterface.getCompleteSearchesFromDB()).thenReturn(storedSearches);
    when(citiesHandlerInterface.getCity(cityIATA1)).thenReturn(null);
    when(marketProviderInterface.getLocale()).thenReturn(locale);
    when(marketProviderInterface.getWebsite()).thenReturn(website);

    updateCitiesInteractor.updateCities();

    verify(autoCompleteNetControllerInterface).getDestination(eq(cityIATA1), eq(locale),
        eq(website), onRequestDataListener.capture());

    List<City> cityList = new ArrayList<>();
    cityList.add(CityMocks.provideCityWithIATA(cityIATA1));

    onRequestDataListener.getValue().onResponse(cityList);

    verify(citiesHandlerInterface).storeCity(cityList.get(0));
  }
}
