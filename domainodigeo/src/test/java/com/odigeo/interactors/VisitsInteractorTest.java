package com.odigeo.interactors;

import com.odigeo.data.entity.session.Visit;
import com.odigeo.data.entity.session.request.VisitRequest;
import com.odigeo.data.net.controllers.VisitsNetController;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.presenter.listeners.VisitInteractorListener;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class VisitsInteractorTest {

  @Mock private VisitsNetController visitsNetController;
  @Mock private PreferencesControllerInterface preferencesController;
  @Mock private Visit visit;
  @Mock private VisitInteractorListener visitInteractorListener;
  @Captor private ArgumentCaptor<OnRequestDataListener<Visit>> onRequestDataListener;


  private VisitsInteractor visitsInteractor;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    visitsInteractor = new VisitsInteractor(visitsNetController, preferencesController);
  }

  @Test public void shouldFetchTestAssignments() {
    Map map = new HashMap<>();
    map.put("A", 1);

    doReturn(map).when(visit).getTestAssignments();

    visitsInteractor.fetch(visitInteractorListener);

    verify(visitsNetController).getVisits(onRequestDataListener.capture(), any(VisitRequest.class));
    onRequestDataListener.getValue().onResponse(visit);
    verify(visitInteractorListener, times(1)).onSuccess();
    verify(preferencesController, times(1)).clearTestAssignments();
    verify(preferencesController, times(1)).saveTestAssignment(anyString(), anyInt());
  }

  @Test public void shouldFetchDimensions() {
    Map map = new HashMap<>();
    map.put("A", 1);

    doReturn(map).when(visit).getDimensions();

    visitsInteractor.fetch(visitInteractorListener);

    verify(visitsNetController).getVisits(onRequestDataListener.capture(), any(VisitRequest.class));
    onRequestDataListener.getValue().onResponse(visit);
    verify(visitInteractorListener, times(1)).onSuccess();
    verify(preferencesController, times(1)).clearDimensions();
    verify(preferencesController, times(1)).saveDimensions(anyString(), anyInt());
  }

  @Test public void shouldCallListener() {
    visitsInteractor.fetch(visitInteractorListener);

    verify(visitsNetController).getVisits(onRequestDataListener.capture(), any(VisitRequest.class));
    onRequestDataListener.getValue().onResponse(visit);
    verify(visitInteractorListener, times(1)).onSuccess();
  }



  @After public void tearDown() {
    verifyNoMoreInteractions(visitsNetController);
  }
}
