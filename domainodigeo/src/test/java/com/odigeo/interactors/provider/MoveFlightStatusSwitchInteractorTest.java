package com.odigeo.interactors.provider;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.data.net.controllers.NotificationNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.CheckUserCredentialsInteractor;
import com.odigeo.interactors.MoveFlightStatusSwitchInteractor;
import com.odigeo.interactors.MyTripsInteractor;
import com.odigeo.presenter.listeners.FlightStatusSwitchListener;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class MoveFlightStatusSwitchInteractorTest {

  private static final String GCM_TOKEN = "GCM_TOKEN";
  private static final MslError MSL_ERROR = MslError.UNK_001;
  private static final String REQUEST_ERROR_MESSAGE = "Could not add search";
  @Mock private NotificationNetControllerInterface mNotificationNetController;
  @Mock private SessionController mSessionController;
  @Mock private CheckUserCredentialsInteractor mUserCredentialsInteractor;
  @Mock private OnAuthRequestDataListener<Boolean> mInteractorCallback;
  @Mock private OnAuthRequestDataListener<MyTripsProvider> getBookingInteractor;
  @Mock private FlightStatusSwitchListener flightStatusSwitchListener;
  @Mock private MyTripsInteractor mMyTripsInteractor;
  @Mock private LocalizableProvider localizableProvider;
  @Captor private ArgumentCaptor<OnAuthRequestDataListener<Boolean>> mOnAuthRequestDataListener;
  @Captor private ArgumentCaptor<OnRequestDataListener<Boolean>> mOnRequestDataListener;
  @Captor private ArgumentCaptor<OnRequestDataListener<MyTripsProvider>> onGetBookingListener;
  private MoveFlightStatusSwitchInteractor mMoveFlightStatusSwitchInteractor;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);

    mMoveFlightStatusSwitchInteractor =
        new MoveFlightStatusSwitchInteractor(mNotificationNetController, mSessionController,
            mUserCredentialsInteractor, mMyTripsInteractor, localizableProvider);
  }

  @Test public void turnOnNotificationsSuccessTest() {
    when(mUserCredentialsInteractor.isUserLogin()).thenReturn(true);
    when(mSessionController.getGcmToken()).thenReturn(GCM_TOKEN);

    mMoveFlightStatusSwitchInteractor.turnOnNotifications(mInteractorCallback);

    verify(mNotificationNetController).enableNotifications(mOnRequestDataListener.capture(),
        Matchers.eq(GCM_TOKEN));
    mOnRequestDataListener.getValue().onResponse(true);
    verify(mInteractorCallback).onResponse(true);
  }

  @Test public void turnOnNotificationsFailTest() {
    when(mUserCredentialsInteractor.isUserLogin()).thenReturn(true);
    when(mSessionController.getGcmToken()).thenReturn(GCM_TOKEN);

    mMoveFlightStatusSwitchInteractor.turnOnNotifications(mInteractorCallback);

    verify(mNotificationNetController).enableNotifications(mOnRequestDataListener.capture(),
        Matchers.eq(GCM_TOKEN));
    mOnRequestDataListener.getValue().onError(MSL_ERROR, REQUEST_ERROR_MESSAGE);
    verify(mSessionController).invalidateGcmToken();
    verify(mInteractorCallback).onError(MSL_ERROR, REQUEST_ERROR_MESSAGE);
  }

  @Test public void turnOffNotificationsTestSuccess() {
    when(mUserCredentialsInteractor.isUserLogin()).thenReturn(true);
    when(mSessionController.getGcmToken()).thenReturn(GCM_TOKEN);

    mMoveFlightStatusSwitchInteractor.turnOffNotifications(mInteractorCallback);

    verify(mNotificationNetController).disableNotifications(mOnAuthRequestDataListener.capture(),
        Matchers.eq(GCM_TOKEN));
    mOnAuthRequestDataListener.getValue().onResponse(true);
    verify(mInteractorCallback).onResponse(true);
  }

  @Test public void turnOffNotificationsTestFail() {
    when(mUserCredentialsInteractor.isUserLogin()).thenReturn(true);
    when(mSessionController.getGcmToken()).thenReturn(GCM_TOKEN);

    mMoveFlightStatusSwitchInteractor.turnOffNotifications(mInteractorCallback);
    verify(mNotificationNetController).disableNotifications(mOnAuthRequestDataListener.capture(),
        Matchers.eq(GCM_TOKEN));
    mOnAuthRequestDataListener.getValue().onError(MSL_ERROR, REQUEST_ERROR_MESSAGE);
    verify(mInteractorCallback).onError(MSL_ERROR, REQUEST_ERROR_MESSAGE);
  }

  @Test public void turnOffNotificationsTestAuthError() {
    when(mUserCredentialsInteractor.isUserLogin()).thenReturn(true);
    when(mSessionController.getGcmToken()).thenReturn(GCM_TOKEN);

    mMoveFlightStatusSwitchInteractor.turnOffNotifications(mInteractorCallback);
    verify(mNotificationNetController).disableNotifications(mOnAuthRequestDataListener.capture(),
        Matchers.eq(GCM_TOKEN));
    mOnAuthRequestDataListener.getValue().onAuthError();
    verify(mInteractorCallback).onResponse(false);
  }

  @Test public void shouldHideNotificationWhenControlKeyEmptyTest() {
    String controlKey = "controlKey";
    MyTripsProvider provider = mock(MyTripsProvider.class);
    List<Booking> bookings = new ArrayList<Booking>() {{
      add(new Booking());
    }};

    when(localizableProvider.getString(controlKey)).thenReturn("");
    when(mUserCredentialsInteractor.isUserLogin()).thenReturn(true);
    when(provider.getBookingList()).thenReturn(bookings);

    mMoveFlightStatusSwitchInteractor.handleFlightStatusSwitchVisibility(flightStatusSwitchListener,
        controlKey);

    verify(mMyTripsInteractor).getBookings(onGetBookingListener.capture());
    onGetBookingListener.getValue().onResponse(provider);

    verify(flightStatusSwitchListener, never()).showFlightStatusSwitch();
  }

  @Test public void shouldShowNotificationWhenControlKeyIsNotEmptyTest() {
    String controlKey = "controlKey";
    MyTripsProvider provider = mock(MyTripsProvider.class);
    List<Booking> bookings = new ArrayList<Booking>() {{
      add(new Booking());
    }};

    when(localizableProvider.getString(controlKey)).thenReturn("notEmpty");
    when(mUserCredentialsInteractor.isUserLogin()).thenReturn(true);
    when(provider.getBookingList()).thenReturn(bookings);

    mMoveFlightStatusSwitchInteractor.handleFlightStatusSwitchVisibility(flightStatusSwitchListener,
        controlKey);

    verify(mMyTripsInteractor).getBookings(onGetBookingListener.capture());
    onGetBookingListener.getValue().onResponse(provider);

    verify(flightStatusSwitchListener, times(1)).showFlightStatusSwitch();
  }

  @After public void tearDown() {
    verifyNoMoreInteractions(mNotificationNetController);
  }
}
