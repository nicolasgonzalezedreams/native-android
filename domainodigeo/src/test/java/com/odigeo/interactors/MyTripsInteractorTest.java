package com.odigeo.interactors;

import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.db.helper.BookingsHandlerInterface;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.tools.DateHelperInterface;
import java.util.ArrayList;
import java.util.Calendar;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class MyTripsInteractorTest {

  @Mock private BookingDBDAOInterface bookingDBDAOInterface;
  @Mock private BookingsHandlerInterface bookingsHandlerInterface;
  @Mock private DateHelperInterface dateHelperInterface;

  private MyTripsInteractor interactor;

  @Before public void setup() {
    interactor =
        new MyTripsInteractor(bookingDBDAOInterface, bookingsHandlerInterface, dateHelperInterface);
  }

  @Test public void testGetActiveOneWayBookingsReturnsActiveBookings() {
    Calendar calendar = Calendar.getInstance();

    calendar.add(Calendar.DATE, 1);
    final Booking activeBooking = new Booking();
    activeBooking.setDepartureFirstLeg(calendar.getTimeInMillis());
    activeBooking.setTripType(Booking.TRIP_TYPE_ONE_WAY);

    calendar.add(Calendar.DATE, -2);
    final Booking inActiveBooking = new Booking();
    inActiveBooking.setDepartureFirstLeg(calendar.getTimeInMillis());
    inActiveBooking.setTripType(Booking.TRIP_TYPE_ONE_WAY);

    when(bookingDBDAOInterface.getAllBookings()).thenReturn(new ArrayList<Booking>() {{
      add(activeBooking);
      add(inActiveBooking);
    }});

    assertThat("There`s no active booking", interactor.getActiveBookings(), hasItem(activeBooking));
  }

  @Test public void testHasActiveOneWayBookings() {

    Calendar calendar = Calendar.getInstance();

    calendar.add(Calendar.DATE, 1);
    final Booking activeBooking = new Booking();
    activeBooking.setDepartureFirstLeg(calendar.getTimeInMillis());
    activeBooking.setTripType(Booking.TRIP_TYPE_ONE_WAY);

    calendar.add(Calendar.DATE, -2);
    final Booking inActiveBooking = new Booking();
    inActiveBooking.setDepartureFirstLeg(calendar.getTimeInMillis());
    inActiveBooking.setTripType(Booking.TRIP_TYPE_ONE_WAY);

    when(bookingDBDAOInterface.getAllBookings()).thenReturn(new ArrayList<Booking>() {{
      add(activeBooking);
      add(inActiveBooking);
    }});

    assertThat("There`s no active booking", interactor.hasActiveBookings(), is(true));
  }

  @Test public void testHasNoActiveOneWayBookings() {

    Calendar calendar = Calendar.getInstance();

    calendar.add(Calendar.DATE, -2);
    final Booking inActiveBooking = new Booking();
    inActiveBooking.setDepartureFirstLeg(calendar.getTimeInMillis());
    inActiveBooking.setTripType(Booking.TRIP_TYPE_ONE_WAY);

    when(bookingDBDAOInterface.getAllBookings()).thenReturn(new ArrayList<Booking>() {{
      add(inActiveBooking);
    }});

    assertThat("There`s an active booking when it shouldn`t", interactor.hasActiveBookings(),
        is(not(true)));
  }

  @Test public void testGetActiveRoundtripBookingsReturnsActiveBookings() {
    Calendar calendar = Calendar.getInstance();

    calendar.add(Calendar.DATE, 1);
    final Booking activeBooking = new Booking();
    activeBooking.setArrivalLastLeg(calendar.getTimeInMillis());
    activeBooking.setTripType(Booking.TRIP_TYPE_ROUND_TRIP);

    calendar.add(Calendar.DATE, -2);
    final Booking inActiveBooking = new Booking();
    inActiveBooking.setArrivalLastLeg(calendar.getTimeInMillis());
    inActiveBooking.setTripType(Booking.TRIP_TYPE_ROUND_TRIP);

    when(bookingDBDAOInterface.getAllBookings()).thenReturn(new ArrayList<Booking>() {{
      add(activeBooking);
      add(inActiveBooking);
    }});

    assertThat("There`s no active booking", interactor.getActiveBookings(), hasItem(activeBooking));
  }

  @Test public void testHasActiveRoundtripBookings() {

    Calendar calendar = Calendar.getInstance();

    calendar.add(Calendar.DATE, 1);
    final Booking activeBooking = new Booking();
    activeBooking.setArrivalLastLeg(calendar.getTimeInMillis());
    activeBooking.setTripType(Booking.TRIP_TYPE_ROUND_TRIP);

    calendar.add(Calendar.DATE, -2);
    final Booking inActiveBooking = new Booking();
    inActiveBooking.setArrivalLastLeg(calendar.getTimeInMillis());
    inActiveBooking.setTripType(Booking.TRIP_TYPE_ROUND_TRIP);

    when(bookingDBDAOInterface.getAllBookings()).thenReturn(new ArrayList<Booking>() {{
      add(activeBooking);
      add(inActiveBooking);
    }});

    assertThat("There`s no active booking", interactor.hasActiveBookings(), is(true));
  }

  @Test public void testHasNoActiveRoundtripBookings() {

    Calendar calendar = Calendar.getInstance();

    calendar.add(Calendar.DATE, -2);
    final Booking inActiveBooking = new Booking();
    inActiveBooking.setArrivalLastLeg(calendar.getTimeInMillis());
    inActiveBooking.setTripType(Booking.TRIP_TYPE_ROUND_TRIP);

    when(bookingDBDAOInterface.getAllBookings()).thenReturn(new ArrayList<Booking>() {{
      add(inActiveBooking);
    }});

    assertThat("There`s an active booking when it shouldn`t", interactor.hasActiveBookings(),
        is(not(true)));
  }
}
