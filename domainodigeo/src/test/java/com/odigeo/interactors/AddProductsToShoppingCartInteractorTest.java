package com.odigeo.interactors;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.request.ModifyShoppingCartRequest;
import com.odigeo.data.net.controllers.AddProductsNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.trustdefender.TrustDefenderController;
import com.odigeo.presenter.listeners.OnAddProductsToShoppingCartListener;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.odigeo.data.preferences.PreferencesControllerInterface.JSESSION_COOKIE;
import static com.odigeo.test.mock.MocksProvider.providesPaymentMocks;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class AddProductsToShoppingCartInteractorTest {

  private static String ERROR_ADDING_INSURANCE = "COULD NOT ADD INSURANCE";

  @Mock private AddProductsNetControllerInterface mAddInsurancesNetController;
  @Mock private TrustDefenderController trustDefenderController;
  @Mock private PreferencesControllerInterface preferencesController;
  @Mock private ModifyShoppingCartRequest mModifyShoppingCartRequest;
  @Mock private OnAddProductsToShoppingCartListener mOnAddProductsToShoppingCartListener;
  @Mock private CreateShoppingCartResponse mCreateShoppingCartResponse;
  @Captor private ArgumentCaptor<OnRequestDataListener<CreateShoppingCartResponse>>
      mOnRequestDataListener;

  private AddProductsToShoppingCartInteractor mAddProductsToShoppingCartInteractor;
  private MslError mMslRandomError = MslError.UNK_001;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    mAddProductsToShoppingCartInteractor =
        new AddProductsToShoppingCartInteractor(mAddInsurancesNetController,
            trustDefenderController, preferencesController);
  }

  @Test public void shouldAddInsuranceToShoppingCart() {
    when(mCreateShoppingCartResponse.getShoppingCart()).thenReturn(
        providesPaymentMocks().provideShoppingCart());
    mAddProductsToShoppingCartInteractor.addProducts(mModifyShoppingCartRequest,
        mOnAddProductsToShoppingCartListener);
    verify(mAddInsurancesNetController).addProductsToShoppingCart(mOnRequestDataListener.capture(),
        eq(mModifyShoppingCartRequest));
    mOnRequestDataListener.getValue().onResponse(mCreateShoppingCartResponse);
    verify(preferencesController).getStringValue(JSESSION_COOKIE);
    verify(trustDefenderController).sendFingerPrint(anyString(), anyString());
    verifyNoMoreInteractions(mAddInsurancesNetController);
  }

  @Test public void shouldNotAddInsuranceToShoppingCart() {
    mAddProductsToShoppingCartInteractor.addProducts(mModifyShoppingCartRequest,
        mOnAddProductsToShoppingCartListener);
    verify(mAddInsurancesNetController).addProductsToShoppingCart(mOnRequestDataListener.capture(),
        eq(mModifyShoppingCartRequest));
    mOnRequestDataListener.getValue().onError(mMslRandomError, ERROR_ADDING_INSURANCE);
    verifyNoMoreInteractions(mAddInsurancesNetController);
  }
}