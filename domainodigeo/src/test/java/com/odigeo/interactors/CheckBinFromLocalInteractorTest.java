package com.odigeo.interactors;

import com.odigeo.constants.PaymentMethodsKeys;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import static com.odigeo.test.mock.MocksProvider.providesPaymentMocks;
import static org.junit.Assert.assertEquals;

public class CheckBinFromLocalInteractorTest {

  private CheckBinFromLocalInteractor mCheckBinFromLocalInteractor;
  private List<ShoppingCartCollectionOption> mShoppingCartCollectionOptions;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    mCheckBinFromLocalInteractor = new CheckBinFromLocalInteractor();
    mShoppingCartCollectionOptions = providesPaymentMocks().provideCollectionOptions();
  }

  @Test public void shouldRecognizeVisa() {
    String result =
        mCheckBinFromLocalInteractor.checkBinLocally("4", mShoppingCartCollectionOptions);
    assertEquals(result, PaymentMethodsKeys.VI_LOCAL);
  }

  @Test public void shouldRecognizeAmericanExpressFirstCase() {
    String resultCase1 =
        mCheckBinFromLocalInteractor.checkBinLocally("34", mShoppingCartCollectionOptions);
    assertEquals(resultCase1, PaymentMethodsKeys.AX_LOCAL);
  }

  @Test public void shouldRecognizeAmericanExpressSecondCase() {
    String resultCase2 =
        mCheckBinFromLocalInteractor.checkBinLocally("37", mShoppingCartCollectionOptions);
    assertEquals(resultCase2, PaymentMethodsKeys.AX_LOCAL);
  }

  @Test public void shouldRecognizeMasterCard() {
    String result =
        mCheckBinFromLocalInteractor.checkBinLocally("5", mShoppingCartCollectionOptions);
    assertEquals(result, PaymentMethodsKeys.MD_LOCAL);
  }

  @Test public void shouldRecognizeJCB() {
    String result =
        mCheckBinFromLocalInteractor.checkBinLocally("35", mShoppingCartCollectionOptions);
    assertEquals(result, PaymentMethodsKeys.JC_LOCAL);
  }

  @Test public void shouldRecognizeDinnersClubFirstCase() {
    String result =
        mCheckBinFromLocalInteractor.checkBinLocally("36", mShoppingCartCollectionOptions);
    assertEquals(result, PaymentMethodsKeys.DC_LOCAL);
  }

  @Test public void shouldRecognizeDinnersClubSecondCase() {
    String resultCase2 =
        mCheckBinFromLocalInteractor.checkBinLocally("38", mShoppingCartCollectionOptions);
    assertEquals(resultCase2, PaymentMethodsKeys.DC_LOCAL);
  }

  @Test public void shouldRecognizeDinnersClubThirdCase() {
    String resultCase3 =
        mCheckBinFromLocalInteractor.checkBinLocally("39", mShoppingCartCollectionOptions);
    assertEquals(resultCase3, PaymentMethodsKeys.DC_LOCAL);
  }

  @Test public void shouldRecognizeDinnersClubFourthCase() {
    String resultCase4 =
        mCheckBinFromLocalInteractor.checkBinLocally("309", mShoppingCartCollectionOptions);
    assertEquals(resultCase4, PaymentMethodsKeys.DC_LOCAL);
  }

  @Test public void shouldRecognizeUnknownCardFirstCase() {
    String result =
        mCheckBinFromLocalInteractor.checkBinLocally("", mShoppingCartCollectionOptions);
    assertEquals(result, PaymentMethodsKeys.UNKNOWN);
  }

  @Test public void shouldRecognizeUnknownCardSecondCase() {
    String resultCase2 =
        mCheckBinFromLocalInteractor.checkBinLocally("1", mShoppingCartCollectionOptions);
    assertEquals(resultCase2, PaymentMethodsKeys.UNKNOWN);
  }

  @Test public void shouldRecognizeUnknownCardThirdCase() {
    String resultCase3 =
        mCheckBinFromLocalInteractor.checkBinLocally("2", mShoppingCartCollectionOptions);
    assertEquals(resultCase3, PaymentMethodsKeys.UNKNOWN);
  }

  @Test public void shouldRecognizeUnknownCardFourthCase() {
    String resultCase4 =
        mCheckBinFromLocalInteractor.checkBinLocally("6", mShoppingCartCollectionOptions);
    assertEquals(resultCase4, PaymentMethodsKeys.UNKNOWN);
  }

  @Test public void shouldRecognizeUnknownCardFifthCase() {
    String resultCase5 =
        mCheckBinFromLocalInteractor.checkBinLocally("7", mShoppingCartCollectionOptions);
    assertEquals(resultCase5, PaymentMethodsKeys.UNKNOWN);
  }

  @Test public void shouldRecognizeUnknownCardSixthCase() {
    String resultCase6 =
        mCheckBinFromLocalInteractor.checkBinLocally("8", mShoppingCartCollectionOptions);
    assertEquals(resultCase6, PaymentMethodsKeys.UNKNOWN);
  }

  @Test public void shouldRecognizeUnknownCardSeventhCase() {
    String resultCase7 =
        mCheckBinFromLocalInteractor.checkBinLocally("9", mShoppingCartCollectionOptions);
    assertEquals(resultCase7, PaymentMethodsKeys.UNKNOWN);
  }
}