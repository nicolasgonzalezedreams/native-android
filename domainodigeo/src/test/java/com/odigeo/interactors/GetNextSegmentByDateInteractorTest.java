package com.odigeo.interactors;

import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GetNextSegmentByDateInteractorTest {

  private static long fakeLoswestOffset = 60000;
  private static long fakeMediumOffset = 18000000;
  private static long fakeHighOffset = 32000000;
  private List<Segment> fakeSegments = new ArrayList<>();
  private Segment nearestSegment;

  private GetNextSegmentByDateInteractor mGetNextSegmentByDateInteractor;

  @Before public void setUp() {
    initFakeSegments();
    mGetNextSegmentByDateInteractor = new GetNextSegmentByDateInteractor();
  }

  @Test public void getNextValidSegmentSuccessfully() {
    initValidFakeSegmentTimes();
    nearestSegment = mGetNextSegmentByDateInteractor.getNextValidSegment(fakeSegments);
    assertEquals(nearestSegment, fakeSegments.get(0));
  }

  @Test public void getNextValidSegmentUnsuccessfully() {
    initInvalidFakeSegmentTimes();
    nearestSegment = mGetNextSegmentByDateInteractor.getNextValidSegment(fakeSegments);
    assertEquals(nearestSegment, null);
  }

  private void initFakeSegments() {
    List<Section> nearestSectionsList = new ArrayList<>();
    List<Section> mediumSectionsList = new ArrayList<>();
    List<Section> mostFarSectionsList = new ArrayList<>();

    Segment nearestSegment = new Segment();
    Segment mediumSegment = new Segment();
    Segment mostFarSegment = new Segment();

    Section nearestSection = new Section();
    Section mediumSection = new Section();
    Section mostFarSection = new Section();

    nearestSectionsList.add(nearestSection);
    nearestSegment.setSectionsList(nearestSectionsList);
    fakeSegments.add(nearestSegment);

    mediumSectionsList.add(mediumSection);
    mediumSegment.setSectionsList(mediumSectionsList);
    fakeSegments.add(mediumSegment);

    mostFarSectionsList.add(mostFarSection);
    mostFarSegment.setSectionsList(mostFarSectionsList);
    fakeSegments.add(mostFarSegment);
  }

  private void initInvalidFakeSegmentTimes() {
    fakeSegments.get(0)
        .getSectionsList()
        .get(0)
        .setDepartureDate(System.currentTimeMillis() - fakeLoswestOffset);
    fakeSegments.get(1)
        .getSectionsList()
        .get(0)
        .setDepartureDate(System.currentTimeMillis() - fakeLoswestOffset);
    fakeSegments.get(2)
        .getSectionsList()
        .get(0)
        .setDepartureDate(System.currentTimeMillis() - fakeLoswestOffset);
  }

  private void initValidFakeSegmentTimes() {
    fakeSegments.get(0)
        .getSectionsList()
        .get(0)
        .setArrivalDate(System.currentTimeMillis() + fakeLoswestOffset);
    fakeSegments.get(1)
        .getSectionsList()
        .get(0)
        .setArrivalDate(System.currentTimeMillis() + fakeMediumOffset);
    fakeSegments.get(2)
        .getSectionsList()
        .get(0)
        .setArrivalDate(System.currentTimeMillis() + fakeHighOffset);
  }
}