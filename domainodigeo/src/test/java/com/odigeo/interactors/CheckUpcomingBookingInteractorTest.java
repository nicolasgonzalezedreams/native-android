package com.odigeo.interactors;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.tools.DateHelperInterface;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class CheckUpcomingBookingInteractorTest {

  private static final long CURRENT_SYSTEM_MILLIS = 2L;

  @Mock private DateHelperInterface dateHelper;

  private CheckUpcomingBookingInteractor interactor;

  @Before public void setup() {
    when(dateHelper.getCurrentSystemMillis()).thenReturn(CURRENT_SYSTEM_MILLIS);
    when(dateHelper.millisecondsLeastOffset(CURRENT_SYSTEM_MILLIS)).thenReturn(
        CURRENT_SYSTEM_MILLIS);
    interactor = new CheckUpcomingBookingInteractor(dateHelper);
  }

  @Test public void testCheckIsUpcomingBooking() {
    final long bookingMillis = 3L;
    final Booking booking = new Booking();
    booking.setArrivalLastLeg(bookingMillis);
    when(dateHelper.millisecondsLeastOffset(bookingMillis)).thenReturn(bookingMillis);

    boolean isUpcomingBooking = interactor.isUpcomingBooking(booking);

    assertThat(isUpcomingBooking, is(true));
  }

  @Test public void testCheckIsPastBooking() {
    final long bookingMillis = 1L;
    final Booking booking = new Booking();
    booking.setArrivalLastLeg(bookingMillis);
    when(dateHelper.millisecondsLeastOffset(bookingMillis)).thenReturn(bookingMillis);

    boolean isUpcomingBooking = interactor.isUpcomingBooking(booking);

    assertThat(isUpcomingBooking, is(false));
  }
}
