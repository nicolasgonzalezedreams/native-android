package com.odigeo.interactors;

import com.odigeo.data.db.helper.UserCreateOrUpdateHandlerInterface;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.net.controllers.UserNetControllerInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.dataodigeo.session.CredentialsInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.test.mock.mocks.UserMocks;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.odigeo.presenter.contracts.views.LoginSocialInterface.GOOGLE_SOURCE;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.PASSWORD_SOURCE;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LoginInteractorTest {

  @Mock private UserNetControllerInterface userNetControllerInterface;
  @Mock private SessionController sessionControllerInterface;
  @Mock private UserCreateOrUpdateHandlerInterface userCreateOrUpdateHandlerInterface;
  @Mock private TravellerDetailInteractor travellerDetailInteractor;
  @Mock private EnableNotificationsInteractor enableNotificationsInteractor;
  @Mock private PreferencesControllerInterface preferencesControllerInterface;
  @Mock private UpdateCitiesInteractor updateCitiesInteractor;
  @Mock private OnRequestDataListener<User> onRequestDataListener;

  @Captor private ArgumentCaptor<OnRequestDataListener<User>> listener;

  private LoginInteractor loginInteractor;
  private User user;
  private UserProfile profile;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    profile = UserMocks.provideDefaultUserProfile();
    user = new UserMocks.Builder().build();
    loginInteractor =
        new LoginInteractor(userNetControllerInterface, userCreateOrUpdateHandlerInterface,
            sessionControllerInterface, travellerDetailInteractor, enableNotificationsInteractor,
            preferencesControllerInterface, updateCitiesInteractor);
  }

  @Test public void loginWithEmailAndPasswordAndUserTravellersSuccess() {
    when(userCreateOrUpdateHandlerInterface.addOrCreateUserAndAllComponents(
        any(User.class))).thenReturn(true);
    when(preferencesControllerInterface.getBooleanValue(anyString(), anyBoolean())).thenReturn(
        true);

    loginInteractor.login(UserMocks.EMAIL, UserMocks.ACCESS_TOKEN, UserMocks.PROFILE_URL,
        UserMocks.NAME, onRequestDataListener, profile, PASSWORD_SOURCE);

    verify(userNetControllerInterface).login(listener.capture(), eq(UserMocks.EMAIL),
        eq(UserMocks.ACCESS_TOKEN), eq(PASSWORD_SOURCE));

    listener.getValue().onResponse(user);

    verify(userCreateOrUpdateHandlerInterface, times(1)).addOrCreateUserAndAllComponents(user);
    verify(sessionControllerInterface, times(1)).saveUserInfo(user.getUserId(), UserMocks.NAME,
        UserMocks.PROFILE_URL, UserMocks.EMAIL, User.Status.UNREGISTERED);
    verify(sessionControllerInterface, times(1)).savePasswordCredentials(UserMocks.EMAIL,
        UserMocks.ACCESS_TOKEN, CredentialsInterface.CredentialsType.PASSWORD);
    verify(sessionControllerInterface).saveUserName(UserMocks.NAME + " " + UserMocks.SURNAME);
    verify(enableNotificationsInteractor).enableNotificationsDuringLogging(onRequestDataListener,
        user);
    verify(updateCitiesInteractor).updateCities();
  }

  @Test public void loginFailAddingUserToDB() {
    when(userCreateOrUpdateHandlerInterface.addOrCreateUserAndAllComponents(
        any(User.class))).thenReturn(false);

    loginInteractor.login(UserMocks.EMAIL, UserMocks.ACCESS_TOKEN, UserMocks.PROFILE_URL,
        UserMocks.NAME, onRequestDataListener, profile, PASSWORD_SOURCE);

    verify(userNetControllerInterface).login(listener.capture(), eq(UserMocks.EMAIL),
        eq(UserMocks.ACCESS_TOKEN), eq(PASSWORD_SOURCE));

    listener.getValue().onResponse(user);

    verify(userCreateOrUpdateHandlerInterface, times(1)).addOrCreateUserAndAllComponents(user);
    verify(sessionControllerInterface, times(1)).removeSharedPreferencesInfo();
    verify(onRequestDataListener).onError(com.odigeo.data.net.error.MslError.UNK_001, null);
  }

  @Test public void loginFailMslRequest() {
    loginInteractor.login(UserMocks.EMAIL, UserMocks.ACCESS_TOKEN, UserMocks.PROFILE_URL,
        UserMocks.NAME, onRequestDataListener, profile, PASSWORD_SOURCE);

    verify(userNetControllerInterface).login(listener.capture(), eq(UserMocks.EMAIL),
        eq(UserMocks.ACCESS_TOKEN), eq(PASSWORD_SOURCE));

    listener.getValue().onResponse(user);

    verify(sessionControllerInterface, times(1)).removeSharedPreferencesInfo();
    verify(onRequestDataListener).onError(com.odigeo.data.net.error.MslError.UNK_001, null);
  }

  @Test public void loginWithGoogleAndUserTravellersSuccess() {
    when(userCreateOrUpdateHandlerInterface.addOrCreateUserAndAllComponents(
        any(User.class))).thenReturn(true);
    when(preferencesControllerInterface.getBooleanValue(anyString(), anyBoolean())).thenReturn(
        true);

    loginInteractor.login(UserMocks.EMAIL, UserMocks.ACCESS_TOKEN, UserMocks.PROFILE_URL,
        UserMocks.NAME, onRequestDataListener, profile, GOOGLE_SOURCE);

    verify(userNetControllerInterface).login(listener.capture(), eq(UserMocks.EMAIL),
        eq(UserMocks.ACCESS_TOKEN), eq(GOOGLE_SOURCE));

    listener.getValue().onResponse(user);

    verify(userCreateOrUpdateHandlerInterface, times(1)).addOrCreateUserAndAllComponents(user);
    verify(sessionControllerInterface, times(1)).saveUserInfo(user.getUserId(), UserMocks.NAME,
        UserMocks.PROFILE_URL, UserMocks.EMAIL, User.Status.UNREGISTERED);
    verify(sessionControllerInterface).saveSocialCredentials(UserMocks.EMAIL,
        UserMocks.ACCESS_TOKEN, CredentialsInterface.CredentialsType.GOOGLE);
    verify(sessionControllerInterface).saveUserName(UserMocks.NAME + " " + UserMocks.SURNAME);
    verify(enableNotificationsInteractor).enableNotificationsDuringLogging(onRequestDataListener,
        user);
    verify(updateCitiesInteractor).updateCities();
  }
}
