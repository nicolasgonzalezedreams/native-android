package com.odigeo.validators;

import com.odigeo.data.entity.shoppingCart.Itinerary;
import com.odigeo.data.entity.shoppingCart.TravellerRequiredFields;
import com.odigeo.tools.DateHelperInterface;
import com.odigeo.validations.BirthdateValidator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.odigeo.data.entity.shoppingCart.TravellerType.CHILD;
import static com.odigeo.data.entity.shoppingCart.TravellerType.INFANT;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

public class BirthdateValidatorTest {

  private static final int CURRENT_YEAR = 2017;
  private static final long JUL25_17_TIMESTAMP = 1500970800532L;

  private final int infantMaxAge = 2;
  private final int childMaxAge = 12;
  private BirthdateValidator birthdateValidator;

  @Mock private DateHelperInterface dateHelper;
  @Mock private TravellerRequiredFields passengerInformation;
  @Mock private Itinerary itinerary;

  @Before public void setup() {
    MockitoAnnotations.initMocks(this);
    birthdateValidator = new BirthdateValidator(dateHelper, passengerInformation);
  }

  @Test public void shouldShowBirthdateErrorWhenInfantIsTurningIntoChildDuringTrip() {
    int birthdate[] = { 25, 7, 2015 };
    long birthdateTimeStamp = 1500940852854L;

    when(passengerInformation.getTravellerType()).thenReturn(INFANT);
    when(dateHelper.getCurrentYear()).thenReturn(CURRENT_YEAR);
    when(dateHelper.getTimeStampFromDayMonthYear(anyInt(), anyInt(), anyInt())).thenReturn(
        birthdateTimeStamp);
    when(itinerary.getLastArrivalDate()).thenReturn(JUL25_17_TIMESTAMP);

    boolean isValid = birthdateValidator.isTravellerTypeTurningIntoAnotherTypeDuringTrip(birthdate,
        itinerary, INFANT, infantMaxAge);

    assertTrue(isValid);
  }

  @Test public void shouldShowBirthdateErrorWhenInfantIsTurningIntoChildBeforeTrip() {
    int birthdate[] = { 24, 7, 2015 };
    long birthdateTimeStamp = 1500854448736L;

    when(passengerInformation.getTravellerType()).thenReturn(INFANT);
    when(dateHelper.getCurrentYear()).thenReturn(CURRENT_YEAR);
    when(dateHelper.getTimeStampFromDayMonthYear(anyInt(), anyInt(), anyInt())).thenReturn(
        birthdateTimeStamp);
    when(itinerary.getLastArrivalDate()).thenReturn(JUL25_17_TIMESTAMP);

    boolean isValid = birthdateValidator.isTravellerTypeTurningIntoAnotherTypeDuringTrip(birthdate,
        itinerary, INFANT, infantMaxAge);

    assertTrue(isValid);
  }

  @Test public void shouldNotShowBirthdateErrorWhenChildIsTurningIntoAdultAfterTrip() {
      int birthdate[] = { 25, 7, 2005 };
      long birthdateTimeStamp = 1500940855583L;

    when(passengerInformation.getTravellerType()).thenReturn(CHILD);
    when(dateHelper.getCurrentYear()).thenReturn(CURRENT_YEAR);
    when(dateHelper.getTimeStampFromDayMonthYear(anyInt(), anyInt(), anyInt())).thenReturn(
        birthdateTimeStamp);
    when(itinerary.getLastArrivalDate()).thenReturn(JUL25_17_TIMESTAMP);

    boolean isValid = birthdateValidator.isTravellerTypeTurningIntoAnotherTypeDuringTrip(birthdate,
        itinerary, CHILD, childMaxAge);

    assertTrue(isValid);
  }

  @Test public void shouldShowBirthdateErrorWhenChildIsTurningIntoAdultDuringTrip() {
    int birthdate[] = { 24, 7, 2005 };
    long birthdateTimeStamp = 1500854451665L;

    when(passengerInformation.getTravellerType()).thenReturn(CHILD);
    when(dateHelper.getCurrentYear()).thenReturn(CURRENT_YEAR);
    when(dateHelper.getTimeStampFromDayMonthYear(anyInt(), anyInt(), anyInt())).thenReturn(
        birthdateTimeStamp);
    when(itinerary.getLastArrivalDate()).thenReturn(JUL25_17_TIMESTAMP);

    boolean isValid = birthdateValidator.isTravellerTypeTurningIntoAnotherTypeDuringTrip(birthdate,
        itinerary, CHILD, childMaxAge);

    assertTrue(isValid);
  }

  @Test public void shouldShowBirthdateErrorWhenChildIsTurningIntoAdultBeforeTrip() {
    int birthdate[] = { 24, 7, 2005 };
    long birthdateTimeStamp = 1500854451665L;

    when(passengerInformation.getTravellerType()).thenReturn(CHILD);
    when(dateHelper.getCurrentYear()).thenReturn(CURRENT_YEAR);
    when(dateHelper.getTimeStampFromDayMonthYear(anyInt(), anyInt(), anyInt())).thenReturn(
        birthdateTimeStamp);
    when(itinerary.getLastArrivalDate()).thenReturn(JUL25_17_TIMESTAMP);

    boolean isValid = birthdateValidator.isTravellerTypeTurningIntoAnotherTypeDuringTrip(birthdate,
        itinerary, CHILD, childMaxAge);

    assertTrue(isValid);
  }
}
