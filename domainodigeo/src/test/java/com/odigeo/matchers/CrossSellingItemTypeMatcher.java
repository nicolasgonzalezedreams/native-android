package com.odigeo.matchers;

import com.odigeo.data.entity.xsell.CrossSelling;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

public class CrossSellingItemTypeMatcher extends BaseMatcher<CrossSelling> {

  private final int type;

  public CrossSellingItemTypeMatcher(int crossSellingType) {
    this.type = crossSellingType;
  }

  @Override public boolean matches(Object item) {
    return item instanceof CrossSelling && ((CrossSelling) item).getType() == type;
  }

  @Override public void describeTo(Description description) {
    description.appendText("CrossSelling type should match " + type);
  }
}
