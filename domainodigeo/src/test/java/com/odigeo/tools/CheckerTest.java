package com.odigeo.tools;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by julio.kun on 9/10/2015.
 */
public class CheckerTest {

  @Test public void checkEmailTest() {

    String email1 = "prueba@lala.com";
    String email2 = "brom@live.com.ar";

    String wEmail = "lala";
    String wEmail1 = "brom@";
    String wEmail2 = "brom@live";
    String wEmail3 = "brom@.com";
    String wEmail4 = "@live.com";
    String wEmail5 = "@live.com.ar";
    String wEmail6 = "live.com.ar";

    Assert.assertTrue(CheckerTool.checkEmail(email1));
    Assert.assertTrue(CheckerTool.checkEmail(email2));

    Assert.assertFalse(CheckerTool.checkEmail(wEmail));
    Assert.assertFalse(CheckerTool.checkEmail(wEmail1));
    Assert.assertFalse(CheckerTool.checkEmail(wEmail2));
    Assert.assertFalse(CheckerTool.checkEmail(wEmail3));
    Assert.assertFalse(CheckerTool.checkEmail(wEmail4));
    Assert.assertFalse(CheckerTool.checkEmail(wEmail5));
    Assert.assertFalse(CheckerTool.checkEmail(wEmail6));
  }

  @Test public void checkCountryTest() {

    String country = "Antartide (territorio compreso entro i 60° di latitudine sud)";
    String emptyCountry = "";
    String nullCountry = null;

    Assert.assertTrue(CheckerTool.checkCountry(country));
    Assert.assertFalse(CheckerTool.checkCountry(emptyCountry));
    Assert.assertFalse(CheckerTool.checkCountry(nullCountry));
  }
}
