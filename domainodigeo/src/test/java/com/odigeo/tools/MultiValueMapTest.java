package com.odigeo.tools;

import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 5/05/16
 */

public class MultiValueMapTest {

  public static final int INSERT_LIMIT = 100;
  private MultiValueMap<Integer, Integer> mMap;

  @Before public void setUp() {
    mMap = new MultiValueMap<>();
  }

  @Test public void testCreationOfMap() {
    Assert.assertTrue(mMap.isEmpty());
    Assert.assertNull(mMap.get(0));
    Assert.assertTrue(mMap.keySet().isEmpty());
  }

  @Test public void testInsertionOnSameKey() {
    insertOnSameKey();
    Assert.assertFalse(mMap.isEmpty());
    Assert.assertEquals(1, mMap.size());
  }

  @Test public void testInsertionOnDifferentKey() {
    insertOnDifferentKey();
    Assert.assertFalse(mMap.isEmpty());
    Assert.assertEquals(INSERT_LIMIT, mMap.size());
  }

  @Test public void testGetOnSameKey() {
    insertOnSameKey();
    List<Integer> value = mMap.get(0);
    Assert.assertNotNull(value);
    Assert.assertFalse(value.isEmpty());
    int index = INSERT_LIMIT / 2;
    Assert.assertEquals((Integer) index, value.get(index));
  }

  @Test public void testGetOnDifferentKey() {
    insertOnDifferentKey();
    int index = INSERT_LIMIT / 2;
    List<Integer> value = mMap.get(index);
    Assert.assertNotNull(value);
    Assert.assertFalse(value.isEmpty());
    Assert.assertEquals((Integer) index, value.get(0));
  }

  private void insertOnSameKey() {
    for (int index = 0; index < INSERT_LIMIT; index++) {
      mMap.put(0, index);
    }
  }

  private void insertOnDifferentKey() {
    for (int index = 0; index < INSERT_LIMIT; index++) {
      mMap.put(index, index);
    }
  }
}
