package com.odigeo.builder;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UrlBuilderTest {

  private static final String BASE_URL = "http://odigeo.com";

  private static final String FIRST_PARAMETER = "param1";
  private static final String SECOND_PARAMETER = "param2";
  private static final String FIRST_PARAMETER_VALUE = "hello";
  private static final String SECOND_PARAMETER_VALUE = "howuru";

  UrlBuilder.Builder builder;

  @Before public void setUp() {
    builder = new UrlBuilder.Builder();
    builder.path(BASE_URL);
  }

  @Test public void shouldBuildWithNoParamsUrl() throws UnsupportedEncodingException {
    UrlBuilder.Builder builder = new UrlBuilder.Builder(BASE_URL);
    UrlBuilder build = builder.build();

    String returnedUrl = build.getCompleteUrl();
    String expectedUrlBuilt = BASE_URL;

    assertEquals(returnedUrl, expectedUrlBuilt);
  }

  @Test public void shouldBuildWithOneParamUrl() throws UnsupportedEncodingException {
    UrlBuilder.Builder builder =
        new UrlBuilder.Builder(BASE_URL + "?" + FIRST_PARAMETER + "=" + FIRST_PARAMETER_VALUE);
    UrlBuilder build = builder.build();

    String returnedUrl = build.getCompleteUrl();
    String expectedUrlBuilt = BASE_URL + "?" + FIRST_PARAMETER + "=" + FIRST_PARAMETER_VALUE;

    assertEquals(returnedUrl, expectedUrlBuilt);
  }

  @Test public void shouldBuildWithTwoParamsUrl() throws UnsupportedEncodingException {
    UrlBuilder.Builder builder = new UrlBuilder.Builder(BASE_URL
        + "?"
        + FIRST_PARAMETER
        + "="
        + FIRST_PARAMETER_VALUE
        + "&"
        + SECOND_PARAMETER
        + "="
        + SECOND_PARAMETER_VALUE);
    UrlBuilder build = builder.build();

    String returnedUrl = build.getCompleteUrl();
    String expectedUrlBuilt = BASE_URL
        + "?"
        + FIRST_PARAMETER
        + "="
        + FIRST_PARAMETER_VALUE
        + "&"
        + SECOND_PARAMETER
        + "="
        + SECOND_PARAMETER_VALUE;

    assertEquals(returnedUrl, expectedUrlBuilt);
  }

  @Test public void shouldBuildUrlNoParams() throws UnsupportedEncodingException {
    UrlBuilder urlBuilder = builder.build();

    String returnedUrl = urlBuilder.getCompleteUrl();
    String expectedUrlBuilt = BASE_URL;

    assertEquals(returnedUrl, expectedUrlBuilt);
  }

  @Test public void shouldBuildUrlOneParam() throws UnsupportedEncodingException {
    Map<String, String> params = new HashMap<>();
    params.put(FIRST_PARAMETER, FIRST_PARAMETER_VALUE);
    builder.params(params);
    UrlBuilder urlBuilder = builder.build();

    String returnedUrl = urlBuilder.getCompleteUrl();
    String expectedUrlBuilt = BASE_URL + "?" + FIRST_PARAMETER + "=" + FIRST_PARAMETER_VALUE;

    assertEquals(returnedUrl, expectedUrlBuilt);
  }

  @Test public void shouldBuildUrlTwoParams() throws UnsupportedEncodingException {
    Map<String, String> params = new HashMap<>();
    params.put(FIRST_PARAMETER, FIRST_PARAMETER_VALUE);
    params.put(SECOND_PARAMETER, SECOND_PARAMETER_VALUE);
    builder.params(params);
    UrlBuilder urlBuilder = builder.build();

    String returnedUrl = urlBuilder.getCompleteUrl();
    String expectedUrlBuilt = BASE_URL
        + "?"
        + FIRST_PARAMETER
        + "="
        + FIRST_PARAMETER_VALUE
        + "&"
        + SECOND_PARAMETER
        + "="
        + SECOND_PARAMETER_VALUE;

    assertEquals(returnedUrl, expectedUrlBuilt);
  }
}