package com.odigeo.builder;

import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.entity.booking.SearchSegment;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class CarouselDropOffBuilderTest {

  @Mock private SearchesHandlerInterface searchesHandlerInterface;
  @Mock private PreferencesControllerInterface preferencesControllerInterface;

  private DropOffCarouselCardBuilder dropOffCarouselCardBuilder;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    dropOffCarouselCardBuilder =
        new DropOffCarouselCardBuilder(searchesHandlerInterface, preferencesControllerInterface);
  }

  @Test public void buildToWithoutAnySearch() {
    when(preferencesControllerInterface.getBooleanValue(
        PreferencesControllerInterface.SHOULD_ADD_DROPOFF_CARD)).thenReturn(true);
    when(searchesHandlerInterface.getCompleteSearchesFromDB()).thenReturn(
        new ArrayList<StoredSearch>());

    List<Card> cards = dropOffCarouselCardBuilder.buildCards();
    assertTrue(cards.size() == 0);
  }

  @Test public void buildToWithSimpleTripSearch() {
    when(preferencesControllerInterface.getBooleanValue(
        PreferencesControllerInterface.SHOULD_ADD_DROPOFF_CARD)).thenReturn(true);
    when(searchesHandlerInterface.getCompleteSearchesFromDB()).thenReturn(
        getSimpleTripStoredSearch());

    List<Card> cards = dropOffCarouselCardBuilder.buildCards();
    assertTrue(cards.size() == 1);
  }

  @Test public void buildToWithRoundTripSearch() {
    when(preferencesControllerInterface.getBooleanValue(
        PreferencesControllerInterface.SHOULD_ADD_DROPOFF_CARD)).thenReturn(true);
    when(searchesHandlerInterface.getCompleteSearchesFromDB()).thenReturn(
        getRoundTripStoredSearch());

    List<Card> cards = dropOffCarouselCardBuilder.buildCards();
    assertTrue(cards.size() == 1);
  }

  @Test public void buildToWithMultiTripSearch() {
    when(preferencesControllerInterface.getBooleanValue(
        PreferencesControllerInterface.SHOULD_ADD_DROPOFF_CARD)).thenReturn(true);
    when(searchesHandlerInterface.getCompleteSearchesFromDB()).thenReturn(
        getMultiTripStoredSearch());

    List<Card> cards = dropOffCarouselCardBuilder.buildCards();
    assertTrue(cards.size() == 0);
  }

  @Test public void buildWithExpirationDatePassed() {
    when(preferencesControllerInterface.getBooleanValue(
        PreferencesControllerInterface.SHOULD_ADD_DROPOFF_CARD)).thenReturn(false);
    List<Card> cards = dropOffCarouselCardBuilder.buildCards();
    assertTrue(cards.size() == 0);
  }

  private List<StoredSearch> getSimpleTripStoredSearch() {
    ArrayList<StoredSearch> resultList = new ArrayList<>();
    resultList.add(getStoredSearchByType(StoredSearch.TripType.O));

    return resultList;
  }

  private List<StoredSearch> getRoundTripStoredSearch() {
    ArrayList<StoredSearch> resultList = new ArrayList<>();
    resultList.add(getStoredSearchByType(StoredSearch.TripType.R));

    return resultList;
  }

  private List<StoredSearch> getMultiTripStoredSearch() {
    ArrayList<StoredSearch> resultList = new ArrayList<>();
    resultList.add(getStoredSearchByType(StoredSearch.TripType.M));

    return resultList;
  }

  private StoredSearch getStoredSearchByType(StoredSearch.TripType type) {
    StoredSearch storedSearch =
        new StoredSearch(0, 0, 0, 0, 0, StoredSearch.CabinClass.TOURIST, true, type, false,
            getSearchSegmentList(), 0, 0);

    return storedSearch;
  }

  private List<SearchSegment> getSearchSegmentList() {
    SearchSegment segment = new SearchSegment(0, 0, "MAD", "BCN", System.currentTimeMillis(), 0);
    ArrayList<SearchSegment> segmentList = new ArrayList<>();
    segmentList.add(segment);
    return segmentList;
  }
}
