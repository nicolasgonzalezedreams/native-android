package com.odigeo.builder;

import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.net.controllers.CarrouselNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.CarouselManagerInterface;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;

/**
 * Created by daniel.morales on 9/3/17.
 */

public class CarouselPromotionBuilderTest {

  @Mock private CarrouselNetControllerInterface mCarouselNetControllerInterface;
  @Mock private CarouselManagerInterface mCarouselManagerInterface;
  @Mock private PromotionCarouselCardBuilder.PromotionCardsInterface mPromotionCardsInterface;
  @Captor private ArgumentCaptor<OnRequestDataListener<String>> mOnRequestDataListener;

  private PromotionCarouselCardBuilder mPromotionCardsBuilder;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    mPromotionCardsBuilder = new PromotionCarouselCardBuilder(mCarouselNetControllerInterface,
        mCarouselManagerInterface);
    mPromotionCardsBuilder.setPromotionCardsInterface(mPromotionCardsInterface);
  }

  @Test public void buildCardsWithoutMSLError() {
    mPromotionCardsBuilder.buildCards(true);
    verify(mCarouselNetControllerInterface).getCarouselPromotion(mOnRequestDataListener.capture(),
        anyBoolean());
    mOnRequestDataListener.getValue().onResponse(anyString());
    verify(mPromotionCardsInterface).onPromotionCardsLoaded(anyListOf(Card.class));
  }

  @Test public void buildCardsWithMSLError() {
    mPromotionCardsBuilder.buildCards(true);
    verify(mCarouselNetControllerInterface).getCarouselPromotion(mOnRequestDataListener.capture(),
        anyBoolean());
    mOnRequestDataListener.getValue().onError(MslError.VAL_002, "");
    verify(mPromotionCardsInterface).onPromotionCardsError();
  }
}
