package com.odigeo.builder;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.preferences.CarouselManagerInterface;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.GetNextSegmentByDateInteractor;
import com.odigeo.interactors.MyTripsInteractor;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

public class CarouselBookingBuilderTest {

  private static int NEAREST_SEGMENT = 0;

  private List<Segment> fakeSegments = new ArrayList<>();
  private Booking fakeBooking = new Booking();

  private BookingCarouselCardBuilder mCarouselBookingBuilder;

  @Mock private MyTripsInteractor mMyTripsInteractor;
  @Mock private GetNextSegmentByDateInteractor mGetNextSegmentByDateInteractor;
  @Mock private CarouselManagerInterface mCarouselManagerInterface;
  @Mock private SectionCardBuilder mSectionCardBuilder;
  @Mock private TrackerControllerInterface mTrackerControllerInterface;
  @Mock private SessionController mSessionController;
  @Mock private PreferencesControllerInterface mPreferencesControllerInterface;

  private List<Card> fakeCardList;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    mCarouselBookingBuilder =
        new BookingCarouselCardBuilder(mMyTripsInteractor, mGetNextSegmentByDateInteractor,
            mCarouselManagerInterface, mSectionCardBuilder, mTrackerControllerInterface,
            mSessionController, mPreferencesControllerInterface);
    initFakeSegments();
    initFakeBooking();
    initFakeCards();
  }

  public void initFakeCards() {
    fakeCardList = new ArrayList<>();

    Card card = new Card();
    fakeCardList.add(card);
  }

  private void initFakeSegments() {
    List<Section> nearestSectionsList = new ArrayList<>();
    List<Section> mediumSectionsList = new ArrayList<>();
    List<Section> mostFarSectionsList = new ArrayList<>();

    Segment nearestSegment = new Segment();
    Segment mediumSegment = new Segment();
    Segment mostFarSegment = new Segment();

    Section nearestSection = new Section();
    Section mediumSection = new Section();
    Section mostFarSection = new Section();

    LocationBooking locationBooking = new LocationBooking();
    locationBooking.setCityName("Berlin");
    nearestSection.setTo(locationBooking);
    nearestSectionsList.add(nearestSection);
    nearestSegment.setSectionsList(nearestSectionsList);
    fakeSegments.add(nearestSegment);

    mediumSectionsList.add(mediumSection);
    mediumSegment.setSectionsList(mediumSectionsList);
    fakeSegments.add(mediumSegment);

    mostFarSectionsList.add(mostFarSection);
    mostFarSegment.setSectionsList(mostFarSectionsList);
    fakeSegments.add(mostFarSegment);
  }

  private void initFakeBooking() {
    fakeBooking.setSegments(fakeSegments);
    fakeBooking.setTripType(Booking.TRIP_TYPE_ROUND_TRIP);
  }

  @Test public void buildToWithBooking() {
    when(mCarouselManagerInterface.mustShow(anyLong(), any(Card.CardType.class))).thenReturn(true);
    when(mMyTripsInteractor.getNextBooking()).thenReturn(fakeBooking);
    when(mGetNextSegmentByDateInteractor.getNextValidSegment(anyListOf(Segment.class))).thenReturn(
        fakeSegments.get(NEAREST_SEGMENT));
    when(mSectionCardBuilder.buildTo(any(Section.class), anyBoolean(), anyLong())).thenReturn(
        fakeCardList);

    List<Card> cards = mCarouselBookingBuilder.buildCards();

    assertEquals(1, cards.size());
  }

  @Test public void buildToWithoutBookings() {
    when(mCarouselManagerInterface.mustShow(anyLong(), any(Card.CardType.class))).thenReturn(true);
    when(mMyTripsInteractor.getNextBooking()).thenReturn(null);
    when(mGetNextSegmentByDateInteractor.getNextValidSegment(anyListOf(Segment.class))).thenReturn(
        fakeSegments.get(NEAREST_SEGMENT));
    when(mSectionCardBuilder.buildTo(any(Section.class), anyBoolean(), anyLong())).thenReturn(
        fakeCardList);

    List<Card> cards = mCarouselBookingBuilder.buildCards();

    assertEquals(0, cards.size());
  }

  @Test public void buildToWithBookingsButNotValidTime() {
    when(mCarouselManagerInterface.mustShow(anyLong(), any(Card.CardType.class))).thenReturn(true);
    when(mMyTripsInteractor.getNextBooking()).thenReturn(fakeBooking);
    when(mGetNextSegmentByDateInteractor.getNextValidSegment(anyListOf(Segment.class))).thenReturn(
        fakeSegments.get(NEAREST_SEGMENT));
    when(mSectionCardBuilder.correctTimeToShowCard()).thenReturn(false);

    List<Card> cards = mCarouselBookingBuilder.buildCards();

    assertEquals(0, cards.size());
  }
}
