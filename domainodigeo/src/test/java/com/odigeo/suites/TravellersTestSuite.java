package com.odigeo.suites;

import com.odigeo.data.parser.TravellerRequestToUserTravellerParserTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    TravellerRequestToUserTravellerParserTest.class
}) public class TravellersTestSuite {
}
