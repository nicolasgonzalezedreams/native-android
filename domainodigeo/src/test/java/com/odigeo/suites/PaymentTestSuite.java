package com.odigeo.suites;

import com.odigeo.interactors.CheckBinFromLocalInteractorTest;
import com.odigeo.interactors.CheckBinFromNetworkInteractorTest;
import com.odigeo.presenter.BinCheckPresenterTest;
import com.odigeo.presenter.PaymentFormWidgetPresenterTest;
import com.odigeo.presenter.PaymentPresenterTest;
import com.odigeo.presenter.PaymentPurchasePresenterTest;
import com.odigeo.presenter.PriceBreakdownWidgetPresenterTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    CheckBinFromLocalInteractorTest.class, CheckBinFromNetworkInteractorTest.class,
    BinCheckPresenterTest.class, PaymentFormWidgetPresenterTest.class, PaymentPresenterTest.class,
    PriceBreakdownWidgetPresenterTest.class, PaymentPurchasePresenterTest.class
}) public class PaymentTestSuite {
}
