package com.odigeo.suites;

import com.odigeo.builder.CarouselBookingBuilderTest;
import com.odigeo.builder.CarouselDropOffBuilderTest;
import com.odigeo.builder.CarouselPromotionBuilderTest;
import com.odigeo.interactors.GetNextSegmentByDateInteractorTest;
import com.odigeo.presenter.SearchDropOffCardViewPresenterTest;
import com.odigeo.presenter.SplashPresenterTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    CarouselBookingBuilderTest.class, CarouselPromotionBuilderTest.class,
    CarouselDropOffBuilderTest.class, SearchDropOffCardViewPresenterTest.class,
    GetNextSegmentByDateInteractorTest.class, SplashPresenterTest.class
}) public class HomeTestSuite {
}
