package com.odigeo.suites;

import com.odigeo.interactors.provider.MoveFlightStatusSwitchInteractorTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    MoveFlightStatusSwitchInteractorTest.class,
}) public class SettingsTestSuite {
}
