package com.odigeo.suites;

import com.odigeo.tools.CheckerTest;
import com.odigeo.tools.MultiValueMapTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    CheckerTest.class, MultiValueMapTest.class
}) public class ToolsTestSuite {
}
