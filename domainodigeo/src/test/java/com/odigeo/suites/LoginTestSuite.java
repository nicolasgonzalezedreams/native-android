package com.odigeo.suites;

import com.odigeo.interactors.LoginInteractorTest;
import com.odigeo.interactors.LogoutInteractorTest;
import com.odigeo.interactors.UpdateCitiesInteractorTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    UpdateCitiesInteractorTest.class, LoginInteractorTest.class,
    LogoutInteractorTest.class
}) public class LoginTestSuite {
}
