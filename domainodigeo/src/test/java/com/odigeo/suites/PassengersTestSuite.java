package com.odigeo.suites;

import com.odigeo.interactors.AddPassengerToShoppingCartInteractorTest;
import com.odigeo.presenter.BaggageCollectionItemPresenterTest;
import com.odigeo.presenter.BaggageCollectionPresenterTest;
import com.odigeo.presenter.PassengerPresenterTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    AddPassengerToShoppingCartInteractorTest.class, PassengerPresenterTest.class,
    BaggageCollectionPresenterTest.class, BaggageCollectionItemPresenterTest.class
}) public class PassengersTestSuite {
}
