package com.odigeo.suites;

import com.odigeo.presenter.InsurancesPresenterTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    InsurancesPresenterTest.class
}) public class InsurancesTestSuite {
}
