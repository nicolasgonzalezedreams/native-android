package com.odigeo.suites;

import com.odigeo.interactors.RemoveHistorySearchesInteractorTest;
import com.odigeo.interactors.SearchFlightInteractorTest;
import com.odigeo.presenter.CalendarPresenterTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    RemoveHistorySearchesInteractorTest.class, SearchFlightInteractorTest.class,
    CalendarPresenterTest.class
}) public class SearchTestSuite {
}
