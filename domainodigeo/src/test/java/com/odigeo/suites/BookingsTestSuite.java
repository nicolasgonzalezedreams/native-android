package com.odigeo.suites;

import com.odigeo.interactors.AddProductsToShoppingCartInteractorTest;
import com.odigeo.interactors.BookingInteractorTest;
import com.odigeo.interactors.ConfirmBookingInteractorTest;
import com.odigeo.interactors.DeleteBookingsInteractorTest;
import com.odigeo.interactors.RemoveProductsFromShoppingCartInteractorTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    AddProductsToShoppingCartInteractorTest.class,
    RemoveProductsFromShoppingCartInteractorTest.class, BookingInteractorTest.class,
    ConfirmBookingInteractorTest.class, DeleteBookingsInteractorTest.class
}) public class BookingsTestSuite {
}
