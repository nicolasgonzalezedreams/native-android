package com.odigeo.suites;

import com.odigeo.interactors.RegisterPasswordInteractorTest;
import com.odigeo.presenter.RegisterPresenterTest;
import com.odigeo.presenter.RegisterViewPresenterTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    RegisterPasswordInteractorTest.class, RegisterPresenterTest.class,
    RegisterViewPresenterTest.class
}) public class RegistrationTestSuite {
}
