package com.odigeo.suites;

import com.odigeo.interactors.SendFeedbackInteractorTest;
import com.odigeo.presenter.ArrivalGuidesCardPresenterTest;
import com.odigeo.presenter.MyTripsPresenterTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    SendFeedbackInteractorTest.class, ArrivalGuidesCardPresenterTest.class,
    MyTripsPresenterTest.class
}) public class TripsTestSuite {
}
