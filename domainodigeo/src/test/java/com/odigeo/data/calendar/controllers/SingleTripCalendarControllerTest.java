package com.odigeo.data.calendar.controllers;

import com.odigeo.builder.CalendarControllerBuilder;
import com.odigeo.data.entity.TravelType;
import com.odigeo.presenter.CalendarPresenter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static com.odigeo.data.calendar.controllers.CalendarController.SELECTION_MODE_RANGE;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class) public class SingleTripCalendarControllerTest {

  private static final int MOCK_SEGMENT_NUMBER = 0;

  @Mock private CalendarPresenter presenter;

  private CalendarController controller;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    controller = new CalendarControllerBuilder().build(TravelType.SIMPLE, presenter);
  }

  @Test public void shouldInitCalendar() {
    ArrayList<Date> selectedDates = getSelectedDates();

    controller.initCalendar(selectedDates, MOCK_SEGMENT_NUMBER);

    verify(presenter, atLeastOnce()).hideDatesHeader();
    verify(presenter, atLeastOnce()).setSelectorBackground();
    verify(presenter, atLeastOnce()).setMinDate(any(Date.class));
    verify(presenter, atLeastOnce()).setMaxDate(any(Date.class));
  }

  @Test public void shouldShowSelectedDates() {
    ArrayList<Date> selectedDates = getSelectedDates();
    controller.initCalendar(selectedDates, MOCK_SEGMENT_NUMBER);

    controller.showSelectedDates(selectedDates, false);

    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_RANGE), eq(false));
  }

  @Test public void shouldAddDateSelected() {
    ArrayList<Date> selectedDates = getSelectedDates();
    controller.initCalendar(selectedDates, MOCK_SEGMENT_NUMBER);
    ArgumentCaptor<Date> captorSelectedDate = ArgumentCaptor.forClass(Date.class);

    Date newSelectedDate = getNewSelectedDates(selectedDates);
    controller.dateSelected(selectedDates, newSelectedDate, MOCK_SEGMENT_NUMBER);

    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_RANGE), eq(true));
    verify(presenter, atLeastOnce()).setConfirmationButtonDates(captorSelectedDate.capture());
    assertTrue(areSameDay(newSelectedDate, captorSelectedDate.getValue()));
    verify(presenter, atLeastOnce()).showConfirmationButton();
  }

  public ArrayList<Date> getSelectedDates() {
    ArrayList<Date> selectedDates = new ArrayList<>();
    selectedDates.add(new Date());
    return selectedDates;
  }

  public Date getNewSelectedDates(ArrayList<Date> selectedDates) {
    Date currentSelected = selectedDates.get(SingleTripCalendarController.DATES_INDEX_DEPARTURE);

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(currentSelected);
    calendar.add(Calendar.DAY_OF_YEAR, 1);

    return calendar.getTime();
  }

  public boolean areSameDay(Date first, Date second) {
    Calendar cal1 = Calendar.getInstance();
    Calendar cal2 = Calendar.getInstance();
    cal1.setTime(first);
    cal2.setTime(second);
    return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
        && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
  }
}
