package com.odigeo.data.calendar.controllers;

import com.odigeo.builder.CalendarControllerBuilder;
import com.odigeo.data.entity.TravelType;
import com.odigeo.presenter.CalendarPresenter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static com.odigeo.data.calendar.controllers.CalendarController.SELECTION_MODE_MULTI;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class) public class MultiTripCalendarControllerTest {

  private static final int MOCK_SEGMENT_NUMBER_ZERO = 0;
  private static final int MOCK_SEGMENT_NUMBER_ONE = 1;
  private static final int MOCK_SEGMENT_NUMBER_TWO = 2;
  private static final int MOCK_SEGMENT_NUMBER_THREE = 3;
  private static final int MOCK_SEGMENT_NUMBER_FOUR = 4;
  private static final int MOCK_SEGMENT_NUMBER_FIVE = 5;
  private static final int DATES_LIST_SIZE = 6;

  @Mock private CalendarPresenter presenter;

  private CalendarController controller;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    controller = new CalendarControllerBuilder().build(TravelType.MULTIDESTINATION, presenter);
  }

  @Test public void shouldInitCalendar() {
    ArrayList<Date> selectedDates = getEmptySelectedDates();

    controller.initCalendar(selectedDates, MOCK_SEGMENT_NUMBER_ZERO);

    verify(presenter, atLeastOnce()).hideDatesHeader();
    verify(presenter, atLeastOnce()).setSelectorBackground();
    verify(presenter, atLeastOnce()).setMinDate(any(Date.class));
    verify(presenter, atLeastOnce()).setMaxDate(any(Date.class));
  }

  @Test public void shouldShowSelectedDatesEmpty() {
    ArrayList<Date> selectedDates = getEmptySelectedDates();
    controller.initCalendar(selectedDates, MOCK_SEGMENT_NUMBER_ZERO);

    controller.showSelectedDates(selectedDates, false);

    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_MULTI), eq(false));
  }

  @Test public void shouldShowSelectedDates() {
    ArrayList<Date> selectedDates = getSelectedDates(MOCK_SEGMENT_NUMBER_FIVE);
    controller.initCalendar(selectedDates, MOCK_SEGMENT_NUMBER_FIVE);

    controller.showSelectedDates(selectedDates, false);

    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_MULTI), eq(false));
  }

  @Test public void shouldAddDateSelectedSegmentZero() {
    ArrayList<Date> selectedDates = getEmptySelectedDates();
    controller.initCalendar(selectedDates, selectedDates.size());
    ArgumentCaptor<Date> captorSelectedDate = ArgumentCaptor.forClass(Date.class);
    Date newSelectedDate = getNewSelectedDate(null);

    controller.dateSelected(selectedDates, newSelectedDate, MOCK_SEGMENT_NUMBER_ZERO);

    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_MULTI), eq(true));
    assertNotNull(selectedDates.get(MOCK_SEGMENT_NUMBER_ZERO));
    assertTrue(areSameDay(newSelectedDate, selectedDates.get(MOCK_SEGMENT_NUMBER_ZERO)));
    verify(presenter, atLeastOnce()).setConfirmationButtonDates(captorSelectedDate.capture());
    assertTrue(areSameDay(newSelectedDate, captorSelectedDate.getValue()));
    verify(presenter, atLeastOnce()).showConfirmationButton();
  }

  @Test public void shouldAddDateSelectedSegmentOne() {
    ArrayList<Date> selectedDates = getSelectedDates(MOCK_SEGMENT_NUMBER_ZERO);
    controller.initCalendar(selectedDates, selectedDates.size());
    ArgumentCaptor<Date> captorSelectedDate = ArgumentCaptor.forClass(Date.class);
    Date newSelectedDate = getNewSelectedDate(selectedDates.get(MOCK_SEGMENT_NUMBER_ZERO));

    controller.dateSelected(selectedDates, newSelectedDate, MOCK_SEGMENT_NUMBER_ONE);

    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_MULTI), eq(true));
    assertNotNull(selectedDates.get(MOCK_SEGMENT_NUMBER_ONE));
    assertTrue(areSameDay(newSelectedDate, selectedDates.get(MOCK_SEGMENT_NUMBER_ONE)));
    verify(presenter, atLeastOnce()).setConfirmationButtonDates(captorSelectedDate.capture());
    assertTrue(areSameDay(newSelectedDate, captorSelectedDate.getValue()));
    verify(presenter, atLeastOnce()).showConfirmationButton();
  }

  @Test public void shouldAddDateSelectedSegmentTwo() {
    ArrayList<Date> selectedDates = getSelectedDates(MOCK_SEGMENT_NUMBER_ONE);
    controller.initCalendar(selectedDates, selectedDates.size());
    ArgumentCaptor<Date> captorSelectedDate = ArgumentCaptor.forClass(Date.class);
    Date newSelectedDate = getNewSelectedDate(selectedDates.get(MOCK_SEGMENT_NUMBER_ONE));

    controller.dateSelected(selectedDates, newSelectedDate, MOCK_SEGMENT_NUMBER_TWO);

    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_MULTI), eq(true));
    assertNotNull(selectedDates.get(MOCK_SEGMENT_NUMBER_TWO));
    assertTrue(areSameDay(newSelectedDate, selectedDates.get(MOCK_SEGMENT_NUMBER_TWO)));
    verify(presenter, atLeastOnce()).setConfirmationButtonDates(captorSelectedDate.capture());
    assertTrue(areSameDay(newSelectedDate, captorSelectedDate.getValue()));
    verify(presenter, atLeastOnce()).showConfirmationButton();
  }

  @Test public void shouldAddDateSelectedSegmentThree() {
    ArrayList<Date> selectedDates = getSelectedDates(MOCK_SEGMENT_NUMBER_TWO);
    controller.initCalendar(selectedDates, selectedDates.size());
    ArgumentCaptor<Date> captorSelectedDate = ArgumentCaptor.forClass(Date.class);
    Date newSelectedDate = getNewSelectedDate(selectedDates.get(MOCK_SEGMENT_NUMBER_TWO));

    controller.dateSelected(selectedDates, newSelectedDate, MOCK_SEGMENT_NUMBER_THREE);

    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_MULTI), eq(true));
    assertNotNull(selectedDates.get(MOCK_SEGMENT_NUMBER_THREE));
    assertTrue(areSameDay(newSelectedDate, selectedDates.get(MOCK_SEGMENT_NUMBER_THREE)));
    verify(presenter, atLeastOnce()).setConfirmationButtonDates(captorSelectedDate.capture());
    assertTrue(areSameDay(newSelectedDate, captorSelectedDate.getValue()));
    verify(presenter, atLeastOnce()).showConfirmationButton();
  }

  @Test public void shouldAddDateSelectedSegmentFour() {
    ArrayList<Date> selectedDates = getSelectedDates(MOCK_SEGMENT_NUMBER_THREE);
    controller.initCalendar(selectedDates, selectedDates.size());
    ArgumentCaptor<Date> captorSelectedDate = ArgumentCaptor.forClass(Date.class);
    Date newSelectedDate = getNewSelectedDate(selectedDates.get(MOCK_SEGMENT_NUMBER_THREE));

    controller.dateSelected(selectedDates, newSelectedDate, MOCK_SEGMENT_NUMBER_FOUR);

    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_MULTI), eq(true));
    assertNotNull(selectedDates.get(MOCK_SEGMENT_NUMBER_FOUR));
    assertTrue(areSameDay(newSelectedDate, selectedDates.get(MOCK_SEGMENT_NUMBER_FOUR)));
    verify(presenter, atLeastOnce()).setConfirmationButtonDates(captorSelectedDate.capture());
    assertTrue(areSameDay(newSelectedDate, captorSelectedDate.getValue()));
    verify(presenter, atLeastOnce()).showConfirmationButton();
  }

  @Test public void shouldAddDateSelectedSegmentFive() {
    ArrayList<Date> selectedDates = getSelectedDates(MOCK_SEGMENT_NUMBER_FOUR);
    controller.initCalendar(selectedDates, selectedDates.size());
    ArgumentCaptor<Date> captorSelectedDate = ArgumentCaptor.forClass(Date.class);
    Date newSelectedDate = getNewSelectedDate(selectedDates.get(MOCK_SEGMENT_NUMBER_FOUR));

    controller.dateSelected(selectedDates, newSelectedDate, MOCK_SEGMENT_NUMBER_FIVE);

    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_MULTI), eq(true));
    assertNotNull(selectedDates.get(MOCK_SEGMENT_NUMBER_FIVE));
    assertTrue(areSameDay(newSelectedDate, selectedDates.get(MOCK_SEGMENT_NUMBER_FIVE)));
    verify(presenter, atLeastOnce()).setConfirmationButtonDates(captorSelectedDate.capture());
    assertTrue(areSameDay(newSelectedDate, captorSelectedDate.getValue()));
    verify(presenter, atLeastOnce()).showConfirmationButton();
  }

  private ArrayList<Date> getEmptySelectedDates() {
    ArrayList<Date> selectedDates = new ArrayList<>();
    for (int i = 0; i < DATES_LIST_SIZE; i++) {
      selectedDates.add(null);
    }

    return selectedDates;
  }

  private ArrayList<Date> getSelectedDates(int segmentNumber) {
    ArrayList<Date> selectedDates = getEmptySelectedDates();
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());

    for (int i = 0; i <= segmentNumber; i++) {
      calendar.add(Calendar.DAY_OF_YEAR, 2);
      selectedDates.set(i, calendar.getTime());
    }

    return selectedDates;
  }

  private Date getNewSelectedDate(Date date) {
    Calendar calendar = Calendar.getInstance();
    if (date != null) {
      calendar.setTime(date);
    } else {
      calendar.setTime(new Date());
    }
    calendar.add(Calendar.DAY_OF_YEAR, 2);

    return calendar.getTime();
  }

  private boolean areSameDay(Date first, Date second) {
    Calendar cal1 = Calendar.getInstance();
    Calendar cal2 = Calendar.getInstance();
    cal1.setTime(first);
    cal2.setTime(second);
    return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
        && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
  }
}
