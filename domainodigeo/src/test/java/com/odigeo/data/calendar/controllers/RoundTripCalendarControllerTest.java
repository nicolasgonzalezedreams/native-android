package com.odigeo.data.calendar.controllers;

import com.odigeo.builder.CalendarControllerBuilder;
import com.odigeo.data.entity.TravelType;
import com.odigeo.presenter.CalendarPresenter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static com.odigeo.data.calendar.controllers.CalendarController.SELECTION_MODE_RANGE;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class) public class RoundTripCalendarControllerTest {

  @Mock private CalendarPresenter presenter;

  private CalendarController controller;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    controller = new CalendarControllerBuilder().build(TravelType.ROUND, presenter);
  }

  @Test public void shouldInitCalendar() {
    ArrayList<Date> selectedDates = getSelectedDates(true);

    controller.initCalendar(selectedDates, RoundTripCalendarController.DATES_INDEX_DEPARTURE);

    verify(presenter, atLeastOnce()).showDatesHeader();
    verify(presenter, atLeastOnce()).setSelectorBackground();
    verify(presenter, atLeastOnce()).setMinDate(any(Date.class));
    verify(presenter, atLeastOnce()).setMaxDate(any(Date.class));
  }

  @Test public void shouldShowSelectedDatesEmpty() {
    ArrayList<Date> selectedDates = getEmptySelectedDates();
    controller.initCalendar(selectedDates, RoundTripCalendarController.DATES_INDEX_DEPARTURE);

    controller.showSelectedDates(selectedDates, false);

    verify(presenter, atLeastOnce()).resetDepartureDateHeader();
    verify(presenter, atLeastOnce()).resetReturnDateHeader();
    verify(presenter, atLeastOnce()).hideSkipReturnButton();
    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_RANGE), eq(false));
  }

  @Test public void shouldShowSelectedDatesOnlyDeparture() {
    ArrayList<Date> selectedDates = getSelectedDates(true);
    controller.initCalendar(selectedDates, RoundTripCalendarController.DATES_INDEX_DEPARTURE);
    ArgumentCaptor<Date> captorSelectedDate = ArgumentCaptor.forClass(Date.class);

    controller.showSelectedDates(selectedDates, false);

    verify(presenter, atLeastOnce()).setDepartureDateHeader(captorSelectedDate.capture());
    assertTrue(areSameDay(selectedDates.get(RoundTripCalendarController.DATES_INDEX_DEPARTURE),
        captorSelectedDate.getValue()));
    verify(presenter, atLeastOnce()).resetReturnDateHeader();
    verify(presenter, atLeastOnce()).showSkipReturnButton();
    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_RANGE), eq(false));
  }

  @Test public void shouldShowSelectedDatesDepartureAndReturn() {
    ArrayList<Date> selectedDates = getSelectedDates(false);
    controller.initCalendar(selectedDates, RoundTripCalendarController.DATES_INDEX_RETURN);
    ArgumentCaptor<Date> captorSelectedDate = ArgumentCaptor.forClass(Date.class);

    controller.showSelectedDates(selectedDates, false);

    verify(presenter, atLeastOnce()).setDepartureDateHeader(captorSelectedDate.capture());
    assertTrue(areSameDay(selectedDates.get(RoundTripCalendarController.DATES_INDEX_DEPARTURE),
        captorSelectedDate.getValue()));
    verify(presenter, atLeastOnce()).setReturnDateHeader(captorSelectedDate.capture());
    assertTrue(areSameDay(selectedDates.get(RoundTripCalendarController.DATES_INDEX_RETURN),
        captorSelectedDate.getValue()));
    verify(presenter, atLeastOnce()).hideSkipReturnButton();
    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_RANGE), eq(false));
  }

  @Test public void shouldAddDateSelectedOnlyDeparture() {
    ArrayList<Date> selectedDates = getEmptySelectedDates();
    controller.initCalendar(selectedDates, RoundTripCalendarController.DATES_INDEX_DEPARTURE);
    ArgumentCaptor<Date> captorSelectedDate = ArgumentCaptor.forClass(Date.class);
    Date newSelectedDate = getNewSelectedDateDeparture();

    controller.dateSelected(selectedDates, newSelectedDate,
        RoundTripCalendarController.DATES_INDEX_DEPARTURE);

    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_RANGE), eq(true));
    assertNotNull(selectedDates.get(RoundTripCalendarController.DATES_INDEX_DEPARTURE));
    verify(presenter, atLeastOnce()).setDepartureDateHeader(captorSelectedDate.capture());
    assertTrue(areSameDay(newSelectedDate, captorSelectedDate.getValue()));
    assertTrue(areSameDay(newSelectedDate,
        selectedDates.get(RoundTripCalendarController.DATES_INDEX_DEPARTURE)));
    assertNull(selectedDates.get(RoundTripCalendarController.DATES_INDEX_RETURN));
    verify(presenter, atLeastOnce()).showSkipReturnButton();
    verify(presenter, atLeastOnce()).hideConfirmationButton();
  }

  @Test public void shouldAddDateSelectedDepartureAndReturn() {
    ArrayList<Date> selectedDates = getEmptySelectedDates();
    controller.initCalendar(selectedDates, RoundTripCalendarController.DATES_INDEX_DEPARTURE);
    ArgumentCaptor<Date> captorSelectedDateDeparture = ArgumentCaptor.forClass(Date.class);
    ArgumentCaptor<Date> captorSelectedDateReturn = ArgumentCaptor.forClass(Date.class);
    Date newSelectedDate = getNewSelectedDateDeparture();
    controller.dateSelected(selectedDates, newSelectedDate,
        RoundTripCalendarController.DATES_INDEX_DEPARTURE);
    Date newSelectedDateReturn = getNewSelectedDateReturn(newSelectedDate);

    controller.dateSelected(selectedDates, newSelectedDateReturn,
        RoundTripCalendarController.DATES_INDEX_RETURN);

    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_RANGE), eq(true));
    assertNotNull(selectedDates.get(RoundTripCalendarController.DATES_INDEX_RETURN));
    verify(presenter, atLeastOnce()).setReturnDateHeader(captorSelectedDateReturn.capture());
    assertTrue(areSameDay(newSelectedDateReturn, captorSelectedDateReturn.getValue()));
    assertTrue(areSameDay(newSelectedDateReturn,
        selectedDates.get(RoundTripCalendarController.DATES_INDEX_RETURN)));
    verify(presenter, atLeastOnce()).hideSkipReturnButton();
    verify(presenter, atLeastOnce()).setConfirmationButtonDates(
        captorSelectedDateDeparture.capture(), captorSelectedDateReturn.capture());
    assertTrue(areSameDay(newSelectedDate, captorSelectedDateDeparture.getValue()));
    assertTrue(areSameDay(newSelectedDateReturn, captorSelectedDateReturn.getValue()));
    verify(presenter, atLeastOnce()).showConfirmationButton();
  }

  @Test public void shouldAddDateSelectedReturnBeforeDepartureDate() {
    ArrayList<Date> selectedDates = getEmptySelectedDates();
    controller.initCalendar(selectedDates, RoundTripCalendarController.DATES_INDEX_DEPARTURE);
    ArgumentCaptor<Date> captorSelectedDateDeparture = ArgumentCaptor.forClass(Date.class);
    Date newSelectedDate = getNewSelectedDateDeparture();
    controller.dateSelected(selectedDates, newSelectedDate,
        RoundTripCalendarController.DATES_INDEX_DEPARTURE);
    Date newSelectedDateReturn = getNewSelectedDateReturnBeforeDeparture(newSelectedDate);

    controller.dateSelected(selectedDates, newSelectedDateReturn,
        RoundTripCalendarController.DATES_INDEX_RETURN);

    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_RANGE), eq(true));
    verify(presenter, atLeastOnce()).setDepartureDateHeader(captorSelectedDateDeparture.capture());
    assertTrue(areSameDay(newSelectedDateReturn, captorSelectedDateDeparture.getValue()));
    assertTrue(areSameDay(newSelectedDateReturn,
        selectedDates.get(RoundTripCalendarController.DATES_INDEX_DEPARTURE)));
    assertNull(selectedDates.get(RoundTripCalendarController.DATES_INDEX_RETURN));
    verify(presenter, atLeastOnce()).showSkipReturnButton();
    verify(presenter, atLeastOnce()).hideConfirmationButton();
  }

  @Test public void shouldAddDateSelectedWhenDepartureAndReturnAlreadySelected() {
    ArrayList<Date> selectedDates = getEmptySelectedDates();
    controller.initCalendar(selectedDates, RoundTripCalendarController.DATES_INDEX_DEPARTURE);
    ArgumentCaptor<Date> captorSelectedDate = ArgumentCaptor.forClass(Date.class);
    Date newSelectedDate = getNewSelectedDateDeparture();
    controller.dateSelected(selectedDates, newSelectedDate,
        RoundTripCalendarController.DATES_INDEX_DEPARTURE);
    Date newSelectedDateReturn = getNewSelectedDateReturn(newSelectedDate);
    controller.dateSelected(selectedDates, newSelectedDateReturn,
        RoundTripCalendarController.DATES_INDEX_RETURN);
    newSelectedDate = getNewSelectedDateDeparture();

    controller.dateSelected(selectedDates, newSelectedDate,
        RoundTripCalendarController.DATES_INDEX_DEPARTURE);

    verify(presenter, atLeastOnce()).setCalendarBounds(any(Date.class), any(Date.class),
        eq(SELECTION_MODE_RANGE), eq(true));
    assertNotNull(selectedDates.get(RoundTripCalendarController.DATES_INDEX_DEPARTURE));
    verify(presenter, atLeastOnce()).setDepartureDateHeader(captorSelectedDate.capture());
    assertTrue(areSameDay(newSelectedDate, captorSelectedDate.getValue()));
    assertTrue(areSameDay(newSelectedDate,
        selectedDates.get(RoundTripCalendarController.DATES_INDEX_DEPARTURE)));
    assertNull(selectedDates.get(RoundTripCalendarController.DATES_INDEX_RETURN));
    verify(presenter, atLeastOnce()).showSkipReturnButton();
    verify(presenter, atLeastOnce()).hideConfirmationButton();
  }

  private ArrayList<Date> getEmptySelectedDates() {
    ArrayList<Date> selectedDates = new ArrayList<>();
    selectedDates.add(null);
    selectedDates.add(null);

    return selectedDates;
  }

  private ArrayList<Date> getSelectedDates(boolean onlyDeparture) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());
    ArrayList<Date> selectedDates = new ArrayList<>();
    selectedDates.add(calendar.getTime());

    if (onlyDeparture) {
      selectedDates.add(null);
    } else {
      calendar.add(Calendar.DAY_OF_MONTH, 5);
      selectedDates.add(calendar.getTime());
    }

    return selectedDates;
  }

  private Date getNewSelectedDateDeparture() {
    Date currentSelected = new Date();

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(currentSelected);
    calendar.add(Calendar.DAY_OF_YEAR, 1);

    return calendar.getTime();
  }

  private Date getNewSelectedDateReturn(Date departureDate) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(departureDate);
    calendar.add(Calendar.DAY_OF_YEAR, 1);

    return calendar.getTime();
  }

  private Date getNewSelectedDateReturnBeforeDeparture(Date departureDate) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(departureDate);
    calendar.add(Calendar.DAY_OF_YEAR, -1);

    return calendar.getTime();
  }

  private boolean areSameDay(Date first, Date second) {
    Calendar cal1 = Calendar.getInstance();
    Calendar cal2 = Calendar.getInstance();
    cal1.setTime(first);
    cal2.setTime(second);
    return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
        && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
  }
}
