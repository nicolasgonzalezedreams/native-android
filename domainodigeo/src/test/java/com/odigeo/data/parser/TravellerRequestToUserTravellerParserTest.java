package com.odigeo.data.parser;

import com.odigeo.data.entity.parser.TravellerRequestToUserTravellerParser;
import com.odigeo.data.entity.shoppingCart.request.FrequentFlyerCardCodeRequest;
import com.odigeo.data.entity.shoppingCart.request.TravellerRequest;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TravellerRequestToUserTravellerParserTest {

  private TravellerRequest mTravellerRequest;

  @Before public void setUpTravellerRequest() {
    mTravellerRequest = new TravellerRequest();
    mTravellerRequest.setTravellerTypeName("ADULT");
    mTravellerRequest.setTitleName("MR");
    mTravellerRequest.setName("Javier");
    mTravellerRequest.setFirstLastName("Rebollo");
    mTravellerRequest.setSecondLastName("Cinta");
    mTravellerRequest.setMiddleName("Anom");
    mTravellerRequest.setGender("MALE");
    mTravellerRequest.setDayOfBirth(30);
    mTravellerRequest.setMonthOfBirth(1);
    mTravellerRequest.setYearOfBirth(1990);
    mTravellerRequest.setNationalityCountryCode("ES");
    mTravellerRequest.setIdentification("11111111A");
    mTravellerRequest.setIdentificationTypeName("NIE");
    mTravellerRequest.setIdentificationExpirationDay(1);
    mTravellerRequest.setIdentificationExpirationMonth(5);
    mTravellerRequest.setIdentificationExpirationYear(2020);
    mTravellerRequest.setIdentificationIssueContryCode("ES");
    mTravellerRequest.setFrequentFlyerAirlineCodes(buildFrequentFlyer());
  }

  private List<FrequentFlyerCardCodeRequest> buildFrequentFlyer() {
    List<FrequentFlyerCardCodeRequest> frequentFlyerCardCodeRequestsList = new ArrayList<>();

    FrequentFlyerCardCodeRequest frequentFlyerCardCodeRequest = new FrequentFlyerCardCodeRequest();
    frequentFlyerCardCodeRequest.setCarrierCode("IB");
    frequentFlyerCardCodeRequest.setPassengerCardNumber("123abc");

    frequentFlyerCardCodeRequestsList.add(frequentFlyerCardCodeRequest);

    return frequentFlyerCardCodeRequestsList;
  }

  @Test public void travellerRequestToUserTraveller() {
    UserTraveller userTraveller =
        new TravellerRequestToUserTravellerParser().travellerRequestToUserTraveller(
            mTravellerRequest);
    Assert.assertEquals(userTraveller.getTypeOfTraveller().toString(),
        mTravellerRequest.getTravellerTypeName());
    Assert.assertEquals(userTraveller.getUserProfile().getName(), mTravellerRequest.getName());
    Assert.assertEquals(userTraveller.getUserProfile().getFirstLastName(),
        mTravellerRequest.getFirstLastName());
    Assert.assertEquals(userTraveller.getUserProfile().getSecondLastName(),
        mTravellerRequest.getSecondLastName());
    Assert.assertEquals(userTraveller.getUserProfile().getMiddleName(),
        mTravellerRequest.getMiddleName());
    Assert.assertEquals(userTraveller.getUserProfile().getGender(), mTravellerRequest.getGender());
    Assert.assertEquals(
        getDateToLongDate(userTraveller.getUserProfile().getBirthDate(), Calendar.DAY_OF_MONTH),
        mTravellerRequest.getDayOfBirth());
    Assert.assertEquals(
        getDateToLongDate(userTraveller.getUserProfile().getBirthDate(), Calendar.MONTH) + 1,
        mTravellerRequest.getMonthOfBirth());
    Assert.assertEquals(
        getDateToLongDate(userTraveller.getUserProfile().getBirthDate(), Calendar.YEAR),
        mTravellerRequest.getYearOfBirth());
    Assert.assertEquals(userTraveller.getUserProfile().getNationalityCountryCode(),
        mTravellerRequest.getNationalityCountryCode());
    Assert.assertEquals(userTraveller.getUserProfile()
        .getUserIdentificationList()
        .get(0)
        .getIdentificationType()
        .toString(), mTravellerRequest.getIdentificationTypeName());
    Assert.assertEquals(getDateToLongDate(userTraveller.getUserProfile()
            .getUserIdentificationList()
            .get(0)
            .getIdentificationExpirationDate(), Calendar.DAY_OF_MONTH),
        mTravellerRequest.getIdentificationExpirationDay());
    Assert.assertEquals(getDateToLongDate(userTraveller.getUserProfile()
            .getUserIdentificationList()
            .get(0)
            .getIdentificationExpirationDate(), Calendar.MONTH) + 1,
        mTravellerRequest.getIdentificationExpirationMonth());
    Assert.assertEquals(getDateToLongDate(userTraveller.getUserProfile()
            .getUserIdentificationList()
            .get(0)
            .getIdentificationExpirationDate(), Calendar.YEAR),
        mTravellerRequest.getIdentificationExpirationYear());
    Assert.assertEquals(userTraveller.getUserProfile()
        .getUserIdentificationList()
        .get(0)
        .getIdentificationCountryCode(), mTravellerRequest.getIdentificationIssueContryCode());
    Assert.assertEquals(userTraveller.getUserFrequentFlyers().get(0).getFrequentFlyerNumber(),
        mTravellerRequest.getFrequentFlyerAirlineCodes().get(0).getPassengerCardNumber());
    Assert.assertEquals(userTraveller.getUserFrequentFlyers().get(0).getAirlineCode(),
        mTravellerRequest.getFrequentFlyerAirlineCodes().get(0).getCarrierCode());
  }

  private long getDateToLongDate(long date, int field) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(date);
    return calendar.get(field);
  }
}