package com.odigeo.helper;

import com.odigeo.configuration.ABAlias;
import com.odigeo.configuration.ABPartition;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.tracker.CrashlyticsController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class ABTestHelperTest {

  @Mock private PreferencesControllerInterface preferencesController;
  @Mock private CrashlyticsController crashlyticsController;

  private ABTestHelper abTestHelper;
  private int activePartitions;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    abTestHelper = new ABTestHelper(preferencesController, crashlyticsController);
    activePartitions = 4;
  }

  @Test public void shouldGetPartitionOne() {
    when(preferencesController.getTestAssignment(any(ABAlias.class))).thenReturn(1);
    assertEquals(abTestHelper.getPartition(any(ABAlias.class), activePartitions), ABPartition.ONE);
  }

  @Test public void shouldGetPartitionTwo() {
    when(preferencesController.getTestAssignment(any(ABAlias.class))).thenReturn(2);
    assertEquals(abTestHelper.getPartition(any(ABAlias.class), activePartitions), ABPartition.TWO);
  }

  @Test public void shouldGetPartitionThree() {
    when(preferencesController.getTestAssignment(any(ABAlias.class))).thenReturn(3);
    assertEquals(abTestHelper.getPartition(any(ABAlias.class), activePartitions),
        ABPartition.THREE);
  }

  @Test public void shouldGetPartitionFour() {
    when(preferencesController.getTestAssignment(any(ABAlias.class))).thenReturn(4);
    assertEquals(abTestHelper.getPartition(any(ABAlias.class), activePartitions), ABPartition.FOUR);
  }
}
