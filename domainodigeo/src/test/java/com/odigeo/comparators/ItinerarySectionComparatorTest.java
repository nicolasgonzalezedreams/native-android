package com.odigeo.comparators;

import com.odigeo.data.entity.shoppingCart.Section;
import com.odigeo.data.entity.shoppingCart.SectionResult;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class ItinerarySectionComparatorTest {

  private ItinerarySectionComparator itinerarySectionComparator;
  private SectionResult soonerSectionResult, laterSectionResult;

  @Before public void setUp() throws Exception {
    itinerarySectionComparator = new ItinerarySectionComparator();
    soonerSectionResult = provideSoonerSectionResult();
    laterSectionResult = provideLaterSectionResult();
  }

  @Test public void testEqual() {
    int result = itinerarySectionComparator.compare(soonerSectionResult, soonerSectionResult);
    assertTrue(result == 0);
  }

  @Test public void testGreaterThan() {
    int result = itinerarySectionComparator.compare(soonerSectionResult, laterSectionResult);
    assertTrue(result == -1);
  }

  @Test public void testLessThan() {
    int result = itinerarySectionComparator.compare(laterSectionResult, soonerSectionResult);
    assertTrue(result == 1);
  }

  private SectionResult provideSoonerSectionResult() {
    SectionResult sectionResult = new SectionResult();
    Section section = new Section();
    section.setDepartureDate("1500966000532");
    section.setArrivalDate("1500970800532");
    sectionResult.setSection(section);
    return sectionResult;
  }

  private SectionResult provideLaterSectionResult() {
    SectionResult sectionResult = new SectionResult();
    Section section = new Section();
    section.setDepartureDate("1531918731000");
    section.setArrivalDate("1532005131000");
    sectionResult.setSection(section);
    return sectionResult;
  }
}
