package com.odigeo.comparators;

import com.odigeo.data.entity.shoppingCart.CollectionMethod;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import java.math.BigDecimal;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ShoppingCartCollectionOptionComparatorTest {

  ShoppingCartCollectionOptionComparator shoppingCartCollectionOptionComparator;

  @Before public void init() {
    shoppingCartCollectionOptionComparator = new ShoppingCartCollectionOptionComparator();
  }

  @Test public void shouldBeCrediCardBeforePaypal() {

    ShoppingCartCollectionOption creditCardCollectionMethod = givenCreditCardCollectionMethod();
    ShoppingCartCollectionOption paypalCollectionMethod = givenPaypalCollectionMethod();

    int compare = shoppingCartCollectionOptionComparator.compare(creditCardCollectionMethod,
        paypalCollectionMethod);

    assertTrue(compare < 0);
  }

  @Test public void shouldBeCrediCardBeforeTrustly() {

    ShoppingCartCollectionOption creditCardCollectionMethod = givenCreditCardCollectionMethod();
    ShoppingCartCollectionOption trustlyCollectionMethod = givenTrustlyCollectionMethod();

    int compare = shoppingCartCollectionOptionComparator.compare(creditCardCollectionMethod,
        trustlyCollectionMethod);

    assertTrue(compare < 0);
  }

  @Test public void shouldBeCrediCardBeforeKlarna() {

    ShoppingCartCollectionOption creditCardCollectionMethod = givenCreditCardCollectionMethod();
    ShoppingCartCollectionOption klarnaCollectionMethod = givenKlarnaCollectionMethod();

    int compare = shoppingCartCollectionOptionComparator.compare(creditCardCollectionMethod,
        klarnaCollectionMethod);

    assertTrue(compare < 0);
  }

  @Test public void shouldBeCrediCardBeforeBankTransfer() {

    ShoppingCartCollectionOption creditCardCollectionMethod = givenCreditCardCollectionMethod();
    ShoppingCartCollectionOption bankTransferCollectionMethod = givenBankTransferCollectionMethod();

    int compare = shoppingCartCollectionOptionComparator.compare(creditCardCollectionMethod,
        bankTransferCollectionMethod);

    assertTrue(compare < 0);
  }

  @Test public void shouldBePaypalBeforeTrustly() {

    ShoppingCartCollectionOption paypalCollectionMethod = givenPaypalCollectionMethod();
    ShoppingCartCollectionOption trustlyCollectionMethod = givenTrustlyCollectionMethod();

    int compare = shoppingCartCollectionOptionComparator.compare(paypalCollectionMethod,
        trustlyCollectionMethod);

    assertTrue(compare < 0);
  }

  @Test public void shouldBePaypalBeforeKlarna() {

    ShoppingCartCollectionOption paypalCollectionMethod = givenPaypalCollectionMethod();
    ShoppingCartCollectionOption klarnaCollectionMethod = givenKlarnaCollectionMethod();

    int compare = shoppingCartCollectionOptionComparator.compare(paypalCollectionMethod,
        klarnaCollectionMethod);

    assertTrue(compare < 0);
  }

  @Test public void shouldBePaypalBeforeBankTransfer() {

    ShoppingCartCollectionOption paypalCollectionMethod = givenPaypalCollectionMethod();
    ShoppingCartCollectionOption bankTransferCollectionMethod = givenBankTransferCollectionMethod();

    int compare = shoppingCartCollectionOptionComparator.compare(paypalCollectionMethod,
        bankTransferCollectionMethod);

    assertTrue(compare < 0);
  }

  @Test public void shouldBeBankTransferBeforeTrustly() {

    ShoppingCartCollectionOption bankTransferCollectionMethod = givenBankTransferCollectionMethod();
    ShoppingCartCollectionOption trustlyCollectionMethod = givenTrustlyCollectionMethod();

    int compare = shoppingCartCollectionOptionComparator.compare(bankTransferCollectionMethod,
        trustlyCollectionMethod);

    assertTrue(compare < 0);
  }

  @Test public void shouldBeBankTransferBeforeKlarna() {

    ShoppingCartCollectionOption bankTransferCollectionMethod = givenBankTransferCollectionMethod();
    ShoppingCartCollectionOption klarnaCollectionMethod = givenKlarnaCollectionMethod();

    int compare = shoppingCartCollectionOptionComparator.compare(bankTransferCollectionMethod,
        klarnaCollectionMethod);

    assertTrue(compare < 0);
  }

  @Test public void shouldBeTrustlyBeforeKlarna() {

    ShoppingCartCollectionOption trustlyCollectionMethod = givenTrustlyCollectionMethod();
    ShoppingCartCollectionOption klarnaCollectionMethod = givenKlarnaCollectionMethod();

    int compare = shoppingCartCollectionOptionComparator.compare(trustlyCollectionMethod,
        klarnaCollectionMethod);

    assertTrue(compare < 0);
  }

  private ShoppingCartCollectionOption givenBankTransferCollectionMethod() {

    ShoppingCartCollectionOption collectionOption = new ShoppingCartCollectionOption();
    CollectionMethod banktransferCollectionMethod = new CollectionMethod();
    banktransferCollectionMethod.setType(CollectionMethodType.BANKTRANSFER);
    collectionOption.setMethod(banktransferCollectionMethod);

    return collectionOption;
  }

  private ShoppingCartCollectionOption givenKlarnaCollectionMethod() {

    ShoppingCartCollectionOption collectionOption = new ShoppingCartCollectionOption();
    CollectionMethod klarnaCardCollectionMethod = new CollectionMethod();
    klarnaCardCollectionMethod.setType(CollectionMethodType.KLARNA);
    collectionOption.setFee(new BigDecimal(3.00));
    collectionOption.setMethod(klarnaCardCollectionMethod);
    return collectionOption;
  }

  private ShoppingCartCollectionOption givenTrustlyCollectionMethod() {

    ShoppingCartCollectionOption collectionOption = new ShoppingCartCollectionOption();
    CollectionMethod trustlyCardCollectionMethod = new CollectionMethod();
    trustlyCardCollectionMethod.setType(CollectionMethodType.TRUSTLY);
    collectionOption.setFee(new BigDecimal(3.00));
    collectionOption.setMethod(trustlyCardCollectionMethod);
    return collectionOption;
  }

  private ShoppingCartCollectionOption givenCreditCardCollectionMethod() {

    ShoppingCartCollectionOption collectionOption = new ShoppingCartCollectionOption();
    CollectionMethod creditCardCollectionMethod = new CollectionMethod();
    creditCardCollectionMethod.setType(CollectionMethodType.CREDITCARD);
    collectionOption.setMethod(creditCardCollectionMethod);
    return collectionOption;
  }

  private ShoppingCartCollectionOption givenPaypalCollectionMethod() {

    ShoppingCartCollectionOption collectionOption = new ShoppingCartCollectionOption();
    collectionOption.setFee(new BigDecimal(3.00));
    CollectionMethod paypalCollectionMethod = new CollectionMethod();
    paypalCollectionMethod.setType(CollectionMethodType.PAYPAL);
    collectionOption.setMethod(paypalCollectionMethod);
    return collectionOption;
  }
}