package com.odigeo.dataodigeo.session;

import com.odigeo.data.entity.userData.User.Status;

public interface UserInfoInterface {

  long getUserId();

  String getProfilePicture();

  String getName();

  Status getUserStatus();

  String getEmail();
}
