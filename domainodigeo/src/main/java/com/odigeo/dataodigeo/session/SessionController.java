package com.odigeo.dataodigeo.session;

import com.odigeo.data.entity.userData.CreditCard;
import com.odigeo.data.entity.userData.User;
import java.util.List;

public interface SessionController {

  String CREDENTIALS_KEY = "credentials";
  String USER_INFO_KEY = "userinfo";
  String USER_INFO_ID = "userId";
  String USER_INFO_NAME = "name";
  String USER_INFO_IMAGE = "image";
  String USER_INFO_STATUS = "status";
  String USER_INFO_EMAIL = "email";
  String SPLIT_TOKEN = "-#TOKEN#-";
  String SECRET_PHRASE = "superMegaHiperSecreta";
  String GCM_TOKEN_KEY = "gcmKey";
  String GCM_TOKEN = "gcmToken";
  String LOGOUT_WAS_MADE = "logoutWasMade";

  CredentialsInterface getCredentials();

  UserInfoInterface getUserInfo();

  void savePasswordCredentials(String user, String password,
      CredentialsInterface.CredentialsType type);

  void saveSocialCredentials(String user, String token, CredentialsInterface.CredentialsType type);

  void saveUserInfo(long userId, String name, String pictureUrl, String email, User.Status status);

  void saveUserName(String name);

  void saveUserId(long userId);

  void removeUserInfo();

  void removeAllData();

  void saveGcmToken(String token);

  String getGcmToken();

  void invalidateGcmToken();

  void removeSharedPreferencesInfo();

  void saveLogoutWasMade(boolean logoutWasMade);

  boolean getLogoutWasMade();

  void attachCreditCardsToSession(List<CreditCard> creditCards);

  List<CreditCard> getCreditCards();
}
