package com.odigeo.dataodigeo.session;

public interface CredentialsInterface {

  String getUser();

  String getPassword();

  void setPassword(String newPassword);

  CredentialsType getType();

  String getCredentialTypeValue();

  enum CredentialsType {
    PASSWORD, FACEBOOK, GOOGLE
  }
}
