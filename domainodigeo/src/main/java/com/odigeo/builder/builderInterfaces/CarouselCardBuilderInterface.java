package com.odigeo.builder.builderInterfaces;

import com.odigeo.data.entity.carrousel.Card;
import java.util.List;

/**
 * Created by daniel.morales on 9/3/17.
 */

public interface CarouselCardBuilderInterface {
  List<Card> buildCards();
}
