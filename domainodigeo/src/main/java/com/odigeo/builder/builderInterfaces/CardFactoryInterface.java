package com.odigeo.builder.builderInterfaces;

import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.CarouselLocation;

public interface CardFactoryInterface {

  Card builderSectionCardWithFlightStat(Section section, long cardId);

  Card builderSectionCardWithOutFlightStats(Section section, long cardId);

  Card builderBoardingGateCard(Section section, long cardId);

  Card builderCancelledCard(Section section, long cardId);

  Card builderDivertidedCard(Section section, long cardId);

  Card builderDelayCard(Section section, long cardId);

  Card builderBaggageCard(Section section, long cardId);

  CarouselLocation builderLocationFromWithoutFlightStats(Section section);

  CarouselLocation builderLocationFrom(Section section);

  CarouselLocation builderLocationFromUpdated(Section section);

  CarouselLocation builderLocationFromNonUpdated(Section section);

  CarouselLocation builderLocationTo(Section section);

  CarouselLocation builderLocationToWithoutFlightStats(Section section);
}
