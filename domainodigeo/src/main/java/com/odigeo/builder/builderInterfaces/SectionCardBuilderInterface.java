package com.odigeo.builder.builderInterfaces;

public interface SectionCardBuilderInterface<TFrom, TTo> {
  TFrom buildTo(TTo model, boolean ignoreFlightStat, long cardId);
}
