package com.odigeo.builder;

import com.odigeo.builder.builderInterfaces.CarouselCardBuilderInterface;
import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.entity.booking.SearchSegment;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.SearchDropOffCard;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DropOffCarouselCardBuilder implements CarouselCardBuilderInterface {

  private SearchesHandlerInterface mSearchesHandlerInterface;
  private PreferencesControllerInterface mPreferencesControllerInterface;

  public DropOffCarouselCardBuilder(SearchesHandlerInterface searchesHandlerInterface,
      PreferencesControllerInterface preferencesControllerInterface) {
    mSearchesHandlerInterface = searchesHandlerInterface;
    mPreferencesControllerInterface = preferencesControllerInterface;
  }

  @Override public List<Card> buildCards() {
    List<StoredSearch> storedSearches = mSearchesHandlerInterface.getCompleteSearchesFromDB();
    ArrayList<Card> cards = new ArrayList<>();
    if (mPreferencesControllerInterface.getBooleanValue(
        PreferencesControllerInterface.SHOULD_ADD_DROPOFF_CARD)) {
      for (int i = 0; i < storedSearches.size(); ++i) {
        if (storedSearches.get(i).getTripType() != StoredSearch.TripType.M) {
          long departureDate = getDepartureDateFromSegments(storedSearches.get(i).getSegmentList());
          if (allDatesAreCorrect(departureDate)) {
            SearchDropOffCard searchDropOffCard = new SearchDropOffCard();
            searchDropOffCard.setId(storedSearches.get(i).getId());
            searchDropOffCard.setArrivalCityIATA(
                getArrivalCityIATAFromSegments(storedSearches.get(i).getSegmentList()));
            searchDropOffCard.setDepartureCityIATA(
                getDepartureCityIATAFromSegments(storedSearches.get(i).getSegmentList()));
            searchDropOffCard.setDepartureDate(departureDate);

            if (storedSearches.get(i).getSegmentList().size() > 1) {
              searchDropOffCard.setArrivalDate(
                  getArrivalDateFromSegments(storedSearches.get(i).getSegmentList()));
            }

            searchDropOffCard.setNumAdults(storedSearches.get(i).getNumAdults());
            searchDropOffCard.setNumChildren(storedSearches.get(i).getNumChildren());
            searchDropOffCard.setNumInfants(storedSearches.get(i).getNumInfants());
            searchDropOffCard.setDirect(storedSearches.get(i).getIsDirectFlight());
            searchDropOffCard.setCabinClass(storedSearches.get(i).getCabinClass());
            searchDropOffCard.setTripType(storedSearches.get(i).getTripType());
            searchDropOffCard.setType(Card.CardType.DROP_OFF_CARD);
            searchDropOffCard.setPriority(9);

            cards.add(searchDropOffCard);
            return cards;
          }
        }
      }
    }

    return cards;
  }

  private boolean allDatesAreCorrect(long departure) {
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    long todayTimeStamp = calendar.getTimeInMillis();
    return todayTimeStamp <= departure;
  }

  public String getDepartureCityIATAFromSegments(List<SearchSegment> segmentList) {
    SearchSegment segment = segmentList.get(0);
    return segment.getOriginIATACode();
  }

  public String getArrivalCityIATAFromSegments(List<SearchSegment> segmentList) {
    SearchSegment segment = segmentList.get(0);
    return segment.getDestinationIATACode();
  }

  public long getArrivalDateFromSegments(List<SearchSegment> segmentList) {
    SearchSegment segment = segmentList.get(segmentList.size() - 1);
    return segment.getDepartureDate();
  }

  public long getDepartureDateFromSegments(List<SearchSegment> segmentList) {
    SearchSegment segment = segmentList.get(0);
    return segment.getDepartureDate();
  }
}
