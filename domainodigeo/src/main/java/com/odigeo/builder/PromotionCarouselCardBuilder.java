package com.odigeo.builder;

import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.net.controllers.CarrouselNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.CarouselManagerInterface;
import java.util.List;

public class PromotionCarouselCardBuilder {

  private CarrouselNetControllerInterface mCarrouselNetControllerInterface;
  private CarouselManagerInterface mCarouselManagerInterface;
  private PromotionCardsInterface mPromotionCardsInterface;

  public PromotionCarouselCardBuilder(
      CarrouselNetControllerInterface mCarrouselNetControllerInterface,
      CarouselManagerInterface mCarouselManagerInterface) {
    this.mCarrouselNetControllerInterface = mCarrouselNetControllerInterface;
    this.mCarouselManagerInterface = mCarouselManagerInterface;
  }

  public void setPromotionCardsInterface(PromotionCardsInterface promotionCardsInterface) {
    this.mPromotionCardsInterface = promotionCardsInterface;
  }

  public void buildCards(boolean hasToCleanCache) {
    mCarrouselNetControllerInterface.getCarouselPromotion(new OnRequestDataListener<String>() {
      @Override public void onResponse(String carouselPromotion) {
        List<Card> promotionCards =
            mCarouselManagerInterface.carouselManagerMapper(carouselPromotion);
        mPromotionCardsInterface.onPromotionCardsLoaded(promotionCards);
      }

      @Override public void onError(MslError error, String message) {
        mPromotionCardsInterface.onPromotionCardsError();
      }
    }, hasToCleanCache);
  }

  public interface PromotionCardsInterface {
    void onPromotionCardsLoaded(List<Card> cards);

    void onPromotionCardsError();
  }
}
