package com.odigeo.builder;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class UrlBuilder {

  public static final String ENCODING_UTF_8 = "UTF-8";

  private String path;
  private Map<String, String> params;

  private UrlBuilder(Builder builder) {
    path = builder.path;
    params = builder.params;
  }

  public String getCompleteUrl() throws UnsupportedEncodingException {
    if (params == null || params.size() == 0) {
      return path;
    }

    String url = path + "?";
    Set<Map.Entry<String, String>> entries = params.entrySet();
    for (Map.Entry<String, String> param : entries) {
      url = url + param.getKey() + "=" + URLEncoder.encode(param.getValue(), ENCODING_UTF_8) + "&";
    }

    url = url.substring(0, url.length() - 1);

    return url;
  }

  public String getQueryString() throws UnsupportedEncodingException {
    String queryString = "";
    Set<Map.Entry<String, String>> entries = params.entrySet();
    for (Map.Entry<String, String> param : entries) {
      queryString = queryString
          + param.getKey()
          + "="
          + URLEncoder.encode(param.getValue(), ENCODING_UTF_8)
          + "&";
    }
    return queryString;
  }

  public Map<String, String> getParams() {
    return params;
  }

  public static final class Builder {
    private String path;
    private Map<String, String> params;

    public Builder() {
    }

    public Builder(String completeUrl) throws UnsupportedEncodingException {
      params = new HashMap<>();

      String[] pairs = completeUrl.split("\\?");

      if (pairs.length > 1) {
        path = pairs[0];
        String[] paramsUrl = pairs[1].split("&");
        for (String param : paramsUrl) {
          int index = param.indexOf("=");
          params.put(URLDecoder.decode(param.substring(0, index), ENCODING_UTF_8),
              URLDecoder.decode(param.substring(index + 1), ENCODING_UTF_8));
        }
      } else {
        path = completeUrl;
      }
    }

    public Builder path(String val) {
      path = val;
      return this;
    }

    public Builder params(Map<String, String> val) {
      params = val;
      return this;
    }

    public UrlBuilder build() {
      return new UrlBuilder(this);
    }
  }
}
