package com.odigeo.builder;

import com.odigeo.data.calendar.controllers.CalendarController;
import com.odigeo.data.calendar.controllers.MultiTripCalendarController;
import com.odigeo.data.calendar.controllers.RoundTripCalendarController;
import com.odigeo.data.calendar.controllers.SingleTripCalendarController;
import com.odigeo.data.entity.TravelType;
import com.odigeo.presenter.CalendarPresenter;

public class CalendarControllerBuilder {

  public CalendarControllerBuilder() {
  }

  public CalendarController build(TravelType travelType, CalendarPresenter presenter) {
    CalendarController calendarController = null;

    switch (travelType) {
      case ROUND:
        calendarController = new RoundTripCalendarController(presenter);
        break;

      case SIMPLE:
        calendarController = new SingleTripCalendarController(presenter);
        break;

      case MULTIDESTINATION:
        calendarController = new MultiTripCalendarController(presenter);
        break;
    }

    return calendarController;
  }
}
