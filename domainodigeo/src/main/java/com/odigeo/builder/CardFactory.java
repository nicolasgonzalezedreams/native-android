package com.odigeo.builder;

import com.odigeo.builder.builderInterfaces.CardFactoryInterface;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.carrousel.BaggageBeltCard;
import com.odigeo.data.entity.carrousel.BoardingGateCard;
import com.odigeo.data.entity.carrousel.CancelledSectionCard;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.CarouselLocation;
import com.odigeo.data.entity.carrousel.DelayedSectionCard;
import com.odigeo.data.entity.carrousel.DivertedSectionCard;
import com.odigeo.data.entity.carrousel.SectionCard;

public class CardFactory implements CardFactoryInterface {

  private static int CANCELLED_PRIORITY = 1;
  private static int DIVERTED_PRIORITY = 2;
  private static int DELAY_PRIORITY = 3;
  private static int SECTION_PRIORITY = 3;
  private static int BOARDING_PRIORITY = 4;
  private static int BAGGAGE_PRIORITY = 5;

  @Override public Card builderSectionCardWithFlightStat(Section section, long cardId) {
    return new SectionCard(cardId, Card.CardType.SECTION_CARD, SECTION_PRIORITY, null,
        section.getCarrier().getCode(), section.getCarrier().getName(), section.getSectionId(),
        builderLocationFrom(section), builderLocationTo(section));
  }

  @Override public Card builderSectionCardWithOutFlightStats(Section section, long cardId) {
    return new SectionCard(cardId, Card.CardType.SECTION_CARD, SECTION_PRIORITY, null,
        section.getCarrier().getCode(), section.getCarrier().getName(), section.getSectionId(),
        builderLocationFromWithoutFlightStats(section),
        builderLocationToWithoutFlightStats(section));
  }

  @Override public Card builderBoardingGateCard(Section section, long cardId) {
    return new BoardingGateCard(cardId, Card.CardType.BOARDING_GATE_CARD, BOARDING_PRIORITY, null,
        section.getCarrier().getCode(), section.getCarrier().getName(), section.getSectionId(),
        builderLocationFrom(section), builderLocationTo(section), getDepartureGate(section));
  }

  @Override public Card builderCancelledCard(Section section, long cardId) {
    return new CancelledSectionCard(cardId, Card.CardType.CANCELLED_SECTION_CARD,
        CANCELLED_PRIORITY, null, section.getCarrier().getCode(), section.getCarrier().getName(),
        section.getSectionId(), builderLocationFrom(section), builderLocationTo(section));
  }

  @Override public Card builderDivertidedCard(Section section, long cardId) {
    return new DivertedSectionCard(cardId, Card.CardType.DIVERTED_SECTION_CARD, DIVERTED_PRIORITY,
        null, section.getCarrier().getCode(), section.getCarrier().getName(),
        section.getSectionId(), builderLocationFrom(section), builderLocationTo(section));
  }

  @Override public Card builderDelayCard(Section section, long cardId) {
    return new DelayedSectionCard(cardId, Card.CardType.DELAYED_SECTION_CARD, DELAY_PRIORITY, null,
        section.getCarrier().getCode(), section.getCarrier().getName(), section.getSectionId(),
        builderLocationFrom(section), builderLocationTo(section),
        section.getFlightStats().getDepartureTimeDelay(),
        section.getFlightStats().getArrivalTimeDelay());
  }

  @Override public Card builderBaggageCard(Section section, long cardId) {
    return new BaggageBeltCard(cardId, Card.CardType.BAGGAGE_BELT_CARD, BAGGAGE_PRIORITY, null,
        section.getCarrier().getCode(), section.getCarrier().getName(), section.getSectionId(),
        builderLocationFrom(section), builderLocationTo(section),
        section.getFlightStats().getBaggageClaim());
  }

  @Override public CarouselLocation builderLocationFromWithoutFlightStats(Section section) {
    return new CarouselLocation(section.getFrom().getLocationCode(), section.getDepartureDate(),
        section.getDepartureTerminal(), section.getFrom().getCityName());
  }

  @Override public CarouselLocation builderLocationFrom(Section section) {
    if (section.getFlightStats().getUpdatedDepartureTerminal() != null) {
      return builderLocationFromUpdated(section);
    } else {
      return builderLocationFromNonUpdated(section);
    }
  }

  @Override public CarouselLocation builderLocationFromUpdated(Section section) {
    return new CarouselLocation(section.getFrom().getLocationCode(), section.getDepartureDate(),
        section.getFlightStats().getUpdatedDepartureTerminal(), section.getFrom().getCityName());
  }

  @Override public CarouselLocation builderLocationFromNonUpdated(Section section) {
    return new CarouselLocation(section.getFrom().getLocationCode(), section.getDepartureDate(),
        section.getDepartureTerminal(), section.getFrom().getCityName());
  }

  @Override public CarouselLocation builderLocationTo(Section section) {
    return new CarouselLocation(section.getTo().getLocationCode(), section.getArrivalDate(),
        section.getArrivalTerminal(), section.getTo().getCityName());
  }

  @Override public CarouselLocation builderLocationToWithoutFlightStats(Section section) {
    return new CarouselLocation(section.getTo().getLocationCode(), section.getArrivalDate(),
        section.getArrivalTerminal(), section.getTo().getCityName());
  }

  private String getDepartureGate(Section section) {
    if (section.getFlightStats().getGateChanged() != null) {
      return section.getFlightStats().getGateChanged();
    } else {
      return section.getFlightStats().getDepartureGate();
    }
  }
}
