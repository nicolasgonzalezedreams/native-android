package com.odigeo.builder;

import com.odigeo.builder.builderInterfaces.CarouselCardBuilderInterface;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.SectionCard;
import com.odigeo.data.preferences.CarouselManagerInterface;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.GetNextSegmentByDateInteractor;
import com.odigeo.interactors.MyTripsInteractor;
import java.util.ArrayList;
import java.util.List;

public class BookingCarouselCardBuilder implements CarouselCardBuilderInterface {

  private final MyTripsInteractor myTripsInteractor;
  private final TrackerControllerInterface trackerController;
  private SessionController sessionController;
  private PreferencesControllerInterface preferencesControllerInterface;
  private GetNextSegmentByDateInteractor getNextSegmentByDateInteractor;
  private CarouselManagerInterface carouselManagerInterface;
  private SectionCardBuilder sectionCardBuilder;

  public BookingCarouselCardBuilder(MyTripsInteractor myTripsInteractor,
      GetNextSegmentByDateInteractor getNextSegmentByDateInteractor,
      CarouselManagerInterface carouselManagerInterface, SectionCardBuilder sectionCardBuilder,
      TrackerControllerInterface trackerController,
      SessionController sessionController,
      PreferencesControllerInterface preferencesControllerInterface) {
    this.myTripsInteractor = myTripsInteractor;
    this.getNextSegmentByDateInteractor = getNextSegmentByDateInteractor;
    this.carouselManagerInterface = carouselManagerInterface;
    this.sectionCardBuilder = sectionCardBuilder;
    this.trackerController = trackerController;
    this.sessionController = sessionController;
    this.preferencesControllerInterface = preferencesControllerInterface;
  }

  @Override public List<Card> buildCards() {
    Booking booking = myTripsInteractor.getNextBooking();

    if (booking == null) {
      return new ArrayList<>();
    } else {
      boolean ignoreFlightStats = false;

      if (shouldHideFlightStatus(booking)) {
        ignoreFlightStats = true;
        deleteCarouselLastUpdate();
      }

      if (isSSOUserWithServiceOption(booking)) {
        trackerController.trackSSOUserWithServiceOptions();
      }
      List<Card> bookingCards = buildBookingCards(booking, ignoreFlightStats);
      setFlightStatsVisibility(booking, bookingCards);
      return bookingCards;
    }
  }

  private List<Card> buildBookingCards(Booking booking, boolean ignoreFlightStats) {
    List<Segment> segments = booking.getSegments();
    Segment nearestSegment = getNextSegmentByDateInteractor.getNextValidSegment(segments);
    List<Card> bookingCards = new ArrayList<>();

    if (nearestSegment != null) {
      bookingCards = buildSegments(nearestSegment, ignoreFlightStats);
      setTripToHeaderCarousel(bookingCards, booking, nearestSegment);
      setImageCarousel(bookingCards, booking, nearestSegment);
      return bookingCards;
    }

    return bookingCards;
  }

  private List<Card> buildSegments(Segment segment, boolean ignoreFlightStats) {
    List<Card> sectionCardsBuilt;
    List<Card> segmentCards = new ArrayList<>();
    long cardId = segment.getBookingId();

    for (Section section : segment.getSectionsList()) {
      sectionCardsBuilt = sectionCardBuilder.buildTo(section, ignoreFlightStats, cardId);
      if (sectionCardsBuilt != null) {
        segmentCards.addAll(addBuiltSegments(sectionCardsBuilt));
      }
    }

    return segmentCards;
  }

  private List<Card> addBuiltSegments(List<Card> sectionCardsBuilt) {
    List<Card> segmentCards = new ArrayList<>();
    for (Card sectionCard : sectionCardsBuilt) {
      if (carouselManagerInterface.mustShow(sectionCard.getId(), sectionCard.getType())) {
        segmentCards.add(sectionCard);
      }
    }

    return segmentCards;
  }

  private void setTripToHeaderCarousel(List<Card> bookingCards, Booking booking,
      Segment validSegment) {
    String lastCityHeader;
    if (booking.getTripType().equals(Booking.TRIP_TYPE_ROUND_TRIP)) {
      lastCityHeader = booking.getSegments().get(0).getLastSection().getTo().getCityName();
    } else {
      lastCityHeader = validSegment.getLastSection().getTo().getCityName();
    }
    setTripToHeaderCards(bookingCards, lastCityHeader);
  }

  private void setTripToHeaderCards(List<Card> bookingCards, String lastCityHeader) {
    for (Card card : bookingCards) {
      if (card instanceof SectionCard) {
        ((SectionCard) card).setTripToHeader(lastCityHeader);
      }
    }
  }

  private void setImageCarousel(List<Card> bookingCards, Booking booking, Segment validSegment) {
    if (booking.getTripType().equals(Booking.TRIP_TYPE_ROUND_TRIP)) {
      Segment segment = booking.getSegments().get(0);
      setCarouselImageCards(bookingCards, segment);
    } else {
      setCarouselImageCards(bookingCards, validSegment);
    }
  }

  private void setCarouselImageCards(List<Card> bookingCards, Segment segment) {
    String urlImage = carouselManagerInterface.getCarouselImage(segment);
    if (urlImage != null) {
      for (Card carouselCard : bookingCards) {
        carouselCard.setImage(urlImage);
      }
    }
  }

  private void setFlightStatsVisibility(Booking booking, List<Card> bookingCards) {
    boolean shouldShowFlightStatus = shouldShowFlightStatus(booking);
    for (Card card : bookingCards) {
      if (card instanceof SectionCard) {
        ((SectionCard) card).setHasToShowRefreshButton(shouldShowFlightStatus);
      }
    }
  }

  private void deleteCarouselLastUpdate() {
    preferencesControllerInterface.saveLongValue(
        PreferencesControllerInterface.LAST_UPDATE_BOOKING_STATUS, 0);
  }

  private boolean shouldHideFlightStatus(Booking booking) {
    return ((userIsLogged() && !tripBelongToLoggedUser(booking))
        || (!userIsLogged())
        || booking.getIsSoundOfData()
        || !booking.getIsEnrichedBooking());
  }

  private boolean shouldShowFlightStatus(Booking booking) {
    return !shouldHideFlightStatus(booking);
  }

  private boolean userIsLogged() {
    return (sessionController.getCredentials() != null);
  }

  private boolean tripBelongToLoggedUser(Booking booking) {
    if (booking.getBuyer() != null) {
      return (sessionController.getCredentials()
          .getUser()
          .equals(booking.getBuyer().getEmail()));
    }
    return false;
  }

  private boolean isSSOUserWithServiceOption(Booking booking) {
    return (userIsLogged())
        && (userIsLogged() && tripBelongToLoggedUser(booking))
        && booking.getIsSoundOfData();
  }
}
