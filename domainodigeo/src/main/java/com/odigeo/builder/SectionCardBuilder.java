package com.odigeo.builder;

import com.odigeo.builder.builderInterfaces.CardFactoryInterface;
import com.odigeo.builder.builderInterfaces.SectionCardBuilderInterface;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.tools.DateHelperInterface;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SectionCardBuilder implements SectionCardBuilderInterface<List<Card>, Section> {

  private static String CANCELLED_STATE = "CANCELLED";
  private static String DIVERTED_STATE = "DIVERTED";
  private static String DELAYED_STATE = "DELAYED";
  private static String DEFAULT_BAGGAGE = "defaultBaggageClaim";
  private static int MAX_TIME_TO_SHOW_CARD = 30;
  List<Card> mCards;
  long mCardsId;
  private long mTimeAfterLanding;
  private CardFactoryInterface mCardFactoryInterface;
  private DateHelperInterface mDateHelperInterface;

  public SectionCardBuilder(CardFactoryInterface cardFactoryInterface,
      DateHelperInterface dateHelperInterface) {
    this.mCardFactoryInterface = cardFactoryInterface;
    this.mDateHelperInterface = dateHelperInterface;
  }

  @Override public List<Card> buildTo(Section section, boolean ignoreFlightStat, long cardId) {
    mCards = new ArrayList<>();
    mCardsId = cardId;

    setTimesForBuildingCards(section, ignoreFlightStat);

    if (ignoreFlightStat) {
      buildOnlyNextFlightCard(section);
    } else {
      String flightStatus = getFlightStatus(section);
      checkFlightStatusAndBuildCards(flightStatus, section);
    }
    return mCards;
  }

  private String getFlightStatus(Section section) {
    if (section.getFlightStats() != null) {
      return section.getFlightStats().getFlightStatus();
    }
    return null;
  }

  private void checkFlightStatusAndBuildCards(String flightStatus, Section section) {
    if (flightStatus != null) {
      buildAllCards(section, flightStatus);
    } else {
      buildNextFlightCardWithoutFlightStats(section);
    }
  }

  private void buildOnlyNextFlightCard(Section section) {
    buildNextFlightCardWithoutFlightStats(section);
  }

  private void buildAllCards(Section section, String flightStat) {
    if (flightStat.equals(CANCELLED_STATE)) {
      buildCancelledCard(section);
    } else if (flightStat.equals(DIVERTED_STATE)) {
      buildDivertidedCard(section);
      buildDepartureCard(section);
      buildBaggageCard(section);
    } else if (flightStat.equals(DELAYED_STATE)) {
      buildDelayCard(section);
      buildDepartureCard(section);
      buildBaggageCard(section);
    } else {
      checkAndBuildNextFlightCardWithFlightStats(section);
      buildDepartureCard(section);
      buildBaggageCard(section);
    }
  }

  private void checkAndBuildNextFlightCardWithFlightStats(Section section) {
    if (validNextFlightStatusArguments(section)) {
      buildNextFlightCardWithFlightStats(section);
    } else {
      buildNextFlightCardWithoutFlightStats(section);
    }
  }

  private boolean validNextFlightStatusArguments(Section section) {
    return ((section.getFlightStats().getDepartureTime() != 0) && (section.getFlightStats()
        .getArrivalTime() != 0));
  }

  private void buildNextFlightCardWithFlightStats(Section section) {
    if (correctTimeToShowCard()) {
      mCards.add(mCardFactoryInterface.builderSectionCardWithFlightStat(section, mCardsId));
    }
  }

  private void buildNextFlightCardWithoutFlightStats(Section section) {
    if (correctTimeToShowCard()) {
      mCards.add(mCardFactoryInterface.builderSectionCardWithOutFlightStats(section, mCardsId));
    }
  }

  private void buildCancelledCard(Section section) {
    if (correctTimeToShowCard()) {
      mCards.add(mCardFactoryInterface.builderCancelledCard(section, mCardsId));
    }
  }

  private void buildDivertidedCard(Section section) {
    if (correctTimeToShowCard()) {
      mCards.add(mCardFactoryInterface.builderDivertidedCard(section, mCardsId));
    }
  }

  private void buildDelayCard(Section section) {
    if (correctTimeToShowCard()) {
      mCards.add(mCardFactoryInterface.builderDelayCard(section, mCardsId));
    }
  }

  private void buildDepartureCard(Section section) {
    if ((section.getFlightStats().getDepartureGate() != null) && correctTimeToShowCard()) {
      mCards.add(mCardFactoryInterface.builderBoardingGateCard(section, mCardsId));
    }
  }

  private void buildBaggageCard(Section section) {
    if ((section.getFlightStats().getBaggageClaim() != null) && !(section.getFlightStats()
        .getBaggageClaim()
        .equals(DEFAULT_BAGGAGE)) && correctTimeToShowCard()) {
      mCards.add(mCardFactoryInterface.builderBaggageCard(section, mCardsId));
    }
  }

  private void setTimesForBuildingCards(Section section, boolean ignoreFlightStats) {
    long gmtArrivalMinutes;
    long currentTime;
    long gmtArrivalDate;

    if (haveToConfigureDelayTime(section, ignoreFlightStats)) {
      gmtArrivalDate =
          mDateHelperInterface.millisecondsLeastOffset(section.getFlightStats().getArrivalTime());
      gmtArrivalMinutes = TimeUnit.MILLISECONDS.toMinutes(gmtArrivalDate);
    } else {
      gmtArrivalDate = mDateHelperInterface.millisecondsLeastOffset(section.getArrivalDate());
      gmtArrivalMinutes = TimeUnit.MILLISECONDS.toMinutes(gmtArrivalDate);
    }

    currentTime = TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis());
    mTimeAfterLanding = currentTime - gmtArrivalMinutes;
  }

  private boolean haveToConfigureDelayTime(Section section, boolean ignoreFlightStats) {
    if ((section.getFlightStats() != null)
        && (section.getFlightStats().getArrivalTimeDelay()
        != null)
        && (section.getFlightStats().getArrivalTime() != 0)
        && (!ignoreFlightStats)
        && (section.getFlightStats().getFlightStatus().equals(DELAYED_STATE))) {
      return true;
    }
    return false;
  }

  public boolean correctTimeToShowCard() {
    if (mTimeAfterLanding > MAX_TIME_TO_SHOW_CARD) {
      return false;
    }
    return true;
  }
}