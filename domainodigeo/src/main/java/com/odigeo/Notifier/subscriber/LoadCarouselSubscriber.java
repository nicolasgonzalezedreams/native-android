package com.odigeo.Notifier.subscriber;

import com.odigeo.Notifier.event.Event;
import com.odigeo.Notifier.event.LoadCarouselEvent;

public class LoadCarouselSubscriber implements Subscriber {

  LoadCarouselEvent mLoadCarouselEvent;

  public LoadCarouselSubscriber() {
  }

  @Override public boolean applyEventFilter(Event event) {
    mLoadCarouselEvent = (LoadCarouselEvent) event;
    return mLoadCarouselEvent.getSingleReceiver().equals(LoadCarouselEvent.DEFAULT_RECEIVER);
  }
}
