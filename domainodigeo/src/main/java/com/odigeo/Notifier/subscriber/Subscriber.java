package com.odigeo.Notifier.subscriber;

import com.odigeo.Notifier.event.Event;

public interface Subscriber {
  boolean applyEventFilter(Event event);
}
