package com.odigeo.Notifier;

import com.odigeo.Notifier.event.Event;
import com.odigeo.Notifier.subscription.Subscription;
import java.util.ArrayList;
import java.util.List;

public class EventsNotifierImpl implements EventsNotifier {

  private static EventsNotifierImpl ourInstance = new EventsNotifierImpl();
  private List<Subscription> mSubscriptions = new ArrayList<>();

  private EventsNotifierImpl() {
  }

  public static EventsNotifierImpl getInstance() {
    return ourInstance;
  }

  @Override public void subscribe(Subscription subscription) {
    if (!mSubscriptions.contains(subscription)) {
      mSubscriptions.add(subscription);
    }
  }

  @Override public void unSubscribe(Subscription subscription) {
    mSubscriptions.remove(subscription);
  }

  @Override public void fireEvent(Event event) {
    for (Subscription subscription : mSubscriptions) {
      if ((isEventActive(subscription)) && (havePermissionToSendEvent(subscription, event))) {
        subscription.getSubscriberlistener().onEventReceived(event);
      }
    }
  }

  @Override public boolean isEventActive(Subscription subscription) {
    return mSubscriptions.contains(subscription);
  }

  @Override public boolean havePermissionToSendEvent(Subscription subscription, Event event) {
    return ((subscription.receiveAlwaysEvent()) || (subscription.getSubscriber()
        .applyEventFilter(event)));
  }
}
