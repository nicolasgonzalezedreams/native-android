package com.odigeo.Notifier.subscription;

import com.odigeo.Notifier.subscriber.Subscriber;

public class SubscriptionImpl implements Subscription {

  private SubscriptionListener mSubscriptionListener;
  private Subscriber mSubscriber;

  public SubscriptionImpl(Subscriber subscriber) {
    this.mSubscriber = subscriber;
  }

  @Override public void setSubscriptionListener(SubscriptionListener subscriptionListener) {
    this.mSubscriptionListener = subscriptionListener;
  }

  @Override public Subscriber getSubscriber() {
    return mSubscriber;
  }

  @Override public SubscriptionListener getSubscriberlistener() {
    return mSubscriptionListener;
  }

  @Override public boolean receiveAlwaysEvent() {
    return (mSubscriber == null);
  }
}
