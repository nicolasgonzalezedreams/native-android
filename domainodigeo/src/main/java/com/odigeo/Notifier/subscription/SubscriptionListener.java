package com.odigeo.Notifier.subscription;

import com.odigeo.Notifier.event.Event;

public interface SubscriptionListener {
  void onEventReceived(Event event);
}
