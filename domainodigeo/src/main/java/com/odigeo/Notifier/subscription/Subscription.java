package com.odigeo.Notifier.subscription;

import com.odigeo.Notifier.subscriber.Subscriber;

public interface Subscription {
  boolean receiveAlwaysEvent();

  SubscriptionListener getSubscriberlistener();

  Subscriber getSubscriber();

  void setSubscriptionListener(SubscriptionListener subscriptionListener);
}
