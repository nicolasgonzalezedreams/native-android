package com.odigeo.Notifier.event.type;

import com.odigeo.Notifier.event.Event;
import java.util.List;

public interface MultipleReceiver extends Event {
  List<String> getAllReceivers();

  void addReceiver();
}
