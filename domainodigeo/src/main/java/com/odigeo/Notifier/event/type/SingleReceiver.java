package com.odigeo.Notifier.event.type;

import com.odigeo.Notifier.event.Event;

public interface SingleReceiver extends Event {
  int DEFAULT_RECEIVER_ID = 0;

  String getSingleReceiver();
}
