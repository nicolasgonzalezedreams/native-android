package com.odigeo.Notifier.event;

import com.odigeo.Notifier.event.type.SingleReceiver;
import java.util.ArrayList;
import java.util.List;

public class LoadCarouselEvent implements SingleReceiver {

  public static String DEFAULT_RECEIVER = "HomeView";
  List<String> mReceivers = new ArrayList<>();

  public LoadCarouselEvent() {
    mReceivers.add(DEFAULT_RECEIVER);
  }

  @Override public String getSingleReceiver() {
    return mReceivers.get(DEFAULT_RECEIVER_ID);
  }
}
