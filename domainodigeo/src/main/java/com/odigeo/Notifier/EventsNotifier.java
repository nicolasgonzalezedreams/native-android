package com.odigeo.Notifier;

import com.odigeo.Notifier.event.Event;
import com.odigeo.Notifier.subscription.Subscription;

public interface EventsNotifier {
  void subscribe(Subscription subscription);

  void unSubscribe(Subscription subscription);

  void fireEvent(Event event);

  boolean isEventActive(Subscription subscription);

  boolean havePermissionToSendEvent(Subscription subscription, Event event);
}
