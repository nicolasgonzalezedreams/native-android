package com.odigeo.validations;

import com.odigeo.data.entity.shoppingCart.Itinerary;
import com.odigeo.data.entity.shoppingCart.TravellerRequiredFields;
import com.odigeo.data.entity.shoppingCart.TravellerType;
import com.odigeo.tools.DateHelperInterface;

public class BirthdateValidator {

  private static int DAY = 0;
  private static int MONTH = 1;
  private static int YEAR = 2;

  private DateHelperInterface dateHelper;
  private TravellerRequiredFields passengerInformation;

  public BirthdateValidator(final DateHelperInterface dateHelper, final TravellerRequiredFields
      passengerInformation){
    this.dateHelper = dateHelper;
    this.passengerInformation = passengerInformation;
  }

  public boolean isTravellerTypeTurningIntoAnotherTypeDuringTrip(int birthdate[],
      Itinerary itineraryShoppingCart, TravellerType travellerType, int maxAge) {

    final int currentYear = dateHelper.getCurrentYear();
    final int travellerAge = dateHelper.getCurrentYear() - birthdate[YEAR];
    final long arrivalTripTimeStamp = itineraryShoppingCart.getLastArrivalDate();
    final long travellerBirthdateTimeStamp =
        dateHelper.getTimeStampFromDayMonthYear(currentYear, birthdate[MONTH],
            birthdate[DAY]);

    return isTravellerMaxAge(travellerAge, travellerType, maxAge)
        && travellerBirthdateTimeStamp <= arrivalTripTimeStamp;
  }

  private boolean isTravellerMaxAge(int travellerAge, TravellerType travellerType, int maxAge) {
    return passengerInformation.getTravellerType() == travellerType && travellerAge == maxAge;
  }
}
