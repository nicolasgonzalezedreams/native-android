package com.odigeo.exceptions;

public class GoogleTokenAuthException extends Exception {

  private final static String MESSAGE = "There token is not valid";

  public GoogleTokenAuthException() {
    super(MESSAGE);
  }
}
