package com.odigeo.exceptions;

/**
 * Created by matias.dirusso on 2/3/2017.
 */

public class SocialLoginTimeOutException extends Exception {
  private final static String MESSAGE = "There has been a TimeOut while trying to login";

  public SocialLoginTimeOutException() {
    super(MESSAGE);
  }
}
