package com.odigeo.exceptions;

public class FacebookTokenAuthException extends Exception {

  private final static String MESSAGE = "There token is not valid";

  public FacebookTokenAuthException() {
    super(MESSAGE);
  }
}
