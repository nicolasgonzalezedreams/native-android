package com.odigeo.exceptions;

public class SessionCredentialsAuthException extends Exception {

  private final static String MESSAGE = "Session credentials are null";

  public SessionCredentialsAuthException() {
    super(MESSAGE);
  }
}
