package com.odigeo.configuration;

public enum ABAlias {
  BBU_BBUSTERS534("BBU_BBUSTERS534"), BBU_BBUSTERS430("BBU_BBUSTERS430"),
  TRA_TRIPATT_548("TRA_TRIPATT-548"), BBU_BBUSTERS530("BBU_BBUSTERS530");

  private final String alias;

  ABAlias(String alias) {
    this.alias = alias;
  }

  public String value() {
    return alias;
  }
}
