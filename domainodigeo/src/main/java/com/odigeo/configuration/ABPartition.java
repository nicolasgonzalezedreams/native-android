package com.odigeo.configuration;

public enum ABPartition {
  NONE(0), ONE(1), TWO(2), THREE(3), FOUR(4);

  private int value;

  ABPartition(int value) {
    this.value = value;
  }

  public static ABPartition fromValue(int value) {
    for (ABPartition abPartition : ABPartition.values()) {
      if (abPartition.value == value) {
        return abPartition;
      }
    }
    return NONE;
  }

  public int value() {
    return value;
  }
}
