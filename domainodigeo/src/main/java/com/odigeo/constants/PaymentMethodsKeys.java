package com.odigeo.constants;

public class PaymentMethodsKeys {

  public static final String VI_LOCAL = "VI_LOCAL";
  public static final String MD_LOCAL = "MD_LOCAL";
  public static final String AX_LOCAL = "AX_LOCAL";
  public static final String JC_LOCAL = "JC_LOCAL";
  public static final String DC_LOCAL = "DC_LOCAL";

  public static final String VI = "VI";
  public static final String VD = "VD";
  public static final String VE = "VE";
  public static final String E1 = "E1";

  public static final String MA = "MA";
  public static final String CA = "CA";
  public static final String MD = "MD";
  public static final String MP = "MP";

  public static final String EV = "EV";
  public static final String ME = "ME";

  public static final String VB = "VB";
  public static final String SOLO = "SOLO";
  public static final String DU = "DU";
  public static final String DL = "DL";
  public static final String DS = "DS";
  public static final String CB = "CB";
  public static final String VV = "VV";
  public static final String AX = "AX";
  public static final String DC = "DC";
  public static final String JC = "JC";
  public static final String COF = "COF";

  public static final String UNKNOWN = "??";
}