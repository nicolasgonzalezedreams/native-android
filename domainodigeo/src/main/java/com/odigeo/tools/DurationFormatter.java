package com.odigeo.tools;

public interface DurationFormatter {

  String format(long minutes);

  String format(long hour, long minutes);
}
