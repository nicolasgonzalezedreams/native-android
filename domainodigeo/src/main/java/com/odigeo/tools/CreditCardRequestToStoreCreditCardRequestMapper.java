package com.odigeo.tools;

import com.odigeo.data.entity.shoppingCart.request.CreditCardCollectionDetailsParametersRequest;
import com.odigeo.data.entity.userData.InsertCreditCardRequest;

public class CreditCardRequestToStoreCreditCardRequestMapper
    implements Mapper<InsertCreditCardRequest, CreditCardCollectionDetailsParametersRequest> {

  @Override public InsertCreditCardRequest buildTo(
      CreditCardCollectionDetailsParametersRequest creditCardRequest) {
    return new InsertCreditCardRequest(creditCardRequest.getCardNumber(),
        creditCardRequest.getCardExpirationMonth(), creditCardRequest.getCardExpirationYear(),
        creditCardRequest.getCardOwner(), 0, 0, 0, false, creditCardRequest.getCardTypeCode());
  }
}
