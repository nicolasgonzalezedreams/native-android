package com.odigeo.tools;

import java.util.List;

public interface DateHelperInterface {

  String millisecondsToDate(long dateInMilliseconds, String dateFormat);

  String millisecondsToDateGMT(long dateInMilliseconds, String dateFormat);

  long millisecondsLeastOffset(long date);

  long minutesToMiliseconds(long date);

  long getTimeStampFromDayMonthYear(int year, int month, int day);

  List<Integer> getDayMonthYearFromTimeStamp(long date);

  int getDay(long date);

  int getMonth(long date);

  int getYear(long date);

  int getHour(long date);

  int getMinute(long date);

  int getAge(long birthdate);

  int getCurrentYearLastTwoCharacters();

  int getCurrentYear();

  int getCurrentMonth();

  long dateToMiliseconds(String date, String dateFormat);

  long getCurrentSystemMillis();
}
