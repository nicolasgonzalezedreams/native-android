package com.odigeo.tools;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class DateUtils {

  public static final String DATE_TIME_FORMAT = "MM/dd/yyyy HH:mm";

  /**
   * Eliminate the time part to a Date object
   *
   * @param date The date to reset its time
   * @return Return the Date object, without time, only the date part
   */
  public static Date resetTimeToDate(Date date) {
    final Calendar dateWithoutTime = Calendar.getInstance();

    dateWithoutTime.setTime(date);
    dateWithoutTime.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
    dateWithoutTime.set(Calendar.MINUTE, 0);                 // set minute in hour
    dateWithoutTime.set(Calendar.SECOND, 0);                 // set second in minute
    dateWithoutTime.set(Calendar.MILLISECOND, 0);            // set millis in second

    return dateWithoutTime.getTime();
  }

  /**
   * Execute a compare dates, but ignoring the time
   *
   * @return 0 if the argument Date1 is equal to Date2; a value less than 0 if this Date1 is before
   * the Date2; and a value greater than 0
   * if Date1 is after the Date2.
   */
  public static int compareDatesWithoutTime(Date date1, Date date2) {
    Date dateWithoutTime1 = resetTimeToDate(date1);
    Date dateWithoutTime2 = resetTimeToDate(date2);

    return dateWithoutTime1.compareTo(dateWithoutTime2);
  }

  public static List<Date> convertToLocalTime(List<Date> listInGmt) {
    List<Date> listInLocal = new ArrayList<>();

    for (Date gmtTime : listInGmt) {
      if (gmtTime != null) {
        listInLocal.add(convertToLocalTime(gmtTime));
      }
    }

    return listInLocal;
  }

  public static Date convertToLocalTime(Date dateTimeInGmt) {

    Calendar calendarInGmt = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    calendarInGmt.setTimeInMillis(dateTimeInGmt.getTime());

    Calendar calendarInLocal = Calendar.getInstance();
    calendarInLocal.set(calendarInGmt.get(Calendar.YEAR), calendarInGmt.get(Calendar.MONTH),
        calendarInGmt.get(Calendar.DATE), 0, 0, 0);

    return calendarInLocal.getTime();
  }

  public static Date convertToGmtTime(Date dateTimeInLocal) {

    Calendar calendarInLocal = Calendar.getInstance();
    calendarInLocal.setTimeInMillis(dateTimeInLocal.getTime());

    Calendar calendarInGmt = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    calendarInGmt.set(calendarInLocal.get(Calendar.YEAR), calendarInLocal.get(Calendar.MONTH),
        calendarInLocal.get(Calendar.DATE), 0, 0, 0);

    return calendarInGmt.getTime();
  }

  public static List<Date> convertToDate(String dates) {
    List<Date> dateList = new ArrayList<>();

    if (dates != null) {
      String[] splittedDates = dates.split(",");

      for (String date : splittedDates) {
        dateList.add(convertStringToDate(date));
      }
    }

    return dateList;
  }

  public static Date convertStringToDate(String date) {
    try {
      DateFormat formatter = new SimpleDateFormat("dd/MM/yy");
      return formatter.parse(date);
    } catch (ParseException ex) {
      ex.printStackTrace();
    }

    return new Date();
  }

  public static boolean isWeekend(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);

    int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

    return dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY;
  }

  public static String getDateFormatWithDiff(String format, long timeInMilis, int calendarField,
      int diff) {

    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(timeInMilis);
    calendar.add(calendarField, diff);
    Date date = calendar.getTime();
    DateFormat dateFormat = new SimpleDateFormat(format);
    dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
    return dateFormat.format(date);
  }

  public static boolean areSameDay(Date first, Date last) {
    if (first != null && last != null) {
      Calendar calendarFirst = Calendar.getInstance();
      calendarFirst.setTime(first);

      Calendar calendarLast = Calendar.getInstance();
      calendarLast.setTime(last);

      return calendarFirst.get(Calendar.YEAR) == calendarLast.get(Calendar.YEAR)
          && calendarFirst.get(Calendar.DAY_OF_YEAR) == calendarLast.get(Calendar.DAY_OF_YEAR);
    }

    return false;
  }

  public static boolean isBefore(Date first, Date last) {
    if (first != null && last != null) {
      Calendar calendarFirst = Calendar.getInstance();
      calendarFirst.setTime(first);

      Calendar calendarLast = Calendar.getInstance();
      calendarLast.setTime(last);

      return calendarFirst.get(Calendar.YEAR) < calendarLast.get(Calendar.YEAR)
          || (calendarFirst.get(Calendar.YEAR) == calendarLast.get(Calendar.YEAR)
          && calendarFirst.get(Calendar.DAY_OF_YEAR) < calendarLast.get(Calendar.DAY_OF_YEAR));
    }

    return false;
  }

  public static boolean isAfter(Date first, Date last) {
    if (first != null && last != null) {
      Calendar calendarFirst = Calendar.getInstance();
      calendarFirst.setTime(first);

      Calendar calendarLast = Calendar.getInstance();
      calendarLast.setTime(last);

      return calendarFirst.get(Calendar.YEAR) > calendarLast.get(Calendar.YEAR)
          || (calendarFirst.get(Calendar.YEAR) == calendarLast.get(Calendar.YEAR)
          && calendarFirst.get(Calendar.DAY_OF_YEAR) > calendarLast.get(Calendar.DAY_OF_YEAR));
    }

    return false;
  }
}
