package com.odigeo.tools;

public class FieldTool {

  private RequiredItemType mRequiredItemType;

  public FieldTool(RequiredItemType requiredItemType) {
    mRequiredItemType = requiredItemType;
  }

  public RequiredItemType getmRequiredItemType() {
    return mRequiredItemType;
  }

  public enum RequiredItemType {
    MANDATORY, OPTIONAL, NOT_REQUIRED;
  }
}
