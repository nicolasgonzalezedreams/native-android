package com.odigeo.tools;

public interface Mapper<TFrom, TTo> {
  TFrom buildTo(TTo model);
}

