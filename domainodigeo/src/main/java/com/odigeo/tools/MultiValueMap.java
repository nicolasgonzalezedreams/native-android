package com.odigeo.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 5/05/16
 */
public class MultiValueMap<K, V> {
  private final Map<K, List<V>> mMap;

  public MultiValueMap() {
    mMap = new HashMap<>();
  }

  public MultiValueMap(int initialCapacity) {
    mMap = new HashMap<>(initialCapacity);
  }

  public int size() {
    return mMap.size();
  }

  public Set<K> keySet() {
    return mMap.keySet();
  }

  public void clear() {
    mMap.clear();
  }

  public Set<Map.Entry<K, List<V>>> entrySet() {
    return mMap.entrySet();
  }

  public Collection<List<V>> values() {
    return mMap.values();
  }

  public boolean isEmpty() {
    return mMap.isEmpty();
  }

  public List<V> put(K key, V value) {
    List<V> values = mMap.get(key);
    if (values == null) {
      values = new ArrayList<>();
    }
    if (!values.contains(value)) {
      values.add(value);
    }
    return mMap.put(key, values);
  }

  public List<V> get(K key) {
    return mMap.get(key);
  }

  public List<V> remove(K key) {
    return mMap.remove(key);
  }
}
