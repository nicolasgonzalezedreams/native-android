package com.odigeo.tools;

import com.odigeo.data.entity.shoppingCart.TravellerInformationField;
import com.odigeo.data.entity.shoppingCart.TravellerInformationFieldConditionRule;
import java.util.List;

public class ConditionRulesHelper {

  List<TravellerInformationFieldConditionRule> mTravellerInformationFieldConditionRuleList;

  public void setTravellerInformationFieldConditionRuleList(
      List<TravellerInformationFieldConditionRule> travellerInformationFieldConditionRuleList) {
    mTravellerInformationFieldConditionRuleList = travellerInformationFieldConditionRuleList;
  }

  public boolean isTitleMandatory() {
    return existInConditionRules(TravellerInformationField.TITLE);
  }

  public boolean isNameMandatory() {
    return existInConditionRules(TravellerInformationField.NAME);
  }

  public boolean isMiddleNameMandatory() {
    return existInConditionRules(TravellerInformationField.MIDDLE_NAME);
  }

  public boolean isFirstLastNameMandatory() {
    return existInConditionRules(TravellerInformationField.FIRST_LAST_NAME);
  }

  public boolean isSecondLastNameMandatory() {
    return existInConditionRules(TravellerInformationField.SECOND_LAST_NAME);
  }

  public boolean isBirthDateMandatory() {
    return existInConditionRules(TravellerInformationField.BIRTH_DATE);
  }

  public boolean isAgeMandatory() {
    return existInConditionRules(TravellerInformationField.AGE);
  }

  public boolean isGenderMandatory() {
    return existInConditionRules(TravellerInformationField.GENDER);
  }

  public boolean isCountryResidentMandatory() {
    return existInConditionRules(TravellerInformationField.COUNTRY_OF_RESIDENCE);
  }

  public boolean isNationalityMandatory() {
    return existInConditionRules(TravellerInformationField.NATIONALITY);
  }

  public boolean isIdentificationMandatory() {
    return existInConditionRules(TravellerInformationField.IDENTIFICATION);
  }

  public boolean isIdentificationTypeMandatory() {
    return existInConditionRules(TravellerInformationField.IDENTIFICATION_TYPE);
  }

  public boolean isIdentificationExpirationDateMandatory() {
    return existInConditionRules(TravellerInformationField.IDENTIFICATION_EXPIRATION_DATE);
  }

  public boolean isIdentificationCountryMandatory() {
    return existInConditionRules(TravellerInformationField.IDENTIFICATION_COUNTRY);
  }

  public boolean isResidentMandatory() {
    return existInConditionRules(TravellerInformationField.RESIDENT);
  }

  public boolean isPhoneNumberMandatory() {
    return existInConditionRules(TravellerInformationField.PHONE_NUMBER);
  }

  public boolean isMobilePhoneNumberMandatory() {
    return existInConditionRules(TravellerInformationField.MOBILE_NUMBER);
  }

  public boolean isFrequentFlyerCardAirlineCodeMandatory() {
    return existInConditionRules(TravellerInformationField.FREQUENT_FLYER_CARD_AIRLINE_CODE);
  }

  public boolean isFrequentFlyerCardNumberMandatory() {
    return existInConditionRules(TravellerInformationField.FREQUENT_FLYER_CARD_NUMBER);
  }

  private boolean existInConditionRules(TravellerInformationField travellerInformationField) {

    if (travellerInformationField != null) {
      for (int i = 0; i < mTravellerInformationFieldConditionRuleList.size(); i++) {
        if (mTravellerInformationFieldConditionRuleList.get(i)
            .getConditionallyRequiredTravellerInformationField() == travellerInformationField) {
          return true;
        }
      }
    }
    return false;
  }
}
