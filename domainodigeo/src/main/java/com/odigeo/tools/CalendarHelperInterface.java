package com.odigeo.tools;

import com.odigeo.data.entity.booking.Booking;

/**
 * Created by matia on 3/2/2017.
 */

public interface CalendarHelperInterface {

  void addBookingToCalendar(Booking booking);
}
