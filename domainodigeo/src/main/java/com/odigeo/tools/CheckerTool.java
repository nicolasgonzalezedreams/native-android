package com.odigeo.tools;

import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Deprecated public class CheckerTool {

  // Types of fields
  public static final int TYPE_EMAIL = 1;
  public static final int TYPE_NAME = 2;
  public static final int TYPE_ADDRESS = 3;
  public static final int TYPE_PHONE = 4;
  public static final int TYPE_CITY = 5;
  public static final int TYPE_ZIP_CODE = 6;
  public static final int TYPE_COUNTRY = 7;
  public static final int TYPE_PHONE_CODE = 8;
  public static final int TYPE_NIF = 9;
  public static final int TYPE_NIE = 10;
  public static final int TYPE_PASSPORT = 11;
  public static final int TYPE_DATE = 12;
  public static final int TYPE_CPF = 13;
  public static final int TYPE_CIF = 14;
  private static final int MIN_CHARACTERS_FIELD = 2;
  private static final int MIN_ZIP_CODE_CHARACTERS_FIELD = 1;
  private static final int MIN_CHARACTERS_EMAIL = 6;
  private static final int MIN_PASSWORD_LENGTH = 7;
  private static final int MAX_CHARACTERS_PASSPORT = 15;
  private static final int MAX_CHARACTERS_NAME = 30;
  private static final int MAX_ZIP_CODE_CHARACTERS_FIELD = 10;
  private static final int MAX_CHARACTERS_EMAIL = 64;
  private static final int MAX_PASSWORD_LENGTH = 100;
  //Spanish documents
  private static final int ID_LETTER = 8;
  private static final int MAX_CHARACTERS_ID = 9;
  private static final int MIN_CHARACTERS_NIE_DOCUMENT = 8;
  private static final int MAX_CHARACTERS_NIE_DOCUMENT = 9;
  private static final int CIF_VALIDATION_DIGIT = 10;
  private static final int MAX_CHARACTERS_CIF = 9;
  private static final int LAST_VALUE_CIF = 8;

  //Brazil document
  private static final int CPF_LENGTH = 11;

  private static final String REGEX = "[%s]+";
  private static final String NAME_ALLOWED_CHARS =
      "A-Za-zÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜäëïöüşŞıçÇ'ŒœßØøÅåÆæÞþÐ ";
  private static final String ADDRESS_ALLOWED_CHARS = NAME_ALLOWED_CHARS + "0-9.,_";
  private static final String EMAIL_ALLOWED_CHARS =
      "[A-Z0-9a-z_%+-]+(\\.[A-Z0-9a-z_%+-]+)*@[A-Za-z0-9]+([\\-]*[A-Za-z0-9])*(\\"
          + ".[A-Za-z0-9]+([\\-]*[A-Za-z0-9])*)*\\.[A-Za-z]{2,}";
  private static final String PHONE_ALLOWED_CHARS = "[0-9]{7,15}";
  private static final String CHARACTERS_NOT_ALLOWED = "~!@#$%^&*+=`|(){}[]:;\"<>?";
  private static final String PASSWORD_EXPRESSION =
      "((.*[0-9]+.*[aA-zZ]+.*)|().*[aA-zZ]+.*[0-9]+.*)";
  private static final String PASSWORD_EXPRESSION_LOGIN = "((.*[aA-zZ]+.*)|().*[aA-zZ]+.*)";
  private static final String BOOKING_ID_EXPRESSION = "[A-Z0-9].{2,}";
  private static final String PASSPORT_EXPRESSION = "[aA-zZ0-9]+";
  private static final String DNI_EXPRESSION = "[0-9]+";
  private static final String DNI_ALLOWED_CHARS = "TRWAGMYFPDXBNJZSQVHLCKE";
  private static final String CPF_EXPRESSION = "[0-9]+(\\.[0-9]+)?";
  private static final String LETTER_EXPRESSION = "[a-zA-Z]+";
  private static final String REGEX_NATIONAL_ID_CARD = "[0-9A-Za-z]{1,20}";

  public static boolean isEmptyField(String value) {
    if (value != null) {
      value = value.trim();
      return (value.isEmpty());
    }
    return true;
  }

  public static boolean checkEmail(String value) {
    return validateLatinCharset(value) && validateLengthField(value, MIN_CHARACTERS_EMAIL,
        MAX_CHARACTERS_EMAIL) && value.matches(EMAIL_ALLOWED_CHARS);
  }

  public static boolean checkPassword(String password) {
    return validateLengthField(password, MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH)
        && password.matches(PASSWORD_EXPRESSION);
  }

  public static boolean checkPasswordOnLogin(String password) {
    return validateLengthField(password, MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH)
        && password.matches(PASSWORD_EXPRESSION_LOGIN);
  }

  public static boolean checkBookingId(String bookingId) {
    boolean isValid = false;

    Pattern pattern = Pattern.compile(BOOKING_ID_EXPRESSION, Pattern.CASE_INSENSITIVE);
    Matcher matcher = pattern.matcher(bookingId);

    if (matcher.matches()) {
      isValid = true;
    }

    return isValid;
  }

  public static boolean checkTravellerNameCharacters(String name, int minCharacters,
      int maxCharacters) {
    return validateLengthField(name, minCharacters, maxCharacters) && name != null && name.matches(
        String.format(REGEX, NAME_ALLOWED_CHARS));
  }

  public static boolean checkTravellerAddressCharacters(String address, int minCharacters,
      int maxCharacters) {
    return validateLengthField(address, minCharacters, maxCharacters)
        && address != null
        && address.matches(String.format(REGEX, ADDRESS_ALLOWED_CHARS));
  }

  public static boolean checkTravellerPhoneNumberNumbers(String phoneNumber) {
    return !(phoneNumber == null || phoneNumber.trim().equals("")) && phoneNumber.matches(
        PHONE_ALLOWED_CHARS);
  }

  private static boolean checkTravellerPassportCharacters(String passport) {
    Pattern pattern = Pattern.compile(PASSPORT_EXPRESSION);
    Matcher matcher = pattern.matcher(passport);
    return matcher.matches();
  }

  public static boolean validateLengthField(String value, int minCharacters, int maxCharacters) {
    if (value == null) {
      return minCharacters == 0;
    }

    return value.length() >= minCharacters && value.length() <= maxCharacters;
  }

  public static boolean validateLengthField(String value, int minCharacters) {
    if (value == null) {
      return minCharacters == 0;
    }

    return value.length() >= minCharacters;
  }

  public static boolean checkTravellerName(String name) {
    return checkTravellerNameCharacters(name, MIN_CHARACTERS_FIELD, MAX_CHARACTERS_NAME);
  }

  public static boolean checkCountry(String name) {
    return name != null && validateLatinCharset(name) && validateLengthField(name,
        MIN_CHARACTERS_FIELD);
  }

  public static boolean checkTravellerAddress(String address) {
    return validateLatinCharset(address) && checkTravellerAddressCharacters(address,
        MIN_CHARACTERS_FIELD, MAX_CHARACTERS_NAME);
  }

  public static boolean checkTravellerZipCode(String name) {
    return checkTravellerAddressCharacters(name, MIN_ZIP_CODE_CHARACTERS_FIELD,
        MAX_ZIP_CODE_CHARACTERS_FIELD);
  }

  public static boolean checkTravellerIdCard(String idcard) {
    return !isEmptyField(idcard);
  }

  public static boolean checkTravellerPassport(String passport) {
    return validateLengthField(passport, 1, MAX_CHARACTERS_PASSPORT)
        && checkTravellerPassportCharacters(passport);
  }

  public static boolean checkTravellerDNI(String dni) {
    StringBuilder number = new StringBuilder();
    String defaultValue = "0";
    number.append(dni);
    boolean isValid;
    if (number.length() < 2) {
      return false;
    }
    while (number.length() < MAX_CHARACTERS_ID) {
            /* For Android its recommended use String and not StringBuffer, because Android perform
             * a optimization in build time.
             */
      number.append(defaultValue);
      number.append(number);
    }

    String idNumber = number.substring(0, ID_LETTER);
    String idLetter = number.substring(ID_LETTER);
    idLetter = idLetter.toUpperCase();
    if (!idNumber.matches(DNI_EXPRESSION)) {
      return false;
    }
    int mod = Integer.parseInt(idNumber) % DNI_ALLOWED_CHARS.length();

    String com = String.valueOf(DNI_ALLOWED_CHARS.charAt(mod));
    isValid = idLetter.equals(com);

    return isValid;
  }

  /**
   * Perform all the validation of CIF
   *
   * @param value Value tobe checked
   * @return If its ok
   */
  private static boolean checkCIF(String value) {
    if (isEmptyField(value)) {
      return false;
    } else if (value.length() < MAX_CHARACTERS_CIF) {
      return false;
    }
    return checkFirstAndLastValue(value);
  }

  /**
   * Check if the first ans last value its ok in the CIF
   *
   * @param value Value tobe checked
   * @return If its ok
   */
  private static boolean checkFirstAndLastValue(String value) {
    List<String> lastValueReferencesLetter = Arrays.asList("K", "Q", "S");
    List<String> lastValueReferencesNumber = Arrays.asList("A", "B", "E", "H");
    List<String> characters =
        new ArrayList(Arrays.asList(value.toUpperCase(Locale.getDefault()).split("(?<!^)")));
    List<String> references =
        Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q",
            "R", "S", "V", "W");
    String firstValue = characters.get(0);
    String lastValue = characters.get(LAST_VALUE_CIF);
    boolean containsMustBeLetter = lastValueReferencesLetter.contains(firstValue);
    boolean containsMustBeNumber = lastValueReferencesNumber.contains(firstValue);
    if (containsMustBeLetter || containsMustBeNumber) {
      if (containsMustBeLetter && !isLetter(lastValue)) {
        return false;
      }
      if (containsMustBeNumber && !isDigit(lastValue)) {
        return false;
      }
    }
    return isLetter(firstValue) && references.contains(firstValue);
  }

  public static boolean checkTravellerCPFCharacters(String cpf) {
    Pattern pattern = Pattern.compile(CPF_EXPRESSION);
    Matcher matcher = pattern.matcher(cpf);
    return validateLengthField(cpf, CPF_LENGTH, CPF_LENGTH) && matcher.matches();
  }

  public static boolean checkTravellerIDCardCharacters(String idCard) {
    return checkTravellerIdCard(idCard);
  }

  public static boolean checkTravellerDNICharacters(String dni) {
    return dni != null
        && dni.replace(" ", "").length() > 0
        && validateLatinCharset(dni)
        && dni.matches(REGEX_NATIONAL_ID_CARD);
  }

  public static boolean checkTravellerNIECharacters(String nie) {
    return nie.length() < MIN_CHARACTERS_NIE_DOCUMENT || validateNIE(nie);
  }

  public static boolean validateNIE(String stringNieDocument) {
    StringBuilder nieDocument = new StringBuilder();
    String defaultValue = "0";
    nieDocument.append(stringNieDocument);
    boolean response = false;
    if (nieDocument.length() < MIN_CHARACTERS_NIE_DOCUMENT
        || nieDocument.length() > MAX_CHARACTERS_NIE_DOCUMENT) {
      return false;
    }
    while (nieDocument.length() <= MAX_CHARACTERS_NIE_DOCUMENT) {
            /* For Android its recommended use String and not StringBuffer, because Android perform
             * a optimization in build time.
             */
      nieDocument.append(defaultValue);
      nieDocument.append(nieDocument);
    }

    //Get the idLetter
    String idLetter = nieDocument.substring(nieDocument.length() - 1);
    String xyz = String.valueOf(nieDocument.charAt(0)).toUpperCase();

    if ("0".equals(xyz)) {
      xyz = String.valueOf(nieDocument.charAt(1)).toUpperCase();
    }

    //Get idNumber
    String idNumber = nieDocument.substring(0, MAX_CHARACTERS_NIE_DOCUMENT);

    if ("XYZ".contains(xyz)) {
      idNumber = idNumber.replaceAll("X", "0");
      idNumber = idNumber.replaceAll("Y", "1");
      idNumber = idNumber.replaceAll("Z", "2");

      int mod = Integer.parseInt(idNumber) % DNI_ALLOWED_CHARS.length();
      String com = String.valueOf(DNI_ALLOWED_CHARS.charAt(mod)).toUpperCase();
      response = idLetter.equals(com);
    }

    return response;
  }

  public static boolean validateCIF(String value) {
    if (checkCIF(value)) {
      int finalDigit = getFinalDigit(value);
      return ((CIF_VALIDATION_DIGIT - finalDigit) == 1);
    } else {
      return false;
    }
  }

  /**
   * Perform the operations necessary to get the finalDigit
   *
   * @param value Input value with what by performed the operations
   * @return The final digit resulting of all the operations
   */
  private static int getFinalDigit(String value) {
    char[] subStringToValidate = value.substring(1, LAST_VALUE_CIF).toCharArray();
    int pairSum = 0;
    int oddSum = 0;
    for (int i = 0; i < subStringToValidate.length; i++) {
      boolean isPairPos = ((i + 1) % 2) == 0;
      if (isPairPos) {
        pairSum += Character.getNumericValue(subStringToValidate[i]);
      }
      if (!isPairPos) {
        char[] multi =
            String.valueOf((Character.getNumericValue(subStringToValidate[i]) * 2)).toCharArray();
        int interSum = 0;
        for (char valueChar : multi) {
          interSum += Character.getNumericValue(valueChar);
        }
        oddSum += interSum;
      }
    }
    char[] finalSum = String.valueOf((pairSum + oddSum)).toCharArray();
    return Character.getNumericValue(finalSum[finalSum.length - 1]);
  }

  public static boolean isLetter(String value) {
    return value.matches(LETTER_EXPRESSION);
  }

  public static boolean isDigit(String value) {
    char character = value.charAt(0);
    return (character >= '0' && character <= '9');
  }

  public static boolean validateLatinCharset(String value) {
    String[] codingAccepted = { "ISO_8859_1", "ISO_8859_2", "ISO_8859_9", "ISO_8859_15" };
    boolean isAccepted = false;
    for (String charset : codingAccepted) {
      try {
        if (Charset.forName(charset).newEncoder().canEncode(value)) {
          isAccepted = true;
        }
      } catch (UnsupportedCharsetException e) {
        //Do nothing
      }
    }
    return isAccepted;
  }
}