package com.odigeo;

import com.odigeo.Notifier.EventsNotifierImpl;
import com.odigeo.Notifier.event.LoadCarouselEvent;
import com.odigeo.Notifier.subscriber.LoadCarouselSubscriber;
import com.odigeo.Notifier.subscription.SubscriptionImpl;
import com.odigeo.builder.BookingCarouselCardBuilder;
import com.odigeo.builder.CardFactory;
import com.odigeo.builder.DropOffCarouselCardBuilder;
import com.odigeo.builder.PromotionCarouselCardBuilder;
import com.odigeo.builder.SectionCardBuilder;
import com.odigeo.data.ab.RemoteConfigControllerInterface;
import com.odigeo.data.db.dao.BaggageDBDAOInterface;
import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.db.dao.BuyerDBDAOInterface;
import com.odigeo.data.db.dao.CarrierDBDAOInterface;
import com.odigeo.data.db.dao.CityDBDAOInterface;
import com.odigeo.data.db.dao.FlightStatsDBDAOInterface;
import com.odigeo.data.db.dao.GuideDBDAOInterface;
import com.odigeo.data.db.dao.InsuranceDBDAOInterface;
import com.odigeo.data.db.dao.ItineraryBookingDBDAOInterface;
import com.odigeo.data.db.dao.LocationBookingDBDAOInterface;
import com.odigeo.data.db.dao.MembershipDBDAOInterface;
import com.odigeo.data.db.dao.SearchSegmentDBDAOInterface;
import com.odigeo.data.db.dao.SectionDBDAOInterface;
import com.odigeo.data.db.dao.SegmentDBDAOInterface;
import com.odigeo.data.db.dao.StoredSearchDBDAOInterface;
import com.odigeo.data.db.dao.TravellerDBDAOInterface;
import com.odigeo.data.db.dao.UserAddressDBDAOInterface;
import com.odigeo.data.db.dao.UserDBDAOInterface;
import com.odigeo.data.db.dao.UserFrequentFlyerDBDAOInterface;
import com.odigeo.data.db.dao.UserIdentificationDBDAOInterface;
import com.odigeo.data.db.dao.UserProfileDBDAOInterface;
import com.odigeo.data.db.dao.UserTravellerDBDAOInterface;
import com.odigeo.data.db.helper.BookingsHandlerInterface;
import com.odigeo.data.db.helper.CitiesHandlerInterface;
import com.odigeo.data.db.helper.CountriesDbHelperInterface;
import com.odigeo.data.db.helper.MembershipHandlerInterface;
import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.db.helper.TravellersHandlerInterface;
import com.odigeo.data.db.helper.UserCreateOrUpdateHandlerInterface;
import com.odigeo.data.download.DownloadController;
import com.odigeo.data.entity.parser.TravellerRequestToUserTravellerParser;
import com.odigeo.data.entity.parser.UpdateUserTravellerWithTravellerRequestAndBuyerRequest;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.data.localizable.OneCMSAlarmsController;
import com.odigeo.data.net.NetToolInterface;
import com.odigeo.data.net.controllers.AddPassengerNetControllerInterface;
import com.odigeo.data.net.controllers.AddProductsNetControllerInterface;
import com.odigeo.data.net.controllers.ArrivalGuideNetControllerInterface;
import com.odigeo.data.net.controllers.AutoCompleteNetControllerInterface;
import com.odigeo.data.net.controllers.BinCheckNetControllerInterface;
import com.odigeo.data.net.controllers.BookingNetControllerInterface;
import com.odigeo.data.net.controllers.CarrouselNetControllerInterface;
import com.odigeo.data.net.controllers.ConfirmBookingNetControllerInterface;
import com.odigeo.data.net.controllers.CountryNetControllerInterface;
import com.odigeo.data.net.controllers.LocalizablesNetController;
import com.odigeo.data.net.controllers.NotificationNetControllerInterface;
import com.odigeo.data.net.controllers.RemoveProductsNetControllerInterface;
import com.odigeo.data.net.controllers.ResumeBookingNetController;
import com.odigeo.data.net.controllers.SavePaymentMethodNetController;
import com.odigeo.data.net.controllers.SearchesNetControllerInterface;
import com.odigeo.data.net.controllers.SendMailNetControllerInterface;
import com.odigeo.data.net.controllers.ShoppingCartNetControllerInterface;
import com.odigeo.data.net.controllers.UserNetControllerInterface;
import com.odigeo.data.net.controllers.VisitUserNetController;
import com.odigeo.data.net.controllers.VisitsNetController;
import com.odigeo.data.net.helper.AddPassengerNetJSONBuilderInterface;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.preferences.CarouselManagerInterface;
import com.odigeo.data.preferences.PackageController;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.repositories.CrossSellingRepository;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.data.tracker.CustomDimensionFormatter;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.data.tracker.TuneTrackerInterface;
import com.odigeo.data.trustdefender.TrustDefenderController;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.helper.ABTestHelper;
import com.odigeo.helper.CrossCarUrlHandlerInterface;
import com.odigeo.helper.IdentificationHelper;
import com.odigeo.helper.MixBuyerAndTravellerHelper;
import com.odigeo.interactors.AddGuideInformationInteractor;
import com.odigeo.interactors.AddPassengerToShoppingCartInteractor;
import com.odigeo.interactors.AddProductsToShoppingCartInteractor;
import com.odigeo.interactors.ArrivalGuidesCardsInteractor;
import com.odigeo.interactors.AutoCompleteInteractor;
import com.odigeo.interactors.BookingInteractor;
import com.odigeo.interactors.ChangePasswordInteractor;
import com.odigeo.interactors.CheckBinFromLocalInteractor;
import com.odigeo.interactors.CheckBinFromNetworkInteractor;
import com.odigeo.interactors.CheckUserBirthdayInteractor;
import com.odigeo.interactors.CheckUserCredentialsInteractor;
import com.odigeo.interactors.ConfirmBookingInteractor;
import com.odigeo.interactors.CountriesInteractor;
import com.odigeo.interactors.CreateShoppingCartInteractor;
import com.odigeo.interactors.DeleteArrivalGuideInformationInteractor;
import com.odigeo.interactors.DeleteBookingsInteractor;
import com.odigeo.interactors.EnableNotificationsInteractor;
import com.odigeo.interactors.FindBuyerInLocalPassengersInteractor;
import com.odigeo.interactors.FlightDetailsInteractor;
import com.odigeo.interactors.GetCrossSellingForBookingInteractor;
import com.odigeo.interactors.GetNextSegmentByDateInteractor;
import com.odigeo.interactors.GetTravellersByTypeInteractor;
import com.odigeo.interactors.GetTravellersInteractor;
import com.odigeo.interactors.IdentificationsInteractor;
import com.odigeo.interactors.LocalizablesInteractor;
import com.odigeo.interactors.LoginInteractor;
import com.odigeo.interactors.LogoutInteractor;
import com.odigeo.interactors.MembershipInteractor;
import com.odigeo.interactors.MoveFlightStatusSwitchInteractor;
import com.odigeo.interactors.CheckUpcomingBookingInteractor;
import com.odigeo.interactors.MyTripsInteractor;
import com.odigeo.interactors.MyTripsManualImportInteractor;
import com.odigeo.interactors.ObfuscateCreditCardInteractor;
import com.odigeo.interactors.OrderCreditCardsByLastUsageInteractor;
import com.odigeo.interactors.RegisterPasswordInteractor;
import com.odigeo.interactors.RemoveAccountInteractor;
import com.odigeo.interactors.RemoveHistorySearchesInteractor;
import com.odigeo.interactors.RemoveProductsFromShoppingCartInteractor;
import com.odigeo.interactors.RequestForgottenPasswordInteractor;
import com.odigeo.interactors.ResumeBookingInteractor;
import com.odigeo.interactors.SavePaymentMethodInteractor;
import com.odigeo.interactors.SaveTravellersInteractor;
import com.odigeo.interactors.SearchFlightInteractor;
import com.odigeo.interactors.SendFeedbackInteractor;
import com.odigeo.interactors.SortItinerarySectionsInteractor;
import com.odigeo.interactors.TotalPriceCalculatorInteractor;
import com.odigeo.interactors.TravellerDetailInteractor;
import com.odigeo.interactors.UpdateAppVersionCacheInteractor;
import com.odigeo.interactors.UpdateBookingStatusInteractor;
import com.odigeo.interactors.UpdateBookingsFromLocalInteractor;
import com.odigeo.interactors.UpdateBookingsFromNetworkInteractor;
import com.odigeo.interactors.UpdateCitiesInteractor;
import com.odigeo.interactors.VisitUserInteractor;
import com.odigeo.interactors.VisitsInteractor;
import com.odigeo.interactors.managers.BookingStatusAlarmManagerInterface;
import com.odigeo.interactors.notification.OdigeoNotificationManagerInterface;
import com.odigeo.interactors.provider.AssetsProvider;
import com.odigeo.interactors.provider.CarouselProvider;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.interactors.provider.MyTripsProvider;
import com.odigeo.presenter.AboutContactUsPresenter;
import com.odigeo.presenter.AboutFaqPresenter;
import com.odigeo.presenter.AboutUsPresenter;
import com.odigeo.presenter.AccountPreferencesPresenter;
import com.odigeo.presenter.AppRateHelpImprovePresenter;
import com.odigeo.presenter.AppRatePresenter;
import com.odigeo.presenter.AppRateThanksPresenter;
import com.odigeo.presenter.ArrivalGuidePresenter;
import com.odigeo.presenter.ArrivalGuidesCardPresenter;
import com.odigeo.presenter.BaggageCollectionItemPresenter;
import com.odigeo.presenter.BaggageCollectionPresenter;
import com.odigeo.presenter.BinCheckPresenter;
import com.odigeo.presenter.BlockedAccountPresenter;
import com.odigeo.presenter.BoardingGatePresenter;
import com.odigeo.presenter.CalendarPresenter;
import com.odigeo.presenter.CancelledTripPresenter;
import com.odigeo.presenter.ChangePasswordPresenter;
import com.odigeo.presenter.CrossSellingCardsPresenter;
import com.odigeo.presenter.DivertedTripPresenter;
import com.odigeo.presenter.ExternalPaymentPresenter;
import com.odigeo.presenter.FacebookAlreadyRegisteredPresenter;
import com.odigeo.presenter.FlightDetailsPresenter;
import com.odigeo.presenter.FrequentFlyerCodesPresenter;
import com.odigeo.presenter.FrequentFlyerDetailPresenter;
import com.odigeo.presenter.GoogleAlreadyRegisteredPresenter;
import com.odigeo.presenter.HomePresenter;
import com.odigeo.presenter.IdentificationsPresenter;
import com.odigeo.presenter.InsurancesPresenter;
import com.odigeo.presenter.JoinUsPresenter;
import com.odigeo.presenter.LegacyUserPresenter;
import com.odigeo.presenter.LocalizablesUpdaterPresenter;
import com.odigeo.presenter.LoginViewPresenter;
import com.odigeo.presenter.MembershipQAPresenter;
import com.odigeo.presenter.MyTripDetailsPresenter;
import com.odigeo.presenter.MyTripsManualImportPresenter;
import com.odigeo.presenter.MyTripsPresenter;
import com.odigeo.presenter.NavigationDrawerPresenter;
import com.odigeo.presenter.NextTripPresenter;
import com.odigeo.presenter.PassengerPresenter;
import com.odigeo.presenter.PassengerWidgetPresenter;
import com.odigeo.presenter.PaymentFormWidgetPresenter;
import com.odigeo.presenter.PaymentPresenter;
import com.odigeo.presenter.PaymentPurchasePresenter;
import com.odigeo.presenter.PriceBreakdownWidgetPresenter;
import com.odigeo.presenter.PromoCodeWidgetPresenter;
import com.odigeo.presenter.QAModePresenter;
import com.odigeo.presenter.RegisterViewPresenter;
import com.odigeo.presenter.RequestForgottenPasswordInstructionsSentPresenter;
import com.odigeo.presenter.RequestForgottenPasswordPresenter;
import com.odigeo.presenter.SearchDropOffCardViewPresenter;
import com.odigeo.presenter.SplashPresenter;
import com.odigeo.presenter.TermsAndConditionsPresenter;
import com.odigeo.presenter.TopBriefWidgetPresenter;
import com.odigeo.presenter.TravellerDetailPresenter;
import com.odigeo.presenter.TravellersListPresenter;
import com.odigeo.presenter.contracts.ExternalPaymentNavigatorInterface;
import com.odigeo.presenter.contracts.mappers.xsell.CrossSellingCardMapper;
import com.odigeo.presenter.contracts.navigators.AboutUsNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.AccountPreferencesNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.AccountPreferencesViewInterface;
import com.odigeo.presenter.contracts.navigators.AppRateFeedbackNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.AppRateNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.ArrivalGuideViewerNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.CalendarNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.FlightDetailsNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.FrequentFlyerCodesInterface;
import com.odigeo.presenter.contracts.navigators.FrequentFlyerCodesNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.IdentificationsNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.InsurancesNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.JoinUsNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.LocalizablesUpdaterNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.LoginNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.MembershipQANavigatorInterface;
import com.odigeo.presenter.contracts.navigators.MyTripDetailsNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.MyTripsNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.NavigationDrawerNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.PassengerNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.PaymentNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.QAModeNavigator;
import com.odigeo.presenter.contracts.navigators.RegisterNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.RequestForgottenPasswordNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.SocialLoginNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.SplashNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.TravellersListNavigatorInterface;
import com.odigeo.presenter.contracts.views.AboutContactUsViewInterface;
import com.odigeo.presenter.contracts.views.AboutFaqViewInterface;
import com.odigeo.presenter.contracts.views.AboutUsViewInterface;
import com.odigeo.presenter.contracts.views.AppRateHelpImproveViewInterface;
import com.odigeo.presenter.contracts.views.AppRateThanksViewInterface;
import com.odigeo.presenter.contracts.views.AppRateViewInterface;
import com.odigeo.presenter.contracts.views.ArrivalGuideViewInterface;
import com.odigeo.presenter.contracts.views.ArrivalGuidesProviderViewInterface;
import com.odigeo.presenter.contracts.views.BaggageCollection;
import com.odigeo.presenter.contracts.views.BaggageCollectionItem;
import com.odigeo.presenter.contracts.views.BinCheckViewInterface;
import com.odigeo.presenter.contracts.views.BlockedAccountViewInterface;
import com.odigeo.presenter.contracts.views.BoardingGateViewInterface;
import com.odigeo.presenter.contracts.views.CalendarViewInterface;
import com.odigeo.presenter.contracts.views.CancelledTripViewInterface;
import com.odigeo.presenter.contracts.views.ChangePasswordViewInterface;
import com.odigeo.presenter.contracts.views.CrossSellingCardsView;
import com.odigeo.presenter.contracts.views.DivertedTripViewInterface;
import com.odigeo.presenter.contracts.views.ExternalPaymentViewInterface;
import com.odigeo.presenter.contracts.views.FacebookAlreadyRegisteredViewInterface;
import com.odigeo.presenter.contracts.views.FlightDetailsViewInterface;
import com.odigeo.presenter.contracts.views.FrequentFlyerDetailViewInterface;
import com.odigeo.presenter.contracts.views.GoogleAlreadyRegisteredViewInterface;
import com.odigeo.presenter.contracts.views.HomeViewInterface;
import com.odigeo.presenter.contracts.views.IdentificationsViewInterface;
import com.odigeo.presenter.contracts.views.InsurancesViewInterface;
import com.odigeo.presenter.contracts.views.JoinUsViewInterface;
import com.odigeo.presenter.contracts.views.LegacyUserViewInterface;
import com.odigeo.presenter.contracts.views.LocalizablesUpdaterViewInterface;
import com.odigeo.presenter.contracts.views.LoginSocialInterface;
import com.odigeo.presenter.contracts.views.MembershipQAViewInterface;
import com.odigeo.presenter.contracts.views.MyTripDetailsViewInterface;
import com.odigeo.presenter.contracts.views.MyTripsManualImportViewInterface;
import com.odigeo.presenter.contracts.views.MyTripsViewInterface;
import com.odigeo.presenter.contracts.views.NavigationDrawerViewInterface;
import com.odigeo.presenter.contracts.views.NextTripViewInterface;
import com.odigeo.presenter.contracts.views.PassengerViewInterface;
import com.odigeo.presenter.contracts.views.PassengerWidget;
import com.odigeo.presenter.contracts.views.PaymentFormWidgetInterface;
import com.odigeo.presenter.contracts.views.PaymentPurchaseWidgetInterface;
import com.odigeo.presenter.contracts.views.PaymentViewInterface;
import com.odigeo.presenter.contracts.views.PriceBreakdownWidgetInterface;
import com.odigeo.presenter.contracts.views.PromoCodeWidget;
import com.odigeo.presenter.contracts.views.QAModeView;
import com.odigeo.presenter.contracts.views.RegisterViewInterface;
import com.odigeo.presenter.contracts.views.RequestForgottenPasswordIntructionsSentViewInterface;
import com.odigeo.presenter.contracts.views.RequestForgottenPasswordViewInterface;
import com.odigeo.presenter.contracts.views.SearchDropOffCardViewInterface;
import com.odigeo.presenter.contracts.views.SplashViewInterface;
import com.odigeo.presenter.contracts.views.TermsAndConditionsViewInterface;
import com.odigeo.presenter.contracts.views.TopBriefViewInterface;
import com.odigeo.presenter.contracts.views.TravellerDetailViewInterface;
import com.odigeo.presenter.contracts.views.TravellersListViewInterface;
import com.odigeo.presenter.listeners.LocalizablesUtilsListener;
import com.odigeo.presenter.listeners.PaymentWidgetFormListener;
import com.odigeo.provider.FileProvider;
import com.odigeo.tools.ConditionRulesHelper;
import com.odigeo.tools.CreditCardRequestToStoreCreditCardRequestMapper;
import com.odigeo.tools.DateHelperInterface;
import com.odigeo.tools.DurationFormatter;

public abstract class DependencyInjector {

  public static FrequentFlyerCodesPresenter provideFrequentFlyerCodesPresenter(
      FrequentFlyerCodesInterface view, FrequentFlyerCodesNavigatorInterface navigator) {
    return new FrequentFlyerCodesPresenter(view, navigator);
  }

  public abstract NetToolInterface provideNetTool();

  public abstract CarouselManagerInterface provideCarouselManager();

  public abstract CarrouselNetControllerInterface provideCarouselNetController();

  public abstract UserNetControllerInterface provideUserNetController();

  public abstract NotificationNetControllerInterface provideNotificationsNetController();

  public abstract BookingNetControllerInterface provideBookingNetController();

  public abstract CountryNetControllerInterface provideCountryNetController();

  public abstract SearchesNetControllerInterface provideSearchesNetController();

  public abstract DomainHelperInterface provideDomainHelper();

  public abstract HeaderHelperInterface provideHeaderHelper();

  public abstract UserCreateOrUpdateHandlerInterface provideUserCreateOrUpdateDBHandler();

  public abstract SessionController provideSessionController();

  public abstract PreferencesControllerInterface providePreferencesController();

  public abstract UserDBDAOInterface provideUserDBDAO();

  public abstract DownloadController provideDownloadController();

  public abstract BookingDBDAOInterface provideBookingDBDAO();

  public abstract BuyerDBDAOInterface provideBuyerDBDAO();

  public abstract TravellerDBDAOInterface provideTravellerDBDAO();

  public abstract BaggageDBDAOInterface provideBaggageDBDAO();

  public abstract SegmentDBDAOInterface provideSegmentDBDAO();

  public abstract CarrierDBDAOInterface provideCarrierDBDAO();

  public abstract SectionDBDAOInterface provideSectionDBDAO();

  public abstract ItineraryBookingDBDAOInterface provideItineraryDBDAO();

  public abstract LocationBookingDBDAOInterface provideLocationBookingDBDAO();

  public abstract InsuranceDBDAOInterface provideInsuranceDBDAO();

  public abstract UserTravellerDBDAOInterface provideUserTravellersDBDAO();

  public abstract UserProfileDBDAOInterface provideUserProfileDBDao();

  public abstract UserAddressDBDAOInterface provideUserAddressDBDao();

  public abstract UserIdentificationDBDAOInterface provideUserIdentificationDBDao();

  public abstract UserFrequentFlyerDBDAOInterface provideUserFrequentFlyerDBDao();

  public abstract StoredSearchDBDAOInterface provideStoredSearchesDBDao();

  public abstract SearchSegmentDBDAOInterface provideSearchSegmentDBDao();

  public abstract CityDBDAOInterface provideCityDBDao();

  public abstract TravellersHandlerInterface provideTravellersHandler();

  public abstract TrackerControllerInterface provideTrackerController();

  public abstract MembershipDBDAOInterface provideMembershipDBDao();

  public abstract CustomDimensionFormatter provideAnalyticsCustomDimensionFormatter();

  public abstract BookingsHandlerInterface provideBookingsHandler();

  public abstract SearchesHandlerInterface provideSearchesHandler();

  public abstract CitiesHandlerInterface provideCitiesHandler();

  public abstract DeleteBookingsInteractor provideDeleteBookingsInteractor();

  public abstract ArrivalGuideNetControllerInterface provideArrivalGuideNetController();

  public abstract MembershipHandlerInterface provideMembershipHandler();

  public abstract GuideDBDAOInterface provideGuideDBDAO();

  public abstract FlightStatsDBDAOInterface provideFlightStatsDBDAO();

  public abstract CountriesDbHelperInterface provideCountriesDbHelper();

  public abstract OdigeoNotificationManagerInterface provideOdigeoNotificationManager();

  public abstract BookingStatusAlarmManagerInterface provideBookingStatusAlarmManager();

  public abstract CardFactory provideCardFactory();

  public abstract SectionCardBuilder provideSectionCardBuilder();

  public abstract DateHelperInterface provideDateHelper();

  public abstract ConditionRulesHelper provideConditionRulesHelper();

  public abstract ConfirmBookingNetControllerInterface provideConfirmBookingNetController();

  public abstract ResumeBookingNetController provideResumeBookingNetController();

  public abstract AddPassengerNetControllerInterface provideAddPassengerNetController();

  public abstract ShoppingCartNetControllerInterface provideShoppingCartNetController();

  public abstract SendMailNetControllerInterface provideSendMailNetController();

  public abstract AutoCompleteNetControllerInterface provideAutoCompleteNetController();

  public abstract MarketProviderInterface provideMarketProvider();

  public abstract MyTripsProvider provideMyTripsProvider();

  public abstract AddProductsNetControllerInterface provideAddProductsNetController();

  public abstract BinCheckNetControllerInterface provideBinCheckNetController();

  public abstract RemoveProductsNetControllerInterface provideRemoveProductsNetController();

  public abstract TuneTrackerInterface provideTuneTracker();

  public abstract CrossCarUrlHandlerInterface provideCrossCarUrlHandler();

  public abstract RemoteConfigControllerInterface provideRemoteConfigController();

  public abstract VisitsNetController provideVisitsNetController();

  public abstract CrashlyticsController provideCrashlyticsController();

  public abstract PackageController providePackageController();

  public abstract LocalizableProvider provideLocalizables();

  public abstract BaggageCollectionItemPresenter provideBaggageCollectionItemPresenter(
      BaggageCollectionItem baggageCollectionItem);

  public abstract BaggageCollectionPresenter provideBaggageCollectionPresenter(
      BaggageCollection baggageCollection);

  public abstract VisitUserNetController provideVisitUserNetController();

  public abstract AssetsProvider provideAssetsProvider();

  public abstract FileProvider provideFileProvider();

  public abstract SavePaymentMethodNetController provideSavePaymentMethodNetController();

  protected abstract CreditCardRequestToStoreCreditCardRequestMapper provideCreditCardRequestToStoreCreditCardRequestMapper();

  public abstract TrustDefenderController provideTrustDefenderController();

  public abstract LocalizablesNetController provideLocalizablesNetController();

  public abstract LocalizablesUtilsListener provideLocalizablesListener();

  public abstract OneCMSAlarmsController provideOneCMSAlarmsController();

  //*********************
  //     BUILDER
  //*********************

  //*********************
  //     PRESENTER
  //*********************
  public abstract AddPassengerNetJSONBuilderInterface provideAddPassengerNetJSONBuilder();

  public AboutContactUsPresenter provideAboutContactUsPresenter(AboutContactUsViewInterface view,
      AboutUsNavigatorInterface navigator) {
    return new AboutContactUsPresenter(view, navigator);
  }

  public AboutFaqPresenter provideAboutFaqPresenter(AboutFaqViewInterface view,
      AboutUsNavigatorInterface navigator) {
    return new AboutFaqPresenter(view, navigator);
  }

  public AboutUsPresenter provideAboutUsPresenter(AboutUsViewInterface view,
      AboutUsNavigatorInterface navigator) {
    return new AboutUsPresenter(view, navigator);
  }

  public TermsAndConditionsPresenter provideTermsAndConditionsPresenter(
      TermsAndConditionsViewInterface view, AboutUsNavigatorInterface navigator) {
    return new TermsAndConditionsPresenter(view, navigator);
  }

  public PassengerWidgetPresenter providePassengerWidgetPresenter(PassengerWidget view) {
    return new PassengerWidgetPresenter(view, provideCheckUserBirthdayInteractor(),
        provideDateHelper(), provideConditionRulesHelper(),
        provideUpdateUserTravellerWithTravellerRequestAndBuyerRequest(),
        provideTravellerRequestToUserTravellerParser(), provideGetTravellersByTypeInteractor(),
        provideSortItinerarySectionsInteractor(), provideMarketProvider(),
        provideMembershipInteractor());
  }

  /**
   * Inject {@link FrequentFlyerDetailPresenter}
   *
   * @param viewInterface Instance of {@link FrequentFlyerDetailViewInterface}.
   * @param navigatorInterface Instance of {@link FrequentFlyerCodesNavigatorInterface}.
   * @return A new instance of {@link FrequentFlyerDetailPresenter}
   */
  public FrequentFlyerDetailPresenter provideFrequentFlyerDetailPresenter(
      FrequentFlyerDetailViewInterface viewInterface,
      FrequentFlyerCodesNavigatorInterface navigatorInterface) {
    return new FrequentFlyerDetailPresenter(viewInterface, navigatorInterface);
  }

  public LoginViewPresenter provideLoginPresenter(LoginSocialInterface view,
      LoginNavigatorInterface navigator) {
    return new LoginViewPresenter(view, navigator, provideLoginInteractor(),
        provideTravellerDetailInteractor(), provideMyTripsInteractor(), provideTrackerController());
  }

  /**
   * Inject {@link AccountPreferencesPresenter}
   * <p/>
   * return {@link AccountPreferencesPresenter}
   */
  public AccountPreferencesPresenter provideAccountPreferencesPresenter(
      AccountPreferencesViewInterface view, AccountPreferencesNavigatorInterface navigator) {
    return new AccountPreferencesPresenter(view, navigator, provideRemoveAccountInteractor(),
        provideCheckUserCredentialsInteractor(), provideLogoutInteractor(),
        provideSessionController());
  }

  /**
   * Inject {@link RemoveAccountInteractor}
   *
   * @return {@link RemoveAccountInteractor}
   */
  private RemoveAccountInteractor provideRemoveAccountInteractor() {
    return new RemoveAccountInteractor(provideUserNetController(), provideSessionController());
  }

  /**
   * Inject {@link ChangePasswordPresenter}
   *
   * @return {@link ChangePasswordPresenter}
   */
  public ChangePasswordPresenter provideChangePasswordPresenter(ChangePasswordViewInterface view,
      AccountPreferencesNavigatorInterface navigator) {
    return new ChangePasswordPresenter(view, navigator, provideChangePasswordInteractor(),
        provideLogoutInteractor(), provideSessionController());
  }

  public PassengerPresenter providePassengersPresenter(PassengerViewInterface view,
      PassengerNavigatorInterface navigator) {
    return new PassengerPresenter(view, navigator, provideAddPassengerToShoppingCartInteractor(),
        provideCheckUserCredentialsInteractor(), provideGetTravellersByTypeInteractor(),
        provideCheckUserBirthdayInteractor(), provideDateHelper(),
        provideFindBuyerInLocalPassengersInteractor(), provideCountriesInteractor(),
        provideSaveTravellersInteractor(),
        provideUpdateUserTravellerWithTravellerRequestAndBuyerRequest(),
        provideMixBuyerAndTravellerHelper(), provideVisitUserInteractor(),
        provideMembershipInteractor(), provideTotalPriceCalculatorInteractor(),
        provideSessionController());
  }

  private GetTravellersByTypeInteractor provideGetTravellersByTypeInteractor() {
    return new GetTravellersByTypeInteractor(provideTravellersHandler());
  }

  /**
   * Inject {@link ChangePasswordInteractor}
   *
   * @return {@link ChangePasswordInteractor}
   */
  private ChangePasswordInteractor provideChangePasswordInteractor() {
    return new ChangePasswordInteractor(provideUserNetController(), provideSessionController());
  }

  /**
   * Inject {@link RegisterViewPresenter}
   *
   * @return {@link RegisterViewPresenter}
   */
  public RegisterViewPresenter provideRegisterPresenter(RegisterViewInterface view,
      RegisterNavigatorInterface navigator) {
    return new RegisterViewPresenter(view, provideRegisterUserPasswordInteractor(),
        provideLoginInteractor(), provideTravellerDetailInteractor(), provideMyTripsInteractor(),
        navigator, provideLogoutInteractor(), provideSessionController(),
        provideTrackerController());
  }

  /**
   * Inject {@link GoogleAlreadyRegisteredPresenter}
   *
   * @return {@link GoogleAlreadyRegisteredPresenter}
   */
  public GoogleAlreadyRegisteredPresenter provideGoogleRegisteredPresenter(
      GoogleAlreadyRegisteredViewInterface view, SocialLoginNavigatorInterface navigator) {
    return new GoogleAlreadyRegisteredPresenter(view, provideLoginInteractor(),
        provideTravellerDetailInteractor(), navigator, provideMyTripsInteractor(),
        provideTrackerController());
  }

  /**
   * Inject {@link FacebookAlreadyRegisteredPresenter}
   *
   * @return {@link FacebookAlreadyRegisteredPresenter}
   */
  public FacebookAlreadyRegisteredPresenter provideFacebookRegisteredPresenter(
      FacebookAlreadyRegisteredViewInterface view, SocialLoginNavigatorInterface navigator) {
    return new FacebookAlreadyRegisteredPresenter(view, provideLoginInteractor(),
        provideTravellerDetailInteractor(), navigator, provideMyTripsInteractor(),
        provideTrackerController());
  }

  /**
   * Inject {@link RequestForgottenPasswordPresenter}
   *
   * @return {@link RequestForgottenPasswordPresenter}
   */
  public RequestForgottenPasswordPresenter provideRequestForgottenPasswordPresenter(
      RequestForgottenPasswordViewInterface view,
      RequestForgottenPasswordNavigatorInterface navigator) {
    return new RequestForgottenPasswordPresenter(view, provideRequestForgottenPasswordInteractor(),
        navigator, provideLogoutInteractor(), provideSessionController(),
        provideTrackerController());
  }

  /**
   * Inject {@link RequestForgottenPasswordInstructionsSentPresenter}
   *
   * @return {@link RequestForgottenPasswordInstructionsSentPresenter}
   */
  public RequestForgottenPasswordInstructionsSentPresenter provideRequestForgottenPasswordIntructionsSentPresenter(
      RequestForgottenPasswordIntructionsSentViewInterface view,
      RequestForgottenPasswordNavigatorInterface navigator) {
    return new RequestForgottenPasswordInstructionsSentPresenter(view, navigator);
  }

  /**
   * Inject {@link BlockedAccountPresenter}
   *
   * @return {@link BlockedAccountPresenter}
   */
  public BlockedAccountPresenter provideBlockedAccountPresenter(BlockedAccountViewInterface view,
      LoginNavigatorInterface navigator) {
    return new BlockedAccountPresenter(view, navigator);
  }

  /**
   * Inject {@link JoinUsPresenter}
   *
   * @return {@link JoinUsPresenter}
   */
  public JoinUsPresenter provideJoinUsPresenter(JoinUsViewInterface view,
      JoinUsNavigatorInterface navigator) {
    return new JoinUsPresenter(view, navigator);
  }

  /**
   * Inject {@link NavigationDrawerPresenter}
   *
   * @return {@link HomePresenter}
   */
  public HomePresenter provideHomePresenter(HomeViewInterface view,
      NavigationDrawerNavigatorInterface navigator) {
    return new HomePresenter(view, navigator, provideEventsNotifier(), provideCarouselProvider(),
        provideSubscriptionImpl(), provideSessionController(), provideLogoutInteractor(),
        provideCheckUserCredentialsInteractor(), provideCrossCarUrlHandler(),
        providePreferencesController(), provideMarketProvider(), provideUpdateCitiesInteractor());
  }

  /**
   * Inject {@link NavigationDrawerPresenter}
   *
   * @return {@link NavigationDrawerPresenter}
   */
  public NavigationDrawerPresenter provideNavigationDrawerPresenter(
      NavigationDrawerViewInterface view, NavigationDrawerNavigatorInterface navigator) {
    return new NavigationDrawerPresenter(view, navigator, provideCheckUserCredentialsInteractor(),
        provideTravellerDetailInteractor(), provideMyTripsInteractor(), provideLogoutInteractor(),
        provideSessionController(), provideTrackerController());
  }

  /**
   * Inject {@link LegacyUserPresenter}
   *
   * @return {@link LegacyUserPresenter}
   */
  public LegacyUserPresenter provideLegacyUserPresenter(LegacyUserViewInterface view,
      LoginNavigatorInterface navigator) {
    return new LegacyUserPresenter(view, navigator);
  }

  /**
   * Inject {@link MyTripsPresenter}
   *
   * @return {@link MyTripsPresenter}
   */
  public MyTripsPresenter provideMyTripsPresenter(MyTripsViewInterface view,
      MyTripsNavigatorInterface navigator) {

    return new MyTripsPresenter(view, navigator, provideMyTripsInteractor(),
        provideMyTripsProvider(), provideTrackerController(),
        provideUpdateBookingFromNetworkInteractor(), provideUpdateBookingStatusInteractor(),
        providePreferencesController(), provideCheckUserCredentialsInteractor());
  }

  /**
   * Inject {@link MyTripDetailsPresenter}
   *
   * @return {@link MyTripDetailsPresenter}
   */

  public MyTripDetailsPresenter provideMyTripDetailsPresenter(MyTripDetailsViewInterface view,
      MyTripDetailsNavigatorInterface navigator) {
    return new MyTripDetailsPresenter(view, navigator, provideCheckUpcomingBookingInteractor(),
        provideArrivalGuidesCardsInteractor(), provideUpdateBookingStatusInteractor(),
        provideCheckUserCredentialsInteractor(), provideTrackerController());
  }

  /**
   * Inject {@link MyTripsManualImportPresenter}
   *
   * @return {@link MyTripsManualImportPresenter}
   */
  public MyTripsManualImportPresenter provideMyTripsManualImportPresenter(
      MyTripsManualImportViewInterface view, MyTripsNavigatorInterface navigator) {
    return new MyTripsManualImportPresenter(view, navigator, provideMyTripsManualImportInteractor(),
        provideTrackerController(), provideMyTripsInteractor());
  }

  /**
   * Inject {@link FlightDetailsPresenter}
   *
   * @return {@link FlightDetailsPresenter}
   */

  public FlightDetailsPresenter provideFlightDetailsPresenter(FlightDetailsViewInterface view,
      FlightDetailsNavigatorInterface navigator) {
    return new FlightDetailsPresenter(view, navigator, provideFlightDetailsInteractor());
  }

  public InsurancesPresenter provideInsurancesPresenter(InsurancesViewInterface view,
      InsurancesNavigatorInterface navigator) {

    return new InsurancesPresenter(view, navigator, provideAddProductsInteractor(),
        provideRemoveProductsInteractor(), provideMembershipInteractor(),
        provideTotalPriceCalculatorInteractor());
  }

  public PaymentPresenter providePaymentPresenter(PaymentViewInterface view,
      PaymentNavigatorInterface navigator) {

    return new PaymentPresenter(view, navigator, provideConfirmBookingInteractor(),
        provideMyTripsManualImportInteractor(), provideResumeBookingInteractor(),
        provideCreditCardRequestToStoreCreditCardRequestMapper(), provideABTestHelper(),
        provideMembershipInteractor(), provideTotalPriceCalculatorInteractor());
  }

  public CalendarPresenter provideCalendarPresenter(CalendarViewInterface view,
      CalendarNavigatorInterface navigator) {
    return new CalendarPresenter(view, navigator);
  }

  public ExternalPaymentPresenter provideExternalPaymentPresenter(ExternalPaymentViewInterface view,
      ExternalPaymentNavigatorInterface navigator) {
    return new ExternalPaymentPresenter(view, navigator, provideAssetsProvider());
  }

  //*********************
  //     PROVIDER
  //*********************

  /**
   * Inject {@link CarouselProvider}
   *
   * @return {@link CarouselProvider}
   */
  private CarouselProvider provideCarouselProvider() {
    return new CarouselProvider(provideDropOffCarouselCardBuilder(),
        provideBookingCarouselCardBuilder(), providePromotionCarouselCardBuilder(),
        provideRemoteConfigController());
  }

  //*********************
  //     NOTIFIER
  //*********************
  public EventsNotifierImpl provideEventsNotifier() {
    return EventsNotifierImpl.getInstance();
  }

  private LoadCarouselSubscriber provideLoadCarouselSubscriber() {
    return new LoadCarouselSubscriber();
  }

  private SubscriptionImpl provideSubscriptionImpl() {
    return new SubscriptionImpl(provideLoadCarouselSubscriber());
  }

  public LoadCarouselEvent provideLoadCarouselEvent() {
    return new LoadCarouselEvent();
  }

  //*********************
  //     INTERACTOR
  //*********************

  private ResumeBookingInteractor provideResumeBookingInteractor() {
    return new ResumeBookingInteractor(provideResumeBookingNetController());
  }

  private ConfirmBookingInteractor provideConfirmBookingInteractor() {
    return new ConfirmBookingInteractor(provideConfirmBookingNetController());
  }

  public BinCheckPresenter provideCheckBinPresenter(BinCheckViewInterface binCheckViewInterface) {
    return new BinCheckPresenter(binCheckViewInterface, provideCheckBinFromLocalInteractor(),
        provideCheckBinFromNetworkInteractor());
  }

  private CheckBinFromLocalInteractor provideCheckBinFromLocalInteractor() {
    return new CheckBinFromLocalInteractor();
  }

  public CheckBinFromNetworkInteractor provideCheckBinFromNetworkInteractor() {
    return new CheckBinFromNetworkInteractor(provideBinCheckNetController());
  }

  private AddProductsToShoppingCartInteractor provideAddProductsInteractor() {
    return new AddProductsToShoppingCartInteractor(provideAddProductsNetController(),
        provideTrustDefenderController(), providePreferencesController());
  }

  private RemoveProductsFromShoppingCartInteractor provideRemoveProductsInteractor() {
    return new RemoveProductsFromShoppingCartInteractor(provideRemoveProductsNetController(),
        provideTrustDefenderController(), providePreferencesController());
  }

  private AddPassengerToShoppingCartInteractor provideAddPassengerToShoppingCartInteractor() {
    return new AddPassengerToShoppingCartInteractor(provideAddPassengerNetController(),
        provideTrustDefenderController(), providePreferencesController());
  }

  private FindBuyerInLocalPassengersInteractor provideFindBuyerInLocalPassengersInteractor() {
    return new FindBuyerInLocalPassengersInteractor();
  }

  public GetTravellersInteractor provideGetTravellersInteractor() {
    return new GetTravellersInteractor(provideTravellersHandler());
  }

  private SaveTravellersInteractor provideSaveTravellersInteractor() {
    return new SaveTravellersInteractor(provideUserCreateOrUpdateDBHandler());
  }

  private CheckUserBirthdayInteractor provideCheckUserBirthdayInteractor() {
    return new CheckUserBirthdayInteractor();
  }

  private GetNextSegmentByDateInteractor provideGetNextSegmentByDateInteractor() {
    return new GetNextSegmentByDateInteractor();
  }

  private LogoutInteractor provideLogoutInteractor() {
    return new LogoutInteractor(provideSessionController(), provideNotificationsNetController(),
        provideBookingsHandler(), provideSearchesHandler(), provideVisitUserInteractor(),
        provideMembershipHandler());
  }

  public MoveFlightStatusSwitchInteractor provideNotificationSwitchInteractor() {
    return new MoveFlightStatusSwitchInteractor(provideNotificationsNetController(),
        provideSessionController(), provideCheckUserCredentialsInteractor(),
        provideMyTripsInteractor(), provideLocalizables());
  }

  public UpdateBookingsFromNetworkInteractor provideUpdateBookingFromNetworkInteractor() {
    return new UpdateBookingsFromNetworkInteractor(provideBookingNetController(),
        provideBookingDBDAO(), provideBookingsHandler(), provideAddGuideInformationInteractor(),
        provideUpdateBookingStatusInteractor(), providePreferencesController());
  }

  public UpdateBookingsFromLocalInteractor provideUpdateBookingFromLocalInteractor() {
    return new UpdateBookingsFromLocalInteractor(provideBookingNetController(),
        provideBookingDBDAO(), provideBookingsHandler(), provideAddGuideInformationInteractor(),
        provideUpdateBookingStatusInteractor(), providePreferencesController());
  }

  private AddGuideInformationInteractor provideAddGuideInformationInteractor() {
    return new AddGuideInformationInteractor(provideSegmentDBDAO(), provideSectionDBDAO(),
        provideLocationBookingDBDAO(), provideGuideDBDAO(), provideArrivalGuideNetController());
  }

  public UpdateBookingStatusInteractor provideUpdateBookingStatusInteractor() {
    return new UpdateBookingStatusInteractor(provideBookingDBDAO(), provideBookingsHandler(),
        provideBookingNetController(), provideOdigeoNotificationManager(),
        provideCheckUserCredentialsInteractor(), provideBookingStatusAlarmManager(),
        provideEventsNotifier());
  }

  /**
   * Inject {@link LoginInteractor}
   *
   * @return {@link LoginInteractor}
   */
  private LoginInteractor provideLoginInteractor() {
    return new LoginInteractor(provideUserNetController(), provideUserCreateOrUpdateDBHandler(),
        provideSessionController(), provideTravellerDetailInteractor(),
        provideEnableNotificationsInteractor(), providePreferencesController(),
        provideUpdateCitiesInteractor());
  }

  public EnableNotificationsInteractor provideEnableNotificationsInteractor() {
    return new EnableNotificationsInteractor(provideNotificationsNetController(),
        provideSessionController());
  }

  /**
   * Inject {@link RegisterPasswordInteractor}
   *
   * @return {@link RegisterPasswordInteractor}
   */
  private RegisterPasswordInteractor provideRegisterUserPasswordInteractor() {
    return new RegisterPasswordInteractor(provideUserNetController(), provideSessionController());
  }

  /**
   * Inject {@link CheckUserCredentialsInteractor}
   *
   * @return {@link CheckUserCredentialsInteractor}
   */
  public CheckUserCredentialsInteractor provideCheckUserCredentialsInteractor() {
    return new CheckUserCredentialsInteractor(provideSessionController(), provideBookingDBDAO());
  }

  /**
   * Inject {@link RequestForgottenPasswordInteractor}
   *
   * @return {@link RequestForgottenPasswordInteractor}
   */
  private RequestForgottenPasswordInteractor provideRequestForgottenPasswordInteractor() {
    return new RequestForgottenPasswordInteractor(provideUserNetController());
  }

  /**
   * Inject {@link MyTripsInteractor}
   *
   * @return {@link MyTripsInteractor}
   */
  public MyTripsInteractor provideMyTripsInteractor() {
    return new MyTripsInteractor(provideBookingDBDAO(), provideBookingsHandler(),
        provideDateHelper());
  }

  /**
   * Inject {@link DeleteArrivalGuideInformationInteractor}
   *
   * @return {@link DeleteArrivalGuideInformationInteractor}
   */
  private DeleteArrivalGuideInformationInteractor provideDeleteArrivalGuideInformationInteractor() {
    return new DeleteArrivalGuideInformationInteractor(provideGuideDBDAO(), provideBookingDBDAO(),
        provideLocationBookingDBDAO(), provideSegmentDBDAO(), provideSectionDBDAO());
  }

  /**
   * Inject {@link MyTripsManualImportInteractor}
   *
   * @return {@link MyTripsManualImportInteractor}
   */
  public MyTripsManualImportInteractor provideMyTripsManualImportInteractor() {
    return new MyTripsManualImportInteractor(provideBookingNetController(),
        provideBookingsHandler(), provideAddGuideInformationInteractor(),
        provideBookingStatusAlarmManager());
  }

  /**
   * Inject {@link TravellersListPresenter}
   *
   * @return {@link TravellersListPresenter}
   */
  public TravellersListPresenter provideTravellersListPresenter(TravellersListViewInterface view,
      TravellersListNavigatorInterface navigator) {
    return new TravellersListPresenter(view, navigator, provideTravellerDetailInteractor());
  }

  /**
   * Inject {@link TravellerDetailPresenter}
   *
   * @return {@link TravellerDetailPresenter}
   */
  public TravellerDetailPresenter provideTravellerDetailPresenter(TravellerDetailViewInterface view,
      TravellersListNavigatorInterface navigator) {
    return new TravellerDetailPresenter(view, navigator, provideTravellerDetailInteractor(),
        provideSessionController(), provideLogoutInteractor());
  }

  private TravellerDetailInteractor provideTravellerDetailInteractor() {
    return new TravellerDetailInteractor(provideSessionController(), provideUserNetController(),
        provideUserCreateOrUpdateDBHandler(), provideTravellersHandler());
  }

  /**
   * Inject {@link IdentificationsPresenter}
   *
   * @return {@link IdentificationsPresenter}
   */
  public IdentificationsPresenter provideIdentificationsPresenter(IdentificationsViewInterface view,
      IdentificationsNavigatorInterface navigator) {
    return new IdentificationsPresenter(view, navigator, provideIdentificationsInteractor(),
        provideCountriesDbHelper());
  }

  private IdentificationsInteractor provideIdentificationsInteractor() {
    return new IdentificationsInteractor();
  }

  public BookingInteractor provideBookingInteractor() {
    return new BookingInteractor(provideBookingDBDAO(), provideBookingsHandler());
  }

  /**
   * Inject {@link FlightDetailsInteractor}
   *
   * @return {@link FlightDetailsInteractor}
   */
  private FlightDetailsInteractor provideFlightDetailsInteractor() {
    return new FlightDetailsInteractor();
  }

  private ArrivalGuidesCardsInteractor provideArrivalGuidesCardsInteractor() {
    return new ArrivalGuidesCardsInteractor(provideFileProvider(), provideGuideDBDAO());
  }

  private CheckUpcomingBookingInteractor provideCheckUpcomingBookingInteractor() {
    return new CheckUpcomingBookingInteractor(provideDateHelper());
  }

  public OrderCreditCardsByLastUsageInteractor provideOrderCreditCardsByLastUsageInteractor() {
    return new OrderCreditCardsByLastUsageInteractor();
  }

  /**
   * Inject {@link NextTripPresenter}
   *
   * @return {@link NextTripPresenter}
   */
  public NextTripPresenter provideNextTripPresenter(NextTripViewInterface nextTripView,
      NavigationDrawerNavigatorInterface homeViewInterface) {
    return new NextTripPresenter(nextTripView, homeViewInterface, provideBookingInteractor());
  }

  /**
   * Inject {@link BoardingGatePresenter}
   *
   * @return {@link BoardingGatePresenter}
   */
  public BoardingGatePresenter provideBoardingGatePresenter(
      BoardingGateViewInterface boardingGateViewInterface,
      NavigationDrawerNavigatorInterface homeViewInterface) {
    return new BoardingGatePresenter(boardingGateViewInterface, homeViewInterface,
        provideBookingInteractor());
  }

  /**
   * Inject {@link CancelledTripPresenter}
   *
   * @return {@link CancelledTripPresenter}
   */
  public CancelledTripPresenter provideCancelledTripPresenter(
      CancelledTripViewInterface cancelledTripView,
      NavigationDrawerNavigatorInterface homeViewInterface) {
    return new CancelledTripPresenter(cancelledTripView, homeViewInterface,
        provideBookingInteractor(), provideCarouselManager(), provideEventsNotifier(),
        provideLoadCarouselEvent());
  }

  /**
   * Inject {@link com.odigeo.presenter.DivertedTripPresenter}
   *
   * @return {@link com.odigeo.presenter.DivertedTripPresenter}
   */
  public DivertedTripPresenter provideDivertedTripPresenter(
      DivertedTripViewInterface divertedTripView,
      NavigationDrawerNavigatorInterface homeViewInterface) {
    return new DivertedTripPresenter(divertedTripView, homeViewInterface,
        provideBookingInteractor(), provideCarouselManager(), provideEventsNotifier(),
        provideLoadCarouselEvent());
  }

  public ArrivalGuidesCardPresenter provideArrivalGuidesCardsPresenter(
      ArrivalGuidesProviderViewInterface view, MyTripDetailsNavigatorInterface navigator) {
    return new ArrivalGuidesCardPresenter(view, provideArrivalGuidesCardsInteractor(), navigator,
        provideDownloadController(), provideTrackerController());
  }

  public ArrivalGuidePresenter provideArrivalGuidesPresenter(ArrivalGuideViewInterface view,
      ArrivalGuideViewerNavigatorInterface navigator) {
    return new ArrivalGuidePresenter(view, navigator);
  }

  public CountriesInteractor provideCountriesInteractor() {
    return new CountriesInteractor(provideCountryNetController(), provideCountriesDbHelper());
  }

  public AppRateThanksPresenter provideAppRateThanksPresenter(AppRateThanksViewInterface view,
      AppRateFeedbackNavigatorInterface navigator) {
    return new AppRateThanksPresenter(view, navigator, providePreferencesController());
  }

  public AppRateHelpImprovePresenter provideAppRateHelpImprovePresenter(
      AppRateHelpImproveViewInterface view, AppRateFeedbackNavigatorInterface navigator) {
    return new AppRateHelpImprovePresenter(view, provideSendFeedbackInteractor(), navigator,
        provideSessionController(), providePreferencesController());
  }

  public AppRatePresenter provideAppRatePresenter(AppRateViewInterface view,
      AppRateNavigatorInterface navigator) {
    return new AppRatePresenter(view, navigator);
  }

  public PaymentFormWidgetPresenter providePaymentFormPresenter(PaymentFormWidgetInterface view,
      BinCheckPresenter binCheckPresenter, PaymentWidgetFormListener paymentWidgetFormListener) {
    return new PaymentFormWidgetPresenter(view, binCheckPresenter, provideSessionController(),
        provideOrderCreditCardsByLastUsageInteractor(), paymentWidgetFormListener,
        provideTrackerController());
  }

  private SendFeedbackInteractor provideSendFeedbackInteractor() {
    return new SendFeedbackInteractor(provideSendMailNetController(), provideMarketProvider());
  }

  public AutoCompleteInteractor provideAutoCompleteInteractor() {
    return new AutoCompleteInteractor(provideAutoCompleteNetController(), provideMarketProvider());
  }

  public SortItinerarySectionsInteractor provideSortItinerarySectionsInteractor() {
    return new SortItinerarySectionsInteractor();
  }

  public SearchFlightInteractor provideSearchFlightInteractor() {
    return new SearchFlightInteractor(provideSearchesNetController(), provideSearchesHandler(),
        provideCheckUserCredentialsInteractor());
  }

  public RemoveHistorySearchesInteractor provideRemoveHistorySearchesInteractor() {
    return new RemoveHistorySearchesInteractor(provideSearchesNetController(),
        provideSearchesHandler(), provideCheckUserCredentialsInteractor());
  }

  public PriceBreakdownWidgetPresenter providePriceBreakdownWidgetPresenter(
      PriceBreakdownWidgetInterface priceBreakdownWidget) {
    return new PriceBreakdownWidgetPresenter(priceBreakdownWidget, provideMembershipInteractor(),
        provideTotalPriceCalculatorInteractor());
  }

  public PromoCodeWidgetPresenter providePromoCodeWidgetPresenter(PromoCodeWidget promoCodeWidget) {
    return new PromoCodeWidgetPresenter(promoCodeWidget, provideAddProductsInteractor(),
        provideRemoveProductsInteractor(), providePreferencesController());
  }

  public DropOffCarouselCardBuilder provideDropOffCarouselCardBuilder() {
    return new DropOffCarouselCardBuilder(provideSearchesHandler(), providePreferencesController());
  }

  public PromotionCarouselCardBuilder providePromotionCarouselCardBuilder() {
    return new PromotionCarouselCardBuilder(provideCarouselNetController(),
        provideCarouselManager());
  }

  public BookingCarouselCardBuilder provideBookingCarouselCardBuilder() {
    return new BookingCarouselCardBuilder(provideMyTripsInteractor(),
        provideGetNextSegmentByDateInteractor(), provideCarouselManager(),
        provideSectionCardBuilder(), provideTrackerController(), provideSessionController(),
        providePreferencesController());
  }

  public PaymentPurchasePresenter providePaymentPurchasePresenter(
      PaymentPurchaseWidgetInterface paymentPurchaseWidgetInterface) {
    return new PaymentPurchasePresenter(paymentPurchaseWidgetInterface, provideTrackerController());
  }

  public VisitsInteractor provideVisitsInteractor() {
    return new VisitsInteractor(provideVisitsNetController(), providePreferencesController());
  }

  public SearchDropOffCardViewPresenter provideSearchDropOffCardViewPresenter(
      SearchDropOffCardViewInterface view, NavigationDrawerNavigatorInterface navigator) {
    return new SearchDropOffCardViewPresenter(view, navigator, providePreferencesController(),
        provideCitiesHandler());
  }

  public SplashPresenter provideSplashPresenter(SplashViewInterface splashView,
      SplashNavigatorInterface navigator) {
    return new SplashPresenter(splashView, navigator, providePreferencesController(),
        provideSessionController(), provideVisitsInteractor(), provideTrackerController(),
        provideMyTripsInteractor(), provideUpdateAppVersionCacheInteractor(),
        provideRemoteConfigController(), provideOneCMSAlarmsController());
  }

  public LocalizablesUpdaterPresenter provideLocalizablesUpdaterPresenter(
      LocalizablesUpdaterViewInterface view, LocalizablesUpdaterNavigatorInterface navigator) {
    return new LocalizablesUpdaterPresenter(view, navigator, provideLocalizablesInteractor(),
        provideMarketProvider());
  }

  public VisitUserInteractor provideVisitUserInteractor() {
    return new VisitUserInteractor(provideVisitUserNetController(), provideSessionController(),
        provideCrashlyticsController());
  }

  private UpdateAppVersionCacheInteractor provideUpdateAppVersionCacheInteractor() {
    return new UpdateAppVersionCacheInteractor(providePreferencesController(),
        providePackageController());
  }

  public ObfuscateCreditCardInteractor provideObfuscateCreditCardInteractor() {
    return new ObfuscateCreditCardInteractor();
  }

  public UpdateCitiesInteractor provideUpdateCitiesInteractor() {
    return new UpdateCitiesInteractor(provideAutoCompleteNetController(), provideCitiesHandler(),
        provideMarketProvider(), provideSearchesHandler());
  }

  public MembershipInteractor provideMembershipInteractor() {
    return new MembershipInteractor(provideMembershipHandler(), provideMarketProvider());
  }

  public MembershipQAPresenter provideMembershipQAPresenter(MembershipQAViewInterface view,
      MembershipQANavigatorInterface navigator) {
    return new MembershipQAPresenter(view, navigator, provideMembershipInteractor(),
        provideMarketProvider(), provideSessionController());
  }

  public LocalizablesInteractor provideLocalizablesInteractor() {
    return new LocalizablesInteractor(provideLocalizablesNetController(),
        provideLocalizablesListener(), provideCrashlyticsController());
  }

  //******************
  //     PARSER
  //******************

  private TravellerRequestToUserTravellerParser provideTravellerRequestToUserTravellerParser() {
    return new TravellerRequestToUserTravellerParser();
  }

  private UpdateUserTravellerWithTravellerRequestAndBuyerRequest provideUpdateUserTravellerWithTravellerRequestAndBuyerRequest() {
    return new UpdateUserTravellerWithTravellerRequestAndBuyerRequest(provideIdentificationHelper(),
        provideDateHelper());
  }

  //******************
  //     HELPER
  //******************

  private IdentificationHelper provideIdentificationHelper() {
    return new IdentificationHelper();
  }

  private MixBuyerAndTravellerHelper provideMixBuyerAndTravellerHelper() {
    return new MixBuyerAndTravellerHelper(provideIdentificationHelper());
  }

  public abstract ABTestHelper provideABTestHelper();

  public SavePaymentMethodInteractor provideSavePaymentMethodInteractor() {
    return new SavePaymentMethodInteractor(provideSavePaymentMethodNetController(),
        provideTrackerController(), provideCrashlyticsController());
  }

  public abstract DurationFormatter provideDurationFormatter();

  public abstract CreateShoppingCartInteractor provideCreateShoppingCartInteractor();

  public CrossSellingCardsPresenter provideCrossSellingCardsPresenter(CrossSellingCardsView view,
      MyTripDetailsNavigatorInterface navigator) {
    return new CrossSellingCardsPresenter(view, provideGetCrossSellingForBookingInteractor(),
        provideCrossSellingCardMapper(), provideDownloadController(),
        provideArrivalGuidesCardsInteractor(), navigator);
  }

  public CrossSellingCardMapper provideCrossSellingCardMapper() {
    return new CrossSellingCardMapper();
  }

  public GetCrossSellingForBookingInteractor provideGetCrossSellingForBookingInteractor() {
    return new GetCrossSellingForBookingInteractor(provideCrossSellingRepository());
  }

  public abstract CrossSellingRepository provideCrossSellingRepository();

  public TopBriefWidgetPresenter provideTopBriefWidgetPresenter(TopBriefViewInterface view) {
    return new TopBriefWidgetPresenter(view, provideMembershipInteractor());
  }

  public TotalPriceCalculatorInteractor provideTotalPriceCalculatorInteractor() {
    return new TotalPriceCalculatorInteractor();
  }

  public QAModePresenter provideQAModePresenter(QAModeView qaModeView,
      QAModeNavigator qaModeNavigator) {
    return new QAModePresenter(qaModeView, qaModeNavigator, providePreferencesController());
  }
}
