package com.odigeo.provider;

import java.io.File;

public interface FileProvider {
  File getExternalStorageDirectory();

  File getExternalStorageDownloadDirectory();
}
