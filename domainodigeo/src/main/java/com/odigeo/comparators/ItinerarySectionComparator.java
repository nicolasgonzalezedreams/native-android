package com.odigeo.comparators;

import com.odigeo.data.entity.shoppingCart.SectionResult;
import java.util.Comparator;

public class ItinerarySectionComparator implements Comparator<SectionResult> {

  @Override public int compare(SectionResult t1, SectionResult t2) {
    if (t1.getSection().getDepartureDate().equals(t2.getSection().getDepartureDate())) return 0;
    return Long.valueOf(t1.getSection().getDepartureDate()) < Long.valueOf(
        t2.getSection().getDepartureDate()) ? -1 : 1;
  }
}
