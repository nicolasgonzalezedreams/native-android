package com.odigeo.comparators;

import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import java.util.Comparator;

public class ShoppingCartCollectionOptionComparator
    implements Comparator<ShoppingCartCollectionOption> {

  @Override public int compare(ShoppingCartCollectionOption collection1,
      ShoppingCartCollectionOption collection2) {

    int UPPER_POSITION = 1;
    int LOWER_POSITION = -1;
    int MEDIUM_POSITION = 0;

    switch (collection2.getMethod().getType()) {
      case PREPAY:
      case ELV:
      case SECURE_3_D:
      case INSTALLMENT:
      case SOFORT:
      case GIROPAY:
      case CREDITCARD:
        return UPPER_POSITION;
      case PAYPAL:
        if (collection1.getMethod().getType() == CollectionMethodType.CREDITCARD) {
          return LOWER_POSITION;
        }
        return UPPER_POSITION;
      case BANKTRANSFER:
        if (collection1.getMethod().getType() == CollectionMethodType.CREDITCARD
            || collection1.getMethod().getType() == CollectionMethodType.PAYPAL) {
          return LOWER_POSITION;
        }
        return UPPER_POSITION;
      case TRUSTLY:
        if (collection1.getMethod().getType() == CollectionMethodType.CREDITCARD
            || collection1.getMethod().getType() == CollectionMethodType.PAYPAL
            || collection1.getMethod().getType() == CollectionMethodType.BANKTRANSFER) {
          return LOWER_POSITION;
        }
        return UPPER_POSITION;
      case KLARNA:
        return LOWER_POSITION;
      default:
        return MEDIUM_POSITION;
    }
  }
}
