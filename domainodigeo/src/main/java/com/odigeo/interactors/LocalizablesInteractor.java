package com.odigeo.interactors;

import com.odigeo.data.entity.localizables.LocalizablesResponse;
import com.odigeo.data.net.controllers.LocalizablesNetController;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.presenter.listeners.LocalizablesUtilsListener;
import com.odigeo.presenter.listeners.OnGetLocalizablesListener;

public class LocalizablesInteractor {

  private LocalizablesNetController localizablesNetController;
  private LocalizablesUtilsListener localizablesUtilsListener;
  private CrashlyticsController crashlyticsController;

  public LocalizablesInteractor(LocalizablesNetController localizablesNetController,
      LocalizablesUtilsListener localizablesUtilsListener,
      CrashlyticsController crashlyticsController) {
    this.localizablesNetController = localizablesNetController;
    this.localizablesUtilsListener = localizablesUtilsListener;
    this.crashlyticsController = crashlyticsController;
  }

  public void getLocalizables(final String market,
      final OnGetLocalizablesListener onGetLocalizablesListener) {
    localizablesNetController.getLocalizables(new OnRequestDataListener<LocalizablesResponse>() {
      @Override public void onResponse(LocalizablesResponse response) {
        processLocalizablesResponse(market, response, onGetLocalizablesListener);
      }

      @Override public void onError(MslError error, String message) {
        onGetLocalizablesListener.onError(error, message);
      }
    });
  }

  public void processLocalizablesResponse(final String market, final LocalizablesResponse response,
      final OnGetLocalizablesListener listener) {
    new Thread(new Runnable() {
      @Override public void run() {
        try {
          localizablesUtilsListener.updateLocalizables(market, response.getLocalizables());
          listener.onSuccess();
        } catch (Exception exception) {
          crashlyticsController.trackNonFatal(
              new Exception(CrashlyticsController.INSERT_LOCALIZABLE_EXCEPTION, exception));
          listener.onError(null, exception.getMessage());
        }
      }
    }).start();
  }
}
