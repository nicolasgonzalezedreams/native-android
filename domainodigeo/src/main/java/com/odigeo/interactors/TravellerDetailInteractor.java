package com.odigeo.interactors;

import com.odigeo.data.db.helper.TravellersHandlerInterface;
import com.odigeo.data.db.helper.UserCreateOrUpdateHandlerInterface;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.entity.userData.UserAddress;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.data.net.controllers.UserNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.presenter.listeners.OnUserUpdated;
import com.odigeo.tools.CheckerTool;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by carlos.navarrete
 * on 02/09/15.
 */
public class TravellerDetailInteractor {

  public static final int MAX_AGE_INFANT = 2;
  public static final int MAX_AGE_CHILD = 12;
  public static final int MAX_AGE_ADULT = 100;

  private SessionController mSessionController;
  private UserNetControllerInterface mUserNetController;
  private UserCreateOrUpdateHandlerInterface mUserCreateOrUpdateHandler;
  private TravellersHandlerInterface mTravellersHandlerDB;

  public TravellerDetailInteractor(SessionController sessionController,
      UserNetControllerInterface userNetController,
      UserCreateOrUpdateHandlerInterface userCreateOrUpdateHandler,
      TravellersHandlerInterface readEntitiesInDB) {
    this.mTravellersHandlerDB = readEntitiesInDB;
    this.mSessionController = sessionController;
    this.mUserNetController = userNetController;
    this.mUserCreateOrUpdateHandler = userCreateOrUpdateHandler;
  }

  public boolean isEmptyField(String value) {
    return CheckerTool.isEmptyField(value);
  }

  public boolean isAllEmpty(String... fields) {
    for (String string : fields) {
      if (!isEmptyField(string)) {
        return false;
      }
    }
    return true;
  }

  public Date getMinValidDate(UserTraveller.TypeOfTraveller userTravellerType) {
    if (UserTraveller.TypeOfTraveller.INFANT.equals(userTravellerType)) {
      return getMinBabyDate();
    } else if (UserTraveller.TypeOfTraveller.CHILD.equals(userTravellerType)) {
      return getMinChildDate();
    } else if (UserTraveller.TypeOfTraveller.ADULT.equals(userTravellerType)) {
      return getMinAdultDate();
    }
    return null;
  }

  public Date getMinValidDate(UserTraveller.TypeOfTraveller userTravellerType, long date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(date);
    if (UserTraveller.TypeOfTraveller.INFANT.equals(userTravellerType)) {
      calendar.add(Calendar.YEAR, -MAX_AGE_INFANT);
    } else if (UserTraveller.TypeOfTraveller.CHILD.equals(userTravellerType)) {
      calendar.add(Calendar.YEAR, -MAX_AGE_CHILD);
    } else if (UserTraveller.TypeOfTraveller.ADULT.equals(userTravellerType)) {
      calendar.setTime(getMinAdultDate());
    }
    return calendar.getTime();
  }

  public Date getMaxValidDate(UserTraveller.TypeOfTraveller userTravellerType) {
    if (UserTraveller.TypeOfTraveller.INFANT.equals(userTravellerType)) {
      return getMaxBabyDate();
    } else if (UserTraveller.TypeOfTraveller.CHILD.equals(userTravellerType)) {
      return getMaxChildDate();
    } else if (UserTraveller.TypeOfTraveller.ADULT.equals(userTravellerType)) {
      return getMaxAdultDate();
    }
    return null;
  }

  public Date getMaxBabyDate() {
    return Calendar.getInstance().getTime();
  }

  public Date getMinBabyDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.YEAR, -MAX_AGE_INFANT);
    calendar.add(Calendar.DATE, 1);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    return calendar.getTime();
  }

  public Date getMaxChildDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.YEAR, -MAX_AGE_INFANT);
    calendar.set(Calendar.HOUR_OF_DAY, 23);
    calendar.set(Calendar.MINUTE, 59);
    calendar.set(Calendar.SECOND, 59);
    calendar.set(Calendar.MILLISECOND, 59);
    return calendar.getTime();
  }

  public Date getMinChildDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.YEAR, -MAX_AGE_CHILD);
    calendar.add(Calendar.DATE, 1);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    return calendar.getTime();
  }

  public Date getMaxAdultDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.YEAR, -MAX_AGE_CHILD);
    calendar.set(Calendar.HOUR_OF_DAY, 23);
    calendar.set(Calendar.MINUTE, 59);
    calendar.set(Calendar.SECOND, 59);
    calendar.set(Calendar.MILLISECOND, 59);
    return calendar.getTime();
  }

  public Date getMinAdultDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.YEAR, -MAX_AGE_ADULT);
    calendar.add(Calendar.HOUR_OF_DAY, 0);
    calendar.add(Calendar.MINUTE, 0);
    calendar.add(Calendar.SECOND, 0);
    calendar.add(Calendar.MILLISECOND, 0);
    return calendar.getTime();
  }

  private UserAddress buildUserAddress(long userAddressId, String address, String addressType,
      String city, String country, String state, String postalCode, boolean isPrimary, String alias,
      long userProfileId) {
    if (isAllEmpty(address, addressType, city, country, state, postalCode)) {
      return null;
    } else {
      // Note: for the moment the address Type is "ST."
      return new UserAddress(-1, userAddressId, address, "ST.", city, country, state, postalCode,
          isPrimary, alias, userProfileId);
    }
  }

  private List<UserIdentification> buildIdentificationList(
      Map<UserIdentification.IdentificationType, Long> userIdentificationIds,
      Map<UserIdentification.IdentificationType, UserIdentification> identificationMap,
      long userProfileId) {
    if (identificationMap == null || identificationMap.isEmpty()) {
      return null;
    }
    List<UserIdentification> mapUserIdentificationList =
        new ArrayList<>(identificationMap.values());
    if (userProfileId <= 0) {
      return mapUserIdentificationList;
    }
    List<UserIdentification> validUserIdentificationList = new ArrayList<>();
    for (UserIdentification userIdentification : mapUserIdentificationList) {
      if (userIdentification != null) {
        long userIdentificationId = -1;
        if (userIdentificationIds.containsKey(userIdentification.getIdentificationType())) {
          userIdentificationId =
              userIdentificationIds.get(userIdentification.getIdentificationType());
        }
        UserIdentification newUserIdentification = new UserIdentification(-1, userIdentificationId,
            userIdentification.getIdentificationId(),
            userIdentification.getIdentificationCountryCode(),
            userIdentification.getIdentificationExpirationDate(),
            userIdentification.getIdentificationType(), userProfileId);
        validUserIdentificationList.add(newUserIdentification);
      }
    }
    return validUserIdentificationList;
  }

  public UserProfile buildUserProfile(long userProfileId, String gender, UserProfile.Title title,
      String name, String middleName, String surname, String secondLastName, long birthDate,
      String prefixPhoneNumber, String phoneNumber, String prefixAlternatePhoneNumber,
      String alternatePhoneNumber, String mobilePhoneNumber, String nationalityCountryCode,
      String cpf, boolean isDefault, String photoProfile, UserAddress userAddress,
      List<UserIdentification> userIdentificationList, long userTravellerId) {

    if (gender == null) {
      if (title == UserProfile.Title.MR) {
        gender = UserProfile.Gender.MALE.name();
      } else if (title == UserProfile.Title.MRS || title == UserProfile.Title.MS) {
        gender = UserProfile.Gender.FEMALE.name();
      }
    }

    return new UserProfile(-1, userProfileId, gender, title, name, middleName, surname,
        secondLastName, birthDate, prefixPhoneNumber, phoneNumber, prefixAlternatePhoneNumber,
        alternatePhoneNumber, mobilePhoneNumber, nationalityCountryCode, cpf, isDefault,
        photoProfile, userAddress, userIdentificationList, userTravellerId);
  }

  private List<UserFrequentFlyer> buildFrequentFlyerList(Map<String, Long> userFrequentFlyerIds,
      List<UserFrequentFlyer> frequentFlyerList, long userTravellerId) {
    List<UserFrequentFlyer> validUserFrequentFlyerList = new ArrayList<>();
    if (frequentFlyerList == null || frequentFlyerList.isEmpty()) {
      return validUserFrequentFlyerList;
    }
    for (UserFrequentFlyer userFrequentFlyer : frequentFlyerList) {
      if (userFrequentFlyer != null) {
        long userFrequentFlyerId = -1;
        if (userFrequentFlyerIds.containsKey(userFrequentFlyer.getAirlineCode())) {
          userFrequentFlyerId = userFrequentFlyerIds.get(userFrequentFlyer.getAirlineCode());
        }
        UserFrequentFlyer validUserFrequentFlyer =
            new UserFrequentFlyer(-1, userFrequentFlyerId, userFrequentFlyer.getAirlineCode(),
                userFrequentFlyer.getFrequentFlyerNumber(), userTravellerId);
        validUserFrequentFlyerList.add(validUserFrequentFlyer);
      }
    }
    return validUserFrequentFlyerList;
  }

  public UserTraveller buildUserTraveller(long userTravellerId, String name, String middleName,
      String surname, String secondLastName, UserTraveller.TypeOfTraveller typeOfTraveller,
      UserProfile.Title title, long birthDate, String nationalityCountryCode, boolean isBuyer,
      String email, String gender, boolean isDefault, String photoProfile, String mealType,
      String cpf, String state, String city, String address, String zipCode,
      String addressCountryCode, String addressType, boolean isAddressPrimary, String addressAlias,
      String phoneCode, String phone, String alternatePhoneCode, String alternatePhone,
      String mobilePhone,
      HashMap<UserIdentification.IdentificationType, UserIdentification> identificationMap,
      List<UserFrequentFlyer> frequentFlyerList, long userId) {

    UserTraveller userTraveller;
    long userProfileId = -1;
    long userAddressId = -1;
    Map<UserIdentification.IdentificationType, Long> userIdentificationIds = new HashMap<>();
    Map<String, Long> userFrequentFlyerIds = new HashMap<>();

    if (userTravellerId > 0) {
      userTraveller = mTravellersHandlerDB.getFullUserTraveller(userTravellerId);
      if (userTraveller != null) {
        UserProfile userProfile = userTraveller.getUserProfile();
        if (userProfile != null) {
          userProfileId = userProfile.getUserProfileId();
          if (userProfile.getUserAddress() != null) {
            userAddressId = userProfile.getUserAddress().getUserAddressId();
          }
          if (userProfile.getUserIdentificationList() != null) {
            for (UserIdentification userIdentification : userProfile.getUserIdentificationList()) {
              if (userIdentification != null) {
                userIdentificationIds.put(userIdentification.getIdentificationType(),
                    userIdentification.getId());
              }
            }
          }
        }
        if (userTraveller.getUserFrequentFlyers() != null) {
          for (UserFrequentFlyer userFrequentFlyer : userTraveller.getUserFrequentFlyers()) {
            if (userFrequentFlyer != null) {
              userFrequentFlyerIds.put(userFrequentFlyer.getAirlineCode(),
                  userFrequentFlyer.getFrequentFlyerId());
            }
          }
        }
      }
    }

    List<UserIdentification> userIdentificationList =
        buildIdentificationList(userIdentificationIds, identificationMap, userProfileId);
    UserAddress userAddress =
        buildUserAddress(userAddressId, address, addressType, city, addressCountryCode, state,
            zipCode, isAddressPrimary, addressAlias, userProfileId);
    UserProfile userProfile =
        buildUserProfile(userProfileId, gender, title, name, middleName, surname, secondLastName,
            birthDate, phoneCode, phone, alternatePhoneCode, alternatePhone, mobilePhone,
            nationalityCountryCode, cpf, isDefault, photoProfile, userAddress,
            userIdentificationList, userTravellerId);

    List<UserFrequentFlyer> validUserFrequentFlyerList =
        buildFrequentFlyerList(userFrequentFlyerIds, frequentFlyerList, userTravellerId);

    return new UserTraveller(-1, userTravellerId, isBuyer, email, typeOfTraveller, mealType,
        validUserFrequentFlyerList, userProfile, userId);
  }

  public void saveUserTraveller(long userTravellerId, String name, String middleName,
      String surname, String secondLastName, UserTraveller.TypeOfTraveller typeOfTraveller,
      UserProfile.Title title, long birthDate, String nationalityCountryCode, boolean isBuyer,
      String email, String gender, boolean isDefault, String photoProfile, String mealType,
      String cpf, String state, String city, String address, String zipCode,
      String addressCountryCode, String addressType, boolean isAddressPrimary, String addressAlias,
      String phoneCode, String phone, String alternatePhoneCode, String alternatePhone,
      String mobilePhone,
      HashMap<UserIdentification.IdentificationType, UserIdentification> identificationMap,
      List<UserFrequentFlyer> frequentFlyerList, long userId, OnUserUpdated listener) {

    UserTraveller userTraveller =
        buildUserTraveller(userTravellerId, name, middleName, surname, secondLastName,
            typeOfTraveller, title, birthDate, nationalityCountryCode, isBuyer, email, gender,
            isDefault, photoProfile, mealType, cpf, state, city, address, zipCode,
            addressCountryCode, addressType, isAddressPrimary, addressAlias, phoneCode, phone,
            alternatePhoneCode, alternatePhone, mobilePhone, identificationMap, frequentFlyerList,
            userId);

    saveUserTravellerList(new ArrayList(Arrays.asList(userTraveller)), listener);
  }

  private boolean storeUserTravellerWithoutUserLogged(UserTraveller userTraveller) {
    if (userTraveller != null) {
      if (userTraveller.getUserProfile() != null && userTraveller.getUserProfile()
          .isDefaultTraveller()) {
        setNoDefaultAllUserTraveller();
      }
      if (userTraveller.getUserTravellerId() < 1) {
        // Case: new traveller
        return (mUserCreateOrUpdateHandler.saveAUserTravellerAndAllComponentsInDBWithoutUserLogged(
            userTraveller) >= 1);
      } else {
        // case: updateTraveller
        return mUserCreateOrUpdateHandler.updateOrCreateAUserTravellerAndAllComponentsInDB(
            userTraveller);
      }
    }
    return false;
  }

  public void saveUserTravellerList(List<UserTraveller> userTravellers, OnUserUpdated listener) {
    boolean result = true;
    //Case 1: No logged User
    if (mSessionController.getCredentials() == null) {
      for (UserTraveller userTraveller : userTravellers) {
        result &= storeUserTravellerWithoutUserLogged(userTraveller);
      }
      // Check if User has got a default traveller
      if (result && !checkIfLocalUserHasDefaultTraveller()) {
        setNewUserTravellerDefaultWithoutUserLogged();
      }
      if (result) {
        listener.onUserUpdatedSuccessful();
      } else {
        listener.onUserUpdatedError();
      }
    } else {
      // Case 2: There is a user logged in the app
      User user;
      if (checkIfListHasDefaultTraveller(userTravellers)) {
        user = mTravellersHandlerDB.getCurrentUser(false);
      } else {
        user = mTravellersHandlerDB.getCurrentUser();
      }
      if (user.getUserTravellerList() == null) {
        user.setUserTravellerList(new ArrayList<UserTraveller>());
      }
      List<UserTraveller> userTravellerListStored = user.getUserTravellerList();
      for (UserTraveller newUserTraveller : userTravellers) {
        if (newUserTraveller.getUserTravellerId() > 0) {
          removeOldUserTraveller(newUserTraveller.getUserTravellerId(), userTravellerListStored);
        }
      }
      userTravellers.addAll(userTravellerListStored);
      // Check if new list has got a default traveller
      if (!checkIfListHasDefaultTraveller(userTravellers)) {
        setNewUserTravellerDefault(userTravellers);
      }
      user.setUserTravellerList(userTravellers);
      mUserNetController.updateUser(getOnRequestDataListener(listener), user);
    }
  }

  public boolean checkIfListHasDefaultTraveller(List<UserTraveller> userTravellerList) {
    for (UserTraveller userTraveller : userTravellerList) {
      if (userTraveller != null
          && userTraveller.getUserProfile() != null
          && userTraveller.getUserProfile().isDefaultTraveller()) {
        return true;
      }
    }
    return false;
  }

  private void removeOldUserTraveller(long userTravellerId, List<UserTraveller> userTravellerList) {
    if (userTravellerList != null) {
      for (UserTraveller userTraveller : userTravellerList) {
        if (userTraveller != null) {
          if (userTraveller.getUserTravellerId() == userTravellerId) {
            userTravellerList.remove(userTraveller);
            break;
          }
        }
      }
    }
  }

  private OnRequestDataListener<User> getOnRequestDataListener(final OnUserUpdated listener) {
    return new OnRequestDataListener<User>() {

      @Override public void onResponse(User user) {
        System.out.println("UPDATED in server, correct");
        List<UserTraveller> userTravellerList = user.getUserTravellerList();
        for (UserTraveller userTraveller : userTravellerList) {
          if (!mUserCreateOrUpdateHandler.updateOrCreateAUserTravellerAndAllComponentsInDB(
              userTraveller)) {
            listener.onUserUpdatedError();
            System.out.println("ERROR: when saved in DB");
            break;
          }
          if (userTraveller.getUserProfile().isDefaultTraveller()) {
            updateUserName(
                userTraveller.getUserProfile().getName() + " " + userTraveller.getUserProfile()
                    .getFirstLastName());
          }
        }
        listener.onUserUpdatedSuccessful();
        System.out.println("UPDATED and SAVED in DB, correct");
      }

      @Override public void onError(MslError error, String message) {
        if (error.equals(MslError.AUTH_000)) {
          listener.onUserAuthError();
        } else {
          listener.onUserUpdatedError();
          System.out.println("ERROR " + error + ": " + message);
        }
        //Check the error, if the credentials are invalid then call the listener...
        if (error.equals(MslError.AUTH_006)) {
          listener.onUserInvalidCredentialsError();
        }
      }
    };
  }

  public void deleteUserTraveller(final long userTravellerId, final OnUserUpdated listener) {
    if (mSessionController.getCredentials() == null) {
      // case: No logged User
      if (mUserCreateOrUpdateHandler.deleteUserTravellerCompletely(userTravellerId)) {
        if (!checkIfLocalUserHasDefaultTraveller()) {
          setNewUserTravellerDefaultWithoutUserLogged();
        }
        listener.onUserUpdatedSuccessful();
      } else {
        listener.onUserUpdatedError();
      }
    } else {
      // case: App has got a logged User
      User user = mTravellersHandlerDB.getCurrentUser();
      if (user != null) {
        final List<UserTraveller> userTravellerList = user.getUserTravellerList();
        if (userTravellerList != null) {
          boolean isNecessaryNewUserTravellerDefault = false;
          for (UserTraveller userTraveller : userTravellerList) {
            if (userTraveller.getUserTravellerId() == userTravellerId) {
              if (userTraveller.getUserProfile() != null && userTraveller.getUserProfile()
                  .isDefaultTraveller()) {
                isNecessaryNewUserTravellerDefault = true;
              }
              userTravellerList.remove(userTraveller);
              break;
            }
          }
          //Search another default traveller if it is necessary
          final UserTraveller newDefaultTraveller;
          if (isNecessaryNewUserTravellerDefault) {
            newDefaultTraveller = setNewUserTravellerDefault(userTravellerList);
          } else {
            newDefaultTraveller = null;
          }
          mUserNetController.updateUser(new OnRequestDataListener<User>() {
            @Override public void onResponse(User newUser) {
              if (mUserCreateOrUpdateHandler.deleteUserTravellerCompletely(userTravellerId)) {
                // If we have a new default traveller, we make a update in the database.
                if (newDefaultTraveller != null) {
                  mUserCreateOrUpdateHandler.updateOrCreateAUserTravellerAndAllComponentsInDB(
                      newDefaultTraveller);
                }
                listener.onUserUpdatedSuccessful();
              } else {
                listener.onUserUpdatedError();
              }
            }

            @Override public void onError(MslError error, String message) {
              if (error.equals(MslError.AUTH_000)) {
                listener.onUserAuthError();
              } else {
                listener.onUserUpdatedError();
              }
              System.err.print("ERROR - " + error + ": " + message);
            }
          }, user);
        }
      }
    }
  }

  /**
   * Set all User travellers not default
   */
  private void setNoDefaultAllUserTraveller() {
    List<UserTraveller> userTravellerList = mTravellersHandlerDB.getFullUserTravellerList();
    for (UserTraveller userTraveller : userTravellerList) {
      if (userTraveller != null
          && userTraveller.getUserProfile() != null
          && userTraveller.getUserProfile().isDefaultTraveller()) {
        UserProfile userProfile = userTraveller.getUserProfile();
        UserProfile noDefaultUserProfile =
            new UserProfile(userProfile.getId(), userProfile.getUserProfileId(),
                userProfile.getGender(), userProfile.getTitle(), userProfile.getName(),
                userProfile.getMiddleName(), userProfile.getFirstLastName(),
                userProfile.getSecondLastName(), userProfile.getBirthDate(),
                userProfile.getPrefixPhoneNumber(), userProfile.getPhoneNumber(),
                userProfile.getPrefixAlternatePhoneNumber(), userProfile.getAlternatePhoneNumber(),
                userProfile.getMobilePhoneNumber(), userProfile.getNationalityCountryCode(),
                userProfile.getCpf(), false, userProfile.getPhoto(), userProfile.getUserAddress(),
                userProfile.getUserIdentificationList(), userProfile.getUserTravellerId());

        UserTraveller noDefaultUserTraveller =
            new UserTraveller(userTraveller.getId(), userTraveller.getUserTravellerId(),
                userTraveller.getBuyer(), userTraveller.getEmail(),
                userTraveller.getTypeOfTraveller(), userTraveller.getMealType(),
                userTraveller.getUserFrequentFlyers(), noDefaultUserProfile,
                userTraveller.getUserId());
        mUserCreateOrUpdateHandler.updateOrCreateAUserTravellerAndAllComponentsInDB(
            noDefaultUserTraveller);
      }
    }
  }

  /**
   * Check if User in the app has got a Default UserTraveller
   *
   * @return TRUE if User has got a default UserTraveller, FALSE in other case
   */
  public boolean checkIfLocalUserHasDefaultTraveller() {
    return hasDefaultTraveller(mTravellersHandlerDB.getFullUserTravellerList());
  }

  /**
   * Check if the traveller list has got a default UserTraveller
   *
   * @param userTravellerList all UserTravellers of a User
   * @return TRUE if List has got a default UserTraveller
   */
  private boolean hasDefaultTraveller(List<UserTraveller> userTravellerList) {
    if (userTravellerList != null) {
      for (UserTraveller isDefaultUserTraveller : userTravellerList) {
        if (isDefaultUserTraveller != null
            && isDefaultUserTraveller.getUserProfile() != null
            && isDefaultUserTraveller.getUserProfile().isDefaultTraveller()) {
          return true;
        }
      }
    }
    return false;
  }

  private void setNewUserTravellerDefaultWithoutUserLogged() {
    List<UserTraveller> userTravellerList = mTravellersHandlerDB.getFullUserTravellerList();
    setNewUserTravellerDefault(userTravellerList);
    for (UserTraveller userTraveller : userTravellerList) {
      mUserCreateOrUpdateHandler.updateOrCreateAUserTravellerAndAllComponentsInDB(userTraveller);
    }
  }

  /**
   * If list has got a Adult UserTraveller, that is assigned as Default UserTraveller
   *
   * @param userTravellerList List of a UserTraveller
   */
  private UserTraveller setNewUserTravellerDefault(List<UserTraveller> userTravellerList) {
    if (userTravellerList != null && !userTravellerList.isEmpty()) {
      for (UserTraveller userTraveller : userTravellerList) {
        if (userTraveller != null
            && userTraveller.getTypeOfTraveller() == UserTraveller.TypeOfTraveller.ADULT) {
          userTravellerList.remove(userTraveller);
          UserProfile userProfile = userTraveller.getUserProfile();
          if (userProfile != null) {
            UserProfile defaultUserProfile =
                new UserProfile(userProfile.getId(), userProfile.getUserProfileId(),
                    userProfile.getGender(), userProfile.getTitle(), userProfile.getName(),
                    userProfile.getMiddleName(), userProfile.getFirstLastName(),
                    userProfile.getSecondLastName(), userProfile.getBirthDate(),
                    userProfile.getPrefixPhoneNumber(), userProfile.getPhoneNumber(),
                    userProfile.getPrefixAlternatePhoneNumber(),
                    userProfile.getAlternatePhoneNumber(), userProfile.getMobilePhoneNumber(),
                    userProfile.getNationalityCountryCode(), userProfile.getCpf(), true,
                    userProfile.getPhoto(), userProfile.getUserAddress(),
                    userProfile.getUserIdentificationList(), userProfile.getUserTravellerId());
            UserTraveller defaultUserTraveller =
                new UserTraveller(userTraveller.getId(), userTraveller.getUserTravellerId(),
                    userTraveller.getBuyer(), userTraveller.getEmail(),
                    userTraveller.getTypeOfTraveller(), userTraveller.getMealType(),
                    userTraveller.getUserFrequentFlyers(), defaultUserProfile,
                    userTraveller.getUserId());
            userTravellerList.add(0, defaultUserTraveller);
            updateUserName(userProfile.getName() + " " + userProfile.getFirstLastName());
            return defaultUserTraveller;
          }
        }
      }
    }
    updateUserName("");
    return null;
  }

  public void updateUserName(String name) {
    mSessionController.saveUserName(name);
  }

  public List<UserTraveller> getTravellersList() {
    List<UserTraveller> userTravellers = mTravellersHandlerDB.getSimpleUserTravellerList();
    if (!userTravellers.isEmpty()) {
      return mTravellersHandlerDB.getFullUserTravellerList(userTravellers);
    }
    return userTravellers;
  }

  public UserTraveller getFullUserTraveller(long userTravellerId) {
    return mTravellersHandlerDB.getFullUserTraveller(userTravellerId);
  }
}
