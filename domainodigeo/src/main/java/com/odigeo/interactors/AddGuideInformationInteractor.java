package com.odigeo.interactors;

import com.odigeo.data.db.dao.GuideDBDAOInterface;
import com.odigeo.data.db.dao.LocationBookingDBDAOInterface;
import com.odigeo.data.db.dao.SectionDBDAOInterface;
import com.odigeo.data.db.dao.SegmentDBDAOInterface;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Guide;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.net.controllers.ArrivalGuideNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import java.util.List;

public class AddGuideInformationInteractor {

  private SegmentDBDAOInterface mSegmentDBDAO;
  private SectionDBDAOInterface mSectionDBDAO;
  private LocationBookingDBDAOInterface mLocationBookingDBDAO;
  private GuideDBDAOInterface mGuideDBDAO;
  private ArrivalGuideNetControllerInterface mArrivalGuideNetController;

  public AddGuideInformationInteractor(SegmentDBDAOInterface segmentDBDAO,
      SectionDBDAOInterface sectionDBDAO, LocationBookingDBDAOInterface locationBookingDBDAO,
      GuideDBDAOInterface guideDBDAO,
      ArrivalGuideNetControllerInterface arrivalGuideNetController) {
    this.mSegmentDBDAO = segmentDBDAO;
    this.mSectionDBDAO = sectionDBDAO;
    this.mLocationBookingDBDAO = locationBookingDBDAO;
    this.mGuideDBDAO = guideDBDAO;
    this.mArrivalGuideNetController = arrivalGuideNetController;
  }

  public void addGuideInformationToBooking(Booking booking) {
    if (!booking.getTripType().equals(Booking.TRIP_TYPE_MULTI_SEGMENT)) {
      Segment segment = mSegmentDBDAO.getSegments(booking.getBookingId()).get(0);
      List<Section> sections = mSectionDBDAO.getSections(segment.getId());
      Section arrivalFlight = sections.get(sections.size() - 1);
      LocationBooking locationTo =
          mLocationBookingDBDAO.getLocationBooking(arrivalFlight.getTo().getGeoNodeId());
      if (!hasArrivalGuide(locationTo.getGeoNodeId())) {
        storeGuideInformation(locationTo.getCityIATACode(), locationTo.getGeoNodeId());
      }
    }
  }

  private boolean hasArrivalGuide(long geoNodeId) {
    return getArrivalGuide(geoNodeId) != null;
  }

  private Guide getArrivalGuide(long geoNodeId) {
    return mGuideDBDAO.getGuideByGeoNodeId(geoNodeId);
  }

  private void storeGuideInformation(String cityIataCode, long geoNodeId) {
    mArrivalGuideNetController.getGuideInformation(new OnRequestDataListener<Guide>() {
      @Override public void onResponse(Guide guide) {
        if (guide != null && !hasArrivalGuide(guide.getGeoNodeId())) {
          mGuideDBDAO.addGuide(guide);
        }
      }

      @Override public void onError(MslError error, String message) {
      }
    }, cityIataCode, geoNodeId);
  }
}
