package com.odigeo.interactors;

import com.odigeo.data.entity.CreditCardBinDetails;
import com.odigeo.data.net.controllers.BinCheckNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.presenter.listeners.OnCheckBinListener;
import java.util.HashMap;
import java.util.Map;

public class CheckBinFromNetworkInteractor {

  private final BinCheckNetControllerInterface mBinCheckController;

  private Map<String, CreditCardBinDetails> mBinCheckCache = new HashMap<>();

  public CheckBinFromNetworkInteractor(BinCheckNetControllerInterface binCheckNetController) {
    mBinCheckController = binCheckNetController;
  }

  public void checkBinMSL(final String creditCardNumber,
      final OnCheckBinListener onCheckBinListener) {
    if (isCreditCardCached(creditCardNumber)) {
      onCheckBinListener.onSuccessBinDetails(mBinCheckCache.get(creditCardNumber));
    } else {
      mBinCheckController.checkBin(new OnRequestDataListener<CreditCardBinDetails>() {
        @Override public void onResponse(CreditCardBinDetails creditCardBinDetails) {
          onCheckBinListener.onSuccessBinDetails(creditCardBinDetails);
          mBinCheckCache.put(creditCardNumber, creditCardBinDetails);
        }

        @Override public void onError(MslError error, String message) {
          onCheckBinListener.onError(error, message);
        }
      }, creditCardNumber);
    }
  }

  private boolean isCreditCardCached(String creditCardNumber) {
    return mBinCheckCache.get(creditCardNumber) != null;
  }

  protected Map<String, CreditCardBinDetails> getBinCheckCache() {
    return mBinCheckCache;
  }

  protected void setBinCheckCache(Map<String, CreditCardBinDetails> mBinCheckCache) {
    this.mBinCheckCache = mBinCheckCache;
  }
}
