package com.odigeo.interactors;

import com.odigeo.Notifier.EventsNotifier;
import com.odigeo.Notifier.event.LoadCarouselEvent;
import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.db.helper.BookingsHandlerInterface;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.BookingSummary;
import com.odigeo.data.entity.booking.BookingSummaryRequestItem;
import com.odigeo.data.net.controllers.BookingNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.interactors.managers.BookingStatusAlarmManagerInterface;
import com.odigeo.interactors.notification.OdigeoNotificationManagerInterface;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jetbrains.annotations.Nullable;

import static com.odigeo.data.entity.booking.Booking.BOOKING_STATUS_CONTRACT;
import static com.odigeo.data.entity.booking.Booking.BOOKING_STATUS_PENDING;
import static com.odigeo.data.entity.booking.Booking.BOOKING_STATUS_REJECTED;

public class UpdateBookingStatusInteractor {

  private final BookingDBDAOInterface mBookingDBDAO;
  private final BookingsHandlerInterface mBookingHandler;
  private final BookingNetControllerInterface mBookingNetController;
  private final CheckUserCredentialsInteractor checkUserCredentialsInteractor;
  private final OdigeoNotificationManagerInterface odigeoNotificationManager;
  private final BookingStatusAlarmManagerInterface bookingStatusAlarmManager;
  private final EventsNotifier eventsNotifier;

  public UpdateBookingStatusInteractor(BookingDBDAOInterface bookingDBDAO,
      BookingsHandlerInterface bookingHandler, BookingNetControllerInterface bookingNetController,
      OdigeoNotificationManagerInterface odigeoNotificationManager,
      CheckUserCredentialsInteractor checkUserCredentialsInteractor,
      BookingStatusAlarmManagerInterface bookingStatusAlarmManager, EventsNotifier eventsNotifier) {

    this.mBookingDBDAO = bookingDBDAO;
    this.mBookingHandler = bookingHandler;
    this.mBookingNetController = bookingNetController;
    this.odigeoNotificationManager = odigeoNotificationManager;
    this.checkUserCredentialsInteractor = checkUserCredentialsInteractor;
    this.bookingStatusAlarmManager = bookingStatusAlarmManager;
    this.eventsNotifier = eventsNotifier;
  }

  public void updateBookingStatus(BookingStatusListener listener) {
    launchUpdateBookingStatusEvent();
    List<Booking> pendingBookings = getPendingBookings();
    if (!pendingBookings.isEmpty()) {
      getBookingStatusOfAList(getBookingSummaryList(pendingBookings), false, listener);
    } else {
      listener.onComplete(null);
    }
  }

  public void updateBookingStatus(final Booking booking, final BookingStatusListener listener) {
    List<BookingSummaryRequestItem> bookings = new ArrayList<>();
    BookingSummaryRequestItem bookingSummaryRequestItem =
        new BookingSummaryRequestItem(checkUserCredentialsInteractor.getUserEmail());
    bookingSummaryRequestItem.addBookingId(booking.getBookingId());
    bookings.add(bookingSummaryRequestItem);

    mBookingNetController.getBookingStatus(new OnRequestDataListener<List<BookingSummary>>() {
      @Override public void onResponse(List<BookingSummary> updates) {
        if (updates.isEmpty()) {
          listener.onComplete(null);
        } else {
          final BookingSummary statusUpdate = updates.get(0);
          final String status = mapBookingSummaryStatus(statusUpdate.getStatus());
          boolean updated = mBookingDBDAO.updateStatus(booking.getBookingId(), status) > 0;
          listener.onComplete(updated ? mBookingDBDAO.getBooking(booking.getBookingId()) : null);
        }
      }

      @Override public void onError(MslError error, String message) {
        listener.onComplete(null);
      }
    }, bookings);
  }

  public void updateBookingStatus(boolean shouldSendLocalNotification) {
    launchUpdateBookingStatusEvent();
    List<Booking> pendingBookings = getPendingBookings();
    if (!pendingBookings.isEmpty()) {
      getBookingStatusOfAList(getBookingSummaryList(pendingBookings), shouldSendLocalNotification,
          null);
    }
  }

  private void launchUpdateBookingStatusEvent() {
    LoadCarouselEvent loadCarouselEvent = new LoadCarouselEvent();
    eventsNotifier.fireEvent(loadCarouselEvent);
  }

  private List<Booking> getPendingBookings() {
    List<Booking> bookingList = mBookingDBDAO.getPendingBookings();
    mBookingHandler.completeBookingData(bookingList);
    return bookingList;
  }

  private List<BookingSummaryRequestItem> getBookingSummaryList(List<Booking> pendingBookings) {

    Map<String, BookingSummaryRequestItem> bookingSummaryPerEmail = new HashMap<>();

    for (Booking booking : pendingBookings) {
      if (booking != null && booking.getBuyer() != null) {
        String email = booking.getBuyer().getEmail();
        BookingSummaryRequestItem item =
            bookingSummaryPerEmail.get(email) == null ? new BookingSummaryRequestItem(email)
                : bookingSummaryPerEmail.get(email);

        item.addBookingId(booking.getBookingId());
        bookingSummaryPerEmail.put(email, item);
      }
    }
    return new ArrayList<>(bookingSummaryPerEmail.values());
  }

  private void getBookingStatusOfAList(
      final List<BookingSummaryRequestItem> bookingSummaryListToRequest,
      final boolean shouldSendLocalNotification, final BookingStatusListener listener) {

    mBookingNetController.getBookingStatus(new OnRequestDataListener<List<BookingSummary>>() {
      @Override public void onResponse(List<BookingSummary> bookingSummaryListResponse) {
        updateBookingStatusFromList(bookingSummaryListResponse, shouldSendLocalNotification);
        if (listener != null) {
          listener.onComplete(null);
        }
      }

      @Override public void onError(MslError error, String message) {

      }
    }, bookingSummaryListToRequest);
  }

  private void updateBookingStatusFromList(List<BookingSummary> newBookingSummaryList,
      boolean shouldSendLocalNotification) {
    if (newBookingSummaryList != null) {
      for (BookingSummary bookingSummary : newBookingSummaryList) {
        String status = null;
        switch (bookingSummary.getStatus()) {
          case KO:
            status = Booking.BOOKING_STATUS_DIDNOTBUY;
            break;
          case PENDING:
            status = Booking.BOOKING_STATUS_PENDING;
            break;
          case OK:
            status = Booking.BOOKING_STATUS_CONTRACT;
            break;
        }
        long rows = mBookingDBDAO.updateStatus(bookingSummary.getId(), status);

        if (rows > 0) {
          Booking booking = mBookingDBDAO.getBooking(bookingSummary.getId());
          if (booking != null) {
            mBookingHandler.completeBooking(booking);
          }
          sendLocalNotification(booking, shouldSendLocalNotification);
          resetAlarmStatusIfNeeded();
        }
      }
    }
  }

  private void sendLocalNotification(Booking booking, boolean shouldSendLocalNotification) {
    if (shouldSendLocalNotification && bookingHasChangedStatus(booking)) {
      odigeoNotificationManager.sendBookingStatusNotification(booking);
    }
  }

  private void resetAlarmStatusIfNeeded() {
    List<Booking> pendingBookings = getPendingBookings();
    if (!pendingBookings.isEmpty()) {
      bookingStatusAlarmManager.updateAlarm();
    } else {
      bookingStatusAlarmManager.cancelAlarm();
    }
  }

  private boolean bookingHasChangedStatus(Booking booking) {
    return (booking.getBookingStatus().equals(Booking.BOOKING_STATUS_CONTRACT)
        || booking.getBookingStatus().equals(Booking.BOOKING_STATUS_DIDNOTBUY));
  }

  private static String mapBookingSummaryStatus(BookingSummary.Status status) {
    switch (status) {
      case OK:
        return BOOKING_STATUS_CONTRACT;
      case KO:
        return BOOKING_STATUS_REJECTED;
      default:
        return BOOKING_STATUS_PENDING;
    }
  }

  public interface BookingStatusListener {
    void onComplete(@Nullable Booking booking);
  }
}
