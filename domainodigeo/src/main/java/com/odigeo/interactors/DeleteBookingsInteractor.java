package com.odigeo.interactors;

import com.odigeo.data.db.dao.BaggageDBDAOInterface;
import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.db.dao.BuyerDBDAOInterface;
import com.odigeo.data.db.dao.InsuranceDBDAOInterface;
import com.odigeo.data.db.dao.SectionDBDAOInterface;
import com.odigeo.data.db.dao.SegmentDBDAOInterface;
import com.odigeo.data.db.dao.TravellerDBDAOInterface;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.presenter.listeners.OnRemoveBookingsListener;
import java.util.ArrayList;
import java.util.List;

public class DeleteBookingsInteractor {

  private final BookingDBDAOInterface mBookingDBDAOInterface;
  private final TravellerDBDAOInterface mTravellerDBDAOInterface;
  private final BuyerDBDAOInterface mBuyerDBDAOInterface;
  private final InsuranceDBDAOInterface mInsuranceDBDAOInterface;
  private final BaggageDBDAOInterface mBaggageDBDAOInterface;
  private final SegmentDBDAOInterface mSegmentDBDAOInterface;
  private final SectionDBDAOInterface mSectionDBDAOInterface;

  public DeleteBookingsInteractor(BookingDBDAOInterface mBookingDBDAOInterface,
      TravellerDBDAOInterface mTravellerDBDAOInterface, BuyerDBDAOInterface mBuyerDBDAOInterface,
      InsuranceDBDAOInterface mInsuranceDBDAOInterface,
      BaggageDBDAOInterface mBaggageDBDAOInterface, SegmentDBDAOInterface mSegmentDBDAOInterface,
      SectionDBDAOInterface mSectionDBDAOInterface) {
    this.mBookingDBDAOInterface = mBookingDBDAOInterface;
    this.mTravellerDBDAOInterface = mTravellerDBDAOInterface;
    this.mBuyerDBDAOInterface = mBuyerDBDAOInterface;
    this.mInsuranceDBDAOInterface = mInsuranceDBDAOInterface;
    this.mBaggageDBDAOInterface = mBaggageDBDAOInterface;
    this.mSegmentDBDAOInterface = mSegmentDBDAOInterface;
    this.mSectionDBDAOInterface = mSectionDBDAOInterface;
  }

  public void removeBookings(List<Booking> bookings, OnRemoveBookingsListener listener) {
    List<Long> removedBookings = deleteBookingsCompletely(bookings);
    if (removedBookings.size() == bookings.size()) {
      listener.onSuccess();
    } else {
      listener.onError(removedBookings);
    }
  }

  private List<Long> deleteBookingsCompletely(List<Booking> bookings) {
    boolean isSuccessTransaction;
    List<Long> removedBookingIds = new ArrayList<>();
    for (Booking booking : bookings) {
      isSuccessTransaction = true;
      mBookingDBDAOInterface.initTransaction();

      if (booking.getSegments() != null) {
        isSuccessTransaction = deleteSegmentsCompletely(booking);
      }
      if (booking.getTravellers() != null && isSuccessTransaction) {
        isSuccessTransaction = mTravellerDBDAOInterface.deleteTraveller(booking.getBookingId());
      }
      if (booking.getBuyer() != null && isSuccessTransaction) {
        isSuccessTransaction = mBuyerDBDAOInterface.deleteBuyer(booking.getBookingId());
      }
      if (booking.getInsurances() != null
          && !booking.getInsurances().isEmpty()
          && isSuccessTransaction) {
        isSuccessTransaction = mInsuranceDBDAOInterface.removeInsurance(booking.getBookingId());
      }
      if (isSuccessTransaction) {
        isSuccessTransaction = mBookingDBDAOInterface.deleteBooking(booking.getBookingId());
      }
      if (isSuccessTransaction) {
        mBookingDBDAOInterface.setTransactionSuccessful();
        removedBookingIds.add(booking.getBookingId());
      }

      mBookingDBDAOInterface.endTransaction();
    }

    return removedBookingIds;
  }

  private boolean deleteSegmentsCompletely(Booking booking) {
    for (Segment segment : booking.getSegments()) {
      if (segment.getBaggagesList() != null) {
        if (!mBaggageDBDAOInterface.deleteBaggage(segment.getId())) {
          return false;
        }
      }
      if (segment.getSectionsList() != null) {
        if (!deleteSectionCompletely(segment)) {
          return false;
        }
      }
    }
    return mSegmentDBDAOInterface.deleteSegments(booking.getBookingId());
  }

  private boolean deleteSectionCompletely(Segment segment) {
    return mSectionDBDAOInterface.deleteSection(segment.getId());
  }
}
