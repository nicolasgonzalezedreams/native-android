package com.odigeo.interactors;

import com.odigeo.data.db.dao.GuideDBDAOInterface;
import com.odigeo.data.entity.booking.Guide;
import com.odigeo.provider.FileProvider;
import java.io.File;

public class ArrivalGuidesCardsInteractor {

  private static final String GUIDE_EXTENSION = ".pdf";

  private final FileProvider fileProvider;
  private final GuideDBDAOInterface guideDBDAO;
  private Guide guide;

  public ArrivalGuidesCardsInteractor(FileProvider fileProvider, GuideDBDAOInterface guideDBDAO) {
    this.fileProvider = fileProvider;
    this.guideDBDAO = guideDBDAO;
  }

  public boolean hasArrivalGuide(long geoNodeId) {
    return guideDBDAO.getGuideByGeoNodeId(geoNodeId) != null;
  }

  public boolean isSameGuideLanguage(long geoNodeId, String language) {
    if (guide == null || guide.getGeoNodeId() != geoNodeId) {
      guide = guideDBDAO.getGuideByGeoNodeId(geoNodeId);
    }
    return guide.getLanguage().equals(language);
  }

  public String getGuideUrl(long geoNodeId) {
    if (guide == null || guide.getGeoNodeId() != geoNodeId) {
      guide = guideDBDAO.getGuideByGeoNodeId(geoNodeId);
    }
    return guide.getUrl();
  }

  public boolean isGuideDownloaded(String filename) {
    File pdfFile = new File(buildGuideFileName(filename));
    return pdfFile.exists();
  }

  public String getFilePath(String filename) {
    File pdfFile = new File(buildGuideFileName(filename));
    return pdfFile.getAbsolutePath();
  }

  public boolean deleteFile(String filename) {
    File pdfFile = new File(buildGuideFileName(filename));
    return pdfFile.delete();
  }

  private String buildGuideFileName(String filename) {
    return fileProvider.getExternalStorageDownloadDirectory().getAbsolutePath()
        + File.separator
        + filename
        + GUIDE_EXTENSION;
  }
}
