package com.odigeo.interactors;

import com.odigeo.data.net.controllers.UserNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.presenter.listeners.UserRequestForgottenPasswordListener;
import com.odigeo.tools.CheckerTool;

/**
 * Created by matias.dirusso on 21/08/2015.
 */
public class RequestForgottenPasswordInteractor {

  private UserNetControllerInterface mUserNetController;

  public RequestForgottenPasswordInteractor(UserNetControllerInterface userNetControllerInterface) {
    this.mUserNetController = userNetControllerInterface;
  }

  public void requestForgottenPassword(final UserRequestForgottenPasswordListener listener,
      final String email) {

    if (!CheckerTool.checkEmail(email)) {
      listener.onUserNameError(true);
    } else {
      mUserNetController.requestForgottenPassword(new OnRequestDataListener<Boolean>() {
        @Override public void onResponse(Boolean object) {
          if ((object)) {
            listener.onUserRequestForgottenPasswordOk();
          }
        }

        @Override public void onError(MslError error, String message) {
          if (error == MslError.AUTH_008) {
            listener.onUserRequestForgottenPasswordFailTryFacebookOrGoogle(email);
          } else if (error == MslError.AUTH_000) {
            listener.onUserAuthError();
          } else if (error == MslError.STA_011) {
            listener.onAccountInactive(email);
          } else if (error == MslError.FND_001 || error == MslError.FND_000) {
            listener.onUserDoesNotExist();
          } else {
            listener.onUserRequestForgottenPasswordFail();
          }
        }
      }, email);
    }
  }

  public boolean validateUsernameFormat(String username) {
    boolean isValid = false;
    if (CheckerTool.checkEmail(username)) {
      isValid = true;
    }
    return isValid;
  }
}
