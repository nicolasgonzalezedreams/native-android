package com.odigeo.interactors;

import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.db.helper.BookingsHandlerInterface;
import com.odigeo.data.net.controllers.BookingNetControllerInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import java.util.List;

public class UpdateBookingsFromLocalInteractor extends UpdateBookingsFromNetworkInteractor {

  public UpdateBookingsFromLocalInteractor(BookingNetControllerInterface bookingNetController,
      BookingDBDAOInterface bookingDBDAO, BookingsHandlerInterface bookingHandler,
      AddGuideInformationInteractor addGuideInformationInteractor,
      UpdateBookingStatusInteractor updateBookingStatusInteractor,
      PreferencesControllerInterface preferencesController) {

    super(bookingNetController, bookingDBDAO, bookingHandler, addGuideInformationInteractor,
        updateBookingStatusInteractor, preferencesController);
  }

  public void updateMyTrips(List<Long> bookingsId, OnRequestDataListener<Void> listener) {
    processBookingsId(bookingsId, listener);
  }
}
