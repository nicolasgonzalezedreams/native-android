package com.odigeo.interactors;

import com.odigeo.constants.PaymentMethodsKeys;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import java.util.List;
import org.jetbrains.annotations.Nullable;

import static com.odigeo.interactors.BinCheckRules.isAmericanExpress;
import static com.odigeo.interactors.BinCheckRules.isAmericanExpressOnShoppingCartCollectionOption;
import static com.odigeo.interactors.BinCheckRules.isDinnersClub;
import static com.odigeo.interactors.BinCheckRules.isDinnersClubOnShoppingCartCollectionOption;
import static com.odigeo.interactors.BinCheckRules.isEmptyCard;
import static com.odigeo.interactors.BinCheckRules.isJBC;
import static com.odigeo.interactors.BinCheckRules.isJBCOnShoppingCartCollectionOption;
import static com.odigeo.interactors.BinCheckRules.isMasterCardOnShoppingCartCollectionOption;
import static com.odigeo.interactors.BinCheckRules.isMastercard;
import static com.odigeo.interactors.BinCheckRules.isVisa;
import static com.odigeo.interactors.BinCheckRules.isVisaOnShoppingCartCollectionOption;

public class CheckBinFromLocalInteractor {

  public CheckBinFromLocalInteractor() {
    //nothing
  }

  @Nullable public String checkBinLocally(String creditCardNumber,
      List<ShoppingCartCollectionOption> shoppingCartCollectionOptions) {
    if (isVisa(creditCardNumber) && isVisaOnShoppingCartCollectionOption(
        shoppingCartCollectionOptions)) {
      return PaymentMethodsKeys.VI_LOCAL;
    } else if (isMastercard(creditCardNumber) && isMasterCardOnShoppingCartCollectionOption(
        shoppingCartCollectionOptions)) {
      return PaymentMethodsKeys.MD_LOCAL;
    } else if (isAmericanExpress(creditCardNumber)
        && isAmericanExpressOnShoppingCartCollectionOption(shoppingCartCollectionOptions)) {
      return PaymentMethodsKeys.AX_LOCAL;
    } else if (isJBC(creditCardNumber) && isJBCOnShoppingCartCollectionOption(
        shoppingCartCollectionOptions)) {
      return PaymentMethodsKeys.JC_LOCAL;
    } else if (isEmptyCard(creditCardNumber)) {
      return PaymentMethodsKeys.UNKNOWN;
    } else if (isDinnersClub(creditCardNumber) && isDinnersClubOnShoppingCartCollectionOption(
        shoppingCartCollectionOptions)) {
      return PaymentMethodsKeys.DC_LOCAL;
    } else {
      return PaymentMethodsKeys.UNKNOWN;
    }
  }
}