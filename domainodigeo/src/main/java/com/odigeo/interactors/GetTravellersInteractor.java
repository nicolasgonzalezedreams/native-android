package com.odigeo.interactors;

import com.odigeo.data.db.helper.TravellersHandlerInterface;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.List;

public class GetTravellersInteractor {

  TravellersHandlerInterface mTravellersHandlerDB;

  public GetTravellersInteractor(TravellersHandlerInterface travellersHandlerDB) {
    mTravellersHandlerDB = travellersHandlerDB;
  }

  public List<UserTraveller> getTravellersList() {
    List<UserTraveller> userTravellers = mTravellersHandlerDB.getSimpleUserTravellerList();
    if (!userTravellers.isEmpty()) {
      return mTravellersHandlerDB.getFullUserTravellerList(userTravellers);
    }
    return userTravellers;
  }
}
