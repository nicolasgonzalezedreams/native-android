package com.odigeo.interactors;

import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.db.dao.GuideDBDAOInterface;
import com.odigeo.data.db.dao.LocationBookingDBDAOInterface;
import com.odigeo.data.db.dao.SectionDBDAOInterface;
import com.odigeo.data.db.dao.SegmentDBDAOInterface;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Guide;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Created by matias.dirusso on 01/12/2015.
 */
public class DeleteArrivalGuideInformationInteractor {

  private GuideDBDAOInterface mGuideDBDAO;
  private BookingDBDAOInterface mBookingDBDAO;
  private SegmentDBDAOInterface mSegmentDBDAO;
  private SectionDBDAOInterface mSectionDBDAO;
  private LocationBookingDBDAOInterface mLocationBookingDBDAO;

  public DeleteArrivalGuideInformationInteractor(GuideDBDAOInterface guideDBDAO,
      BookingDBDAOInterface bookingDBDAO, LocationBookingDBDAOInterface locationBookingDBDAO,
      SegmentDBDAOInterface segmentDBDAO, SectionDBDAOInterface sectionDBDAO) {
    mGuideDBDAO = guideDBDAO;
    mBookingDBDAO = bookingDBDAO;
    mLocationBookingDBDAO = locationBookingDBDAO;
    mSegmentDBDAO = segmentDBDAO;
    mSectionDBDAO = sectionDBDAO;
  }

  public void deleteUnusedGuides(File directory, String packageName) {
    Map<Long, Guide> unusedGuides = getUnusedGuides(mGuideDBDAO.getAllGuides());
    for (Map.Entry<Long, Guide> entry : unusedGuides.entrySet()) {
      mGuideDBDAO.deleteGuide(entry.getKey());
      deleteGuideFromStorage(directory, packageName, entry.getKey());
    }
  }

  private Map<Long, Guide> getUnusedGuides(Map<Long, Guide> guidesStoredOnDatabase) {
    List<Booking> bookingList = mBookingDBDAO.getAllBookings();
    for (Booking booking : bookingList) {
      long geoNodeId = getDestination(booking).getGeoNodeId();
      if (guidesStoredOnDatabase.get(geoNodeId) != null) {
        guidesStoredOnDatabase.remove(geoNodeId);
      }
    }
    return guidesStoredOnDatabase;
  }

  private LocationBooking getDestination(Booking booking) {
    Segment segment = mSegmentDBDAO.getSegments(booking.getBookingId()).get(0);
    List<Section> sections = mSectionDBDAO.getSections(segment.getId());
    Section arrivalFlight = sections.get(sections.size() - 1);
    return arrivalFlight.getTo();
  }

  private boolean deleteGuideFromStorage(File directory, String packageName, long geoNodeId) {
    LocationBooking locationBooking = mLocationBookingDBDAO.getLocationBooking(geoNodeId);
    if (locationBooking != null) {
      File pdfFile = new File(directory.getAbsolutePath()
          + "/Android/data/"
          + packageName
          + "/files/Download/"
          + locationBooking.getCityIATACode()
          + ".pdf");
      return pdfFile.delete();
    }

    return false;
  }
}
