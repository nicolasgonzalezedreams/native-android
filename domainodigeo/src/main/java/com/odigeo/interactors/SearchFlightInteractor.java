package com.odigeo.interactors;

import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.net.controllers.SearchesNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;

/**
 * Created by Javier Marsicano on 26/07/16.
 */
public class SearchFlightInteractor {
  private SearchesNetControllerInterface mSearchesNetController;
  private SearchesHandlerInterface mSearchesHandler;
  private CheckUserCredentialsInteractor mUserSessionInteractor;

  public SearchFlightInteractor(SearchesNetControllerInterface mSearchesNetController,
      SearchesHandlerInterface searchesHandler,
      CheckUserCredentialsInteractor userSessionInteractor) {
    this.mSearchesNetController = mSearchesNetController;
    this.mSearchesHandler = searchesHandler;
    this.mUserSessionInteractor = userSessionInteractor;
  }

  public void addNewFlightSearch(final StoredSearch storedSearch) {
    if (mUserSessionInteractor.isUserLogin()) {
      mSearchesNetController.postUserSearch(new OnRequestDataListener<StoredSearch>() {
        @Override public void onResponse(StoredSearch newStoredSearch) {
          newStoredSearch.setSynchronized(true);
          mSearchesHandler.saveStoredSearch(newStoredSearch);
        }

        @Override public void onError(MslError error, String message) {
          mSearchesHandler.saveStoredSearch(storedSearch);
        }
      }, storedSearch);
    } else {
      mSearchesHandler.saveStoredSearch(storedSearch);
    }
  }

  public void updateFlightSearch(final StoredSearch storedSearch) {
    mSearchesHandler.updateStoredSearch(storedSearch);
  }
}
