package com.odigeo.interactors;

/**
 * Created by Matias on 11/8/2016.
 */

import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.net.controllers.SearchesNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import java.util.List;

public class RemoveHistorySearchesInteractor {
  private SearchesNetControllerInterface mSearchesNetController;
  private SearchesHandlerInterface mSearchesHandler;
  private CheckUserCredentialsInteractor mUserSessionInteractor;

  public RemoveHistorySearchesInteractor(SearchesNetControllerInterface searchesNetController,
      SearchesHandlerInterface searchesHandler,
      CheckUserCredentialsInteractor userSessionInteractor) {
    this.mSearchesNetController = searchesNetController;
    this.mSearchesHandler = searchesHandler;
    this.mUserSessionInteractor = userSessionInteractor;
  }

  public void removeAllSearches(final OnRequestDataListener<Boolean> listener,
      StoredSearch.TripType tripType) {
    if (mUserSessionInteractor.isUserLogin()) {
      List<StoredSearch> searches = mSearchesHandler.getCompleteSearchesFromDB();
      for (StoredSearch storedSearch : searches) {
        if (storedSearch.isSynchronized()) {
          if (storedSearch.getTripType().equals(tripType) || (tripType.equals(
              StoredSearch.TripType.O) && storedSearch.getTripType()
              .equals(StoredSearch.TripType.R))) {
            mSearchesNetController.deleteUserSearch(new OnRequestDataListener<Boolean>() {
              @Override

              public void onResponse(Boolean success) {
                if (success) {
                  listener.onResponse(true);
                }
              }

              @Override public void onError(MslError error, String message) {
                listener.onError(error, message);
              }
            }, storedSearch);
          }
        }
      }
    } else {
      listener.onResponse(false);
    }
    mSearchesHandler.removeAllSearchesLocally(tripType);
  }
}
