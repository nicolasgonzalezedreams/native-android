package com.odigeo.interactors;

import com.odigeo.data.net.controllers.UserNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.session.SessionController;

public class RemoveAccountInteractor {
  private UserNetControllerInterface userNetController;
  private SessionController sessionController;

  public RemoveAccountInteractor(UserNetControllerInterface userNetControllerInterface,
      SessionController sessionController) {
    this.userNetController = userNetControllerInterface;
    this.sessionController = sessionController;
  }

  public void removeAccount(final OnAuthRequestDataListener<Boolean> listener) {
    userNetController.deleteUser(new OnRequestDataListener<Boolean>() {
      @Override public void onResponse(Boolean object) {
        sessionController.removeAllData();
        listener.onResponse(object);
      }

      @Override public void onError(MslError error, String message) {
        if (error.equals(MslError.AUTH_000)) {
          listener.onAuthError();
        } else {
          listener.onError(error, message);
        }
      }
    });
  }
}
