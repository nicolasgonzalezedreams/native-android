package com.odigeo.interactors;

import com.odigeo.data.entity.userData.User;
import com.odigeo.data.net.controllers.UserNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.session.SessionController;

public class RegisterPasswordInteractor {

  private UserNetControllerInterface mUserNetController;
  private SessionController mSessionController;

  public RegisterPasswordInteractor(UserNetControllerInterface userNetController,
      SessionController sessionController) {
    mUserNetController = userNetController;
    mSessionController = sessionController;
  }

  public void registerUserPassword(final OnAuthRequestDataListener<User> listener,
      final String email, final String password, Boolean hasRegisterToNewsletter) {

    User userToRegister =
        new User(email, password, User.Source.REGISTERED, hasRegisterToNewsletter);
    mUserNetController.registerUser(new OnRequestDataListener<User>() {

      @Override public void onResponse(User user) {
        mSessionController.saveUserInfo(user.getUserId(), "", "", user.getEmail(),
            user.getStatus());
        listener.onResponse(user);
      }

      @Override public void onError(MslError error, String message) {
        if (error == MslError.AUTH_000) {
          listener.onAuthError();
        } else {
          listener.onError(error, message);
        }
      }
    }, userToRegister);
  }
}
