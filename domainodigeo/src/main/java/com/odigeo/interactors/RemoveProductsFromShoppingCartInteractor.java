package com.odigeo.interactors;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.request.ModifyShoppingCartRequest;
import com.odigeo.data.net.controllers.RemoveProductsNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.trustdefender.TrustDefenderController;
import com.odigeo.presenter.listeners.OnRemoveProductsFromShoppingCartListener;

import static com.odigeo.data.preferences.PreferencesControllerInterface.JSESSION_COOKIE;

public class RemoveProductsFromShoppingCartInteractor {

  private final RemoveProductsNetControllerInterface removeProductsNetController;
  private final TrustDefenderController trustDefenderController;
  private final PreferencesControllerInterface preferencesController;

  public RemoveProductsFromShoppingCartInteractor(
      RemoveProductsNetControllerInterface removeProductsNetController,
      TrustDefenderController trustDefenderController,
      PreferencesControllerInterface preferencesController) {
    this.removeProductsNetController = removeProductsNetController;
    this.trustDefenderController = trustDefenderController;
    this.preferencesController = preferencesController;
  }

  public void removeProducts(ModifyShoppingCartRequest modifyShoppingCartRequest,
      final OnRemoveProductsFromShoppingCartListener onRemoveProductsFromShoppingCartListener) {

    removeProductsNetController.removeProductsFromShoppingCart(
        new OnRequestDataListener<CreateShoppingCartResponse>() {
          @Override public void onResponse(CreateShoppingCartResponse createShoppingCartResponse) {
            if (createShoppingCartResponse.getShoppingCart() != null) {
              trustDefenderController.sendFingerPrint(
                  createShoppingCartResponse.getShoppingCart().getCyberSourceMerchantId(),
                  preferencesController.getStringValue(JSESSION_COOKIE));
            }

            onRemoveProductsFromShoppingCartListener.onSuccess(createShoppingCartResponse);
          }

          @Override public void onError(MslError error, String message) {
            onRemoveProductsFromShoppingCartListener.onError(error, message);
          }
        }, modifyShoppingCartRequest);
  }
}