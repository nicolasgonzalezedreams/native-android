package com.odigeo.interactors;

import com.odigeo.data.db.helper.TravellersHandlerInterface;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.presenter.listeners.OnUserTravellerRequestListener;
import java.util.List;

/**
 * Created by julio.kun on 9/21/2015.
 */
public class GetTravellersByTypeInteractor {

  TravellersHandlerInterface createUserTraveller;

  public GetTravellersByTypeInteractor(TravellersHandlerInterface createUserTraveller) {
    this.createUserTraveller = createUserTraveller;
  }

  public void getTravellers(UserTraveller.TypeOfTraveller typeOfTraveller,
      OnUserTravellerRequestListener listener) {
    List<UserTraveller> userTravellersOptions =
        createUserTraveller.getUserTravellersByType(typeOfTraveller);
    if (!userTravellersOptions.isEmpty()) {
      userTravellersOptions = createUserTraveller.getFullUserTravellerList(userTravellersOptions);
    }
    if (listener != null) {
      listener.onUserTravellersListByTypeAvailable(typeOfTraveller, userTravellersOptions);
    }
  }
}
