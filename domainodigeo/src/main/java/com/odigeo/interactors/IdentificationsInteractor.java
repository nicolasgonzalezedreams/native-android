package com.odigeo.interactors;

import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.tools.CheckerTool;

/**
 * Created by carlos.navarrete on 10/09/15.
 */
public class IdentificationsInteractor {

  public UserIdentification mUserIdentification;

  public boolean isEmpty(String string) {
    if (string != null && string.trim().length() > 0) {
      return false;
    } else {
      return true;
    }
  }

  public boolean validateIdCardLive(String idcard) {
    return CheckerTool.checkTravellerIDCardCharacters(idcard);
  }

  public boolean validatePassportLive(String passport) {
    return CheckerTool.checkTravellerPassport(passport);
  }

  public boolean validateDNILive(String passport) {
    return CheckerTool.checkTravellerDNICharacters(passport);
  }

  public boolean validateNIELive(String passport) {
    return CheckerTool.checkTravellerNIECharacters(passport);
  }
}
