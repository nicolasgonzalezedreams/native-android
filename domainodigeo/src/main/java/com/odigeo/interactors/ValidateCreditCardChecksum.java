package com.odigeo.interactors;

public class ValidateCreditCardChecksum {

  private static final int CREDIT_CARD_NUMBER_DIVIDER = 10;

  public static boolean usingLuhn(String cardNumber) {
    if (cardNumber == null) {
      return false;
    }
    int lastIndex = cardNumber.length() - 1;
    int luhn = 0;
    boolean isValid = false;
    try {
      for (int i = 0; i < cardNumber.length(); i++) {
        int doubled = Integer.parseInt(String.valueOf(cardNumber.charAt(lastIndex - i)));
        if (i % 2 == 1) {
          doubled *= 2;
        }
        String strDoubled = String.valueOf(doubled);
        if (strDoubled.length() > 1) {
          luhn = luhn + Integer.parseInt(String.valueOf(strDoubled.charAt(0))) + Integer.parseInt(
              String.valueOf(strDoubled.charAt(1)));
        } else {
          luhn += doubled;
        }
      }
      isValid = luhn % CREDIT_CARD_NUMBER_DIVIDER == 0;
    } catch (Exception e) {
      //nothing
    }
    return isValid;
  }
}
