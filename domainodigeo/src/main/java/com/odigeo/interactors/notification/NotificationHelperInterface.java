package com.odigeo.interactors.notification;

import com.odigeo.data.entity.booking.Booking;

public interface NotificationHelperInterface {

  void notifyBookingStatusChange(Booking booking);
}
