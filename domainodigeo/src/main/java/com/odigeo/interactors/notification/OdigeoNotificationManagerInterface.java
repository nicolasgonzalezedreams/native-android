package com.odigeo.interactors.notification;

import com.odigeo.data.entity.booking.Booking;

public interface OdigeoNotificationManagerInterface {

  void sendBookingStatusNotification(Booking booking);
}
