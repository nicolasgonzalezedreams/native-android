package com.odigeo.interactors;

import com.odigeo.data.db.helper.UserCreateOrUpdateHandlerInterface;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.List;

public class SaveTravellersInteractor {

  private final UserCreateOrUpdateHandlerInterface mUserCreateOrUpdateHandlerInterface;

  public SaveTravellersInteractor(
      UserCreateOrUpdateHandlerInterface userCreateOrUpdateHandlerInterface) {
    mUserCreateOrUpdateHandlerInterface = userCreateOrUpdateHandlerInterface;
  }

  public void saveUserTravellerList(List<UserTraveller> userTravellerList, boolean isLogged) {
    if (isLogged) {
      for (int i = 0; i < userTravellerList.size(); i++) {
        mUserCreateOrUpdateHandlerInterface.updateOrCreateAUserTravellerAndAllComponentsInDB(
            userTravellerList.get(i));
      }
    } else {
      for (int i = 0; i < userTravellerList.size(); i++) {
        UserTraveller userTraveller = userTravellerList.get(i);

        if (userTraveller != null) {

          if (userTraveller.getUserTravellerId() < 1) {
            // Case: new traveller
            mUserCreateOrUpdateHandlerInterface.saveAUserTravellerAndAllComponentsInDBWithoutUserLogged(
                userTraveller);
          } else {
            // case: updateTraveller
            mUserCreateOrUpdateHandlerInterface.updateOrCreateAUserTravellerAndAllComponentsInDB(
                userTraveller);
          }
        }
      }
    }
  }
}
