package com.odigeo.interactors;

import com.odigeo.data.db.helper.BookingsHandlerInterface;
import com.odigeo.data.db.helper.MembershipHandlerInterface;
import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.net.controllers.NotificationNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.session.SessionController;

public class LogoutInteractor {

  private SessionController sessionController;
  private NotificationNetControllerInterface mNotificationsNetController;
  private BookingsHandlerInterface mBookingsHandler;
  private SearchesHandlerInterface mSearchesHandler;
  private VisitUserInteractor visitUserInteractor;
  private MembershipHandlerInterface membershipHandler;

  public LogoutInteractor(SessionController sessionController,
      NotificationNetControllerInterface notificationsNetController,
      BookingsHandlerInterface bookingsHandler, SearchesHandlerInterface searchesHandler,
      VisitUserInteractor visitUserInteractor, MembershipHandlerInterface membershipHandler) {
    this.sessionController = sessionController;
    this.mNotificationsNetController = notificationsNetController;
    this.mSearchesHandler = searchesHandler;
    this.mBookingsHandler = bookingsHandler;
    this.visitUserInteractor = visitUserInteractor;
    this.membershipHandler = membershipHandler;
  }

  public void logout(final OnRequestDataListener<Boolean> listener) {
    String token = sessionController.getGcmToken();
    if (token != null) {
      mNotificationsNetController.disableNotifications(new OnAuthRequestDataListener<Boolean>() {
        @Override public void onAuthError() {
          localLogout();
          if (listener != null) {
            listener.onResponse(true);
          }
        }

        @Override public void onResponse(Boolean object) {
          localLogout();
          if (listener != null) {
            listener.onResponse(true);
          }
        }

        @Override public void onError(MslError error, String message) {
          boolean result = false;
          if (error == MslError.STA_000) {
            localLogout();
            result = true;
          }
          if (listener != null) {
            listener.onResponse(result);
          }
        }
      }, token);
    } else {
      localLogout();
      listener.onResponse(true);
    }
  }

  private void localLogout() {
    visitUserInteractor.logoutVisitUser();
    mSearchesHandler.removeSynchronizedStoredSearches();
    sessionController.removeAllData();
    mBookingsHandler.deleteAllBookingData();
    sessionController.attachCreditCardsToSession(null);
    membershipHandler.clearMembership();
  }
}
