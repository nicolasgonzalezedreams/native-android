package com.odigeo.interactors;

import com.odigeo.data.entity.geo.City;
import com.odigeo.data.net.controllers.AutoCompleteNetControllerInterface;
import com.odigeo.data.net.listener.OnRequestDataListListener;
import com.odigeo.interactors.provider.MarketProviderInterface;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 3/05/16
 */
public class AutoCompleteInteractor {

  private final AutoCompleteNetControllerInterface mAutoCompleteNetController;
  private final MarketProviderInterface mMarketProviderInterface;

  public AutoCompleteInteractor(AutoCompleteNetControllerInterface autoCompleteNetController,
      MarketProviderInterface marketProviderInterface) {
    this.mAutoCompleteNetController = autoCompleteNetController;
    this.mMarketProviderInterface = marketProviderInterface;
  }

  public void getDestination(String query,
      OnRequestDataListListener<City> onRequestDataListListener) {
    mAutoCompleteNetController.getDestination(query, mMarketProviderInterface.getLocale(),
        mMarketProviderInterface.getWebsite(), onRequestDataListListener);
  }
}
