package com.odigeo.interactors;

import com.odigeo.data.entity.userData.User;
import com.odigeo.data.net.controllers.UserNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.session.CredentialsInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.presenter.listeners.OnChangePasswordListener;
import com.odigeo.tools.CheckerTool;

public class ChangePasswordInteractor {

  UserNetControllerInterface userNetControllerInterface;
  SessionController sessionController;

  public ChangePasswordInteractor(UserNetControllerInterface userNetControllerInterface,
      SessionController sessionController) {
    this.userNetControllerInterface = userNetControllerInterface;
    this.sessionController = sessionController;
  }

  public void changePassword(String currentPassword, final String newPassword,
      String repeatPassword, final OnChangePasswordListener listener) {

    if (!isValidPassword(currentPassword) && !isSocialNetworkAccount()) {
      listener.onCurrentPasswordInvalidFormat();
    } else if (!validateCurrentPassword(currentPassword) && !isSocialNetworkAccount()) {
      listener.onCurrentPasswordWrong();
    } else if (!isValidPassword(newPassword)) {
      listener.onNewPasswordInvalidFormat();
    } else if (!isValidPassword(repeatPassword)) {
      listener.onRepeatPasswordInvalidFormat();
    } else if (!passwordMatch(newPassword, repeatPassword)) {
      listener.onPasswordDoesNotMatch();
    } else {
      userNetControllerInterface.resetPasswordUser(new OnRequestDataListener<User>() {
        @Override public void onResponse(User object) {
          sessionController.savePasswordCredentials(
              sessionController.getCredentials().getUser(), newPassword,
              CredentialsInterface.CredentialsType.PASSWORD);
          listener.onSuccess();
        }

        @Override public void onError(MslError error, String message) {
          if (error.equals(MslError.AUTH_000)) {
            listener.onAuthError();
          } else {
            listener.onError();
          }
        }
      }, newPassword);
    }
  }

  private boolean isValidPassword(String contentToValidate) {
    return !(contentToValidate == null || contentToValidate.isEmpty() || !CheckerTool.checkPassword(
        contentToValidate));
  }

  private boolean passwordMatch(String newPassword, String repeatPassword) {
    return newPassword.equals(repeatPassword);
  }

  private boolean validateCurrentPassword(String currentPassword) {
    return currentPassword.equals(sessionController.getCredentials().getPassword());
  }

  public boolean isSocialNetworkAccount() {
    return sessionController.getCredentials().getType()
        == CredentialsInterface.CredentialsType.FACEBOOK
        || sessionController.getCredentials().getType()
        == CredentialsInterface.CredentialsType.GOOGLE;
  }
}