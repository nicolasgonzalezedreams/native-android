package com.odigeo.interactors;

import com.odigeo.data.db.helper.MembershipHandlerInterface;
import com.odigeo.data.entity.userData.Membership;
import com.odigeo.interactors.provider.MarketProviderInterface;
import java.util.List;

public class MembershipInteractor {

  private MembershipHandlerInterface membershipHandlerInterface;
  private MarketProviderInterface marketProviderInterface;

  public MembershipInteractor(MembershipHandlerInterface membershipHandlerInterface,
      MarketProviderInterface marketProviderInterface) {

    this.membershipHandlerInterface = membershipHandlerInterface;
    this.marketProviderInterface = marketProviderInterface;
  }

  public List<Membership> getAllMemberships() {
    return membershipHandlerInterface.getAllMembershipsOfUser();
  }

  public Membership getMembershipForCurrentMarket() {
    String website = marketProviderInterface.getWebsite();
    return membershipHandlerInterface.getMembershipFromMarket(website);
  }

  public boolean clearMembership() {
    return membershipHandlerInterface.clearMembership();
  }

  public boolean isMemberForCurrentMarket() {
    String marketKey = marketProviderInterface.getMarketKey();
    Membership membership =
        membershipHandlerInterface.getMembershipFromMarket(marketKey.toUpperCase());
    return membership != null;
  }

  public void createMembershipSchemeIfNeeded() {
    membershipHandlerInterface.createTableIfNotExists();
  }

  public void addMembership(long memberId, String firstName, String lastName, String website) {
    Membership membership = new Membership();
    membership.setMemberId(memberId);
    membership.setFirstName(firstName);
    membership.setLastNames(lastName);
    membership.setWebsite(website);
    membershipHandlerInterface.addMembership(membership);
  }
}
