package com.odigeo.interactors;

import com.odigeo.data.db.helper.BookingsHandlerInterface;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.net.controllers.BookingNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.net.listener.OnSaveImportedTripListener;
import com.odigeo.interactors.managers.BookingStatusAlarmManagerInterface;
import com.odigeo.tools.CheckerTool;

import static com.odigeo.data.entity.booking.Booking.BOOKING_STATUS_PENDING;
import static com.odigeo.data.entity.booking.Booking.BOOKING_STATUS_REQUEST;

public class MyTripsManualImportInteractor {

  private BookingNetControllerInterface mBookingNetControllerInterface;
  private BookingsHandlerInterface mImportMyTripsHandlerInterface;
  private AddGuideInformationInteractor mAddGuideInformationInteractor;
  private BookingStatusAlarmManagerInterface mBookingStatusAlarmManager;

  public MyTripsManualImportInteractor(BookingNetControllerInterface bookingNetControllerInterface,
      BookingsHandlerInterface bookingsHandlerInterface,
      AddGuideInformationInteractor addGuideInformationInteractor,
      BookingStatusAlarmManagerInterface bookingStatusAlarmManager) {

    mBookingNetControllerInterface = bookingNetControllerInterface;
    mImportMyTripsHandlerInterface = bookingsHandlerInterface;
    mAddGuideInformationInteractor = addGuideInformationInteractor;
    mBookingStatusAlarmManager = bookingStatusAlarmManager;
  }

  public boolean validateEmailAndBookingIdFormat(String email, String bookingId) {
    return CheckerTool.checkEmail(email) && CheckerTool.checkBookingId(bookingId);
  }

  public boolean showErrorEmail(String email) {
    return !CheckerTool.checkEmail(email);
  }

  public boolean showErrorBookingId(String bookingId) {
    return !CheckerTool.checkBookingId(bookingId);
  }

  public void importTrip(final OnSaveImportedTripListener listener, String email, String id) {
    mBookingNetControllerInterface.getBookingByEmail(new OnRequestDataListener<Booking>() {
      @Override public void onResponse(Booking booking) {

        if (booking != null) {
          boolean saved = mImportMyTripsHandlerInterface.saveBooking(booking);
          if (saved) {
            updateBookingGuideInformation(booking);
            resetAlarmStatusIfNeeded(booking);
          }
          listener.onResponse(booking, !saved);
        } else {
          onError(MslError.UNK_001, "Booking null");
        }
      }

      @Override public void onError(MslError error, String message) {
        listener.onError(error, message);
      }
    }, email, id);
  }

  private void updateBookingGuideInformation(Booking bookingToUpdateGuide) {
    mAddGuideInformationInteractor.addGuideInformationToBooking(bookingToUpdateGuide);
  }

  private void resetAlarmStatusIfNeeded(Booking booking) {
    if (BOOKING_STATUS_REQUEST.equals(booking.getBookingStatus()) || BOOKING_STATUS_PENDING.equals(
        booking.getBookingStatus())) {
      mBookingStatusAlarmManager.updateAlarm();
    }
  }
}
