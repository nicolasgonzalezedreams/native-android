package com.odigeo.interactors;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.request.PersonalInfoRequest;
import com.odigeo.data.net.controllers.AddPassengerNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.trustdefender.TrustDefenderController;
import com.odigeo.presenter.listeners.OnAddPassengersToShoppingCartListener;

import static com.odigeo.data.preferences.PreferencesControllerInterface.JSESSION_COOKIE;

public class AddPassengerToShoppingCartInteractor {

  private final AddPassengerNetControllerInterface addPassengerNetController;
  private final TrustDefenderController trustDefenderController;
  private final PreferencesControllerInterface preferencesController;

  public AddPassengerToShoppingCartInteractor(
      AddPassengerNetControllerInterface addPassengerNetController, TrustDefenderController
      trustDefenderController, PreferencesControllerInterface preferencesController) {
    this.addPassengerNetController = addPassengerNetController;
    this.trustDefenderController = trustDefenderController;
    this.preferencesController = preferencesController;
  }

  public void addAddPassenger(PersonalInfoRequest personalInfoRequest,
      final OnAddPassengersToShoppingCartListener onAddPassengersToShoppingCartListener) {

    addPassengerNetController.addPassengerToShoppingCart(
        new OnRequestDataListener<CreateShoppingCartResponse>() {
          @Override public void onResponse(CreateShoppingCartResponse createShoppingCartResponse) {
            if (createShoppingCartResponse.getShoppingCart() != null) {
              trustDefenderController.sendFingerPrint(
                  createShoppingCartResponse.getShoppingCart().getCyberSourceMerchantId(),
                  preferencesController.getStringValue(JSESSION_COOKIE));
            }

            onAddPassengersToShoppingCartListener.onSuccess(createShoppingCartResponse);
          }

          @Override public void onError(MslError error, String message) {
            onAddPassengersToShoppingCartListener.onError(error, message);
          }
        }, personalInfoRequest);
  }
}
