package com.odigeo.interactors.managers;

public interface BookingStatusAlarmManagerInterface {

  void initializeAlarm();

  void updateAlarm();

  void cancelAlarm();
}
