package com.odigeo.interactors;

import com.odigeo.data.entity.shoppingCart.TravellerType;
import java.util.Calendar;
import java.util.Date;

public class CheckUserBirthdayInteractor {

  private static final int MAX_AGE_INFANT = 2;
  private static final int MAX_AGE_CHILD = 12;
  private static final int MAX_AGE_ADULT = 100;
  private static final int MIN_AGE_EIGHTEEN_ADULT = 18;

  public CheckUserBirthdayInteractor() {
  }

  public Date getMinValidDate(TravellerType userTravellerType) {
    if (TravellerType.INFANT.equals(userTravellerType)) {
      return getMinBabyDate();
    } else if (TravellerType.CHILD.equals(userTravellerType)) {
      return getMinChildDate();
    } else if (TravellerType.ADULT.equals(userTravellerType)) {
      return getMinAdultDate();
    }
    return null;
  }

  public Date getMinValidDate(TravellerType userTravellerType, long date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(date);
    if (TravellerType.INFANT.equals(userTravellerType)) {
      calendar.add(Calendar.YEAR, -MAX_AGE_INFANT);
    } else if (TravellerType.CHILD.equals(userTravellerType)) {
      calendar.add(Calendar.YEAR, -MAX_AGE_CHILD);
    } else if (TravellerType.ADULT.equals(userTravellerType)) {
      calendar.setTime(getMinAdultDate());
    }
    return calendar.getTime();
  }

  public Date getMaxValidDate(TravellerType userTravellerType) {
    if (TravellerType.INFANT.equals(userTravellerType)) {
      return getMaxBabyDate();
    } else if (TravellerType.CHILD.equals(userTravellerType)) {
      return getMaxChildDate();
    } else if (TravellerType.ADULT.equals(userTravellerType)) {
      return getMaxAdultDate();
    }
    return null;
  }

  public Date getMaxBabyDate() {
    return Calendar.getInstance().getTime();
  }

  public Date getMinBabyDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.YEAR, -MAX_AGE_INFANT);
    calendar.add(Calendar.DATE, 1);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    return calendar.getTime();
  }

  public Date getMaxChildDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.YEAR, -MAX_AGE_INFANT);
    calendar.set(Calendar.HOUR_OF_DAY, 23);
    calendar.set(Calendar.MINUTE, 59);
    calendar.set(Calendar.SECOND, 59);
    calendar.set(Calendar.MILLISECOND, 59);
    return calendar.getTime();
  }

  public Date getMinChildDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.YEAR, -MAX_AGE_CHILD);
    calendar.add(Calendar.DATE, 1);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    return calendar.getTime();
  }

  public Date getMaxAdultDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.YEAR, -MAX_AGE_CHILD);
    calendar.set(Calendar.HOUR_OF_DAY, 23);
    calendar.set(Calendar.MINUTE, 59);
    calendar.set(Calendar.SECOND, 59);
    calendar.set(Calendar.MILLISECOND, 59);
    return calendar.getTime();
  }

  public Date getMinAdultDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.YEAR, -MAX_AGE_ADULT);
    calendar.add(Calendar.HOUR_OF_DAY, 0);
    calendar.add(Calendar.MINUTE, 0);
    calendar.add(Calendar.SECOND, 0);
    calendar.add(Calendar.MILLISECOND, 0);
    return calendar.getTime();
  }

  public Date getMinAdultDateEighteen() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.YEAR, -MIN_AGE_EIGHTEEN_ADULT);
    calendar.add(Calendar.HOUR_OF_DAY, 0);
    calendar.add(Calendar.MINUTE, 0);
    calendar.add(Calendar.SECOND, 0);
    calendar.add(Calendar.MILLISECOND, 0);
    return calendar.getTime();
  }
}
