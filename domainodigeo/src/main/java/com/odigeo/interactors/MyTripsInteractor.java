package com.odigeo.interactors;

import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.db.helper.BookingsHandlerInterface;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.interactors.provider.MyTripsProvider;
import com.odigeo.tools.DateHelperInterface;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MyTripsInteractor {

  private static final int TIME_TO_SHOW_BOOKING_AFTER_LANDING = 30;

  private final BookingDBDAOInterface mBookingDBDAOInterface;
  private final BookingsHandlerInterface mBookingsHandlerInterface;
  private final DateHelperInterface mDateHelperInterface;
  private final MyTripsProvider mTripsProvider;

  public MyTripsInteractor(BookingDBDAOInterface bookingDBDAOInterface,
      BookingsHandlerInterface bookingsHandlerInterface, DateHelperInterface dateHelperInterface) {

    this.mBookingDBDAOInterface = bookingDBDAOInterface;
    this.mBookingsHandlerInterface = bookingsHandlerInterface;
    this.mDateHelperInterface = dateHelperInterface;
    this.mTripsProvider = new MyTripsProvider();
  }

  /**
   * Retrieves Synchronously the next booking stored on the database.
   *
   * @return The next booking on the database.
   */
  public Booking getNextBooking() {
    List<Booking> bookingList = mBookingDBDAOInterface.getAllBookings();
    mBookingsHandlerInterface.completeBookingData(bookingList);

    long currentDate = (new Date()).getTime();

    Booking nearest = null;

    Segment firstSegment;
    Segment lastSegment;

    for (Booking booking : bookingList) {
      firstSegment = booking.getFirstSegment();
      lastSegment = booking.getLastSegment();

      if (isValidBooking(booking)) {
        if (firstSegment != null && ((transformToGMT(firstSegment.getDepartureDate()) > currentDate)
            || isPassengerOnFlight(firstSegment, currentDate))) {
          nearest = getNearestSegment(firstSegment, nearest, booking, currentDate);
        } else if (lastSegment != null && ((transformToGMT(lastSegment.getDepartureDate())
            >= currentDate) || isPassengerOnFlight(lastSegment, currentDate))) {
          // At least the first flight of the trip has already taken place, but not the last!
          // Same flight picking mechanic as in previous block
          nearest = getNearestSegment(lastSegment, nearest, booking, currentDate);
        }
      }
    }
    return nearest;
  }

  private boolean isPassengerOnFlight(Segment segment, long currentDate) {
    long landingOffset = TimeUnit.MINUTES.toMillis(TIME_TO_SHOW_BOOKING_AFTER_LANDING);
    long landingTime = transformToGMT(segment.getArrivalDate()) + landingOffset;
    return ((currentDate > transformToGMT(segment.getDepartureDate())) && (currentDate
        < landingTime));
  }

  private Booking getNearestSegment(Segment segment, Booking nearest, Booking booking,
      long currentDate) {
    // If no trip has been yet picked as 'nearest', then currently cycled element is designed as such
    if (nearest == null) {
      return booking;
    } else {
      // If the date on the segment of the trip is earlier than the date on the FIRST flight of the trip of
      // the currently selected searchOption then this trip is before the nearest... If the first flight of the
      //currently selected already took place, then we compare to the LAST
      if (transformToGMT(nearest.getFirstSegment().getDepartureDate()) > currentDate) {
        if (transformToGMT(segment.getDepartureDate()) < transformToGMT(
            nearest.getFirstSegment().getDepartureDate())) {
          return booking;
        }
      } else {
        if (transformToGMT(segment.getDepartureDate()) < transformToGMT(
            nearest.getLastSegment().getDepartureDate())) {
          return booking;
        }
      }
    }
    return nearest;
  }

  private boolean isValidBooking(Booking booking) {
    if ((booking.getBookingStatus().equals(Booking.BOOKING_STATUS_CONTRACT))
        || ((booking.getBookingStatus().equals(Booking.BOOKING_STATUS_PENDING)))) {
      return true;
    }
    return false;
  }

  public void getBookings(final OnRequestDataListener<MyTripsProvider> listenerLocal) {
    new Thread(new Runnable() {
      @Override public void run() {

        List<Booking> bookingLocalList = mBookingDBDAOInterface.getAllBookings();

        List<Booking> bookingList = new ArrayList<>();
        Booking bookingToAdd;

        for (int i = 0; i < bookingLocalList.size(); i++) {
          bookingToAdd = checkBookingStatus(bookingLocalList.get(i));
          bookingList.add(bookingToAdd);
        }

        mTripsProvider.clear();
        mTripsProvider.addAllBookings(bookingList);
        mBookingsHandlerInterface.completeBookingData(mTripsProvider);

        listenerLocal.onResponse(mTripsProvider);
      }
    }).start();
  }

  private Booking checkBookingStatus(Booking booking) {
    for (int i = 0; i < mTripsProvider.getCurrentSize(); ++i) {
      if (booking != null && mTripsProvider.get(i) != null) {
        if (booking.getBookingId() == mTripsProvider.get(i).getBookingId()
            && !booking.getBookingStatus()
            .equalsIgnoreCase(mTripsProvider.get(i).getBookingStatus())) {
          booking.setStateChanged(true);
          return booking;
        }
      }
    }

    return booking;
  }

  private long transformToGMT(long date) {
    return mDateHelperInterface.millisecondsLeastOffset(date);
  }

  public List<Booking> getActiveBookings() {
    List<Booking> activeBookings = new ArrayList<>();
    List<Booking> bookings = mBookingDBDAOInterface.getAllBookings();

    for (Booking booking : bookings) {
      if (booking.isActive()) {
        activeBookings.add(booking);
      }
    }

    return activeBookings;
  }

  public boolean hasActiveBookings() {
    return getActiveBookings().size() > 0;
  }
}
