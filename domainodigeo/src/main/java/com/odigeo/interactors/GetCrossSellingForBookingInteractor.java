package com.odigeo.interactors;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.xsell.CrossSelling;
import com.odigeo.data.repositories.CrossSellingRepository;
import com.odigeo.presenter.model.CrossSellingType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GetCrossSellingForBookingInteractor {

  private final CrossSellingRepository crossSellingRepository;

  public GetCrossSellingForBookingInteractor(CrossSellingRepository crossSellingRepository) {
    this.crossSellingRepository = crossSellingRepository;
  }

  public List<CrossSelling> getCrossSellingForBooking(Booking booking) {
    if (booking.isPastBooking()) {
      return Collections.emptyList();
    }

    List<CrossSelling> crossSellings = crossSellingRepository.getCrossSellingForBooking(booking);
    List<CrossSelling> filteredCrossSellings = new ArrayList<>();

    for (CrossSelling crossSelling : crossSellings) {
      switch (crossSelling.getType()) {
        case CrossSellingType.TYPE_GUIDE:
          checkCrossSellingGuideAvailability(booking, filteredCrossSellings, crossSelling);
          break;
        case CrossSellingType.TYPE_GROUND:
          checkCrossSellingGroundTransportationAvailability(filteredCrossSellings, crossSelling);
          break;
        default:
          filteredCrossSellings.add(crossSelling);
      }
    }

    return filteredCrossSellings;
  }

  private void checkCrossSellingGroundTransportationAvailability(
      List<CrossSelling> filteredCrossSellings, CrossSelling crossSelling) {
    boolean shouldShowGroundTransportation =
        crossSellingRepository.shouldShowGroundTransportation();
    if (shouldShowGroundTransportation) {
      filteredCrossSellings.add(crossSelling);
    }
  }

  private void checkCrossSellingGuideAvailability(Booking booking,
      List<CrossSelling> filteredCrossSellings, CrossSelling crossSelling) {
    if (!booking.getTripType().equals(Booking.TRIP_TYPE_MULTI_SEGMENT)) {
      boolean hasArrivalGuide = crossSellingRepository.getCrossSellingArrivalGuide(booking);
      if (hasArrivalGuide) {
        filteredCrossSellings.add(crossSelling);
      }
    }
  }
}
