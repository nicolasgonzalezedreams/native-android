package com.odigeo.interactors;

import com.odigeo.data.net.controllers.VisitUserNetController;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.dataodigeo.session.SessionController;

public class VisitUserInteractor {

  private VisitUserNetController visitUserNetController;
  private SessionController sessionController;
  private CrashlyticsController crashlyticsController;

  public VisitUserInteractor(VisitUserNetController visitUserNetController,
      SessionController sessionController, CrashlyticsController crashlyticsController) {

    this.visitUserNetController = visitUserNetController;
    this.sessionController = sessionController;
    this.crashlyticsController = crashlyticsController;
  }

  public void loginVisitUser() {
    if (sessionController.getCredentials() != null) {
      visitUserNetController.loginVisitUser(new OnRequestDataListener<String>() {
        @Override public void onResponse(String object) {

        }

        @Override public void onError(MslError error, String message) {
          crashlyticsController.trackNonFatal(
              new Exception(CrashlyticsController.VISIT_USER_EXCEPTION,
                  new Throwable(CrashlyticsController.VISIT_USER_EXCEPTION)));
          crashlyticsController.setString(CrashlyticsController.VISIT_USER_METHOD,
              CrashlyticsController.PUT_METHOD);
          crashlyticsController.setLong(CrashlyticsController.VISIT_USER_ID,
              sessionController.getUserInfo().getUserId());
        }
      }, sessionController.getUserInfo().getUserId());
    }
  }

  void logoutVisitUser() {
    visitUserNetController.logoutVisitUser(new OnRequestDataListener<String>() {
      @Override public void onResponse(String object) {

      }

      @Override public void onError(MslError error, String message) {
        crashlyticsController.trackNonFatal(
            new Exception(CrashlyticsController.VISIT_USER_EXCEPTION,
                new Throwable(CrashlyticsController.VISIT_USER_EXCEPTION)));
        crashlyticsController.setString(CrashlyticsController.VISIT_USER_METHOD,
            CrashlyticsController.VISIT_USER_METHOD_DELETE);
        crashlyticsController.setLong(CrashlyticsController.VISIT_USER_ID,
            sessionController.getUserInfo().getUserId());
      }
    }, sessionController.getUserInfo().getUserId());
  }
}
