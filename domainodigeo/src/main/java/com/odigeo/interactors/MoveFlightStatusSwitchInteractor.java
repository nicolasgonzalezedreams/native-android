package com.odigeo.interactors;

import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.data.net.controllers.NotificationNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.provider.MyTripsProvider;
import com.odigeo.presenter.listeners.FlightStatusSwitchListener;

/**
 * Created by Matias on 9/16/2016.
 */
public class MoveFlightStatusSwitchInteractor {

  private NotificationNetControllerInterface mNotificationNetController;
  private SessionController mSessionController;
  private CheckUserCredentialsInteractor mUserCredentialsInteractor;
  private MyTripsInteractor mMyTripsInteractor;
  private LocalizableProvider localizableProvider;

  public MoveFlightStatusSwitchInteractor(
      NotificationNetControllerInterface notificationNetController,
      SessionController sessionController,
      CheckUserCredentialsInteractor userCredentialsInteractor, MyTripsInteractor myTripsInteractor,
      LocalizableProvider localizableProvider) {
    mNotificationNetController = notificationNetController;
    mSessionController = sessionController;
    mUserCredentialsInteractor = userCredentialsInteractor;
    mMyTripsInteractor = myTripsInteractor;
    this.localizableProvider = localizableProvider;
  }

  public void turnOffNotifications(final OnRequestDataListener<Boolean> listener) {
    String token = mSessionController.getGcmToken();
    if (mUserCredentialsInteractor.isUserLogin() && token != null) {
      mNotificationNetController.disableNotifications(new OnAuthRequestDataListener<Boolean>() {
        @Override public void onAuthError() {
          if (listener != null) {
            listener.onResponse(false);
          }
        }

        @Override public void onResponse(Boolean object) {
          if (listener != null) {
            listener.onResponse(true);
          }
        }

        @Override public void onError(MslError error, String message) {
          if (listener != null) {
            listener.onError(error, message);
          }
        }
      }, token);
    }
  }

  public void turnOnNotifications(final OnAuthRequestDataListener<Boolean> listener) {
    String GCMToken = mSessionController.getGcmToken();
    if (mUserCredentialsInteractor.isUserLogin() && !GCMToken.equals("")) {
      mNotificationNetController.enableNotifications(new OnRequestDataListener<Boolean>() {
        @Override public void onResponse(Boolean object) {
          if (listener != null) {
            listener.onResponse(true);
          }
        }

        @Override public void onError(MslError error, String message) {
          mSessionController.invalidateGcmToken();
          if (listener != null) {
            if (error.equals(MslError.AUTH_000)) {
              listener.onAuthError();
            } else {
              listener.onError(error, message);
            }
          }
        }
      }, GCMToken);
    }
  }

  public void handleFlightStatusSwitchVisibility(final FlightStatusSwitchListener listener,
      final String controlKey) {
    final boolean[] hasTrips = { false };
    final boolean isUserLoggedIn = mUserCredentialsInteractor.isUserLogin();
    final boolean isControlKeyEmpty = localizableProvider.getString(controlKey).isEmpty();

    mMyTripsInteractor.getBookings(new OnRequestDataListener<MyTripsProvider>() {
      @Override public void onResponse(MyTripsProvider object) {
        hasTrips[0] = object.getBookingList().size() > 0;
        if (!isControlKeyEmpty && isUserLoggedIn && hasTrips[0]) {
          listener.showFlightStatusSwitch();
        }
      }

      @Override public void onError(MslError error, String message) {

      }
    });
  }
}
