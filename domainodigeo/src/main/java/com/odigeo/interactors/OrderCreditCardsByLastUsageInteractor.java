package com.odigeo.interactors;

import com.odigeo.data.entity.userData.CreditCard;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OrderCreditCardsByLastUsageInteractor {

  public List<CreditCard> run(List<CreditCard> savedCreditCards) {
    List<CreditCard> savedCreditCardsNeverUsed = new ArrayList<>();
    List<CreditCard> savedCreditCardsUsed = new ArrayList<>();
    List<CreditCard> savedCreditCardsOrdered = new ArrayList<>();

    for (CreditCard savedCreditCard : savedCreditCards) {
      if (savedCreditCard.getLastUsageDate() == 0) {
        savedCreditCardsNeverUsed.add(savedCreditCard);
      } else {
        savedCreditCardsUsed.add(savedCreditCard);
      }
    }

    Collections.sort(savedCreditCardsUsed, creditCardComparator());
    savedCreditCardsOrdered.addAll(savedCreditCardsUsed);
    savedCreditCardsOrdered.addAll(savedCreditCardsNeverUsed);

    return savedCreditCardsOrdered;
  }

  private Comparator<CreditCard> creditCardComparator() {
    return new Comparator<CreditCard>() {
      @Override public int compare(CreditCard c1, CreditCard c2) {
        if (c1.getLastUsageDate() > c2.getLastUsageDate()) return 0;
        return 1;
      }
    };
  }
}
