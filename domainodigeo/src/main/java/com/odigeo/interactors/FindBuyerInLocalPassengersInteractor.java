package com.odigeo.interactors;

import com.odigeo.data.entity.userData.UserTraveller;
import java.util.List;

public class FindBuyerInLocalPassengersInteractor {

  private static int FIRST_ADULT_BUYER = 1;
  private static int EMPTY_ADULT_BUYER = 0;
  private List<UserTraveller> mUserTravellers;
  private int mBuyerPosition, mDefaultPassengerPosition, mNextPassengerPosition;

  public FindBuyerInLocalPassengersInteractor() {
  }

  public int findBuyerInLocalPassengers(List<UserTraveller> userTravellers) {
    mUserTravellers = userTravellers;
    if (findBuyer()) {
      return mBuyerPosition;
    } else if (findDefaultPassenger()) {
      return mDefaultPassengerPosition;
    } else {
      findNextPassengerPosition();
      return mNextPassengerPosition;
    }
  }

  private boolean findBuyer() {
    for (int i = 0; i < mUserTravellers.size(); i++) {
      if (mUserTravellers.get(i).getBuyer()) {
        mBuyerPosition = i;
        return true;
      }
    }
    return false;
  }

  private boolean findDefaultPassenger() {
    for (int i = 0; i < mUserTravellers.size(); i++) {
      if (mUserTravellers.get(i).getUserProfile().isDefaultTraveller()) {
        mDefaultPassengerPosition = i;
        return true;
      }
    }
    return false;
  }

  private void findNextPassengerPosition() {
    if (isThereLocalData()) {
      mNextPassengerPosition = FIRST_ADULT_BUYER;
    } else {
      mNextPassengerPosition = EMPTY_ADULT_BUYER;
    }
  }

  private boolean isThereLocalData() {
    return (mUserTravellers.size() > 1);
  }
}
