package com.odigeo.interactors;

import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.db.helper.BookingsHandlerInterface;
import com.odigeo.data.entity.booking.Booking;

public class BookingInteractor {
  private BookingDBDAOInterface mBookingDBDAOInterface;
  private BookingsHandlerInterface mBookingsHandlerInterface;

  public BookingInteractor(BookingDBDAOInterface bookingDBDAOInterface,
      BookingsHandlerInterface bookingsHandlerInterface) {
    mBookingDBDAOInterface = bookingDBDAOInterface;
    mBookingsHandlerInterface = bookingsHandlerInterface;
  }

  public Booking getBookingById(long id) {
    Booking booking = mBookingDBDAOInterface.getBooking(id);
    if (booking != null) {
      mBookingsHandlerInterface.completeBooking(booking);
    }
    return booking;
  }
}
