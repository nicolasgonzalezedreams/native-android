package com.odigeo.interactors;

import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.entity.userData.User;
import com.odigeo.dataodigeo.session.CredentialsInterface;
import com.odigeo.dataodigeo.session.SessionController;

public class CheckUserCredentialsInteractor {

  private SessionController mSessionController;
  private BookingDBDAOInterface dbdaoInterface;

  public CheckUserCredentialsInteractor(SessionController sessionController,
      BookingDBDAOInterface dbdaoInterface) {
    mSessionController = sessionController;
    this.dbdaoInterface = dbdaoInterface;
  }

  public boolean isUserLogin() {
    return mSessionController.getCredentials() != null;
  }

  public String getUserEmail() {
    return mSessionController.getCredentials().getUser();
  }

  public boolean isUserLoggedByUserPassword() {
    return mSessionController.getCredentials()
        .getType()
        .equals(CredentialsInterface.CredentialsType.PASSWORD);
  }

  public String getUserName() {
    return mSessionController.getUserInfo().getName();
  }

  public String getUrlUserImageProfile() {
    return mSessionController.getUserInfo().getProfilePicture();
  }

  public boolean isUserInActive() {
    User.Status userStatus = mSessionController.getUserInfo().getUserStatus();
    return userStatus != null && userStatus == User.Status.PENDING_MAIL_CONFIRMATION;
  }

  public String getInactiveEmail() {
    return mSessionController.getUserInfo().getEmail();
  }

  public void removeAllData() {
    mSessionController.removeAllData();
    dbdaoInterface.closeDatabase();
  }
}
