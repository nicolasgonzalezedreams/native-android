package com.odigeo.interactors;

import com.odigeo.comparators.ItinerarySectionComparator;
import com.odigeo.data.entity.shoppingCart.Itinerary;
import com.odigeo.data.entity.shoppingCart.SectionResult;
import java.util.Collections;
import java.util.List;

public class SortItinerarySectionsInteractor {

  public SortItinerarySectionsInteractor() {

  }

  public Itinerary sort(Itinerary itinerary) {
    List<SectionResult> sectionResults = itinerary.getLegend().getSectionResults();
    Collections.sort(sectionResults, new ItinerarySectionComparator());
    return itinerary;
  }
}
