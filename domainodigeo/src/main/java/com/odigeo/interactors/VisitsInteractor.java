package com.odigeo.interactors;

import com.odigeo.data.entity.session.Visit;
import com.odigeo.data.entity.session.request.VisitRequest;
import com.odigeo.data.net.controllers.VisitsNetController;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.presenter.listeners.VisitInteractorListener;
import java.util.Map;

public class VisitsInteractor {

  private VisitsNetController visitsNetController;
  private PreferencesControllerInterface preferencesController;

  public VisitsInteractor(VisitsNetController visitsNetController,
      PreferencesControllerInterface preferencesController) {
    this.visitsNetController = visitsNetController;
    this.preferencesController = preferencesController;
  }

  public void fetch(final VisitInteractorListener visitInteractorListener) {
    VisitRequest visitRequest = new VisitRequest();
      visitsNetController.getVisits(new OnRequestDataListener<Visit>() {
        @Override public void onResponse(Visit visit) {
          if (visit != null) {
            saveTestAssigment(visit);
            saveDimensions(visit);
            visitInteractorListener.onSuccess();
          }
        }

        @Override public void onError(MslError error, String message) {

        }
      }, visitRequest);
  }

  private void saveDimensions(Visit visit) {
    if (visit.getDimensions() != null) {
      preferencesController.clearDimensions();
      for (Map.Entry<String, Integer> entry : visit.getDimensions().entrySet()) {
        int value = (entry.getValue() != null) ? entry.getValue() : 0;
        preferencesController.saveDimensions(entry.getKey(), value);
      }
    }
  }

  private void saveTestAssigment(Visit visit) {
    if (visit.getTestAssignments() != null) {
      preferencesController.clearTestAssignments();
      for (Map.Entry<String, Integer> entry : visit.getTestAssignments().entrySet()) {
        int value = (entry.getValue() != null) ? entry.getValue() : 0;
        preferencesController.saveTestAssignment(entry.getKey(), value);
      }
    }
  }
}
