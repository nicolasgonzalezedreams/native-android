package com.odigeo.interactors;

import com.odigeo.data.preferences.PackageController;
import com.odigeo.data.preferences.PreferencesControllerInterface;

public class UpdateAppVersionCacheInteractor {

  private final String LAST_APP_VERSION = "LAST_APP_VERSION";

  private PreferencesControllerInterface preferencesController;
  private PackageController packageController;

  public UpdateAppVersionCacheInteractor(PreferencesControllerInterface preferencesController,
      PackageController packageController) {
    this.preferencesController = preferencesController;
    this.packageController = packageController;
  }

  public void updateLastAppVersion() {
    preferencesController.saveIntValue(LAST_APP_VERSION,
        packageController.getApplicationVersionCode());
  }

  public boolean isLastVersion() {
    return preferencesController.getIntValue(LAST_APP_VERSION)
        == packageController.getApplicationVersionCode();
  }
}