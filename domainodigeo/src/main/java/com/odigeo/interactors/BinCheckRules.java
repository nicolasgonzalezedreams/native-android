package com.odigeo.interactors;

import com.odigeo.constants.PaymentMethodsKeys;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import java.util.List;

final class BinCheckRules {

  static boolean isVisa(String creditCardNumber) {
    return ((creditCardNumber.length() >= 1) && (creditCardNumber.charAt(0) == '4'));
  }

  static boolean isVisaOnShoppingCartCollectionOption(
      List<ShoppingCartCollectionOption> shoppingCartCollectionOptions) {
    for (ShoppingCartCollectionOption shoppingCartCollectionOption : shoppingCartCollectionOptions) {
      if (shoppingCartCollectionOption.getMethod().getCreditCardType() != null && (
          shoppingCartCollectionOption.getMethod()
              .getCreditCardType()
              .getCode()
              .equals(PaymentMethodsKeys.VI)
              || shoppingCartCollectionOption.getMethod()
              .getCreditCardType()
              .getCode()
              .equals(PaymentMethodsKeys.VD))) {
        return true;
      }
    }
    return false;
  }

  static boolean isMastercard(String creditCardNumber) {
    return ((creditCardNumber.length() >= 1) && (creditCardNumber.charAt(0) == '5'));
  }

  static boolean isMasterCardOnShoppingCartCollectionOption(
      List<ShoppingCartCollectionOption> shoppingCartCollectionOptions) {
    for (ShoppingCartCollectionOption shoppingCartCollectionOption : shoppingCartCollectionOptions) {
      if (shoppingCartCollectionOption.getMethod().getCreditCardType() != null && (
          shoppingCartCollectionOption.getMethod()
              .getCreditCardType()
              .getCode()
              .equals(PaymentMethodsKeys.CA)
              || shoppingCartCollectionOption.getMethod()
              .getCreditCardType()
              .getCode()
              .equals(PaymentMethodsKeys.MD))) {
        return true;
      }
    }
    return false;
  }

  static boolean isAmericanExpress(String creditCardNumber) {
    return ((creditCardNumber.length() >= 2) && (((creditCardNumber.charAt(0) == '3') && (
        creditCardNumber.charAt(1)
            == '4')) || (((creditCardNumber.charAt(0) == '3') && (creditCardNumber.charAt(1)
        == '7')))));
  }

  static boolean isAmericanExpressOnShoppingCartCollectionOption(
      List<ShoppingCartCollectionOption> shoppingCartCollectionOptions) {
    for (ShoppingCartCollectionOption shoppingCartCollectionOption : shoppingCartCollectionOptions) {
      if (shoppingCartCollectionOption.getMethod().getCreditCardType() != null
          && shoppingCartCollectionOption.getMethod()
          .getCreditCardType()
          .getCode()
          .equals(PaymentMethodsKeys.AX)) {
        return true;
      }
    }
    return false;
  }

  static boolean isJBC(String creditCardNumber) {
    return ((creditCardNumber.length() >= 2) && ((creditCardNumber.charAt(0) == '3') && (
        creditCardNumber.charAt(1)
            == '5')));
  }

  static boolean isJBCOnShoppingCartCollectionOption(
      List<ShoppingCartCollectionOption> shoppingCartCollectionOptions) {
    for (ShoppingCartCollectionOption shoppingCartCollectionOption : shoppingCartCollectionOptions) {
      if (shoppingCartCollectionOption.getMethod().getCreditCardType() != null
          && shoppingCartCollectionOption.getMethod()
          .getCreditCardType()
          .getCode()
          .equals(PaymentMethodsKeys.JC)) {
        return true;
      }
    }
    return false;
  }

  static boolean isEmptyCard(String creditCardNumber) {
    return ((creditCardNumber != null) && (creditCardNumber.isEmpty()));
  }

  static boolean isDinnersClub(String creditCardNumber) {
    int creditCardDigitsInt;
    String creditCardFirstDigits;

    if (creditCardNumber.length() >= 3) {
      creditCardFirstDigits = creditCardNumber.substring(0, 3);
      try {
        creditCardDigitsInt = Integer.parseInt(creditCardFirstDigits);
      } catch (NumberFormatException e) {
        return false;
      }
      return (((creditCardDigitsInt >= 300) && (creditCardDigitsInt <= 305))
          || creditCardDigitsInt == 309);
    } else if (creditCardNumber.length() >= 2) {
      creditCardFirstDigits = creditCardNumber.substring(0, 2);
      try {
        creditCardDigitsInt = Integer.parseInt(creditCardFirstDigits);
      } catch (NumberFormatException e) {
        return false;
      }
      return ((creditCardDigitsInt == 36) || (creditCardDigitsInt == 38) || (creditCardDigitsInt
          == 39));
    }
    return false;
  }

  static boolean isDinnersClubOnShoppingCartCollectionOption(
      List<ShoppingCartCollectionOption> shoppingCartCollectionOptions) {
    for (ShoppingCartCollectionOption shoppingCartCollectionOption : shoppingCartCollectionOptions) {
      if (shoppingCartCollectionOption.getMethod().getCreditCardType() != null
          && shoppingCartCollectionOption.getMethod()
          .getCreditCardType()
          .getCode()
          .equals(PaymentMethodsKeys.DC)) {
        return true;
      }
    }
    return false;
  }
}


