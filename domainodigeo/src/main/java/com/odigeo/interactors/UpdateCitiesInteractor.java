package com.odigeo.interactors;

import com.odigeo.data.db.helper.CitiesHandlerInterface;
import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.entity.booking.SearchSegment;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.net.controllers.AutoCompleteNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.interactors.provider.MarketProviderInterface;
import java.util.ArrayList;
import java.util.List;

public class UpdateCitiesInteractor {

  private AutoCompleteNetControllerInterface autoCompleteNetControllerInterface;
  private CitiesHandlerInterface citiesHandlerInterface;
  private MarketProviderInterface marketProviderInterface;
  private SearchesHandlerInterface searchesHandlerInterface;

  private OnRequestDataListener<Void> listener;
  private int remainingCityRequests = 0;

  private List<String> cities;

  private OnRequestDataListListener<City> onRequestDataListener =
      new OnRequestDataListListener<City>() {
        @Override public void onResponse(List<City> list) {
          --remainingCityRequests;
          City correctCity = getCorrectCityByIata(list);
          if (correctCity != null) {
            updateCityOnDB(correctCity);
          }

          if (listener != null && remainingCityRequests == 0) {
            listener.onResponse(null);
          }
        }

        @Override public void onError(MslError error, String message) {
          --remainingCityRequests;
          if (listener != null) {
            listener.onError(error, message);
          }
        }
      };

  public UpdateCitiesInteractor(
      AutoCompleteNetControllerInterface autoCompleteNetControllerInterface,
      CitiesHandlerInterface citiesHandlerInterface,
      MarketProviderInterface marketProviderInterface,
      SearchesHandlerInterface searchesHandlerInterface) {
    this.autoCompleteNetControllerInterface = autoCompleteNetControllerInterface;
    this.citiesHandlerInterface = citiesHandlerInterface;
    this.marketProviderInterface = marketProviderInterface;
    this.searchesHandlerInterface = searchesHandlerInterface;
    remainingCityRequests = 0;
  }

  public void updateCities() {
    cities = new ArrayList<>();
    List<StoredSearch> storedSearches = searchesHandlerInterface.getCompleteSearchesFromDB();
    List<String> currentIATAsToSearch = new ArrayList<>();

    for (StoredSearch storedSearch : storedSearches) {
      for (SearchSegment segment : storedSearch.getSegmentList()) {
        currentIATAsToSearch.add(segment.getDestinationIATACode());
        currentIATAsToSearch.add(segment.getOriginIATACode());
      }
    }

    for (String iata : currentIATAsToSearch) {
      City city = citiesHandlerInterface.getCity(iata);
      if (city == null) {
        ++remainingCityRequests;
        cities.add(iata);
        getCityFromNet(iata);
      }
    }
  }

  public void updateCities(OnRequestDataListener<Void> listener) {
    this.listener = listener;
    updateCities();
  }

  private void getCityFromNet(String iata) {
    autoCompleteNetControllerInterface.getDestination(iata, marketProviderInterface.getLocale(),
        marketProviderInterface.getWebsite(), onRequestDataListener);
  }

  private City getCorrectCityByIata(List<City> citiesList) {
    for (City city : citiesList) {
      if (cities.contains(city.getIataCode())) {
        return city;
      }
    }

    return null;
  }

  private void updateCityOnDB(City correctCity) {
    citiesHandlerInterface.storeCity(correctCity);
  }
}
