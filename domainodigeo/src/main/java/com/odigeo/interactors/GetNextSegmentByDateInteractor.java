package com.odigeo.interactors;

import com.odigeo.data.entity.booking.Segment;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GetNextSegmentByDateInteractor {

  private static int TIME_TO_SHOW_SEGMENT_AFTER_LANDING = 30;

  public GetNextSegmentByDateInteractor() {
  }

  public Segment getNextValidSegment(List<Segment> segments) {
    long currentTime = System.currentTimeMillis();
    long offsetAfterLanding = TimeUnit.MINUTES.toMillis(TIME_TO_SHOW_SEGMENT_AFTER_LANDING);
    long lastSectionArrivalTime;

    for (Segment segment : segments) {
      lastSectionArrivalTime = segment.getLastSection().getArrivalDate() + offsetAfterLanding;
      if (lastSectionArrivalTime > currentTime) {
        return segment;
      }
    }
    return null;
  }
}
