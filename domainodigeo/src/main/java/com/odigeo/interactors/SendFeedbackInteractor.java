package com.odigeo.interactors;

import com.odigeo.data.entity.AttachmentItem;
import com.odigeo.data.net.controllers.SendMailNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.interactors.provider.MarketProviderInterface;
import java.util.ArrayList;
import java.util.List;

public class SendFeedbackInteractor {

  private static final String SEPARATOR_BETWEEN_MARKET_AND_SUBJECT = " - ";
  private static final String SCREENSHOT_NAME = "screenshot_";
  private static final String SCREENSHOT_EXTENSION = ".png";

  SendMailNetControllerInterface mSendMailNetController;
  MarketProviderInterface mMarketProvider;

  public SendFeedbackInteractor(SendMailNetControllerInterface sendMailNetController,
      MarketProviderInterface marketProvider) {
    mSendMailNetController = sendMailNetController;
    mMarketProvider = marketProvider;
  }

  public void sendFeedback(final OnRequestDataListener<Boolean> listener, String email,
      String subject, String message, List<String> screenshots) {

    mSendMailNetController.sendMail(new OnRequestDataListener<Boolean>() {
                                      @Override public void onResponse(Boolean object) {
                                        listener.onResponse(true);
                                      }

                                      @Override public void onError(MslError error, String message) {
                                        listener.onError(error, message);
                                      }
                                    }, email, mMarketProvider.getFeedbackEmail(),
        mMarketProvider.getBrand() + SEPARATOR_BETWEEN_MARKET_AND_SUBJECT + subject, message,
        getAttachments(screenshots));
  }

  private List<AttachmentItem> getAttachments(List<String> screenshots) {
    List<AttachmentItem> attachments = new ArrayList<>();
    int screenshotIndex = 0;
    for (String screenshot : screenshots) {
      screenshotIndex++;
      AttachmentItem attachment =
          new AttachmentItem(SCREENSHOT_NAME + screenshotIndex + SCREENSHOT_EXTENSION, screenshot);
      attachments.add(attachment);
    }
    return attachments;
  }
}
