package com.odigeo.interactors;

import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.data.entity.shoppingCart.MessageResponse;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.shoppingCart.request.BookingRequest;
import com.odigeo.data.net.controllers.ConfirmBookingNetControllerInterface;
import com.odigeo.data.net.error.DAPIError;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.presenter.listeners.OnConfirmBookingListener;
import java.util.ArrayList;
import java.util.List;

public class ConfirmBookingInteractor {

  private ConfirmBookingNetControllerInterface confirmBookingNetController;

  public ConfirmBookingInteractor(
      ConfirmBookingNetControllerInterface confirmBookingNetControllerInterface) {
    this.confirmBookingNetController = confirmBookingNetControllerInterface;
  }

  public void confirmBooking(BookingRequest bookingRequest,
      final OnConfirmBookingListener onConfirmBookingListener) {
    confirmBookingNetController.confirmBooking(new OnRequestDataListener<BookingResponse>() {
      @Override public void onResponse(BookingResponse bookingResponse) {
        if (hasToShowErrors(bookingResponse.getError())) {
          onConfirmBookingListener.onGeneralError(bookingResponse);
        } else if (hasInternalErrors(bookingResponse.getMessages())) {
          onConfirmBookingListener.onInternalError(bookingResponse);
        } else {
          if (bookingResponse.getCollectionOptions() != null) {
            bookingResponse.setCollectionOptions(
                checkAllowedPaymentMethods(bookingResponse.getCollectionOptions()));
          }
          onConfirmBookingListener.onSuccess(bookingResponse);
        }
      }

      @Override public void onError(MslError error, String message) {
        onConfirmBookingListener.onNoConnectionError();
      }
    }, bookingRequest);
  }

  private List<ShoppingCartCollectionOption> checkAllowedPaymentMethods(
      List<ShoppingCartCollectionOption> collectionOptions) {
    List<ShoppingCartCollectionOption> allowedCollectionOptions = new ArrayList<>();

    for (ShoppingCartCollectionOption shoppingCartCollectionOption : collectionOptions) {
      if (shoppingCartCollectionOption.getMethod().getType() != null) {
        allowedCollectionOptions.add(shoppingCartCollectionOption);
      }
    }
    return allowedCollectionOptions;
  }

  private boolean hasToShowErrors(com.odigeo.data.entity.shoppingCart.MslError error) {
    return error != null;
  }

  private boolean hasInternalErrors(List<MessageResponse> messages) {
    if (messages != null && !messages.isEmpty()) {
      for (MessageResponse message : messages) {
        if (message.getCode().equalsIgnoreCase(DAPIError.INT_001.getDapiError())
            || message.getCode().equalsIgnoreCase(DAPIError.INT_002.getDapiError())
            || message.getCode().equalsIgnoreCase(DAPIError.INT_003.getDapiError())) {
          return true;
        }
      }
    }
    return false;
  }
}