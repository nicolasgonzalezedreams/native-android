package com.odigeo.interactors;

import com.odigeo.data.db.helper.UserCreateOrUpdateHandlerInterface;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.data.net.controllers.UserNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.dataodigeo.session.CredentialsInterface.CredentialsType;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.presenter.listeners.OnUserUpdated;
import org.jetbrains.annotations.NotNull;

import static com.odigeo.data.entity.userData.User.Status.UNREGISTERED;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.FACEBOOK_SOURCE;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.GOOGLE_SOURCE;

public class LoginInteractor {

  public static final String PREFERENCE_STATUS_FLIGHTS_NOTIFICATIONS = "flights_notifications";
  private UserNetControllerInterface mUserNetController;
  private SessionController mSessionController;
  private UserCreateOrUpdateHandlerInterface mUserCreateOrUpdateHandler;
  private TravellerDetailInteractor travellerDetailInteractor;
  private EnableNotificationsInteractor mEnableNotificationsInteractor;
  private PreferencesControllerInterface mPreferencesController;
  private UpdateCitiesInteractor updateCitiesInteractor;

  public LoginInteractor(UserNetControllerInterface userNetController,
      UserCreateOrUpdateHandlerInterface userCreateOrUpdateHandler,
      SessionController sessionController,
      TravellerDetailInteractor travellerDetailInteractor,
      EnableNotificationsInteractor enableNotificationsInteractor,
      PreferencesControllerInterface preferencesController,
      UpdateCitiesInteractor updateCitiesInteractor) {
    mUserNetController = userNetController;
    mUserCreateOrUpdateHandler = userCreateOrUpdateHandler;
    this.mSessionController = sessionController;
    this.travellerDetailInteractor = travellerDetailInteractor;
    this.mEnableNotificationsInteractor = enableNotificationsInteractor;
    mPreferencesController = preferencesController;
    this.updateCitiesInteractor = updateCitiesInteractor;
  }

  public void login(@NotNull final String email, @NotNull final String token,
      final String profileUrl, final String name,
      @NotNull final OnRequestDataListener<User> listener, final UserProfile userProfile,
      @NotNull final String source) {
    mUserNetController.login(new OnRequestDataListener<User>() {
      @Override public void onResponse(User user) {
        if (mUserCreateOrUpdateHandler.addOrCreateUserAndAllComponents(user)) {
          mSessionController.saveUserInfo(user.getUserId(), name, profileUrl, email, UNREGISTERED);
          saveCredentials(buildCredentialType(source), email, token);
          createDefaultTravellerIfNeeded(user, userProfile, email, listener);
          updateCitiesInteractor.updateCities();
        } else {
          mSessionController.removeSharedPreferencesInfo();
          listener.onError(MslError.UNK_001, null);
        }

        if (user != null && user.getCreditCards() != null) {
          mSessionController.attachCreditCardsToSession(user.getCreditCards());
        }
      }

      @Override public void onError(MslError error, String message) {
        mSessionController.removeSharedPreferencesInfo();
        listener.onError(error, message);
      }
    }, email, token, source);
  }

  private void saveCredentials(CredentialsType credentialsType, String email, String token) {
    if (credentialsType.equals(CredentialsType.PASSWORD)) {
      mSessionController.savePasswordCredentials(email, token, credentialsType);
    } else {
      mSessionController.saveSocialCredentials(email, token, credentialsType);
    }
  }

  private CredentialsType buildCredentialType(String source) {
    switch (source) {
      case FACEBOOK_SOURCE:
        return CredentialsType.FACEBOOK;
      case GOOGLE_SOURCE:
        return CredentialsType.GOOGLE;
      default:
        return CredentialsType.PASSWORD;
    }
  }

  private void createDefaultTravellerIfNeeded(final User user, final UserProfile userProfile,
      String email, final OnRequestDataListener<User> listener) {
    if ((user.getUserTravellerList() == null || user.getUserTravellerList().isEmpty())
        && userProfile != null
        && travellerDetailInteractor.getTravellersList().isEmpty()
        && !travellerDetailInteractor.checkIfLocalUserHasDefaultTraveller()) {

      travellerDetailInteractor.saveUserTraveller(-1, userProfile.getName(), null,
          userProfile.getFirstLastName(), null, UserTraveller.TypeOfTraveller.ADULT, null,
          UserProfile.UNKNOWN_BIRTHDATE, null, false, email, userProfile.getGender(), true, null,
          null, null, null, null, null, null, null, null, false, null, null, null, null, null, null,
          null, null, -1, new OnUserUpdated() {
            @Override public void onUserUpdatedSuccessful() {
              setDefaultUser(user, listener, userProfile);
            }

            @Override public void onUserUpdatedError() {
              listener.onError(MslError.UNK_001, null);
            }

            @Override public void onUserInvalidCredentialsError() {
              listener.onError(MslError.UNK_001, null);
            }

            @Override public void onUserAuthError() {
              listener.onError(MslError.AUTH_000, null);
            }
          });
    } else {
      setDefaultUser(user, listener, userProfile);
    }
  }

  private void setDefaultUser(User user, OnRequestDataListener<User> listener,
      UserProfile userProfile) {

    UserProfile defaultTravellerUserProfile = getDefaultTraveller(user);

    if (defaultTravellerUserProfile != null) {
      mSessionController.saveUserName(getFullName(defaultTravellerUserProfile));
    } else if (userProfile != null && userProfile.getName() != null) {
      mSessionController.saveUserName(userProfile.getName() + " " + userProfile.getFirstLastName());
    }
    if (mPreferencesController.getBooleanValue(PREFERENCE_STATUS_FLIGHTS_NOTIFICATIONS, true)) {
      enableNotifications(listener, user);
    }
  }

  private UserProfile getDefaultTraveller(User user) {
    if (user.getUserTravellerList() != null) {
      for (UserTraveller traveller : user.getUserTravellerList()) {
        if (traveller.getUserProfile().isDefaultTraveller()) {
          return traveller.getUserProfile();
        }
      }
    }
    return null;
  }

  private String getFullName(UserProfile user) {
    return user.getName() + " " + user.getFirstLastName();
  }

  private void enableNotifications(final OnRequestDataListener<User> listener, final User user) {
    mEnableNotificationsInteractor.enableNotificationsDuringLogging(listener, user);
  }
}
