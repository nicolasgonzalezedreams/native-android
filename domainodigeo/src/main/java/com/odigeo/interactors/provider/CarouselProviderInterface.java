package com.odigeo.interactors.provider;

/**
 * Created by daniel.morales on 9/3/17.
 */
public interface CarouselProviderInterface {

  void getCarousel(boolean hasToCleanCache);

  void updateBookingCards();
}
