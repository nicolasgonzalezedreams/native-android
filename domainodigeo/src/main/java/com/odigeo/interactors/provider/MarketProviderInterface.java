package com.odigeo.interactors.provider;

public interface MarketProviderInterface {

  String getWebsite();

  String getLocale();

  String getFeedbackEmail();

  String getBrand();

  String getBrandVisualName();

  String getBrandKey();

  int getAdultsByDefault();

  int getKidsByDefault();

  int getInfantsByDefault();

  String getLanguage();

  String getMarketKey();

  String getCrossSellingMarketKey();

  String getCurrencyKey();

  String getLocalizedCurrencyValue(double value);

  String getName();

  String getAid();

  String getHotelsLabel();

  String getxSellingSearchResultsHotelsUrl();

  String getxSellingFilterHotelsUrl();

  String getWebViewHotelsURL();

  String getAppStoreUrl();

  String getOneCMSTableForCurrentMarket();

  String getProductionUrl();
}