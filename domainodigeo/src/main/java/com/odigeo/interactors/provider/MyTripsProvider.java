package com.odigeo.interactors.provider;

import com.odigeo.data.entity.booking.Booking;
import java.util.ArrayList;
import java.util.List;

public class MyTripsProvider {

  private List<Booking> mCurrentBookings;
  private List<Booking> mOldBookings;

  public MyTripsProvider() {
    mCurrentBookings = new ArrayList<>();
    mOldBookings = new ArrayList<>();
  }

  public List<Booking> getBookingList() {
    return mCurrentBookings;
  }

  public List<Booking> getOldBookingList() {
    return mOldBookings;
  }

  /**
   * Returns the number of elements in this list.
   *
   * @return the number of elements in this list
   */
  public int size() {
    return mCurrentBookings.size() + mOldBookings.size();
  }

  /**
   * Returns the element at the specified position in this list
   *
   * @param index index of the element to return
   * @return Booking at the specified position in this list
   * @throws IndexOutOfBoundsException - if the index is out of range (index < 0 || index >=
   * size())
   */
  public Booking get(int index) throws IndexOutOfBoundsException {
    if (index < mCurrentBookings.size()) {
      return mCurrentBookings.get(index);
    } else {
      return mOldBookings.get(index - mCurrentBookings.size());
    }
  }

  /**
   * Add booking at the specified list
   *
   * @param booking booking to insert
   */
  private void addBooking(Booking booking) {
    if (booking.isPastBooking()) {
      mOldBookings.add(booking);
    } else {
      mCurrentBookings.add(booking);
    }
  }

  public void addRateAppCardSpace() {
    mCurrentBookings.add(0, null);
  }

  private void addFooter() {
    if (mOldBookings.size() > 0) {
      mOldBookings.add(mOldBookings.size(), null);
    }
  }

  private void addHeader() {
    if (mOldBookings.size() > 0) {
      mOldBookings.add(0, null);
    }
  }

  public void removeRateAppCard() {
    if (mCurrentBookings.size() > 0) {
      mCurrentBookings.remove(0);
    }
  }

  public int getCurrentSize() {
    return mCurrentBookings.size();
  }

  public void clear() {
    mCurrentBookings.clear();
    mOldBookings.clear();
  }

  public void addAllBookings(List<Booking> bookingList) {
    for (int i = 0; i < bookingList.size(); i++) {
      Booking booking = bookingList.get(i);
      if (booking != null) {
        addBooking(booking);
      }
    }

    if (hasAnyBooking()) {
      addRateAppCardSpace();
    }

    if (mOldBookings.size() != 0) {
      addHeader();
      addFooter();
    }
  }

  public void addAllBookings(List<Booking> bookingList, boolean shouldShowRateApp) {
    addAllBookings(bookingList);

    if (hasAnyBooking() && !shouldShowRateApp) {
      removeRateAppCard();
    }
  }

  private boolean hasAnyBooking() {
    return !mCurrentBookings.isEmpty() || !mOldBookings.isEmpty();
  }
}
