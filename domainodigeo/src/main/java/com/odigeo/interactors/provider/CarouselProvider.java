package com.odigeo.interactors.provider;

import com.odigeo.builder.BookingCarouselCardBuilder;
import com.odigeo.builder.DropOffCarouselCardBuilder;
import com.odigeo.builder.PromotionCarouselCardBuilder;
import com.odigeo.data.ab.RemoteConfigControllerInterface;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.entity.carrousel.Carousel;
import com.odigeo.presenter.listeners.OnLoadCarrouselListener;
import java.util.ArrayList;
import java.util.List;

public class CarouselProvider
    implements CarouselProviderInterface, PromotionCarouselCardBuilder.PromotionCardsInterface {

  private Carousel mCarousel;
  private DropOffCarouselCardBuilder mDropOffCarouselCardBuilder;
  private BookingCarouselCardBuilder mBookingCarouselCardBuilder;
  private PromotionCarouselCardBuilder mPromotionCarouselCardBuilder;
  private RemoteConfigControllerInterface remoteConfigControllerInterface;
  private OnLoadCarrouselListener mListener;

  public CarouselProvider(DropOffCarouselCardBuilder dropOffCarouselCardBuilder,
      BookingCarouselCardBuilder bookingCarouselCardBuilder,
      PromotionCarouselCardBuilder promotionCarouselCardBuilder,
      RemoteConfigControllerInterface remoteConfigControllerInterface) {
    mDropOffCarouselCardBuilder = dropOffCarouselCardBuilder;
    mBookingCarouselCardBuilder = bookingCarouselCardBuilder;
    mPromotionCarouselCardBuilder = promotionCarouselCardBuilder;
    mPromotionCarouselCardBuilder.setPromotionCardsInterface(this);
    this.remoteConfigControllerInterface = remoteConfigControllerInterface;

    mCarousel = new Carousel();
  }

  public void setOnLoadCarouselListener(OnLoadCarrouselListener listener) {
    this.mListener = listener;
  }

  @Override public void getCarousel(boolean hasToCleanCache) {
    List<Card> dropOff = new ArrayList<>();

    if (remoteConfigControllerInterface.isDropOffActive()) {
      dropOff = mDropOffCarouselCardBuilder.buildCards();
    }

    List<Card> bookingCards = mBookingCarouselCardBuilder.buildCards();

    if (bookingCards.size() == 0) {
      mCarousel.setDropOffCards(dropOff);
    } else {
      mCarousel.setDropOffCards(new ArrayList<Card>());
    }

    mCarousel.setBookingCards(bookingCards);

    mListener.onCarouselLoaded(mCarousel);
    mPromotionCarouselCardBuilder.buildCards(hasToCleanCache);
  }

  @Override public void updateBookingCards() {
    List<Card> bookingCards = mBookingCarouselCardBuilder.buildCards();
    mCarousel.setBookingCards(bookingCards);
    mListener.onCarouselLoaded(mCarousel);
  }

  @Override public void onPromotionCardsLoaded(List<Card> cards) {
    mCarousel.setPromotionCards(cards);
    mListener.onCarouselLoaded(mCarousel);
  }

  @Override public void onPromotionCardsError() {
    mListener.onErrorLoadingCarousel();
  }
}
