package com.odigeo.interactors.provider;

public interface AssetsProvider {

  String loadAssetAsString(String pathToAsset);
}
