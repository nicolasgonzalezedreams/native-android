package com.odigeo.interactors;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartRequestModel;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.net.controllers.ShoppingCartNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.trustdefender.TrustDefenderController;
import com.odigeo.presenter.listeners.OnCreateShoppingCartListener;

import static com.odigeo.data.preferences.PreferencesControllerInterface.JSESSION_COOKIE;

public class CreateShoppingCartInteractor {

  private final ShoppingCartNetControllerInterface shoppingCartNetController;
  private final TrustDefenderController trustDefenderController;
  private final PreferencesControllerInterface preferencesController;

  public CreateShoppingCartInteractor(ShoppingCartNetControllerInterface
      shoppingCartNetController, TrustDefenderController trustDefenderController,
      PreferencesControllerInterface preferencesController) {
    this.shoppingCartNetController = shoppingCartNetController;
    this.trustDefenderController = trustDefenderController;
    this.preferencesController = preferencesController;
  }

  public void createShoppingCart(CreateShoppingCartRequestModel shoppingCartModel,
      final OnCreateShoppingCartListener onCreateShoppingCartListener) {
    shoppingCartNetController.getShoppingCart(shoppingCartModel, new OnRequestDataListener<CreateShoppingCartResponse>
        () {
      @Override public void onResponse(CreateShoppingCartResponse createShoppingCartResponse) {
        if (createShoppingCartResponse.getShoppingCart() != null) {
          trustDefenderController.sendFingerPrint(
              createShoppingCartResponse.getShoppingCart().getCyberSourceMerchantId(),
              preferencesController.getStringValue(JSESSION_COOKIE));
        }

        onCreateShoppingCartListener.onSuccess(createShoppingCartResponse);
      }

      @Override public void onError(MslError error, String message) {
        onCreateShoppingCartListener.onError(error, message);
      }
    });
  }
}
