package com.odigeo.interactors;

import com.odigeo.data.entity.userData.User;
import com.odigeo.data.net.controllers.NotificationNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.session.SessionController;

public class EnableNotificationsInteractor {

  private NotificationNetControllerInterface mNotificationNetController;
  private SessionController mSessionController;

  public EnableNotificationsInteractor(NotificationNetControllerInterface notificationNetController,
      SessionController sessionController) {
    this.mNotificationNetController = notificationNetController;
    this.mSessionController = sessionController;
  }

  public void enableNotifications() {
    String GCMToken = getGCMToken();
    if (!GCMToken.equals("")) {
      mNotificationNetController.enableNotifications(new OnRequestDataListener<Boolean>() {
        @Override public void onResponse(Boolean object) {
        }

        @Override public void onError(MslError error, String message) {
          mSessionController.invalidateGcmToken();
        }
      }, GCMToken);
    }
  }

  public void enableNotificationsDuringLogging(final OnRequestDataListener<User> listener,
      final User user) {
    String GCMToken = getGCMToken();
    if (!GCMToken.equals("")) {
      mNotificationNetController.enableNotifications(new OnRequestDataListener<Boolean>() {
        @Override public void onResponse(Boolean object) {
          listener.onResponse(user);
        }

        @Override public void onError(MslError error, String message) {
          listener.onError(error, message);
        }
      }, GCMToken);
    } else {
      listener.onResponse(user);
    }
  }

  private String getGCMToken() {
    return mSessionController.getGcmToken();
  }
}