package com.odigeo.interactors;

public class ObfuscateCreditCardInteractor {

  public ObfuscateCreditCardInteractor() {
  }

  public String obfuscate(String creditCard) {
    if (creditCard == null) {
      return null;
    }
    int maxCharactersShowed = 4;
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < creditCard.length(); i++) {
      if (i < creditCard.length() - maxCharactersShowed) {
        builder.append('*');
      } else {
        builder.append(creditCard.charAt(i));
      }
    }
    return builder.toString();
  }
}
