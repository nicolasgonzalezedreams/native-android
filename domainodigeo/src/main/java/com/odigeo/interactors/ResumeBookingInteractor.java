package com.odigeo.interactors;

import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.data.entity.shoppingCart.MessageResponse;
import com.odigeo.data.entity.shoppingCart.ResumeBooking;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.net.controllers.ResumeBookingNetController;
import com.odigeo.data.net.error.DAPIError;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.presenter.listeners.OnResumeBookingListener;

import java.util.ArrayList;
import java.util.List;

public class ResumeBookingInteractor {

  private ResumeBookingNetController mResumeBookingNetController;

  public ResumeBookingInteractor(ResumeBookingNetController resumeBookingNetController) {
    mResumeBookingNetController = resumeBookingNetController;
  }

  public void resumeBooking(ResumeBooking resumeBooking,
      final OnResumeBookingListener onResumeBookingListener) {
    mResumeBookingNetController.resumeBooking(new OnRequestDataListener<BookingResponse>() {
      @Override public void onResponse(BookingResponse bookingResponse) {
        if (hasToShowErrors(bookingResponse.getError())) {
          onResumeBookingListener.onGeneralError(bookingResponse);
        } else if (hasInternalErrors(bookingResponse.getMessages())) {
          onResumeBookingListener.onInternalError(bookingResponse);
        } else {
          if (bookingResponse.getCollectionOptions() != null) {
            bookingResponse.setCollectionOptions(
                checkAllowedPaymentMethods(bookingResponse.getCollectionOptions()));
          }
          onResumeBookingListener.onSuccess(bookingResponse);
        }
      }

      @Override public void onError(MslError error, String message) {
        onResumeBookingListener.onNoConnectionError();
      }
    }, resumeBooking);
  }

  private List<ShoppingCartCollectionOption> checkAllowedPaymentMethods(
      List<ShoppingCartCollectionOption> collectionOptions) {
    List<ShoppingCartCollectionOption> allowedCollectionOptions = new ArrayList<>();

    for (ShoppingCartCollectionOption shoppingCartCollectionOption : collectionOptions) {
      if (shoppingCartCollectionOption.getMethod().getType() != null) {
        allowedCollectionOptions.add(shoppingCartCollectionOption);
      }
    }
    return allowedCollectionOptions;
  }

  private boolean hasToShowErrors(com.odigeo.data.entity.shoppingCart.MslError error) {
    return error != null;
  }

  private boolean hasInternalErrors(List<MessageResponse> messages) {
    if (messages != null && !messages.isEmpty()) {
      for (MessageResponse message : messages) {
        if (message.getCode().equalsIgnoreCase(DAPIError.INT_001.getDapiError())
            || message.getCode().equalsIgnoreCase(DAPIError.INT_002.getDapiError())
            || message.getCode().equalsIgnoreCase(DAPIError.INT_003.getDapiError())) {
          return true;
        }
      }
    }
    return false;
  }
}
