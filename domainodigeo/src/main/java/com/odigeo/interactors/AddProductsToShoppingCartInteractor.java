package com.odigeo.interactors;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.request.ModifyShoppingCartRequest;
import com.odigeo.data.net.controllers.AddProductsNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.trustdefender.TrustDefenderController;
import com.odigeo.presenter.listeners.OnAddProductsToShoppingCartListener;

import static com.odigeo.data.preferences.PreferencesControllerInterface.JSESSION_COOKIE;

public class AddProductsToShoppingCartInteractor {

  private final AddProductsNetControllerInterface addProductsNetController;
  private final TrustDefenderController trustDefenderController;
  private final PreferencesControllerInterface preferencesController;

  public AddProductsToShoppingCartInteractor(
      AddProductsNetControllerInterface addProductsNetControllerInterface,
      TrustDefenderController trustDefenderController, PreferencesControllerInterface preferencesController) {
    this.addProductsNetController = addProductsNetControllerInterface;
    this.trustDefenderController = trustDefenderController;
    this.preferencesController = preferencesController;
  }

  public void addProducts(ModifyShoppingCartRequest modifyShoppingCartRequest,
      final OnAddProductsToShoppingCartListener onAddProductsToShoppingCartListener) {

    addProductsNetController.addProductsToShoppingCart(
        new OnRequestDataListener<CreateShoppingCartResponse>() {
          @Override public void onResponse(CreateShoppingCartResponse createShoppingCartResponse) {
            if (createShoppingCartResponse.getShoppingCart() != null) {
              trustDefenderController.sendFingerPrint(
                  createShoppingCartResponse.getShoppingCart().getCyberSourceMerchantId(),
                  preferencesController.getStringValue(JSESSION_COOKIE));
            }

            onAddProductsToShoppingCartListener.onSuccess(createShoppingCartResponse);
          }

          @Override public void onError(MslError error, String message) {
            onAddProductsToShoppingCartListener.onError(error, message);
          }
        }, modifyShoppingCartRequest);
  }
}