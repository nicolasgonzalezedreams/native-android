package com.odigeo.interactors;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.tools.DateHelperInterface;

public class CheckUpcomingBookingInteractor {

  private final DateHelperInterface dateHelper;

  public CheckUpcomingBookingInteractor(DateHelperInterface dateHelper) {
    this.dateHelper = dateHelper;
  }

  public boolean isUpcomingBooking(Booking booking) {
    return dateHelper.millisecondsLeastOffset(booking.getArrivalLastLeg())
        > dateHelper.millisecondsLeastOffset(dateHelper.getCurrentSystemMillis());
  }
}
