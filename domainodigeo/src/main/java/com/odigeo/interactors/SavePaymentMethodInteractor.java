package com.odigeo.interactors;

import com.odigeo.data.entity.userData.InsertCreditCardRequest;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.net.controllers.SavePaymentMethodNetController;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.data.tracker.TrackerControllerInterface;

public class SavePaymentMethodInteractor {

  private SavePaymentMethodNetController savePaymentMethodNetController;
  private TrackerControllerInterface trackerController;
  private CrashlyticsController crashlyticsController;

  public SavePaymentMethodInteractor(SavePaymentMethodNetController savePaymentMethodNetController,
      TrackerControllerInterface trackerController, CrashlyticsController crashlyticsController) {
    this.savePaymentMethodNetController = savePaymentMethodNetController;
    this.trackerController = trackerController;
    this.crashlyticsController = crashlyticsController;
  }

  public void savePaymentMethod(InsertCreditCardRequest creditCard) {
    savePaymentMethodNetController.savePaymentMethod(new OnRequestDataListener<User>() {
      @Override public void onResponse(User user) {

      }

      @Override public void onError(MslError error, String message) {
        crashlyticsController.trackNonFatal(
            new Exception(CrashlyticsController.INSERT_CREDITCARD_EXCEPTION,
                new Throwable(CrashlyticsController.INSERT_CREDITCARD_EXCEPTION)));
        crashlyticsController.setString(CrashlyticsController.INSERT_CREDITCARD_METHOD,
            CrashlyticsController.POST_METHOD);
      }
    }, creditCard);
  }
}
