package com.odigeo.interactors;

import com.odigeo.data.db.helper.CountriesDbHelperInterface;
import com.odigeo.data.db.listener.DatabaseUpdateListener;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.net.NetCountry;
import com.odigeo.data.net.controllers.CountryNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListListener;
import java.util.List;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/02/16.
 */
public class CountriesInteractor
    implements OnRequestDataListListener<NetCountry>, DatabaseUpdateListener {

  private final CountryNetControllerInterface mNetControllerInterface;
  private final CountriesDbHelperInterface mDbHelperInterface;
  private OnUpdateListener mOnUpdateListener;

  public CountriesInteractor(CountryNetControllerInterface countryNetController,
      CountriesDbHelperInterface countriesHelper) {
    this.mNetControllerInterface = countryNetController;
    this.mDbHelperInterface = countriesHelper;
  }

  public void updateCountries(OnUpdateListener onUpdateListener) {
    this.mOnUpdateListener = onUpdateListener;
    mNetControllerInterface.getCountries(this);
  }

  public void getCountries(String languageIsoCode,
      OnRequestDataListListener<Country> onRequestDataListListener) {
    mDbHelperInterface.getCountries(languageIsoCode, false, onRequestDataListListener);
  }

  public void getCountries(String languageIsoCode, boolean filterNullPhonePrefix,
      OnRequestDataListListener<Country> onRequestDataListListener) {
    mDbHelperInterface.getCountries(languageIsoCode, filterNullPhonePrefix,
        onRequestDataListListener);
  }

  public Country getCountryByCountryCode(String languageIsoCode, String countryCode) {
    return mDbHelperInterface.getCountryByCountryCode(languageIsoCode, countryCode);
  }

  @Override public void onResponse(List<NetCountry> response) {
    if (response != null) {
      mDbHelperInterface.updateCountries(response, this);
    } else if (mOnUpdateListener != null) {
      mOnUpdateListener.onFinish();
    }
  }

  @Override public void onError(MslError error, String message) {
    mOnUpdateListener.onFinish();
  }

  @Override public void onUpdateFinish() {
    if (mOnUpdateListener != null) {
      mOnUpdateListener.onFinish();
    }
  }

  public interface OnUpdateListener {

    void onFinish();
  }
}
