package com.odigeo.interactors;

import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.db.helper.BookingsHandlerInterface;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.net.controllers.BookingNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import java.util.List;

public class UpdateBookingsFromNetworkInteractor {

  private BookingNetControllerInterface mBookingNetController;
  private BookingDBDAOInterface mBookingDBDAO;
  private BookingsHandlerInterface mBookingHandler;
  private long mTotalBookingsToAdd;
  private long mBookingAddedCounter;
  private AddGuideInformationInteractor mAddGuideInformationInteractor;
  private UpdateBookingStatusInteractor mUpdateBookingStatusInteractor;
  private PreferencesControllerInterface mPreferencesController;

  public UpdateBookingsFromNetworkInteractor(BookingNetControllerInterface bookingNetController,
      BookingDBDAOInterface bookingDBDAO, BookingsHandlerInterface bookingHandler,
      AddGuideInformationInteractor addGuideInformationInteractor,
      UpdateBookingStatusInteractor updateBookingStatusInteractor,
      PreferencesControllerInterface preferencesController) {

    mBookingNetController = bookingNetController;
    mBookingDBDAO = bookingDBDAO;
    mBookingHandler = bookingHandler;
    mAddGuideInformationInteractor = addGuideInformationInteractor;
    mUpdateBookingStatusInteractor = updateBookingStatusInteractor;
    mPreferencesController = preferencesController;
    mTotalBookingsToAdd = 0;
    mBookingAddedCounter = 0;
  }

  public void updateMyTrips(final OnRequestDataListener<Void> listener) {

    mBookingNetController.getBookingsId(new OnRequestDataListListener<Long>() {
      @Override public void onResponse(List<Long> serverBookingIds) {
        processBookingsId(serverBookingIds, listener);
      }

      @Override public void onError(MslError error, String message) {
        updateBookingStatus();
        listener.onError(error, message);
      }
    });
  }

  protected void processBookingsId(List<Long> bookingsId, OnRequestDataListener<Void> listener) {
    saveLastUpdateTime();
    updateLocalBookings(listener);
    addNewBookings(listener, bookingsId);
  }

  private List<Long> getNewBookingIds(List<Long> serverBookingIdsList) {
    List<Long> localBookingIdsList = mBookingDBDAO.getAllBookingIds();
    serverBookingIdsList.removeAll(localBookingIdsList);
    return serverBookingIdsList;
  }

  private void updateLocalBookings(final OnRequestDataListener<Void> listener) {
    List<Long> localBookingIdsList = mBookingDBDAO.getAllBookingIds();

    for (final Long bookingId : localBookingIdsList) {
      mBookingNetController.getBookingById(new OnRequestDataListener<Booking>() {
        @Override public void onResponse(Booking booking) {
          if (booking != null) {
            mBookingHandler.updateBookingFlightStats(booking);
            mBookingDBDAO.changeEnrichedBooking(booking.getBookingId(),
                booking.getIsEnrichedBooking());
            mBookingHandler.updateItineraryBooking(booking);
          }
        }

        @Override public void onError(MslError error, String message) {
          listener.onError(error, message);
        }
      }, String.valueOf(bookingId));
    }
  }

  private void addNewBookings(OnRequestDataListener<Void> listener, List<Long> bookingsId) {
    List<Long> newBookingIds = getNewBookingIds(bookingsId);
    mTotalBookingsToAdd = newBookingIds.size();
    if (mTotalBookingsToAdd == 0) {
      updateBookingStatus();
      listener.onResponse(null);
    } else {
      processNewBookings(newBookingIds, listener);
    }
  }

  private void processNewBookings(List<Long> newBookingIds,
      final OnRequestDataListener<Void> listener) {

    for (Long bookingId : newBookingIds) {
      mBookingNetController.getBookingById(new OnRequestDataListener<Booking>() {
        @Override public void onResponse(Booking bookingToSave) {
          if (bookingToSave != null) {
            mBookingHandler.saveBooking(bookingToSave);
            updateBookingGuideInformation(bookingToSave);
          }
          increaseCounterAndCheckIfFinished(listener);
        }

        @Override public void onError(MslError error, String message) {
                    /* If there is an error with a booking, the process should continue with
                     * the other ones. We just ignore this booking */
          increaseCounterAndCheckIfFinished(listener);
        }
      }, String.valueOf(bookingId));
    }
  }

  private void increaseCounterAndCheckIfFinished(OnRequestDataListener<Void> listener) {
    mBookingAddedCounter++;
    if (isUpdateCompleted()) {
      mTotalBookingsToAdd = 0;
      mBookingAddedCounter = 0;
      updateBookingStatus();
      listener.onResponse(null);
    }
  }

  private void saveLastUpdateTime() {
    mPreferencesController.saveLongValue(PreferencesControllerInterface.LAST_UPDATE_BOOKING_STATUS,
        System.currentTimeMillis());
  }

  private void updateBookingGuideInformation(Booking bookingToUpdateGuide) {
    mAddGuideInformationInteractor.addGuideInformationToBooking(bookingToUpdateGuide);
  }

  private boolean isUpdateCompleted() {
    return mBookingAddedCounter == mTotalBookingsToAdd;
  }

  private void updateBookingStatus() {
    mUpdateBookingStatusInteractor.updateBookingStatus(false);
  }
}
