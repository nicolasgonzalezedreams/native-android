package com.odigeo.interactors;

import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.PricingBreakdown;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItem;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItemType;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownStep;
import com.odigeo.data.entity.shoppingCart.ShoppingCart;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.shoppingCart.Step;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.Nullable;

public class TotalPriceCalculatorInteractor {

  private CollectionMethodType collectionMethodType;
  private ShoppingCartCollectionOption shoppingCartCollectionOption;

  public TotalPriceCalculatorInteractor() {
  }

  public double getTotalPrice(ShoppingCart shoppingCart, PricingBreakdown pricingBreakdown,
      BigDecimal collectionMethodWithPriceValue, Step step) {
    PricingBreakdownStep pricingBreakdownStep = getPricingBreakdownStep(pricingBreakdown, step);

    Double totalPrice;
    if (pricingBreakdownStep != null && pricingBreakdownStep.getTotalPrice() != null) {
      totalPrice = pricingBreakdownStep.getTotalPrice().doubleValue();
    } else {
      totalPrice = shoppingCart.getTotalPrice().doubleValue();
    }

    List<PricingBreakdownItem> pricingBreakdownItems =
        getPricingBreakdownItems(pricingBreakdown, collectionMethodWithPriceValue, step);

    if (pricingBreakdownItems != null && !pricingBreakdownItems.isEmpty()) {
      for (PricingBreakdownItem item : pricingBreakdownItems) {
        switch (item.getPriceItemType()) {
          case PAYMENT_METHOD_PRICE_FULLPRICE:
          case PAYMENT_METHOD_PRICE:
            totalPrice += item.getPriceItemAmount().doubleValue();
        }
      }
    }
    return totalPrice;
  }

  public double getTotalPriceWithoutMembershipPerks(ShoppingCart shoppingCart,
      PricingBreakdown pricingBreakdown, BigDecimal collectionMethodWithPriceValue, Step step) {

    return getTotalPrice(shoppingCart, pricingBreakdown, collectionMethodWithPriceValue, step)
        - getPriceBreakdownItem(pricingBreakdown, step, PricingBreakdownItemType.MEMBERSHIP_PERKS);
  }

  public List<PricingBreakdownItem> getPricingBreakdownItems(PricingBreakdown pricingBreakdown,
      BigDecimal collectionMethodWithPriceValue, Step step) {
    List<PricingBreakdownItem> pricingBreakdownItems = new ArrayList<>();
    if (pricingBreakdown != null && pricingBreakdown.getPricingBreakdownSteps() != null) {
      PricingBreakdownStep selectedStep = getPricingBreakdownStep(pricingBreakdown, step);

      if (selectedStep != null) {
        for (PricingBreakdownItem item : selectedStep.getPricingBreakdownItems()) {
          if (item.getPriceItemType() != PricingBreakdownItemType.TOTAL_PRICE) {
            pricingBreakdownItems.add(item);
          }
        }
      }
    }

    if (collectionMethodType != null) {
      PricingBreakdownItem paymentMethodItem = getFeeItem(collectionMethodWithPriceValue);

      if (paymentMethodItem != null) {
        pricingBreakdownItems.add(paymentMethodItem);
      }
    }

    return pricingBreakdownItems;
  }

  public double getPriceBreakdownItem(PricingBreakdown pricingBreakdown, Step step,
      PricingBreakdownItemType type) {
    if (pricingBreakdown != null && pricingBreakdown.getPricingBreakdownSteps() != null) {
      PricingBreakdownStep selectedStep = getPricingBreakdownStep(pricingBreakdown, step);

      if (selectedStep != null) {
        for (PricingBreakdownItem item : selectedStep.getPricingBreakdownItems()) {
          if (item.getPriceItemType() == type) {
            return item.getPriceItemAmount().doubleValue();
          }
        }
      }
    }

    return 0d;
  }

  private PricingBreakdownStep getPricingBreakdownStep(PricingBreakdown pricingBreakdown,
      Step step) {
    PricingBreakdownStep selectedStep = null;

    if (pricingBreakdown != null && pricingBreakdown.getPricingBreakdownSteps() != null) {
      for (PricingBreakdownStep pricingBreakdownStep : pricingBreakdown.getPricingBreakdownSteps()) {
        if ((pricingBreakdownStep.getStep() == Step.DEFAULT && selectedStep == null)
            || pricingBreakdownStep.getStep() == step) {
          selectedStep = pricingBreakdownStep;
        }
      }
    }

    return selectedStep;
  }

  @Nullable private PricingBreakdownItem getFeeItem(BigDecimal collectionMethodWithPriceValue) {
    PricingBreakdownItem paymentMethod = null;
    if (collectionMethodWithPriceValue != null || shoppingCartCollectionOption != null) {
      paymentMethod = new PricingBreakdownItem();
      BigDecimal price;

      if (collectionMethodWithPriceValue != null) {
        paymentMethod.setPriceItemType(PricingBreakdownItemType.PAYMENT_METHOD_PRICE_FULLPRICE);
      } else {
        paymentMethod.setPriceItemType(PricingBreakdownItemType.PAYMENT_METHOD_PRICE);
      }

      if (shoppingCartCollectionOption == null) {
        price = collectionMethodWithPriceValue;
      } else {
        price = shoppingCartCollectionOption.getFee();
      }

      if (price.doubleValue() == 0d) {
        return null;
      } else {
        paymentMethod.setPriceItemAmount(price);
      }
    }

    return paymentMethod;
  }

  public void setCollectionMethodType(CollectionMethodType collectionMethodType) {
    this.collectionMethodType = collectionMethodType;
  }

  public void setShoppingCartCollectionOption(
      ShoppingCartCollectionOption shoppingCartCollectionOption) {
    this.shoppingCartCollectionOption = shoppingCartCollectionOption;
  }

  public boolean shouldApplyMembershipPerks(PricingBreakdown pricingBreakdown) {
    return hasMembershipApplied(pricingBreakdown);
  }

  private boolean hasMembershipApplied(PricingBreakdown pricingBreakdown) {
    if (pricingBreakdown != null && pricingBreakdown.getPricingBreakdownSteps() != null) {
      PricingBreakdownStep selectedStep = getPricingBreakdownStep(pricingBreakdown, null);

      if (selectedStep != null) {
        for (PricingBreakdownItem item : selectedStep.getPricingBreakdownItems()) {
          if (item.getPriceItemType() == PricingBreakdownItemType.MEMBERSHIP_PERKS) {
            return true;
          } else if (item.getPriceItemType() == PricingBreakdownItemType.SLASHED_MEMBERSHIP_PERKS) {
            return false;
          }
        }
      }
    }

    return false;
  }
}
