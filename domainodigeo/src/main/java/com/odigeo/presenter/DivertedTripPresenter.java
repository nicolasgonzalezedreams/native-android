package com.odigeo.presenter;

import com.odigeo.Notifier.EventsNotifier;
import com.odigeo.Notifier.event.LoadCarouselEvent;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.preferences.CarouselManagerInterface;
import com.odigeo.interactors.BookingInteractor;
import com.odigeo.presenter.contracts.navigators.NavigationDrawerNavigatorInterface;
import com.odigeo.presenter.contracts.views.DivertedTripViewInterface;

/**
 * Created by matia on 1/27/2016.
 */
public class DivertedTripPresenter
    extends BasePresenter<DivertedTripViewInterface, NavigationDrawerNavigatorInterface> {

  private final CarouselManagerInterface mCarouselManagerInterface;
  private final EventsNotifier mEventsNotifier;
  private BookingInteractor mBookingInteractor;
  private LoadCarouselEvent mLoadCarouselEvent;

  public DivertedTripPresenter(DivertedTripViewInterface view,
      NavigationDrawerNavigatorInterface navigator, BookingInteractor bookingInteractor,
      CarouselManagerInterface carouselManagerInterface, EventsNotifier eventsNotifier,
      LoadCarouselEvent loadCarouselEvent) {
    super(view, navigator);
    mBookingInteractor = bookingInteractor;
    mCarouselManagerInterface = carouselManagerInterface;
    mEventsNotifier = eventsNotifier;
    mLoadCarouselEvent = loadCarouselEvent;
  }

  public void showBookingDetail(long bookingId) {
    Booking booking = mBookingInteractor.getBookingById(bookingId);
    if (booking != null) {
      mNavigator.navigateToTripDetail(booking);
    }
  }

  public void removeBookingCard(long bookingId) {
    Booking booking = mBookingInteractor.getBookingById(bookingId);
    mCarouselManagerInterface.hideBookingCard(bookingId, booking.getArrivalLastLeg(),
        Card.CardType.DIVERTED_SECTION_CARD);
    mEventsNotifier.fireEvent(mLoadCarouselEvent);
  }
}
