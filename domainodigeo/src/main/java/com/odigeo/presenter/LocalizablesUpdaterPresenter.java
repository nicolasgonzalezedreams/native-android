package com.odigeo.presenter;

import com.odigeo.data.net.error.MslError;
import com.odigeo.interactors.LocalizablesInteractor;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.presenter.contracts.navigators.LocalizablesUpdaterNavigatorInterface;
import com.odigeo.presenter.contracts.views.LocalizablesUpdaterViewInterface;
import com.odigeo.presenter.listeners.OnGetLocalizablesListener;

public class LocalizablesUpdaterPresenter
    extends BasePresenter<LocalizablesUpdaterViewInterface, LocalizablesUpdaterNavigatorInterface> {

  private LocalizablesInteractor localizablesInteractor;
  private MarketProviderInterface marketProviderInterface;

  public LocalizablesUpdaterPresenter(LocalizablesUpdaterViewInterface view,
      LocalizablesUpdaterNavigatorInterface navigator,
      LocalizablesInteractor localizablesInteractor,
      MarketProviderInterface marketProviderInterface) {
    super(view, navigator);
    this.localizablesInteractor = localizablesInteractor;
    this.marketProviderInterface = marketProviderInterface;
  }

  public void updateCurrentMarket() {
    String oneCMSTable = marketProviderInterface.getOneCMSTableForCurrentMarket();

    updateMarket(oneCMSTable);
  }

  private void updateMarket(final String oneCMSTable) {
    localizablesInteractor.getLocalizables(oneCMSTable, new OnGetLocalizablesListener() {
      @Override public void onSuccess() {
        mView.onUpdatingMarketSuccess(oneCMSTable);
      }

      @Override public void onError(MslError error, String message) {
        mView.onUpdatingMarketFail();
      }
    });
  }
}
