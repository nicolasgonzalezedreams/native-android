package com.odigeo.presenter;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.presenter.contracts.navigators.ArrivalGuideViewerNavigatorInterface;
import com.odigeo.presenter.contracts.views.ArrivalGuideViewInterface;
import java.io.File;
import java.util.List;

/**
 * Created by julio.kun on 11/18/2015.
 */
public class ArrivalGuidePresenter
    extends BasePresenter<ArrivalGuideViewInterface, ArrivalGuideViewerNavigatorInterface> {

  public ArrivalGuidePresenter(ArrivalGuideViewInterface view,
      ArrivalGuideViewerNavigatorInterface navigator) {
    super(view, navigator);
  }

  public void showArrivalGuideInApp(Booking booking, File baseDirectory, String packageName) {
    getNavigatorInterface().showDestinationTitle(booking.getArrivalCityName(0));
    mView.initPdfViewer(getPdfFile(booking, baseDirectory, packageName));
  }

  public void showArrivalGuideExternal(Booking booking, File baseDirectory, String packageName) {
    getNavigatorInterface().showDestinationTitle(booking.getArrivalCityName(0));
    mView.showPdfInExternalApp(getPdfFile(booking, baseDirectory, packageName));
  }

  private String getIata(Booking booking) {
    Segment segment = booking.getSegments().get(0);
    List<Section> sections = segment.getSectionsList();
    Section arrivalFlight = sections.get(sections.size() - 1);
    return arrivalFlight.getTo().getCityIATACode();
  }

  private File getPdfFile(Booking booking, File baseDirectory, String packageName) {
    return new File(baseDirectory
        + "/Android/data/"
        + packageName
        + "/files/Download/"
        + getIata(booking)
        + ".pdf");
  }
}
