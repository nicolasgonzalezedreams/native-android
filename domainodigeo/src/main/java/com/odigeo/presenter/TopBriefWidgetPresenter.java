package com.odigeo.presenter;

import com.odigeo.interactors.MembershipInteractor;
import com.odigeo.presenter.contracts.components.BaseCustomComponentPresenter;
import com.odigeo.presenter.contracts.views.TopBriefViewInterface;

public class TopBriefWidgetPresenter extends BaseCustomComponentPresenter<TopBriefViewInterface>{

  private MembershipInteractor membershipInteractor;

  public TopBriefWidgetPresenter(TopBriefViewInterface view,
      MembershipInteractor membershipInteractor) {
    super(view);
    this.membershipInteractor = membershipInteractor;
  }

  public boolean shouldShowMembershipLabel() {
    return membershipInteractor.isMemberForCurrentMarket();
  }

  public void configureTotalPrice() {
    boolean isMembershipApplied = mView.getMembershipApplied();

    if (shouldShowMembershipLabel()) {
      if (isMembershipApplied) {
        mView.showMembershipWithDiscount();
      } else {
        mView.showMembershipWithNoDiscount();
      }
    } else {
      mView.showFullPrice();
    }
  }
}
