package com.odigeo.presenter;

import com.odigeo.presenter.contracts.navigators.AppRateNavigatorInterface;
import com.odigeo.presenter.contracts.views.AppRateViewInterface;

/**
 * Created by matias.dirusso on 4/29/2016.
 */
public class AppRatePresenter
    extends BasePresenter<AppRateViewInterface, AppRateNavigatorInterface> {

  public AppRatePresenter(AppRateViewInterface view, AppRateNavigatorInterface navigator) {
    super(view, navigator);
  }

  public void navigateToThanksScreen() {
    mNavigator.navigateToThanksScreen();
  }

  public void navigateToHelpImproveScreen() {
    mNavigator.navigateToHelpImproveScreen();
  }
}
