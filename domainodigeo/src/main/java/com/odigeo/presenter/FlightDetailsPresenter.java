package com.odigeo.presenter;

import com.odigeo.interactors.FlightDetailsInteractor;
import com.odigeo.presenter.contracts.navigators.FlightDetailsNavigatorInterface;
import com.odigeo.presenter.contracts.views.FlightDetailsViewInterface;

public class FlightDetailsPresenter
    extends BasePresenter<FlightDetailsViewInterface, FlightDetailsNavigatorInterface> {

  private FlightDetailsInteractor mFlightDetailsInteractor;

  public FlightDetailsPresenter(FlightDetailsViewInterface view,
      FlightDetailsNavigatorInterface navigator, FlightDetailsInteractor interactor) {
    super(view, navigator);
    this.mFlightDetailsInteractor = interactor;
  }
}
