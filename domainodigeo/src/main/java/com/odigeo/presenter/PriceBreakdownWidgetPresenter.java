package com.odigeo.presenter;

import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.InsuranceShoppingItem;
import com.odigeo.data.entity.shoppingCart.PricingBreakdown;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItem;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItemType;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownStep;
import com.odigeo.data.entity.shoppingCart.ShoppingCart;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.data.entity.userData.Membership;
import com.odigeo.interactors.MembershipInteractor;
import com.odigeo.interactors.TotalPriceCalculatorInteractor;
import com.odigeo.presenter.contracts.components.BaseCustomComponentPresenter;
import com.odigeo.presenter.contracts.views.PriceBreakdownWidgetInterface;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.Nullable;

public class PriceBreakdownWidgetPresenter
    extends BaseCustomComponentPresenter<PriceBreakdownWidgetInterface> {

  private PriceBreakdownWidgetInterface mView;
  private MembershipInteractor membershipInteractor;
  private CollectionMethodType mCollectionMethodType;
  private ShoppingCartCollectionOption mShoppingCartCollectionOption;
  private Step mStep;
  private ShoppingCart mShoppingCart;
  private PricingBreakdown mPricingBreakdown;
  private TotalPriceCalculatorInteractor totalPriceCalculatorInteractor;

  public PriceBreakdownWidgetPresenter(PriceBreakdownWidgetInterface view, MembershipInteractor
      membershipInteractor, TotalPriceCalculatorInteractor totalPriceCalculatorInteractor) {
    super(view);
    mView = view;
    this.membershipInteractor = membershipInteractor;
    this.totalPriceCalculatorInteractor = totalPriceCalculatorInteractor;
  }

  public void configurePresenter(Step step, ShoppingCart shoppingCart,
      PricingBreakdown pricingBreakdown) {
    mStep = step;
    mShoppingCart = shoppingCart;
    mPricingBreakdown = pricingBreakdown;

    mView.checkUAEMessage();
  }

  public void onTaxRefundableInfoCheck() {

    if (mPricingBreakdown.getTaxRefund() != null && mPricingBreakdown.getTaxRefund() > 0) {
      mView.showTaxRefundableInfo(mPricingBreakdown.getTaxRefund());
    }
  }

  void setCollectionMethodType(CollectionMethodType collectionMethodType) {
    mCollectionMethodType = collectionMethodType;
  }

  public void setShoppingCartCollectionOption(
      ShoppingCartCollectionOption shoppingCartCollectionOption) {
    mShoppingCartCollectionOption = shoppingCartCollectionOption;
  }

  public ShoppingCart getShoppingCart() {
    return mShoppingCart;
  }

  public void setShoppingCart(ShoppingCart shoppingCart) {
    mShoppingCart = shoppingCart;
  }

  public void setPricingBreakdown(PricingBreakdown pricingBreakdown) {
    mPricingBreakdown = pricingBreakdown;
  }

  public List<PricingBreakdownItem> getPricingBreakdownItems() {
    return totalPriceCalculatorInteractor.getPricingBreakdownItems(mPricingBreakdown, mView.getCollectionMethodWithPrice(),
        mStep);
  }

  public Double getTotalPrice() {
    return totalPriceCalculatorInteractor.getTotalPrice(mShoppingCart, mPricingBreakdown, mView
        .getCollectionMethodWithPrice(), mStep);
  }

  public BigDecimal getPriceItemAmountWithType(PricingBreakdownItemType pricingItemType, int row,
      InsuranceShoppingItem insuranceShoppingItem) {
    if (pricingItemType == PricingBreakdownItemType.INSURANCE_PRICE) {
      return insuranceShoppingItem.getTotalPrice();
    } else {
      return getPricingBreakdownItems().get(row).getPriceItemAmount();
    }
  }

  void updatePricesBreakdown() {
    mView.showPricesBreakdown();
  }

  public boolean shouldShowMembershipAppliedLabel() {
    return membershipInteractor.isMemberForCurrentMarket();
  }

  public boolean membershipIsApplied() {
    if(mShoppingCart.getTravellers().isEmpty()) {
      return true;
    } else {
      Membership currentMembership = membershipInteractor.getMembershipForCurrentMarket();

      if(currentMembership != null) {
        String memberFirstName = currentMembership.getFirstName();
        String memberLastName = currentMembership.getLastNames();

        String memberName = (memberFirstName == null ? "" : memberFirstName);
        memberName += " " + (memberLastName == null ? "" : memberLastName);
        memberName = memberName.toLowerCase().trim();

        for (Traveller traveller : mShoppingCart.getTravellers()) {

          String travellerName = (traveller.getName() == null ? "" : traveller.getName());
          travellerName += " " + (traveller.getFirstLastName() == null ? "" : traveller.getFirstLastName());
          travellerName += " " + (traveller.getSecondLastName() == null ? "" : traveller.getSecondLastName());
          travellerName = travellerName.toLowerCase().trim();

          if (memberName.equals(travellerName)) {
            return true;
          }
        }
      }

      return false;
    }
  }
}