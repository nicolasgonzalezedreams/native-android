package com.odigeo.presenter;

import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.CheckUserCredentialsInteractor;
import com.odigeo.interactors.LogoutInteractor;
import com.odigeo.interactors.RemoveAccountInteractor;
import com.odigeo.presenter.contracts.navigators.AccountPreferencesNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.AccountPreferencesViewInterface;

public class AccountPreferencesPresenter
    extends BasePresenter<AccountPreferencesViewInterface, AccountPreferencesNavigatorInterface> {

  private RemoveAccountInteractor mRemoveAccountInteractor;
  private CheckUserCredentialsInteractor mCheckUserCredentialsInteractor;
  private LogoutInteractor mLogoutInteractor;
  private SessionController mSessionController;

  public AccountPreferencesPresenter(AccountPreferencesViewInterface view,
      AccountPreferencesNavigatorInterface navigator,
      RemoveAccountInteractor mRemoveAccountInteractor,
      CheckUserCredentialsInteractor checkUserCredentialsInteractor,
      LogoutInteractor logoutInteractor, SessionController sessionController) {
    super(view, navigator);
    this.mRemoveAccountInteractor = mRemoveAccountInteractor;
    this.mCheckUserCredentialsInteractor = checkUserCredentialsInteractor;
    mLogoutInteractor = logoutInteractor;
    mSessionController = sessionController;
  }

  public void removeAccount() {
    mView.showRemovingAccountDialog();
    mRemoveAccountInteractor.removeAccount(new OnAuthRequestDataListener<Boolean>() {
      @Override public void onAuthError() {
        mLogoutInteractor.logout(new OnRequestDataListener<Boolean>() {
          @Override public void onResponse(Boolean object) {
            mView.showAuthError();
            mSessionController.saveLogoutWasMade(true);
          }

          @Override public void onError(MslError error, String message) {
            mView.onLogoutError(error, message);
          }
        });
      }

      @Override public void onResponse(Boolean object) {
        mView.accountRemoved(object != null);
        mCheckUserCredentialsInteractor.removeAllData();
      }

      @Override public void onError(MslError error, String message) {
        mView.accountRemoved(false);
      }
    });
  }

  public void initializeUserEmail() {
    mView.initializeUserEmail(mCheckUserCredentialsInteractor.getUserEmail());
  }

  public void changeEmailAction() {
    mView.showCantChangeEmailDialog();
  }

  public void deleteAccountAction() {
    mView.showDeleteAccountDialog();
  }

  public void deleteView() {
    mNavigator.finishNavigator();
  }

  public void editPasswordAction() {
    mNavigator.showResetPasswordScreen();
  }

  public void onAuthOkClicked() {
    mNavigator.navigateToLogin();
  }
}
