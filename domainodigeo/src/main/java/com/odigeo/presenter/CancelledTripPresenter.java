package com.odigeo.presenter;

import com.odigeo.Notifier.EventsNotifier;
import com.odigeo.Notifier.event.LoadCarouselEvent;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.preferences.CarouselManagerInterface;
import com.odigeo.interactors.BookingInteractor;
import com.odigeo.presenter.contracts.navigators.NavigationDrawerNavigatorInterface;
import com.odigeo.presenter.contracts.views.CancelledTripViewInterface;

/**
 * Created by matia on 1/27/2016.
 */
public class CancelledTripPresenter
    extends BasePresenter<CancelledTripViewInterface, NavigationDrawerNavigatorInterface> {

  private BookingInteractor mBookingInteractor;
  private CarouselManagerInterface mCarouselManagerInterface;
  private EventsNotifier mEventsNotifier;
  private LoadCarouselEvent mLoadCarouselEvent;

  public CancelledTripPresenter(CancelledTripViewInterface view,
      NavigationDrawerNavigatorInterface navigator, BookingInteractor bookingInteractor,
      CarouselManagerInterface carouselManagerInterface, EventsNotifier eventsNotifier,
      LoadCarouselEvent loadCarouselEvent) {
    super(view, navigator);
    mBookingInteractor = bookingInteractor;
    mCarouselManagerInterface = carouselManagerInterface;
    mEventsNotifier = eventsNotifier;
    mLoadCarouselEvent = loadCarouselEvent;
  }

  public void showBookingDetail(long bookingId) {
    Booking booking = mBookingInteractor.getBookingById(bookingId);
    if (booking != null) {
      mNavigator.navigateToTripDetail(booking);
    }
  }

  public void removeBookingCard(long bookingId) {
    Booking booking = mBookingInteractor.getBookingById(bookingId);
    mCarouselManagerInterface.hideBookingCard(bookingId, booking.getArrivalLastLeg(),
        Card.CardType.CANCELLED_SECTION_CARD);
    mEventsNotifier.fireEvent(mLoadCarouselEvent);
  }
}
