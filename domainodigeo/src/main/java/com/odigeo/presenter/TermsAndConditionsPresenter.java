package com.odigeo.presenter;

import com.odigeo.presenter.contracts.navigators.AboutUsNavigatorInterface;
import com.odigeo.presenter.contracts.views.TermsAndConditionsViewInterface;

public class TermsAndConditionsPresenter
    extends BasePresenter<TermsAndConditionsViewInterface, AboutUsNavigatorInterface> {

  public TermsAndConditionsPresenter(TermsAndConditionsViewInterface view,
      AboutUsNavigatorInterface navigator) {
    super(view, navigator);
  }

  public void openTCBrand() {
    mNavigator.openTCBrand();
  }

  public void openTCAirline() {
    mNavigator.openTCAirline();
  }

  public void openPrivacyPolicy() {
    mNavigator.openPrivacyPolicy();
  }

  public void setActionBarTitle(String title) {
    mNavigator.setNavigationTitle(title);
  }
}
