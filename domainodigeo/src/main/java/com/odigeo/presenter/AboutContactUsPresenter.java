package com.odigeo.presenter;

import com.odigeo.presenter.contracts.navigators.AboutUsNavigatorInterface;
import com.odigeo.presenter.contracts.views.AboutContactUsViewInterface;

public class AboutContactUsPresenter
    extends BasePresenter<AboutContactUsViewInterface, AboutUsNavigatorInterface> {

  public AboutContactUsPresenter(AboutContactUsViewInterface view,
      AboutUsNavigatorInterface navigator) {
    super(view, navigator);
  }

  public void setActionBarTitle(String title) {
    mNavigator.setNavigationTitle(title);
  }
}
