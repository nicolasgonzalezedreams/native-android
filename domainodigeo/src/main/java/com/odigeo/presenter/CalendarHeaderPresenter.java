package com.odigeo.presenter;

import com.odigeo.presenter.contracts.components.BaseCustomComponentPresenter;
import com.odigeo.presenter.contracts.views.holder.CalendarHeader;
import java.util.Date;

public class CalendarHeaderPresenter extends BaseCustomComponentPresenter<CalendarHeader> {

  private Date departureDate;
  private Date returnDate;

  private boolean isOpeningView;

  public CalendarHeaderPresenter(CalendarHeader view) {
    super(view);
  }

  private void setLayout() {
    if (departureDate == null) {
      mView.setSelectDateOnDeparture();

      mView.highlightDepartureLabel();
      mView.shadeReturnLabel();
    } else if (returnDate == null) {
      mView.setDepartureDateLabel(departureDate);
      mView.shadeDepartureLabel();

      mView.setSelectDateOnReturn();
      mView.highlightReturnLabel();
    } else if (isOpeningView) {
      mView.setDepartureDateLabel(departureDate);
      mView.highlightDepartureLabel();

      mView.setReturnDateLabel(returnDate);
      mView.shadeReturnLabel();

      isOpeningView = false;
    } else {
      mView.setDepartureDateLabel(departureDate);
      mView.normalizeDepartureLabel();

      mView.setReturnDateLabel(returnDate);
      mView.normalizeReturnLabel();
    }
  }

  public void openingView() {
    isOpeningView = true;
  }

  public Date getDepartureDate() {
    return departureDate;
  }

  public void setDepartureDate(Date departureDate) {
    this.departureDate = departureDate;

    setLayout();
  }

  public void resetDepartureDate() {
    this.departureDate = null;

    setLayout();
  }

  public Date getReturnDate() {
    return returnDate;
  }

  public void setReturnDate(Date returnDate) {
    this.returnDate = returnDate;

    setLayout();
  }

  public void resetReturnDate() {
    this.returnDate = null;

    setLayout();
  }
}
