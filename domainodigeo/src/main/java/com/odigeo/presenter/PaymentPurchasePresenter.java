package com.odigeo.presenter;

import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.presenter.contracts.views.PaymentPurchaseWidgetInterface;
import com.odigeo.presenter.listeners.PaymentPurchaseListener;

public class PaymentPurchasePresenter {

  private PaymentPurchaseWidgetInterface paymentPurchaseWidgetView;
  private PaymentPurchaseListener paymentPurchaseListener;
  private TrackerControllerInterface trackerController;

  public PaymentPurchasePresenter(PaymentPurchaseWidgetInterface paymentPurchaseWidgetView,
      TrackerControllerInterface trackerController) {
    this.paymentPurchaseWidgetView = paymentPurchaseWidgetView;
    this.trackerController = trackerController;
  }

  void setPaymentPurchaseListener(PaymentPurchaseListener paymentPurchaseListener) {
    this.paymentPurchaseListener = paymentPurchaseListener;
  }

  public void onExplicitAcceptanceChecked(boolean checked) {
    paymentPurchaseListener.onExplicitAcceptanceChecked(checked);
  }

  void onValidateOptionsChange(boolean areFieldsCorrect) {
    paymentPurchaseWidgetView.setContinueButtonEnable(areFieldsCorrect);
  }

  public void onTACClick() {
    trackerController.trackPaymentTACClick();
    trackerController.trackPaymentTACScreen();
    paymentPurchaseWidgetView.openConditionsDialog();
  }

  public void onContinueButtonClick() {
    trackerController.trackLocalyticsEventPurchase();
    trackerController.trackPaymentPurchaseClick();
    paymentPurchaseListener.onPaymentPurchaseContinue();
  }
}