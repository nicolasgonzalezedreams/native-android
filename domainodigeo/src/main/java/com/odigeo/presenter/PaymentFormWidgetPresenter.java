package com.odigeo.presenter;

import com.odigeo.comparators.ShoppingCartCollectionOptionComparator;
import com.odigeo.constants.PaymentMethodsKeys;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.shoppingCart.request.CreditCardCollectionDetailsParametersRequest;
import com.odigeo.data.entity.userData.CreditCard;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.OrderCreditCardsByLastUsageInteractor;
import com.odigeo.presenter.contracts.views.PaymentFormWidgetInterface;
import com.odigeo.presenter.listeners.PaymentWidgetFormListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.jetbrains.annotations.Nullable;

public class PaymentFormWidgetPresenter {

  private static final int VISIBILITY_GONE = 8;
  private static final int MAX_DIGITS_TO_BIN_CHECK = 6;
  private static final int MIN_CREDIT_CARD_SIZE = 12;
  private static final int MAX_CREDIT_CARD_SIZE = 19;
  private static final String TRACK_CVV_NO_CARD = "no_card";
  private static final String TRACK_CVV_3_DIGITS = "3_digits";
  private static final String TRACK_CVV_4_DIGITS = "4_digits";

  private PaymentFormWidgetInterface mPaymentFormWidgetView;
  private BinCheckPresenter mBinCheckPresenter;
  private List<ShoppingCartCollectionOption> mShoppingCartCollectionOptions;
  private PaymentWidgetFormListener mPaymentWidgetFormListener;
  private int previousCardLength = 0;
  private OrderCreditCardsByLastUsageInteractor mOrderCreditCardsByLastUsageInteractor;
  private List<SavedPaymentMethodFormPresenter> savedPaymentMethodFormPresenters =
      new ArrayList<>();
  private CollectionMethodType mCollectionMethodType;
  private SessionController sessionController;
  private TrackerControllerInterface trackerController;

  public PaymentFormWidgetPresenter(PaymentFormWidgetInterface paymentFormWidgetInterface,
      BinCheckPresenter binCheckPresenter, SessionController sessionController,
      OrderCreditCardsByLastUsageInteractor orderCreditCardsByLastUsageInteractor,
      PaymentWidgetFormListener paymentWidgetFormListener,
      TrackerControllerInterface trackerController) {

    mPaymentFormWidgetView = paymentFormWidgetInterface;
    mBinCheckPresenter = binCheckPresenter;
    mCollectionMethodType = CollectionMethodType.CREDITCARD;
    mPaymentWidgetFormListener = paymentWidgetFormListener;
    this.sessionController = sessionController;
    this.mOrderCreditCardsByLastUsageInteractor = orderCreditCardsByLastUsageInteractor;
    this.trackerController = trackerController;
  }

  public void fireValidationsListener(boolean thereAreNoErrosInFields) {
    if (mPaymentWidgetFormListener != null) {
      mPaymentWidgetFormListener.onPaymentWidgetValidation(thereAreNoErrosInFields);
    }
  }

  public void configurePaymentFormView(
      List<ShoppingCartCollectionOption> shoppingCartCollectionOptions) {
    mShoppingCartCollectionOptions = shoppingCartCollectionOptions;

    sortShoppingCartCollectionOptions();

    boolean externalPaymentAvailable = false;
    boolean lastPaymentMethod;
    for (int i = 0; i < mShoppingCartCollectionOptions.size(); i++) {

      ShoppingCartCollectionOption shoppingCartCollectionOption =
          mShoppingCartCollectionOptions.get(i);
      lastPaymentMethod = (i == mShoppingCartCollectionOptions.size() - 1);

      if (shoppingCartCollectionOption.getMethod().getType() == CollectionMethodType.PAYPAL) {
        externalPaymentAvailable = true;
        showPaypal(shoppingCartCollectionOption.getFee(), lastPaymentMethod);
      } else if (shoppingCartCollectionOption.getMethod().getType()
          == CollectionMethodType.BANKTRANSFER) {
        externalPaymentAvailable = true;
        showBankTransfer(lastPaymentMethod);
      } else if (shoppingCartCollectionOption.getMethod().getType()
          == CollectionMethodType.TRUSTLY) {
        externalPaymentAvailable = true;
        showTrustly(shoppingCartCollectionOption.getFee(), lastPaymentMethod);
      } else if (shoppingCartCollectionOption.getMethod().getType()
          == CollectionMethodType.KLARNA) {
        externalPaymentAvailable = true;
        showKlarna(shoppingCartCollectionOption.getFee(), lastPaymentMethod);
      }
    }

    if (savePaymentMethodIsNotRefreshing()) {
      addSavePaymentMethod();
    }

    if (!externalPaymentAvailable && sessionController.getCreditCards() == null) {
      mPaymentFormWidgetView.hideCreditCardRadioButton();
    } else {
      mPaymentFormWidgetView.showCreditCardRadioButton();
    }

    if (!externalPaymentAvailable && sessionController.getCreditCards() != null) {
      mPaymentFormWidgetView.hideCreditCardSeparator();
    }

    if (sessionController.getCredentials() != null) {
      mPaymentFormWidgetView.showSavePaymentMethodSwitch();
    }
  }

  private boolean savePaymentMethodIsNotRefreshing() {
    return !mPaymentFormWidgetView.isSavedPaymentMethodAdded();
  }

  private void sortShoppingCartCollectionOptions() {
    Collections.sort(mShoppingCartCollectionOptions, new ShoppingCartCollectionOptionComparator());
  }

  private void showPaypal(BigDecimal fee, boolean lastPaymentMethod) {
    mPaymentFormWidgetView.showPaypalRadioButton();
    if (fee != null) {
      mPaymentFormWidgetView.setPaypalFee(fee.doubleValue());
    }
    if (!lastPaymentMethod) {
      mPaymentFormWidgetView.showPaypalSeparator();
    }
  }

  private void showBankTransfer(boolean lastPaymentMethod) {
    mPaymentFormWidgetView.showBankTransferRadioButton();
    if (!lastPaymentMethod) {
      mPaymentFormWidgetView.showBankTransferSeparator();
    }
  }

  private void showTrustly(BigDecimal fee, boolean lastPaymentMethod) {
    mPaymentFormWidgetView.showTrustlyRadioButton();
    if (fee != null) {
      mPaymentFormWidgetView.setTrustlyFee(fee.doubleValue());
    }
    if (!lastPaymentMethod) {
      mPaymentFormWidgetView.showTrustlySeparator();
    }
  }

  private void showKlarna(BigDecimal fee, boolean lastPaymentMethod) {
    mPaymentFormWidgetView.showKlarnaRadioButton();
    if (fee != null) {
      mPaymentFormWidgetView.setKlarnaFee(fee.doubleValue());
    }
    if (!lastPaymentMethod) {
      mPaymentFormWidgetView.showKlarnaSeparator();
    }
  }

  private void addSavePaymentMethod() {
    List<CreditCard> savedCreditCards = sessionController.getCreditCards();

    if (savedCreditCards != null && !savedCreditCards.isEmpty()) {
      List<CreditCard> mCreditCardsToOrder =
          mOrderCreditCardsByLastUsageInteractor.run(savedCreditCards);
      for (CreditCard creditCard : mCreditCardsToOrder) {
        mPaymentFormWidgetView.showSavedPaymentMethodRow(creditCard, false);
      }
    }
  }

  public void trackLoggedUserWithPaymentMethods() {
    if (sessionController.getCredentials() != null
        && sessionController.getCreditCards() != null
        && !sessionController.getCreditCards().isEmpty()) {
      trackerController.trackUserLoggedWithPaymentMethods();
    } else if (sessionController.getCredentials() != null) {
      trackerController.trackerUserLoggedWithoutPaymentMethods();
    }
  }

  public List<ShoppingCartCollectionOption> createListForCreditCardSpinner() {
    List<ShoppingCartCollectionOption> creditCardMethods = new ArrayList<>();
    for (ShoppingCartCollectionOption shoppingCartCollectionOption : mShoppingCartCollectionOptions) {
      if (shoppingCartCollectionOption.getMethod().getType() == CollectionMethodType.CREDITCARD) {
        creditCardMethods.add(shoppingCartCollectionOption);
      }
    }
    return creditCardMethods;
  }

  public void runBinCheck(CharSequence cardNumberText, int length) {
    if (userHasCopyAndPasteCreditCard(previousCardLength, length)) {
      String creditcard =
          removeWhiteSpaces(cardNumberText.toString()).substring(0, MAX_DIGITS_TO_BIN_CHECK);
      checkBin(creditcard);
    } else {
      checkBin(removeWhiteSpaces(cardNumberText.toString()));
    }
    previousCardLength = length;
  }

  private boolean userHasCopyAndPasteCreditCard(int previousLength, int length) {
    return (previousLength == 0
        && length >= MIN_CREDIT_CARD_SIZE
        && length <= MAX_CREDIT_CARD_SIZE);
  }

  private String removeWhiteSpaces(String creditCardNumber) {
    return creditCardNumber.replaceAll("\\s", "");
  }

  public void checkBin(String creditCardNumber) {
    mBinCheckPresenter.checkBin(creditCardNumber);
  }

  public void onCreditCardSelectedItemSpinner(String creditCardCode) {
    if (creditCardCode == null) return;

    mPaymentFormWidgetView.trackBinCheckNoDetected();
    updateCVVValidator(creditCardCode, false);
    mPaymentFormWidgetView.setCreditCardCode(creditCardCode);
  }

  public void updateCVVValidator(String paymentMethod, boolean comesFromBinCheck) {
    if (paymentMethod.equals(PaymentMethodsKeys.UNKNOWN)) return;

    if (isAmericanExpress(paymentMethod)) {
      mPaymentFormWidgetView.updateCVVAmexValidator();
    } else {
      mPaymentFormWidgetView.updateCVVNormalValidator();
    }

    if (haveToTrackBinCheckSuccess(paymentMethod, comesFromBinCheck)) {
      mPaymentFormWidgetView.trackBinCheckSuccess();
    }
    fireUpdatePricingBreakdownListener(paymentMethod);
  }

  public boolean isAmericanExpress(String creditCardMethodType) {
    return (creditCardMethodType != null && (PaymentMethodsKeys.AX.equals(creditCardMethodType)
        || PaymentMethodsKeys.AX_LOCAL.equals(creditCardMethodType)));
  }

  private boolean haveToTrackBinCheckSuccess(String paymentMethod, boolean comesFromBinCheck) {
    return comesFromBinCheck && (!paymentMethod.contains("LOCAL"));
  }

  public void fireUpdatePricingBreakdownListener(String paymentMethod) {
    if (mPaymentWidgetFormListener != null) {
      mPaymentWidgetFormListener.onPaymentMethodSelected(paymentMethod);
    }
  }

  public void fireUpdateSavedPaymentMethodPricingBreakdownListener(String paymentMethod) {
    if (mPaymentWidgetFormListener != null) {
      mPaymentWidgetFormListener.onSavedPaymentMethodSelected(paymentMethod);
    }
  }

  @Nullable public String getCardNumberObfuscated() {
    String buyerCardNumber = getCardNumber();

    if (buyerCardNumber == null) {
      return null;
    }

    int maxCharactersShowed = 4;
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < buyerCardNumber.length(); i++) {
      if (i < buyerCardNumber.length() - maxCharactersShowed) {
        builder.append('*');
      } else {
        builder.append(buyerCardNumber.charAt(i));
      }
    }
    return builder.toString();
  }

  public String getCardNumber() {
    if (mCollectionMethodType == CollectionMethodType.CREDITCARD) {
      return removeWhiteSpaces(mPaymentFormWidgetView.getCreditCardNumber());
    }
    return null;
  }

  @Nullable public CreditCardCollectionDetailsParametersRequest createCreditCardRequest() {
    CreditCardCollectionDetailsParametersRequest creditCardRequest;
    SavedPaymentMethodFormPresenter savedPaymentMethodFormPresenterSelected =
        getSavedPaymentMethodPresenterSelected();

    if (savedPaymentMethodFormPresenterSelected == null) {
      creditCardRequest = createCreditCardRequestFromForm();
      checkIfHaveToTrackStoreMethodNotUsed();
    } else {
      boolean isCVVInfoValid = savedPaymentMethodFormPresenterSelected.isCVVInfoValid();
      if (!isCVVInfoValid) {
        savedPaymentMethodFormPresenterSelected.scrollToCVVFieldWithError();
        return null;
      }
      trackerController.trackStorePaymentMethodIsUsed();
      return savedPaymentMethodFormPresenterSelected.createCreditCardRequest();
    }
    return creditCardRequest;
  }

  public void checkIfHaveToTrackStoreMethodNotUsed() {
    if (sessionController.getCreditCards() != null) {
      trackerController.trackStorePaymentMethodNotUsed();
    }
  }

  private CreditCardCollectionDetailsParametersRequest createCreditCardRequestFromForm() {
    CreditCardCollectionDetailsParametersRequest creditCardCollectionDetailsParametersRequest;

    creditCardCollectionDetailsParametersRequest =
        new CreditCardCollectionDetailsParametersRequest();
    creditCardCollectionDetailsParametersRequest.setCardNumber(
        removeWhiteSpaces(mPaymentFormWidgetView.getCreditCardNumber()));
    creditCardCollectionDetailsParametersRequest.setCardOwner(
        mPaymentFormWidgetView.getCreditCardOwner());
    creditCardCollectionDetailsParametersRequest.setCardSecurityNumber(
        mPaymentFormWidgetView.getCVV());
    creditCardCollectionDetailsParametersRequest.setCardTypeCode(
        mPaymentFormWidgetView.getPaymentMethodCodeDetected());
    creditCardCollectionDetailsParametersRequest.setCardExpirationMonth(
        mPaymentFormWidgetView.getExpirationMonth());
    creditCardCollectionDetailsParametersRequest.setCardExpirationYear(
        mPaymentFormWidgetView.getExpirationYear());

    return creditCardCollectionDetailsParametersRequest;
  }

  @Nullable private SavedPaymentMethodFormPresenter getSavedPaymentMethodPresenterSelected() {
    for (SavedPaymentMethodFormPresenter savedPaymentMethodFormPresenter : savedPaymentMethodFormPresenters) {
      if (savedPaymentMethodFormPresenter.isSelectedThisSavedPaymentMethod()) {
        return savedPaymentMethodFormPresenter;
      }
    }
    return null;
  }

  public CollectionMethodType getCollectionMethodTypeDetected() {
    return mCollectionMethodType;
  }

  public void onClickCVVInfoTooltip(final int visibility, String creditCardMethod) {
    if (visibility == VISIBILITY_GONE) {
      if (isAmericanExpress(creditCardMethod)) {
        mPaymentFormWidgetView.showCVVTooltipAMEX();
        mPaymentFormWidgetView.trackCVVTooltip(TRACK_CVV_4_DIGITS);
      } else if (isMasterCardOrVisa(creditCardMethod)) {
        mPaymentFormWidgetView.showCVVTooltipVisaOrMastercard();
        mPaymentFormWidgetView.trackCVVTooltip(TRACK_CVV_3_DIGITS);
      } else {
        mPaymentFormWidgetView.showCVVTooltipAllTypes();
        mPaymentFormWidgetView.trackCVVTooltip(TRACK_CVV_NO_CARD);
      }
      mPaymentFormWidgetView.showCVVTooltip();
    } else {
      mPaymentFormWidgetView.hideCVVTooltip();
    }
  }

  public boolean isMasterCardOrVisa(String creditCardMethodType) {
    return (creditCardMethodType != null && (creditCardMethodType.equals(
        PaymentMethodsKeys.VI_LOCAL)
        || creditCardMethodType.equals(PaymentMethodsKeys.MD_LOCAL)
        || creditCardMethodType.equals(PaymentMethodsKeys.VI)
        || creditCardMethodType.equals(PaymentMethodsKeys.VD)
        || creditCardMethodType.equals(PaymentMethodsKeys.VE)
        || creditCardMethodType.equals(PaymentMethodsKeys.E1)
        || creditCardMethodType.equals(PaymentMethodsKeys.MA)
        || creditCardMethodType.equals(PaymentMethodsKeys.CA)
        || creditCardMethodType.equals(PaymentMethodsKeys.MD)
        || creditCardMethodType.equals(PaymentMethodsKeys.MP)));
  }

  public void onBankTransferSelected() {
    unCheckSavedPaymentMethodRadioButtons();
    mPaymentFormWidgetView.setPaymentMethodsUnchecked();
    mPaymentFormWidgetView.setBankTransferChecked();
    mPaymentFormWidgetView.showBankTransferWidget();
    mCollectionMethodType = CollectionMethodType.BANKTRANSFER;
    fireOnCollectionOptionSelected(CollectionMethodType.BANKTRANSFER);
    onSelectedCollectionMethod(true);
  }

  private void fireOnCollectionOptionSelected(CollectionMethodType collectionMethodType) {
    mPaymentWidgetFormListener.onCollectionOptionSelected(collectionMethodType);
  }

  public void onSelectedCollectionMethod(boolean thereWasFocusAtLeastOnce) {
    mPaymentWidgetFormListener.onPaymentWidgetValidation(thereWasFocusAtLeastOnce);
  }

  public void onCreditCardSelected() {
    unCheckSavedPaymentMethodRadioButtons();
    mPaymentFormWidgetView.setPaymentMethodsUnchecked();
    mPaymentFormWidgetView.setCreditCardChecked();
    mPaymentFormWidgetView.showCreditCardWidget();
    mCollectionMethodType = CollectionMethodType.CREDITCARD;
    fireOnCollectionOptionSelected(CollectionMethodType.CREDITCARD);
    onSelectedCollectionMethod(mPaymentFormWidgetView.isCreditCardFocused());
  }

  public void onPaypalSelected() {
    unCheckSavedPaymentMethodRadioButtons();
    mPaymentFormWidgetView.setPaymentMethodsUnchecked();
    mPaymentFormWidgetView.setPaypalChecked();
    mPaymentFormWidgetView.showPaypalWidget();
    mCollectionMethodType = CollectionMethodType.PAYPAL;
    fireOnCollectionOptionSelected(CollectionMethodType.PAYPAL);
    onSelectedCollectionMethod(true);
  }

  public void onTrustlySelected() {
    unCheckSavedPaymentMethodRadioButtons();
    mPaymentFormWidgetView.setPaymentMethodsUnchecked();
    mPaymentFormWidgetView.setTrustlyChecked();
    mPaymentFormWidgetView.showTrustlyWidget();
    mCollectionMethodType = CollectionMethodType.TRUSTLY;
    fireOnCollectionOptionSelected(CollectionMethodType.TRUSTLY);
    onSelectedCollectionMethod(true);
  }

  public void onKlarnaSelected() {
    unCheckSavedPaymentMethodRadioButtons();
    mPaymentFormWidgetView.setPaymentMethodsUnchecked();
    mPaymentFormWidgetView.setKlarnaChecked();
    mPaymentFormWidgetView.showKlarnaWidget();
    mCollectionMethodType = CollectionMethodType.KLARNA;
    fireOnCollectionOptionSelected(CollectionMethodType.KLARNA);
    onSelectedCollectionMethod(true);
  }

  public void setCreditCardCollectionType() {
    mCollectionMethodType = CollectionMethodType.CREDITCARD;
  }

  public void addSavedPaymentMethodPresenter(SavedPaymentMethodFormPresenter presenter) {
    savedPaymentMethodFormPresenters.add(presenter);
  }

  public void unCheckSavedPaymentMethodRadioButtons() {
    for (SavedPaymentMethodFormPresenter savedPaymentMethodFormPresenter : savedPaymentMethodFormPresenters) {
      savedPaymentMethodFormPresenter.unCheckRadioButton();
    }
  }

  public boolean checkDefaultSavedPaymentMethodRadioButton() {
    for (SavedPaymentMethodFormPresenter savedPaymentMethodFormPresenter : savedPaymentMethodFormPresenters) {
      if (!savedPaymentMethodFormPresenter.isCreditCardExpired()
          && savedPaymentMethodFormPresenter.isCreditCardAccepted()) {
        savedPaymentMethodFormPresenter.initDefaultRadioButton();
        mPaymentFormWidgetView.hideCreditCardWidget();
        mPaymentFormWidgetView.savedPaymentMethodWasSelected();
        fireUpdateSavedPaymentMethodPricingBreakdownListener(
            savedPaymentMethodFormPresenter.getCreditCardCode());
        return true;
      }
    }
    mPaymentFormWidgetView.setCreditCardChecked();
    return false;
  }

  public boolean isStorePaymentMethodChecked() {
    return mPaymentFormWidgetView.isStoreMethodCheck();
  }

  public void onRefreshPaymentFormWidgetView(List<ShoppingCartCollectionOption> collectionOptions) {
    mShoppingCartCollectionOptions = collectionOptions;
    clearCreditCardFields();
    refreshCreditCardSpinner();
    refreshBinCheck(collectionOptions);
    refreshPaymentMethods();
    refreshSavedPaymentMethods(collectionOptions);
    refreshPaymentMethodRadioButtons();
  }

  private void clearCreditCardFields() {
    mPaymentFormWidgetView.clearCreditCardFields();
  }

  private void refreshCreditCardSpinner() {
    mPaymentFormWidgetView.initCreditCardSpinner();
  }

  private void refreshBinCheck(List<ShoppingCartCollectionOption> collectionOptions) {
    mBinCheckPresenter.initPresenter(collectionOptions);
  }

  private void refreshPaymentMethods() {
    mPaymentFormWidgetView.hideBankTransfer();
    mPaymentFormWidgetView.hideTrustlyWidget();
    mPaymentFormWidgetView.hidePaypalWidget();
    mPaymentFormWidgetView.hideKlarnaWidget();
    configurePaymentFormView(mShoppingCartCollectionOptions);
  }

  private void refreshPaymentMethodRadioButtons() {
    mPaymentFormWidgetView.setPaymentMethodsUnchecked();
    boolean isSavePaymentMethodSelected = checkDefaultSavedPaymentMethodRadioButton();
    if (isSavePaymentMethodSelected) {
      mCollectionMethodType = CollectionMethodType.CREDITCARD;
      onSelectedCollectionMethod(true);
    } else {
      mPaymentFormWidgetView.performClickOnCreditCardRadioButton();
      onSelectedCollectionMethod(false);
    }
  }

  private void refreshSavedPaymentMethods(List<ShoppingCartCollectionOption> collectionOptions) {
    for (SavedPaymentMethodFormPresenter savedPaymentMethodFormPresenter : savedPaymentMethodFormPresenters) {
      savedPaymentMethodFormPresenter.refreshRow(collectionOptions);
    }
  }
}
