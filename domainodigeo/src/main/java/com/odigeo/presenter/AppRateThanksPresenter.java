package com.odigeo.presenter;

import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.presenter.contracts.navigators.AppRateFeedbackNavigatorInterface;
import com.odigeo.presenter.contracts.views.AppRateThanksViewInterface;

public class AppRateThanksPresenter
    extends BasePresenter<AppRateThanksViewInterface, AppRateFeedbackNavigatorInterface> {

  private PreferencesControllerInterface mPreferencesController;

  public AppRateThanksPresenter(AppRateThanksViewInterface view,
      AppRateFeedbackNavigatorInterface navigator,
      PreferencesControllerInterface preferencesController) {
    super(view, navigator);
    mPreferencesController = preferencesController;
  }

  public void navigateToPlayStore() {
    mNavigator.navigateToPlayStore();
    mPreferencesController.saveBooleanValue(PreferencesControllerInterface.MYTRIPS_APPRATE_SHOWCARD,
        false);
  }

  public void closeAppRateThanksNavigator() {
    mNavigator.closeAppRateThanksNavigator();
  }
}
