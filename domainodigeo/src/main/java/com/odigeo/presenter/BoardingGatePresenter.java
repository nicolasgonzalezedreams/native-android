package com.odigeo.presenter;

import com.odigeo.interactors.BookingInteractor;
import com.odigeo.presenter.contracts.navigators.NavigationDrawerNavigatorInterface;
import com.odigeo.presenter.contracts.views.BoardingGateViewInterface;

/**
 * Created by gonzalo.lodi on 1/29/2016.
 */
public class BoardingGatePresenter
    extends BasePresenter<BoardingGateViewInterface, NavigationDrawerNavigatorInterface> {

  private BookingInteractor mBookingInteractor;

  public BoardingGatePresenter(BoardingGateViewInterface view,
      NavigationDrawerNavigatorInterface navigator, BookingInteractor bookingInteractor) {
    super(view, navigator);
    mBookingInteractor = bookingInteractor;
  }
}
