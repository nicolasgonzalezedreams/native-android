package com.odigeo.presenter;

import com.odigeo.data.entity.userData.Membership;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.MembershipInteractor;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.presenter.contracts.navigators.MembershipQANavigatorInterface;
import com.odigeo.presenter.contracts.views.MembershipQAViewInterface;
import java.util.List;
import java.util.Random;

public class MembershipQAPresenter
    extends BasePresenter<MembershipQAViewInterface, MembershipQANavigatorInterface> {

  private final MembershipInteractor membershipInteractor;
  private final MarketProviderInterface marketProviderInterface;
  private final SessionController sessionController;

  public MembershipQAPresenter(MembershipQAViewInterface view,
      MembershipQANavigatorInterface navigator, MembershipInteractor membershipInteractor,
      MarketProviderInterface marketProviderInterface, SessionController sessionController) {
    super(view, navigator);
    this.membershipInteractor = membershipInteractor;
    this.marketProviderInterface = marketProviderInterface;
    this.sessionController = sessionController;
  }

  public List<Membership> getAllMemberships() {
    List<Membership> membershipList = membershipInteractor.getAllMemberships();
    return membershipList;
  }

  public void checkUserIsMemberForCurrentMarket() {
    if(sessionController.getCredentials() == null) {
      mView.showNotMemberLabel();
      mView.hideAddMembershipButton();
    } else {
      if (membershipInteractor.isMemberForCurrentMarket()) {
        mView.hideNotMemberLabel();
        mView.hideAddMembershipButton();
      } else {
        mView.showNotMemberLabel();
        mView.showAddMembershipButton();
      }
    }
  }

  public void addMembershipForCurrentMarket() {
    String name = sessionController.getUserInfo().getName();
    String website = marketProviderInterface.getWebsite();
    long id = new Random().nextLong();
    membershipInteractor.addMembership(id, name, "", website);
    mView.updateMembershipList(membershipInteractor.getAllMemberships());
    checkUserIsMemberForCurrentMarket();
  }
}
