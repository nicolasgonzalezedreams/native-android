package com.odigeo.presenter;

import com.odigeo.presenter.contracts.navigators.AboutUsNavigatorInterface;
import com.odigeo.presenter.contracts.views.AboutFaqViewInterface;

public class AboutFaqPresenter
    extends BasePresenter<AboutFaqViewInterface, AboutUsNavigatorInterface> {

  public AboutFaqPresenter(AboutFaqViewInterface view, AboutUsNavigatorInterface navigator) {
    super(view, navigator);
  }

  public void setActionBarTitle(String title) {
    mNavigator.setNavigationTitle(title);
  }

  public void openWebView(String key, String defaultUrl) {
    mNavigator.openWebView(key, defaultUrl);
  }
}
