package com.odigeo.presenter;

import com.odigeo.data.tracker.TrackerControllerInterface;

public class ExpirateDateCreditCardValidator {

  public static boolean validateDate(String month, String year, int currentMonth, int currentYear,
      TrackerControllerInterface tracker) {
    if (isNumeric(month) && isNumeric(year)) {
      int selectedMonth = Integer.valueOf(month);
      int selectedYear = Integer.valueOf(year);
      boolean isValid = ((selectedMonth >= currentMonth && selectedYear == currentYear) || (
          selectedYear
              > currentYear));
      if (!isValid) {
        if (tracker != null) {
          tracker.trackExpirationDateInvalidInPayment();
        }
        return false;
      }
      return true;
    }
    if (tracker != null) {
      tracker.trackExpirationDateMissingInPayment();
    }
    return false;
  }

  private static boolean isNumeric(final CharSequence cs) {
    if (isEmpty(cs)) {
      return false;
    }
    final int sz = cs.length();
    for (int i = 0; i < sz; i++) {
      if (!Character.isDigit(cs.charAt(i))) {
        return false;
      }
    }
    return true;
  }

  private static boolean isEmpty(final CharSequence cs) {
    return cs == null || cs.length() == 0;
  }
}
