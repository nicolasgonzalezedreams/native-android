package com.odigeo.presenter;

import com.odigeo.builder.UrlBuilder;
import com.odigeo.data.entity.contract.UserInteractionNeededResponse;
import com.odigeo.data.entity.shoppingCart.BookingAdditionalParameter;
import com.odigeo.data.entity.shoppingCart.ResumeDataRequest;
import com.odigeo.interactors.provider.AssetsProvider;
import com.odigeo.presenter.contracts.ExternalPaymentNavigatorInterface;
import com.odigeo.presenter.contracts.views.ExternalPaymentViewInterface;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.odigeo.presenter.PaymentPresenter.REDIRECT_URL_EXTERNAL_PAYMENT;

public class ExternalPaymentPresenter
    extends BasePresenter<ExternalPaymentViewInterface, ExternalPaymentNavigatorInterface> {

  public static final int EXTERNAL_PAYMENT_CODE_OK = 2;
  public static final int EXTERNAL_PAYMENT_CODE_KO = 3;
  private final AssetsProvider assetsProvider;
  private String serializeJS;

  public ExternalPaymentPresenter(ExternalPaymentViewInterface view,
      ExternalPaymentNavigatorInterface navigator, AssetsProvider assetsProvider) {
    super(view, navigator);

    this.assetsProvider = assetsProvider;
  }

  private String buildExternalPaymentUrl(
      UserInteractionNeededResponse userInteractionNeededResponse) {
    UrlBuilder.Builder builder = new UrlBuilder.Builder();
    builder.path(userInteractionNeededResponse.getRedirectUrl());
    builder.params(userInteractionNeededResponse.getParameters());
    UrlBuilder urlBuider = builder.build();

    try {
      return urlBuider.getCompleteUrl();
    } catch (UnsupportedEncodingException e) {
      return "";
    }
  }

  protected ResumeDataRequest buildResumeDataRequest(String url)
      throws UnsupportedEncodingException, MalformedURLException {
    ResumeDataRequest resumeDataRequest = new ResumeDataRequest();
    List<BookingAdditionalParameter> bookingAdditionalParameterList = new ArrayList<>();

    UrlBuilder.Builder builder = new UrlBuilder.Builder(url);
    UrlBuilder urlBuilder = builder.build();

    Set<Map.Entry<String, String>> entriesParams = urlBuilder.getParams().entrySet();
    for (Map.Entry<String, String> param : entriesParams) {
      BookingAdditionalParameter bookingAdditionalParameter = new BookingAdditionalParameter();
      bookingAdditionalParameter.setName(param.getKey());
      bookingAdditionalParameter.setValue(param.getValue());
      bookingAdditionalParameterList.add(bookingAdditionalParameter);
    }

    resumeDataRequest.setAdditionalParameters(bookingAdditionalParameterList);
    return resumeDataRequest;
  }

  private void navigateBackToPayment(String url) {
    try {
      ResumeDataRequest resumeDataRequest = buildResumeDataRequest(url);
      mNavigator.finishExternalPaymentNavigator(EXTERNAL_PAYMENT_CODE_OK, resumeDataRequest);
    } catch (UnsupportedEncodingException | MalformedURLException e) {
      e.printStackTrace();
      mNavigator.finishExternalPaymentNavigator(EXTERNAL_PAYMENT_CODE_KO, null);
    }
  }

  public boolean isPaymentFinishedOrCancelled(String url) {
    return url.startsWith(REDIRECT_URL_EXTERNAL_PAYMENT);
  }

  public void onUrlChanged(String url) {
    if (isPaymentFinishedOrCancelled(url)) {
      navigateBackToPayment(url);
    }
  }

  public void onFormDataParsed(String formDataParams) {
    String url = REDIRECT_URL_EXTERNAL_PAYMENT + "?" + formDataParams;
    onUrlChanged(url);
  }

  public void loadUrl(UserInteractionNeededResponse userInteractionNeededResponse) {

    if (userInteractionNeededResponse.isPost()) {
      if (userInteractionNeededResponse.getRedirectUrl() == null
          || userInteractionNeededResponse.getParameters() == null) {
        navigateBackToPayment("");
      } else {
        mView.loadPostUrl(userInteractionNeededResponse.getRedirectUrl(),
            userInteractionNeededResponse.getParameters());
      }
    } else {
      String paymentUrl = buildExternalPaymentUrl(userInteractionNeededResponse);
      if (paymentUrl.isEmpty()) {
        navigateBackToPayment("");
      } else {
        mView.loadUrl(paymentUrl);
      }
    }
  }

  public void onUrlLoadedFinished() {
    mView.hideProgressBar();
  }

  public void onBackPressed() {

  }

  public void onParseFormData() {
    /*
      This is a hack to avoid the submission of the form and handle that ourselves.
			To achieve this we transverse the DOM to find the form that matches the url we know
			in it's action attribute and the POST method, and set the submit event handler
			to a no-op function forwarding the event to the parseFormData method defined
			in WebViewJavascriptInterface.
			The serialize function is defined inside the serialize.js. This function transforms
			the form into a query string to be able to append it to the url and reuse the
			same code that handles GET requests.
		 */
    if (serializeJS == null) {
      serializeJS = assetsProvider.loadAssetAsString("js/serialize.js");
    }

    String injectedJs = "var element = document.querySelector(\"form[action^='"
        + REDIRECT_URL_EXTERNAL_PAYMENT
        + "'][method='POST']\");";
    injectedJs += "if (element != undefined) { "
        + "element.submit = function(){};"
        + "android.parseFormData(serialize(element));"
        + "}";

    mView.loadUrl("javascript:" + serializeJS);
    mView.loadUrl("javascript:" + injectedJs);
  }
}
