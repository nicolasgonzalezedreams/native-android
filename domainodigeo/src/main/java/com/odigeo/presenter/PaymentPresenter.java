package com.odigeo.presenter;

import com.odigeo.configuration.ABAlias;
import com.odigeo.configuration.ABPartition;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.shoppingCart.BookingDetail;
import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.data.entity.shoppingCart.BookingStatus;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.MessageResponse;
import com.odigeo.data.entity.shoppingCart.PricingBreakdown;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItem;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItemType;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownStep;
import com.odigeo.data.entity.shoppingCart.PromotionalCodeUsageResponse;
import com.odigeo.data.entity.shoppingCart.ResumeBooking;
import com.odigeo.data.entity.shoppingCart.ResumeDataRequest;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionState;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.shoppingCart.request.BookingRequest;
import com.odigeo.data.entity.shoppingCart.request.CreditCardCollectionDetailsParametersRequest;
import com.odigeo.data.entity.shoppingCart.request.InvoiceRequest;
import com.odigeo.data.entity.shoppingCart.request.PaymentDataRequest;
import com.odigeo.data.entity.shoppingCart.request.ShoppingCartBookingRequest;
import com.odigeo.data.entity.shoppingCart.request.UserInteractionNeededRequest;
import com.odigeo.data.entity.userData.InsertCreditCardRequest;
import com.odigeo.data.entity.userData.Membership;
import com.odigeo.data.net.error.DAPIError;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnSaveImportedTripListener;
import com.odigeo.helper.ABTestHelper;
import com.odigeo.interactors.ConfirmBookingInteractor;
import com.odigeo.interactors.MembershipInteractor;
import com.odigeo.interactors.MyTripsManualImportInteractor;
import com.odigeo.interactors.ResumeBookingInteractor;
import com.odigeo.interactors.TotalPriceCalculatorInteractor;
import com.odigeo.presenter.contracts.navigators.PaymentNavigatorInterface;
import com.odigeo.presenter.contracts.views.PaymentViewInterface;
import com.odigeo.presenter.listeners.OnConfirmBookingListener;
import com.odigeo.presenter.listeners.OnResumeBookingListener;
import com.odigeo.presenter.listeners.PaymentPurchaseListener;
import com.odigeo.presenter.listeners.PromoCodeWidgetListener;
import com.odigeo.tools.CreditCardRequestToStoreCreditCardRequestMapper;
import java.math.BigDecimal;
import java.util.List;

public class PaymentPresenter extends BasePresenter<PaymentViewInterface, PaymentNavigatorInterface>
    implements PromoCodeWidgetListener, PaymentPurchaseListener {

  static final String REDIRECT_URL_EXTERNAL_PAYMENT = "http://odigeo.com";
  private static final int RANDOM_BOOKING_ID = 12345;

  private ConfirmBookingInteractor confirmBookingInteractor;
  private MyTripsManualImportInteractor myTripsManualImportInteractor;
  private PaymentFormWidgetPresenter paymentFormWidgetPresenter;
  private PriceBreakdownWidgetPresenter priceBreakdownWidgetPresenter;
  private PromoCodeWidgetPresenter promoCodeWidgetPresenter;
  private PaymentPurchasePresenter paymentPurchasePresenter;
  private ResumeBookingInteractor resumeBookingInteractor;

  private CreateShoppingCartResponse createShoppingCartResponse;
  private boolean isFullTransparency;
  private double lastTicketsPrice;
  private double lastInsurancePrice;
  private ShoppingCartCollectionOption shoppingCartCollectionOption;
  private boolean areAllFormFieldsCorrect;
  private InsertCreditCardRequest savePaymentMethodRequest;
  private CreditCardRequestToStoreCreditCardRequestMapper
      creditCardRequestToStoreCreditCardRequestMapper;
  private ABTestHelper abTestHelper;

  private String velocityErrorMessage;
  private MembershipInteractor membershipInteractor;
  private TotalPriceCalculatorInteractor totalPriceCalculatorInteractor;
  private BigDecimal collectionMethodWithPriceValue;

  public PaymentPresenter(PaymentViewInterface view, PaymentNavigatorInterface navigator,
      ConfirmBookingInteractor confirmBookingInteractor,
      MyTripsManualImportInteractor myTripsManualImportInteractor,
      ResumeBookingInteractor resumeBookingInteractor,
      CreditCardRequestToStoreCreditCardRequestMapper creditCardRequestToStoreCreditCardRequestMapper,
      ABTestHelper abTestHelper, MembershipInteractor membershipInteractor,
      TotalPriceCalculatorInteractor totalPriceCalculatorInteractor) {

    super(view, navigator);
    this.confirmBookingInteractor = confirmBookingInteractor;
    this.myTripsManualImportInteractor = myTripsManualImportInteractor;
    this.resumeBookingInteractor = resumeBookingInteractor;
    this.creditCardRequestToStoreCreditCardRequestMapper =
        creditCardRequestToStoreCreditCardRequestMapper;
    this.abTestHelper = abTestHelper;
    this.membershipInteractor = membershipInteractor;
    this.totalPriceCalculatorInteractor = totalPriceCalculatorInteractor;
  }

  public void initializePresenter(CreateShoppingCartResponse createShoppingCartResponse,
      boolean isFullTransparency, double lastTicketsPrice, double lastInsurancePrice,
      BigDecimal collectionMethodWithPriceValue) {

    this.createShoppingCartResponse = createShoppingCartResponse;
    this.isFullTransparency = isFullTransparency;
    this.lastTicketsPrice = lastTicketsPrice;
    this.lastInsurancePrice = lastInsurancePrice;
    this.collectionMethodWithPriceValue = collectionMethodWithPriceValue;
    areAllFormFieldsCorrect = false;

    checkRepricing();
    checkPromoCode();
    configureDefaultRadioButton();
  }

  public void setPresenterWidgets(PaymentFormWidgetPresenter paymentFormWidgetPresenter,
      PriceBreakdownWidgetPresenter priceBreakdownWidgetPresenter,
      PromoCodeWidgetPresenter promoCodeWidgetPresenter,
      PaymentPurchasePresenter paymentPurchasePresenter) {
    this.paymentFormWidgetPresenter = paymentFormWidgetPresenter;
    this.priceBreakdownWidgetPresenter = priceBreakdownWidgetPresenter;
    this.promoCodeWidgetPresenter = promoCodeWidgetPresenter;
    this.paymentPurchasePresenter = paymentPurchasePresenter;
    this.promoCodeWidgetPresenter.setPromoCodeWidgetListener(this);
    this.paymentPurchasePresenter.setPaymentPurchaseListener(this);
  }

  private void checkPromoCode() {
    List<PricingBreakdownItem> pricingBreakdownItems =
        priceBreakdownWidgetPresenter.getPricingBreakdownItems();

    if (hasPricingBreakdownPromoCode(pricingBreakdownItems)) {
      String code = promoCodeWidgetPresenter.getExistingCode();
      if (!code.isEmpty()) {
        PromotionalCodeUsageResponse promotionalCodeUsageResponse =
            new PromotionalCodeUsageResponse();
        promotionalCodeUsageResponse.setAmount(getPromoCodeValue(pricingBreakdownItems));
        promotionalCodeUsageResponse.setCode(code);
        promoCodeWidgetPresenter.setPromotionalCode(promotionalCodeUsageResponse);
        promoCodeWidgetPresenter.expandWidget();
      }
    }
  }

  private void checkRepricing() {
    double repricingAmount;

    double currentInsurancesPrices = getInsurancesTotalPrice();
    if (lastInsurancePrice != currentInsurancesPrices) {
      repricingAmount = currentInsurancesPrices - lastInsurancePrice;
      if (repricingAmount != 0) {
        mView.showRepricingInsuranceMessage(repricingAmount);
      }
    }

    double currentTicketsPrices = getTicketsTotalPrice();
    if (lastTicketsPrice != currentTicketsPrices) {
      repricingAmount = currentTicketsPrices - lastTicketsPrice;
      if (repricingAmount != 0) {
        mView.showRepricingTicketsMessage(repricingAmount);
      }
    }
  }

  private double getInsurancesTotalPrice() {
    double total = 0.0;
    int lastStep =
        createShoppingCartResponse.getPricingBreakdown().getPricingBreakdownSteps().size() - 1;
    PricingBreakdownStep step =
        createShoppingCartResponse.getPricingBreakdown().getPricingBreakdownSteps().get(lastStep);
    for (PricingBreakdownItem item : step.getPricingBreakdownItems()) {
      switch (item.getPriceItemType()) {
        case INSURANCE_PRICE:
          total += item.getPriceItemAmount().doubleValue();
          break;
      }
    }
    return total;
  }

  private double getTicketsTotalPrice() {
    double total = 0.0;
    int lastStep =
        createShoppingCartResponse.getPricingBreakdown().getPricingBreakdownSteps().size() - 1;
    PricingBreakdownStep step =
        createShoppingCartResponse.getPricingBreakdown().getPricingBreakdownSteps().get(lastStep);
    for (PricingBreakdownItem item : step.getPricingBreakdownItems()) {
      switch (item.getPriceItemType()) {
        case ADULTS_PRICE:
        case CHILDREN_PRICE:
        case INFANTS_PRICE:
          total += item.getPriceItemAmount().doubleValue();
          break;
      }
    }
    return total;
  }

  private boolean hasPricingBreakdownPromoCode(List<PricingBreakdownItem> pricingBreakdownItems) {
    for (PricingBreakdownItem pricingBreakdownItem : pricingBreakdownItems) {
      if (pricingBreakdownItem.getPriceItemType() == PricingBreakdownItemType.PROMOCODE_DISCOUNT) {
        return true;
      }
    }
    return false;
  }

  private BigDecimal getPromoCodeValue(List<PricingBreakdownItem> pricingBreakdownItems) {
    for (PricingBreakdownItem item : pricingBreakdownItems) {
      if (item.getPriceItemType() == PricingBreakdownItemType.PROMOCODE_DISCOUNT) {
        return item.getPriceItemAmount();
      }
    }
    return BigDecimal.ZERO;
  }

  public void onContinueButtonClick() {
    final CollectionMethodType collectionMethodTypeSelected =
        paymentFormWidgetPresenter.getCollectionMethodTypeDetected();

    mView.trackPaymentMethodSelected(collectionMethodTypeSelected.value());

    switch (collectionMethodTypeSelected) {
      case BANKTRANSFER:
        mView.trackSuccessfulPurchase();
        onContinueButtonClickWithBankTransfer();
        break;
      case CREDITCARD:
        onContinueButtonClickWithCreditCard();
        break;
      case PAYPAL:
      case TRUSTLY:
      case KLARNA:
        onContinueButtonClickWithExternalPayment();
        break;
    }
  }

  private void onContinueButtonClickWithExternalPayment() {
    mView.showLoadingDialog();

    ShoppingCartBookingRequest shoppingCartBookingRequest = new ShoppingCartBookingRequest();
    UserInteractionNeededRequest userInteractionNeedRequest = new UserInteractionNeededRequest();
    userInteractionNeedRequest.setReturnUrl(REDIRECT_URL_EXTERNAL_PAYMENT);

    shoppingCartBookingRequest.setUserInteractionNeededRequest(userInteractionNeedRequest);
    shoppingCartBookingRequest.setCollectionMethodType(
        paymentFormWidgetPresenter.getCollectionMethodTypeDetected());
    BookingRequest bookingRequest = createBookingRequest(shoppingCartBookingRequest);
    confirmBooking(bookingRequest);
  }

  void onContinueButtonClickWithBankTransfer() {
    mView.showLoadingDialog();
    ShoppingCartBookingRequest shoppingCartBookingRequest = new ShoppingCartBookingRequest();
    shoppingCartBookingRequest.setCollectionMethodType(
        paymentFormWidgetPresenter.getCollectionMethodTypeDetected());
    BookingRequest bookingRequest = createBookingRequest(shoppingCartBookingRequest);
    confirmBooking(bookingRequest);
  }

  void onContinueButtonClickWithCreditCard() {
    ShoppingCartBookingRequest shoppingCartBookingRequest = new ShoppingCartBookingRequest();
    shoppingCartBookingRequest.setCollectionMethodType(
        paymentFormWidgetPresenter.getCollectionMethodTypeDetected());
    CreditCardCollectionDetailsParametersRequest creditCardRequest =
        paymentFormWidgetPresenter.createCreditCardRequest();

    if (paymentFormWidgetPresenter.isStorePaymentMethodChecked()) {
      savePaymentMethodRequest =
          creditCardRequestToStoreCreditCardRequestMapper.buildTo(creditCardRequest);
    }

    if (creditCardRequest != null) {
      mView.showLoadingDialog();
      shoppingCartBookingRequest.setCreditCardRequest(creditCardRequest);

      UserInteractionNeededRequest userInteractionNeedRequest = new UserInteractionNeededRequest();
      userInteractionNeedRequest.setReturnUrl(REDIRECT_URL_EXTERNAL_PAYMENT);
      shoppingCartBookingRequest.setUserInteractionNeededRequest(userInteractionNeedRequest);

      BookingRequest bookingRequest = createBookingRequest(shoppingCartBookingRequest);
      confirmBooking(bookingRequest);
    }
  }

  private BookingRequest createBookingRequest(
      ShoppingCartBookingRequest shoppingCartBookingRequest) {
    BookingRequest bookingRequest = new BookingRequest();
    bookingRequest.setBookingId(createShoppingCartResponse.getShoppingCart().getBookingId());
    bookingRequest.setClientBookingReferenceId(
        String.valueOf(createShoppingCartResponse.getShoppingCart().getBookingId()));
    bookingRequest.setSortCriteria(createShoppingCartResponse.getSortCriteria());
    bookingRequest.setInvoice(createInvoice());

    PaymentDataRequest paymentDataRequest = new PaymentDataRequest();
    paymentDataRequest.setBookingRequest(shoppingCartBookingRequest);
    bookingRequest.setPaymentData(paymentDataRequest);

    return bookingRequest;
  }

  private InvoiceRequest createInvoice() {
    InvoiceRequest invoiceRequest = new InvoiceRequest();
    invoiceRequest.setRequired(false);
    invoiceRequest.setProfessional(false);
    return invoiceRequest;
  }

  private void confirmBooking(final BookingRequest bookingRequest) {
    confirmBookingInteractor.confirmBooking(bookingRequest, new OnConfirmBookingListener() {
      @Override public void onSuccess(BookingResponse bookingResponse) {
        processBooking(bookingResponse);
      }

      @Override public void onGeneralError(BookingResponse bookingResponse) {
        mView.hideLoadingDialog();
        mView.showGeneralMslError();
        mView.trackBookingResponseException(bookingResponse);
      }

      @Override public void onNoConnectionError() {
        mView.hideLoadingDialog();
        mView.showNoConnectionActivity();
      }

      @Override public void onInternalError(BookingResponse bookingResponse) {
        mView.hideLoadingDialog();
        mView.showPaymentRetry();
        mView.trackBookingResponseException(bookingResponse);
      }
    });
  }

  private void processBooking(BookingResponse bookingResponse) {
    checkUpdateShoppingCartWithBookingDetail(bookingResponse.getBookingDetail());
    checkUpdatePricingBreakdown(bookingResponse.getPricingBreakdown());

    ABPartition abPartition = abTestHelper.getPartition(ABAlias.BBU_BBUSTERS534, 2);
    if (abPartition == ABPartition.ONE) {
      checkUpdateCollectionOptions(bookingResponse.getCollectionOptions());
    }

    switch (bookingResponse.getStatus()) {
      case USER_INTERACTION_NEEDED:
        mView.hideLoadingDialog();
        mNavigator.navigateToExternalPayment(bookingResponse,
            paymentFormWidgetPresenter.getCollectionMethodTypeDetected());
        break;
      case BOOKING_CONFIRMED:
        bookingResponse.getBookingDetail()
            .setBookingStatus(convertBookingStatusToFinalState(bookingResponse));
        storeBookingOnDB(bookingResponse);
        break;
      case BOOKING_PAYMENT_RETRY:
        mView.hideLoadingDialog();
        if (hasOnlyBTPaymentMethod()) {
          onContinueButtonClickWithBankTransfer();
        } else if (hasVelocityErrors(bookingResponse.getMessages())) {
          mView.showVelocityError(velocityErrorMessage);
        } else {
          mView.showPaymentRetry();
        }
        break;
      case BOOKING_REPRICING:
        mView.hideLoadingDialog();
        mNavigator.setHasBeenRepricing();
        updateTotalPrice();
        break;
      case BOOKING_ERROR:
      case BROKEN_FLOW:
      case BOOKING_STOP:
      case ON_HOLD:
      default:
        goToBookingRejected();
        break;
    }
  }

  private void checkUpdateShoppingCartWithBookingDetail(BookingDetail bookingDetail) {
    if (bookingDetail != null) {
      createShoppingCartResponse.getShoppingCart()
          .setBookingId(bookingDetail.getBookingBasicInfo().getId());
      createShoppingCartResponse.getShoppingCart()
          .setTotalPrice(bookingDetail.getPrice().getAmount());
    }
  }

  private void checkUpdatePricingBreakdown(PricingBreakdown pricingBreakdown) {
    if (pricingBreakdown != null) {
      createShoppingCartResponse.setPricingBreakdown(pricingBreakdown);
      priceBreakdownWidgetPresenter.setPricingBreakdown(pricingBreakdown);
      priceBreakdownWidgetPresenter.setShoppingCart(createShoppingCartResponse.getShoppingCart());
      priceBreakdownWidgetPresenter.updatePricesBreakdown();
    }
  }

  private void checkUpdateCollectionOptions(List<ShoppingCartCollectionOption> collectionOptions) {
    if (collectionOptions != null && collectionOptions.size() > 0) {
      ABPartition abPartition = abTestHelper.getPartition(ABAlias.BBU_BBUSTERS530, 2);
      if (abPartition == ABPartition.ONE) {
        updateCollectionOptionsWithRemoveView(collectionOptions);
      } else {
        updateCollectionOptionsWithRefreshView(collectionOptions);
      }
    }
  }

  private void updateCollectionOptionsWithRemoveView(
      List<ShoppingCartCollectionOption> collectionOptions) {
    createShoppingCartResponse.getShoppingCart().setCollectionOptions(collectionOptions);
    mView.updatePaymentCardDetailsWidget();
    configureDefaultRadioButton();
    onPaymentWidgetValidation(false);
  }

  private void updateCollectionOptionsWithRefreshView(
      List<ShoppingCartCollectionOption> collectionOptions) {
    createShoppingCartResponse.getShoppingCart().setCollectionOptions(collectionOptions);
    paymentFormWidgetPresenter.onRefreshPaymentFormWidgetView(collectionOptions);
  }

  private void updateTotalPrice() {
    BigDecimal newPrice =
        getTotalPriceFromRepricing(createShoppingCartResponse.getPricingBreakdown());
    createShoppingCartResponse.getShoppingCart().setTotalPrice(newPrice);
    mView.showRepricingDialog(newPrice.doubleValue() - lastTicketsPrice);
  }

  private BigDecimal getTotalPriceFromRepricing(PricingBreakdown pricingBreakdown) {
    for (PricingBreakdownStep pricingBreakdownStep : pricingBreakdown.getPricingBreakdownSteps()) {
      if (pricingBreakdownStep.getStep().equals(Step.DEFAULT)) {
        return pricingBreakdownStep.getTotalPrice();
      }
    }
    return pricingBreakdown.getPricingBreakdownSteps().get(0).getTotalPrice();
  }

  private boolean hasOnlyBTPaymentMethod() {
    return createShoppingCartResponse.getShoppingCart().getCollectionOptions().size() == 1
        && createShoppingCartResponse.getShoppingCart()
        .getCollectionOptions()
        .get(0)
        .getMethod()
        .getType()
        .equals(CollectionMethodType.BANKTRANSFER);
  }

  private boolean hasVelocityErrors(List<MessageResponse> messages) {
    if (messages != null && !messages.isEmpty()) {
      for (MessageResponse message : messages) {
        if (message.getCode().equalsIgnoreCase(DAPIError.VEL_003.getDapiError())) {
          velocityErrorMessage = message.getDescription();
          return true;
        }
      }
    }
    return false;
  }

  private void goToBookingRejected() {
    BookingDetail bookingDetail = new BookingDetail();
    bookingDetail.setBookingStatus(BookingStatus.REJECTED);
    mNavigator.navigateToBookingRejected(bookingDetail);
  }

  private BookingStatus convertBookingStatusToFinalState(BookingResponse bookingResponse) {
    switch (bookingResponse.getBookingDetail().getBookingStatus()) {
      case CONTRACT:
        return BookingStatus.CONTRACT;
      case REJECTED:
      case FINAL_RET:
        return BookingStatus.REJECTED;
      case REQUEST:
        return bookingResponse.getBookingDetail().getCollectionState().
            equals(ShoppingCartCollectionState.COLLECTED.value()) ? BookingStatus.CONTRACT
            : BookingStatus.PENDING;
      case PENDING:
      case HOLD:
      case RETAINED:
      case UNKNOWN:
        return BookingStatus.PENDING;
      default:
        return BookingStatus.REJECTED;
    }
  }

  private void storeBookingOnDB(final BookingResponse bookingResponse) {
    myTripsManualImportInteractor.importTrip(new OnSaveImportedTripListener() {
                                               @Override public void onResponse(Booking booking, boolean wasSave) {

                                                 mNavigator.navigateToBookingConfirmed(savePaymentMethodRequest,
                                                     paymentFormWidgetPresenter.getCardNumberObfuscated(), createShoppingCartResponse,
                                                     bookingResponse.getBookingDetail(), bookingResponse.getMarketingRevenue(),
                                                     bookingResponse.getBankTransferResponse(), bookingResponse.getBankTransferData(),
                                                     booking, shoppingCartCollectionOption);
                                               }

                                               @Override public void onError(MslError error, String message) {

                                                 mNavigator.navigateToBookingConfirmed(savePaymentMethodRequest,
                                                     paymentFormWidgetPresenter.getCardNumberObfuscated(), createShoppingCartResponse,
                                                     bookingResponse.getBookingDetail(), bookingResponse.getMarketingRevenue(),
                                                     bookingResponse.getBankTransferResponse(), bookingResponse.getBankTransferData(), null,
                                                     shoppingCartCollectionOption);
                                               }
                                             }, createShoppingCartResponse.getShoppingCart().getBuyer().getMail(),
        Long.toString(createShoppingCartResponse.getShoppingCart().getBookingId()));
  }

  public void processMockedResponse(BookingStatus bookingStatus) {
    BookingDetail bookingDetail = new BookingDetail();
    bookingDetail.setBookingStatus(bookingStatus);
    if (bookingStatus == BookingStatus.PENDING) {
      bookingDetail.setCollectionState(ShoppingCartCollectionState.NOT_COLLECTED.value());
    }

    mNavigator.navigateToMockBooking(bookingDetail, mView.getMockedBooking());
  }

  public void onCollectionOptionSelected(CollectionMethodType collectionMethodType) {
    for (ShoppingCartCollectionOption collectionOption : createShoppingCartResponse.getShoppingCart()
        .getCollectionOptions()) {
      if (collectionOption.getMethod().getType() == collectionMethodType) {
        shoppingCartCollectionOption = collectionOption;
        priceBreakdownWidgetPresenter.setCollectionMethodType(collectionMethodType);
        priceBreakdownWidgetPresenter.setShoppingCartCollectionOption(shoppingCartCollectionOption);
        priceBreakdownWidgetPresenter.updatePricesBreakdown();
        paymentPurchasePresenter.onValidateOptionsChange(false);
      }
    }
  }

  public void onPaymentMethodSelected(String creditCardType, boolean haveValidationOptionsChanged) {
    for (ShoppingCartCollectionOption collectionOption : createShoppingCartResponse.getShoppingCart()
        .getCollectionOptions()) {
      if (collectionOption.getMethod().getCreditCardType() != null && collectionOption.getMethod()
          .getCreditCardType()
          .getCode()
          .equals(creditCardType)) {
        shoppingCartCollectionOption = collectionOption;
        priceBreakdownWidgetPresenter.setCollectionMethodType(CollectionMethodType.CREDITCARD);
        priceBreakdownWidgetPresenter.setShoppingCartCollectionOption(shoppingCartCollectionOption);
        priceBreakdownWidgetPresenter.updatePricesBreakdown();
        paymentPurchasePresenter.onValidateOptionsChange(haveValidationOptionsChanged);
      }
    }
  }

  public void onPaymentWidgetValidation(boolean areFieldsCorrect) {
    areAllFormFieldsCorrect = areFieldsCorrect;
    paymentPurchasePresenter.onValidateOptionsChange(areFieldsCorrect);
  }

  public void onExternalPaymentFinished(ResumeDataRequest resumeDataRequest, boolean test) {
    ResumeBooking resumeBooking = new ResumeBooking();
    resumeBooking.setResumeData(resumeDataRequest);

    if (!test) {
      resumeBooking.setBookingId(createShoppingCartResponse.getShoppingCart().getBookingId());
      resumeBooking.setSortCriteria(createShoppingCartResponse.getSortCriteria());
    } else {
      resumeBooking.setBookingId(RANDOM_BOOKING_ID);
      resumeBooking.setSortCriteria(null);
    }

    mView.showLoadingResumeBooking();
    resumeBookingInteractor.resumeBooking(resumeBooking, new OnResumeBookingListener() {

      @Override public void onSuccess(BookingResponse bookingResponse) {
        processBooking(bookingResponse);
      }

      @Override public void onGeneralError(BookingResponse bookingResponse) {
        mView.hideLoadingDialog();
        mView.showGeneralMslError();
        mView.trackBookingResponseException(bookingResponse);
      }

      @Override public void onNoConnectionError() {
        mView.hideLoadingDialog();
      }

      @Override public void onInternalError(BookingResponse bookingResponse) {
        mView.hideLoadingDialog();
        mView.showPaymentRetry();
        mView.trackBookingResponseException(bookingResponse);
      }
    });
  }

  @Override public void onAddPromoCodeSuccessful(CreateShoppingCartResponse shoppingCartResponse) {
    createShoppingCartResponse.setPricingBreakdown(shoppingCartResponse.getPricingBreakdown());
    createShoppingCartResponse.getShoppingCart()
        .setTotalPrice(shoppingCartResponse.getShoppingCart().getTotalPrice());
    createShoppingCartResponse.getShoppingCart()
        .setPromotionalCodes(shoppingCartResponse.getShoppingCart().getPromotionalCodes());

    priceBreakdownWidgetPresenter.setPricingBreakdown(
        createShoppingCartResponse.getPricingBreakdown());
    priceBreakdownWidgetPresenter.setShoppingCart(createShoppingCartResponse.getShoppingCart());
    priceBreakdownWidgetPresenter.updatePricesBreakdown();

    if (isFullTransparency) {
      mView.updateTopBrief();
    }
  }

  @Override
  public void onDeletePromoCodeSuccessful(CreateShoppingCartResponse shoppingCartResponse) {
    createShoppingCartResponse.setPricingBreakdown(shoppingCartResponse.getPricingBreakdown());
    createShoppingCartResponse.getShoppingCart()
        .setTotalPrice(shoppingCartResponse.getShoppingCart().getTotalPrice());
    createShoppingCartResponse.getShoppingCart()
        .setPromotionalCodes(shoppingCartResponse.getShoppingCart().getPromotionalCodes());

    priceBreakdownWidgetPresenter.setPricingBreakdown(
        createShoppingCartResponse.getPricingBreakdown());
    priceBreakdownWidgetPresenter.setShoppingCart(createShoppingCartResponse.getShoppingCart());
    priceBreakdownWidgetPresenter.updatePricesBreakdown();

    if (isFullTransparency) {
      mView.updateTopBrief();
    }
  }

  @Override public void onOutdatedBookingId() {
    mView.showOutdatedBookingId();
  }

  @Override public void onGeneralMslError() {
    mView.showGeneralMslError();
  }

  @Override public void onExplicitAcceptanceChecked(boolean checked) {
    paymentPurchasePresenter.onValidateOptionsChange(checked && areAllFormFieldsCorrect);
  }

  @Override public void onPaymentPurchaseContinue() {
    onContinueButtonClick();
  }

  private void configureDefaultRadioButton() {
    paymentFormWidgetPresenter.checkDefaultSavedPaymentMethodRadioButton();
  }

  private void resetRadioButtons() {
    paymentFormWidgetPresenter.unCheckSavedPaymentMethodRadioButtons();
  }

  public void trackLoggedUserHasPaymentMethods() {
    paymentFormWidgetPresenter.trackLoggedUserWithPaymentMethods();
  }

  public boolean isMembershipForCurrentMarket() {
    return membershipInteractor.isMemberForCurrentMarket();
  }

  public boolean shouldApplyMembershipPerks() {
    return totalPriceCalculatorInteractor.shouldApplyMembershipPerks(
        createShoppingCartResponse.getPricingBreakdown());
  }

  public Double getTotalPrice() {
    return totalPriceCalculatorInteractor.getTotalPrice(
        createShoppingCartResponse.getShoppingCart(),
        createShoppingCartResponse.getPricingBreakdown(), collectionMethodWithPriceValue,
        Step.PAYMENT);
  }
}