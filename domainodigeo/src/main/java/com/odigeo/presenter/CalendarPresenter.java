package com.odigeo.presenter;

import com.odigeo.builder.CalendarControllerBuilder;
import com.odigeo.data.calendar.controllers.CalendarController;
import com.odigeo.data.entity.TravelType;
import com.odigeo.presenter.contracts.navigators.CalendarNavigatorInterface;
import com.odigeo.presenter.contracts.views.CalendarViewInterface;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CalendarPresenter
    extends BasePresenter<CalendarViewInterface, CalendarNavigatorInterface> {

  private CalendarController calendarController;

  private List<Date> selectedDates;
  private List<Date> countryHolidays;
  private List<Date> regionHolidays;

  private TravelType travelType;
  private int segmentNumber;

  private Date minDate;
  private Date maxDate;

  public CalendarPresenter(CalendarViewInterface view, CalendarNavigatorInterface navigator) {
    super(view, navigator);
  }

  public void initializePresenter(int segmentNumber, TravelType travelType,
      List<Date> selectedDates) {
    this.segmentNumber = segmentNumber;
    this.travelType = travelType;
    this.selectedDates = (selectedDates == null ? new ArrayList<Date>() : selectedDates);

    this.calendarController = new CalendarControllerBuilder().build(travelType, this);
    this.calendarController.initCalendar(selectedDates, segmentNumber);
  }

  public void setSelectorBackground() {
    mView.setSelectorBackground(travelType);
  }

  public void setCalendarBounds(Date minDate, Date maxDate, int mode, boolean avoidScrolling) {
    mView.setCalendarBounds(minDate, maxDate, selectedDates, mode, avoidScrolling);
  }

  public void setDepartureDateHeader(Date departureDate) {
    mView.setDepartureDateHeader(departureDate);
  }

  public void resetDepartureDateHeader() {
    mView.resetDepartureDateHeader();
  }

  public void setReturnDateHeader(Date returnDate) {
    mView.setReturnDateHeader(returnDate);
  }

  public void resetReturnDateHeader() {
    mView.resetReturnDateHeader();
  }

  public void showSkipReturnButton() {
    mView.showSkipReturnButton();
  }

  public void hideSkipReturnButton() {
    mView.hideSkipReturnButton();
  }

  public final void showSelectedDates() {
    calendarController.showSelectedDates(selectedDates, false);
  }

  public void showConfirmationButton() {
    mView.showConfirmationButton();
  }

  public void hideConfirmationButton() {
    mView.hideConfirmationButton();
  }

  public void setConfirmationButtonDates(Date departureDate, Date returnDate) {
    mView.setConfirmationButtonDates(departureDate, returnDate);
  }

  public void setConfirmationButtonDates(Date departureDate) {
    mView.setConfirmationButtonDate(departureDate);
  }

  public void showDatesHeader() {
    mView.showRoundTripHeader();
  }

  public void hideDatesHeader() {
    mView.hideRoundTripHeader();
  }

  public void onDateSelected(Date date) {
    calendarController.dateSelected(selectedDates, date, segmentNumber);
  }

  public void setCountryHolidays(List<Date> countryHolidays) {
    this.countryHolidays = countryHolidays;
  }

  public void setRegionHolidays(List<Date> regionHolidays) {
    this.regionHolidays = regionHolidays;
  }

  public boolean isCountryHoliday(Date date) {
    if (countryHolidays != null) {
      return countryHolidays.contains(date);
    }

    return false;
  }

  public boolean isRegionHoliday(Date date) {
    if (regionHolidays != null) {
      return regionHolidays.contains(date);
    }

    return false;
  }

  public void onConfirmationButtonClicked() {
    getNavigatorInterface().setResultAndFinish(segmentNumber, travelType, selectedDates);
  }

  public void onSkipReturnButtonClicked() {
    travelType = TravelType.SIMPLE;
    calendarController = new CalendarControllerBuilder().build(travelType, this);
    getNavigatorInterface().setResultAndFinish(segmentNumber, travelType, selectedDates);
  }

  public void setCalendarFilter() {
    mView.setSegmentCalendarFilter();
  }

  public boolean isDateSelectable(Date dateInLocal) {
    return calendarController.isDateSelectable(selectedDates, dateInLocal, segmentNumber);
  }

  public TravelType getTravelType() {
    return travelType;
  }

  public Date getMinDate() {
    return minDate;
  }

  public void setMinDate(Date minDate) {
    this.minDate = minDate;
  }

  public Date getMaxDate() {
    return maxDate;
  }

  public void setMaxDate(Date maxDate) {
    this.maxDate = maxDate;
  }

  public int getSegmentNumber() {
    return segmentNumber;
  }
}
