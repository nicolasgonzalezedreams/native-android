package com.odigeo.presenter;

import com.odigeo.presenter.contracts.navigators.FrequentFlyerCodesNavigatorInterface;
import com.odigeo.presenter.contracts.views.FrequentFlyerDetailViewInterface;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/09/15
 */
public class FrequentFlyerDetailPresenter
    extends BasePresenter<FrequentFlyerDetailViewInterface, FrequentFlyerCodesNavigatorInterface> {

  public FrequentFlyerDetailPresenter(FrequentFlyerDetailViewInterface view,
      FrequentFlyerCodesNavigatorInterface navigator) {
    super(view, navigator);
  }
}
