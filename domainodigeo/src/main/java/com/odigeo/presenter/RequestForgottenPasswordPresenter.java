package com.odigeo.presenter;

import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.LogoutInteractor;
import com.odigeo.interactors.RequestForgottenPasswordInteractor;
import com.odigeo.presenter.contracts.navigators.RequestForgottenPasswordNavigatorInterface;
import com.odigeo.presenter.contracts.views.RequestForgottenPasswordViewInterface;
import com.odigeo.presenter.listeners.UserRequestForgottenPasswordListener;

public class RequestForgottenPasswordPresenter extends
    BasePresenter<RequestForgottenPasswordViewInterface, RequestForgottenPasswordNavigatorInterface> {

  private final RequestForgottenPasswordInteractor mRequestForgottenPasswordInteractor;
  private LogoutInteractor mLogoutInteractor;
  private SessionController mSessionController;
  private TrackerControllerInterface mTrackerController;

  public RequestForgottenPasswordPresenter(RequestForgottenPasswordViewInterface view,
      RequestForgottenPasswordInteractor requestForgottenPasswordInteractor,
      RequestForgottenPasswordNavigatorInterface navigator, LogoutInteractor logoutInteractor,
      SessionController sessionController,
      TrackerControllerInterface trackerControllerInterface) {
    super(view, navigator);
    this.mRequestForgottenPasswordInteractor = requestForgottenPasswordInteractor;
    mLogoutInteractor = logoutInteractor;
    mSessionController = sessionController;
    mTrackerController = trackerControllerInterface;
  }

  public void requestForgottenPassword(final String email) {
    mView.showProgress(email);
    mRequestForgottenPasswordInteractor.requestForgottenPassword(
        new UserRequestForgottenPasswordListener() {
          @Override public void onUserRequestForgottenPasswordOk() {
            mView.hideProgress();
            mNavigator.showInstructionsPage(email);
          }

          @Override public void onUserRequestForgottenPasswordFail() {
            mTrackerController.trackApiRecoverPasswordError();
            mView.hideProgress();
            mView.showUnknownError();
          }

          @Override public void onUserNameError(boolean empty) {
            if (empty) {
              mView.setUsernameEmpty();
            } else {
              mView.setUsernameError();
            }
          }

          @Override public void onUserDoesNotExist() {
            mView.hideProgress();
            mView.showUserDoesNotExistError();
          }

          @Override
          public void onUserRequestForgottenPasswordFailTryFacebookOrGoogle(String email) {
            mView.hideProgress();
            mView.showConnectionErrorTryFacebookOrGooglePlus(email);
          }

          @Override public void onUserAuthError() {
            mLogoutInteractor.logout(new OnRequestDataListener<Boolean>() {
              @Override public void onResponse(Boolean object) {
                mView.showAuthError();
                mSessionController.saveLogoutWasMade(true);
              }

              @Override public void onError(MslError error, String message) {
                mView.onLogoutError(error, message);
              }
            });
          }

          @Override public void onAccountInactive(String email) {
            mView.hideProgress();
            mView.showAccountInactiveError(email);
          }
        }, email);
  }

  public final void validateUsernameFormat(String username) {
    boolean isValid = mRequestForgottenPasswordInteractor.validateUsernameFormat(username);
    mView.showEmailFormatError(isValid);
    mView.enableButton(isValid);
  }

  public void onAuthOkClicked() {
    mNavigator.navigateToLogin();
  }
}
