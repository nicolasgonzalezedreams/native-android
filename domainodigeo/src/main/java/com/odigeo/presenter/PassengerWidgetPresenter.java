package com.odigeo.presenter;

import com.odigeo.data.entity.parser.TravellerRequestToUserTravellerParser;
import com.odigeo.data.entity.parser.UpdateUserTravellerWithTravellerRequestAndBuyerRequest;
import com.odigeo.data.entity.shoppingCart.AirlineGroup;
import com.odigeo.data.entity.shoppingCart.Carrier;
import com.odigeo.data.entity.shoppingCart.Itinerary;
import com.odigeo.data.entity.shoppingCart.ItinerarySortCriteria;
import com.odigeo.data.entity.shoppingCart.RequiredField;
import com.odigeo.data.entity.shoppingCart.ResidentGroupContainer;
import com.odigeo.data.entity.shoppingCart.ResidentGroupLocality;
import com.odigeo.data.entity.shoppingCart.TravellerIdentificationType;
import com.odigeo.data.entity.shoppingCart.TravellerRequiredFields;
import com.odigeo.data.entity.shoppingCart.TravellerType;
import com.odigeo.data.entity.shoppingCart.request.TravellerRequest;
import com.odigeo.data.entity.userData.Membership;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.data.entity.userData.UserTravellerFactory;
import com.odigeo.helper.CreateTravellerRequestHelper;
import com.odigeo.interactors.CheckUserBirthdayInteractor;
import com.odigeo.interactors.GetTravellersByTypeInteractor;
import com.odigeo.interactors.MembershipInteractor;
import com.odigeo.interactors.SortItinerarySectionsInteractor;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.presenter.contracts.components.BaseCustomComponentPresenter;
import com.odigeo.presenter.contracts.views.PassengerWidget;
import com.odigeo.presenter.listeners.OnUserTravellerRequestListener;
import com.odigeo.tools.ConditionRulesHelper;
import com.odigeo.tools.DateHelperInterface;
import com.odigeo.validations.BirthdateValidator;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.jetbrains.annotations.Nullable;

import static com.odigeo.data.entity.shoppingCart.TravellerType.CHILD;
import static com.odigeo.data.entity.shoppingCart.TravellerType.INFANT;

public class PassengerWidgetPresenter extends BaseCustomComponentPresenter<PassengerWidget> {

  private static final String FREQUENT_FLYER_CODE_SEPARATOR = " - ";
  private static final String FREQUENT_FLYER_SEPARATOR = ", ";
  private static final String FREQUENT_FLYER_DOTS_LENGTH_EXCEED = "...";
  private static final int FREQUENT_FLYER_MAX_LENGHT = 28;
  private static final boolean THERE_IS_NO_BUYER = false;
  private static final boolean THERE_IS_NO_DEFAULT_PASSENGER = false;
  private static final String SPANISH_MARKET = "es_ES";
  private static final int NO_BUYER = -1;
  private static int DAY = 0;
  private static int MONTH = 1;
  private static int YEAR = 2;
  private final ConditionRulesHelper mConditionRulesHelper;
  private final UpdateUserTravellerWithTravellerRequestAndBuyerRequest
      mUpdateUserTravellerWithTravellerRequestAndBuyerRequest;
  private final TravellerRequestToUserTravellerParser mTravellerRequestToUserTravellerParser;
  private UserTraveller mSelectedTraveller;
  private UserTraveller buyerMembershipTraveller;
  private List<UserTraveller> mTravellersGroupByWidgetType;
  private int mSelectedTravellerPosition = -1;
  private int selectedMembershipTravellerPosition = NO_BUYER;
  private TravellerRequiredFields mPassengerInformation;
  private CheckUserBirthdayInteractor mCheckUserBirthdayInteractor;
  private GetTravellersByTypeInteractor mGetTravellersByTypeInteractor;
  private DateHelperInterface mDateHelperInterface;
  private HashMap<String, String> mResidentLocalities;
  private String mEmptyPassenger;
  private MarketProviderInterface marketProvider;
  private BirthdateValidator birthdateValidator;
  private SortItinerarySectionsInteractor sortItineraryInteractor;
  private MembershipInteractor membershipInteractor;
  private boolean membershipWasSelected = false;

  public PassengerWidgetPresenter(PassengerWidget view,
      CheckUserBirthdayInteractor checkUserBirthdayInteractor,
      DateHelperInterface dateHelperInterface, ConditionRulesHelper conditionRulesHelper,
      UpdateUserTravellerWithTravellerRequestAndBuyerRequest updateUserTravellerWithTravellerRequestAndBuyerRequest,
      TravellerRequestToUserTravellerParser travellerRequestToUserTravellerParser,
      GetTravellersByTypeInteractor getTravellersByTypeInteractor,
      SortItinerarySectionsInteractor sortItinerarySectionsInteractor,
      MarketProviderInterface marketProviderInterface, MembershipInteractor membershipInteractor) {
    super(view);
    mCheckUserBirthdayInteractor = checkUserBirthdayInteractor;
    mDateHelperInterface = dateHelperInterface;
    mConditionRulesHelper = conditionRulesHelper;
    mUpdateUserTravellerWithTravellerRequestAndBuyerRequest =
        updateUserTravellerWithTravellerRequestAndBuyerRequest;
    mTravellerRequestToUserTravellerParser = travellerRequestToUserTravellerParser;
    mGetTravellersByTypeInteractor = getTravellersByTypeInteractor;
    this.sortItineraryInteractor = sortItinerarySectionsInteractor;
    this.marketProvider = marketProviderInterface;
    this.membershipInteractor = membershipInteractor;
  }

  public void setPassengerInformation(TravellerRequiredFields passengerInformation,
      BirthdateValidator birthdateValidator) {
    mPassengerInformation = passengerInformation;
    mConditionRulesHelper.setTravellerInformationFieldConditionRuleList(
        mPassengerInformation.getConditionallyNeededFields());
    this.birthdateValidator = birthdateValidator;
  }

  public void setUserTravellers(List<UserTraveller> userTravellers) {
    mTravellersGroupByWidgetType = userTravellers;
    buyerMembershipTraveller = getBuyerMembershipTraveller();
  }

  public UserTraveller getSelectedSpinnerTraveller() {
    return mSelectedTraveller;
  }

  public void setSelectedSpinnerTraveller(int index) {
    mSelectedTraveller = mTravellersGroupByWidgetType.get(index);
    mSelectedTravellerPosition = index;
  }

  public void setInitialSpinnerTraveller() {
    if (isThisFirstWidget()) {
      setFirstWidgetSpinnerTraveller();
    }
  }

  private void setFirstWidgetSpinnerTraveller() {
    if (buyerMembershipTraveller == null) {
      findBuyer();
    } else if (membershipInteractor.isMemberForCurrentMarket()) {
      mView.showSpinnerMembershipTitle();
      mView.showImageMembershipProfile();
    }
  }

  private void findBuyer() {
    if (findAndSetBuyerSpinnerTraveller() == THERE_IS_NO_BUYER) {
      findAndSetNormalPassengerSpinnerTraveller();
    }
  }

  private boolean findAndSetBuyerSpinnerTraveller() {
    for (int i = 0; i < mTravellersGroupByWidgetType.size(); i++) {
      if (mTravellersGroupByWidgetType.get(i).getBuyer()) {
        mSelectedTravellerPosition = i;
        mSelectedTraveller = mTravellersGroupByWidgetType.get(i);
        return true;
      }
    }
    return false;
  }

  private void findAndSetNormalPassengerSpinnerTraveller() {
    if (findAndSetDefaultPassengerSpinnerTraveller() == THERE_IS_NO_DEFAULT_PASSENGER) {
      findAndSetNextAdultPassengerSpinnerTraveller();
    }
  }

  private boolean findAndSetDefaultPassengerSpinnerTraveller() {
    for (int i = 0; i < mTravellersGroupByWidgetType.size(); i++) {
      if (mTravellersGroupByWidgetType.get(i).getUserProfile().isDefaultTraveller()) {
        mSelectedTravellerPosition = i;
        mSelectedTraveller = mTravellersGroupByWidgetType.get(i);
        return true;
      }
    }
    return false;
  }

  private void findAndSetNextAdultPassengerSpinnerTraveller() {
    if (mTravellersGroupByWidgetType.size() > 0) {
      int travellerPosition = mView.getWidgetPosition();
      mSelectedTravellerPosition = travellerPosition;
      mSelectedTraveller = mTravellersGroupByWidgetType.get(travellerPosition);
    }
  }

  public void configureWidgetFields() {
    configureCardTittle();
    configureImagePassanger();
    configureHeaderSpinner();
    configureSpinnerName();
    configureTitles();
    configureName();
    configureMiddleName();
    configureLastName();
    configureSecondLastname();
    configureNationality();
    configureResidentGroup();
    configureBirthdateField();
    configureCountryOfResidence();
    configureIdentificationField();
    configureIdentificationExpiration();
    configureIdentificationCountry();
    configureFrequentFlyerField();
  }

  private void configureCardTittle() {
    if (mPassengerInformation.getTravellerType() == TravellerType.ADULT) {
      mView.showCardTitleAdult();
    } else if (mPassengerInformation.getTravellerType() == TravellerType.CHILD) {
      mView.showCardTitleChild();
    } else {
      mView.showCardTitleInfant();
    }
  }

  private void configureImagePassanger() {
    mView.showImagePassengerProfile();
  }

  private void configureHeaderSpinner() {
    if (!isThereLocalData()) {
      mView.hidePassengerHeaderSpinner();
      mView.hideButtonOpenOrCloseDetails();
    }
  }

  private void configureSpinnerName() {
    if (isThisFirstWidget()) {
      mView.showSpinnerName(mSelectedTravellerPosition);
    }
  }

  private void configureTitles() {
    if (mPassengerInformation.getNeedsTitle() != RequiredField.MANDATORY
        && !mConditionRulesHelper.isTitleMandatory()
        && !mConditionRulesHelper.isGenderMandatory()) {
      mView.hideTitle();
    } else {
      showTitle();
    }
  }

  private void configureName() {
    if ((mPassengerInformation.getNeedsName() == RequiredField.MANDATORY)
        || (mConditionRulesHelper.isNameMandatory())) {
      showName();
    } else if (!mConditionRulesHelper.isFirstLastNameMandatory()) {
      mView.hideNameEditable();
    }
  }

  private void configureLastName() {
    if ((mPassengerInformation.getNeedsName() == RequiredField.MANDATORY)
        || (mConditionRulesHelper.isFirstLastNameMandatory())) {
      showLastName();
    } else {
      mView.hideLastNameEditable();
    }
  }

  private void configureMiddleName() {
    if (!mConditionRulesHelper.isMiddleNameMandatory()) {
      mView.hideMiddleNameEditable();
    } else {
      showMiddleName();
    }
  }

  private void configureSecondLastname() {
    String market = marketProvider.getLocale();
    if ((!mConditionRulesHelper.isSecondLastNameMandatory()) && (!market.equals(SPANISH_MARKET))) {
      mView.hideSecondLastNameEditable();
    } else {
      showSecondLastName();
    }
  }

  private void configureCountryOfResidence() {
    if (mPassengerInformation.getNeedsCountryOfResidence() != RequiredField.MANDATORY
        && !mConditionRulesHelper.isCountryResidentMandatory()) {
      mView.hideCountryOfResidence();
    }
  }

  private void configureIdentificationCountry() {
    if (mPassengerInformation.getNeedsIdentificationCountry() != RequiredField.MANDATORY
        && !mConditionRulesHelper.isIdentificationCountryMandatory()) {
      mView.hideIdentificationCountry();
    } else {
      mView.setExpirationIdentificationCalendarMaxAndMin();
      showIdentificationCountry();
    }
  }

  private void configureIdentificationExpiration() {
    if (mPassengerInformation.getNeedsIdentificationExpirationDate() != RequiredField.MANDATORY
        && !mConditionRulesHelper.isIdentificationExpirationDateMandatory()) {
      mView.hideIdentificationExpirationDate();
    } else {
      showExpirationDate();
    }
  }

  private void configureNationality() {
    if (mPassengerInformation.getNeedsNationality() != RequiredField.MANDATORY
        && !mConditionRulesHelper.isNationalityMandatory()) {
      mView.hideNationality();
    } else {
      showNationality();
    }
  }

  private void configureResidentGroup() {
    if ((mPassengerInformation.getResidentGroups() == null)
        || (mPassengerInformation.getResidentGroups().isEmpty())) {
      mView.hideResidentWidgetGroup();
    } else {
      initResidentGroup();
      initLocalities();
    }
  }

  private void initResidentGroup() {
    if (mResidentLocalities == null) {
      List<String> residentGroup = new ArrayList<>();
      for (ResidentGroupContainer residentGroupContainer : mPassengerInformation.getResidentGroups()) {
        residentGroup.add(residentGroupContainer.getName().value());
      }
      mView.initResidentGroupAdapter(residentGroup);
    }
  }

  private void initLocalities() {
    if (mResidentLocalities == null) {
      List<String> residentLocalities = new ArrayList<>();
      mResidentLocalities = new HashMap<>();
      for (ResidentGroupContainer residentGroupContainer : mPassengerInformation.getResidentGroups()) {
        for (ResidentGroupLocality residentGroupLocality : residentGroupContainer.getResidentLocalities()) {
          mResidentLocalities.put(residentGroupLocality.getName(), residentGroupLocality.getCode());
          residentLocalities.add(residentGroupLocality.getName());
        }
      }
      mView.initResidentLocalitiesAdapter(residentLocalities);
    }
  }

  private void configureBirthdateField() {
    if (mPassengerInformation.getNeedsBirthDate() != RequiredField.MANDATORY
        && !mConditionRulesHelper.isBirthDateMandatory()
        && !mConditionRulesHelper.isAgeMandatory()) {
      mView.hideBirthdate();
    } else {
      TravellerType widgetTravellerType = mPassengerInformation.getTravellerType();
      Date minValidDate = mCheckUserBirthdayInteractor.getMinValidDate(widgetTravellerType);
      Date maxValidDate = mCheckUserBirthdayInteractor.getMaxValidDate(widgetTravellerType);
      mView.setBirthDateCalendarMaxAndMin(minValidDate, maxValidDate);
      showBirthdate();
    }
  }

  private void configureIdentificationField() {
    if (mPassengerInformation.getNeedsIdentification() != RequiredField.MANDATORY
        && !mConditionRulesHelper.isIdentificationMandatory()
        && !mConditionRulesHelper.isIdentificationTypeMandatory()) {
      mView.hideIdentification();
    } else {
      checkIdentificationNumber();
    }
    configureIdentificationFieldSpinner();
  }

  private void configureIdentificationFieldSpinner() {
    int UNIQUE_IDENTIFICATION = 1;
    if (mPassengerInformation.getIdentificationTypes().size() == UNIQUE_IDENTIFICATION) {
      mView.enableIdentificationTypeUniqueOption();
    } else {
      mView.enableIdentificationTypeMultipleOptions();
    }
  }

  private void configureFrequentFlyerField() {
    if (!isAvailableFrequentFlyer()) {
      mView.hideFrequentFlyer();
    } else {
      showFrequentFlyer();
    }
  }

  public void configureCollapse() {
    if ((mSelectedTraveller != null) && (!mView.isThereAnyTextInputLayoutEmpty())) {
      mView.hideFieldsContainer();
      mView.showCollapseEditText();
    } else {
      mView.showCollapseCloseText();
    }
  }

  public void loadInformationInUI(int position) {
    setSelectedSpinnerTraveller(position);
    cleanPassengerFields();
    configureWidgetFields();
  }

  private void showTitle() {
    if ((mSelectedTraveller != null) && (mSelectedTraveller.getUserProfile() != null) && (
        mSelectedTraveller.getUserProfile().getTitle()
            != null)) {
      switch (mSelectedTraveller.getUserProfile().getTitle()) {
        case MR:
          mView.selectTitleMR();
          break;
        case MRS:
          mView.selectTitleMRS();
          break;
      }
    }
  }

  public String transformMarketTittleToGeneric(String tittleSelected, String MR) {
    if (tittleSelected.equals(MR)) {
      return UserProfile.Title.MR.name();
    } else {
      return UserProfile.Title.MRS.name();
    }
  }

  private void showName() {
    if ((mSelectedTraveller != null) && (mSelectedTraveller.getUserProfile() != null) && (
        mSelectedTraveller.getUserProfile().getName()
            != null) && (!mSelectedTraveller.getUserProfile().getName().isEmpty()) && (
        mEmptyPassenger
            != null) && (!mEmptyPassenger.equals(mSelectedTraveller.getUserProfile().getName()))) {
      String name = mSelectedTraveller.getUserProfile().getName();
      mView.showName(name);
    }
  }

  private void showMiddleName() {
    if ((mSelectedTraveller != null) && (mSelectedTraveller.getUserProfile() != null) && (
        mSelectedTraveller.getUserProfile().getMiddleName()
            != null) && (!mSelectedTraveller.getUserProfile().getMiddleName().isEmpty())) {
      String middleName = mSelectedTraveller.getUserProfile().getMiddleName();
      mView.showMiddleName(middleName);
    }
  }

  private void showLastName() {
    if ((mSelectedTraveller != null) && (mSelectedTraveller.getUserProfile() != null) && (
        mSelectedTraveller.getUserProfile().getFirstLastName()
            != null) && (!mSelectedTraveller.getUserProfile().getFirstLastName().isEmpty())) {
      String lastName = mSelectedTraveller.getUserProfile().getFirstLastName();
      mView.showLastName(lastName);
    }
  }

  private void showSecondLastName() {
    if ((mSelectedTraveller != null) && (mSelectedTraveller.getUserProfile() != null) && (
        mSelectedTraveller.getUserProfile().getSecondLastName()
            != null) && (!mSelectedTraveller.getUserProfile().getSecondLastName().isEmpty())) {
      String secondLastName = mSelectedTraveller.getUserProfile().getSecondLastName();
      mView.showSecondLastname(secondLastName);
    }
  }

  private void showBirthdate() {
    if ((mSelectedTraveller != null) && (mSelectedTraveller.getUserProfile() != null) && (
        mSelectedTraveller.getUserProfile().getBirthDate()
            != 0)) {
      long birthDate = mSelectedTraveller.getUserProfile().getBirthDate();
      TravellerType travellerType =
          TravellerType.fromValue(mSelectedTraveller.getTypeOfTraveller().toString());
      Date minValidDate = mCheckUserBirthdayInteractor.getMinValidDate(travellerType);
      Date maxValidDate = mCheckUserBirthdayInteractor.getMaxValidDate(travellerType);
      if (birthDate >= minValidDate.getTime() && birthDate <= maxValidDate.getTime()) {
        List<Integer> dayMonthYear = mDateHelperInterface.getDayMonthYearFromTimeStamp(birthDate);
        int day = dayMonthYear.get(DAY);
        int month = dayMonthYear.get(MONTH) + 1;
        int year = dayMonthYear.get(YEAR);
        String birthdate = day + "-" + month + "-" + year;
        mView.showBirthdate(birthdate);
      }
    }
  }

  private void showNationality() {
    if ((mSelectedTraveller != null) && (mSelectedTraveller.getUserProfile() != null) && (
        mSelectedTraveller.getUserProfile().getNationalityCountryCode()
            != null)) {
      String nationality = mSelectedTraveller.getUserProfile().getNationalityCountryCode();
      mView.showNationality(nationality);
    }
  }

  private void checkIdentificationNumber() {
    if (userHaveIdentificationList()) {
      for (TravellerIdentificationType travellerIdentificationType : mPassengerInformation.getIdentificationTypes()) {
        compareTravellerIdentificationWithUserIdentification(travellerIdentificationType);
      }
    }
  }

  private boolean userHaveIdentificationList() {
    return mSelectedTraveller != null
        && mSelectedTraveller.getUserProfile() != null
        && mSelectedTraveller.getUserProfile().getUserIdentificationList() != null;
  }

  private void compareTravellerIdentificationWithUserIdentification(
      TravellerIdentificationType travellerIdentificationType) {
    for (UserIdentification userIdentificationType : mSelectedTraveller.getUserProfile()
        .getUserIdentificationList()) {
      if (travellerIdentificationType.value()
          .equals(userIdentificationType.getIdentificationType().name())) {
        showIdentificationInfo(travellerIdentificationType, userIdentificationType);
      }
    }
  }

  private void showIdentificationInfo(TravellerIdentificationType travellerIdentificationType,
      UserIdentification userIdentificationType) {
    mView.setIdentificationTypePosition(
        mPassengerInformation.getIdentificationTypes().indexOf(travellerIdentificationType));
    mView.showIdentificationNumber(userIdentificationType.getIdentificationId());
  }

  private void showExpirationDate() {
    if ((mSelectedTraveller != null) && (mSelectedTraveller.getUserProfile() != null) && (
        mSelectedTraveller.getUserProfile().getUserIdentificationList()
            != null)) {
      for (int i = 0; i < mPassengerInformation.getIdentificationTypes().size(); i++) {
        for (int j = 0; j < mSelectedTraveller.getUserProfile().getUserIdentificationList().size();
            j++) {
          UserIdentification.IdentificationType userId = mSelectedTraveller.getUserProfile()
              .getUserIdentificationList()
              .get(j)
              .getIdentificationType();
          if (userId != null) {
            String identificationTypeTraveller = mSelectedTraveller.getUserProfile()
                .getUserIdentificationList()
                .get(j)
                .getIdentificationType()
                .name();
            String identificationTypePassenger =
                mPassengerInformation.getIdentificationTypes().get(i).value();
            if (identificationTypeTraveller.equals(identificationTypePassenger)) {
              long expirationDate = mSelectedTraveller.getUserProfile()
                  .getUserIdentificationList()
                  .get(j)
                  .getIdentificationExpirationDate();
              List<Integer> dayMonthYear =
                  mDateHelperInterface.getDayMonthYearFromTimeStamp(expirationDate);
              int day = dayMonthYear.get(DAY);
              int month = dayMonthYear.get(MONTH) + 1;
              int year = dayMonthYear.get(YEAR);
              String expDate = day + "-" + month + "-" + year;
              mView.showIdentificationExpiration(expDate);
            }
          }
        }
      }
    }
  }

  private void showIdentificationCountry() {
    if (userHaveIdentificationList()) {
      for (TravellerIdentificationType travellerIdentificationType : mPassengerInformation.getIdentificationTypes()) {
        compareTravellerIdentificationCountryWithUserIdentificationCountry(
            travellerIdentificationType);

      }
    }
  }

  private void compareTravellerIdentificationCountryWithUserIdentificationCountry(
      TravellerIdentificationType travellerIdentificationType) {
    for (UserIdentification userIdentificationType : mSelectedTraveller.getUserProfile()
        .getUserIdentificationList()) {
      if (travellerIdentificationType.value()
          .equals(userIdentificationType.getIdentificationType().name())) {
        showIdentificationCountryTravellerInfo(userIdentificationType);
      }
    }
  }

  private void showIdentificationCountryTravellerInfo(UserIdentification userIdentificationType) {
    mView.showIdentificationCountry(userIdentificationType.getIdentificationCountryCode());
  }

  private void showFrequentFlyer() {
    if (mSelectedTraveller != null) {
      List<UserFrequentFlyer> frequentFlyerCards = mSelectedTraveller.getUserFrequentFlyers();
      formartFrequentFlyerInformation(frequentFlyerCards);
    }
  }

  public void cleanPassengerFields() {
    mView.showName(null);
    mView.showMiddleName(null);
    mView.showLastName(null);
    mView.showSecondLastname(null);
    mView.showBirthdate(null);
    mView.showNationality(null);
    mView.showCountryOfResident(null);
    mView.showIdentificationNumber(null);
    mView.showIdentificationExpiration(null);
    mView.showIdentificationCountry(null);
    mView.showFrequentFlyer(null);
    mView.setNullTextWatchersError();
  }

  private boolean isAvailableFrequentFlyer() {
    return ((mPassengerInformation.getFrequentFlyerAirlineGroups() != null)
        && (!mPassengerInformation.getFrequentFlyerAirlineGroups().isEmpty()));
  }

  public List<Carrier> getCarriersFromPassenger() {
    List<Carrier> carriers = new ArrayList<>();
    if (isAvailableFrequentFlyer()) {
      for (AirlineGroup airlineGroup : mPassengerInformation.getFrequentFlyerAirlineGroups()) {
        for (Carrier carrier : airlineGroup.getCarriers()) {
          carriers.add(new Carrier(carrier.getCode(), carrier.getName()));
        }
      }
    }
    return carriers;
  }

  public void formartFrequentFlyerInformation(List<UserFrequentFlyer> frequentFlyerCards) {
    if ((frequentFlyerCards != null) && !frequentFlyerCards.isEmpty()) {
      String frequentFlyerCodesToShow = "";
      for (UserFrequentFlyer frequentFlyerCard : frequentFlyerCards) {
        frequentFlyerCodesToShow =
            concatenateFrequentFlyerCodes(frequentFlyerCodesToShow, frequentFlyerCard);
      }
      frequentFlyerCodesToShow = removeFrequentFlyerCodesLastComa(frequentFlyerCodesToShow);
      frequentFlyerCodesToShow =
          formatFrequentFlyerCodesIfExceedMaxLenght(frequentFlyerCodesToShow);
      mView.showFrequentFlyer(frequentFlyerCodesToShow);
    }
  }

  private String concatenateFrequentFlyerCodes(String frequentFlyerCodes,
      UserFrequentFlyer userFrequentFlyer) {
    frequentFlyerCodes = frequentFlyerCodes
        + userFrequentFlyer.getAirlineCode()
        + FREQUENT_FLYER_CODE_SEPARATOR
        + userFrequentFlyer.getFrequentFlyerNumber()
        + FREQUENT_FLYER_SEPARATOR;
    return frequentFlyerCodes;
  }

  private String removeFrequentFlyerCodesLastComa(String frequentFlyerCodesToShow) {
    int comaAndSpaceLenght = 2;
    return frequentFlyerCodesToShow.substring(0,
        frequentFlyerCodesToShow.length() - comaAndSpaceLenght);
  }

  private String formatFrequentFlyerCodesIfExceedMaxLenght(String frequentFlyerCodesToShow) {
    if (frequentFlyerCodesToShow.length() >= FREQUENT_FLYER_MAX_LENGHT) {
      frequentFlyerCodesToShow = frequentFlyerCodesToShow.substring(0, FREQUENT_FLYER_MAX_LENGHT)
          + FREQUENT_FLYER_DOTS_LENGTH_EXCEED;
    }
    return frequentFlyerCodesToShow;
  }

  public List<String> formatPassengersNameToShowOnSpinner(
      List<UserTraveller> mTravellersListForWidget) {
    List<String> travellersName = new ArrayList<>();
    String travellerFullName;
    for (UserTraveller userTraveller : mTravellersListForWidget) {
      travellerFullName = concatenateNameAndSurnames(userTraveller);
      travellersName.add(travellerFullName);
    }
    return travellersName;
  }

  private String concatenateNameAndSurnames(UserTraveller userTraveller) {
    String travellerFullName;
    if ((userTraveller.getUserProfile() != null) && (userTraveller.getUserProfile()
        .getFirstLastName() != null)) {
      travellerFullName =
          userTraveller.getUserProfile().getName() + " " + userTraveller.getUserProfile()
              .getFirstLastName();
    } else {
      travellerFullName = userTraveller.getUserProfile().getName();
    }
    return travellerFullName;
  }

  private boolean isThereLocalData() {
    return (mTravellersGroupByWidgetType.size() != 0);
  }

  private boolean isThisFirstWidget() {
    return (mView.getWidgetPosition() == 0);
  }

  public boolean getSeconLastNameConditionRule() {
    return mConditionRulesHelper.isSecondLastNameMandatory();
  }

  public UserTraveller getEmptyUserTraveller(int position, String namePassenger) {
    mEmptyPassenger = namePassenger;
    return UserTravellerFactory.getEmptyUserTraveller(position, namePassenger);
  }

  public TravellerRequest getTraveller() {
    CreateTravellerRequestHelper createTravellerRequestHelper =
        new CreateTravellerRequestHelper(mView, mPassengerInformation, mConditionRulesHelper);
    return createTravellerRequestHelper.create();
  }

  public UserTraveller getUserTraveller() {
    int passengerSelectedIndex = mView.getPassengerSelected();
    UserTraveller userTraveller;
    TravellerRequest travellerRequest = getTraveller();

    if (passengerSelectedIndex > 0) {
      userTraveller = mTravellersGroupByWidgetType.get(passengerSelectedIndex);
      mUpdateUserTravellerWithTravellerRequestAndBuyerRequest.updateUserTravellerWithTravellerRequest(
          userTraveller, travellerRequest);
    } else {
      userTraveller =
          mTravellerRequestToUserTravellerParser.travellerRequestToUserTraveller(travellerRequest);
    }

    final boolean isMainPassengerChecked = mView.getWidgetPosition() == 0;
    userTraveller.setBuyer(isMainPassengerChecked);
    userTraveller.getUserProfile().setDefaultTraveller(isMainPassengerChecked);
    return userTraveller;
  }

  @Nullable
  UserTraveller getBuyerMembershipTraveller() {
    Membership membership = membershipInteractor.getMembershipForCurrentMarket();

    if (membership == null){
      return null;
    }

    for (int i = 0; i < mTravellersGroupByWidgetType.size(); i++) {
        String memberFirstName = membership.getFirstName();
        String memberLastName = membership.getLastNames();

        String memberName = (memberFirstName == null ? "" : memberFirstName);
        memberName += " " + (memberLastName == null ? "" : memberLastName);

        String travellerName = (mTravellersGroupByWidgetType.get(i).getUserProfile() == null
            || mTravellersGroupByWidgetType.get(i).getUserProfile().getName() == null ? ""
            : mTravellersGroupByWidgetType.get(i).getUserProfile().getName());

        travellerName += " " + (mTravellersGroupByWidgetType.get(i).getUserProfile() == null
            || mTravellersGroupByWidgetType.get(i).getUserProfile().getFirstLastName() == null ? ""
            : mTravellersGroupByWidgetType.get(i).getUserProfile().getFirstLastName());

        travellerName += " " + (mTravellersGroupByWidgetType.get(i).getUserProfile() == null
            || mTravellersGroupByWidgetType.get(i).getUserProfile().getSecondLastName() == null ? ""
            : mTravellersGroupByWidgetType.get(i).getUserProfile().getSecondLastName());

        memberName = memberName.toLowerCase().trim();
        travellerName = travellerName.toLowerCase().trim();

        if (memberName.equals(travellerName)) {
          selectedMembershipTravellerPosition = i;
          mSelectedTravellerPosition = i;
          mSelectedTraveller = mTravellersGroupByWidgetType.get(i);
          return mSelectedTraveller;
        }
    }
    return null;
  }

  public int getMembershipPosition() {
    return selectedMembershipTravellerPosition;
  }

  @Nullable public String getResidentLocalityByCode(String locality) {
    return mResidentLocalities.get(locality);
  }

  public List<UserTraveller> removeNullUserTravellers(List<UserTraveller> userTravellers) {
    List<UserTraveller> userTravellersNonNull = new ArrayList<>(userTravellers);
    for (UserTraveller userTraveller : userTravellers) {
      if ((userTraveller == null) || (userTraveller.getUserProfile() == null)) {
        userTravellersNonNull.remove(userTraveller);
      }
    }
    return userTravellersNonNull;
  }

  public void refreshUserTravellers() {
    UserTraveller.TypeOfTraveller typeOfTraveller;
    if (mPassengerInformation.getTravellerType() == TravellerType.ADULT) {
      typeOfTraveller = UserTraveller.TypeOfTraveller.ADULT;
    } else if (mPassengerInformation.getTravellerType() == TravellerType.CHILD) {
      typeOfTraveller = UserTraveller.TypeOfTraveller.CHILD;
    } else {
      typeOfTraveller = UserTraveller.TypeOfTraveller.INFANT;
    }

    mGetTravellersByTypeInteractor.getTravellers(typeOfTraveller,
        new OnUserTravellerRequestListener() {
          @Override public void onUserTravellersListByTypeAvailable(
              UserTraveller.TypeOfTraveller typeOfTraveller,
              List<UserTraveller> userTravellersList) {
            setUserTravellers(userTravellersList);
            if (userTravellersList.size() > 0) {
              setSelectedSpinnerTraveller(mView.getWidgetPosition());
            }
          }
        });
  }

  public void validateBirthdate(int birthdateDay, int birthdateMonth, int birthdateYear,
      Itinerary itineraryShoppingCart) {

    final int date[] = { birthdateDay, birthdateMonth, birthdateYear };
    final int infantMaxAge = 2;
    final int childMaxAge = 12;

    Itinerary sortedItinerary = sortItineraryInteractor.sort(itineraryShoppingCart);

    if (mPassengerInformation.getTravellerType() == INFANT
        && (birthdateValidator.isTravellerTypeTurningIntoAnotherTypeDuringTrip(date,
        sortedItinerary, INFANT, infantMaxAge))) {
      mView.showInfantBirthdateEror();
    } else if (mPassengerInformation.getTravellerType() == TravellerType.CHILD && birthdateValidator
        .isTravellerTypeTurningIntoAnotherTypeDuringTrip(date, sortedItinerary, CHILD,
            childMaxAge)) {
      mView.showChildBirthdateError();
    }
  }

  public void onChangeMembership(String travellerName) {
    String buyerName = getBuyerName();
    if (thereIsMembershipAndIsNotTravelling(travellerName, buyerName)) {
      mView.showMembershipAlert();
    } else if (thereIsMembershipAndIsTravelling(travellerName, buyerName)) {
      mView.showSpinnerMembershipTitle();
      mView.showImageMembershipProfile();
      mView.propagateItinerarySortCriteria(
          ItinerarySortCriteria.MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE);
      membershipWasSelected = true;
      mView.showMembershipPrices();
    }
  }

  private boolean thereIsMembershipAndIsTravelling(String travellerName, String buyerName) {
    return buyerName != null
        && travellerName.equals(buyerName)
        && membershipInteractor.isMemberForCurrentMarket();
  }

  private boolean thereIsMembershipAndIsNotTravelling(String travellerName, String buyerName) {
    return buyerName != null
        && !travellerName.equals(buyerName)
        && !isMembershipSelectedInOtherWidget(buyerName) && membershipWasSelected
        && membershipInteractor.isMemberForCurrentMarket();
}

  @Nullable
  private String getBuyerName() {
    if (buyerMembershipTraveller != null && buyerMembershipTraveller.getUserProfile() != null) {
      return buyerMembershipTraveller.getUserProfile().getName()
          + " "
          + buyerMembershipTraveller.getUserProfile().getFirstLastName();
    }
    return null;
  }

  private boolean isMembershipSelectedInOtherWidget(String buyerName) {
    return mView.isPassengerSelectedInAnotherTravellerSpinner(buyerName);
  }

  public void membershipWasNotSelected() {
    membershipWasSelected = false;
  }

  public void membershipWasSelected() {
    membershipWasSelected = true;
  }

  public String getSecondLastNameFromPassenger() {
    if ((mSelectedTraveller != null) && (mSelectedTraveller.getUserProfile() != null) && (
        mSelectedTraveller.getUserProfile().getSecondLastName()
            != null) && (!mSelectedTraveller.getUserProfile().getSecondLastName().isEmpty())) {
      String secondLastName = mSelectedTraveller.getUserProfile().getSecondLastName();
      return secondLastName;
    } else {
      return null;
    }
  }
}