package com.odigeo.presenter;

import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.dataodigeo.session.UserInfoInterface;
import com.odigeo.interactors.SendFeedbackInteractor;
import com.odigeo.presenter.contracts.navigators.AppRateFeedbackNavigatorInterface;
import com.odigeo.presenter.contracts.views.AppRateHelpImproveViewInterface;
import java.util.List;

/**
 * Created by matias.dirusso on 4/29/2016.
 */
public class AppRateHelpImprovePresenter
    extends BasePresenter<AppRateHelpImproveViewInterface, AppRateFeedbackNavigatorInterface> {

  private SendFeedbackInteractor mSendFeedbackInteractor;
  private SessionController mSessionController;
  private PreferencesControllerInterface mPreferencesController;

  public AppRateHelpImprovePresenter(AppRateHelpImproveViewInterface view,
      SendFeedbackInteractor sendFeedbackInteractor, AppRateFeedbackNavigatorInterface navigator,
      SessionController sessionController,
      PreferencesControllerInterface preferencesController) {
    super(view, navigator);
    mSendFeedbackInteractor = sendFeedbackInteractor;
    mSessionController = sessionController;
    mPreferencesController = preferencesController;
  }

  public void sendFeedback(String email, String subject, String message, List<String> screenshots) {
    mSendFeedbackInteractor.sendFeedback(new OnRequestDataListener<Boolean>() {
      @Override public void onResponse(Boolean object) {
        if (object) {
          mView.showSuccessFeedback();
          mPreferencesController.saveBooleanValue(
              PreferencesControllerInterface.MYTRIPS_APPRATE_SHOWCARD, false);
        }
      }

      @Override public void onError(MslError error, String message) {
        mView.hideProgress();
        mView.showErrorDialog();
      }
    }, email, subject, message, screenshots);
  }

  public void navigateToHome() {
    mNavigator.navigateToHome();
  }

  public void checkIfIsLogged() {
    UserInfoInterface userInfo = mSessionController.getUserInfo();
    if (userInfo != null) {
      mView.setEmail(userInfo.getEmail());
    }
  }

  public void validateFields(String comment, CharSequence errorText) {
    mView.changeButtonHelpImproveStatus(comment.length() > 0 && errorText == null);
  }
}
