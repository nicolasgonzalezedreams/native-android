package com.odigeo.presenter;

import com.odigeo.presenter.contracts.navigators.LoginNavigatorInterface;
import com.odigeo.presenter.contracts.views.LegacyUserViewInterface;

/**
 * Created by ximena.perez on 21/08/2015.
 */
public class LegacyUserPresenter
    extends BasePresenter<LegacyUserViewInterface, LoginNavigatorInterface> {

  public LegacyUserPresenter(LegacyUserViewInterface view, LoginNavigatorInterface navigator) {
    super(view, navigator);
  }

  public void goToLogin() {
    mNavigator.showLoginView();
  }
}
