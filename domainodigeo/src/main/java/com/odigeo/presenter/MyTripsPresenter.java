package com.odigeo.presenter;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.BookingSummary;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.interactors.CheckUserCredentialsInteractor;
import com.odigeo.interactors.DeleteArrivalGuideInformationInteractor;
import com.odigeo.interactors.MyTripsInteractor;
import com.odigeo.interactors.UpdateBookingStatusInteractor;
import com.odigeo.interactors.UpdateBookingsFromNetworkInteractor;
import com.odigeo.interactors.provider.MyTripsProvider;
import com.odigeo.presenter.contracts.navigators.MyTripsNavigatorInterface;
import com.odigeo.presenter.contracts.views.MyTripsViewInterface;
import java.io.File;
import org.jetbrains.annotations.Nullable;

import static com.odigeo.data.preferences.PreferencesControllerInterface.MYTRIPS_APPRATE_SHOWCARD;

public class MyTripsPresenter
    extends BasePresenter<MyTripsViewInterface, MyTripsNavigatorInterface> {

  private final MyTripsInteractor mMyTripsInteractor;
  private final TrackerControllerInterface trackerController;
  private final UpdateBookingsFromNetworkInteractor mUpdateBookingsInteractor;
  private final UpdateBookingStatusInteractor mUpdateBookingStatusInteractor;
  private final PreferencesControllerInterface mPreferencesController;
  private final CheckUserCredentialsInteractor checkUserCredentialsInteractor;

  private MyTripsProvider mMyTripsProvider;

  public MyTripsPresenter(MyTripsViewInterface view, MyTripsNavigatorInterface navigator,
      MyTripsInteractor interactor, MyTripsProvider myTripsProvider,
      TrackerControllerInterface trackerController,
      UpdateBookingsFromNetworkInteractor updateBookingsInteractor,
      UpdateBookingStatusInteractor updateBookingStatusInteractor,
      PreferencesControllerInterface preferencesControllerInterface,
      CheckUserCredentialsInteractor checkUserCredentialsInteractor) {

    super(view, navigator);
    this.mMyTripsInteractor = interactor;
    this.mMyTripsProvider = myTripsProvider;
    this.trackerController = trackerController;
    this.mUpdateBookingsInteractor = updateBookingsInteractor;
    this.mUpdateBookingStatusInteractor = updateBookingStatusInteractor;
    this.mPreferencesController = preferencesControllerInterface;
    this.checkUserCredentialsInteractor = checkUserCredentialsInteractor;
  }

  public void initializeLocalBookings() {
    mView.showLoading();
    mMyTripsInteractor.getBookings(new OnRequestDataListener<MyTripsProvider>() {
      @Override public void onResponse(MyTripsProvider bookings) {
        mMyTripsProvider = bookings;
        if (!shouldShowRateApp()) {
          mMyTripsProvider.removeRateAppCard();
        }
        mView.initializeBookings(mMyTripsProvider, shouldShowRateApp());
        mView.onRequestFinished(mMyTripsProvider.size() == 0);
      }

      @Override public void onError(MslError error, String message) {
        mView.onRequestFinished(mMyTripsProvider.size() == 0);
        mView.trackScreenLoadError();
      }
    });
  }

  public void triggerUpdate() {
    if (checkUserCredentialsInteractor.isUserLogin()) {
      mView.showLoading();
      updateBookings();
    } else {
      mUpdateBookingStatusInteractor.updateBookingStatus(
          new UpdateBookingStatusInteractor.BookingStatusListener() {
            @Override public void onComplete(@Nullable Booking booking) {
              getAllBookings();
            }
          });
    }
  }

  public void updateBookings() {
    mUpdateBookingsInteractor.updateMyTrips(new OnRequestDataListener<Void>() {
      @Override public void onResponse(Void object) {
        getAllBookings();
      }

      @Override public void onError(MslError error, String message) {
        mView.onRequestFinished(mMyTripsProvider.size() == 0);
        mView.trackPullToRefreshError();
      }
    });
  }

  private void getAllBookings() {
    mMyTripsInteractor.getBookings(new OnRequestDataListener<MyTripsProvider>() {

      @Override public void onResponse(MyTripsProvider bookings) {
        mMyTripsProvider = bookings;
        if (!shouldShowRateApp()) {
          mMyTripsProvider.removeRateAppCard();
        }
        mView.notifyBookingsHasChanged(mMyTripsProvider, shouldShowRateApp());
        mView.onRequestFinished(mMyTripsProvider.size() == 0);
      }

      @Override public void onError(MslError error, String message) {
        mView.onRequestFinished(mMyTripsProvider.size() == 0);
        mView.trackScreenLoadError();
      }
    });
  }

  public void openDetails(Booking booking, int position) {
    if (booking.isPastBooking()) {
      mNavigator.navigateToPastTripDetails(booking, position);
    } else {
      mNavigator.navigateToTripDetails(booking, position);
    }
  }

  public void openImport() {
    mNavigator.navigateToImportMyTrips();
  }

  private boolean shouldShowRateApp() {
    return mPreferencesController.getBooleanValue(MYTRIPS_APPRATE_SHOWCARD);
  }

  public void navigateToImportMyTrips() {
    mNavigator.navigateToImportMyTrips();
  }

  public void openLogin() {
    trackerController.trackLocalyticsMyTripsClickedOnLogin();
    mNavigator.navigateToLogin();
  }

  public void checkIfUserIsLoggedIn() {
    if (!checkUserCredentialsInteractor.isUserLogin()) {
      mView.showLoginViews();
    } else {
      mView.hideLoginViews();
    }
  }

  public void setNavigationTitle(String title) {
    mNavigator.setNavigationTitle(title);
  }

  public boolean checkShouldShowAppRate() {
    return mPreferencesController.getBooleanValue(MYTRIPS_APPRATE_SHOWCARD);
  }
}
