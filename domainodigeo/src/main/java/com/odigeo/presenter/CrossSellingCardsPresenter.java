package com.odigeo.presenter;

import com.odigeo.data.download.DownloadController;
import com.odigeo.data.download.DownloadManagerController;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.xsell.CrossSelling;
import com.odigeo.interactors.ArrivalGuidesCardsInteractor;
import com.odigeo.interactors.GetCrossSellingForBookingInteractor;
import com.odigeo.presenter.contracts.mappers.xsell.CrossSellingCardMapper;
import com.odigeo.presenter.contracts.navigators.MyTripDetailsNavigatorInterface;
import com.odigeo.presenter.contracts.views.CrossSellingCardsView;
import com.odigeo.presenter.model.CrossSellingCard;
import com.odigeo.presenter.model.CrossSellingType;
import java.io.File;
import java.util.Collection;
import java.util.List;

public class CrossSellingCardsPresenter implements DownloadManagerController {

  private final CrossSellingCardMapper crossSellingCardMapper;
  private final GetCrossSellingForBookingInteractor getCrossSellingForBookingInteractor;
  private final CrossSellingCardsView crossSellingCardsView;
  private final DownloadController downloadController;
  private final ArrivalGuidesCardsInteractor arrivalGuidesCardsInteractor;
  private final MyTripDetailsNavigatorInterface navigator;
  private Booking booking;

  public CrossSellingCardsPresenter(CrossSellingCardsView crossSellingCardsView,
      GetCrossSellingForBookingInteractor getCrossSellingForBookingInteractor,
      CrossSellingCardMapper crossSellingCardMapper, DownloadController downloadController,
      ArrivalGuidesCardsInteractor arrivalGuidesCardsInteractor,
      MyTripDetailsNavigatorInterface navigator) {
    this.crossSellingCardsView = crossSellingCardsView;
    this.getCrossSellingForBookingInteractor = getCrossSellingForBookingInteractor;
    this.crossSellingCardMapper = crossSellingCardMapper;
    this.downloadController = downloadController;
    this.arrivalGuidesCardsInteractor = arrivalGuidesCardsInteractor;
    this.navigator = navigator;
  }

  public Collection<CrossSellingCard> buildXsellCards(Booking booking) {
    List<CrossSelling> crossSellings =
        getCrossSellingForBookingInteractor.getCrossSellingForBooking(booking);
    for (CrossSelling card : crossSellings) {
      if (card.getType() == CrossSellingType.TYPE_GROUND) {
        crossSellingCardsView.trackGroundTransportationCardAppearance();
        break;
      }
    }
    return crossSellingCardMapper.transform(crossSellings);
  }

  public void onCrossSellingCardSelected(CrossSellingCard card) {
    booking = card.getBooking();

    switch (card.getType()) {
      case CrossSellingType.TYPE_HOTEL:
        crossSellingCardsView.openHotels(booking);
        break;
      case CrossSellingType.TYPE_CARS:
        crossSellingCardsView.openCars(booking);
        break;
      case CrossSellingType.TYPE_GROUND:
        crossSellingCardsView.openGroundTransportation(booking);
        break;
      case CrossSellingType.TYPE_GUIDE:
        handleCityGuideOpening();
        break;
    }
  }

  private void handleCityGuideOpening() {
    if (crossSellingCardsView.checkStoragePermission()) {
      downloadController.subscribeDownloader(this);

      if (arrivalGuidesCardsInteractor.isGuideDownloaded(booking.getArrivalIataCode())) {
        doAfterDownloadAction(booking.getArrivalIataCode());
      } else if (downloadController.isDownloadRunning()) {
        crossSellingCardsView.showGuideDownloading();
      } else {
        downloadController.startDownload(
            arrivalGuidesCardsInteractor.getGuideUrl(booking.getArrivalGeoNodeId()),
            booking.getArrivalIataCode());
        crossSellingCardsView.showGuideDownloading();
      }
    }
  }

  @Override public void doAfterDownloadAction(String filename) {
    crossSellingCardsView.hideGuideDownloading();
    crossSellingCardsView.openCityGuide(booking);
    downloadController.unsubscribeDownloader();
  }

  @Override public void setTotalDownloadSize(double totalSize) {
  }

  @Override public void onProgress(double progress) {
  }

  @Override public void onNoConnectionError() {
    crossSellingCardsView.showNoConnectionDialog();
  }

  @Override public void onDownloadFailedError() {
    crossSellingCardsView.showDownloadFailedDialog();
  }

  @Override public void onInsufficientSpaceError() {
    crossSellingCardsView.showInsufficientSpaceDialog();
  }

  public boolean hasArrivalGuide(long arrivalGeoNodeId) {
    return arrivalGuidesCardsInteractor.hasArrivalGuide(arrivalGeoNodeId);
  }

  public void navigateToGroundTransportation(String url) {
    navigator.navigateToGroundTransportation(url);
  }
}
