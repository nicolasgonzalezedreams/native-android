package com.odigeo.presenter;

import com.odigeo.configuration.ABAlias;
import com.odigeo.configuration.ABPartition;
import com.odigeo.data.entity.shoppingCart.BaggageConditions;
import com.odigeo.data.entity.shoppingCart.BaggageDescriptor;
import com.odigeo.data.entity.shoppingCart.SegmentTypeIndex;
import com.odigeo.data.entity.shoppingCart.SelectableBaggageDescriptor;
import com.odigeo.data.entity.shoppingCart.request.BaggageSelectionRequest;
import com.odigeo.helper.ABTestHelper;
import com.odigeo.presenter.contracts.components.BaseCustomComponentPresenter;
import com.odigeo.presenter.contracts.views.BaggageCollectionItem;
import java.math.BigDecimal;
import java.util.List;

public class BaggageCollectionItemPresenter
    extends BaseCustomComponentPresenter<BaggageCollectionItem> {

  static final int AB_ACTIVE_PARTITIONS = 2;

  private ABTestHelper abTestHelper;
  private BaggageConditions mBaggageConditions;
  private boolean mBaggageIncluded;
  private boolean mBaggageSelectable;

  public BaggageCollectionItemPresenter(BaggageCollectionItem view, ABTestHelper abTestHelper) {
    super(view);
    this.abTestHelper = abTestHelper;
  }

  public void processBaggage(BaggageConditions baggageConditions, int preselectedBaggagePosition) {
    mBaggageConditions = baggageConditions;
    mView.showTravelInfo();
    mBaggageIncluded = processBaggageIncluded();
    mBaggageSelectable = processBaggageSelectable(preselectedBaggagePosition);

    if (!mBaggageIncluded && !mBaggageSelectable) {
      mView.showBaggageNotIncluded();
    }
  }

  private boolean processBaggageSelectable(int preselectedBaggagePosition) {
    boolean selectableBaggage = true;
    List<SelectableBaggageDescriptor> selectableBaggageDescriptorList =
        mBaggageConditions.getSelectableBaggageDescriptor();

    if (!selectableBaggageDescriptorList.isEmpty()) {
      mView.showBaggageOptions(mBaggageConditions.getSelectableBaggageDescriptor());
      if (preselectedBaggagePosition >= 0) {
        onBaggageOptionSelected(
            mBaggageConditions.getSelectableBaggageDescriptor().get(preselectedBaggagePosition));
      }
    } else {
      mView.hideAddBaggageOption();
      selectableBaggage = false;
    }

    return selectableBaggage;
  }

  private boolean processBaggageIncluded() {
    BaggageDescriptor baggageDescriptor = mBaggageConditions.getBaggageDescriptorIncludedInPrice();
    if (baggageDescriptor == null) {
      return false;
    }

    if (baggageDescriptor.hasBaggageWithoutKilos()) {
      mView.setBaggageIncludedWithoutKilos(baggageDescriptor.getPieces());
    } else if (baggageDescriptor.hasBaggageWithKilos()) {
      mView.setBaggageIncludedWithKilos(baggageDescriptor.getPieces(),
          baggageDescriptor.getKilos());
    } else if (baggageDescriptor.hasBaggageWithKilosAndWithoutPieces()) {
      mView.setBaggageIncludedWithKilos(1, baggageDescriptor.getKilos());
    } else {
      return false;
    }

    return true;
  }

  public BaggageSelectionRequest buildBaggageSelectionRequest() {
    BaggageSelectionRequest baggageSelectionRequest = null;

    if (mBaggageSelectable && mView.getSelectedItem() != null) {
      baggageSelectionRequest = new BaggageSelectionRequest();
      SelectableBaggageDescriptor baggageDescriptor;
      baggageDescriptor = mView.getSelectedItem();
      baggageSelectionRequest.setBaggageDescriptor(baggageDescriptor.getBaggageDescriptor());
      baggageSelectionRequest.setSegmentTypeIndex(mBaggageConditions.getSegmentTypeIndex());
    } else if (mBaggageIncluded) {
      baggageSelectionRequest = new BaggageSelectionRequest();
      SelectableBaggageDescriptor baggageDescriptor;
      baggageDescriptor =
          new SelectableBaggageDescriptor(mBaggageConditions.getBaggageDescriptorIncludedInPrice(),
              new BigDecimal(0));
      baggageSelectionRequest.setBaggageDescriptor(baggageDescriptor.getBaggageDescriptor());
      baggageSelectionRequest.setSegmentTypeIndex(mBaggageConditions.getSegmentTypeIndex());
    }

    return baggageSelectionRequest;
  }

  public void onBaggageOptionSelected(SelectableBaggageDescriptor selectedBaggageDescriptor) {
    if (selectedBaggageDescriptor.getBaggageDescriptor().getPieces() > 0) {
      mView.setSelectedItem(selectedBaggageDescriptor);
      mView.hideAddBaggageOption();
      mView.showBaggageSelected(selectedBaggageDescriptor);
    } else {
      mView.setSelectedItem(null);
      mView.showAddBaggageOption();
    }
  }

  public void onRemoveBaggageClick() {
    ABPartition abPartition =
        abTestHelper.getPartition(ABAlias.BBU_BBUSTERS430, AB_ACTIVE_PARTITIONS);
    if (abPartition == ABPartition.ONE) {
      onBaggageSelectedRemoved();
    } else {
      mView.showConfirmRemoveBaggage();
    }
  }

  public void onBaggageSelectedRemoved() {
    onClearBaggageSelected();
    mView.notifyBaggageSelectionHasChanged();
    mView.trackBaggageRemoved();
  }

  public void onClearBaggageSelected() {
    mView.setSelectedItem(null);
    mView.hideBaggageSelected();
    mView.showAddBaggageOption();
  }

  public SegmentTypeIndex getSegmentTypeIndex() {
    return mBaggageConditions.getSegmentTypeIndex();
  }

  public void updateBaggageSelected(BaggageDescriptor baggageDescriptor) {
    for (int i = 0; i < mBaggageConditions.getSelectableBaggageDescriptor().size(); i++) {
      if (mBaggageConditions.getSelectableBaggageDescriptor()
          .get(i)
          .getBaggageDescriptor()
          .equals(baggageDescriptor)) {
        mView.showBaggageOptions(mBaggageConditions.getSelectableBaggageDescriptor());
        onBaggageOptionSelected(mBaggageConditions.getSelectableBaggageDescriptor().get(i));
      }
    }
  }
}
