package com.odigeo.presenter;

import com.odigeo.presenter.contracts.navigators.PassengerNavigatorInterface;
import com.odigeo.presenter.contracts.views.ContactPassengerItemInterface;

/**
 * Author: Oscar alvarez
 * Date: 30/09/2015.
 */
public class ContactPassengerItemPresenter
    extends BasePresenter<ContactPassengerItemInterface, PassengerNavigatorInterface> {

  public ContactPassengerItemPresenter(ContactPassengerItemInterface view,
      PassengerNavigatorInterface navigator) {
    super(view, navigator);
  }
}
