package com.odigeo.presenter;

import com.odigeo.data.entity.userData.User;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.LoginInteractor;
import com.odigeo.interactors.LogoutInteractor;
import com.odigeo.interactors.MyTripsInteractor;
import com.odigeo.interactors.RegisterPasswordInteractor;
import com.odigeo.interactors.TravellerDetailInteractor;
import com.odigeo.presenter.contracts.navigators.RegisterNavigatorInterface;
import com.odigeo.presenter.contracts.views.RegisterViewInterface;
import com.odigeo.tools.CheckerTool;

public class RegisterViewPresenter extends SocialLoginPresenter {

  private static final String FACEBOOK_SOURCE_REGISTER = "facebook";
  private static final String GOOGLE_SOURCE_REGISTER = "google";

  private RegisterPasswordInteractor registerInteractor;
  private LogoutInteractor mLogoutInteractor;
  private SessionController mSessionController;
  private TrackerControllerInterface mTrackerController;

  public RegisterViewPresenter(RegisterViewInterface view,
      RegisterPasswordInteractor registerInteractor, LoginInteractor loginInteractor,
      TravellerDetailInteractor travellersInteractor, MyTripsInteractor myTripsInteractor,
      RegisterNavigatorInterface navigator, LogoutInteractor logoutInteractor,
      SessionController sessionController,
      TrackerControllerInterface trackerControllerInterface) {
    super(view, navigator, loginInteractor, myTripsInteractor, travellersInteractor,
        trackerControllerInterface);
    this.registerInteractor = registerInteractor;
    mLogoutInteractor = logoutInteractor;
    mSessionController = sessionController;
    mTrackerController = trackerControllerInterface;
  }

  public final void validateUsernameAndPasswordFormat(String username, String password) {
    boolean isValid = CheckerTool.checkEmail(username) && CheckerTool.checkPassword(password);
    ((RegisterViewInterface) mView).enableSignUpButton(isValid);
  }

  public void signUpPassword(final String user, String password, Boolean hasRegisterToNewsletter) {

    registerInteractor.registerUserPassword(new OnAuthRequestDataListener<User>() {
      @Override public void onAuthError() {
        mLogoutInteractor.logout(new OnRequestDataListener<Boolean>() {
          @Override public void onResponse(Boolean object) {
            mView.showAuthError();
            mSessionController.saveLogoutWasMade(true);
          }

          @Override public void onError(MslError error, String message) {
            mTrackerController.trackApiRegisterError();
            mView.onLogoutError(error, message);
          }
        });
      }

      @Override public void onResponse(User user) {
        mView.hideProgress();
        ((RegisterNavigatorInterface) mNavigator).registerSuccess(user.getEmail());
      }

      @Override public void onError(MslError error, String message) {
        mView.hideProgress();
        handleRegisterError(error, user);
      }
    }, user, password, hasRegisterToNewsletter);
  }

  public void showLogInUserAlreadyRegistered(String email) {
    ((RegisterNavigatorInterface) mNavigator).navigateToLogIn(email);
  }

  @Override protected void handleLoginError(MslError error, String source, String email) {
    buildSocialLoginErrorMessage(error, source);
  }

  private void handleRegisterError(MslError error, String userEmail) {
    if (error == MslError.STA_001) {
      ((RegisterViewInterface) mView).showUserAlreadyExist();
    } else if (error == MslError.STA_006) {
      mNavigator.showUserRegisteredFacebook(userEmail);
    } else if (error == MslError.STA_007) {
      mNavigator.showUserRegisteredGoogle(userEmail);
    } else {
      ((RegisterViewInterface) mView).showRegisterFail();
    }
  }
}
