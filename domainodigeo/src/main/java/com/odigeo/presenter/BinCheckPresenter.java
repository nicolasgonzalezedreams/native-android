package com.odigeo.presenter;

import com.odigeo.constants.PaymentMethodsKeys;
import com.odigeo.data.entity.CreditCardBinDetails;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.net.error.MslError;
import com.odigeo.interactors.CheckBinFromLocalInteractor;
import com.odigeo.interactors.CheckBinFromNetworkInteractor;
import com.odigeo.presenter.contracts.views.BinCheckViewInterface;
import com.odigeo.presenter.listeners.OnCheckBinListener;
import java.util.List;
import org.jetbrains.annotations.Nullable;

public class BinCheckPresenter {

  private static int MINIMUN_SIZE_TO_CHECKBIN_MSL = 6;

  private final CheckBinFromNetworkInteractor mCheckBinFromNetworkInteractor;
  private final CheckBinFromLocalInteractor mCheckBinFromLocalInteractor;
  private final BinCheckViewInterface mView;

  private List<ShoppingCartCollectionOption> mShoppingCartCollectionOptions;
  private String mPaymentMethodDetected;

  public BinCheckPresenter(BinCheckViewInterface binCheckViewInterface,
      CheckBinFromLocalInteractor checkBinFromLocalInteractor,
      CheckBinFromNetworkInteractor checkBinFromNetworkInteractor) {
    mView = binCheckViewInterface;
    mCheckBinFromLocalInteractor = checkBinFromLocalInteractor;
    mCheckBinFromNetworkInteractor = checkBinFromNetworkInteractor;
  }

  public void initPresenter(List<ShoppingCartCollectionOption> shoppingCartCollectionOption) {
    mShoppingCartCollectionOptions = shoppingCartCollectionOption;
  }

  void checkBin(String creditCardNumber) {
    if (haveToCheckBinLocally(creditCardNumber)) {
      mPaymentMethodDetected = mCheckBinFromLocalInteractor.checkBinLocally(creditCardNumber,
          mShoppingCartCollectionOptions);
      showPaymentMethod(mPaymentMethodDetected);
    } else if (haveToCheckBinMSL(creditCardNumber)) {
      getBinCheckFromMsl(creditCardNumber);
    }
  }

  private boolean haveToCheckBinLocally(String creditCardNumber) {
    return (creditCardNumber.length() < MINIMUN_SIZE_TO_CHECKBIN_MSL);
  }

  private boolean haveToCheckBinMSL(String creditCardNumber) {
    return creditCardNumber.length() == MINIMUN_SIZE_TO_CHECKBIN_MSL;
  }

  private void getBinCheckFromMsl(final String creditCardNumber) {
    mView.isBinCheckValid(false);
    mCheckBinFromNetworkInteractor.checkBinMSL(creditCardNumber, new OnCheckBinListener() {
      @Override public void onSuccessBinDetails(CreditCardBinDetails creditCardBinDetails) {
        handleSuccessBinDetails(creditCardBinDetails, creditCardNumber);
      }

      @Override public void onError(MslError error, String message) {

      }
    });
  }

  protected void handleSuccessBinDetails(CreditCardBinDetails creditCardBinDetails,
      String creditCardNumber) {
    showPaymentDetails(creditCardNumber, creditCardBinDetails);
  }

  private void showPaymentDetails(String creditCardNumber,
      CreditCardBinDetails creditCardBinDetails) {
    String creditCardType = getCreditCardType(creditCardNumber, creditCardBinDetails);

    if (creditCardType != null && isPaymentMethodDetectedInShoppingCart(creditCardType)) {
      showPaymentMethod(creditCardType);
    } else {
      showUnknownPaymentMethod();
    }
    mView.isBinCheckValid(true);
  }

  private String getCreditCardType(String creditCardNumber,
      CreditCardBinDetails creditCardBinDetails) {
    String creditCardType = getCreditCardType(creditCardBinDetails);
    String creditCardSubType = getCreditCardSubType(creditCardNumber, creditCardBinDetails);

    if (creditCardSubType == null) {
      return creditCardType;
    }

    return creditCardSubType;
  }

  private void showUnknownPaymentMethod() {
    mView.setCreditCardPaymentType();
    mView.showPaymentMethodSpinner();
    mView.flipCardInUI(PaymentMethodsKeys.UNKNOWN);
  }

  private void showPaymentMethod(String creditCardType) {
    mView.setCreditCardPaymentType();
    mView.hidePaymentMethodSpinner();
    mView.flipCardInUI(creditCardType);
  }

  @Nullable private String getCreditCardSubType(String crediCardNumber,
      CreditCardBinDetails creditCardBinDetails) {
    if ((creditCardBinDetails.getCreditCardBinSubtypes() != null)
        && (!creditCardBinDetails.getCreditCardBinSubtypes().isEmpty())) {
      return creditCardBinDetails.getCreditCardBinSubtypes().get(crediCardNumber);
    }
    return null;
  }

  @Nullable private String getCreditCardType(CreditCardBinDetails creditCardBinDetails) {
    return creditCardBinDetails.getCreditCardTypeCode();
  }

  public boolean isPaymentMethodDetectedInShoppingCart(String paymentMethodDetected) {
    for (ShoppingCartCollectionOption shoppingCartCollectionOption : mShoppingCartCollectionOptions) {
      String paymentMethodCode;
      if (isCreditCard(shoppingCartCollectionOption)) {
        paymentMethodCode = shoppingCartCollectionOption.getMethod().getCreditCardType().getCode();
        if (paymentMethodCode.equals(paymentMethodDetected)) {
          return true;
        }
      }
    }
    return false;
  }

  private boolean isCreditCard(ShoppingCartCollectionOption shoppingCartCollectionOption) {
    return (shoppingCartCollectionOption.getMethod().getType() != null
        && shoppingCartCollectionOption.getMethod().getType() == CollectionMethodType.CREDITCARD);
  }
}
