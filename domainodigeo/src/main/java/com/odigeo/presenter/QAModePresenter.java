package com.odigeo.presenter;

import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.presenter.contracts.navigators.QAModeNavigator;
import com.odigeo.presenter.contracts.views.QAModeView;
import com.odigeo.presenter.model.QAModeUrlModel;
import java.util.List;

public class QAModePresenter extends BasePresenter<QAModeView, QAModeNavigator> {

  private final PreferencesControllerInterface preferencesController;

  public QAModePresenter(QAModeView view, QAModeNavigator navigator,
      PreferencesControllerInterface preferencesController) {
    super(view, navigator);
    this.preferencesController = preferencesController;
  }

  public void getQAModeUrls() {
    final List<QAModeUrlModel> qaModeUrls = preferencesController.getQAModeUrls();
    if (qaModeUrls.size() == 1) {
      mView.disableQAModeUrlDeletion();
    }
    mView.showQAModeUrls(qaModeUrls);
  }

  public void onAddQAModeUrl(String url) {
    if (url != null && (!url.trim().isEmpty())) {
      final QAModeUrlModel qaModeUrlModel = new QAModeUrlModel(url, true);
      final List<QAModeUrlModel> qaModeUrlModels =
          preferencesController.saveQAModeUrl(qaModeUrlModel);
      mView.showAddQAModeUrlSuccess(qaModeUrlModels);
      mView.enableQAModeUrlDeletion();
    } else {
      mView.showEmptyUrlMessage();
    }
  }

  public void onRemoveQAModeUrl(QAModeUrlModel qaModeUrlModel) {
    final List<QAModeUrlModel> qaModeUrls = preferencesController.deleteQAModeUrl(qaModeUrlModel);
    if (qaModeUrls.size() == 1) {
      mView.disableQAModeUrlDeletion();
    } else {
      mView.enableQAModeUrlDeletion();
    }
    mView.showQAModeUrls(qaModeUrls);
  }

  public void onEditQAModeUrl(QAModeUrlModel qaModeUrlModel, String newUrl) {
    if (!newUrl.isEmpty()) {
      final List<QAModeUrlModel> qaModeUrls = preferencesController.editQAModeUrl(qaModeUrlModel,
          new QAModeUrlModel(newUrl, qaModeUrlModel.isSelected()));
      mView.showQAModeUrls(qaModeUrls);
    }
  }

  public void onSelectQAModeUrl(QAModeUrlModel qaModeUrlModel) {
    final List<QAModeUrlModel> qaModeUrls = preferencesController.editQAModeUrl(qaModeUrlModel,
        new QAModeUrlModel(qaModeUrlModel.getUrl(), true));
    mView.showQAModeUrlSelected(qaModeUrls, qaModeUrlModel);
    mNavigator.navigateBackWithSelectedQAModeUrl(qaModeUrlModel);
  }
}
