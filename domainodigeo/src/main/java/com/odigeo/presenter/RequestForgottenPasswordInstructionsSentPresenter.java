package com.odigeo.presenter;

import com.odigeo.presenter.contracts.navigators.RequestForgottenPasswordNavigatorInterface;
import com.odigeo.presenter.contracts.views.RequestForgottenPasswordIntructionsSentViewInterface;

public class RequestForgottenPasswordInstructionsSentPresenter extends
    BasePresenter<RequestForgottenPasswordIntructionsSentViewInterface, RequestForgottenPasswordNavigatorInterface> {

  public RequestForgottenPasswordInstructionsSentPresenter(
      RequestForgottenPasswordIntructionsSentViewInterface view,
      RequestForgottenPasswordNavigatorInterface navigator) {
    super(view, navigator);
  }

  public void goToLoginPage() {
    mNavigator.navigateToLogin();
  }
}
