package com.odigeo.presenter;

import com.odigeo.data.entity.shoppingCart.AvailableProductsResponse;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.FlowConfigurationResponse;
import com.odigeo.data.entity.shoppingCart.Insurance;
import com.odigeo.data.entity.shoppingCart.InsuranceOffer;
import com.odigeo.data.entity.shoppingCart.InsuranceShoppingItem;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.shoppingCart.request.ModifyShoppingCartRequest;
import com.odigeo.data.net.error.MslError;
import com.odigeo.interactors.AddProductsToShoppingCartInteractor;
import com.odigeo.interactors.MembershipInteractor;
import com.odigeo.interactors.RemoveProductsFromShoppingCartInteractor;
import com.odigeo.interactors.TotalPriceCalculatorInteractor;
import com.odigeo.presenter.contracts.navigators.InsurancesNavigatorInterface;
import com.odigeo.presenter.contracts.views.InsurancesViewInterface;
import com.odigeo.presenter.listeners.OnAddProductsToShoppingCartListener;
import com.odigeo.presenter.listeners.OnRemoveProductsFromShoppingCartListener;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class InsurancesPresenter
    extends BasePresenter<InsurancesViewInterface, InsurancesNavigatorInterface> {

  private static final String INVALID_BOOKING_ID = "ERR-003";
  private static final String BROKEN_FLOW = "INT-003";

  private AddProductsToShoppingCartInteractor mAddProductsToShoppingCartInteractor;
  private RemoveProductsFromShoppingCartInteractor mRemoveProductsFromShoppingCartInteractor;
  private CreateShoppingCartResponse mCreateShoppingCartResponse;
  private BigDecimal collectionMethodWithPriceValue;
  private List<String> insuranceOffersToRemove;
  private List<String> insuranceOffersToAdd;

  private TotalPriceCalculatorInteractor totalPriceCalculatorInteractor;
  private MembershipInteractor membershipInteractor;

  public InsurancesPresenter(InsurancesViewInterface view, InsurancesNavigatorInterface navigator,
      AddProductsToShoppingCartInteractor addProductsToShoppingCartInteractor,
      RemoveProductsFromShoppingCartInteractor removeProductsFromShoppingCartInteractor,
      MembershipInteractor membershipInteractor,
      TotalPriceCalculatorInteractor totalPriceCalculatorInteractor) {
    super(view, navigator);
    mAddProductsToShoppingCartInteractor = addProductsToShoppingCartInteractor;
    mRemoveProductsFromShoppingCartInteractor = removeProductsFromShoppingCartInteractor;
    this.membershipInteractor = membershipInteractor;
    this.totalPriceCalculatorInteractor = totalPriceCalculatorInteractor;
  }

  public void initializePresenter(AvailableProductsResponse availableProductsResponse,
      CreateShoppingCartResponse createShoppingCartResponse,
      FlowConfigurationResponse flowConfigurationResponse, Double lastTotalPriceTicket,
      Locale currentLocale, BigDecimal collectionMethodWithPriceValue) {
    mCreateShoppingCartResponse = createShoppingCartResponse;
    this.collectionMethodWithPriceValue = collectionMethodWithPriceValue;

    if (mCreateShoppingCartResponse != null
        && mCreateShoppingCartResponse.getShoppingCart() != null) {
      mView.updateInsurancesList(availableProductsResponse,
          mCreateShoppingCartResponse.getShoppingCart().getTravellers().size());

      List<InsuranceShoppingItem> insuranceShoppingItemList =
          mCreateShoppingCartResponse.getShoppingCart()
              .getOtherProductsShoppingItems()
              .getInsuranceShoppingItems();
      if (insuranceShoppingItemList.size() > 0) {
        for (InsuranceShoppingItem insuranceShoppingItem : insuranceShoppingItemList) {
          if (!insuranceShoppingItem.isSelectable()) {
            mView.addMandatoryWidget(insuranceShoppingItem.getInsurance());
          } else {
            mView.setInsuranceSelected(insuranceShoppingItem.getId());
          }
        }
      } else {
        for (InsuranceOffer insuranceOffer : availableProductsResponse.getInsuranceOffers()) {
          for (Insurance insurance : insuranceOffer.getInsurances()) {
            if (insurance.getType().equals(flowConfigurationResponse.getInsuranceOptout())) {
              mView.setInsuranceSelected(insuranceOffer.getId());
            }
            if (insurance.getType().equals(flowConfigurationResponse.getInsuranceRecomended())) {
              mView.setRecommendedInsurance(insuranceOffer.getId());
            }
          }
        }
      }
      mView.showRecommendedInsurance();
      checkTicketsRepricing(mCreateShoppingCartResponse.getTotalPriceTickets(),
          lastTotalPriceTicket, currentLocale);
    } else {
      mView.trackShoppingCartResponseException(mCreateShoppingCartResponse);
      mView.showGeneralError();
    }
  }

  public void updateCreateShoppingCartResponse(
      CreateShoppingCartResponse createShoppingCartResponse) {
    mCreateShoppingCartResponse = createShoppingCartResponse;
  }

  public void processInsurancesSelection(List<InsuranceOffer> insuranceOffers) {
    mView.showLoadingDialog();
    List<InsuranceShoppingItem> insuranceShoppingItemList =
        mCreateShoppingCartResponse.getShoppingCart()
            .getOtherProductsShoppingItems()
            .getInsuranceShoppingItems();
    double currentInsurancePrice = 0.0;

    insuranceOffersToRemove = new ArrayList<>();
    for (InsuranceShoppingItem insuranceShoppingItem : insuranceShoppingItemList) {
      if (insuranceShoppingItem.isSelectable() && insuranceHasBeenUnselected(insuranceOffers,
          insuranceShoppingItem)) {
        insuranceOffersToRemove.add(insuranceShoppingItem.getId());
      }
    }

    insuranceOffersToAdd = new ArrayList<>();
    for (InsuranceOffer insuranceOffer : insuranceOffers) {
      if (!isInsuranceOnCurrentShoppingCart(insuranceShoppingItemList, insuranceOffer)) {
        insuranceOffersToAdd.add(insuranceOffer.getId());
        currentInsurancePrice =
            currentInsurancePrice + insuranceOffer.getTotalPrice().doubleValue();
      }
    }

    if (insuranceOffersToRemove.size() > 0) {
      removeInsurancesFromShoppingCart(currentInsurancePrice);
    } else if (insuranceOffersToAdd.size() > 0) {
      addInsurancesToShoppingCart(currentInsurancePrice);
    } else if (mCreateShoppingCartResponse.getShoppingCart() == null) {
      mView.hideLoadingDialog();
      mView.showGeneralError();
    } else {
      for (InsuranceShoppingItem insuranceShoppingItem : insuranceShoppingItemList) {
        if (insuranceShoppingItem.isSelectable()) {
          currentInsurancePrice =
              currentInsurancePrice + insuranceShoppingItem.getTotalPrice().doubleValue();
        }
      }

      mView.hideLoadingDialog();
      mNavigator.navigateToPayment(mCreateShoppingCartResponse, currentInsurancePrice);
    }
  }

  private void removeInsurancesFromShoppingCart(final double currentInsurancePrice) {

    final ModifyShoppingCartRequest modifyShoppingCartRequest =
        createShoppingCartRequest(insuranceOffersToRemove);
    mRemoveProductsFromShoppingCartInteractor.removeProducts(modifyShoppingCartRequest,
        new OnRemoveProductsFromShoppingCartListener() {
          @Override public void onSuccess(CreateShoppingCartResponse createShoppingCartResponse) {
            if (isOutdatedBookingId(createShoppingCartResponse)) {
              mView.hideLoadingDialog();
              mView.showOutdatedBookingId();
            } else if (insuranceOffersToAdd.size() > 0) {
              addInsurancesToShoppingCart(currentInsurancePrice);
            } else if (createShoppingCartResponse.getShoppingCart() == null) {
              mView.hideLoadingDialog();
              mView.showGeneralError();
            } else {
              mView.hideLoadingDialog();
              mNavigator.navigateToPayment(createShoppingCartResponse, currentInsurancePrice);
            }
          }

          @Override public void onError(MslError error, String message) {
            mView.hideLoadingDialog();
            mNavigator.navigateToNoConnectionActivityFromRemoveInsuranceRequest();
          }
        });
  }

  private void addInsurancesToShoppingCart(final double currentInsurancePrice) {

    final ModifyShoppingCartRequest modifyShoppingCartRequest =
        createShoppingCartRequest(insuranceOffersToAdd);
    mAddProductsToShoppingCartInteractor.addProducts(modifyShoppingCartRequest,
        new OnAddProductsToShoppingCartListener() {
          @Override public void onSuccess(CreateShoppingCartResponse createShoppingCartResponse) {
            if (isOutdatedBookingId(createShoppingCartResponse)) {
              mView.hideLoadingDialog();
              mView.showOutdatedBookingId();
            } else if (createShoppingCartResponse.getShoppingCart() == null) {
              mView.hideLoadingDialog();
              mView.showGeneralError();
            } else {
              mView.hideLoadingDialog();
              mNavigator.navigateToPayment(createShoppingCartResponse, currentInsurancePrice);
            }
          }

          @Override public void onError(MslError error, String message) {
            mView.hideLoadingDialog();
            mNavigator.navigateToNoConnectionActivityFromAddInsurancesRequest();
          }
        });
  }

  private ModifyShoppingCartRequest createShoppingCartRequest(List<String> insuranceOffers) {
    ModifyShoppingCartRequest modifyShoppingCartRequest =
        new ModifyShoppingCartRequest(mCreateShoppingCartResponse.getShoppingCart().getBookingId(),
            mCreateShoppingCartResponse.getSortCriteria(), Step.INSURANCE);

    modifyShoppingCartRequest.setInsuranceOffers(insuranceOffers);
    return modifyShoppingCartRequest;
  }

  private boolean isInsuranceOnCurrentShoppingCart(
      List<InsuranceShoppingItem> insuranceShoppingItems, InsuranceOffer insuranceOffer) {
    for (InsuranceShoppingItem insuranceShoppingItem : insuranceShoppingItems) {
      if (insuranceShoppingItem.isSelectable() && insuranceShoppingItem.getId()
          .equals(insuranceOffer.getId())) {
        return true;
      }
    }
    return false;
  }

  private boolean insuranceHasBeenUnselected(List<InsuranceOffer> insuranceOffers,
      InsuranceShoppingItem insuranceShoppingItem) {
    for (InsuranceOffer insuranceOffer : insuranceOffers) {
      if (insuranceOffer.getId().equals(insuranceShoppingItem.getId())) {
        return false;
      }
    }
    return true;
  }

  private void checkTicketsRepricing(double currentTicketsPrices, double lastTicketsPrices,
      Locale currentLocale) {
    NumberFormat numberFormat = NumberFormat.getNumberInstance(currentLocale);
    DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
    decimalFormat.applyPattern("#.##");
    try {
      String sCurrentTicketsPrice = decimalFormat.format(currentTicketsPrices);
      String sLastTicketsPrice = decimalFormat.format(lastTicketsPrices);
      double formattedCurrentTicketsPrice = Double.valueOf(sCurrentTicketsPrice.replace(",", "."));
      double formattedLastTicketsPrice = Double.valueOf(sLastTicketsPrice.replace(",", "."));

      double differenceBetweenPrices = formattedCurrentTicketsPrice - formattedLastTicketsPrice;
      if (differenceBetweenPrices > 0) {
        mView.showIncreaseTicketRepricingMessage(differenceBetweenPrices);
      } else if (differenceBetweenPrices < 0) {
        mView.showDecreaseTicketRepricingMessage(Math.abs(differenceBetweenPrices));
      }
    } catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  private boolean isOutdatedBookingId(CreateShoppingCartResponse shoppingCartResponse) {
    return ((shoppingCartResponse.getMessages() != null) && (!shoppingCartResponse.getMessages()
        .isEmpty()) && ((shoppingCartResponse.getMessages()
        .get(0)
        .getCode()
        .equals(INVALID_BOOKING_ID)) || ((shoppingCartResponse.getMessages()
        .get(0)
        .getCode()
        .equals(BROKEN_FLOW)))));
  }

  public double getTotalPrice() {
    return totalPriceCalculatorInteractor.getTotalPrice(
        mCreateShoppingCartResponse.getShoppingCart(),
        mCreateShoppingCartResponse.getPricingBreakdown(), collectionMethodWithPriceValue,
        Step.INSURANCE);
  }

  public boolean isMemberForCurrentMarket() {
    return membershipInteractor.isMemberForCurrentMarket();
  }

  public boolean shouldApplyMembershipPerks() {
    return totalPriceCalculatorInteractor.shouldApplyMembershipPerks(
        mCreateShoppingCartResponse.getPricingBreakdown());
  }
}