package com.odigeo.presenter.contracts.views;

public interface TopBriefViewInterface extends BaseCustomComponentInterface{
  boolean getMembershipApplied();

  void showMembershipWithDiscount();

  void showMembershipWithNoDiscount();

  void showFullPrice();
}
