package com.odigeo.presenter.contracts.views;

import com.odigeo.data.entity.booking.Booking;

public interface MyTripsManualImportViewInterface extends BaseViewInterface {

  void enableAddBookingButton(boolean isEnabled);

  void showEmailError(boolean showError);

  void showBookingIdError(boolean showError);

  void showImportSuccess(Booking booking);

  void showImportFail();

  void showAlreadyImported(Booking booking);

  void showLoading();

  void hideLoading();

  boolean isViewVisible();
}
