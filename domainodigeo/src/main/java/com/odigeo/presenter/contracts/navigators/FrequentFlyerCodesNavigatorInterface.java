package com.odigeo.presenter.contracts.navigators;

/**
 * @author Oscar Alvarez
 * @version 1.0
 * @since 15/09/2015.
 */
public interface FrequentFlyerCodesNavigatorInterface extends BaseNavigatorInterface {

  /**
   * Show Frequent Flyer View.
   */
  void showFrequentFlyerView();

  /**
   * When the user add new code manually.
   *
   * @param passengerCode code of frequent flyer.
   * @param carrierCode carrier of frequent flyer code.
   */
  void addNewCode(long frequentFlyerId, String passengerCode, String carrierCode);

  /**
   * Remove an item from the list of frequent flyer codes.
   *
   * @param position position of item to be removed.
   */
  void removeCode(int position);
}
