package com.odigeo.presenter.contracts.views;

import com.odigeo.interactors.provider.MyTripsProvider;

public interface MyTripsViewInterface extends BaseViewInterface, LoadingView {

  void initializeBookings(MyTripsProvider bookings, boolean isRateAppPresent);

  /**
   * Displays or hides the empty view.
   *
   * @param isEmpty The state of the empty view.
   */
  void showEmptyTrips(boolean isEmpty);

  /**
   * Tracks the error on recovering the bookings on the initial load.
   */
  void trackScreenLoadError();

  /**
   * Tracks the error on updating the bookings on pull to refresh.
   */
  void trackPullToRefreshError();

  void notifyBookingsHasChanged(MyTripsProvider myTripsProvider, boolean isRateAppPresent);

  void onRequestFinished(boolean isEmpty);

  void showLoginViews();

  void showLoggedInIsUserBookings();

  void hideLoginViews();
}
