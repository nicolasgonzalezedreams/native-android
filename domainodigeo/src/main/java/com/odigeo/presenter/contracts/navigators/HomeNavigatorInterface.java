package com.odigeo.presenter.contracts.navigators;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Segment;

public interface HomeNavigatorInterface extends BaseNavigatorInterface {

  void navigateToJoinUs();

  void navigateToTravellers();

  void navigateToMyTrips();

  void navigateToSettings();

  void navigateToAbout();

  void navigateToAccountPreferences();

  void updateBackground(Segment segment);

  void navigateToWalkthrough();

  void navigateToSearch();

  void navigateToHotels(String url);

  void navigateToCars(String url);

  void navigateToTripDetail(Booking booking);

  void navigateToLastMinute();

  void navigateToPacks();
}
