package com.odigeo.presenter.contracts.views;

import com.odigeo.presenter.model.QAModeUrlModel;
import java.util.List;

public interface QAModeView extends BaseViewInterface {

  void showQAModeUrls(List<QAModeUrlModel> qaModeUrls);

  void showEmptyUrlMessage();

  void showAddQAModeUrlSuccess(List<QAModeUrlModel> qaModeUrls);

  void showQAModeUrlSelected(List<QAModeUrlModel> qaModeUrls, QAModeUrlModel selectedQAQaModeUrl);

  void disableQAModeUrlDeletion();

  void enableQAModeUrlDeletion();
}
