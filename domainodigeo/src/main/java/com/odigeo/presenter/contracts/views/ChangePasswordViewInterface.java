package com.odigeo.presenter.contracts.views;

import com.odigeo.data.net.error.MslError;

public interface ChangePasswordViewInterface extends BaseViewInterface {

  void invalidFormatCurrentPassword();

  void invalidFormatNewPassword();

  void invalidFormatRepeatPassword();

  void invalidConcurrentPassword();

  void newPasswordMatch(boolean isMatching);

  void showChangingPasswordDialog();

  void passwordChanged(boolean wasPasswordChanged);

  void newPasswordIsTheSame(boolean isTheSame);

  void changeCurrentPasswordVisibility(boolean isVisible);

  void showAuthError();

  void onLogoutError(MslError error, String message);
}
