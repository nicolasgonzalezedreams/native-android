package com.odigeo.presenter.contracts.views.holder;

import com.odigeo.presenter.contracts.views.BaseCustomComponentInterface;
import java.util.Date;

public interface CalendarHeader extends BaseCustomComponentInterface {

  void setDepartureDateLabel(Date departureDate);

  void setDepartureDate(Date departureDate);

  void setSelectDateOnDeparture();

  void resetDepartureDateLabel();

  void normalizeDepartureLabel();

  void highlightDepartureLabel();

  void shadeDepartureLabel();

  void setReturnDateLabel(Date returnDate);

  void setSelectDateOnReturn();

  void setReturnDate(Date returnDate);

  void resetReturnDateLabel();

  void normalizeReturnLabel();

  void highlightReturnLabel();

  void shadeReturnLabel();
}
