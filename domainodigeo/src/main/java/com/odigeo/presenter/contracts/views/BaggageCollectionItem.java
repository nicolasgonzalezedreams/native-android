package com.odigeo.presenter.contracts.views;

import com.odigeo.data.entity.shoppingCart.BaggageDescriptor;
import com.odigeo.data.entity.shoppingCart.SegmentTypeIndex;
import com.odigeo.data.entity.shoppingCart.SelectableBaggageDescriptor;
import com.odigeo.data.entity.shoppingCart.request.BaggageSelectionRequest;
import java.util.List;

public interface BaggageCollectionItem extends BaseCustomComponentInterface {

  void showTravelInfo();

  void showBaggageNotIncluded();

  void setBaggageIncludedWithoutKilos(int pieces);

  void setBaggageIncludedWithKilos(int pieces, int kilos);

  void showAddBaggageOption();

  void hideAddBaggageOption();

  void showBaggageOptions(List<SelectableBaggageDescriptor> selectableBaggageDescriptorList);

  SelectableBaggageDescriptor getSelectedItem();

  void setSelectedItem(SelectableBaggageDescriptor selectedBaggageDescriptor);

  BaggageSelectionRequest getBaggageSelectionRequest();

  void showBaggageSelected(SelectableBaggageDescriptor selectedBaggageDescriptor);

  void hideBaggageSelected();

  SegmentTypeIndex getSegmentTypeIndex();

  void updateSelection(BaggageDescriptor baggageDescriptor);

  void clearSelection();

  void showConfirmRemoveBaggage();

  void notifyBaggageSelectionHasChanged();

  void trackBaggageRemoved();
}