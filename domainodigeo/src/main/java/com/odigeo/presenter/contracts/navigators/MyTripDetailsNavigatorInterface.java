package com.odigeo.presenter.contracts.navigators;

import com.odigeo.data.entity.booking.Booking;

public interface MyTripDetailsNavigatorInterface extends BaseNavigatorInterface {

  String EXTRA_BOOKING = "booking";

  void navigateToBookingGuideViewer(Booking booking);

  void addToCalendar(Booking booking);

  void exitFromNavigator();

  void navigateToGroundTransportation(String url);
}
