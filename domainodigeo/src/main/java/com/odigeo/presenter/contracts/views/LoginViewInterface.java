package com.odigeo.presenter.contracts.views;

public interface LoginViewInterface extends LoginSocialInterface {

  void enableLoginButton(boolean isEnabled);

  void showPendingEmailDialog(String email);

  void setUsernameError();

  void setUserDoesNotExistError();

  void setCredentialsError();

  void setPasswordDoesNotValidate();

  void setServerError();

  void hideProgress();
}
