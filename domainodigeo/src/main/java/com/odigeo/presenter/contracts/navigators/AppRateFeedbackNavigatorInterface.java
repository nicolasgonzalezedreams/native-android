package com.odigeo.presenter.contracts.navigators;

/**
 * Created by matias.dirusso on 4/29/2016.
 */
public interface AppRateFeedbackNavigatorInterface extends BaseNavigatorInterface {

  void showThanksScreen(boolean isRestoringState);

  void showHelpImproveScreen(boolean isRestoringState);

  void navigateToPlayStore();

  void navigateToHome();

  void closeAppRateThanksNavigator();
}
