package com.odigeo.presenter.contracts.views;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.shoppingCart.BookingResponse;

public interface PaymentViewInterface extends BaseViewInterface {

  void initTopBrief();

  void updateTopBrief();

  void showLoadingResumeBooking();

  void showLoadingDialog();

  void hideLoadingDialog();

  void showRepricingInsuranceMessage(double repricingAmount);

  void showRepricingTicketsMessage(double repricingAmount);

  void showRepricingDialog(double repricingAmount);

  void showTimeoutException();

  void showNoConnectionActivity();

  void showOutdatedBookingId();

  void showGeneralMslError();

  void showVelocityError(String description);

  void showPaymentRetry();

  void trackSuccessfulPurchase();

  void trackPaymentMethodSelected(String collectionOptionType);

  void trackBookingResponseException(BookingResponse bookingResponse);

  Booking getMockedBooking();

  void updatePaymentCardDetailsWidget();
}