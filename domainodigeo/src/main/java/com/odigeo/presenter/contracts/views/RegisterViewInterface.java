package com.odigeo.presenter.contracts.views;

public interface RegisterViewInterface extends LoginSocialInterface {

  void enableSignUpButton(boolean enabled);

  void showRegisterFail();

  void showUserAlreadyExist();
}
