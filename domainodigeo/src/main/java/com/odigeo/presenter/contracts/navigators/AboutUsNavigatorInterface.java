package com.odigeo.presenter.contracts.navigators;

public interface AboutUsNavigatorInterface extends BaseNavigatorInterface {
  void openAboutFaq();

  void openAboutContactUs();

  void openTermsAndConditions();

  void openAppRate();

  void openHelpCenter();

  void openWebView(String key, String defaultUrl);

  void openTCBrand();

  void openTCAirline();

  void openPrivacyPolicy();
}
