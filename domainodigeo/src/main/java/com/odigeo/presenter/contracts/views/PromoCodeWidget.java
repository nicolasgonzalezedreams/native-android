package com.odigeo.presenter.contracts.views;

import java.math.BigDecimal;

public interface PromoCodeWidget extends BaseCustomComponentInterface {
  void expandPromoCodeWidget();

  void showPromoCodeError(String errorCode, String description);

  String getInputCode();

  void clearInputCode();

  String getValidCode();

  void setValidCode(String code);

  void setPromoCodeDiscountAmount(BigDecimal discountAmount);

  void trackPromoCodeAdded();

  void showValidateButton();

  void hideValidateButton();

  void showValidateProgressBar();

  void hideValidateProgressBar();

  void showDeleteProgressBar();

  void hideDeleteProgressBar();

  void showValidPromoCodeContainer();

  void hideValidPromoCodeContainer();

  void showDeletePromoCodeContainer();

  void hideDeletePromoCodeContainer();

  void showValidateContainer();

  void hideValidateContainer();

  void showTAC();
}