package com.odigeo.presenter.contracts.navigators;

public interface UsersNavigatorInterface extends BaseNavigatorInterface {

  /**
   * Show user list
   */
  void showUserList();

  /**
   * Show user details with a id
   *
   * @param userId User id
   */
  void showUserDetails(int userId);
}
