package com.odigeo.presenter.contracts.navigators;

/**
 * Created by matias.dirusso on 5/2/2016.
 */
public interface AppRateNavigatorInterface extends BaseNavigatorInterface {

  void navigateToThanksScreen();

  void navigateToHelpImproveScreen();

  void showAppRateView(boolean isRestoringState);
}
