package com.odigeo.presenter.contracts.views;

/**
 * @author José Eduardo Cabrera Rivera
 * @version 1.0
 * @since 11/11/2015.
 */
public interface CampaignCardViewInterface extends CardViewInterface {
}
