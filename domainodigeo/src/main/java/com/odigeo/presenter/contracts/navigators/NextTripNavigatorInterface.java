package com.odigeo.presenter.contracts.navigators;

/**
 * @author José Eduardo Cabrera Rivera
 * @version 1.0
 * @since 27/11/2015.
 */
public interface NextTripNavigatorInterface extends BaseNavigatorInterface {
}
