package com.odigeo.presenter.contracts.views;

import org.jetbrains.annotations.Nullable;

public interface BinCheckViewInterface {
  void flipCardInUI(@Nullable String paymentMethod);

  void showPaymentMethodSpinner();

  void hidePaymentMethodSpinner();

  void setCreditCardPaymentType();

  void isBinCheckValid(boolean isFinished);
}
