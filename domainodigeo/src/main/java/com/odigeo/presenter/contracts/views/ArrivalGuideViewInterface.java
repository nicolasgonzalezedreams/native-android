package com.odigeo.presenter.contracts.views;

import java.io.File;

/**
 * Created by julio.kun on 11/18/2015.
 */
public interface ArrivalGuideViewInterface extends BaseViewInterface {

  void showPdfInExternalApp(File pdfFile);

  void initPdfViewer(File pdfFile);
}
