package com.odigeo.presenter.contracts.views;

public interface PaymentPurchaseWidgetInterface {

  void openConditionsDialog();

  void setContinueButtonEnable(boolean enable);
}