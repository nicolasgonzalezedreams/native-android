package com.odigeo.presenter.contracts.navigators;

import com.odigeo.data.entity.booking.Booking;

public interface MyTripsNavigatorInterface extends BaseNavigatorInterface {

  /**
   * Navigate to booking summary with transition animation
   *
   * @param booking booking to view
   */
  void navigateToTripDetails(Booking booking, int position);

  /**
   * Navigate to booking summary without transition animation
   *
   * @param booking booking to view
   */
  void navigateToTripDetailsWithoutTransition(Booking booking);

  void navigateToPastTripDetails(Booking booking, int position);

  /**
   * Navigate to import my trips
   */
  void navigateToImportMyTrips();

  /**
   * Navigate to search flights
   */
  void navigateToSearchFlights();

  void navigateToLogin();
}
