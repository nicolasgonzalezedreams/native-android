package com.odigeo.presenter.contracts.navigators;

public interface FlightDetailsNavigatorInterface extends BaseNavigatorInterface {

  String EXTRA_BOOKING = "booking";
}
