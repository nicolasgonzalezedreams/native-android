package com.odigeo.presenter.contracts.navigators;

/**
 * @author José Eduardo Cabrera Rivera
 * @version 1.0
 * @since 11/11/2015.
 */
public interface NotificationNavigatorInterface extends BaseNavigatorInterface {
}
