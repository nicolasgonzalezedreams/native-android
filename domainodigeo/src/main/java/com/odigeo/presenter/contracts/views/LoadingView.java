package com.odigeo.presenter.contracts.views;

public interface LoadingView {

  void showLoading();

  void hideLoading();
}
