package com.odigeo.presenter.contracts.views;

import com.odigeo.data.entity.carrousel.Carousel;

/**
 * Created by ximena.perez on 23/11/2015.
 */
public interface MTTCardsViewInterface extends BaseViewInterface {
  void showCarousel(Carousel carousel);

  void hideEmptyView();
}
