package com.odigeo.presenter.contracts.views;

public interface LocalizablesUpdaterViewInterface extends BaseViewInterface {
  void onUpdatingMarketSuccess(String oneCMSTable);

  void onUpdatingMarketFail();
}
