package com.odigeo.presenter.contracts.views;

public interface SavedPaymentMethodFormViewInterface {
  void showRadioButtonText(String creditcard);

  void showExpiryDateError();

  void showCreditCardNotAccepted();

  void disableRadioButton();

  void enableRadioButton();

  boolean isRadioButtonChecked();

  void showCreditCardImage(String paymentMethod);

  void hideCVV();

  void showCVV();

  void hideCVVSeparatorToolTip();

  void hideCVVAMEXToolTip();

  void hideCVVNormalToolTip();

  void reduceCardImageOpacity();

  void hideCreditCardError();

  void increaseCardImageOpacity();

  void initCVVAmexValidator();

  void initCVVNormalValidator();

  void hideCVVToolTip();

  void showCVVAMEXToolTip();

  void showCVVNormalToolTip();

  void unCheckRadioButton();

  void checkRadioButton();

  String getCVV();

  void scrollToCVVWithError();

  boolean isTilCVVInfoValid();

  void collapseCVV();

  void expandCVV();

  void clearCVV();
}
