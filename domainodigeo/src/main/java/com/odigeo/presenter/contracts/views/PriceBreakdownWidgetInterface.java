package com.odigeo.presenter.contracts.views;

import java.math.BigDecimal;

public interface PriceBreakdownWidgetInterface extends BaseCustomComponentInterface {

  boolean isCollectionMethodWithPriceNull();

  BigDecimal getCollectionMethodWithPrice();

  void showPricesBreakdown();

  void checkUAEMessage();

  void checkTaxRefundableInfo();

  void showTaxRefundableInfo(Double taxRefundableAmount);
}