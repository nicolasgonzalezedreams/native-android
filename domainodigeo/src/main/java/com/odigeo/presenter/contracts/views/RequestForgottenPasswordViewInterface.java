package com.odigeo.presenter.contracts.views;

import com.odigeo.data.net.error.MslError;

/**
 * Created by jose.cabrera on 19/08/2015.
 */
public interface RequestForgottenPasswordViewInterface extends BaseViewInterface {

  /**
   * Enable/disable the Request Forgotten Password button
   */
  void enableButton(boolean isEnabled);

  /**
   * Show the error in the text field
   */
  void showEmailFormatError(boolean isValid);

  /**
   * Show the user name default error
   */
  void setUsernameError();

  /**
   * Show the user name empty error
   */
  void setUsernameEmpty();

  /**
   * Show the progress spinner
   */
  void showProgress(String email);

  /**
   * Hide the progress spinner
   */
  void hideProgress();

  /**
   * show an error message when the user does not exist
   */
  void showUserDoesNotExistError();

  /**
   * show an error message when a problem has ocurred
   */
  void showUnknownError();

  /**
   * show an error message when it fails from the MSL because user has already register with
   * Facebook or Google Plus
   */
  void showConnectionErrorTryFacebookOrGooglePlus(String email);

  void showAccountInactiveError(String email);

  void showAuthError();

  void onLogoutError(MslError error, String message);
}
