package com.odigeo.presenter.contracts.components;

import com.odigeo.presenter.contracts.views.BaseCustomComponentInterface;

public abstract class BaseCustomComponentPresenter<V extends BaseCustomComponentInterface> {

  protected V mView;

  public BaseCustomComponentPresenter(V view) {
    mView = view;
  }
}
