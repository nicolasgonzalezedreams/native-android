package com.odigeo.presenter.contracts.navigators;

public interface AccountPreferencesNavigatorInterface extends BaseNavigatorInterface {

  void showAccountPreferencesView();

  void showResetPasswordScreen();

  void finishNavigator();

  void navigateToLogin();
}
