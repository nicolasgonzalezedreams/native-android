package com.odigeo.presenter.contracts;

import com.odigeo.data.entity.shoppingCart.ResumeDataRequest;
import com.odigeo.presenter.contracts.navigators.BaseNavigatorInterface;

public interface ExternalPaymentNavigatorInterface extends BaseNavigatorInterface {
  void finishExternalPaymentNavigator(int externalPaymentCode, ResumeDataRequest resumeDataRequest);
}
