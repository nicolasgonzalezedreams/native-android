package com.odigeo.presenter.contracts.navigators;

import com.odigeo.presenter.model.QAModeUrlModel;

public interface QAModeNavigator extends BaseNavigatorInterface {

  void navigateBackWithSelectedQAModeUrl(QAModeUrlModel selectedQAQaModeUrl);
}
