package com.odigeo.presenter.contracts.navigators;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;

public interface InsurancesNavigatorInterface extends BaseNavigatorInterface {

  void navigateToSummary();

  void navigateToPayment(CreateShoppingCartResponse createShoppingCartResponse,
      double currentInsurancePrice);

  void navigateToNoConnectionActivityFromRemoveInsuranceRequest();

  void navigateToNoConnectionActivityFromAddInsurancesRequest();
}