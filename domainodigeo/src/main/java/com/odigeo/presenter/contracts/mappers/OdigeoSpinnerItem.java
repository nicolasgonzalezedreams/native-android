package com.odigeo.presenter.contracts.mappers;

/**
 * Interface to be implemented by each class that will be used with {@link OdigeoSpinnerItem} as
 * items
 *
 * @author M.Sc. Javier Silva Perez
 * @author Ing. Fernando Sierra Pastrana
 * @version 1.0
 * @since 13/05/15.
 */
public interface OdigeoSpinnerItem {
  int EMPTY_RESOURCE = 0;
  String EMPTY_STRING = "";

  /**
   * If the spinner item uses a key string resource to get its item content from database.
   *
   * @return The string resource to fill the spinner item content
   */
  String getShownTextKey();

  /**
   * If the spinner item contains an image, this method will return the drawable resource to use
   *
   * @return The drawable resource id to use in the spinner item, for empty values MUST return
   * {@link #EMPTY_RESOURCE}.
   */
  //@DrawableRes
  int getImageId();

  /**
   * If the spinner item uses an string to fill its content
   *
   * @return The string value to fill the spinner items, or null if this value is empty.
   */
  //@Nullable
  String getShownText();

  /**
   * Retrieves the image URL for a item.
   *
   * @return The String of the URL to be set.
   */
  //@Nullable
  String getImageUrl();

  /**
   * Gets the placeholder if the view need it.
   *
   * @return resource's id.
   */
  int getPlaceHolder();

  //@Nullable
  Object getData();
}
