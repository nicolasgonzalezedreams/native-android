package com.odigeo.presenter.contracts.views;

public interface NavigationDrawerViewInterface extends BaseViewInterface {

  void refreshNavigationDrawer(int resourceDrawable, String firstText, String secondText);

  void refreshNavigationDrawerUserLogged();

  void refreshNavigationDrawerUserUnLogged();

  void refreshNavigationDrawerUserInactive(String userEmail);

  void refreshMenu();

  void showLogOutConfirmationAlert();

  void hideUserName();

  void showUserName(String name);

  void closeDrawer();

  boolean drawerIsOpened();

  void updateCarousel();
}
