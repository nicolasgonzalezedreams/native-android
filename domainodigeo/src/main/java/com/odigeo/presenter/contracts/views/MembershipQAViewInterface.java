package com.odigeo.presenter.contracts.views;

import com.odigeo.data.entity.userData.Membership;
import java.util.List;

public interface MembershipQAViewInterface extends BaseViewInterface {

  void hideNotMemberLabel();

  void showNotMemberLabel();

  void hideAddMembershipButton();

  void showAddMembershipButton();

  void updateMembershipList(List<Membership> memberships);
}
