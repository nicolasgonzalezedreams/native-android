package com.odigeo.presenter.contracts.views;

/**
 * Created by carlos.navarrete on 10/09/15.
 */
public interface IdentificationsViewInterface extends BaseViewInterface {

  void showOnNumberIdentificationError();

  void showOnExpireDateError();

  void showOnCountrySelectedError();
}
