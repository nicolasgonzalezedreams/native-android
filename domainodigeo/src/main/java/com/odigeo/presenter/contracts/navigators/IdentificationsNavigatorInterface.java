package com.odigeo.presenter.contracts.navigators;

import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.userData.UserIdentification;

/**
 * Created by carlos.navarrete on 21/09/15.
 */
public interface IdentificationsNavigatorInterface extends BaseNavigatorInterface {

  void showBackView();

  void setValuesActionBar(String title, int idIcon);

  void returnIdentifications(String numberIdentification, long dateExpiration,
      Country identificationCountry, UserIdentification.IdentificationType identificationType);

  void returnForResult();

  void navigateToSelectCountry(String fragmentTag, int constant);
}
