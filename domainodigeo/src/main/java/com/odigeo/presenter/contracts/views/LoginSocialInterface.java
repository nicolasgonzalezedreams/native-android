package com.odigeo.presenter.contracts.views;

import com.odigeo.data.net.error.MslError;

public interface LoginSocialInterface extends BaseViewInterface {

  String IMAGE_URL = "image_url";
  String EMAIL = "email";
  String NAME = "name";
  String FIRST_NAME = "first_name";
  String LAST_NAME = "last_name";
  String GENDER = "gender";
  String TOKEN = "token";
  String PASSWORD = "password";

  String FACEBOOK_SOURCE = "facebook";
  String GOOGLE_SOURCE = "google";
  String PASSWORD_SOURCE = "password";

  String FEMALE_GENDER = "Female";
  String FEMALE_GENDER_ABREVIATED = "F";
  String MALE_GENDER = "Male";

  void loginSuccess(String source, String email);

  void hideProgress();

  void showFacebookAuthError();

  void showFacebookPermissionError();

  void showUnknownLoginError();

  void showGoogleAuthError();

  void showAuthError();

  void onLogoutError(MslError error, String message);

  String getSSOTrackingCategory();
}
