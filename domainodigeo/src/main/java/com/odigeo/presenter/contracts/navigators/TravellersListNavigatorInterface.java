package com.odigeo.presenter.contracts.navigators;

import com.odigeo.data.entity.userData.UserIdentification;

/**
 * Created by carlos.navarrete on 28/08/15.
 */
public interface TravellersListNavigatorInterface extends BaseNavigatorInterface {

  void showTravellerDetailView(String title, long id, boolean isDefaultTraveller);

  void navigateToIdentifications(UserIdentification.IdentificationType typeIdentification,
      int requestCode, UserIdentification userIdentification);

  void showBackView();

  void setValuesActionBar(String title, int idIcon);

  void navigateToSelectCountry(String fragmentTag, int constant);

  void navigateToCountryCode();

  void navigateToLogin();

  /**
   * Show Frequent Flyer Codes View.
   */
  void navigateToFrequentFlyerCodes();

    /*
     * Retrieves identification elements
     */

  void returnIdentifications(String numberIdentification, long dateExpiration,
      String identificationCountryCode, UserIdentification.IdentificationType identificationType);
}
