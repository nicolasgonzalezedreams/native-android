package com.odigeo.presenter.contracts.views;

/**
 * Created by matia on 11/23/2015.
 */
public interface ArrivalGuidesProviderViewInterface extends BaseViewInterface {

  void showDownloadGuideState();

  void showGuideDownloadedState();

  void showDownloadingGuideState();

  void showGuideInDifferentLanguageMessage();

  void showDownloadProgress(double progress, double totalDownloadSize);

  void showNoConnectionDialog();

  void showDownloadFailedDialog();

  void showInsufficientSpaceDialog();

  void showDeleteGuideConfirmationDialog(String arrivalCityName);
}