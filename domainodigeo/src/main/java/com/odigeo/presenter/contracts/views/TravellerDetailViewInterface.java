package com.odigeo.presenter.contracts.views;

import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.net.error.MslError;
import java.util.List;

/**
 * Created by carlos.navarrete on 31/08/15.
 */
public interface TravellerDetailViewInterface extends BaseViewInterface {

  void showLoadingDialog();

  void hideLoadingDialog();

  void showSavedErrorDialog();

  void setUserFrequentFlyerList(List<UserFrequentFlyer> frequentFlyerList);

  List<UserFrequentFlyer> getUserFrequentFlyer();

  void showDateError();

  void showAuthError();

  void onLogoutError(MslError error, String message);

  void showSecondSurnameView();
}
