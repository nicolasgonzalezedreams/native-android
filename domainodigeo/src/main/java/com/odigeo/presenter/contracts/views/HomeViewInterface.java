package com.odigeo.presenter.contracts.views;

import com.odigeo.data.entity.carrousel.Carousel;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;

/**
 * @author José Eduardo Cabrera Rivera
 * @version 1.0
 * @since 11/11/2015.
 */
public interface HomeViewInterface extends BaseViewInterface {

  void setCampaignBackgroundImage();

  void setCityBackgroundImage(String imageUrl);

  void setDefaultBackgroundImage();

  void showCards(Carousel carousel);

  void translateBackgroundPosition(float position);

  void synchronizeData(OnAuthRequestDataListener<Void> listener);

  void showAuthErrorMessage();
}
