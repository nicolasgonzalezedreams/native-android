package com.odigeo.presenter.contracts.views;

import com.odigeo.data.entity.booking.Booking;

public interface MyTripDetailsViewInterface extends BaseViewInterface {

  void setupRefreshBooking();

  void hideRefreshing();

  void disableRefreshBooking();

  void updateBookingStatus(Booking updatedBooking);
}
