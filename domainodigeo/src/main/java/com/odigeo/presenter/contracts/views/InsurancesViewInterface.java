package com.odigeo.presenter.contracts.views;

import com.odigeo.data.entity.shoppingCart.AvailableProductsResponse;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.Insurance;

public interface InsurancesViewInterface extends BaseViewInterface {

  void updateInsurancesList(AvailableProductsResponse availableProductsResponse,
      int totalPassengers);

  void addMandatoryWidget(Insurance insurance);

  void setInsuranceSelected(String insuranceSelected);

  void setRecommendedInsurance(String insuranceRecommended);

  void showRecommendedInsurance();

  void hideLoadingDialog();

  void showLoadingDialog();

  void showIncreaseTicketRepricingMessage(double difference);

  void showDecreaseTicketRepricingMessage(double difference);

  void showOutdatedBookingId();

  void showGeneralError();

  void trackShoppingCartResponseException(CreateShoppingCartResponse shoppingCartResponse);
}