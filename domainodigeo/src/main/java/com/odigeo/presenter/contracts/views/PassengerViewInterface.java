package com.odigeo.presenter.contracts.views;

import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.shoppingCart.BuyerIdentificationType;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.TravellerRequiredFields;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.Date;
import java.util.List;

public interface PassengerViewInterface
    extends com.odigeo.presenter.contracts.views.BaseViewInterface {
  void showContactImage();

  void showPhoneCode(String phoneCodeAndCountry, Country countryPhoneCode);

  void hideShowOrHideButton();

  void showLoadingDialog();

  void showContactResidentCountry(String country);

  void hideLoadingDialog();

  void showCollapseEditText();

  void showCollapseCloseText();

  void showContactSpinnerItem(int position);

  void showContactDateOfBirth(String dateOfBirth);

  void showContactIdentification(String identification);

  void showContactCPF(String cpf);

  void showContactAddress(String address);

  void showContactCity(String city);

  void showContactState(String state);

  void showContactZipCode(String zipcode);

  void showContactPhoneNumber(String phoneNumber);

  void showContactAlternativePhoneNumber(String alternativePhone);

  void showContactCountryCode(Country country);

  void showContactEditableMail(String mail);

  void setIdentificationTypePosition(int position);

  void setBirthDateCalendarMaxAndMin(Date minDate, Date maxDate);

  void showContactName();

  void showContactLastName();

  void showContactName(String name);

  void showContactLastName(String lastName);

  void showIncreaseTicketRepricingMessage(double difference);

  void showDecreaseTicketRepricingMessage(double difference);

  void showUnEditableEmail(String email);

  void showValidationErrorMessage();

  void showOutdatedBookingId();

  void showGeneralError();

  void invalidCPF();

  void setNullTextWatchersError();

  void hideContactHeader();

  void hideFieldsContainer();

  void hideContactName();

  void hideContactLastName();

  void hideContactEditableMail();

  void hideContactIdentification();

  void hideContactDateOfBirth();

  void hideContactCPF();

  void hideContactAddress();

  void hideContactCity();

  void hideContactState();

  void hideContactZipCode();

  void hideContactPhoneNumber();

  void hideContactAlternativePhoneNumber();

  void hideContactCountryCode();

  void hideFooterMail();

  String getContactName();

  String getContactLastName();

  int getBuyerIndexSelected();

  String getIdentificationBuyerType();

  String getContactIdentification();

  String getContactCPF();

  String getContactDateOfBirth();

  String getContactAddress();

  String getContactCity();

  String getContactState();

  String getContactZipCode();

  String getCountryCode();

  String getContactPhoneCode();

  String getContactPhoneNumber();

  String getPhoneCode();

  String getContactAlternativePhoneNumber();

  String getContactEmail();

  List<UserTraveller> getUserTravellers();

  void addPassengersWidgetViews(List<TravellerRequiredFields> requiredTravellerInformation);

  void initPassengerCustomKeyboard();

  void initIdentificationAdapter(List<BuyerIdentificationType> identificationTypes);

  void setDefaultPassenger(boolean hasMoreNonChildPassengers);

  void trackCrashlyticException(Exception e);

  void trackShoppingCartResponseException(CreateShoppingCartResponse shoppingCartResponse);

  void trackPassengerValidationException(CreateShoppingCartResponse shoppingCartResponse);

  void initPersuasionSnackBar();

  void initContactCustomKeyBoard();

  void autoCompleteGeolocalizationInfo();

  void trackCountryException(String countryCode, String mLanguageIsoCode);
}
