package com.odigeo.presenter.contracts.views;

public interface FlightDetailsViewInterface extends BaseViewInterface {

  String EXTRA_BOOKING = "booking";
}
