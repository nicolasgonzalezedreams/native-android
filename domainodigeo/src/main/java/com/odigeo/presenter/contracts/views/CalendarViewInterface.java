package com.odigeo.presenter.contracts.views;

import com.odigeo.data.entity.TravelType;
import java.util.Date;
import java.util.List;

public interface CalendarViewInterface extends BaseViewInterface {

  void setCalendarBounds(Date minDate, Date maxDate, List<Date> lstSelectedDates, int mode,
      boolean avoidScrolling);

  void selectDateOnCalendar(Date date);

  void setCalendarFilter(Date minSelectableDateInLocal);

  void setSegmentCalendarFilter();

  void showSkipReturnButton();

  void hideSkipReturnButton();

  void showConfirmationButton();

  void hideConfirmationButton();

  void setConfirmationButtonDates(Date departureDate, Date returnDate);

  void setConfirmationButtonDate(Date departureDate);

  void setDepartureDateHeader(Date departureDate);

  void setReturnDateHeader(Date returnDate);

  void resetDepartureDateHeader();

  void resetReturnDateHeader();

  void showRoundTripHeader();

  void hideRoundTripHeader();

  void setSelectorBackground(TravelType travelType);
}
