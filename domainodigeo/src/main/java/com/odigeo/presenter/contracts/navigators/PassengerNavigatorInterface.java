package com.odigeo.presenter.contracts.navigators;

import com.odigeo.data.entity.shoppingCart.Carrier;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.PassengerConflictDetails;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import java.util.List;

public interface PassengerNavigatorInterface extends BaseNavigatorInterface {

  void navigateToCountryResident(int widgetPosition);

  void navigateToCountryNationality(int widgetPosition);

  void navigateToCountryIdentification(int widgetPosition);

  void navigateToPhoneCode();

  void navigateToFrequentFlyers(List<UserFrequentFlyer> frequentFlyers, List<Carrier> carriers,
      int passengerWidgetPosition, List<Carrier> filteredCarriers);

  void navigateToDuplicateBooking(PassengerConflictDetails passengerConflictDetails,
      List<String> clientBookingIds);

  void navigateToInsurance(CreateShoppingCartResponse createShoppingCartResponse, int numBaggages);

  void navigateToPayment(CreateShoppingCartResponse createShoppingCartResponse, int numBaggages);

  void navigateToHome();

  void navigateToNoConnectionActivity();

  void navigateToLogin();

  void navigateToSearch();
}