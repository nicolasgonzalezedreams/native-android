package com.odigeo.presenter.contracts.navigators;

public interface SocialLoginNavigatorInterface extends BaseNavigatorInterface {

  void showUserRegisteredFacebook(String email);

  void showUserRegisteredGoogle(String email);

  void navigateToHome();

  void navigateToLogin();

  void onTimeOutError();

  String getSSOTrackingCategory();
}
