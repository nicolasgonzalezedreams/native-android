package com.odigeo.presenter.contracts.views;

import com.odigeo.data.entity.shoppingCart.ItinerarySortCriteria;
import com.odigeo.data.entity.shoppingCart.request.BaggageSelectionRequest;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import java.util.Date;
import java.util.List;

public interface PassengerWidget extends BaseCustomComponentInterface {

  void initResidentGroupAdapter(List<String> residentGroup);

  void initResidentLocalitiesAdapter(List<String> residentLocalities);

  void showSpinnerNonMembershipTitle();

  void showCardTitleAdult();

  void showCardTitleChild();

  void showCardTitleInfant();

  void showImagePassengerProfile();

  void hideFieldsContainer();

  void hidePassengerHeaderSpinner();

  void hideButtonOpenOrCloseDetails();

  void hideBirthdate();

  void hideNameEditable();

  void hideMiddleNameEditable();

  void hideLastNameEditable();

  void hideSecondLastNameEditable();

  void hideIdentification();

  void hideFrequentFlyer();

  void hideResidentWidgetGroup();

  void showSpinnerMembershipTitle();

  void showCollapseEditText();

  void showCollapseCloseText();

  void showSpinnerName(int position);

  void showName(String name);

  void showLastName(String lastName);

  void showMiddleName(String middleName);

  void showSecondLastname(String secondLastName);

  void showBirthdate(String birthdate);

  void showCountryOfResident(String countryOfResident);

  void showNationality(String nationality);

  void showIdentificationNumber(String identificationNumber);

  void showIdentificationExpiration(String expirationDate);

  void showIdentificationCountry(String identificationCountry);

  void showFrequentFlyer(String frequentFlyerInformation);

  void showInfantBirthdateEror();

  void showChildBirthdateError();

  void enableIdentificationTypeMultipleOptions();

  void setIdentificationTypePosition(int position);

  void enableIdentificationTypeUniqueOption();

  void setBirthDateCalendarMaxAndMin(Date minDate, Date maxDate);

  void setExpirationIdentificationCalendarMaxAndMin();

  void hideTitle();

  void hideNationality();

  void hideIdentificationExpirationDate();

  void hideCountryOfResidence();

  void hideIdentificationCountry();

  int getWidgetPosition();

  void setNullTextWatchersError();

  boolean isPassengerSelectedInAnotherTravellerSpinner(String spinnerPosition);

  String getPassengerType();

  String getPassengerTitleName();

  String getPassengerName();

  String getPassengerMiddleName();

  String getPassengerFirstLastName();

  String getPassengerSecondLastName();

  String getPassengerDayOfBirth();

  String getPassengerNationalityCountryCode();

  String getPassengerResidentCountryCode();

  String getPassengerIdentification();

  String getPassengerIdentificationType();

  String getPassengerIdentificationExpiration();

  String getPassengerIdentificationCountryCode();

  String getLocalityResidentCode();

  List<BaggageSelectionRequest> getPassengerBaggage();

  List<UserFrequentFlyer> getPassengerFrequentFlyerCode();

  int getPassengerSelected();

  boolean isThereAnyTextInputLayoutEmpty();

  void updateBaggageSelection(List<BaggageSelectionRequest> baggageSelectionRequestList);

  void disableApplyBaggageSelection();

  void showMembershipAlert();

  void showImageMembershipProfile();

  void hideImageMembershipProfile();

  void propagateItinerarySortCriteria(ItinerarySortCriteria itinerarySortCriteria);

  void showMembershipPrices();

  void selectTitleMR();

  void selectTitleMRS();
}