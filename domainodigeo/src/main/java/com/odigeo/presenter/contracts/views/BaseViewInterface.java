package com.odigeo.presenter.contracts.views;

public interface BaseViewInterface {

  boolean isActive();
}