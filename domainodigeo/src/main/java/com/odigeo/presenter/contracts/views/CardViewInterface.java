package com.odigeo.presenter.contracts.views;

public interface CardViewInterface extends BaseViewInterface {
  void setCardPosition(int position);
}
