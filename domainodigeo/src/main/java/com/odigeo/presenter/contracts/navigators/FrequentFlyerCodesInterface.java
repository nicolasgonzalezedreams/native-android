package com.odigeo.presenter.contracts.navigators;

import com.odigeo.presenter.contracts.views.BaseViewInterface;

/**
 * Author: Oscar Alvarez
 * Date: 15/09/2015.
 */
public interface FrequentFlyerCodesInterface extends BaseViewInterface {
}
