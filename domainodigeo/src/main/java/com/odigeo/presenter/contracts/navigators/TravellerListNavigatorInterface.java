package com.odigeo.presenter.contracts.navigators;

/**
 * Created by matias.dirusso on 10/08/2015.
 */
public interface TravellerListNavigatorInterface extends BaseNavigatorInterface {

  void navigateToEditTraveller();

  void navigateToAddTraveller();
}
