package com.odigeo.presenter.contracts.views;

import java.util.Map;

public interface ExternalPaymentViewInterface extends BaseViewInterface {
  void hideProgressBar();

  void loadUrl(String paymentUrl);

  void onBackPressed();

  void loadPostUrl(String redirectUrl, Map<String, String> params);
}
