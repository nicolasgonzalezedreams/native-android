package com.odigeo.presenter.contracts.views;

import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.userData.CreditCard;

public interface PaymentFormWidgetInterface {

  String getCVV();

  String getPaymentMethodCodeDetected();

  String getExpirationMonth();

  String getExpirationYear();

  int getPaymentMethodSpinnerVisibility();

  void showCreditCardWidget();

  void showBankTransferRadioButton();

  void showBankTransferSeparator();

  void showPaypalRadioButton();

  void showTrustlyRadioButton();

  void showKlarnaRadioButton();

  void showPaypalWidget();

  void showTrustlyWidget();

  void showKlarnaWidget();

  void showPaypalSeparator();

  void showTrustlySeparator();

  void showKlarnaSeparator();

  void showCVVTooltip();

  void hideCVVTooltip();

  void hideCreditCardWidget();

  void hidePaypalWidget();

  void hideTrustlyWidget();

  void hideKlarnaWidget();

  void hideCreditCardRadioButton();

  void showCreditCardRadioButton();

  void showBankTransferWidget();

  void updateCVVAmexValidator();

  void updateCVVNormalValidator();

  void trackBinCheckSuccess();

  void trackBinCheckNoDetected();

  void setCreditCardCode(String code);

  boolean validateCreditCardFields();

  void onCheckAllValidations(boolean areCorrect);

  void setPaypalFee(Double fee);

  void setTrustlyFee(Double fee);

  void setKlarnaFee(Double fee);

  boolean validateExpirationDate();

  void showCVVTooltipAllTypes();

  void showCVVTooltipAMEX();

  void showCVVTooltipVisaOrMastercard();

  void showSavePaymentMethodSwitch();

  void trackCVVTooltip(String length);

  void showSavedPaymentMethodRow(CreditCard creditCard, boolean checkByDefault);

  void unCheckRadioButtons(Object tag);

  void setPaymentMethodsUnchecked();

  boolean isCreditCardFocused();

  boolean isStoreMethodCheck();

  void setBankTransferChecked();

  void setCreditCardChecked();

  void setPaypalChecked();

  void setTrustlyChecked();

  void setKlarnaChecked();

  String getCreditCardOwner();

  String getCreditCardNumber();

  CollectionMethodType getCollectionMethodTypeDetected();

  void setCreditCardPaymentType();

  void savedPaymentMethodWasSelected();

  void hideCreditCardSeparator();

  void hideBankTransfer();

  void clearCreditCardFields();

  void initCreditCardSpinner();

  void performClickOnCreditCardRadioButton();

  boolean isSavedPaymentMethodAdded();
}