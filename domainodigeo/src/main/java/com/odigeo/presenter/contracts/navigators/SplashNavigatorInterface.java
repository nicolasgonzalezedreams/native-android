package com.odigeo.presenter.contracts.navigators;

/**
 * Created by daniel.morales on 28/3/17.
 */

public interface SplashNavigatorInterface extends BaseNavigatorInterface {
  void goToWalkThroughActivity();

  void goToHomeActivity();

  void startFirebaseRegistrationTokenService();
}
