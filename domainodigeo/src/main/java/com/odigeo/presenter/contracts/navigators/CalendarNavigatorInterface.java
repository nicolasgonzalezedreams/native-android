package com.odigeo.presenter.contracts.navigators;

import com.odigeo.data.entity.TravelType;
import java.util.Date;
import java.util.List;

public interface CalendarNavigatorInterface extends BaseNavigatorInterface {
  void setResultAndFinish(int segmentNumber, TravelType travelType, List<Date> selectedDates);
}
