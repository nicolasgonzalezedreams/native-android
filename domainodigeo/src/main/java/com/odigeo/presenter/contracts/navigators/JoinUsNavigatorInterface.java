package com.odigeo.presenter.contracts.navigators;

public interface JoinUsNavigatorInterface extends BaseNavigatorInterface {

  void showLoginView();

  void showRegisterView();
}
