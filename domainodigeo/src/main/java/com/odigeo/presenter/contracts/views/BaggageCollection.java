package com.odigeo.presenter.contracts.views;

import com.odigeo.data.entity.shoppingCart.BaggageConditions;
import com.odigeo.data.entity.shoppingCart.request.BaggageSelectionRequest;
import java.util.List;

public interface BaggageCollection extends BaseCustomComponentInterface {

  void hideBaggageCollection();

  List<BaggageSelectionRequest> getBaggageSelections();

  void updateBaggageSelections(List<BaggageSelectionRequest> baggageSelectionRequestList);

  void addBaggageCollectionItem(BaggageConditions baggageConditions, String segmentDirection,
      String carrierCode, String arrival, String departure, int preselectedBaggagePosition,
      boolean hasSelectableBaggage, boolean isRoundTrip);

  void addSeparatorOnBaggageList();

  String getOutBound();

  String getInBound();

  String getLeg(int position);

  String getRoundTrip();

  void showHeaderSubtitleInfo();

  void showPersuasiveMessage();

  void setDefaultBaggageSelectionToAllPassengers(boolean isChecked);
}