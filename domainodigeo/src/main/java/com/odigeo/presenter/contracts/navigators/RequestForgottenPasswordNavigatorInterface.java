package com.odigeo.presenter.contracts.navigators;

/**
 * Created by matias.dirusso on 10/08/2015.
 */
public interface RequestForgottenPasswordNavigatorInterface extends BaseNavigatorInterface {

  void navigateToLogin();

  void showInstructionsPage(String email);
}
