package com.odigeo.presenter.contracts.views;

public interface BaseCustomComponentInterface {

  void initComponent();

  int getComponentLayout();

  void initOneCMSText();
}