package com.odigeo.presenter.contracts.views;

/**
 * Created by matias.dirusso on 4/29/2016.
 */
public interface AppRateHelpImproveViewInterface extends BaseViewInterface {

  void changeButtonHelpImproveStatus(boolean enable);

  void hideProgress();

  void showSuccessFeedback();

  void showErrorDialog();

  void setEmail(String email);
}
