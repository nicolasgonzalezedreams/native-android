package com.odigeo.presenter.contracts.navigators;

public interface BaseNavigatorInterface {

  void setNavigationTitle(String title);

  void onAuthProblem();
}
