package com.odigeo.presenter.contracts.navigators;

public interface RegisterNavigatorInterface extends SocialLoginNavigatorInterface {

  void registerSuccess(String email);

  void navigateToLogIn(String email);
}
