package com.odigeo.presenter.contracts.navigators;

/**
 * Created by matias.dirusso on 10/08/2015.
 */
public interface ChangePasswordNavigatorInterface extends BaseNavigatorInterface {

  /**
   * Returns to Account Preferences screen
   */
  void navigateToAccountPreferences();

  /**
   * Show Change Password view
   */
  void showChangePasswordView();
}
