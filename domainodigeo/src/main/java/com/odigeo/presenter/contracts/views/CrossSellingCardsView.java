package com.odigeo.presenter.contracts.views;

import com.odigeo.data.entity.booking.Booking;

public interface CrossSellingCardsView extends BaseViewInterface {

  void openCityGuide(Booking booking);

  void openCars(Booking booking);

  void openGroundTransportation(Booking booking);

  void openHotels(Booking booking);

  void trackGroundTransportationCardAppearance();

  void showDownloadProgress(double progress, double totalDownloadSize);

  void showNoConnectionDialog();

  void showDownloadFailedDialog();

  void showInsufficientSpaceDialog();

  void showGuideDownloading();

  void hideGuideDownloading();

  boolean checkStoragePermission();

  void onStoragePermissionAccepted();
}
