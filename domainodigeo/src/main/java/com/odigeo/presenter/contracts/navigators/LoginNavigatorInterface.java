package com.odigeo.presenter.contracts.navigators;

public interface LoginNavigatorInterface extends SocialLoginNavigatorInterface {

  void navigateToRequestForgottenPassword(String email);

  void showBlockedAccountError(String email);

  void showLegacyUserError(String email);

  void showLoginView();

  void showLoginViewUserAlreadyRegistered(String email);

  void openMailApp();

  void navigateToPendingEmailScreen();

  void returnToPassengerWithLoginSuccess();

  boolean isComingFromPassenger();

  boolean isComingFromMyTrips();

  void returnToMyTripsWithLoginSuccess();
}

