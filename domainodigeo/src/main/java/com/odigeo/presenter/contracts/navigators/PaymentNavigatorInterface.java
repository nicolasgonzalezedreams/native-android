package com.odigeo.presenter.contracts.navigators;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.shoppingCart.BankTransferData;
import com.odigeo.data.entity.shoppingCart.BankTransferResponse;
import com.odigeo.data.entity.shoppingCart.BookingDetail;
import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.MarketingRevenueDataAdapter;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.userData.InsertCreditCardRequest;

import java.util.List;

public interface PaymentNavigatorInterface extends BaseNavigatorInterface {

  void navigateToSummary();

  void navigateToBookingConfirmed(InsertCreditCardRequest savePaymentMethodRequest,
      String cardNumberObfuscated, CreateShoppingCartResponse createShoppingCartResponse,
      BookingDetail bookingDetail, List<MarketingRevenueDataAdapter> marketingRevenue,
      BankTransferResponse bankTransferResponse, List<BankTransferData> bankTransferData,
      Booking booking, ShoppingCartCollectionOption mShoppingCartCollectionOption);

  void navigateToBookingRejected(BookingDetail bookingDetail);

  void navigateToMockBooking(BookingDetail bookingDetail, Booking booking);

  void setHasBeenRepricing();

  void navigateToExternalPayment(BookingResponse bookingResponse,
      CollectionMethodType collectionMethodType);
}