package com.odigeo.presenter.contracts.navigators;

import com.odigeo.data.entity.booking.Booking;
import java.io.Serializable;

public interface NavigationDrawerNavigatorInterface extends BaseNavigatorInterface {

  void navigateToJoinUs();

  void navigateToLogin();

  void navigateToTravellers();

  void navigateToMyTrips();

  void navigateToSettings();

  void navigateToAbout();

  void navigateToAccountPreferences();

  void navigateToWalkthrough();

  void navigateToSearch();

  void navigateToSearchWithSearchOptions(Serializable searchOptions);

  void navigateToHotels(String url);

  void navigateToCars(String url, boolean showVoucher);

  void navigateToTripDetail(Booking booking);

  void navigateToLastMinute();

  void navigateToPacks();

  void onAuthError(String source);

  boolean hasChangedMarket();
}
