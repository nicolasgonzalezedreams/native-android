package com.odigeo.presenter.contracts.navigators;

/**
 * Created by julio.kun on 11/18/2015.
 */
public interface ArrivalGuideViewerNavigatorInterface extends BaseNavigatorInterface {

  String EXTRA_BOOKING = "booking";

  void showDestinationTitle(String destination);
}
