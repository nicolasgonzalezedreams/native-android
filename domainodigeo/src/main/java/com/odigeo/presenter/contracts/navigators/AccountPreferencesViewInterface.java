package com.odigeo.presenter.contracts.navigators;

import com.odigeo.data.net.error.MslError;
import com.odigeo.presenter.contracts.views.BaseViewInterface;

public interface AccountPreferencesViewInterface extends BaseViewInterface {

  void showCantChangeEmailDialog();

  void showDeleteAccountDialog();

  void initializeUserEmail(String email);

  void showRemovingAccountDialog();

  void accountRemoved(boolean isCorrect);

  void showAuthError();

  void onLogoutError(MslError error, String message);
}
