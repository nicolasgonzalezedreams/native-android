package com.odigeo.presenter.contracts.mappers.xsell;

import com.odigeo.data.entity.xsell.CrossSelling;
import com.odigeo.presenter.model.CrossSellingCard;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class CrossSellingCardMapper {

  public CrossSellingCard transform(CrossSelling crossSelling) {
    return new CrossSellingCard(crossSelling.getBooking(), crossSelling.isShowLabel(),
        crossSelling.getLabel(), crossSelling.getTitle(), crossSelling.getType(),
        crossSelling.getSubtitle());
  }

  public Collection<CrossSellingCard> transform(Collection<CrossSelling> crossSellings) {
    Collection<CrossSellingCard> crossSellingCards;
    if (crossSellings != null && !crossSellings.isEmpty()) {
      crossSellingCards = new ArrayList<>();
      for (CrossSelling crossSelling : crossSellings) {
        crossSellingCards.add(transform(crossSelling));
      }
    } else {
      crossSellingCards = Collections.emptyList();
    }

    return crossSellingCards;
  }
}
