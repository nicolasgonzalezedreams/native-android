package com.odigeo.presenter;

import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.shoppingCart.BaggageApplicationLevel;
import com.odigeo.data.entity.shoppingCart.BaggageConditions;
import com.odigeo.data.entity.shoppingCart.BaggageDescriptor;
import com.odigeo.data.entity.shoppingCart.BaggageSelectionResponse;
import com.odigeo.data.entity.shoppingCart.Itinerary;
import com.odigeo.data.entity.shoppingCart.Location;
import com.odigeo.data.entity.shoppingCart.Section;
import com.odigeo.data.entity.shoppingCart.SectionResult;
import com.odigeo.data.entity.shoppingCart.Segment;
import com.odigeo.data.entity.shoppingCart.SegmentTypeIndex;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.data.entity.shoppingCart.request.BaggageSelectionRequest;
import com.odigeo.presenter.contracts.components.BaseCustomComponentPresenter;
import com.odigeo.presenter.contracts.views.BaggageCollection;
import com.odigeo.presenter.contracts.views.BaggageCollectionItem;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BaggageCollectionPresenter extends BaseCustomComponentPresenter<BaggageCollection> {

  private Itinerary itinerary;
  private BaggageApplicationLevel baggageApplicationLevel;
  private List<Traveller> travellers;
  private int passengerWidgetPosition;
  private TravelType travelType;

  public BaggageCollectionPresenter(BaggageCollection view) {
    super(view);
  }

  public void processBaggage(List<BaggageConditions> baggageConditionsList, Itinerary itinerary,
      List<Traveller> travellers, int passengerWidgetPosition, TravelType travelType) {
    this.itinerary = itinerary;
    this.travellers = travellers;
    this.passengerWidgetPosition = passengerWidgetPosition;
    this.travelType = travelType;

    if (!baggageConditionsList.isEmpty()) {
      buildBaggageSelectionItems(baggageConditionsList);
    } else {
      mView.hideBaggageCollection();
    }
  }

  private void buildBaggageSelectionItems(List<BaggageConditions> baggageConditionsList) {
    if (checkIfAllCarriersAreTheSame(baggageConditionsList) && (
        checkIfAllBaggageConditionsNotIncludeBaggage(baggageConditionsList)
            || checkIfAllBaggageConditionsIncludedTheSameBaggage(baggageConditionsList))) {
      addBaggageCollectionItem(baggageConditionsList.get(0), -1, true);
    } else {
      Collections.sort(baggageConditionsList, new Comparator<BaggageConditions>() {
        @Override public int compare(BaggageConditions baggageCondition1,
            BaggageConditions baggageCondition2) {
          return baggageCondition1.getSegmentTypeIndex()
              .compareTo(baggageCondition2.getSegmentTypeIndex());
        }
      });

      int i = 0;
      for (BaggageConditions baggageConditions : baggageConditionsList) {
        if (i > 0) {
          mView.addSeparatorOnBaggageList();
        }
        addBaggageCollectionItem(baggageConditions, i, false);
        i++;
      }
    }
  }

  private boolean checkIfAllCarriersAreTheSame(List<BaggageConditions> baggageConditionsList) {
    Segment firstSegment = getSegment(baggageConditionsList.get(0));
    Section firstSection = getSectionById(firstSegment.getSections().get(0));
    String firstCarrier = getCarrierCode(firstSection.getCarrier());

    for (int i = 1; i < baggageConditionsList.size(); i++) {
      Segment segment = getSegment(baggageConditionsList.get(i));
      Section section = getSectionById(segment.getSections().get(0));
      if (!firstCarrier.equals(getCarrierCode(section.getCarrier()))) {
        return false;
      }
    }
    return true;
  }

  private Segment getSegment(BaggageConditions baggageConditions) {
    if (baggageConditions.getBaggageApplicationLevel() == BaggageApplicationLevel.ITINERARY) {
      baggageApplicationLevel = BaggageApplicationLevel.ITINERARY;
      return itinerary.getLegend().getSegmentResults().get(0).getSegment();
    } else {

      return itinerary.getLegend()
          .getSegmentResults()
          .get(baggageConditions.getSegmentTypeIndex().getIndex())
          .getSegment();
    }
  }

  private void addBaggageCollectionItem(BaggageConditions baggageConditions, int position,
      boolean allBaggageConditionsIncludedTheSameBaggage) {
    Segment segment = getSegment(baggageConditions);

    List<Integer> sectionIdList = segment.getSections();
    Section departureSection = getSectionById(sectionIdList.get(0));
    Section arrivalSection = getSectionById(sectionIdList.get(sectionIdList.size() - 1));

    if (departureSection != null && arrivalSection != null) {
      String segmentDirection =
          getSegmentDirection(position, baggageConditions.getSegmentTypeIndex(),
              allBaggageConditionsIncludedTheSameBaggage);
      String carrierCode = getCarrierCode(departureSection.getCarrier());
      String departureCityName = getCityIataCode(departureSection.getFrom());
      String arrivalCityName = getCityIataCode(arrivalSection.getTo());

      boolean hasSelectableBaggage =
          baggageConditions.getSelectableBaggageDescriptor().size() > 0;

      if (departureCityName != null && arrivalCityName != null) {
        mView.addBaggageCollectionItem(baggageConditions, segmentDirection, carrierCode,
            departureCityName, arrivalCityName, getPreselectedBaggagePosition(baggageConditions),
            hasSelectableBaggage, isRoundTrip(allBaggageConditionsIncludedTheSameBaggage));
      }
    }
  }

  private String getSegmentDirection(int position, SegmentTypeIndex segmentTypeIndex,
      boolean allBaggageConditionsIncludedTheSameBaggage) {
    switch (travelType) {
      case SIMPLE:
        return mView.getOutBound();
      case ROUND:
        if (isItinerary() || allBaggageConditionsIncludedTheSameBaggage) {
          return mView.getRoundTrip();
        } else if (segmentTypeIndex.equals(SegmentTypeIndex.FIRST)) {
          return mView.getOutBound();
        } else {
          return mView.getInBound();
        }
      case MULTIDESTINATION:
      default:
        if (allBaggageConditionsIncludedTheSameBaggage) {
          return mView.getRoundTrip();
        } else {
          return mView.getLeg(position);
        }
    }
  }

  private boolean isRoundTrip(boolean allBaggageConditionsIncludedTheSameBaggage) {
    switch (travelType) {
      case SIMPLE:
        break;
      case ROUND:
        if (isItinerary() || allBaggageConditionsIncludedTheSameBaggage) {
          return true;
        }
        break;
      case MULTIDESTINATION:
      default:
        if (allBaggageConditionsIncludedTheSameBaggage) {
          return true;
        }
        break;
    }
    return false;
  }

  private String getCarrierCode(int carrier) {
    return itinerary.getLegend().getCarriers().get(carrier).getCode();
  }

  private int getPreselectedBaggagePosition(BaggageConditions baggageConditions) {
    BaggageSelectionResponse baggageSelectionResponse =
        getTravellerBaggageSelectionResponse(baggageConditions);
    if (baggageSelectionResponse == null) {
      return 0;
    }

    for (int i = 0; i < baggageConditions.getSelectableBaggageDescriptor().size(); i++) {
      if (baggageConditions.getSelectableBaggageDescriptor()
          .get(i)
          .getPrice()
          .equals(baggageSelectionResponse.getPrice())) {
        return i;
      }
    }

    return 0;
  }

  private BaggageSelectionResponse getTravellerBaggageSelectionResponse(
      BaggageConditions baggageConditions) {
    if (travellers.size() <= passengerWidgetPosition) {
      return null;
    }
    for (BaggageSelectionResponse baggageSelectionResponse : travellers.get(passengerWidgetPosition)
        .getBaggageSelections()) {
      if (baggageSelectionResponse.getBaggageApplicationLevel()
          .equals(baggageConditions.getBaggageApplicationLevel()) && (isItinerary()
          || baggageSelectionResponse.getSegmentTypeIndex()
          .equals(baggageConditions.getSegmentTypeIndex()))) {
        return baggageSelectionResponse;
      }
    }
    return null;
  }

  boolean checkIfAllBaggageConditionsIncludedTheSameBaggage(
      List<BaggageConditions> baggageConditionsList) {
    boolean allConditionsIncludedTheSameBaggage = true;

    BaggageDescriptor baggageDescriptor;

    if (baggageConditionsList.size() > 0
        && baggageConditionsList.get(0).getSelectableBaggageDescriptor().size() == 0
        && ((baggageConditionsList.get(0).getBaggageDescriptorIncludedInPrice().getKilos() != null
        && baggageConditionsList.get(0).getBaggageDescriptorIncludedInPrice().getKilos() > 0)
        || baggageConditionsList.get(0).getBaggageDescriptorIncludedInPrice().getPieces() > 0)) {

      baggageDescriptor = new BaggageDescriptor(
          baggageConditionsList.get(0).getBaggageDescriptorIncludedInPrice().getPieces(),
          baggageConditionsList.get(0).getBaggageDescriptorIncludedInPrice().getKilos());

      for (int i = 1; i < baggageConditionsList.size(); i++) {
        if (baggageConditionsList.get(i).getSelectableBaggageDescriptor().size() > 0
            || baggageConditionsList.get(i).getBaggageDescriptorIncludedInPrice().getKilos()
            != baggageDescriptor.getKilos()
            || baggageConditionsList.get(i).getBaggageDescriptorIncludedInPrice().getPieces()
            != baggageDescriptor.getPieces()) {
          allConditionsIncludedTheSameBaggage = false;
          mView.showPersuasiveMessage();
        }
      }
    } else {
      allConditionsIncludedTheSameBaggage = false;
      mView.showPersuasiveMessage();
    }

    return allConditionsIncludedTheSameBaggage;
  }

  boolean checkIfAllBaggageConditionsNotIncludeBaggage(
      List<BaggageConditions> baggageConditionsList) {
    boolean allSelectionNotIncludeBaggage = true;

    for (BaggageConditions baggageConditions : baggageConditionsList) {
      if (baggageConditions.getSelectableBaggageDescriptor().size() > 0
          || (baggageConditions.getBaggageDescriptorIncludedInPrice().getKilos() != null
          && baggageConditions.getBaggageDescriptorIncludedInPrice().getKilos() > 0)
          || baggageConditions.getBaggageDescriptorIncludedInPrice().getPieces() > 0) {
        allSelectionNotIncludeBaggage = false;
        mView.showHeaderSubtitleInfo();
      }
    }

    return allSelectionNotIncludeBaggage;
  }

  private Section getSectionById(int sectionId) {
    for (SectionResult section : itinerary.getLegend().getSectionResults()) {
      if (section.getId() == sectionId) {
        return section.getSection();
      }
    }

    return null;
  }

  private String getCityIataCode(int locationId) {
    for (Location location : itinerary.getLegend().getLocations()) {
      if (location.getGeoNodeId() == locationId) {
        return location.getCityIataCode();
      }
    }

    return null;
  }

  private boolean isItinerary() {
    return baggageApplicationLevel != null
        && baggageApplicationLevel == BaggageApplicationLevel.ITINERARY;
  }

  public List<BaggageSelectionRequest> getBaggageSelections(
      List<BaggageCollectionItem> baggageSelectionItemsList) {
    List<BaggageSelectionRequest> baggageSelectionRequestsList = new ArrayList<>();
    for (BaggageCollectionItem baggageSelectionItem : baggageSelectionItemsList) {
      if (isItinerary()) {
        multiplyBaggages(baggageSelectionRequestsList, baggageSelectionItem);
      } else {
        BaggageSelectionRequest baggageSelectionRequest =
            baggageSelectionItem.getBaggageSelectionRequest();
        if (baggageSelectionRequest != null) {
          baggageSelectionRequestsList.add(baggageSelectionRequest);
        }
      }
    }

    return baggageSelectionRequestsList;
  }

  private void multiplyBaggages(List<BaggageSelectionRequest> baggageSelectionRequestsList,
      BaggageCollectionItem baggageSelectionItem) {
    for (int i = 0; i < itinerary.getSegments().size(); i++) {
      BaggageSelectionRequest baggageSelectionRequest =
          baggageSelectionItem.getBaggageSelectionRequest();
      if (baggageSelectionRequest != null) {
        baggageSelectionRequest.setSegmentTypeIndex(SegmentTypeIndex.fromIndex(i));
        baggageSelectionRequestsList.add(baggageSelectionRequest);
      }
    }
  }
}
