package com.odigeo.presenter;

import com.odigeo.data.ab.RemoteConfigControllerInterface;
import com.odigeo.data.localizable.OneCMSAlarmsController;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.session.CredentialsInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.MyTripsInteractor;
import com.odigeo.interactors.UpdateAppVersionCacheInteractor;
import com.odigeo.interactors.VisitsInteractor;
import com.odigeo.presenter.contracts.navigators.SplashNavigatorInterface;
import com.odigeo.presenter.contracts.views.SplashViewInterface;
import com.odigeo.presenter.listeners.VisitInteractorListener;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class SplashPresenter extends BasePresenter<SplashViewInterface, SplashNavigatorInterface> {

  public static final String FULL_TRANSPARENCY_FIRST_TIME = "FULL_TRANSPARENCY_FIRST_TIME";
  public static final String LAST_APP_VERSION = "LAST_APP_VERSION";
  private final VisitsInteractor visitsInteractor;
  private final MyTripsInteractor mMyTripsInteractor;
  private final TrackerControllerInterface mTrackerControllerInterface;
  private PreferencesControllerInterface mPreferencesController;
  private SessionController mCredentialsInterface;
  private RemoteConfigControllerInterface remoteConfigControllerInterface;
  private OneCMSAlarmsController oneCMSAlarmsController;
  private UpdateAppVersionCacheInteractor updateAppVersionCacheInteractor;

  public SplashPresenter(SplashViewInterface view, SplashNavigatorInterface navigator,
      PreferencesControllerInterface preferencesControllerInterface,
      SessionController credentialsInterface, VisitsInteractor visitsInteractor,
      TrackerControllerInterface trackerControllerInterface, MyTripsInteractor myTripsInteractor,
      UpdateAppVersionCacheInteractor updateAppVersionCacheInteractor,
      RemoteConfigControllerInterface remoteConfigControllerInterface,
      OneCMSAlarmsController oneCMSAlarmsController) {
    super(view, navigator);
    this.mPreferencesController = preferencesControllerInterface;
    this.mCredentialsInterface = credentialsInterface;
    this.visitsInteractor = visitsInteractor;
    this.mTrackerControllerInterface = trackerControllerInterface;
    this.mMyTripsInteractor = myTripsInteractor;
    this.updateAppVersionCacheInteractor = updateAppVersionCacheInteractor;
    this.remoteConfigControllerInterface = remoteConfigControllerInterface;
    this.oneCMSAlarmsController = oneCMSAlarmsController;

    mPreferencesController.saveBooleanValue(FULL_TRANSPARENCY_FIRST_TIME, false);
  }

  public void startFirebaseService() {
    mNavigator.startFirebaseRegistrationTokenService();
  }

  public void goToNextActivity() {
    if (shouldGoToWalkthrough()) {
      updateLastAppVersion();
      mNavigator.goToWalkThroughActivity();
    } else {
      mNavigator.goToHomeActivity();
    }
  }

  public void initABManager() {
    visitsInteractor.fetch(new VisitInteractorListener() {
      @Override public void onSuccess() {
        mTrackerControllerInterface.updateDimensionForABTest();
      }
    });
  }

  public void initRemoteConfig() {
    remoteConfigControllerInterface.fetchConfig();
  }

  public void updateLastAppOpening(String numberOfDays) {
    long lastOpeningTimestamp = System.currentTimeMillis();

    if (mPreferencesController.containsValue(PreferencesControllerInterface.LAST_APP_OPENING)) {
      lastOpeningTimestamp =
          mPreferencesController.getLongValue(PreferencesControllerInterface.LAST_APP_OPENING);
    }

    Date dateLastOpening = new Date(lastOpeningTimestamp);
    Date dateToday = new Date();

    long diffInMillies = dateToday.getTime() - dateLastOpening.getTime();
    long diffInDays = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

    long threshold = 3;
    try {
      threshold = Long.valueOf(numberOfDays);
    } catch (NumberFormatException exception) {
      exception.printStackTrace();
    }

    mPreferencesController.saveBooleanValue(PreferencesControllerInterface.SHOULD_ADD_DROPOFF_CARD,
        diffInDays <= threshold);
    mPreferencesController.saveLongValue(PreferencesControllerInterface.LAST_APP_OPENING,
        System.currentTimeMillis());
  }

  private void updateLastAppVersion() {
    updateAppVersionCacheInteractor.updateLastAppVersion();
  }

  private boolean shouldGoToWalkthrough() {
    CredentialsInterface credentials = mCredentialsInterface.getCredentials();

    return remoteConfigControllerInterface.isWalkthroughActive() && !updateAppVersionCacheInteractor
        .isLastVersion() && (!remoteConfigControllerInterface.shouldShowDefaultWalkthrough()
        || credentials == null);
  }

  public void checkActiveBookingsOnLaunch() {
    if (mMyTripsInteractor.hasActiveBookings()) {
      mTrackerControllerInterface.trackActiveBookingsOnLaunch();
    }
  }

  public void startOneCMSTimer() {
    oneCMSAlarmsController.setOneCMSUpdateAlarms();
  }
}
