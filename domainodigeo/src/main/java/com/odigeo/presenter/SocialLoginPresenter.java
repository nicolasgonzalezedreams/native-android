package com.odigeo.presenter;

import com.odigeo.data.entity.userData.User;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.interactors.LoginInteractor;
import com.odigeo.interactors.MyTripsInteractor;
import com.odigeo.interactors.TravellerDetailInteractor;
import com.odigeo.interactors.provider.MyTripsProvider;
import com.odigeo.presenter.contracts.navigators.LoginNavigatorInterface;
import com.odigeo.presenter.contracts.navigators.SocialLoginNavigatorInterface;
import com.odigeo.presenter.contracts.views.LoginSocialInterface;
import com.odigeo.presenter.contracts.views.RegisterViewInterface;
import com.odigeo.tools.CheckerTool;
import java.util.Map;

import static com.odigeo.presenter.contracts.views.LoginSocialInterface.EMAIL;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.FACEBOOK_SOURCE;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.GOOGLE_SOURCE;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.IMAGE_URL;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.NAME;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.TOKEN;

public abstract class SocialLoginPresenter
    extends BasePresenter<LoginSocialInterface, SocialLoginNavigatorInterface> {

  private LoginInteractor mLoginInteractor;
  private MyTripsInteractor mMyTripsInteractor;
  private TravellerDetailInteractor mTravellerDetailInteractor;
  private TrackerControllerInterface mTrackerController;

  public SocialLoginPresenter(LoginSocialInterface view, SocialLoginNavigatorInterface navigator,
      LoginInteractor loginInteractor, MyTripsInteractor myTripsInteractor,
      TravellerDetailInteractor travellerDetailInteractor,
      TrackerControllerInterface trackerControllerInterface) {
    super(view, navigator);
    mLoginInteractor = loginInteractor;
    mMyTripsInteractor = myTripsInteractor;
    mTravellerDetailInteractor = travellerDetailInteractor;
    mTrackerController = trackerControllerInterface;
  }

  public void login(Map<String, String> loginData, UserProfile userProfile, final String source) {
    mLoginInteractor.login(loginData.get(EMAIL), loginData.get(TOKEN), loginData.get(IMAGE_URL),
        loginData.get(NAME), buildRequestDataListener(source, loginData.get(EMAIL)), userProfile,
        source);
  }

  private OnRequestDataListener<User> buildRequestDataListener(final String source,
      final String email) {
    return new OnRequestDataListener<User>() {
      @Override public void onResponse(User user) {
        mView.loginSuccess(source, user.getEmail());
      }

      @Override public void onError(MslError error, String message) {
        mTrackerController.trackApiLoginError();
        if (error == MslError.TO_001) {
          mNavigator.onTimeOutError();
        }
        handleLoginError(error, source, email);
      }
    };
  }

  protected abstract void handleLoginError(MslError error, String source, String email);

  public boolean hasLocalData() {
    final boolean[] hasLocalData = { false };
    mMyTripsInteractor.getBookings(new OnRequestDataListener<MyTripsProvider>() {
      @Override public void onResponse(MyTripsProvider object) {
        hasLocalData[0] =
            !object.getBookingList().isEmpty() || !mTravellerDetailInteractor.getTravellersList()
                .isEmpty();
      }

      @Override public void onError(MslError error, String message) {

      }
    });

    return hasLocalData[0];
  }

  public void onLoginSuccessDialogClosed() {
    if (isComingFromPassenger()) {
      ((LoginNavigatorInterface) mNavigator).returnToPassengerWithLoginSuccess();
    } else if (isComingFromMyTrips()) {
      ((LoginNavigatorInterface) mNavigator).returnToMyTripsWithLoginSuccess();
    } else {
      mNavigator.navigateToHome();
    }
  }

  private boolean isComingFromMyTrips() {
    return mNavigator instanceof LoginNavigatorInterface
        && ((LoginNavigatorInterface) mNavigator).isComingFromMyTrips();
  }

  public boolean isComingFromPassenger() {
    return mNavigator instanceof LoginNavigatorInterface
        && ((LoginNavigatorInterface) mNavigator).isComingFromPassenger();
  }

  public String getSSOTrackingCategory() {
    return mNavigator.getSSOTrackingCategory();
  }

  public final Boolean validateUsernameFormat(String email) {
    return CheckerTool.checkEmail(email);
  }

  public void buildSocialLoginErrorMessage(MslError error, String source) {
    mView.hideProgress();
    switch (source) {
      case FACEBOOK_SOURCE:
        if (error == MslError.AUTH_005) {
          trackAuthenticationError(source);
          mView.showFacebookAuthError();
        } else if (error == MslError.FB_001) {
          mView.showFacebookPermissionError();
        } else {
          mView.showUnknownLoginError();
        }
        break;

      case GOOGLE_SOURCE:
        if (error == MslError.AUTH_006) {
          trackAuthenticationError(source);
          mView.showGoogleAuthError();
        } else {
          mView.showUnknownLoginError();
        }
        break;
    }
  }

  public void onAuthOkClicked() {
    mNavigator.navigateToLogin();
  }

  private void trackAuthenticationError(String source) {
    switch (source) {
      case FACEBOOK_SOURCE:
        if (mView instanceof RegisterViewInterface) {
          mTrackerController.trackFacebookAuthenticationErrorRegister();
        } else {
          mTrackerController.trackFacebookAuthenticationErrorLogin();
        }
        break;
      case GOOGLE_SOURCE:
        if (mView instanceof RegisterViewInterface) {
          mTrackerController.trackGoogleAuthenticationErrorRegister();
        } else {
          mTrackerController.trackGoogleAuthenticationErrorLogin();
        }
        break;
    }
  }
}
