package com.odigeo.presenter;

import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.interactors.TravellerDetailInteractor;
import com.odigeo.presenter.contracts.navigators.TravellersListNavigatorInterface;
import com.odigeo.presenter.contracts.views.TravellersListViewInterface;
import java.util.List;

/**
 * Created by carlos.navarrete on 28/08/15.
 */
public class TravellersListPresenter
    extends BasePresenter<TravellersListViewInterface, TravellersListNavigatorInterface> {

  private TravellerDetailInteractor mTravellersInteractor;

  public TravellersListPresenter(TravellersListViewInterface view,
      TravellersListNavigatorInterface navigator, TravellerDetailInteractor interactor) {
    super(view, navigator);
    this.mTravellersInteractor = interactor;
  }

  /**
   * Retrieves a list of UserTraveller, if it is distinct from empty it will set 2 null values, one
   * at the begining of the list other at
   * the end to set the Header or the Footer.
   *
   * @return List<UserTraveller>
   */
  public List<UserTraveller> getTravellersFromDB() {
    List<UserTraveller> userTravellers = mTravellersInteractor.getTravellersList();
    UserTraveller userTraveller = null;
    for (int i = 0; i < userTravellers.size(); i++) {
      if (userTravellers.size() > 1) {
        if (userTravellers.get(i).getUserProfile() != null) {
          if (userTravellers.get(i).getUserProfile().isDefaultTraveller()) {
            userTraveller = userTravellers.get(i);
            userTravellers.remove(i);
            break;
          }
        }
      }
    }
    if (userTravellers.size() > 0 && userTraveller != null) {
      userTravellers.add(0, userTraveller);
    }

    if (!userTravellers.isEmpty()) {
      //This value is to set the header when it will be shown on the screen
      userTravellers.add(0, null);
      //This value is to set the footer when it will be shown on the screen
      userTravellers.add(null);
    }
    return userTravellers;
  }
}
