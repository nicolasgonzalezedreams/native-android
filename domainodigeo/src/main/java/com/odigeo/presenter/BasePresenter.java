package com.odigeo.presenter;

import com.odigeo.presenter.contracts.navigators.BaseNavigatorInterface;
import com.odigeo.presenter.contracts.views.BaseViewInterface;

public abstract class BasePresenter<V extends BaseViewInterface, N extends BaseNavigatorInterface> {

  protected V mView;
  protected N mNavigator;

  public BasePresenter(V view, N navigator) {
    this.mView = view;
    this.mNavigator = navigator;
  }

  public N getNavigatorInterface() {
    return mNavigator;
  }
}
