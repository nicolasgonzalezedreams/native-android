package com.odigeo.presenter;

import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.ChangePasswordInteractor;
import com.odigeo.interactors.LogoutInteractor;
import com.odigeo.presenter.contracts.navigators.AccountPreferencesNavigatorInterface;
import com.odigeo.presenter.contracts.views.ChangePasswordViewInterface;
import com.odigeo.presenter.listeners.OnChangePasswordListener;
import com.odigeo.tools.CheckerTool;

public class ChangePasswordPresenter
    extends BasePresenter<ChangePasswordViewInterface, AccountPreferencesNavigatorInterface> {

  private ChangePasswordInteractor mChangePasswordInteractor;
  private LogoutInteractor mLogoutInteractor;
  private SessionController mSessionController;

  public ChangePasswordPresenter(ChangePasswordViewInterface view,
      AccountPreferencesNavigatorInterface navigator,
      ChangePasswordInteractor changePasswordInteractor, LogoutInteractor logoutInteractor,
      SessionController sessionController) {
    super(view, navigator);
    this.mChangePasswordInteractor = changePasswordInteractor;
    mLogoutInteractor = logoutInteractor;
    mSessionController = sessionController;
  }

  public void changePasswordAction(String currentPassword, String newPassword,
      String repeatNewPassword) {
    mView.showChangingPasswordDialog();
    mChangePasswordInteractor.changePassword(currentPassword, newPassword, repeatNewPassword,
        createOnChangePasswordListener());
  }

  private OnChangePasswordListener createOnChangePasswordListener() {
    return new OnChangePasswordListener() {
      @Override public void onSuccess() {
        mView.passwordChanged(true);
        mNavigator.showAccountPreferencesView();
      }

      @Override public void onError() {
        mView.passwordChanged(false);
      }

      @Override public void onCurrentPasswordWrong() {
        mView.invalidConcurrentPassword();
        mView.passwordChanged(false);
      }

      @Override public void onPasswordDoesNotMatch() {
        mView.newPasswordMatch(false);
        mView.passwordChanged(false);
      }

      @Override public void onCurrentPasswordInvalidFormat() {
        mView.invalidFormatCurrentPassword();
        mView.passwordChanged(false);
      }

      @Override public void onNewPasswordInvalidFormat() {
        mView.invalidFormatNewPassword();
        mView.passwordChanged(false);
      }

      @Override public void onRepeatPasswordInvalidFormat() {
        mView.invalidFormatRepeatPassword();
        mView.passwordChanged(false);
      }

      @Override public void onAuthError() {
        mLogoutInteractor.logout(new OnRequestDataListener<Boolean>() {
          @Override public void onResponse(Boolean object) {
            mView.showAuthError();
            mSessionController.saveLogoutWasMade(true);
          }

          @Override public void onError(MslError error, String message) {
            mView.onLogoutError(error, message);
          }
        });
      }
    };
  }

  public void onAuthOkClicked() {
    mNavigator.navigateToLogin();
  }

  public boolean validatePasswords(String currentPassword, String newPassword,
      String repeatPassword) {
    if (currentPassword.equals(newPassword)) {
      if (!newPassword.isEmpty()) {
        mView.newPasswordIsTheSame(true);
      }
      return false;
    } else {
      mView.newPasswordIsTheSame(false);
      if (newPassword.isEmpty()) {
        return false;
      } else {
        if (newPassword.length() == repeatPassword.length()) {
          if (newPassword.equals(repeatPassword)) {
            if (!CheckerTool.checkPassword(currentPassword)
                && !mChangePasswordInteractor.isSocialNetworkAccount()) {
              mView.invalidFormatCurrentPassword();
              return false;
            } else if (!CheckerTool.checkPassword(newPassword)) {
              mView.invalidFormatNewPassword();
              return false;
            } else if (!CheckerTool.checkPassword(repeatPassword)) {
              mView.invalidFormatRepeatPassword(); // This will never happen as new and repeat passwords should be the same and format check for new password is already done before.
              return false;
            } else {
              return true;
            }
          } else {
            mView.newPasswordMatch(false);
            return false;
          }
        } else {
          return false;
        }
      }
    }
  }

  public void checkAccountType() {
    mView.changeCurrentPasswordVisibility(!mChangePasswordInteractor.isSocialNetworkAccount());
  }
}
