package com.odigeo.presenter;

import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.CheckUserCredentialsInteractor;
import com.odigeo.interactors.LogoutInteractor;
import com.odigeo.interactors.MyTripsInteractor;
import com.odigeo.interactors.TravellerDetailInteractor;
import com.odigeo.interactors.provider.MyTripsProvider;
import com.odigeo.presenter.contracts.navigators.NavigationDrawerNavigatorInterface;
import com.odigeo.presenter.contracts.views.NavigationDrawerViewInterface;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class NavigationDrawerPresenter
    extends BasePresenter<NavigationDrawerViewInterface, NavigationDrawerNavigatorInterface> {

  private static final int DRAWER_CLOSE_DELAY_MS = 250;

  private CheckUserCredentialsInteractor mCheckUserCredentialsInteractor;
  private MyTripsInteractor mMyTripsInteractor;
  private TravellerDetailInteractor mTravellersInteractor;
  private ScheduledExecutorService mWorker;
  private LogoutInteractor logoutInteractor;
  private SessionController mSessionController;
  private TrackerControllerInterface mTrackController;

  public NavigationDrawerPresenter(NavigationDrawerViewInterface view,
      NavigationDrawerNavigatorInterface navigator,
      CheckUserCredentialsInteractor checkUserCredentialsInteractor,
      TravellerDetailInteractor travellersInteractor, MyTripsInteractor myTripsInteractor,
      LogoutInteractor logoutInteractor, SessionController sessionController,
      TrackerControllerInterface trackerControllerInterface) {
    super(view, navigator);
    mCheckUserCredentialsInteractor = checkUserCredentialsInteractor;
    this.mTravellersInteractor = travellersInteractor;
    this.mMyTripsInteractor = myTripsInteractor;
    this.logoutInteractor = logoutInteractor;
    this.mWorker = Executors.newSingleThreadScheduledExecutor();
    mSessionController = sessionController;
    mTrackController = trackerControllerInterface;
  }

  public void checkUserSession() {
    if (mCheckUserCredentialsInteractor.isUserLogin()) {
      mView.refreshNavigationDrawerUserLogged();
    } else {
      checkSessionStatus();
    }
  }

  public void checkSessionStatus() {
    if (mCheckUserCredentialsInteractor.isUserInActive()) {
      mView.refreshNavigationDrawerUserInactive(mCheckUserCredentialsInteractor.getInactiveEmail());
    } else {
      mView.refreshNavigationDrawerUserUnLogged();
    }
  }

  public void goToJoinUs() {
    closeAndOpenSelection(NavigationDrawerItem.JOIN_US);
  }

  public void goToTravellers() {
    closeAndOpenSelection(NavigationDrawerItem.TRAVELLERS);
  }

  public void goToTrips() {
    closeAndOpenSelection(NavigationDrawerItem.TRIPS);
  }

  public void goToSettings() {
    closeAndOpenSelection(NavigationDrawerItem.SETTINGS);
  }

  public void goToAbout() {
    closeAndOpenSelection(NavigationDrawerItem.ABOUT);
  }

  public void goToAccountPreferences() {
    closeAndOpenSelection(NavigationDrawerItem.PREFERENCES);
  }

  public void logOut() {
    logoutInteractor.logout(new OnRequestDataListener<Boolean>() {
      @Override public void onResponse(Boolean response) {
        if (response) {
          mView.refreshNavigationDrawerUserUnLogged();
          mView.updateCarousel();
        }
      }

      @Override public void onError(MslError error, String message) {
        // nothing
      }
    });
  }

  public void changeMenu() {
    if (mCheckUserCredentialsInteractor.isUserLogin()) {
      mView.refreshMenu();
    }
  }

  public String getUserEmailFromCredentials() {
    return mCheckUserCredentialsInteractor.getUserEmail();
  }

  public String getUserName() {
    return mCheckUserCredentialsInteractor.getUserName();
  }

  public String getUrlUserImageProfile() {
    if (!mCheckUserCredentialsInteractor.isUserLoggedByUserPassword()) {
      return mCheckUserCredentialsInteractor.getUrlUserImageProfile();
    }
    return null;
  }

  public boolean hasLocalData() {
    final boolean[] hasLocalData = { false };
    mMyTripsInteractor.getBookings(new OnRequestDataListener<MyTripsProvider>() {
      @Override public void onResponse(MyTripsProvider object) {
        hasLocalData[0] =
            !object.getBookingList().isEmpty() || !mTravellersInteractor.getTravellersList()
                .isEmpty();
      }

      @Override public void onError(MslError error, String message) {

      }
    });

    return hasLocalData[0];
  }

  public boolean isUserLoggedIn() {
    return mCheckUserCredentialsInteractor.isUserLogin();
  }

  public boolean isUserInactive() {
    return mCheckUserCredentialsInteractor.isUserInActive();
  }

  public void refreshUserNameTextView(String name) {
    if (name == null || name.length() == 0) {
      mView.hideUserName();
    } else {
      mView.showUserName(name);
    }
  }

  private void closeAndOpenSelection(final NavigationDrawerItem selection) {
    mView.closeDrawer();

    //Wait to close drawer
    Runnable task = new Runnable() {
      public void run() {
        switch (selection) {
          case JOIN_US:
            mNavigator.navigateToJoinUs();
            break;
          case TRAVELLERS:
            mNavigator.navigateToTravellers();
            break;
          case TRIPS:
            mNavigator.navigateToMyTrips();
            break;
          case SETTINGS:
            mNavigator.navigateToSettings();
            break;
          case ABOUT:
            mNavigator.navigateToAbout();
            break;
          case PREFERENCES:
            mNavigator.navigateToAccountPreferences();
            break;
        }
      }
    };

    mWorker.schedule(task, DRAWER_CLOSE_DELAY_MS, TimeUnit.MILLISECONDS);
  }

  public void checkLogoutWasMade() {
    if (mSessionController.getLogoutWasMade()) {
      mView.refreshNavigationDrawerUserUnLogged();
      mView.updateCarousel();
      mSessionController.saveLogoutWasMade(false);
    }
  }

  public void trackOpenMenu() {
    mTrackController.trackOpenMenu();
  }

  private enum NavigationDrawerItem {
    JOIN_US, TRAVELLERS, TRIPS, SETTINGS, ABOUT, PREFERENCES
  }
}
