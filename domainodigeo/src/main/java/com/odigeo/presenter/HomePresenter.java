package com.odigeo.presenter;

import com.odigeo.Notifier.EventsNotifier;
import com.odigeo.Notifier.event.Event;
import com.odigeo.Notifier.subscription.Subscription;
import com.odigeo.Notifier.subscription.SubscriptionListener;
import com.odigeo.data.entity.carrousel.CampaignCard;
import com.odigeo.data.entity.carrousel.Carousel;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.helper.CrossCarUrlHandlerInterface;
import com.odigeo.interactors.CheckUserCredentialsInteractor;
import com.odigeo.interactors.LogoutInteractor;
import com.odigeo.interactors.UpdateCitiesInteractor;
import com.odigeo.interactors.provider.CarouselProvider;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.presenter.contracts.navigators.NavigationDrawerNavigatorInterface;
import com.odigeo.presenter.contracts.views.HomeViewInterface;
import com.odigeo.presenter.listeners.OnLoadCarrouselListener;

public class HomePresenter
    extends BasePresenter<HomeViewInterface, NavigationDrawerNavigatorInterface>
    implements SubscriptionListener, OnLoadCarrouselListener {

  private final EventsNotifier mEventsNotifier;
  private final CarouselProvider mCarouselProvider;
  private final Subscription mSubscription;
  private final SessionController mSessionController;
  private final LogoutInteractor mLogoutInteractor;
  private final CheckUserCredentialsInteractor mCheckUserCredentialsInteractor;
  private final CrossCarUrlHandlerInterface mCrossCarUrlHandler;
  private final PreferencesControllerInterface mPreferencesControllerInterface;
  private final MarketProviderInterface mMarketProvider;
  private UpdateCitiesInteractor updateCitiesInteractor;

  private OnRequestDataListener<Boolean> logoutListener = new OnRequestDataListener<Boolean>() {
    @Override public void onResponse(Boolean object) {
      mView.showAuthErrorMessage();
      if (mSessionController.getCredentials() == null) {
        mNavigator.onAuthError(null);
      } else {
        String credentials = mSessionController.getCredentials().getCredentialTypeValue();
        mNavigator.onAuthError(credentials);
      }
    }

    @Override public void onError(MslError error, String message) {

    }
  };

  public HomePresenter(HomeViewInterface view, NavigationDrawerNavigatorInterface navigator,
      EventsNotifier eventsNotifier, CarouselProvider carouselProvider,
      Subscription subscriptionEvent, SessionController sessionController,
      LogoutInteractor logoutInteractor,
      CheckUserCredentialsInteractor checkUserCredentialsInteractor,
      CrossCarUrlHandlerInterface crossCarUrlHandlerInterface,
      PreferencesControllerInterface preferencesControllerInterface,
      MarketProviderInterface marketProvider, UpdateCitiesInteractor updateCitiesInteractor) {
    super(view, navigator);
    mEventsNotifier = eventsNotifier;
    mCarouselProvider = carouselProvider;
    mSubscription = subscriptionEvent;
    mSessionController = sessionController;
    mLogoutInteractor = logoutInteractor;
    mCheckUserCredentialsInteractor = checkUserCredentialsInteractor;
    mCrossCarUrlHandler = crossCarUrlHandlerInterface;
    mPreferencesControllerInterface = preferencesControllerInterface;
    mMarketProvider = marketProvider;
    this.updateCitiesInteractor = updateCitiesInteractor;

    mCarouselProvider.setOnLoadCarouselListener(this);
  }

  public void subscribeToUpdateBooking() {
    mSubscription.setSubscriptionListener(this);
    mEventsNotifier.subscribe(mSubscription);
  }

  public void unSubscribeToUpdateBooking() {
    mEventsNotifier.unSubscribe(mSubscription);
  }

  public boolean isUpdateBookingEventActive() {
    return mEventsNotifier.isEventActive(mSubscription);
  }

  public void showSearchView() {
    mNavigator.navigateToSearch();
  }

  public void showHotelsView(String url) {
    mNavigator.navigateToHotels(url);
  }

  public void showCarsView(String url) {
    String realUrl =
        url.isEmpty() ? mCrossCarUrlHandler.getCarUrl(CrossCarUrlHandlerInterface.CAMPAIGN_HEADER)
            : url;
    mNavigator.navigateToCars(realUrl, false);
  }

  public void openLastMinute() {
    mNavigator.navigateToLastMinute();
  }

  public void openPacks() {
    mNavigator.navigateToPacks();
  }

  @Override public void onEventReceived(Event event) {
    updateBookingsInCarousel();
  }

  private void updateBookingsInCarousel() {
    mCarouselProvider.updateBookingCards();
  }

  public void getCachedCarousel() {
    mCarouselProvider.getCarousel(mNavigator.hasChangedMarket());
  }

  public void setCampaignBackgroundImage() {
    mView.setCampaignBackgroundImage();
  }

  public void setCityBackgroundImage(String imageUrl) {
    mView.setCityBackgroundImage(imageUrl);
  }

  public void translateHomeBackgroundPosition(float position) {
    mView.translateBackgroundPosition(position);
  }

  public void synchronizeData() {
    mView.synchronizeData(new OnAuthRequestDataListener<Void>() {
      @Override public void onAuthError() {
        mLogoutInteractor.logout(logoutListener);
      }

      @Override public void onResponse(Void object) {

      }

      @Override public void onError(MslError error, String message) {

      }
    });
  }

  public void onAuthOkClicked() {
    mNavigator.navigateToLogin();
  }

  public boolean userIsLoggedIn() {
    return mCheckUserCredentialsInteractor.isUserLogin();
  }

  @Override public void onCarouselLoaded(Carousel carousel) {
    if (mView.isActive()) {
      mView.showCards(carousel);
    }
  }

  @Override public void onErrorLoadingCarousel() {
  }

  public long getLastCarouselUpdate() {
    return mPreferencesControllerInterface.getLongValue(
        PreferencesControllerInterface.LAST_UPDATE_BOOKING_STATUS);
  }

  public void clearLocalyticsKeys() {
    mPreferencesControllerInterface.deleteValue(CampaignCard.LOCALYTICS_CAMPAIGN_CARD_KEY);
  }

  public boolean cameFromLocalyticsPush() {
    return mPreferencesControllerInterface.containsValue(CampaignCard.LOCALYTICS_CAMPAIGN_CARD_KEY);
  }

  public String getLocalyticsPushContent() {
    return mPreferencesControllerInterface.getStringValue(
        CampaignCard.LOCALYTICS_CAMPAIGN_CARD_KEY);
  }

  public void updateCities() {
    updateCitiesInteractor.updateCities(new OnRequestDataListener<Void>() {
      @Override public void onResponse(Void object) {
        getCachedCarousel();
      }

      @Override public void onError(MslError error, String message) {
        getCachedCarousel();
      }
    });
  }
}
