package com.odigeo.presenter;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.interactors.ArrivalGuidesCardsInteractor;
import com.odigeo.interactors.CheckUpcomingBookingInteractor;
import com.odigeo.interactors.CheckUserCredentialsInteractor;
import com.odigeo.interactors.UpdateBookingStatusInteractor;
import com.odigeo.presenter.contracts.navigators.MyTripDetailsNavigatorInterface;
import com.odigeo.presenter.contracts.views.MyTripDetailsViewInterface;
import org.jetbrains.annotations.Nullable;

public class MyTripDetailsPresenter
    extends BasePresenter<MyTripDetailsViewInterface, MyTripDetailsNavigatorInterface> {

  private final TrackerControllerInterface trackerController;
  private final ArrivalGuidesCardsInteractor arrivalGuidesInteractor;
  private final CheckUpcomingBookingInteractor checkUpcomingBookingInteractor;
  private final UpdateBookingStatusInteractor updatBookingStatusInteractor;
  private final CheckUserCredentialsInteractor checkUserCredentialsInteractor;

  public MyTripDetailsPresenter(MyTripDetailsViewInterface view,
      MyTripDetailsNavigatorInterface navigator,
      CheckUpcomingBookingInteractor checkUpcomingBookingInteractor,
      ArrivalGuidesCardsInteractor arrivalGuidesCardsInteractor,
      UpdateBookingStatusInteractor updateBookingStatusInteractor,
      CheckUserCredentialsInteractor checkUserCredentialsInteractor,
      TrackerControllerInterface trackerController) {
    super(view, navigator);
    this.checkUpcomingBookingInteractor = checkUpcomingBookingInteractor;
    this.arrivalGuidesInteractor = arrivalGuidesCardsInteractor;
    this.updatBookingStatusInteractor = updateBookingStatusInteractor;
    this.checkUserCredentialsInteractor = checkUserCredentialsInteractor;
    this.trackerController = trackerController;
  }

  public boolean onCheckHasArrivalGuide(Booking booking) {
    return !booking.isMultiSegment() && arrivalGuidesInteractor.hasArrivalGuide(booking.getArrivalGeoNodeId());
  }

  public void onAddToCalendar(Booking booking) {
    trackerController.trackAddToCalendar();
    mNavigator.addToCalendar(booking);
  }

  public boolean onCheckUpcomingBooking(Booking booking) {
    return checkUpcomingBookingInteractor.isUpcomingBooking(booking);
  }

  public boolean hasToShowOnlineCheckin(Booking booking, String onlineCheckinUrl,
      String notFoundText) {
        /*
        Feature momentary disabled

        boolean isMarketAvailable = onlineCheckinUrl != null && !onlineCheckinUrl.isEmpty() && !onlineCheckinUrl.contains(notFoundText);
        long currentDate = Calendar.getInstance().getTimeInMillis();
        boolean isPastTrip = booking.getArrivalLastLeg() < currentDate;
        boolean isConfirmed = booking.getBookingStatus().equals(Booking.BOOKING_STATUS_CONTRACT);
        boolean isOneWay =  booking.getTripType().equals(Booking.TRIP_TYPE_ONE_WAY);
        boolean hasOneSection = booking.getSegments().size() == 1 && booking.getFirstSegment().getSectionsList().size() == 1;

        return isMarketAvailable && !isPastTrip  && isConfirmed && isOneWay && hasOneSection;
         */
    return false;
  }

  public void finishDetailsView() {
    mNavigator.exitFromNavigator();
  }

  public void onRefreshBookingStatus(final Booking booking) {
    updatBookingStatusInteractor.updateBookingStatus(booking,
        new UpdateBookingStatusInteractor.BookingStatusListener() {
          @Override public void onComplete(@Nullable Booking updatedBooking) {
            if (mView.isActive()) {
              if (updatedBooking != null) {
                mView.updateBookingStatus(updatedBooking);
              }
              mView.hideRefreshing();
            }
          }
        });
  }

  public void onCheckIfUserIsLoggedIn(Booking booking) {
    if (booking.isPastBooking()) {
      mView.disableRefreshBooking();
      return;
    }
    if (checkUserCredentialsInteractor.isUserLogin()) {
      mView.setupRefreshBooking();
    } else {
      mView.disableRefreshBooking();
    }
  }
}
