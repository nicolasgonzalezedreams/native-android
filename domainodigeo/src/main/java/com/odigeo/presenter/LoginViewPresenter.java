package com.odigeo.presenter;

import com.odigeo.data.net.error.MslError;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.interactors.LoginInteractor;
import com.odigeo.interactors.MyTripsInteractor;
import com.odigeo.interactors.TravellerDetailInteractor;
import com.odigeo.presenter.contracts.navigators.LoginNavigatorInterface;
import com.odigeo.presenter.contracts.views.LoginSocialInterface;
import com.odigeo.presenter.contracts.views.LoginViewInterface;
import com.odigeo.tools.CheckerTool;
import java.util.HashMap;
import java.util.Map;

import static com.odigeo.presenter.contracts.views.LoginSocialInterface.EMAIL;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.PASSWORD_SOURCE;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.TOKEN;

public class LoginViewPresenter extends SocialLoginPresenter {

  public LoginViewPresenter(LoginSocialInterface view, LoginNavigatorInterface navigator,
      LoginInteractor loginInteractor, TravellerDetailInteractor travellerDetailInteractor,
      MyTripsInteractor myTripsInteractor, TrackerControllerInterface trackerControllerInterface) {
    super(view, navigator, loginInteractor, myTripsInteractor, travellerDetailInteractor,
        trackerControllerInterface);
  }

  public void loginWithPassword(String name, String password) {
    Map<String, String> loginData = new HashMap<>();
    loginData.put(EMAIL, name);
    loginData.put(TOKEN, password);
    login(loginData, null, PASSWORD_SOURCE);
  }

  public void requestForgottenPassword(String email) {
    ((LoginNavigatorInterface) mNavigator).navigateToRequestForgottenPassword(email);
  }

  public void goToEmailPendingScreen() {
    ((LoginNavigatorInterface) mNavigator).navigateToPendingEmailScreen();
  }

  public final void validateUsernameAndPasswordFormat(String username, String password) {
    boolean isValid =
        CheckerTool.checkEmail(username) && CheckerTool.checkPasswordOnLogin(password);
    ((LoginViewInterface) mView).enableLoginButton(isValid);
  }

  @Override protected void handleLoginError(MslError error, String source, String email) {
    mView.hideProgress();
    if (!PASSWORD_SOURCE.equals(source)) {
      buildSocialLoginErrorMessage(error, source);
    } else {
      if (error == MslError.STA_000 || error == MslError.STA_009) {
        ((LoginNavigatorInterface) mNavigator).showBlockedAccountError(email);
      } else if (error == MslError.STA_002) {
        ((LoginNavigatorInterface) mNavigator).showLegacyUserError(email);
      } else if (error == MslError.STA_003) {
        ((LoginViewInterface) mView).showPendingEmailDialog(email);
      } else if (error == MslError.STA_006) {
        mNavigator.showUserRegisteredFacebook(email);
      } else if (error == MslError.STA_007) {
        mNavigator.showUserRegisteredGoogle(email);
      } else if (error == MslError.FND_000) {
        ((LoginViewInterface) mView).setUsernameError();
      } else if (error == MslError.FND_001) {
        ((LoginViewInterface) mView).setUserDoesNotExistError();
      } else if (error == MslError.AUTH_000) {
        ((LoginViewInterface) mView).setCredentialsError();
      } else if (error == MslError.VAL_002 || error == MslError.VAL_003) {
        ((LoginViewInterface) mView).setPasswordDoesNotValidate();
      } else {
        ((LoginViewInterface) mView).setServerError();
      }
    }
  }
}
