package com.odigeo.presenter;

import com.odigeo.presenter.contracts.navigators.AboutUsNavigatorInterface;
import com.odigeo.presenter.contracts.views.AboutUsViewInterface;

public class AboutUsPresenter
    extends BasePresenter<AboutUsViewInterface, AboutUsNavigatorInterface> {
  public AboutUsPresenter(AboutUsViewInterface view, AboutUsNavigatorInterface navigator) {
    super(view, navigator);
  }

  public void openAboutFaq() {
    mNavigator.openAboutFaq();
  }

  public void openAboutContactUs() {
    mNavigator.openAboutContactUs();
  }

  public void openTermsAndConditions() {
    mNavigator.openTermsAndConditions();
  }

  public void openAppRate() {
    mNavigator.openAppRate();
  }

  public void openHelpCenter() {
    mNavigator.openHelpCenter();
  }

  public void setActionBarTitle(String title) {
    mNavigator.setNavigationTitle(title);
  }
}
