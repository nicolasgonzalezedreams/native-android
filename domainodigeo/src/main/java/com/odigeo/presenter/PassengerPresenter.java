package com.odigeo.presenter;

import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.parser.UpdateUserTravellerWithTravellerRequestAndBuyerRequest;
import com.odigeo.data.entity.shoppingCart.AvailableProductsResponse;
import com.odigeo.data.entity.shoppingCart.BaggageSelectionResponse;
import com.odigeo.data.entity.shoppingCart.BuyerRequiredFields;
import com.odigeo.data.entity.shoppingCart.Carrier;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.ItinerarySortCriteria;
import com.odigeo.data.entity.shoppingCart.PassengerConflictDetails;
import com.odigeo.data.entity.shoppingCart.PricingBreakdown;
import com.odigeo.data.entity.shoppingCart.PricingBreakdownItemType;
import com.odigeo.data.entity.shoppingCart.RequiredField;
import com.odigeo.data.entity.shoppingCart.ShoppingCart;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.data.entity.shoppingCart.TravellerRequiredFields;
import com.odigeo.data.entity.shoppingCart.TravellerType;
import com.odigeo.data.entity.shoppingCart.request.BuyerRequest;
import com.odigeo.data.entity.shoppingCart.request.PersonalInfoRequest;
import com.odigeo.data.entity.shoppingCart.request.TravellerRequest;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.data.entity.userData.UserTravellerFactory;
import com.odigeo.data.net.error.MslError;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.helper.CreateBuyerRequestHelper;
import com.odigeo.helper.MixBuyerAndTravellerHelper;
import com.odigeo.interactors.AddPassengerToShoppingCartInteractor;
import com.odigeo.interactors.CheckUserBirthdayInteractor;
import com.odigeo.interactors.CheckUserCredentialsInteractor;
import com.odigeo.interactors.CountriesInteractor;
import com.odigeo.interactors.FindBuyerInLocalPassengersInteractor;
import com.odigeo.interactors.GetTravellersByTypeInteractor;
import com.odigeo.interactors.MembershipInteractor;
import com.odigeo.interactors.SaveTravellersInteractor;
import com.odigeo.interactors.TotalPriceCalculatorInteractor;
import com.odigeo.interactors.VisitUserInteractor;
import com.odigeo.presenter.contracts.navigators.PassengerNavigatorInterface;
import com.odigeo.presenter.contracts.views.PassengerViewInterface;
import com.odigeo.presenter.listeners.OnAddPassengersToShoppingCartListener;
import com.odigeo.presenter.listeners.OnUserTravellerRequestListener;
import com.odigeo.tools.DateHelperInterface;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class PassengerPresenter
    extends BasePresenter<PassengerViewInterface, PassengerNavigatorInterface> {

  private static final String INVALID_BOOKING_ID = "ERR-003";
  private static final String BROKEN_FLOW = "INT-003";
  private static final String INCORRECT_CPF = "Incorrect CPF";

  private static final int GENERAL_ERROR = 1000;
  private static final int EMPTY_BUYER_ID = 0;
  private static final int DAY = 0;
  private static final int MONTH = 1;
  private static final int YEAR = 2;
  private static final int LIST_ONLY_WITH_EMPTY_CONTACT = 1;

  private final UpdateUserTravellerWithTravellerRequestAndBuyerRequest
      updateUserTravellerWithTravellerRequestAndBuyerRequest;
  private final MixBuyerAndTravellerHelper mixBuyerAndTravellerHelper;
  private CheckUserCredentialsInteractor checkUserCredentialsInteractor;
  private BuyerRequiredFields buyerRequiredFields;
  private List<TravellerRequiredFields> requiredTravellerInformation;
  private GetTravellersByTypeInteractor getTravellersByTypeInteractor;
  private AddPassengerToShoppingCartInteractor addPassengerToShoppingCartInteractor;
  private CountriesInteractor countriesInteractor;
  private CheckUserBirthdayInteractor checkUserBirthdayInteractor;
  private FindBuyerInLocalPassengersInteractor findBuyerInLocalPassengersInteractor;
  private Map<UserTraveller.TypeOfTraveller, List<UserTraveller>> localTravellersGroupByType =
      new HashMap<>();
  private DateHelperInterface dateHelperInterface;
  private List<UserTraveller> buyerSpinner;
  private UserTraveller selectedBuyer;
  private SaveTravellersInteractor saveTravellersInteractor;
  private int selectedBuyerPosition = 0;
  private String languageIsoCode;
  private String nameEmptyContact;
  private boolean showPersuasive;
  private VisitUserInteractor visitUserInteractor;
  private MembershipInteractor membershipInteractor;
  private TotalPriceCalculatorInteractor totalPriceCalculatorInteractor;
  private BigDecimal collectionMethodWithPriceValue;
  private SessionController sessionController;

  public PassengerPresenter(PassengerViewInterface view, PassengerNavigatorInterface navigator,
      AddPassengerToShoppingCartInteractor addPassengerToShoppingCartInteractor,
      CheckUserCredentialsInteractor checkUserCredentialsInteractor,
      GetTravellersByTypeInteractor getTravellersByTypeInteractor,
      CheckUserBirthdayInteractor checkUserBirthdateInteractor,
      DateHelperInterface dateHelperInterface,
      FindBuyerInLocalPassengersInteractor findBuyerInLocalPassengersInteractor,
      CountriesInteractor countriesInteractor, SaveTravellersInteractor saveTravellersInteractor,
      UpdateUserTravellerWithTravellerRequestAndBuyerRequest updateUserTravellerWithTravellerRequestAndBuyerRequest,
      MixBuyerAndTravellerHelper mixBuyerAndTravellerHelper,
      VisitUserInteractor visitUserInteractor, MembershipInteractor membershipInteractor,
      TotalPriceCalculatorInteractor totalPriceCalculatorInteractor,
      SessionController sessionController) {
    super(view, navigator);
    this.checkUserCredentialsInteractor = checkUserCredentialsInteractor;
    this.addPassengerToShoppingCartInteractor = addPassengerToShoppingCartInteractor;
    this.getTravellersByTypeInteractor = getTravellersByTypeInteractor;
    checkUserBirthdayInteractor = checkUserBirthdateInteractor;
    this.dateHelperInterface = dateHelperInterface;
    this.findBuyerInLocalPassengersInteractor = findBuyerInLocalPassengersInteractor;
    this.countriesInteractor = countriesInteractor;
    this.saveTravellersInteractor = saveTravellersInteractor;
    this.updateUserTravellerWithTravellerRequestAndBuyerRequest =
        updateUserTravellerWithTravellerRequestAndBuyerRequest;
    this.mixBuyerAndTravellerHelper = mixBuyerAndTravellerHelper;
    this.visitUserInteractor = visitUserInteractor;
    this.membershipInteractor = membershipInteractor;
    this.totalPriceCalculatorInteractor = totalPriceCalculatorInteractor;
    this.sessionController = sessionController;
  }

  public void initializePresenter(String languageIsoCode, List<UserTraveller> possibleBuyers,
      List<TravellerRequiredFields> requiredTravellerInformation,
      BuyerRequiredFields buyerRequiredFields, Double totalPriceTicket, Double lastTotalPriceTicket,
      Locale currentLocale, boolean showPersuasive, BigDecimal collectionMethodWithPriceValue) {
    if (buyerRequiredFields != null) {
      this.languageIsoCode = languageIsoCode;
      this.buyerRequiredFields = buyerRequiredFields;
      buyerSpinner = possibleBuyers;
      this.showPersuasive = showPersuasive;
      this.requiredTravellerInformation = requiredTravellerInformation;
      this.collectionMethodWithPriceValue = collectionMethodWithPriceValue;
      mView.addPassengersWidgetViews(this.requiredTravellerInformation);
      mView.initIdentificationAdapter(buyerRequiredFields.getIdentificationTypes());
      setInitialSpinnerBuyerTraveller();
      configureContactFields();
      checkTicketsRepricing(totalPriceTicket, lastTotalPriceTicket, currentLocale);
      mView.setDefaultPassenger(hasMoreNonChildPassengers());
      if (this.showPersuasive) mView.initPersuasionSnackBar();
      mView.initPassengerCustomKeyboard();
      mView.initContactCustomKeyBoard();
      mView.autoCompleteGeolocalizationInfo();
    } else {
      mView.showGeneralError();
    }
  }

  public void refreshPresenterDataAfterUserLogin(List<UserTraveller> possibleBuyers) {
    buyerSpinner = possibleBuyers;
    mView.addPassengersWidgetViews(requiredTravellerInformation);
    mView.initIdentificationAdapter(buyerRequiredFields.getIdentificationTypes());
    setInitialSpinnerBuyerTraveller();
    configureContactFields();
    mView.setDefaultPassenger(hasMoreNonChildPassengers());
    if (showPersuasive) mView.initPersuasionSnackBar();
    mView.initPassengerCustomKeyboard();
    mView.initContactCustomKeyBoard();
    mView.autoCompleteGeolocalizationInfo();
  }

  public boolean hasMoreNonChildPassengers() {
    for (int i = 0; i < requiredTravellerInformation.size(); i++) {
      if (i > 0 && requiredTravellerInformation.get(i).getTravellerType() != TravellerType.INFANT) {
        return true;
      }
    }
    return false;
  }

  private void setSelectedSwitchOrSpinnerBuyer(UserTraveller buyer, int positionId) {
    selectedBuyer = buyer;
    selectedBuyerPosition = positionId;
  }

  private void setInitialSpinnerBuyerTraveller() {
    selectedBuyerPosition =
        findBuyerInLocalPassengersInteractor.findBuyerInLocalPassengers(buyerSpinner);
    selectedBuyer = buyerSpinner.get(selectedBuyerPosition);
  }

  void configureContactFields() {
    configureContactHeader();
    configureContactImage();
    configureContactSpinnerName();
    configureContactName();
    configureContactLastName();
    configureContactIdentification();
    configureContactDateOfBirth();
    configureContactCPF();
    configureContactAddress();
    configureContactCity();
    configureContactState();
    configureContactZipCode();
    configureContactPhonePrefix();
    configureContactPhoneNumber();
    configureContactAlternativePhoneNumber();
    configureCountryCode();
    configureContactEmail();
    configureShowOrHideButton();
  }

  private void configureContactHeader() {
    if (!isThereLocalData()) {
      mView.hideContactHeader();
    }
  }

  private void configureContactImage() {
    mView.showContactImage();
  }

  private void configureContactSpinnerName() {
    mView.showContactSpinnerItem(selectedBuyerPosition);
  }

  private void configureContactName() {
    if (((isThereLocalData() && (!isBuyerSelectedNameNewContact()))
        || (buyerRequiredFields.getNeedsName() != RequiredField.MANDATORY))) {
      mView.hideContactName();
    }
    showContactName();
  }

  private void configureContactLastName() {
    if ((isThereLocalData() && (!isBuyerSelectedNameNewContact()))
        || (buyerRequiredFields.getNeedsLastNames() != RequiredField.MANDATORY)) {
      mView.hideContactLastName();
    }
    showContactLastName();
  }

  private void configureContactIdentification() {
    if (buyerRequiredFields.getNeedsIdentification() != RequiredField.MANDATORY) {
      mView.hideContactIdentification();
    } else {
      if (selectedBuyer.getUserProfile().getUserIdentificationList() != null
          && selectedBuyer.getUserProfile().getUserIdentificationList().size() > 0) {

        for (int i = 0; i < buyerRequiredFields.getIdentificationTypes().size(); i++) {
          for (int x = 0; x < selectedBuyer.getUserProfile().getUserIdentificationList().size();
              x++) {
            String identificationTypePassenger =
                buyerRequiredFields.getIdentificationTypes().get(i).value();
            String identificationTypeTraveller = selectedBuyer.getUserProfile()
                .getUserIdentificationList()
                .get(x)
                .getIdentificationType()
                .name();

            if (identificationTypePassenger.equals(identificationTypeTraveller)) {
              mView.setIdentificationTypePosition(i);
              mView.showContactIdentification(selectedBuyer.getUserProfile()
                  .getUserIdentificationList()
                  .get(x)
                  .getIdentificationId());
            }
          }
        }
      }
    }
  }

  private void configureContactDateOfBirth() {
    if (buyerRequiredFields.getNeedsDateOfBirth() != RequiredField.MANDATORY) {
      mView.hideContactDateOfBirth();
    } else {
      configureDateOfBirthMaxAndMin();
      showContactDateOfBirth();
    }
  }

  private void configureDateOfBirthMaxAndMin() {
    Date minValidDate = checkUserBirthdayInteractor.getMinValidDate(TravellerType.ADULT);
    Date maxValidDate = checkUserBirthdayInteractor.getMaxValidDate(TravellerType.ADULT);
    mView.setBirthDateCalendarMaxAndMin(minValidDate, maxValidDate);
  }

  private void configureContactCPF() {
    if (buyerRequiredFields.getNeedsCpf() != RequiredField.MANDATORY) {
      mView.hideContactCPF();
    } else {
      showContactCPF();
    }
  }

  private void configureContactAddress() {
    if (buyerRequiredFields.getNeedsAddress() != RequiredField.MANDATORY) {
      mView.hideContactAddress();
    } else {
      showContactAddress();
    }
  }

  private void configureContactCity() {
    if (buyerRequiredFields.getNeedsCityName() != RequiredField.MANDATORY) {
      mView.hideContactCity();
    } else {
      showContactCity();
    }
  }

  private void configureContactState() {
    if ((buyerRequiredFields.getNeedsStateName() != RequiredField.MANDATORY)) {
      mView.hideContactState();
    } else {
      showContactState();
    }
  }

  private void configureContactZipCode() {
    if (buyerRequiredFields.getNeedsZipCode() != RequiredField.MANDATORY) {
      mView.hideContactZipCode();
    } else {
      showContactZipCode();
    }
  }

  private void configureContactPhonePrefix() {
    if ((selectedBuyer != null)
        && (selectedBuyer.getUserProfile() != null)
        && (selectedBuyer.getUserProfile().getUserAddress() != null)
        && (selectedBuyer.getUserProfile().getUserAddress().getCountry() != null)
        && (!selectedBuyer.getUserProfile().getUserAddress().getCountry().isEmpty())) {
      String countryCode = selectedBuyer.getUserProfile().getUserAddress().getCountry();
      Country country = countriesInteractor.getCountryByCountryCode(languageIsoCode, countryCode);
      setPhoneCode(country, countryCode);
    }
  }

  private void configureContactPhoneNumber() {
    if (buyerRequiredFields.getNeedsPhoneNumber() != RequiredField.MANDATORY) {
      mView.hideContactPhoneNumber();
    } else {
      showContactPhoneNumber();
    }
  }

  private void configureContactAlternativePhoneNumber() {
    if (buyerRequiredFields.getNeedsAlternativePhoneNumber() != RequiredField.MANDATORY) {
      mView.hideContactAlternativePhoneNumber();
    } else {
      showContactAlternativePhoneNumber();
    }
  }

  private void configureCountryCode() {
    if (buyerRequiredFields.getNeedsCountryCode() != RequiredField.MANDATORY) {
      mView.hideContactCountryCode();
    } else {
      showContactCountryCode();
    }
  }

  private void configureContactEmail() {
    if (isUserLogged()) {
      String userEmail = checkUserCredentialsInteractor.getUserEmail();
      mView.showUnEditableEmail(userEmail);
      mView.hideContactEditableMail();
    } else {
      mView.hideFooterMail();
      showContactEditableEmail();
    }
  }

  private void configureShowOrHideButton() {
    if (!isThereLocalData()) {
      mView.hideShowOrHideButton();
    }
  }

  private void showContactName() {
    if ((selectedBuyer != null)
        && (selectedBuyer.getUserProfile() != null)
        && (selectedBuyer.getUserProfile().getName() != null)
        && (!selectedBuyer.getUserProfile().getName().isEmpty())) {
      String name = selectedBuyer.getUserProfile().getName();
      mView.showContactName(name);
    }
  }

  private void showContactLastName() {
    if ((selectedBuyer != null)
        && (selectedBuyer.getUserProfile() != null)
        && (selectedBuyer.getUserProfile().getFirstLastName() != null)
        && (!selectedBuyer.getUserProfile().getFirstLastName().isEmpty())) {
      String lastName = selectedBuyer.getUserProfile().getFirstLastName();
      mView.showContactLastName(lastName);
    }
  }

  private void showContactDateOfBirth() {
    if ((selectedBuyer != null)
        && (selectedBuyer.getUserProfile() != null)
        && (selectedBuyer.getUserProfile().getBirthDate() != 0)) {
      long birthDate = selectedBuyer.getUserProfile().getBirthDate();
      List<Integer> dayMonthYear = dateHelperInterface.getDayMonthYearFromTimeStamp(birthDate);
      int day = dayMonthYear.get(DAY);
      int month = dayMonthYear.get(MONTH) + 1;
      int year = dayMonthYear.get(YEAR);
      String birthdate = day + "-" + month + "-" + year;
      mView.showContactDateOfBirth(birthdate);
    }
  }

  private void showContactCPF() {
    if ((selectedBuyer != null)
        && (selectedBuyer.getUserProfile() != null)
        && (selectedBuyer.getUserProfile().getCpf() != null)
        && (!selectedBuyer.getUserProfile().getCpf().isEmpty())) {
      String cpf = selectedBuyer.getUserProfile().getCpf();
      mView.showContactCPF(cpf);
    }
  }

  private void showContactAddress() {
    if ((selectedBuyer != null)
        && (selectedBuyer.getUserProfile() != null)
        && (selectedBuyer.getUserProfile().getUserAddress() != null)
        && (selectedBuyer.getUserProfile().getUserAddress().getAddress() != null)
        && (!selectedBuyer.getUserProfile().getUserAddress().getAddress().isEmpty())) {
      String address = selectedBuyer.getUserProfile().getUserAddress().getAddress();
      mView.showContactAddress(address);
    }
  }

  private void showContactCity() {
    if ((selectedBuyer != null)
        && (selectedBuyer.getUserProfile() != null)
        && (selectedBuyer.getUserProfile().getUserAddress() != null)
        && (selectedBuyer.getUserProfile().getUserAddress().getCity() != null)
        && (!selectedBuyer.getUserProfile().getUserAddress().getCity().isEmpty())) {
      String city = selectedBuyer.getUserProfile().getUserAddress().getCity();
      mView.showContactCity(city);
    }
  }

  private void showContactState() {
    if ((selectedBuyer != null)
        && (selectedBuyer.getUserProfile() != null)
        && (selectedBuyer.getUserProfile().getUserAddress() != null)
        && (selectedBuyer.getUserProfile().getUserAddress().getState() != null)
        && (!selectedBuyer.getUserProfile().getUserAddress().getState().isEmpty())) {
      String state = selectedBuyer.getUserProfile().getUserAddress().getState();
      mView.showContactState(state);
    }
  }

  private void showContactZipCode() {
    if ((selectedBuyer != null)
        && (selectedBuyer.getUserProfile() != null)
        && (selectedBuyer.getUserProfile().getUserAddress() != null)
        && (selectedBuyer.getUserProfile().getUserAddress().getPostalCode() != null)
        && (!selectedBuyer.getUserProfile().getUserAddress().getPostalCode().isEmpty())) {
      String zipCode = selectedBuyer.getUserProfile().getUserAddress().getPostalCode();
      mView.showContactZipCode(zipCode);
    }
  }

  private void showContactPhoneNumber() {
    if ((selectedBuyer != null)
        && (selectedBuyer.getUserProfile() != null)
        && (selectedBuyer.getUserProfile().getPhoneNumber() != null)
        && (!selectedBuyer.getUserProfile().getPhoneNumber().isEmpty())) {
      String phoneNumber = selectedBuyer.getUserProfile().getPhoneNumber();
      mView.showContactPhoneNumber(phoneNumber);
    }
  }

  private void showContactAlternativePhoneNumber() {
    if ((selectedBuyer != null)
        && (selectedBuyer.getUserProfile() != null)
        && (selectedBuyer.getUserProfile().getMobilePhoneNumber() != null)
        && (!selectedBuyer.getUserProfile().getMobilePhoneNumber().isEmpty())) {
      String alternativePhoneNumber = selectedBuyer.getUserProfile().getMobilePhoneNumber();
      mView.showContactAlternativePhoneNumber(alternativePhoneNumber);
    }
  }

  private void showContactCountryCode() {
    if ((selectedBuyer != null)
        && (selectedBuyer.getUserProfile() != null)
        && (selectedBuyer.getUserProfile().getUserAddress() != null)
        && (selectedBuyer.getUserProfile().getUserAddress().getCountry() != null)
        && (!selectedBuyer.getUserProfile().getUserAddress().getCountry().isEmpty())) {
      String countryCode = selectedBuyer.getUserProfile().getUserAddress().getCountry();
      Country country = countriesInteractor.getCountryByCountryCode(languageIsoCode, countryCode);
      mView.showContactCountryCode(country);
    }
  }

  private void showContactEditableEmail() {
    if ((selectedBuyer != null) && (selectedBuyer.getEmail() != null) && (!selectedBuyer.getEmail()
        .isEmpty())) {
      String email = selectedBuyer.getEmail();
      mView.showContactEditableMail(email);
    }
  }

  public void showContactGeolocalizationInfo(String countryCode) {
    String viewPhoneCode = mView.getContactPhoneCode();
    String viewCountryCode = mView.getCountryCode();
    if (isGeolocalizationInfoEmpty(viewPhoneCode, viewCountryCode)) {
      Country country = countriesInteractor.getCountryByCountryCode(languageIsoCode, countryCode);
      mView.showContactCountryCode(country);
      setPhoneCode(country, countryCode);
    }
  }

  private boolean isGeolocalizationInfoEmpty(String phoneCode, String countryCode) {
    return (!((phoneCode != null)
        && (!phoneCode.isEmpty())
        && (countryCode != null)
        && (!countryCode.isEmpty())));
  }

  public void cleanContactFields() {
    mView.showContactName(null);
    mView.showContactLastName(null);
    mView.showContactDateOfBirth(null);
    mView.showContactCPF(null);
    mView.showContactIdentification(null);
    mView.showContactAddress(null);
    mView.showContactCity(null);
    mView.showContactState(null);
    mView.showContactZipCode(null);
    mView.showContactPhoneNumber(null);
    mView.showPhoneCode(null, null);
    mView.showContactResidentCountry(null);
    mView.showContactAlternativePhoneNumber(null);
    mView.showContactCountryCode(null);
    if (!isUserLogged()) {
      mView.showContactEditableMail(null);
    }
    mView.setNullTextWatchersError();
  }

  public void setDefaultPassenger(UserTraveller buyer, int newMainPosition) {
    if (isThereDataWhenSwitchSelected(buyer, newMainPosition)) {
      mView.hideContactName();
      mView.hideContactLastName();
      cleanContactFields();
      setSelectedSwitchOrSpinnerBuyer(buyer, newMainPosition);
      configureContactFields();
    } else if (isThereNoDataWhenSwitchSelected(buyer)) {
      mView.hideContactName();
      mView.hideContactLastName();
      setSelectedSwitchOrSpinnerBuyer(buyerSpinner.get(EMPTY_BUYER_ID), EMPTY_BUYER_ID);
      cleanContactFields();
    }
  }

  private boolean isThereDataWhenSwitchSelected(UserTraveller buyer, int newMainPosition) {
    return ((buyer != null) && (newMainPosition != -1));
  }

  private boolean isThereNoDataWhenSwitchSelected(UserTraveller buyer) {
    return ((buyer == null));
  }

  public void setNewDefaultPassengerSpinner(int position) {
    cleanContactFields();
    setSelectedSwitchOrSpinnerBuyer(buyerSpinner.get(position), position);
    configureContactFields();
  }

  public List<String> formatPassengersNameToShowOnSpinner(
      List<UserTraveller> mTravellersListForWidget) {
    List<String> travellersName = new ArrayList<>();
    String travellerFullName;
    for (UserTraveller userTraveller : mTravellersListForWidget) {
      travellerFullName = concatenateNameAndSurname(userTraveller);
      travellersName.add(travellerFullName);
    }
    return travellersName;
  }

  private String concatenateNameAndSurname(UserTraveller userTraveller) {
    String travellerFullName;
    if (userTraveller.getUserProfile().getFirstLastName() != null) {
      travellerFullName =
          userTraveller.getUserProfile().getName() + " " + userTraveller.getUserProfile()
              .getFirstLastName();
    } else {
      travellerFullName = userTraveller.getUserProfile().getName();
    }
    return travellerFullName;
  }

  public void setResidentCountry(Country country) {
    mView.showContactCountryCode(country);
  }

  public void setPhoneCode(Country country) {
    String phoneCodeAndCountry = "(" + country.getPhonePrefix() + ")" + " " + country.getName();
    mView.showPhoneCode(phoneCodeAndCountry, country);
  }

  private void setPhoneCode(Country country, String countryCode) {
    if (country != null) {
      setPhoneCode(country);
    } else {
      // TODO 07/07/17 TRIPATT-536 Review if this exception keeps happening
      mView.trackCountryException(countryCode, languageIsoCode);
    }
  }

  public void loadLocalPassengersByType() {
    for (UserTraveller.TypeOfTraveller type : UserTraveller.TypeOfTraveller.values()) {
      getTravellersByTypeInteractor.getTravellers(type, new OnUserTravellerRequestListener() {
        @Override public void onUserTravellersListByTypeAvailable(
            UserTraveller.TypeOfTraveller typeOfTraveller, List<UserTraveller> userTravellersList) {

          localTravellersGroupByType.put(typeOfTraveller, userTravellersList);
        }
      });
    }
  }

  private void checkTicketsRepricing(double currentTicketsPrices, double lastTicketsPrices,
      Locale currentLocale) {
    NumberFormat numberFormat = NumberFormat.getNumberInstance(currentLocale);
    DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
    decimalFormat.applyPattern("#.##");
    try {
      String sCurrentTicketsPrice = decimalFormat.format(currentTicketsPrices);
      String sLastTicketsPrice = decimalFormat.format(lastTicketsPrices);
      double formattedCurrentTicketsPrice = Double.valueOf(sCurrentTicketsPrice.replace(",", "."));
      double formattedLastTicketsPrice = Double.valueOf(sLastTicketsPrice.replace(",", "."));

      double differenceBetweenPrices = formattedCurrentTicketsPrice - formattedLastTicketsPrice;
      if (differenceBetweenPrices > 0) {
        mView.showIncreaseTicketRepricingMessage(differenceBetweenPrices);
      } else if (differenceBetweenPrices < 0) {
        mView.showDecreaseTicketRepricingMessage(Math.abs(differenceBetweenPrices));
      }
    } catch (NumberFormatException e) {
      e.printStackTrace();
      mView.trackCrashlyticException(e);
    }
  }

  private boolean isThereLocalData() {
    if (buyerSpinner != null) {
      return (buyerSpinner.size() > LIST_ONLY_WITH_EMPTY_CONTACT);
    }
    return false;
  }

  private boolean isUserLogged() {
    return checkUserCredentialsInteractor.isUserLogin();
  }

  public void navigateToPhonePrefix() {
    mNavigator.navigateToPhoneCode();
  }

  public void navigateToFrequentFlyer(List<UserFrequentFlyer> frequentFlyersSelected,
      List<Carrier> passengerCarriers, int passengerWidgetPosition, List<Carrier> flightCarriers) {
    List<Carrier> filteredCarriers = getFilteredCarriers(flightCarriers, passengerCarriers);
    mNavigator.navigateToFrequentFlyers(frequentFlyersSelected, passengerCarriers,
        passengerWidgetPosition, filteredCarriers);
  }

  public void navigateToCountryResident(int widgetPosition) {
    mNavigator.navigateToCountryResident(widgetPosition);
  }

  public void navigateToCountryNationality(int widgetPosition) {
    mNavigator.navigateToCountryNationality(widgetPosition);
  }

  public void navigateToCountryIdentification(int widgetPosition) {
    mNavigator.navigateToCountryIdentification(widgetPosition);
  }

  private void navigateToDuplicateBooking(CreateShoppingCartResponse shoppingCartResponse) {
    PassengerConflictDetails passengerConflictDetails = shoppingCartResponse.getShoppingCart()
        .getItineraryShoppingItem()
        .getPassengersConflictDetails()
        .get(0);
    mNavigator.navigateToDuplicateBooking(passengerConflictDetails,
        passengerConflictDetails.getPnrs());
  }

  public List<UserTraveller> getBuyers(String nameEmptyContactDetail, int contactWidgetId) {
    List<UserTraveller> localContacts = new ArrayList<>();
    UserTraveller defaultContactTraveller =
        getEmptyContactUserTraveller(nameEmptyContactDetail, contactWidgetId);
    List<UserTraveller> travellersAdults = getWidgetPassengersType(TravellerType.ADULT);
    localContacts.add(defaultContactTraveller);
    localContacts.addAll(removeNullUserTravellers(travellersAdults));
    return localContacts;
  }

  private UserTraveller getEmptyContactUserTraveller(String nameEmptyContactDetail, int contactId) {
    nameEmptyContact = nameEmptyContactDetail;
    return UserTravellerFactory.getEmptyContactUserTraveller(nameEmptyContactDetail, contactId);
  }

  public List<UserTraveller> getWidgetPassengersType(TravellerType travellerType) {
    if (TravellerType.ADULT == travellerType) {
      return localTravellersGroupByType.get(UserTraveller.TypeOfTraveller.ADULT);
    } else if (TravellerType.CHILD == travellerType) {
      return localTravellersGroupByType.get(UserTraveller.TypeOfTraveller.CHILD);
    } else {
      return localTravellersGroupByType.get(UserTraveller.TypeOfTraveller.INFANT);
    }
  }

  private List<Carrier> getFilteredCarriers(List<Carrier> visibleCarriersCodes,
      List<Carrier> availableCarriersCodes) {
    List<Carrier> availableCarriersList = new ArrayList<>();
    for (Carrier carrierCode : visibleCarriersCodes) {
      for (Carrier carrier : availableCarriersCodes) {
        if (carrierCode.getCode().equals(carrier.getCode())) {
          Carrier newCarrier = new Carrier(carrier.getCode(), carrier.getName());
          if (!availableCarriersList.contains(newCarrier)) {
            availableCarriersList.add(newCarrier);
          }
        }
      }
    }
    return availableCarriersList;
  }

  public void addPassengersToShoppingCart(final List<TravellerRequest> travellerRequests,
      CreateShoppingCartResponse createShoppingCartResponse,
      final AvailableProductsResponse mAvailableProductsResponse,
      ItinerarySortCriteria sortCriteria) {
    final PersonalInfoRequest personalInfoRequest =
        createRequestInformation(travellerRequests, createShoppingCartResponse, sortCriteria);
    mView.showLoadingDialog();

    addPassengerToShoppingCartInteractor.addAddPassenger(personalInfoRequest,
        new OnAddPassengersToShoppingCartListener() {
          @Override public void onSuccess(CreateShoppingCartResponse shoppingCartResponse) {
            mView.hideLoadingDialog();
            if (isOutdatedBookingId(shoppingCartResponse)) {
              mView.showOutdatedBookingId();
            } else if (isInvalidCPF(shoppingCartResponse)) {
              mView.invalidCPF();
            } else if (haveToShowDuplicateBooking(shoppingCartResponse)) {
              navigateToDuplicateBooking(shoppingCartResponse);
            } else if (haveToShowAddPassengerError(shoppingCartResponse)) {
              mView.trackPassengerValidationException(shoppingCartResponse);
              mView.showValidationErrorMessage();
            } else if (isGeneralMslError(shoppingCartResponse)) {
              mView.showGeneralError();
            } else {
              List<UserTraveller> userTravellerList = mView.getUserTravellers();
              UserTraveller buyerUserTraveller = parseBuyerRequestToUserTraveller();
              mixBuyerAndTravellerHelper.addBuyerToTheList(userTravellerList, buyerUserTraveller);
              saveTravellersInteractor.saveUserTravellerList(userTravellerList, isUserLogged());
              if (shoppingCartResponse.getShoppingCart() != null
                  && shoppingCartResponse.getShoppingCart().getTravellers() != null) {
                if (mAvailableProductsResponse.getInsuranceOffers().size() > 0) {
                  mNavigator.navigateToInsurance(shoppingCartResponse,
                      getNumBaggages(shoppingCartResponse.getShoppingCart().getTravellers()));
                } else {
                  mNavigator.navigateToPayment(shoppingCartResponse,
                      getNumBaggages(shoppingCartResponse.getShoppingCart().getTravellers()));
                }
              } else {
                mView.trackShoppingCartResponseException(shoppingCartResponse);
                mView.showGeneralError();
              }
            }
          }

          @Override public void onError(MslError error, String message) {
            mView.hideLoadingDialog();
            mNavigator.navigateToNoConnectionActivity();
          }
        });
  }

  private int getNumBaggages(List<Traveller> travellers) {
    int baggages = 0;
    for (Traveller traveller : travellers) {
      for (BaggageSelectionResponse baggageSelectionResponse : traveller.getBaggageSelections()) {
        baggages += baggageSelectionResponse.getBaggageDescriptor().getPieces();
      }
    }
    return baggages;
  }

  private PersonalInfoRequest createRequestInformation(List<TravellerRequest> travellerRequests,
      CreateShoppingCartResponse createShoppingCartResponse, ItinerarySortCriteria sortCriteria) {
    BuyerRequest buyerRequest = buildBuyerRequest();
    PersonalInfoRequest personalInfoRequest =
        new PersonalInfoRequest(buyerRequest, travellerRequests,
            createShoppingCartResponse.getShoppingCart().getBookingId(),
            createShoppingCartResponse.getSortCriteria(), Step.PASSENGER);

    if (sortCriteria != null) {
      personalInfoRequest.setSortCriteria(sortCriteria);
    }

    return personalInfoRequest;
  }

  private BuyerRequest buildBuyerRequest() {
    CreateBuyerRequestHelper createBuyerRequestHelper =
        new CreateBuyerRequestHelper(mView, buyerRequiredFields, checkUserCredentialsInteractor,
            isUserLogged());
    return createBuyerRequestHelper.create();
  }

  private UserTraveller parseBuyerRequestToUserTraveller() {
    BuyerRequest buyerRequest = buildBuyerRequest();
    UserTraveller userTraveller;
    int buyerIndexSelected = mView.getBuyerIndexSelected();

    if (buyerIndexSelected > 0) {
      userTraveller = buyerSpinner.get(buyerIndexSelected);
    } else {
      userTraveller = new UserTraveller();
      userTraveller.setUserProfile(new UserProfile());
    }
    userTraveller.setBuyer(true);

    updateUserTravellerWithTravellerRequestAndBuyerRequest.updateUserTravellerWithBuyerRequest(
        userTraveller, buyerRequest);

    return userTraveller;
  }

  private boolean haveToShowDuplicateBooking(
      CreateShoppingCartResponse createShoppingCartResponse) {
    return ((createShoppingCartResponse.getShoppingCart() != null)
        && (createShoppingCartResponse.getShoppingCart().getItineraryShoppingItem() != null)
        && (createShoppingCartResponse.getShoppingCart()
        .getItineraryShoppingItem()
        .getPassengersConflictDetails() != null)
        && (!createShoppingCartResponse.getShoppingCart()
        .getItineraryShoppingItem()
        .getPassengersConflictDetails()
        .isEmpty()));
  }

  private boolean haveToShowAddPassengerError(CreateShoppingCartResponse shoppingCartResponse) {
    return ((shoppingCartResponse.getMessages() != null)
        && (!shoppingCartResponse.getMessages()
        .isEmpty())
        && (shoppingCartResponse.getMessages().get(0).getCode() != null)
        && (!shoppingCartResponse.getMessages().get(0).getCode().isEmpty()));
  }

  private boolean isOutdatedBookingId(CreateShoppingCartResponse shoppingCartResponse) {
    return ((shoppingCartResponse.getMessages() != null) && (!shoppingCartResponse.getMessages()
        .isEmpty()) && ((shoppingCartResponse.getMessages()
        .get(0)
        .getCode()
        .equals(INVALID_BOOKING_ID)) || ((shoppingCartResponse.getMessages()
        .get(0)
        .getCode()
        .equals(BROKEN_FLOW)))));
  }

  private boolean isInvalidCPF(CreateShoppingCartResponse shoppingCartResponse) {
    return ((shoppingCartResponse.getMessages() != null) && (!shoppingCartResponse.getMessages()
        .isEmpty()) && (shoppingCartResponse.getMessages()
        .get(0)
        .getDescription()
        .contains(INCORRECT_CPF)));
  }

  private boolean isGeneralMslError(CreateShoppingCartResponse shoppingCartResponse) {
    return ((shoppingCartResponse.getError() != null) && ((shoppingCartResponse.getError().getCode()
        == GENERAL_ERROR)));
  }

  private boolean isBuyerSelectedNameNewContact() {
    if ((selectedBuyer != null)
        && (selectedBuyer.getUserProfile() != null)
        && (selectedBuyer.getUserProfile().getName() != null)
        && (!selectedBuyer.getUserProfile().getName().isEmpty())) {
      return selectedBuyer.getUserProfile().getName().equals(nameEmptyContact);
    }
    return false;
  }

  private List<UserTraveller> removeNullUserTravellers(List<UserTraveller> userTravellers) {
    List<UserTraveller> userTravellersNonNull = new ArrayList<>(userTravellers);
    for (UserTraveller userTraveller : userTravellers) {
      if ((userTraveller == null) || (userTraveller.getUserProfile() == null)) {
        userTravellersNonNull.remove(userTraveller);
      }
    }
    return userTravellersNonNull;
  }

  public boolean hasToShowLoginWidget(String contentKey, String notFoundText) {
    boolean isWidgetEnabled =
        contentKey != null && !contentKey.isEmpty() && !contentKey.contains(notFoundText);
    return isWidgetEnabled && !isUserLogged();
  }

  public void onTapLogin() {
    mNavigator.navigateToLogin();
  }

  public void onUserLoggedIn() {
    visitUserInteractor.loginVisitUser();
  }

  public void navigateToSearch() {
    mNavigator.navigateToSearch();
  }

  public boolean isMemberForCurrentMarket() {
    return membershipInteractor.isMemberForCurrentMarket();
  }

  public Double getTotalPrice(ShoppingCart shoppingCart, PricingBreakdown pricingBreakdown) {
    return totalPriceCalculatorInteractor.getTotalPrice(shoppingCart, pricingBreakdown,
        collectionMethodWithPriceValue, Step.PASSENGER);
  }

  public double getTotalPriceWithoutMembershipPerks(ShoppingCart shoppingCart,
      PricingBreakdown pricingBreakdown) {
    return totalPriceCalculatorInteractor.getTotalPriceWithoutMembershipPerks(shoppingCart,
        pricingBreakdown, collectionMethodWithPriceValue, Step.PASSENGER);
  }

  public double getMembershipPerks(PricingBreakdown pricingBreakdown) {
    return totalPriceCalculatorInteractor.getPriceBreakdownItem(pricingBreakdown, Step.PASSENGER,
        PricingBreakdownItemType.MEMBERSHIP_PERKS);
  }

  public double getSlashedMembershipPerks(PricingBreakdown pricingBreakdown) {
    return totalPriceCalculatorInteractor.getPriceBreakdownItem(pricingBreakdown, Step.PASSENGER,
        PricingBreakdownItemType.SLASHED_MEMBERSHIP_PERKS);
  }

  public boolean shouldApplyMembershipPerks(PricingBreakdown pricingBreakdown) {
    return totalPriceCalculatorInteractor.shouldApplyMembershipPerks(pricingBreakdown);
  }

  public boolean isUserLoggedIn() {
    return sessionController.getCredentials() != null;
  }
}
