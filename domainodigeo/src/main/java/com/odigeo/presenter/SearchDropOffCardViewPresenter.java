package com.odigeo.presenter;

import com.odigeo.data.db.helper.CitiesHandlerInterface;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.presenter.contracts.navigators.NavigationDrawerNavigatorInterface;
import com.odigeo.presenter.contracts.views.SearchDropOffCardViewInterface;

public class SearchDropOffCardViewPresenter
    extends BasePresenter<SearchDropOffCardViewInterface, NavigationDrawerNavigatorInterface> {

  private PreferencesControllerInterface mPreferencesControllerInterface;
  private CitiesHandlerInterface citiesHandlerInterface;

  public SearchDropOffCardViewPresenter(SearchDropOffCardViewInterface view,
      NavigationDrawerNavigatorInterface navigator,
      PreferencesControllerInterface preferencesControllerInterface,
      CitiesHandlerInterface citiesHandlerInterface) {
    super(view, navigator);
    mPreferencesControllerInterface = preferencesControllerInterface;
    this.citiesHandlerInterface = citiesHandlerInterface;
  }

  public void updateDropOffCardStatus() {
    mPreferencesControllerInterface.saveBooleanValue(
        PreferencesControllerInterface.SHOULD_ADD_DROPOFF_CARD, false);
  }

  public String getCityName(String cityIATA) {
    City city = citiesHandlerInterface.getCity(cityIATA);
    if (city == null) {
      return cityIATA;
    } else {
      return city.getCityName();
    }
  }
}
