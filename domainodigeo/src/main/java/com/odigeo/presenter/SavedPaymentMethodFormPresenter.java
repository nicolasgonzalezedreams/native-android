package com.odigeo.presenter;

import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.shoppingCart.request.CreditCardCollectionDetailsParametersRequest;
import com.odigeo.data.entity.userData.CreditCard;
import com.odigeo.interactors.ObfuscateCreditCardInteractor;
import com.odigeo.presenter.contracts.views.SavedPaymentMethodFormViewInterface;
import com.odigeo.presenter.listeners.OnClickSavedPaymentMethodRowListener;
import com.odigeo.tools.DateHelperInterface;
import java.util.List;

public class SavedPaymentMethodFormPresenter {

  private static final int VISIBILITY_GONE = 8;
  private static final String AMEX_CARD = "AX";
  private static final String AMEX_CONSUMER = "AB";

  private List<ShoppingCartCollectionOption> shoppingCartCollectionOptions;
  private OnClickSavedPaymentMethodRowListener onClickSavedPaymentMethodRowListener;
  private SavedPaymentMethodFormViewInterface savedPaymentMethodFormView;
  private ObfuscateCreditCardInteractor obfuscateCreditCardInteractor;
  private DateHelperInterface dateHelper;
  private CreditCard creditCard;

  public SavedPaymentMethodFormPresenter(
      SavedPaymentMethodFormViewInterface savedPaymentMethodFormView,
      OnClickSavedPaymentMethodRowListener onClickSavedPaymentMethodRowListener,
      ObfuscateCreditCardInteractor obfuscateCreditCardInteractor, DateHelperInterface dateHelper,
      CreditCard creditCard, List<ShoppingCartCollectionOption> shoppingCartCollectionOptions) {
    this.shoppingCartCollectionOptions = shoppingCartCollectionOptions;
    this.onClickSavedPaymentMethodRowListener = onClickSavedPaymentMethodRowListener;
    this.savedPaymentMethodFormView = savedPaymentMethodFormView;
    this.obfuscateCreditCardInteractor = obfuscateCreditCardInteractor;
    this.dateHelper = dateHelper;
    this.creditCard = creditCard;
  }

  public void buildRow() {

    String creditCardNumberObfuscated =
        obfuscateCreditCardInteractor.obfuscate(creditCard.getCreditCardNumber());

    savedPaymentMethodFormView.showRadioButtonText(creditCardNumberObfuscated);
    savedPaymentMethodFormView.showCreditCardImage(creditCard.getCreditCardTypeId());

    if (isCreditCardExpired()) {
      savedPaymentMethodFormView.showExpiryDateError();
      disableRow();
    } else if (!isCreditCardAccepted()) {
      savedPaymentMethodFormView.showCreditCardNotAccepted();
      disableRow();
    }

    if (isAmericanExpressCard()) {
      savedPaymentMethodFormView.initCVVAmexValidator();
      savedPaymentMethodFormView.hideCVVNormalToolTip();
    } else {
      savedPaymentMethodFormView.initCVVNormalValidator();
      savedPaymentMethodFormView.hideCVVAMEXToolTip();
    }

    savedPaymentMethodFormView.hideCVVSeparatorToolTip();
  }

  private void disableRow() {
    savedPaymentMethodFormView.disableRadioButton();
    savedPaymentMethodFormView.hideCVV();
    savedPaymentMethodFormView.reduceCardImageOpacity();
  }

  public void refreshRow(List<ShoppingCartCollectionOption> collectionOptions) {
    shoppingCartCollectionOptions = collectionOptions;
    resetRow();
    buildRow();
  }

  private void resetRow() {
    savedPaymentMethodFormView.enableRadioButton();
    savedPaymentMethodFormView.increaseCardImageOpacity();
    savedPaymentMethodFormView.hideCreditCardError();
    savedPaymentMethodFormView.clearCVV();
  }

  boolean isCreditCardExpired() {
    return !(ExpirateDateCreditCardValidator.validateDate(creditCard.getExpiryMonth(),
        creditCard.getExpiryYear(), dateHelper.getCurrentMonth(),
        dateHelper.getCurrentYearLastTwoCharacters(), null));
  }

  boolean isCreditCardAccepted() {
    String userPaymentMethod = creditCard.getCreditCardTypeId();
    for (ShoppingCartCollectionOption shoppingCartCollectionOption : shoppingCartCollectionOptions) {
      String paymentMethodCode;
      if (isCreditCard(shoppingCartCollectionOption)) {
        paymentMethodCode = shoppingCartCollectionOption.getMethod().getCreditCardType().getCode();
        if (paymentMethodCode.equals(userPaymentMethod)) {
          return true;
        }
      }
    }
    return false;
  }

  private boolean isCreditCard(ShoppingCartCollectionOption shoppingCartCollectionOption) {
    return (shoppingCartCollectionOption.getMethod().getType() != null
        && shoppingCartCollectionOption.getMethod().getType() == CollectionMethodType.CREDITCARD);
  }

  private boolean isAmericanExpressCard() {
    return (creditCard.getCreditCardTypeId().equals(AMEX_CARD) || creditCard.getCreditCardTypeId()
        .equals(AMEX_CONSUMER));
  }

  public void onClickCVVInfoTooltip(final int visibility) {
    if (visibility == VISIBILITY_GONE) {
      if (isAmericanExpressCard()) {
        savedPaymentMethodFormView.showCVVAMEXToolTip();
      } else {
        savedPaymentMethodFormView.showCVVNormalToolTip();
      }
    } else {
      savedPaymentMethodFormView.hideCVVToolTip();
    }
  }

  void scrollToCVVFieldWithError() {
    savedPaymentMethodFormView.scrollToCVVWithError();
  }

  CreditCardCollectionDetailsParametersRequest createCreditCardRequest() {
    CreditCardCollectionDetailsParametersRequest creditCardCollectionDetailsParametersRequest =
        new CreditCardCollectionDetailsParametersRequest();
    creditCardCollectionDetailsParametersRequest.setCardNumber(creditCard.getCreditCardNumber());
    creditCardCollectionDetailsParametersRequest.setCardOwner(creditCard.getOwner());
    creditCardCollectionDetailsParametersRequest.setCardSecurityNumber(
        savedPaymentMethodFormView.getCVV());
    creditCardCollectionDetailsParametersRequest.setCardTypeCode(creditCard.getCreditCardTypeId());
    creditCardCollectionDetailsParametersRequest.setCardExpirationMonth(
        creditCard.getExpiryMonth());
    creditCardCollectionDetailsParametersRequest.setCardExpirationYear(creditCard.getExpiryYear());
    return creditCardCollectionDetailsParametersRequest;
  }

  boolean isSelectedThisSavedPaymentMethod() {
    return savedPaymentMethodFormView.isRadioButtonChecked();
  }

  boolean isCVVInfoValid() {
    return savedPaymentMethodFormView.isTilCVVInfoValid();
  }

  public void onClickRadioButton(boolean isRadioButtonChecked, Object tag) {
    if (isRadioButtonChecked) {
      savedPaymentMethodFormView.expandCVV();
      onClickSavedPaymentMethodRowListener.OnClickSavedPaymentMethodRadioButton(tag,
          creditCard.getCreditCardTypeId());
    }
  }

  void initDefaultRadioButton() {
    savedPaymentMethodFormView.checkRadioButton();
    savedPaymentMethodFormView.showCVV();
  }

  void unCheckRadioButton() {
    savedPaymentMethodFormView.unCheckRadioButton();
  }

  public String getCreditCardCode() {
    return creditCard.getCreditCardTypeId();
  }
}
