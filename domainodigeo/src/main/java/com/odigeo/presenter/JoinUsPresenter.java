package com.odigeo.presenter;

import com.odigeo.presenter.contracts.navigators.JoinUsNavigatorInterface;
import com.odigeo.presenter.contracts.views.JoinUsViewInterface;

public class JoinUsPresenter extends BasePresenter<JoinUsViewInterface, JoinUsNavigatorInterface> {

  public JoinUsPresenter(JoinUsViewInterface view, JoinUsNavigatorInterface navigator) {
    super(view, navigator);
  }

  public void goToLogin() {
    mNavigator.showLoginView();
  }

  public void goToRegisterAccount() {
    mNavigator.showRegisterView();
  }
}
