package com.odigeo.presenter;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnSaveImportedTripListener;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.interactors.MyTripsInteractor;
import com.odigeo.interactors.MyTripsManualImportInteractor;
import com.odigeo.presenter.contracts.navigators.MyTripsNavigatorInterface;
import com.odigeo.presenter.contracts.views.MyTripsManualImportViewInterface;

public class MyTripsManualImportPresenter
    extends BasePresenter<MyTripsManualImportViewInterface, MyTripsNavigatorInterface> {

  private final TrackerControllerInterface trackerController;
  private final MyTripsInteractor myTripsInteractor;
  private final MyTripsManualImportInteractor myTripsManualImportInteractor;
  private Booking bookingImported;
  private boolean isAlreadyImported;
  OnSaveImportedTripListener onSaveImportedTripListener = new OnSaveImportedTripListener() {
    @Override public void onResponse(Booking booking, boolean isImported) {
      if (isImported) {
        handleAlreadyImported(booking);
      } else {
        handleSuccessImportTrip(booking);
      }
    }

    @Override public void onError(MslError error, String message) {
      handleErrorImportTrip();
    }
  };

  public MyTripsManualImportPresenter(MyTripsManualImportViewInterface view,
      MyTripsNavigatorInterface navigator, MyTripsManualImportInteractor interactor,
      TrackerControllerInterface trackerController, MyTripsInteractor myTripsInteractor) {
    super(view, navigator);
    this.myTripsManualImportInteractor = interactor;
    this.trackerController = trackerController;
    this.myTripsInteractor = myTripsInteractor;
  }

  public void init() {
    if (bookingImported != null) {
      showBookingImported();
    }
  }

  private void showBookingImported() {
    if (isAlreadyImported) {
      handleAlreadyImported(bookingImported);
    } else {
      handleSuccessImportTrip(bookingImported);
    }
  }

  public void validateEmailAndBookingIdFormat(String email, String bookingId) {
    mView.enableAddBookingButton(
        myTripsManualImportInteractor.validateEmailAndBookingIdFormat(email, bookingId));
  }

  public void checkErrorEmail(String email) {
    mView.showEmailError(myTripsManualImportInteractor.showErrorEmail(email));
  }

  public void checkErrorBookingId(String bookingId) {
    mView.showBookingIdError(myTripsManualImportInteractor.showErrorBookingId(bookingId));
  }

  public void importTrip(String email, final String bookingId) {
    mView.showLoading();

    myTripsManualImportInteractor.importTrip(onSaveImportedTripListener, email, bookingId);
  }

  private void handleAlreadyImported(Booking booking) {
    if (mView.isViewVisible()) {
      mView.hideLoading();
      mView.showAlreadyImported(booking);
    } else {
      this.isAlreadyImported = true;
      this.bookingImported = booking;
    }
  }

  private void handleErrorImportTrip() {
    mView.hideLoading();
    mView.showImportFail();
  }

  private void handleSuccessImportTrip(Booking booking) {
    if (mView.isViewVisible()) {
      mView.hideLoading();
      mView.showImportSuccess(booking);

      if (booking.isActive()) {
        checkActiveBookingsOnTripImport();
      }
    } else {
      this.isAlreadyImported = false;
      this.bookingImported = booking;
    }
  }

  public void checkActiveBookingsOnTripImport() {
    if (myTripsInteractor.hasActiveBookings()) {
      trackerController.trackActiveBookingsOnImportTrip();
    }
  }
}
