package com.odigeo.presenter.model;

import com.odigeo.data.entity.booking.Booking;

public class CrossSellingCard {

  private final Booking booking;
  private final String label;
  private final boolean showLabel;
  private final String title;
  private final int type;
  private final String subtitle;

  public CrossSellingCard(Booking booking, boolean showLabel, String label, String title, int type,
      String subtitle) {
    this.booking = booking;
    this.showLabel = showLabel;
    this.label = label;
    this.title = title;
    this.type = type;
    this.subtitle = subtitle;
  }

  public Booking getBooking() {
    return booking;
  }

  public String getLabel() {
    return label;
  }

  public boolean isShowLabel() {
    return showLabel;
  }

  public String getTitle() {
    return title;
  }

  public int getType() {
    return type;
  }

  public String getSubtitle() {
    return subtitle;
  }
}
