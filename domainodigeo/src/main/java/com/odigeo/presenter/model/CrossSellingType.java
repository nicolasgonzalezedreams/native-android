package com.odigeo.presenter.model;

public class CrossSellingType {
  public static final int TYPE_HOTEL = 1;
  public static final int TYPE_CARS = 2;
  public static final int TYPE_GROUND = 3;
  public static final int TYPE_GUIDE = 4;
}
