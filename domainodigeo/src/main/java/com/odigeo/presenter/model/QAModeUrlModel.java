package com.odigeo.presenter.model;

import java.io.Serializable;

public class QAModeUrlModel implements Serializable {

  private final String url;
  private final boolean isSelected;

  public QAModeUrlModel(String url, boolean isSelected) {
    this.url = url;
    this.isSelected = isSelected;
  }

  public String getUrl() {
    return url;
  }

  public boolean isSelected() {
    return isSelected;
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    QAModeUrlModel that = (QAModeUrlModel) o;
    return url != null ? url.equals(that.url) : that.url == null;
  }

  @Override public int hashCode() {
    int result = url != null ? url.hashCode() : 0;
    result = 31 * result + (isSelected ? 1 : 0);
    return result;
  }
}
