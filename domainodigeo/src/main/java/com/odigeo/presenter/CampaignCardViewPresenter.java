package com.odigeo.presenter;

import com.odigeo.presenter.contracts.navigators.NotificationNavigatorInterface;
import com.odigeo.presenter.contracts.views.CampaignCardViewInterface;

/**
 * @author José Eduardo Cabrera Rivera
 * @version 1.0
 * @since 11/11/2015.
 */
public class CampaignCardViewPresenter
    extends BasePresenter<CampaignCardViewInterface, NotificationNavigatorInterface> {
  public CampaignCardViewPresenter(CampaignCardViewInterface view,
      NotificationNavigatorInterface navigator) {
    super(view, navigator);
  }
}
