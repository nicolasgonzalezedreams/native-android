package com.odigeo.presenter;

import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.interactors.LogoutInteractor;
import com.odigeo.interactors.TravellerDetailInteractor;
import com.odigeo.presenter.contracts.navigators.TravellersListNavigatorInterface;
import com.odigeo.presenter.contracts.views.TravellerDetailViewInterface;
import com.odigeo.presenter.listeners.OnUserUpdated;
import com.odigeo.tools.CheckerTool;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by carlos.navarrete on 02/09/15.
 */
public class TravellerDetailPresenter
    extends BasePresenter<TravellerDetailViewInterface, TravellersListNavigatorInterface> {

  private TravellerDetailInteractor mTravellerDetailInteractor;
  private SessionController sessionController;
  private LogoutInteractor mLogoutInteractor;

  public TravellerDetailPresenter(TravellerDetailViewInterface view,
      TravellersListNavigatorInterface navigator, TravellerDetailInteractor interactor,
      SessionController sessionController, LogoutInteractor logoutInteractor) {
    super(view, navigator);
    mTravellerDetailInteractor = interactor;
    this.sessionController = sessionController;
    mLogoutInteractor = logoutInteractor;
  }

  public void initActionBar(String title, int idIcon) {
    mNavigator.setValuesActionBar(title, idIcon);
  }

  public Date getMinValidDate(UserTraveller.TypeOfTraveller travellerType) {
    return mTravellerDetailInteractor.getMinValidDate(travellerType);
  }

  public Date getMaxValidDate(UserTraveller.TypeOfTraveller travellerType) {
    return mTravellerDetailInteractor.getMaxValidDate(travellerType);
  }

  public boolean saveTraveller(long userTravellerId, final String name, String middleName,
      final String surname, String secondLastName, UserTraveller.TypeOfTraveller typeOfTraveller,
      UserProfile.Title title, long birthDate, String nationalityCountryCode, boolean isBuyer,
      String email, String gender, final boolean isDefault, String photoProfile, String mealType,
      String cpf, String state, String city, String address, String zipCode,
      String addressCountryCode, String addressType, boolean isAddressPrimary, String addressAlias,
      String phoneCode, String phone, String alternatePhoneCode, String alternatePhone,
      String mobilePhone,
      HashMap<UserIdentification.IdentificationType, UserIdentification> identificationMap,
      List<UserFrequentFlyer> frequentFlyerList) {

    mView.showLoadingDialog();
    mTravellerDetailInteractor.saveUserTraveller(userTravellerId, name, middleName, surname,
        secondLastName, typeOfTraveller, title, birthDate, nationalityCountryCode, isBuyer, email,
        gender, isDefault, photoProfile, mealType, cpf, state, city, address, zipCode,
        addressCountryCode, addressType, isAddressPrimary, addressAlias, phoneCode, phone,
        alternatePhoneCode, alternatePhone, mobilePhone, identificationMap, frequentFlyerList, -1,
        new OnUserUpdated() {
          @Override public void onUserUpdatedSuccessful() {
            mNavigator.showBackView();
            mView.hideLoadingDialog();
          }

          @Override public void onUserUpdatedError() {
            mView.hideLoadingDialog();
            mView.showSavedErrorDialog();
          }

          @Override public void onUserInvalidCredentialsError() {
          }

          @Override public void onUserAuthError() {
            mLogoutInteractor.logout(new OnRequestDataListener<Boolean>() {
              @Override public void onResponse(Boolean object) {
                mView.showAuthError();
                sessionController.saveLogoutWasMade(true);
              }

              @Override public void onError(MslError error, String message) {
                mView.onLogoutError(error, message);
              }
            });
          }
        });
    return true;
  }

  public UserTraveller getUserTraveller(long userTravellerId) {
    return mTravellerDetailInteractor.getFullUserTraveller(userTravellerId);
  }

  public void deleteUserTraveller(long userTravellerId) {
    mView.showLoadingDialog();
    mTravellerDetailInteractor.deleteUserTraveller(userTravellerId, new OnUserUpdated() {
      @Override public void onUserUpdatedSuccessful() {
        mView.hideLoadingDialog();
        mNavigator.showBackView();
      }

      @Override public void onUserUpdatedError() {
        mView.hideLoadingDialog();
        mView.showSavedErrorDialog();
      }

      @Override public void onUserInvalidCredentialsError() {
      }

      @Override public void onUserAuthError() {
        mLogoutInteractor.logout(new OnRequestDataListener<Boolean>() {
          @Override public void onResponse(Boolean object) {
            mView.showAuthError();
            sessionController.saveLogoutWasMade(true);
          }

          @Override public void onError(MslError error, String message) {
            mView.onLogoutError(error, message);
          }
        });
      }
    });
  }

  public boolean validateBirthDate(UserTraveller.TypeOfTraveller typeOfTraveller, long birthDate,
      String dateString) {
    if (CheckerTool.isEmptyField(dateString)) {
      return true;
    }
    long minDate = mTravellerDetailInteractor.getMinValidDate(typeOfTraveller).getTime();
    long maxDate = mTravellerDetailInteractor.getMaxValidDate(typeOfTraveller).getTime();
    if (birthDate < minDate || birthDate > maxDate) {
      mView.showDateError();
      return false;
    }
    return true;
  }

  public void onAuthOkClicked() {
    mNavigator.navigateToLogin();
  }

  public void showSecondTextView() {
    mView.showSecondSurnameView();
  }
}
