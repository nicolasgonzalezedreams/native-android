package com.odigeo.presenter.listeners;

/**
 * Created by matias.dirusso on 09/07/2015.
 */
public interface UserChangeMailListener {

  void onChangeMailOk(boolean response);

  void onChangeMailFail(String error);
}
