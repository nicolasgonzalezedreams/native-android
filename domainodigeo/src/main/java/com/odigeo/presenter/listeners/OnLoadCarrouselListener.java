package com.odigeo.presenter.listeners;

import com.odigeo.data.entity.carrousel.Carousel;

public interface OnLoadCarrouselListener {

  void onCarouselLoaded(Carousel carousel);

  void onErrorLoadingCarousel();
}
