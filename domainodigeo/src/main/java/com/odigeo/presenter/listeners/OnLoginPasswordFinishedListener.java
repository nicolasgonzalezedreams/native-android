package com.odigeo.presenter.listeners;

import com.odigeo.data.entity.userData.User;

public interface OnLoginPasswordFinishedListener {

  void onUsernameError(boolean empty);

  void onPasswordError(boolean empty);

  void onSuccess(User user);

  void onServerError();

  void onBlockedUser(String email);

  void onLegacyUser(String email);

  void onPendingEmail(String email);

  void onFacebookUser(String email);

  void onGoogleUser(String email);

  void onWrongEmail();

  void onWrongBrand();

  void onInvalidCredentials();

  void onPasswordDoesNotValiate();

  void onDBError();
}
