package com.odigeo.presenter.listeners;

public interface OnClickSavedPaymentMethodRowListener {
  void OnClickSavedPaymentMethodRadioButton(Object tag, String creditCardCode);
}
