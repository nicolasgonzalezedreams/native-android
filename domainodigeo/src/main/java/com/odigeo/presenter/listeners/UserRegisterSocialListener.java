package com.odigeo.presenter.listeners;

//TODO: Es exactamente igual que UserRegisterFacebookListener
public interface UserRegisterSocialListener {

  void onUserRegisterOk();

  void onUserRegisterFail(int statusCode, String error);
}
