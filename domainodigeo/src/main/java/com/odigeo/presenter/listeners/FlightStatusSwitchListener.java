package com.odigeo.presenter.listeners;

/**
 * Created by eleazarspak on 28/9/16.
 */

public interface FlightStatusSwitchListener {

  void showFlightStatusSwitch();
}
