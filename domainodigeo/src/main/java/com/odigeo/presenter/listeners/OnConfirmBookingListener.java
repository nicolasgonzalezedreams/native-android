package com.odigeo.presenter.listeners;

import com.odigeo.data.entity.shoppingCart.BookingResponse;

public interface OnConfirmBookingListener {
  void onSuccess(BookingResponse bookingResponse);

  void onGeneralError(BookingResponse bookingResponse);

  void onNoConnectionError();

  void onInternalError(BookingResponse bookingResponse);
}