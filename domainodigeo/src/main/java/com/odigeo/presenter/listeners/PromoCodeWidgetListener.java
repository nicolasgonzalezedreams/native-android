package com.odigeo.presenter.listeners;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;

public interface PromoCodeWidgetListener {

  void onAddPromoCodeSuccessful(CreateShoppingCartResponse shoppingCartResponse);

  void onDeletePromoCodeSuccessful(CreateShoppingCartResponse shoppingCartResponse);

  void onOutdatedBookingId();

  void onGeneralMslError();
}