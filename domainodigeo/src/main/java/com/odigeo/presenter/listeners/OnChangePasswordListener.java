package com.odigeo.presenter.listeners;

public interface OnChangePasswordListener {

  void onSuccess();

  void onError();

  void onCurrentPasswordWrong();

  void onPasswordDoesNotMatch();

  void onCurrentPasswordInvalidFormat();

  void onNewPasswordInvalidFormat();

  void onRepeatPasswordInvalidFormat();

  void onAuthError();
}
