package com.odigeo.presenter.listeners;

/**
 * @author Irving
 * @since 18/09/2015
 */
public interface OnUserUpdated {

  void onUserUpdatedSuccessful();

  void onUserUpdatedError();

  void onUserInvalidCredentialsError();

  void onUserAuthError();
}
