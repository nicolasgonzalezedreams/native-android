package com.odigeo.presenter.listeners;

import com.odigeo.data.entity.userData.User;
import com.odigeo.data.net.error.MslError;

public interface OnClickCheckSavePaymentMethod {

  void onSuccess(User user);

  void onError(MslError error, String message);
}
