package com.odigeo.presenter.listeners;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 28/10/15
 */
public interface OnUpdateBookingsListener {
  void onUserLogged();

  void onNoUser();
}
