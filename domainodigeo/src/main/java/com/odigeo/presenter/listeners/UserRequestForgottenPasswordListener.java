package com.odigeo.presenter.listeners;

/**
 * Created by matias.dirusso on 21/08/2015.
 */
public interface UserRequestForgottenPasswordListener {

  void onUserRequestForgottenPasswordOk();

  void onUserRequestForgottenPasswordFail();

  void onAccountInactive(String email);

  void onUserNameError(boolean empty);

  void onUserDoesNotExist();

  void onUserRequestForgottenPasswordFailTryFacebookOrGoogle(String email);

  void onUserAuthError();
}
