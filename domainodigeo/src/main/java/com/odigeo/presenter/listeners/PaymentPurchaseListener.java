package com.odigeo.presenter.listeners;

public interface PaymentPurchaseListener {

  void onExplicitAcceptanceChecked(boolean checked);

  void onPaymentPurchaseContinue();
}