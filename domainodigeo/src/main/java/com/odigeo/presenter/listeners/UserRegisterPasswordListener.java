package com.odigeo.presenter.listeners;

public interface UserRegisterPasswordListener {

  void onUserRegisterOk(String email);

  void onUserRegisterFail();

  void onPasswordWrong();

  void onUserWrong();

  //MSL ERRORS

  void onInvalidCredentialsForUser();

  void onUserAlreadyExist();

  void onUserRegisteredFacebook(String email);

  void onUserRegisteredGoogle(String email);
}
