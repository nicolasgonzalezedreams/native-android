package com.odigeo.presenter.listeners;

import com.odigeo.data.entity.carrousel.Carousel;

public interface OnLoadStoredCarouselListener {
  void onLoadStoredCarrousel(Carousel carousel);

  void onErrorStoredCarrousel(String error);
}
