package com.odigeo.presenter.listeners;

import com.odigeo.data.entity.userData.UserTraveller;
import java.util.List;

/**
 * Created by julio.kun on 9/22/2015.
 */
public interface OnUserTravellerRequestListener {

  void onUserTravellersListByTypeAvailable(UserTraveller.TypeOfTraveller typeOfTraveller,
      List<UserTraveller> userTravellersList);
}
