package com.odigeo.presenter.listeners;

import com.odigeo.data.net.error.MslError;

public interface OnGetLocalizablesListener {
  void onSuccess();

  void onError(MslError error, String message);
}
