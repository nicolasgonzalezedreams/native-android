package com.odigeo.presenter.listeners;

import java.util.List;

/**
 * @author Oscar Álvarez
 */

public interface OnRemoveBookingsListener {

  void onSuccess();

  void onError(List<Long> removedBookings);
}