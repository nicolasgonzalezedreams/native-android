package com.odigeo.presenter.listeners;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.net.error.MslError;

public interface OnRemoveProductsFromShoppingCartListener {
  void onSuccess(CreateShoppingCartResponse shoppingCartResponse);

  void onError(MslError error, String message);
}