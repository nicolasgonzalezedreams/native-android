package com.odigeo.presenter.listeners;

public interface VisitInteractorListener {
  void onSuccess();
}
