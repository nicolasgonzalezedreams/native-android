package com.odigeo.presenter.listeners;

import com.odigeo.data.entity.localizables.OdigeoLocalizable;
import java.util.List;

public interface LocalizablesUtilsListener {
  void updateLocalizables(String oneCMSTable, List<OdigeoLocalizable> localizables);
}
