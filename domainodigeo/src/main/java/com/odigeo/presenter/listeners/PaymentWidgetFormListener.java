package com.odigeo.presenter.listeners;

import com.odigeo.data.entity.shoppingCart.CollectionMethodType;

public interface PaymentWidgetFormListener {
  void onCollectionOptionSelected(CollectionMethodType collectionMethodType);

  void onPaymentMethodSelected(String paymentMethod);

  void onPaymentWidgetValidation(boolean areFieldsCorrect);

  void onSavedPaymentMethodSelected(String paymentMethod);
}