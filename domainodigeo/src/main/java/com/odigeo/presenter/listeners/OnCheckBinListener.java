package com.odigeo.presenter.listeners;

import com.odigeo.data.entity.CreditCardBinDetails;
import com.odigeo.data.net.error.MslError;

public interface OnCheckBinListener {
  void onSuccessBinDetails(CreditCardBinDetails creditCardBinDetails);

  void onError(MslError error, String message);
}
