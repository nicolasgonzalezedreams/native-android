package com.odigeo.presenter;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.MessageResponse;
import com.odigeo.data.entity.shoppingCart.PromotionalCodeUsageResponse;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.shoppingCart.request.ModifyShoppingCartRequest;
import com.odigeo.data.entity.shoppingCart.request.PromotionalCodeRequest;
import com.odigeo.data.net.error.DAPIError;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.interactors.AddProductsToShoppingCartInteractor;
import com.odigeo.interactors.RemoveProductsFromShoppingCartInteractor;
import com.odigeo.presenter.contracts.components.BaseCustomComponentPresenter;
import com.odigeo.presenter.contracts.views.PromoCodeWidget;
import com.odigeo.presenter.listeners.OnAddProductsToShoppingCartListener;
import com.odigeo.presenter.listeners.OnRemoveProductsFromShoppingCartListener;
import com.odigeo.presenter.listeners.PromoCodeWidgetListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PromoCodeWidgetPresenter extends BaseCustomComponentPresenter<PromoCodeWidget> {

  private static final String INVALID_BOOKING_ID = "ERR-003";
  private static final String BROKEN_FLOW = "INT-003";
  private static final String PROMO_CODE_KEY = "promocode_key";

  private PromoCodeWidget view;
  private AddProductsToShoppingCartInteractor addProductsToShoppingCartInteractor;
  private RemoveProductsFromShoppingCartInteractor removeProductsFromShoppingCartInteractor;
  private PreferencesControllerInterface preferencesControllerInterface;
  private PromoCodeWidgetListener promoCodeWidgetListener;
  private CreateShoppingCartResponse createShoppingCartResponse;

  public PromoCodeWidgetPresenter(PromoCodeWidget view,
      AddProductsToShoppingCartInteractor addProductsToShoppingCartInteractor,
      RemoveProductsFromShoppingCartInteractor removeProductsFromShoppingCartInteractor,
      PreferencesControllerInterface preferencesControllerInterface) {
    super(view);
    this.view = view;
    this.addProductsToShoppingCartInteractor = addProductsToShoppingCartInteractor;
    this.removeProductsFromShoppingCartInteractor = removeProductsFromShoppingCartInteractor;
    this.preferencesControllerInterface = preferencesControllerInterface;
  }

  public void configurePromoCodeWidgetPresenter(
      CreateShoppingCartResponse createShoppingCartResponse) {
    this.createShoppingCartResponse = createShoppingCartResponse;
  }

  void setPromotionalCode(PromotionalCodeUsageResponse promotionalCode) {
    if (promotionalCode.getAmount() != null) {
      double promoCodeAmount = promotionalCode.getAmount().doubleValue();

      if (promoCodeAmount > 0) {
        promoCodeAmount = -promoCodeAmount;
        promotionalCode.setAmount(new BigDecimal(promoCodeAmount));
      }
    }

    view.hideValidateContainer();
    view.showValidPromoCodeContainer();
    view.setValidCode(promotionalCode.getCode());
    view.setPromoCodeDiscountAmount(promotionalCode.getAmount());
  }

  void expandWidget() {
    view.expandPromoCodeWidget();
  }

  void setPromoCodeWidgetListener(PromoCodeWidgetListener promoCodeWidgetListener) {
    this.promoCodeWidgetListener = promoCodeWidgetListener;
  }

  String getExistingCode() {
    return preferencesControllerInterface.getStringValue(PROMO_CODE_KEY);
  }

  public void onValidatePromoCodeButtonClick() {
    view.hideValidateButton();
    view.showValidateProgressBar();

    PromotionalCodeRequest promotionalCodeRequest = new PromotionalCodeRequest();
    promotionalCodeRequest.setCode(view.getInputCode());

    List<PromotionalCodeRequest> promotionalCodeRequests = new ArrayList<>();
    promotionalCodeRequests.add(promotionalCodeRequest);

    final ModifyShoppingCartRequest modifyShoppingCartRequest =
        createShoppingCartRequest(promotionalCodeRequests);

    addProductsToShoppingCartInteractor.addProducts(modifyShoppingCartRequest,
        new OnAddProductsToShoppingCartListener() {
          @Override public void onSuccess(CreateShoppingCartResponse shoppingCartResponse) {
            if (isOutdatedBookingId(shoppingCartResponse)) {
              promoCodeWidgetListener.onOutdatedBookingId();
            } else if (shoppingCartResponse.getShoppingCart() == null) {
              promoCodeWidgetListener.onGeneralMslError();
            } else if (havePromoCodeErrors(shoppingCartResponse.getMessages())) {
              view.hideValidateProgressBar();
              view.showValidateButton();
            } else {
              view.trackPromoCodeAdded();
              view.hideValidateProgressBar();
              view.showValidateButton();
              view.clearInputCode();

              for (PromotionalCodeUsageResponse promotionalCodeUsageResponse : shoppingCartResponse.getShoppingCart()
                  .getPromotionalCodes()) {
                storePromoCodeInPreferences(promotionalCodeUsageResponse.getCode());
                setPromotionalCode(promotionalCodeUsageResponse);
              }

              promoCodeWidgetListener.onAddPromoCodeSuccessful(shoppingCartResponse);
            }
          }

          @Override public void onError(MslError error, String message) {
            view.hideValidateProgressBar();
            view.showValidateButton();
          }
        });
  }

  public void onDeletePromoCodeButtonClick() {
    view.hideDeletePromoCodeContainer();
    view.showDeleteProgressBar();

    PromotionalCodeRequest promotionalCodeRequest = new PromotionalCodeRequest();
    promotionalCodeRequest.setCode(view.getValidCode());

    List<PromotionalCodeRequest> promotionalCodeRequests = new ArrayList<>();
    promotionalCodeRequests.add(promotionalCodeRequest);

    final ModifyShoppingCartRequest modifyShoppingCartRequest =
        createShoppingCartRequest(promotionalCodeRequests);

    removeProductsFromShoppingCartInteractor.removeProducts(modifyShoppingCartRequest,
        new OnRemoveProductsFromShoppingCartListener() {
          @Override public void onSuccess(CreateShoppingCartResponse shoppingCartResponse) {
            if (isOutdatedBookingId(shoppingCartResponse)) {
              promoCodeWidgetListener.onOutdatedBookingId();
            } else if (shoppingCartResponse.getShoppingCart() == null) {
              promoCodeWidgetListener.onGeneralMslError();
            } else if (havePromoCodeErrors(shoppingCartResponse.getMessages())) {
              view.hideDeleteProgressBar();
              view.showDeletePromoCodeContainer();
            } else {
              removePromoCodeFromPreferences();

              view.hideValidPromoCodeContainer();
              view.hideDeleteProgressBar();
              view.showDeletePromoCodeContainer();
              view.showValidateContainer();

              promoCodeWidgetListener.onDeletePromoCodeSuccessful(shoppingCartResponse);
            }
          }

          @Override public void onError(MslError error, String message) {
            view.hideDeleteProgressBar();
            view.showDeletePromoCodeContainer();
          }
        });
  }

  private ModifyShoppingCartRequest createShoppingCartRequest(
      List<PromotionalCodeRequest> promotionalCodeRequests) {
    ModifyShoppingCartRequest modifyShoppingCartRequest =
        new ModifyShoppingCartRequest(createShoppingCartResponse.getShoppingCart().getBookingId(),
            createShoppingCartResponse.getSortCriteria(), Step.PAYMENT);
    modifyShoppingCartRequest.setPromotionalCodes(promotionalCodeRequests);
    return modifyShoppingCartRequest;
  }

  private boolean isOutdatedBookingId(CreateShoppingCartResponse shoppingCartResponse) {
    return ((shoppingCartResponse.getMessages() != null) && (!shoppingCartResponse.getMessages()
        .isEmpty()) && ((shoppingCartResponse.getMessages()
        .get(0)
        .getCode()
        .equals(INVALID_BOOKING_ID)) || ((shoppingCartResponse.getMessages()
        .get(0)
        .getCode()
        .equals(BROKEN_FLOW)))));
  }

  private boolean havePromoCodeErrors(List<MessageResponse> messages) {
    List<String> errorCodes =
        Arrays.asList(DAPIError.WNG_007.getDapiError(), DAPIError.WNG_008.getDapiError(),
            DAPIError.WNG_009.getDapiError(), DAPIError.WNG_010.getDapiError());

    boolean foundPromoCodeError = false;
    if (messages != null) {
      for (MessageResponse messageResponse : messages) {
        if (messageResponse.getCode().equals(DAPIError.WNG_006.getDapiError())) {
          foundPromoCodeError = false;
        } else if (errorCodes.contains(messageResponse.getCode())) {
          foundPromoCodeError = true;
          String errorCode = messageResponse.getCode().replace("-", "_").toLowerCase();
          view.showPromoCodeError(errorCode, messageResponse.getDescription());
        }
      }
    }
    return foundPromoCodeError;
  }

  private void storePromoCodeInPreferences(String promoCode) {
    preferencesControllerInterface.saveStringValue(PROMO_CODE_KEY, promoCode);
  }

  private void removePromoCodeFromPreferences() {
    preferencesControllerInterface.deleteValue(PROMO_CODE_KEY);
  }

  public void onTACClick() {
    mView.showTAC();
  }
}