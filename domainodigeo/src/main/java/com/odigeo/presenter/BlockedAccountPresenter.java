package com.odigeo.presenter;

import com.odigeo.presenter.contracts.navigators.LoginNavigatorInterface;
import com.odigeo.presenter.contracts.views.BlockedAccountViewInterface;

/**
 * Created by ximena.perez on 21/08/2015.
 */
public class BlockedAccountPresenter
    extends BasePresenter<BlockedAccountViewInterface, LoginNavigatorInterface> {

  public BlockedAccountPresenter(BlockedAccountViewInterface view,
      LoginNavigatorInterface navigator) {
    super(view, navigator);
  }

  public void goToLogin() {
    mNavigator.showLoginView();
  }

  public void goToMail() {
    mNavigator.openMailApp();
  }
}
