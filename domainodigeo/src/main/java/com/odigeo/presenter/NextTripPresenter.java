package com.odigeo.presenter;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.interactors.BookingInteractor;
import com.odigeo.presenter.contracts.navigators.NavigationDrawerNavigatorInterface;
import com.odigeo.presenter.contracts.views.NextTripViewInterface;

/**
 * @author José Eduardo Cabrera Rivera
 * @version 1.0
 * @since 27/11/2015.
 */
public class NextTripPresenter
    extends BasePresenter<NextTripViewInterface, NavigationDrawerNavigatorInterface> {

  private BookingInteractor mBookingInteractor;

  public NextTripPresenter(NextTripViewInterface view, NavigationDrawerNavigatorInterface navigator,
      BookingInteractor bookingInteractor) {
    super(view, navigator);
    mBookingInteractor = bookingInteractor;
  }

  public void showBookingDetail(long bookingId) {
    Booking booking = mBookingInteractor.getBookingById(bookingId);
    if (booking != null) {
      mNavigator.navigateToTripDetail(booking);
    }
  }
}
