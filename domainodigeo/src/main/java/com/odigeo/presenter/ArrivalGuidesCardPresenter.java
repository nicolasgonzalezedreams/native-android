package com.odigeo.presenter;

import com.odigeo.data.download.DownloadController;
import com.odigeo.data.download.DownloadManagerController;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.interactors.ArrivalGuidesCardsInteractor;
import com.odigeo.presenter.contracts.navigators.MyTripDetailsNavigatorInterface;
import com.odigeo.presenter.contracts.views.ArrivalGuidesProviderViewInterface;
import com.odigeo.provider.FileProvider;
import java.io.File;
import java.util.List;

public class ArrivalGuidesCardPresenter
    extends BasePresenter<ArrivalGuidesProviderViewInterface, MyTripDetailsNavigatorInterface>
    implements DownloadManagerController {

  private static final String LANGUAGE_ENGLISH = "en";
  private final ArrivalGuidesCardsInteractor arrivalGuidesCardsInteractor;
  private final DownloadController downloadController;
  private final TrackerControllerInterface tracker;
  private Booking mBooking;
  private String mIATACode;
  private String mArrivalCityName;
  private long mGeoNodeId;
  private double mTotalDownloadSize;

  public ArrivalGuidesCardPresenter(ArrivalGuidesProviderViewInterface view,
      ArrivalGuidesCardsInteractor arrivalGuidesCardsInteractor,
      MyTripDetailsNavigatorInterface navigator, DownloadController downloadController,
      TrackerControllerInterface tracker) {
    super(view, navigator);
    this.arrivalGuidesCardsInteractor = arrivalGuidesCardsInteractor;
    this.tracker = tracker;
    this.downloadController = downloadController;
  }

  public void initCard(Booking booking, String language) {
    mBooking = booking;
    setDestinationData();
    downloadController.subscribeDownloader(this);

    if (!downloadController.isDownloadRunning()) {
      if (arrivalGuidesCardsInteractor.isGuideDownloaded(mIATACode)) {
        mView.showGuideDownloadedState();
      } else {
        mView.showDownloadGuideState();
      }
    } else {
      if (arrivalGuidesCardsInteractor.isGuideDownloaded(mIATACode)) {
        mView.showDownloadingGuideState();
      } else {
        mView.showDownloadGuideState();
      }
    }

    if (!language.equals(LANGUAGE_ENGLISH) && !arrivalGuidesCardsInteractor.isSameGuideLanguage(
        mGeoNodeId, language)) {
      mView.showGuideInDifferentLanguageMessage();
    }
  }

  public void downloadGuide(boolean isGoodNetworkConnectivityAvailable) {
    if (isGoodNetworkConnectivityAvailable) {
      mView.showDownloadingGuideState();
      downloadController.startDownload(arrivalGuidesCardsInteractor.getGuideUrl(mGeoNodeId),
          mIATACode);
    } else {
      mView.showNoConnectionDialog();
    }
  }

  public void confirmedDeleteGuide() {
    boolean wasGuideDeleted = arrivalGuidesCardsInteractor.deleteFile(mIATACode);
    if (wasGuideDeleted) {
      mView.showDownloadGuideState();
      downloadController.subscribeDownloader(this);
    }
  }

  public void openGuide() {
    tracker.trackOpenTripGuide();
    mNavigator.navigateToBookingGuideViewer(mBooking);
  }

  public void deleteGuide() {
    mView.showDeleteGuideConfirmationDialog(mArrivalCityName);
  }

  public void cancelDownload() {
    downloadController.cancelDownload();
    mView.showDownloadGuideState();
  }

  private void setDestinationData() {
    Segment segment = mBooking.getSegments().get(0);
    List<Section> sections = segment.getSectionsList();
    Section arrivalFlight = sections.get(sections.size() - 1);
    mArrivalCityName = arrivalFlight.getTo().getCityName();
    mIATACode = arrivalFlight.getTo().getCityIATACode();
    mGeoNodeId = arrivalFlight.getTo().getGeoNodeId();
  }

  @Override public void doAfterDownloadAction(String filename) {
    downloadController.unsubscribeDownloader();
    if (mIATACode.equals(filename)) {
      mView.showGuideDownloadedState();
    }
  }

  @Override public void setTotalDownloadSize(double totalSize) {
    mTotalDownloadSize = totalSize;
  }

  @Override public void onProgress(double progress) {
    mView.showDownloadProgress(progress, mTotalDownloadSize);
  }

  @Override public void onNoConnectionError() {
    mView.showNoConnectionDialog();
  }

  @Override public void onDownloadFailedError() {
    mView.showDownloadFailedDialog();
  }

  @Override public void onInsufficientSpaceError() {
    mView.showInsufficientSpaceDialog();
  }

  public void subscribeDownloadManager() {
    downloadController.subscribeDownloader(this);
  }

  public void unsubscribeDownloadManager() {
    downloadController.unsubscribeDownloader();
  }
}
