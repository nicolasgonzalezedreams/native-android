package com.odigeo.presenter;

import com.odigeo.data.net.error.MslError;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.interactors.LoginInteractor;
import com.odigeo.interactors.MyTripsInteractor;
import com.odigeo.interactors.TravellerDetailInteractor;
import com.odigeo.presenter.contracts.navigators.SocialLoginNavigatorInterface;
import com.odigeo.presenter.contracts.views.FacebookAlreadyRegisteredViewInterface;

public class FacebookAlreadyRegisteredPresenter extends SocialLoginPresenter {

  public FacebookAlreadyRegisteredPresenter(FacebookAlreadyRegisteredViewInterface view,
      LoginInteractor loginInteractor, TravellerDetailInteractor travellersInteractor,
      SocialLoginNavigatorInterface navigator, MyTripsInteractor myTripsInteractor,
      TrackerControllerInterface trackerControllerInterface) {
    super(view, navigator, loginInteractor, myTripsInteractor, travellersInteractor,
        trackerControllerInterface);
  }

  @Override protected void handleLoginError(MslError error, String source, String email) {
    buildSocialLoginErrorMessage(error, source);
  }
}
