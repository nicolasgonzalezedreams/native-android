package com.odigeo.presenter;

import com.odigeo.presenter.contracts.navigators.FrequentFlyerCodesInterface;
import com.odigeo.presenter.contracts.navigators.FrequentFlyerCodesNavigatorInterface;

/**
 * Author: Oscar Alvarez
 * Date: 15/09/2015.
 */
public class FrequentFlyerCodesPresenter
    extends BasePresenter<FrequentFlyerCodesInterface, FrequentFlyerCodesNavigatorInterface> {
  public FrequentFlyerCodesPresenter(FrequentFlyerCodesInterface view,
      FrequentFlyerCodesNavigatorInterface navigator) {
    super(view, navigator);
  }
}
