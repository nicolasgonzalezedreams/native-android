package com.odigeo.presenter;

import com.odigeo.data.db.helper.CountriesDbHelperInterface;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.interactors.IdentificationsInteractor;
import com.odigeo.presenter.contracts.navigators.IdentificationsNavigatorInterface;
import com.odigeo.presenter.contracts.views.IdentificationsViewInterface;
import com.odigeo.tools.CheckerTool;

/**
 * Created by carlos.navarrete on 10/09/15.
 * This class is legacy , new validators can be found in odigeoLib\fieldsvalidator package
 * @link #com.odigeo.app.android.view.textwatchers.fieldsvalidation
 */

@Deprecated
public class IdentificationsPresenter
    extends BasePresenter<IdentificationsViewInterface, IdentificationsNavigatorInterface> {

  private IdentificationsInteractor mIdentificationInteractor;
  private CountriesDbHelperInterface mCountriesDbHelper;

  public IdentificationsPresenter(IdentificationsViewInterface view,
      IdentificationsNavigatorInterface navigator, IdentificationsInteractor interactor,
      CountriesDbHelperInterface countriesDbHelper) {
    super(view, navigator);
    this.mIdentificationInteractor = interactor;
    mCountriesDbHelper = countriesDbHelper;
  }

  public Country getCountryByCC(String languageIso, String countryCode) {
    return mCountriesDbHelper.getCountryByCountryCode(languageIso, countryCode);
  }

  public boolean validateFields(String numberID, String date, String country) {
    return validateNumeberID(numberID) && validateDate(date) && validateCountry(country);
  }

  public boolean validateIdCard(String idcard) {
    if (!mIdentificationInteractor.isEmpty(idcard)) {
      return mIdentificationInteractor.validateIdCardLive(idcard);
    }
    return true;
  }

  public boolean validatePassport(String passport) {
    if (!mIdentificationInteractor.isEmpty(passport)) {
      return mIdentificationInteractor.validatePassportLive(passport);
    }
    return true;
  }

  public boolean validateDNI(String dni) {
    if (!mIdentificationInteractor.isEmpty(dni)) {
      return mIdentificationInteractor.validateDNILive(dni);
    }
    return true;
  }

  public boolean validateNIE(String nie) {
    if (!mIdentificationInteractor.isEmpty(nie)) {
      return mIdentificationInteractor.validateNIELive(nie);
    }
    return true;
  }

  private boolean validateCountry(String country) {
    if (mIdentificationInteractor.isEmpty(country)) {
      mView.showOnCountrySelectedError();
      return false;
    }
    return true;
  }

  private boolean validateDate(String date) {
    if (mIdentificationInteractor.isEmpty(date)) {
      mView.showOnExpireDateError();
      return false;
    }
    return true;
  }

  private boolean validateNumeberID(String numberID) {
    if (!CheckerTool.checkTravellerDNICharacters(numberID)) {
      mView.showOnNumberIdentificationError();
      return false;
    }
    return true;
  }
}
