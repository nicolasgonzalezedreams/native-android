package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.request.PersonalInfoRequest;
import com.odigeo.data.net.listener.OnRequestDataListener;
import java.util.HashMap;

public interface AddPassengerNetControllerInterface extends BaseNetControllerInterface {

  void addPassengerToShoppingCart(OnRequestDataListener<CreateShoppingCartResponse> listener,
      PersonalInfoRequest personalInfoRequest);

  HashMap<String, String> getMapHeaders();
}
