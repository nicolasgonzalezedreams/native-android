package com.odigeo.data.net.listener;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.net.error.MslError;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 26/10/15
 */
public interface OnSaveImportedTripListener {

  /**
   * Correct Result of the import a trip
   *
   * @param booking Booking added.
   * @param isImported If the booking is already in database, so if it's already imported
   */
  void onResponse(Booking booking, boolean isImported);

  /**
   * Result with a fail
   *
   * @param error Error code
   * @param message Fail message
   */
  void onError(MslError error, String message);
}
