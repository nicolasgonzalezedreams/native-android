package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.data.entity.shoppingCart.request.BookingRequest;
import com.odigeo.data.net.listener.OnRequestDataListener;

public interface ConfirmBookingNetControllerInterface extends BaseNetControllerInterface {

  void confirmBooking(OnRequestDataListener<BookingResponse> listener,
      BookingRequest bookingRequest);
}