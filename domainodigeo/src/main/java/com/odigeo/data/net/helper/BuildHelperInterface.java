package com.odigeo.data.net.helper;

public interface BuildHelperInterface {

  void putUrl(String url);

  String getUrl();
}
