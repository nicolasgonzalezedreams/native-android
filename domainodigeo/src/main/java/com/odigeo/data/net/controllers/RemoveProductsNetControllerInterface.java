package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.request.ModifyShoppingCartRequest;
import com.odigeo.data.net.listener.OnRequestDataListener;

public interface RemoveProductsNetControllerInterface extends BaseNetControllerInterface {

  void removeProductsFromShoppingCart(OnRequestDataListener<CreateShoppingCartResponse> listener,
      ModifyShoppingCartRequest modifyShoppingCartRequest);
}