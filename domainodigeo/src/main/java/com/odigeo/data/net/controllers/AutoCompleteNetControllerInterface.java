package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.geo.City;
import com.odigeo.data.net.listener.OnRequestDataListListener;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 3/05/16
 */
public interface AutoCompleteNetControllerInterface {

  String API_URL = "autoComplete/";

  void getDestination(String query, String locale, String website,
      OnRequestDataListListener<City> onRequestDataListListener);
}
