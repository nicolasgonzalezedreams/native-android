package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.localizables.LocalizablesResponse;
import com.odigeo.data.net.listener.OnRequestDataListener;
import java.util.HashMap;

public interface LocalizablesNetController {
  void getLocalizables(OnRequestDataListener<LocalizablesResponse> listener);

  HashMap<String, String> getMapHeaders();
}
