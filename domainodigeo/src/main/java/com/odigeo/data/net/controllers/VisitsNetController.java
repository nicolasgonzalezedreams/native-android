package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.session.Visit;
import com.odigeo.data.entity.session.request.VisitRequest;
import com.odigeo.data.net.listener.OnRequestDataListener;
import java.util.HashMap;

public interface VisitsNetController extends BaseNetControllerInterface {

  void getVisits(final OnRequestDataListener<Visit> listener, VisitRequest visitRequest);

  HashMap<String, String> getMapHeaders();
}
