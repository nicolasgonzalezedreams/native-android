package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.shoppingCart.CreateShoppingCartRequestModel;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.net.listener.OnRequestDataListener;

public interface ShoppingCartNetControllerInterface {
  void getShoppingCart(CreateShoppingCartRequestModel createShoppingCartRequestModel,
      final OnRequestDataListener<CreateShoppingCartResponse> listener);

  void getAvailableProducts(long bookingId, OnRequestDataListener<String> listener);
}
