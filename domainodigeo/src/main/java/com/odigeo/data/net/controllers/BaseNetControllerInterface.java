package com.odigeo.data.net.controllers;

public interface BaseNetControllerInterface {

  String API_URL_USER = "user";
  String API_UR_DEVICE_TOKEN = ";deviceToken=";
  String API_URL_SEARCH = "search";
  String API_URL_NOTIFICATIONS = "notifications";
  String API_URL_VISIT = "visits";
  String API_URL_VISIT_USER = "visit/user";
}
