package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.userData.InsertCreditCardRequest;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.net.listener.OnRequestDataListener;
import java.util.HashMap;

public interface SavePaymentMethodNetController {

  void savePaymentMethod(final OnRequestDataListener<User> callback,
      InsertCreditCardRequest creditCard);

  HashMap<String, String> getMapHeaders();
}
