package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.userData.User;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;

public interface UserNetControllerInterface extends BaseNetControllerInterface {

  void registerUser(OnRequestDataListener<User> listener, User user);

  void getUser(OnAuthRequestDataListener<User> listener);

  void login(OnRequestDataListener<User> listener, String email, String token, String source);

  void requestForgottenPassword(OnRequestDataListener<Boolean> listener, String email);

  void resetPasswordUser(OnRequestDataListener<User> listener, String newPassword);

  void deleteUser(OnRequestDataListener<Boolean> listener);

  void updateUser(OnRequestDataListener<User> listener, User user);
}