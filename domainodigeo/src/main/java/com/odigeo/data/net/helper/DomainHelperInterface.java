package com.odigeo.data.net.helper;

public interface DomainHelperInterface {

  void putUrl(String url);

  String getUrl();
}
