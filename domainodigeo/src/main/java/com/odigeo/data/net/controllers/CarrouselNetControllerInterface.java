package com.odigeo.data.net.controllers;

import com.odigeo.data.net.listener.OnRequestDataListener;

public interface CarrouselNetControllerInterface {

  void getCarouselPromotion(OnRequestDataListener<String> listener, boolean hasToForceCache);

  String getCarouselFromMock();
}
