package com.odigeo.data.net.listener;

import com.odigeo.data.net.error.MslError;

public interface OnRequestDataListener<T> {
  /**
   * Result with a object
   *
   * @param object Object
   */
  void onResponse(T object);

  /**
   * Result with a fail
   *
   * @param error Error code
   * @param message Fail message
   */
  void onError(MslError error, String message);
}
