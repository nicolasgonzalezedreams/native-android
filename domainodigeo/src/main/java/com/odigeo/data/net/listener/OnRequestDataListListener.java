package com.odigeo.data.net.listener;

import com.odigeo.data.net.error.MslError;
import java.util.List;

public interface OnRequestDataListListener<T> {

  /**
   * Result with a list of objects
   *
   * @param list List
   */
  void onResponse(List<T> list);

  /**
   * Result with a fail
   *
   * @param error Error code
   * @param message Fail message
   */
  void onError(MslError error, String message);
}
