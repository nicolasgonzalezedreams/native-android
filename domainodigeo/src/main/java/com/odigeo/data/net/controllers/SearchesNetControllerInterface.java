package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.net.listener.OnRequestDataListener;

/**
 * Created by matias.dirusso on 25/7/2016.
 */
public interface SearchesNetControllerInterface extends BaseNetControllerInterface {

  void postUserSearch(OnRequestDataListener<StoredSearch> listener,
      final StoredSearch storedSearch);

  void deleteUserSearch(OnRequestDataListener<Boolean> listener, StoredSearch storedSearch);
}
