package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.BookingSummary;
import com.odigeo.data.entity.booking.BookingSummaryRequestItem;
import com.odigeo.data.net.listener.OnRequestDataListListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import java.util.List;

public interface BookingNetControllerInterface extends BaseNetControllerInterface {

  void getBookingsId(OnRequestDataListListener<Long> listener);

  void getBookingById(OnRequestDataListener<Booking> listener, String id);

  void getBookingByEmail(OnRequestDataListener<Booking> listener, String email, String id);

  void getBookingStatus(OnRequestDataListener<List<BookingSummary>> listener,
      List<BookingSummaryRequestItem> items);

  void changeBookingNotificationStatus(OnRequestDataListener<Boolean> listener, String id,
      boolean status);
}