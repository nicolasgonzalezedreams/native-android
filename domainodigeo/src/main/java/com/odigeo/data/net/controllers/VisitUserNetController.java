package com.odigeo.data.net.controllers;

import com.odigeo.data.net.listener.OnRequestDataListener;
import java.util.HashMap;

public interface VisitUserNetController extends BaseNetControllerInterface {

  void loginVisitUser(final OnRequestDataListener<String> listener, long userId);

  void logoutVisitUser(final OnRequestDataListener<String> listener, long userId);

  HashMap<String, String> getMapHeaders();
}
