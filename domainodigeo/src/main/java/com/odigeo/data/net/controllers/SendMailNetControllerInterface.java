package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.AttachmentItem;
import com.odigeo.data.net.listener.OnRequestDataListener;
import java.util.List;

/**
 * Created by eleazarspak on 2/5/16.
 */
public interface SendMailNetControllerInterface {

  void sendMail(OnRequestDataListener<Boolean> listener, String from, String to, String subject,
      String body, List<AttachmentItem> attachments);
}