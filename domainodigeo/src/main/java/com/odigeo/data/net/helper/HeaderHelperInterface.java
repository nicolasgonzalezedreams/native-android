package com.odigeo.data.net.helper;

import java.util.Map;

public interface HeaderHelperInterface {

  void putContentType(Map<String, String> hashMap);

  void putAcceptEncoding(Map<String, String> hashMap);

  void putAccept(Map<String, String> hashMap);

  void putAcceptWithoutVersion(Map<String, String> hashMap);

  void putHeaderNoneMatchParam(Map<String, String> hashMap);

  void putDeviceId(Map<String, String> hashMap);

  void createLoginAuthorizationHeader(Map<String, String> hashMap, String header);

  void putAcceptV4(Map<String, String> hashMap);
}
