package com.odigeo.data.net.error;

public enum MslError {

  //*******************
  //  AUTHENTICATION
  //*******************
  /**
   * When user fills the forms to Login or Register, and the User or the Password information
   * doesn't match with CAPI or users have change
   * the password or the mail in the website and returns to the App.
   */
  AUTH_000, /**
   * When user presses Login or Register with Fb, and the request with FB fails.
   */
  AUTH_005, /**
   * When user presses Login or Register with G+, and the request with g+ fails.
   */
  AUTH_006, /**
   * When the user tries to recover his password but his account was created with fb or g+.
   */
  AUTH_008,

  //************
  //  STORAGE
  //************

  /**
   * General error in Database
   */
  STO_000, /**
   * The user will not receive the email in any case, due to an issue during the generation of the
   * mail template.
   */
  STO_005,

  //************
  //   STATUS
  //************

  /**
   * The user's account is blocked
   */
  STA_000, /**
   * When the user tries to register with an already registered email
   */
  STA_001, /**
   * A Legacy user tries to login (need to change his password)
   */
  STA_002, /**
   * User tries to perform any CAPI operation but hasn't activated his account.
   */
  STA_003, /**
   * User tries to login or register with an email already registered via facebook
   */
  STA_006, /**
   * User tries to login or register with an email already registered via google+
   */
  STA_007, /**
   * the user just blocked his account due to max login attempts.
   */
  STA_009, /**
   * User tries to recover password but hasn't activated his account
   */
  STA_011,

  //************
  //   FIND
  //************

  /**
   * The email doesn't exists on BBDD
   */
  FND_000, /**
   * The email exists but not in the current brand
   */
  FND_001,

  //************
  //   VALIDATION
  //************

  /**
   * Password has less than 7 characters
   */
  VAL_002, /**
   * Password does not meet the regular expression ^(?=.*\\d)(?=.*[a-z]).{7,}$
   */
  VAL_003,

  //*************
  //   DEFAULT
  //*************

  /**
   * There is a unknown error
   */
  UNK_001,

  /**
   * There is a TimeOut Error
   */
  TO_001,

  //*************
  //   DEFAULT
  //*************

  /**
   * Facebook permission Error
   */
  FB_001;

  /**
   * This method replace to valueOf();
   *
   * @param value String to convert to enum
   * @return If found enum return enum but if not found return {@link #UNK_001}
   */
  public static MslError getEnum(String value) {

    for (MslError v : values()) {
      if (v.toString().equalsIgnoreCase(value)) {
        return v;
      }
    }

    return UNK_001;
  }

}
