package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.CreditCardBinDetails;
import com.odigeo.data.net.listener.OnRequestDataListener;

public interface BinCheckNetControllerInterface {
  void checkBin(OnRequestDataListener<CreditCardBinDetails> listener, String creditCardNumber);
}
