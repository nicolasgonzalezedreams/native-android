package com.odigeo.data.net.controllers;

import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;

/**
 * Created by matia on 1/16/2017.
 */

public interface NotificationNetControllerInterface extends BaseNetControllerInterface {

  void disableNotifications(OnAuthRequestDataListener<Boolean> listener, String token);

  void enableNotifications(OnRequestDataListener<Boolean> listener, String token);
}
