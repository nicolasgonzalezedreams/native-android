package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.booking.Guide;
import com.odigeo.data.net.listener.OnRequestDataListener;

public interface ArrivalGuideNetControllerInterface extends BaseNetControllerInterface {

  void getGuideInformation(final OnRequestDataListener<Guide> listener, String cityIataCode,
      final long geoNodeId);
}
