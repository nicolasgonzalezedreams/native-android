package com.odigeo.data.net;

/**
 * Created by Jaime Toca on 29/1/16.
 */
public interface NetToolInterface {

  boolean isThereInternetConnection();
}
