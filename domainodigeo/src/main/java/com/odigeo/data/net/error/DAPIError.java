package com.odigeo.data.net.error;

public enum DAPIError {
  WNG_006("WNG-006"), WNG_007("WNG-007"), WNG_008("WNG-008"), WNG_009("WNG-009"), WNG_010(
      "WNG-010"), WNG_019("WNG-019"), WNG_022("WNG-022"), WNG_024("WNG-024"),

  INT_001("INT-001"), INT_002("INT-002"), INT_003("INT-003"),

  VEL_001("VEL-001"), VEL_002("VEL-002"), VEL_003("VEL-003");

  private final String dapiError;

  DAPIError(final String dapiError) {

    this.dapiError = dapiError;
  }

  public String getDapiError() {
    return dapiError;
  }
}
