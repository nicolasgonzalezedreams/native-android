package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.net.NetCountry;
import com.odigeo.data.net.listener.OnRequestDataListListener;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 11/02/16
 */
public interface CountryNetControllerInterface {

  String API_URL = "countries";

  void getCountries(OnRequestDataListListener<NetCountry> onRequestDataListListener);
}
