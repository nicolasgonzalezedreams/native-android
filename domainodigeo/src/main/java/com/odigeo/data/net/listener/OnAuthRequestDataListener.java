package com.odigeo.data.net.listener;

/**
 * Use this listener just if when you need to manage an Auth Token Exception
 */
public interface OnAuthRequestDataListener<T> extends OnRequestDataListener<T> {

  void onAuthError();
}
