package com.odigeo.data.net.controllers;

import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.data.entity.shoppingCart.ResumeBooking;
import com.odigeo.data.net.listener.OnRequestDataListener;

public interface ResumeBookingNetController {
  void resumeBooking(final OnRequestDataListener<BookingResponse> listener,
      ResumeBooking resumeBooking);
}
