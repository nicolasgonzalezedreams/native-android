package com.odigeo.data.download;

public interface DownloadManagerController {

  void doAfterDownloadAction(String filename);

  void setTotalDownloadSize(double totalSize);

  void onProgress(double progress);

  void onNoConnectionError();

  void onDownloadFailedError();

  void onInsufficientSpaceError();
}
