package com.odigeo.data.download;

import java.util.Map;

public interface DownloadController {

  void subscribeDownloader(DownloadManagerController downloader);

  void unsubscribeDownloader();

  void startDownload(String url, String filename);

  void startDownload(String url, String filename, Map<String, String> headers);

  void cancelDownload();

  boolean isDownloadRunning();
}
