package com.odigeo.data.calendar.controllers;

import com.odigeo.presenter.CalendarPresenter;
import com.odigeo.tools.DateUtils;
import java.util.Date;
import java.util.List;

public class SingleTripCalendarController extends CalendarController {

  public static final int DATES_INDEX_DEPARTURE = 0;

  public SingleTripCalendarController(CalendarPresenter presenter) {
    super(presenter);
  }

  @Override public void showSelectedDates(List<Date> selectedDates, boolean avoidScrolling) {
    presenter.setCalendarBounds(presenter.getMinDate(), presenter.getMaxDate(),
        SELECTION_MODE_RANGE, avoidScrolling);
  }

  @Override
  public void dateSelected(List<Date> selectedDates, Date selectedDate, int segmentNumber) {
    Date dateInGmt = DateUtils.convertToGmtTime(selectedDate);

    selectedDates.set(DATES_INDEX_DEPARTURE, dateInGmt);

    showSelectedDates(selectedDates, true);

    presenter.setConfirmationButtonDates(selectedDates.get(DATES_INDEX_DEPARTURE));
    presenter.showConfirmationButton();
  }
}
