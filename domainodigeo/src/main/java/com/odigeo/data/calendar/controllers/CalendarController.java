package com.odigeo.data.calendar.controllers;

import com.odigeo.presenter.CalendarPresenter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public abstract class CalendarController {

  public static final int SELECTION_MODE_MULTI = 1;
  public static final int SELECTION_MODE_RANGE = 2;

  protected CalendarPresenter presenter;

  public CalendarController(CalendarPresenter presenter) {
    this.presenter = presenter;
  }

  public void initCalendar(List<Date> selectedDates, int segmentNumber) {
    presenter.hideDatesHeader();
    presenter.setSelectorBackground();
    setMinMaxDates();
  }

  public void setMinMaxDates() {
    Calendar nextYear = Calendar.getInstance();
    nextYear.add(Calendar.YEAR, 1);
    presenter.setMinDate(new Date());
    presenter.setMaxDate(nextYear.getTime());
  }

  public boolean isDateSelectable(List<Date> selectedDates, Date dateInLocal, int segmentNumber) {
    return true;
  }

  public abstract void showSelectedDates(List<Date> selectedDates, boolean avoidScrolling);

  public abstract void dateSelected(List<Date> selectedDates, Date selectedDate, int segmentNumber);
}
