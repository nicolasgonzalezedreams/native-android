package com.odigeo.data.calendar.controllers;

import com.odigeo.presenter.CalendarPresenter;
import com.odigeo.tools.DateUtils;
import java.util.Date;
import java.util.List;

public class MultiTripCalendarController extends CalendarController {

  public MultiTripCalendarController(CalendarPresenter presenter) {
    super(presenter);

    this.presenter.setCalendarFilter();
  }

  @Override public void showSelectedDates(List<Date> selectedDates, boolean avoidScrolling) {
    presenter.setCalendarBounds(presenter.getMinDate(), presenter.getMaxDate(),
        SELECTION_MODE_MULTI, avoidScrolling);
  }

  @Override
  public void dateSelected(List<Date> selectedDates, Date selectedDate, int segmentNumber) {
    Date dateInGmt = DateUtils.convertToGmtTime(selectedDate);
    selectedDates.set(segmentNumber, dateInGmt);
    presenter.setConfirmationButtonDates(dateInGmt);
    presenter.showConfirmationButton();
    showSelectedDates(selectedDates, true);
  }

  @Override
  public boolean isDateSelectable(List<Date> selectedDates, Date dateInLocal, int segmentNumber) {
    boolean allowedForPrevious = true;
    boolean allowedForFollowing = true;

    if (segmentNumber > 0 && DateUtils.isBefore(dateInLocal,
        selectedDates.get(segmentNumber - 1))) {
      allowedForPrevious = false;
    }

    if (segmentNumber < selectedDates.size() - 1 && DateUtils.isAfter(dateInLocal,
        selectedDates.get(segmentNumber + 1))) {
      allowedForFollowing = false;
    }

    return allowedForPrevious && allowedForFollowing;
  }
}
