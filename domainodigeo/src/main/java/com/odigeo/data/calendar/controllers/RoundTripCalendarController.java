package com.odigeo.data.calendar.controllers;

import com.odigeo.presenter.CalendarPresenter;
import com.odigeo.tools.DateUtils;
import java.util.Date;
import java.util.List;

public class RoundTripCalendarController extends CalendarController {

  public static final int DATES_INDEX_DEPARTURE = 0;
  public static final int DATES_INDEX_RETURN = 1;

  public RoundTripCalendarController(CalendarPresenter presenter) {
    super(presenter);
  }

  @Override public void initCalendar(List<Date> selectedDates, int segmentNumber) {
    super.initCalendar(selectedDates, segmentNumber);
    presenter.showDatesHeader();
  }

  @Override public void showSelectedDates(List<Date> selectedDates, boolean avoidScrolling) {
    updateDepartureHeader(selectedDates);
    updateReturnHeader(selectedDates);
    updateSkipLayout(selectedDates);

    presenter.setCalendarBounds(presenter.getMinDate(), presenter.getMaxDate(),
        SELECTION_MODE_RANGE, avoidScrolling);
  }

  @Override
  public void dateSelected(List<Date> selectedDates, Date selectedDate, int segmentNumber) {
    Date dateInGmt = DateUtils.convertToGmtTime(selectedDate);

    if (selectedDates.get(DATES_INDEX_DEPARTURE) == null) {
      initRoundTripFlow(selectedDates, dateInGmt);
    } else if (selectedDates.get(DATES_INDEX_RETURN) != null) {
      initRoundTripFlow(selectedDates, dateInGmt);
    } else if (dateInGmt.before(selectedDates.get(DATES_INDEX_DEPARTURE))) {
      initRoundTripFlow(selectedDates, dateInGmt);
    } else if (dateInGmt.after(selectedDates.get(DATES_INDEX_DEPARTURE))) {
      selectedDates.set(DATES_INDEX_RETURN, dateInGmt);
      presenter.setReturnDateHeader(dateInGmt);
    }

    showSelectedDates(selectedDates, true);

    if (selectedDates.get(DATES_INDEX_RETURN) != null) {
      presenter.setConfirmationButtonDates(selectedDates.get(DATES_INDEX_DEPARTURE),
          selectedDates.get(DATES_INDEX_RETURN));
      presenter.showConfirmationButton();
    } else {
      presenter.hideConfirmationButton();
      presenter.resetReturnDateHeader();
    }
  }

  private void updateDepartureHeader(List<Date> selectedDates) {
    if (selectedDates.size() > 0 && selectedDates.get(DATES_INDEX_DEPARTURE) != null) {
      presenter.setDepartureDateHeader(selectedDates.get(DATES_INDEX_DEPARTURE));
    } else {
      presenter.resetDepartureDateHeader();
    }
  }

  private void updateReturnHeader(List<Date> selectedDates) {
    if (selectedDates.size() > 1 && selectedDates.get(DATES_INDEX_RETURN) != null) {
      presenter.setReturnDateHeader(selectedDates.get(DATES_INDEX_RETURN));
    } else {
      presenter.resetReturnDateHeader();
    }
  }

  private void updateSkipLayout(List<Date> selectedDates) {
    if (selectedDates.size() > 1
        && selectedDates.get(DATES_INDEX_DEPARTURE) != null
        && selectedDates.get(DATES_INDEX_RETURN) == null) {
      presenter.showSkipReturnButton();
    } else {
      presenter.hideSkipReturnButton();
    }
  }

  private void initRoundTripFlow(List<Date> selectedDates, Date selectedDateInGmt) {
    selectedDates.set(DATES_INDEX_DEPARTURE, selectedDateInGmt);
    selectedDates.set(DATES_INDEX_RETURN, null);
    presenter.setDepartureDateHeader(selectedDateInGmt);
  }
}
