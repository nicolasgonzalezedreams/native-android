package com.odigeo.data.trustdefender;

public interface TrustDefenderController {

  void sendFingerPrint(String cyberSourceMerchantId, String jSessionId);
}
