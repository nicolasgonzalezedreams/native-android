package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.Baggage;
import java.util.ArrayList;
import java.util.List;

public interface BaggageDBDAOInterface {

  //TABLE
  String TABLE_NAME = "baggage";

  //FIELDS
  String INCLUDED_IN_PRICE = "included_in_price";
  String PIECES = "pieces";
  String WEIGHT = "weight";
  String SEGMENT_ID = "segment_id";
  String TRAVELLER_ID = "traveller_id";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  //FILTERS
  String FILTER_BY_SEGMENT_ID = SEGMENT_ID + "=?";

  /**
   * Get user baggage
   *
   * @param baggageId User baggage id
   * @return Baggage with id {@param BaggageId}
   */
  Baggage getBaggage(long baggageId);

  /**
   * Get user baggage list
   *
   * @param travellerId User traveller id
   * @return Baggage with id {@param travellerId}
   */
  List<Baggage> getBaggages(long travellerId);

  /**
   * Insert user baggage
   *
   * @param baggage User baggage to insert
   * @return The id of the inserted user booking, but if the insert fail return -1
   */
  long addBaggage(Baggage baggage);

  /**
   * Insert user baggage list
   *
   * @param baggages User baggage list to insert
   * @return The id of the inserted user booking, but if the insert fail return -1
   */
  ArrayList<Long> addBaggages(ArrayList<Baggage> baggages);

  /**
   * Delete user baggage
   *
   * @param segmentId segment for User baggage to delete
   * @return true if it was deleted
   */
  boolean deleteBaggage(long segmentId);

  /**
   * Delete all baggages
   *
   * @return rows affected
   */
  long deleteBagagges();
}
