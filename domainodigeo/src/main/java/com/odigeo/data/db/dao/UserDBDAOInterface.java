package com.odigeo.data.db.dao;

import com.odigeo.data.entity.userData.User;

public interface UserDBDAOInterface {

  //TABLE
  String TABLE_NAME = "user";

  //FIELDS
  String USER_ID = "user_id";
  String EMAIL = "email";
  String WEBSITE = "website";
  String ACCEPTS_NEWSLETTER = "accepts_newsletter";
  String ACCEPTS_PARTNERS_INFO = "accepts_partners_info";
  String CREATION_DATE = "creation_date";
  String LAST_MODIFIED = "last_modified";
  String LOCALE = "locale";
  String MARKETING_PORTAL = "marketing_portal";
  String SOURCE = "source";
  String STATUS = "status";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get the current user in the app
   *
   * @return Usert with data
   */
  User getCurrentUser();

  /**
   * Get user
   *
   * @param email User email
   * @return User with data
   */
  User getUser(String email);

  /**
   * Add user
   *
   * @param user User
   * @return The id of the inserted user, but if the insert fail return -1
   */
  long addUser(User user);

  /**
   * Update user
   *
   * @param user User
   * @param email User email to updated
   * @return true if update user, false if the update fail.
   */
  boolean updateUser(String email, User user);

  /**
   * If exist the user update it or if not exist create it
   *
   * @param user User
   * @return true if create or update user, false if the creation fail.
   */
  boolean updateOrCreateUser(User user);

  /**
   * Delete the current user and all the information related with that user.
   *
   * @return If the delete was successful.
   */
  boolean deleteUsers();
}