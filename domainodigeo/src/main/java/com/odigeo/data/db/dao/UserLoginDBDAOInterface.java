package com.odigeo.data.db.dao;

import com.odigeo.data.entity.userData.UserLogin;

public interface UserLoginDBDAOInterface {

  //TABLE
  String TABLE_NAME = "user_login";

  //FIELDS
  String USER_LOGIN_ID = "user_login_id";
  String PASSWORD = "password";
  String ACCESS_TOKEN = "access_token";
  String EXPIRATION_TOKEN_DATE = "expiration_token_date";
  String HASH_CODE = "hash_code";
  String ACTIVATION_CODE = "activation_code";
  String ACTIVATION_CODE_DATE = "activation_code_date";
  String LOGIN_ATTEMPT_FAILED = "login_attempt_failed";
  String LAST_LOGIN_DATE = "last_login_date";
  String REFRESH_TOKEN = "refresh_token";
  String USER_ID = "user_id"; //relation with local id not remote

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get user login
   *
   * @param userLoginId User login id
   * @return UserLogin with id {@param userLoginId}
   */
  UserLogin getUserLogin(long userLoginId);

  /**
   * Insert user login
   *
   * @param userLogin User login to insert
   * @return The id of the inserted user login, but if the insert fail return -1
   */
  long addUserLogin(UserLogin userLogin);
}