package com.odigeo.data.db.helper;

import com.odigeo.data.entity.geo.City;

public interface CitiesHandlerInterface {
  City getCity(String iata);

  void storeCity(City city);
}
