package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.Booking;
import java.util.List;

public interface BookingDBDAOInterface {

  //TABLE
  String TABLE_NAME = "booking";

  //FIELDS
  String ID = "id";
  String BOOKING_ID = "booking_id";
  String ARRIVAL_AIRPORT_CODE = "arrival_airport_code";
  String ARRIVAL_LAST_LEG = "arrival_last_leg";
  String BOOKING_STATUS = "booking_status";
  String CURRENCY = "currency";
  String DEPARTURE_FIRST_LEG = "departure_first_leg";
  String LOCALE = "locale";
  String MARKET = "market";
  String PRICE = "price";
  String SORT_CRITERIA = "sort_criteria";
  String TIMESTAMP = "timestamp";
  String TOTAL = "total";
  String TRIP_TYPE = "trip_type";
  String MULTI_DESTINY_INDEX = "multi_destiny_index";
  String TRAVEL_COMPANION_NOTIFICATION = "travel_companion_notification";
  String SOUND_OF_DATA = "sound_of_data";
  String ENRICHED_BOOKING = "enriched_booking";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  //FILTERS
  String FILTER_BY_BOOKING_ID = BOOKING_ID + "=?";
  String FILTER_BY_STATUS = BOOKING_STATUS + " IN(?,?)";

  /**
   * Get user booking
   *
   * @param bookingId User booking id
   * @return Booking with id {@param BookingId}
   */
  Booking getBooking(long bookingId);

  List<Booking> getNextBookingsByDate(long dateInMiliseconds);

  /**
   * Get all user booking
   *
   * @return All bookings
   */
  List<Booking> getAllBookings();

  List<Long> getAllBookingIds();

  List<Booking> getUpcomingBookings();

  List<Booking> getPastBookings();

  /**
   * Insert user booking
   *
   * @param booking User booking to insert
   * @return The id of the inserted user booking, but if the insert fail return -1
   */
  long addBooking(Booking booking);

  /**
   * Delete user booking
   *
   * @param bookingId User booking id
   * @return TRUE if delete is success
   */
  boolean deleteBooking(long bookingId);

  /**
   * Update booking status
   *
   * @param bookingId Booking id
   * @param status New Booking status
   * @return The numbers affected rows.
   */
  long updateStatus(long bookingId, String status);

  /**
   * Change travel companion booking notifications
   *
   * @param bookingId Booking id
   * @param status Booking notifications status
   * @return True if is modify
   */
  boolean changeBookingNotifications(long bookingId, boolean status);

  /**
   * Change enroched booking for MTT notifications
   *
   * @param bookingId Booking id
   * @param enriched Booking enriched status
   * @return True if is modify
   */
  boolean changeEnrichedBooking(long bookingId, boolean enriched);

  int getNextMultiDestinyIndex(String tripType);

  long getLatestPendingBookingAddedTime();

  List<Booking> getPendingBookings();

  void initTransaction();

  void endTransaction();

  void setTransactionSuccessful();

  void closeDatabase();

  int removeBookings();
}