package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.Carrier;
import java.util.ArrayList;

public interface CarrierDBDAOInterface {

  //TABLE
  String TABLE_NAME = "carrier";

  //FIELDS
  String CODE = "code";
  String NAME = "name";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get carrier
   *
   * @param carrierId Carrier id
   * @return Carrier with id {@param CarrierId}
   */
  Carrier getCarrier(long carrierId);

  /**
   * Get carrier with code {@param code}
   *
   * @param code Carrier code
   * @return Carrier with code {@param code}
   */
  Carrier getCarrierWithCode(String code);

  /**
   * Insert carrier
   *
   * @param carrier Carrier to insert
   * @return The id of the inserted carrier, but if the insert fail return -1
   */
  long addCarrier(Carrier carrier);

  /**
   * Insert carrier list
   *
   * @param carriers Carrier list to insert
   * @return The id list of the inserted carrier, but if the insert fail return -1
   */
  ArrayList<Long> addCarriers(ArrayList<Carrier> carriers);

  /**
   * delete all Carriers
   *
   * @return rows affected
   */
  long deleteCarrier();
}
