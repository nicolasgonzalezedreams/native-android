package com.odigeo.data.db.dao;

import com.odigeo.data.entity.userData.UserTraveller;
import java.util.List;

public interface UserTravellerDBDAOInterface {

  //TABLE
  String TABLE_NAME = "user_traveller";

  //FIELDS
  String USER_TRAVELLER_ID = "user_traveller_id";
  String BUYER = "buyer";
  String EMAIL = "email";
  String TYPE_OF_TRAVELLER = "type_of_traveller";
  String MEAL_TYPE = "meal_type";
  String USER_ID = "user_id"; //relation with local id not remote

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get user traveller
   *
   * @param userTravellerId User traveller id
   * @return UserTraveller with id {@param userTravellerId}
   */
  UserTraveller getUserTraveller(long userTravellerId);

  /**
   * Get user traveller
   *
   * @param userTravellerId User traveller id
   * @return UserTraveller with id {@param userTravellerId}
   */
  UserTraveller getUserTravellerByUserTravellerId(long userTravellerId, String column);

  /**
   * Insert user traveller
   *
   * @param userTraveller User traveller to insert
   * @return The id of the inserted user traveller, but if the insert fail return -1
   */
  long addUserTraveller(UserTraveller userTraveller);

  /**
   * Update user traveller
   *
   * @param userTraveller User traveller to update
   * @return The UserTravellerId of the inserted user traveller, but if the update fail return 0
   */
  long updateUserTraveller(UserTraveller userTraveller);

  /**
   * Update user traveller with local id
   *
   * @param userTraveller User traveller to update
   * @return The id of the inserted user traveller, but if the update fail return 0
   */
  long updateUserTravellerByLocalId(UserTraveller userTraveller);

  /**
   * If exist the user traveller update it or if not exist create it
   *
   * @param userTraveller UserTraveller
   * @return true if create or update user traveller, false if the creation fail.
   */
  boolean updateOrCreateUserTraveller(UserTraveller userTraveller);

  /**
   * Get the list of all the user travellers
   *
   * @return List with all the user travellers
   */
  List<UserTraveller> getAllUserTravellers();

  /**
   * Get the lis tof all user travellers of the given type
   *
   * @param typeOfTraveller User traveller type
   * @return List of all user traveller of the type {@param typeOfTraveller}
   */
  List<UserTraveller> getAllUserTravellerByType(UserTraveller.TypeOfTraveller typeOfTraveller);

  /**
   * Delete user traveller
   *
   * @param userTravellerId user Traveller Id of UserTraveller to delete
   * @return true if delete user traveller, false if the creation fail.
   */
  boolean deleteUserTraveller(long userTravellerId);

  /**
   * Get the list of all user travellers with just the id and the traveller type
   *
   * @return List of all user traveller with just the values of id and traveller type
   */
  List<UserTraveller> getUserTravellerForListing();
}