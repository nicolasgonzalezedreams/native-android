package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.LastOriginDestination;

public interface LastOriginDestinationDBDAOInterface extends LocationDBDAOInterface {

  //TABLE
  String TABLE_NAME = "last_origin_destination";

  //FIELDS
  String IS_ORIGIN = "is_origin";
  String TIMESTAMP = "timestamp";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get last origin destinations
   *
   * @param lastOriginDestinationId last origin destination id
   * @return LastOriginDestination with id {@param lastOriginDestinationId}
   */
  LastOriginDestination getLastOriginDestination(long lastOriginDestinationId);

  /**
   * Insert last origin destinations
   *
   * @param lastOriginDestination last origin destination to insert
   * @return The id of the inserted last origin destination, but if the insert fail return -1
   */
  long addLastOriginDestination(LastOriginDestination lastOriginDestination);
}
