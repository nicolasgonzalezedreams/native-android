package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.Segment;
import java.util.List;

public interface SegmentDBDAOInterface {

  //TABLE
  String TABLE_NAME = "segment";

  //FIELDS
  String DURATION = "duration";
  String SEATS = "seats";
  String MAIN_CARRIER_ID = "main_carrier_id";
  String BOOKING_ID = "booking_id";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  //FILTERS
  String FILTER_BY_BOOKING_ID = BOOKING_ID + "=?";

  /**
   * Get segment
   *
   * @param segmentId Segment id
   * @return Segment with id {@param SegmentId}
   */
  Segment getSegment(long segmentId);

  /**
   * Get segment list
   *
   * @param bookingId Booking id
   * @return Segment with id {@param bookingId}
   */
  List<Segment> getSegments(long bookingId);

  /**
   * Insert segment
   *
   * @param segment Segment to insert
   * @return The id of the inserted segment, but if the insert fail return -1
   */
  long addSegment(Segment segment);

  /**
   * Insert segment list
   *
   * @param segments Segment list to insert
   * @return The id list of the inserted segment, but if the insert fail return -1
   */
  List<Long> addSegments(List<Segment> segments);

  boolean deleteSegments(long bookingId);

  /**
   * Delete all Segments
   *
   * @return rows affected
   */
  long deleteAllSegments();
}
