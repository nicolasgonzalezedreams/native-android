package com.odigeo.data.db.dao;

import com.odigeo.data.entity.userData.UserAirportPreferences;

public interface UserAirportPreferencesDBDAOInterface {

  //TABLE
  String TABLE_NAME = "user_airport_preferences";

  //FIELDS
  String USER_AIRPORT_PREFERENCES_ID = "user_airport_preferences_id";
  String AIRPORT = "airport";
  String USER_ID = "user_id"; //relation with local id not remote

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get user airline preferences
   *
   * @param userAirportPreferencesId User airport preferences id
   * @return User airport preferences with id {@param userAirportPreferencesId}
   */
  UserAirportPreferences getUserAirportPreferences(long userAirportPreferencesId);

  /**
   * Insert user airline preferences
   *
   * @param userAirportPreferences User airport preferences id
   * @return The id of the inserted user airport preferences, but if the insert fail return -1
   */
  long addUserAirportPreferences(UserAirportPreferences userAirportPreferences);
}