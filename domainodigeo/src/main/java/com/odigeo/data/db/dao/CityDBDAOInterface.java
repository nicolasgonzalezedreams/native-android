package com.odigeo.data.db.dao;

import com.odigeo.data.entity.geo.City;

/**
 * Created by daniel.morales on 12/5/17.
 */

public interface CityDBDAOInterface {

  String TABLE_NAME = "location";

  String IATA_CODE = "iata_code";
  String CITY_NAME = "city_name";
  String NAME = "name";
  String COUNTRY_CODE = "country_code";
  String COUNTRY_NAME = "country_name";
  String GEONODE_ID = "geonode_id";
  String LATITUDE = "latitude";
  String LONGITUDE = "longitude";
  String TYPE = "type";

  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  boolean addCity(City city);

  City getCityByIATA(String iataCode);
}
