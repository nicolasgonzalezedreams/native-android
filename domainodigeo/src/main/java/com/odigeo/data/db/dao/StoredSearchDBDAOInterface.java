package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.StoredSearch;
import java.util.List;

public interface StoredSearchDBDAOInterface {

  //TABLE
  String TABLE_NAME = "stored_search";

  //FIELDS
  String STORED_SEARCH_ID = "stored_search_id";
  String NUM_ADULTS = "adults";
  String NUM_CHILDREN = "children";
  String NUM_INFANTS = "infants";
  String IS_DIRECT_FLIGHT = "is_direct_flight";
  String CABIN_CLASS = "cabin_class";
  String TRIP_TYPE = "trip_type";
  String IS_SYNCHRONIZED = "is_synchronized";
  String CREATION_DATE = "creation_date";
  String USER_ID = "user_id";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get stored search
   *
   * @param storedSearchId stored search id
   * @return StoredSearch with id {@param storedSearchId}
   */
  StoredSearch getStoredSearch(long storedSearchId);

  /**
   * Insert stored search
   *
   * @param storedSearch stored search to insert
   * @return The id of the inserted stored search, but if the insert fail return -1
   */
  long addStoredSearch(StoredSearch storedSearch);

  /**
   * Update stored search
   *
   * @param storedSearch stored search to update
   * @return The id of the inserted stored search, but if the update fail return -1
   */
  boolean updateStoredSearch(StoredSearch storedSearch);

  /**
   * Get all stored searches. Synchronized and not synchronized.
   */
  List<StoredSearch> getAllStoredSearches();

  /**
   * Get local stored searches (not synchronized ones).
   */
  List<StoredSearch> getLocalStoredSearches();

  /**
   * Get synchronized stored searches. The ones that came from MSL.
   */
  List<StoredSearch> getSynchronizedStoredSearches();

  /**
   * Remove all stored searches. Local and synchronized.
   */
  boolean removeAllStoredSearches();

  boolean removeMultiTripSearches();

  boolean removeNonMultiTripSearches();

  /*
  * Remove a StoredSearch
  *  @param storedSearch stored search to remove
  * */
  boolean removeStoredSearch(StoredSearch storedSearch);
}
