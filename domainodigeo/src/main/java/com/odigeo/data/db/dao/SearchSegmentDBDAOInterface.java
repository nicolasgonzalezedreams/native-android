package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.SearchSegment;
import java.util.List;

public interface SearchSegmentDBDAOInterface {

  //TABLE
  String TABLE_NAME = "search_segment";

  //FIELDS
  String SEARCH_SEGMENT_ID = "search_segment_id";
  String ORIGIN_IATA_CODE = "origin_iata_code";
  String DESTINATION_IATA_CODE = "destination_iata_code";
  String DEPARTURE_DATE = "departure_date";
  String SEGMENT_ORDER = "segment_order";
  String PARENT_SEARCH_ID = "parent_search_id";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get search segment
   *
   * @param searchSegmentId search segment id
   * @return SearchSegment with id {@param searchSegmentId}
   */
  SearchSegment getSearchSegment(long searchSegmentId);

  /**
   * Insert search segments
   *
   * @param searchSegment search segment to insert
   * @return if the search segment was added correctly.
   */
  boolean addSearchSegment(SearchSegment searchSegment);

  /**
   * Get search segments list for a stored search
   *
   * @param storedSearchId stored search id of the parent search.
   * @return the list of the search segments of the given stored search.
   */
  List<SearchSegment> getSearchSegmentList(long storedSearchId);

  /**
   * Updates search segment in DB.
   *
   * @param searchSegmentId search segment id of the segment to change.
   * @param searchSegment search segment with the new info.
   * @return if the update was successful
   */
  boolean updateSearchSegment(long searchSegmentId, SearchSegment searchSegment);

  /**
   * Deletes the segment from the DB.
   *
   * @param searchSegmentId segment id of the segment to be deleted.
   */
  long deleteSearchSegment(long searchSegmentId);

  /**
   * @param parentId StoredSearch id that these searchSegments belong to
   */
  long deleteSearchSegmentFromParent(long parentId);

  /**
   * Remove all searches from DB.
   */
  boolean removeAllSearchSegments();
}
