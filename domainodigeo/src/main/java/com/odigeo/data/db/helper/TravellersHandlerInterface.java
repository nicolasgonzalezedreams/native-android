package com.odigeo.data.db.helper;

import com.odigeo.data.entity.userData.User;
import com.odigeo.data.entity.userData.UserAddress;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.List;

/**
 * Created by carlos.navarrete on 26/09/15.
 */
public interface TravellersHandlerInterface {

  /**
   * This method only is valid when the app has got a logged user
   *
   * @return Current logged User
   */
  User getCurrentUser();

  User getCurrentUser(boolean areDefault);

  List<UserTraveller> getUserTravellersByType(UserTraveller.TypeOfTraveller travellerType);

  List<UserTraveller> getSimpleUserTravellerList();

  UserTraveller getFullUserTraveller(long userTravellerId);

  UserTraveller getFullUserTraveller(long userTravellerId, boolean isDefault);

  List<UserTraveller> getFullUserTravellerList();

  List<UserTraveller> getFullUserTravellerList(List<UserTraveller> userTravellerList);

  UserAddress getUserAddressByUserProfileId(long userProfileId);

  List<UserIdentification> getUserIdentifications(long userProfileId);

  UserProfile getFullUserProfile(UserProfile userProfile);

  UserProfile getFullUserProfile(UserProfile userProfile, boolean isDefault);
}
