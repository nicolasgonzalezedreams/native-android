package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.Buyer;

public interface BuyerDBDAOInterface extends PersonalBookingDBDAOInterface {

  //TABLE
  String TABLE_NAME = "buyer";

  //FIELDS
  String NAME = "name";
  String LAST_NAMES = "last_names";
  String ADDRESS = "address";
  String CITY_NAME = "city_name";
  String COUNTRY = "country";
  String EMAIL = "email";
  String STATE_NAME = "state_name";
  String ZIP_CODE = "zip_code";
  String PHONE_NUMBER = "phone";
  String PHONE_COUNTRY_CODE = "phone_country_code";
  String ALTERNATIVE_PHONE = "alternative_phone";
  String CPF = "cpf";
  String IDENTIFICATION = "identification";
  String IDENTIFICATION_TYPE = "identification_type";
  String BOOKING_ID = "booking_id";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  //FILTERS
  String FILTER_BY_BOOKING_ID = BOOKING_ID + "=?";

  /**
   * Get buyer
   *
   * @param bookingId Buyer id
   * @return Buyer with id {@param bookingId}
   */
  Buyer getBuyer(long bookingId);

  /**
   * Insert buyer
   *
   * @param buyer Buyer to insert
   * @return The id of the inserted buyer, but if the insert fail return -1
   */
  long addBuyer(Buyer buyer);

  /**
   * Delete buyer
   *
   * @param bookingId bookingId of Buyer to delete
   * @return true if it was deleted
   */
  boolean deleteBuyer(long bookingId);

  /**
   * Delete all rows
   *
   * @return number of rows affected
   */
  long deleteBuyers();
}
