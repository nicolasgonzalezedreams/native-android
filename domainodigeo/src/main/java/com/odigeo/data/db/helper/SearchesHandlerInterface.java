package com.odigeo.data.db.helper;

import com.odigeo.data.entity.booking.StoredSearch;
import java.util.List;

/**
 * Created by matias.dirusso on 27/7/2016.
 */
public interface SearchesHandlerInterface {
  int MAX_SEARCHES = 10;

  List<StoredSearch> completeStoredSearchesData(List<StoredSearch> storedSearches);

  StoredSearch completeSearch(StoredSearch storedSearch);

  List<StoredSearch> getCompleteSearchesFromDB();

  void saveStoredSearch(StoredSearch storedSearch);

  void removeSynchronizedStoredSearches();

  void updateStoredSearch(StoredSearch storedSearch);

  void removeAllSearchesLocally(StoredSearch.TripType tripType);
}
