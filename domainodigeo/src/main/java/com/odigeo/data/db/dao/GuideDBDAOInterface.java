package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.Guide;
import java.util.Map;

public interface GuideDBDAOInterface {

  //TABLE
  String TABLE_NAME = "guide";

  //FIELDS
  String GEO_NODE_ID = "geo_node_id";
  String URL = "url";
  String LANGUAGE = "language";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get guide
   *
   * @param guideId Guide id
   * @return Guide with id {@param guideId}
   */
  Guide getGuide(long guideId);

  /**
   * Get guide
   *
   * @param geoNodeId Guide geoNodeId
   * @return Guide with geoNodeId {@param geoNodeId}
   */
  Guide getGuideByGeoNodeId(long geoNodeId);

  /**
   * Insert guide
   *
   * @param guide Guide to insert
   * @return The id of the inserted guide, but if the insert fail return -1
   */
  long addGuide(Guide guide);

  /**
   * Get all Guides on a Map with key GeoNodeId
   *
   * @return a Map<Long, Guide> where the key is GeoNodeId
   */
  Map<Long, Guide> getAllGuides();

  /**
   * Delete guide with provided GeoNodeId
   *
   * @return true if the guide was deleted, false if not
   */
  boolean deleteGuide(long geoNodeId);

  /**
   * Delete all Guides that are on the provided Map from database
   *
   * @return a Map<Long, Guide> with Key GeoNodeID with the guides that were not deleted.
   * if all guides were deleted the Map will be empty.
   */
  Map<Long, Guide> deleteGuides(Map<Long, Guide> guidesToDelete);
}
