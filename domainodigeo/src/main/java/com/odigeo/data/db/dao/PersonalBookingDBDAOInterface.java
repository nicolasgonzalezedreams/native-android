package com.odigeo.data.db.dao;

//This is just an interface to extend two tables and methods not repeated. There isn't a real dao.
public interface PersonalBookingDBDAOInterface {

  //FIELDS
  String DATE_OF_BIRTH = "date_of_birth";
  String IDENTIFICATION = "identification";
  String IDENTIFICATION_TYPE = "identification_type";
  String LASTNAME = "lastname";
  String NAME = "name";
}
