package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.MSLLocation;

public interface MSLLocationDBDAOInterface extends LocationDBDAOInterface {

  //TABLE
  String TABLE_NAME = "msl_location";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get msl location
   *
   * @param mslLocationId msl location id
   * @return MSLLocation with id {@param mslLocation}
   */
  MSLLocation getMSLLocation(long mslLocationId);

  /**
   * Insert msl location
   *
   * @param mslLocation msl location to insert
   * @return The id of the inserted last origin destination, but if the insert fail return -1
   */
  long addMSLLocation(MSLLocation mslLocation);
}
