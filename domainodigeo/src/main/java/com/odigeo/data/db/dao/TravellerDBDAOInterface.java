package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.Traveller;
import java.util.List;

public interface TravellerDBDAOInterface extends PersonalBookingDBDAOInterface {

  //TABLE
  String TABLE_NAME = "traveller";

  //FIELDS
  String COUNTRY_CODE = "country_code";
  String IDENTIFICATION_EXPIRATION_DATE = "identification_expiration_date";
  String IDENTIFICATION_ISSUE_COUNTRY_CODE = "identification_issue_country_code";
  String LOCALITY_CODE = "locality_code";
  String MEAL = "meal";
  String NATIONALITY = "nationality";
  String TITLE = "title";
  String TRAVELLER_GENDER = "traveller_gender";
  String TRAVELLER_TYPE = "traveller_type";
  String BOOKING_ID = "booking_id";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  //FILTERS
  String FILTER_BY_BOOKING_ID = BOOKING_ID + "=?";

  /**
   * Get traveller
   *
   * @param travellerId Traveller id
   * @return Traveller with id {@param travellerId}
   */
  Traveller getTraveller(long travellerId);

  /**
   * Get traveller list
   *
   * @param bookingId Booking id
   * @return Traveller with id {@param bookingId}
   */
  List<Traveller> getTravellers(long bookingId);

  /**
   * Insert traveller
   *
   * @param traveller Traveller to insert
   * @return The id of the inserted traveller, but if the insert fail return -1
   */
  long addTraveller(Traveller traveller);

  /**
   * Insert traveller list
   *
   * @param traveller Travellers list to insert
   * @return The id of the inserted traveller, but if the insert fail return -1
   */
  List<Long> addTravellers(List<Traveller> traveller);

  /**
   * Delete traveller
   *
   * @param bookingId bookingId of Traveller to delete
   * @return True if the travellers were deleted
   */
  boolean deleteTraveller(long bookingId);

  /**
   * Delete all travellers
   *
   * @return rows affected
   */

  long deleteTravellers();
}
