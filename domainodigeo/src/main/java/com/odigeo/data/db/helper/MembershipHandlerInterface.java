package com.odigeo.data.db.helper;

import com.odigeo.data.entity.userData.Membership;

import java.util.List;

public interface MembershipHandlerInterface {
  boolean addMembership(Membership membership);

  Membership getMembershipFromMarket(String market);

  boolean clearMembership();

  List<Membership> getAllMembershipsOfUser();

  boolean deleteMembership(Membership membership);

  void createTableIfNotExists();
}
