package com.odigeo.data.db.dao;

import com.odigeo.data.entity.userData.UserDestinationPreferences;

public interface UserDestinationPreferencesDBDAOInterface {

  //TABLE
  String TABLE_NAME = "user_destination_preferences";

  //FIELDS
  String USER_DESTINATION_PREFERENCES_ID = "user_destination_preferences_id";
  String DESTINATION = "destination";
  String USER_ID = "user_id"; //relation with local id not remote

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get user airline preferences
   *
   * @param userDestinationPreferencesId User destination preferences id
   * @return User destination preferences with id {@param userDestinationPreferencesId}
   */
  UserDestinationPreferences getUserDestinationPreferences(long userDestinationPreferencesId);

  /**
   * Insert user airline preferences
   *
   * @param userDestinationPreferences User destination preferences id
   * @return The id of the inserted user destination preferences, but if the insert fail return -1
   */
  long addUserDestinationPreferences(UserDestinationPreferences userDestinationPreferences);
}