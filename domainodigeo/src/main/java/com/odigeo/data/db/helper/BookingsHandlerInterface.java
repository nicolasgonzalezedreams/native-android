package com.odigeo.data.db.helper;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.interactors.provider.MyTripsProvider;
import java.util.List;

/**
 * Created by javier.rebollo on 7/9/15.
 */
public interface BookingsHandlerInterface {

  boolean saveBooking(Booking booking);

  void completeBookingData(MyTripsProvider myTripsProvider);

  void completeBookingData(List<Booking> bookings);

  void completeBooking(Booking booking);

  void updateBookingFlightStats(Booking booking);

  void updateItineraryBooking(Booking booking);

  void deleteAllBookingData();
}
