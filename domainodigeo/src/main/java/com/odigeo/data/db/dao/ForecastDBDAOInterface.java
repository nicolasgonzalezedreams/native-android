package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.Forecast;

public interface ForecastDBDAOInterface {

  //TABLE
  String TABLE_NAME = "forecast";

  //FIELDS
  String FORECAST_ID = "forecast_id";
  String FORECAST_DATE = "forecast_date";
  String FORECAST_DESCRIPTION = "forecast_description";
  String FORECAST_ICON = "forecast_icon";
  String FORECAST_UPDATED_DATE = "forecast_updated_date";
  String TEMPERATURE = "temperature";
  String LOCATION_ID = "location_id";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get forecast
   *
   * @param forecastId Forecast id
   * @return Forecast with id {@param ForecastId}
   */
  Forecast getForecast(long forecastId);

  /**
   * Insert forecast
   *
   * @param forecast Forecast to insert
   * @return The id of the inserted forecast, but if the insert fail return -1
   */
  long addForecast(Forecast forecast);
}
