package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.LocationBooking;
import java.util.List;

public interface LocationBookingDBDAOInterface extends LocationDBDAOInterface {

  //TABLE
  String TABLE_NAME = "location_bookings";

  //FIELDS
  String COUNTRY_CODE = "country_code";
  String CURRENCY_CODE = "currency_code";
  String CURRENCY_RATE = "currency_rate";
  String CURRENCY_UPDATED_DATE = "currency_updated_date";
  String TIMEZONE = "timezone";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  //FILTERS
  String FILTER_BY_GEO_NODE_ID = GEO_NODE_ID + "=?";

  /**
   * Get location bookings
   *
   * @param locationBookingId location booking id
   * @return LocationBooking with id {@param locationBookingId}
   */
  LocationBooking getLocationBooking(long locationBookingId);

  /**
   * Insert location bookings
   *
   * @param locationBooking location booking to insert
   * @return The id of the inserted location booking, but if the insert fail return -1
   */
  long addLocationBooking(LocationBooking locationBooking);

  /**
   * Insert location bookings list
   *
   * @param locationBookings location booking list to insert
   * @return The ids of the inserted location booking, but if the insert fail return -1
   */
  List<Long> addLocationBookings(List<LocationBooking> locationBookings);

  /**
   * Delete all Locations
   *
   * @return rows affected
   */
  long deleteLocations();
}
