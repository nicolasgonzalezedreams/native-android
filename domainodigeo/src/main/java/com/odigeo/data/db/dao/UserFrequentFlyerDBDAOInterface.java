package com.odigeo.data.db.dao;

import com.odigeo.data.entity.userData.UserFrequentFlyer;
import java.util.List;

public interface UserFrequentFlyerDBDAOInterface {

  //TABLE
  String TABLE_NAME = "user_frequent_flyer";

  //FIELDS
  String USER_FREQUENT_FLYER_ID = "user_frequent_flyer_id";
  String AIRLINE_CODE = "airline_code";
  String FREQUENT_FLYER_NUMBER = "frequent_flyer_number";
  String USER_TRAVELLER_ID = "user_traveller_id"; //relation with local id not remote

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get user frequent flyer
   *
   * @param userFrequentFlyerId User frequent flyer id
   * @return {@link UserFrequentFlyer} with id {@param userFrequentFlyerId}
   */
  UserFrequentFlyer getUserFrequentFlyer(long userFrequentFlyerId);

  /**
   * Gets a list of user frequent flyer
   *
   * @param userTravellerId User frequent flyer id
   * @return {@link UserFrequentFlyer} with id {@param userFrequentFlyerId}
   */
  List<UserFrequentFlyer> getUserFrequentFlyerList(long userTravellerId);

  /**
   * Insert user frequent flyer
   *
   * @param userFrequentFlyer User frequent flyer to insert
   * @return The id of the inserted user frequent flyer, but if the insert fail return -1
   */
  long addUserFrequentFlyer(UserFrequentFlyer userFrequentFlyer);

  /**
   * Update User Frequent Flyer
   *
   * @param userFrequentFlyer UserFrequentFlyer
   * @return true if update userFrequentFlyer, false if the update fail.
   */
  boolean updateUserFrequentFlyer(long userFrequentFlyerId, UserFrequentFlyer userFrequentFlyer);

  /**
   * Update User Frequent Flyer
   *
   * @param userFrequentFlyer UserFrequentFlyer to update
   * @return number of rows affected
   */
  long updateUserFrequentFlyerByLocalId(UserFrequentFlyer userFrequentFlyer);

  /**
   * If exist the userIdentification update it or if not exist create it
   *
   * @param userFrequentFlyer UserFrequentFlyer
   * @return true if create or update userFrequentFlyer, false if the creation fail.
   */
  boolean updateOrCreateUserFrequentFlyer(UserFrequentFlyer userFrequentFlyer);

  /**
   * Deletes an existing {@link UserFrequentFlyer} from the database.
   *
   * @param userFrequentFlyerId Element Id to be deleted.
   * @return Number of rows affected.
   */
  long deleteUserFrequentFlyer(long userFrequentFlyerId);
}