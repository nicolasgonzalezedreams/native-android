package com.odigeo.data.db.dao;

import com.odigeo.data.entity.userData.UserAddress;

public interface UserAddressDBDAOInterface {

  //TABLE
  String TABLE_NAME = "user_address";

  //FIELDS
  String USER_ADDRESS_ID = "user_address_id";
  String ADDRESS = "address";
  String ADDRESS_TYPE = "address_type";
  String CITY = "city";
  String COUNTRY = "country";
  String STATE = "state";
  String POSTAL_CODE = "postal_code";
  String IS_PRIMARY = "is_primary";
  String ALIAS = "alias";
  String USER_PROFILE_ID = "user_profile_id";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get user address
   *
   * @param userAddressId User address id
   * @return UserAddress with id {@param userAddressId}
   */
  UserAddress getUserAddress(long userAddressId);

  /**
   * Get user address
   *
   * @param userProfileId User profile id
   * @return UserAddress of the UserProfile with {@param userProfileId}
   */
  UserAddress getUserAddressByUserProfileId(long userProfileId);

  /**
   * Insert user address
   *
   * @param userAddress User address to insert
   * @return The id of the inserted user address, but if the insert fail return -1
   */
  long addUserAddress(UserAddress userAddress);

  /**
   * Update User Address
   *
   * @param userAddress UserAddress
   * @return true if update userAddress, false if the update fail.
   */
  boolean updateUserAddress(long userAddressId, UserAddress userAddress);

  /**
   * Update User Address
   *
   * @param userAddress UserAddress with new data
   * @return rows affected if update userAddress, 0 if the update fail.
   */
  long updateUserAddressByLocalId(UserAddress userAddress);

  /**
   * If exist the userAddress update it or if not exist create it
   *
   * @param userAddress UserAddress
   * @return true if create or update userAddress, false if the creation fail.
   */
  boolean updateOrCreateUserAddress(UserAddress userAddress);

  boolean deleteUserAddress(long userAddressId);
}