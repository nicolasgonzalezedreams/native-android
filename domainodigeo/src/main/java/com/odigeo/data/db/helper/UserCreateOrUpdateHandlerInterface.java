package com.odigeo.data.db.helper;

import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.entity.userData.UserTraveller;

/**
 * Created by matias.dirusso on 9/25/2015.
 */
public interface UserCreateOrUpdateHandlerInterface {

  boolean addOrCreateUserAndAllComponents(User user);

  boolean updateOrCreateAUserTravellerAndAllComponentsInDB(UserTraveller userTraveller);

  /**
   * Save a UserTraveller and all Entities inside it, when in the app does not have a user logged
   * on and make the relations between this entities
   *
   * @param userTraveller A full UserTraveller to save
   * @return id in DB, or -1 on error
   */
  long saveAUserTravellerAndAllComponentsInDBWithoutUserLogged(UserTraveller userTraveller);

  boolean deleteUserTravellerCompletely(long fullUserTravellerId);

  /**
   * Delete a UserTraveller in DB and all relations with this UserTraveller
   *
   * @param fullUserTraveller UserTraveller with all data
   * @return TRUE if delete was success
   */
  boolean deleteUserTravellerCompletely(UserTraveller fullUserTraveller);

  boolean updateOrCreateStoredSearchAndAllComponentsInDB(StoredSearch storedSearch);
}
