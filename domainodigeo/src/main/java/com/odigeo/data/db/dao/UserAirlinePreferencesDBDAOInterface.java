package com.odigeo.data.db.dao;

import com.odigeo.data.entity.userData.UserAirlinePreferences;

public interface UserAirlinePreferencesDBDAOInterface {

  //TABLE
  String TABLE_NAME = "user_airline_preferences";

  //FIELDS
  String USER_AIRLINE_PREFERENCES_ID = "user_airline_preferences_id";
  String AIRLINE_CODE = "airline_code";
  String USER_ID = "user_id"; //relation with local id not remote

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get user airline preferences
   *
   * @param userAirlinePreferencesId User airline preferences id
   * @return User airline preferences with id {@param userAirlinePreferencesId}
   */
  UserAirlinePreferences getUserAirlinePreferences(long userAirlinePreferencesId);

  /**
   * Insert user airline preferences
   *
   * @param userAirlinePreferences User airline preferences id
   * @return The id of the inserted user airline preferences, but if the insert fail return -1
   */
  long addUserAirlinePreferences(UserAirlinePreferences userAirlinePreferences);
}