package com.odigeo.data.db.dao;

import com.odigeo.data.entity.userData.UserIdentification;
import java.util.List;

public interface UserIdentificationDBDAOInterface {

  //TABLE
  String TABLE_NAME = "user_identification";

  //FIELDS
  String USER_IDENTIFICATION_ID = "user_identification_id";
  String IDENTIFICATION_ID = "identification_ID";
  String IDENTIFICATION_COUNTRY_CODE = "identification_country_code";
  String IDENTIFICATION_EXPIRATION_DATE = "identification_expiration_date";
  String IDENTIFICATION_TYPE = "identification_type";
  String USER_PROFILE_ID = "user_profile_id"; //relation with local id not remote

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get user identification
   *
   * @param userIdentificationId User identification id
   * @return UserIdentification with id {@param userIdentificationId}
   */
  UserIdentification getUserIdentification(long userIdentificationId);

  /**
   * Insert user identification
   *
   * @param userIdentification User identification to insert
   * @return The id of the inserted user identification, but if the insert fail return -1
   */
  long addUserIdentification(UserIdentification userIdentification);

  /**
   * Update User Identification
   *
   * @param userIdentification UserIdentification
   * @return true if update userIdentification, false if the update fail.
   */
  boolean updateUserIdentification(long userIdentificationId,
      UserIdentification userIdentification);

  /**
   * Update User Identification
   *
   * @param userIdentification UserIdentification to update
   * @return number of rows affected for the update.
   */
  long updateUserIdentificationByLocalId(UserIdentification userIdentification);

  /**
   * If exist the userIdentification update it or if not exist create it
   *
   * @param userIdentification UserIdentification
   * @return true if create or update userIdentification, false if the creation fail.
   */
  boolean updateOrCreateUserIdentification(UserIdentification userIdentification);

  /**
   * If exist the UserProfile retrieves the whole identifications
   *
   * @param userProfileId the id of the user
   * @return list of UserIdentifications.
   */
  List<UserIdentification> getUserIdentifications(long userProfileId);

  /**
   * Delete a USerIdentification
   *
   * @param userIdentificationId to delete
   * @return TRUE if delete is success
   */
  boolean deleteUserIdentification(long userIdentificationId);
}