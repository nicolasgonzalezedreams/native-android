package com.odigeo.data.db.dao;

import com.odigeo.data.entity.userData.UserDevice;

public interface UserDeviceDBDAOInterface {

  //TABLE
  String TABLE_NAME = "user_device";

  //FIELDS
  String USER_DEVICE_ID = "user_device_id";
  String DEVICE_NAME = "device_name";
  String DEVICE_ID = "device_id";
  String IS_PRIMARY = "is_primary";
  String ALIAS = "alias";
  String USER_ID = "user_id"; //relation with local id not remote

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get user device
   *
   * @param userDeviceId User device id
   * @return UserDevice with id {@param userDeviceId}
   */
  UserDevice getUserDevice(long userDeviceId);

  /**
   * Insert user device
   *
   * @param userDevice User device to insert
   * @return The id of the inserted user device, but if the insert fail return -1
   */
  long addUserDevice(UserDevice userDevice);
}