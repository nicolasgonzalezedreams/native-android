package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.Insurance;
import java.util.List;

public interface InsuranceDBDAOInterface {

  //TABLE
  String TABLE_NAME = "insurance";

  //FIELDS
  String INSURANCE_ID = "insurance_id";
  String CONDITION_URL_PRIMARY = "condition_url_primary";
  String CONDITION_URL_SECUNDARY = "condition_url_secundary";
  String INSURANCE_DESCRIPTION = "insurance_description";
  String INSURANCE_TYPE = "insurance_type";
  String POLICY = "policy";
  String SELECTABLE = "selectable";
  String SUBTITLE = "subtitle";
  String TITLE = "title";
  String TOTAL = "total";
  String BOOKING_ID = "booking_id";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get insurance
   *
   * @param insuranceId Insurance id
   * @return Insurance with id {@param InsuranceId}
   */
  Insurance getInsurance(long insuranceId);

  /**
   * Get insurance list
   *
   * @param bookingId Booking id
   * @return Insurances with id {@param bookingId}
   */
  List<Insurance> getInsurances(long bookingId);

  /**
   * Insert insurance
   *
   * @param insurance Insurance to insert
   * @return The id of the inserted insurance, but if the insert fail return -1
   */
  long addInsurance(Insurance insurance);

  /**
   * Insert insurance list
   *
   * @param insurances Insurance list to insert
   * @return The id list of the inserted insurance, but if the insert fail return -1
   */
  List<Long> addInsurances(List<Insurance> insurances);

  boolean removeInsurance(long insuranceId);

  /**
   * Delete all insurances
   *
   * @return rows affected
   */

  long removeAllInsurances();
}
