package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.ItineraryBooking;
import java.util.ArrayList;

public interface ItineraryBookingDBDAOInterface {

  //TABLE
  String TABLE_NAME = "itinerary_bookings";

  //FIELDS
  String BOOKING_STATUS = "booking_status";
  String PNR = "pnr";
  String TIMESTAMP = "timestamp";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get itinerary bookings
   *
   * @param itineraryBookingId itinerary booking id
   * @return ItineraryBooking with id {@param itineraryBookingId}
   */
  ItineraryBooking getItineraryBooking(long itineraryBookingId);

  /**
   * Get itinerary bookings with pnr {@param pnr}
   *
   * @param pnr itinerary booking pnr
   * @return ItineraryBooking with id {@param pnr}
   */
  ItineraryBooking getItineraryBookingWithPNR(String pnr);

  /**
   * Insert itinerary bookings
   *
   * @param itineraryBooking itinerary booking to insert
   * @return The id of the inserted itinerary booking, but if the insert fail return -1
   */
  long addItineraryBooking(ItineraryBooking itineraryBooking);

  /**
   * Insert itinerary bookings list
   *
   * @param itineraryBookings itinerary booking list to insert
   * @return The id list of the inserted itinerary booking, but if the insert fail return -1
   */
  ArrayList<Long> addItineraryBookings(ArrayList<ItineraryBooking> itineraryBookings);

  /**
   * Update itinerary booking's PNR
   *
   * @param itineraryBookingID itinerary booking id to update
   * @param itineraryBooking itinerary booking with new values
   * @return The id list of the updated itinerary booking, but if the insert fail return -1
   */
  long updateItineraryBooking(String itineraryBookingID, ItineraryBooking itineraryBooking);

  /**
   * delete all itineraries
   *
   * @return rows affected
   */
  long deleteItineraries();
}
