package com.odigeo.data.db.dao;

import com.odigeo.data.entity.userData.UserProfile;

public interface UserProfileDBDAOInterface {

  //TABLE
  String TABLE_NAME = "user_profile";

  //FIELDS
  String USER_PROFILE_ID = "user_profile_id";
  String GENDER = "gender";
  String TITLE = "title";
  String NAME = "name";
  String MIDDLE_NAME = "middle_name";
  String FIRST_LAST_NAME = "first_last_name";
  String SECOND_LAST_NAME = "second_last_name";
  String BIRTHDATE = "birthdate";
  String PREFIX_PHONE_NUMBER = "prefix_phone_number";
  String PHONE_NUMBER = "phone_number";
  String PREFIX_ALTERNATE_PHONE_NUMBER = "prefix_alternate_phone_number";
  String ALTERNATE_PHONE_NUMBER = "alternate_phone_number";
  String MOBILE_PHONE_NUMBER = "mobile_phone_number";
  String NATIONALITY_COUNTRY_CODE = "nationality_country_code";
  String CPF = "cpf";
  //    String DEFAULT_PROFILE = "default_profile";
  String IS_DEFAULT = "is_default";
  String PHOTO = "photo";
  String USER_TRAVELLER_ID = "user_traveller_id"; //relation with local id not remote

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get user profile
   *
   * @param userProfileId User profile id
   * @return UserProfile with id {@param userProfileId}
   */
  UserProfile getUserProfile(long userProfileId);

  /**
   * Get user profile
   *
   * @param id User profile id
   * @param column where it will search for the given id
   * @return UserProfile with id {@param id}
   */
  UserProfile getUserProfile(long id, String column);

  /**
   * Insert user profile
   *
   * @param userProfile User profile to insert
   * @return The id of the inserted user profile, but if the insert fail return -1
   */
  long addUserProfile(UserProfile userProfile);

  /**
   * Update user profile
   *
   * @param userProfile User profile to update
   * @return The id of the inserted user profile, but if the update fail return 0
   */
  long updateUserProfileByLocalId(UserProfile userProfile);

  /**
   * Update user profile
   *
   * @param userProfile User profile to update
   * @return The userProfileId of the inserted user profile, but if the update fail return 0
   */
  long updateUserProfile(UserProfile userProfile);

  /**
   * If exist the user profile update it or if not exist create it
   *
   * @param userProfile UserProfile
   * @return true if create or update user profile, false if the creation fail.
   */
  boolean updateOrCreateUserProfile(UserProfile userProfile);

  boolean deleteUserProfile(long userProfileId);
}