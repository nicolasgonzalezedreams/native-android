package com.odigeo.data.db.helper;

import com.odigeo.data.db.listener.DatabaseUpdateListener;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.net.NetCountry;
import com.odigeo.data.net.listener.OnRequestDataListListener;
import java.util.List;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/02/16.
 */
public interface CountriesDbHelperInterface {

  void updateCountries(List<NetCountry> netCountries,
      DatabaseUpdateListener databaseUpdateListener);

  void getCountries(String languageIsoCode, boolean filterNullPhonePrefix,
      OnRequestDataListListener<Country> requestDataListListener);

  Country getCountryByCountryCode(String languageIsoCode, String countryCode);
}
