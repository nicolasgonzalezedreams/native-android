package com.odigeo.data.db.dao;

//This is just an interface to extend two tables and methods not repeated. There isn't a real dao.
public interface LocationDBDAOInterface {

  //FIELDS
  String CITY_IATA_CODE = "city_iata_code";
  String CITY_NAME = "city_name";
  String COUNTRY = "country";
  String GEO_NODE_ID = "geo_node_id";
  String LATITUDE = "latitude";
  String LOCATION_CODE = "location_code";
  String LOCATION_NAME = "location_name";
  String LOCATION_TYPE = "location_type";
  String LONGITUDE = "longitude";
  String MATCH_NAME = "match_name";
}
