package com.odigeo.data.db.listener;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 17/02/16.
 */
public interface DatabaseUpdateListener {
  void onUpdateFinish();
}
