package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.FlightStats;

/**
 * Created by ximenaperez1 on 1/12/16.
 */
public interface FlightStatsDBDAOInterface {

  //TABLE
  String TABLE_NAME = "flight_stats";

  //FIELDS
  String SECTION_ID = "section_id";
  String ARRIVAL_GATE = "arrival_gate";
  String ARRIVAL_TIME = "arrival_time";
  String ARRIVAL_TIME_DELAY = "arrival_time_delay";
  String ARRIVAL_TIME_TYPE = "arrival_time_type";
  String BAGGAGE_CLAIM = "baggage_claim";
  String DEPARTURE_GATE = "departure_gate";
  String DEPARTURE_TIME = "departure_time";
  String DEPARTURE_TIME_DELAY = "departure_time_delay";
  String DEPARTURE_TIME_TYPE = "departure_time_type";
  String DESTINATION_AIRPORT_NAME = "destination_airport_name";
  String DESTINATION_IATA_CODE = "destination_iata_code";
  String EVENT_RECEIVED = "event_received";
  String FLIGHT_STATUS = "flight_status";
  String GATE_CHANGED = "gate_changed";
  String OPERATING_VENDOR = "operating_vendor";
  String OPERATING_VENDOR_CODE = "operating_vendor_code";
  String OPERATING_VENDOR_LEG_NUMBER = "operating_vendor_leg_number";
  String UPDATED_ARRIVAL_TERMINAL = "updated_arrival_terminal";
  String UPDATED_DEPARTURE_TERMINAL = "updated_departure_terminal";
  String BOOKING_ID = "booking_id";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  /**
   * Get flight stats
   *
   * @param sectionId Flight Stats id
   * @return Flight stats with id {@param flightStatsId}
   */
  FlightStats getFlightStats(String sectionId, String bookingId);

  /**
   * Insert flight stats
   *
   * @param flightStats
   *     Flight stats to insert
   * @return The id of the inserted flight stats, but if the insert fails return -1
   */
  //long addFlightStats(FlightStats flightStats);

  /**
   * Update flight stats
   *
   * @param flightStats Flight stats to update
   * @return True if the flight stats was updated successfully
   */

  long addOrUpdateFlightStats(FlightStats flightStats, String bookingId);

  /**
   * delete all Flight Stats
   *
   * @return rows affected
   */
  long deleteFlightStats();
}
