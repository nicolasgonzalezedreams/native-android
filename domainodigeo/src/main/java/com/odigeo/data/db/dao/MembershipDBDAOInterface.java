package com.odigeo.data.db.dao;

import com.odigeo.data.entity.userData.Membership;

import java.util.List;

public interface MembershipDBDAOInterface {

  String TABLE_NAME = "membership";

  String MEMBER_ID = "member_id";
  String FIRST_NAME = "first_name";
  String LAST_NAMES = "last_names";
  String WEBSITE = "website";

  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  boolean addMembership(Membership membership);

  Membership getMembershipForMarket(String market);

  boolean clearMembership();

  List<Membership> getAllMembershipsOfUser();

  boolean deleteMembership(Membership membership);

  void createTableIfNotExists();
}
