package com.odigeo.data.db.dao;

import com.odigeo.data.entity.booking.Section;
import java.util.ArrayList;
import java.util.List;

public interface SectionDBDAOInterface {

  //TABLE
  String TABLE_NAME = "section";

  //FIELDS
  String SECTION_ID = "section_id";
  String AIRCRAFT = "aircraft";
  String ARRIVAL_DATE = "arrival_date";
  String ARRIVAL_TERMINAL = "arrival_terminal";
  String BAGGAGE_ALLOWANCE_QUANTITY = "baggage_allowance_quatity";
  String BAGGAGE_ALLOWANCE_TYPE = "baggage_allowance_type";
  String CABIN_CLASS = "cabin_class";
  String DEPARTURE_DATE = "departure_date";
  String DEPARTURE_TERMINAL = "departure_terminal";
  String DURATION = "duration";
  String FLIGHT_ID = "flight_id";
  String SECTION_TYPE = "section_type";
  String CARRIER_ID = "carrier_id";
  String FROM = "location_from";
  String ITINERARY_BOOKING_ID = "itinerary_booking_id";
  String SEGMENT_ID = "segment_id";
  String TO = "location_to";
  String FLIGHT_STATS_ID = "flight_stats_id";

  //DROP
  String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

  //FILTERS
  String FILTER_BY_SEGMENT_ID = SEGMENT_ID + "=?";

  /**
   * Get section
   *
   * @param sectionId Section id
   * @return Section with id {@param SectionId}
   */
  Section getSection(long sectionId);

  /**
   * Get section list
   *
   * @param segmentId Section id
   * @return Section with id {@param segmentId}
   */
  List<Section> getSections(long segmentId);

  /**
   * Insert section
   *
   * @param section Section to insert
   * @return The id of the inserted section, but if the insert fail return -1
   */
  long addSection(Section section);

  /**
   * Insert section list
   *
   * @param sections Section list to insert
   * @return The id list of the inserted section, but if the insert fail return -1
   */
  List<Long> addSections(ArrayList<Section> sections);

  long getIdSegmentFormEarlierSection(long date);

  /**
   * Delete section
   *
   * @param segmentId segment for Section to delete
   * @return true if it was deleted
   */
  boolean deleteSection(long segmentId);

  /**
   * delete all sections
   *
   * @return rows affected
   */
  long deleteSections();
}
