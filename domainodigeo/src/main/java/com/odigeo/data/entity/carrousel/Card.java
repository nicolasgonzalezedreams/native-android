package com.odigeo.data.entity.carrousel;

/**
 * @author José Eduardo Cabrera Rivera
 * @version 1.0
 * @since 11/11/2015.
 */
public class Card {

  public static final String KEY_LAST_UPDATE = "KEY_LAST_UPDATE";
  public static final String KEY_CARD_POSITION = "KEY_CARD_POSITION";

  private long id;
  private int priority;
  private String image;
  private CardType type;

  public Card(long id, CardType type, int priority, String image) {
    this.id = id;
    this.type = type;
    this.priority = priority;
    this.image = image;
  }

  public Card() {
    //Empty
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public int getPriority() {
    return priority;
  }

  public void setPriority(int priority) {
    this.priority = priority;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public CardType getType() {
    return type;
  }

  public void setType(CardType type) {
    this.type = type;
  }

  public enum CardType {
    HOME_CARD, SECTION_CARD, DELAYED_SECTION_CARD, DIVERTED_SECTION_CARD, CANCELLED_SECTION_CARD, BAGGAGE_BELT_CARD, BOARDING_GATE_CARD, PROMOTION_CARD, DROP_OFF_CARD
  }
}
