package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class CrossFaringMultiDestinationItinerary implements Serializable {

  private Integer id;
  private Integer firstItinerary;
  private Integer secondItinerary;
  private Integer thirdItinerary;
  private Integer fourthItinerary;
  private Integer fifthItinerary;
  private Integer sixthItinerary;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getFirstItinerary() {
    return firstItinerary;
  }

  public void setFirstItinerary(Integer firstItinerary) {
    this.firstItinerary = firstItinerary;
  }

  public Integer getSecondItinerary() {
    return secondItinerary;
  }

  public void setSecondItinerary(Integer secondItinerary) {
    this.secondItinerary = secondItinerary;
  }

  public Integer getThirdItinerary() {
    return thirdItinerary;
  }

  public void setThirdItinerary(Integer thirdItinerary) {
    this.thirdItinerary = thirdItinerary;
  }

  public Integer getFourthItinerary() {
    return fourthItinerary;
  }

  public void setFourthItinerary(Integer fourthItinerary) {
    this.fourthItinerary = fourthItinerary;
  }

  public Integer getFifthItinerary() {
    return fifthItinerary;
  }

  public void setFifthItinerary(Integer fifthItinerary) {
    this.fifthItinerary = fifthItinerary;
  }

  public Integer getSixthItinerary() {
    return sixthItinerary;
  }

  public void setSixthItinerary(Integer sixthItinerary) {
    this.sixthItinerary = sixthItinerary;
  }
}
