package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum ErrorTypeTestConfiguration implements Serializable {
  UNKNOWN, NO_ERROR, CONNECTION_TIMEOUT, CARD_DENIED
}