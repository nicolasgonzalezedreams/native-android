package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;

public class CollectionMethodKeyPrice implements Serializable {

  private Integer collectionMethodKey;
  private BigDecimal price;

  public Integer getCollectionMethodKey() {
    return collectionMethodKey;
  }

  public void setCollectionMethodKey(Integer collectionMethodKey) {
    this.collectionMethodKey = collectionMethodKey;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }
}
