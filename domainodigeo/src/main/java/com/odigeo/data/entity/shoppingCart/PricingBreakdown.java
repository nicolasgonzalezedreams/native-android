package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class PricingBreakdown implements Serializable {
  private List<PricingBreakdownStep> pricingBreakdownSteps;
  private Double taxRefund;

  public List<PricingBreakdownStep> getPricingBreakdownSteps() {
    return pricingBreakdownSteps;
  }

  public void setPricingBreakdownSteps(List<PricingBreakdownStep> pricingBreakdownSteps) {
    this.pricingBreakdownSteps = pricingBreakdownSteps;
  }

  public Double getTaxRefund() {

    return taxRefund;
  }
}
