package com.odigeo.data.entity.booking;

import java.io.Serializable;

public class Country implements Serializable, Comparable<Country> {

  private int dbId;
  private int geoNodeId;
  private String countryCode;
  private String phonePrefix;
  private String name;

  public Country() {
  }

  public Country(String countryCode, String name) {
    this.countryCode = countryCode;
    this.name = name;
  }

  public int getId() {
    return dbId;
  }

  public void setId(int id) {
    this.dbId = id;
  }

  public int getGeoNodeId() {
    return geoNodeId;
  }

  public void setGeoNodeId(int geoNodeId) {
    this.geoNodeId = geoNodeId;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getPhonePrefix() {
    return phonePrefix;
  }

  public void setPhonePrefix(String phonePrefix) {
    this.phonePrefix = phonePrefix;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override public int compareTo(Country country) {
    return this.name.compareTo(country.name);
  }
}
