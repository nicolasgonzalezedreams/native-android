package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class ShoppingCartCollectionOption implements Serializable {

  private CollectionMethod method;
  private List<InstallmentInformationDescription> installments;
  private BigDecimal fee;
  private String bankTransferText;

  public CollectionMethod getMethod() {
    return method;
  }

  public void setMethod(CollectionMethod method) {
    this.method = method;
  }

  public List<InstallmentInformationDescription> getInstallments() {
    return installments;
  }

  public void setInstallments(List<InstallmentInformationDescription> installments) {
    this.installments = installments;
  }

  public BigDecimal getFee() {
    return fee;
  }

  public void setFee(BigDecimal fee) {
    this.fee = fee;
  }

  public String getBankTransferText() {
    return bankTransferText;
  }

  public void setBankTransferText(String bankTransferText) {
    this.bankTransferText = bankTransferText;
  }
}