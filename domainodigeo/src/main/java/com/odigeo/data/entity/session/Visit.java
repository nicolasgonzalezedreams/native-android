package com.odigeo.data.entity.session;

import java.util.LinkedHashMap;
import java.util.Map;

public class Visit {

  private Map<String, Integer> testAssignments;

  private Map<String, Integer> dimensions;

  public Map<String, Integer> getDimensions() {
    return dimensions;
  }

  public Map<String, Integer> getTestAssignments() {
    return testAssignments;
  }
}
