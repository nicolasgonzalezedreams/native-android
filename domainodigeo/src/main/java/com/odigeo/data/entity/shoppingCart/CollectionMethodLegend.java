package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class CollectionMethodLegend implements Serializable {

  private CollectionMethod collectionMethod;
  private int id;

  public CollectionMethod getCollectionMethod() {
    return collectionMethod;
  }

  public void setCollectionMethod(CollectionMethod collectionMethod) {
    this.collectionMethod = collectionMethod;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}
