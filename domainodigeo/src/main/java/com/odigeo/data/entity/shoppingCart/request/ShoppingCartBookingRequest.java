package com.odigeo.data.entity.shoppingCart.request;

import com.odigeo.data.entity.shoppingCart.CollectionMethodType;

public class ShoppingCartBookingRequest {

  private CollectionMethodType collectionMethodType;
  private String bookingUserComment;
  private CreditCardCollectionDetailsParametersRequest creditCardRequest;
  private UserInteractionNeededRequest userInteractionNeededRequest;

  public CollectionMethodType getCollectionMethodType() {
    return collectionMethodType;
  }

  public void setCollectionMethodType(CollectionMethodType collectionMethodType) {
    this.collectionMethodType = collectionMethodType;
  }

  public String getBookingUserComment() {
    return bookingUserComment;
  }

  public void setBookingUserComment(String bookingUserComment) {
    this.bookingUserComment = bookingUserComment;
  }

  public CreditCardCollectionDetailsParametersRequest getCreditCardRequest() {
    return creditCardRequest;
  }

  public void setCreditCardRequest(CreditCardCollectionDetailsParametersRequest creditCardRequest) {
    this.creditCardRequest = creditCardRequest;
  }

  public UserInteractionNeededRequest getUserInteractionNeededRequest() {
    return userInteractionNeededRequest;
  }

  public void setUserInteractionNeededRequest(
      UserInteractionNeededRequest userInteractionNeededRequest) {
    this.userInteractionNeededRequest = userInteractionNeededRequest;
  }
}