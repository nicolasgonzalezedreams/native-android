package com.odigeo.data.entity.userData;

import java.io.Serializable;

public class InsertCreditCardRequest implements Serializable {

  private String creditCardNumber;
  private String expirationDateMonth;
  private String expirationDateYear;
  private String owner;
  private long creationDate;
  private long modifiedDate;
  private long lastUsageDate;
  private Boolean isDefault;
  private String creditCardTypeId;

  public InsertCreditCardRequest(String creditCardNumber, String expirationDateMonth,
      String expirationDateYear, String owner, long creationDate, long modifiedDate,
      long lastUsageDate, Boolean isDefault, String creditCardTypeId) {
    this.creditCardNumber = creditCardNumber;
    this.expirationDateMonth = expirationDateMonth;
    this.expirationDateYear = expirationDateYear;
    this.owner = owner;
    this.creationDate = creationDate;
    this.modifiedDate = modifiedDate;
    this.lastUsageDate = lastUsageDate;
    this.isDefault = isDefault;
    this.creditCardTypeId = creditCardTypeId;
  }

  public String getCreditCardNumber() {
    return creditCardNumber;
  }

  public void setCreditCardNumber(String value) {
    this.creditCardNumber = value;
  }

  public String getExpirationDateMonth() {
    return expirationDateMonth;
  }

  public void setExpirationDateMonth(String value) {
    this.expirationDateMonth = value;
  }

  public String getExpirationDateYear() {
    return expirationDateYear;
  }

  public void setExpirationDateYear(String value) {
    this.expirationDateYear = value;
  }

  public String getOwner() {
    return owner;
  }

  public void setOwner(final String owner) {
    this.owner = owner;
  }

  public long getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(long creationDate) {
    this.creationDate = creationDate;
  }

  public long getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(long modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public long getLastUsageDate() {
    return lastUsageDate;
  }

  public void setLastUsageDate(long lastUsageDate) {
    this.lastUsageDate = lastUsageDate;
  }

  public Boolean getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(final Boolean isDefault) {
    this.isDefault = isDefault;
  }

  public String getCreditCardTypeId() {
    return creditCardTypeId;
  }

  public void setCreditCardTypeId(final String creditCardTypeId) {
    this.creditCardTypeId = creditCardTypeId;
  }
}
