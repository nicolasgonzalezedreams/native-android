package com.odigeo.data.entity.tracker;

public class Product {
  private String transactionId;
  private String itemName;
  private String itemSku;
  private String itemCategory;
  private double itemPrice;
  private long itemQuantity;
  private String currencyCode;

  public Product(String transactionId, String itemName) {
    this.transactionId = transactionId;
    this.itemName = itemName;
  }

  public final String getTransactionId() {
    return transactionId;
  }

  public final void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public final String getItemName() {
    return itemName;
  }

  public final void setItemName(String itemName) {
    this.itemName = itemName;
  }

  public final String getItemSku() {
    return itemSku;
  }

  public final void setItemSku(String itemSku) {
    this.itemSku = itemSku;
  }

  public final String getItemCategory() {
    return itemCategory;
  }

  public final void setItemCategory(String itemCategory) {
    this.itemCategory = itemCategory;
  }

  public final double getItemPrice() {
    return itemPrice;
  }

  public final void setItemPrice(double itemPrice) {
    this.itemPrice = itemPrice;
  }

  public final long getItemQuantity() {
    return itemQuantity;
  }

  public final void setItemQuantity(long itemQuantity) {
    this.itemQuantity = itemQuantity;
  }

  public final String getCurrencyCode() {
    return currencyCode;
  }

  public final void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }
}
