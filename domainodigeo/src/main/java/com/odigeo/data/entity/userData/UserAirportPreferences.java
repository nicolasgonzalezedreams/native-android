package com.odigeo.data.entity.userData;

public class UserAirportPreferences {

  private long mId;
  private long mUserAirportPreferencesId;
  private String mAirport;
  private long mUserId;

  public UserAirportPreferences() {
    //empty
  }

  public UserAirportPreferences(long id, long userAirportPreferencesId, String airport,
      long userId) {
    mId = id;
    mAirport = airport;
    mUserAirportPreferencesId = userAirportPreferencesId;
    mUserId = userId;
  }

  public long getId() {
    return mId;
  }

  public long getUserAirportPreferencesId() {
    return mUserAirportPreferencesId;
  }

  public String getAirport() {
    return mAirport;
  }

  public long getUserId() {
    return mUserId;
  }
}
