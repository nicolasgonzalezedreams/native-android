package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum AgreementsType implements Serializable {
  ETICKET, PAPER, NO_AGREEMENT;

  public static AgreementsType fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }
}
