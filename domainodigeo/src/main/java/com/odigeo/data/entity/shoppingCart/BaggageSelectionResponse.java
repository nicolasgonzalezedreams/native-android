package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;

public class BaggageSelectionResponse implements Serializable {
  private BaggageDescriptor baggageDescriptor;
  private SegmentTypeIndex segmentTypeIndex;
  private BaggageDescriptor baggageDescriptorIncludedInPrice;
  private BaggageApplicationLevel baggageApplicationLevel;
  private boolean allowanceChanged;
  private BigDecimal price;

  public BaggageDescriptor getBaggageDescriptor() {
    return baggageDescriptor;
  }

  public void setBaggageDescriptor(BaggageDescriptor baggageDescriptor) {
    this.baggageDescriptor = baggageDescriptor;
  }

  public SegmentTypeIndex getSegmentTypeIndex() {
    return segmentTypeIndex;
  }

  public void setSegmentTypeIndex(SegmentTypeIndex segmentTypeIndex) {
    this.segmentTypeIndex = segmentTypeIndex;
  }

  public BaggageDescriptor getBaggageDescriptorIncludedInPrice() {
    return baggageDescriptorIncludedInPrice;
  }

  public void setBaggageDescriptorIncludedInPrice(
      BaggageDescriptor baggageDescriptorIncludedInPrice) {
    this.baggageDescriptorIncludedInPrice = baggageDescriptorIncludedInPrice;
  }

  public BaggageApplicationLevel getBaggageApplicationLevel() {
    return baggageApplicationLevel;
  }

  public void setBaggageApplicationLevel(BaggageApplicationLevel baggageApplicationLevel) {
    this.baggageApplicationLevel = baggageApplicationLevel;
  }

  public void setAllowanceChanged(boolean allowanceChanged) {
    this.allowanceChanged = allowanceChanged;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }
}
