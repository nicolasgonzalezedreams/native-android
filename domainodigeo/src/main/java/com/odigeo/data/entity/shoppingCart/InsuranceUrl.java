package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class InsuranceUrl implements Serializable {

  private InsuranceConditionsUrlType type;
  private String url;

  public InsuranceConditionsUrlType getType() {
    return type;
  }

  public void setType(InsuranceConditionsUrlType type) {
    this.type = type;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
