package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum BookingResponseStatusTestConfiguration implements Serializable {
  BROKEN_FLOW, BOOKING_ERROR, BOOKING_PAYMENT_RETRY, BOOKING_STOP
}
