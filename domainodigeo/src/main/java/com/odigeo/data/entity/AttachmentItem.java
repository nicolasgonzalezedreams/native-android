package com.odigeo.data.entity;

/**
 * Created by eleazarspak on 2/5/16.
 */
public class AttachmentItem {

  private String mName;
  private String mContent;

  public AttachmentItem(String name, String content) {
    mName = name;
    mContent = content;
  }

  public String getName() {
    return mName;
  }

  public void setName(String name) {
    mName = name;
  }

  public String getContent() {
    return mContent;
  }

  public void setContent(String content) {
    mContent = content;
  }
}
