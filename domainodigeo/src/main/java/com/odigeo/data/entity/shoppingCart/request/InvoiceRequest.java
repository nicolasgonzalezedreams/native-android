package com.odigeo.data.entity.shoppingCart.request;

public class InvoiceRequest {

  private boolean required;
  private boolean professional;
  private String fullName;
  private String address;
  private String city;
  private String postalCode;
  private String country;
  private String id;
  private String invoiceMail;

  public boolean isRequired() {
    return required;
  }

  public void setRequired(boolean required) {
    this.required = required;
  }

  public boolean isProfessional() {
    return professional;
  }

  public void setProfessional(boolean professional) {
    this.professional = professional;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getInvoiceMail() {
    return invoiceMail;
  }

  public void setInvoiceMail(String invoiceMail) {
    this.invoiceMail = invoiceMail;
  }
}