package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class TravellerRequiredFields implements Serializable {

  private List<TravellerIdentificationType> identificationTypes;
  private List<BaggageConditions> baggageConditions;
  private List<AirlineGroup> frequentFlyerAirlineGroups;
  private List<ResidentGroupContainer> residentGroups;
  private List<TravellerInformationFieldConditionRule> conditionallyNeededFields;
  private TravellerType travellerType;
  private RequiredField needsTitle;
  private RequiredField needsName;
  private RequiredField needsGender;
  private RequiredField needsNationality;
  private RequiredField needsBirthDate;
  private RequiredField needsCountryOfResidence;
  private RequiredField needsIdentification;
  private RequiredField needsIdentificationExpirationDate;
  private RequiredField needsIdentificationCountry;

  //##########################
  //### GETTERS AND SETTERS###
  //##########################

  public List<TravellerIdentificationType> getIdentificationTypes() {
    return identificationTypes;
  }

  public void setIdentificationTypes(List<TravellerIdentificationType> identificationTypes) {
    this.identificationTypes = identificationTypes;
  }

  public List<BaggageConditions> getBaggageConditions() {
    return baggageConditions;
  }

  public void setBaggageConditions(List<BaggageConditions> baggageConditions) {
    this.baggageConditions = baggageConditions;
  }

  public List<AirlineGroup> getFrequentFlyerAirlineGroups() {
    return frequentFlyerAirlineGroups;
  }

  public void setFrequentFlyerAirlineGroups(List<AirlineGroup> frequentFlyerAirlineGroups) {
    this.frequentFlyerAirlineGroups = frequentFlyerAirlineGroups;
  }

  public List<ResidentGroupContainer> getResidentGroups() {
    return residentGroups;
  }

  public void setResidentGroups(List<ResidentGroupContainer> residentGroups) {
    this.residentGroups = residentGroups;
  }

  public List<TravellerInformationFieldConditionRule> getConditionallyNeededFields() {
    return conditionallyNeededFields;
  }

  public void setConditionallyNeededFields(
      List<TravellerInformationFieldConditionRule> conditionallyNeededFields) {
    this.conditionallyNeededFields = conditionallyNeededFields;
  }

  public TravellerType getTravellerType() {
    return travellerType;
  }

  public void setTravellerType(TravellerType travellerType) {
    this.travellerType = travellerType;
  }

  public RequiredField getNeedsTitle() {
    return needsTitle;
  }

  public void setNeedsTitle(RequiredField needsTitle) {
    this.needsTitle = needsTitle;
  }

  public RequiredField getNeedsName() {
    return needsName;
  }

  public void setNeedsName(RequiredField needsName) {
    this.needsName = needsName;
  }

  public RequiredField getNeedsGender() {
    return needsGender;
  }

  public void setNeedsGender(RequiredField needsGender) {
    this.needsGender = needsGender;
  }

  public RequiredField getNeedsNationality() {
    return needsNationality;
  }

  public void setNeedsNationality(RequiredField needsNationality) {
    this.needsNationality = needsNationality;
  }

  public RequiredField getNeedsBirthDate() {
    return needsBirthDate;
  }

  public void setNeedsBirthDate(RequiredField needsBirthDate) {
    this.needsBirthDate = needsBirthDate;
  }

  public RequiredField getNeedsCountryOfResidence() {
    return needsCountryOfResidence;
  }

  public void setNeedsCountryOfResidence(RequiredField needsCountryOfResidence) {
    this.needsCountryOfResidence = needsCountryOfResidence;
  }

  public RequiredField getNeedsIdentification() {
    return needsIdentification;
  }

  public void setNeedsIdentification(RequiredField needsIdentification) {
    this.needsIdentification = needsIdentification;
  }

  public RequiredField getNeedsIdentificationExpirationDate() {
    return needsIdentificationExpirationDate;
  }

  public void setNeedsIdentificationExpirationDate(
      RequiredField needsIdentificationExpirationDate) {
    this.needsIdentificationExpirationDate = needsIdentificationExpirationDate;
  }

  public RequiredField getNeedsIdentificationCountry() {
    return needsIdentificationCountry;
  }

  public void setNeedsIdentificationCountry(RequiredField needsIdentificationCountry) {
    this.needsIdentificationCountry = needsIdentificationCountry;
  }
}
