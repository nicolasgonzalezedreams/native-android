package com.odigeo.data.entity.contract;

import com.odigeo.data.entity.shoppingCart.CollectionMethod;
import com.odigeo.data.entity.shoppingCart.CollectionMethodType;
import com.odigeo.data.entity.shoppingCart.CreditCard;
import com.odigeo.data.entity.shoppingCart.Movement;
import java.math.BigDecimal;
import java.util.List;

public class CollectionAttempt {

  private CollectionMethod collectionMethod;
  private List<Movement> movements;
  private CreditCard creditCard;
  private ElvAccount elvAccount;
  private TravelAccount travelAccount;
  private BigDecimal amount;
  private CollectionMethodType type;

  public CollectionMethod getCollectionMethod() {
    return collectionMethod;
  }

  public void setCollectionMethod(CollectionMethod collectionMethod) {
    this.collectionMethod = collectionMethod;
  }

  public List<Movement> getMovements() {
    return movements;
  }

  public void setMovements(List<Movement> movements) {
    this.movements = movements;
  }

  public CreditCard getCreditCard() {
    return creditCard;
  }

  public void setCreditCard(CreditCard creditCard) {
    this.creditCard = creditCard;
  }

  public ElvAccount getElvAccount() {
    return elvAccount;
  }

  public void setElvAccount(ElvAccount elvAccount) {
    this.elvAccount = elvAccount;
  }

  public TravelAccount getTravelAccount() {
    return travelAccount;
  }

  public void setTravelAccount(TravelAccount travelAccount) {
    this.travelAccount = travelAccount;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public CollectionMethodType getType() {
    return type;
  }

  public void setType(CollectionMethodType type) {
    this.type = type;
  }
}