package com.odigeo.data.entity;

/**
 * Created by ManuelOrtiz on 02/09/2014.
 */
public enum FlightsDirection {
  DEPARTURE(0), RETURN(1);

  private final int value;

  private FlightsDirection(int value) {
    this.value = value;
  }

  public static FlightsDirection fromInt(int value) {
    if (value == DEPARTURE.value) {
      return DEPARTURE;
    } else if (value == RETURN.value) {
      return RETURN;
    }

    return DEPARTURE;
  }

  public int getValue() {
    return value;
  }

}
