package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class ResidentGroupContainer implements Serializable {

  private List<ResidentGroupLocality> residentLocalities;
  private ResidentGroup name;

  public List<ResidentGroupLocality> getResidentLocalities() {
    return residentLocalities;
  }

  public void setResidentLocalities(List<ResidentGroupLocality> residentLocalities) {
    this.residentLocalities = residentLocalities;
  }

  public ResidentGroup getName() {
    return name;
  }

  public void setName(ResidentGroup name) {
    this.name = name;
  }
}
