package com.odigeo.data.entity.userData;

public class UserIdentification {

  private long mId;
  private long mUserIdentificationId;
  private String mIdentificationId;
  private String mIdentificationCountryCode;
  private long mIdentificationExpirationDate;
  private IdentificationType mIdentificationType;
  private long mUserProfileId;

  public UserIdentification() {
    //empty
  }

  public UserIdentification(long id, long userIdentificationId, String identificationId,
      String identificationCountryCode, long identificationExpirationDate,
      IdentificationType identificationType, long userProfileId) {
    mId = id;
    mUserIdentificationId = userIdentificationId;
    mIdentificationId = identificationId;
    mIdentificationCountryCode = identificationCountryCode;
    mIdentificationExpirationDate = identificationExpirationDate;
    mIdentificationType = identificationType;
    mUserProfileId = userProfileId;
  }

  public long getId() {
    return mId;
  }

  public void setId(long id) {
    mId = id;
  }

  public String getIdentificationId() {
    return mIdentificationId;
  }

  public void setIdentificationId(String identificationId) {
    mIdentificationId = identificationId;
  }

  public long getUserIdentificationId() {
    return mUserIdentificationId;
  }

  public void setUserIdentificationId(long userIdentificationId) {
    mUserIdentificationId = userIdentificationId;
  }

  public String getIdentificationCountryCode() {
    return mIdentificationCountryCode;
  }

  public void setIdentificationCountryCode(String identificationCountryCode) {
    mIdentificationCountryCode = identificationCountryCode;
  }

  public long getIdentificationExpirationDate() {
    return mIdentificationExpirationDate;
  }

  public void setIdentificationExpirationDate(long identificationExpirationDate) {
    mIdentificationExpirationDate = identificationExpirationDate;
  }

  public IdentificationType getIdentificationType() {
    return mIdentificationType;
  }

  public void setIdentificationType(IdentificationType identificationType) {
    mIdentificationType = identificationType;
  }

  public long getUserProfileId() {
    return mUserProfileId;
  }

  public void setUserProfileId(long userProfileId) {
    mUserProfileId = userProfileId;
  }

  @Override public boolean equals(Object userIdentification2) {
    return userIdentification2 != null
        && userIdentification2 instanceof UserIdentification
        && getUserIdentificationId()
        == ((UserIdentification) userIdentification2).getUserIdentificationId();
  }

  public enum IdentificationType {
    PASSPORT, NIE, NIF, CIF, NATIONAL_ID_CARD, BIRTH_DATE, UNKNOWN
  }
}
