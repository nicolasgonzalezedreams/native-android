package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResumeDataRequest implements Serializable {

  private List<BookingAdditionalParameter> additionalParameters;

  public List<BookingAdditionalParameter> getAdditionalParameters() {
    if (this.additionalParameters == null) {
      this.additionalParameters = new ArrayList<>();
    }

    return this.additionalParameters;
  }

  public void setAdditionalParameters(List<BookingAdditionalParameter> additionalParameters) {
    this.additionalParameters = additionalParameters;
  }
}
