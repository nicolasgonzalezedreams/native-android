package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum TravellerTitle implements Serializable {
  MR, MRS, MS;

  public static TravellerTitle fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
