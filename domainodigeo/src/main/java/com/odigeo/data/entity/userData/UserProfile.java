package com.odigeo.data.entity.userData;

import java.util.List;

public class UserProfile {

  public static long UNKNOWN_BIRTHDATE = 9223372036000L; //near Long.MAX_VALUE / 1000000
  private long mId;
  private long mUserProfileId;
  private String mGender;
  private Title mTitle;
  private String mName;
  private String mMiddleName;
  private String mFirstLastName;
  private String mSecondLastName;
  private long mBirthDate;
  private String mPrefixPhoneNumber;
  private String mPhoneNumber;
  private String mPrefixAlternatePhoneNumber;
  private String mAlternatePhoneNumber;
  private String mMobilePhoneNumber;
  private String mNationalityCountryCode;
  private String mCpf;
  private boolean mIsDefaultTraveller;
  private String mPhoto;
  private long mUserTravellerId;
  private UserAddress mUserAddress;
  private List<UserIdentification> mUserIdentificationList;

  public UserProfile() {
  }

  public UserProfile(long id, long userProfileId, String gender, Title title, String name,
      String middleName, String firstLastName, String secondLastName, long birthDate,
      String prefixPhoneNumber, String phoneNumber, String prefixAlternatePhoneNumber,
      String alternatePhoneNumber, String mobilePhoneNumber, String nationalityCountryCode,
      String cpf, boolean isDefaultTraveller, String photo, UserAddress userAddress,
      List<UserIdentification> userIdentificationList, long userTravellerId) {
    mId = id;
    mUserProfileId = userProfileId;
    mGender = gender;
    mTitle = title;
    mName = name;
    mMiddleName = middleName;
    mFirstLastName = firstLastName;
    mSecondLastName = secondLastName;
    mBirthDate = birthDate;
    mPrefixPhoneNumber = prefixPhoneNumber;
    mPhoneNumber = phoneNumber;
    mPrefixAlternatePhoneNumber = prefixAlternatePhoneNumber;
    mAlternatePhoneNumber = alternatePhoneNumber;
    mMobilePhoneNumber = mobilePhoneNumber;
    mNationalityCountryCode = nationalityCountryCode;
    mCpf = cpf;
    mIsDefaultTraveller = isDefaultTraveller;
    mPhoto = photo;
    mUserTravellerId = userTravellerId;
    mUserAddress = userAddress;
    mUserIdentificationList = userIdentificationList;
  }

  public UserProfile(long id, long userProfileId, String name, String firstLastName, long birthDate,
      boolean isDefaultTraveller, String photo, long userTravellerId) {
    mId = id;
    mUserProfileId = userProfileId;
    mName = name;
    mFirstLastName = firstLastName;
    mBirthDate = birthDate;
    mIsDefaultTraveller = isDefaultTraveller;
    mPhoto = photo;
    mUserTravellerId = userTravellerId;
  }

  public static UserProfile cloneToDefaultTraveller(UserProfile userProfile) {
    return new UserProfile(userProfile.getId(), userProfile.getUserProfileId(),
        userProfile.getGender(), userProfile.getTitle(), userProfile.getName(),
        userProfile.getMiddleName(), userProfile.getFirstLastName(),
        userProfile.getSecondLastName(), userProfile.getBirthDate(),
        userProfile.getPrefixPhoneNumber(), userProfile.getPhoneNumber(),
        userProfile.getPrefixAlternatePhoneNumber(), userProfile.getAlternatePhoneNumber(),
        userProfile.getMobilePhoneNumber(), userProfile.getNationalityCountryCode(),
        userProfile.getCpf(), true, userProfile.getPhoto(), userProfile.getUserAddress(),
        userProfile.getUserIdentificationList(), userProfile.getUserTravellerId());
  }

  public static void setUnknownBirthdate(long unknownBirthdate) {
    UNKNOWN_BIRTHDATE = unknownBirthdate;
  }

  public long getId() {
    return mId;
  }

  public void setId(long id) {
    mId = id;
  }

  public long getUserProfileId() {
    return mUserProfileId;
  }

  public void setUserProfileId(long userProfileId) {
    mUserProfileId = userProfileId;
  }

  public String getGender() {
    return mGender;
  }

  public void setGender(String gender) {
    mGender = gender;
  }

  public Title getTitle() {
    return mTitle;
  }

  public void setTitle(Title title) {
    mTitle = title;
  }

  public String getName() {
    return mName;
  }

  public void setName(String mName) {
    this.mName = mName;
  }

  public String getMiddleName() {
    return mMiddleName;
  }

  public void setMiddleName(String middleName) {
    mMiddleName = middleName;
  }

  public String getFirstLastName() {
    return mFirstLastName;
  }

  public void setFirstLastName(String firstLastName) {
    mFirstLastName = firstLastName;
  }

  public String getSecondLastName() {
    return mSecondLastName;
  }

  public void setSecondLastName(String secondLastName) {
    mSecondLastName = secondLastName;
  }

  public long getBirthDate() {
    return mBirthDate;
  }

  public void setBirthDate(long birthDate) {
    mBirthDate = birthDate;
  }

  public String getPrefixPhoneNumber() {
    return mPrefixPhoneNumber;
  }

  public void setPrefixPhoneNumber(String prefixPhoneNumber) {
    mPrefixPhoneNumber = prefixPhoneNumber;
  }

  public String getPhoneNumber() {
    return mPhoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    mPhoneNumber = phoneNumber;
  }

  public String getPrefixAlternatePhoneNumber() {
    return mPrefixAlternatePhoneNumber;
  }

  public void setPrefixAlternatePhoneNumber(String prefixAlternatePhoneNumber) {
    mPrefixAlternatePhoneNumber = prefixAlternatePhoneNumber;
  }

  public String getAlternatePhoneNumber() {
    return mAlternatePhoneNumber;
  }

  public void setAlternatePhoneNumber(String alternatePhoneNumber) {
    mAlternatePhoneNumber = alternatePhoneNumber;
  }

  public String getMobilePhoneNumber() {
    return mMobilePhoneNumber;
  }

  public void setMobilePhoneNumber(String mobilePhoneNumber) {
    mMobilePhoneNumber = mobilePhoneNumber;
  }

  public String getNationalityCountryCode() {
    return mNationalityCountryCode;
  }

  public void setNationalityCountryCode(String nationalityCountryCode) {
    mNationalityCountryCode = nationalityCountryCode;
  }

  public String getCpf() {
    return mCpf;
  }

  public void setCpf(String cpf) {
    mCpf = cpf;
  }

  public boolean isDefaultTraveller() {
    return mIsDefaultTraveller;
  }

  public void setDefaultTraveller(boolean defaultTraveller) {
    mIsDefaultTraveller = defaultTraveller;
  }

  public String getPhoto() {
    return mPhoto;
  }

  public void setPhoto(String photo) {
    mPhoto = photo;
  }

  public long getUserTravellerId() {
    return mUserTravellerId;
  }

  public void setUserTravellerId(long userTravellerId) {
    mUserTravellerId = userTravellerId;
  }

  public UserAddress getUserAddress() {
    return mUserAddress;
  }

  public void setUserAddress(UserAddress userAddress) {
    mUserAddress = userAddress;
  }

  public List<com.odigeo.data.entity.userData.UserIdentification> getUserIdentificationList() {
    return mUserIdentificationList;
  }

  public void setUserIdentificationList(
      List<com.odigeo.data.entity.userData.UserIdentification> userIdentificationList) {
    mUserIdentificationList = userIdentificationList;
  }

  public enum Title {
    MR, MRS, MS, UNKNOWN
  }

  public enum Gender {
    MALE, FEMALE
  }
}
