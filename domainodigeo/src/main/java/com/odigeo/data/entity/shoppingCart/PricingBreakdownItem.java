package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;

public class PricingBreakdownItem implements Serializable {
  private PricingBreakdownItemType priceItemType;
  private BigDecimal priceItemAmount;

  public PricingBreakdownItemType getPriceItemType() {
    return priceItemType;
  }

  public void setPriceItemType(PricingBreakdownItemType priceItemType) {
    this.priceItemType = priceItemType;
  }

  public BigDecimal getPriceItemAmount() {
    return priceItemAmount;
  }

  public void setPriceItemAmount(BigDecimal priceItemAmount) {
    this.priceItemAmount = priceItemAmount;
  }
}
