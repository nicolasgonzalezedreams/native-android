package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class TravellerResidentValidation implements Serializable {

  private String numPassenger;
  private ResidentValidationStatus status;

  public String getNumPassenger() {
    return numPassenger;
  }

  public void setNumPassenger(String numPassenger) {
    this.numPassenger = numPassenger;
  }

  public ResidentValidationStatus getStatus() {
    return status;
  }

  public void setStatus(ResidentValidationStatus status) {
    this.status = status;
  }
}