package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum CollectionMethodType implements Serializable {

  CREDITCARD("CREDITCARD"), ELV("ELV"), PAYPAL("PAYPAL"), SECURE_3_D("SECURE3D"), BANKTRANSFER(
      "BANKTRANSFER"), INSTALLMENT("INSTALLMENT"), COFINOGA("COFINOGA"), SOFORT("SOFORT"), GIROPAY(
      "GIROPAY"), PREPAY("PREPAY"), TRUSTLY("TRUSTLY"), KLARNA("KLARNA");

  private final String value;

  CollectionMethodType(String v) {
    value = v;
  }

  public static CollectionMethodType fromValue(String v) {
    for (CollectionMethodType c : CollectionMethodType.values()) {
      if (c.value.equals(v)) {
        return c;
      }
    }
    throw new IllegalArgumentException(v);
  }

  public String value() {
    return value;
  }
}