package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class CollectionEstimationFees implements Serializable {

  private List<CollectionMethodKeyPrice> collectionMethodFees;
  private int id;

  public List<CollectionMethodKeyPrice> getCollectionMethodFees() {
    return collectionMethodFees;
  }

  public void setCollectionMethodFees(List<CollectionMethodKeyPrice> collectionMethodFees) {
    this.collectionMethodFees = collectionMethodFees;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}
