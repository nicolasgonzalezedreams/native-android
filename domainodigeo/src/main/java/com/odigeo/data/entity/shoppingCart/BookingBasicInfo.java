package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.Date;

public class BookingBasicInfo implements Serializable {

  private long id;
  private Website website;
  private String clientBookingReferenceId;
  private Date creationDate;
  private Date requestDate;
  private Date lastUpdate;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Website getWebsite() {
    return website;
  }

  public void setWebsite(Website website) {
    this.website = website;
  }

  public String getClientBookingReferenceId() {
    return clientBookingReferenceId;
  }

  public void setClientBookingReferenceId(String clientBookingReferenceId) {
    this.clientBookingReferenceId = clientBookingReferenceId;
  }

  public Date getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }

  public Date getRequestDate() {
    return requestDate;
  }

  public void setRequestDate(Date requestDate) {
    this.requestDate = requestDate;
  }

  public Date getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(Date lastUpdate) {
    this.lastUpdate = lastUpdate;
  }
}