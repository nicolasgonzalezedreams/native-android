package com.odigeo.data.entity.carrousel;

import java.util.ArrayList;
import java.util.List;

public class Carousel {

  private List<Card> mDropOffCard;
  private List<Card> mBookingCards;
  private List<Card> mPromotionCards;

  public Carousel() {
  }

  public List<Card> getCarrouselCards() {
    List<Card> cards = new ArrayList<>();
    if (mDropOffCard != null) cards.addAll(mDropOffCard);

    if (mBookingCards != null) cards.addAll(mBookingCards);

    if (mPromotionCards != null) cards.addAll(mPromotionCards);

    return cards;
  }

  public void setDropOffCards(List<Card> mDropOffCard) {
    this.mDropOffCard = mDropOffCard;
  }

  public void setBookingCards(List<Card> mBookingCards) {
    this.mBookingCards = mBookingCards;
  }

  public void setPromotionCards(List<Card> mPromotionCards) {
    this.mPromotionCards = mPromotionCards;
  }
}
