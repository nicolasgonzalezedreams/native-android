package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class CrossFaringItinerary implements Serializable {

  private Integer id;
  private Integer out;
  private Integer in;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getOut() {
    return out;
  }

  public void setOut(Integer out) {
    this.out = out;
  }

  public Integer getIn() {
    return in;
  }

  public void setIn(Integer in) {
    this.in = in;
  }
}
