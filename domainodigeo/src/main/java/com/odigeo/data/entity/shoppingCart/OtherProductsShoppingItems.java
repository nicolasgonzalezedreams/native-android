package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class OtherProductsShoppingItems implements Serializable {

  private List<InsuranceShoppingItem> insuranceShoppingItems;
  private List<AdditionalProductShoppingItem> additionalProductShoppingItems;

  public List<InsuranceShoppingItem> getInsuranceShoppingItems() {
    return insuranceShoppingItems;
  }

  public void setInsuranceShoppingItems(List<InsuranceShoppingItem> insuranceShoppingItems) {
    this.insuranceShoppingItems = insuranceShoppingItems;
  }

  public List<AdditionalProductShoppingItem> getAdditionalProductShoppingItems() {
    return additionalProductShoppingItems;
  }

  public void setAdditionalProductShoppingItems(
      List<AdditionalProductShoppingItem> additionalProductShoppingItems) {
    this.additionalProductShoppingItems = additionalProductShoppingItems;
  }
}
