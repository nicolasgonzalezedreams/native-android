package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class CreateShoppingCartResponse extends PreferencesAwareResponse implements Serializable {

  private ShoppingCart shoppingCart;
  private ItinerarySortCriteria sortCriteria;
  private PricingBreakdown pricingBreakdown;
  private boolean userCommentRequired;
  private ResidentValidationRetry residentValidationRetry;

  public ShoppingCart getShoppingCart() {
    return shoppingCart;
  }

  public void setShoppingCart(ShoppingCart shoppingCart) {
    this.shoppingCart = shoppingCart;
  }

  public ItinerarySortCriteria getSortCriteria() {
    return sortCriteria;
  }

  public void setSortCriteria(ItinerarySortCriteria sortCriteria) {
    this.sortCriteria = sortCriteria;
  }

  public PricingBreakdown getPricingBreakdown() {
    return pricingBreakdown;
  }

  public void setPricingBreakdown(PricingBreakdown pricingBreakdown) {
    this.pricingBreakdown = pricingBreakdown;
  }

  public boolean isUserCommentRequired() {
    return userCommentRequired;
  }

  public void setUserCommentRequired(boolean userCommentRequired) {
    this.userCommentRequired = userCommentRequired;
  }

  public ResidentValidationRetry getResidentValidationRetry() {
    return residentValidationRetry;
  }

  public void setResidentValidationRetry(ResidentValidationRetry residentValidationRetry) {
    this.residentValidationRetry = residentValidationRetry;
  }

  public double getTotalPriceTickets() {
    double total = 0;
    int lastStepPostion = getPricingBreakdown().getPricingBreakdownSteps().size() - 1;
    PricingBreakdownStep step =
        getPricingBreakdown().getPricingBreakdownSteps().get(lastStepPostion);

    for (PricingBreakdownItem item : step.getPricingBreakdownItems()) {
      if ((item.getPriceItemType() == PricingBreakdownItemType.ADULTS_PRICE)
          || (item.getPriceItemType() == PricingBreakdownItemType.CHILDREN_PRICE)
          || (item.getPriceItemType() == PricingBreakdownItemType.INFANTS_PRICE)) {
        total += item.getPriceItemAmount().doubleValue();
      }
    }
    return total;
  }
}
