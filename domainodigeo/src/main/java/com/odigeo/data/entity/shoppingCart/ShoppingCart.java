package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class ShoppingCart implements Serializable {

  private ShoppingCartPriceCalculator price;
  private ItineraryShoppingItem itineraryShoppingItem;
  private OtherProductsShoppingItems otherProductsShoppingItems;
  private List<TravellerRequiredFields> requiredTravellerInformation;
  private BuyerRequiredFields requiredBuyerInformation;
  private List<FormOfIdentificationDescriptor> requiredFoidInformation;
  private Buyer buyer;
  private List<Traveller> travellers;
  private List<ShoppingCartCollectionOption> collectionOptions;
  private FraudResultContainer fraudResultContainer;
  private List<FormOfIdentification> foids;
  private List<PromotionalCodeUsageResponse> promotionalCodes;
  private long bookingId;
  private BigDecimal totalPrice;
  private BigDecimal baggageTotalFee;
  private BigDecimal collectionTotalFee;
  private PromotionalCampaignGroupResponse promotionalCampaignGroup;
  private String cyberSourceMerchantId;

  public ShoppingCart() {
    //empty
  }

  //###########################
  //### GETTERS AND SETTERS ###
  //###########################

  public ShoppingCartPriceCalculator getPrice() {
    return price;
  }

  public void setPrice(ShoppingCartPriceCalculator price) {
    this.price = price;
  }

  public ItineraryShoppingItem getItineraryShoppingItem() {
    return itineraryShoppingItem;
  }

  public void setItineraryShoppingItem(ItineraryShoppingItem itineraryShoppingItem) {
    this.itineraryShoppingItem = itineraryShoppingItem;
  }

  public OtherProductsShoppingItems getOtherProductsShoppingItems() {
    return otherProductsShoppingItems;
  }

  public void setOtherProductsShoppingItems(OtherProductsShoppingItems otherProductsShoppingItems) {
    this.otherProductsShoppingItems = otherProductsShoppingItems;
  }

  public List<TravellerRequiredFields> getRequiredTravellerInformation() {
    return requiredTravellerInformation;
  }

  public void setRequiredTravellerInformation(
      List<TravellerRequiredFields> requiredTravellerInformation) {
    this.requiredTravellerInformation = requiredTravellerInformation;
  }

  public BuyerRequiredFields getRequiredBuyerInformation() {
    return requiredBuyerInformation;
  }

  public void setRequiredBuyerInformation(BuyerRequiredFields requiredBuyerInformation) {
    this.requiredBuyerInformation = requiredBuyerInformation;
  }

  public List<FormOfIdentificationDescriptor> getRequiredFoidInformation() {
    return requiredFoidInformation;
  }

  public void setRequiredFoidInformation(
      List<FormOfIdentificationDescriptor> requiredFoidInformation) {
    this.requiredFoidInformation = requiredFoidInformation;
  }

  public Buyer getBuyer() {
    return buyer;
  }

  public void setBuyer(Buyer buyer) {
    this.buyer = buyer;
  }

  public List<Traveller> getTravellers() {
    return travellers;
  }

  public void setTravellers(List<Traveller> travellers) {
    this.travellers = travellers;
  }

  public List<ShoppingCartCollectionOption> getCollectionOptions() {
    return collectionOptions;
  }

  public void setCollectionOptions(List<ShoppingCartCollectionOption> collectionOptions) {
    this.collectionOptions = collectionOptions;
  }

  public FraudResultContainer getFraudResultContainer() {
    return fraudResultContainer;
  }

  public void setFraudResultContainer(FraudResultContainer fraudResultContainer) {
    this.fraudResultContainer = fraudResultContainer;
  }

  public List<FormOfIdentification> getFoids() {
    return foids;
  }

  public void setFoids(List<FormOfIdentification> foids) {
    this.foids = foids;
  }

  public List<PromotionalCodeUsageResponse> getPromotionalCodes() {
    return promotionalCodes;
  }

  public void setPromotionalCodes(List<PromotionalCodeUsageResponse> promotionalCodes) {
    this.promotionalCodes = promotionalCodes;
  }

  public long getBookingId() {
    return bookingId;
  }

  public void setBookingId(long bookingId) {
    this.bookingId = bookingId;
  }

  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }

  public BigDecimal getBaggageTotalFee() {
    return baggageTotalFee;
  }

  public void setBaggageTotalFee(BigDecimal baggageTotalFee) {
    this.baggageTotalFee = baggageTotalFee;
  }

  public BigDecimal getCollectionTotalFee() {
    return collectionTotalFee;
  }

  public void setCollectionTotalFee(BigDecimal collectionTotalFee) {
    this.collectionTotalFee = collectionTotalFee;
  }

  public PromotionalCampaignGroupResponse getPromotionalCampaignGroup() {
    return promotionalCampaignGroup;
  }

  public void setPromotionalCampaignGroup(
      PromotionalCampaignGroupResponse promotionalCampaignGroup) {
    this.promotionalCampaignGroup = promotionalCampaignGroup;
  }

  public String getCyberSourceMerchantId() {
    return cyberSourceMerchantId;
  }

  public void setCyberSourceMerchantId(String cyberSourceMerchantId) {
    this.cyberSourceMerchantId = cyberSourceMerchantId;
  }
}
