package com.odigeo.data.entity.carrousel;

public class BaggageBeltCard extends SectionCard {

  private String baggageBelt;

  public BaggageBeltCard(long id, CardType type, int priority, String image, String carrier,
      String carrierName, String flightId, CarouselLocation from, CarouselLocation to,
      String baggageBelt) {
    super(id, type, priority, image, carrier, carrierName, flightId, from, to);
    this.baggageBelt = baggageBelt;
  }

  public BaggageBeltCard() {

  }

  public String getBaggageBelt() {
    return baggageBelt;
  }

  public void setBaggageBelt(final String baggageBelt) {
    this.baggageBelt = baggageBelt;
  }
}
