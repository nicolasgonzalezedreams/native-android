package com.odigeo.data.entity.shoppingCart.request;

public class PaymentDataRequest {

  private ShoppingCartBookingRequest bookingRequest;
  private TestConfigurationConfirmRequest testConfigurationConfirmRequest;

  public ShoppingCartBookingRequest getBookingRequest() {
    return bookingRequest;
  }

  public void setBookingRequest(ShoppingCartBookingRequest bookingRequest) {
    this.bookingRequest = bookingRequest;
  }

  public TestConfigurationConfirmRequest getTestConfigurationConfirmRequest() {
    return testConfigurationConfirmRequest;
  }

  public void setTestConfigurationConfirmRequest(
      TestConfigurationConfirmRequest testConfigurationConfirmRequest) {
    this.testConfigurationConfirmRequest = testConfigurationConfirmRequest;
  }
}