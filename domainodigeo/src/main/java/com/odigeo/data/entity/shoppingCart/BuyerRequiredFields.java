package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BuyerRequiredFields implements Serializable {
  private List<BuyerIdentificationType> identificationTypes;
  private RequiredField needsName;
  private RequiredField needsLastNames;
  private RequiredField needsMail;
  private RequiredField needsIdentification;
  private RequiredField needsDateOfBirth;
  private RequiredField needsCpf;
  private RequiredField needsAddress;
  private RequiredField needsCityName;
  private RequiredField needsNeighborhood;
  private RequiredField needsStateName;
  private RequiredField needsZipCode;
  private RequiredField needsPhoneNumber;
  private RequiredField needsAlternativePhoneNumber;
  private RequiredField needsCountryCode;

  public List<BuyerIdentificationType> getIdentificationTypes() {
    return identificationTypes;
  }

  public void setIdentificationTypes(List<BuyerIdentificationType> identificationTypes) {
    this.identificationTypes = identificationTypes;
  }

  public RequiredField getNeedsName() {
    return needsName;
  }

  public void setNeedsName(RequiredField needsName) {
    this.needsName = needsName;
  }

  public RequiredField getNeedsLastNames() {
    return needsLastNames;
  }

  public void setNeedsLastNames(RequiredField needsLastNames) {
    this.needsLastNames = needsLastNames;
  }

  public RequiredField getNeedsMail() {
    return needsMail;
  }

  public void setNeedsMail(RequiredField needsMail) {
    this.needsMail = needsMail;
  }

  public RequiredField getNeedsIdentification() {
    return needsIdentification;
  }

  public void setNeedsIdentification(RequiredField needsIdentification) {
    this.needsIdentification = needsIdentification;
  }

  public RequiredField getNeedsDateOfBirth() {
    return needsDateOfBirth;
  }

  public void setNeedsDateOfBirth(RequiredField needsDateOfBirth) {
    this.needsDateOfBirth = needsDateOfBirth;
  }

  public RequiredField getNeedsCpf() {
    return needsCpf;
  }

  public void setNeedsCpf(RequiredField needsCpf) {
    this.needsCpf = needsCpf;
  }

  public RequiredField getNeedsAddress() {
    return needsAddress;
  }

  public void setNeedsAddress(RequiredField needsAddress) {
    this.needsAddress = needsAddress;
  }

  public RequiredField getNeedsCityName() {
    return needsCityName;
  }

  public void setNeedsCityName(RequiredField needsCityName) {
    this.needsCityName = needsCityName;
  }

  public RequiredField getNeedsNeighborhood() {
    return needsNeighborhood;
  }

  public void setNeedsNeighborhood(RequiredField needsNeighborhood) {
    this.needsNeighborhood = needsNeighborhood;
  }

  public RequiredField getNeedsStateName() {
    return needsStateName;
  }

  public void setNeedsStateName(RequiredField needsStateName) {
    this.needsStateName = needsStateName;
  }

  public RequiredField getNeedsZipCode() {
    return needsZipCode;
  }

  public void setNeedsZipCode(RequiredField needsZipCode) {
    this.needsZipCode = needsZipCode;
  }

  public RequiredField getNeedsPhoneNumber() {
    return needsPhoneNumber;
  }

  public void setNeedsPhoneNumber(RequiredField needsPhoneNumber) {
    this.needsPhoneNumber = needsPhoneNumber;
  }

  public RequiredField getNeedsAlternativePhoneNumber() {
    return needsAlternativePhoneNumber;
  }

  public void setNeedsAlternativePhoneNumber(RequiredField needsAlternativePhoneNumber) {
    this.needsAlternativePhoneNumber = needsAlternativePhoneNumber;
  }

  public RequiredField getNeedsCountryCode() {
    return needsCountryCode;
  }

  public void setNeedsCountryCode(RequiredField needsCountryCode) {
    this.needsCountryCode = needsCountryCode;
  }

  public void addIdentificationTypes(BuyerIdentificationType buyerIdentificationType) {
    if (identificationTypes == null) {
      identificationTypes = new ArrayList<>();
    }

    identificationTypes.add(buyerIdentificationType);
  }
}
