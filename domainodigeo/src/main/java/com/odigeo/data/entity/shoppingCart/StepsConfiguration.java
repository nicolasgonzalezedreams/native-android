package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class StepsConfiguration implements Serializable {

  private Step step;
  private PricingMode totalPriceMode;

  public Step getStep() {
    return step;
  }

  public void setStep(Step step) {
    this.step = step;
  }

  public PricingMode getTotalPriceMode() {
    return totalPriceMode;
  }

  public void setTotalPriceMode(PricingMode totalPriceMode) {
    this.totalPriceMode = totalPriceMode;
  }
}