package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class FlightFareInformationPassenger implements Serializable {

  private String bookingClass;
  private TravellerType paxType;
  private String fareBasisId;
  private boolean resident;
  private FareType fareType;
  private AgreementsType agreementsType;
  private String fareRules;

  public String getBookingClass() {
    return bookingClass;
  }

  public void setBookingClass(String bookingClass) {
    this.bookingClass = bookingClass;
  }

  public TravellerType getPaxType() {
    return paxType;
  }

  public void setPaxType(TravellerType paxType) {
    this.paxType = paxType;
  }

  public String getFareBasisId() {
    return fareBasisId;
  }

  public void setFareBasisId(String fareBasisId) {
    this.fareBasisId = fareBasisId;
  }

  public boolean isResident() {
    return resident;
  }

  public void setResident(boolean resident) {
    this.resident = resident;
  }

  public FareType getFareType() {
    return fareType;
  }

  public void setFareType(FareType fareType) {
    this.fareType = fareType;
  }

  public AgreementsType getAgreementsType() {
    return agreementsType;
  }

  public void setAgreementsType(AgreementsType agreementsType) {
    this.agreementsType = agreementsType;
  }

  public String getFareRules() {
    return fareRules;
  }

  public void setFareRules(String fareRules) {
    this.fareRules = fareRules;
  }
}
