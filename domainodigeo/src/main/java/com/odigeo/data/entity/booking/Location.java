package com.odigeo.data.entity.booking;

import java.io.Serializable;

public class Location implements Serializable {

  private String mCityIATACode;
  private String mCityName;
  private String mCountry;
  private long mGeoNodeId;
  private long mLatitude;
  private String mLocationCode;
  private String mLocationName;
  private String mLocationType;
  private long mLongitude;
  private String mMatchName;

  public Location() {
    //empty
  }

  public Location(long geoNodeId) {
    mGeoNodeId = geoNodeId;
  }

  public Location(long geoNodeId, String cityIATACode, String cityName, String country,
      String matchName, String locationName, String locationType, String locationCode) {
    mGeoNodeId = geoNodeId;
    mCityIATACode = cityIATACode;
    mCityName = cityName;
    mCountry = country;
    mMatchName = matchName;
    mLocationName = locationName;
    mLocationType = locationType;
    mLocationCode = locationCode;
  }

  public Location(String cityIATACode, String cityName, String country, long geoNodeId,
      long latitude, String locationCode, String locationName, String locationType, long longitude,
      String matchName) {
    mCityIATACode = cityIATACode;
    mCityName = cityName;
    mCountry = country;
    mGeoNodeId = geoNodeId;
    mLatitude = latitude;
    mLocationCode = locationCode;
    mLocationName = locationName;
    mLocationType = locationType;
    mLongitude = longitude;
    mMatchName = matchName;
  }

  public String getCityIATACode() {
    return mCityIATACode;
  }

  public String getCityName() {
    return mCityName;
  }

  public void setCityName(String city) {
    this.mCityName = city;
  }

  public String getCountry() {
    return mCountry;
  }

  public long getGeoNodeId() {
    return mGeoNodeId;
  }

  public void setGeoNodeId(long geoNodeId) {
    this.mGeoNodeId = geoNodeId;
  }

  public long getLatitude() {
    return mLatitude;
  }

  public String getLocationCode() {
    return mLocationCode;
  }

  public void setLocationCode(String mLocationCode) {
    this.mLocationCode = mLocationCode;
  }

  public String getLocationName() {
    return mLocationName;
  }

  public String getLocationType() {
    return mLocationType;
  }

  public long getLongitude() {
    return mLongitude;
  }

  public String getMatchName() {
    return mMatchName;
  }
}
