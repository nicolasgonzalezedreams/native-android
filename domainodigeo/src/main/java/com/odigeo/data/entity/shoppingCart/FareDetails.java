package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;

public class FareDetails implements Serializable {

  private PassengerTypeFare adultFares;
  private PassengerTypeFare childFares;
  private PassengerTypeFare infantFares;
  private BigDecimal fare;
  private BigDecimal tax;

  public PassengerTypeFare getAdultFares() {
    return adultFares;
  }

  public void setAdultFares(PassengerTypeFare adultFares) {
    this.adultFares = adultFares;
  }

  public PassengerTypeFare getChildFares() {
    return childFares;
  }

  public void setChildFares(PassengerTypeFare childFares) {
    this.childFares = childFares;
  }

  public PassengerTypeFare getInfantFares() {
    return infantFares;
  }

  public void setInfantFares(PassengerTypeFare infantFares) {
    this.infantFares = infantFares;
  }

  public BigDecimal getFare() {
    return fare;
  }

  public void setFare(BigDecimal fare) {
    this.fare = fare;
  }

  public BigDecimal getTax() {
    return tax;
  }

  public void setTax(BigDecimal tax) {
    this.tax = tax;
  }
}
