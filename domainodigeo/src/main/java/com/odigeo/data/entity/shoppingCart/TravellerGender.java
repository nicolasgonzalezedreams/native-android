package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum TravellerGender implements Serializable {
  MALE, FEMALE;

  public static TravellerGender fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
