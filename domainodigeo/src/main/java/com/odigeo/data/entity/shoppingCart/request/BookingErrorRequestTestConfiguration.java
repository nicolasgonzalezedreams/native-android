package com.odigeo.data.entity.shoppingCart.request;

import com.odigeo.data.entity.shoppingCart.ErrorTypeTestConfiguration;

class BookingErrorRequestTestConfiguration {

  private String providerId;
  private ErrorTypeTestConfiguration errorType;

  public String getProviderId() {
    return providerId;
  }

  public void setProviderId(String providerId) {
    this.providerId = providerId;
  }

  public ErrorTypeTestConfiguration getErrorType() {
    return errorType;
  }

  public void setErrorType(ErrorTypeTestConfiguration errorType) {
    this.errorType = errorType;
  }
}