package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum RequiredField implements Serializable {
  MANDATORY, OPTIONAL, NOT_REQUIRED;

  public static RequiredField fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }
}
