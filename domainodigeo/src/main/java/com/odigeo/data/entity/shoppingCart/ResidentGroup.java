package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum ResidentGroup implements Serializable {
  CANARIAS("mslresidentgroupcontainer_residentgroupnamecanaryislands"), BALEARES(
      "mslresidentgroupcontainer_residentgroupnamebalearicislands"), CEUTA(
      "mslresidentgroupcontainer_residentgroupnameceuta"), MELILLA(
      "mslresidentgroupcontainer_residentgroupnamemelilla");

  private final String mText;

  ResidentGroup(String text) {
    mText = text;
  }

  public static ResidentGroup fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

  @Override public String toString() {
    return mText;
  }
}
