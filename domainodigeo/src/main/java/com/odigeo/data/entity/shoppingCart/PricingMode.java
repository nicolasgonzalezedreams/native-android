package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum PricingMode implements Serializable {
  PER_PASSENGER, TOTAL, BOTH, PER_PASSENGER_TYPE
}