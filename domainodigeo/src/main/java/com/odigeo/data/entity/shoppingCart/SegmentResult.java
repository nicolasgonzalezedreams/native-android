package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class SegmentResult implements Serializable {

  private Segment segment;
  private int id;

  public Segment getSegment() {
    return segment;
  }

  public void setSegment(Segment segment) {
    this.segment = segment;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}
