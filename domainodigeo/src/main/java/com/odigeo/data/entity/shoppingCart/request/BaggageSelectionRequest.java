package com.odigeo.data.entity.shoppingCart.request;

import com.odigeo.data.entity.shoppingCart.BaggageDescriptor;
import com.odigeo.data.entity.shoppingCart.SegmentTypeIndex;

public class BaggageSelectionRequest {

  private BaggageDescriptor baggageDescriptor;
  private SegmentTypeIndex segmentTypeIndex;

  public BaggageDescriptor getBaggageDescriptor() {
    return baggageDescriptor;
  }

  public void setBaggageDescriptor(BaggageDescriptor baggageDescriptor) {
    this.baggageDescriptor = baggageDescriptor;
  }

  public SegmentTypeIndex getSegmentTypeIndex() {
    return segmentTypeIndex;
  }

  public void setSegmentTypeIndex(SegmentTypeIndex segmentTypeIndex) {
    this.segmentTypeIndex = segmentTypeIndex;
  }
}
