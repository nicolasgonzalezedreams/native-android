package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class Buyer implements Serializable {
  private Phone phoneNumber;
  private Phone alternativePhoneNumber;
  private String name;
  private String lastNames;
  private String mail;
  private BuyerIdentificationType buyerIdentificationType;
  private String identification;
  private String dateOfBirth;
  private String cpf;
  private String address;
  private String cityName;
  private String neighborhood;
  private String stateName;
  private String zipCode;
  private String country;

  public Phone getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(Phone phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public Phone getAlternativePhoneNumber() {
    return alternativePhoneNumber;
  }

  public void setAlternativePhoneNumber(Phone alternativePhoneNumber) {
    this.alternativePhoneNumber = alternativePhoneNumber;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLastNames() {
    return lastNames;
  }

  public void setLastNames(String lastNames) {
    this.lastNames = lastNames;
  }

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public BuyerIdentificationType getBuyerIdentificationType() {
    return buyerIdentificationType;
  }

  public void setBuyerIdentificationType(BuyerIdentificationType buyerIdentificationType) {
    this.buyerIdentificationType = buyerIdentificationType;
  }

  public String getIdentification() {
    return identification;
  }

  public void setIdentification(String identification) {
    this.identification = identification;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getCpf() {
    return cpf;
  }

  public void setCpf(String cpf) {
    this.cpf = cpf;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public String getNeighborhood() {
    return neighborhood;
  }

  public void setNeighborhood(String neighborhood) {
    this.neighborhood = neighborhood;
  }

  public String getStateName() {
    return stateName;
  }

  public void setStateName(String stateName) {
    this.stateName = stateName;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }
}
