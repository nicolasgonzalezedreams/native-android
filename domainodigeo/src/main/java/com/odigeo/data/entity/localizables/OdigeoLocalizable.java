package com.odigeo.data.entity.localizables;

import java.io.Serializable;

public class OdigeoLocalizable implements Serializable {

  public String localizableKey;
  public String localizableValue;

  public String getLocalizableKey() {
    return localizableKey;
  }

  public void setLocalizableKey(String localizableKey) {
    this.localizableKey = localizableKey;
  }

  public String getLocalizableValue() {
    return localizableValue;
  }

  public void setLocalizableValue(String localizableValue) {
    this.localizableValue = localizableValue;
  }
}
