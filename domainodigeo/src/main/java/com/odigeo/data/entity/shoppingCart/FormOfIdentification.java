package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class FormOfIdentification implements Serializable {

  private String value;
  private FormOfIdentificationType type;
  private String typeCode;

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public FormOfIdentificationType getType() {
    return type;
  }

  public void setType(FormOfIdentificationType type) {
    this.type = type;
  }

  public String getTypeCode() {
    return typeCode;
  }

  public void setTypeCode(String typeCode) {
    this.typeCode = typeCode;
  }
}
