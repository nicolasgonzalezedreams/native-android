package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class ItinerariesLegend implements Serializable {

  private List<Carrier> carriers;
  private List<Location> locations;
  private List<SectionResult> sectionResults;
  private List<SegmentResult> segmentResults;
  private List<FlightFareInformationPassengerResult> fareInfoPaxResults;
  private List<CollectionEstimationFees> collectionEstimationFees;
  private List<CollectionMethodLegend> collectionMethods;
  private List<BaggageEstimationFees> baggageEstimationFees;
  private Itineraries itineraries;
  private ResidentsValidation residentsValidation;

  public List<Carrier> getCarriers() {
    return carriers;
  }

  public void setCarriers(List<Carrier> carriers) {
    this.carriers = carriers;
  }

  public List<Location> getLocations() {
    return locations;
  }

  public void setLocations(List<Location> locations) {
    this.locations = locations;
  }

  public List<SectionResult> getSectionResults() {
    return sectionResults;
  }

  public void setSectionResults(List<SectionResult> sectionResults) {
    this.sectionResults = sectionResults;
  }

  public List<SegmentResult> getSegmentResults() {
    return segmentResults;
  }

  public void setSegmentResults(List<SegmentResult> segmentResults) {
    this.segmentResults = segmentResults;
  }

  public List<FlightFareInformationPassengerResult> getFareInfoPaxResults() {
    return fareInfoPaxResults;
  }

  public void setFareInfoPaxResults(List<FlightFareInformationPassengerResult> fareInfoPaxResults) {
    this.fareInfoPaxResults = fareInfoPaxResults;
  }

  public List<CollectionEstimationFees> getCollectionEstimationFees() {
    return collectionEstimationFees;
  }

  public void setCollectionEstimationFees(List<CollectionEstimationFees> collectionEstimationFees) {
    this.collectionEstimationFees = collectionEstimationFees;
  }

  public List<CollectionMethodLegend> getCollectionMethods() {
    return collectionMethods;
  }

  public void setCollectionMethods(List<CollectionMethodLegend> collectionMethods) {
    this.collectionMethods = collectionMethods;
  }

  public List<BaggageEstimationFees> getBaggageEstimationFees() {
    return baggageEstimationFees;
  }

  public void setBaggageEstimationFees(List<BaggageEstimationFees> baggageEstimationFees) {
    this.baggageEstimationFees = baggageEstimationFees;
  }

  public Itineraries getItineraries() {
    return itineraries;
  }

  public void setItineraries(Itineraries itineraries) {
    this.itineraries = itineraries;
  }

  public ResidentsValidation getResidentsValidation() {
    return residentsValidation;
  }

  public void setResidentsValidation(ResidentsValidation residentsValidation) {
    this.residentsValidation = residentsValidation;
  }
}
