package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;

public class AdditionalProductShoppingItem implements Serializable {

  private AdditionalProduct additionalProduct;
  private BigDecimal totalPrice;
  private long id;

  public AdditionalProduct getAdditionalProduct() {
    return additionalProduct;
  }

  public void setAdditionalProduct(AdditionalProduct additionalProduct) {
    this.additionalProduct = additionalProduct;
  }

  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
