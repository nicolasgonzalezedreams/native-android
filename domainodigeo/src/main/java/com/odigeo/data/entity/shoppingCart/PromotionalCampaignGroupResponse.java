package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;

public class PromotionalCampaignGroupResponse implements Serializable {

  private String id;
  private String description;
  private BigDecimal maxAccumulativeDiscount;
  private String currency;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public BigDecimal getMaxAccumulativeDiscount() {
    return maxAccumulativeDiscount;
  }

  public void setMaxAccumulativeDiscount(BigDecimal maxAccumulativeDiscount) {
    this.maxAccumulativeDiscount = maxAccumulativeDiscount;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }
}
