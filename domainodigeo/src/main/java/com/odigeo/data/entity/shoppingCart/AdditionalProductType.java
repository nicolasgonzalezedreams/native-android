package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum AdditionalProductType implements Serializable {

  HOTEL, SMS, INSURANCE, SERVICE_PACK, PARKING, OTHER;

  public static AdditionalProductType fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
