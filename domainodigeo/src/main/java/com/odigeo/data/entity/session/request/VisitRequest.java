package com.odigeo.data.entity.session.request;

import com.odigeo.configuration.ABAlias;
import java.util.ArrayList;
import java.util.List;

public class VisitRequest {

  private List<String> testAssignments;

  public VisitRequest() {
    testAssignments = new ArrayList<>();
    for (ABAlias abAlias : ABAlias.values()) {
      testAssignments.add(abAlias.value());
    }
  }

  public List<String> getTestAssignments() {
    return testAssignments;
  }
}
