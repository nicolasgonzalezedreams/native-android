package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum BuyerIdentificationType implements Serializable {

  NATIONAL_ID_CARD, NIE, CIF, PASSPORT, NIF, BIRTH_DATE;

  public static BuyerIdentificationType fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
