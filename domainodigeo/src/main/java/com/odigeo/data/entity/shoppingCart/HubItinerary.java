package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class HubItinerary implements Serializable {

  private Integer id;
  private Integer oth;
  private Integer htd;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getOth() {
    return oth;
  }

  public void setOth(Integer oth) {
    this.oth = oth;
  }

  public Integer getHtd() {
    return htd;
  }

  public void setHtd(Integer htd) {
    this.htd = htd;
  }
}
