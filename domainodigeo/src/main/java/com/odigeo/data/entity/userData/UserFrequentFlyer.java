package com.odigeo.data.entity.userData;

import java.io.Serializable;

public class UserFrequentFlyer implements Serializable {

  private long mId;
  private long mFrequentFlyerId;
  private String mAirlineCode;
  private String mFrequentFlyerNumber;
  private long mUserTravellerId;

  public UserFrequentFlyer() {
    //empty
  }

  public UserFrequentFlyer(long id, long frequentFlyerId, String airlineCode,
      String frequentFlyerNumber, long userTravellerId) {
    mId = id;
    mFrequentFlyerId = frequentFlyerId;
    mAirlineCode = airlineCode;
    mFrequentFlyerNumber = frequentFlyerNumber;
    mUserTravellerId = userTravellerId;
  }

  public long getId() {
    return mId;
  }

  public void setId(long mId) {
    this.mId = mId;
  }

  public long getFrequentFlyerId() {
    return mFrequentFlyerId;
  }

  public void setFrequentFlyerId(long mFrequentFlyerId) {
    this.mFrequentFlyerId = mFrequentFlyerId;
  }

  public String getAirlineCode() {
    return mAirlineCode;
  }

  public void setAirlineCode(String mAirlineCode) {
    this.mAirlineCode = mAirlineCode;
  }

  public String getFrequentFlyerNumber() {
    return mFrequentFlyerNumber;
  }

  public void setFrequentFlyerNumber(String mFrequentFlyerNumber) {
    this.mFrequentFlyerNumber = mFrequentFlyerNumber;
  }

  public long getUserTravellerId() {
    return mUserTravellerId;
  }

  public void setUserTravellerId(long mUserTravellerId) {
    this.mUserTravellerId = mUserTravellerId;
  }

  @Override public boolean equals(Object obj) {
    return obj != null
        && obj instanceof UserFrequentFlyer
        && getAirlineCode().equals(((UserFrequentFlyer) obj).getAirlineCode())
        && getFrequentFlyerId() == ((UserFrequentFlyer) obj).getFrequentFlyerId();
  }
}
