package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class ResidentValidationRetry implements Serializable {

  private List<ResidentValidationStatus> residentValidations;
  private int numberMaximumOfRetries;
  private int numberOfAvailableRetries;

  public List<ResidentValidationStatus> getResidentValidations() {
    return residentValidations;
  }

  public void setResidentValidations(List<ResidentValidationStatus> residentValidations) {
    this.residentValidations = residentValidations;
  }

  public int getNumberMaximumOfRetries() {
    return numberMaximumOfRetries;
  }

  public void setNumberMaximumOfRetries(int numberMaximumOfRetries) {
    this.numberMaximumOfRetries = numberMaximumOfRetries;
  }

  public int getNumberOfAvailableRetries() {
    return numberOfAvailableRetries;
  }

  public void setNumberOfAvailableRetries(int numberOfAvailableRetries) {
    this.numberOfAvailableRetries = numberOfAvailableRetries;
  }
}
