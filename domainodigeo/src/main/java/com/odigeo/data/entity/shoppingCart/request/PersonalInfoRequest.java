package com.odigeo.data.entity.shoppingCart.request;

import com.odigeo.data.entity.shoppingCart.ItinerarySortCriteria;
import com.odigeo.data.entity.shoppingCart.Step;
import java.util.List;

public class PersonalInfoRequest {

  private BuyerRequest buyer;
  private List<TravellerRequest> travellerRequests;
  private long bookingId;
  private ItinerarySortCriteria sortCriteria;
  private Step step;
  private Boolean skipResidentValidationRetries;

  public PersonalInfoRequest(BuyerRequest buyer, List<TravellerRequest> travellerRequests,
      long bookingId, ItinerarySortCriteria sortCriteria, Step step,
      Boolean skipResidentValidationRetries) {
    this.buyer = buyer;
    this.travellerRequests = travellerRequests;
    this.bookingId = bookingId;
    this.sortCriteria = sortCriteria;
    this.step = step;
    this.skipResidentValidationRetries = skipResidentValidationRetries;
  }

  public PersonalInfoRequest(BuyerRequest buyer, List<TravellerRequest> travellerRequests,
      long bookingId, ItinerarySortCriteria sortCriteria, Step step) {
    this.buyer = buyer;
    this.travellerRequests = travellerRequests;
    this.bookingId = bookingId;
    this.sortCriteria = sortCriteria;
    this.step = step;
  }

  public BuyerRequest getBuyer() {
    return buyer;
  }

  public void setBuyer(BuyerRequest buyer) {
    this.buyer = buyer;
  }

  public List<TravellerRequest> getTravellerRequests() {
    return travellerRequests;
  }

  public void setTravellerRequests(List<TravellerRequest> travellerRequests) {
    this.travellerRequests = travellerRequests;
  }

  public long getBookingId() {
    return bookingId;
  }

  public void setBookingId(long bookingId) {
    this.bookingId = bookingId;
  }

  public ItinerarySortCriteria getSortCriteria() {
    return sortCriteria;
  }

  public void setSortCriteria(ItinerarySortCriteria sortCriteria) {
    this.sortCriteria = sortCriteria;
  }

  public Step getStep() {
    return step;
  }

  public void setStep(Step step) {
    this.step = step;
  }

  public Boolean getSkipResidentValidationRetries() {
    return skipResidentValidationRetries;
  }

  public void setSkipResidentValidationRetries(Boolean skipResidentValidationRetries) {
    this.skipResidentValidationRetries = skipResidentValidationRetries;
  }
}
