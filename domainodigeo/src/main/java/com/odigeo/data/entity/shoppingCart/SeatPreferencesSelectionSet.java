package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class SeatPreferencesSelectionSet implements Serializable {

  private List<AmbienceSeatPreferencesSelection> ambienceSeatPreferencesSelections;
  private List<SeatZonePreferencesSelection> seatZonePreferencesSelections;

  public List<AmbienceSeatPreferencesSelection> getAmbienceSeatPreferencesSelections() {
    return ambienceSeatPreferencesSelections;
  }

  public void setAmbienceSeatPreferencesSelections(
      List<AmbienceSeatPreferencesSelection> ambienceSeatPreferencesSelections) {
    this.ambienceSeatPreferencesSelections = ambienceSeatPreferencesSelections;
  }

  public List<SeatZonePreferencesSelection> getSeatZonePreferencesSelections() {
    return seatZonePreferencesSelections;
  }

  public void setSeatZonePreferencesSelections(
      List<SeatZonePreferencesSelection> seatZonePreferencesSelections) {
    this.seatZonePreferencesSelections = seatZonePreferencesSelections;
  }
}
