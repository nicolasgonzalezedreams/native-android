package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class FormOfIdentificationDescriptor implements Serializable {
  private List<FormOfIdentificationType> types;

  public List<FormOfIdentificationType> getTypes() {
    return types;
  }

  public void setTypes(List<FormOfIdentificationType> types) {
    this.types = types;
  }
}
