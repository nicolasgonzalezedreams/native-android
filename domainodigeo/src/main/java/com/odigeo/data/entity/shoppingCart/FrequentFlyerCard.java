package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class FrequentFlyerCard implements Serializable {
  private String carrier;
  private String number;

  public String getCarrier() {
    return carrier;
  }

  public void setCarrier(String carrier) {
    this.carrier = carrier;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }
}