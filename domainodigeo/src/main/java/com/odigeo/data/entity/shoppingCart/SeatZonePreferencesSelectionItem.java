package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class SeatZonePreferencesSelectionItem implements Serializable {

  private int numPassenger;
  private String rowSelection;
  private String columnSelection;
  private List<String> assignedSeats;
  private BigDecimal providerPrice;
  private BigDecimal totalPrice;

  public int getNumPassenger() {
    return numPassenger;
  }

  public void setNumPassenger(int numPassenger) {
    this.numPassenger = numPassenger;
  }

  public String getRowSelection() {
    return rowSelection;
  }

  public void setRowSelection(String rowSelection) {
    this.rowSelection = rowSelection;
  }

  public String getColumnSelection() {
    return columnSelection;
  }

  public void setColumnSelection(String columnSelection) {
    this.columnSelection = columnSelection;
  }

  public List<String> getAssignedSeats() {
    return assignedSeats;
  }

  public void setAssignedSeats(List<String> assignedSeats) {
    this.assignedSeats = assignedSeats;
  }

  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  public void setProviderPrice(BigDecimal providerPrice) {
    this.providerPrice = providerPrice;
  }

  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }
}
