package com.odigeo.data.entity.carrousel;

/**
 * Created by Jaime Toca on 25/1/16.
 */
public class BoardingGateCard extends SectionCard {

  private String gate;

  public BoardingGateCard(long id, CardType type, int priority, String image, String carrier,
      String carrierName, String flightId, CarouselLocation from, CarouselLocation to,
      String gate) {
    super(id, type, priority, image, carrier, carrierName, flightId, from, to);
    this.gate = gate;
  }

  public BoardingGateCard() {

  }

  public String getGate() {

    return gate;
  }

  public void setGate(final String gate) {
    this.gate = gate;
  }
}
