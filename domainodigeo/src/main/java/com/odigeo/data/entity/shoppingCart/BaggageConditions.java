package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BaggageConditions implements Serializable {

  private BaggageDescriptor baggageDescriptorIncludedInPrice;
  private List<SelectableBaggageDescriptor> selectableBaggageDescriptor;
  private BaggageApplicationLevel baggageApplicationLevel;
  private SegmentTypeIndex segmentTypeIndex;

  public BaggageDescriptor getBaggageDescriptorIncludedInPrice() {
    return baggageDescriptorIncludedInPrice;
  }

  public void setBaggageDescriptorIncludedInPrice(
      BaggageDescriptor baggageDescriptorIncludedInPrice) {
    this.baggageDescriptorIncludedInPrice = baggageDescriptorIncludedInPrice;
  }

  public List<SelectableBaggageDescriptor> getSelectableBaggageDescriptor() {
    return selectableBaggageDescriptor;
  }

  public void setSelectableBaggageDescriptor(
      List<SelectableBaggageDescriptor> selectableBaggageDescriptor) {
    this.selectableBaggageDescriptor = selectableBaggageDescriptor;
  }

  public BaggageApplicationLevel getBaggageApplicationLevel() {
    return baggageApplicationLevel;
  }

  public void setBaggageApplicationLevel(BaggageApplicationLevel baggageApplicationLevel) {
    this.baggageApplicationLevel = baggageApplicationLevel;
  }

  public SegmentTypeIndex getSegmentTypeIndex() {
    return segmentTypeIndex;
  }

  public void setSegmentTypeIndex(SegmentTypeIndex segmentTypeIndex) {
    this.segmentTypeIndex = segmentTypeIndex;
  }

  public void addSelectableBaggageDescriptor(
      SelectableBaggageDescriptor selectableBaggageDescriptor) {
    if (this.selectableBaggageDescriptor == null) {
      this.selectableBaggageDescriptor = new ArrayList<>();
    }
    this.selectableBaggageDescriptor.add(selectableBaggageDescriptor);
  }
}
