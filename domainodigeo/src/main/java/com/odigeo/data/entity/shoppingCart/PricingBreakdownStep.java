package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class PricingBreakdownStep implements Serializable {
  private Step step;
  private BigDecimal totalPrice;
  private List<PricingBreakdownItem> pricingBreakdownItems;

  public Step getStep() {
    return step;
  }

  public void setStep(Step step) {
    this.step = step;
  }

  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }

  public List<PricingBreakdownItem> getPricingBreakdownItems() {
    return pricingBreakdownItems;
  }

  public void setPricingBreakdownItems(List<PricingBreakdownItem> pricingBreakdownItems) {
    this.pricingBreakdownItems = pricingBreakdownItems;
  }
}
