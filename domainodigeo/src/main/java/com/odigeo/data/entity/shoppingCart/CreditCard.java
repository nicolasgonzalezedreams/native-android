package com.odigeo.data.entity.shoppingCart;

import com.odigeo.data.entity.user.CreditCardType;
import java.util.Date;

public class CreditCard {

  private String creditCardNumber;
  private String expiryMonth;
  private String expiryYear;
  private Long id;
  private String owner;
  private Date creationDate;
  private Date modifiedDate;
  private Date lastUsageDate;
  private Boolean isDefault;
  private CreditCardType creditCardTypeId;

  public String getCreditCardNumber() {
    return creditCardNumber;
  }

  public void setCreditCardNumber(String creditCardNumber) {
    this.creditCardNumber = creditCardNumber;
  }

  public String getExpiryMonth() {
    return expiryMonth;
  }

  public void setExpiryMonth(String expiryMonth) {
    this.expiryMonth = expiryMonth;
  }

  public String getExpiryYear() {
    return expiryYear;
  }

  public void setExpiryYear(String expiryYear) {
    this.expiryYear = expiryYear;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public Date getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }

  public Date getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Date getLastUsageDate() {
    return lastUsageDate;
  }

  public void setLastUsageDate(Date lastUsageDate) {
    this.lastUsageDate = lastUsageDate;
  }

  public Boolean getDefault() {
    return isDefault;
  }

  public void setDefault(Boolean aDefault) {
    isDefault = aDefault;
  }

  public CreditCardType getCreditCardTypeId() {
    return creditCardTypeId;
  }

  public void setCreditCardTypeId(CreditCardType creditCardTypeId) {
    this.creditCardTypeId = creditCardTypeId;
  }
}