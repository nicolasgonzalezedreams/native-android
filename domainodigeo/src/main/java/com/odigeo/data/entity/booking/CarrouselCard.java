package com.odigeo.data.entity.booking;

/**
 * @author José Eduardo Cabrera Rivera
 * @version 1.0
 * @since 11/11/2015.
 */
public class CarrouselCard {

  private long id;
  private TypeOfCard type;
  private String title;
  private String subtitle;
  private int priority;
  private String image;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public TypeOfCard getType() {
    return type;
  }

  public void setType(TypeOfCard type) {
    this.type = type;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle;
  }

  public int getPriority() {
    return priority;
  }

  public void setPriority(int priority) {
    this.priority = priority;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public enum TypeOfCard {
    PROMOTION, HOME, UNKNOWN
  }
}
