package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class HidingItinerary implements Serializable {

  private Integer id;
  private Integer itinerary;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getItinerary() {
    return itinerary;
  }

  public void setItinerary(Integer itinerary) {
    this.itinerary = itinerary;
  }
}
