package com.odigeo.data.entity.contract;

import java.io.Serializable;
import java.util.Map;

public class UserInteractionNeededResponse implements Serializable {

  private String redirectUrl;
  private FormSendTypeDTO formSendType;
  private String htmlCode;
  private Map<String, String> parameters;

  public String getRedirectUrl() {
    return redirectUrl;
  }

  public void setRedirectUrl(String redirectUrl) {
    this.redirectUrl = redirectUrl;
  }

  public FormSendTypeDTO getFormSendType() {
    return formSendType;
  }

  public void setFormSendType(FormSendTypeDTO formSendType) {
    this.formSendType = formSendType;
  }

  public String getHtmlCode() {
    return htmlCode;
  }

  public void setHtmlCode(String htmlCode) {
    this.htmlCode = htmlCode;
  }

  public Map<String, String> getParameters() {
    return parameters;
  }

  public void setParameters(Map<String, String> parameters) {
    this.parameters = parameters;
  }

  public boolean isGet() {
    return formSendType == FormSendTypeDTO.GET;
  }

  public boolean isPost() {
    return formSendType == FormSendTypeDTO.POST;
  }
}