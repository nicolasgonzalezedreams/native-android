package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class SeatPreferencesDescriptorSet implements Serializable {

  private List<AmbienceSeatPreferencesDescriptor> ambienceSeatPreferencesDescriptors;
  private List<SeatZonePreferencesDescriptor> seatZonePreferencesDescriptors;

  public List<AmbienceSeatPreferencesDescriptor> getAmbienceSeatPreferencesDescriptors() {
    return ambienceSeatPreferencesDescriptors;
  }

  public void setAmbienceSeatPreferencesDescriptors(
      List<AmbienceSeatPreferencesDescriptor> ambienceSeatPreferencesDescriptors) {
    this.ambienceSeatPreferencesDescriptors = ambienceSeatPreferencesDescriptors;
  }

  public List<SeatZonePreferencesDescriptor> getSeatZonePreferencesDescriptors() {
    return seatZonePreferencesDescriptors;
  }

  public void setSeatZonePreferencesDescriptors(
      List<SeatZonePreferencesDescriptor> seatZonePreferencesDescriptors) {
    this.seatZonePreferencesDescriptors = seatZonePreferencesDescriptors;
  }
}
