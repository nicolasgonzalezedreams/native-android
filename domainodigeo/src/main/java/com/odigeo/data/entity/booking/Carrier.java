package com.odigeo.data.entity.booking;

import java.io.Serializable;
import java.util.List;

public class Carrier implements Serializable {

  private long mId;
  private String mCode;
  private String mName;

  //Relations
  private List<Segment> mMainSegments;
  private List<Section> mSections;

  public Carrier() {
    //empty
  }

  public Carrier(long id) {
    mId = id;
  }

  public Carrier(long id, String code, String name) {
    mId = id;
    mCode = code;
    mName = name;
  }

  public Carrier(long id, String code, String name, List<Segment> mainSegments,
      List<Section> sections) {
    mId = id;
    mCode = code;
    mName = name;
    mMainSegments = mainSegments;
    mSections = sections;
  }

  public long getId() {
    return mId;
  }

  public void setId(long id) {
    mId = id;
  }

  public String getCode() {
    return mCode;
  }

  public String getName() {
    return mName;
  }

  public List<Segment> getMainSegments() {
    return mMainSegments;
  }

  public List<Section> getSections() {
    return mSections;
  }

  @Override public boolean equals(Object obj) {
    return obj instanceof Carrier && ((Carrier) obj).mCode.equals(mCode);
  }
}
