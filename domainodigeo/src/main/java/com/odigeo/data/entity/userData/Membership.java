package com.odigeo.data.entity.userData;

import java.io.Serializable;

public class Membership implements Serializable {

  protected long memberId;
  protected String firstName;
  protected String lastNames;
  protected String website;

  public long getMemberId() {
    return memberId;
  }

  public void setMemberId(long memberId) {
    this.memberId = memberId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastNames() {
    return lastNames;
  }

  public void setLastNames(String lastNames) {
    this.lastNames = lastNames;
  }

  public String getWebsite() {
    return website;
  }

  public void setWebsite(String market) {
    this.website = market;
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Membership)) return false;

    Membership that = (Membership) o;

    if (getMemberId() != that.getMemberId()) return false;
    if (getFirstName() != null ? !getFirstName().equals(that.getFirstName())
        : that.getFirstName() != null) {
      return false;
    }
    if (getLastNames() != null ? !getLastNames().equals(that.getLastNames())
        : that.getLastNames() != null) {
      return false;
    }
    return getWebsite() != null ? getWebsite().equals(that.getWebsite())
        : that.getWebsite() == null;
  }

  @Override public int hashCode() {
    int result = (int) (getMemberId() ^ (getMemberId() >>> 32));
    result = 31 * result + (getFirstName() != null ? getFirstName().hashCode() : 0);
    result = 31 * result + (getLastNames() != null ? getLastNames().hashCode() : 0);
    result = 31 * result + (getWebsite() != null ? getWebsite().hashCode() : 0);
    return result;
  }
}
