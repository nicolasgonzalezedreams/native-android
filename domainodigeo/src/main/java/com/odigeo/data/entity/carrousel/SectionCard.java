package com.odigeo.data.entity.carrousel;

public class SectionCard extends Card {

  private String carrier;
  private String carrierName;
  private String flightId;
  private CarouselLocation from;
  private CarouselLocation to;
  private String tripToHeader;
  private boolean hasToShowRefreshButton = true;

  public SectionCard(long id, CardType type, int priority, String image, String carrier,
      String carrierName, String flightId, CarouselLocation from, CarouselLocation to) {
    super(id, type, priority, image);
    this.carrier = carrier;
    this.carrierName = carrierName;
    this.flightId = flightId;
    this.from = from;
    this.to = to;
  }

  public SectionCard() {

  }

  public String getCarrier() {
    return carrier;
  }

  public void setCarrier(final String carrier) {
    this.carrier = carrier;
  }

  public String getCarrierName() {
    return carrierName;
  }

  public void setCarrierName(final String carrierName) {
    this.carrierName = carrierName;
  }

  public String getFlightId() {
    return flightId;
  }

  public void setFlightId(final String flightId) {
    this.flightId = flightId;
  }

  public CarouselLocation getFrom() {
    return from;
  }

  public void setFrom(final CarouselLocation from) {
    this.from = from;
  }

  public CarouselLocation getTo() {
    return to;
  }

  public void setTo(final CarouselLocation to) {
    this.to = to;
  }

  public boolean hasToShowRefreshButton() {
    return hasToShowRefreshButton;
  }

  public void setHasToShowRefreshButton(boolean hasToShowRefreshButton) {
    this.hasToShowRefreshButton = hasToShowRefreshButton;
  }

  public String getTripToHeader() {
    return tripToHeader;
  }

  public void setTripToHeader(String cityTo) {
    this.tripToHeader = cityTo;
  }
}
