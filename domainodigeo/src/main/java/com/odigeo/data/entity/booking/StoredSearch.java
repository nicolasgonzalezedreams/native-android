package com.odigeo.data.entity.booking;

import java.io.Serializable;
import java.util.List;

/**
 * Created by matias.dirusso on 22/07/16.
 */
public class StoredSearch implements Serializable {

  private long mId;
  private long mStoredSearchId;
  private int mNumAdults;
  private int mNumChildren;
  private int mNumInfants;
  private boolean mIsDirectFlight;
  private CabinClass mCabinClass;
  private TripType mTripType;
  private boolean mIsSynchronized;
  private long mCreationDate;
  //Relations
  private List<SearchSegment> mSegmentList;
  private long mUserId;

  public StoredSearch() {
    //empty
  }

  public StoredSearch(long id, long storedSearchId, int numAdults, int numChildren, int numInfants,
      boolean isDirectFlight, CabinClass cabinClass, TripType tripType, boolean isSynchronized,
      long creationDate, long userId) {
    mId = id;
    mStoredSearchId = storedSearchId;
    mNumAdults = numAdults;
    mNumChildren = numChildren;
    mNumInfants = numInfants;
    mIsDirectFlight = isDirectFlight;
    mCabinClass = cabinClass;
    mTripType = tripType;
    mIsSynchronized = isSynchronized;
    mUserId = userId;
    mCreationDate = creationDate;
  }

  public StoredSearch(long id, long storedSearchId, int numAdults, int numChildren, int numInfants,
      CabinClass cabinClass, boolean isDirectFlight, TripType tripType, boolean isSynchronized,
      List<SearchSegment> segmentList, long creationDate, long userId) {
    mId = id;
    mStoredSearchId = storedSearchId;
    mNumAdults = numAdults;
    mNumChildren = numChildren;
    mNumInfants = numInfants;
    mIsDirectFlight = isDirectFlight;
    mCabinClass = cabinClass;
    mTripType = tripType;
    mIsSynchronized = isSynchronized;
    mSegmentList = segmentList;
    mUserId = userId;
    mCreationDate = creationDate;
  }

  public long getId() {
    return mId;
  }

  public void setId(long id) {
    this.mId = id;
  }

  public long getStoredSearchId() {
    return mStoredSearchId;
  }

  public void setStoredSearchId(long id) {
    this.mStoredSearchId = id;
  }

  public int getNumAdults() {
    return mNumAdults;
  }

  public int getNumChildren() {
    return mNumChildren;
  }

  public int getNumInfants() {
    return mNumInfants;
  }

  public boolean getIsDirectFlight() {
    return mIsDirectFlight;
  }

  public CabinClass getCabinClass() {
    return mCabinClass;
  }

  public TripType getTripType() {
    return mTripType;
  }

  public boolean isSynchronized() {
    return mIsSynchronized;
  }

  public void setSynchronized(boolean aSynchronized) {
    mIsSynchronized = aSynchronized;
  }

  public List<SearchSegment> getSegmentList() {
    return mSegmentList;
  }

  public long getUserId() {
    return mUserId;
  }

  public long getCreationDate() {
    return mCreationDate;
  }

  public enum CabinClass {
    BUSINESS, FIRST, PREMIUM_ECONOMY, ECONOMIC_DISCOUNTED, TOURIST, UNKNOWN
  }

  public enum TripType {
    O, R, M, UNKNOWN
  }
}
