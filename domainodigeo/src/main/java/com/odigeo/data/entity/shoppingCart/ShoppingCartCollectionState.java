package com.odigeo.data.entity.shoppingCart;

public enum ShoppingCartCollectionState {

  COLLECTED, PARTIALLY_COLLECTED, NOT_COLLECTED, UNKNOWN;

  public static ShoppingCartCollectionState fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

}
