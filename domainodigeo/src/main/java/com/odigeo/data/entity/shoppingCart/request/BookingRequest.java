package com.odigeo.data.entity.shoppingCart.request;

import com.odigeo.data.entity.shoppingCart.ItinerarySortCriteria;

public class BookingRequest {

  private PaymentDataRequest paymentData;
  private long bookingId;
  private String clientBookingReferenceId;
  private ItinerarySortCriteria sortCriteria;
  private InvoiceRequest invoice;

  public PaymentDataRequest getPaymentData() {
    return paymentData;
  }

  public void setPaymentData(PaymentDataRequest paymentData) {
    this.paymentData = paymentData;
  }

  public long getBookingId() {
    return bookingId;
  }

  public void setBookingId(long bookingId) {
    this.bookingId = bookingId;
  }

  public String getClientBookingReferenceId() {
    return clientBookingReferenceId;
  }

  public void setClientBookingReferenceId(String clientBookingReferenceId) {
    this.clientBookingReferenceId = clientBookingReferenceId;
  }

  public ItinerarySortCriteria getSortCriteria() {
    return sortCriteria;
  }

  public void setSortCriteria(ItinerarySortCriteria sortCriteria) {
    this.sortCriteria = sortCriteria;
  }

  public InvoiceRequest getInvoice() {
    return invoice;
  }

  public void setInvoice(InvoiceRequest invoice) {
    this.invoice = invoice;
  }
}