package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;

public class SelectableBaggageDescriptor implements Serializable {
  protected BaggageDescriptor baggageDescriptor;
  protected BigDecimal price;

  public SelectableBaggageDescriptor() {
    //empty
  }

  public SelectableBaggageDescriptor(BaggageDescriptor baggageDescriptor, BigDecimal price) {
    this.baggageDescriptor = baggageDescriptor;
    this.price = price;
  }

  public BaggageDescriptor getBaggageDescriptor() {
    return baggageDescriptor;
  }

  public void setBaggageDescriptor(BaggageDescriptor baggageDescriptor) {
    this.baggageDescriptor = baggageDescriptor;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }
}
