package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class BaggageDescriptor implements Serializable {

  private Integer pieces;
  private Integer kilos;

  public BaggageDescriptor() {
  }

  public BaggageDescriptor(Integer pieces, Integer kilos) {
    this.pieces = pieces;
    this.kilos = kilos;
  }

  public Integer getPieces() {
    if (pieces == null) {
      pieces = 0;
    }
    return pieces;
  }

  public void setPieces(Integer pieces) {
    this.pieces = pieces;
  }

  public Integer getKilos() {
    return kilos;
  }

  public void setKilos(Integer kilos) {
    this.kilos = kilos;
  }

  public boolean hasBaggageWithoutKilos() {
    return (pieces != null && pieces > 0) && (kilos == null || kilos == 0);
  }

  public boolean hasBaggageWithKilos() {
    return (pieces != null && pieces > 0) && (kilos != null && kilos > 0);
  }

  public boolean hasBaggageWithKilosAndWithoutPieces() {
    return (pieces == null || pieces == 0) && (kilos != null && kilos > 0);
  }

  @Override public boolean equals(Object object) {
    if (!(object instanceof BaggageDescriptor)) {
      return false;
    }

    BaggageDescriptor that = (BaggageDescriptor) object;

    return getPieces() == that.getPieces() && getKilos() == that.getKilos();
  }

  @Override public int hashCode() {
    int hashCode = 1;
    hashCode = hashCode * 37 + this.pieces.hashCode();
    hashCode = hashCode * 37 + this.kilos.hashCode();
    return hashCode;
  }
}
