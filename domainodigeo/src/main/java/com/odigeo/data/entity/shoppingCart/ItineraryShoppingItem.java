package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class ItineraryShoppingItem implements Serializable {

  private Itinerary itinerary;
  private BigDecimal price;
  private BigDecimal baggageFee;
  private String lastTicketingDate;
  private List<PassengerConflictDetails> passengersConflictDetails;
  private List<String> pnrs;
  private SeatPreferencesDescriptorSet seatPreferencesDescriptorSet;
  private SeatPreferencesSelectionSet seatPreferencesSelectionSet;

  public Itinerary getItinerary() {
    return itinerary;
  }

  public void setItinerary(Itinerary itinerary) {
    this.itinerary = itinerary;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getBaggageFee() {
    return baggageFee;
  }

  public void setBaggageFee(BigDecimal baggageFee) {
    this.baggageFee = baggageFee;
  }

  public String getLastTicketingDate() {
    return lastTicketingDate;
  }

  public void setLastTicketingDate(String lastTicketingDate) {
    this.lastTicketingDate = lastTicketingDate;
  }

  public List<PassengerConflictDetails> getPassengersConflictDetails() {
    return passengersConflictDetails;
  }

  public void setPassengersConflictDetails(
      List<PassengerConflictDetails> passengersConflictDetails) {
    this.passengersConflictDetails = passengersConflictDetails;
  }

  public List<String> getPnrs() {
    return pnrs;
  }

  public void setPnrs(List<String> pnrs) {
    this.pnrs = pnrs;
  }

  public SeatPreferencesDescriptorSet getSeatPreferencesDescriptorSet() {
    return seatPreferencesDescriptorSet;
  }

  public void setSeatPreferencesDescriptorSet(
      SeatPreferencesDescriptorSet seatPreferencesDescriptorSet) {
    this.seatPreferencesDescriptorSet = seatPreferencesDescriptorSet;
  }

  public SeatPreferencesSelectionSet getSeatPreferencesSelectionSet() {
    return seatPreferencesSelectionSet;
  }

  public void setSeatPreferencesSelectionSet(
      SeatPreferencesSelectionSet seatPreferencesSelectionSet) {
    this.seatPreferencesSelectionSet = seatPreferencesSelectionSet;
  }
}
