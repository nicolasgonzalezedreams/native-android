package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;

public class MarketingRevenueDataAdapter implements Serializable {

  public static final String ABSOLUTE_VALUE = "ABSOLUTE";

  private String type;
  private BigDecimal value;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public BigDecimal getValue() {
    return value;
  }

  public void setValue(BigDecimal value) {
    this.value = value;
  }
}