package com.odigeo.data.entity.geo;

import java.io.Serializable;

public class Coordinates implements Serializable {

  protected double latitude;
  protected double longitude;

  public Coordinates() {
  }

  public Coordinates(double latitude, double longitude) {
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }
}
