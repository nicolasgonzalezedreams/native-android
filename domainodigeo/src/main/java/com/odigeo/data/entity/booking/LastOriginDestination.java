package com.odigeo.data.entity.booking;

import java.io.Serializable;

public class LastOriginDestination extends Location implements Serializable {

  private boolean mIsOrigin;
  private long mTimestamp;

  public LastOriginDestination() {
    //empty
  }

  public LastOriginDestination(String cityIATACode, String cityName, String country, long geoNodeId,
      long latitude, String locationCode, String locationName, String locationType, long longitude,
      String matchName, boolean isOrigin, long timestamp) {

    super(cityIATACode, cityName, country, geoNodeId, latitude, locationCode, locationName,
        locationType, longitude, matchName);
    this.mIsOrigin = isOrigin;
    this.mTimestamp = timestamp;
  }

  public boolean getIsOrigin() {
    return mIsOrigin;
  }

  public long getTimestamp() {
    return mTimestamp;
  }
}
