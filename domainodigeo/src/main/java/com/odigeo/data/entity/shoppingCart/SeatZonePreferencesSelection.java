package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class SeatZonePreferencesSelection implements Serializable {

  private List<SeatZonePreferencesSelectionItem> seatZonePreferencesSelectionItems;
  private List<Integer> sections;

  public List<SeatZonePreferencesSelectionItem> getSeatZonePreferencesSelectionItems() {
    return seatZonePreferencesSelectionItems;
  }

  public void setSeatZonePreferencesSelectionItems(
      List<SeatZonePreferencesSelectionItem> seatZonePreferencesSelectionItems) {
    this.seatZonePreferencesSelectionItems = seatZonePreferencesSelectionItems;
  }

  public List<Integer> getSections() {
    return sections;
  }

  public void setSections(List<Integer> sections) {
    this.sections = sections;
  }
}
