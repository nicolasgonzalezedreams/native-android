package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public class ItineraryProviderBookings implements Serializable {

  private List<ItineraryProviderBooking> bookings;
  private ItinerariesLegend legend;
  private Collection<SegmentResult> itinerarySegments;

  public List<ItineraryProviderBooking> getBookings() {
    return bookings;
  }

  public void setBookings(List<ItineraryProviderBooking> bookings) {
    this.bookings = bookings;
  }

  public ItinerariesLegend getLegend() {
    return legend;
  }

  public void setLegend(ItinerariesLegend legend) {
    this.legend = legend;
  }

  public Collection<SegmentResult> getItinerarySegments() {
    return itinerarySegments;
  }

  public void setItinerarySegments(Collection<SegmentResult> itinerarySegments) {
    this.itinerarySegments = itinerarySegments;
  }
}