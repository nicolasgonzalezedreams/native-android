package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum BookingStatus implements Serializable {

  REQUEST, CONTRACT, REJECTED, PENDING, HOLD, RETAINED, UNKNOWN, FINAL_RET;

  public static BookingStatus fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }
}