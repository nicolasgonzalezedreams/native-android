package com.odigeo.data.entity.shoppingCart;

import com.odigeo.data.entity.contract.CreditCardType;
import java.io.Serializable;

public class CollectionMethod implements Serializable {

  private CreditCardType creditCardType;
  private CollectionMethodType type;

  public CreditCardType getCreditCardType() {
    return creditCardType;
  }

  public void setCreditCardType(CreditCardType creditCardType) {
    this.creditCardType = creditCardType;
  }

  public CollectionMethodType getType() {
    return type;
  }

  public void setType(CollectionMethodType type) {
    this.type = type;
  }
}
