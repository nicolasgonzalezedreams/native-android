package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum CollectionStatusTestConfiguration implements Serializable {
  REQUESTED, COLLECTED, COLLECTION_DECLINED, COLLECTION_ERROR
}
