package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class ItinerarySelectionRequest implements Serializable {

  private ItinerarySortCriteria sortCriteria;
  private String fareItineraryKey;
  private List<String> segmentKeys;
  private List<ExtensionRequest> extensions;

  public ItinerarySortCriteria getSortCriteria() {
    return sortCriteria;
  }

  public void setSortCriteria(ItinerarySortCriteria sortCriteria) {
    this.sortCriteria = sortCriteria;
  }

  public String getFareItineraryKey() {
    return fareItineraryKey;
  }

  public void setFareItineraryKey(String fareItineraryKey) {
    this.fareItineraryKey = fareItineraryKey;
  }

  public List<String> getSegmentKeys() {
    return segmentKeys;
  }

  public void setSegmentKeys(List<String> segmentKeys) {
    this.segmentKeys = segmentKeys;
  }

  public List<ExtensionRequest> getExtensions() {
    return extensions;
  }

  public void setExtensions(List<ExtensionRequest> extensions) {
    this.extensions = extensions;
  }
}
