package com.odigeo.data.entity.shoppingCart.request;

public class UserInteractionNeededRequest {

  private String returnUrl;

  public String getReturnUrl() {
    return returnUrl;
  }

  public void setReturnUrl(String returnUrl) {
    this.returnUrl = returnUrl;
  }
}