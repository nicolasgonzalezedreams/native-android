package com.odigeo.data.entity.shoppingCart.request;

public class FrequentFlyerCardCodeRequest {

  private String carrierCode;
  private String passengerCardNumber;

  public String getCarrierCode() {
    return carrierCode;
  }

  public void setCarrierCode(String value) {
    this.carrierCode = value;
  }

  public String getPassengerCardNumber() {
    return passengerCardNumber;
  }

  public void setPassengerCardNumber(String value) {
    this.passengerCardNumber = value;
  }
}
