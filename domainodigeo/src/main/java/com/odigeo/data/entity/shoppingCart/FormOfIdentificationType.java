package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum FormOfIdentificationType implements Serializable {

  NATIONAL_ID_CARD, PASSPORT, CREDIT_CARD;

  public static FormOfIdentificationType fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }
}
