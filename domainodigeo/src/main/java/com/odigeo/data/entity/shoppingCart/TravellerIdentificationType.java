package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum TravellerIdentificationType implements Serializable {
  EMPTY(""), PASSPORT("msltravellerinformationdescription_identificationtypepassport"), NIE(
      "msltravellerinformationdescription_identificationtypenie"), NIF(
      "msltravellerinformationdescription_identificationtypenif"), NATIONAL_ID_CARD(
      "msltravellerinformationdescription_identificationtypenif"), BIRTH_DATE(
      "datepicker_birthdate"), CIF("msltravellerinformationdescription_identificationcif");

  private final String mText;

  TravellerIdentificationType(String text) {
    mText = text;
  }

  public static TravellerIdentificationType fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

  @Override public String toString() {
    return mText;
  }

}
