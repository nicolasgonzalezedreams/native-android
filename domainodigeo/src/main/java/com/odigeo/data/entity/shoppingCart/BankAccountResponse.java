package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class BankAccountResponse implements Serializable {

  private String bankName;
  private String iban;
  private String swift;

  public String getBankName() {
    return bankName;
  }

  public void setBankName(String bankName) {
    this.bankName = bankName;
  }

  public String getIban() {
    return iban;
  }

  public void setIban(String iban) {
    this.iban = iban;
  }

  public String getSwift() {
    return swift;
  }

  public void setSwift(String swift) {
    this.swift = swift;
  }
}