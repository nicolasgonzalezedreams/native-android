package com.odigeo.data.entity.contract;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class CollectionSummary implements Serializable {

  private List<CollectionAttempt> attempts;
  private String creationDate;
  private String currency;
  private BigDecimal collectedAmount;

  public List<CollectionAttempt> getAttempts() {
    return attempts;
  }

  public void setAttempts(List<CollectionAttempt> attempts) {
    this.attempts = attempts;
  }

  public String getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(String creationDate) {
    this.creationDate = creationDate;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public BigDecimal getCollectedAmount() {
    return collectedAmount;
  }

  public void setCollectedAmount(BigDecimal collectedAmount) {
    this.collectedAmount = collectedAmount;
  }
}