package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class Traveller implements Serializable {

  private TravellerType travellerType;
  private TravellerTitle title;
  private String name;
  private String firstLastName;
  private String secondLastName;
  private TravellerGender travellerGender;
  private long dateOfBirth;
  private String nationalityCountryCode;
  private String countryCodeOfResidence;
  private String identification;
  private TravellerIdentificationType identificationType;
  private long identificationExpirationDate;
  private String identificationIssueCountryCode;
  private String localityCodeOfResidence;
  private List<BaggageSelectionResponse> baggageSelections;
  private String middleName;
  private Integer numPassenger;
  private List<FrequentFlyerCard> frequentFlyerNumbers;

  public TravellerType getTravellerType() {
    return travellerType;
  }

  public void setTravellerType(TravellerType travellerType) {
    this.travellerType = travellerType;
  }

  public TravellerTitle getTitle() {
    return title;
  }

  public void setTitle(TravellerTitle title) {
    this.title = title;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFirstLastName() {
    return firstLastName;
  }

  public void setFirstLastName(String firstLastName) {
    this.firstLastName = firstLastName;
  }

  public String getSecondLastName() {
    return secondLastName;
  }

  public void setSecondLastName(String secondLastName) {
    this.secondLastName = secondLastName;
  }

  public TravellerGender getTravellerGender() {
    return travellerGender;
  }

  public void setTravellerGender(TravellerGender travellerGender) {
    this.travellerGender = travellerGender;
  }

  public long getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(long dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getNationalityCountryCode() {
    return nationalityCountryCode;
  }

  public void setNationalityCountryCode(String nationalityCountryCode) {
    this.nationalityCountryCode = nationalityCountryCode;
  }

  public String getCountryCodeOfResidence() {
    return countryCodeOfResidence;
  }

  public void setCountryCodeOfResidence(String countryCodeOfResidence) {
    this.countryCodeOfResidence = countryCodeOfResidence;
  }

  public String getIdentification() {
    return identification;
  }

  public void setIdentification(String identification) {
    this.identification = identification;
  }

  public TravellerIdentificationType getIdentificationType() {
    return identificationType;
  }

  public void setIdentificationType(TravellerIdentificationType identificationType) {
    this.identificationType = identificationType;
  }

  public long getIdentificationExpirationDate() {
    return identificationExpirationDate;
  }

  public void setIdentificationExpirationDate(long identificationExpirationDate) {
    this.identificationExpirationDate = identificationExpirationDate;
  }

  public String getIdentificationIssueCountryCode() {
    return identificationIssueCountryCode;
  }

  public void setIdentificationIssueCountryCode(String identificationIssueCountryCode) {
    this.identificationIssueCountryCode = identificationIssueCountryCode;
  }

  public String getLocalityCodeOfResidence() {
    return localityCodeOfResidence;
  }

  public void setLocalityCodeOfResidence(String localityCodeOfResidence) {
    this.localityCodeOfResidence = localityCodeOfResidence;
  }

  public List<BaggageSelectionResponse> getBaggageSelections() {
    return baggageSelections;
  }

  public void setBaggageSelections(List<BaggageSelectionResponse> baggageSelections) {
    this.baggageSelections = baggageSelections;
  }

  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public Integer getNumPassenger() {
    return numPassenger;
  }

  public void setNumPassenger(Integer numPassenger) {
    this.numPassenger = numPassenger;
  }

  public List<FrequentFlyerCard> getFrequentFlyerNumbers() {
    return frequentFlyerNumbers;
  }

  public void setFrequentFlyerNumbers(List<FrequentFlyerCard> frequentFlyerNumbers) {
    this.frequentFlyerNumbers = frequentFlyerNumbers;
  }
}
