package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class Location implements Serializable {

  private int geoNodeId;
  private String cityName;
  private String name;
  private String type;
  private String iataCode;
  private String cityIataCode;
  private String countryCode;
  private String countryName;
  private String timezone;

  public int getGeoNodeId() {
    return geoNodeId;
  }

  public void setGeoNodeId(int geoNodeId) {
    this.geoNodeId = geoNodeId;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getIataCode() {
    return iataCode;
  }

  public void setIataCode(String iataCode) {
    this.iataCode = iataCode;
  }

  public String getCityIataCode() {
    return cityIataCode;
  }

  public void setCityIataCode(String cityIataCode) {
    this.cityIataCode = cityIataCode;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getCountryName() {
    return countryName;
  }

  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }

  public String getTimezone() {
    return timezone;
  }

  public void setTimezone(String timezone) {
    this.timezone = timezone;
  }
}
