package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum TravellerInformationField implements Serializable {
  TITLE, NAME, MIDDLE_NAME, FIRST_LAST_NAME, SECOND_LAST_NAME, BIRTH_DATE, AGE, GENDER, COUNTRY_OF_RESIDENCE, NATIONALITY, IDENTIFICATION, IDENTIFICATION_TYPE, IDENTIFICATION_EXPIRATION_DATE, IDENTIFICATION_COUNTRY, RESIDENT, PHONE_NUMBER, MOBILE_NUMBER, FREQUENT_FLYER_CARD_AIRLINE_CODE, FREQUENT_FLYER_CARD_NUMBER;

  public static TravellerInformationField fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }
}
