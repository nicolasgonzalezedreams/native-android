package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum ItinerarySortCriteria implements Serializable {
  MINIMUM_PURCHASABLE_PRICE, MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE, APPARENT_PROVIDER_PRICE, APPARENT_PROVIDER_PRICE_WITH_DISCOUNTS, APPARENT_PROVIDER_PRICE_WITH_SEARCH_FEE;

  public static ItinerarySortCriteria fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }
}
