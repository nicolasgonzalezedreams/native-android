package com.odigeo.data.entity.shoppingCart.request;

import com.odigeo.data.entity.shoppingCart.ItinerarySortCriteria;
import com.odigeo.data.entity.shoppingCart.Step;
import java.util.List;

public class ModifyShoppingCartRequest {

  private long bookingId;
  private List<String> insuranceOffers;
  private List<PromotionalCodeRequest> promotionalCodes;
  private List<FormOfIdentificationRequest> foids;
  private ItinerarySortCriteria sortCriteria;
  private Step step;

  public ModifyShoppingCartRequest(long bookingId, ItinerarySortCriteria sortCriteria, Step step) {
    this.bookingId = bookingId;
    this.sortCriteria = sortCriteria;
    this.step = step;
  }

  public long getBookingId() {
    return bookingId;
  }

  public void setBookingId(long bookingId) {
    this.bookingId = bookingId;
  }

  public List<String> getInsuranceOffers() {
    return insuranceOffers;
  }

  public void setInsuranceOffers(List<String> insuranceOffers) {
    this.insuranceOffers = insuranceOffers;
  }

  public List<PromotionalCodeRequest> getPromotionalCodes() {
    return promotionalCodes;
  }

  public void setPromotionalCodes(List<PromotionalCodeRequest> promotionalCodes) {
    this.promotionalCodes = promotionalCodes;
  }

  public List<FormOfIdentificationRequest> getFoids() {
    return foids;
  }

  public void setFoids(List<FormOfIdentificationRequest> foids) {
    this.foids = foids;
  }

  public ItinerarySortCriteria getSortCriteria() {
    return sortCriteria;
  }

  public void setSortCriteria(ItinerarySortCriteria sortCriteria) {
    this.sortCriteria = sortCriteria;
  }

  public Step getStep() {
    return step;
  }

  public void setStep(Step step) {
    this.step = step;
  }
}