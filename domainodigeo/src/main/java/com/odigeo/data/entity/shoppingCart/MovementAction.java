package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum MovementAction implements Serializable {

  DIRECTY_PAYMENT, AUTHORIZE, REQUEST, CONFIRM, QUERY, REFUND, CANCEL, VERIFY
}