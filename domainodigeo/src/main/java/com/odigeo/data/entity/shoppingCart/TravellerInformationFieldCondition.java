package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class TravellerInformationFieldCondition implements Serializable {

  private String travellerInformationField;
  private String value;

  public String getTravellerInformationField() {
    return travellerInformationField;
  }

  public void setTravellerInformationField(String travellerInformationField) {
    this.travellerInformationField = travellerInformationField;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
