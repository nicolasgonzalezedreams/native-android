package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;

public class SeatZonePreferencesDescriptorItem implements Serializable {

  private String seatZoneRowCode;
  private String seatZoneColumnCode;
  private BigDecimal providerPrice;
  private BigDecimal totalPrice;

  public String getSeatZoneRowCode() {
    return seatZoneRowCode;
  }

  public void setSeatZoneRowCode(String seatZoneRowCode) {
    this.seatZoneRowCode = seatZoneRowCode;
  }

  public String getSeatZoneColumnCode() {
    return seatZoneColumnCode;
  }

  public void setSeatZoneColumnCode(String seatZoneColumnCode) {
    this.seatZoneColumnCode = seatZoneColumnCode;
  }

  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  public void setProviderPrice(BigDecimal providerPrice) {
    this.providerPrice = providerPrice;
  }

  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }
}
