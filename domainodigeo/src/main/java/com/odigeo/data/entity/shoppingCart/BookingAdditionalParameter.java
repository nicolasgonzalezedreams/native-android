package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class BookingAdditionalParameter implements Serializable {

  private String name;
  private String value;

  public String getName() {
    return name;
  }

  public void setName(String value) {
    this.name = value;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
