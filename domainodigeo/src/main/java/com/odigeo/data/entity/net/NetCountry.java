package com.odigeo.data.entity.net;

import java.util.Map;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 11/02/16
 */
public class NetCountry {

  private int geoNodeId;
  private String countryCode;
  private String phonePrefix;
  private Map<String, String> names;

  public int getGeoNodeId() {
    return geoNodeId;
  }

  public void setGeoNodeId(int geoNodeId) {
    this.geoNodeId = geoNodeId;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getPhonePrefix() {
    return phonePrefix;
  }

  public void setPhonePrefix(String phonePrefix) {
    this.phonePrefix = phonePrefix;
  }

  public Map<String, String> getNames() {
    return names;
  }

  public void setNames(Map<String, String> names) {
    this.names = names;
  }
}
