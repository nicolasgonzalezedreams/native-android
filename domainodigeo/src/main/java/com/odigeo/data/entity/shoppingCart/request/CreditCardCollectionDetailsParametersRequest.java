package com.odigeo.data.entity.shoppingCart.request;

public class CreditCardCollectionDetailsParametersRequest {

  private String cardTypeCode;
  private String cardOwner;
  private String cardNumber;
  private String cardSecurityNumber;
  private String cardExpirationMonth;
  private String cardExpirationYear;
  private int installmentsNumber;

  public String getCardTypeCode() {
    return cardTypeCode;
  }

  public void setCardTypeCode(String cardTypeCode) {
    this.cardTypeCode = cardTypeCode;
  }

  public String getCardOwner() {
    return cardOwner;
  }

  public void setCardOwner(String cardOwner) {
    this.cardOwner = cardOwner;
  }

  public String getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
  }

  public String getCardSecurityNumber() {
    return cardSecurityNumber;
  }

  public void setCardSecurityNumber(String cardSecurityNumber) {
    this.cardSecurityNumber = cardSecurityNumber;
  }

  public String getCardExpirationMonth() {
    return cardExpirationMonth;
  }

  public void setCardExpirationMonth(String cardExpirationMonth) {
    this.cardExpirationMonth = cardExpirationMonth;
  }

  public String getCardExpirationYear() {
    return cardExpirationYear;
  }

  public void setCardExpirationYear(String cardExpirationYear) {
    this.cardExpirationYear = cardExpirationYear;
  }

  public int getInstallmentsNumber() {
    return installmentsNumber;
  }

  public void setInstallmentsNumber(int installmentsNumber) {
    this.installmentsNumber = installmentsNumber;
  }
}