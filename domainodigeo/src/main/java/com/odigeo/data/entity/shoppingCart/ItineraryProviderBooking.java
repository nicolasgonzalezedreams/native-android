package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class ItineraryProviderBooking implements Serializable{

  private Long id;
  private String bookingStatus;
  private boolean resident;
  private String pnr;
  private List<Integer> segments;
  private BigDecimal baggageProviderPrice;
  private int numAdults;
  private int numChildren;
  private int numInfants;
  private List<ResidentValidationStatus> residentValidations;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getBookingStatus() {
    return bookingStatus;
  }

  public void setBookingStatus(String bookingStatus) {
    this.bookingStatus = bookingStatus;
  }

  public boolean isResident() {
    return resident;
  }

  public void setResident(boolean resident) {
    this.resident = resident;
  }

  public String getPnr() {
    return pnr;
  }

  public void setPnr(String pnr) {
    this.pnr = pnr;
  }

  public List<Integer> getSegments() {
    return segments;
  }

  public void setSegments(List<Integer> segments) {
    this.segments = segments;
  }

  public BigDecimal getBaggageProviderPrice() {
    return baggageProviderPrice;
  }

  public void setBaggageProviderPrice(BigDecimal baggageProviderPrice) {
    this.baggageProviderPrice = baggageProviderPrice;
  }

  public int getNumAdults() {
    return numAdults;
  }

  public void setNumAdults(int numAdults) {
    this.numAdults = numAdults;
  }

  public int getNumChildren() {
    return numChildren;
  }

  public void setNumChildren(int numChildren) {
    this.numChildren = numChildren;
  }

  public int getNumInfants() {
    return numInfants;
  }

  public void setNumInfants(int numInfants) {
    this.numInfants = numInfants;
  }

  public List<ResidentValidationStatus> getResidentValidations() {
    return residentValidations;
  }

  public void setResidentValidations(List<ResidentValidationStatus> residentValidations) {
    this.residentValidations = residentValidations;
  }
}