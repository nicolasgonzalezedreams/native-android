package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class FareComponent implements Serializable {

  private List<String> fareRules;
  private String passengerType;
  private String origin;
  private String destination;

  public List<String> getFareRules() {
    return fareRules;
  }

  public void setFareRules(List<String> fareRules) {
    this.fareRules = fareRules;
  }

  public String getPassengerType() {
    return passengerType;
  }

  public void setPassengerType(String passengerType) {
    this.passengerType = passengerType;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public String getDestination() {
    return destination;
  }

  public void setDestination(String destination) {
    this.destination = destination;
  }
}
