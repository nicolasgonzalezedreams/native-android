package com.odigeo.data.entity.userData;

public class UserDestinationPreferences {

  private long mId;
  private long mUserDestinationPreferencesId;
  private String mDestination;
  private long mUserId;

  public UserDestinationPreferences() {
    //empty
  }

  public UserDestinationPreferences(long id, long userDestinationPreferencesId, String destination,
      long userId) {
    mId = id;
    mUserDestinationPreferencesId = userDestinationPreferencesId;
    mDestination = destination;
    mUserId = userId;
  }

  public long getId() {
    return mId;
  }

  public long getUserDestinationPreferencesId() {
    return mUserDestinationPreferencesId;
  }

  public String getDestination() {
    return mDestination;
  }

  public long getUserId() {
    return mUserId;
  }
}
