package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;

public class ShoppingCartPriceCalculator implements Serializable {

  private FeeInfo feeInfo;
  private BigDecimal markup;

  public FeeInfo getFeeInfo() {
    return feeInfo;
  }

  public void setFeeInfo(FeeInfo feeInfo) {
    this.feeInfo = feeInfo;
  }

  public BigDecimal getMarkup() {
    return markup;
  }

  public void setMarkup(BigDecimal markup) {
    this.markup = markup;
  }
}
