package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum BookingResponseStatus implements Serializable {
  BOOKING_ERROR, BOOKING_PAYMENT_RETRY, BOOKING_CONFIRMED, BOOKING_REPRICING, BROKEN_FLOW, USER_INTERACTION_NEEDED, BOOKING_STOP, ON_HOLD
}