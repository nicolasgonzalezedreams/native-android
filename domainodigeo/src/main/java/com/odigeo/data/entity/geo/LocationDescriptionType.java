package com.odigeo.data.entity.geo;

public enum LocationDescriptionType {

  AIRPORT, CITY, TRAIN_STATION, IATA_CODE, BUS_STATION;

}
