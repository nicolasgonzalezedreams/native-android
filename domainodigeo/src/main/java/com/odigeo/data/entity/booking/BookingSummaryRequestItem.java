package com.odigeo.data.entity.booking;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public final class BookingSummaryRequestItem implements Serializable {

  private String email;

  private List<Long> bookingIds;

  public BookingSummaryRequestItem(String email) {
    this.email = email;
  }

  public String getEmail() {
    return email;
  }

  public List<Long> getBookingIds() {
    return bookingIds;
  }

  public void addBookingId(long bookingId) {
    if (bookingIds == null) {
      bookingIds = new ArrayList<>();
    }
    bookingIds.add(bookingId);
  }
}
