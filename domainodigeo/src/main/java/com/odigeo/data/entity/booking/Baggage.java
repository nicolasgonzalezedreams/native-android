package com.odigeo.data.entity.booking;

import java.io.Serializable;

public class Baggage implements Serializable {

  private long mId;
  private boolean mIncludedInPrice;
  private long mPieces;
  private long mWeight;

  //Relations
  private Segment mSegment;
  private transient Traveller mTraveller;

  public Baggage() {
    //empty
  }

  public Baggage(long id, boolean includedInPrice, long pieces, long weight) {
    this.mId = id;
    this.mIncludedInPrice = includedInPrice;
    this.mPieces = pieces;
    this.mWeight = weight;
  }

  public Baggage(long id, boolean includedInPrice, long pieces, long weight, Segment segment,
      Traveller traveller) {
    this.mId = id;
    this.mIncludedInPrice = includedInPrice;
    this.mPieces = pieces;
    this.mWeight = weight;
    this.mSegment = segment;
    this.mTraveller = traveller;
  }

  public long getId() {
    return mId;
  }

  public void setId(long id) {
    mId = id;
  }

  public boolean getIncludedInPrice() {
    return mIncludedInPrice;
  }

  public long getPieces() {
    return mPieces;
  }

  public long getWeight() {
    return mWeight;
  }

  public Segment getSegment() {
    return mSegment;
  }

  public void setSegment(Segment segment) {
    mSegment = segment;
  }

  public Traveller getTraveller() {
    return mTraveller;
  }

  public void setTraveller(Traveller traveller) {
    mTraveller = traveller;
  }
}
