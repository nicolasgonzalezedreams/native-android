package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class PassengerConflictDetails implements Serializable {

  private long bookingId;
  private List<String> pnrs;
  private String depTime;
  private String depAirport;
  private String arrAirport;
  private BookingStatus bookingStatus;

  public long getBookingId() {
    return bookingId;
  }

  public void setBookingId(long bookingId) {
    this.bookingId = bookingId;
  }

  public List<String> getPnrs() {
    return pnrs;
  }

  public void setPnrs(List<String> pnrs) {
    this.pnrs = pnrs;
  }

  public String getDepTime() {
    return depTime;
  }

  public void setDepTime(String depTime) {
    this.depTime = depTime;
  }

  public String getDepAirport() {
    return depAirport;
  }

  public void setDepAirport(String depAirport) {
    this.depAirport = depAirport;
  }

  public String getArrAirport() {
    return arrAirport;
  }

  public void setArrAirport(String arrAirport) {
    this.arrAirport = arrAirport;
  }

  public BookingStatus getBookingStatus() {
    return bookingStatus;
  }

  public void setBookingStatus(BookingStatus bookingStatus) {
    this.bookingStatus = bookingStatus;
  }
}
