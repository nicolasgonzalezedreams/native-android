package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class PreferencesAwareResponse extends BaseResponse implements Serializable {

  private Preferences preferences;

  public Preferences getPreferences() {
    return preferences;
  }

  public void setPreferences(Preferences preferences) {
    this.preferences = preferences;
  }
}
