package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class FlightFareInformationPassengerResult implements Serializable {

  private FlightFareInformationPassenger fareInfoPax;
  private int id;

  public FlightFareInformationPassenger getFareInfoPax() {
    return fareInfoPax;
  }

  public void setFareInfoPax(FlightFareInformationPassenger fareInfoPax) {
    this.fareInfoPax = fareInfoPax;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}
