package com.odigeo.data.entity.booking;

import java.io.Serializable;

public class Buyer extends PersonBooking implements Serializable {

  private long mId;
  private String mAddress;
  private String mAlternativePhone;
  private String mCityName;
  private String mCountry;
  private String mCpf;
  private String mEmail;
  private String mPhone;
  private String mPhoneCountryCode;
  private String mStateName;
  private String mZipCode;

  //Realtions
  private long mBookingId;

  public Buyer() {
    super();
  }

  public Buyer(long dateOfBirth, String identification, String identificationType, String lastname,
      String name, long id, String address, String alternativePhone, String cityName,
      String country, String cpf, String email, String phone, String phoneCountryCode,
      String stateName, String zipCode, long bookingId) {

    super(dateOfBirth, identification, identificationType, lastname, name);
    mId = id;
    mAddress = address;
    mAlternativePhone = alternativePhone;
    mCityName = cityName;
    mCountry = country;
    mCpf = cpf;
    mEmail = email;
    mPhone = phone;
    mPhoneCountryCode = phoneCountryCode;
    mStateName = stateName;
    mZipCode = zipCode;
    mBookingId = bookingId;
  }

  public long getId() {
    return mId;
  }

  public void setId(long id) {
    mId = id;
  }

  public String getAddress() {
    return mAddress;
  }

  public void setAddress(String address) {
    mAddress = address;
  }

  public String getAlternativePhone() {
    return mAlternativePhone;
  }

  public void setAlternativePhone(String alternativePhone) {
    mAlternativePhone = alternativePhone;
  }

  public String getCityName() {
    return mCityName;
  }

  public void setCityName(String cityName) {
    mCityName = cityName;
  }

  public String getCountry() {
    return mCountry;
  }

  public void setCountry(String country) {
    mCountry = country;
  }

  public String getCpf() {
    return mCpf;
  }

  public void setCpf(String cpf) {
    mCpf = cpf;
  }

  public String getEmail() {
    return mEmail;
  }

  public void setEmail(String email) {
    mEmail = email;
  }

  public String getPhone() {
    return mPhone;
  }

  public void setPhone(String phone) {
    mPhone = phone;
  }

  public String getPhoneCountryCode() {
    return mPhoneCountryCode;
  }

  public void setPhoneCountryCode(String phoneCountryCode) {
    mPhoneCountryCode = phoneCountryCode;
  }

  public String getStateName() {
    return mStateName;
  }

  public void setStateName(String stateName) {
    mStateName = stateName;
  }

  public String getZipCode() {
    return mZipCode;
  }

  public void setZipCode(String zipCode) {
    mZipCode = zipCode;
  }

  public long getBookingId() {
    return mBookingId;
  }

  public void setBookingId(long bookingId) {
    mBookingId = bookingId;
  }
}
