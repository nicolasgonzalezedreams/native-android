package com.odigeo.data.entity.userData;

public class UserTravellerFactory {

  public static UserTraveller getEmptyUserTraveller(int position, String namePassenger) {
    UserProfile userProfile =
        new UserProfile(-1, -1, null, null, namePassenger, null, null, null, 0, null, null, null,
            null, null, null, null, false, null, null, null, -1);
    UserTraveller userTraveller =
        new UserTraveller(0, -(position + 1), false, null, null, null, null, userProfile, 0);
    return userTraveller;
  }

  public static UserTraveller getEmptyContactUserTraveller(String nameEmptyContactDetail,
      int contactId) {
    UserProfile userProfile =
        new UserProfile(-1, -1, null, null, nameEmptyContactDetail, null, null, null, 0, null, null,
            null, null, null, null, null, false, null, null, null, -1);
    return new UserTraveller(0, contactId, false, null, null, null, null, userProfile, 0);
  }
}
