package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class BookingDetail implements Serializable {

  private BookingBasicInfo bookingBasicInfo;
  private String locale;
  private Money price;
  private Buyer buyer;
  private List<Traveller> travellers;
  private ItineraryProviderBookings itineraryBookings;
  private InsuranceProviderBookings insuranceBookings;
  private BookingStatus bookingStatus;
  private String collectionState;
  private boolean sod;
  private boolean travelCompanionNotifications;
  private boolean enrichedBooking;

  public BookingBasicInfo getBookingBasicInfo() {
    return bookingBasicInfo;
  }

  public void setBookingBasicInfo(BookingBasicInfo bookingBasicInfo) {
    this.bookingBasicInfo = bookingBasicInfo;
  }

  public String getLocale() {
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
  }

  public Money getPrice() {
    return price;
  }

  public void setPrice(Money price) {
    this.price = price;
  }

  public Buyer getBuyer() {
    return buyer;
  }

  public void setBuyer(Buyer buyer) {
    this.buyer = buyer;
  }

  public List<Traveller> getTravellers() {
    return travellers;
  }

  public void setTravellers(List<Traveller> travellers) {
    this.travellers = travellers;
  }

  public ItineraryProviderBookings getItineraryBookings() {
    return itineraryBookings;
  }

  public void setItineraryBookings(ItineraryProviderBookings itineraryBookings) {
    this.itineraryBookings = itineraryBookings;
  }

  public InsuranceProviderBookings getInsuranceBookings() {
    return insuranceBookings;
  }

  public void setInsuranceBookings(InsuranceProviderBookings insuranceBookings) {
    this.insuranceBookings = insuranceBookings;
  }

  public BookingStatus getBookingStatus() {
    return bookingStatus;
  }

  public void setBookingStatus(BookingStatus bookingStatus) {
    this.bookingStatus = bookingStatus;
  }

  public String getCollectionState() {
    return collectionState;
  }

  public void setCollectionState(String collectionState) {
    this.collectionState = collectionState;
  }

  public boolean isSod() {
    return sod;
  }

  public void setSod(boolean sod) {
    this.sod = sod;
  }

  public boolean isTravelCompanionNotifications() {
    return travelCompanionNotifications;
  }

  public void setTravelCompanionNotifications(boolean travelCompanionNotifications) {
    this.travelCompanionNotifications = travelCompanionNotifications;
  }

  public boolean isEnrichedBooking() {
    return enrichedBooking;
  }

  public void setEnrichedBooking(boolean enrichedBooking) {
    this.enrichedBooking = enrichedBooking;
  }
}