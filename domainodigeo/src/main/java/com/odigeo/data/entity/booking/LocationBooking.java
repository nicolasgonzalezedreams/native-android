package com.odigeo.data.entity.booking;

import java.io.Serializable;
import java.util.List;

public class LocationBooking extends Location implements Serializable {

  private String mCountryCode;
  private String mCurrencyCode;
  private long mCurrencyRate;
  private long mCurrencyUpdatedDate;
  private String mTimezone;

  //Relations
  private List<Forecast> mForecasts;
  private List<Section> mSectionFrom;
  private List<Section> mSectionTo;

  public LocationBooking() {
    //empty
  }

  public LocationBooking(long geoNodeId) {
    super(geoNodeId);
  }

  public LocationBooking(long geoNodeId, String cityIATACode, String cityName, String country,
      String matchName, String countryCode, String timezone, String locationName,
      String locationType, String locationCode) {
    super(geoNodeId, cityIATACode, cityName, country, matchName, locationName, locationType,
        locationCode);
    mCountryCode = countryCode;
    mTimezone = timezone;
  }

  public LocationBooking(String cityIATACode, String cityName, String country, long geoNodeId,
      long latitude, String locationCode, String locationName, String locationType, long longitude,
      String matchName, String countryCode, String currencyCode, long currencyRate,
      long currencyUpdatedDate, String timezone) {

    super(cityIATACode, cityName, country, geoNodeId, latitude, locationCode, locationName,
        locationType, longitude, matchName);
    mCountryCode = countryCode;
    mCurrencyCode = currencyCode;
    mCurrencyRate = currencyRate;
    mCurrencyUpdatedDate = currencyUpdatedDate;
    mTimezone = timezone;
  }

  public LocationBooking(String cityIATACode, String cityName, String country, long geoNodeId,
      long latitude, String locationCode, String locationName, String locationType, long longitude,
      String matchName, String countryCode, String currencyCode, long currencyRate,
      long currencyUpdatedDate, String timezone, List<Forecast> forecasts,
      List<Section> sectionFrom, List<Section> sectionTo) {

    super(cityIATACode, cityName, country, geoNodeId, latitude, locationCode, locationName,
        locationType, longitude, matchName);
    mCountryCode = countryCode;
    mCurrencyCode = currencyCode;
    mCurrencyRate = currencyRate;
    mCurrencyUpdatedDate = currencyUpdatedDate;
    mTimezone = timezone;
    mForecasts = forecasts;
    mSectionFrom = sectionFrom;
    mSectionTo = sectionTo;
  }

  public String getCountryCode() {
    return mCountryCode;
  }

  public void setCountryCode(String mCountryCode) {
    this.mCountryCode = mCountryCode;
  }

  public String getCurrencyCode() {
    return mCurrencyCode;
  }

  public void setCurrencyCode(String mCurrencyCode) {
    this.mCurrencyCode = mCurrencyCode;
  }

  public long getCurrencyRate() {
    return mCurrencyRate;
  }

  public void setCurrencyRate(long mCurrencyRate) {
    this.mCurrencyRate = mCurrencyRate;
  }

  public long getCurrencyUpdatedDate() {
    return mCurrencyUpdatedDate;
  }

  public void setCurrencyUpdatedDate(long mCurrencyUpdatedDate) {
    this.mCurrencyUpdatedDate = mCurrencyUpdatedDate;
  }

  public String getTimezone() {
    return mTimezone;
  }

  public void setTimezone(String mTimezone) {
    this.mTimezone = mTimezone;
  }

  public List<Forecast> getForecasts() {
    return mForecasts;
  }

  public List<Section> getSectionFrom() {
    return mSectionFrom;
  }

  public List<Section> getSectionTo() {
    return mSectionTo;
  }
}
