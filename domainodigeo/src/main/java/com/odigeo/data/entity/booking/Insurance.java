package com.odigeo.data.entity.booking;

import java.io.Serializable;

public class Insurance implements Serializable {

  private long mId;
  private long mInsuranceId;
  private String mConditionURLPrimary;
  private String mConditionURLSecundary;
  private String mInsuranceDescription;
  private String mInsuranceType;
  private String mPolicy;
  private boolean mSelectable;
  private String mSubtitle;
  private String mTitle;
  private double mTotal;
  private long mBookingId;

  public Insurance() {
    //empty
  }

  public Insurance(long id, long insuranceId, String conditionURLPrimary,
      String conditionURLSecundary, String insuranceDescription, String insuranceType,
      String policy, boolean selectable, String subtitle, String title, long total) {
    this.mId = id;
    this.mInsuranceId = insuranceId;
    this.mConditionURLPrimary = conditionURLPrimary;
    this.mConditionURLSecundary = conditionURLSecundary;
    this.mInsuranceDescription = insuranceDescription;
    this.mInsuranceType = insuranceType;
    this.mPolicy = policy;
    this.mSelectable = selectable;
    this.mSubtitle = subtitle;
    this.mTitle = title;
    this.mTotal = total;
  }

  public Insurance(long id, long insuranceId, String conditionURLPrimary,
      String conditionURLSecundary, String insuranceDescription, String insuranceType,
      String policy, boolean selectable, String subtitle, String title, double total,
      long bookingId) {
    this.mId = id;
    this.mInsuranceId = insuranceId;
    this.mConditionURLPrimary = conditionURLPrimary;
    this.mConditionURLSecundary = conditionURLSecundary;
    this.mInsuranceDescription = insuranceDescription;
    this.mInsuranceType = insuranceType;
    this.mPolicy = policy;
    this.mSelectable = selectable;
    this.mSubtitle = subtitle;
    this.mTitle = title;
    this.mTotal = total;
    this.mBookingId = bookingId;
  }

  public long getId() {
    return mId;
  }

  public void setId(long id) {
    mId = id;
  }

  public long getInsuranceId() {
    return mInsuranceId;
  }

  public String getConditionURLPrimary() {
    return mConditionURLPrimary;
  }

  public String getConditionURLSecundary() {
    return mConditionURLSecundary;
  }

  public String getInsuranceDescription() {
    return mInsuranceDescription;
  }

  public String getInsuranceType() {
    return mInsuranceType;
  }

  public String getPolicy() {
    return mPolicy;
  }

  public boolean getSelectable() {
    return mSelectable;
  }

  public String getSubtitle() {
    return mSubtitle;
  }

  public String getTitle() {
    return mTitle;
  }

  public double getTotal() {
    return mTotal;
  }

  public long getBookingId() {
    return mBookingId;
  }
}
