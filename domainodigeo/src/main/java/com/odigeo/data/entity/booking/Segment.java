package com.odigeo.data.entity.booking;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Segment implements Serializable {

  private long mId;
  private long mDuration;
  private long mSeats;
  private long mMainCarrierId;
  private long mBookingId;

  //Relations
  private List<Baggage> mBaggagesList;
  private List<Section> mSectionsList;

  public Segment() {
    //empty
  }

  public Segment(long id) {
    mId = id;
  }

  public Segment(long id, long duration, long seats, long mainCarrierId, long bookingId) {
    mId = id;
    mDuration = duration;
    mSeats = seats;
    mMainCarrierId = mainCarrierId;
    mBookingId = bookingId;
  }

  public Segment(long id, long duration, long seats, long mainCarrierId, long bookingId,
      ArrayList<Baggage> baggagesList, ArrayList<Section> sectionsList) {
    mId = id;
    mDuration = duration;
    mSeats = seats;
    mMainCarrierId = mainCarrierId;
    mBookingId = bookingId;
    mBaggagesList = baggagesList;
    mSectionsList = sectionsList;
  }

  public long getId() {
    return mId;
  }

  public void setId(long id) {
    mId = id;
  }

  public long getDuration() {
    return mDuration;
  }

  public long getSeats() {
    return mSeats;
  }

  public long getMainCarrierId() {
    return mMainCarrierId;
  }

  public long getBookingId() {
    return mBookingId;
  }

  public List<Baggage> getBaggagesList() {
    return mBaggagesList;
  }

  public List<Section> getSectionsList() {
    return mSectionsList;
  }

  public void setSectionsList(List<Section> sectionsList) {
    mSectionsList = sectionsList;
  }

  public Section getFirstSection() {
    if (mSectionsList != null && !mSectionsList.isEmpty()) {
      return mSectionsList.get(0);
    }
    return null;
  }

  public Section getLastSection() {
    if (mSectionsList != null && !mSectionsList.isEmpty()) {
      return mSectionsList.get(mSectionsList.size() - 1);
    }
    return null;
  }

  public long getDepartureDate() {
    Section section = getFirstSection();
    if (section != null) {
      return section.getDepartureDate();
    }
    return 0;
  }

  public long getArrivalDate() {
    Section section = getLastSection();
    if (section != null) {
      return section.getArrivalDate();
    }
    return 0;
  }

  //****************
  //    SETTERS
  //****************

  public LocationBooking getDepartureCity() {
    Section section = getFirstSection();
    if (section != null) {
      return section.getFrom();
    }
    return null;
  }

  public LocationBooking getArrivalCity() {
    Section section = getLastSection();
    if (section != null) {
      return section.getTo();
    }
    return null;
  }

  public long checkAndAddSegmentDelayDate() {
    long departureDate = getDepartureDate();
    FlightStats flightStats = getFirstSection().getFlightStats();
    if (flightStats != null
        && flightStats.getFlightStatus() != null
        && flightStats.getFlightStatus().equalsIgnoreCase(FlightStats.STATUS_DELAYED)) {
      departureDate = flightStats.getDepartureTime();
    }
    return departureDate;
  }
}
