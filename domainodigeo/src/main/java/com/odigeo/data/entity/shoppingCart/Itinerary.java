package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public class Itinerary implements Serializable {

  private ItinerariesLegend legend;
  private List<FareComponent> fareComponents;
  private List<Integer> segments;
  private FareDetails fareDetail;
  private Integer itinerary;
  private Collection<SegmentResult> itinerarySegments;

  public ItinerariesLegend getLegend() {
    return legend;
  }

  public void setLegend(ItinerariesLegend legend) {
    this.legend = legend;
  }

  public List<FareComponent> getFareComponents() {
    return fareComponents;
  }

  public void setFareComponents(List<FareComponent> fareComponents) {
    this.fareComponents = fareComponents;
  }

  public List<Integer> getSegments() {
    return segments;
  }

  public void setSegments(List<Integer> segments) {
    this.segments = segments;
  }

  public FareDetails getFareDetail() {
    return fareDetail;
  }

  public void setFareDetail(FareDetails fareDetail) {
    this.fareDetail = fareDetail;
  }

  public Integer getItinerary() {
    return itinerary;
  }

  public void setItinerary(Integer itinerary) {
    this.itinerary = itinerary;
  }

  public Collection<SegmentResult> getItinerarySegments() {
    return itinerarySegments;
  }

  public void setItinerarySegments(Collection<SegmentResult> itinerarySegments) {
    this.itinerarySegments = itinerarySegments;
  }

  public long getLastArrivalDate() {
    int sectionListSize = getLegend().getSectionResults().size();
    return Long.valueOf(getLegend()
        .getSectionResults()
        .get(sectionListSize - 1)
        .getSection()
        .getArrivalDate());
  }
}
