package com.odigeo.data.entity.shoppingCart.request;

import com.odigeo.data.entity.shoppingCart.BookingResponseStatusTestConfiguration;
import com.odigeo.data.entity.shoppingCart.CollectionStatusTestConfiguration;
import com.odigeo.data.entity.shoppingCart.FraudRiskLevel;

class TestConfigurationConfirmRequest {

  private BookingErrorRequestTestConfiguration bookingErrorRequest;
  private FraudRiskLevel fraudRiskLevel;
  private CollectionStatusTestConfiguration collectionStatus;
  private BookingResponseStatusTestConfiguration bookingResponseStatus;
  private boolean manualReviewForced;

  public BookingErrorRequestTestConfiguration getBookingErrorRequest() {
    return bookingErrorRequest;
  }

  public void setBookingErrorRequest(BookingErrorRequestTestConfiguration bookingErrorRequest) {
    this.bookingErrorRequest = bookingErrorRequest;
  }

  public FraudRiskLevel getFraudRiskLevel() {
    return fraudRiskLevel;
  }

  public void setFraudRiskLevel(FraudRiskLevel fraudRiskLevel) {
    this.fraudRiskLevel = fraudRiskLevel;
  }

  public CollectionStatusTestConfiguration getCollectionStatus() {
    return collectionStatus;
  }

  public void setCollectionStatus(CollectionStatusTestConfiguration collectionStatus) {
    this.collectionStatus = collectionStatus;
  }

  public BookingResponseStatusTestConfiguration getBookingResponseStatus() {
    return bookingResponseStatus;
  }

  public void setBookingResponseStatus(
      BookingResponseStatusTestConfiguration bookingResponseStatus) {
    this.bookingResponseStatus = bookingResponseStatus;
  }

  public boolean isManualReviewForced() {
    return manualReviewForced;
  }

  public void setManualReviewForced(boolean manualReviewForced) {
    this.manualReviewForced = manualReviewForced;
  }
}