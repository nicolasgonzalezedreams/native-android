package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class Itineraries implements Serializable {

  private List<CrossFaringItinerary> crossFarings;
  private List<HubItinerary> hubs;
  private List<OwasrtItinerary> owasrts;
  private List<HidingItinerary> hidings;
  private List<ItineraryProvider> simples;
  private List<CrossFaringMultiDestinationItinerary> crossFaringMultiDestinations;

  public List<CrossFaringItinerary> getCrossFarings() {
    return crossFarings;
  }

  public void setCrossFarings(List<CrossFaringItinerary> crossFarings) {
    this.crossFarings = crossFarings;
  }

  public List<HubItinerary> getHubs() {
    return hubs;
  }

  public void setHubs(List<HubItinerary> hubs) {
    this.hubs = hubs;
  }

  public List<OwasrtItinerary> getOwasrts() {
    return owasrts;
  }

  public void setOwasrts(List<OwasrtItinerary> owasrts) {
    this.owasrts = owasrts;
  }

  public List<HidingItinerary> getHidings() {
    return hidings;
  }

  public void setHidings(List<HidingItinerary> hidings) {
    this.hidings = hidings;
  }

  public List<ItineraryProvider> getSimples() {
    return simples;
  }

  public void setSimples(List<ItineraryProvider> simples) {
    this.simples = simples;
  }

  public List<CrossFaringMultiDestinationItinerary> getCrossFaringMultiDestinations() {
    return crossFaringMultiDestinations;
  }

  public void setCrossFaringMultiDestinations(
      List<CrossFaringMultiDestinationItinerary> crossFaringMultiDestinations) {
    this.crossFaringMultiDestinations = crossFaringMultiDestinations;
  }
}
