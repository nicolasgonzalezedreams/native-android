package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;

public class InstallmentInformationDescription implements Serializable {

  private int installmentsNumber;
  private BigDecimal firstInstallmentPrice;
  private BigDecimal installmentPriceExceptFirst;
  private BigDecimal commissionPercent;

  public int getInstallmentsNumber() {
    return installmentsNumber;
  }

  public void setInstallmentsNumber(int installmentsNumber) {
    this.installmentsNumber = installmentsNumber;
  }

  public BigDecimal getFirstInstallmentPrice() {
    return firstInstallmentPrice;
  }

  public void setFirstInstallmentPrice(BigDecimal firstInstallmentPrice) {
    this.firstInstallmentPrice = firstInstallmentPrice;
  }

  public BigDecimal getInstallmentPriceExceptFirst() {
    return installmentPriceExceptFirst;
  }

  public void setInstallmentPriceExceptFirst(BigDecimal installmentPriceExceptFirst) {
    this.installmentPriceExceptFirst = installmentPriceExceptFirst;
  }

  public BigDecimal getCommissionPercent() {
    return commissionPercent;
  }

  public void setCommissionPercent(BigDecimal commissionPercent) {
    this.commissionPercent = commissionPercent;
  }
}
