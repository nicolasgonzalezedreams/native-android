package com.odigeo.data.entity.tracker;

import java.util.List;

public class Transaction {
  private String id;
  private String affiliation;
  private double revenue;
  private double tax;
  private double shipping;
  private String currencyCode;

  private List<Product> productList;

  public Transaction(String id) {
    this.id = id;
  }

  public final String getId() {
    return id;
  }

  public final void setId(String id) {
    this.id = id;
  }

  public final String getAffiliation() {
    return affiliation;
  }

  public final void setAffiliation(String affiliation) {
    this.affiliation = affiliation;
  }

  public final double getRevenue() {
    return revenue;
  }

  public final void setRevenue(double revenue) {
    this.revenue = revenue;
  }

  public final double getTax() {
    return tax;
  }

  public final void setTax(double tax) {
    this.tax = tax;
  }

  public final double getShipping() {
    return shipping;
  }

  public final void setShipping(double shipping) {
    this.shipping = shipping;
  }

  public final String getCurrencyCode() {
    return currencyCode;
  }

  public final void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public final List<Product> getProductList() {
    return productList;
  }

  public final void setProductList(List<Product> productList) {
    this.productList = productList;
  }
}
