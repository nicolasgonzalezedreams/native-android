package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum FraudRiskLevel implements Serializable {

  NO_RISK_LEVEL, LOW_RISK_LEVEL, MEDIUM_RISK_LEVEL, HIGH_RISK_LEVEL, ULTRA_HIGH_LEVEL;

  public static FraudRiskLevel fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }
}
