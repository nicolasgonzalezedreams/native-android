package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class Section implements Serializable {

  private List<TechnicalStop> technicalStops;
  private BaggageAllowanceType baggageAllowanceType;
  private String departureDate;
  private String arrivalDate;
  private CabinClass cabinClass;
  private List<Integer> fareInfoPassengers;
  private String id;
  private int from;
  private String departureTerminal;
  private int to;
  private String arrivalTerminal;
  private int carrier;
  private int operatingCarrier;
  private String vehicleModel;
  private Integer baggageAllowanceQuantity;
  private Long duration;
  private boolean lcc;

  public List<TechnicalStop> getTechnicalStops() {
    return technicalStops;
  }

  public void setTechnicalStops(List<TechnicalStop> technicalStops) {
    this.technicalStops = technicalStops;
  }

  public BaggageAllowanceType getBaggageAllowanceType() {
    return baggageAllowanceType;
  }

  public void setBaggageAllowanceType(BaggageAllowanceType baggageAllowanceType) {
    this.baggageAllowanceType = baggageAllowanceType;
  }

  public String getDepartureDate() {
    return departureDate;
  }

  public void setDepartureDate(String departureDate) {
    this.departureDate = departureDate;
  }

  public String getArrivalDate() {
    return arrivalDate;
  }

  public void setArrivalDate(String arrivalDate) {
    this.arrivalDate = arrivalDate;
  }

  public CabinClass getCabinClass() {
    return cabinClass;
  }

  public void setCabinClass(CabinClass cabinClass) {
    this.cabinClass = cabinClass;
  }

  public List<Integer> getFareInfoPassengers() {
    return fareInfoPassengers;
  }

  public void setFareInfoPassengers(List<Integer> fareInfoPassengers) {
    this.fareInfoPassengers = fareInfoPassengers;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public int getFrom() {
    return from;
  }

  public void setFrom(int from) {
    this.from = from;
  }

  public String getDepartureTerminal() {
    return departureTerminal;
  }

  public void setDepartureTerminal(String departureTerminal) {
    this.departureTerminal = departureTerminal;
  }

  public int getTo() {
    return to;
  }

  public void setTo(int to) {
    this.to = to;
  }

  public String getArrivalTerminal() {
    return arrivalTerminal;
  }

  public void setArrivalTerminal(String arrivalTerminal) {
    this.arrivalTerminal = arrivalTerminal;
  }

  public int getCarrier() {
    return carrier;
  }

  public void setCarrier(int carrier) {
    this.carrier = carrier;
  }

  public int getOperatingCarrier() {
    return operatingCarrier;
  }

  public void setOperatingCarrier(int operatingCarrier) {
    this.operatingCarrier = operatingCarrier;
  }

  public String getVehicleModel() {
    return vehicleModel;
  }

  public void setVehicleModel(String vehicleModel) {
    this.vehicleModel = vehicleModel;
  }

  public Integer getBaggageAllowanceQuantity() {
    return baggageAllowanceQuantity;
  }

  public void setBaggageAllowanceQuantity(Integer baggageAllowanceQuantity) {
    this.baggageAllowanceQuantity = baggageAllowanceQuantity;
  }

  public Long getDuration() {
    return duration;
  }

  public void setDuration(Long duration) {
    this.duration = duration;
  }

  public boolean isLcc() {
    return lcc;
  }

  public void setLcc(boolean lcc) {
    this.lcc = lcc;
  }
}
