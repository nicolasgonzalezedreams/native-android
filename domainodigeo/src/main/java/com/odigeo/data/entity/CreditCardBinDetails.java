package com.odigeo.data.entity;

import java.util.Map;

public class CreditCardBinDetails {

  private String creditCardBin;
  private String creditCardTypeCode;
  private Integer creditCardNumCountryCode;
  private String creditCardCountryCode;
  private Map<String, String> creditCardBinSubtypes;

  public String getCreditCardBin() {
    return creditCardBin;
  }

  public void setCreditCardBin(final String creditCardBin) {
    this.creditCardBin = creditCardBin;
  }

  public String getCreditCardTypeCode() {
    return creditCardTypeCode;
  }

  public void setCreditCardTypeCode(final String creditCardTypeCode) {
    this.creditCardTypeCode = creditCardTypeCode;
  }

  public Integer getCreditCardNumCountryCode() {
    return creditCardNumCountryCode;
  }

  public void setCreditCardNumCountryCode(final Integer creditCardNumCountryCode) {
    this.creditCardNumCountryCode = creditCardNumCountryCode;
  }

  public String getCreditCardCountryCode() {
    return creditCardCountryCode;
  }

  public void setCreditCardCountryCode(final String creditCardCountryCode) {
    this.creditCardCountryCode = creditCardCountryCode;
  }

  public Map<String, String> getCreditCardBinSubtypes() {

    return creditCardBinSubtypes;
  }

  public void setCreditCardBinSubtypes(final Map<String, String> creditCardBinSubtypes) {
    this.creditCardBinSubtypes = creditCardBinSubtypes;
  }
}
