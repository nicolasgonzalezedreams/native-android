package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class AmbienceSeatPreferencesDescriptor implements Serializable {

  private List<AmbienceSeatPreferencesDescriptorItem> ambienceSeatPreferencesDescriptorItems;
  private List<Integer> sections;

  public List<AmbienceSeatPreferencesDescriptorItem> getAmbienceSeatPreferencesDescriptorItems() {
    return ambienceSeatPreferencesDescriptorItems;
  }

  public void setAmbienceSeatPreferencesDescriptorItems(
      List<AmbienceSeatPreferencesDescriptorItem> ambienceSeatPreferencesDescriptorItems) {
    this.ambienceSeatPreferencesDescriptorItems = ambienceSeatPreferencesDescriptorItems;
  }

  public List<Integer> getSections() {
    return sections;
  }

  public void setSections(List<Integer> sections) {
    this.sections = sections;
  }
}
