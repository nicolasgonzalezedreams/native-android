package com.odigeo.data.entity.shoppingCart;

public class ResumeBooking {

  private ResumeDataRequest resumeData;
  private long bookingId;
  private ItinerarySortCriteria sortCriteria;

  public long getBookingId() {
    return bookingId;
  }

  public void setBookingId(long value) {
    this.bookingId = value;
  }

  public ResumeDataRequest getResumeData() {
    return resumeData;
  }

  public void setResumeData(ResumeDataRequest resumeData) {
    this.resumeData = resumeData;
  }

  public ItinerarySortCriteria getSortCriteria() {
    return sortCriteria;
  }

  public void setSortCriteria(ItinerarySortCriteria sortCriteria) {
    this.sortCriteria = sortCriteria;
  }
}
