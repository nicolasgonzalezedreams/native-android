package com.odigeo.data.entity.shoppingCart.request;

import java.math.BigDecimal;

public class PromotionalCodeRequest {

  private String code;
  private BigDecimal discount;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }
}