package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class Insurance implements Serializable {
  private List<InsuranceUrl> conditionsUrls;
  private String policy;
  private String provider;

  private String title;
  private String description;
  private String subtitle;
  private String type;

  private String conditionAcceptance;
  private String documentLink;
  private String textConditions;
  private String totalPrice;
  private String totalPriceDetail;

  public List<InsuranceUrl> getConditionsUrls() {
    return conditionsUrls;
  }

  public void setConditionsUrls(List<InsuranceUrl> conditionsUrls) {
    this.conditionsUrls = conditionsUrls;
  }

  public String getPolicy() {
    return policy;
  }

  public void setPolicy(String policy) {
    this.policy = policy;
  }

  public String getProvider() {
    return provider;
  }

  public void setProvider(String provider) {
    this.provider = provider;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getConditionAcceptance() {
    return conditionAcceptance;
  }

  public void setConditionAcceptance(String conditionAcceptance) {
    this.conditionAcceptance = conditionAcceptance;
  }

  public String getDocumentLink() {
    return documentLink;
  }

  public void setDocumentLink(String documentLink) {
    this.documentLink = documentLink;
  }

  public String getTextConditions() {
    return textConditions;
  }

  public void setTextConditions(String textConditions) {
    this.textConditions = textConditions;
  }

  public String getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(String totalPrice) {
    this.totalPrice = totalPrice;
  }

  public String getTotalPriceDetail() {
    return totalPriceDetail;
  }

  public void setTotalPriceDetail(String totalPriceDetail) {
    this.totalPriceDetail = totalPriceDetail;
  }
}
