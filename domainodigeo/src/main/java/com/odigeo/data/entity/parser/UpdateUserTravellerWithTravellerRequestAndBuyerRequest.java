package com.odigeo.data.entity.parser;

import com.odigeo.data.entity.shoppingCart.request.BuyerRequest;
import com.odigeo.data.entity.shoppingCart.request.TravellerRequest;
import com.odigeo.data.entity.userData.UserAddress;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.helper.IdentificationHelper;
import com.odigeo.tools.DateHelperInterface;

/**
 * Created by javier.rebollo on 4/7/16.
 */
public class UpdateUserTravellerWithTravellerRequestAndBuyerRequest {

  private final IdentificationHelper mIdentificationHelper;
  private final DateHelperInterface mDateHelper;

  public UpdateUserTravellerWithTravellerRequestAndBuyerRequest(
      IdentificationHelper identificationHelper, DateHelperInterface dateHelper) {
    mIdentificationHelper = identificationHelper;
    mDateHelper = dateHelper;
  }

  public void updateUserTravellerWithBuyerRequest(UserTraveller userTraveller,
      BuyerRequest buyerRequest) {
    userTraveller.setBuyer(true);
    userTraveller.getUserProfile().setName(buyerRequest.getName());
    userTraveller.getUserProfile().setFirstLastName(buyerRequest.getLastNames());
    userTraveller.getUserProfile().setPhoneNumber(buyerRequest.getPhoneNumber1());
    userTraveller.getUserProfile().setPrefixPhoneNumber(buyerRequest.getCountryPhoneNumber1());
    userTraveller.setEmail(buyerRequest.getMail());
    mIdentificationHelper.checkAndUpdateIdentification(userTraveller,
        buyerRequest.getBuyerIdentificationTypeName(), buyerRequest.getIdentification());

    if (buyerRequest.getDayOfBirth() > 0) {
      userTraveller.getUserProfile()
          .setBirthDate(mDateHelper.getTimeStampFromDayMonthYear(buyerRequest.getYearOfBirth(),
              buyerRequest.getMonthOfBirth(), buyerRequest.getDayOfBirth()));
    }

    UserAddress userAddress = new UserAddress();

    if (userTraveller.getUserProfile().getUserAddress() != null) {
      userAddress = userTraveller.getUserProfile().getUserAddress();
    } else {
      userAddress.setUserProfileId(userTraveller.getUserProfile().getUserProfileId());
    }

    userAddress.setAddress(buyerRequest.getAddress());
    userAddress.setCity(buyerRequest.getCityName());
    userAddress.setState(buyerRequest.getStateName());
    userAddress.setPostalCode(buyerRequest.getZipCode());
    userAddress.setCountry(buyerRequest.getCountryCode());

    userTraveller.getUserProfile().setUserAddress(userAddress);
  }

  public void updateUserTravellerWithTravellerRequest(UserTraveller userTraveller,
      TravellerRequest travellerRequest) {

    userTraveller.getUserProfile()
        .setName(
            compareAndUpdate(userTraveller.getUserProfile().getName(), travellerRequest.getName()));
    userTraveller.getUserProfile()
        .setFirstLastName(compareAndUpdate(userTraveller.getUserProfile().getFirstLastName(),
            travellerRequest.getFirstLastName()));
    userTraveller.getUserProfile()
        .setSecondLastName(compareAndUpdate(userTraveller.getUserProfile().getSecondLastName(),
            travellerRequest.getSecondLastName()));
    userTraveller.getUserProfile()
        .setMiddleName(compareAndUpdate(userTraveller.getUserProfile().getMiddleName(),
            travellerRequest.getMiddleName()));
    userTraveller.getUserProfile()
        .setGender(compareAndUpdate(userTraveller.getUserProfile().getGender(),
            travellerRequest.getGender()));
    userTraveller.getUserProfile()
        .setNationalityCountryCode(
            compareAndUpdate(userTraveller.getUserProfile().getNationalityCountryCode(),
                travellerRequest.getNationalityCountryCode()));

    if (travellerRequest.getDayOfBirth() > 0 && travellerRequest.getYearOfBirth() > 0) {
      userTraveller.getUserProfile()
          .setBirthDate(mDateHelper.getTimeStampFromDayMonthYear(travellerRequest.getYearOfBirth(),
              travellerRequest.getMonthOfBirth(), travellerRequest.getDayOfBirth()));
    }

    mIdentificationHelper.checkAndUpdateIdentification(userTraveller,
        travellerRequest.getIdentificationTypeName(), travellerRequest.getIdentification());
  }

  private String compareAndUpdate(String original, String updated) {
    if (updated != null) {
      if (original == null) {
        return updated;
      } else {
        if (!original.equals(updated)) {
          return updated;
        }
      }
    }
    return original;
  }
}
