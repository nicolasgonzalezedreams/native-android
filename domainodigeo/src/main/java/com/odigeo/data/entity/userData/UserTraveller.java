package com.odigeo.data.entity.userData;

import java.util.List;

public class UserTraveller {

  private long mId;
  private long mUserTravellerId;
  private boolean mBuyer;
  private String mEmail;
  private TypeOfTraveller mTypeOfTraveller;
  private String mMealType;
  private long mUserId;
  //Relations
  private List<UserFrequentFlyer> mUserFrequentFlyers;
  private UserProfile mUserProfile;

  public UserTraveller() {
    //empty
  }

  public UserTraveller(long id, long userTravellerId, boolean buyer, String email,
      TypeOfTraveller typeOfTraveller, String mealType, long userId) {
    mId = id;
    mUserTravellerId = userTravellerId;
    mBuyer = buyer;
    mEmail = email;
    mTypeOfTraveller = typeOfTraveller;
    mMealType = mealType;
    mUserId = userId;
  }

  public UserTraveller(long id, long userTravellerId, boolean buyer, String email,
      TypeOfTraveller typeOfTraveller, String mealType, List<UserFrequentFlyer> userFrequentFlyers,
      com.odigeo.data.entity.userData.UserProfile userProfile, long userId) {
    mId = id;
    mUserTravellerId = userTravellerId;
    mBuyer = buyer;
    mEmail = email;
    mTypeOfTraveller = typeOfTraveller;
    mMealType = mealType;
    mUserId = userId;
    mUserFrequentFlyers = userFrequentFlyers;
    mUserProfile = userProfile;
  }

  public static UserTraveller cloneWithNewUserProfile(UserTraveller userTraveller,
      com.odigeo.data.entity.userData.UserProfile userProfile) {
    return new UserTraveller(userTraveller.getId(), userTraveller.getUserTravellerId(),
        userTraveller.getBuyer(), userTraveller.getEmail(), userTraveller.getTypeOfTraveller(),
        userTraveller.getMealType(), userTraveller.getUserFrequentFlyers(), userProfile,
        userTraveller.getUserId());
  }

  public long getId() {
    return mId;
  }

  public long getUserTravellerId() {
    return mUserTravellerId;
  }

  public void setUserTravellerId(long userTravellerId) {
    mUserTravellerId = userTravellerId;
  }

  public boolean getBuyer() {
    return mBuyer;
  }

  public void setBuyer(boolean buyer) {
    mBuyer = buyer;
  }

  public String getEmail() {
    return mEmail;
  }

  public void setEmail(String email) {
    mEmail = email;
  }

  public TypeOfTraveller getTypeOfTraveller() {
    return mTypeOfTraveller;
  }

  public void setTypeOfTraveller(TypeOfTraveller typeOfTraveller) {
    mTypeOfTraveller = typeOfTraveller;
  }

  public String getMealType() {
    return mMealType;
  }

  public long getUserId() {
    return mUserId;
  }

  public List<UserFrequentFlyer> getUserFrequentFlyers() {
    return mUserFrequentFlyers;
  }

  public void setUserFrequentFlyers(List<UserFrequentFlyer> userFrequentFlyers) {
    mUserFrequentFlyers = userFrequentFlyers;
  }

  public UserProfile getUserProfile() {
    return mUserProfile;
  }

  public void setUserProfile(UserProfile userProfile) {
    mUserProfile = userProfile;
  }

  @Override public boolean equals(Object userTravellerToCompare) {
    return (userTravellerToCompare instanceof UserTraveller
        && ((UserTraveller) userTravellerToCompare).getUserTravellerId()
        == this.getUserTravellerId());
  }

  public enum TypeOfTraveller {
    ADULT, CHILD, INFANT, UNKNOWN
  }
}
