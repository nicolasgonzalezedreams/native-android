package com.odigeo.data.entity.booking;

import java.io.Serializable;
import java.util.List;

public class MSLLocation extends Location implements Serializable {

  private List<SearchSegment> mSegmentDeparture;
  private List<SearchSegment> mSegmentDestination;

  public MSLLocation() {
    //empty
  }

  public MSLLocation(String cityIATACode, String cityName, String country, long geoNodeId,
      long latitude, String locationCode, String locationName, String locationType, long longitude,
      String matchName) {

    super(cityIATACode, cityName, country, geoNodeId, latitude, locationCode, locationName,
        locationType, longitude, matchName);
  }

  public MSLLocation(String cityIATACode, String cityName, String country, long geoNodeId,
      long latitude, String locationCode, String locationName, String locationType, long longitude,
      String matchName, List<SearchSegment> segmentDeparture,
      List<SearchSegment> segmentDestination) {

    super(cityIATACode, cityName, country, geoNodeId, latitude, locationCode, locationName,
        locationType, longitude, matchName);
    mSegmentDeparture = segmentDeparture;
    mSegmentDestination = segmentDestination;
  }

  public List<SearchSegment> getSegmentDeparture() {
    return mSegmentDeparture;
  }

  public List<SearchSegment> getSegmentDestination() {
    return mSegmentDestination;
  }
}
