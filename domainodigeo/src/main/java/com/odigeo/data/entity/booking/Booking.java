package com.odigeo.data.entity.booking;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

public class Booking implements Serializable {

  //TODO CHANGE THIS
  public static final String TRIP_TYPE_ONE_WAY = "ONE_WAY";
  public static final String TRIP_TYPE_ROUND_TRIP = "ROUND_TRIP";
  public static final String TRIP_TYPE_MULTI_SEGMENT = "MULTI_SEGMENT";

  public static final String BOOKING_STATUS_INIT = "INIT";
  public static final String BOOKING_STATUS_CONTRACT = "CONTRACT";
  public static final String BOOKING_STATUS_REQUEST = "REQUEST";
  public static final String BOOKING_STATUS_PENDING = "PENDING";
  public static final String BOOKING_STATUS_RETAINED = "RETAINED";
  public static final String BOOKING_STATUS_HOLD = "HOLD";
  public static final String BOOKING_STATUS_REJECTED = "REJECTED";
  public static final String BOOKING_STATUS_DIDNOTBUY = "DIDNOTBUY";

  private long mBookingId;
  private String mArrivalAirportCode;
  private long mArrivalLastLeg;
  private String mBookingStatus;
  private String mCurrency;
  private long mDepartureFirstLeg;
  private String mLocale;
  private String mMarket;
  private double mPrice;
  private String mSortCriteria;
  private long mTimestamp;
  private long mTotal;
  private String mTripType;
  private int mMultiDestinyIndex;
  private boolean mTravelCompanionNotification;
  private boolean mSoundOfData;
  private boolean mEnrichedBooking;

  private Buyer mBuyer;
  private List<Insurance> mInsurances;
  private List<Traveller> mTravellers;
  private List<Segment> mSegments;
  private boolean mSelected;

  private String eTag;

  private boolean stateChanged = false;

  public Booking() {
    //empty
  }

  public Booking(long bookingId, String arrivalAirportCode, long arrivalLastLeg,
      String bookingStatus, String currency, long departureFirstLeg, String locale, String market,
      double price, String sortCriteria, long timestamp, long total, String tripType,
      int multiDestinyIndex, boolean travelCompanionNotification, boolean soundOfData,
      boolean enrichedBooking) {
    mArrivalAirportCode = arrivalAirportCode;
    mArrivalLastLeg = arrivalLastLeg;
    mBookingId = bookingId;
    mBookingStatus = bookingStatus;
    mCurrency = currency;
    mDepartureFirstLeg = departureFirstLeg;
    mLocale = locale;
    mMarket = market;
    mPrice = price;
    mSortCriteria = sortCriteria;
    mTimestamp = timestamp;
    mTotal = total;
    mTripType = tripType;
    mMultiDestinyIndex = multiDestinyIndex;
    mTravelCompanionNotification = travelCompanionNotification;
    mSoundOfData = soundOfData;
    mEnrichedBooking = enrichedBooking;
  }

  public Booking(String arrivalAirportCode, long arrivalLastLeg, long bookingId,
      String bookingStatus, String currency, long departureFirstLeg, String locale, String market,
      double price, String sortCriteria, long timestamp, long total, String tripType, Buyer buyer,
      List<Insurance> insurances, List<Traveller> travellers, List<Segment> segments,
      boolean travelCompanionNotification, boolean soundOfData, boolean enrichedBooking) {
    mArrivalAirportCode = arrivalAirportCode;
    mArrivalLastLeg = arrivalLastLeg;
    mBookingId = bookingId;
    mBookingStatus = bookingStatus;
    mCurrency = currency;
    mDepartureFirstLeg = departureFirstLeg;
    mLocale = locale;
    mMarket = market;
    mPrice = price;
    mSortCriteria = sortCriteria;
    mTimestamp = timestamp;
    mTotal = total;
    mTripType = tripType;
    mBuyer = buyer;
    mInsurances = insurances;
    mTravellers = travellers;
    mSegments = segments;
    mTravelCompanionNotification = travelCompanionNotification;
    mSoundOfData = soundOfData;
    mEnrichedBooking = enrichedBooking;
  }

  public long getBookingId() {
    return mBookingId;
  }

  public void setBookingId(long bookingId) {
    mBookingId = bookingId;
  }

  public String getArrivalAirportCode() {
    return mArrivalAirportCode;
  }

  public void setArrivalAirportCode(String arrivalAirportCode) {
    mArrivalAirportCode = arrivalAirportCode;
  }

  public long getArrivalLastLeg() {
    return mArrivalLastLeg;
  }

  public void setArrivalLastLeg(long arrivalLastLeg) {
    mArrivalLastLeg = arrivalLastLeg;
  }

  public String getBookingStatus() {
    return mBookingStatus;
  }

  public void setBookingStatus(String bookingStatus) {
    mBookingStatus = bookingStatus;
  }

  public String getCurrency() {
    return mCurrency;
  }

  public void setCurrency(String currency) {
    mCurrency = currency;
  }

  public long getDepartureFirstLeg() {
    return mDepartureFirstLeg;
  }

  public void setDepartureFirstLeg(long departureFirstLeg) {
    mDepartureFirstLeg = departureFirstLeg;
  }

  public String getLocale() {
    return mLocale;
  }

  public void setLocale(String locale) {
    mLocale = locale;
  }

  public String getMarket() {
    return mMarket;
  }

  public void setMarket(String market) {
    mMarket = market;
  }

  public double getPrice() {
    return mPrice;
  }

  public void setPrice(double price) {
    mPrice = price;
  }

  public String getSortCriteria() {
    return mSortCriteria;
  }

  public void setSortCriteria(String sortCriteria) {
    mSortCriteria = sortCriteria;
  }

  //******************
  //     SETTERS
  //******************

  public long getTimestamp() {
    return mTimestamp;
  }

  public void setTimestamp(long timestamp) {
    mTimestamp = timestamp;
  }

  public long getTotal() {
    return mTotal;
  }

  public void setTotal(long total) {
    mTotal = total;
  }

  public String getTripType() {
    return mTripType;
  }

  public void setTripType(String tripType) {
    mTripType = tripType;
  }

  public Buyer getBuyer() {
    return mBuyer;
  }

  public void setBuyer(Buyer buyer) {
    mBuyer = buyer;
  }

  public List<Insurance> getInsurances() {
    return mInsurances;
  }

  public void setInsurances(List<Insurance> insurances) {
    mInsurances = insurances;
  }

  public List<Traveller> getTravellers() {
    return mTravellers;
  }

  public void setTravellers(List<Traveller> travellers) {
    mTravellers = travellers;
  }

  public List<Segment> getSegments() {
    return mSegments;
  }

  public void setSegments(List<Segment> segments) {
    mSegments = segments;
  }

  public boolean getIsTravelCompanionNotification() {
    return mTravelCompanionNotification;
  }

  public boolean getIsSoundOfData() {
    return mSoundOfData;
  }

  public boolean getIsEnrichedBooking() {
    return mEnrichedBooking;
  }

  public int getMultiDestinyIndex() {
    return mMultiDestinyIndex;
  }

  public void setTravelCompanionNotification(boolean travelCompanionNotification) {
    mTravelCompanionNotification = travelCompanionNotification;
  }

  public void setSoundOfData(boolean soundOfData) {
    mSoundOfData = soundOfData;
  }

  public void setEnrichedBooking(boolean enrichedBooking) {
    mEnrichedBooking = enrichedBooking;
  }

  public long getDepartureDate() {
    return getFirstSegment().getFirstSection().getDepartureDate();
  }

  public long getLastDepartureDate() {
    return getLastSegment().getLastSection().getDepartureDate();
  }

  public String getDepartureAirportCode() {
    return getFirstSegment().getFirstSection().getFrom().getLocationCode();
  }

  public String getArrivalCityName(int segmentPosition) {
    String arrivalCityName;

    try {
      arrivalCityName = getSegments().get(segmentPosition).getLastSection().getTo().getCityName();
    } catch (NullPointerException e) {
      arrivalCityName = "";
    }

    return arrivalCityName;
  }

  public long getArrivalDate(int segmentPosition) {
    return getSegments().get(segmentPosition).getLastSection().getArrivalDate();
  }

  public int getNumberOfKids() {
    int numberOfKids = 0;

    for (Traveller traveller : mTravellers) {
      if (traveller.getTravellerType() != null && traveller.getTravellerType()
          .equalsIgnoreCase(Traveller.TRAVELLER_TYPE_CHILD)) {
        numberOfKids++;
      }
    }

    return numberOfKids;
  }

  public int getNumberOfAdults() {
    int numberOfKids = 0;

    for (Traveller traveller : mTravellers) {
      if (traveller.getTravellerType().equalsIgnoreCase(Traveller.TRAVELLER_TYPE_ADULT)) {
        numberOfKids++;
      }
    }

    return numberOfKids;
  }

  public Segment getFirstSegment() {
    if (mSegments != null && mSegments.size() > 0) {
      return mSegments.get(0);
    }
    return null;
  }

  public Segment getLastSegment() {
    if (mSegments != null && mSegments.size() > 0) {
      return mSegments.get(mSegments.size() - 1);
    }
    return null;
  }

  @Override public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }
    Booking booking = (Booking) obj;
    return booking.getBookingId() == mBookingId;
  }

  public boolean isSelected() {
    return mSelected;
  }

  public void setSelected(boolean selected) {
    mSelected = selected;
  }

  public boolean isStateChanged() {
    return stateChanged;
  }

  public void setStateChanged(boolean stateChanged) {
    this.stateChanged = stateChanged;
  }

  public boolean isActive() {
    long currentTime = Calendar.getInstance().getTimeInMillis();

    switch (mTripType) {
      case TRIP_TYPE_ONE_WAY:
        return mDepartureFirstLeg >= currentTime;
      case TRIP_TYPE_ROUND_TRIP:
        return mArrivalLastLeg >= currentTime;
    }

    return false;
  }

  public boolean isOneWay() {
    return mTripType.equals(TRIP_TYPE_ONE_WAY);
  }

  public boolean isRoundTrip() {
    return mTripType.equals(TRIP_TYPE_ROUND_TRIP);
  }

  public boolean isMultiSegment() {
    return mTripType.equals(TRIP_TYPE_MULTI_SEGMENT);
  }

  public long getArrivalGeoNodeId() {
    Segment segment = getSegments().get(0);
    List<Section> sections = segment.getSectionsList();
    Section arrivalFlight = sections.get(sections.size() - 1);
    return arrivalFlight.getTo().getGeoNodeId();
  }

  public String getArrivalIataCode() {
    Segment segment = getSegments().get(0);
    List<Section> sections = segment.getSectionsList();
    Section arrivalFlight = sections.get(sections.size() - 1);
    return arrivalFlight.getTo().getCityIATACode();
  }

  public boolean isPastBooking() {
    long currentDate = Calendar.getInstance().getTimeInMillis();
    return mArrivalLastLeg > 0 && mArrivalLastLeg < currentDate;
  }

  public boolean isConfirmed() {
    return mBookingStatus.equals(BOOKING_STATUS_CONTRACT);
  }

  public boolean isPending() {
    return !isConfirmed() && !isCancelled();
  }

  public boolean isCancelled() {
    return mBookingStatus.equals(BOOKING_STATUS_REJECTED) || mBookingStatus.equals(
        BOOKING_STATUS_DIDNOTBUY);
  }
}
