package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class FlowConfigurationResponse implements Serializable {

  private String showFeeFromStep;
  private String showBaggageEstimationFromStep;
  private String showCollectionEstimationFromStep;
  private String insurancePriceMode;
  private String insuranceOptout;
  private String insuranceNag;
  private String insuranceRecommended;
  private String pricingModeLabel;
  private List<StepsConfiguration> stepsConfiguration;

  public String getShowFeeFromStep() {
    return showFeeFromStep;
  }

  public void setShowFeeFromStep(String showFeeFromStep) {
    this.showFeeFromStep = showFeeFromStep;
  }

  public String getShowBaggageEstimationFromStep() {
    return showBaggageEstimationFromStep;
  }

  public void setShowBaggageEstimationFromStep(String showBaggageEstimationFromStep) {
    this.showBaggageEstimationFromStep = showBaggageEstimationFromStep;
  }

  public String getShowCollectionEstimationFromStep() {
    return showCollectionEstimationFromStep;
  }

  public void setShowCollectionEstimationFromStep(String showCollectionEstimationFromStep) {
    this.showCollectionEstimationFromStep = showCollectionEstimationFromStep;
  }

  public String getInsurancePriceMode() {
    return insurancePriceMode;
  }

  public void setInsurancePriceMode(String insurancePriceMode) {
    this.insurancePriceMode = insurancePriceMode;
  }

  public String getInsuranceOptout() {
    return insuranceOptout;
  }

  public void setInsuranceOptout(String insuranceOptout) {
    this.insuranceOptout = insuranceOptout;
  }

  public String getInsuranceNag() {
    return insuranceNag;
  }

  public void setInsuranceNag(String insuranceNag) {
    this.insuranceNag = insuranceNag;
  }

  public String getInsuranceRecomended() {
    return insuranceRecommended;
  }

  public void setInsuranceRecomended(String insuranceRecomended) {
    this.insuranceRecommended = insuranceRecomended;
  }

  public String getPricingModeLabel() {
    return pricingModeLabel;
  }

  public void setPricingModeLabel(String pricingModeLabel) {
    this.pricingModeLabel = pricingModeLabel;
  }

  public List<StepsConfiguration> getStepsConfiguration() {
    return stepsConfiguration;
  }

  public void setStepsConfiguration(List<StepsConfiguration> stepsConfiguration) {
    this.stepsConfiguration = stepsConfiguration;
  }
}