package com.odigeo.data.entity.carrousel;

public class DelayedSectionCard extends SectionCard {

  private String departureDelay;
  private String arrivalDelay;

  public DelayedSectionCard(long id, CardType type, int priority, String image, String carrier,
      String carrierName, String flightId, CarouselLocation from, CarouselLocation to,
      String departureDelay, String arrivalDelay) {

    super(id, type, priority, image, carrier, carrierName, flightId, from, to);
    this.departureDelay = departureDelay;
    this.arrivalDelay = arrivalDelay;
  }

  public DelayedSectionCard() {
  }

  public String getDepartureDelay() {
    return departureDelay;
  }

  public void setDepartureDelay(final String departureDelay) {
    this.departureDelay = departureDelay;
  }

  public String getArrivalDelay() {
    return arrivalDelay;
  }

  public void setArrivalDelay(final String arrivalDelay) {
    this.arrivalDelay = arrivalDelay;
  }
}
