package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum BaggageAllowanceType implements Serializable {
  KG, PO, NI, SC, NP, SZ, VL, WE, ND
}
