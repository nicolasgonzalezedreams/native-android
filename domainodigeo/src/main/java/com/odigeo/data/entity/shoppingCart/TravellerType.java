package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum TravellerType implements Serializable {

  ADULT("mydatapassengercell_type_adult"), CHILD("mydatapassengercell_type_child"), INFANT(
      "mydatapassengercell_type_infant");

  private final String mText;

  TravellerType(String resourceId) {
    this.mText = resourceId;
  }

  public static TravellerType fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }

  @Override public String toString() {
    return mText;
  }
}
