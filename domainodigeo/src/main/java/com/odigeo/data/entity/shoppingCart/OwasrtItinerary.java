package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class OwasrtItinerary implements Serializable {

  private Integer id;
  private Integer itinerary;
  private Boolean greyListed;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getItinerary() {
    return itinerary;
  }

  public void setItinerary(Integer itinerary) {
    this.itinerary = itinerary;
  }

  public Boolean getGreyListed() {
    return greyListed;
  }

  public void setGreyListed(Boolean greyListed) {
    this.greyListed = greyListed;
  }
}
