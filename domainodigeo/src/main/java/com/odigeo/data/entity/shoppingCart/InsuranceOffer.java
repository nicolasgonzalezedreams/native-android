package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class InsuranceOffer implements Serializable {

  private List<Insurance> insurances;
  private String id;
  private BigDecimal totalPrice;
  private BigDecimal providerPrice;

  public List<Insurance> getInsurances() {
    return insurances;
  }

  public void setInsurances(List<Insurance> insurances) {
    this.insurances = insurances;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }

  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  public void setProviderPrice(BigDecimal providerPrice) {
    this.providerPrice = providerPrice;
  }
}
