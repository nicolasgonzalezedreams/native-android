package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;

public class Money implements Serializable {

  private BigDecimal amount;
  private String currency;

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }
}