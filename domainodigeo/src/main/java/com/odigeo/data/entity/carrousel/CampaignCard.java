package com.odigeo.data.entity.carrousel;

/**
 * Created by Jaime Toca on 25/1/16.
 */
public class CampaignCard extends Card {

  public static final String LOCALYTICS_CAMPAIGN_CARD_KEY = "campaign_card_key";
  private String mUrlText2;
  private String mUrl2;
  private String mTitle;
  private String mSubtitle;
  private String mUrl;
  private String mUrlText;
  private String mCampaignCardKey;
  private CardSubType subType;

  public CampaignCard() {
  }

  public CampaignCard(long id, CardSubType subtype, int priority, String image, String title,
      String subtitle, String url, String urlText, String url2, String urlText2,
      String campaignCardKey) {
    super(id, Card.CardType.PROMOTION_CARD, priority, image);
    mTitle = title;
    mSubtitle = subtitle;
    mUrl = url;
    mUrlText = urlText;
    mCampaignCardKey = campaignCardKey;
    mUrl2 = url2;
    mUrlText2 = urlText2;
    subType = subtype;
  }

  public String getTitle() {
    return mTitle;
  }

  public void setTitle(final String title) {
    mTitle = title;
  }

  public String getSubtitle() {
    return mSubtitle;
  }

  public void setSubtitle(final String subtitle) {
    mSubtitle = subtitle;
  }

  public String getUrl() {
    return mUrl;
  }

  public void setUrl(String url) {
    this.mUrl = url;
  }

  public String getUrlText() {
    return mUrlText;
  }

  public void setUrlText(String urlText) {
    this.mUrlText = urlText;
  }

  public String getUrlText2() {
    return mUrlText2;
  }

  public void setUrlText2(String mUrlText2) {
    this.mUrlText2 = mUrlText2;
  }

  public String getUrl2() {
    return mUrl2;
  }

  public void setUrl2(String mUrl2) {
    this.mUrl2 = mUrl2;
  }

  public CardSubType getSubType() {
    return subType;
  }

  public String getCampaignCardKey() {
    return mCampaignCardKey;
  }

  public enum CardSubType {
    LOGGED("Promotion offer logged"), NOT_LOGGED("Promotion offer not logged"), PROMOCODE(
        "Promocode offer"), DISCOUNT("Discount offer"), FEATURES("Features promotion"), OTHER(
        "Other offer"), NEWS("News (news)"), INBOX("Promo inbox");

    private String text;

    CardSubType(String text) {
      this.text = text;
    }

    public static CardSubType fromString(String text) {
      if (text != null) {
        for (CardSubType b : CardSubType.values()) {
          if (text.equalsIgnoreCase(b.text)) {
            return b;
          }
        }
      }
      return null;
    }

    public String getText() {
      return text;
    }
  }
}
