package com.odigeo.data.entity.parser;

import com.odigeo.data.entity.shoppingCart.request.BuyerRequest;
import com.odigeo.data.entity.shoppingCart.request.FrequentFlyerCardCodeRequest;
import com.odigeo.data.entity.shoppingCart.request.TravellerRequest;
import com.odigeo.data.entity.userData.UserAddress;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.jetbrains.annotations.Nullable;

public class TravellerRequestToUserTravellerParser {

  public UserTraveller travellerRequestToUserTraveller(TravellerRequest travellerRequest) {
    UserProfile userProfile = buildUserProfile(travellerRequest);
    UserIdentification userIdentification = buildUserIdentification(travellerRequest);

    List<UserIdentification> userIdentificationList = new ArrayList<>();
    if (userIdentification != null) {
      userIdentificationList.add(userIdentification);
    }

    List<UserFrequentFlyer> userFrequentFlyerList = buildUserFrequentFlyer(travellerRequest);

    return buildUserTraveller(travellerRequest, userProfile, userIdentificationList,
        userFrequentFlyerList, null);
  }

  private UserTraveller buildUserTraveller(TravellerRequest travellerRequest,
      UserProfile userProfile, List<UserIdentification> userIdentificationList,
      List<UserFrequentFlyer> userFrequentFlyersList, BuyerRequest buyerRequest) {
    UserTraveller userTraveller = new UserTraveller();

    if (travellerRequest.getTravellerTypeName() != null) {
      userTraveller.setTypeOfTraveller(
          UserTraveller.TypeOfTraveller.valueOf(travellerRequest.getTravellerTypeName()));
    }

    userTraveller.setUserProfile(userProfile);
    userTraveller.getUserProfile().setUserIdentificationList(userIdentificationList);
    userTraveller.setUserFrequentFlyers(userFrequentFlyersList);

    addDataFromBuyerRequestToUserTraveller(buyerRequest, userTraveller);
    return userTraveller;
  }

  private void addDataFromBuyerRequestToUserTraveller(BuyerRequest buyerRequest,
      UserTraveller userTraveller) {
    if (buyerRequest != null) {
      userTraveller.setBuyer(true);

      userTraveller.getUserProfile().setPrefixPhoneNumber(buyerRequest.getCountryPhoneNumber1());
      userTraveller.getUserProfile().setPhoneNumber(buyerRequest.getPhoneNumber1());
      userTraveller.getUserProfile()
          .setPrefixAlternatePhoneNumber(buyerRequest.getCountryPhoneNumber2());
      userTraveller.getUserProfile().setAlternatePhoneNumber(buyerRequest.getPhoneNumber2());

      userTraveller.setEmail(buyerRequest.getMail());
      userTraveller.getUserProfile().setUserAddress(buildUserAddressFromBuyerRequest(buyerRequest));

      if (buyerRequest.getBuyerIdentificationTypeName() != null
          && userTraveller.getUserProfile().getUserIdentificationList().size() == 0) {

        userTraveller.getUserProfile()
            .getUserIdentificationList()
            .add(buildUserIdentificationFromBuyerRequest(buyerRequest));
      }
    }
  }

  private UserAddress buildUserAddressFromBuyerRequest(BuyerRequest buyerRequest) {
    UserAddress userAddress = new UserAddress();
    userAddress.setAddress(buyerRequest.getAddress());
    userAddress.setCity(buyerRequest.getCityName());
    userAddress.setCountry(buyerRequest.getCountryCode());
    userAddress.setState(buyerRequest.getStateName());
    userAddress.setPostalCode(buyerRequest.getZipCode());
    return userAddress;
  }

  private UserIdentification buildUserIdentificationFromBuyerRequest(BuyerRequest buyerRequest) {
    UserIdentification userIdentification = new UserIdentification();
    userIdentification.setIdentificationType(UserIdentification.IdentificationType.valueOf(
        buyerRequest.getBuyerIdentificationTypeName()));
    userIdentification.setIdentificationId(buyerRequest.getIdentification());
    return userIdentification;
  }

  @Nullable private UserIdentification buildUserIdentification(TravellerRequest travellerRequest) {
    if (travellerRequest.getIdentificationTypeName() == null) {
      return null;
    }

    UserIdentification userIdentification = new UserIdentification();

    if (travellerRequest.getIdentificationTypeName() != null) {
      userIdentification.setIdentificationType(UserIdentification.IdentificationType.valueOf(
          travellerRequest.getIdentificationTypeName()));
    }
    userIdentification.setIdentificationCountryCode(
        travellerRequest.getIdentificationIssueContryCode());
    if (travellerRequest.getIdentificationExpirationYear() != 0
        && travellerRequest.getIdentificationExpirationMonth() != 0
        && travellerRequest.getIdentificationExpirationDay() != 0) {
      userIdentification.setIdentificationExpirationDate(
          getLongDate(travellerRequest.getIdentificationExpirationYear(),
              travellerRequest.getIdentificationExpirationMonth(),
              travellerRequest.getIdentificationExpirationDay()));
    }
    userIdentification.setIdentificationId(travellerRequest.getIdentification());

    return userIdentification;
  }

  private UserProfile buildUserProfile(TravellerRequest travellerRequest) {
    UserProfile userProfile = new UserProfile();
    userProfile.setGender(travellerRequest.getGender());

    if (travellerRequest.getTitleName() != null) {
      userProfile.setTitle(
          UserProfile.Title.valueOf(travellerRequest.getTitleName().toUpperCase()));
    }

    userProfile.setName(travellerRequest.getName());
    userProfile.setMiddleName(travellerRequest.getMiddleName());
    userProfile.setFirstLastName(travellerRequest.getFirstLastName());
    userProfile.setSecondLastName(travellerRequest.getSecondLastName());

    if ((travellerRequest.getDayOfBirth() != 0) && (travellerRequest.getMonthOfBirth() != 0) && (
        travellerRequest.getYearOfBirth()
            != 0)) {
      userProfile.setBirthDate(
          getLongDate(travellerRequest.getYearOfBirth(), travellerRequest.getMonthOfBirth(),
              travellerRequest.getDayOfBirth()));
    }

    userProfile.setNationalityCountryCode(travellerRequest.getNationalityCountryCode());

    return userProfile;
  }

  private List<UserFrequentFlyer> buildUserFrequentFlyer(TravellerRequest travellerRequest) {
    List<UserFrequentFlyer> userFrequentFlyerList = new ArrayList<>();

    List<FrequentFlyerCardCodeRequest> frequentFlyerCardCodeRequestsList =
        travellerRequest.getFrequentFlyerAirlineCodes();

    if (frequentFlyerCardCodeRequestsList != null) {

      for (int i = 0; i < frequentFlyerCardCodeRequestsList.size(); i++) {

        userFrequentFlyerList.add(
            new UserFrequentFlyer(0, 0, frequentFlyerCardCodeRequestsList.get(i).getCarrierCode(),
                frequentFlyerCardCodeRequestsList.get(i).getPassengerCardNumber(), 0));
      }
    }

    return userFrequentFlyerList;
  }

  private long getLongDate(int year, int month, int day) {
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.YEAR, year);
    calendar.set(Calendar.MONTH, month - 1);
    calendar.set(Calendar.DAY_OF_MONTH, day);
    return calendar.getTimeInMillis();
  }
}