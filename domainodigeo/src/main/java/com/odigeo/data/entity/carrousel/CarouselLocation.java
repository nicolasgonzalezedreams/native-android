package com.odigeo.data.entity.carrousel;

/**
 * Created by Jaime Toca on 25/1/16.
 */
public class CarouselLocation {

  private String iata;

  private long date;

  private String terminal;

  private String description;

  public CarouselLocation(final String iata, final long date, final String terminal,
      final String description) {
    this.iata = iata;
    this.date = date;
    this.terminal = terminal;
    this.description = description;
  }

  public CarouselLocation() {

  }

  public String getIata() {

    return iata;
  }

  public void setIata(final String iata) {

    this.iata = iata;
  }

  public long getDate() {

    return date;
  }

  public void setDate(final long date) {

    this.date = date;
  }

  public String getTerminal() {

    return terminal;
  }

  public void setTerminal(final String terminal) {

    this.terminal = terminal;
  }

  public String getDescription() {

    return description;
  }

  public void setDescription(final String description) {

    this.description = description;
  }
}
