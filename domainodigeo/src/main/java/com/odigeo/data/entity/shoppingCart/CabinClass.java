package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum CabinClass implements Serializable {
  BUSINESS, FIRST, TOURIST, PREMIUM_ECONOMY, ECONOMIC_DISCOUNTED;
}
