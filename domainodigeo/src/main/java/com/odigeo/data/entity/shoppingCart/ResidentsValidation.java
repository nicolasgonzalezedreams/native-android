package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class ResidentsValidation implements Serializable {
  private List<TravellerResidentValidation> travellersResidentValidation;

  public List<TravellerResidentValidation> getTravellersResidentValidation() {
    return travellersResidentValidation;
  }

  public void setTravellersResidentValidation(
      List<TravellerResidentValidation> travellersResidentValidation) {
    this.travellersResidentValidation = travellersResidentValidation;
  }
}
