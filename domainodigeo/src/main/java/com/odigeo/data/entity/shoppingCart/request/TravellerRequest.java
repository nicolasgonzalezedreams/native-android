package com.odigeo.data.entity.shoppingCart.request;

import java.util.List;

public class TravellerRequest {

  private List<FrequentFlyerCardCodeRequest> frequentFlyerAirlineCodes;
  private String travellerTypeName;
  private String titleName;
  private String name;
  private String firstLastName;
  private String secondLastName;
  private String middleName;
  private String gender;
  private int dayOfBirth;
  private int monthOfBirth;
  private int yearOfBirth;
  private String nationalityCountryCode;
  private String countryCodeOfResidence;
  private String identification;
  private String identificationTypeName;
  private int identificationExpirationDay;
  private int identificationExpirationMonth;
  private int identificationExpirationYear;
  private String identificationIssueContryCode;
  private String localityCodeOfResidence;
  private List<BaggageSelectionRequest> baggageSelections;
  private String mealName;

  public List<FrequentFlyerCardCodeRequest> getFrequentFlyerAirlineCodes() {
    return frequentFlyerAirlineCodes;
  }

  public void setFrequentFlyerAirlineCodes(
      List<FrequentFlyerCardCodeRequest> frequentFlyerAirlineCodes) {
    this.frequentFlyerAirlineCodes = frequentFlyerAirlineCodes;
  }

  public String getTravellerTypeName() {
    return travellerTypeName;
  }

  public void setTravellerTypeName(String travellerTypeName) {
    this.travellerTypeName = travellerTypeName;
  }

  public String getTitleName() {
    return titleName;
  }

  public void setTitleName(String titleName) {
    this.titleName = titleName;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFirstLastName() {
    return firstLastName;
  }

  public void setFirstLastName(String firstLastName) {
    this.firstLastName = firstLastName;
  }

  public String getSecondLastName() {
    return secondLastName;
  }

  public void setSecondLastName(String secondLastName) {
    this.secondLastName = secondLastName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public int getDayOfBirth() {
    return dayOfBirth;
  }

  public void setDayOfBirth(int dayOfBirth) {
    this.dayOfBirth = dayOfBirth;
  }

  public int getMonthOfBirth() {
    return monthOfBirth;
  }

  public void setMonthOfBirth(int monthOfBirth) {
    this.monthOfBirth = monthOfBirth;
  }

  public int getYearOfBirth() {
    return yearOfBirth;
  }

  public void setYearOfBirth(int yearOfBirth) {
    this.yearOfBirth = yearOfBirth;
  }

  public String getNationalityCountryCode() {
    return nationalityCountryCode;
  }

  public void setNationalityCountryCode(String nationalityCountryCode) {
    this.nationalityCountryCode = nationalityCountryCode;
  }

  public String getCountryCodeOfResidence() {
    return countryCodeOfResidence;
  }

  public void setCountryCodeOfResidence(String countryCodeOfResidence) {
    this.countryCodeOfResidence = countryCodeOfResidence;
  }

  public String getIdentification() {
    return identification;
  }

  public void setIdentification(String identification) {
    this.identification = identification;
  }

  public String getIdentificationTypeName() {
    return identificationTypeName;
  }

  public void setIdentificationTypeName(String identificationTypeName) {
    this.identificationTypeName = identificationTypeName;
  }

  public int getIdentificationExpirationDay() {
    return identificationExpirationDay;
  }

  public void setIdentificationExpirationDay(int identificationExpirationDay) {
    this.identificationExpirationDay = identificationExpirationDay;
  }

  public int getIdentificationExpirationMonth() {
    return identificationExpirationMonth;
  }

  public void setIdentificationExpirationMonth(int identificationExpirationMonth) {
    this.identificationExpirationMonth = identificationExpirationMonth;
  }

  public int getIdentificationExpirationYear() {
    return identificationExpirationYear;
  }

  public void setIdentificationExpirationYear(int identificationExpirationYear) {
    this.identificationExpirationYear = identificationExpirationYear;
  }

  public String getIdentificationIssueContryCode() {
    return identificationIssueContryCode;
  }

  public void setIdentificationIssueContryCode(String identificationIssueContryCode) {
    this.identificationIssueContryCode = identificationIssueContryCode;
  }

  public String getLocalityCodeOfResidence() {
    return localityCodeOfResidence;
  }

  public void setLocalityCodeOfResidence(String localityCodeOfResidence) {
    this.localityCodeOfResidence = localityCodeOfResidence;
  }

  public List<BaggageSelectionRequest> getBaggageSelections() {
    return baggageSelections;
  }

  public void setBaggageSelections(List<BaggageSelectionRequest> baggageSelections) {
    this.baggageSelections = baggageSelections;
  }

  public String getMealName() {
    return mealName;
  }

  public void setMealName(String mealName) {
    this.mealName = mealName;
  }

  public int getBaggagePiecesCount() {
    int pieces = 0;

    for (int i = 0; i < baggageSelections.size(); i++) {
      if (baggageSelections.get(i).getBaggageDescriptor().getPieces() != null) {
        pieces += baggageSelections.get(i).getBaggageDescriptor().getPieces();
      }
    }

    return pieces;
  }
}
