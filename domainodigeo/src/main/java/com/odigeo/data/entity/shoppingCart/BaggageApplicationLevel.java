package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum BaggageApplicationLevel implements Serializable {
  SECTION, SEGMENT, ITINERARY;

  public static BaggageApplicationLevel fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }
}
