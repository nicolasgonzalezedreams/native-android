package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class Phone implements Serializable {
  private String countryCode;
  private String number;

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }
}
