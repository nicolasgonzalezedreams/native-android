package com.odigeo.data.entity.userData;

public class UserLogin {

  private long mId;
  private long mUserLoginId;
  private String mPassword;
  private String mAccessToken;
  private long mExpirationTokenDate;
  private String mHashCode;
  private String mActivationCode;
  private long mActivationCodeDate;
  private long mLoginAttemptFailed;
  private long mLastLoginDate;
  private String mRefreshToken;
  private long mUserId;

  public UserLogin() {
    //empty
  }

  public UserLogin(long id, long userLoginId, String password, String accessToken,
      long expirationTokenDate, String hashCode, String activationCode, long activationCodeDate,
      long loginAttemptFailed, long lastLoginDate, String refreshToken, long userId) {

    this.mId = id;
    this.mUserLoginId = userLoginId;
    this.mPassword = password;
    this.mAccessToken = accessToken;
    this.mExpirationTokenDate = expirationTokenDate;
    this.mHashCode = hashCode;
    this.mActivationCode = activationCode;
    this.mActivationCodeDate = activationCodeDate;
    this.mLoginAttemptFailed = loginAttemptFailed;
    this.mLastLoginDate = lastLoginDate;
    this.mRefreshToken = refreshToken;
    this.mUserId = userId;
  }

  public long getId() {
    return mId;
  }

  public long getUserLoginId() {
    return mUserLoginId;
  }

  public String getPassword() {
    return mPassword;
  }

  public void setPassword(String password) {
    mPassword = password;
  }

  public String getAccessToken() {
    return mAccessToken;
  }

  public long getExpirationTokenDate() {
    return mExpirationTokenDate;
  }

  public String getHashCode() {
    return mHashCode;
  }

  public String getActivationCode() {
    return mActivationCode;
  }

  public long getActivationCodeDate() {
    return mActivationCodeDate;
  }

  public long getLoginAttemptFailed() {
    return mLoginAttemptFailed;
  }

  public long getLastLoginDate() {
    return mLastLoginDate;
  }

  public String getRefreshToken() {
    return mRefreshToken;
  }

  public long getUserId() {
    return mUserId;
  }
}
