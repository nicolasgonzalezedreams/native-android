package com.odigeo.data.entity.contract;

import java.io.Serializable;

public enum FormSendTypeDTO implements Serializable {

  GET, POST
}
