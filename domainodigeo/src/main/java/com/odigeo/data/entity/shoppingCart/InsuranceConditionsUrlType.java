package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum InsuranceConditionsUrlType implements Serializable {
  BASIC, EXTENDED;
}
