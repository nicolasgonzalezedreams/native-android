package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum Step implements Serializable {
  RESULTS, SUMMARY, PASSENGER, INSURANCE, PAYMENT, POSTPAYMENT, DEFAULT, CONFIRMATION, MYTRIPS, SEARCH, Step, STEP
}