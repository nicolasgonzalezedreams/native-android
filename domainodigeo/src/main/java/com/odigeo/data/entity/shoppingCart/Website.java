package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class Website implements Serializable {

  private String code;
  private String brand;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }
}