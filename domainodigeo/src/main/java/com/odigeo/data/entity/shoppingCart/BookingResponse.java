package com.odigeo.data.entity.shoppingCart;

import com.odigeo.data.entity.contract.CollectionSummary;
import com.odigeo.data.entity.contract.UserInteractionNeededResponse;
import java.io.Serializable;
import java.util.List;

public class BookingResponse extends BaseResponse implements Serializable {

  private boolean processed;
  private BookingResponseStatus status;
  private PricingBreakdown pricingBreakdown;
  private BookingDetail bookingDetail;
  private List<ShoppingCartCollectionOption> collectionOptions;
  private BankTransferResponse bankTransferResponse;
  private List<BankTransferData> bankTransferData;
  private CollectionSummary collectionSummary;
  private List<MarketingRevenueDataAdapter> marketingRevenue;
  private UserInteractionNeededResponse userInteractionNeededResponse;

  public boolean isProcessed() {
    return processed;
  }

  public void setProcessed(boolean processed) {
    this.processed = processed;
  }

  public BookingResponseStatus getStatus() {
    return status;
  }

  public void setStatus(BookingResponseStatus status) {
    this.status = status;
  }

  public PricingBreakdown getPricingBreakdown() {
    return pricingBreakdown;
  }

  public void setPricingBreakdown(PricingBreakdown pricingBreakdown) {
    this.pricingBreakdown = pricingBreakdown;
  }

  public BookingDetail getBookingDetail() {
    return bookingDetail;
  }

  public void setBookingDetail(BookingDetail bookingDetail) {
    this.bookingDetail = bookingDetail;
  }

  public List<ShoppingCartCollectionOption> getCollectionOptions() {
    return collectionOptions;
  }

  public void setCollectionOptions(List<ShoppingCartCollectionOption> collectionOptions) {
    this.collectionOptions = collectionOptions;
  }

  public BankTransferResponse getBankTransferResponse() {
    return bankTransferResponse;
  }

  public void setBankTransferResponse(BankTransferResponse bankTransferResponse) {
    this.bankTransferResponse = bankTransferResponse;
  }

  public List<BankTransferData> getBankTransferData() {
    return bankTransferData;
  }

  public void setBankTransferData(List<BankTransferData> bankTransferData) {
    this.bankTransferData = bankTransferData;
  }

  public CollectionSummary getCollectionSummary() {
    return collectionSummary;
  }

  public void setCollectionSummary(CollectionSummary collectionSummary) {
    this.collectionSummary = collectionSummary;
  }

  public List<MarketingRevenueDataAdapter> getMarketingRevenue() {
    return marketingRevenue;
  }

  public void setMarketingRevenue(List<MarketingRevenueDataAdapter> marketingRevenue) {
    this.marketingRevenue = marketingRevenue;
  }

  public UserInteractionNeededResponse getUserInteractionNeededResponse() {
    return userInteractionNeededResponse;
  }

  public void setUserInteractionNeededResponse(
      UserInteractionNeededResponse userInteractionNeededResponse) {
    this.userInteractionNeededResponse = userInteractionNeededResponse;
  }
}