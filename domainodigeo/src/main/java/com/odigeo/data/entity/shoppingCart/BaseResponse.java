package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class BaseResponse extends ErrorResponse implements Serializable {

  private List<MessageResponse> messages;
  private List<ExtensionResponse> extensions;
  private List<FlowError> flowErrors;

  public List<MessageResponse> getMessages() {
    return messages;
  }

  public void setMessages(List<MessageResponse> messages) {
    this.messages = messages;
  }

  public List<ExtensionResponse> getExtensions() {
    return extensions;
  }

  public void setExtensions(List<ExtensionResponse> extensions) {
    this.extensions = extensions;
  }

  public List<FlowError> getFlowErrors() {
    return flowErrors;
  }

  public void setFlowErrors(List<FlowError> flowErrors) {
    this.flowErrors = flowErrors;
  }
}
