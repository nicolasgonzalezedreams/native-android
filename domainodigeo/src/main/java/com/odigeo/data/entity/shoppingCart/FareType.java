package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum FareType implements Serializable {

  PUBLIC, NEGOTIATED, PRIVATE;

  public static FareType fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }
}
