package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class InsuranceProviderBookings implements Serializable {

  private List<InsuranceProviderBooking> bookings;

  public List<InsuranceProviderBooking> getBookings() {
    return bookings;
  }

  public void setBookings(List<InsuranceProviderBooking> bookings) {
    this.bookings = bookings;
  }
}