package com.odigeo.data.entity.booking;

import java.io.Serializable;

public class Section implements Serializable {

  private long mId;
  private String mSectionId;
  private String mAircraft;
  private long mArrivalDate;
  private String mArrivalTerminal;
  private String mBaggageAllowanceQuantity;
  private String mBaggageAllowanceType;
  private String mCabinClass;
  private long mDepartureDate;
  private String mDepartureTerminal;
  private long mDuration;
  private String mFlightID;
  private String mSectionType;

  //Relations
  private Carrier mCarrier;
  private LocationBooking mFrom;
  private ItineraryBooking mItineraryBooking;
  private Segment mSegment;
  private LocationBooking mTo;

  private FlightStats mFlightStats;

  public Section() {
    //empty
  }

  public Section(long id, String sectionId, String aircraft, long arrivalDate,
      String arrivalTerminal, String baggageAllowanceQuantity, String baggageAllowanceType,
      String cabinClass, long departureDate, String departureTerminal, long duration,
      String flightID, String sectionType) {
    mId = id;
    mSectionId = sectionId;
    mAircraft = aircraft;
    mArrivalDate = arrivalDate;
    mArrivalTerminal = arrivalTerminal;
    mBaggageAllowanceQuantity = baggageAllowanceQuantity;
    mBaggageAllowanceType = baggageAllowanceType;
    mCabinClass = cabinClass;
    mDepartureDate = departureDate;
    mDepartureTerminal = departureTerminal;
    mDuration = duration;
    mFlightID = flightID;
    mSectionType = sectionType;
  }

  public Section(long id, String sectionId, String aircraft, long arrivalDate, String rivalTerminal,
      String baggageAllowanceQuantity, String baggageAllowanceType, String ccabinClass,
      long departureDate, String departureTerminal, long duration, String flightID,
      String sectionType, Carrier carrier, LocationBooking from, ItineraryBooking itineraryBooking,
      Segment segment, LocationBooking to) {
    mId = id;
    mSectionId = sectionId;
    mAircraft = aircraft;
    mArrivalDate = arrivalDate;
    mArrivalTerminal = rivalTerminal;
    mBaggageAllowanceQuantity = baggageAllowanceQuantity;
    mBaggageAllowanceType = baggageAllowanceType;
    mCabinClass = ccabinClass;
    mDepartureDate = departureDate;
    mDepartureTerminal = departureTerminal;
    mDuration = duration;
    mFlightID = flightID;
    mSectionType = sectionType;
    mCarrier = carrier;
    mFrom = from;
    mItineraryBooking = itineraryBooking;
    mSegment = segment;
    mTo = to;
  }

  public long getId() {
    return mId;
  }

  public void setId(long id) {
    mId = id;
  }

  public String getSectionId() {
    return mSectionId;
  }

  public void setSectionId(String mSectionId) {
    this.mSectionId = mSectionId;
  }

  public String getAircraft() {
    return mAircraft;
  }

  public void setAircraft(String mAircraft) {
    this.mAircraft = mAircraft;
  }

  public long getArrivalDate() {
    return mArrivalDate;
  }

  public void setArrivalDate(long mArrivalDate) {
    this.mArrivalDate = mArrivalDate;
  }

  public String getArrivalTerminal() {
    return mArrivalTerminal;
  }

  public void setArrivalTerminal(String mArrivalTerminal) {
    this.mArrivalTerminal = mArrivalTerminal;
  }

  public String getBaggageAllowanceQuantity() {
    return mBaggageAllowanceQuantity;
  }

  public void setBaggageAllowanceQuantity(String mBaggageAllowanceQuantity) {
    this.mBaggageAllowanceQuantity = mBaggageAllowanceQuantity;
  }

  public String getBaggageAllowanceType() {
    return mBaggageAllowanceType;
  }

  public void setBaggageAllowanceType(String mBaggageAllowanceType) {
    this.mBaggageAllowanceType = mBaggageAllowanceType;
  }

  public String getCabinClass() {
    return mCabinClass;
  }

  public void setCabinClass(String mCabinClass) {
    this.mCabinClass = mCabinClass;
  }

  public long getDepartureDate() {
    return mDepartureDate;
  }

  public void setDepartureDate(long mDepartureDate) {
    this.mDepartureDate = mDepartureDate;
  }

  public String getDepartureTerminal() {
    return mDepartureTerminal;
  }

  public void setDepartureTerminal(String mDepartureTerminal) {
    this.mDepartureTerminal = mDepartureTerminal;
  }

  public long getDuration() {
    return mDuration;
  }

  public void setDuration(long mDuration) {
    this.mDuration = mDuration;
  }

  public String getFlightID() {
    return mFlightID;
  }

  public void setFlightID(String mFlightID) {
    this.mFlightID = mFlightID;
  }

  public String getSectionType() {
    return mSectionType;
  }

  public void setSectionType(String mSectionType) {
    this.mSectionType = mSectionType;
  }

  public Carrier getCarrier() {
    return mCarrier;
  }

  public void setCarrier(Carrier carrier) {
    mCarrier = carrier;
  }

  public LocationBooking getFrom() {
    return mFrom;
  }

  public void setFrom(LocationBooking from) {
    mFrom = from;
  }

  public ItineraryBooking getItineraryBooking() {
    return mItineraryBooking;
  }

  public void setItineraryBooking(ItineraryBooking itineraryBooking) {
    mItineraryBooking = itineraryBooking;
  }

  public Segment getSegment() {
    return mSegment;
  }

  public void setSegment(Segment segment) {
    mSegment = segment;
  }

  public LocationBooking getTo() {
    return mTo;
  }

  public void setTo(LocationBooking to) {
    mTo = to;
  }

  public FlightStats getFlightStats() {
    return mFlightStats;
  }

  public void setFlightStats(FlightStats flightStats) {
    mFlightStats = flightStats;
  }
}
