package com.odigeo.data.entity.userData;

public class UserAirlinePreferences {

  private long mId;
  private long mUserAirlinePreferencesId;
  private String mAirlineCode;
  private long mUserId;

  public UserAirlinePreferences() {
    //empty
  }

  public UserAirlinePreferences(long id, long userAirlinePreferencesId, String airlineCode) {
    this.mId = id;
    this.mUserAirlinePreferencesId = userAirlinePreferencesId;
    this.mAirlineCode = airlineCode;
  }

  public UserAirlinePreferences(long id, long userAirlinePreferencesId, String airlineCode,
      long userId) {
    this.mId = id;
    this.mUserAirlinePreferencesId = userAirlinePreferencesId;
    this.mAirlineCode = airlineCode;
    this.mUserId = userId;
  }

  public long getId() {
    return mId;
  }

  public long getUserAirlinePreferencesId() {
    return mUserAirlinePreferencesId;
  }

  public String getAirlineCode() {
    return mAirlineCode;
  }

  public long getUserId() {
    return mUserId;
  }

  public void setUserId(long userId) {
    this.mUserId = userId;
  }
}
