package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class Preferences implements Serializable {

  private String currency;
  private String locale;
  private String mkportal;
  private String webSite;

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getLocale() {
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
  }

  public String getMkportal() {
    return mkportal;
  }

  public void setMkportal(String mkportal) {
    this.mkportal = mkportal;
  }

  public String getWebSite() {
    return webSite;
  }

  public void setWebSite(String webSite) {
    this.webSite = webSite;
  }
}
