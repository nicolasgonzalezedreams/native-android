package com.odigeo.data.entity.userData;

public class UserDevice {

  private long mId;
  private long mUserDeviceId;
  private String mDeviceName;
  private String mDeviceId;
  private boolean mIsPrimary;
  private String mAlias;
  private long mUserId;

  public UserDevice() {
    //empty
  }

  public UserDevice(long id, long userDeviceId, String deviceName, String deviceId,
      boolean isPrimary, String alias, long userId) {
    mId = id;
    mUserDeviceId = userDeviceId;
    mDeviceName = deviceName;
    mDeviceId = deviceId;
    mIsPrimary = isPrimary;
    mAlias = alias;
    mUserId = userId;
  }

  public long getId() {
    return mId;
  }

  public long getUserDeviceId() {
    return mUserDeviceId;
  }

  public String getDeviceName() {
    return mDeviceName;
  }

  public String getDeviceId() {
    return mDeviceId;
  }

  public boolean getIsPrimary() {
    return mIsPrimary;
  }

  public String getAlias() {
    return mAlias;
  }

  public long getUserId() {
    return mUserId;
  }
}
