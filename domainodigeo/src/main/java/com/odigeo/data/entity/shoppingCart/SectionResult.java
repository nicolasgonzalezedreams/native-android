package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class SectionResult implements Serializable {

  private Section section;
  private int id;

  public Section getSection() {
    return section;
  }

  public void setSection(Section section) {
    this.section = section;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}
