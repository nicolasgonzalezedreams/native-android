package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class AvailableProductsResponse implements Serializable {

  private List<InsuranceOffer> insuranceOffers;

  public List<InsuranceOffer> getInsuranceOffers() {
    return insuranceOffers;
  }

  public void setInsuranceOffers(List<InsuranceOffer> insuranceOffers) {
    this.insuranceOffers = insuranceOffers;
  }
}
