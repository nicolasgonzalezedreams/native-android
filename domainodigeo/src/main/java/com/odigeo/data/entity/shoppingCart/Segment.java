package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class Segment implements Serializable {

  private List<Integer> sections;
  private int carrier;
  private CabinClass cabinClass;
  private Long duration;
  private Integer seats;
  private Boolean timesUndefined;

  public List<Integer> getSections() {
    return sections;
  }

  public void setSections(List<Integer> sections) {
    this.sections = sections;
  }

  public int getCarrier() {
    return carrier;
  }

  public void setCarrier(int carrier) {
    this.carrier = carrier;
  }

  public CabinClass getCabinClass() {
    return cabinClass;
  }

  public void setCabinClass(CabinClass cabinClass) {
    this.cabinClass = cabinClass;
  }

  public Long getDuration() {
    return duration;
  }

  public void setDuration(Long duration) {
    this.duration = duration;
  }

  public Integer getSeats() {
    return seats;
  }

  public void setSeats(Integer seats) {
    this.seats = seats;
  }

  public Boolean getTimesUndefined() {
    return timesUndefined;
  }

  public void setTimesUndefined(Boolean timesUndefined) {
    this.timesUndefined = timesUndefined;
  }
}
