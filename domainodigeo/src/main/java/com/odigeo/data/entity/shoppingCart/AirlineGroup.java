package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class AirlineGroup implements Serializable {
  private List<Carrier> carriers;

  public List<Carrier> getCarriers() {
    return carriers;
  }

  public void setCarriers(List<Carrier> carriers) {
    this.carriers = carriers;
  }
}
