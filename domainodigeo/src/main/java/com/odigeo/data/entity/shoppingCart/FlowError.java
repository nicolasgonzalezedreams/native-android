package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class FlowError implements Serializable {

  private List<FlowErrorMessage> messages;
  private String errorMessageKey;
  private ErrorType errorType;

  public List<FlowErrorMessage> getMessages() {
    return messages;
  }

  public void setMessages(List<FlowErrorMessage> messages) {
    this.messages = messages;
  }

  public String getErrorMessageKey() {
    return errorMessageKey;
  }

  public void setErrorMessageKey(String errorMessageKey) {
    this.errorMessageKey = errorMessageKey;
  }

  public ErrorType getErrorType() {
    return errorType;
  }

  public void setErrorType(ErrorType errorType) {
    this.errorType = errorType;
  }
}
