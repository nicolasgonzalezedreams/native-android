package com.odigeo.data.entity.booking;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Traveller extends PersonBooking implements Serializable {

  public static final String TRAVELLER_TYPE_ADULT = "ADULT";
  public static final String TRAVELLER_TYPE_CHILD = "CHILD";
  public static final String TRAVELLER_TYPE_INFANT = "INFANT";

  private long mId;
  private String mCountryCode;
  private long mIdentificationExpirationDate;
  private String mIdentificationIssueCountryCode;
  private String mLocalityCode;
  private String mMeal;
  private String mNationality;
  private String mTitle;
  private String mTravellerGender;
  private String mTravellerType;
  private long mBookingId;

  //Relations
  private List<Baggage> mBaggageList;

  public Traveller() {
    //empty
  }

  public Traveller(long id) {
    mId = id;
  }

  public Traveller(long dateOfBirth, String identification, String identificationType,
      String lastname, String name, long id, String countryCode, long identificationExpirationDate,
      String identificationIssueCountryCode, String localityCode, String meal, String nationality,
      String title, String travellerGender, String travellerType, long bookingId) {

    super(dateOfBirth, identification, identificationType, lastname, name);
    mId = id;
    mCountryCode = countryCode;
    mIdentificationExpirationDate = identificationExpirationDate;
    mIdentificationIssueCountryCode = identificationIssueCountryCode;
    mLocalityCode = localityCode;
    mMeal = meal;
    mNationality = nationality;
    mTitle = title;
    mTravellerGender = travellerGender;
    mTravellerType = travellerType;
    mBookingId = bookingId;
  }

  public Traveller(long dateOfBirth, String identification, String identificationType,
      String lastname, String name, long id, String countryCode, long identificationExpirationDate,
      String identificationIssueCountryCode, String localityCode, String meal, String nationality,
      String title, String travellerGender, String travellerType, long bookingId,
      ArrayList<Baggage> baggagesList) {

    super(dateOfBirth, identification, identificationType, lastname, name);
    mId = id;
    mCountryCode = countryCode;
    mIdentificationExpirationDate = identificationExpirationDate;
    mIdentificationIssueCountryCode = identificationIssueCountryCode;
    mLocalityCode = localityCode;
    mMeal = meal;
    mNationality = nationality;
    mTitle = title;
    mTravellerGender = travellerGender;
    mTravellerType = travellerType;
    mBookingId = bookingId;
    mBaggageList = baggagesList;
  }

  public long getId() {
    return mId;
  }

  public void setId(long id) {
    mId = id;
  }

  public String getCountryCode() {
    return mCountryCode;
  }

  public long getIdentificationExpirationDate() {
    return mIdentificationExpirationDate;
  }

  public String getIdentificationIssueCountryCode() {
    return mIdentificationIssueCountryCode;
  }

  public String getLocalityCode() {
    return mLocalityCode;
  }

  public String getMeal() {
    return mMeal;
  }

  public String getNationality() {
    return mNationality;
  }

  public String getTitle() {
    return mTitle;
  }

  public String getTravellerGender() {
    return mTravellerGender;
  }

  public String getTravellerType() {
    return mTravellerType;
  }

  public long getBookingId() {
    return mBookingId;
  }

  public List<Baggage> getBaggageList() {
    return mBaggageList;
  }

  public void setBaggageList(List<Baggage> baggagesList) {
    mBaggageList = baggagesList;
  }
}
