package com.odigeo.data.entity.booking;

import java.io.Serializable;

public class BookingSummary implements Serializable {

  private Long id;
  private Status status;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public enum Status {
    OK, PENDING, KO
  }
}
