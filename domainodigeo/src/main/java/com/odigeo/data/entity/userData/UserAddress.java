package com.odigeo.data.entity.userData;

public class UserAddress {

  private long mId;
  private long mUserAddressId;
  private String mAddress;
  private String mAddressType;
  private String mCity;
  private String mCountry;
  private String mState;
  private String mPostalCode;
  private boolean mIsPrimary;
  private String mAlias;

  //Relations
  private long mUserProfileId;

  public UserAddress() {
    //empty
  }

  public UserAddress(long id, long userAddressId, String address, String addressType, String city,
      String country, String state, String postalCode, boolean isPrimary, String alias,
      long userProfileId) {
    mId = id;
    mUserAddressId = userAddressId;
    mAddress = address;
    mAddressType = addressType;
    mCity = city;
    mCountry = country;
    mState = state;
    mPostalCode = postalCode;
    mIsPrimary = isPrimary;
    mAlias = alias;
    mUserProfileId = userProfileId;
  }

  public long getId() {
    return mId;
  }

  public void setId(long id) {
    mId = id;
  }

  public long getUserAddressId() {
    return mUserAddressId;
  }

  public void setUserAddressId(long userAddressId) {
    mUserAddressId = userAddressId;
  }

  public String getAddress() {
    return mAddress;
  }

  public void setAddress(String address) {
    mAddress = address;
  }

  public String getAddressType() {
    return mAddressType;
  }

  public void setAddressType(String addressType) {
    mAddressType = addressType;
  }

  public String getCity() {
    return mCity;
  }

  public void setCity(String city) {
    mCity = city;
  }

  public String getCountry() {
    return mCountry;
  }

  public void setCountry(String country) {
    mCountry = country;
  }

  public String getState() {
    return mState;
  }

  public void setState(String state) {
    mState = state;
  }

  public String getPostalCode() {
    return mPostalCode;
  }

  public void setPostalCode(String postalCode) {
    mPostalCode = postalCode;
  }

  public boolean getIsPrimary() {
    return mIsPrimary;
  }

  public String getAlias() {
    return mAlias;
  }

  public void setAlias(String alias) {
    mAlias = alias;
  }

  public long getUserProfileId() {
    return mUserProfileId;
  }

  public void setUserProfileId(long userProfileId) {
    mUserProfileId = userProfileId;
  }

  public void setPrimary(boolean primary) {
    mIsPrimary = primary;
  }
}
