package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class TravellerInformationFieldConditionRule implements Serializable {

  private List<TravellerInformationFieldCondition> travellerInformationFieldConditions;
  private TravellerInformationField conditionallyRequiredTravellerInformationField;

  public List<TravellerInformationFieldCondition> getTravellerInformationFieldConditions() {
    return travellerInformationFieldConditions;
  }

  public void setTravellerInformationFieldConditions(
      List<TravellerInformationFieldCondition> travellerInformationFieldConditions) {
    this.travellerInformationFieldConditions = travellerInformationFieldConditions;
  }

  public TravellerInformationField getConditionallyRequiredTravellerInformationField() {
    return conditionallyRequiredTravellerInformationField;
  }

  public void setConditionallyRequiredTravellerInformationField(
      TravellerInformationField conditionallyRequiredTravellerInformationField) {
    this.conditionallyRequiredTravellerInformationField =
        conditionallyRequiredTravellerInformationField;
  }
}
