package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class BankTransferResponse implements Serializable {

  List<BankAccountResponse> bankAccountResponses;
  private String notificationFax;
  private String notificationEmail;

  public List<BankAccountResponse> getBankAccountResponses() {
    return bankAccountResponses;
  }

  public void setBankAccountResponses(List<BankAccountResponse> bankAccountResponses) {
    this.bankAccountResponses = bankAccountResponses;
  }

  public String getNotificationFax() {
    return notificationFax;
  }

  public void setNotificationFax(String notificationFax) {
    this.notificationFax = notificationFax;
  }

  public String getNotificationEmail() {
    return notificationEmail;
  }

  public void setNotificationEmail(String notificationEmail) {
    this.notificationEmail = notificationEmail;
  }
}