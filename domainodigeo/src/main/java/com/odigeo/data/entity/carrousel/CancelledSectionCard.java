package com.odigeo.data.entity.carrousel;

/**
 * Created by Jaime Toca on 25/1/16.
 */
public class CancelledSectionCard extends SectionCard {

  public CancelledSectionCard(long id, CardType type, int priority, String image, String carrier,
      String carrierName, String flightId, CarouselLocation from, CarouselLocation to) {
    super(id, type, priority, image, carrier, carrierName, flightId, from, to);
  }

  public CancelledSectionCard() {
  }
}
