package com.odigeo.data.entity.carrousel;

public class DivertedSectionCard extends SectionCard {

  public DivertedSectionCard(long id, CardType type, int priority, String image, String carrier,
      String carrierName, String flightId, CarouselLocation from, CarouselLocation to) {
    super(id, type, priority, image, carrier, carrierName, flightId, from, to);
  }

  public DivertedSectionCard() {
  }
}
