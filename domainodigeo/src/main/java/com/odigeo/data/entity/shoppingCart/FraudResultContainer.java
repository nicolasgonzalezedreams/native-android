package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class FraudResultContainer implements Serializable {

  private FraudRiskLevel fraudRiskLevel;
  private Boolean isWhiteListed;
  private Boolean isBlackListed;

  public FraudRiskLevel getFraudRiskLevel() {
    return fraudRiskLevel;
  }

  public void setFraudRiskLevel(FraudRiskLevel fraudRiskLevel) {
    this.fraudRiskLevel = fraudRiskLevel;
  }

  public Boolean getWhiteListed() {
    return isWhiteListed;
  }

  public void setWhiteListed(Boolean whiteListed) {
    isWhiteListed = whiteListed;
  }

  public Boolean getBlackListed() {
    return isBlackListed;
  }

  public void setBlackListed(Boolean blackListed) {
    isBlackListed = blackListed;
  }
}
