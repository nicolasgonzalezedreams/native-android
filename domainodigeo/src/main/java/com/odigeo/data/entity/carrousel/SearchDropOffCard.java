package com.odigeo.data.entity.carrousel;

import com.odigeo.data.entity.booking.StoredSearch;

/**
 * Created by daniel.morales on 6/3/17.
 */

public class SearchDropOffCard extends Card {

  private String bookNowUrl;
  private String editTripUrl;

  private String description;

  private String departureCityIATA;
  private String arrivalCityIATA;

  private long departureDate = 0;
  private long arrivalDate = 0;

  private int numAdults;
  private int numChildren;
  private int numInfants;

  private boolean isDirect;

  private StoredSearch.CabinClass cabinClass;
  private StoredSearch.TripType tripType;

  public String getBookNowUrl() {
    return bookNowUrl;
  }

  public void setBookNowUrl(String bookNowUrl) {
    this.bookNowUrl = bookNowUrl;
  }

  public String getEditTripUrl() {
    return editTripUrl;
  }

  public void setEditTripUrl(String editTripUrl) {
    this.editTripUrl = editTripUrl;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDepartureCityIATA() {
    return departureCityIATA;
  }

  public void setDepartureCityIATA(String departureCityIATA) {
    this.departureCityIATA = departureCityIATA;
  }

  public String getArrivalCityIATA() {
    return arrivalCityIATA;
  }

  public void setArrivalCityIATA(String arrivalCityIATA) {
    this.arrivalCityIATA = arrivalCityIATA;
  }

  public long getDepartureDate() {
    return departureDate;
  }

  public void setDepartureDate(long departureDate) {
    this.departureDate = departureDate;
  }

  public long getArrivalDate() {
    return arrivalDate;
  }

  public void setArrivalDate(long arrivalDate) {
    this.arrivalDate = arrivalDate;
  }

  public int getNumAdults() {
    return numAdults;
  }

  public void setNumAdults(int numAdults) {
    this.numAdults = numAdults;
  }

  public int getNumChildren() {
    return numChildren;
  }

  public void setNumChildren(int numChildren) {
    this.numChildren = numChildren;
  }

  public int getNumInfants() {
    return numInfants;
  }

  public void setNumInfants(int numInfants) {
    this.numInfants = numInfants;
  }

  public boolean isDirect() {
    return isDirect;
  }

  public void setDirect(boolean direct) {
    isDirect = direct;
  }

  public StoredSearch.CabinClass getCabinClass() {
    return cabinClass;
  }

  public void setCabinClass(StoredSearch.CabinClass cabinClass) {
    this.cabinClass = cabinClass;
  }

  public StoredSearch.TripType getTripType() {
    return tripType;
  }

  public void setTripType(StoredSearch.TripType tripType) {
    this.tripType = tripType;
  }
}
