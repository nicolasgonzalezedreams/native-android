package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class FeeInfo implements Serializable {

  private FeeDetails searchFee;
  private FeeDetails paymentFee;

  public FeeInfo() {

  }

  public FeeInfo(FeeDetails searchFee, FeeDetails paymentFee) {
    this.searchFee = searchFee;
    this.paymentFee = paymentFee;
  }

  public FeeDetails getSearchFee() {
    return searchFee;
  }

  public void setSearchFee(FeeDetails searchFee) {
    this.searchFee = searchFee;
  }

  public FeeDetails getPaymentFee() {
    return paymentFee;
  }

  public void setPaymentFee(FeeDetails paymentFee) {
    this.paymentFee = paymentFee;
  }
}
