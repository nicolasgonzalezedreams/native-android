package com.odigeo.data.entity.booking;

import java.io.Serializable;

/**
 * Created by ximenaperez1 on 1/11/16.
 */
public class FlightStats implements Serializable {

  public static final String STATUS_UNKNOWN = "UNKNOWN";
  public static final String STATUS_CANCELLED = "CANCELLED";
  public static final String STATUS_ARRIVED = "ARRIVED";
  public static final String STATUS_ACTIVE = "ACTIVE";
  public static final String STATUS_REDIRECTED = "REDIRECTED";
  public static final String STATUS_SCHEDULED = "SCHEDULED";
  public static final String STATUS_DIVERTED = "DIVERTED";
  public static final String STATUS_DELAYED = "DELAYED";
  public static final String STATUS_UPDATED = "UPDATED";

  private long mId;
  private String mArrivalGate;
  private long mArrivalTime;
  private String mArrivalTimeDelay;
  private String mArrivalTimeType;
  private String mBaggageClaim;
  private String mDepartureGate;
  private long mDepartureTime;
  private String mDepartureTimeDelay;
  private String mDepartureTimeType;
  private String mDestinationAirportName;
  private String mDestinationIataCode;
  private String mEventReceived;
  private String mFlightStatus;
  private String mGateChanged;
  private String mOperatingVendor;
  private String mOperatingVendorCode;
  private String mOperatingVendorLegNumber;
  private String mUpdatedArrivalTerminal;
  private String mUpdatedDepartureTerminal;

  // Relation with Section
  private String mSectionId;

  public FlightStats(long id, String arrivalGate, long arrivalTime, String arrivalTimeDelay,
      String arrivalTimeType, String baggageClaim, String departureGate, long departureTime,
      String departureTimeDelay, String departureTimeType, String destinationAirportName,
      String destinationIataCode, String eventReceived, String flightStatus, String gateChanged,
      String operatingVendor, String operatingVendorCode, String operatingVendorLegNumber,
      String updatedArrivalTerminal, String updatedDepartureTerminal, String sectionId) {
    mId = id;
    mArrivalGate = arrivalGate;
    mArrivalTime = arrivalTime;
    mArrivalTimeDelay = arrivalTimeDelay;
    mArrivalTimeType = arrivalTimeType;
    mBaggageClaim = baggageClaim;
    mDepartureGate = departureGate;
    mDepartureTime = departureTime;
    mDepartureTimeDelay = departureTimeDelay;
    mDepartureTimeType = departureTimeType;
    mDestinationAirportName = destinationAirportName;
    mDestinationIataCode = destinationIataCode;
    mEventReceived = eventReceived;
    mFlightStatus = flightStatus;
    mGateChanged = gateChanged;
    mOperatingVendor = operatingVendor;
    mOperatingVendorCode = operatingVendorCode;
    mOperatingVendorLegNumber = operatingVendorLegNumber;
    mUpdatedArrivalTerminal = updatedArrivalTerminal;
    mUpdatedDepartureTerminal = updatedDepartureTerminal;
    mSectionId = sectionId;
  }

  public FlightStats(String arrivalGate, long arrivalTime, String arrivalTimeDelay,
      String arrivalTimeType, String baggageClaim, String departureGate, long departureTime,
      String departureTimeDelay, String departureTimeType, String destinationAirportName,
      String destinationIataCode, String eventReceived, String flightStatus, String gateChanged,
      String operatingVendor, String operatingVendorCode, String operatingVendorLegNumber,
      String updatedArrivalTerminal, String updatedDepartureTerminal, String sectionId) {
    mArrivalGate = arrivalGate;
    mArrivalTime = arrivalTime;
    mArrivalTimeDelay = arrivalTimeDelay;
    mArrivalTimeType = arrivalTimeType;
    mBaggageClaim = baggageClaim;
    mDepartureGate = departureGate;
    mDepartureTime = departureTime;
    mDepartureTimeDelay = departureTimeDelay;
    mDepartureTimeType = departureTimeType;
    mDestinationAirportName = destinationAirportName;
    mDestinationIataCode = destinationIataCode;
    mEventReceived = eventReceived;
    mFlightStatus = flightStatus;
    mGateChanged = gateChanged;
    mOperatingVendor = operatingVendor;
    mOperatingVendorCode = operatingVendorCode;
    mOperatingVendorLegNumber = operatingVendorLegNumber;
    mUpdatedArrivalTerminal = updatedArrivalTerminal;
    mUpdatedDepartureTerminal = updatedDepartureTerminal;
    mSectionId = sectionId;
  }

  public long getId() {
    return mId;
  }

  public String getArrivalGate() {
    return mArrivalGate;
  }

  public long getArrivalTime() {
    return mArrivalTime;
  }

  public String getArrivalTimeDelay() {
    return mArrivalTimeDelay;
  }

  public String getArrivalTimeType() {
    return mArrivalTimeType;
  }

  public String getBaggageClaim() {
    return mBaggageClaim;
  }

  public void setBaggageClaim(String baggageClaim) {
    this.mBaggageClaim = baggageClaim;
  }

  public String getDepartureGate() {
    return mDepartureGate;
  }

  public long getDepartureTime() {
    return mDepartureTime;
  }

  public String getDepartureTimeDelay() {
    return mDepartureTimeDelay;
  }

  public String getDepartureTimeType() {
    return mDepartureTimeType;
  }

  public String getDestinationAirportName() {
    return mDestinationAirportName;
  }

  public String getDestinationIataCode() {
    return mDestinationIataCode;
  }

  public String getEventReceived() {
    return mEventReceived;
  }

  public String getFlightStatus() {
    return mFlightStatus;
  }

  public void setFlightStatus(String flightStatus) {
    mFlightStatus = flightStatus;
  }

  public String getGateChanged() {
    return mGateChanged;
  }

  public String getOperatingVendor() {
    return mOperatingVendor;
  }

  public String getOperatingVendorCode() {
    return mOperatingVendorCode;
  }

  public String getOperatingVendorLegNumber() {
    return mOperatingVendorLegNumber;
  }

  public String getUpdatedArrivalTerminal() {
    return mUpdatedArrivalTerminal;
  }

  public String getUpdatedDepartureTerminal() {
    return mUpdatedDepartureTerminal;
  }

  public String getSectionId() {
    return mSectionId;
  }
}