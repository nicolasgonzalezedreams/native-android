package com.odigeo.data.entity.booking;

import java.io.Serializable;

public class Forecast implements Serializable {

  private long mId;
  private long mForecastId;
  private long mForecastDate;
  private String mForecastDescription;
  private String mForecastIcon;
  private long mForecastUpdatedDate;
  private long mTemperature;

  //Relations
  private long mLocationId;

  public Forecast() {
    //empty
  }

  public Forecast(long id, long forecastId, long forecastDate, String forecastDescription,
      String forecastIcon, long forecastUpdatedDate, long temperature) {
    this.mId = id;
    this.mForecastId = forecastId;
    this.mForecastDate = forecastDate;
    this.mForecastDescription = forecastDescription;
    this.mForecastIcon = forecastIcon;
    this.mForecastUpdatedDate = forecastUpdatedDate;
    this.mTemperature = temperature;
  }

  public Forecast(long id, long forecastId, long forecastDate, String forecastDescription,
      String forecastIcon, long forecastUpdatedDate, long temperature, long locationId) {
    this.mId = id;
    this.mForecastId = forecastId;
    this.mForecastDate = forecastDate;
    this.mForecastDescription = forecastDescription;
    this.mForecastIcon = forecastIcon;
    this.mForecastUpdatedDate = forecastUpdatedDate;
    this.mTemperature = temperature;
    this.mLocationId = locationId;
  }

  public long getId() {
    return mId;
  }

  public long getForecastId() {
    return mForecastId;
  }

  public long getForecastDate() {
    return mForecastDate;
  }

  public String getForecastDescription() {
    return mForecastDescription;
  }

  public String getForecastIcon() {
    return mForecastIcon;
  }

  public long getForecastUpdatedDate() {
    return mForecastUpdatedDate;
  }

  public long getTemperature() {
    return mTemperature;
  }

  public long getLocationId() {
    return mLocationId;
  }
}
