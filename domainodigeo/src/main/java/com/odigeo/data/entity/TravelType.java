package com.odigeo.data.entity;

import java.io.Serializable;

/**
 * Created by Irving Lóp on 11/09/2014.
 */
public enum TravelType implements Serializable {
  SIMPLE, ROUND, MULTIDESTINATION
}
