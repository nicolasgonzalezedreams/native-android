package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum Code implements Serializable {

  // Define application errors
  GENERAL_ERROR(1000), PROVIDER_DOWN(1002), // Define data received errors
  INVALID_LOCATION(2000), INVALID_USER_DATA(2001), SEARCH_TIMEOUT(2002), SESSION_TIMEOUT(
      2002), NO_RESULTS(2003), INVALID_DEVICE_ID(2004), INVALID_DAPI_CONTEXT(2005), BROKEN_FLOW(
      2006), STOP_FLOW(2007), VERSION_ERROR(2008), VERSION_TO_BE_DEPRECATED(2009), INVALID_REQUEST(
      2010), // Retry errors
  SELECTION_ITINERARY_PRODUCT_ERROR(3006), // Define informative codes
  NO_ERROR(0), // Users codes
  VALIDATION_ERROR(4001), INVALID_CREDENTIALS(4002), USER_STATUS_ERROR(4003), // Mail codes
  INVALID_MAIL_PARAMETERS(5001), ERROR_SENDING_EMAIL(5002), FAIL_MAILER_RESPONSE(5003);

  private final Integer errorCode;

  Code(final Integer errorCode) {
    this.errorCode = errorCode;
  }

  /**
   * @return
   */
  public Integer getErrorCode() {
    return errorCode;
  }

}
