package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum ResidentValidationType implements Serializable {

  OK, PENDING, KO, DATA_ERROR, AVAILABILITY, NOT_RESPONSE;

  public static ResidentValidationType fromValue(String v) {
    return valueOf(v);
  }

  public String value() {
    return name();
  }
}
