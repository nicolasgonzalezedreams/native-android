package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;

public class PassengerTypeFare implements Serializable {

  private BigDecimal fare;
  private BigDecimal tax;

  public PassengerTypeFare() {
    fare = BigDecimal.ZERO;
    tax = BigDecimal.ZERO;
  }

  public BigDecimal getFare() {
    return fare;
  }

  public void setFare(BigDecimal fare) {
    this.fare = fare;
  }

  public BigDecimal getTax() {
    return tax;
  }

  public void setTax(BigDecimal tax) {
    this.tax = tax;
  }
}
