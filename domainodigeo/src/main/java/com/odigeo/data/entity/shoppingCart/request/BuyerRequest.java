package com.odigeo.data.entity.shoppingCart.request;

public class BuyerRequest {

  private String name;
  private String lastNames;
  private String mail;
  private String buyerIdentificationTypeName;
  private String identification;
  private int dayOfBirth;
  private int monthOfBirth;
  private int yearOfBirth;
  private String cpf;
  private String address;
  private String cityName;
  private String stateName;
  private String zipCode;
  private String phoneNumber1;
  private String phoneNumber2;
  private String countryPhoneNumber1;
  private String countryPhoneNumber2;
  private String countryCode;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLastNames() {
    return lastNames;
  }

  public void setLastNames(String lastNames) {
    this.lastNames = lastNames;
  }

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public String getBuyerIdentificationTypeName() {
    return buyerIdentificationTypeName;
  }

  public void setBuyerIdentificationTypeName(String buyerIdentificationTypeName) {
    this.buyerIdentificationTypeName = buyerIdentificationTypeName;
  }

  public String getIdentification() {
    return identification;
  }

  public void setIdentification(String identification) {
    this.identification = identification;
  }

  public int getDayOfBirth() {
    return dayOfBirth;
  }

  public void setDayOfBirth(int dayOfBirth) {
    this.dayOfBirth = dayOfBirth;
  }

  public int getMonthOfBirth() {
    return monthOfBirth;
  }

  public void setMonthOfBirth(int monthOfBirth) {
    this.monthOfBirth = monthOfBirth;
  }

  public int getYearOfBirth() {
    return yearOfBirth;
  }

  public void setYearOfBirth(int yearOfBirth) {
    this.yearOfBirth = yearOfBirth;
  }

  public String getCpf() {
    return cpf;
  }

  public void setCpf(String cpf) {
    this.cpf = cpf;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public String getStateName() {
    return stateName;
  }

  public void setStateName(String stateName) {
    this.stateName = stateName;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getPhoneNumber1() {
    return phoneNumber1;
  }

  public void setPhoneNumber1(String phoneNumber1) {
    this.phoneNumber1 = phoneNumber1;
  }

  public String getPhoneNumber2() {
    return phoneNumber2;
  }

  public void setPhoneNumber2(String phoneNumber2) {
    this.phoneNumber2 = phoneNumber2;
  }

  public String getCountryPhoneNumber1() {
    return countryPhoneNumber1;
  }

  public void setCountryPhoneNumber1(String countryPhoneNumber1) {
    this.countryPhoneNumber1 = countryPhoneNumber1;
  }

  public String getCountryPhoneNumber2() {
    return countryPhoneNumber2;
  }

  public void setCountryPhoneNumber2(String countryPhoneNumber2) {
    this.countryPhoneNumber2 = countryPhoneNumber2;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }
}
