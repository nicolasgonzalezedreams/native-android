package com.odigeo.data.entity.booking;

import java.io.Serializable;
import java.util.ArrayList;

public class ItineraryBooking implements Serializable {

  private long mId;
  private String mBookingStatus;
  private String mPnr;
  private long mTimestamp;

  //Realations
  private ArrayList<Section> mSections;

  public ItineraryBooking() {
    //empty
  }

  public ItineraryBooking(long id) {
    mId = id;
  }

  public ItineraryBooking(long id, String bookingStatus, String pnr, long timestamp) {
    mId = id;
    mBookingStatus = bookingStatus;
    mPnr = pnr;
    mTimestamp = timestamp;
  }

  public ItineraryBooking(long id, String bookingStatus, String pnr, long timestamp,
      ArrayList<Section> sections) {
    mId = id;
    mBookingStatus = bookingStatus;
    mPnr = pnr;
    mTimestamp = timestamp;
    mSections = sections;
  }

  public long getId() {
    return mId;
  }

  public void setId(long id) {
    mId = id;
  }

  public String getBookingStatus() {
    return mBookingStatus;
  }

  public String getPnr() {
    return mPnr;
  }

  public long getTimestamp() {
    return mTimestamp;
  }

  //****************
  //    SETTERS
  //****************

  public ArrayList<Section> getSections() {
    if (mSections == null) {
      mSections = new ArrayList<>();
    }
    return mSections;
  }

  public void setSections(ArrayList<Section> sections) {
    mSections = sections;
  }

  public void addSection(Section section) {
    getSections().add(section);
  }
}
