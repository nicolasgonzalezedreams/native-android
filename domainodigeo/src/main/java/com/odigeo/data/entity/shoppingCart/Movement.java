package com.odigeo.data.entity.shoppingCart;

import java.math.BigDecimal;

public class Movement {

  private String orderId;
  private String code;
  private MovementErrorType errorType;
  private String errorDescription;
  private MovementAction action;
  private MovementStatus status;
  private BigDecimal amount;
  private String creationDate;
  private String commerceId;
  private String terminalId;
  private String transactionId;
  private String transactionSubId;
  private String commerceStatus;
  private String commerceStatusSubcode;
  private String accountNumber;
  private String bankName;
  private String bankCity;
  private String bankCountry;
  private String paymentReference;
  private String swiftCode;
  private String iban;
  private String commerceType;
  private String accountHolder;

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public MovementErrorType getErrorType() {
    return errorType;
  }

  public void setErrorType(MovementErrorType errorType) {
    this.errorType = errorType;
  }

  public String getErrorDescription() {
    return errorDescription;
  }

  public void setErrorDescription(String errorDescription) {
    this.errorDescription = errorDescription;
  }

  public MovementAction getAction() {
    return action;
  }

  public void setAction(MovementAction action) {
    this.action = action;
  }

  public MovementStatus getStatus() {
    return status;
  }

  public void setStatus(MovementStatus status) {
    this.status = status;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(String creationDate) {
    this.creationDate = creationDate;
  }

  public String getCommerceId() {
    return commerceId;
  }

  public void setCommerceId(String commerceId) {
    this.commerceId = commerceId;
  }

  public String getTerminalId() {
    return terminalId;
  }

  public void setTerminalId(String terminalId) {
    this.terminalId = terminalId;
  }

  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public String getTransactionSubId() {
    return transactionSubId;
  }

  public void setTransactionSubId(String transactionSubId) {
    this.transactionSubId = transactionSubId;
  }

  public String getCommerceStatus() {
    return commerceStatus;
  }

  public void setCommerceStatus(String commerceStatus) {
    this.commerceStatus = commerceStatus;
  }

  public String getCommerceStatusSubcode() {
    return commerceStatusSubcode;
  }

  public void setCommerceStatusSubcode(String commerceStatusSubcode) {
    this.commerceStatusSubcode = commerceStatusSubcode;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public String getBankName() {
    return bankName;
  }

  public void setBankName(String bankName) {
    this.bankName = bankName;
  }

  public String getBankCity() {
    return bankCity;
  }

  public void setBankCity(String bankCity) {
    this.bankCity = bankCity;
  }

  public String getBankCountry() {
    return bankCountry;
  }

  public void setBankCountry(String bankCountry) {
    this.bankCountry = bankCountry;
  }

  public String getPaymentReference() {
    return paymentReference;
  }

  public void setPaymentReference(String paymentReference) {
    this.paymentReference = paymentReference;
  }

  public String getSwiftCode() {
    return swiftCode;
  }

  public void setSwiftCode(String swiftCode) {
    this.swiftCode = swiftCode;
  }

  public String getIban() {
    return iban;
  }

  public void setIban(String iban) {
    this.iban = iban;
  }

  public String getCommerceType() {
    return commerceType;
  }

  public void setCommerceType(String commerceType) {
    this.commerceType = commerceType;
  }

  public String getAccountHolder() {
    return accountHolder;
  }

  public void setAccountHolder(String accountHolder) {
    this.accountHolder = accountHolder;
  }
}