package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class AmbienceSeatPreferencesSelection implements Serializable {

  private List<Integer> sections;
  private String ambiencePreferenceCode;
  private BigDecimal providerPrice;
  private BigDecimal totalPrice;

  public List<Integer> getSections() {
    return sections;
  }

  public void setSections(List<Integer> sections) {
    this.sections = sections;
  }

  public String getAmbiencePreferenceCode() {
    return ambiencePreferenceCode;
  }

  public void setAmbiencePreferenceCode(String ambiencePreferenceCode) {
    this.ambiencePreferenceCode = ambiencePreferenceCode;
  }

  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  public void setProviderPrice(BigDecimal providerPrice) {
    this.providerPrice = providerPrice;
  }

  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }
}
