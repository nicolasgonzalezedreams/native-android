package com.odigeo.data.entity.booking;

public class Guide {

  private long mId;
  private String mUrl;
  private String mLanguage;

  // Relation with LocationBooking
  private long mGeoNodeId;

  public Guide() {
    //empty
  }

  public Guide(long id) {
    mId = id;
  }

  public Guide(long id, long geoNodeId, String url, String language) {
    mId = id;
    mGeoNodeId = geoNodeId;
    mUrl = url;
    mLanguage = language;
  }

  public long getId() {
    return mId;
  }

  public long getGeoNodeId() {
    return mGeoNodeId;
  }

  public String getUrl() {
    return mUrl;
  }

  public String getLanguage() {
    return mLanguage;
  }
}
