package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;

public class AdditionalProduct implements Serializable {

  protected String reference;
  protected BigDecimal providerPrice;
  protected String providerCurrency;
  protected BigDecimal price;
  protected String description;
  protected AdditionalProductType additionalProductType;

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  public void setProviderPrice(BigDecimal providerPrice) {
    this.providerPrice = providerPrice;
  }

  public String getProviderCurrency() {
    return providerCurrency;
  }

  public void setProviderCurrency(String providerCurrency) {
    this.providerCurrency = providerCurrency;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public AdditionalProductType getAdditionalProductType() {
    return additionalProductType;
  }

  public void setAdditionalProductType(AdditionalProductType additionalProductType) {
    this.additionalProductType = additionalProductType;
  }
}
