package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class MslError implements Serializable {
  private String message;
  private Integer code;
  private Code codeString;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer code) {
    this.code = code;
  }

  public Code getCodeString() {
    return codeString;
  }

  public void setCodeString(Code codeString) {
    this.codeString = codeString;
  }
}
