package com.odigeo.data.entity.userData;

import java.io.Serializable;

public class CreditCard implements Serializable {

  private String creditCardNumber;
  private String expiryMonth;
  private String expiryYear;
  private Long id;
  private String owner;
  private long creationDate;
  private long modifiedDate;
  private long lastUsageDate;
  private Boolean isDefault;
  private String creditCardTypeId;

  public CreditCard(String creditCardNumber, String expiryMonth, String expiryYear, Long id,
      String owner, long creationDate, long modifiedDate, long lastUsageDate, Boolean isDefault,
      String creditCardTypeId) {
    this.creditCardNumber = creditCardNumber;
    this.expiryMonth = expiryMonth;
    this.expiryYear = expiryYear;
    this.id = id;
    this.owner = owner;
    this.creationDate = creationDate;
    this.modifiedDate = modifiedDate;
    this.lastUsageDate = lastUsageDate;
    this.isDefault = isDefault;
    this.creditCardTypeId = creditCardTypeId;
  }

  public String getCreditCardNumber() {
    return creditCardNumber;
  }

  public void setCreditCardNumber(String value) {
    this.creditCardNumber = value;
  }

  public String getExpiryMonth() {
    return expiryMonth;
  }

  public void setExpiryMonth(String value) {
    this.expiryMonth = value;
  }

  public String getExpiryYear() {
    return expiryYear;
  }

  public void setExpiryYear(String value) {
    this.expiryYear = value;
  }

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public String getOwner() {
    return owner;
  }

  public void setOwner(final String owner) {
    this.owner = owner;
  }

  public long getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(long creationDate) {
    this.creationDate = creationDate;
  }

  public long getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(long modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public long getLastUsageDate() {
    return lastUsageDate;
  }

  public void setLastUsageDate(long lastUsageDate) {
    this.lastUsageDate = lastUsageDate;
  }

  public Boolean getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(final Boolean isDefault) {
    this.isDefault = isDefault;
  }

  public String getCreditCardTypeId() {
    return creditCardTypeId;
  }

  public void setCreditCardTypeId(final String creditCardTypeId) {
    this.creditCardTypeId = creditCardTypeId;
  }
}
