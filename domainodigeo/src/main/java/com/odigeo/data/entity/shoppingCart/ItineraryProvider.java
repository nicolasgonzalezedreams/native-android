package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class ItineraryProvider implements Serializable {

  private PassengerTypeFare adultFares;
  private PassengerTypeFare childFares;
  private PassengerTypeFare infantFares;
  private int id;
  private String provider;
  private List<Integer> sections;
  private BigDecimal price;
  private BigDecimal providerPrice;
  private String providerCurrency;
  private String flightType;
  private List<String> corporateCodes;
  private Long providerItineraryId;

  public PassengerTypeFare getAdultFares() {
    return adultFares;
  }

  public void setAdultFares(PassengerTypeFare adultFares) {
    this.adultFares = adultFares;
  }

  public PassengerTypeFare getChildFares() {
    return childFares;
  }

  public void setChildFares(PassengerTypeFare childFares) {
    this.childFares = childFares;
  }

  public PassengerTypeFare getInfantFares() {
    return infantFares;
  }

  public void setInfantFares(PassengerTypeFare infantFares) {
    this.infantFares = infantFares;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getProvider() {
    return provider;
  }

  public void setProvider(String provider) {
    this.provider = provider;
  }

  public List<Integer> getSections() {
    return sections;
  }

  public void setSections(List<Integer> sections) {
    this.sections = sections;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getProviderPrice() {
    return providerPrice;
  }

  public void setProviderPrice(BigDecimal providerPrice) {
    this.providerPrice = providerPrice;
  }

  public String getProviderCurrency() {
    return providerCurrency;
  }

  public void setProviderCurrency(String providerCurrency) {
    this.providerCurrency = providerCurrency;
  }

  public String getFlightType() {
    return flightType;
  }

  public void setFlightType(String flightType) {
    this.flightType = flightType;
  }

  public List<String> getCorporateCodes() {
    return corporateCodes;
  }

  public void setCorporateCodes(List<String> corporateCodes) {
    this.corporateCodes = corporateCodes;
  }

  public Long getProviderItineraryId() {
    return providerItineraryId;
  }

  public void setProviderItineraryId(Long providerItineraryId) {
    this.providerItineraryId = providerItineraryId;
  }
}
