package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class BaggageEstimationFees implements Serializable {

  private BaggageConditions baggageConditions;
  private int id;

  public BaggageConditions getBaggageConditions() {
    return baggageConditions;
  }

  public void setBaggageConditions(BaggageConditions baggageConditions) {
    this.baggageConditions = baggageConditions;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}
