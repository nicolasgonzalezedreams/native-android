package com.odigeo.data.entity.booking;

import java.io.Serializable;

public class SearchSegment implements Serializable {

  private long mId;
  private long mSearchSegmentId;
  private String mOriginIATACode;
  private String mDestinationIATACode;
  private long mDepartureDate;
  private long mSegmentOrder;

  //Relations
  private long mParentSearchId;

  public SearchSegment() {
    //empty
  }

  public SearchSegment(long id, long searchSegmentId, String originIATACode,
      String destinationIATACode, long departureDate, long segmentOrder) {
    mId = id;
    mSearchSegmentId = searchSegmentId;
    mOriginIATACode = originIATACode;
    mDestinationIATACode = destinationIATACode;
    mDepartureDate = departureDate;
    mSegmentOrder = segmentOrder;
  }

  public SearchSegment(long id, long searchSegmentId, String originIATACode,
      String destinationIATACode, long departureDate, long segmentOrder, long parentSearchId) {
    mId = id;
    mSearchSegmentId = searchSegmentId;
    mOriginIATACode = originIATACode;
    mDestinationIATACode = destinationIATACode;
    mDepartureDate = departureDate;
    mSegmentOrder = segmentOrder;
    mParentSearchId = parentSearchId;
  }

  public long getId() {
    return mId;
  }

  public long getSearchSegmentId() {
    return mSearchSegmentId;
  }

  public String getOriginIATACode() {
    return mOriginIATACode;
  }

  public String getDestinationIATACode() {
    return mDestinationIATACode;
  }

  public long getDepartureDate() {
    return mDepartureDate;
  }

  public long getSegmentOrder() {
    return mSegmentOrder;
  }

  public long getParentSearchId() {
    return mParentSearchId;
  }

  public void setParentSearchId(long parentSearchId) {
    this.mParentSearchId = parentSearchId;
  }

  @Override public boolean equals(Object obj) {
    if (obj instanceof SearchSegment) {
      SearchSegment otherSearchSegment = (SearchSegment) obj;
      return otherSearchSegment.getSearchSegmentId() == this.getSearchSegmentId();
    }
    return false;
  }
}
