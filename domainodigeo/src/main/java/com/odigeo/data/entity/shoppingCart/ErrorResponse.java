package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class ErrorResponse implements Serializable {

  private MslError error;

  public MslError getError() {
    return error;
  }

  public void setError(MslError error) {
    this.error = error;
  }
}
