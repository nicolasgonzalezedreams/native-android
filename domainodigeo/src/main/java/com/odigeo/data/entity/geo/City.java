package com.odigeo.data.entity.geo;

import java.io.Serializable;
import java.util.List;

public class City implements Serializable {

  protected String name;
  protected LocationDescriptionType type;
  protected int geoNodeId;
  protected String iataCode;
  protected String cityName;
  protected String countryCode;
  protected String countryName;
  protected List<String> locationNames;
  protected List<City> relatedLocations;
  protected Coordinates coordinates;
  protected float distanceToCurrentLocation;
  protected boolean isGroupParent;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public LocationDescriptionType getType() {
    return type;
  }

  public void setType(LocationDescriptionType type) {
    this.type = type;
  }

  public int getGeoNodeId() {
    return geoNodeId;
  }

  public void setGeoNodeId(int geoNodeId) {
    this.geoNodeId = geoNodeId;
  }

  public String getIataCode() {
    return iataCode;
  }

  public void setIataCode(String iataCode) {
    this.iataCode = iataCode;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getCountryName() {
    return countryName;
  }

  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }

  public List<String> getLocationNames() {
    return locationNames;
  }

  public void setLocationNames(List<String> locationNames) {
    this.locationNames = locationNames;
  }

  public List<City> getRelatedLocations() {
    return relatedLocations;
  }

  public void setRelatedLocations(List<City> relatedLocations) {
    this.relatedLocations = relatedLocations;
  }

  public Coordinates getCoordinates() {
    return coordinates;
  }

  public void setCoordinates(Coordinates coordinates) {
    this.coordinates = coordinates;
  }

  public float getDistanceToCurrentLocation() {
    return distanceToCurrentLocation;
  }

  public void setDistanceToCurrentLocation(float distanceToCurrentLocation) {
    this.distanceToCurrentLocation = distanceToCurrentLocation;
  }

  public boolean isGroupParent() {
    return isGroupParent;
  }

  public void setGroupParent(boolean groupParent) {
    isGroupParent = groupParent;
  }

  @Override public int hashCode() {
    int result = geoNodeId;
    result = 31 * result + iataCode.hashCode();
    return result;
  }

  @Override public final boolean equals(Object city) {
    return (city instanceof City) && (iataCode.equals(((City) city).iataCode));
  }
}
