package com.odigeo.data.entity.localizables;

import com.odigeo.data.entity.shoppingCart.BaseResponse;
import java.io.Serializable;
import java.util.List;

public class LocalizablesResponse extends BaseResponse implements Serializable {

  private List<OdigeoLocalizable> localizables;

  public List<OdigeoLocalizable> getLocalizables() {
    return localizables;
  }

  public void setLocalizables(List<OdigeoLocalizable> localizables) {
    this.localizables = localizables;
  }
}
