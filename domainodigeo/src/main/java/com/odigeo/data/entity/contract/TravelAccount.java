package com.odigeo.data.entity.contract;

public class TravelAccount {

  private String cardNumber;
  private String cardType;
  private String cardExpirationMonth;
  private String cardExpirationYear;

  public String getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
  }

  public String getCardType() {
    return cardType;
  }

  public void setCardType(String cardType) {
    this.cardType = cardType;
  }

  public String getCardExpirationMonth() {
    return cardExpirationMonth;
  }

  public void setCardExpirationMonth(String cardExpirationMonth) {
    this.cardExpirationMonth = cardExpirationMonth;
  }

  public String getCardExpirationYear() {
    return cardExpirationYear;
  }

  public void setCardExpirationYear(String cardExpirationYear) {
    this.cardExpirationYear = cardExpirationYear;
  }
}