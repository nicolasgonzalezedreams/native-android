package com.odigeo.data.entity.userData;

import com.odigeo.data.entity.booking.StoredSearch;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class User implements Serializable {

  public static final String DEFAULT = "default";
  private long mId;
  private long mUserId;
  private String mEmail;
  private String mWebSite;
  private Boolean mAcceptsNewsletter;
  private Boolean mAcceptsPartnersInfo;
  private long mCreationDate;
  private long mLastModified;
  private String mLocale;
  private String mMarketingPortal;
  private Source mSource;
  private Status mStatus;
  //Relations
  private List<UserAirlinePreferences> mUserAirlinePreferencesList;
  private List<UserAirportPreferences> mUserAirportPreferencesList;
  private List<UserDestinationPreferences> mUserDestinationPreferencesList;
  private List<UserDevice> mUserDevicesList;
  private UserLogin mUserLogin;
  private List<UserTraveller> mUserTravellerList;
  private List<Long> mBookingsId;
  private List<StoredSearch> mUserSearches;
  private List<CreditCard> creditCards;
  private List<Membership> memberships;

  public User() {
    //empty
  }

  public User(String email, String password, Source source, Boolean acceptsNewsletter) {
    mEmail = email;
    mUserLogin = new UserLogin();
    mUserLogin.setPassword(password);
    mSource = source;
    mAcceptsNewsletter = acceptsNewsletter;
    mAcceptsPartnersInfo = acceptsNewsletter;
    mBookingsId = new ArrayList<>();
  }

  public User(long id, long userId, String email, String webSite, Boolean acceptsNewsletter,
      Boolean acceptsPartnersInfo, long creationDate, long lastModified, String locale,
      String marketingPortal, Source source, Status status) {
    mId = id;
    mUserId = userId;
    mEmail = email;
    mWebSite = webSite;
    mAcceptsNewsletter = acceptsNewsletter;
    mAcceptsPartnersInfo = acceptsPartnersInfo;
    mCreationDate = creationDate;
    mLastModified = lastModified;
    mLocale = locale;
    mMarketingPortal = marketingPortal;
    mSource = source;
    mStatus = status;
    mBookingsId = new ArrayList<>();
  }

  public User(long id, long userId, String email, String webSite, Boolean acceptsNewsletter,
      Boolean acceptsPartnersInfo, long creationDate, long lastModified, String locale,
      String marketingPortal, Source source, Status status,
      List<UserAirlinePreferences> userAirlinePreferencesList,
      List<UserAirportPreferences> userAirportPreferencesList,
      List<UserDestinationPreferences> userDestinationPreferencesList,
      List<UserDevice> userDevicesList, UserLogin userLogin, List<UserTraveller> userTravellerList,
      List<StoredSearch> userSearches, List<CreditCard> creditCards, List<Membership> memberships) {
    mId = id;
    mUserId = userId;
    mEmail = email;
    mWebSite = webSite;
    mAcceptsNewsletter = acceptsNewsletter;
    mAcceptsPartnersInfo = acceptsPartnersInfo;
    mCreationDate = creationDate;
    mLastModified = lastModified;
    mLocale = locale;
    mMarketingPortal = marketingPortal;
    mSource = source;
    mStatus = status;
    mUserAirlinePreferencesList = userAirlinePreferencesList;
    mUserAirportPreferencesList = userAirportPreferencesList;
    mUserDestinationPreferencesList = userDestinationPreferencesList;
    mUserDevicesList = userDevicesList;
    mUserLogin = userLogin;
    mUserTravellerList = userTravellerList;
    mBookingsId = new ArrayList<>();
    mUserSearches = userSearches;
    this.creditCards = creditCards;
    this.memberships = memberships;
  }

  public List<Long> getBookingsId() {
    return mBookingsId;
  }

  public void setBookingsId(List<Long> bookingsId) {
    this.mBookingsId = bookingsId;
  }

  public long getId() {
    return mId;
  }

  public long getUserId() {
    return mUserId;
  }

  public String getEmail() {
    return mEmail;
  }

  public void setEmail(String email) {
    mEmail = email;
  }

  public String getWebSite() {
    return mWebSite;
  }

  public Boolean getAcceptsNewsletter() {
    return mAcceptsNewsletter;
  }

  public Boolean getAcceptsPartnersInfo() {
    return mAcceptsPartnersInfo;
  }

  public long getCreationDate() {
    return mCreationDate;
  }

  public long getLastModified() {
    return mLastModified;
  }

  public void setLasModified(long lastModified) {
    mLastModified = lastModified;
  }

  public String getLocale() {
    return mLocale;
  }

  public String getMarketingPortal() {
    return mMarketingPortal;
  }

  public Source getSource() {
    return mSource;
  }

  public Status getStatus() {
    return mStatus;
  }

  public String getPassword() {
    return mUserLogin.getPassword();
  }

  public boolean isAcceptsNewsletter() {
    return mAcceptsNewsletter;
  }

  public boolean isAcceptsPartnersInfo() {
    return mAcceptsPartnersInfo;
  }

  public List<UserAirlinePreferences> getUserAirlinePreferencesList() {
    return mUserAirlinePreferencesList;
  }

  public List<UserAirportPreferences> getUserAirportPreferencesList() {
    return mUserAirportPreferencesList;
  }

  public List<UserDestinationPreferences> getUserDestinationPreferencesList() {
    return mUserDestinationPreferencesList;
  }

  public List<UserDevice> getUserDevicesList() {
    return mUserDevicesList;
  }

  public UserLogin getUserLogin() {
    return mUserLogin;
  }

  public List<UserTraveller> getUserTravellerList() {
    return mUserTravellerList;
  }

  public void setUserTravellerList(List<UserTraveller> travellers) {
    mUserTravellerList = travellers;
  }

  public List<StoredSearch> getUserSearchesList() {
    return mUserSearches;
  }

  public void setUserSearchesList(List<StoredSearch> searches) {
    mUserSearches = searches;
  }

  public List<CreditCard> getCreditCards() {
    return creditCards;
  }

  public void setCreditCards(List<CreditCard> creditCards) {
    this.creditCards = creditCards;
  }

  public List<Membership> getMemberships() {
    return memberships;
  }

  public void setMemberships(List<Membership> memberships) {
    this.memberships = memberships;
  }

  public String getJsonParam() {
    String jsonParam;
    switch (mSource) {
      case FACEBOOK:
        jsonParam =
            "{\"type\":\"facebook\",\"accessToken\":\"" + mUserLogin.getAccessToken() + "\"}";
        break;
      case GOOGLE:
        jsonParam = "{\"type\":\"google\",\"accessToken\":\"" + mUserLogin.getAccessToken() + "\"}";
        break;
      default:
        jsonParam = "{\"type\":\"password\",\"login\":\""
            + getEmail()
            + "\",\"password\":\""
            + mUserLogin.getPassword()
            + "\"}";
        break;
    }
    return jsonParam;
  }

  @Override public String toString() {
    return "User{"
        + "mId="
        + mId
        + ", mEmail='"
        + mEmail
        + '\''
        + ", mWebSite='"
        + mWebSite
        + '\''
        + ", mAcceptsNewsletter="
        + mAcceptsNewsletter
        + ", mAcceptsPartnersInfo="
        + mAcceptsPartnersInfo
        + ", mCreationDate="
        + mCreationDate
        + ", mLastModified="
        + mLastModified
        + ", mLocale='"
        + mLocale
        + '\''
        + ", mMarketingPortal='"
        + mMarketingPortal
        + '\''
        + ", mSource="
        + mSource
        + ", mStatus="
        + mStatus
        + '}';
  }

  public enum Source {
    REGISTERED, BUYER, FACEBOOK, GOOGLE, LEGACY, OTHER, UNKNOWN
  }

  public enum Brand {
    GV, ED, OP, TL, UNKNOWN
  }

  public enum Status {
    ACTIVE, PENDING_MAIL_CONFIRMATION, DELETED, BLOCKED, UNREGISTERED, LEGACY, UNKNOWN
  }

  public enum TrafficInterface {
    UNKNOWN, WEB, WEB_MOBILE, XML_SEARCH, OTHERS, OFFLINE, META_API, ONE_FRONT_TABLET, NATIVE_IOS_EDREAMS, NATIVE_ANDROID, B2B_API, NATIVE_IOS_OPODO, NATIVE_IOS_GOV, OPODO_DESKTOP, OPODO_MOBILE, GOV_DESKTOP, ONE_FRONT_DESKTOP, ONE_FRONT_SMARTPHONE, OPODO_METAS, GOV_METAS, TRAVELLINK_CORPORATE, TRAVELLINK_LEISURE, NATIVE_ANDROID_GOV, NATIVE_ANDROID_OPODO, NATIVE_ANDROID_EDREAMS, NATIVE_IPAD_EDREAMS, NATIVE_IPAD_OPODO, NATIVE_IPAD_GOV
  }
}
