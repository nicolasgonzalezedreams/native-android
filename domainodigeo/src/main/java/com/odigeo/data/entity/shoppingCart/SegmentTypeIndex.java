package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public enum SegmentTypeIndex implements Serializable {
  FIRST(0), SECOND(1), THIRD(2), FOURTH(3), FIFTH(4), SIXTH(5);

  int index;

  SegmentTypeIndex(int index) {
    this.index = index;
  }

  public static SegmentTypeIndex fromValue(String v) {
    return valueOf(v);
  }

  public static SegmentTypeIndex fromIndex(int index) {
    SegmentTypeIndex segmentTypeIndex;
    switch (index) {
      case 0:
        segmentTypeIndex = FIRST;
        break;
      case 1:
        segmentTypeIndex = SECOND;
        break;
      case 2:
        segmentTypeIndex = THIRD;
        break;
      case 3:
        segmentTypeIndex = FOURTH;
        break;
      case 4:
        segmentTypeIndex = FIFTH;
        break;
      default:
        segmentTypeIndex = FIRST;
    }
    return segmentTypeIndex;
  }

  public int getIndex() {
    return index;
  }

  public String value() {
    return name();
  }
}
