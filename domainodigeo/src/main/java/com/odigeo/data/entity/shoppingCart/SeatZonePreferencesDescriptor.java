package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class SeatZonePreferencesDescriptor implements Serializable {

  private List<SeatZonePreferencesDescriptorItem> seatZonePreferencesDescriptorItems;
  private List<Integer> sections;

  public List<SeatZonePreferencesDescriptorItem> getSeatZonePreferencesDescriptorItems() {
    return seatZonePreferencesDescriptorItems;
  }

  public void setSeatZonePreferencesDescriptorItems(
      List<SeatZonePreferencesDescriptorItem> seatZonePreferencesDescriptorItems) {
    this.seatZonePreferencesDescriptorItems = seatZonePreferencesDescriptorItems;
  }

  public List<Integer> getSections() {
    return sections;
  }

  public void setSections(List<Integer> sections) {
    this.sections = sections;
  }
}
