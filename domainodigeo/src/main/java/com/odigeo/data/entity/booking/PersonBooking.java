package com.odigeo.data.entity.booking;

import java.io.Serializable;

public class PersonBooking implements Serializable {

  private long mDateOfBirth;
  private String mIdentification;
  private String mIdentificationType;
  private String mLastname;
  private String mName;

  public PersonBooking() {
    //empty
  }

  public PersonBooking(long dateOfBirth, String identification, String identificationType,
      String lastname, String name) {
    this.mDateOfBirth = dateOfBirth;
    this.mIdentification = identification;
    this.mIdentificationType = identificationType;
    this.mLastname = lastname;
    this.mName = name;
  }

  public long getDateOfBirth() {
    return mDateOfBirth;
  }

  public void setDateOfBirth(Long dateOfBirth) {
    mDateOfBirth = dateOfBirth;
  }

  public String getIdentification() {
    return mIdentification;
  }

  public void setIdentification(String identification) {
    mIdentification = identification;
  }

  public String getIdentificationType() {
    return mIdentificationType;
  }

  public void setIdentificationType(String identificationType) {
    mIdentificationType = identificationType;
  }

  public String getLastname() {
    return mLastname;
  }

  public void setLastname(String lastname) {
    mLastname = lastname;
  }

  public String getName() {
    return mName;
  }

  public void setName(String name) {
    mName = name;
  }
}
