package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;

public class ResidentValidationStatus implements Serializable {

  private int numPassenger;
  private boolean allowResidentValidationRetry;
  private ResidentValidationType residentValidationType;

  public ResidentValidationStatus() {

  }

  public int getNumPassenger() {
    return numPassenger;
  }

  public void setNumPassenger(int numPassenger) {
    this.numPassenger = numPassenger;
  }

  public boolean isAllowResidentValidationRetry() {
    return allowResidentValidationRetry;
  }

  public void setAllowResidentValidationRetry(boolean allowResidentValidationRetry) {
    this.allowResidentValidationRetry = allowResidentValidationRetry;
  }

  public ResidentValidationType getResidentValidationType() {
    return residentValidationType;
  }

  public void setResidentValidationType(ResidentValidationType residentValidationType) {
    this.residentValidationType = residentValidationType;
  }
}
