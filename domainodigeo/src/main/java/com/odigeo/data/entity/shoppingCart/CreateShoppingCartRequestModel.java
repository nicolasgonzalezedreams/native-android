package com.odigeo.data.entity.shoppingCart;

import java.io.Serializable;
import java.util.List;

public class CreateShoppingCartRequestModel implements Serializable {

  private long searchId;
  private ItinerarySelectionRequest itinerarySelectionRequest;
  private List<ExtensionRequest> extensions;

  public final long getSearchId() {
    return searchId;
  }

  public final void setSearchId(long searchId) {
    this.searchId = searchId;
  }

  public final ItinerarySelectionRequest getItinerarySelectionRequest() {
    return itinerarySelectionRequest;
  }

  public final void setItinerarySelectionRequest(
      ItinerarySelectionRequest itinerarySelectionRequest) {
    this.itinerarySelectionRequest = itinerarySelectionRequest;
  }

  public final List<ExtensionRequest> getExtensions() {
    return extensions;
  }

  public final void setExtensions(List<ExtensionRequest> extensions) {
    this.extensions = extensions;
  }
}
