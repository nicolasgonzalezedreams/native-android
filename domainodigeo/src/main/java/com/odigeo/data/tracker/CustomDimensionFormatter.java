package com.odigeo.data.tracker;

import java.util.List;
import java.util.TreeMap;

public interface CustomDimensionFormatter {

  List<String> formatCustomDimension(TreeMap<String, Integer> dimensions);
}
