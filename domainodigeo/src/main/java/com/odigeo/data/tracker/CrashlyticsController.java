package com.odigeo.data.tracker;

public interface CrashlyticsController {

  String PUT_METHOD = "PUT";
  String POST_METHOD = "POST";

  String TEST_EXCEPTION = "ABTestableDomain";
  String TEST_ALIAS = "Alias";
  String TEST_PARTITION = "Partition";

  String VISIT_USER_EXCEPTION = "VisitUser";
  String VISIT_USER_METHOD = "method";
  String VISIT_USER_METHOD_DELETE = "DELETE";
  String VISIT_USER_ID = "userId";

  String INSERT_CREDITCARD_EXCEPTION = "InsertCreditCard";
  String INSERT_CREDITCARD_METHOD = "SavedPaymentmethod";

  String PAYMENT_RETRY_3DS = "PaymentRetry 3DS";

  String PASSENGER_COUNTRY_EXCEPTION = "PassengerCountry";

  String TRUST_DEFENDER_CONFIGURATION = "TrustDefenderConfiguration";
  String TRUST_DEFENDER_STATUS_DESCRIPTION = "TrustDefenderStatus";
  String TRUST_DEFENDER_ID = "TrustDefenderId";
  String CYBERSOURCE_MERCHANT_ID = "cyberSourceMerchantId";
  String DAPI_JSESSION = "JSESSIONID";

  String INSERT_LOCALIZABLE_EXCEPTION = "InsertLocalizable";
  String LOCALIZABLES_PARSING_EXCEPTION = "LocalizablesParsingException";

  void trackNonFatal(Throwable throwable);

  void setString(String key, String value);

  void setInt(String key, int value);

  void setLong(String key, long value);

  void setBool(String key, boolean value);

  void setDouble(String key, double value);

  void setFloat(String key, float value);
}
