package com.odigeo.data.tracker;

public interface TuneTrackerInterface {

  String SEPARATOR = "-";
  String DEEPLINK_LAUNCH = "deeplink_launch";
  String VIEW_PRODUCT = "ViewProduct";
  String VIEW_BASKET = "ViewBasket";
  String PURCHASE = "Purchase";

  void setExistingUser(boolean isExistingUser);

  void trackSearch(boolean isSearch);

  void trackTransaction();

  void trackViewProduct();

  void trackViewBasket();
}
