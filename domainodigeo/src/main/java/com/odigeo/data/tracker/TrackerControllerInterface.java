package com.odigeo.data.tracker;

import com.odigeo.data.entity.tracker.Product;
import com.odigeo.data.entity.tracker.Transaction;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;
import java.util.Map;

public interface TrackerControllerInterface {

  void initTracker();

  void startSession();

  void trackDirectAppLaunch();

  void trackPushNotificationAppLaunch();

  void trackLocalNotificationAppLaunch();

  void trackOneWaySearchFlights(String departureAirport, String arrivalAirport, long departureDate,
      int adults, int children, int infants, boolean onlyDirectFlight, String classType,
      int duration);

  void trackRoundTripSearchFlights(String departureAirport, String arrivalAirport,
      long departureDate, long returnDate, int adults, int children, int infants,
      boolean onlyDirectFlight, String classType, int duration);

  void trackMultiStopSearchFlights(String departureAirport1, String arrivalAirport1,
      long departureDate1, String departureAirport2, String arrivalAirport2, long departureDate2,
      String departureAirport3, String arrivalAirport3, long departureDate3, long returnDate,
      int adults, int children, int infants, boolean onlyDirectFlight, String classType,
      int duration);

  void trackFlightBookingSummary(int numAdults, int numChildren, String classFlight,
      BigDecimal flightPrice, int numInfants, String insuranceType, String airline1,
      String arrivalAirport1, String departureAirport1, long departureDate1, String airline2,
      String arrivalAirport2, String departureAirport2, long departureDate2, String airline3,
      String arrivalAirport3, String departureAirport3, long departureDate3, String purchaseSuccess,
      boolean purchaseInsurance, long returnDate, String tripType, int valuePerPassenger,
      int numberOfBags);

  void trackLocalyticsRegisterPage(boolean enteredEmail, boolean enteredPassword,
      boolean clickSignUp, boolean clickFacebook, boolean clickGooglePlus);

  void trackRegisterPage(String category, String action, String label, String event,
      boolean enteredEmail, boolean enteredPassword, boolean clickSignUp, boolean clickFacebook,
      boolean clickGooglePlus);

  void trackLocalyticsHomeLoginPage(String event, boolean userLoginClick, boolean userSignUpClick);

  void trackJoinUsButtons(String event, boolean userLoginClick, boolean userSignUpClick,
      String category, String action, String label);

  void trackLocalyticsLoginPage(boolean enteredEmail, boolean enteredPassword, boolean clickSignUp,
      boolean clickFacebook, boolean clickGooglePlus, boolean forgotPassword);

  void trackLoginPage(String category, String action, String label, String event,
      boolean enteredEmail, boolean enteredPassword, boolean clickSignUp, boolean clickFacebook,
      boolean clickGooglePlus, boolean forgotPassword);

  void trackLoginRequestPassPage(String category, String action, String label, boolean enteredEmail,
      boolean enteredPassword, boolean clickSignUp, boolean clickFacebook, boolean clickGooglePlus,
      boolean forgotPassword);

  void trackPAXViewPage(String event, String category, String action1, String label1,
      String label2);

  void trackMyTripsManualImportViewPage(String event, String category, String action, String label);

  void trackAppseeEvent(String event);

  void trackAppseeEvent(String event, Map<String, Object> properties);

  void trackLocalyticsEvent(String event);

  void trackLocalyticsEventWithOneAtribute(String event, String atribute, String value);

  void trackAnalyticsScreen(String screen);

  void trackAnalyticsEvent(String category, String action, String label);

  void trackPaxTitleValidation(boolean isTitleValid);

  void trackPaxNameValidation(boolean isNameValid);

  void trackPaxSurnameValidation(boolean isLastnameValid);

  void trackContactInformation(boolean isNameValid, boolean isSurnameValid, boolean isEmailValid,
      boolean isCityValid, boolean isAddressValid, boolean isPostalCodeValid, boolean isPhoneValid);

  void updateDimensionOnUserLogin(boolean isLogged);

  void updateDimensionForABTest();

  void trackPassengerBaggage(int adultPassengersCount, int childPassengersCount,
      int adultPassengerBaggagePieces, int childPassengerBaggagePieces);

  void trackOpenTripGuide();

  void trackMTTCardEvent(String label);

  void trackFlightNotificationsEnabled(boolean enabled);

  void trackSSOUserWithServiceOptions();

  void setUserLocalyticsData(String name, String lastName, String email);

  void trackFullTransparencyDialogAppearance(boolean isShown);

  void trackAppRateThanksView();

  void trackYourAppExperienceLikeItButton();

  void trackYourAppExperienceNotLikeItButton();

  void trackMyTripsRateAppLikeItButton();

  void trackMyTripsRateAppNotLikeItButton();

  void trackAppRateHelpUsImproveView();

  void trackHelpCenterButtonAbout();

  void trackAppLaunched();

  void trackSearchLaunched(String flightType, int adults, int kids, int infants,
      List<String> origin, List<String> destination, List<String> date,
      List<String> departureCountries, List<String> arrivalCountries);

  void trackSummaryReached(String currency, String flightType, int adults, int kids, int infants,
      double price, List<String> departures, List<String> arrivals, List<String> dates,
      List<String> airlines, List<String> departureCountries, List<String> arrivalCountries);

  void trackSummaryContinue(String currency, String flightType, int adults, int kids, int infants,
      double price, List<String> departures, List<String> arrivals, List<String> dates,
      List<String> airlines, List<String> departureCountries, List<String> arrivalCountries);

  void trackPaymentReached(String currency, String flightType, int adults, int kids, int infants,
      double price, List<String> departures, List<String> arrivals, List<String> dates,
      List<String> airlines, List<String> departureCountries, List<String> arrivalCountries);

  void trackPaymentSuccessful(BigDecimal revenueMargin, Currency currency, String flightType,
      int adults, int kids, int infants, List<String> departures, List<String> arrivals,
      List<String> dates, List<String> airlines, List<String> departureCountries,
      List<String> arrivalCountries);

  void trackSignUp();

  void trackHotels();

  void trackCars();

  void trackSwitchNotificationsFlightStatus(boolean isOn);

  void trackConfirmationRejectedPhoneCall();

  void trackEcommerceTransaction(Transaction transaction, String gaCustomDim);

  void trackEcommerceProduct(Product product, String gaCustomDim);

  void trackApiRegisterError();

  void trackApiLoginError();

  void trackApiRecoverPasswordError();

  void trackFacebookAuthenticationErrorRegister();

  void trackGoogleAuthenticationErrorRegister();

  void trackFacebookAuthenticationErrorLogin();

  void trackGoogleAuthenticationErrorLogin();

  void trackOpenMenu();

  void trackExpirationDateMissingInPayment();

  void trackExpirationDateInvalidInPayment();

  void trackPaymentTACClick();

  void trackPaymentTACScreen();

  void trackLocalyticsEventPurchase();

  void trackPaymentPurchaseClick();

  void trackActiveBookingsOnLaunch();

  void trackActiveBookingsOnImportTrip();

  void trackActiveBookingsOnFlightConfirmed();

  void trackActiveBookingsOnFlightPending();

  void setAnalyticsMarketKey(String marketKey);

  void clearAnalyticsMarketKey();

  void trackUserLoggedWithPaymentMethods();

  void trackerUserLoggedWithoutPaymentMethods();

  void trackStorePaymentMethodIsUsed();

  void trackStorePaymentMethodNotUsed();

  void trackPaymentMethodIsStore();

  void trackPaymentMethodIsNotStore();

  void trackLocalyticsMyTripsClickedOnLogin();

  void trackAddToCalendar();
}