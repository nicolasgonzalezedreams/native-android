package com.odigeo.data.localizable;

public interface LocalizableProvider {

  String getString(String key);

  String getString(String key, String... params);

  String getRawString(String key);
}
