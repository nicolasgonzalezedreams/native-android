package com.odigeo.data.localizable;

public interface OneCMSAlarmsController {
  void setOneCMSUpdateAlarms();

  void retryOneCMSUpdate();
}
