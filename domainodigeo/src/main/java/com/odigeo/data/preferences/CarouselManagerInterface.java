package com.odigeo.data.preferences;

import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.entity.carrousel.Card;
import java.util.List;

public interface CarouselManagerInterface {
  List<Card> carouselManagerMapper(String carouselInfo);

  long getCarouselLastUpdate();

  void deleteCarouselLastUpdate();

  String getCarouselImage(Segment segment);

  void hideBookingCard(long bookingId, long date, Card.CardType cardType);

  boolean mustShow(long bookingId, Card.CardType cardType);

  void cleanPastHiddenCards();
}
