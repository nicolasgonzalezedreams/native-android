package com.odigeo.data.preferences.listener;

/**
 * Created by Jaime Toca on 27/1/16.
 */
public interface OnRequestPreferencesListener<T> {

  void onResponse(T object);

  void onError(String Error);
}
