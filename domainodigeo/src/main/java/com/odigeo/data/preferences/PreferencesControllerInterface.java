package com.odigeo.data.preferences;

import com.odigeo.configuration.ABAlias;
import com.odigeo.presenter.model.QAModeUrlModel;
import java.util.List;
import java.util.Map;

public interface PreferencesControllerInterface {

  String SHARED_PREFERENCES_ODIGEO = "com.odigeo.app.android.lib.shared_preferences";
  String SHARED_PREFERENCES_AB_TEST = "com.odigeo.app.android.lib.ab_test";
  String SHARED_PREFERENCES_AB_TEST_DIMENSIONS = "com.odigeo.app.android.lib.ab_test.dimensions";
  String SHARED_PREFERENCE_FILE_HISTORY_SEARCH = "com.odigeo.app.android.lib.historysearch";
  String LAST_UPDATE_BOOKING_STATUS = "lastupdatebooking";
  String BOOKING_CANCELED = "bookingCanceledIds";
  String BOOKING_DIVERTED = "bookingDivertedIds";
  String RECENT_SEARCHES_MIGRATED = "recentSearchesMigrated";
  String LAST_APP_OPENING = "lastAppOpening";
  String SHOULD_ADD_DROPOFF_CARD = "shouldAddDropOffCard";
  String MYTRIPS_APPRATE_SHOWCARD = "apprate_mytrips_showcard";

  String SHARED_PREFERENCE_FILE_QA_MODE_URLS = "com.odigeo.app.android.lib.qa_mode_urls";
  String PREFERENCE_QA_MODE_URLS = "qaModeUrls";
  String PREFERENCE_QA_MODE_URL = "qaModeUrl";

  String JSESSION_COOKIE = "DAPI.JSESSIONID";

  void saveStringValue(String constant, String value);

  void saveLongValue(String constant, long value);

  void saveIntValue(String constant, int value);

  void saveFloatValue(String constant, float value);

  void saveBooleanValue(String constant, boolean value);

  String getStringValue(String constant);

  Long getLongValue(String constant);

  int getIntValue(String constant);

  float getFloatValue(String constant);

  boolean getBooleanValue(String constant);

  boolean getBooleanValue(String constant, boolean defaultValue);

  void deleteAllPreferences();

  void deleteValue(String constant);

  void clearHistorySearch();

  boolean containsValue(String constant);

  void saveTestAssignment(String alias, int value);

  int getTestAssignment(ABAlias abAlias);

  void clearTestAssignments();

  void clearDimensions();

  void saveDimensions(String key, int value);

  Map<String, Integer> getDimensions();

  List<QAModeUrlModel> getQAModeUrls();

  List<QAModeUrlModel> saveQAModeUrl(QAModeUrlModel qaModeUrlModel);

  List<QAModeUrlModel> editQAModeUrl(QAModeUrlModel oldQaModeUrlModel,
      QAModeUrlModel newQaModeUrlModel);

  List<QAModeUrlModel> deleteQAModeUrl(QAModeUrlModel qaModeUrlModel);

  String getQAModeUrl();
}