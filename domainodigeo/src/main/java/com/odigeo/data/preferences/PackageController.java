package com.odigeo.data.preferences;

public interface PackageController {

  int getApplicationVersionCode();
}
