package com.odigeo.data.ab;

public interface RemoteConfigControllerInterface {
  void fetchConfig();

  void initExperiment(String experiment, String variantKeyName);

  String getStringVariant(String parameter);

  boolean getBoolVariant(String parameter);

  double getDoubleVariant(String parameter);

  long getLongVariant(String parameter);

  boolean isDropOffActive();

  boolean isWalkthroughActive();

  boolean shouldShowDefaultWalkthrough();
}
