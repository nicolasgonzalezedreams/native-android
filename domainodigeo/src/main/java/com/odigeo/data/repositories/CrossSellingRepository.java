package com.odigeo.data.repositories;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.xsell.CrossSelling;
import java.util.List;

public interface CrossSellingRepository {

  List<CrossSelling> getCrossSellingForBooking(Booking booking);

  boolean getCrossSellingArrivalGuide(Booking booking);

  boolean shouldShowGroundTransportation();
}
