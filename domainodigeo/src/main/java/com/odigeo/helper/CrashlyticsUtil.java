package com.odigeo.helper;

/**
 * @deprecated should use {@link com.odigeo.data.tracker.CrashlyticsController} instead
 */
@Deprecated public interface CrashlyticsUtil {

  void trackNonFatal(Throwable throwable);

  void setBool(String key, boolean value);

  void setDouble(String key, double value);

  void setFloat(String key, float value);

  void setInt(String key, int value);

  void setString(String key, String value);

  void setLong(String key, long value);
}
