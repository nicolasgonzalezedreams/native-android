package com.odigeo.helper;

public interface CrossCarUrlHandlerInterface {
  String CAMPAIGN_HEADER = "cartab-hp-appand";
  String CAMPAIGN_CONFIRMATION_PAGE = "confPageLink-flights-appand";
  String CAMPAIGN_MY_TRIPS = "mytrips-xsell-appand";
  String CAMPAIGN_CARD = "card-hp-appand";

  String getCarUrl(String campaign);

  String getCarUrlWithSearchLoader(String arrival, long arrivalDate, long lastDepartureDate,
      long passengerBirthdate, String campaign);
}
