package com.odigeo.helper;

import com.odigeo.configuration.ABAlias;
import com.odigeo.configuration.ABPartition;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.data.tracker.CrashlyticsController;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class ABTestHelper {

  private PreferencesControllerInterface preferencesController;
  private CrashlyticsController crashlyticsController;

  public ABTestHelper(PreferencesControllerInterface preferencesController,
      CrashlyticsController crashlyticsController) {
    this.preferencesController = preferencesController;
    this.crashlyticsController = crashlyticsController;
  }

  public ABPartition getPartition(ABAlias abAlias, int activePartitions) {
    int aliasPartition = preferencesController.getTestAssignment(abAlias);
    if (aliasPartition != 0 && activePartitions >= aliasPartition) {
      return ABPartition.fromValue(aliasPartition);
    } else {
      crashlyticsController.trackNonFatal(
          new Exception(CrashlyticsController.TEST_EXCEPTION, new Throwable()));
      crashlyticsController.setString(CrashlyticsController.TEST_ALIAS, abAlias.value());
      crashlyticsController.setInt(CrashlyticsController.TEST_PARTITION, aliasPartition);
      return ABPartition.ONE;
    }
  }

  public TreeMap<String, Integer> getDimensions() {
    Map<String, Integer> dimensions = preferencesController.getDimensions();
    TreeMap<String, Integer> sortedDimensions = new TreeMap<String, Integer>(dimensions);
    return sortedDimensions;
  }
}
