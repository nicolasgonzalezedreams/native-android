package com.odigeo.helper;

import com.odigeo.data.entity.shoppingCart.RequiredField;
import com.odigeo.data.entity.shoppingCart.TravellerRequiredFields;
import com.odigeo.data.entity.shoppingCart.request.FrequentFlyerCardCodeRequest;
import com.odigeo.data.entity.shoppingCart.request.TravellerRequest;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.presenter.contracts.views.PassengerWidget;
import com.odigeo.tools.ConditionRulesHelper;
import java.util.ArrayList;
import java.util.List;

public class CreateTravellerRequestHelper {

  private static final String MALE_GENDER = "MALE";
  private static final String FEMALE_GENDER = "FEMALE";
  private static final String STANDARD_MEAL = "STANDARD";

  private PassengerWidget mPassengerWidgetView;
  private TravellerRequiredFields mTravellerRequiredFields;
  private TravellerRequest mTravellerRequest;
  private ConditionRulesHelper mConditionsRulesHelper;

  public CreateTravellerRequestHelper(PassengerWidget passengerWidget,
      TravellerRequiredFields travellerRequiredFields, ConditionRulesHelper conditionRulesHelper) {
    mPassengerWidgetView = passengerWidget;
    mTravellerRequiredFields = travellerRequiredFields;
    mConditionsRulesHelper = conditionRulesHelper;
  }

  public TravellerRequest create() {
    mTravellerRequest = new TravellerRequest();
    setTravellerType();
    setTravellerTitleName();
    setTravellerName();
    setTravellerMiddleName();
    setTravellerFirstLastName();
    setTravellerSecondLastName();
    setTravellerGender();
    setTravellerDayOfBirth();
    setTravellerNationalityCountryCode();
    setTravellerResidentCountryCode();
    setTravellerLocalityResidentCode();
    setTravellerIdentification();
    setIdentificationExpirationDate();
    setIdentificationCountryCode();
    setTravellerBaggageSelection();
    setTravellerFrequentFlyer();
    setTravellerRequestMeal();
    return mTravellerRequest;
  }

  private void setTravellerType() {
    String travellerTypeName = mPassengerWidgetView.getPassengerType();
    mTravellerRequest.setTravellerTypeName(travellerTypeName);
  }

  private void setTravellerTitleName() {
    if ((mTravellerRequiredFields.getNeedsTitle() == RequiredField.MANDATORY)
        || (mConditionsRulesHelper.isTitleMandatory())) {
      String passengerTitleName = mPassengerWidgetView.getPassengerTitleName();
      mTravellerRequest.setTitleName(passengerTitleName);
    }
  }

  private void setTravellerName() {
    if ((mTravellerRequiredFields.getNeedsName() == RequiredField.MANDATORY)
        || (mConditionsRulesHelper.isNameMandatory())) {
      mTravellerRequest.setName(mPassengerWidgetView.getPassengerName().trim());
    }
  }

  private void setTravellerMiddleName() {
    if ((mTravellerRequiredFields.getNeedsName() == RequiredField.MANDATORY)
        || (mConditionsRulesHelper.isMiddleNameMandatory())) {
      String middleName = mPassengerWidgetView.getPassengerMiddleName();
      mTravellerRequest.setMiddleName(middleName);
    }
  }

  private void setTravellerFirstLastName() {
    if ((mTravellerRequiredFields.getNeedsName() == RequiredField.MANDATORY)
        || (mConditionsRulesHelper.isFirstLastNameMandatory())) {
      mTravellerRequest.setFirstLastName(mPassengerWidgetView.getPassengerFirstLastName().trim());
    }
  }

  private void setTravellerSecondLastName() {
    String secondLastName = mPassengerWidgetView.getPassengerSecondLastName();
    if (secondLastName != null) {
      mTravellerRequest.setSecondLastName(secondLastName);
    }
  }

  private void setTravellerGender() {
    if (mTravellerRequiredFields.getNeedsTitle() == RequiredField.MANDATORY) {
      String titleName = mPassengerWidgetView.getPassengerTitleName();
      if (titleName.equals(UserProfile.Title.MR.toString())) {
        mTravellerRequest.setGender(MALE_GENDER);
      } else {
        mTravellerRequest.setGender(FEMALE_GENDER);
      }
    }
  }

  private void setTravellerDayOfBirth() {
    if ((mTravellerRequiredFields.getNeedsBirthDate() == RequiredField.MANDATORY)
        || (mConditionsRulesHelper.isBirthDateMandatory())) {
      String dayOfBirth = mPassengerWidgetView.getPassengerDayOfBirth();
      if ((dayOfBirth != null) && (!dayOfBirth.isEmpty())) {
        String[] dayMonthYear = dayOfBirth.split("-");
        String day = dayMonthYear[0];
        String month = dayMonthYear[1];
        String year = dayMonthYear[2];
        mTravellerRequest.setDayOfBirth(Integer.parseInt(day));
        mTravellerRequest.setMonthOfBirth(Integer.parseInt(month));
        mTravellerRequest.setYearOfBirth(Integer.parseInt(year));
      }
    }
  }

  private void setTravellerNationalityCountryCode() {
    if ((mTravellerRequiredFields.getNeedsNationality() == RequiredField.MANDATORY)
        || (mConditionsRulesHelper.isNationalityMandatory())) {
      String nationalityCountryCode = mPassengerWidgetView.getPassengerNationalityCountryCode();
      mTravellerRequest.setNationalityCountryCode(nationalityCountryCode);
    }
  }

  private void setTravellerResidentCountryCode() {
    if ((mTravellerRequiredFields.getNeedsCountryOfResidence() == RequiredField.MANDATORY)
        || (mConditionsRulesHelper.isResidentMandatory())) {
      String residentCountryCode = mPassengerWidgetView.getPassengerResidentCountryCode();
      mTravellerRequest.setCountryCodeOfResidence(residentCountryCode);
    }
  }

  private void setTravellerLocalityResidentCode() {
    String localityResidentCode = mPassengerWidgetView.getLocalityResidentCode();
    if (localityResidentCode != null) {
      mTravellerRequest.setLocalityCodeOfResidence(localityResidentCode);
    }
  }

  private void setTravellerIdentification() {
    if ((mTravellerRequiredFields.getNeedsIdentification() == RequiredField.MANDATORY)
        || (mConditionsRulesHelper.isIdentificationMandatory())) {
      String identification = mPassengerWidgetView.getPassengerIdentification();
      String identificationType = mPassengerWidgetView.getPassengerIdentificationType();
      mTravellerRequest.setIdentification(identification);
      mTravellerRequest.setIdentificationTypeName(identificationType);
    }
  }

  private void setIdentificationExpirationDate() {
    if ((mTravellerRequiredFields.getNeedsIdentificationExpirationDate() == RequiredField.MANDATORY)
        || (mConditionsRulesHelper.isIdentificationExpirationDateMandatory())) {
      String identificationExpirationDate =
          mPassengerWidgetView.getPassengerIdentificationExpiration();
      if ((identificationExpirationDate != null) && (!identificationExpirationDate.isEmpty())) {
        String[] dayMonthYear = identificationExpirationDate.split("-");
        String day = dayMonthYear[0];
        String month = dayMonthYear[1];
        String year = dayMonthYear[2];
        mTravellerRequest.setIdentificationExpirationDay(Integer.parseInt(day));
        mTravellerRequest.setIdentificationExpirationMonth(Integer.parseInt(month));
        mTravellerRequest.setIdentificationExpirationYear(Integer.parseInt(year));
      }
    }
  }

  private void setIdentificationCountryCode() {
    if ((mTravellerRequiredFields.getNeedsIdentificationCountry() == RequiredField.MANDATORY)
        || (mConditionsRulesHelper.isIdentificationCountryMandatory())) {
      String identificationCountryCode =
          mPassengerWidgetView.getPassengerIdentificationCountryCode();
      mTravellerRequest.setIdentificationIssueContryCode(identificationCountryCode);
    }
  }

  private void setTravellerBaggageSelection() {
    mTravellerRequest.setBaggageSelections(mPassengerWidgetView.getPassengerBaggage());
  }

  private void setTravellerFrequentFlyer() {
    List<UserFrequentFlyer> userFrequentFlyers =
        mPassengerWidgetView.getPassengerFrequentFlyerCode();
    if ((userFrequentFlyers != null) && (!userFrequentFlyers.isEmpty())) {
      List<FrequentFlyerCardCodeRequest> frequentFlyerCardCodeRequestsList = new ArrayList<>();
      for (UserFrequentFlyer userFrequentFlyer : userFrequentFlyers) {
        FrequentFlyerCardCodeRequest frequentFlyerCardCodeRequest =
            new FrequentFlyerCardCodeRequest();
        frequentFlyerCardCodeRequest.setCarrierCode(userFrequentFlyer.getAirlineCode());
        frequentFlyerCardCodeRequest.setPassengerCardNumber(
            userFrequentFlyer.getFrequentFlyerNumber());
        frequentFlyerCardCodeRequestsList.add(frequentFlyerCardCodeRequest);
      }
      mTravellerRequest.setFrequentFlyerAirlineCodes(frequentFlyerCardCodeRequestsList);
    }
  }

  private void setTravellerRequestMeal() {
    mTravellerRequest.setMealName(STANDARD_MEAL);
  }
}
