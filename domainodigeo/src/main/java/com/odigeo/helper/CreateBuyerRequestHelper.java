package com.odigeo.helper;

import com.odigeo.data.entity.shoppingCart.BuyerRequiredFields;
import com.odigeo.data.entity.shoppingCart.RequiredField;
import com.odigeo.data.entity.shoppingCart.request.BuyerRequest;
import com.odigeo.interactors.CheckUserCredentialsInteractor;
import com.odigeo.presenter.contracts.views.PassengerViewInterface;

public class CreateBuyerRequestHelper {

  private PassengerViewInterface mPassengerViewInterface;
  private BuyerRequiredFields mBuyerRequiredFields;
  private CheckUserCredentialsInteractor mCheckUserCredentialsInteractor;
  private boolean mIsUserLogged;
  private BuyerRequest mBuyerRequest;

  public CreateBuyerRequestHelper(PassengerViewInterface passengerView,
      BuyerRequiredFields buyerRequiredFields,
      CheckUserCredentialsInteractor checkUserCredentialsInteractor, boolean isUserLogged) {

    mPassengerViewInterface = passengerView;
    mBuyerRequiredFields = buyerRequiredFields;
    mCheckUserCredentialsInteractor = checkUserCredentialsInteractor;
    mIsUserLogged = isUserLogged;
  }

  public BuyerRequest create() {

    mBuyerRequest = new BuyerRequest();
    setBuyerRequestName();
    setBuyerRequestLastNames();
    setBuyerRequestMail();
    setBuyerRequestIdentificationType();
    setBuyerRequestDateOfBirth();
    setBuyerRequestCPF();
    setBuyerRequestAddress();
    setBuyerRequestCity();
    setBuyerRequestState();
    setBuyerRequestZipCode();
    setBuyerRequestPhoneNumber();
    setBuyerRequestAlternativePhoneNumber();
    setBuyerRequestCountryCode();
    return mBuyerRequest;
  }

  private void setBuyerRequestName() {

    if (mBuyerRequiredFields.getNeedsName() == RequiredField.MANDATORY) {
      mBuyerRequest.setName(mPassengerViewInterface.getContactName());
    }
  }

  private void setBuyerRequestLastNames() {

    if (mBuyerRequiredFields.getNeedsLastNames() == RequiredField.MANDATORY) {
      mBuyerRequest.setLastNames(mPassengerViewInterface.getContactLastName());
    }
  }

  private void setBuyerRequestMail() {

    String mail;
    if ((mBuyerRequiredFields.getNeedsMail() == RequiredField.MANDATORY) && (mIsUserLogged)) {
      mail = mCheckUserCredentialsInteractor.getUserEmail();
      mBuyerRequest.setMail(mail);
    } else if ((mBuyerRequiredFields.getNeedsMail() == RequiredField.MANDATORY)
        && (!mIsUserLogged)) {
      mail = mPassengerViewInterface.getContactEmail();
      mBuyerRequest.setMail(mail);
    }
  }

  private void setBuyerRequestIdentificationType() {

    if (mBuyerRequiredFields.getNeedsIdentification() == RequiredField.MANDATORY) {
      String identification = mPassengerViewInterface.getContactIdentification();
      String identificationType = mPassengerViewInterface.getIdentificationBuyerType();
      mBuyerRequest.setIdentification(identification);
      mBuyerRequest.setBuyerIdentificationTypeName(identificationType);
    }
  }

  private void setBuyerRequestDateOfBirth() {

    if (mBuyerRequiredFields.getNeedsDateOfBirth() == RequiredField.MANDATORY) {
      String birthdate = mPassengerViewInterface.getContactDateOfBirth();
      String[] dayMonthYear = birthdate.split("-");
      String day = dayMonthYear[0];
      String month = dayMonthYear[1];
      String year = dayMonthYear[2];
      mBuyerRequest.setDayOfBirth(Integer.parseInt(day));
      mBuyerRequest.setMonthOfBirth(Integer.parseInt(month));
      mBuyerRequest.setYearOfBirth(Integer.parseInt(year));
    }
  }

  private void setBuyerRequestCPF() {

    if (mBuyerRequiredFields.getNeedsCpf() == RequiredField.MANDATORY) {
      String cpf = mPassengerViewInterface.getContactCPF();
      mBuyerRequest.setCpf(cpf);
    }
  }

  private void setBuyerRequestAddress() {

    if (mBuyerRequiredFields.getNeedsAddress() == RequiredField.MANDATORY) {
      String address = mPassengerViewInterface.getContactAddress();
      mBuyerRequest.setAddress(address);
    }
  }

  private void setBuyerRequestCity() {

    if (mBuyerRequiredFields.getNeedsCityName() == RequiredField.MANDATORY) {
      String city = mPassengerViewInterface.getContactCity();
      mBuyerRequest.setCityName(city);
    }
  }

  private void setBuyerRequestState() {

    if (mBuyerRequiredFields.getNeedsStateName() == RequiredField.MANDATORY) {
      String state = mPassengerViewInterface.getContactState();
      mBuyerRequest.setStateName(state);
    }
  }

  private void setBuyerRequestZipCode() {

    if (mBuyerRequiredFields.getNeedsZipCode() == RequiredField.MANDATORY) {
      String zipCode = mPassengerViewInterface.getContactZipCode();
      mBuyerRequest.setZipCode(zipCode);
    }
  }

  private void setBuyerRequestPhoneNumber() {

    if (mBuyerRequiredFields.getNeedsPhoneNumber() == RequiredField.MANDATORY) {
      String phoneNumber = mPassengerViewInterface.getContactPhoneNumber();
      String phoneCode = mPassengerViewInterface.getPhoneCode();
      mBuyerRequest.setPhoneNumber1(phoneNumber);
      mBuyerRequest.setCountryPhoneNumber1(phoneCode);
    }
  }

  private void setBuyerRequestAlternativePhoneNumber() {

    if (mBuyerRequiredFields.getNeedsAlternativePhoneNumber() == RequiredField.MANDATORY) {
      String phoneNumber = mPassengerViewInterface.getContactAlternativePhoneNumber();
      String phoneCode = mPassengerViewInterface.getPhoneCode();
      mBuyerRequest.setPhoneNumber2(phoneNumber);
      mBuyerRequest.setCountryPhoneNumber2(phoneCode);
    }
  }

  private void setBuyerRequestCountryCode() {

    if (mBuyerRequiredFields.getNeedsCountryCode() == RequiredField.MANDATORY) {
      String countryCode = mPassengerViewInterface.getCountryCode();
      mBuyerRequest.setCountryCode(countryCode);
    }
  }
}
