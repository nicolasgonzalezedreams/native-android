package com.odigeo.helper;

import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.List;

/**
 * Created by javier.rebollo on 5/7/16.
 */
public class MixBuyerAndTravellerHelper {

  private final IdentificationHelper mIdentificationHelper;

  public MixBuyerAndTravellerHelper(IdentificationHelper identificationHelper) {
    mIdentificationHelper = identificationHelper;
  }

  public void addBuyerToTheList(List<UserTraveller> travellerList, UserTraveller buyer) {
    boolean isAdded = false;

    for (int i = 0; i < travellerList.size(); i++) {
      if (travellerList.get(i).getBuyer()) {
        isAdded = true;
        travellerList.get(i).setEmail(buyer.getEmail());
        travellerList.get(i)
            .getUserProfile()
            .setPhoneNumber(buyer.getUserProfile().getPhoneNumber());
        travellerList.get(i)
            .getUserProfile()
            .setPrefixPhoneNumber(buyer.getUserProfile().getPrefixPhoneNumber());
        travellerList.get(i)
            .getUserProfile()
            .setUserAddress(buyer.getUserProfile().getUserAddress());

        if (buyer.getUserProfile().getUserIdentificationList() != null) {
          for (int x = 0; x < buyer.getUserProfile().getUserIdentificationList().size(); x++) {
            UserIdentification.IdentificationType identificationType =
                buyer.getUserProfile().getUserIdentificationList().get(x).getIdentificationType();
            if (identificationType != null) {
              mIdentificationHelper.checkAndUpdateIdentification(travellerList.get(i),
                  identificationType.name(),
                  buyer.getUserProfile().getUserIdentificationList().get(x).getIdentificationId());
            }
          }
        }
      }
    }

    if (!isAdded) {
      travellerList.add(buyer);
    }
  }
}
