package com.odigeo.helper;

import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by javier.rebollo on 5/7/16.
 */
public class IdentificationHelper {

  public void checkAndUpdateIdentification(UserTraveller userTraveller, String identificationType,
      String identification) {
    if (identificationType != null) {
      int identificationIndex =
          checkIfIdentificationTypeExist(userTraveller.getUserProfile().getUserIdentificationList(),
              identificationType);

      if (identificationIndex >= 0) {
        userTraveller.getUserProfile()
            .getUserIdentificationList()
            .get(identificationIndex)
            .setIdentificationId(identification);
      } else {
        if (userTraveller.getUserProfile().getUserIdentificationList() == null) {
          userTraveller.getUserProfile()
              .setUserIdentificationList(new ArrayList<UserIdentification>());
        }
        UserIdentification userIdentification = new UserIdentification();
        userIdentification.setIdentificationId(identification);
        userIdentification.setIdentificationType(
            UserIdentification.IdentificationType.valueOf(identificationType));
        userTraveller.getUserProfile().getUserIdentificationList().add(userIdentification);
      }
    }
  }

  private int checkIfIdentificationTypeExist(List<UserIdentification> identificationList,
      String newIdentificationType) {
    UserIdentification.IdentificationType identificationType =
        UserIdentification.IdentificationType.valueOf(newIdentificationType);
    int sameIdentificationPosition = -1;

    for (int i = 0; i < identificationList.size(); i++) {

      if (identificationList.get(i).getIdentificationType() == identificationType) {
        sameIdentificationPosition = i;
      }
    }

    return sameIdentificationPosition;
  }
}
