package com.odigeo.test;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 22/12/16
 */
public class AutomationConstants {
  public static final String DESTINATION_IATA_4 = "CGN";
  public static final String DESTINATION_IATA_2 = "PAR";
  public static final String DESTINATION_IATA_3 = "BER";
  public static final String DESTINATION_IATA_1 = "MAD";
  public static final String DESTINATION_CITY_1 = "Madrid";
  public static final String DESTINATION_CITY_2 = "Paris";
  public static final String CLASS_ALL = "all";
  public static final String CLASS_ECONOMY = "economy";
  public static final String CLASS_PREMIUM = "premium";
  public static final String CLASS_BUSINESS = "business";
  public static final String CLASS_FIRST = "first";
  public static final String ADULT_PAX = "1";
  public static final String CHILDREN_PAX = "0";
  public static final String INFANT_PAX = "0";

  private AutomationConstants() {
    // To prevent instantiation
  }
}
