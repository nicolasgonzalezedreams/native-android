package com.odigeo.test.mocks;

import com.odigeo.app.android.lib.models.OldMyShoppingCart;
import com.odigeo.app.android.lib.models.dto.BuyerDTO;
import com.odigeo.app.android.lib.models.dto.MoneyDTO;
import java.math.BigDecimal;

/**
 * Created by Javier Marsicano on 16/03/17.
 */

public class MyShoppingCartMocks {

  public OldMyShoppingCart provideOldMyShoppingCart() {
    OldMyShoppingCart myCart = new OldMyShoppingCart();

    MoneyDTO moneyDTO = new MoneyDTO();
    moneyDTO.setCurrency("USD");
    myCart.setPrice(moneyDTO);

    myCart.setBuyer(new BuyerDTO());
    myCart.setTotalPrice(new BigDecimal(0));
    myCart.setLocale("en_US");

    return myCart;
  }
}
