package com.odigeo.test.mocks;

import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.LocationDTO;
import com.odigeo.app.android.lib.models.dto.SectionDTO;
import com.odigeo.app.android.lib.models.dto.SegmentDTO;
import com.odigeo.app.android.model.BookingInfoViewModel;
import java.util.ArrayList;
import java.util.List;

public class BookingInfoMockProvider {

  public BookingInfoViewModel provideBookingInfo(){
    BookingInfoViewModel bookingInfo = new BookingInfoViewModel();

    bookingInfo.setSegmentsWrappers(provideSegmentWrapperList());

    return bookingInfo;
  }

  private List<SegmentWrapper> provideSegmentWrapperList() {
    List<SegmentWrapper> segmentWrapperList = new ArrayList<>();
    SegmentWrapper segmentWrapper = new SegmentWrapper();
    segmentWrapper.setKey("0,TO3141");
    CarrierDTO carrier = new CarrierDTO();
    carrier.setCode("TO");
    carrier.setId(0);
    carrier.setName("Transavia France");
    segmentWrapper.setCarrier(carrier);
    segmentWrapper.setSectionsObjects(provideListSectionsDTO());
    segmentWrapper.setSegment(provideSegmentDTO());
    segmentWrapperList.add(segmentWrapper);
    return segmentWrapperList;
  }

  private SegmentDTO provideSegmentDTO() {
    SegmentDTO segmentDTO = new SegmentDTO();
    segmentDTO.setDuration(125L);
    segmentDTO.setSeats(null);
    return segmentDTO;
  }

  private List<SectionDTO> provideListSectionsDTO() {
    List<SectionDTO> sectionDTOList = new ArrayList<>();
    SectionDTO sectionDTO = new SectionDTO();
    sectionDTO.setDepartureDate(1490779200500L);
    sectionDTO.setDuration(125L);
    sectionDTO.setLocationTo(provideLocationTo());
    sectionDTO.setLocationFrom(provideLocationFrom());
    sectionDTOList.add(sectionDTO);
    return sectionDTOList;
  }

  private LocationDTO provideLocationTo() {
    LocationDTO locationDTOTo = new LocationDTO();
    locationDTOTo.setCityIataCode("PAR");
    locationDTOTo.setIataCode("PAR");
    return locationDTOTo;
  }

  private LocationDTO provideLocationFrom() {
    LocationDTO locationDTOFrom = new LocationDTO();
    locationDTOFrom.setCityIataCode("MAD");
    locationDTOFrom.setIataCode("MAD");
    return locationDTOFrom;
  }
}
