package com.odigeo.test.mocks;

import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.models.FlightSegment;
import com.odigeo.app.android.lib.models.Section;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.app.android.lib.models.dto.LocationDTO;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.entity.geo.LocationDescriptionType;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackHelper;
import java.util.ArrayList;
import java.util.List;

public class SearchMocks {
  public SearchOptions provideSearchOptions() {
    SearchOptions searchOptions = new SearchOptions();
    searchOptions.setId(0);
    searchOptions.setNumberOfAdults(1);
    searchOptions.setNumberOfBabies(0);
    searchOptions.setNumberOfKids(0);
    searchOptions.setDirectFlight(false);
    searchOptions.setCabinClass(CabinClassDTO.DEFAULT);
    searchOptions.setTravelType(TravelType.SIMPLE);
    searchOptions.setFlightSegments(provideFLightSegments());
    searchOptions.setNumBaggages(0);
    searchOptions.setLocale("es_ES");
    searchOptions.setMultiDestinationIndex(1);
    searchOptions.setSearchDate(System.currentTimeMillis());
    return searchOptions;
  }

  private List<FlightSegment> provideFLightSegments() {
    List<FlightSegment> flightSegments = new ArrayList<>();
    flightSegments.add(provideFlightSegment());
    return flightSegments;
  }

  private FlightSegment provideFlightSegment() {
    FlightSegment flightSegment = new FlightSegment();
    flightSegment.setDepartureCity(provideDepartureCity());
    flightSegment.setArrivalCity(provideArrivalCity());
    flightSegment.setDate(1514726497);
    flightSegment.setArrivalDate(1514726497);
    flightSegment.setSections(provideSections());
    return flightSegment;
  }

  public City provideDepartureCity() {
    City city = new City();
    city.setName("El Prat");
    city.setType(LocationDescriptionType.AIRPORT);
    city.setCityName("Barcelona");
    city.setIataCode("BCN");
    city.setGeoNodeId(157);
    city.setCountryCode("ES");
    city.setCountryName("España");
    return city;
  }

  public City provideArrivalCity() {
    City city = new City();
    city.setName("Madrid");
    city.setType(LocationDescriptionType.CITY);
    city.setCityName("Madrid");
    city.setIataCode("MAD");
    city.setGeoNodeId(9748);
    city.setCountryCode("ES");
    city.setCountryName("España");
    return city;
  }

  private List<Section> provideSections() {
    List<Section> sections = new ArrayList<>();
    sections.add(provideSection());
    return sections;
  }

  private Section provideSection() {
    Section section = new Section();
    section.setDepartureDate(1514706000);
    section.setArrivalDate(1514711400);
    section.setCarrier("1");
    section.setOperatingCarrier("null");
    section.setLocationFrom(provideLocationFrom());
    section.setLocationTo(provideLocationTo());
    section.setId("2723");
    return section;
  }

  private LocationDTO provideLocationFrom() {
    LocationDTO location = new LocationDTO();
    location.setGeoNodeId(157);
    location.setCityName("Barcelona");
    location.setName("El Prat");
    location.setType("Airport");
    location.setIataCode("BCN");
    location.setCityIataCode("BCN");
    location.setCountryCode("ES");
    location.setCountryName("España");
    return location;
  }

  private LocationDTO provideLocationTo() {
    LocationDTO location = new LocationDTO();
    location.setGeoNodeId(1147);
    location.setCityName("Madrid");
    location.setName("Adolfo Suarez Madrid - Barajas");
    location.setType("Airport");
    location.setIataCode("MAD");
    location.setCityIataCode("MAD");
    location.setCountryCode("ES");
    location.setCountryName("España");
    return location;
  }

  public SearchTrackHelper provideSearchTrackHelper() {
    SearchTrackHelper searchTrackHelper = new SearchTrackHelper();
    searchTrackHelper.setAdults(1);
    searchTrackHelper.setInfants(1);
    searchTrackHelper.setKids(1);
    searchTrackHelper.setPrice(0.0);
    searchTrackHelper.addAirline("Delta");
    searchTrackHelper.addSegment("departure", "arrival", 0, "depCountry", "arrCountry");
    return searchTrackHelper;
  }
}
