package com.odigeo.test.mocks;

/**
 * Created by Javier Marsicano on 14/03/17.
 *
 * WARNING: This class is here due to LEGACY OBJECTS usage, should be moved to odigeoMocks
 * once all are refactorred
 */

public class LegacyModelMockProviders {
  private static SearchMocks searchMocks;
  private static MyShoppingCartMocks myShoppingCartMocks;
  private static BookingInfoMockProvider bookingInfoMockProvider;

  private LegacyModelMockProviders() {
    //Nothing
  }

  public static MyShoppingCartMocks provideMyShoppingCartMocks() {
    if (myShoppingCartMocks == null) {
      myShoppingCartMocks = new MyShoppingCartMocks();
    }
    return myShoppingCartMocks;
  }

  public static SearchMocks provideSearchMocks() {
    if (searchMocks == null) {
      searchMocks = new SearchMocks();
    }
    return searchMocks;
  }

  public static BookingInfoMockProvider provideBookingInfoMock(){
    if (bookingInfoMockProvider == null) {
      bookingInfoMockProvider = new BookingInfoMockProvider();
    }
    return bookingInfoMockProvider;
  }
}
