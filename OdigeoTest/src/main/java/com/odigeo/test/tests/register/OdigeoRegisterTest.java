package com.odigeo.test.tests.register;

import com.odigeo.test.robot.RegisterRobot;
import com.odigeo.test.robot.mockserver.MockServerConfigurator;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

public abstract class OdigeoRegisterTest extends BaseTest {
  public static final String REGISTER_PAGE = "Register page";
  public static final String USER_REGISTERS = "user register with email <email> and password "
      + "<password>";
  public static final String USER_IS_REGISTERED = "user is registered";

  private static final String REGISTER_PAGE_TAG = "REGISTER_PAGE";
  private static final String REGISTER_SUCCESS_PAGE = "REGISTER_SUCCESS_PAGE";

  private RegisterRobot mRegisterRobot;

  @Override public void setUp() throws Exception {
    super.setUp();
    mRegisterRobot = new RegisterRobot(mTargetContext);
  }

  @Given(REGISTER_PAGE) public void givenRegisterPage() {
    configureMockServer(MockServerConfigurator.USER_REGISTRATION_SUCCESS);
    mRegisterRobot.takeScreenshot(this, REGISTER_PAGE_TAG);
    mRegisterRobot.showRegisterPage();
    mRegisterRobot.takeScreenshot(this, REGISTER_PAGE_TAG);
  }

  @When(USER_REGISTERS) public void userRegisters(String email, String password) {
    mRegisterRobot.setEmailAndPassword(email, password)
        .clickOnRegisterButton();
  }

  @Then(USER_IS_REGISTERED) public void userIsRegistered() {
    mRegisterRobot.takeScreenshot(this, REGISTER_SUCCESS_PAGE);
    mRegisterRobot.checkUserIsRegistered();
    mRegisterRobot.takeScreenshot(this, REGISTER_SUCCESS_PAGE);
  }
}
