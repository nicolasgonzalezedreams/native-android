package com.odigeo.test.tests.payment;

import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.test.robot.PaymentRobot;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

import static com.odigeo.test.robot.mockserver.MockServerConfigurator.PROMO_CODE_ADD_SUCCESSFUL;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.PROMO_CODE_ADD_UNSUCCESSFUL;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.PROMO_CODE_REMOVE_SUCCESSFUL;

public abstract class OdigeoPromoCodeTest extends BasePaymentTest {

  private static final String PAYMENT_PAGE = "We are in the Payment page";
  private static final String CLICK_ADD_PROMO_CODE_BUTTON =
      "User select the button add promo code button";
  private static final String INSERT_A_PROMO_CODE = "Insert a <promocode>";
  private static final String SELECT_VALIDATE_BUTTON = "Select the button validate button";
  private static final String SELECT_PRICE_BREAK_DOWN_SEE_DETAILS_BUTTON =
      "Select the button price break down see details";
  private static final String PROMO_CODE_IS_INCLUDED_IN_PRICE_BREAKDOWN =
      "The promo code is included in the price break down";
  private static final String SELECT_REMOVE_PROMO_CODE_BUTTON =
      "Select the button remove promo code button";
  private static final String SELECT_CONFIRM_REMOVE_PROMO_CODE =
      "Select the button confirm remove promo code";
  private static final String PROMO_CODE_FIELD_IS_EMPTY = "The promo code field is empty";
  private static final String TOTAL_PRICE_RESULT = "Total price <result>";
  private static final String TAP_TAC_BUTTON = "User tap in terms and conditions button";
  private static final String TAC_OPTIONS_APPEARS = "Terms and conditions open options appears";

  private static final String VALID_CODE = "VALIDCODE";
  private static final String INVALID_CODE = "INVALIDCODE";
  private static final String DECREASED = "Decreased";
  private static final String REMAINS_EQUAL = "Remains equal";

  private PaymentRobot mPaymentRobot;

  @Given(PAYMENT_PAGE) public void givePaymentPage() {
    mPaymentRobot = new PaymentRobot(mTargetContext);
    Configuration.getInstance().getCurrentMarket().setNeedsExplicitAcceptance(false);
    launchPaymentActivity();
  }

  @When(CLICK_ADD_PROMO_CODE_BUTTON) public void insertValidPromoCode() {
    mPaymentRobot.takeScreenshot(this, "CLICK_ADD_PROMO_CODE_BUTTON");
    mPaymentRobot.clickOnAddPromoCodeButton();
    mPaymentRobot.takeScreenshot(this, "CLICK_ADD_PROMO_CODE_BUTTON");
  }

  @And(INSERT_A_PROMO_CODE) public void insertPromoCode(String promocode) {
    if (promocode.equals(VALID_CODE)) {
      configureMockServer(PROMO_CODE_ADD_SUCCESSFUL);
    } else if (promocode.equals(INVALID_CODE)) {
      configureMockServer(PROMO_CODE_ADD_UNSUCCESSFUL);
    }
    mPaymentRobot.takeScreenshot(this, "INSERT_A_PROMO_CODE");
    mPaymentRobot.insertPromoCode(promocode);
    mPaymentRobot.takeScreenshot(this, "INSERT_A_PROMO_CODE");
  }

  @And(SELECT_VALIDATE_BUTTON) public void clickValidateButton() {
    mPaymentRobot.takeScreenshot(this, "SELECT_VALIDATE_BUTTON");
    mPaymentRobot.clickOnValidateButton();
    mPaymentRobot.takeScreenshot(this, "SELECT_VALIDATE_BUTTON");
  }

  @And(SELECT_PRICE_BREAK_DOWN_SEE_DETAILS_BUTTON)
  public void clickPriceBreakDownSeeDetailsButton() {
    mPaymentRobot.takeScreenshot(this, "SELECT_PRICE_BREAK_DOWN_SEE_DETAILS_BUTTON");
    mPaymentRobot.clickOnPriceBreakDownSeeDetailsButton();
    mPaymentRobot.takeScreenshot(this, "SELECT_PRICE_BREAK_DOWN_SEE_DETAILS_BUTTON");
  }

  @And(SELECT_REMOVE_PROMO_CODE_BUTTON) public void clickRemovePromoCodeButton() {
    mPaymentRobot.takeScreenshot(this, "SELECT_REMOVE_PROMO_CODE_BUTTON");
    mPaymentRobot.clickOnRemovePromoCodeButton();
    mPaymentRobot.takeScreenshot(this, "SELECT_REMOVE_PROMO_CODE_BUTTON");
  }

  @And(SELECT_CONFIRM_REMOVE_PROMO_CODE) public void clickConfirmRemovePromoCode() {
    mPaymentRobot.takeScreenshot(this, "SELECT_CONFIRM_REMOVE_PROMO_CODE");
    configureMockServer(PROMO_CODE_REMOVE_SUCCESSFUL);
    mPaymentRobot.clickOnConfirmRemovePromoCode();
    mPaymentRobot.takeScreenshot(this, "SELECT_CONFIRM_REMOVE_PROMO_CODE");
  }

  @Then(PROMO_CODE_IS_INCLUDED_IN_PRICE_BREAKDOWN)
  public void checkPromoCodeIsIncludedOnPriceBreakDown() {
    mPaymentRobot.takeScreenshot(this, "SELECT_CONFIRM_REMOVE_PROMO_CODE");
    mPaymentRobot.checkPromoCodeIsIncludedOnPriceBreakDown();
    mPaymentRobot.takeScreenshot(this, "SELECT_CONFIRM_REMOVE_PROMO_CODE");
  }

  @Then(PROMO_CODE_FIELD_IS_EMPTY) public void checkPromoCodeFieldIsEmpty() {
    mPaymentRobot.takeScreenshot(this, "SELECT_CONFIRM_REMOVE_PROMO_CODE");
    mPaymentRobot.checkPromoCodeFieldIsEmpty();
  }

  @Then(TOTAL_PRICE_RESULT) public void checkTotalPrice(String result) {
    if (result.equals(DECREASED)) {
      mPaymentRobot.checkTotalPriceHasDecreased();
    } else if (result.equals(REMAINS_EQUAL)) {
      mPaymentRobot.checkTotalPriceRemainsEqual();
    }
  }

  @And(TAP_TAC_BUTTON) public void clickOnTACButton() {
    mPaymentRobot.clickOnTACButton();
  }

  @Then(TAC_OPTIONS_APPEARS) public void checkTACOpenOptionsAppear() {
    mPaymentRobot.checkTACOpenOptionsAppear();
  }
}