package com.odigeo.test.tests;

import com.odigeo.test.injection.AndroidTestInjector;
import com.odigeo.test.robot.PreferencesRobot;
import com.odigeo.test.robot.TestRobot;
import com.odigeo.test.robot.mockserver.MockServerConfigurator;
import com.pepino.TestFeature;
import org.junit.Before;

import static com.odigeo.test.robot.mockserver.MockServerConfigurator.MockServerConfiguration;

public abstract class BaseTest extends TestFeature {
  private static boolean firstRun = true;
  protected TestRobot mTestRobot = AndroidTestInjector.getInstance().provideTestRobot();
  protected PreferencesRobot mPreferencesRobot =
      AndroidTestInjector.getInstance().providePreferencesRobot();
  protected MockServerConfigurator mockServerConfigurator =
      AndroidTestInjector.getInstance().provideMockServerConfigurator();

  @Before public void setUp() throws Exception {
    mockServerConfigurator.startServer();
    mTestRobot.enableDataLoadInOdigeoSession();
    if (!firstRun) {
      mTestRobot.unregisterIdlingResource();
    }
    mTestRobot.registerIdlingResource();
    firstRun = false;
  }

  @Override public void tearDown() throws Exception {
    mockServerConfigurator.resetMockServer();
    mockServerConfigurator.stopServer();
    mTestRobot.cleanDatabase();
    mPreferencesRobot.clean();
    super.tearDown();
  }

  @Override public void featureTest() throws Exception {
    try {
      super.featureTest();
    } catch (Exception exception) {
      //            mTestRobot.takeScreenshot(this, "TestFailed"); TODO fix spoon IllegalAccessException
      throw exception;
    }
  }

  protected void configureMockServer(@MockServerConfiguration int configuration) {
    mockServerConfigurator.configure(configuration);
  }
}
