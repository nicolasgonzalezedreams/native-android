package com.odigeo.test.tests.search;

import android.Manifest;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.activities.OdigeoSearchActivity;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.entity.geo.City;
import com.odigeo.dataodigeo.session.CredentialsInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.test.robot.CalendarRobot;
import com.odigeo.test.robot.PreferencesRobot;
import com.odigeo.test.robot.ResultsRobot;
import com.odigeo.test.robot.SearchMultiStopRobot;
import com.odigeo.test.robot.SearchRobot;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.odigeo.app.android.lib.consts.Constants.SHARED_PREFERENCE_FILE_DEPARTURE_CITIES;
import static com.odigeo.app.android.lib.consts.Constants.SHARED_PREFERENCE_FILE_RETOUR_CITIES;
import static com.odigeo.test.mock.MocksProvider.providesStoredSearchMocks;
import static com.odigeo.test.mocks.LegacyModelMockProviders.provideSearchMocks;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.DESTINATION_GPS;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.SEARCH_ANY;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.SEARCH_EMPTY;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.SEARCH_MULTI_SUCCESS;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.SEARCH_ONLY_DIRECT;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.SEARCH_OW_BUSINESS;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.SEARCH_OW_ECONOMY;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.SEARCH_OW_FIRST;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.SEARCH_OW_PREMIUM;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.SEARCH_OW_SUCCESS;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.SEARCH_ROUND_ERROR;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.SEARCH_ROUND_SUCCESS;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.USER_SYNC_SEARCH;

public abstract class OdigeoSearchTest extends BaseTest {
  private static final String SEARCH_PAGE = "user is in search page";
  private static final String LATEST_SEARCHES = "user have stored latest searches";
  private static final String USER_LOGGED_IN = "user is logged in";
  private static final String SEARCH_FLIGHTS = "User searches <type> flight";
  private static final String SELECT_CITIES_NO_DF =
      "Select origin and destiny with no direct flights";
  private static final String SELECT_ONLY_DIRECT = "user selects only direct flights";
  private static final String RESEARCH_APPEAR = "ReSearch Page should appear";
  private static final String FLIGHTS_APPEAR = "flight results should appear";
  private static final String SEARCH_ERROR_APPEARS = "error page should appear";
  private static final String SEARCH_ONLY_DIRECT_APPEARS = "only direct flights should appear";
  private static final String SEARCH_SELECT_CLASS = "User select <class> class";
  private static final String SEARCH_FLIGHTS_CLASS = "flight class should be <class>";
  private static final String ONE_WAY = "OW";
  private static final String ONE_WAY_LOG_IN = "OW - Logged In";
  private static final String ONE_WAY_ECONOMY = "OW economy";
  private static final String ONE_WAY_PREMIUM = "OW premium";
  private static final String ONE_WAY_BUSINESS = "OW business";
  private static final String ONE_WAY_FIRST = "OW first";
  private static final String ROUND = "Return";
  private static final String RETURN_NO_RESULTS = "Return No Results";
  private static final String RETURN_DIRECT = "Return Direct";
  private static final String MULTI_STOP = "Multistop";
  private static final String SEARCH_ERROR = "Search_error";
  private static final String DESTINATION_SEARCHED = "the user have searched destination <place>";
  private static final String DELETE_ALL_LATEST_SEARCHES = "User delete all latest searches";
  private static final String ORIGIN_SEARCHED = "the user have searched origin <place>";
  private static final String SELECT_RECENT_DEST = "user select recent destination <place>";
  private static final String SELECT_RECENT_ORIG = "user select recent origin <place>";
  private static final String SELECT_PASSENGERS =
      "user select <Adults> adults, <Infants> infants and <Babies> babies";
  private static final String SELECT_LATEST_SEARCH = "User select a latest search";
  private static final String CHECK_RECENT_DEST = "destination selected should be <place>";
  private static final String CHECK_RECENT_ORIG = "origin selected should be <place>";
  private static final String CHECK_NO_HISTORY = "Latest search list should not appear";
  private static final String CHECK_PASSENGERS_COMBINATION = "combination <IsPossible> possible";
  private static final String TRUE = "yes";
  private static final String SYNC_SEARCHES = "searches should be syncronized in user account";

  private static final String MOCK_USER_NAME = "user";
  private static final String MOCK_USER_PWD = "1234567A";
  private static final String USER_SEARCH = "user perform search";

  private Date selectedDepartureDate;
  private Date selectedReturnDate;
  private Calendar calendar;

  private SearchRobot mSearchRobot;
  private ResultsRobot mResultsRobot;
  private PreferencesRobot mPreferencesRobot;
  private City mCityMock;
  private StoredSearch mStoredSearchMock;
  private CalendarRobot mCalendarRobot;
  private SessionController sessionController;
  private SearchMultiStopRobot mSearchMultiStopRobot;

  @Override public void setUp() throws Exception {
    super.setUp();
    mSearchRobot = new SearchRobot(mTargetContext);
    mPreferencesRobot = new PreferencesRobot(mTargetContext);
    mCalendarRobot = new CalendarRobot(mTargetContext);
    mSearchMultiStopRobot = new SearchMultiStopRobot(mTargetContext);
    sessionController = AndroidDependencyInjector.getInstance().provideSessionController();
    mCityMock = provideSearchMocks().provideDepartureCity();
    mStoredSearchMock = providesStoredSearchMocks().provideStoredSearchWithSegment();
    populateDates();
  }

  private void populateDates() {
    calendar = Calendar.getInstance();
    calendar.add(Calendar.DAY_OF_MONTH, 1);
    selectedDepartureDate = calendar.getTime();
    calendar = Calendar.getInstance();
    calendar.add(Calendar.DAY_OF_MONTH, 2);
    selectedReturnDate = calendar.getTime();
  }

  @Override public void tearDown() throws Exception {
    mPreferencesRobot.clearRecentDestinies();
    sessionController.removeAllData();
    super.tearDown();
  }

  @And(SEARCH_PAGE) @Given(SEARCH_PAGE) public void givenSearchPage() {

    configureMockServer(DESTINATION_GPS);
    getActivityTestRule().launchActivity(new Intent(mTargetContext, OdigeoSearchActivity.class));
    mSearchRobot.takeScreenshot(this, "SEARCH_PAGE");
    mTestRobot.allowPermission(Manifest.permission.ACCESS_FINE_LOCATION);
  }

  @Given(LATEST_SEARCHES) public void setLatestSearches() {
    mSearchRobot.saveStoredSearch(mStoredSearchMock);
    mSearchRobot.takeScreenshot(this, "LATEST_SEARCHES");
    configureMockServer(SEARCH_ANY);
  }

  @Given(USER_LOGGED_IN) public void userLoggedIn() {
    mTestRobot.cleanDatabase();
    sessionController.removeAllData();
    mSearchRobot.takeScreenshot(this, "USER_LOGGED_IN");

    sessionController.savePasswordCredentials(MOCK_USER_NAME, MOCK_USER_PWD,
        CredentialsInterface.CredentialsType.PASSWORD);
  }

  @When(SEARCH_FLIGHTS) @And(SEARCH_FLIGHTS) public void searchFlight(String type) {
    mSearchRobot.takeScreenshot(this, "USER_LOGGED_IN");
    switch (type) {
      case ONE_WAY_LOG_IN:
        configureMockServer(USER_SYNC_SEARCH);
        searchOneWay();
        break;
      case ONE_WAY:
        configureMockServer(SEARCH_OW_SUCCESS);
        searchOneWay();
        break;
      case ONE_WAY_ECONOMY:
        configureMockServer(SEARCH_OW_ECONOMY);
        searchOneWay();
        break;
      case ONE_WAY_PREMIUM:
        configureMockServer(SEARCH_OW_PREMIUM);
        searchOneWay();
        break;
      case ONE_WAY_BUSINESS:
        configureMockServer(SEARCH_OW_BUSINESS);
        searchOneWay();
        break;
      case ONE_WAY_FIRST:
        configureMockServer(SEARCH_OW_FIRST);
        searchOneWay();
        break;
      case ROUND:
        configureMockServer(SEARCH_ROUND_SUCCESS);
        searchRoundTrip();
        break;
      case MULTI_STOP:
        configureMockServer(SEARCH_MULTI_SUCCESS);
        searchMultiTrip();
        break;
      case SEARCH_ERROR:
        configureMockServer(SEARCH_ROUND_ERROR);
        searchRoundTrip();
        break;
      case RETURN_NO_RESULTS:
        configureMockServer(SEARCH_EMPTY);
        searchRoundTripNoResults();
        break;
      case RETURN_DIRECT:
        configureMockServer(SEARCH_ONLY_DIRECT);
        searchOneWayDirectWithoutResults();
        break;
    }
    mSearchRobot.takeScreenshot(this, "USER_LOGGED_IN_END");
  }

  private void searchOneWay() {
    mSearchRobot.selectOneWayTripTab();
    mSearchRobot.selectCitiesOneWay();
    selectDatesOneWay();
    mResultsRobot = mSearchRobot.search();
  }

  private void searchOneWayDirectWithoutResults() {
    mSearchRobot.selectOneWayTripTab();
    mSearchRobot.selectCitiesWithoutDirectFlights();
    selectDatesOneWay();
    mResultsRobot = mSearchRobot.search();
  }

  private void searchRoundTrip() {
    mSearchRobot.selectRoundTripTab();
    mSearchRobot.selectCitiesRoundTrip();
    selectDatesRoundTrip();
    mResultsRobot = mSearchRobot.search();
  }

  private void searchRoundTripNoResults() {
    mSearchRobot.selectRoundTripTab();
    mSearchRobot.selectCitiesWithoutDirectFlights();
    selectDatesRoundTrip();
    mResultsRobot = mSearchRobot.search();
  }

  private void searchMultiTrip() {
    mSearchRobot.selectCitiesMultiStop();
    mSearchMultiStopRobot.selectFirstDate();
    mCalendarRobot.selectDate(selectedDepartureDate);
    mCalendarRobot.submitDates();
    mSearchMultiStopRobot.selectSecondDate();
    mCalendarRobot.selectReturnDate(selectedReturnDate);
    mCalendarRobot.submitDates();
    mResultsRobot = mSearchRobot.search();
  }

  private void selectDatesOneWay() {
    mSearchRobot.openCalendar();
    mCalendarRobot.selectDate(selectedDepartureDate);
    mCalendarRobot.submitDates();
  }

  private void selectDatesRoundTrip() {
    mSearchRobot.openCalendar();
    mCalendarRobot.selectDepartureAndReturnDate(selectedDepartureDate, selectedReturnDate);
    mCalendarRobot.submitDates();
  }

  @And(SELECT_CITIES_NO_DF) public void selectCitiesWithoutDF() {
    mSearchRobot.selectCitiesWithoutDirectFlights();
  }

  @And(SELECT_ONLY_DIRECT) @When(SELECT_ONLY_DIRECT) public void selectOnlyDirectOption() {
    mSearchRobot.setDirectFlights();
  }

  @Then(RESEARCH_APPEAR) public void checkResearch() {
    mResultsRobot.checkResearchPageIsDisplayed();
  }

  @Then(FLIGHTS_APPEAR) public void checkFlightsAreDisplayed() {
    mResultsRobot.checkResultsAreDisplayed();
  }

  @Then(SEARCH_ERROR_APPEARS) public void checkErrorPageIsDisplayed() {
    mResultsRobot.checkErrorPageIsDisplayed();
  }

  @When(SEARCH_SELECT_CLASS) public void selectClass(@NonNull String classToSelect) {
    mSearchRobot.selectClass(classToSelect);
  }

  @And(SEARCH_FLIGHTS_CLASS) public void checkFlightsClassIs(@NonNull String classToCheck) {
    mResultsRobot.checkResultsCabinClass(classToCheck);
  }

  @And(USER_SEARCH) public void search() {
    mResultsRobot = mSearchRobot.search();
  }

  @Then(SEARCH_ONLY_DIRECT_APPEARS) public void checkOnlyDirect() {
    mResultsRobot.checkOnlyDirectFlights();
  }

  @And(DESTINATION_SEARCHED) public void searchDestination(@NonNull String place) {
    mCityMock.setIataCode(place);
    List<City> cities = new ArrayList<>();
    cities.add(mCityMock);

    mPreferencesRobot.saveMockSearchedCities(SHARED_PREFERENCE_FILE_RETOUR_CITIES, cities);
  }

  @When(SELECT_RECENT_DEST) public void selectRecentDestination(@NonNull String place) {
    mSearchRobot.selectRecentArrival(place);
  }

  @Then(CHECK_RECENT_DEST) public void verifyDestination(@NonNull String place) {
    mSearchRobot.checkDestination(place);
  }

  @And(ORIGIN_SEARCHED) public void searchOrigin(@NonNull String place) {
    mCityMock.setIataCode(place);
    List<City> cities = new ArrayList<>();
    cities.add(mCityMock);

    mPreferencesRobot.saveMockSearchedCities(SHARED_PREFERENCE_FILE_DEPARTURE_CITIES, cities);
  }

  @When(SELECT_RECENT_ORIG) public void selectRecentOrigin(@NonNull String place) {
    mSearchRobot.selectRecentDestination(place);
  }

  @Then(CHECK_RECENT_ORIG) public void verifyOrigin(@NonNull String place) {
    mSearchRobot.checkOrigin(place);
  }

  @When(SELECT_LATEST_SEARCH) public void selectLatestSearch() {
    mResultsRobot = mSearchRobot.selectLatestSearch(
        mStoredSearchMock.getSegmentList().get(0).getDestinationIATACode());
  }

  @When(DELETE_ALL_LATEST_SEARCHES) public void clearHistorySearch() {
    mSearchRobot.removeAllSearchesHistory();
  }

  @Then(CHECK_NO_HISTORY) public void checkNoSearchesInHistory() {
    mSearchRobot.checkSearchHistoryNotVisible();
  }

  @Then(SYNC_SEARCHES) public void verifySearchWasSinchronized() {
    mSearchRobot.verifySyncSearch();
  }

  @When(SELECT_PASSENGERS)
  public void selectPassengers(String adults, String infants, String babies) {
    mSearchRobot.selectPassengers(adults, infants, babies);
  }

  @Then(CHECK_PASSENGERS_COMBINATION)
  public void checkPassengersCombinationValid(String isPossible) {
    mSearchRobot.verifyPassengerPickerDismissed(isPossible.equals(TRUE));
  }
}
