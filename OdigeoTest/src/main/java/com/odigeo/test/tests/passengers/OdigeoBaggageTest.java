package com.odigeo.test.tests.passengers;

import android.content.Intent;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.mappers.StoredSearchOptionsMapper;
import com.odigeo.app.android.navigator.PaymentNavigator;
import com.odigeo.configuration.ABAlias;
import com.odigeo.data.entity.TravelType;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.test.mocks.LegacyModelMockProviders;
import com.odigeo.test.robot.PassengerRobot;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

import static com.odigeo.app.android.lib.consts.Constants.EXTRA_AVAILABLE_PRODUCTS_RESPONSE;
import static com.odigeo.app.android.lib.consts.Constants.EXTRA_BOOKING_INFO;
import static com.odigeo.app.android.lib.consts.Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE;
import static com.odigeo.app.android.lib.consts.Constants.EXTRA_SEARCH_OPTIONS;
import static com.odigeo.test.mock.MocksProvider.providesAvailableProductsMocks;
import static com.odigeo.test.mock.MocksProvider.providesBookingMocks;
import static com.odigeo.test.mock.MocksProvider.providesPaymentMocks;
import static com.odigeo.test.mock.MocksProvider.providesStoredSearchMocks;

public abstract class OdigeoBaggageTest extends BaseTest {
  private static final String PASSENGER_PAGE = "user is in Passenger page";
  private static final String PASSENGER_PAGE_TWO_PASSENGERS =
      "user is in Passenger page with two passengers";
  private static final String SELECT_BAGGAGES = "user select <numberOfBaggages> baggages";
  private static final String REMOVE_BAGGAGE_NAG_ACTIVE = "remove baggage nag is active";
  private static final String UNSELECT_BAGGAGES = "user unselect baggage";
  private static final String CONFIRM_REMOVE_BAGGAGE = "user confirm remove baggage";
  private static final String CANCEL_REMOVE_BAGGAGE = "user cancel remove baggage";
  private static final String ADD_BAGGAGE_DEFAULT_PASSENGER =
      "user adds baggages for the default passenger";
  private static final String SELECT_APPLY_BAGGAGE_TO_ALL_PASSENGERS =
      "user select apply baggage to all passengers";
  private static final String REMOVE_BAGGAGE_DEFAULT_PASSENGER =
      "user remove baggage from default passengers";
  private static final String REMOVE_BAGGAGE_SECOND_PASSENGER =
      "user remove baggage from second passenger";
  private static final String BAGGAGE_SELECTED = "user has <numberOfBaggages> baggages selected";
  private static final String NO_BAGGAGE_SELECTED = "user has no baggage selected";
  private static final String BAGGAGE_SELECTED_ALL_PASSENGERS =
      "user should see the same selection to the rest of passengers";

  private static final String PASSANGER_PAGE_BAGGAGE_INCLUDED =
      "user is in Passenger page With baggage included";
  private static final String BAGGAGE_INCLUDED = "user has baggage included";
  private static final String BAGGAGE_INFO_ONE_ROW = "baggage info appears in one row";
  private static final String ANY_PASSENGER_HAS_SELECTED_BAGGAGE =
      "any passenger should have baggage";
  private static final String APPLY_SELECTION_SWITCH_TOGGLE_OFF = "switch toggle off";
  private static final String DEFAULT_PASSENGER_HAS_BAGGAGE_SELECTED =
      "default passenger maintain it's selection";

  private PassengerRobot passengerRobot;
  private StoredSearchOptionsMapper storedSearchOptionsMapper;
  private PreferencesControllerInterface preferencesController;

  @Override public void setUp() throws Exception {
    super.setUp();
    passengerRobot = new PassengerRobot(mTargetContext);
    storedSearchOptionsMapper =
        AndroidDependencyInjector.getInstance().provideStoredSearchOptionsMapper();
    preferencesController = AndroidDependencyInjector.getInstance().providePreferencesController();
  }

  @Given(PASSENGER_PAGE) public void givenPassengerPage() {
    Intent intent = new Intent(mTargetContext, PaymentNavigator.class);

    SearchOptions searchOptions = storedSearchOptionsMapper.storedSearchToSearchOptionMapper(
        providesStoredSearchMocks().provideStoredSearchWithSegment());
    intent.putExtra(EXTRA_SEARCH_OPTIONS, searchOptions);
    intent.putExtra(EXTRA_CREATE_SHOPPING_CART_RESPONSE,
        providesPaymentMocks().provideCreateShoppingCartResponse());
    intent.putExtra(EXTRA_AVAILABLE_PRODUCTS_RESPONSE,
        providesAvailableProductsMocks().provideAvailableProducts());
    intent.putExtra(EXTRA_BOOKING_INFO,
        LegacyModelMockProviders.provideBookingInfoMock().provideBookingInfo());
    getActivityTestRule().launchActivity(intent);
  }

  @Given(PASSENGER_PAGE_TWO_PASSENGERS) public void givenPassengerPageWithTwoPassengers() {
    Intent intent = new Intent(mTargetContext, PaymentNavigator.class);

    SearchOptions searchOptions = storedSearchOptionsMapper.storedSearchToSearchOptionMapper(
        providesStoredSearchMocks().provideStoredSearchWithSegment());
    intent.putExtra(EXTRA_SEARCH_OPTIONS, searchOptions);
    CreateShoppingCartResponse createShoppingCartResponse =
        providesPaymentMocks().provideCreateShoppingCartResponse();
    createShoppingCartResponse.getShoppingCart()
        .getTravellers()
        .add(providesPaymentMocks().provideTraveller());
    createShoppingCartResponse.getShoppingCart()
        .getRequiredTravellerInformation()
        .add(providesPaymentMocks().provideTravellerRequiredFields());
    intent.putExtra(EXTRA_CREATE_SHOPPING_CART_RESPONSE, createShoppingCartResponse);
    intent.putExtra(EXTRA_AVAILABLE_PRODUCTS_RESPONSE,
        providesAvailableProductsMocks().provideAvailableProducts());
    intent.putExtra(EXTRA_BOOKING_INFO,
        LegacyModelMockProviders.provideBookingInfoMock().provideBookingInfo());
    getActivityTestRule().launchActivity(intent);
  }

  @Given(PASSANGER_PAGE_BAGGAGE_INCLUDED) public void givenPassengerPageWithBaggageIncluded() {
    Intent intent = new Intent(mTargetContext, PaymentNavigator.class);

    SearchOptions searchOptions = storedSearchOptionsMapper.storedSearchToSearchOptionMapper(
        providesStoredSearchMocks().provideStoredSearchWithSegment());
    searchOptions.setTravelType(TravelType.ROUND);

    intent.putExtra(EXTRA_SEARCH_OPTIONS, searchOptions);

    CreateShoppingCartResponse createShoppingCartResponse =
        providesPaymentMocks().provideCreateShoppingCartResponse();
    createShoppingCartResponse.getShoppingCart()
        .getRequiredTravellerInformation()
        .get(0)
        .setBaggageConditions(providesPaymentMocks().provideBaggageConditionsIncluded());

    intent.putExtra(EXTRA_CREATE_SHOPPING_CART_RESPONSE, createShoppingCartResponse);
    intent.putExtra(EXTRA_AVAILABLE_PRODUCTS_RESPONSE,
        providesAvailableProductsMocks().provideAvailableProducts());
    intent.putExtra(EXTRA_BOOKING_INFO,
        LegacyModelMockProviders.provideBookingInfoMock().provideBookingInfo());
    getActivityTestRule().launchActivity(intent);
  }

  @When(SELECT_BAGGAGES) public void selectBaggage(String numberOfBaggages) {
    if (Integer.parseInt(numberOfBaggages) == 0) {
      passengerRobot.selectNoBaggage();
    } else {
      passengerRobot.selectBaggage();
    }
  }

  @When(BAGGAGE_INCLUDED) public void baggageIsIncluded() {
    passengerRobot.goToBaggageIncluded();
  }

  @And(REMOVE_BAGGAGE_NAG_ACTIVE) public void setBPartition() {
    preferencesController.saveTestAssignment(ABAlias.BBU_BBUSTERS430.name(), 2);
  }

  @And(UNSELECT_BAGGAGES) public void unselectBaggage() {
    passengerRobot.removeBaggage(0);
  }

  @And(CONFIRM_REMOVE_BAGGAGE) public void setConfirmRemoveBaggage() {
    passengerRobot.confirmRemoveBaggage();
  }

  @And(CANCEL_REMOVE_BAGGAGE) public void setCancelRemoveBaggage() {
    passengerRobot.cancelRemoveBaggage();
  }

  @When(ADD_BAGGAGE_DEFAULT_PASSENGER) public void addBaggageDefaultPassenger() {
    passengerRobot.selectBaggage();
  }

  @And(SELECT_APPLY_BAGGAGE_TO_ALL_PASSENGERS) public void applyBaggageToAllPAssengers() {
    passengerRobot.selectApplyBaggageSelection();
  }

  @And(REMOVE_BAGGAGE_DEFAULT_PASSENGER) public void removeBaggageFromDefaultPassenger() {
    passengerRobot.removeBaggage(0);
  }

  @And(REMOVE_BAGGAGE_SECOND_PASSENGER) public void removeBaggageFromSecondPassenger() {
    passengerRobot.removeBaggage(1);
  }

  @Then(BAGGAGE_SELECTED) public void checkBaggageSelected(String numberOfBaggages) {
    if (Integer.parseInt(numberOfBaggages) == 0) {
      passengerRobot.checkNoBaggageIsSelected();
    } else {
      passengerRobot.checkBaggageIsSelected();
    }
  }

  @Then(NO_BAGGAGE_SELECTED) public void checkUNoBaggageSelected() {
    passengerRobot.checkNoBaggageIsSelected();
  }

  @Then(BAGGAGE_INFO_ONE_ROW) public void baggageInfoAppearsInOneRow() {
    passengerRobot.checkRoundTripInfoIsDisplayed();
  }

  @Then(BAGGAGE_SELECTED_ALL_PASSENGERS) public void checkSameBaggageSelectedForAllPassengers() {
    passengerRobot.checkSecondPassengerBaggageIsSelected();
  }

  @Then(ANY_PASSENGER_HAS_SELECTED_BAGGAGE) public void checkAnyPassengerHasSelectedBaggage() {
    passengerRobot.checkAnyPassengerHasSelectedBaggage();
  }

  @Then(APPLY_SELECTION_SWITCH_TOGGLE_OFF) public void checkApplySelectionSwitchIsToggleOff() {
    passengerRobot.checkApplySelectionSwitchIsToggleOff();
  }

  @And(DEFAULT_PASSENGER_HAS_BAGGAGE_SELECTED)
  public void checkDefaultPassengerHasBaggageSelected() {
    passengerRobot.checkDefaultPassengerHasBaggageSelected();
  }

  @Override public void tearDown() throws Exception {
    preferencesController.clearTestAssignments();
    super.tearDown();
  }
}
