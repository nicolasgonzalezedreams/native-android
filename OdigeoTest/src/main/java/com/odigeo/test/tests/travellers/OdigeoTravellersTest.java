package com.odigeo.test.tests.travellers;

import android.content.Intent;
import android.support.annotation.NonNull;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.navigator.TravellersNavigator;
import com.odigeo.data.db.helper.UserCreateOrUpdateHandlerInterface;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.test.robot.TravellerDetailsRobot;
import com.odigeo.test.robot.TravellersRobot;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;
import org.junit.Before;

import static com.odigeo.test.mock.MocksProvider.providesPassengerMocks;

public abstract class OdigeoTravellersTest extends BaseTest {

  private static final String TRAVELLERS_PAGE_EMPTY =
      "the Page MyData passengers without travelers added";
  private static final String ADD_NEW_TRAVELLER = "User add new traveler with the following data: "
      + "<name> name, <surname> surname, <email> email, <address> address, <city> city, "
      + "<postcode> postcode, <phoneNumber> phone, <countryCode> country";
  private static final String CHECK_TRAVELLER_ADDED =
      "new traveler has been added with <name> name and <surname> surname";
  private static final String ONE_SAVED_TRAVELLER = "travellers page with one saved traveller";
  private static final String DELETE_TRAVELLER = "user deletes existing traveller";
  private static final String CHECK_TRAVELLER_DELETED = "travaller should be deleted";
  private static final String EDIT_TRAVELLER = "user edit existing traveller with the following "
      + "data: <name> name, <surname> surname, <email> email, <address> address, <city> city, "
      + "<postcode> postcode, <phoneNumber> phone, <countryCode> country";
  private static final String CHECK_TRAVELLER_EDITED =
      "travaller info should be updated with <name> name and <surname> surname";

  private static final String TRAVELLERS_PAGE_WITH_MORE_THAN_ONE_SAVED_TRAVELLERS =
      "travellers page with more than one saved travellers";

  private static final String USER_SET_NON_MAIN_TRAVELLER_AS_MAIN =
      "user set non main traveller as main";
  private static final String CHECK_SELECTED_TRAVELLER_IS_SETTED_AS_MAIN =
      "selected traveller is setted as main";

  private TravellersRobot mTravellersRobot;
  private TravellerDetailsRobot mTravellerDetailsRobot;
  private UserTraveller mainUserTraveller;
  private UserTraveller userTraveller;
  private UserCreateOrUpdateHandlerInterface mUserCreateOrUpdateHandler;

  @Before public void setUp() throws Exception {
    super.setUp();
    mTravellersRobot = new TravellersRobot(mTargetContext);
    mTravellerDetailsRobot = new TravellerDetailsRobot(mTargetContext);
    mUserCreateOrUpdateHandler =
        AndroidDependencyInjector.getInstance().provideUserCreateOrUpdateDBHandler();
    mTestRobot.cleanDatabase();
  }

  @Given(TRAVELLERS_PAGE_EMPTY) public void openTravellersPage() {
    getActivityTestRule().launchActivity(new Intent(mTargetContext, TravellersNavigator.class));
  }

  @Given(ONE_SAVED_TRAVELLER) public void launchActivityWithOneTraveller() {
    mainUserTraveller = providesPassengerMocks().provideMainUserTraveller();
    mUserCreateOrUpdateHandler.saveAUserTravellerAndAllComponentsInDBWithoutUserLogged(
        mainUserTraveller);

    getActivityTestRule().launchActivity(new Intent(mTargetContext, TravellersNavigator.class));
  }

  @Given(TRAVELLERS_PAGE_WITH_MORE_THAN_ONE_SAVED_TRAVELLERS)
  public void launchActivityWithTwoTravellers() {
    mainUserTraveller = providesPassengerMocks().provideMainUserTraveller();
    mUserCreateOrUpdateHandler.saveAUserTravellerAndAllComponentsInDBWithoutUserLogged(
        mainUserTraveller);

    userTraveller = providesPassengerMocks().provideUserTraveller();
    mUserCreateOrUpdateHandler.saveAUserTravellerAndAllComponentsInDBWithoutUserLogged(
        userTraveller);

    getActivityTestRule().launchActivity(new Intent(mTargetContext, TravellersNavigator.class));
  }

  @When(DELETE_TRAVELLER) public void deleteTraveler() {
    mTravellersRobot.openExistingTraveller();
    mTravellerDetailsRobot.deleteTraveller();
  }

  @When(ADD_NEW_TRAVELLER)
  public void addNewTraveller(String name, String surname, String email, String address,
      String city, String postCode, String phoneNumber, String country) {
    mTravellerDetailsRobot = mTravellersRobot.openTravellerDetailsPage()
        .addTraveller(name, surname, email, address, city, postCode, phoneNumber, country);

    mTravellersRobot = mTravellerDetailsRobot.submit();
  }

  @When(EDIT_TRAVELLER)
  public void editTraveller(String name, String surname, String email, String address, String city,
      String postCode, String phoneNumber, String country) {
    mTravellerDetailsRobot = mTravellersRobot.openExistingTraveller()
        .editTraveller(name, surname, email, address, city, postCode, phoneNumber, country);

    mTravellersRobot = mTravellerDetailsRobot.submit();
  }

  @Then(CHECK_TRAVELLER_EDITED)
  public void checkTravellerHasBeenEdited(String name, String surname) {
    String travellerFullName = getTravellerCompleteName(name, surname);
    mTravellersRobot.checkTravellerHasBeenEdited(travellerFullName);
  }

  @Then(CHECK_TRAVELLER_ADDED) public void checkMainTravellerAdded(String name, String surname) {
    String travellerFullName = getTravellerCompleteName(name, surname);
    mTravellersRobot.checkTravellerHasBeenAdded(travellerFullName);
  }

  @When(USER_SET_NON_MAIN_TRAVELLER_AS_MAIN) public void setNonMainTravellerAsMain() {
    String travellerCompleteName =
        getTravellerCompleteName(userTraveller.getUserProfile().getName(),
            userTraveller.getUserProfile().getFirstLastName());
    mTravellersRobot.selectMainTraveller(travellerCompleteName);
  }

  @Then(CHECK_TRAVELLER_DELETED) public void checkTravellerHasBeenDeleted() {
    mTravellersRobot.checkThereAreNoTravellers();
  }

  @Then(CHECK_SELECTED_TRAVELLER_IS_SETTED_AS_MAIN) public void checkTravellerIsSettedAsMain() {
    String travellerCompleteName =
        getTravellerCompleteName(userTraveller.getUserProfile().getName(),
            userTraveller.getUserProfile().getFirstLastName());
    mTravellersRobot.checkIsMainTraveller(travellerCompleteName);
  }

  @NonNull private String getTravellerCompleteName(String name, String surname) {
    return name + " " + surname;
  }
}
