package com.odigeo.test.tests.payment;

import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.model.BookingInfoViewModel;
import com.odigeo.app.android.navigator.PaymentNavigator;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackerFlowSession;
import com.odigeo.test.tests.BaseTest;

import static com.odigeo.test.mock.MocksProvider.providesPaymentMocks;
import static com.odigeo.test.mocks.LegacyModelMockProviders.provideBookingInfoMock;
import static com.odigeo.test.mocks.LegacyModelMockProviders.provideMyShoppingCartMocks;
import static com.odigeo.test.mocks.LegacyModelMockProviders.provideSearchMocks;

public abstract class BasePaymentTest extends BaseTest {

  public void launchPaymentActivity() {
    Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
    OdigeoApp odigeoApp = (OdigeoApp) instrumentation.getTargetContext().getApplicationContext();
    OdigeoSession odigeoSession = odigeoApp.getOdigeoSession();
    odigeoSession.setOldMyShoppingCart(
        provideMyShoppingCartMocks().provideOldMyShoppingCart());

    SearchTrackerFlowSession.getInstance()
        .setSearchTrackHelper(provideSearchMocks().provideSearchTrackHelper());
    Intent intent = new Intent(mTargetContext, PaymentNavigator.class);

    CreateShoppingCartResponse createShoppingCartResponse =
        providesPaymentMocks().provideCreateShoppingCartResponse();
    SearchOptions searchOptions = provideSearchMocks().provideSearchOptions();
    BookingInfoViewModel bookingInfo = provideBookingInfoMock().provideBookingInfo();

    intent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE, createShoppingCartResponse);
    intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, searchOptions);
    intent.putExtra(Constants.EXTRA_BOOKING_INFO, bookingInfo);
    getActivityTestRule().launchActivity(intent);
  }
}
