package com.odigeo.test.tests.logout;

import android.Manifest;
import android.content.Intent;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.navigator.NavigationDrawerNavigator;
import com.odigeo.dataodigeo.session.CredentialsInterface;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.test.robot.NavigationDrawerRobot;
import com.odigeo.test.robot.mockserver.MockServerConfigurator;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

public abstract class OdigeoLogoutTest extends BaseTest {

  private static final String USER_LOGGED_IN = "The user is logged in";
  private static final String USER_LOG_OUT = "User log out";
  private static final String USER_IS_NOT_LOGGED_IN = "User is not logged in";

  private NavigationDrawerRobot mNavigationDrawerRobot;
  private SessionController mSessionController;

  @Override public void setUp() throws Exception {
    super.setUp();
    mNavigationDrawerRobot = new NavigationDrawerRobot(mTargetContext);
    mSessionController = AndroidDependencyInjector.getInstance().provideSessionController();
  }

  @Override public void tearDown() throws Exception {
    mSessionController.removeAllData();
    super.tearDown();
  }

  @Given(USER_LOGGED_IN) public void givenUserLoggedIn() {
    String mockedUserName = "Test User";
    String mockedUserPassword = "1234567A";
    mSessionController.removeAllData();
    mSessionController.savePasswordCredentials(mockedUserName, mockedUserPassword,
        CredentialsInterface.CredentialsType.PASSWORD);
    getActivityTestRule().launchActivity(
        new Intent(mTargetContext, NavigationDrawerNavigator.class));
  }

  @When(USER_LOG_OUT) public void logout() {
    configureMockServer(MockServerConfigurator.DISABLE_NOTIFICATIONS_SUCCESS);
    mNavigationDrawerRobot.logout();
    mTestRobot.allowPermission(Manifest.permission.ACCESS_FINE_LOCATION);
  }

  @Then(USER_IS_NOT_LOGGED_IN) public void checkUserNotLoggedIn() {
    mNavigationDrawerRobot.checkUserNotLogged();
  }
}
