package com.odigeo.test.tests.settings;

import com.odigeo.test.robot.PreferencesRobot;
import com.odigeo.test.robot.SettingsRobot;
import com.odigeo.test.robot.mockserver.MockServerConfigurator;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

/**
 * Created by Javier Marsicano on 09/01/17.
 */

public abstract class OdigeoSettingsTest extends BaseTest {
  public static final String SETTINGS_PAGE = "Settings page";
  public static final String SELECT_COUNTRY_IN_SETTINGS = "user select <country> country";
  public static final String VERIFY_COUNTRY_IN_SETTINGS = "country selected is <country>";
  public static final String OPEN_COUNTRIES = "User change \"Distance Settings\"";
  public static final String VERIFY_UNITS_CHANGED = "distance units have changed";
  public static final String SELECT_PASSENGER_SETTINGS = "User select \"Passenger Settings\"";
  public static final String SELECT_PASSENGERS =
      "user select <adults> adults, <children> children and <infants> infants";
  public static final String VERIFY_PASSENGERS =
      "selected users are <adults> adults, <children> children and <infants> infants.";
  public static final String CHANGE_PN_STATUS = "User changes Push Notification status";
  public static final String VERITY_PN_STATUS = "Push notification status has changed";

  private SettingsRobot mSettingsRobot;
  private PreferencesRobot mPreferencesRobot;

  @Override public void setUp() throws Exception {
    super.setUp();
    mSettingsRobot = new SettingsRobot(mTargetContext);
    mPreferencesRobot = new PreferencesRobot(mTargetContext);
  }

  @Override public void tearDown() throws Exception {
    mPreferencesRobot.cleanSettingsPreferences();
    super.tearDown();
  }

  @Given(SETTINGS_PAGE) public void givenSettingsPage() {
    mPreferencesRobot.takeScreenshot(this, "SETTINGS_PAGE");
    mPreferencesRobot.cleanSettingsPreferences();
    mPreferencesRobot.takeScreenshot(this, "SETTINGS_PAGE");
  }

  @When(SELECT_COUNTRY_IN_SETTINGS) public void selectCountry(String country) {
    configureMockServer(MockServerConfigurator.GET_VISITS);
    mSettingsRobot.takeScreenshot(this, "SELECT_COUNTRY_IN_SETTINGS");
    mSettingsRobot.selectCountry(country);
    mSettingsRobot.takeScreenshot(this, "SELECT_COUNTRY_IN_SETTINGS");
  }

  @Then(VERIFY_COUNTRY_IN_SETTINGS) public void checkSelectedCountry(String country) {
    mSettingsRobot.takeScreenshot(this, "VERIFY_COUNTRY_IN_SETTINGS");
    mSettingsRobot.verifyCountry(country);
    mSettingsRobot.takeScreenshot(this, "VERIFY_COUNTRY_IN_SETTINGS");
  }

  @When(OPEN_COUNTRIES) public void selectUnits() {
    mSettingsRobot.takeScreenshot(this, "OPEN_COUNTRIES");
    mSettingsRobot.changeDistanceUnits();
    mSettingsRobot.takeScreenshot(this, "OPEN_COUNTRIES");
  }

  @Then(VERIFY_UNITS_CHANGED) public void verifyUnitsChanged() {
    mSettingsRobot.takeScreenshot(this, "VERIFY_UNITS_CHANGED");
    mSettingsRobot.verifyDistanceUnits();
    mSettingsRobot.takeScreenshot(this, "VERIFY_UNITS_CHANGED");
  }

  @When(SELECT_PASSENGER_SETTINGS) public void selectPassengerSettings() {
    //nothing
  }

  @And(SELECT_PASSENGERS)
  public void setPassengersCombination(String adults, String children, String infants) {
    mSettingsRobot.takeScreenshot(this, "SELECT_PASSENGERS");
    mSettingsRobot.selectPassengers(adults, children, infants);
    mSettingsRobot.takeScreenshot(this, "SELECT_PASSENGERS");
  }

  @Then(VERIFY_PASSENGERS)
  public void verifySelectedPassengers(String adults, String children, String infants) {
    mSettingsRobot.takeScreenshot(this, "VERIFY_PASSENGERS");
    mSettingsRobot.verifyPassengers(adults, children, infants);
    mSettingsRobot.takeScreenshot(this, "VERIFY_PASSENGERS");
  }

  @When(CHANGE_PN_STATUS) public void changePushNotificationStatus() {
    mSettingsRobot.takeScreenshot(this, "CHANGE_PN_STATUS");
    mSettingsRobot.changePushNotificationsStatus();
    mSettingsRobot.takeScreenshot(this, "CHANGE_PN_STATUS");
  }

  @Then(VERITY_PN_STATUS) public void checkPNStatus() {
    mSettingsRobot.takeScreenshot(this, "VERITY_PN_STATUS");
    mSettingsRobot.verifyPushNotificationsStatus();
    mSettingsRobot.takeScreenshot(this, "VERITY_PN_STATUS");
  }
}
