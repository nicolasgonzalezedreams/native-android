package com.odigeo.test.tests.login;

import android.content.Intent;
import com.odigeo.test.robot.LoginRobot;
import com.odigeo.test.robot.NavigationDrawerRobot;
import com.odigeo.test.robot.mockserver.MockServerConfigurator;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

public abstract class OdigeoLoginTest extends BaseTest {

  private static final String LOGIN_PAGE = "user is in login page";
  private static final String LOGIN_VALID_DETAILS = "user login with valid details";
  private static final String SHOULD_BE_LOGGED_IN = "user should be logged in";
  private static final String LOGIN_INVALID_EMAIL = "user enter invalid email";
  private static final String LOGIN_INVALID_PASSWORD = "user enter invalid password";
  private static final String INVALID_EMAIL_ERROR =
      "email not registered error should be displayed";
  private static final String INVALID_PASSWORD_ERROR = "invalid password error should be displayed";

  private LoginRobot mLoginRobot;
  private NavigationDrawerRobot mNavigationDrawerRobot;

  @Given(LOGIN_PAGE) public void givenLoginPage() {
    mLoginRobot = new LoginRobot(mTargetContext);
    Intent intent = new Intent();
    getActivityTestRule().launchActivity(intent);
  }

  @When(LOGIN_VALID_DETAILS) public void loginWithValidDetails() {
    configureMockServer(MockServerConfigurator.LOGIN_SUCCESSFUL);
    mNavigationDrawerRobot = mLoginRobot.emailLoginWithValidCredentials();
  }

  @When(LOGIN_INVALID_EMAIL) public void loginWithInvalidEmail() {
    configureMockServer(MockServerConfigurator.LOGIN_EMAIL_ERROR);
    mLoginRobot.emailLoginWithInvalidCredentials();
  }

  @When(LOGIN_INVALID_PASSWORD) public void loginWithInvalidPassword() {
    configureMockServer(MockServerConfigurator.LOGIN_PWD_ERROR);
    mLoginRobot.emailLoginWithInvalidCredentials();
  }

  @Then(SHOULD_BE_LOGGED_IN) public void checkUserIsLoggedIn() {
    mNavigationDrawerRobot.checkUserIsLoggedIn();
  }

  @Then(INVALID_EMAIL_ERROR) public void checkEmailLoginErrorIsDisplayed() {
    mLoginRobot.checkErrorEmailMessageIsDisplayed();
  }

  @Then(INVALID_PASSWORD_ERROR) public void checkPasswordLoginErrorIsDisplayed() {
    mLoginRobot.checkErrorPasswordMessageIsDisplayed();
  }
}
