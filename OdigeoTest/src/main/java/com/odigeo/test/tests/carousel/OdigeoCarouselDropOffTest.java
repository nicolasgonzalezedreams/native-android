package com.odigeo.test.tests.carousel;

import android.Manifest;
import android.content.Intent;
import com.odigeo.app.android.navigator.NavigationDrawerNavigator;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.test.robot.CarouselRobot;
import com.odigeo.test.robot.NavigationDrawerRobot;
import com.odigeo.test.robot.PreferencesRobot;
import com.odigeo.test.robot.SearchRobot;
import com.odigeo.test.robot.mockserver.MockServerConfigurator;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

import static com.odigeo.test.AutomationConstants.ADULT_PAX;
import static com.odigeo.test.AutomationConstants.CHILDREN_PAX;
import static com.odigeo.test.AutomationConstants.DESTINATION_IATA_1;
import static com.odigeo.test.AutomationConstants.DESTINATION_IATA_2;
import static com.odigeo.test.AutomationConstants.INFANT_PAX;
import static com.odigeo.test.mock.MocksProvider.providesStoredSearchMocks;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.EDREAMS;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.GOVOYAGES;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.OPODO;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.TRAVELLINK;

public abstract class OdigeoCarouselDropOffTest extends BaseTest {

  private static final String USER_SEARCH_FLIGHT = "user search a flight";
  private static final String USER_HAVE_ONE_DROP_OFF_CARD = "user have one drop-off card";
  private static final String USER_ONE_DROP_OFF_PAST_DATE =
      "user have one drop-off card with a past date";
  private static final String USER_RETURNS_HOME_INCOMPLETE_BOOKING =
      "user returns to home page without complete the booking";
  private static final String USER_SELECTS_EDIT_SEARCH = "user select edit search";
  private static final String USER_SELECTS_RESEARCH = "user select re-search";
  private static final String USER_OPENS_APP = "user open the app";
  private static final String USER_HAVE_CARD_IN_FIRST_POSITION =
      "user have a card in first position";
  private static final String USER_IS_IN_RESULTS_PAGE = "user is in results page";
  private static final String SEARCH_PAGE_IS_DISPLAYED = "search page is displayed";
  private static final String SEARCH_INFORMATION_IS_DISPLAYED = "search information is displayed";
  private static final String SEARCH_DETAILS_FILLED =
      "search details are filled with the card information";
  private static final String CARD_SHOULD_NOT_BE_DISPLAYED = "card should not be displayed";
  private static final String GO_BACK_TO_HOME_PAGE = "go back to Home page";

  private CarouselRobot carouselRobot;
  private SearchRobot searchRobot;
  private NavigationDrawerRobot navigatorRobot;
  private PreferencesRobot preferencesRobot;

  private StoredSearch mStoredSearchMock;
  private StoredSearch mStoredSearchMockPastDate;

  @Override public void setUp() throws Exception {
    super.setUp();
    carouselRobot = new CarouselRobot(mTargetContext);
    searchRobot = new SearchRobot(mTargetContext);
    navigatorRobot = new NavigationDrawerRobot(mTargetContext);
    preferencesRobot = new PreferencesRobot(mTargetContext);

    mStoredSearchMock = providesStoredSearchMocks().provideStoredSearchWithSegment();
    mStoredSearchMockPastDate = providesStoredSearchMocks().provideStoredSearchPastDate();

    preferencesRobot.setShouldShowDropOffCards(true);

    int mockserverConfiguration = getMockServerConfigurationPerBrand(getBrand());
    configureMockServer(mockserverConfiguration);
  }

  @Given(USER_HAVE_ONE_DROP_OFF_CARD) public void userHaveOneDropOffCard() {
    searchRobot.saveStoredSearch(mStoredSearchMock);

    getActivityTestRule().launchActivity(
        new Intent(mTargetContext, NavigationDrawerNavigator.class));
    configureMockServer(MockServerConfigurator.SEARCH_OW_SUCCESS);
  }

  @Given(USER_ONE_DROP_OFF_PAST_DATE) public void userOneDropOffCardPastDate() {
    searchRobot.saveStoredSearch(mStoredSearchMockPastDate);
  }

  @When(USER_SELECTS_EDIT_SEARCH) public void userSelectsEditSearch() {
    carouselRobot.clickOnEditSearch();
  }

  @When(USER_SELECTS_RESEARCH) public void userSelectsReSearch() {
    carouselRobot.clickOnReSearch();
  }

  @When(USER_OPENS_APP) public void userOpensApp() {
    getActivityTestRule().launchActivity(
        new Intent(mTargetContext, NavigationDrawerNavigator.class));
  }

  @Then(SEARCH_PAGE_IS_DISPLAYED) public void searchPageIsDisplayed() {
    mTestRobot.allowPermission(Manifest.permission.ACCESS_FINE_LOCATION);
    carouselRobot.isSearchScreenShowing();
  }

  @Then(USER_IS_IN_RESULTS_PAGE) public void userIsInResultsPage() {
    mTestRobot.allowPermission(Manifest.permission.ACCESS_FINE_LOCATION);
    carouselRobot.isResultsPageShowing();
  }

  @Then(CARD_SHOULD_NOT_BE_DISPLAYED) public void cardShouldNotBeDisplayed() {
    carouselRobot.checkDropOffCardIsNotShowing();
  }

  @And(SEARCH_DETAILS_FILLED) public void searchDetailsFilled() {
    searchRobot.checkDestination(DESTINATION_IATA_2).checkOrigin(DESTINATION_IATA_1);
  }

  @And(GO_BACK_TO_HOME_PAGE) public void goBackToHomePage() {
    searchRobot.back();
  }

  @MockServerConfigurator.MockServerConfiguration
  private int getMockServerConfigurationPerBrand(@MockServerConfigurator.Brand int brand) {
    @MockServerConfigurator.MockServerConfiguration int configuration =
        MockServerConfigurator.CAROUSEL_2CTA_SUCCESS_ED;

    switch (brand) {
      case EDREAMS:
        configuration = MockServerConfigurator.CAROUSEL_2CTA_SUCCESS_ED;
        break;
      case OPODO:
        configuration = MockServerConfigurator.CAROUSEL_2CTA_SUCCESS_OP;
        break;
      case GOVOYAGES:
        configuration = MockServerConfigurator.CAROUSEL_2CTA_SUCCESS_GO;
        break;
      case TRAVELLINK:
        configuration = MockServerConfigurator.CAROUSEL_2CTA_SUCCESS_TL;
    }

    return configuration;
  }

  public abstract @MockServerConfigurator.Brand int getBrand();

  public String getTotalNumberOfPax() {
    int adult = Integer.valueOf(ADULT_PAX);
    int children = Integer.valueOf(CHILDREN_PAX);
    int infant = Integer.valueOf(INFANT_PAX);

    return String.valueOf(adult + children + infant);
  }
}
