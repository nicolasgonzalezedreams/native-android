package com.odigeo.test.tests.payment;

import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.test.robot.ConfirmationRobot;
import com.odigeo.test.robot.PaymentRobot;
import com.odigeo.test.robot.mockserver.MockServerConfigurator;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

public abstract class OdigeoPaymentLogicTest extends BasePaymentTest {

  private static final String ADD_PAYMENT_DETAILS = "user entered a valid payment method";
  private static final String SELECT_PAYMENT_METHOD = "user select the following <payment_method>";
  private static final String MSL_RESPONSE =
      "MSL is going to return <bookingStatus> <collectionState>";
  private static final String MSL_RESPONSE_STATUS = "MSL is going to return <status>";
  private static final String MSL_RESPONSE_PAYMENT_RETRY =
      "MSL is going to return BOOKING_PAYMENT_RETRY with <collectionOptions>";
  private static final String MSL_RESPONSE_REPRICING = "MSL is going to return BOOKING_REPRICING";
  private static final String MSL_RESPONSE_USER_INTERACTION_NEEDED =
      "MSL is going to return USER_INTERACTION_NEEDED with " + "<payment_method>";
  private static final String PURCHASE = "user purchase";
  private static final String CHECK_CONFIRMATION_PAGE_CONFIRMED =
      "user should have a confirmed booking";
  private static final String CHECK_CONFIRMATION_PAGE_PENDING =
      "user should have a pending booking";
  private static final String CHECK_CONFIRMATION_PAGE_REJECTED =
      "user should have a rejected booking";
  private static final String CHECK_PAYMENT_RETRY_MESSAGE = "user should see payment retry message";
  private static final String CHECK_PAYMENT_REPRICING_MESSAGE = "user should see repricing message";
  private static final String CHECK_EXTERNAL_PAYMENT_VIEW = "user should see External payment view";
  private static final String GROUND_TRANSPORTATION_WIDGET_SHOWN =
      "ground transportation widget is shown";

  private static final String creditCardNumber = "4111111111111111";
  private static final String creditCardName = "Test User";
  private static final String creditCardExpirationMonth = "12";
  private static final String creditCardExpirationYear = "21";
  private static final String creditCardCVV = "123";

  private PaymentRobot mPaymentRobot;
  private ConfirmationRobot mConfirmationRobot;

  @Given(ADD_PAYMENT_DETAILS) public void addValidPaymentDetails() {
    mPaymentRobot = new PaymentRobot(mTargetContext);
    Configuration.getInstance().getCurrentMarket().setNeedsExplicitAcceptance(false);
    launchPaymentActivity();
    configureMockServer(MockServerConfigurator.BIN_CHECK_VALIDATION_SUCCESSFUL);
    mPaymentRobot.addCreditcard(creditCardNumber);
    mPaymentRobot.addCreditcardName(creditCardName);
    mPaymentRobot.addCreditcardExpirationMonth(creditCardExpirationMonth);
    mPaymentRobot.addCreditcardExpirationYear(creditCardExpirationYear);
    mPaymentRobot.addCVV(creditCardCVV);
  }

  @Given(SELECT_PAYMENT_METHOD) public void selectPaymentMethod(String payment_method) {
    mPaymentRobot = new PaymentRobot(mTargetContext);
    Configuration.getInstance().getCurrentMarket().setNeedsExplicitAcceptance(false);
    launchPaymentActivity();
    switch (payment_method) {
      case "PAYPAL":
        mPaymentRobot.clickAndCheckOnPaypalPaymentMethod();
        break;
      case "TRUSTLY":
        mPaymentRobot.clickAndCheckOnTrustlyPaymentMethod();
        break;
      case "KLARNA":
        mPaymentRobot.clickAndCheckOnKlarnaPaymentMethod();
        break;
    }
  }

  @When(MSL_RESPONSE)
  public void configureMockServerMSLResponse(String bookingStatus, String collectionState) {
    switch (bookingStatus) {
      case "CONTRACT":
        configureMockServer(MockServerConfigurator.BOOK_BOOKINGCONFIRMED_CONTRACT);
        break;
      case "REQUEST":
        if (collectionState.equals("COLLECTED")) {
          configureMockServer(MockServerConfigurator.BOOK_BOOKINGCONFIRMED_REQUEST_COLLECTED);
        } else {
          configureMockServer(
              MockServerConfigurator.BOOK_BOOKINGCONFIRMED_REQUEST_COLLECTION_NEEDED);
        }
        break;
      case "RETAINED":
        configureMockServer(MockServerConfigurator.BOOK_BOOKINGCONFIRMED_RETAINED);
        break;
      case "HOLD":
        configureMockServer(MockServerConfigurator.BOOK_BOOKINGCONFIRMED_ON_HOLD);
        break;
      case "UNKNOWN":
        configureMockServer(MockServerConfigurator.BOOK_BOOKINGCONFIRMED_UNKNOWN);
        break;
      case "REJECTED":
        configureMockServer(MockServerConfigurator.BOOK_BOOKINGCONFIRMED_REJECTED);
        break;
      case "FINAL_RET":
        configureMockServer(MockServerConfigurator.BOOK_BOOKINGCONFIRMED_FINAL_RET);
        break;
    }
  }

  @When(MSL_RESPONSE_STATUS) public void configureMockServerMSLResponse(String status) {
    switch (status) {
      case "BOOKING_ERROR":
        configureMockServer(MockServerConfigurator.BOOK_BOOKING_ERROR);
        break;
      case "BROKEN_FLOW":
        configureMockServer(MockServerConfigurator.BOOK_BROKEN_FLOW);
        break;
      case "BOOKING_STOP":
        configureMockServer(MockServerConfigurator.BOOK_BOOKING_STOP);
        break;
      case "HOLD":
        configureMockServer(MockServerConfigurator.BOOK_ON_HOLD);
        break;
    }
  }

  @When(MSL_RESPONSE_PAYMENT_RETRY)
  public void configureMockServerMSLResponsePaymentRetry(String collectionOptions) {
    switch (collectionOptions) {
      case "ONLY_PAY_PAL":
        configureMockServer(MockServerConfigurator.BOOK_PAYMENT_RETRY_ONLY_PAY_PAL);
        break;
      case "BANK_TRANSFER_AND_CREDIT_CARDS":
        configureMockServer(
            MockServerConfigurator.BOOK_PAYMENT_RETRY_BANK_TRANSFER_AND_CREDIT_CARDS);
        break;
    }
  }

  @When(MSL_RESPONSE_REPRICING) public void configureMockServerMSLResponsePaymentRepricing() {
    configureMockServer(MockServerConfigurator.BOOK_PAYMENT_REPRICING);
  }

  @When(MSL_RESPONSE_USER_INTERACTION_NEEDED)
  public void configureMockServerMSLResponseUserInteractionNeeded(String payment_method) {
    switch (payment_method) {
      case "PAYPAL":
        configureMockServer(MockServerConfigurator.BOOK_PAYMENT_USER_INTERACTION_NEEDED_PAYPAL);
        break;
      case "TRUSTLY":
        configureMockServer(MockServerConfigurator.BOOK_PAYMENT_USER_INTERACTION_NEEDED_TRUSTLY);
        break;
      case "KLARNA":
        configureMockServer(MockServerConfigurator.BOOK_PAYMENT_USER_INTERACTION_NEEDED_KLARNA);
        break;
    }
  }

  @And(PURCHASE) public ConfirmationRobot purchase() {
    mConfirmationRobot = mPaymentRobot.purchase();
    return mConfirmationRobot;
  }

  @Then(CHECK_CONFIRMATION_PAGE_CONFIRMED) public void checkIsConfirmationPageSuccess() {
    mConfirmationRobot.checkConfirmationSuccess();
  }

  @Then(CHECK_CONFIRMATION_PAGE_PENDING) public void checkIsConfirmationPagePending() {
    mConfirmationRobot.checkConfirmationPending();
  }

  @Then(CHECK_CONFIRMATION_PAGE_REJECTED) public void checkIsConfirmationPageRejected() {
    mConfirmationRobot.checkConfirmationRejected();
  }

  @Then(CHECK_PAYMENT_RETRY_MESSAGE) public void checkIsPaymentRetryMessage() {
    mPaymentRobot.checkPaymentRetryMessageIsShown();
  }

  @Then(CHECK_PAYMENT_REPRICING_MESSAGE) public void checkIsPaymentRepricingMessage() {
    mPaymentRobot.checkPaymentRepricingMessageIsShown();
  }

  @Then(CHECK_EXTERNAL_PAYMENT_VIEW) public void checkIsExternalPaymentViewOpen() {
    mPaymentRobot.checkExternalPaymentViewIsShown();
  }

  @Then(GROUND_TRANSPORTATION_WIDGET_SHOWN) public void groundTransportationWidgetIsShown() {
    mConfirmationRobot.checkGroundTransportationWidgetIsShown(true);
  }


}

