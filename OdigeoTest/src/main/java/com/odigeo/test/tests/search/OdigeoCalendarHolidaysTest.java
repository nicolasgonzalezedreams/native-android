package com.odigeo.test.tests.search;

import android.Manifest;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Market;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.test.robot.CalendarRobot;
import com.odigeo.test.robot.SearchRobot;
import com.odigeo.test.tests.BaseTest;
import com.odigeo.tools.DateUtils;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;
import java.util.Date;
import java.util.List;

public abstract class OdigeoCalendarHolidaysTest extends BaseTest {

  private CalendarRobot mCalendarRobot;
  private SearchRobot searchRobot;

  private List<Date> holidays;

  @Override public void setUp() throws Exception {
    super.setUp();
    searchRobot = new SearchRobot(mTargetContext);
    mCalendarRobot = new CalendarRobot(mTargetContext);
  }

  @Given("I have selected <country> market") public void selectMarket(String country) {
    for (Market market : Configuration.getInstance().getMarkets()) {
      if (market.getKey().equals(country)) {

        Configuration.getInstance().setCurrentMarket(market);
        break;
      }
    }
    mTestRobot.takeScreenshot(this, "SELECT_COUNTRY");
    mTestRobot.allowPermission(Manifest.permission.ACCESS_FINE_LOCATION);
    mTestRobot.takeScreenshot(this, "SELECT_COUNTRY");
  }

  @When("I am in Calendar screen") public void openCalendar() {
    mTestRobot.takeScreenshot(this, "IN_CALENDAR_SCREEN");
    searchRobot.openCalendarFromReturn();
    mTestRobot.takeScreenshot(this, "IN_CALENDAR_SCREEN");
  }

  @Then("Weekends are marked as holidays") public void weekendsAreMarked() {
    mTestRobot.takeScreenshot(this, "WEEKEND_HOLLIDAYS");
    mCalendarRobot.checkWeekendsAreMarked();
    mTestRobot.takeScreenshot(this, "WEEKEND_HOLLIDAYS");
  }

  @Then("Regional holidays are marked as holiday") public void regionalHolidaysAreMarked() {
    holidays = DateUtils.convertToDate(
        LocalizablesFacade.getString(mTargetContext, OneCMSKeys.CALENDAR_HOLIDAYS_COUNTRY)
            .toString());
    mTestRobot.takeScreenshot(this, "WEEKEND_REGIONAL_HOLLIDAYS");
    mCalendarRobot.checkHolidaysAreMarked(holidays);
    mTestRobot.takeScreenshot(this, "WEEKEND_REGIONAL_HOLLIDAYS");
  }

  @Then("Regional holidays are NOT marked as holiday") public void regionalHolidaysAreNOTMarked() {
    mTestRobot.takeScreenshot(this, "WEEKEND_NOT_REGIONAL_HOLLIDAYS");
    mCalendarRobot.checkRegionalLegendIsNotShown();
    mTestRobot.takeScreenshot(this, "WEEKEND_NOT_REGIONAL_HOLLIDAYS");
  }
}
