package com.odigeo.test.tests.insurances;

import android.content.Intent;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.model.BookingInfoViewModel;
import com.odigeo.data.entity.shoppingCart.AvailableProductsResponse;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.FlowConfigurationResponse;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackerFlowSession;
import com.odigeo.test.robot.InsuranceRobot;
import com.odigeo.test.robot.PaymentRobot;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

import static com.odigeo.test.mock.MocksProvider.providesInsurancesMocks;
import static com.odigeo.test.mock.MocksProvider.providesPaymentMocks;
import static com.odigeo.test.mocks.LegacyModelMockProviders.provideBookingInfoMock;
import static com.odigeo.test.mocks.LegacyModelMockProviders.provideMyShoppingCartMocks;
import static com.odigeo.test.mocks.LegacyModelMockProviders.provideSearchMocks;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.ADD_PRODUCT;

public abstract class OdigeoInsurancesTest extends BaseTest {

  public static final String USER_SELECT_FLIGHT_DETAILS = "user select Flight details";
  public static final String USER_REACHED_FLIGHT_DETAILS_PAGE = "user reached Flight details page";
  public static final String USER_SELECT_NO_INSURANCE_OPTION = "User select no insurance option";
  private static final String INSURANCE_PAGE = "User is in Insurance page";
  private static final String CLICK_INSURANCE = "User select <insurance_type>";
  private static final String CLICK_CONTINUE_BUTTON = "User select the continue";
  private static final String PAYMENT_PAGE_REACHED = "User reached Payment page";
  private static final String INSURANCE_TYPE_CANCELATION = "cancelation";
  private static final String INSURANCE_TYPE_CANCELATION_AND_ASSISTANCE =
      "cancelation and assistance";
  private static final String USER_IS_IN_INSURANCE_PAGE = "user is in Insurance page";
  private static final String ALL_INSURANCE_OPTIONS_ARE_UNSELECTED =
      "all insurance options are unselected";
  private static final String CONTINUE_BUTTON_DISABLE = "continue button is not clickable";
  private Intent parametersIntent;

  private InsuranceRobot mInsuranceRobot;
  private PaymentRobot mPaymentRobot;

  @Given(INSURANCE_PAGE) public void givenInsurancePage() {
    mInsuranceRobot = new InsuranceRobot(mTargetContext);
    mPaymentRobot = new PaymentRobot(mTargetContext);
    SearchTrackerFlowSession.getInstance();
    Intent intent = new Intent();
    CreateShoppingCartResponse createShoppingCartResponse =
        providesPaymentMocks().provideCreateShoppingCartResponseWithoutInsurance();
    AvailableProductsResponse availableProductsResponse =
        providesInsurancesMocks().provideAvailableProductResponse();
    FlowConfigurationResponse flowConfigurationResponse =
        providesInsurancesMocks().provideEmptyFLowConfigurationResponse();
    SearchOptions searchOptions = provideSearchMocks().provideSearchOptions();
    BookingInfoViewModel bookingInfo = provideBookingInfoMock().provideBookingInfo();

    intent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE, createShoppingCartResponse);
    intent.putExtra(Constants.EXTRA_AVAILABLE_PRODUCTS_RESPONSE, availableProductsResponse);
    intent.putExtra(Constants.EXTRA_FLOW_CONFIGURATION_RESPONSE, flowConfigurationResponse);
    intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, searchOptions);
    intent.putExtra(Constants.EXTRA_BOOKING_INFO, bookingInfo);
    getActivityTestRule().launchActivity(intent);
  }

  @When(CLICK_INSURANCE) public void clickInsurance(String insuranceType) {
    switch (insuranceType) {
      case INSURANCE_TYPE_CANCELATION:
        mInsuranceRobot.clickOnInsuranceWidget(
            providesInsurancesMocks().provideCancelationInsurance());
        break;
      case INSURANCE_TYPE_CANCELATION_AND_ASSISTANCE:
        mInsuranceRobot.clickOnInsuranceWidget(
            providesInsurancesMocks().provideCancelationAndAssistanceInsurance());
        break;
    }
  }

  @And(CLICK_CONTINUE_BUTTON) public void clickContinueButton() {
    configureMockServer(ADD_PRODUCT);
    mInsuranceRobot.clickOnContinueButton();
  }

  @Then(PAYMENT_PAGE_REACHED) public void verifyPaymentPage() {
    mPaymentRobot.checkPaymentPageReached();
  }

  @When(ALL_INSURANCE_OPTIONS_ARE_UNSELECTED) public void unselectAnInsuranceOption() {
    mInsuranceRobot.checkCheckBoxIsUnselected(
        providesInsurancesMocks().provideListInsuranceOffer());
  }

  @Then(CONTINUE_BUTTON_DISABLE) public void checkSelectToContinue() {
    mInsuranceRobot.checkEnableContinueButton();
  }

  @When(USER_SELECT_FLIGHT_DETAILS) public void selectFlightDetails() {
    ((OdigeoApp) mTargetContext.getApplicationContext()).getOdigeoSession()
        .setOldMyShoppingCart(provideMyShoppingCartMocks().provideOldMyShoppingCart());
    SearchTrackerFlowSession.getInstance()
        .setSearchTrackHelper(provideSearchMocks().provideSearchTrackHelper());
    mInsuranceRobot.openFlightDetails();
  }

  @Then(USER_REACHED_FLIGHT_DETAILS_PAGE) public void checkFlightDetailsOpen() {
    mInsuranceRobot.checkFlightDetailOpenned();
  }

  @When(USER_SELECT_NO_INSURANCE_OPTION) public void clickInsuranceDecline() {
    mInsuranceRobot.clickOnInsuranceDeclineWidget();
  }
}