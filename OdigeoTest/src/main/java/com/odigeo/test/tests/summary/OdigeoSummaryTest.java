package com.odigeo.test.tests.summary;

import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.OdigeoSession;
import com.odigeo.app.android.lib.activities.OdigeoSummaryActivity;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.model.BookingInfoViewModel;
import com.odigeo.data.entity.shoppingCart.AvailableProductsResponse;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.FlowConfigurationResponse;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackerFlowSession;
import com.odigeo.test.mock.mocks.CollectionOptionsMocks;
import com.odigeo.test.robot.PassengerRobot;
import com.odigeo.test.robot.SummaryRobot;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;
import org.junit.Before;

import static com.odigeo.test.mock.MocksProvider.providesInsurancesMocks;
import static com.odigeo.test.mock.MocksProvider.providesPaymentMocks;
import static com.odigeo.test.mocks.LegacyModelMockProviders.provideBookingInfoMock;
import static com.odigeo.test.mocks.LegacyModelMockProviders.provideSearchMocks;
import static com.odigeo.test.tests.summary.SummaryOldShoppingCartStub.provdeOldShoppingCart;

public abstract class OdigeoSummaryTest extends BaseTest {

  private static final String SUMMARY_PAGE = "User is in summary page";
  private static final String CO2_MESSAGE_DISPLAYED = "CO2 message is displayed";
  private static final String CO2_MESSAGE_IS_CLICKABLE = "User click in CO2 message";
  private static final String CLICK_CONTINUES_BUTTON = "User click in continue button";
  private static final String PASSENGERS_PAGE_IS_DISPLAYED = "User reach Passengers page";

  private SummaryRobot mSummaryRobot;
  private OdigeoApp app;
  private PassengerRobot mPassengerRobot;

  @Before public void setUp() throws Exception {
    super.setUp();
    Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
    app = (OdigeoApp) instrumentation.getTargetContext().getApplicationContext();
  }

  @Given(SUMMARY_PAGE) public void giveSummaryPage() {
    OdigeoSession odigeoSession = app.getOdigeoSession();
    odigeoSession.setOldMyShoppingCart(provdeOldShoppingCart());

    mSummaryRobot = new SummaryRobot(mTargetContext);
    mPassengerRobot = new PassengerRobot(mTargetContext);

    BookingInfoViewModel bookingInfo = provideBookingInfoMock().provideBookingInfo();
    SearchTrackerFlowSession.getInstance().setSearchTrackHelper(null);
    CreateShoppingCartResponse createShoppingCartResponse =
        providesPaymentMocks().provideCreateShoppingCartResponse();
    createShoppingCartResponse.getShoppingCart()
        .setCollectionOptions(CollectionOptionsMocks.provideCollectionOptions());
    SearchOptions searchOptions = provideSearchMocks().provideSearchOptions();
    AvailableProductsResponse availableProductsResponse =
        providesInsurancesMocks().provideAvailableProductResponse();
    FlowConfigurationResponse flowConfigurationResponse =
        providesInsurancesMocks().provideEmptyFLowConfigurationResponse();

    Intent intent = new Intent(mTargetContext, OdigeoSummaryActivity.class);
    intent.putExtra(Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE, createShoppingCartResponse);
    intent.putExtra(Constants.EXTRA_SEARCH_OPTIONS, searchOptions);
    intent.putExtra(Constants.EXTRA_BOOKING_INFO, bookingInfo);
    intent.putExtra(Constants.EXTRA_AVAILABLE_PRODUCTS_RESPONSE, availableProductsResponse);
    intent.putExtra(Constants.EXTRA_FLOW_CONFIGURATION_RESPONSE, flowConfigurationResponse);
    Configuration.getInstance().getCurrentMarket().setHasCo2(true);

    getActivityTestRule().launchActivity(intent);
  }

  @Then(CO2_MESSAGE_DISPLAYED) public void checkCo2MessageIsDisplayed() {
    mSummaryRobot.checkCo2MessageIsDisplayed();
  }

  @Then(CO2_MESSAGE_IS_CLICKABLE) public void checkCo2MessageIsClickable() {
    mSummaryRobot.checkCo2MessageIsClickable();
  }

  @When(CLICK_CONTINUES_BUTTON) public void clickContinuesButton() {
    mSummaryRobot.clickOnContinueButton();
  }

  @Then(PASSENGERS_PAGE_IS_DISPLAYED) public void checkPassengerPageIsDisplayed() {
    mPassengerRobot.checkPassengerPageDisplayed();
  }
}


