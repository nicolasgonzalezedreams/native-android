package com.odigeo.test.tests.importTrip;

import android.content.Intent;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.test.robot.ImportTripRobot;
import com.odigeo.test.robot.MyTripsRobot;
import com.odigeo.test.robot.mockserver.MockServerConfigurator;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

import static com.odigeo.app.android.lib.consts.Constants.EXTRA_MY_TRIPS_WAITING_ANIMATION_ENABLED;
import static com.odigeo.test.mock.MocksProvider.providesBookingMocks;

public abstract class OdigeoImportTripTest extends BaseTest {

  private static final String IMPORT_TRIP_PAGE = "Import Trip Page";
  private static final String USER_IMPORT_A_VALID_TRIP = "User import a valid trip";
  private static final String TRIP_IS_IMPORTED = "Trip is imported";
  private static final String SELECT_TRIP = "user select the trip";
  private static final String INFO_IS_DISPLAYED = "trip information is displayed";
  private static final String PAGE_WITH_ONE_TRIP = "the page My Trips with one imported trip";
  private static final String SELECT_RATE_APP = "user select the button I like it";
  private static final String FEEDBACK_PAGE = "we reached the page leave your feedback";
  private static final String RATE_APP_WIDGET_TITLE_CORRECT =
      "the title of the widget is the proper one";
  private static final String SELECT_HOTEL = "user select hotel";
  private static final String SELECT_CARS = "user select cars";
  private static final String SELECT_HELP = "user select help";

  private static final String HELP_WEBVIEW = "user is in help webview";
  private static final String CARS_WEBVIEW = "user is in cars webview";
  private static final String HOTEL_WEBVIEW = "user is in hotel webview";

  private static final String EMAIL = "carlos.navarrete@globant.com";
  private static final String BOOKING_ID = "ZZZL93";
  private Booking mBooking;

  private MyTripsRobot myTripsRobot;
  private ImportTripRobot importTripRobot;

  @Override public void setUp() throws Exception {
    super.setUp();
    mBooking = providesBookingMocks().provideBooking();
    myTripsRobot = new MyTripsRobot(mTargetContext);
    mTestRobot.cleanDatabase();
  }

  @Given(IMPORT_TRIP_PAGE) public void givenImportTrip() {
    configureMockServer(MockServerConfigurator.IMPORT_SUCCESS);
    getActivityTestRule().launchActivity(new Intent());
    importTripRobot = myTripsRobot.navigateToImportTrip();
  }

  @Given(PAGE_WITH_ONE_TRIP) public void openPageWithOneImportedTrip() {
    AndroidDependencyInjector.getInstance().provideBookingsHandler().saveBooking(mBooking);
    Intent intent = new Intent();
    intent.putExtra(EXTRA_MY_TRIPS_WAITING_ANIMATION_ENABLED, false);
    getActivityTestRule().launchActivity(intent);
  }

  @When(USER_IMPORT_A_VALID_TRIP) public void validTrip() {
    importTripRobot.addEmail(EMAIL).addReservationNumber(BOOKING_ID).addToMyTrips();
  }

  @Then(TRIP_IS_IMPORTED) public void confirmImportedTrip() {
    myTripsRobot.checkImportSuccess();
  }

  @And(SELECT_TRIP) @When(SELECT_TRIP) public void openTripDetails() {
    myTripsRobot.openTripDetails(mBooking);
  }

  @Then(INFO_IS_DISPLAYED) public void verifyImportedTripDetails() {
    myTripsRobot.checkTripDetailsAreDisplayed(mBooking);
  }

  @When(SELECT_RATE_APP) public void selectRateAppOption() {
    myTripsRobot.goToRateApp();
  }

  @Then(FEEDBACK_PAGE) @And(FEEDBACK_PAGE) public void verifyFeedbackPage() {
    myTripsRobot.checkFeedbackPage();
  }

  @Then(RATE_APP_WIDGET_TITLE_CORRECT) public void checkRateAppTitle() {
    myTripsRobot.checkRateAppWidgetTitle();
  }

  @When(SELECT_HOTEL) public void goToHotels() {
    myTripsRobot.openHotels();
  }

  @When(SELECT_CARS) public void goToCars() {
    myTripsRobot.openCars();
  }

  @When(SELECT_HELP) public void goToHelp() {
    myTripsRobot.openHelp();
  }

  @Then({ HOTEL_WEBVIEW, CARS_WEBVIEW, HELP_WEBVIEW }) public void verifyWebView() {
    myTripsRobot.checkWebView();
  }
}
