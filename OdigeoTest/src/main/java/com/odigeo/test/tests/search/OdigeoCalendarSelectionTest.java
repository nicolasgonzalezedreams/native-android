package com.odigeo.test.tests.search;

import android.content.Intent;
import com.odigeo.app.android.lib.activities.OdigeoSearchActivity;
import com.odigeo.test.robot.CalendarRobot;
import com.odigeo.test.robot.SearchRobot;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;
import java.util.Calendar;
import java.util.Date;

public abstract class OdigeoCalendarSelectionTest extends BaseTest {

  //Given
  private static final String USER_IN_SEARCH_RT =
      "user is in Search page searching for a round trip";
  private static final String USER_IN_SEARCH_OW =
      "user is in Search page searching for a one way trip";
  private static final String USER_HAS_SELECTED_DEPARTURE_DATE =
      "user has selected a departure date";
  private static final String IT_IS_FIRST_DAY_OF_THE_MONTH = "it’s the 1st day of the month";

  //When
  private static final String USER_SELECT_DEPARTURE = "select a departure date";
  private static final String USER_SKIP_RETURN_SELECTION = "skip return selection";
  private static final String USER_SELECT_DATES = "user select departure and return dates";
  private static final String USER_SELECT_DEPARTURE_AND_SUBMIT =
      "select a departure date submitting the selection";
  private static final String USER_SELECT_NEW_DEPARTURE_AFTER = "select a new departure date";
  private static final String USER_SELECT_ONE_WAY_TAB = "user switch to one way tab";
  private static final String USER_OPEN_THE_CALENDAR = "user open the calendar";

  //Then
  private static final String HAS_SELECTED_ONE_WAY_TRIP = "user should have selected One Way trip";
  private static final String DEPARTURE_IS_THE_SELECTED =
      "the departure date should be the selected one";
  private static final String DEPARTURE_IS_THE_LATEST_SELECTED =
      "the departure date should be the latest selected one";
  private static final String RETURN_IS_EMPTY = "return date should be empty";
  private static final String DEPARTURE_AND_RETURN_OK = "the dates should be the selected ones";
  private static final String PREVIOUS_MONTH_NOT_VISIBLE = "previous month is not visible";

  private static final String DEPARTURE_DATE = "tomorrow";
  private static final String RETURN_DATE = "day after tomorrow";
  private static final String DEPARTURE_DATE_AFTER = "two days after tomorrow";

  private CalendarRobot mCalendarRobot;
  private SearchRobot mSearchRobot;

  private Date selectedDepartureDate;
  private Date selectedDepartureDateAfter;
  private Date selectedReturnDate;
  private Date firstDayOfMonth;

  @Override public void setUp() throws Exception {
    super.setUp();
    mCalendarRobot = new CalendarRobot(mTargetContext);
    mSearchRobot = new SearchRobot(mTargetContext);
    populateDates();
  }

  @Given(USER_IN_SEARCH_RT) public void openSearchAndSelectRTTab() {
    getActivityTestRule().launchActivity(new Intent(mTargetContext, OdigeoSearchActivity.class));
    mSearchRobot.takeScreenshot(this, "USER_IN_SEARCH_RT");
    mSearchRobot.selectRoundTripTab();
    mSearchRobot.takeScreenshot(this, "USER_IN_SEARCH_RT");
  }

  @Given(USER_IN_SEARCH_OW) public void openSearchAndSelectOWTab() {
    getActivityTestRule().launchActivity(new Intent(mTargetContext, OdigeoSearchActivity.class));
    mSearchRobot.takeScreenshot(this, "USER_IN_SEARCH_OW");
    mSearchRobot.selectOneWayTripTab();
    mSearchRobot.takeScreenshot(this, "USER_IN_SEARCH_OW");
  }

  @Given(IT_IS_FIRST_DAY_OF_THE_MONTH) public void openCalendarAndSelectDayOne() {
    getActivityTestRule().launchActivity(new Intent(mTargetContext, OdigeoSearchActivity.class));
    mSearchRobot.takeScreenshot(this, "IT_IS_FIRST_DAY_OF_THE_MONTH");
    mSearchRobot.selectOneWayTripTab();
    mSearchRobot.takeScreenshot(this, "IT_IS_FIRST_DAY_OF_THE_MONTH");
    mSearchRobot.openCalendar();
    mSearchRobot.takeScreenshot(this, "IT_IS_FIRST_DAY_OF_THE_MONTH");
    mCalendarRobot.selectDate(firstDayOfMonth);
    mSearchRobot.takeScreenshot(this, "IT_IS_FIRST_DAY_OF_THE_MONTH");
    mCalendarRobot.submitDates();
    mSearchRobot.takeScreenshot(this, "IT_IS_FIRST_DAY_OF_THE_MONTH");
  }

  @When(USER_SELECT_DATES) @And(USER_SELECT_DATES) public void selectDepartureAndReturnDates() {
    mSearchRobot.openCalendar();
    mSearchRobot.takeScreenshot(this, "USER_SELECT_DATES");
    mCalendarRobot.selectDepartureAndReturnDate(selectedDepartureDate, selectedReturnDate);
    mSearchRobot.takeScreenshot(this, "USER_SELECT_DATES");
    mCalendarRobot.submitDates();
    mSearchRobot.takeScreenshot(this, "USER_SELECT_DATES");
  }

  @Given(USER_HAS_SELECTED_DEPARTURE_DATE) @And(USER_HAS_SELECTED_DEPARTURE_DATE)
  public void preSelectDepartureDate() {
    selectDepartureDate(selectedDepartureDate);
    mSearchRobot.takeScreenshot(this, "USER_HAS_SELECTED_DEPARTURE_DATE");
  }

  @When(USER_SELECT_NEW_DEPARTURE_AFTER) public void selectNewDepartureDate() {
    selectDepartureDate(selectedDepartureDateAfter);
    mSearchRobot.takeScreenshot(this, "USER_SELECT_NEW_DEPARTURE_AFTER");
  }

  private void selectDepartureDate(Date date) {
    mSearchRobot.openCalendar();
    mCalendarRobot.selectDate(date);
    mCalendarRobot.submitDates();
  }

  @And(USER_SELECT_ONE_WAY_TAB) @When(USER_SELECT_ONE_WAY_TAB) public void selectOneWayTrip() {
    mSearchRobot.selectOneWayTripTab();
    mSearchRobot.takeScreenshot(this, "USER_SELECT_ONE_WAY_TAB");
  }

  @When(USER_SELECT_DEPARTURE) @And(USER_SELECT_DEPARTURE) public void selectDeparture() {
    mSearchRobot.openCalendar();
    mSearchRobot.takeScreenshot(this, "USER_SELECT_DEPARTURE");
    mCalendarRobot.selectDate(selectedDepartureDate);
    mSearchRobot.takeScreenshot(this, "USER_SELECT_DEPARTURE");
  }

  @When(USER_SELECT_DEPARTURE_AND_SUBMIT) @And(USER_SELECT_DEPARTURE_AND_SUBMIT)
  public void selectDepartureAndSubmit() {
    mSearchRobot.takeScreenshot(this, "USER_SELECT_DEPARTURE_AND_SUBMIT");
    mSearchRobot.openCalendar();
    mSearchRobot.takeScreenshot(this, "USER_SELECT_DEPARTURE_AND_SUBMIT");
    mCalendarRobot.selectDate(selectedDepartureDate);
    mSearchRobot.takeScreenshot(this, "USER_SELECT_DEPARTURE_AND_SUBMIT");
    mCalendarRobot.submitDates();
    mSearchRobot.takeScreenshot(this, "USER_SELECT_DEPARTURE_AND_SUBMIT");
  }

  @And(USER_SKIP_RETURN_SELECTION) public void skipReturnSelection() {
    mCalendarRobot.skipReturnSelection();
    mSearchRobot.takeScreenshot(this, "USER_SKIP_RETURN_SELECTION");
  }

  @Then(HAS_SELECTED_ONE_WAY_TRIP) public void isOneWayTripSelected() {
    mSearchRobot.checkIsOneWayTrip();
    mSearchRobot.takeScreenshot(this, "HAS_SELECTED_ONE_WAY_TRIP");
  }

  @And(DEPARTURE_IS_THE_SELECTED) @Then(DEPARTURE_IS_THE_SELECTED)
  public void isDepartureDateSelectedCorrect() {
    mSearchRobot.isDepartureDateSelectedCorrect(selectedDepartureDate);
    mSearchRobot.takeScreenshot(this, "DEPARTURE_IS_THE_SELECTED");
  }

  @Then(DEPARTURE_IS_THE_LATEST_SELECTED) public void isDepartureDateSelectedTheLatest() {
    mSearchRobot.isDepartureDateSelectedCorrect(selectedDepartureDateAfter);
    mSearchRobot.takeScreenshot(this, "DEPARTURE_IS_THE_LATEST_SELECTED");
  }

  @And(RETURN_IS_EMPTY) @Then(RETURN_IS_EMPTY) public void isReturnDateEmpty() {
    mSearchRobot.isReturnDateSelectedEmpty();
    mSearchRobot.takeScreenshot(this, "DEPARTURE_IS_THE_LATEST_SELECTED");
  }

  @Then(DEPARTURE_AND_RETURN_OK) public void departureAndReturnAreOk() {
    mSearchRobot.takeScreenshot(this, "DEPARTURE_AND_RETURN_OK");
    mSearchRobot.isDepartureDateSelectedCorrect(selectedDepartureDate);
    mSearchRobot.takeScreenshot(this, "DEPARTURE_AND_RETURN_OK");
    mSearchRobot.isReturnDateSelectedCorrect(selectedReturnDate);
    mSearchRobot.takeScreenshot(this, "DEPARTURE_AND_RETURN_OK");
  }

  @When(USER_OPEN_THE_CALENDAR) public void openCalendar() {

    mSearchRobot.takeScreenshot(this, "USER_OPEN_THE_CALENDAR");
    mSearchRobot.openCalendar();
    mSearchRobot.takeScreenshot(this, "USER_OPEN_THE_CALENDAR");
  }

  @Then(PREVIOUS_MONTH_NOT_VISIBLE) public void previousMonthIsNotShown() {
    mSearchRobot.takeScreenshot(this, "PREVIOUS_MONTH_NOT_VISIBLE");
    mCalendarRobot.isPreviousMonthShown(mTargetContext, firstDayOfMonth);
    mSearchRobot.takeScreenshot(this, "PREVIOUS_MONTH_NOT_VISIBLE");
  }

  private void populateDates() {
    selectedDepartureDate = getSelectedDate(DEPARTURE_DATE);
    selectedReturnDate = getSelectedDate(RETURN_DATE);
    selectedDepartureDateAfter = getSelectedDate(DEPARTURE_DATE_AFTER);
    firstDayOfMonth = getFirstDayNextMonth();
  }

  private Date getSelectedDate(String date) {
    Calendar calendar = Calendar.getInstance();

    if (date.equals(DEPARTURE_DATE)) {
      calendar.add(Calendar.DAY_OF_MONTH, 1);
    } else if (date.equals(RETURN_DATE)) {
      calendar.add(Calendar.DAY_OF_MONTH, 2);
    } else if (date.equals(DEPARTURE_DATE_AFTER)) {
      calendar.add(Calendar.DAY_OF_MONTH, 3);
    }

    return calendar.getTime();
  }

  private Date getFirstDayNextMonth() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MONTH, 1);
    calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));

    return calendar.getTime();
  }
}
