package com.odigeo.test.tests.payment;

import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.test.robot.PaymentRobot;
import com.odigeo.test.robot.RobotUtils;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

public abstract class OdigeoPaymentPurchaseTest extends BasePaymentTest {

  private static final String PAYMENT_PAGE = "We are in the Payment page";
  private static final String TAP_TAC_CB = "User Tap in Terms And Conditions Checkbox";
  private static final String USER_TAPS_TAC = "User taps on T&C";
  private static final String SELECT_AIRLINE_TAC = "Selects airline T&C";
  private static final String TAC_CB_IS_CHECKED = "The Terms And Conditions checkbox is checked";
  private static final String AIRLINE_TAC_OPENS = "Airline T&C document opens";
  private PaymentRobot mPaymentRobot;

  @Given(PAYMENT_PAGE) public void givenPaymentPage() {
    mPaymentRobot = new PaymentRobot(mTargetContext);
    Configuration.getInstance().getCurrentMarket().setNeedsExplicitAcceptance(true);
    launchPaymentActivity();
  }

  @When(TAP_TAC_CB) public void tapOnTACCheckBox() {
    mPaymentRobot.takeScreenshot(this, "TAP_TAC_CB");
    mPaymentRobot.tapOnExplicitTACCheckbox();
    mPaymentRobot.takeScreenshot(this, "TAP_TAC_CB");
  }

  @When(USER_TAPS_TAC) public void tapOnTAC() {

    mPaymentRobot.takeScreenshot(this, "USER_TAPS_TAC");
    mPaymentRobot.tapOnTAC();
    mPaymentRobot.takeScreenshot(this, "USER_TAPS_TAC");
  }

  @And(SELECT_AIRLINE_TAC) public void selectAirlineTAC() {

    mPaymentRobot.takeScreenshot(this, "SELECT_AIRLINE_TAC");
    mPaymentRobot.tapOnAirlinesTAC();
    mPaymentRobot.takeScreenshot(this, "SELECT_AIRLINE_TAC");
  }

  @Then(TAC_CB_IS_CHECKED) public void TermsAndConditionsIsChecked() {
    mPaymentRobot.takeScreenshot(this, "TAC_CB_IS_CHECKED");
    mPaymentRobot.checkExplicitTACAcceptanceIsChecked();
    mPaymentRobot.takeScreenshot(this, "TAC_CB_IS_CHECKED");
  }

  @Then(AIRLINE_TAC_OPENS) public void airlineTACOpens() {
    RobotUtils.waitTime(500);
    mPaymentRobot.takeScreenshot(this, "TAC_CB_IS_CHECKED");
    mPaymentRobot.checkOdigeoWebActivityReached();
    mPaymentRobot.takeScreenshot(this, "TAC_CB_IS_CHECKED");
  }
}