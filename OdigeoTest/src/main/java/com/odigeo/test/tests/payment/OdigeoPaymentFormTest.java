package com.odigeo.test.tests.payment;

import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.test.robot.PaymentRobot;
import com.odigeo.test.robot.mockserver.MockServerConfigurator;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

public abstract class OdigeoPaymentFormTest extends BasePaymentTest {

  private static final String PAYPAL = "paypal";
  private static final String BANKTRANSFER = "banktransfer";
  private static final String PAYMENT_PAGE = "We are in the Payment page";
  private static final String ADD_CREDIT_CARD_MONTH = "Adds <cc_date_month>";
  private static final String ADD_CREDIT_CARD_YEAR = "Adds <cc_date_year>";
  private static final String ADD_CREDIT_CARD_CVV = "Adds <cvv>";
  private static final String ADD_CREDIT_CARD_NUMBER = "User Adds <creditcard>";
  private static final String ADD_CREDIT_CARD_NAME = "Adds <cc_name>";
  private static final String CONTINUE_BUTTON_IS_NOT_ENABLE = "Continue button is not clickable";
  private static final String CONTINUE_BUTTON_IS_ENABLE = "Continue button is clickable";
  private static final String SELECT_PAYMENT_METHOD = "User Selects <payment_method>";
  private PaymentRobot mPaymentRobot;

  @Given(PAYMENT_PAGE) public void givePaymentPage() {
    mPaymentRobot = new PaymentRobot(mTargetContext);
    Configuration.getInstance().getCurrentMarket().setNeedsExplicitAcceptance(false);
    launchPaymentActivity();
  }

  @When(ADD_CREDIT_CARD_NUMBER) public void setAddCreditCardNumber(String creditcard) {
    configureMockServer(MockServerConfigurator.BIN_CHECK_VALIDATION_SUCCESSFUL);
    mPaymentRobot.addCreditcard(creditcard);
  }

  @When(SELECT_PAYMENT_METHOD) public void setSelectPaymentMethod(String payment_method) {
    switch (payment_method) {
      case PAYPAL:
        mPaymentRobot.clickAndCheckOnPaypalPaymentMethod();
        break;
      case BANKTRANSFER:
        mPaymentRobot.clickAndCheckOnBankTransferPaymentMethod();
        break;
      default:
        break;
    }
  }

  @And(ADD_CREDIT_CARD_NAME) public void setAddCreditCardName(String cc_name) {
    mPaymentRobot.addCreditcardName(cc_name);
  }

  @And(ADD_CREDIT_CARD_MONTH) public void setAddCreditCardMonth(String cc_date_month) {
    mPaymentRobot.addCreditcardExpirationMonth(cc_date_month);
  }

  @And(ADD_CREDIT_CARD_YEAR) public void setAddCreditYear(String cc_date_year) {
    mPaymentRobot.addCreditcardExpirationYear(cc_date_year);
  }

  @And(ADD_CREDIT_CARD_CVV) public void setAddCreditCardCvv(String cvv) {
    mPaymentRobot.addCVV(cvv);
  }

  @Then(CONTINUE_BUTTON_IS_NOT_ENABLE) public void checkContinueButtonIsNotEnable() {
    mPaymentRobot.checkPaymentButtonIsNotAvailable();
  }

  @Then(CONTINUE_BUTTON_IS_ENABLE) public void checkContinueButtonIsEnable() {
    mPaymentRobot.checkPaymentButtonIsAvailable();
  }
}