package com.odigeo.test.tests.confirmation;

import android.content.Intent;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.mappers.StoredSearchOptionsMapper;
import com.odigeo.app.android.lib.models.OldMyShoppingCart;
import com.odigeo.app.android.lib.models.Section;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.BuyerDTO;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.LocationDTO;
import com.odigeo.app.android.lib.models.dto.MoneyDTO;
import com.odigeo.app.android.lib.models.dto.SectionDTO;
import com.odigeo.app.android.lib.models.dto.SegmentDTO;
import com.odigeo.app.android.model.BookingInfoViewModel;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.shoppingCart.BookingDetail;
import com.odigeo.data.entity.shoppingCart.BookingStatus;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionOption;
import com.odigeo.data.entity.shoppingCart.ShoppingCartCollectionState;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackHelper;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackerFlowSession;
import com.odigeo.test.mock.mocks.CollectionOptionsMocks;
import com.odigeo.test.robot.ConfirmationRobot;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.odigeo.app.android.lib.consts.Constants.EXTRA_AVAILABLE_PRODUCTS_RESPONSE;
import static com.odigeo.app.android.lib.consts.Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE;
import static com.odigeo.app.android.lib.consts.Constants.EXTRA_SEARCH_OPTIONS;
import static com.odigeo.test.mock.MocksProvider.providesAvailableProductsMocks;
import static com.odigeo.test.mock.MocksProvider.providesPaymentMocks;
import static com.odigeo.test.mock.MocksProvider.providesStoredSearchMocks;
import static com.odigeo.test.mocks.LegacyModelMockProviders.provideBookingInfoMock;

public abstract class OdigeoConfirmationTest extends BaseTest {

  private static final String USER_DID_ENTERED_NOT_TRUSTFUL_DETAILS =
      "user did entered not trustful details";
  private static final String TRANSACTION_IS_IN_PENDING_STATUS = "transaction is in pending status";
  private static final String USER_DID_ENTERED_INVALID_PAYMENT_DATA =
      "user did entered invalid payment data";
  private static final String TRANSACTION_WAS_REJECTED = "transaction was rejected";
  private static final String USER_DID_ENTERED_VALID_PAYMENT_DETAILS =
      "user did entered valid payment details";
  private static final String USER_IS_IN_CONFIRMATION_PAGE = "user is in confirmation page";
  private static final String TRANSACTION_HAS_BEEN_SUCCESSFULL = "transaction has been successfull";
  private static final String GROUND_TRANSPORTATION_WIDGET_SHOWN =
      "ground transportation widget is shown";

  private static final String ONECMS_KEY_GROUNDTRANSPORTATION_VALUE =
      "onecms key of ground transportation value is";
  private static final String USER_IS_ON_CONFIRMATION_PAGE_WITH_STATUS =
      "user is on confirmation page with booking status <bookingStatus>";
  private static final String GROUNDTRANSPORTATION_IS_SHOWN =
      "ground transportation widget <should> appear";

  private ConfirmationRobot confirmationRobot;
  private Intent parametersIntent;
  private StoredSearchOptionsMapper storedSearchOptionsMapper;

  @Override public void setUp() throws Exception {
    super.setUp();
    parametersIntent = new Intent();
    confirmationRobot = new ConfirmationRobot(mTargetContext);
    storedSearchOptionsMapper =
        AndroidDependencyInjector.getInstance().provideStoredSearchOptionsMapper();
  }

  @Given(USER_DID_ENTERED_VALID_PAYMENT_DETAILS) public void givenValidDetails() {
    createMocks();
    putExtraBookingDetail(BookingStatus.CONTRACT);
  }

  @Given(USER_DID_ENTERED_NOT_TRUSTFUL_DETAILS) public void givenUntrustfullDetails() {
    createMocks();
    putExtraBookingDetail(BookingStatus.PENDING);
  }

  @Given(USER_DID_ENTERED_INVALID_PAYMENT_DATA) public void givenInvalidDetails() {
    createMocks();
    putExtraBookingDetail(BookingStatus.REJECTED);
  }

  @Given(ONECMS_KEY_GROUNDTRANSPORTATION_VALUE) public void givenKeyGroundTransportation() {
    createMocks();
  }

  @When(USER_IS_IN_CONFIRMATION_PAGE) public void userOpensConfirmationPage() {
    getActivityTestRule().launchActivity(parametersIntent);
  }

  @When(USER_IS_ON_CONFIRMATION_PAGE_WITH_STATUS)
  public void userIsInConformationPage(String bookingStatus) {
    BookingStatus status = null;

    try {
      status = BookingStatus.fromValue(bookingStatus);
    } catch (IllegalArgumentException e) {
      throw new RuntimeException("Please, review the booking status " + bookingStatus);
    }

    if (status == BookingStatus.PENDING) {
      parametersIntent.putExtra(Constants.EXTRA_COLLECTION_STATE,
          ShoppingCartCollectionState.PARTIALLY_COLLECTED.value());
    }

    putExtraBookingDetail(status);

    parametersIntent.putExtra(Constants.EXTRA_BOOKING_STATUS, status);
    getActivityTestRule().launchActivity(parametersIntent);
  }

  private void putExtraBookingDetail(BookingStatus status) {
    BookingDetail bookingDetail = new BookingDetail();
    bookingDetail.setBookingStatus(status);
    parametersIntent.putExtra(Constants.EXTRA_BOOKING_DETAIL, bookingDetail);
  }

  @Then(TRANSACTION_HAS_BEEN_SUCCESSFULL) @And(TRANSACTION_HAS_BEEN_SUCCESSFULL)
  public void succesfulTransaction() {
    confirmationRobot.checkConfirmationSuccess();
  }

  @Then(TRANSACTION_IS_IN_PENDING_STATUS) public void checkTransactionPending() {
    confirmationRobot.checkConfirmationPending();
  }

  @Then(TRANSACTION_WAS_REJECTED) public void checkTransactionRejected() {
    confirmationRobot.checkConfirmationRejected();
  }

  @Then(GROUND_TRANSPORTATION_WIDGET_SHOWN) public void groundTransportationWidgetIsShown() {
    confirmationRobot.checkGroundTransportationWidgetIsShown(true);
  }

  @Then(GROUNDTRANSPORTATION_IS_SHOWN) public void groundTransportationVisibility(String should) {
    confirmationRobot.checkGroundTransportationWidgetIsShown(Boolean.parseBoolean(should));
  }

  private void createMocks() {
    SearchOptions searchOptions = storedSearchOptionsMapper.storedSearchToSearchOptionMapper(
        providesStoredSearchMocks().provideStoredSearchWithSegment());
    searchOptions.getFlightSegments().get(0).setSections(provideListSections());
    CreateShoppingCartResponse createShoppingCartResponse =
        providesPaymentMocks().provideCreateShoppingCartResponseWithoutInsurance();
    ShoppingCartCollectionOption shoppingCartCollectionOption =
        CollectionOptionsMocks.provideCollectionOptions().get(0);
    Booking booking = providesPaymentMocks().provideBooking();
    BookingInfoViewModel bookingInfo = provideBookingInfoMock().provideBookingInfo();

    parametersIntent.putExtra(EXTRA_SEARCH_OPTIONS, searchOptions);
    parametersIntent.putExtra(EXTRA_CREATE_SHOPPING_CART_RESPONSE, createShoppingCartResponse);
    parametersIntent.putExtra(EXTRA_AVAILABLE_PRODUCTS_RESPONSE,
        providesAvailableProductsMocks().provideAvailableProducts());
    parametersIntent.putExtra(Constants.EXTRA_COLLECTION_OPTION_SELECTED,
        shoppingCartCollectionOption);
    parametersIntent.putExtra(Constants.EXTRA_BOOKING, booking);
    parametersIntent.putExtra(Constants.EXTRA_BOOKING_INFO, bookingInfo);

    ((OdigeoApp) mTargetContext.getApplicationContext()).getOdigeoSession()
        .setOldMyShoppingCart(provideMockOldMyShoppingCart());

    SearchTrackerFlowSession.getInstance().setSearchTrackHelper(provideSearchTrackHelper());
  }

  private List<Section> provideListSections() {
    List<Section> sectionList = new ArrayList<>();
    Section section = new Section();
    section.setArrivalDate(1490802900955L);
    section.setDepartureDate(1490790600955L);
    section.setCarrier("1");
    section.setId("642");
    section.setOperatingCarrier(null);
    section.setLocationFrom(provideLocationFrom());
    section.setLocationTo(provideLocationTo());
    sectionList.add(section);
    return sectionList;
  }

  private LocationDTO provideLocationTo() {
    LocationDTO locationDTOTo = new LocationDTO();
    locationDTOTo.setCityIataCode("PAR");
    locationDTOTo.setIataCode("PAR");
    return locationDTOTo;
  }

  private LocationDTO provideLocationFrom() {
    LocationDTO locationDTOFrom = new LocationDTO();
    locationDTOFrom.setCityIataCode("MAD");
    locationDTOFrom.setIataCode("MAD");
    return locationDTOFrom;
  }

  private SearchTrackHelper provideSearchTrackHelper() {
    SearchTrackHelper searchTrackHelper = new SearchTrackHelper();
    searchTrackHelper.setAdults(1);
    searchTrackHelper.setInfants(1);
    searchTrackHelper.setKids(1);
    searchTrackHelper.setPrice(0.0);
    searchTrackHelper.addAirline("Delta");
    searchTrackHelper.addSegment("departure", "arrival", 0, "depCountry", "arrCountry");
    return searchTrackHelper;
  }

  private OldMyShoppingCart provideMockOldMyShoppingCart() {
    OldMyShoppingCart myCart = new OldMyShoppingCart();

    MoneyDTO moneyDTO = new MoneyDTO();
    moneyDTO.setCurrency("USD");
    myCart.setPrice(moneyDTO);

    myCart.setBuyer(new BuyerDTO());
    myCart.setTotalPrice(new BigDecimal(0));
    myCart.setLocale("en_US");
    return myCart;
  }

  private List<SegmentWrapper> provideSegmentWrapperList() {
    List<SegmentWrapper> segmentWrapperList = new ArrayList<>();
    SegmentWrapper segmentWrapper = new SegmentWrapper();
    segmentWrapper.setKey("0,TO3141");
    CarrierDTO carrier = new CarrierDTO();
    carrier.setCode("TO");
    carrier.setId(0);
    carrier.setName("Transavia France");
    segmentWrapper.setCarrier(carrier);
    segmentWrapper.setSectionsObjects(provideListSectionsDTO());
    segmentWrapper.setSegment(provideSegmentDTO());
    segmentWrapperList.add(segmentWrapper);
    return segmentWrapperList;
  }

  private SegmentDTO provideSegmentDTO() {
    SegmentDTO segmentDTO = new SegmentDTO();
    segmentDTO.setDuration(125L);
    segmentDTO.setSeats(null);
    return segmentDTO;
  }

  private List<SectionDTO> provideListSectionsDTO() {
    List<SectionDTO> sectionDTOList = new ArrayList<>();
    SectionDTO sectionDTO = new SectionDTO();
    sectionDTO.setDepartureDate(1490779200500L);
    sectionDTO.setDuration(125L);
    sectionDTO.setLocationTo(provideLocationTo());
    sectionDTO.setLocationFrom(provideLocationFrom());
    sectionDTOList.add(sectionDTO);
    return sectionDTOList;
  }
}