package com.odigeo.test.tests.passengers;

import android.content.Intent;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.app.android.lib.mappers.StoredSearchOptionsMapper;
import com.odigeo.app.android.model.BookingInfoViewModel;
import com.odigeo.data.db.helper.UserCreateOrUpdateHandlerInterface;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackerFlowSession;
import com.odigeo.test.robot.ExpirationDialogRobot;
import com.odigeo.test.robot.FrequentFlierRobot;
import com.odigeo.test.robot.InsuranceRobot;
import com.odigeo.test.robot.PassengerRobot;
import com.odigeo.test.robot.SummaryRobot;
import com.odigeo.test.robot.mockserver.MockServerConfigurator;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

import static com.odigeo.app.android.lib.consts.Constants.EXTRA_AVAILABLE_PRODUCTS_RESPONSE;
import static com.odigeo.app.android.lib.consts.Constants.EXTRA_BOOKING_INFO;
import static com.odigeo.app.android.lib.consts.Constants.EXTRA_CREATE_SHOPPING_CART_RESPONSE;
import static com.odigeo.app.android.lib.consts.Constants.EXTRA_SEARCH_OPTIONS;
import static com.odigeo.test.mock.MocksProvider.providesAvailableProductsMocks;
import static com.odigeo.test.mock.MocksProvider.providesPassengerMocks;
import static com.odigeo.test.mock.MocksProvider.providesPaymentMocks;
import static com.odigeo.test.mock.MocksProvider.providesStoredSearchMocks;
import static com.odigeo.test.mocks.LegacyModelMockProviders.provideBookingInfoMock;
import static com.odigeo.test.mocks.LegacyModelMockProviders.provideMyShoppingCartMocks;
import static com.odigeo.test.mocks.LegacyModelMockProviders.provideSearchMocks;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.ADD_PASSENGER;

public abstract class OdigeoPassengerTest extends BaseTest {

  private static final String USER_CONTINUES_WITH_THE_BOOKING_FLOW =
      "user continues with the booking flow";
  private static final String DUPLICATED_BOOKING_MESSAGE_APPEARS =
      "duplicated booking message appears";
  private static final String PASSENGER_PAGE = "user is in Passenger page";
  private static final String NORMAL_FILL = "User have saved a complete passenger in MyData";
  private static final String SELECT_FLIGHT_DETAILS = "user select flight details button";
  private static final String FLIGHT_DETAILS_PAGE = "user is in Flight details page";
  private static final String PASSENGER_FILLED = "Passenger information is filled";
  private static final String CONTINUE = "user select the Continue button";
  private static final String INCOMPLETE_FORM = "alert message appears";
  private static final String TIMEOUT_EXPIRED = "Timeout has expired";
  private static final String TIMECOUNTER_POPUP = "TimeCounter popup appears";
  private static final String ASSURANCE_PAGE_REACHED = "we reached the Assurance page";
  private static final String FIELDS_DISPLAYED = "Name and Surname fields <FieldStatus> present";
  private static final String USER_SELECT_FREQUENT_FLIER_INFORMATION =
      "User select frequent flier information";
  private static final String IN_FREQUENT_FLIER_INFORMATION_PAGE =
      "User is in frequent flier information page";
  private static final String FILL_FORM = "fields are filled with the following data: "
      + "<titleKey> title, <name> name, <surname> surname, "
      + "<address> address, <city> city, <postcode> postcode, <countryOfResidence> country of residence, "
      + "<countryCode> country code, <phoneNumber> phone number, <email> email";
  private static final int TIMEOUT_SECONDS = 2;
  private final String FILL_FORM_SAME_DATA =
      "fields are filled with the same data as a booking stored in the " + "device";
  private PassengerRobot passengerRobot;
  private SummaryRobot summaryRobot;
  private InsuranceRobot insuranceRobot;
  private FrequentFlierRobot frequentFlierRobot;

  private SessionController mSessionController;
  private UserCreateOrUpdateHandlerInterface mUserCreateOrUpdateHandler;
  private ExpirationDialogRobot mExpirationDialogRobot;
  private Intent parametersIntent;
  private UserTraveller mUserTraveller;
  private StoredSearchOptionsMapper storedSearchOptionsMapper;

  @Override public void setUp() throws Exception {

    super.setUp();
    passengerRobot = new PassengerRobot(mTargetContext);
    parametersIntent = new Intent();
    mUserCreateOrUpdateHandler =
        AndroidDependencyInjector.getInstance().provideUserCreateOrUpdateDBHandler();
    mSessionController =
        AndroidDependencyInjector.getInstance().provideSessionController();
    storedSearchOptionsMapper =
        AndroidDependencyInjector.getInstance().provideStoredSearchOptionsMapper();
    mSessionController.removeAllData();
    mTestRobot.cleanDatabase();
  }

  @Override public void tearDown() throws Exception {

    super.tearDown();
    mSessionController.removeAllData();
  }

  @Given(PASSENGER_PAGE) public void givenPassengerPage() {

    SearchOptions searchOptions = storedSearchOptionsMapper.storedSearchToSearchOptionMapper(
        providesStoredSearchMocks().provideStoredSearchWithSegment());
    BookingInfoViewModel bookingInfo = provideBookingInfoMock().provideBookingInfo();

    parametersIntent.putExtra(EXTRA_SEARCH_OPTIONS, searchOptions);
    parametersIntent.putExtra(EXTRA_CREATE_SHOPPING_CART_RESPONSE,
        providesPaymentMocks().provideCreateShoppingCartResponse());
    parametersIntent.putExtra(EXTRA_AVAILABLE_PRODUCTS_RESPONSE,
        providesAvailableProductsMocks().provideAvailableProducts());
    parametersIntent.putExtra(EXTRA_BOOKING_INFO, bookingInfo);
  }

  @When(NORMAL_FILL) public void fillPassenger() {

    mUserTraveller = providesPassengerMocks().provideNonEmptyUserTravellerListMock().get(1);
    mUserCreateOrUpdateHandler.saveAUserTravellerAndAllComponentsInDBWithoutUserLogged(
        mUserTraveller);

    rule.launchActivity(parametersIntent);
  }

  @Then(PASSENGER_FILLED) public void checkPassenger() {

    passengerRobot.verifyFilledData(mUserTraveller);
  }

  @When(FILL_FORM)
  public void fillPassengerDataForm(String title, String name, String surname, String address,
      String city, String postcode, String countryOfResidence, String countryCode,
      String phoneNumber, String email) {

    rule.launchActivity(parametersIntent);
    passengerRobot.selectTitle(title)
        .writeName(name)
        .writeSurname(surname)
        .writeAddress(address)
        .writeCityName(city)
        .writePostalCode(postcode)
        .setCountryCode(countryOfResidence)
        .setPhoneCode(countryCode)
        .writePhoneNumber(phoneNumber)
        .writeEmail(email);
  }

  @And(CONTINUE) public void tryToGetNextPage() {

    configureMockServer(ADD_PASSENGER);
    insuranceRobot = passengerRobot.goForward();
  }

  @Then(INCOMPLETE_FORM) public void verifyIncompleteFormMessage() {

    passengerRobot.checkIncompleteFieldMsg();
  }

  @When(TIMEOUT_EXPIRED) public void setTimeoutExpired() {

    mExpirationDialogRobot = new ExpirationDialogRobot(mTargetContext);
    parametersIntent.putExtra(Constants.TIMEOUT_WIDGET_PREVIOUS_TIME, TIMEOUT_SECONDS);
    rule.launchActivity(parametersIntent);
  }

  @Then(TIMECOUNTER_POPUP) public void verifyTimeoutMessage() {

    mExpirationDialogRobot.checkTimeExpirationDialogIsDisplayed();
  }

  @Then(ASSURANCE_PAGE_REACHED) public void verifyAssurancePage() {

    insuranceRobot.checkInsurancePageReached();
  }

  @Then(FIELDS_DISPLAYED) public void verifyFieldsAreDisplayed(String displayed) {

    passengerRobot.checkFieldsDisplayed(displayed);
  }

  @When(USER_SELECT_FREQUENT_FLIER_INFORMATION) public void selectFrequentFlierCode() {

    rule.launchActivity(parametersIntent);
    frequentFlierRobot = passengerRobot.openFrequentFlierCode();
  }

  @Then(IN_FREQUENT_FLIER_INFORMATION_PAGE) public void checkFrequentFlierPage() {

    frequentFlierRobot.checkFrequentFlier();
  }

  @When(FILL_FORM_SAME_DATA) public void openPassengerPage() {

    rule.launchActivity(parametersIntent);
    passengerRobot.writeName("Mock Name")
        .writeSurname("Mock Surname")
        .writeAddress("addr")
        .writeCityName("city")
        .writePostalCode("1212")
        .setCountryCode("AR")
        .setPhoneCode("AR +54")
        .writePhoneNumber("123456789")
        .writeEmail("eleazarsp@gmail.com");
  }

  @And(USER_CONTINUES_WITH_THE_BOOKING_FLOW) public void searchSameFlightBooked() {

    configureMockServer(MockServerConfigurator.ADD_PASSENGER_DUPLICATED_BOOKING);
    passengerRobot.goForward();
  }

  @Then(DUPLICATED_BOOKING_MESSAGE_APPEARS) public void verifyDuplicatedBookingMessage() {

    passengerRobot.checkDuplicatedBooking();
  }

  @When(SELECT_FLIGHT_DETAILS) public void goFlightDetails() {

    ((OdigeoApp) mTargetContext.getApplicationContext()).getOdigeoSession()
        .setOldMyShoppingCart(provideMyShoppingCartMocks().provideOldMyShoppingCart());
    SearchTrackerFlowSession.getInstance()
        .setSearchTrackHelper(provideSearchMocks().provideSearchTrackHelper());

    rule.launchActivity(parametersIntent);
    summaryRobot = passengerRobot.goFlightDetails();
  }

  @Then(FLIGHT_DETAILS_PAGE) public void verifyFlightDetailsPage() {

    summaryRobot.checkSummaryPageDisplayed();
  }
}
