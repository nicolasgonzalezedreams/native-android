package com.odigeo.test.tests.search;

import android.Manifest;
import com.odigeo.test.robot.DestinationRobot;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

import static com.odigeo.test.robot.mockserver.MockServerConfigurator.DESTINATION_GPS;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.DESTINATION_NO_GPS;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 23/01/17
 */
public abstract class OdigeoDestinationTest extends BaseTest {
  private static final double MOCK_LATITUDE = 40.416749;
  private static final double MOCK_LONGITUDE = -3.703510;
  private static final String GPS_TURNED_OFF = "GPS turned off";
  private static final String GPS_TURNED_ON = "GPS turned on";
  private static final String CITY_SELECTION_PAGE = "City selection page";
  private static final String USER_SELECTS_CURRENT_LOCATION = "User selects current location";
  private static final String WE_REACHED_NO_CITY_RESULTS_PAGE = "We reached no city results page";
  private static final String CURRENT_CITY_RESULTS_ARE_SHOWN = "Current city results are shown";
  private DestinationRobot destinationRobot;

  @Given(GPS_TURNED_OFF) public void turnOffGPS() {
    destinationRobot = new DestinationRobot(mTargetContext);
    configureMockServer(DESTINATION_NO_GPS);
    destinationRobot.mockLocation(MOCK_LATITUDE, MOCK_LONGITUDE);
    destinationRobot.takeScreenshot(this, "GPS_TURNED_OFF");
  }

  @Given(GPS_TURNED_ON) public void turnOnGPS() {
    destinationRobot = new DestinationRobot(mTargetContext);
    configureMockServer(DESTINATION_GPS);
    destinationRobot.mockLocation(MOCK_LATITUDE, MOCK_LONGITUDE);
    destinationRobot.takeScreenshot(this, "GPS_TURNED_ON");
  }

  @And(CITY_SELECTION_PAGE) public void givenCitySelectionPage() {
    destinationRobot.takeScreenshot(this, "CITY_SELECTION_PAGE");
    getActivityTestRule().launchActivity(null);
  }

  @When(USER_SELECTS_CURRENT_LOCATION) public void clickCurrentLocation() {
    destinationRobot.useCurrentLocation();
    mTestRobot.allowPermission(Manifest.permission.ACCESS_FINE_LOCATION);
    destinationRobot.takeScreenshot(this, "USER_SELECTS_CURRENT_LOCATION");
  }

  @Then(WE_REACHED_NO_CITY_RESULTS_PAGE) public void checkNoResultsPage() {
    destinationRobot.takeScreenshot(this, "WE_REACHED_NO_CITY_RESULTS_PAGE");
    destinationRobot.checkDestinationError();
    destinationRobot.takeScreenshot(this, "WE_REACHED_NO_CITY_RESULTS_PAGE_END");
  }

  @Then(CURRENT_CITY_RESULTS_ARE_SHOWN) public void checkCities() {
    destinationRobot.checkCitiesAreDisplayed();
  }
}
