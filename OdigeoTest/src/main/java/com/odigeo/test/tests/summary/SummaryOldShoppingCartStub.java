package com.odigeo.test.tests.summary;

import com.odigeo.app.android.lib.models.OldMyShoppingCart;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.BaggageAllowanceTypeDTO;
import com.odigeo.app.android.lib.models.dto.BookingSummaryStatus;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.app.android.lib.models.dto.CarrierDTO;
import com.odigeo.app.android.lib.models.dto.LocationDTO;
import com.odigeo.app.android.lib.models.dto.MoneyDTO;
import com.odigeo.app.android.lib.models.dto.SectionDTO;
import com.odigeo.app.android.lib.models.dto.SegmentDTO;
import com.odigeo.data.entity.shoppingCart.PricingMode;
import com.odigeo.data.entity.shoppingCart.Step;
import com.odigeo.data.entity.shoppingCart.StepsConfiguration;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

class SummaryOldShoppingCartStub {

  static OldMyShoppingCart provdeOldShoppingCart() {
    OldMyShoppingCart oldMyShoppingCart = new OldMyShoppingCart();
    oldMyShoppingCart.setBookingId(12345L);
    oldMyShoppingCart.setBookingSummaryStatus(BookingSummaryStatus.PENDING);
    oldMyShoppingCart.setLocale("fr_FR");
    MoneyDTO money = new MoneyDTO();
    money.setCurrency("USD");

    List<SegmentWrapper> segmentsWrappers = new ArrayList();
    SegmentWrapper segmentWrapper = new SegmentWrapper();
    CarrierDTO carrierDTO = new CarrierDTO();
    carrierDTO.setCode("IB");
    carrierDTO.setName("Iberia");

    segmentWrapper.setCarrier(carrierDTO);
    segmentWrapper.setKey("1234");

    List<SectionDTO> sectionsDTO = new ArrayList<>();
    sectionsDTO.add(provideOldShoppingCartSectionDTO());
    segmentWrapper.setSectionsObjects(sectionsDTO);

    SegmentDTO segmentDTO = new SegmentDTO();
    segmentDTO.setCarrier(0);
    segmentDTO.setDuration(75L);
    segmentDTO.setSelected(true);
    segmentDTO.setDuration(12346L);
    segmentWrapper.setSegment(segmentDTO);
    segmentsWrappers.add(segmentWrapper);

    oldMyShoppingCart.setStepsFlowConfiguration(provideStepConfigurationDTO());

    oldMyShoppingCart.setTotalPrice(new BigDecimal(136.40));
    return oldMyShoppingCart;
  }

  private static SectionDTO provideOldShoppingCartSectionDTO() {
    SectionDTO sectionDTO = new SectionDTO();
    sectionDTO.setArrivalDate(1234512);
    sectionDTO.setArrivalTerminal("1");
    sectionDTO.setBaggageAllowanceQuantity(0);
    sectionDTO.setBaggageAllowanceType(BaggageAllowanceTypeDTO.NP);
    sectionDTO.setCabinClass(CabinClassDTO.ECONOMIC_DISCOUNTED);
    sectionDTO.setCarrier(0);
    sectionDTO.setDepartureDate(1234533);
    sectionDTO.setDepartureTerminal("4");
    sectionDTO.setDuration(75L);
    sectionDTO.setId("1234");
    sectionDTO.setLocationFrom(provideLocationDTOFrom());
    sectionDTO.setLocationFrom(provideLocationDTOTo());
    sectionDTO.setTo(157);
    return sectionDTO;
  }

  private static LocationDTO provideLocationDTOFrom() {
    LocationDTO locationDTOFrom = new LocationDTO();
    locationDTOFrom.setCityIataCode("MAD");
    locationDTOFrom.setCityName("Madrid");
    locationDTOFrom.setCountryCode("ES");
    locationDTOFrom.setCountryName("Spain");
    locationDTOFrom.setGeoNodeId(1147);
    locationDTOFrom.setIataCode("MAD");
    locationDTOFrom.setName("Adolfo Suárez Madrid - Barajas");
    locationDTOFrom.setType("Airport");
    return locationDTOFrom;
  }

  private static LocationDTO provideLocationDTOTo() {
    LocationDTO locationDTOTo = new LocationDTO();
    locationDTOTo.setCityIataCode("BCN");
    locationDTOTo.setCityName("Barcelona");
    locationDTOTo.setCountryCode("ES");
    locationDTOTo.setCountryName("Spain");
    locationDTOTo.setGeoNodeId(1147);
    locationDTOTo.setIataCode("BCN");
    locationDTOTo.setName("El Prat");
    locationDTOTo.setType("Airport");
    return locationDTOTo;
  }

  private static List<StepsConfiguration> provideStepConfigurationDTO() {
    List<StepsConfiguration> stepsConfigurations = new ArrayList<>();
    StepsConfiguration stepsConfiguration = new StepsConfiguration();
    stepsConfiguration.setStep(Step.SUMMARY);
    stepsConfiguration.setTotalPriceMode(PricingMode.PER_PASSENGER);
    stepsConfigurations.add(stepsConfiguration);
    return stepsConfigurations;
  }
}
