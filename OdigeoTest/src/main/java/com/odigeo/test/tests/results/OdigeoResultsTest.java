package com.odigeo.test.tests.results;

import android.content.Intent;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Market;
import com.odigeo.app.android.lib.config.SearchOptions;
import com.odigeo.app.android.lib.mappers.StoredSearchOptionsMapper;
import com.odigeo.app.android.providers.MarketProvider;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.entity.geo.LocationDescriptionType;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackHelper;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackerFlowSession;
import com.odigeo.interactors.MembershipInteractor;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.test.mocks.LegacyModelMockProviders;
import com.odigeo.test.robot.ResultsRobot;
import com.odigeo.test.robot.SummaryRobot;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

import static com.odigeo.app.android.lib.consts.Constants.EXTRA_SEARCH_OPTIONS;
import static com.odigeo.test.mock.MocksProvider.providesStoredSearchMocks;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.GET_AVAILABLE_PRODUCTS;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.SEARCH_ANY;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.SEARCH_MEMBERSHIP;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.SEARCH_ONLY_DIRECT;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.SHOPPING_CART_CREATE;

/**
 * Created by Javier Marsicano on 07/03/17.
 */

public abstract class OdigeoResultsTest extends BaseTest {
  private static final String OPEN_RESULTS_PAGE = "user is in Results page";
  private static final String RESULTS_PAGE_FT_MARKET = "the results page on a FT market";
  private static final String FLIGHT_DETAILS_PAGE = "user is in Flight details page";
  private static final String SELECT_FLIGHT = "user select a flight";
  private static final String CONTINUE = "select continue button";
  private static final String NOT_CONTINUE = "continue button is not displayed";
  private static final String OPEN_FT_LAYER = "open layer is displayed regarding FT";
  private static final String SELECT_FT_MSG = "user select FT message";
  private static final String RESULTS_INCLUDE_BAGGAGE =
      "search results <IsIncluded> baggage included";
  private static final String BAGAGGE_INFO_DISPLAYED = "flight baggage information <IsDisplayed>";
  private static final String USER_MADE_SEARCH =
      "user made a flight search with results that not include one of the destinations selected";
  private static final String WIDENING_MSG = "widening message appears";
  private static final String USER_IS_MEMBER_IN_MARKET = "A user that is member in a market that "
      + "allow membership discounts";
  private static final String USER_IS_IN_RESULTS_PAGE = "user is in results page";
  private static final String MEMBERSHIP_PRICE_SHOULD_BE_DISPLAYED = "membership price should be "
      + "displayed";
  private static final String NON_MEMBERSHIP_PRICE_SHOULD_BE_SLASHED = "non membership price should be slashed";
  private static final String MEMBERSHIP_TEXT_SHOULD_BE_DISPLAYED = "membership text should be "
      + "displayed";

  private static final String BOOLEAN_TRUE = "true";
  private static final String CURRENT_MEMBERSHIP_MARKET = "FR";

  private Intent parametersIntent;
  private SearchOptions mSearchOptions;
  private ResultsRobot mResultsRobot;
  private SummaryRobot mSummaryRobot;

  private MembershipInteractor membershipInteractor;
  private MarketProviderInterface marketProvider;

  private StoredSearchOptionsMapper storedSearchOptionsMapper;

  @Override public void setUp() throws Exception {
    super.setUp();
    mResultsRobot = new ResultsRobot(mTargetContext);
    parametersIntent = new Intent();
    storedSearchOptionsMapper =
        AndroidDependencyInjector.getInstance().provideStoredSearchOptionsMapper();
    mSearchOptions = storedSearchOptionsMapper.storedSearchToSearchOptionMapper(
        providesStoredSearchMocks().provideStoredSearchWithSegment());
    membershipInteractor = AndroidDependencyInjector.getInstance().provideMembershipInteractor();
    marketProvider = AndroidDependencyInjector.getInstance().provideMarketProvider();
  }

  @Override public void tearDown() throws Exception {
    super.tearDown();
    SearchTrackerFlowSession.getInstance().setSearchTrackHelper(null);
    membershipInteractor.clearMembership();
  }

  @Given(OPEN_RESULTS_PAGE) @When(OPEN_RESULTS_PAGE) public void openResults() {
    configureMockServer(SHOPPING_CART_CREATE);
    mTestRobot.takeScreenshot(this, "OPEN_RESULTS_PAGE");
    parametersIntent.putExtra(EXTRA_SEARCH_OPTIONS, mSearchOptions);
    mTestRobot.takeScreenshot(this, "OPEN_RESULTS_PAGE");
  }

  @Given(RESULTS_PAGE_FT_MARKET) public void openResultsFTMarket() {
    configureMockServer(SEARCH_ONLY_DIRECT);

    parametersIntent.putExtra(EXTRA_SEARCH_OPTIONS, mSearchOptions);
    mTestRobot.takeScreenshot(this, "RESULTS_PAGE_FT_MARKET");
    rule.launchActivity(parametersIntent);
    mTestRobot.takeScreenshot(this, "RESULTS_PAGE_FT_MARKET");
  }

  @Given(USER_MADE_SEARCH) public void configureSearch() {
    mTestRobot.takeScreenshot(this, "USER_MADE_SEARCH");
    setSearchOptionsWidening(mSearchOptions);
    mTestRobot.takeScreenshot(this, "USER_MADE_SEARCH");
    configureMockServer(SEARCH_ANY);
  }

  @Given(USER_IS_MEMBER_IN_MARKET) public void configureUserMembership() {
    setCurrentMarket(CURRENT_MEMBERSHIP_MARKET);
    addMembershipForUser();
    configureMockServer(SEARCH_MEMBERSHIP);
  }

  @When(SELECT_FLIGHT) public void selectFlight() {
    configureMockServer(SEARCH_ANY);
    mTestRobot.takeScreenshot(this, "SELECT_FLIGHT");
    rule.launchActivity(parametersIntent);
    mTestRobot.takeScreenshot(this, "SELECT_FLIGHT");
    mResultsRobot.selectAnyFlight();
  }

  @And(CONTINUE) public void goNextPage() {
    SearchTrackerFlowSession.getInstance().setSearchTrackHelper(provideSearchTrackHelper());
    mTestRobot.takeScreenshot(this, "CONTINUE");
    configureMockServer(SHOPPING_CART_CREATE);
    mTestRobot.takeScreenshot(this, "CONTINUE");
    configureMockServer(GET_AVAILABLE_PRODUCTS);
    mTestRobot.takeScreenshot(this, "CONTINUE");
    mSummaryRobot = mResultsRobot.goToNextPage();
    mTestRobot.takeScreenshot(this, "CONTINUE");
  }

  @Then(FLIGHT_DETAILS_PAGE) public void verifyFlightDetails() {
    mTestRobot.takeScreenshot(this, "FLIGHT_DETAILS_PAGE");
    mSummaryRobot.checkSummaryPageDisplayed();
    mTestRobot.takeScreenshot(this, "FLIGHT_DETAILS_PAGE");
  }

  @Then(NOT_CONTINUE) public void verifyCannotGoNextPage() {
    mTestRobot.takeScreenshot(this, "NOT_CONTINUE");
    configureMockServer(SEARCH_ANY);
    mTestRobot.takeScreenshot(this, "NOT_CONTINUE");
    rule.launchActivity(parametersIntent);
    mTestRobot.takeScreenshot(this, "NOT_CONTINUE");
    mResultsRobot.checkCannotContinue();
    mTestRobot.takeScreenshot(this, "NOT_CONTINUE");
  }

  @When(SELECT_FT_MSG) public void selectMsgFT() {
    mTestRobot.takeScreenshot(this, "SELECT_FT_MSG");
    mResultsRobot.openFTDetails();
    mTestRobot.takeScreenshot(this, "SELECT_FT_MSG");
  }

  @Then(OPEN_FT_LAYER) public void verifyLayerFT() {
    mTestRobot.takeScreenshot(this, "OPEN_FT_LAYER");
    mResultsRobot.checkFullTransparencyModal();
    mTestRobot.takeScreenshot(this, "OPEN_FT_LAYER");
  }

  @When(RESULTS_INCLUDE_BAGGAGE) public void searchResultsIncludeBaggage(String isIncluded) {
    if (BOOLEAN_TRUE.equals(isIncluded)) {
      configureMockServer(SEARCH_ONLY_DIRECT);
    } else {
      configureMockServer(SEARCH_ANY);
    }
    mTestRobot.takeScreenshot(this, "RESULTS_INCLUDE_BAGGAGE");
    rule.launchActivity(parametersIntent);
    mTestRobot.takeScreenshot(this, "RESULTS_INCLUDE_BAGGAGE");
  }

  @Then(BAGAGGE_INFO_DISPLAYED) public void verifyBagageInfo(String isDisplayed) {
    mTestRobot.takeScreenshot(this, "BAGAGGE_INFO_DISPLAYED");
    mResultsRobot.checkBaggageInfoIsDisplayed(BOOLEAN_TRUE.equals(isDisplayed));
    mTestRobot.takeScreenshot(this, "BAGAGGE_INFO_DISPLAYED");
  }

  @Then(WIDENING_MSG) public void verifyWidening() {
    mTestRobot.takeScreenshot(this, "WIDENING_MSG");
    rule.launchActivity(parametersIntent);
    mTestRobot.takeScreenshot(this, "WIDENING_MSG");
    mResultsRobot.checkWideningMessage();
    mTestRobot.takeScreenshot(this, "WIDENING_MSG");
  }

  @When(USER_IS_IN_RESULTS_PAGE) public void openResultsPage() {
    parametersIntent.putExtra(EXTRA_SEARCH_OPTIONS, mSearchOptions);
    mTestRobot.takeScreenshot(this, "RESULTS_PAGE");
    rule.launchActivity(parametersIntent);
    mTestRobot.takeScreenshot(this, "RESULTS_PAGE");
  }

  @Then(MEMBERSHIP_PRICE_SHOULD_BE_DISPLAYED) public void checkMembershipPricesAreDisplayed() {
    mResultsRobot.checkMembershipPriceIsDisplayed();
  }

  @And(NON_MEMBERSHIP_PRICE_SHOULD_BE_SLASHED) public void checkNonMembershipPricesAreSlashed() {
    mResultsRobot.checkNonMembershipPriceIsDisplayed();
  }

  @And(MEMBERSHIP_TEXT_SHOULD_BE_DISPLAYED) public void checkMembershipLabelIsDisplayed() {
    mResultsRobot.checkMembershipLabelIsDisplayed();
  }

  private void setSearchOptionsWidening(SearchOptions searchOptions) {
    City arrival = searchOptions.getFirstSegment().getArrivalCity();
    arrival.setType(LocationDescriptionType.AIRPORT);
    arrival.setCityName("arrival");

    City departure = searchOptions.getFirstSegment().getDepartureCity();
    departure.setType(LocationDescriptionType.AIRPORT);
    departure.setCityName("departure");
  }

  private SearchTrackHelper provideSearchTrackHelper() {
    SearchTrackHelper searchTrackHelper = new SearchTrackHelper();
    searchTrackHelper.setAdults(1);
    searchTrackHelper.setInfants(1);
    searchTrackHelper.setKids(1);
    searchTrackHelper.setPrice(0.0);
    searchTrackHelper.addAirline("Delta");
    searchTrackHelper.addSegment("departure", "arrival", 0, "depCountry", "arrCountry");

    return searchTrackHelper;
  }

  private void addMembershipForUser() {
    membershipInteractor.addMembership(1323421432l, "Jtoo", "Joto", "FR");
  }

  private void setCurrentMarket(String currentMarket) {
    for (Market market : Configuration.getInstance().getMarkets()) {
      if (market.getKey().equals(currentMarket)) {
        Configuration.getInstance().setCurrentMarket(market);
        break;
      }
    }
  }
}
