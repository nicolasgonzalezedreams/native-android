package com.odigeo.test.tests.mytrips;

import android.content.Intent;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.test.robot.MyTripsRobot;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.And;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;
import java.util.List;

import static com.odigeo.app.android.lib.consts.Constants.EXTRA_MY_TRIPS_WAITING_ANIMATION_ENABLED;
import static com.odigeo.test.mock.MocksProvider.providesBookingMocks;

public abstract class OdigeoMyTripsTest extends BaseTest {

  private static final String THE_PAGE_MY_TRIPS_WITH_ONE_WAY_PAST_TRIP =
      "the page My Trips with one way " + "past trip";
  private static final String ONE_WAY_PAST_TRIP_PROPERLY_SHOWN =
      "one way past trip is properly shown";
  private static final String THE_PAGE_MY_TRIPS_WITH_ROUNDED_PAST_TRIP =
      "the page My Trips with " + "rounded " + "past trip";
  private static final String ROUNDED_PAST_TRIP_PROPERLY_SHOWN =
      "rounded past trip is properly shown";

  private static final String THE_PAGE_MY_TRIPS_WITH_PAST_MULTITRIP =
      "the page My Trips with " + "multitrip past trip";
  private static final String MULTITRIP_PAST_TRIP_PROPERTLY_SHOWN =
      "multitrip past trip is " + "properly " + "shown";

  private MyTripsRobot myTripsRobot;
  private Booking booking;
  private Booking oneWayPastBooking;
  private Booking roundedPastBooking;
  private Booking multitripPastBooking;

  @Override public void setUp() throws Exception {
    super.setUp();
    booking = providesBookingMocks().provideBooking();
    myTripsRobot = new MyTripsRobot(mTargetContext);
    mTestRobot.cleanDatabase();
  }

  @Given(THE_PAGE_MY_TRIPS_WITH_ONE_WAY_PAST_TRIP) public void onMyTripsWithOneWayPastTrip() {
    oneWayPastBooking = providesBookingMocks().providePastBooking();
    AndroidDependencyInjector.getInstance().provideBookingsHandler().saveBooking(oneWayPastBooking);
    launchActivity();
  }

  @Given(THE_PAGE_MY_TRIPS_WITH_ROUNDED_PAST_TRIP) public void onMyTripsWithRoundedPastTrip() {
    roundedPastBooking = providesBookingMocks().provideRoundPastBooking();
    AndroidDependencyInjector.getInstance()
        .provideBookingsHandler()
        .saveBooking(roundedPastBooking);
    launchActivity();
  }

  @Given(THE_PAGE_MY_TRIPS_WITH_PAST_MULTITRIP) public void onMyTripsWithMultitripPastTrip() {
    multitripPastBooking = providesBookingMocks().provideMultitripPastBooking();
    AndroidDependencyInjector.getInstance()
        .provideBookingsHandler()
        .saveBooking(multitripPastBooking);
    launchActivity();
  }

  private void launchActivity() {
    Intent intent = new Intent();
    intent.putExtra(EXTRA_MY_TRIPS_WAITING_ANIMATION_ENABLED, false);
    getActivityTestRule().launchActivity(intent);
  }

  @Then(ONE_WAY_PAST_TRIP_PROPERLY_SHOWN) public void verifyOneWayPastTripDesign() {
    myTripsRobot.checkOneWayArrowShown();
    checkArriveAndDepartureCityNamesDisplayed(oneWayPastBooking);
  }

  @Then(ROUNDED_PAST_TRIP_PROPERLY_SHOWN) public void verifyRoundedPastTripDesign() {
    myTripsRobot.checkRoundedArrowShown();
    checkArriveAndDepartureCityNamesDisplayed(roundedPastBooking);
  }

  @Then(MULTITRIP_PAST_TRIP_PROPERTLY_SHOWN) public void verifyMultiPastTripDesign() {
    myTripsRobot.checkOneWayArrowShown();
    checkArriveAndDepartureCityNamesDisplayed(multitripPastBooking);
    List<Segment> segments = multitripPastBooking.getSegments();
    for (Segment segment : segments) {
      myTripsRobot.checkTextIsShown(segment.getFirstSection().getTo().getCityName());
    }
  }

  private void checkArriveAndDepartureCityNamesDisplayed(Booking booking) {
    myTripsRobot.checkTextIsShown(booking.getFirstSegment().getLastSection().getTo().getCityName());
    myTripsRobot.checkTextIsShown(
        booking.getFirstSegment().getFirstSection().getFrom().getCityName());
  }
}