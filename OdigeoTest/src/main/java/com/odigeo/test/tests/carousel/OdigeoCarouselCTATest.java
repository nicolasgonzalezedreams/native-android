package com.odigeo.test.tests.carousel;

import android.content.Intent;
import com.odigeo.app.android.navigator.NavigationDrawerNavigator;
import com.odigeo.test.robot.CarouselRobot;
import com.odigeo.test.robot.mockserver.MockServerConfigurator;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

import static com.odigeo.test.robot.mockserver.MockServerConfigurator.GOVOYAGES;
import static com.odigeo.test.robot.mockserver.MockServerConfigurator.OPODO;

public abstract class OdigeoCarouselCTATest extends BaseTest {

  private static final String CARD_2_CTA = "A card with 2 CTA";
  private static final String CARD_1_CTA = "A card with 1 CTA";
  private static final String MAX_SIZE_TEXT_CTA = "Each CTA have <maxSize> characters";
  private static final String MAX_SIZE_TEXT_CTA_EXTRA = "Each CTA have an extra large text";
  private static final String ALL_TEXT_IS_DISPLAYED = "All the text is displayed";
  private static final String TEXT_IS_ELLIPSIZED = "The text is ellipsized";
  private static final String TEXT_SIZE_IS_MORE_THAN_MAX =
      "CTA can have more than <maxSize> characters";

  private CarouselRobot carouselRobot;

  @Override public void setUp() throws Exception {
    super.setUp();
    carouselRobot = new CarouselRobot(mTargetContext);
    int mockserverConfiguration = getMockServerConfigurationPerBrand(getBrand());
    configureMockServer(mockserverConfiguration);
  }

  @Given(CARD_2_CTA) public void request2CTACard() {
    getActivityTestRule().launchActivity(
        new Intent(mTargetContext, NavigationDrawerNavigator.class));
  }

  @Given(CARD_1_CTA) public void request1CTACard() {
    getActivityTestRule().launchActivity(
        new Intent(mTargetContext, NavigationDrawerNavigator.class));
    carouselRobot.goToNextCard().goToNextCard();
  }

  @When(MAX_SIZE_TEXT_CTA) public void eachCTAHasMaxSizeCharacters(String maxSize) {
    carouselRobot.checkCTAButtonsSize(maxSize);
  }

  @When(MAX_SIZE_TEXT_CTA_EXTRA) public void eachCTAHasMoreThanMaxSizeCharacters() {
    carouselRobot.goToNextCard();
  }

  @Then(ALL_TEXT_IS_DISPLAYED) public void checkAllTextIsDisplayed() {
    carouselRobot.checkAllTextIsVisible();
  }

  @Then(TEXT_IS_ELLIPSIZED) public void checkOnlyMaxCharacter() {
    carouselRobot.checkNotAllTextIsVisible();
  }

  @Then(TEXT_SIZE_IS_MORE_THAN_MAX) public void checkTextIsMoreThanMax(String maxSize) {
    carouselRobot.checkCTAButtonSizeBiggerThan(maxSize);
  }

  @MockServerConfigurator.MockServerConfiguration
  private int getMockServerConfigurationPerBrand(@MockServerConfigurator.Brand int brand) {
    @MockServerConfigurator.MockServerConfiguration int configuration =
        MockServerConfigurator.CAROUSEL_2CTA_SUCCESS_ED;

    switch (brand) {
      case OPODO:
        configuration = MockServerConfigurator.CAROUSEL_2CTA_SUCCESS_OP;
        break;
      case GOVOYAGES:
        configuration = MockServerConfigurator.CAROUSEL_2CTA_SUCCESS_GO;
    }

    return configuration;
  }

  public abstract @MockServerConfigurator.Brand int getBrand();
}
