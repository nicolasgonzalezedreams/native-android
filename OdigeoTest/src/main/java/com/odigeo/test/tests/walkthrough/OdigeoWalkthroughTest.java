package com.odigeo.test.tests.walkthrough;

import android.content.Intent;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Market;
import com.odigeo.app.android.lib.models.WalkthroughPage;
import com.odigeo.test.robot.WalkthroughRobot;
import com.odigeo.test.tests.BaseTest;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

/**
 * Created by daniel.morales on 3/2/17.
 */

public abstract class OdigeoWalkthroughTest extends BaseTest {

  public static final String WALKTHROUGH_PAGE = "Walkthrough page appears";
  public static final String MARKET_IS_SELECTED = "The <market> market is selected";
  public static final String WALKTHROUGH_IS_SHOWED = "Specific walkthrough <isShowed>";

  private WalkthroughRobot robot;

  @Override public void setUp() throws Exception {
    super.setUp();
    robot = new WalkthroughRobot(mTargetContext);
  }

  @Given(MARKET_IS_SELECTED) public void specificMarketIsSelected(String currentMarket) {
    for (Market market : Configuration.getInstance().getMarkets()) {
      if (market.getKey().equals(currentMarket)) {
        Configuration.getInstance().setCurrentMarket(market);
        break;
      }
    }
  }

  @When(WALKTHROUGH_PAGE) public void userIsInWalkthroughPage() {
    getActivityTestRule().launchActivity(new Intent(mTargetContext, WalkthroughPage.class));
  }

  @Then(WALKTHROUGH_IS_SHOWED) public void checkCorrectWalkthroughIsShown(String isShown) {
    robot.checkCorrectWalkthroughIsShown(isShown);
  }
}
