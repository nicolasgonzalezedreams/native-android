package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.espresso.NoMatchingViewException;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

public class ConfirmationRobot extends BaseRobot {

  public ConfirmationRobot(@NonNull Context context) {

    super(context);
  }

  public ConfirmationRobot checkConfirmationSuccess() {
    String confirmed =
        LocalizablesFacade.getString(mContext, OneCMSKeys.INFOSTATUS_TITLE_CONTRACT).toString();
    onView(withText(confirmed)).check(matches(isDisplayed()));
    return this;
  }

  public ConfirmationRobot checkConfirmationPending() {
    String pending =
        LocalizablesFacade.getString(mContext, OneCMSKeys.INFOSTATUS_TITLE_PENDING).toString();
    onView(withText(pending)).check(matches(isDisplayed()));
    return this;
  }

  public ConfirmationRobot checkConfirmationRejected() {
    String rejected =
        LocalizablesFacade.getString(mContext, OneCMSKeys.INFOSTATUS_TITLE_REJECT).toString();
    onView(withText(rejected)).check(matches(isDisplayed()));
    return this;
  }

  public ConfirmationRobot checkGroundTransportationWidgetIsShown(boolean isVisible) {
    if (isVisible) {
      onView(withId(R.id.ground_transportation_widget)).perform(scrollTo())
          .check(matches(isDisplayed()));
    } else {
      onView(withId(R.id.ground_transportation_widget)).check(doesNotExist());
    }
    return this;
  }
}
