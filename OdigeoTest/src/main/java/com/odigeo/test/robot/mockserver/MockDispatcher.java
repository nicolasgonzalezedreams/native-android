package com.odigeo.test.robot.mockserver;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.RecordedRequest;

public class MockDispatcher extends Dispatcher {
  private static final String NOT_FOUND = "404";
  private static final String TAG = "MOCKSERVER";
  private static final String JSON_EXTENSION = ".json";
  private static final String DISPATCH_LOG_TEMPLATE = "%s %s  body:%s RESP SIZE:%d";
  private ResponsesManager responses;
  private Context context;

  public MockDispatcher(Context context) {
    this.context = context;
    responses = new ResponsesManager.Builder().build();
  }

  public void setResponses(ResponsesManager responses) {
    this.responses = responses;
  }

  @Override public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
    String method = request.getMethod().toUpperCase();
    String path = request.getPath();

    Response response = responses.obtainResponse(method, path);

    if (response.getResourceBody().equals("404")) {
      return getNotFoundResponse(request);
    }

    String rawMockBody = obtainRawFile(response.getResourceBody() + JSON_EXTENSION);
    Log.d(TAG, String.format(DISPATCH_LOG_TEMPLATE, method, path, response.getResourceBody(),
        rawMockBody.length()));

    return mapResponseToMockResponse(response, rawMockBody);
  }

  @NonNull private MockResponse mapResponseToMockResponse(Response response, String rawMockBody) {
    if (response.getResponseCode() == HttpCodes.HTTP_BAD_REQUEST) {
      return new MockResponse().setResponseCode(400).setBody(rawMockBody);
    } else if (response.getResponseCode() == HttpCodes.HTTP_NOT_FOUND) {
      return new MockResponse().setResponseCode(404).setBody(rawMockBody);
    }
    return new MockResponse().setBody(rawMockBody);
  }

  //TODO review this switch and try to create a policy for not found request.
  private MockResponse getNotFoundResponse(RecordedRequest request) {
    if (request.getPath().contains("user")) {
      Log.d(TAG, "returned user stuff");
      return new MockResponse();
    } else {
      Log.d(TAG, "returned not found");

      return new MockResponse().setStatus(NOT_FOUND);
    }
  }

  private String obtainRawFile(String resourceName) {
    long time = System.currentTimeMillis();
    InputStream inputStream;
    StringBuffer data = new StringBuffer();
    try {
      inputStream = context.getAssets().open(resourceName);
      BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));

      String line;
      while ((line = buffer.readLine()) != null) {
        data.append(line);
      }
      buffer.close();
    } catch (IOException e) {
      Log.d(TAG, "Error file:" + e.getLocalizedMessage());
    }
    Log.d(TAG,
        "!!!FINISH.READING FILE(" + resourceName + "):" + data.length() + " in:" + String.valueOf(
            System.currentTimeMillis() - time));
    return data.toString();
  }
}
