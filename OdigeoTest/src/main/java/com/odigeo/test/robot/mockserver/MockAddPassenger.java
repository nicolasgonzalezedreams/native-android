package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.ADD_PASSENGER_URL;
import static com.odigeo.test.robot.MockServerRobot.BASIC_CART_PASSENGER_RESPONSE;

public class MockAddPassenger implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(new ResponsesManager.Builder().addPostResponse(ADD_PASSENGER_URL,
        BASIC_CART_PASSENGER_RESPONSE).build());
  }
}
