package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.espresso.DataInteraction;
import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.ViewInteraction;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.test.espresso.matchers.RecyclerMatcher;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withChild;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.hasBaggageIncludedResults;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.hasCabinClass;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.hasOnlyDirectFlights;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withAdaptedData;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withAllResultsExpanded;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withNoContinueBtn;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withResultsExpanded;
import static com.odigeo.test.espresso.OdigeoViewActions.selectFlight;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by Javier Marsicano on 30/11/16.
 */

public class ResultsRobot extends BaseRobotWithCabinClass {
  private static final int DEFAULT_RESULTS_EXPANDED = 25;
  private ViewInteraction buttonContinue =
      onView(allOf(withId(R.id.button_standard_accept), isDisplayed()));
  private ViewInteraction fullPriceMessage = onView(withText(
      LocalizablesFacade.getString(mContext, OneCMSKeys.FULLPRICE_LAYER_HEADER).toString()));
  private ViewInteraction fullPriceDebitCardMsg = onView(
      withText(LocalizablesFacade.getString(mContext, OneCMSKeys.FULLPRICE_LAYER_TEXT).toString()));
  private ViewInteraction fullTransparencyModalText1 = onView(withText(
      LocalizablesFacade.getString(mContext, OneCMSKeys.FULLPRICE_LAYER_TEXT_1_NEW).toString()));
  private ViewInteraction fullTransparencyModalText2 = onView(withText(
      LocalizablesFacade.getString(mContext, OneCMSKeys.FULLPRICE_LAYER_TEXT_2_NEW).toString()));
  private ViewInteraction fullTransparencyModalText3 = onView(withText(
      LocalizablesFacade.getString(mContext, OneCMSKeys.FULLPRICE_LAYER_TEXT_3_NEW).toString()));
  private ViewInteraction fullPriceBubbleMsg = onView(withText(
      LocalizablesFacade.getString(mContext, OneCMSKeys.FULLPRICE_BUBBLE_MESSAGE).toString()));

  private DataInteraction expandableListViewFirstItem =
      onData(anything()).inAdapterView(withId(R.id.expandableListView_resultsActivity))
      .atPosition(0);

  public ResultsRobot(@NonNull Context context) {
    super(context);
  }

  public SummaryRobot goToNextPage() {
    buttonContinue.perform(click());
    return new SummaryRobot(mContext);
  }

  public ResultsRobot checkCannotContinue() {
    onView(withId(R.id.expandableListView_resultsActivity)).check(matches(withNoContinueBtn()));
    return this;
  }

  public ResultsRobot checkCCSelector() {
    //expand
    onView(withId(R.id.less_info_full_price)).perform(click());
    onView(withId(R.id.spinnerFullPrice)).check(matches(isDisplayed()));
    //collapse
    onView(withId(R.id.less_info_full_price)).perform(click());
    return this;
  }

  public ResultsRobot checkRoundTrip() {
    onView(allOf(withId(R.id.text_header_section_resultWidget),
        withText(CommonLocalizables.getInstance().getCommonInbound(mContext).toString()))).check(
        matches(isDisplayed()));
    return this;
  }

  public ResultsRobot checkPricePerPassenger() {
    onView(withId(R.id.pricePerPassengerWidget)).check(matches(isDisplayed()));
    return this;
  }

  public ResultsRobot checkFullPriceSnackBar() {
    fullPriceMessage.check(matches(isDisplayed()));
    return this;
  }

  public ResultsRobot checkFullTransparencyDialog() {
    onView(withId(R.id.img_full_transparency)).check(matches(isDisplayed()));
    fullPriceDebitCardMsg.check(matches(isDisplayed()));
    fullPriceMessage.check(matches(isDisplayed()));
    return this;
  }

  public ResultsRobot closeFTDialog() {
    onView(withId(R.id.button_full_transparency)).perform(click());
    return this;
  }

  public ResultsRobot closeFullTransparencyModal() {
    onView(withId(R.id.btnOK)).perform(click());
    return this;
  }

  public ResultsRobot checkFullTransparencyModal() {
    onView(withId(R.id.flFullTransparencyContainer)).check(matches(isDisplayed()));

    fullTransparencyModalText1.check(matches(isDisplayed()));
    fullTransparencyModalText2.check(matches(isDisplayed()));
    fullTransparencyModalText3.check(matches(isDisplayed()));
    return this;
  }

  public ResultsRobot checkBottomBubble() {
    fullPriceBubbleMsg.check(matches(isDisplayed()));
    return this;
  }

  public ResultsRobot openFTDetails() {
    onView(withId(R.id.snackbar_action)).perform(click());
    return this;
  }

  public ResultsRobot checkFinalPriceDebitCard() {
    String fullPriceResultsHeader =
        LocalizablesFacade.getString(mContext, OneCMSKeys.FULL_PRICE_RESULTS_HEADER_NEW).toString();

    onView(allOf(withId(R.id.fullPriceSentence),
        withParent(withId(R.id.layoutcontainer_header_price)))).check(
        matches(withText(fullPriceResultsHeader)));
    return this;
  }

  public ResultsRobot checkResultsAreDisplayed() {
    closeBanner();
    onView(withId(R.id.expandableListView_resultsActivity)).check(matches(withAdaptedData()));
    return this;
  }

  private void closeBanner() {
    //TODO review why appear a banner. (this 3 lines are cancer)
    if (onView(withContentDescription("close_button")) != null) {
      try {
        onView(withContentDescription("close_button")).perform(click());
      } catch (Exception e) {

      }
    }
  }

  public ResultsRobot selectAnyFlight() {
    closeBanner();
    onView(withId(R.id.expandableListView_resultsActivity)).perform(selectFlight());
    return this;
  }

  public ResultsRobot checkAllGroupsAreExpanded() {
    onView(withId(R.id.expandableListView_resultsActivity)).check(
        matches(withAllResultsExpanded()));
    return this;
  }

  public ResultsRobot checkGroupsAreExpanded() {
    onView(withId(R.id.expandableListView_resultsActivity)).check(
        matches(withResultsExpanded(DEFAULT_RESULTS_EXPANDED)));
    return this;
  }

  public ResultsRobot checkErrorPageIsDisplayed() {
    onView(withId(R.id.view_activity_noconnection_emptyview)).check(matches(isDisplayed()));
    return this;
  }

  public ResultsRobot checkWideningMessage() {
    onView(withId(R.id.widening)).check(matches(isDisplayed()));
    return this;
  }

  public ResultsRobot checkResearchPageIsDisplayed() {
    closeBanner();
    onView(withId(R.id.tvResetSearch)).check(matches(isDisplayed()));
    return this;
  }

  public ResultsRobot checkOnlyDirectFlights() {
    closeBanner();
    onView(withId(R.id.expandableListView_resultsActivity)).check(matches(hasOnlyDirectFlights()));
    return this;
  }

  public ResultsRobot checkBaggageInfoIsDisplayed(boolean isDisplayed) {
    ViewAssertion assertion;
    if (isDisplayed) {
      assertion = matches(hasBaggageIncludedResults());
    } else {
      assertion = matches(not(hasBaggageIncludedResults()));
    }

    onView(withId(R.id.expandableListView_resultsActivity)).check(assertion);
    return this;
  }

  public ResultsRobot checkResultsCabinClass(@NonNull String classToCheck) {
    onView(withId(R.id.expandableListView_resultsActivity)).check(
        matches(hasCabinClass(getCabinClass(classToCheck))));
    return this;
  }

  public void checkMembershipPriceIsDisplayed() {
    expandableListViewFirstItem
        .onChildView(withId(R.id.flights_price))
        .check(matches(isDisplayed()));
  }

  public void checkNonMembershipPriceIsDisplayed() {
    expandableListViewFirstItem
        .onChildView(withId(R.id.flights_slashed_price))
        .check(matches(isDisplayed()));
  }

  public void checkMembershipLabelIsDisplayed() {
    expandableListViewFirstItem
        .onChildView(withId(R.id.member_label))
        .check(matches(isDisplayed()));
  }
}
