package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.LOGIN_EMAIL_ERROR;
import static com.odigeo.test.robot.MockServerRobot.USER_URL;

public class MockLoginEmailError implements MockServerStrategy {
  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(
        new ResponsesManager.Builder().addGetResponseWithCode(USER_URL, LOGIN_EMAIL_ERROR,
            HttpCodes.HTTP_BAD_REQUEST).build());
  }
}
