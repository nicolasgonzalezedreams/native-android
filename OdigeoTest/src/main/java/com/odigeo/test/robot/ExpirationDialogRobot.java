package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import com.odigeo.app.android.lib.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by Javier Marsicano on 14/02/17.
 */

public class ExpirationDialogRobot extends BaseRobot {

  public ExpirationDialogRobot(@NonNull Context context) {
    super(context);
  }

  public ExpirationDialogRobot checkTimeExpirationDialogIsDisplayed() {
    onView(withId(R.id.dialog_expired_title)).check(matches(isDisplayed()));
    onView(withId(R.id.btn_second_dialog_expired)).check(matches(isDisplayed()));
    return this;
  }
}
