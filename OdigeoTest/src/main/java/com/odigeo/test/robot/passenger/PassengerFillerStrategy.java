package com.odigeo.test.robot.passenger;

import com.odigeo.test.robot.PassengerRobot;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 31/01/17
 */
public interface PassengerFillerStrategy {
  void fill(PassengerRobot robot);
}
