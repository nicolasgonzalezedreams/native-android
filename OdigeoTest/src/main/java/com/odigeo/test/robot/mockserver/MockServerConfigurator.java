package com.odigeo.test.robot.mockserver;

import android.support.annotation.IntDef;
import android.util.Log;
import android.util.SparseArray;
import com.odigeo.test.robot.MockServerRobot;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 22/12/16
 */

public class MockServerConfigurator {

  public static final int EDREAMS = 0;
  public static final int OPODO = 1;
  public static final int GOVOYAGES = 2;
  public static final int TRAVELLINK = 3;

  public static final int SEARCH_OW_SUCCESS = 0;
  public static final int SEARCH_ROUND_SUCCESS = 1;
  public static final int SEARCH_MULTI_SUCCESS = 2;
  public static final int SEARCH_EMPTY = 3;
  public static final int SEARCH_ROUND_ERROR = 4;
  public static final int SEARCH_OW_ECONOMY = 5;
  public static final int SEARCH_OW_PREMIUM = 6;
  public static final int SEARCH_OW_FIRST = 7;
  public static final int SEARCH_OW_BUSINESS = 8;
  public static final int CAROUSEL_2CTA_SUCCESS_ED = 9;
  public static final int CAROUSEL_2CTA_SUCCESS_OP = 10;
  public static final int CAROUSEL_2CTA_SUCCESS_GO = 11;
  public static final int CAROUSEL_2CTA_SUCCESS_TL = 12;
  public static final int SEARCH_ONLY_DIRECT = 13;
  public static final int SEARCH_ANY = 14;
  public static final int PROMO_CODE_ADD_SUCCESSFUL = 15;
  public static final int PROMO_CODE_ADD_UNSUCCESSFUL = 16;
  public static final int PROMO_CODE_REMOVE_SUCCESSFUL = 17;
  public static final int USER_SYNC_SEARCH = 18;
  public static final int ADD_PASSENGER = 19;
  public static final int DESTINATION_NO_GPS = 20;
  public static final int DESTINATION_GPS = 21;
  public static final int DISABLE_NOTIFICATIONS_SUCCESS = 22;
  public static final int ADD_PRODUCT = 23;
  public static final int BIN_CHECK_VALIDATION_SUCCESSFUL = 24;
  public static final int ADD_PASSENGER_DUPLICATED_BOOKING = 25;
  public static final int GET_AVAILABLE_PRODUCTS = 28;
  public static final int SHOPPING_CART_CREATE = 29;
  public static final int IMPORT_SUCCESS = 30;
  public static final int BOOK_BOOKINGCONFIRMED_CONTRACT = 31;
  public static final int BOOK_BOOKINGCONFIRMED_REQUEST_COLLECTED = 32;
  public static final int BOOK_BOOKINGCONFIRMED_RETAINED = 33;
  public static final int BOOK_BOOKINGCONFIRMED_ON_HOLD = 34;
  public static final int BOOK_BOOKINGCONFIRMED_REQUEST_COLLECTION_NEEDED = 35;
  public static final int BOOK_BOOKINGCONFIRMED_UNKNOWN = 36;
  public static final int BOOK_BOOKINGCONFIRMED_REJECTED = 37;
  public static final int BOOK_BOOKINGCONFIRMED_FINAL_RET = 38;
  public static final int BOOK_BROKEN_FLOW = 39;
  public static final int BOOK_BOOKING_STOP = 40;
  public static final int BOOK_ON_HOLD = 41;
  public static final int BOOK_BOOKING_ERROR = 42;
  public static final int BOOK_PAYMENT_RETRY_ONLY_BANK_TRANSFER = 43;
  public static final int BOOK_PAYMENT_RETRY_ONLY_PAY_PAL = 44;
  public static final int BOOK_PAYMENT_RETRY_BANK_TRANSFER_AND_CREDIT_CARDS = 45;
  public static final int BOOK_PAYMENT_REPRICING = 46;
  public static final int BOOK_PAYMENT_USER_INTERACTION_NEEDED_PAYPAL = 47;
  public static final int BOOK_PAYMENT_USER_INTERACTION_NEEDED_TRUSTLY = 48;
  public static final int BOOK_PAYMENT_USER_INTERACTION_NEEDED_KLARNA = 49;
  public static final int USER_REGISTRATION_SUCCESS = 50;
  public static final int LOGIN_SUCCESSFUL = 51;
  public static final int LOGIN_PWD_ERROR = 52;
  public static final int LOGIN_EMAIL_ERROR = 53;
  public static final int GET_VISITS = 54;
  public static final int SEARCH_MEMBERSHIP = 55;

  private static final SparseArray<MockServerStrategy> configurationsMap = new SparseArray<>();

  static {
    configurationsMap.put(USER_REGISTRATION_SUCCESS, new MockUserRegisterSuccess());
    configurationsMap.put(BOOK_PAYMENT_USER_INTERACTION_NEEDED_KLARNA,
        new MockBookUserInteractionNeededKlarna());
    configurationsMap.put(BOOK_PAYMENT_USER_INTERACTION_NEEDED_TRUSTLY,
        new MockBookUserInteractionNeededTrustly());
    configurationsMap.put(BOOK_PAYMENT_USER_INTERACTION_NEEDED_PAYPAL,
        new MockBookUserInteractionNeededPayPal());
    configurationsMap.put(BOOK_PAYMENT_REPRICING, new MockBookRepricing());
    configurationsMap.put(BOOK_PAYMENT_RETRY_BANK_TRANSFER_AND_CREDIT_CARDS,
        new MockBookRetryBTAndCC());
    configurationsMap.put(BOOK_PAYMENT_RETRY_ONLY_PAY_PAL, new MockBookRetryOnlyPaypal());
    configurationsMap.put(BOOK_PAYMENT_RETRY_ONLY_BANK_TRANSFER, new MockBookRetryOnlyBT());
    configurationsMap.put(BOOK_BOOKING_ERROR, new MockBookBookingError());
    configurationsMap.put(BOOK_BROKEN_FLOW, new MockBookBrokenFlow());
    configurationsMap.put(BOOK_BOOKING_STOP, new MockBookBookingStop());
    configurationsMap.put(BOOK_ON_HOLD, new MockBookOnHold());
    configurationsMap.put(BOOK_BOOKINGCONFIRMED_REJECTED, new MockBookBookingConfirmedRejected());
    configurationsMap.put(BOOK_BOOKINGCONFIRMED_FINAL_RET, new MockBookBookingConfirmedFinalRet());
    configurationsMap.put(BOOK_BOOKINGCONFIRMED_REQUEST_COLLECTION_NEEDED,
        new MockBookBookingConfirmedRequestNotCollected());
    configurationsMap.put(BOOK_BOOKINGCONFIRMED_UNKNOWN, new MockBookBookingConfirmedUnknown());
    configurationsMap.put(BOOK_BOOKINGCONFIRMED_ON_HOLD, new MockBookBookingConfirmedOnHold());
    configurationsMap.put(BOOK_BOOKINGCONFIRMED_RETAINED, new MockBookBookingConfirmedRetained());
    configurationsMap.put(BOOK_BOOKINGCONFIRMED_CONTRACT, new MockBookBookingConfirmedContract());
    configurationsMap.put(BOOK_BOOKINGCONFIRMED_REQUEST_COLLECTED,
        new MockBookBookingConfirmedRequestCollected());
    configurationsMap.put(SEARCH_OW_SUCCESS, new MockSuccessfulOneWaySearch());
    configurationsMap.put(SEARCH_ROUND_SUCCESS, new MockSuccessfulRoundSearch());
    configurationsMap.put(SEARCH_MULTI_SUCCESS, new MockSuccessfulMultiSearch());
    configurationsMap.put(SEARCH_EMPTY, new MockEmptySearch());
    configurationsMap.put(SEARCH_ROUND_ERROR, new MockErrorRoundSearch());
    configurationsMap.put(SEARCH_OW_ECONOMY, new MockSuccessfulOneWayEconomySearch());
    configurationsMap.put(SEARCH_OW_PREMIUM, new MockSuccessfulOneWayPremiumSearch());
    configurationsMap.put(SEARCH_OW_FIRST, new MockSuccessfulOneWayFirstSearch());
    configurationsMap.put(SEARCH_OW_BUSINESS, new MockSuccessfulOneWayBusinessSearch());
    configurationsMap.put(SEARCH_ONLY_DIRECT, new MockOnlyDirectFlights());
    configurationsMap.put(SEARCH_ANY, new MockSearchFlights());
    configurationsMap.put(CAROUSEL_2CTA_SUCCESS_ED, new MockCarousel2CTAED());
    configurationsMap.put(CAROUSEL_2CTA_SUCCESS_OP, new MockCarousel2CTAOP());
    configurationsMap.put(CAROUSEL_2CTA_SUCCESS_GO, new MockCarousel2CTAGO());
    configurationsMap.put(CAROUSEL_2CTA_SUCCESS_TL, new MockCarousel2CTATL());
    configurationsMap.put(PROMO_CODE_ADD_SUCCESSFUL, new MockPromoCodeAddSuccessful());
    configurationsMap.put(PROMO_CODE_ADD_UNSUCCESSFUL, new MockPromoCodeAddUnsuccessful());
    configurationsMap.put(PROMO_CODE_REMOVE_SUCCESSFUL, new MockPromoCodeRemove());
    configurationsMap.put(DESTINATION_NO_GPS, new MockDestinationNoGps());
    configurationsMap.put(DESTINATION_GPS, new MockDestinationGps());
    configurationsMap.put(DISABLE_NOTIFICATIONS_SUCCESS, new MockDisableNotificationsSuccessful());
    configurationsMap.put(USER_SYNC_SEARCH, new MockSyncSearchFlight());
    configurationsMap.put(IMPORT_SUCCESS, new MockImportTripSuccess());
    configurationsMap.put(ADD_PASSENGER, new MockAddPassenger());
    configurationsMap.put(SHOPPING_CART_CREATE, new MockCreateShoppingCart());
    configurationsMap.put(GET_AVAILABLE_PRODUCTS, new MockGetAvailableProducts());
    configurationsMap.put(ADD_PRODUCT, new MockServerAddProduct());
    configurationsMap.put(BIN_CHECK_VALIDATION_SUCCESSFUL, new MockBinCheckSuccessful());
    configurationsMap.put(ADD_PASSENGER_DUPLICATED_BOOKING,
        new MockAddPassengerDuplicatedBooking());
    configurationsMap.put(LOGIN_SUCCESSFUL, new MockLoginSuccessful());
    configurationsMap.put(LOGIN_EMAIL_ERROR, new MockLoginEmailError());
    configurationsMap.put(LOGIN_PWD_ERROR, new MockLoginPasswordError());
    configurationsMap.put(GET_VISITS, new MockGetVisits());
    configurationsMap.put(SEARCH_MEMBERSHIP, new MockSearchMembership());
  }

  private MockServerRobot robot;

  public MockServerConfigurator(MockServerRobot robot) {
    this.robot = robot;
  }

  public void configure(@MockServerConfiguration int configuration) {
    // TODO: 20/6/17 REFACTOR: make each test to configure its own mockserver to avoid maps
    Log.e("MOCKSERVER", "Getting configuration " + configuration);
    MockServerStrategy mockServerStrategy = configurationsMap.get(configuration);
    if (mockServerStrategy == null) {
      throw new RuntimeException("Mock Configuration does not exist");
    }
    mockServerStrategy.configure(robot);
  }

  public void startServer() {
    robot.start();
    this.robot.init();
  }

  public void stopServer() {
    robot.stop();
  }

  public void resetMockServer() {
    robot.clean();
  }

  @Retention(RetentionPolicy.SOURCE) @IntDef({
      SEARCH_OW_SUCCESS, SEARCH_ROUND_SUCCESS, SEARCH_MULTI_SUCCESS, SEARCH_ROUND_ERROR, SEARCH_ANY,
      SEARCH_EMPTY, SEARCH_OW_ECONOMY, SEARCH_OW_PREMIUM, SEARCH_OW_FIRST, SEARCH_OW_BUSINESS,
      SEARCH_ONLY_DIRECT, CAROUSEL_2CTA_SUCCESS_ED, CAROUSEL_2CTA_SUCCESS_OP, USER_SYNC_SEARCH,
      CAROUSEL_2CTA_SUCCESS_GO, CAROUSEL_2CTA_SUCCESS_TL, PROMO_CODE_ADD_SUCCESSFUL,
      PROMO_CODE_ADD_UNSUCCESSFUL, PROMO_CODE_REMOVE_SUCCESSFUL, DESTINATION_NO_GPS,
      DESTINATION_GPS, IMPORT_SUCCESS, DISABLE_NOTIFICATIONS_SUCCESS, ADD_PASSENGER,
      SHOPPING_CART_CREATE, GET_AVAILABLE_PRODUCTS, ADD_PRODUCT, ADD_PASSENGER_DUPLICATED_BOOKING,
      BIN_CHECK_VALIDATION_SUCCESSFUL, BOOK_BOOKINGCONFIRMED_CONTRACT,
      BOOK_BOOKINGCONFIRMED_REQUEST_COLLECTED, BOOK_BOOKINGCONFIRMED_RETAINED,
      BOOK_BOOKINGCONFIRMED_REQUEST_COLLECTION_NEEDED, BOOK_BOOKINGCONFIRMED_UNKNOWN,
      BOOK_BOOKINGCONFIRMED_REJECTED, BOOK_BOOKINGCONFIRMED_FINAL_RET, BOOK_BROKEN_FLOW,
      BOOK_BOOKING_STOP, BOOK_ON_HOLD, BOOK_BOOKINGCONFIRMED_ON_HOLD, BOOK_BOOKING_ERROR,
      BOOK_PAYMENT_RETRY_ONLY_BANK_TRANSFER, BOOK_PAYMENT_RETRY_ONLY_PAY_PAL,
      BOOK_PAYMENT_RETRY_BANK_TRANSFER_AND_CREDIT_CARDS, BOOK_PAYMENT_REPRICING,
      BOOK_PAYMENT_USER_INTERACTION_NEEDED_PAYPAL, BOOK_PAYMENT_USER_INTERACTION_NEEDED_TRUSTLY,
      BOOK_PAYMENT_USER_INTERACTION_NEEDED_KLARNA, LOGIN_SUCCESSFUL, LOGIN_PWD_ERROR,
      LOGIN_EMAIL_ERROR, USER_REGISTRATION_SUCCESS, GET_VISITS, SEARCH_MEMBERSHIP
  })

  public @interface MockServerConfiguration {
  }

  @Retention(RetentionPolicy.SOURCE) @IntDef({ EDREAMS, OPODO, GOVOYAGES, TRAVELLINK })
  public @interface Brand {
  }
}
