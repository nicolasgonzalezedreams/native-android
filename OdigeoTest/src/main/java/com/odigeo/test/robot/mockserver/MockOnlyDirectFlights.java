package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.ROUND_DIRECT_FLIGHTS_RESPONSE;
import static com.odigeo.test.robot.MockServerRobot.SEARCH_URL;

class MockOnlyDirectFlights implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(
        new ResponsesManager.Builder().addPostResponse(SEARCH_URL, ROUND_DIRECT_FLIGHTS_RESPONSE)
            .build());
  }
}
