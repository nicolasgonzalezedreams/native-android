package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.BASIC_CART_PASSENGER_RESPONSE;
import static com.odigeo.test.robot.MockServerRobot.GET_AVAILABLE_PRODUCTS_URL;

public class MockGetAvailableProducts implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(
        new ResponsesManager.Builder().addPostResponse(GET_AVAILABLE_PRODUCTS_URL,
            BASIC_CART_PASSENGER_RESPONSE).build());
  }
}
