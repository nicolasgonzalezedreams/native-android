package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by daniel.morales on 3/2/17.
 */
public class WalkthroughRobot extends BaseRobot {

  public WalkthroughRobot(@NonNull Context context) {
    super(context);
  }

  public void checkCorrectWalkthroughIsShown(String isShown) {
    boolean parsedResult = isShown.equals("true") ? true : false;
    String title = getHeaderText(parsedResult);

    onView(withId(R.id.txt_walkthrough_header)).check(matches(withText(title)));
  }

  public String getHeaderText(boolean isShown) {
    String cmsKey = isShown ? "odgwalkthroughview_paypal_title" : "sso_signin_ready_title_new_user";
    CharSequence title = LocalizablesFacade.getString(mContext, cmsKey).toString();

    return title.toString();
  }
}
