package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.consts.DistanceUnits;
import com.odigeo.app.android.lib.utils.PreferencesManager;
import com.odigeo.interactors.provider.MarketProviderInterface;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.hasCountryName;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withDistanceValue;
import static com.odigeo.test.espresso.OdigeoViewActions.openSpinnerPopup;
import static com.odigeo.test.espresso.OdigeoViewActions.setPassengerCount;
import static com.odigeo.test.espresso.OdigeoViewActions.setSwitchChecked;
import static junit.framework.Assert.assertEquals;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by Javier Marsicano on 20/12/16.
 */

public class SettingsRobot extends BaseRobot {

  private DistanceUnits mSelectedDistanceUnits;
  private boolean mPushNotificationsDisabled;

  public SettingsRobot(@NonNull Context context) {
    super(context);
  }

  public SettingsRobot selectCountry(String country) {
    onView(withId(R.id.button_settings_country)).perform(click());
    onData(hasCountryName(country)).perform(click());
    return this;
  }

  public SettingsRobot verifyCountry(String country) {
    MarketProviderInterface marketProvider =
        AndroidDependencyInjector.getInstance().provideMarketProvider();

    assertEquals("Country \"" + country + "\" was not configured in Settings ", country,
        marketProvider.getName());
    return this;
  }

  private SettingsRobot selectDistanceUnits(String unit) {
    onView(withId(R.id.odigeo_spinner)).perform(openSpinnerPopup());
    onData(withDistanceValue(unit)).perform(click());
    return this;
  }

  public SettingsRobot changeDistanceUnits() {
    String selectedValue;
    DistanceUnits units = PreferencesManager.readSettings(mContext).getDistanceUnit();
    if (units.equals(DistanceUnits.MILES)) {
      selectedValue = DistanceUnits.KILOMETERS.value();
      mSelectedDistanceUnits = DistanceUnits.KILOMETERS;
    } else {
      selectedValue = DistanceUnits.MILES.value();
      mSelectedDistanceUnits = DistanceUnits.MILES;
    }
    selectDistanceUnits(selectedValue);

    back(); //settings page saves all the changes when going back
    return this;
  }

  public SettingsRobot verifyDistanceUnits() {
    DistanceUnits actualUnits = PreferencesManager.readSettings(mContext).getDistanceUnit();
    assertEquals("Distance units was not actually changed in Settings", mSelectedDistanceUnits,
        actualUnits);
    return this;
  }

  public SettingsRobot selectPassengers(String numAdults, String numChildren, String numInfants) {
    onView(withId(R.id.button_settings_passengers)).perform(click());
    onView(withId(R.id.numberPickerAdults)).perform(setPassengerCount(Integer.valueOf(numAdults)));
    onView(withId(R.id.numberPickerKids)).perform(setPassengerCount(Integer.valueOf(numChildren)));
    onView(withId(R.id.numberPickerBabies)).perform(setPassengerCount(Integer.valueOf(numInfants)));
    onView(withId(android.R.id.button1)).perform(click());

    //an invisible ListPopUpWindow remains after passenger picker dialog is dismissed
    back();
    //settings page saves all the changes when going back
    back();
    return this;
  }

  public SettingsRobot verifyPassengers(String adults, String children, String infants) {
    int actualAdults = PreferencesManager.readSettings(mContext).getDefaultAdults();
    int actualChildren = PreferencesManager.readSettings(mContext).getDefaultKids();
    int actualInfants = PreferencesManager.readSettings(mContext).getDefaultBabies();

    final String errorMsg = String.format(
        "Passengers configuration '%s adults, %s children, %s infants' was not actually changed",
        adults, children, infants);

    assertEquals(errorMsg, Integer.parseInt(adults), actualAdults);
    assertEquals(errorMsg, Integer.parseInt(children), actualChildren);
    assertEquals(errorMsg, Integer.parseInt(infants), actualInfants);

    return this;
  }

  public SettingsRobot changePushNotificationsStatus() {
    if (PreferencesManager.readStatusNotifications(mContext)) {
      setPushNotificationsSwitchState(true);
      mPushNotificationsDisabled = false;
    } else {
      setPushNotificationsSwitchState(false);
      mPushNotificationsDisabled = true;
    }
    return this;
  }

  private SettingsRobot setPushNotificationsSwitchState(boolean checked) {
    onView(allOf(withId(R.id.switchPassengerPayment),
        withParent(withParent(withId(R.id.switchSettings))))).perform(setSwitchChecked(checked));
    return this;
  }

  public SettingsRobot verifyPushNotificationsStatus() {
    onView(allOf(withId(R.id.switchPassengerPayment),
        withParent(withParent(withId(R.id.switchSettings))))).check(matches(not(isChecked())));
    return this;
  }
}
