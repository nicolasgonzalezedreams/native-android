package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class FrequentFlierRobot extends BaseRobot {

  FrequentFlierRobot(@NonNull Context context) {
    super(context);
  }

  public FrequentFlierRobot checkFrequentFlier() {
    CharSequence toolbarTitle =
        LocalizablesFacade.getString(mContext, OneCMSKeys.FREQUENT_FLYER_NEW);
    onView(withText(toolbarTitle.toString())).check(matches(isDisplayed()));
    return this;
  }
}
