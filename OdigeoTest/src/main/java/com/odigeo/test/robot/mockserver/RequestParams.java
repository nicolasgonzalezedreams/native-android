package com.odigeo.test.robot.mockserver;

public class RequestParams {
  private final String method;
  private final String path;
  private final String mockResourceName;

  private RequestParams(String method, String path, String mockResourceName) {
    this.method = method;
    this.path = path;
    this.mockResourceName = mockResourceName;
  }

  public String getMethod() {
    return method;
  }

  public String getPath() {
    return path;
  }

  public String getMockResourceName() {
    return mockResourceName;
  }

  public static class Builder {
    private String method;
    private String path;
    private String mockResourceName;

    public Builder setMethod(String method) {
      this.method = method;
      return this;
    }

    public Builder setPath(String path) {
      this.path = path;
      return this;
    }

    public Builder setMockResourceName(String mockResourceName) {
      this.mockResourceName = mockResourceName;
      return this;
    }

    public RequestParams build() {
      return new RequestParams(method, path, mockResourceName);
    }
  }
}
