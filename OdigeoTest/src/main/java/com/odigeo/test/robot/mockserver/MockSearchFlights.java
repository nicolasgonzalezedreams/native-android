package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.ROUND_OK_RESPONSE;
import static com.odigeo.test.robot.MockServerRobot.SEARCH_URL;

class MockSearchFlights implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(
        new ResponsesManager.Builder().addPostResponse(SEARCH_URL, ROUND_OK_RESPONSE).build());
  }
}
