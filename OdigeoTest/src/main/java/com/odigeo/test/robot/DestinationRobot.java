package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import com.odigeo.app.android.lib.R;
import com.odigeo.dataodigeo.location.LocationController;
import com.odigeo.test.injection.AndroidTestInjector;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.odigeo.app.android.lib.adapters.DestinationAdapter.LOCATION;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withAdaptedData;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withDestinationType;
import static org.hamcrest.CoreMatchers.allOf;

public class DestinationRobot extends BaseRobot {
  private LocationController locationController;

  public DestinationRobot(@NonNull Context context) {
    super(context);
    locationController = AndroidTestInjector.getInstance().provideLocationController();
  }

  public DestinationRobot mockLocation(double latitude, double longitude) {
    locationController.mockLocation(latitude, longitude);
    return this;
  }

  public DestinationRobot useCurrentLocation() {
    onView(withId(R.id.layout_geo_location)).perform(click());
    return this;
  }

  public DestinationRobot checkDestinationError() {
    onView(withId(R.id.widgetErrorDestination)).check(matches(isDisplayed()));
    return this;
  }

  public DestinationRobot checkCitiesAreDisplayed() {
    onView(withId(R.id.listView_destinationActivity)).check(
        matches(allOf(withAdaptedData(), withDestinationType(LOCATION))));
    return this;
  }
}
