package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.espresso.ViewInteraction;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.interactors.CountriesInteractor;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class TravellerDetailsRobot extends BaseRobot {

  private CountriesInteractor mCountriesInteractor;
  private String deviceLang = LocaleUtils.getCurrentLanguageIso();

  //Locators
  private ViewInteraction firstName = onView(withId(R.id.edit_text_add_passenger_name));
  private ViewInteraction surName = onView(withId(R.id.edit_text_add_passenger_surname));
  private ViewInteraction birthDate = onView(withId(R.id.edit_text_add_passenger_birthdate));
  private ViewInteraction contactCountryOfResidence =
      onView(withId(R.id.edit_text_add_passenger_country_residence));
  private ViewInteraction contactEmail = onView(withId(R.id.edit_text_add_passenger_email));
  private ViewInteraction contactAddress = onView(withId(R.id.edit_text_add_passenger_address));
  private ViewInteraction contactPostCode = onView(withId(R.id.edit_text_add_passenger_zip_code));
  private ViewInteraction contactCity = onView(withId(R.id.edit_text_add_passenger_city));
  private ViewInteraction contactPhone = onView(withId(R.id.edit_text_add_passenger_cell_phone));
  private ViewInteraction submitBtn = onView(withId(R.id.sso_confirmation_add_traveller));
  private ViewInteraction deleteTraveller =
      onView(withId(R.id.button_add_traveller_reset_password));
  private ViewInteraction switchViewInteraction = onView(withId(R.id.switch_default_traveller));

  public TravellerDetailsRobot(@NonNull Context context) {
    super(context);
    mCountriesInteractor = AndroidDependencyInjector.getInstance().provideCountriesInteractor();
  }

  public TravellerDetailsRobot addTraveller(String name, String surname, String email,
      String address, String city, String postCode, String phoneNumber, String country) {
    sendKeys(firstName, name);
    sendKeys(surName, surname);
    addBirthDate();
    sendKeys(contactEmail, email);
    sendKeys(contactCity, city);
    sendKeys(contactAddress, address);
    sendKeys(contactPostCode, postCode);
    addCountry(country);
    sendKeys(contactPhone, phoneNumber);
    return this;
  }

  private void addCountry(String countryCode) {
    Country country = mCountriesInteractor.getCountryByCountryCode(deviceLang, countryCode);
    openCountrySelector().selectCountry(country);
  }

  private CountrySelectorRobot openCountrySelector() {
    contactCountryOfResidence.perform(click());
    return new CountrySelectorRobot(mContext);
  }

  private void addBirthDate() {
    birthDate.perform(click());
    onView(withId(android.R.id.button1)).perform(click());
  }

  public TravellersRobot submit() {
    submitBtn.perform(click());
    return new TravellersRobot(mContext);
  }

  public TravellersRobot deleteTraveller() {
    deleteTraveller.perform(scrollTo(), click());
    return new TravellersRobot(mContext);
  }

  public TravellerDetailsRobot editTraveller(String name, String surname, String email,
      String address, String city, String postCode, String phoneNumber, String country) {
    addTraveller(name, surname, email, address, city, postCode, phoneNumber, country);
    return this;
  }

  public void setMainTraveller() {
    switchViewInteraction.perform(scrollTo(), click());
    submitBtn.perform(click());
  }
}
