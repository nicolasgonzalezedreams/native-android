package com.odigeo.test.robot.mockserver;

import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ResponsesManager {
  private static final String METHOD_GET = "GET";
  private static final String METHOD_POST = "POST";
  private static final String METHOD_PUT = "PUT";
  private static final String METHOD_DELETE = "DELETE";
  private static final String EMPTY = "";
  private static final String TAG = ResponsesManager.class.getCanonicalName();
  private static final String NOT_FOUND_TEMPLATE = "No %s response found for path %s";

  private Map<String, LinkedHashMap<String, String>> wildcardsMap;
  private Map<String, HttpCodes> responsesCode;

  private ResponsesManager() {
    createMaps();
    populate();
  }

  private void createMaps() {
    responsesCode = new HashMap<>();
    wildcardsMap = new HashMap<>();
    wildcardsMap.put(METHOD_GET, new LinkedHashMap<String, String>());
    wildcardsMap.put(METHOD_POST, new LinkedHashMap<String, String>());
    wildcardsMap.put(METHOD_PUT, new LinkedHashMap<String, String>());
    wildcardsMap.put(METHOD_DELETE, new LinkedHashMap<String, String>());
  }

  private void populate() {
    addResponse(METHOD_GET, "/autoComplete/BER.*", "auto_complete_ber");
    addResponse(METHOD_GET, "/autoComplete/CGN.*", "auto_complete_cgn");
    addResponse(METHOD_GET, "/autoComplete/MAD.*", "auto_complete_mad");
    addResponse(METHOD_GET, "/autoComplete/PAR.*", "auto_complete_par");
    addResponse(METHOD_GET, "/bin/.*", "bin_check_master_card_credit");
    addResponse(METHOD_GET, "/nearestLocations.*", "destination_empty");
    addResponse(METHOD_GET, "/nearestLocations.*", "destination_mad");
    addResponse(METHOD_GET, "/carousel.*", "carousel_cta_ed");
    addResponse(METHOD_POST, "/notifications/.*", "disable_notifications_successful");
    addResponse(METHOD_POST, "/search/.*", "round_direct_flights");
    addResponse(METHOD_POST, "/addProduct.*", "basic_add_product");
    addResponse(METHOD_POST, "/getAvailableProducts.*", "basic_cart_passenger");
    addResponse(METHOD_POST, "/nearestLocations.*", "destination_mad");
    addResponse(METHOD_POST, "/createShoppingCart.*", "basic_shopping_cart");
    addResponse(METHOD_POST, "/addPassenger.*", "basic_cart_passenger");
    addResponse(METHOD_DELETE, "/notifications.*", "disable_notifications_successful");
    addResponse(METHOD_GET, "/booking.*", "import_trip_success");
    addResponse(METHOD_GET, "/tripInformation.*", "trip_information");
    addResponse(METHOD_PUT, "/notifications/.*", "empty");
    addResponse(METHOD_PUT, "/user/.*", "empty");
  }

  public Response obtainResponse(String method, String path) {
    String resourceName = findBy(method, path);
    if (resourceName.isEmpty()) {
      throw new RuntimeException(String.format(NOT_FOUND_TEMPLATE, method, path));
    }
    HttpCodes httpCode = responsesCode.get(resourceName);
    return new Response(httpCode, resourceName);
  }

  private String findBy(String method, String currentPath) {
    Map<String, String> resourcesMap = wildcardsMap.get(method);
    String filename = EMPTY;
    List<String> wildcards = new ArrayList<>(resourcesMap.keySet());
    Collections.reverse(wildcards);
    for (String wildcard : wildcards) {
      filename = responseNameFromWild(resourcesMap, wildcard, currentPath);
      if (!filename.isEmpty()) break;
    }
    return filename;
  }

  private String responseNameFromWild(Map<String, String> resourcesMap, String wildcard,
      String currentPath) {
    String filename = EMPTY;
    boolean match = checkWildcard(wildcard, currentPath);
    if (match) {
      Log.d(TAG, "FIND WILD :" + wildcard + " for :" + currentPath);
      filename = resourcesMap.get(wildcard);
    }
    return filename;
  }

  private boolean checkWildcard(String wildcard, String toCheck) {
    Pattern pattern = Pattern.compile(wildcard);
    Matcher matcher = pattern.matcher(toCheck);
    return matcher.matches();
  }

  private void addResponse(String method, String path, String responseName) {
    LinkedHashMap<String, String> resourcesMap = wildcardsMap.get(method);
    resourcesMap.put(path, responseName);
    responsesCode.put(responseName, HttpCodes.HTTP_OK);
  }

  private void addResponse(String method, String path, String responseName,
      HttpCodes responseCode) {
    LinkedHashMap<String, String> resourcesMap = wildcardsMap.get(method);
    resourcesMap.put(path, responseName);
    responsesCode.put(responseName, responseCode);
  }

  public static class Builder {
    private final ResponsesManager responses;

    public Builder() {
      responses = new ResponsesManager();
    }

    public Builder addGetResponse(String pathRegex, String responseName) {
      responses.addResponse(METHOD_GET, pathRegex, responseName);
      return this;
    }

    public Builder addGetResponseWithCode(String pathRegex, String responseName, HttpCodes code) {
      responses.addResponse(METHOD_GET, pathRegex, responseName, code);
      return this;
    }

    public Builder addPostResponseWithCode(String pathRegex, String responseName, HttpCodes code) {
      responses.addResponse(METHOD_POST, pathRegex, responseName, code);
      return this;
    }

    public Builder addPostResponse(String pathRegex, String responseName) {
      responses.addResponse(METHOD_POST, pathRegex, responseName);
      return this;
    }

    public Builder addPutResponse(String pathRegex, String responseName) {
      responses.addResponse(METHOD_PUT, pathRegex, responseName);
      return this;
    }

    public Builder addDeleteResponse(String pathRegex, String responseName) {
      responses.addResponse(METHOD_DELETE, pathRegex, responseName);
      return this;
    }

    public ResponsesManager build() {
      return responses;
    }
  }
}
