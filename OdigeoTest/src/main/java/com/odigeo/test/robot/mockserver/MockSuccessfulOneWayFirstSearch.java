package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.OW_OK_FIRST_RESPONSE;
import static com.odigeo.test.robot.MockServerRobot.SEARCH_URL;

class MockSuccessfulOneWayFirstSearch implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(
        new ResponsesManager.Builder().addPostResponse(SEARCH_URL, OW_OK_FIRST_RESPONSE).build());
  }
}
