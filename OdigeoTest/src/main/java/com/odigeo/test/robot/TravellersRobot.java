package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.espresso.ViewInteraction;
import android.view.View;
import com.odigeo.app.android.lib.R;
import org.hamcrest.Matcher;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.odigeo.test.espresso.matchers.RecyclerMatcher.atPosition;

public class TravellersRobot extends BaseRobot {

  public TravellersRobot(@NonNull Context context) {
    super(context);
  }

  ViewInteraction addTravellerButton = onView(withId(R.id.sso_travellers_empty_view_action));
  ViewInteraction storedTravellersTitle = onView(withId(R.id.sso_travellers_list_header_list));
  ViewInteraction storedTraveller = onView(withId(R.id.txtButtonWithHint_text));
  ViewInteraction noStoredTravellersTitle = onView(withId(R.id.sso_travellers_empty_view_title));

  public TravellerDetailsRobot openTravellerDetailsPage() {
    addTravellerButton.perform(click());
    return new TravellerDetailsRobot(mContext);
  }

  public void checkTravellerHasBeenAdded(String expectedName) {
    storedTravellersTitle.check(matches(isDisplayed()));
    onView(withText(expectedName)).check(matches(isDisplayed()));
  }

  public void checkThereAreNoTravellers() {
    noStoredTravellersTitle.check(matches(isDisplayed()));
  }

  public void checkTravellerHasBeenEdited(String expectedName) {
    onView(withText(expectedName)).check(matches(isDisplayed()));
  }

  public TravellerDetailsRobot openExistingTraveller() {
    storedTraveller.perform(click());
    return new TravellerDetailsRobot(mContext);
  }

  public void selectMainTraveller(String completeName) {
    onView(withText(completeName)).perform(click());

    TravellerDetailsRobot travellerDetailsRobot = new TravellerDetailsRobot(mContext);
    travellerDetailsRobot.setMainTraveller();
  }

  public void checkIsMainTraveller(String travellerName) {
    int mainTravellerPosition = 1;
    Matcher<View> recyclerViewMatcher = withId(R.id.sso_travellers_recyclerview);

    onView(recyclerViewMatcher).check(
        matches(atPosition(mainTravellerPosition, hasDescendant(withText(travellerName)))));
    onView(recyclerViewMatcher).check(
        matches(atPosition(mainTravellerPosition, R.id.imgTravellerTypeDefault)));
  }
}
