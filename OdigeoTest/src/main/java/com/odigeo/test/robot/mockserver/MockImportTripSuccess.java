package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.IMPORT_TRIP_SUCCESS_RESPONSE;
import static com.odigeo.test.robot.MockServerRobot.TRIP_URL;

class MockImportTripSuccess implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(
        new ResponsesManager.Builder().addPostResponse(TRIP_URL, IMPORT_TRIP_SUCCESS_RESPONSE)
            .build());
  }
}
