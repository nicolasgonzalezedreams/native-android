package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;

import static com.odigeo.test.AutomationConstants.CLASS_ALL;
import static com.odigeo.test.AutomationConstants.CLASS_BUSINESS;
import static com.odigeo.test.AutomationConstants.CLASS_ECONOMY;
import static com.odigeo.test.AutomationConstants.CLASS_FIRST;
import static com.odigeo.test.AutomationConstants.CLASS_PREMIUM;

public class BaseRobotWithCabinClass extends BaseRobot {

  BaseRobotWithCabinClass(@NonNull Context context) {
    super(context);
  }

  @NonNull CabinClassDTO getCabinClass(@NonNull String classToSelect) {
    CabinClassDTO cabinClass;
    switch (classToSelect) {
      case CLASS_ALL:
        cabinClass = CabinClassDTO.DEFAULT;
        break;
      case CLASS_ECONOMY:
        cabinClass = CabinClassDTO.TOURIST;
        break;
      case CLASS_PREMIUM:
        cabinClass = CabinClassDTO.PREMIUM_ECONOMY;
        break;
      case CLASS_BUSINESS:
        cabinClass = CabinClassDTO.BUSINESS;
        break;
      case CLASS_FIRST:
        cabinClass = CabinClassDTO.FIRST;
        break;
      default:
        throw new RuntimeException("Cabin class not found");
    }
    return cabinClass;
  }
}
