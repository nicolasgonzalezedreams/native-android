package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.espresso.ViewInteraction;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class LoginRobot extends BaseRobot {

  private ViewInteraction facebookLogin = onView(withId(R.id.btnFacebookSignUp));
  private ViewInteraction googleLogin = onView(withId(R.id.btnGoogleSignUp));
  private ViewInteraction emailTextField = onView(withId(R.id.etEmail));
  private ViewInteraction passwordTextField = onView(withId(R.id.etPassword));
  private ViewInteraction loginButton = onView(withId(R.id.btnLogin));
  private ViewInteraction loginErrorMsg = onView(withId(R.id.tvError));
  private String errMsgNotRegisteredEmail =
      LocalizablesFacade.getString(mContext, OneCMSKeys.SSO_ERROR_EMAIL_NOT_REGISTER_OR_ACTIVE)
          .toString();
  private String errMsgInvalidPassword =
      LocalizablesFacade.getString(mContext, OneCMSKeys.SSO_SIGNIN_ERROR_PASSWORD_EMAIL_INCORRECT)
          .toString();

  public LoginRobot(@NonNull Context context) {
    super(context);
  }

  public NavigationDrawerRobot emailLoginWithValidCredentials() {
    loginWithEmail("valid@email.com", "validPassword123");
    return new NavigationDrawerRobot(mContext);
  }

  public void emailLoginWithInvalidCredentials() {
    loginWithEmail("invalidemail@email.com", "invalidPassword123");
  }

  private void loginWithEmail(String email, String password) {
    sendKeys(emailTextField, email);
    sendKeys(passwordTextField, password);
    loginButton.perform(click());
  }

  public void loginWithFacebook() {
    facebookLogin.perform(click());
  }

  public void loginWithGoogle() {
    googleLogin.perform(click());
  }

  public void checkErrorEmailMessageIsDisplayed() {
    loginErrorMsg.check(matches(isDisplayed()));
    onView(withText(errMsgNotRegisteredEmail)).check(matches(isDisplayed()));
  }

  public void checkErrorPasswordMessageIsDisplayed() {
    loginErrorMsg.check(matches(isDisplayed()));
    onView(withText(errMsgInvalidPassword)).check(matches(isDisplayed()));
  }
}
