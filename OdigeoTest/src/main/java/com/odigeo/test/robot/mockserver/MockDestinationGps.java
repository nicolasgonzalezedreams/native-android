package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.DESTINATION_MAD_RESPONSE;
import static com.odigeo.test.robot.MockServerRobot.NEAREST_LOCATIONS_URL;

class MockDestinationGps implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(new ResponsesManager.Builder().addPostResponse(NEAREST_LOCATIONS_URL,
        DESTINATION_MAD_RESPONSE).build());
  }
}
