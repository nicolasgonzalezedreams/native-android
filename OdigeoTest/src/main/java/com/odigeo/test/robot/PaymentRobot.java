package com.odigeo.test.robot;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.test.espresso.ViewInteraction;
import android.widget.TextView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import org.hamcrest.core.AllOf;
import org.hamcrest.core.Is;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.init;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.release;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withChild;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.priceRemainsEqual;
import static com.odigeo.test.espresso.OdigeoViewActions.openSpinnerPopup;
import static com.odigeo.test.mock.MocksProvider.providesPaymentMocks;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.StringContains.containsString;

public class PaymentRobot extends BaseRobot {

  private String mContinueButtonText =
      LocalizablesFacade.getString(mContext, OneCMSKeys.COMMON_BUTTONCONTINUE).toString();
  private ViewInteraction monthSelector =
      onView(AllOf.allOf(withId(R.id.spMonthDate), withChild(isAssignableFrom(TextView.class))));
  private ViewInteraction yearSelector =
      onView(AllOf.allOf(withId(R.id.spYearDate), withChild(isAssignableFrom(TextView.class))));

  private ViewInteraction continueButton = onView(withText(mContinueButtonText));

  public PaymentRobot(@NonNull Context context) {
    super(context);
  }

  public PaymentRobot checkPaymentPageReached() {
    onView(withId(R.id.tbPayment)).check(matches(isDisplayed()));
    return this;
  }

  public void addCreditcardName(String creditcardName) {
    onView(withId(R.id.etNameOnCard)).perform(typeText(creditcardName), closeSoftKeyboard());
  }

  public void addCreditcard(String creditcardNumber) {
    onView(withId(R.id.etCreditCardNumber)).perform(typeText(creditcardNumber),
        closeSoftKeyboard());
  }

  public void addCreditcardExpirationMonth(String month) {
    if (!month.isEmpty()) {
      monthSelector.perform(openSpinnerPopup());
      onData(Is.is(month)).perform(click());
      onView(withId(R.id.spMonthDate)).check(matches(withSpinnerText(containsString(month))));
    }
  }

  public void addCreditcardExpirationYear(String year) {
    if (!year.isEmpty()) {
      yearSelector.perform(openSpinnerPopup());
      onData(Is.is(year)).perform(click());
      onView(withId(R.id.spYearDate)).check(matches(withSpinnerText(containsString(year))));
    }
  }

  public void addCVV(String cvv) {
    onView(withId(R.id.etCreditCardCVV)).perform(typeText(cvv), closeSoftKeyboard());
  }

  public void checkPaymentButtonIsNotAvailable() {
    continueButton.perform(scrollTo()).check(matches(not(isEnabled())));
  }

  public void checkPaymentButtonIsAvailable() {
    continueButton.perform(scrollTo()).check(matches((isEnabled())));
  }

  public ConfirmationRobot purchase() {
    continueButton.perform(scrollTo()).perform(click());
    return new ConfirmationRobot(mContext);
  }

  public void clickAndCheckOnPaypalPaymentMethod() {
    onView(withText(mContext.getString(R.string.paypal))).perform(scrollTo())
        .perform(click())
        .check(matches(isEnabled()));
  }

  public void clickAndCheckOnBankTransferPaymentMethod() {
    onView(withId(R.id.rbtnBanktransfer)).perform(scrollTo())
        .perform(click())
        .check(matches(isEnabled()));
  }

  public void clickAndCheckOnTrustlyPaymentMethod() {
    onView(withText(mContext.getString(R.string.trustly))).perform(scrollTo())
        .perform(click())
        .check(matches(isEnabled()));
  }

  public void clickAndCheckOnKlarnaPaymentMethod() {
    onView(withText(mContext.getString(R.string.klarna))).perform(scrollTo())
        .perform(click())
        .check(matches(isEnabled()));
  }

  public void clickOnAddPromoCodeButton() {
    onView(withId(R.id.btnExpandPromoCode)).perform(scrollTo()).perform(click());
  }

  public PaymentRobot clickOnValidateButton() {
    onView(withId(R.id.btnValidatePromoCode)).perform(click());
    return this;
  }

  public void clickOnPriceBreakDownSeeDetailsButton() {
    onView(withId(R.id.tvSeeDetails)).perform(click());
  }

  public void checkPromoCodeIsIncludedOnPriceBreakDown() {
    String priceBreakDownPromoCodeDiscount =
        LocalizablesFacade.getString(mContext, OneCMSKeys.PRICING_BREAKDOWN_PROMOCODE_DISCOUNT)
            .toString();
    ViewInteraction pricingBreakDownPromoCodeDiscount =
        onView(withText(priceBreakDownPromoCodeDiscount));
    pricingBreakDownPromoCodeDiscount.perform(scrollTo()).check(matches(isDisplayed()));
  }

  public void clickOnRemovePromoCodeButton() {
    onView(withId(R.id.ivDeletePromoCode)).perform(click());
  }

  public PaymentRobot clickOnConfirmRemovePromoCode() {
    String commonYes = LocalizablesFacade.getString(mContext, OneCMSKeys.COMMON_YES).toString();
    onView(withText(commonYes)).perform(click());
    return this;
  }

  public void checkPromoCodeFieldIsEmpty() {
    onView((withId(R.id.etPromoCode))).check(matches(withText("")));
  }

  public void insertPromoCode(String promocode) {
    onView(withId(R.id.etPromoCode)).perform(scrollTo())
        .perform(typeText(promocode), closeSoftKeyboard());
  }

  public void checkTotalPriceHasDecreased() {
    onView(withId(R.id.tvPriceBreakdownPrice)).check(matches(not(priceRemainsEqual(
        providesPaymentMocks().provideCreateShoppingCartResponse()
            .getShoppingCart()
            .getTotalPrice()
            .doubleValue()))));
  }

  public void checkTotalPriceRemainsEqual() {
    onView(withId(R.id.tvPriceBreakdownPrice)).check(matches(priceRemainsEqual(
        providesPaymentMocks().provideCreateShoppingCartResponse()
            .getShoppingCart()
            .getTotalPrice()
            .doubleValue())));
  }

  public void clickOnTACButton() {
    init();
    onView(withId(R.id.tvTAC)).perform(scrollTo()).perform(click());
  }

  public void checkTACOpenOptionsAppear() {
    String urlString =
        "http://www.edreams.com/images/mobile/WL/eDreams/Docs/General/Promocodes/ED_Promocodes_es_ES.pdf";
    intended(anyOf(hasAction(Intent.ACTION_CHOOSER), hasAction(Intent.ACTION_VIEW),
        hasExtra("URL", equalTo(urlString))));
    release();
  }

  public void tapOnExplicitTACCheckbox() {
    onView(withId(R.id.cbTACAAcceptance)).perform(scrollTo()).perform(click());
  }

  public void checkExplicitTACAcceptanceIsChecked() {
    onView(withId(R.id.cbTACAAcceptance)).check(matches(isChecked()));
  }

  public void tapOnTAC() {
    onView(withId(R.id.conditionsExplicitAcceptance)).perform(scrollTo()).perform(click());
  }

  public void tapOnAirlinesTAC() {
    onView(withId(R.id.airlineConditions)).perform(click());
  }

  public void checkOdigeoWebActivityReached() {
    onView(withId(R.id.webView)).check(matches(isDisplayed()));
  }

  public void checkPaymentRetryMessageIsShown() {
    onView(withId(R.id.dialog_expired_title)).check(matches(isDisplayed()));
  }

  public void checkPaymentRepricingMessageIsShown() {
    String repricingTitle =
        LocalizablesFacade.getString(mContext, OneCMSKeys.REPRICING_ALERT_TITLE).toString();
    onView(withText(repricingTitle)).check(matches(isDisplayed()));
  }

  public void checkExternalPaymentViewIsShown() {
    onView(withId(R.id.wbExternalPayment)).check(matches(isDisplayed()));
  }
}
