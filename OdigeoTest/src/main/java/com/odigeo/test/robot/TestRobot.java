package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.espresso.Espresso;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.BookingDBDAO;
import com.odigeo.dataodigeo.net.helper.semaphore.UIAutomationSemaphore;
import com.odigeo.test.espresso.PermissionTestHelper;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 22/11/16
 */

public class TestRobot extends BaseRobot {

  public TestRobot(@NonNull Context context) {
    super(context);
  }

  public TestRobot enableDataLoadInOdigeoSession() {
    ((OdigeoApp) mContext.getApplicationContext()).getOdigeoSession().setDataHasBeenLoaded(true);
    return this;
  }

  public TestRobot cleanDatabase() {
    mContext.deleteDatabase(OdigeoSQLiteOpenHelper.DATABASE_NAME);
    BookingDBDAO dbdao = new BookingDBDAO(mContext);
    dbdao.closeOdigeoDatabase();
    dbdao.getOdigeoDatabase();
    return this;
  }

  public TestRobot allowPermission(@NonNull String permission) {
    PermissionTestHelper.allowPermission(permission);
    return this;
  }

  public TestRobot denyPermission(@NonNull String permission) {
    PermissionTestHelper.denyPermission(permission);
    return this;
  }

  public TestRobot registerIdlingResource() {
    UIAutomationSemaphore.getInstance().reset();
    Espresso.registerIdlingResources(UIAutomationSemaphore.getInstance().getIdlingResource());
    return this;
  }

  public TestRobot unregisterIdlingResource() {
    Espresso.unregisterIdlingResources(UIAutomationSemaphore.getInstance().getIdlingResource());
    return this;
  }
}
