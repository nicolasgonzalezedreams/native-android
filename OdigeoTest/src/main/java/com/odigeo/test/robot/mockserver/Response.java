package com.odigeo.test.robot.mockserver;

class Response {

  private HttpCodes code;
  private String resourceBody;

  public Response(HttpCodes responseCode, String resourceBody) {
    this.code = responseCode;
    this.resourceBody = resourceBody;
  }

  public HttpCodes getResponseCode() {
    return code;
  }

  public void setResponseCode(HttpCodes responseCode) {
    this.code = responseCode;
  }

  public String getResourceBody() {
    return resourceBody;
  }

  public void setResourceBody(String resourceBody) {
    this.resourceBody = resourceBody;
  }
}
