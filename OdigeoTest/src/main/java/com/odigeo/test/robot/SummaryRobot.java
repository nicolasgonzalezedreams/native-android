package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.activities.OdigeoSummaryActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.assertCurrentActivityIs;

public class SummaryRobot extends BaseRobot {

  public SummaryRobot(@NonNull Context context) {
    super(context);
  }

  public SummaryRobot share() {
    onView(withId(R.id.menu_item_share_trip)).perform(click());
    return this;
  }

  public SummaryRobot checkSummaryPageDisplayed() {
    assertCurrentActivityIs(OdigeoSummaryActivity.class);
    return this;
  }

  public SummaryRobot checkCo2MessageIsDisplayed() {
    onView(withId(R.id.txtCO2TermsAndConditions)).perform(scrollTo()).check(matches(isDisplayed()));

    return this;
  }

  public SummaryRobot checkCo2MessageIsClickable() {
    onView(withId(R.id.txtCO2TermsAndConditions)).perform(scrollTo(), click());
    return this;
  }

  public SummaryRobot clickOnContinueButton() {
    onView(withId(R.id.btnContinuarSummary)).perform(scrollTo(), click());
    return this;
  }
}
