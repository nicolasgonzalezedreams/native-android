package com.odigeo.test.robot.passenger;

import com.odigeo.test.robot.PassengerRobot;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 31/01/17
 */
class NormalPassengerFiller implements PassengerFillerStrategy {
  @Override public void fill(PassengerRobot robot) {
    robot.writeName("Mock Name").writeSurname("Mock Surname");
  }
}
