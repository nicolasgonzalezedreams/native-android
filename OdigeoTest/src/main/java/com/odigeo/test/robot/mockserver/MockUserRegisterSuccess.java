package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.USER_REGISTRATION_SUCCESS;
import static com.odigeo.test.robot.MockServerRobot.USER_URL;

public class MockUserRegisterSuccess implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(new ResponsesManager.Builder().addPostResponse(USER_URL,
        USER_REGISTRATION_SUCCESS).build());
  }
}
