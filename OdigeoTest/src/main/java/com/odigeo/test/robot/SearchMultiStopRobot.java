package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.test.espresso.ViewInteraction;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.CommonLocalizables;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.withChild;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withDestinationCity;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withMonthAndYear;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withOdigeoHint;
import static org.hamcrest.core.AllOf.allOf;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 23/12/16
 */
public class SearchMultiStopRobot extends SearchRobot {

  private static final String DATE_FORMAT = "dd/MM/yyyy";
  private SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());

  public SearchMultiStopRobot(@NonNull Context context) {
    super(context);
  }

  @NonNull private ViewInteraction selectLegItem(int leg, @IdRes int idSearched) {
    String titleToBeMatched =
        CommonLocalizables.getInstance().getCommonLeg(mContext) + " " + (leg + 1);
    return onView(
        allOf(withId(idSearched), withParent(hasSibling(withChild(withText(titleToBeMatched))))));
  }

  private void clickOnLegItem(int leg, @IdRes int idToBeClicked) {
    selectLegItem(leg, idToBeClicked).perform(click());
  }

  private void selectDestiny(int leg, @NonNull String iataCode, boolean isDeparture) {
    int idItem = isDeparture ? R.id.panelDeparture : R.id.panelDestination;
    clickOnLegItem(leg, idItem);
    onView(withId(R.id.search_src_text)).perform(typeText(iataCode), closeSoftKeyboard());
    onData(withDestinationCity(iataCode)).perform(click());
  }

  @NonNull SearchMultiStopRobot selectDepartureCity(int leg, @NonNull String departure) {
    selectDestiny(leg, departure, true);
    return this;
  }

  @NonNull SearchMultiStopRobot selectArrivalCity(int leg, @NonNull String arrival) {
    selectDestiny(leg, arrival, false);
    return this;
  }

  @NonNull SearchMultiStopRobot selectDepartureDate(int leg) {
    clickOnLegItem(leg, R.id.panelDepartureDate);
    //selectDateOnUi(dateString);
    return this;
  }

  public void selectFirstDate() {
    selectDepartureDate(0);
  }

  public void selectSecondDate() {
    selectDepartureDate(1);
  }

  @NonNull SearchMultiStopRobot checkDepartureAndArrivalAreEqual(int departureLeg, int arrivalLeg,
      @NonNull String iataCode) {
    selectLegItem(departureLeg, R.id.panelDeparture).check(matches(withOdigeoHint(iataCode)));
    selectLegItem(arrivalLeg, R.id.panelDestination).check(matches(withOdigeoHint(iataCode)));
    return this;
  }

  @NonNull public ResultsRobot search() {
    onView(withId(R.id.button_acept_widgetSearchActivity)).perform(scrollTo(), click());
    return new ResultsRobot(mContext);
  }

  void selectDateOnUi(String dateString) {
    Calendar calendar = extractDate(dateString);
    onData(withMonthAndYear(calendar.get(Calendar.MONTH) + 1,
        calendar.get(Calendar.YEAR))).onChildView(
        withText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)))).perform(click());
  }

  @NonNull private Calendar extractDate(@NonNull String stringDate) {
    try {
      Calendar calendar = GregorianCalendar.getInstance();
      calendar.setTime(dateFormat.parse(stringDate));
      return calendar;
    } catch (ParseException e) {
      throw new IllegalArgumentException("Invalid date(dd/MM/yyyy): " + stringDate);
    }
  }
}
