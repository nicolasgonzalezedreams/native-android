package com.odigeo.test.robot;

import android.content.Context;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.test.espresso.ViewInteraction;
import android.support.test.uiautomator.UiDevice;
import android.view.View;
import com.odigeo.test.tests.BaseTest;
import org.hamcrest.Matcher;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.odigeo.app.android.lib.R.id;

public abstract class BaseRobot {
  @NonNull Context mContext;

  BaseRobot(@NonNull Context context) {
    mContext = context;
        /* It is necessary to run the tests on real devices, because some activities' inflate is
         * slow.*/
    SystemClock.sleep(100);
  }

  public BaseRobot back() {
    UiDevice.getInstance(getInstrumentation()).pressBack();
    return this;
  }

  public BaseRobot up() {
    onView(withId(id.home)).perform(click());
    return this;
  }

  public void sendKeys(ViewInteraction view, String text) {
    view.perform(scrollTo(), clearText(), typeText(text), closeSoftKeyboard());
  }

  public void takeScreenshot(@NonNull BaseTest baseTest, @NonNull String tag) {
    //        final Activity[] currentActivity = new Activity[1];
    //        UIAutomationSemaphore.getInstance().idleUp();
    //        InstrumentationRegistry.getInstrumentation().runOnMainSync(new Runnable() {
    //            @Override
    //            public void run() {
    //                Collection<Activity> activities = ActivityLifecycleMonitorRegistry.getInstance()
    //                        .getActivitiesInStage(RESUMED);
    //                if (activities != null && !activities.isEmpty()) {
    //                    currentActivity[0] = Iterables.getOnlyElement(activities);
    //                }
    //            }
    //        });
    //        if (currentActivity[0] != null) {
    //            Spoon.screenshot(currentActivity[0], tag, baseTest.getClass().getName(),
    //                    baseTest.getTestDescription());
    //        }
    //        UIAutomationSemaphore.getInstance().idleDown();
  }

  protected void waitUntilViewIsInflated(Matcher<View> matcher, int milliseconds) {
    final long startTime = System.currentTimeMillis();
    final long endTime = startTime + milliseconds;
    boolean isDisplayed;
    do {
      waitTwoHundredMillis();
      isDisplayed = onView(matcher) != null;
    } while (System.currentTimeMillis() < endTime && !isDisplayed);
  }

  protected void waitUntilViewIsDisplayed(Matcher<View> matcher, int milliseconds) {
    final long startTime = System.currentTimeMillis();
    final long endTime = startTime + milliseconds;
    boolean isDisplayed;
    do {
      waitTwoHundredMillis();
      isDisplayed = checkIfViewIsDisplayed(onView(matcher));
    } while (System.currentTimeMillis() < endTime && !isDisplayed);
  }
/*
    @NonNull
    CabinClassDTO getCabinClass(@NonNull String classToSelect) {
        CabinClassDTO cabinClass;
        switch (classToSelect) {
            case CLASS_ALL:
                cabinClass = CabinClassDTO.DEFAULT;
                break;
            case CLASS_ECONOMY:
                cabinClass = CabinClassDTO.TOURIST;
                break;
            case CLASS_PREMIUM:
                cabinClass = CabinClassDTO.PREMIUM_ECONOMY;
                break;
            case CLASS_BUSINESS:
                cabinClass = CabinClassDTO.BUSINESS;
                break;
            case CLASS_FIRST:
                cabinClass = CabinClassDTO.FIRST;
                break;
            default:
                throw new RuntimeException("Cabin class not found");
        }
        return cabinClass;
    }
*/

  private void waitTwoHundredMillis() {
    try {
      Thread.sleep(200);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private boolean checkIfViewIsDisplayed(ViewInteraction view) {
    try {
      view.check(matches(isDisplayed()));
      return true;
    } catch (Exception e) {
      return false;
    }
  }
}