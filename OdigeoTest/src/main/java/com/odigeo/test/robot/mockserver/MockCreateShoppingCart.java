package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.BASIC_SHOPPING_CART_RESPONSE;
import static com.odigeo.test.robot.MockServerRobot.CREATE_SHOPPING_CART_URL;

public class MockCreateShoppingCart implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(new ResponsesManager.Builder().addPostResponse(CREATE_SHOPPING_CART_URL,
        BASIC_SHOPPING_CART_RESPONSE).build());
  }
}
