package com.odigeo.test.robot.passenger;

import android.support.annotation.IntDef;
import android.util.SparseArray;
import com.odigeo.test.robot.PassengerRobot;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 31/01/17
 */
public class PassengerFillerHelper {
  public static final int NORMAL = 0;
  private static SparseArray<PassengerFillerStrategy> fillersMap = new SparseArray<>();

  static {
    fillersMap.put(NORMAL, new NormalPassengerFiller());
  }

  private PassengerRobot passengerRobot;

  public PassengerFillerHelper(PassengerRobot passengerRobot) {
    this.passengerRobot = passengerRobot;
  }

  public void fillPassenger(@PassengerFiller int type) {
    PassengerFillerStrategy filler = fillersMap.get(type);
    if (filler == null) {
      throw new RuntimeException("Filler not found");
    }
    filler.fill(passengerRobot);
  }

  @IntDef({ NORMAL }) @Retention(RetentionPolicy.SOURCE) @interface PassengerFiller {
  }
}
