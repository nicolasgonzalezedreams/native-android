package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.CAROUSEL_CTA_ED_RESPONSE;
import static com.odigeo.test.robot.MockServerRobot.CAROUSEL_URL;

/**
 * Created by daniel.morales on 9/1/17.
 */
class MockCarousel2CTAED implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(
        new ResponsesManager.Builder().addGetResponse(CAROUSEL_URL, CAROUSEL_CTA_ED_RESPONSE)
            .build());
  }
}
