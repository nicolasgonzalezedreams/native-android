package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.test.espresso.NestedScrollViewActions;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItem;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.odigeo.test.espresso.OdigeoViewActions.clickButton;
import static com.odigeo.test.espresso.matchers.DrawableMatcher.withDrawable;
import static com.odigeo.test.espresso.matchers.MultipleViewsMatcher.withIndex;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.hasTripItem;
import static com.odigeo.test.espresso.matchers.RecyclerMatcher.atPosition;
import static org.hamcrest.CoreMatchers.allOf;

public class MyTripsRobot extends BaseRobot {
  private static final String DEPARTURE_IATA = "MNL";
  private static final String ARRIVAL_IATA = "NRT";
  private static final long DEPARTURE_DATE = 1480121400000L;

  public MyTripsRobot(@NonNull Context context) {
    super(context);
  }

  public ImportTripRobot navigateToImportTrip() {
    onView(withId(R.id.btnImportTrip)).perform(click());
    return new ImportTripRobot(mContext);
  }

  public MyTripsRobot checkImportSuccess() {
    onView(
        allOf(withId(R.id.bookingsListRecyclerView), isAssignableFrom(RecyclerView.class))).check(
        matches(hasTripItem(DEPARTURE_IATA, ARRIVAL_IATA, DEPARTURE_DATE)));
    return this;
  }

  public MyTripsRobot goToRateApp() {
    onView(withId(R.id.btn_like)).perform(click());
    return this;
  }

  public MyTripsRobot checkFeedbackPage() {
    String feedbackTitle =
        LocalizablesFacade.getString(mContext, OneCMSKeys.LEAVEFEEDBACK_HEADER).toString();
    onView(allOf(withParent(withId(R.id.action_bar)), withText(feedbackTitle))).check(
        matches(isDisplayed()));

    onView(withId(R.id.tv_apprate_thanks_title)).check(matches(isDisplayed()));

    return this;
  }

  public MyTripsRobot checkImportFail() {
    String textImportFailed =
        LocalizablesFacade.getString(mContext, OneCMSKeys.MY_TRIPS_IMPORT_ERROR).toString();
    onView(withText(textImportFailed)).check(matches(isDisplayed()));
    return this;
  }

  public MyTripsRobot openTripDetails(Booking booking) {
    String cityName = booking.getFirstSegment().getLastSection().getTo().getCityName();

    onView(withId(R.id.bookingsListRecyclerView)).perform(
        actionOnItem(hasDescendant(withText(cityName)), click()));
    return this;
  }

  public MyTripsRobot checkTripDetailsAreDisplayed(Booking booking) {
    onView(withId(R.id.booking_destination_title)).check(
        matches(withText(booking.getArrivalCityName(0))));
    onView(withId(R.id.tvBookingDeparture)).check(
        matches(withText(booking.getDepartureAirportCode())));
    return this;
  }

  public MyTripsRobot clickOnCarsButton() {
    onView(withId(R.id.btnCar)).perform(NestedScrollViewActions.nestedScrollTo(), click());
    return this;
  }

  public MyTripsRobot checkCarsWebview() {
    onView(withId(R.id.webView)).check(matches(isDisplayed()));
    return this;
  }

  public MyTripsRobot openHelp() {
    onView(withId(R.id.helpview_container)).perform(NestedScrollViewActions.nestedScrollTo(),
        click());
    return this;
  }

  public MyTripsRobot checkRateAppWidgetTitle() {
    String rateAppTitle =
        LocalizablesFacade.getString(mContext, OneCMSKeys.MYTRIPSVIEWCONTROLLER_RATE_TITLE)
            .toString();

    onView(withId(R.id.bookingsListRecyclerView)).check(
        matches(atPosition(0, hasDescendant(withText(rateAppTitle)))));
    return this;
  }

  public MyTripsRobot openHotels() {
    onView(withId(R.id.btnHotel)).perform(clickButton());
    return this;
  }

  public MyTripsRobot openCars() {
    onView(withId(R.id.btnCar)).perform(clickButton());
    return this;
  }

  public MyTripsRobot checkWebView() {
    onView(withId(R.id.webView)).check(matches(isDisplayed()));
    return this;
  }

  public MyTripsRobot checkOneWayArrowShown() {
    onView(withIndex(withId(R.id.customImageView), 0)).check(
        matches(withDrawable(R.drawable.arrow_one_way_trip)));
    return this;
  }

  public MyTripsRobot checkRoundedArrowShown() {
    onView(withIndex(withId(R.id.customImageView), 0)).check(
        matches(withDrawable(R.drawable.arrow_round_trip)));
    return this;
  }

  public MyTripsRobot checkTextIsShown(String text) {
    onView(withIndex(withText(text), 0));
    return this;
  }
}