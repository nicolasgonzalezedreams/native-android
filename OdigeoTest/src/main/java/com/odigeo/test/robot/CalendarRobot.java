package com.odigeo.test.robot;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.widget.ImageView;
import com.odigeo.app.android.lib.R;
import com.odigeo.tools.DateUtils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.hasNullImageDrawable;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.isPreviousMonthVisible;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withMonthAndYear;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withTypeface;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.not;

public class CalendarRobot extends BaseRobot {

  public CalendarRobot(@NonNull Context context) {
    super(context);
  }

  public CalendarRobot checkWeekendsAreMarked() {
    Calendar calendar = Calendar.getInstance();
    Calendar calendarNextYear = Calendar.getInstance();
    calendarNextYear.add(Calendar.YEAR, 1);

    Typeface typeface;

    while (calendar.getTime().compareTo(calendarNextYear.getTime()) < 0) {
      if (DateUtils.isWeekend(calendar.getTime())) {
        typeface = Typeface.defaultFromStyle(Typeface.BOLD);
      } else {
        typeface = null;
      }

      onData(withMonthAndYear(calendar.get(Calendar.MONTH) + 1,
          calendar.get(Calendar.YEAR))).onChildView(
          withText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))))
          .check(matches(withTypeface(typeface)));

      calendar.add(Calendar.DAY_OF_MONTH, 1);
    }

    return this;
  }

  public CalendarRobot checkHolidaysAreMarked(List<Date> holidays) {
    Calendar calendar = Calendar.getInstance();
    Calendar calendarActual = Calendar.getInstance();

    for (Date date : holidays) {
      calendar.setTime(date);
      if (calendar.getTime().compareTo(calendarActual.getTime()) > 0) {
        onData(withMonthAndYear(calendar.get(Calendar.MONTH) + 1,
            calendar.get(Calendar.YEAR))).onChildView(allOf(isAssignableFrom(ImageView.class),
            hasSibling(withText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))))))
            .check(matches(not(hasNullImageDrawable())));
      }
    }
    return this;
  }

  public CalendarRobot checkAnyDayIsMarkedAsHoliday() {
    Calendar calendar = Calendar.getInstance();
    Calendar calendarNextYear = Calendar.getInstance();
    calendarNextYear.add(Calendar.YEAR, 1);

    while (calendar.getTime().compareTo(calendarNextYear.getTime()) <= 0) {
      onData(withMonthAndYear(calendar.get(Calendar.MONTH) + 1,
          calendar.get(Calendar.YEAR))).onChildView(allOf(isAssignableFrom(ImageView.class),
          hasSibling(withText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))))))
          .check(matches(hasNullImageDrawable()));

      calendar.add(Calendar.DAY_OF_MONTH, 1);
    }

    return this;
  }

  public CalendarRobot checkRegionalLegendIsNotShown() {
    onData(anything()).atPosition(0)
        .onChildView(withId(com.odigeo.app.android.lib.R.id.tvRegionCaption))
        .check(matches(not(isDisplayed())));

    return this;
  }

  public CalendarRobot selectDate(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);

    onData(withMonthAndYear(calendar.get(Calendar.MONTH) + 1,
        calendar.get(Calendar.YEAR))).onChildView(
        withText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)))).perform(click());

    return this;
  }

  public CalendarRobot skipReturnSelection() {
    onView(withId(com.odigeo.app.android.lib.R.id.skip_return_button)).perform(click());
    return this;
  }

  public CalendarRobot selectReturnDate(Date returnDate) {
    return selectDate(returnDate);
  }

  public CalendarRobot selectDepartureAndReturnDate(Date departureDate, Date returnDate) {
    selectDate(departureDate);
    selectDate(returnDate);
    return this;
  }

  public CalendarRobot submitDates() {
    onView(withId(com.odigeo.app.android.lib.R.id.confirmation_button)).perform(click());
    return this;
  }

  public CalendarRobot isPreviousMonthShown(Context context, Date selectedDate) {
    DateFormat monthNameFormat =
        new SimpleDateFormat(context.getString(com.odigeo.calendar.R.string.month_name_format),
            Locale.getDefault());
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(selectedDate);
    calendar.add(Calendar.MONTH, -1);

    String currentMonth = monthNameFormat.format(calendar.getTime());

    onView(withId(R.id.calendar_view)).check(matches(not(isPreviousMonthVisible(currentMonth))));

    return this;
  }
}
