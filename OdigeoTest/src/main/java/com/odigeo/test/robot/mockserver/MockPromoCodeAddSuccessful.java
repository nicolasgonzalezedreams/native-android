package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.ADD_PRODUCT_URL;
import static com.odigeo.test.robot.MockServerRobot.PROMO_CODE_ADD_SUCCESSFUL_RESPONSE;

class MockPromoCodeAddSuccessful implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(new ResponsesManager.Builder().addPostResponse(ADD_PRODUCT_URL,
        PROMO_CODE_ADD_SUCCESSFUL_RESPONSE).build());
  }
}