package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.espresso.contrib.NavigationViewActions;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class NavigationDrawerRobot extends BaseRobot {

  private String textNavDrawerHeader =
      LocalizablesFacade.getString(mContext, OneCMSKeys.SSO_ND_WHY_TO_LOGIN).toString();
  private String textNavDrawerSubtitle =
      LocalizablesFacade.getString(mContext, OneCMSKeys.SSO_ND_SAVE_RECENT_SEARCH).toString();
  private String textLoginOrRegister =
      LocalizablesFacade.getString(mContext, OneCMSKeys.SSO_ND_LOGIN_OR_REGISTER).toString();

  private ViewInteraction mNavigationDrawerLayout = onView(withId(R.id.drawerLayout));
  private ViewInteraction mNavigationDrawer = onView(withId(R.id.navigationDrawer));
  private ViewInteraction mLoggedAreaArrow = onView(withId(R.id.ivOptionsArrow));
  private ViewInteraction mSearchFlightsButton = onView(withId(R.id.rlFlight));

  private ViewInteraction mNavigationDrawerNotLoggedHeader = onView(withText(textNavDrawerHeader));
  private ViewInteraction mNavigationDrawerNotLoggedSubtitle =
      onView(withText(textNavDrawerSubtitle));
  private ViewInteraction mBtnLoginOrRegister = onView(withText(textLoginOrRegister));
  private ViewInteraction loggedInUserName = onView(withId(R.id.userNameNavigationDrawer));
  private ViewInteraction loggedInUserEmail = onView(withId(R.id.userEmailNavigationDrawer));

  public NavigationDrawerRobot(@NonNull Context context) {
    super(context);
  }

  public NavigationDrawerRobot logout() {
    mNavigationDrawerLayout.perform(DrawerActions.open());
    mLoggedAreaArrow.perform(click());
    mNavigationDrawer.perform(NavigationViewActions.navigateTo(R.id.logOutItem));
    RobotUtils.waitTime(500);
    ViewInteraction viewinteraction = onView(withId(android.R.id.button1)).inRoot(isDialog());
    viewinteraction.perform(click());
    return this;
  }

  public NavigationDrawerRobot checkUserNotLogged() {
    mNavigationDrawerLayout.perform(DrawerActions.open());
    mNavigationDrawerNotLoggedHeader.check(matches(isDisplayed()));
    mNavigationDrawerNotLoggedSubtitle.check(matches(isDisplayed()));
    mBtnLoginOrRegister.check(matches(isDisplayed()));
    return this;
  }

  public NavigationDrawerRobot openSearchScreen() {
    mSearchFlightsButton.perform(click());
    return this;
  }

  public void checkUserIsLoggedIn(){
    waitUntilViewIsDisplayed(withId(R.id.drawerLayout), 7000);
    mNavigationDrawerLayout.perform(DrawerActions.open());
    loggedInUserEmail.check(matches(isDisplayed()));
    loggedInUserName.check(matches(isDisplayed()));
  }
}
