package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.BOOK_BOOKINGCONFIRMED_RETAINED_RESPONSE;
import static com.odigeo.test.robot.MockServerRobot.BOOK_URL;

public class MockBookBookingConfirmedRetained implements MockServerStrategy {
  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(new ResponsesManager.Builder().addPostResponse(BOOK_URL,
        BOOK_BOOKINGCONFIRMED_RETAINED_RESPONSE).build());
  }
}
