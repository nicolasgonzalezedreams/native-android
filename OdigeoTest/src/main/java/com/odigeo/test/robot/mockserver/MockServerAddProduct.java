package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.ADD_PRODUCT_URL;
import static com.odigeo.test.robot.MockServerRobot.BASIC_ADD_PRODUCT_RESPONSE;

public class MockServerAddProduct implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(
        new ResponsesManager.Builder().addPostResponse(ADD_PRODUCT_URL, BASIC_ADD_PRODUCT_RESPONSE)
            .build());
  }
}
