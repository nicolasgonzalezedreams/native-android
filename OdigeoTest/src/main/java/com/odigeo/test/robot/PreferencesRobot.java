package com.odigeo.test.robot;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import com.google.gson.Gson;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.test.injection.AndroidTestInjector;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;
import static com.odigeo.app.android.lib.fragments.OdigeoResultsFragment.FULL_TRANSPARENCY_FIRST_TIME;
import static com.odigeo.app.android.lib.fragments.OdigeoResultsFragment.SHOW_FULL_TRANSPARENCY_DIALOG;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 22/11/16
 */

public class PreferencesRobot extends BaseRobot {
  private PreferencesControllerInterface mPreferencesController;

  public PreferencesRobot(@NonNull Context context) {
    super(context);
    mPreferencesController = AndroidTestInjector.getInstance().providePreferencesController();
  }

  public PreferencesRobot disableFullTransparency() {
    mPreferencesController.saveBooleanValue(FULL_TRANSPARENCY_FIRST_TIME, true);
    mPreferencesController.saveBooleanValue(SHOW_FULL_TRANSPARENCY_DIALOG, false);
    return this;
  }

  public void clean() {
    mPreferencesController.deleteAllPreferences();
  }

  public void clearRecentDestinies() {
    mContext.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_DEPARTURE_CITIES, MODE_PRIVATE)
        .edit()
        .clear()
        .apply();
    mContext.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_RETOUR_CITIES, MODE_PRIVATE)
        .edit()
        .clear()
        .apply();
  }

  public void saveMockSearchedCities(String fileNamePreferences, List<City> cities) {
    List<String> listCities = new ArrayList<>();
    for (City city : cities) {
      String cityString = new Gson().toJson(city);
      listCities.add(cityString);
    }

    SharedPreferences.Editor editor =
        mContext.getSharedPreferences(fileNamePreferences, MODE_PRIVATE).edit();
    editor.putString(Constants.PREFERENCE_LIST_CITIES, new Gson().toJson(listCities)).apply();
  }

  public void cleanSettingsPreferences() {
    mContext.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE_SETTINGS, Context.MODE_PRIVATE)
        .edit()
        .clear()
        .apply();
    //passengers must be reset in configuration component also
    //must be the same values as default values in brandconfiguration.xml
    Configuration.getInstance().getGeneralConstants().setDefaultNumberOfAdults(1);
    Configuration.getInstance().getGeneralConstants().setDefaultNumberOfKids(0);
    Configuration.getInstance().getGeneralConstants().setDefaultNumberOfInfants(0);

    //distance unit must be reset in application also
    ((OdigeoApp) mContext.getApplicationContext()).setSettingsUnits(null);

    resetMarket();
  }

  private void resetMarket() {
    // reset application Locale in order to actually reset Market
    Resources res = mContext.getResources();
    android.content.res.Configuration conf = res.getConfiguration();
    conf.locale = Locale.getDefault();
    res.updateConfiguration(conf, res.getDisplayMetrics());

    //perform language loading with default locale
    ((OdigeoApp) mContext.getApplicationContext()).loadSettings();
  }

  public void setShouldShowDropOffCards(boolean shouldShowDropOffCards) {
    mPreferencesController.saveBooleanValue(PreferencesControllerInterface.SHOULD_ADD_DROPOFF_CARD,
        shouldShowDropOffCards);
  }
}