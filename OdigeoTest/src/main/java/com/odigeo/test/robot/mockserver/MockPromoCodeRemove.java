package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.PROMO_CODE_REMOVE_SUCCESSFUL;
import static com.odigeo.test.robot.MockServerRobot.REMOVE_PRODUCT_URL;

class MockPromoCodeRemove implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(new ResponsesManager.Builder().addPostResponse(REMOVE_PRODUCT_URL,
        PROMO_CODE_REMOVE_SUCCESSFUL).build());
  }
}
