package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.LOGIN_OK;
import static com.odigeo.test.robot.MockServerRobot.USER_URL;

public class MockLoginSuccessful implements MockServerStrategy {
  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(
        new ResponsesManager.Builder().addGetResponse(USER_URL, LOGIN_OK)
            .build());
  }
}
