package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.shoppingCart.Insurance;
import com.odigeo.data.entity.shoppingCart.InsuranceOffer;
import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by Javier Marsicano on 21/02/17.
 */

public class InsuranceRobot extends BaseRobot {

  public InsuranceRobot(@NonNull Context context) {
    super(context);
  }

  public InsuranceRobot checkInsurancePageReached() {
    onView(withId(R.id.tvInsuranceInfoHeader)).check(matches(isDisplayed()));
    return this;
  }

  public void clickOnInsuranceWidget(Insurance insurance) {
    String insuranceTitleKey = OneCMSKeys.INSURANCE_VIEW_CONTROLLER
        + insurance.getPolicy().toLowerCase()
        + OneCMSKeys.INSURANCE_TITLE;
    String insuranceTitle = LocalizablesFacade.getString(mContext, insuranceTitleKey).toString();
    onView(withText(insuranceTitle)).perform(scrollTo()).perform(click());
  }

  public void clickOnInsuranceDeclineWidget() {
    String insuranceDeclineTitle =
        LocalizablesFacade.getString(mContext, OneCMSKeys.INSURANCE_WIDGET_DECLINE_TITLE)
            .toString();
    onView(withText(insuranceDeclineTitle)).perform(scrollTo()).perform(click());
  }

  public void clickOnContinueButton() {
    onView(withId(R.id.btnContinue)).perform(scrollTo()).perform(click());
  }

  public void checkCheckBoxIsUnselected(List<InsuranceOffer> insuranceOfferList) {
    for (InsuranceOffer insuranceOffer : insuranceOfferList) {
      String insuranceTitleKey =
          OneCMSKeys.INSURANCE_VIEW_CONTROLLER + insuranceOffer.getInsurances()
              .get(0)
              .getPolicy()
              .toLowerCase() + OneCMSKeys.INSURANCE_TITLE;
      String insuranceTitle = LocalizablesFacade.getString(mContext, insuranceTitleKey).toString();
      onView(allOf(withId(R.id.cbInsurance), hasSibling(withText(insuranceTitle)))).perform(
          scrollTo()).check(matches(not(isChecked())));
    }

    onView(withId(R.id.cbInsuranceDecline)).perform(scrollTo()).check(matches(not(isChecked())));
  }

  public void checkEnableContinueButton() {
    onView(withId(R.id.btnContinue)).perform(scrollTo()).check(matches(not(isEnabled())));
  }

  public InsuranceRobot openFlightDetails() {
    onView(withId(R.id.menu_item_goto_summary)).perform(click());
    return this;
  }

  public void checkFlightDetailOpenned() {
    String topLabelSummary = LocalizablesFacade.getString(mContext,
        OneCMSKeys.DETAILSVIEWCONTROLLER_INFORMATIVEMODULE_TEXT).toString();
    String title = LocalizablesFacade.getString(mContext, OneCMSKeys.SUMMARY_TITLE).toString();
    onView(withText(title)).check(matches(isDisplayed()));
    onView(withId(R.id.tvSummaryTopLabel)).check(matches(withText(topLabelSummary)));
  }
}
