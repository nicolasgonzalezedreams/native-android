package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 22/12/16
 */
interface MockServerStrategy {
  void configure(MockServerRobot robot);
}
