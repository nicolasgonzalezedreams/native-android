package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.espresso.ViewInteraction;
import android.support.v7.widget.Toolbar;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.isAllTextVisible;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withTextSizeLessThan;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withToolbarTitle;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;

public class CarouselRobot extends BaseRobot {

  private ViewInteraction viewpager = onView(withId(R.id.vpCards));

  private ViewInteraction ctaLeft =
      onView(allOf(isDisplayed(), withId(R.id.TvCampaignActionButtonOne)));
  private ViewInteraction ctaRight =
      onView(allOf(isDisplayed(), withId(R.id.TvCampaignActionButtonTwo)));

  private ViewInteraction departureCity = onView(withId(R.id.TvDropOffDeparture));
  private ViewInteraction arrivalCity = onView(withId(R.id.TvDropOffArrival));
  private ViewInteraction paxNumber = onView(withId(R.id.TvDropOffPassengers));
  private ViewInteraction dropOffDates = onView(withId(R.id.TvDropOffDates));
  private ViewInteraction dropOffBody = onView(withId(R.id.TvDropOffBody));
  private ViewInteraction dropOffResearch = onView(withId(R.id.TvDropOffActionButtonOne));
  private ViewInteraction dropOffEdit = onView(withId(R.id.TvDropOffActionButtonTwo));

  public CarouselRobot(@NonNull Context context) {
    super(context);
  }

  public CarouselRobot checkCTAButtonsSize(String maxSize) {
    ctaLeft.check(matches(allOf(isDisplayed(), withTextSizeLessThan(Integer.valueOf(maxSize)))));
    ctaRight.check(matches(allOf(isDisplayed(), withTextSizeLessThan(Integer.valueOf(maxSize)))));

    return this;
  }

  public CarouselRobot checkAllTextIsVisible() {
    ctaLeft.check(matches(allOf(isDisplayed(), isAllTextVisible())));
    ctaRight.check(matches(allOf(isDisplayed(), isAllTextVisible())));

    return this;
  }

  public CarouselRobot checkNotAllTextIsVisible() {
    ctaLeft.check(matches(allOf(isDisplayed(), not(isAllTextVisible()))));
    ctaRight.check(matches(allOf(isDisplayed(), not(isAllTextVisible()))));

    return this;
  }

  public CarouselRobot goToNextCard() {
    viewpager.perform(swipeLeft());

    return this;
  }

  public CarouselRobot checkCTAButtonSizeBiggerThan(String maxSize) {
    ctaLeft.check(
        matches(allOf(isDisplayed(), not(withTextSizeLessThan(Integer.valueOf(maxSize))))));
    return this;
  }

  public CarouselRobot clickOnEditSearch() {
    dropOffEdit.perform(click());
    return this;
  }

  public CarouselRobot clickOnReSearch() {
    dropOffResearch.perform(click());
    return this;
  }

  public CarouselRobot isSearchScreenShowing() {
    String title =
        LocalizablesFacade.getString(mContext, OneCMSKeys.SEARCHVIEWCONTROLLER_TITLE).toString();
    onView(isAssignableFrom(Toolbar.class)).check(matches(withToolbarTitle(is(title))));
    return this;
  }

  public CarouselRobot isResultsPageShowing() {
    String title =
        LocalizablesFacade.getString(mContext, OneCMSKeys.RESULTSVIEWCONTROLLER_TITLE_RESULTS)
            .toString();
    onView(isAssignableFrom(Toolbar.class)).check(matches(withToolbarTitle(is(title))));
    return this;
  }

  public CarouselRobot checkDropOffCardIsNotShowing() {
    departureCity.check(doesNotExist());
    arrivalCity.check(doesNotExist());
    dropOffDates.check(doesNotExist());
    dropOffBody.check(doesNotExist());
    paxNumber.check(doesNotExist());
    dropOffResearch.check(doesNotExist());
    dropOffEdit.check(doesNotExist());

    return this;
  }
}
