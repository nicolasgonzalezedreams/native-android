package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.espresso.ViewInteraction;
import android.view.View;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.test.injection.AndroidTestInjector;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.hamcrest.Matcher;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isSelected;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.odigeo.test.AutomationConstants.DESTINATION_CITY_1;
import static com.odigeo.test.AutomationConstants.DESTINATION_CITY_2;
import static com.odigeo.test.AutomationConstants.DESTINATION_IATA_1;
import static com.odigeo.test.AutomationConstants.DESTINATION_IATA_2;
import static com.odigeo.test.AutomationConstants.DESTINATION_IATA_3;
import static com.odigeo.test.AutomationConstants.DESTINATION_IATA_4;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.isDepartureDateCorrect;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withCabinClass;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withDestinationCity;
import static com.odigeo.test.espresso.OdigeoViewActions.setPassengerCount;
import static com.odigeo.test.espresso.OdigeoViewActions.setSwitchChecked;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.AllOf.allOf;

/**
 * @author Javier Marsicano
 * @version 1.0
 * @since 24/10/16.
 */
public class SearchRobot extends BaseRobotWithCabinClass {
  private static final String CHECK_SELECT_DATE_IS_CORRECT =
      "Check if selected date is the correct one";
  private static final int FIRST_LEG = 0;
  private static final int SECOND_LEG = 1;

  private Calendar today;
  private Calendar oneMonthForward;

  //Locators
  private SearchesHandlerInterface mSearchesHandler;
  private ViewInteraction residentsDialog = onView(withText(
      LocalizablesFacade.getString(mContext, OneCMSKeys.RESIDENTS_DIALOG_TEXT).toString()));
  private Matcher<View> oneWayTabMatcher = withText(
      LocalizablesFacade.getString(mContext, OneCMSKeys.ONE_WAY_TAB_TEXT).toString().toUpperCase());
  private ViewInteraction oneWayTab = onView(oneWayTabMatcher);
  private Matcher<View> roundTripTabMatcher = withText(
      LocalizablesFacade.getString(mContext, OneCMSKeys.ROUND_TRIP_TAB_TEXT)
          .toString()
          .toUpperCase());
  private ViewInteraction roundTripTab = onView(roundTripTabMatcher);
  private Matcher<View> multiTripTabMatcher = withText(
      LocalizablesFacade.getString(mContext, OneCMSKeys.MULTI_TRIP_TAB_TEXT)
          .toString()
          .toUpperCase());
  private ViewInteraction multiTripTab = onView(multiTripTabMatcher);
  private ViewInteraction passengersPicker = onView(withId(R.id.cell_passengers));
  private ViewInteraction adultsPicker = onView(withId(R.id.numberPickerAdults));
  private ViewInteraction kidsPicker = onView(withId(R.id.numberPickerKids));
  private ViewInteraction babiesPicker = onView(withId(R.id.numberPickerBabies));

  public SearchRobot(@NonNull Context context) {
    super(context);

    today = Calendar.getInstance();
    today.setTime(new Date());
    oneMonthForward = Calendar.getInstance();
    oneMonthForward.setTime(new Date());
    oneMonthForward.add(Calendar.MONTH, 1);

    mSearchesHandler = AndroidTestInjector.getInstance().provideSearchesHandler();
  }

  public SearchRobot selectRoundTripTab() {
    // This is necessary when tab's texts are too large and the tab cut it.
    oneWayTab.perform(swipeRight());
    roundTripTab.perform(click());
    return this;
  }

  public SearchRobot selectOneWayTripTab() {
    // This is necessary when tab's texts are too large and the tab cut it.
    roundTripTab.perform(swipeLeft());
    oneWayTab.perform(click());
    return this;
  }

  public SearchMultiStopRobot selectMultiStopTab() {
    // This is necessary when tab's texts are too large and the tab cut it.
    oneWayTab.perform(swipeLeft());
    multiTripTab.perform(click());
    return new SearchMultiStopRobot(mContext);
  }

  public SearchRobot selectPassengers(String numAdults, String numChildren, String numInfants) {
    passengersPicker.perform(click());
    adultsPicker.perform(setPassengerCount(Integer.valueOf(numAdults)));
    kidsPicker.perform(setPassengerCount(Integer.valueOf(numChildren)));
    babiesPicker.perform(setPassengerCount(Integer.valueOf(numInfants)));
    onView(withId(android.R.id.button1)).perform(click());
    return this;
  }

  public SearchRobot setDirectFlights() {
    onView(withId(R.id.cehck_flightsDirect_only)).perform(click());
    return this;
  }

  public SearchRobot removeAllSearchesHistory() {
    onView(withId(R.id.ibtnClearHistory)).perform(click());
    return grantAlertDialog();
  }

  public SearchRobot checkSearchHistoryNotVisible() {
    onView(withId(R.id.button_acept_widgetSearchActivity)).perform(swipeUp());
    onView(withId(R.id.layout_container_history_search)).check(matches(not(isDisplayed())));
    return this;
  }

  public SearchRobot checkDestination(String destinationIata) {
    onView(allOf(withId(R.id.txtButtonWithHint_hint), withParent(withParent(withId(R.id.cell_to)))))
        .check(matches(withText(destinationIata)));
    return this;
  }

  public SearchRobot checkOrigin(String originIata) {
    onView(allOf(withId(R.id.txtButtonWithHint_hint),
        withParent(withParent(withId(R.id.cell_from))))).check(matches(withText(originIata)));
    return this;
  }

  public SearchRobot checkDepartureDateVisible() {
    onView(allOf(withId(R.id.txtButtonWithHint_text),
        withParent(withParent(withId(R.id.cell_dateGo))))).check(matches(isDisplayed()));
    return this;
  }

  public SearchRobot checkReturnDateNotVisible() {
    onView(allOf(withId(R.id.txtButtonWithHint_hint),
        withParent(withParent(withId(R.id.cell_dateReturn))))).check(matches(not(isDisplayed())));
    return this;
  }

  /**
   * @param departure departure city IATA code
   */
  public SearchRobot selectDepartureCity(String departure) {
    onView(withId(R.id.cell_from)).perform(click());
    onView(withId(R.id.search_src_text)).perform(typeText(departure), closeSoftKeyboard());
    onData(withDestinationCity(departure)).perform(click());
    return this;
  }

  public SearchRobot selectArrivalCity(String arrival) {
    onView(withId(R.id.cell_to)).perform(click());
    onView(withId(R.id.search_src_text)).perform(typeText(arrival), closeSoftKeyboard());
    onData(withDestinationCity(arrival)).perform(click());
    return this;
  }

  public SearchRobot selectRecentArrival(String destination) {
    onView(withId(R.id.cell_to)).perform(click());
    onData(withDestinationCity(destination)).perform(click());
    return this;
  }

  public SearchRobot selectRecentDestination(String destination) {
    onView(withId(R.id.cell_from)).perform(click());
    onData(withDestinationCity(destination)).perform(click());
    return this;
  }

  private SearchRobot grantAlertDialog() {
    onView(withId(android.R.id.button1)).perform(click());
    return this;
  }

  public SearchRobot selectArrivalDate(String dateString) {
    onView(withId(R.id.cell_dateReturn)).perform(click());
    return this;
  }

  private SearchRobot checkResidentsOptionsActive() {
    onView(withId(R.id.residents_widget)).check(matches(isDisplayed()));
    return this;
  }

  private SearchRobot setResidentsSwitchActive(boolean checked) {
    onView(withId(R.id.switchPassengerPayment)).perform(setSwitchChecked(checked));
    return this;
  }

  private SearchRobot grantResidentsDialog() {
    residentsDialog.check(matches(isDisplayed()));
    return grantAlertDialog();
  }

  private SearchRobot checkHistorySearch(String departure, String arrival) {
    onView(allOf(withId(R.id.layout_cities_history_search), withText(containsString(departure)),
        withText(containsString(arrival)))).check(matches(isDisplayed()));
    return this;
  }

  public ResultsRobot search() {
    onView(withId(R.id.button_acept_widgetSearchActivity)).perform(scrollTo(), click());
    return new ResultsRobot(mContext);
  }

  public SearchRobot verifySyncSearch() {
    back();
    checkHistorySearch(DESTINATION_CITY_1, DESTINATION_CITY_2);

    List<StoredSearch> storedSearches = mSearchesHandler.getCompleteSearchesFromDB();
    assertTrue("Searches are not synchronized", storedSearches.get(0).isSynchronized());
    assertEquals("There was a sync error", DESTINATION_IATA_1,
        storedSearches.get(0).getSegmentList().get(0).getOriginIATACode());
    return this;
  }

  public SearchRobot selectCitiesOneWay() {
    return selectDepartureCity(DESTINATION_IATA_2).selectArrivalCity(DESTINATION_IATA_1);
  }

  public SearchRobot selectCitiesRoundTrip() {
    return selectDepartureCity(DESTINATION_IATA_2).selectArrivalCity(DESTINATION_IATA_1);
  }

  public SearchMultiStopRobot selectCitiesMultiStop() {
    return selectMultiStopTab().selectDepartureCity(FIRST_LEG, DESTINATION_IATA_2)
        .selectArrivalCity(FIRST_LEG, DESTINATION_IATA_3)
        .checkDepartureAndArrivalAreEqual(SECOND_LEG, FIRST_LEG, DESTINATION_IATA_3)
        .selectArrivalCity(SECOND_LEG, DESTINATION_IATA_1);
  }

  public SearchRobot selectCitiesWithoutDirectFlights() {
    return selectDepartureCity(DESTINATION_IATA_1).selectArrivalCity(DESTINATION_IATA_4);
  }

  public SearchRobot openCalendar() {
    onView(withId(R.id.cell_dateGo)).perform(click());
    return this;
  }

  public SearchRobot openCalendarFromReturn() {
    onView(withId(R.id.cell_dateReturn)).perform(click());
    return this;
  }

  public SearchRobot selectClass(@NonNull String classToSelect) {
    onView(withId(R.id.spnTravelerClass_tripWidget)).perform(click());
    onData(withCabinClass(getCabinClass(classToSelect))).perform(click());
    return this;
  }

  public SearchRobot saveStoredSearch(StoredSearch storedSearch) {
    mSearchesHandler.saveStoredSearch(storedSearch);
    return this;
  }

  public ResultsRobot selectLatestSearch(String destination) {
    onView(allOf(withId(R.id.layout_cities_history_search),
        withText(containsString(destination)))).perform(scrollTo(), click());
    return new ResultsRobot(mContext);
  }

  public SearchRobot verifyPassengerPickerDismissed(boolean isVisible) {
    if (isVisible) {
      passengersPicker.check(matches(isDisplayed()));
    } else {
      adultsPicker.check(matches(isDisplayed()));
    }
    return this;
  }

  public SearchRobot isDepartureDateSelectedCorrect(Date date) {
    onView(withId(R.id.cell_dateGo)).check(matches(isDepartureDateCorrect(date)));
    return this;
  }

  public SearchRobot isReturnDateSelectedCorrect(Date date) {
    onView(withId(R.id.cell_dateReturn)).check(matches(isDepartureDateCorrect(date)));
    return this;
  }

  public SearchRobot isReturnDateSelectedEmpty() {
    onView(withId(R.id.cell_dateReturn)).check(matches(isDepartureDateCorrect(null)));
    return this;
  }

  public SearchRobot checkIsOneWayTrip() {
    oneWayTab.check(matches(isSelected()));
    return this;
  }

  public SearchRobot checkIsRoundTrip() {
    oneWayTab.check(matches(isSelected()));
    return this;
  }
}
