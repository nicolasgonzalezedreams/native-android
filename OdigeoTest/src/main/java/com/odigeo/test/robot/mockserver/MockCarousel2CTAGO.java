package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.CAROUSEL_CTA_GO_RESPONSE;
import static com.odigeo.test.robot.MockServerRobot.CAROUSEL_URL;

class MockCarousel2CTAGO implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(
        new ResponsesManager.Builder().addGetResponse(CAROUSEL_URL, CAROUSEL_CTA_GO_RESPONSE)
            .build());
  }
}
