package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import com.odigeo.app.android.lib.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 8/11/16
 */

public class ImportTripRobot extends BaseRobot {

  public ImportTripRobot(@NonNull Context context) {
    super(context);
  }

  public ImportTripRobot addEmail(String email) {
    onView(withId(R.id.importTripEmail)).perform(typeText(email), closeSoftKeyboard());
    return this;
  }

  public ImportTripRobot addReservationNumber(String reservationNumber) {
    onView(withId(R.id.importTripBookingId)).perform(typeText(reservationNumber),
        closeSoftKeyboard());
    return this;
  }

  public MyTripsRobot addToMyTrips() {
    onView(withId(R.id.button_add_to_my_trip)).perform(click());
    return new MyTripsRobot(mContext);
  }
}
