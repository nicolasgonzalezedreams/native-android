package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.test.espresso.ViewInteraction;
import android.view.View;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.lib.utils.BaggageUtils;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.view.constants.OneCMSKeys;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.interactors.CountriesInteractor;
import org.hamcrest.Matcher;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.odigeo.test.espresso.matchers.MultipleViewsMatcher.withIndex;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.hasPhoneCode;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withErrorEnabled;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.withErrorMessageShown;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.IsNot.not;

public class PassengerRobot extends BaseRobot {

  private static final String FIELDS_DISPLAYED = "are";

  private CountriesInteractor mCountriesInteractor;
  private String deviceLang = LocaleUtils.getCurrentLanguageIso();

  private ViewInteraction contactDetails_Name =
      onView(allOf(withId(R.id.tilName), hasSibling(withId(R.id.tilCountryCode))));
  private ViewInteraction contactDetails_SurName =
      onView(allOf(withId(R.id.tilLastName), hasSibling(withId(R.id.tilCountryCode))));
  private ViewInteraction contactDetails_CountryResidence = onView(withId(R.id.etCountryCode));
  private ViewInteraction anyErrorMesage =
      onView(allOf(isAssignableFrom(TextInputLayout.class), withErrorEnabled()));
  private ViewInteraction contactAddress = onView(withId(R.id.etAddress));
  private ViewInteraction contactCityName = onView(withId(R.id.etCityName));
  private ViewInteraction postalCode = onView(withId(R.id.etZipCode));
  private ViewInteraction phoneNumber = onView(withId(R.id.etPhoneNumber));
  private ViewInteraction contactEmail = onView(withId(R.id.etMail));

  public PassengerRobot(@NonNull Context context) {
    super(context);
    mCountriesInteractor = AndroidDependencyInjector.getInstance().provideCountriesInteractor();
  }

  public PassengerRobot writeName(String name) {
    sendKeys(onView(withIndex(withId(R.id.etPassengerName), 0)), name);
    return this;
  }

  public PassengerRobot writeSurname(String surname) {
    sendKeys(onView(withIndex(withId(R.id.etFirstLastName), 0)), surname);
    return this;
  }

  public PassengerRobot selectTitle(@NonNull String titleKey) {
    String translatedTitle = LocalizablesFacade.getString(mContext, titleKey).toString();
    onView(withText(translatedTitle)).perform(click());
    return this;
  }

  public PassengerRobot writeAddress(String address) {
    sendKeys(contactAddress, address);
    return this;
  }

  public PassengerRobot writeCityName(String cityName) {
    sendKeys(contactCityName, cityName);
    return this;
  }

  public PassengerRobot writePostalCode(String code) {
    sendKeys(postalCode, code);
    return this;
  }

  public PassengerRobot setCountryCode(String code) {
    if (code.length() > 0) {
      Country country = mCountriesInteractor.getCountryByCountryCode(deviceLang, code);
      openCountrySelector().selectCountry(country);
    }
    return this;
  }

  private CountrySelectorRobot openCountrySelector() {
    onView(withId(R.id.etCountryCode)).perform(scrollTo(), click());
    return new CountrySelectorRobot(mContext);
  }

  public PassengerRobot setPhoneCode(String value) {
    if (value.length() > 0) {
      String phoneCode = value.split(" ")[1];
      String countryCode = value.split(" ")[0];
      Country country = mCountriesInteractor.getCountryByCountryCode(deviceLang, countryCode);

      onView(withId(R.id.etPhoneCode)).perform(scrollTo(), click());
      onView(withId(R.id.search_button)).perform(click());
      onView(withId(R.id.search_src_text)).perform(replaceText(country.getName()),
          typeText(" " + phoneCode), closeSoftKeyboard());
      onData(hasPhoneCode(phoneCode)).perform(click());
    }
    return this;
  }

  public PassengerRobot writePhoneNumber(String number) {
    sendKeys(phoneNumber, number);
    return this;
  }

  public PassengerRobot writeEmail(String email) {
    sendKeys(contactEmail, email);
    return this;
  }

  public InsuranceRobot goForward() {
    onView(withId(R.id.continueButtonPassengers)).perform(scrollTo(), click());
    return new InsuranceRobot(mContext);
  }

  public SummaryRobot goFlightDetails() {
    onView(withId(R.id.menu_item_goto_summary)).perform(click());
    return new SummaryRobot(mContext);
  }

  public PassengerRobot checkIncompleteFieldMsg() {
    anyErrorMesage.check(matches(withErrorMessageShown()));
    return this;
  }

  public PassengerRobot checkFieldsDisplayed(String displayed) {
    contactDetails_CountryResidence.perform(
        scrollTo()); //Scroll until contact details view is visible

    Matcher<View> condition =
        displayed.equals(FIELDS_DISPLAYED) ? isDisplayed() : not(isDisplayed());

    contactDetails_Name.check(matches(condition));
    contactDetails_SurName.check(matches(condition));
    return this;
  }

  public FrequentFlierRobot openFrequentFlierCode() {
    onView(withIndex(withId(R.id.cetFrequentFlyer), 0)).perform(scrollTo()).perform(click());
    return new FrequentFlierRobot(mContext);
  }

  public PassengerRobot verifyFilledData(UserTraveller userTraveller) {
    onView(withIndex(withId(R.id.etPassengerName), 0)).check(matches(withText(userTraveller.getUserProfile().getName())));
    onView(withIndex(withId(R.id.etFirstLastName), 0)).check(matches(withText(userTraveller.getUserProfile().getFirstLastName())));
    contactAddress.check(
        matches(withText(userTraveller.getUserProfile().getUserAddress().getAddress())));
    contactCityName.check(
        matches(withText(userTraveller.getUserProfile().getUserAddress().getCity())));
    postalCode.check(
        matches(withText(userTraveller.getUserProfile().getUserAddress().getPostalCode())));
    phoneNumber.check(matches(withText(userTraveller.getUserProfile().getPhoneNumber())));
    contactEmail.check(matches(withText(userTraveller.getEmail())));
    return this;
  }

  public PassengerRobot checkDuplicatedBooking() {
    CharSequence titleHeaderConfirmation = LocalizablesFacade.getString(mContext,
        OneCMSKeys.CONFIRMATION_DUPLICATE_BOOKING_INFORMATION_TITLE);
    CharSequence textHeaderConfirmation = LocalizablesFacade.getString(mContext,
        OneCMSKeys.CONFIRMATION_DUPLICATE_BOOKING_INFORMATION_SUBTITLE);
    onView(withId(R.id.title_header_confirmation)).check(
        matches(withText(titleHeaderConfirmation.toString())));
    onView(withId(R.id.text_header_confirmation)).check(
        matches(withText(textHeaderConfirmation.toString())));
    return this;
  }

  public void selectNoBaggage() {
    onView(withIndex(withId(R.id.addBaggage), 0)).perform(scrollTo()).perform(click());

    String noBaggageText =
        LocalizablesFacade.getString(mContext, OneCMSKeys.PASSENGER_NO_BAGGAGE_SELECTION)
            .toString();
    onView(withText(noBaggageText)).inRoot(isPlatformPopup()).perform(click());
  }

  public void selectBaggage() {
    onView(withIndex(withId(R.id.addBaggage), 0)).perform(scrollTo()).perform(click());

    String baggageOptionText = BaggageUtils.getBaggageOptionText(mContext, 1, 20,
        LocalizablesFacade.getRawString(mContext, OneCMSKeys.PASSENGER_BAGGAGE_MAX_WEIGHT));
    onView(withText(baggageOptionText)).inRoot(isPlatformPopup()).perform(click());
  }

  public void checkNoBaggageIsSelected() {
    onView(withIndex(withId(R.id.addBaggage), 0)).check(matches(isDisplayed()));
  }

  public void checkBaggageIsSelected() {
    onView(withIndex(withId(R.id.addBaggage), 0)).check(matches(not(isDisplayed())));
  }

  public void removeBaggage(int index) {
    onView(withIndex(withId(R.id.removeBaggage), index)).perform(scrollTo()).perform(click());
  }

  public void goToBaggageIncluded() {
    onView(withId(R.id.baggageIncluded)).perform(scrollTo());
  }

  public void checkRoundTripInfoIsDisplayed() {
    String roundTripText =
        LocalizablesFacade.getString(mContext, OneCMSKeys.ROUND_TRIP_TAB_TEXT).toString();
    onView(withText(roundTripText)).check(matches(isDisplayed()));
  }

  public PassengerRobot checkPassengerPageDisplayed() {
    onView(withId(R.id.topbrief_passenger)).check(matches(isDisplayed()));
    return this;
  }

  public void selectApplyBaggageSelection() {
    onView(withIndex(withId(R.id.applyBaggageSelectionSwitch), 0)).perform(scrollTo())
        .perform(click());
  }

  public void checkSecondPassengerBaggageIsSelected() {
    onView(withIndex(withId(R.id.removeBaggage), 1)).perform(scrollTo())
        .check(matches(isDisplayed()));
  }

  public void checkAnyPassengerHasSelectedBaggage() {
    onView(withIndex(withId(R.id.addBaggage), 0)).perform(scrollTo()).check(matches(isDisplayed()));
    onView(withIndex(withId(R.id.addBaggage), 1)).perform(scrollTo()).check(matches(isDisplayed()));
  }

  public void checkApplySelectionSwitchIsToggleOff() {
    onView(withIndex(withId(R.id.applyBaggageSelectionSwitch), 0)).perform(scrollTo())
        .check(matches(not(isChecked())));
  }

  public void checkDefaultPassengerHasBaggageSelected() {
    onView(withIndex(withId(R.id.addBaggage), 0)).check(matches(not(isDisplayed())));
  }

  public void confirmRemoveBaggage() {
    String removeBaggageText =
        LocalizablesFacade.getString(mContext, OneCMSKeys.BAGGAGE_NAG_REMOVE_BUTTON_NEGATIVE + "_b")
            .toString();
    onView(withText(removeBaggageText)).inRoot(isDialog()).perform(click());
  }

  public void cancelRemoveBaggage() {
    String removeBaggageText =
        LocalizablesFacade.getString(mContext, OneCMSKeys.BAGGAGE_NAG_REMOVE_BUTTON_POSITIVE + "_b")
            .toString();
    onView(withText(removeBaggageText)).inRoot(isDialog()).perform(click());
  }
}