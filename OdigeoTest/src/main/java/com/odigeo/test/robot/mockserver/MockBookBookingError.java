package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.BOOK_BOOKING_ERROR_RESPONSE;
import static com.odigeo.test.robot.MockServerRobot.BOOK_URL;

public class MockBookBookingError implements MockServerStrategy {
  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(
        new ResponsesManager.Builder().addPostResponse(BOOK_URL, BOOK_BOOKING_ERROR_RESPONSE)
            .build());
  }
}
