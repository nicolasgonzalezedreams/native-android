package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.DESTINATION_EMPTY_RESPONSE;
import static com.odigeo.test.robot.MockServerRobot.NEAREST_LOCATIONS_URL;

class MockDestinationNoGps implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(new ResponsesManager.Builder().addPostResponse(NEAREST_LOCATIONS_URL,
        DESTINATION_EMPTY_RESPONSE).build());
  }
}
