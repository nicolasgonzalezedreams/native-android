package com.odigeo.test.robot.mockserver;

public enum HttpCodes {

  HTTP_OK(200), HTTP_BAD_REQUEST(400), HTTP_NOT_FOUND(404);

  private final int code;

  HttpCodes(int code) {
    this.code = code;
  }

  public int getValue() {
    return code;
  }
}
