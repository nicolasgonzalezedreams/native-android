package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.ADD_PASSENGER_URL;
import static com.odigeo.test.robot.MockServerRobot.CART_PASSENGER_DUPLICATED_BOOKING_RESPONSE;

public class MockAddPassengerDuplicatedBooking implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(new ResponsesManager.Builder().addPostResponse(ADD_PASSENGER_URL,
        CART_PASSENGER_DUPLICATED_BOOKING_RESPONSE).build());
  }
}
