package com.odigeo.test.robot.mockserver;

import okhttp3.mockwebserver.MockResponse;

public interface ResponseCreator {
  MockResponse create();
}
