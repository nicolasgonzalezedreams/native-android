package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.GET_VISIT;
import static com.odigeo.test.robot.MockServerRobot.VISIT_URL;

public class MockGetVisits implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(
        new ResponsesManager.Builder().addPostResponse(VISIT_URL, GET_VISIT)
            .build());
  }
}
