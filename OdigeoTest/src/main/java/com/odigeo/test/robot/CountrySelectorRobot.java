package com.odigeo.test.robot;

import com.odigeo.app.android.lib.R;
import com.odigeo.data.entity.booking.Country;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.espresso.ViewInteraction;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.odigeo.test.espresso.matchers.OdigeoMatchers.hasCountryName;

public class CountrySelectorRobot extends BaseRobot {

  //Locators
  ViewInteraction search = onView(withId(R.id.search_button));
  ViewInteraction textBox = onView(withId(R.id.search_src_text));

  CountrySelectorRobot(@NonNull Context context) {
    super(context);
  }

  public CountrySelectorRobot selectCountry(Country country) {
    search.perform(click());
    textBox.perform(replaceText(country.getName()), closeSoftKeyboard());
    onData(hasCountryName(country.getName())).perform(click());
    return this;
  }
}
