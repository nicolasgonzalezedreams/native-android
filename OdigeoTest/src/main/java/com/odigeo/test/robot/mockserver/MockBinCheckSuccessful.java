package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.BIN_CHECK_VISA_CREDIT_RESPONSE;
import static com.odigeo.test.robot.MockServerRobot.BIN_URL;

public class MockBinCheckSuccessful implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(
        new ResponsesManager.Builder().addGetResponse(BIN_URL, BIN_CHECK_VISA_CREDIT_RESPONSE)
            .build());
  }
}
