package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.util.Log;
import com.odigeo.app.android.lib.consts.Constants;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.test.robot.mockserver.LocalMockServerNetController;
import com.odigeo.test.robot.mockserver.ResponsesManager;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.ExecutionException;
import org.json.JSONException;

public class MockServerRobot extends BaseRobot {
  public static final String SEARCH_URL = "/search/.*";
  public static final String ADD_PRODUCT_URL = "/addProduct.*";
  public static final String REMOVE_PRODUCT_URL = "/removeProduct.*";
  public static final String TRIP_URL = "/ojete/";
  public static final String GET_AVAILABLE_PRODUCTS_URL = "/getAvailableProducts.*";
  public static final String NEAREST_LOCATIONS_URL = "/nearestLocations.*";
  public static final String CREATE_SHOPPING_CART_URL = "/createShoppingCart.*";
  public static final String ADD_PASSENGER_URL = "/addPassenger.*";
  public static final String BIN_URL = "/bin/.*";
  public static final String CAROUSEL_URL = "/carousel.*";
  public static final String BOOK_URL = "/book";
  public static final String USER_URL = "/user.*";
  public static final String VISIT_URL = "/visits";

  public static final String OW_OK_RESPONSE = "ow_ok";
  public static final String MULTI_TRIP_RESPONSE = "multi_trip";
  public static final String OW_OK_BUSINESS_RESPONSE = "ow_ok_business";
  public static final String OW_OK_ECONOMY_RESPONSE = "ow_ok_economy";
  public static final String OW_OK_FIRST_RESPONSE = "ow_ok_first";
  public static final String OW_OK_PREMIUM_RESPONSE = "ow_ok_premium";
  public static final String ROUND_OK_RESPONSE = "round_ok";
  public static final String MEMBERSHIP_OK_RESPONSE = "membership_ok";
  public static final String ROUND_DIRECT_FLIGHTS_RESPONSE = "round_direct_flights";
  public static final String BASIC_ADD_PRODUCT_RESPONSE = "basic_add_product";
  public static final String PROMO_CODE_REMOVE_SUCCESSFUL = "promo_code_remove_successful";
  public static final String PROMO_CODE_ADD_UNSUCCESSFUL_RESPONSE = "promo_code_add_unsuccessful";
  public static final String PROMO_CODE_ADD_SUCCESSFUL_RESPONSE = "promo_code_add_successful";
  public static final String IMPORT_TRIP_SUCCESS_RESPONSE = "import_trip_success";
  public static final String EMPTY_FILE_ERROR_RESPONSE = "empty_file_error";
  public static final String NO_PRODUCTS_RESPONSE = "no_products";
  public static final String DESTINATION_EMPTY_RESPONSE = "destination_empty";
  public static final String DESTINATION_MAD_RESPONSE = "destination_mad";
  public static final String BASIC_SHOPPING_CART_RESPONSE = "basic_shopping_cart";
  public static final String BASIC_CART_PASSENGER_RESPONSE = "basic_cart_passenger";
  public static final String CART_PASSENGER_DUPLICATED_BOOKING_RESPONSE =
      "cart_passenger_duplicated_booking";
  public static final String BIN_CHECK_VISA_CREDIT_RESPONSE = "bin_check_visa_credit";
  public static final String CAROUSEL_CTA_ED_RESPONSE = "carousel_cta_ed";
  public static final String CAROUSEL_CTA_GO_RESPONSE = "carousel_cta_go";
  public static final String CAROUSEL_CTA_OP_RESPONSE = "carousel_cta_op";
  public static final String CAROUSEL_CTA_TL_RESPONSE = "carousel_cta_ed";
  public static final String BOOK_BOOKINGCONFIRMED_CONTRACT_RESPONSE =
      "book_booking_confirmed_contract";
  public static final String BOOK_BOOKINGCONFIRMED_REQUEST_COLLECTED_RESPONSE =
      "book_booking_confirmed_request_collected";
  public static final String BOOK_BOOKINGCONFIRMED_RETAINED_RESPONSE =
      "book_booking_confirmed_retained";
  public static final String BOOK_BOOKINGCONFIRMED_ON_HOLD_RESPONSE = "book_booking_confirmed_hold";
  public static final String BOOK_BOOKINGCONFIRMED_REQUEST_NOT_COLLECTED_RESPONSE =
      "book_booking_confirmed_request_not_collected";
  public static final String BOOK_BOOKINGCONFIRMED_UNKNOWN_RESPONSE =
      "book_booking_confirmed_unknown";
  public static final String BOOK_BOOKINGCONFIRMED_REJECTED_RESPONSE =
      "book_booking_confirmed_rejected";
  public static final String BOOK_BOOKINGCONFIRMED_FINAL_RET_RESPONSE =
      "book_booking_confirmed_final_ret";
  public static final String BOOK_BOOKING_STOP_RESPONSE = "book_booking_stop_rejected";
  public static final String BOOK_BROKEN_FLOW_RESPONSE = "book_rejected_broken_flow";
  public static final String BOOK_ON_HOLD_RESPONSE = "book_rejected_on_hold";
  public static final String BOOK_BOOKING_ERROR_RESPONSE = "book_booking_rejected_error";
  public static final String BOOK_PAYMENT_RETRY_ONLY_BT = "book_payment_retry_only_bt";
  public static final String BOOK_PAYMENT_RETRY_ONLY_PAY_PAL = "book_payment_retry_only_paypal";
  public static final String BOOK_PAYMENT_RETRY_BT_AND_CC = "book_payment_retry_bt_and_cc";
  public static final String BOOK_PAYMENT_REPRICING = "book_booking_repricing";
  public static final String BOOK_PAYMENT_USER_INTERACTION_NEEDED_PAYPAL =
      "book_user_interaction_needed_paypal";
  public static final String BOOK_PAYMENT_USER_INTERACTION_NEEDED_TRUSTLY =
      "book_user_interaction_needed_trustly";
  public static final String BOOK_PAYMENT_USER_INTERACTION_NEEDED_KLARNA =
      "book_user_interaction_needed_klarna";
  public static final String LOGIN_OK = "login_ok";
  public static final String LOGIN_EMAIL_ERROR = "login_email_error";
  public static final String LOGIN_PWD_ERROR = "login_password_error";

  private static final String SEARCH = "search";
  private static final String AUTO_COMPLETE = "autoComplete";
  private static final String IMPORT_TRIP = "importTrip";
  private static final String CAROUSEL = "carousel";
  private static final String DESTINATION = "destination";
  private static final String SYNC_SEARCH = "sync_search_flight";
  private static final String PROMO_CODE_ADD = "promo_code_add";
  private static final String PROMO_CODE_REMOVE = "promo_code_remove";
  private static final String DISABLE_NOTIFICATIONS = "disable_notifications";
  private static final String ADD_PASSENGER = "addPassenger";
  private static final String ADD_PRODUCT = "addProduct";
  private static final String BIN_CHECK_VALIDATION = "bin_check_validation";
  private static final String CREATE_SHOPPING_CART = "createShoppingCart";
  private static final String AVAILABLE_PRODUCTS = "getAvailableProducts";
  private static final String USER = "user";

  private static final String OW = "ow";
  private static final String USER_SEARCH_SYNC_OK = "user_search_ok";
  private static final String OW_ECONOMY = "ow_economy";
  private static final String OW_PREMIUM = "ow_premium";
  private static final String OW_BUSINESS = "ow_business";
  private static final String OW_FIRST = "ow_first";
  private static final String ROUND = "round";
  private static final String MULTI_STOP = "multi";
  private static final String ERROR = "error";
  private static final String EMPTY = "empty";
  private static final String ONLY_DIRECT = "only_direct";
  private static final String NO_FULL_TRANSPARENCY = "no_full_transparency";
  private static final String IMPORT_SUCCESS = "import_trip_success";
  private static final String IMPORT_FAILURE = "import_trip_failed";
  private static final String CAROUSEL_CTA_ED = "carousel_cta_ed";
  private static final String CAROUSEL_CTA_OP = "carousel_cta_op";
  private static final String CAROUSEL_CTA_GO = "carousel_cta_go";
  private static final String MOBILE_API_MSL = "/mobile-api/msl/";
  private static final String NO_GPS = "no_gps";
  private static final String GPS = "gps";
  private static final String PROMO_CODE_ADD_SUCCESSFUL = "promo_code_add_successful";
  private static final String PROMO_CODE_ADD_UNSUCCESSFUL = "promo_code_add_unsuccessful";
  private static final String DISABLE_NOTIFICATIONS_SUCCESSFUL = "disable_notifications_successful";
  private static final String CART_PASSENGER = "cart_with_passenger";
  private static final String CART = "cart";
  private static final String NO_PRODUCTS = "no_products";
  private static final String CART_PASSENGER_DUPLICATED_BOOKING = "cart_with_duplicated_booking";
  private static final String CART_WITH_PRODUCTS = "cart_with_products";
  private static final String BIN_CHECK_VALIDATION_SUCCESSFUL = "bin_check_visa_credit";

  public static final String USER_REGISTRATION_SUCCESS = "user_registration_success";
  public static final String GET_VISIT = "visit";

  private LocalMockServerNetController mController;
  private DomainHelperInterface mDomainHelper;

  public MockServerRobot(@NonNull Context context, LocalMockServerNetController controller,
      DomainHelperInterface domain) {
    super(context);
    mController = controller;
    mDomainHelper = domain;
  }

  @NonNull public MockServerRobot init() {
    try {
      String url = mController.getUrl();
      mDomainHelper.putUrl(url);
    } catch (ExecutionException | InterruptedException | JSONException e) {
      Log.e(Constants.TAG_LOG, e.getMessage(), e);
    }
    return this;
  }

  public void start() {
    mController.start();
  }

  @NonNull public MockServerRobot clean() {
    mController.invalidateCaches();
    return this;
  }

  @NonNull public MockServerRobot stop() {
    mController.stop();
    return this;
  }

  public void addPathResponses(ResponsesManager responses) {
    mController.addResponses(responses);
  }

  @Retention(RetentionPolicy.SOURCE) @StringDef({
      SEARCH, AUTO_COMPLETE, IMPORT_TRIP, CAROUSEL, DESTINATION, PROMO_CODE_ADD, PROMO_CODE_REMOVE,
      SYNC_SEARCH, DISABLE_NOTIFICATIONS, ADD_PASSENGER, CREATE_SHOPPING_CART, AVAILABLE_PRODUCTS,
      ADD_PRODUCT, BIN_CHECK_VALIDATION, USER
  }) public @interface Step {
  }

  @Retention(RetentionPolicy.SOURCE) @StringDef({
      OW, OW_ECONOMY, OW_PREMIUM, OW_BUSINESS, OW_FIRST, ROUND, ERROR, EMPTY, MULTI_STOP,
      NO_FULL_TRANSPARENCY, IMPORT_SUCCESS, IMPORT_FAILURE, CAROUSEL_CTA_ED, USER_SEARCH_SYNC_OK,
      CAROUSEL_CTA_OP, CAROUSEL_CTA_GO, ONLY_DIRECT, PROMO_CODE_ADD_SUCCESSFUL,
      PROMO_CODE_ADD_UNSUCCESSFUL, PROMO_CODE_REMOVE_SUCCESSFUL, NO_GPS, GPS,
      DISABLE_NOTIFICATIONS_SUCCESSFUL, CART_PASSENGER, CART, NO_PRODUCTS, CART_WITH_PRODUCTS,
      CART_PASSENGER_DUPLICATED_BOOKING, BIN_CHECK_VALIDATION_SUCCESSFUL,
      BOOK_BOOKINGCONFIRMED_CONTRACT_RESPONSE, BOOK_BOOKINGCONFIRMED_REQUEST_COLLECTED_RESPONSE,
      BOOK_BOOKINGCONFIRMED_RETAINED_RESPONSE, BOOK_BOOKINGCONFIRMED_ON_HOLD_RESPONSE,
          BOOK_BOOKINGCONFIRMED_REQUEST_NOT_COLLECTED_RESPONSE,
      BOOK_BOOKINGCONFIRMED_UNKNOWN_RESPONSE, BOOK_BOOKINGCONFIRMED_REJECTED_RESPONSE,
      BOOK_BOOKINGCONFIRMED_FINAL_RET_RESPONSE, BOOK_BOOKING_STOP_RESPONSE,
      BOOK_BROKEN_FLOW_RESPONSE, BOOK_ON_HOLD_RESPONSE, BOOK_BOOKING_ERROR_RESPONSE,
      BOOK_PAYMENT_RETRY_ONLY_BT, BOOK_PAYMENT_RETRY_ONLY_PAY_PAL, BOOK_PAYMENT_RETRY_BT_AND_CC,
      BOOK_PAYMENT_REPRICING, BOOK_PAYMENT_USER_INTERACTION_NEEDED_PAYPAL,
      BOOK_PAYMENT_USER_INTERACTION_NEEDED_TRUSTLY, BOOK_PAYMENT_USER_INTERACTION_NEEDED_KLARNA,
      LOGIN_OK, LOGIN_EMAIL_ERROR, LOGIN_PWD_ERROR, USER_REGISTRATION_SUCCESS
  }) public @interface Response {
  }
}