package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.EMPTY_FILE_ERROR_RESPONSE;
import static com.odigeo.test.robot.MockServerRobot.SEARCH_URL;

class MockErrorRoundSearch implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(
        new ResponsesManager.Builder().addPostResponse(SEARCH_URL, EMPTY_FILE_ERROR_RESPONSE)
            .build());
  }
}
