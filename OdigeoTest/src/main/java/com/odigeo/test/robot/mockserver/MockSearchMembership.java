package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.MEMBERSHIP_OK_RESPONSE;
import static com.odigeo.test.robot.MockServerRobot.SEARCH_URL;

class MockSearchMembership implements MockServerStrategy {
  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(new ResponsesManager.Builder().addPostResponse(SEARCH_URL,
        MEMBERSHIP_OK_RESPONSE).build());
  }
}
