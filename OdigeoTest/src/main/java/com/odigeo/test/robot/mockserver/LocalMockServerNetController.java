package com.odigeo.test.robot.mockserver;

import android.content.Context;
import android.util.Log;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import okhttp3.mockwebserver.MockWebServer;
import org.json.JSONException;

public class LocalMockServerNetController {

  public static final String LOCALWEBSERVER = "MOCKSERVER";
  MockWebServer server;
  private MockDispatcher dispatcher;

  public LocalMockServerNetController(RequestHelper requestHelper, Context context) {
    dispatcher = new MockDispatcher(context);
    server = new MockWebServer();
  }

  public void addResponses(ResponsesManager responses) {
    dispatcher.setResponses(responses);
  }

  public void start() {
    Log.d(LOCALWEBSERVER, "--------------START SERVER------------------");
    startServer();
    server.setDispatcher(dispatcher);
  }

  public void stop() {
    Log.d(LOCALWEBSERVER, "--------------STOP SERVER------------------");
    stopServer();
  }

  public String getUrl() throws ExecutionException, InterruptedException, JSONException {
    return server.url("/").toString();
  }

  public void invalidateCaches() {
    Log.d(LOCALWEBSERVER, "clear paths");
  }

  private void startServer() {
    try {
      Log.d(LOCALWEBSERVER, "LocalMockServerNetController");
      server.start();
    } catch (IOException e) {
      Log.d(LOCALWEBSERVER, "Exception" + e.getLocalizedMessage());
      e.printStackTrace();
    }
  }

  private void stopServer() {
    try {
      server.shutdown();
    } catch (IOException e) {
      Log.d(LOCALWEBSERVER, "reset:Exception" + e.getLocalizedMessage());
      e.printStackTrace();
    }
  }
}
