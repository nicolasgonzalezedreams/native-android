package com.odigeo.test.robot.mockserver;

import com.odigeo.test.robot.MockServerRobot;

import static com.odigeo.test.robot.MockServerRobot.BOOK_PAYMENT_RETRY_BT_AND_CC;
import static com.odigeo.test.robot.MockServerRobot.BOOK_URL;

public class MockBookRetryBTAndCC implements MockServerStrategy {

  @Override public void configure(MockServerRobot robot) {
    robot.addPathResponses(
        new ResponsesManager.Builder().addPostResponse(BOOK_URL, BOOK_PAYMENT_RETRY_BT_AND_CC)
            .build());
  }
}
