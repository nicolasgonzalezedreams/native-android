package com.odigeo.test.robot;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.espresso.ViewInteraction;
import android.widget.ImageButton;
import com.odigeo.app.android.lib.R;
import com.odigeo.app.android.lib.data.LocalizablesFacade;
import com.odigeo.app.android.view.constants.OneCMSKeys;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

public class RegisterRobot extends BaseRobot {

  private ViewInteraction buttonBack = onView(withId(R.id.flHome));
  private ViewInteraction editTextEmail = onView(withId(R.id.etEmail));
  private ViewInteraction editTextPassword = onView(withId(R.id.etPassword));
  private ViewInteraction buttonSignUp = onView(withId(R.id.btnSignUp));

  public RegisterRobot(@NonNull Context context) {
    super(context);
  }

  public RegisterRobot showRegisterPage() {
    onView(withId(R.id.scroll_register_view)).check(matches(isDisplayed()));
    return this;
  }

  public RegisterRobot goBack() {
    onView(allOf(isAssignableFrom(ImageButton.class), withParent(withId(R.id.action_bar)))).perform(
        click());
    return this;
  }

  public RegisterRobot showHomePage() {
    buttonBack.check(matches(isDisplayed()));
    return this;
  }

  public RegisterRobot setEmail(String email) {
    editTextEmail.perform(typeText(email));
    return this;
  }

  public RegisterRobot setPassword(String password) {
    editTextPassword.perform(typeText(password));
    return this;
  }

  public RegisterRobot checkUserIsRegistered() {
    String message =
        LocalizablesFacade.getString(mContext, OneCMSKeys.SSO_SIGNIN_READY_TITLE).toString();
    onView(withText(message)).check(matches(isDisplayed()));
    return this;
  }

  public RegisterRobot clickOnRegisterButton() {
    buttonSignUp.perform(click());
    return this;
  }

  public RegisterRobot setEmailAndPassword(String email, String password) {
    editTextEmail.perform(typeText(email));
    editTextPassword.perform(typeText(password), closeSoftKeyboard());
    return this;
  }
}
