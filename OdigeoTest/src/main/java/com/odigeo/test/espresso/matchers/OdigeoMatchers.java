package com.odigeo.test.espresso.matchers;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;
import android.support.test.runner.lifecycle.Stage;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.floatingexpandiblelistview.FloatingGroupExpandableListView;
import com.floatingexpandiblelistview.WrapperExpandableListAdapter;
import com.odigeo.app.android.lib.adapters.DestinationAdapter;
import com.odigeo.app.android.lib.adapters.ResultsActivityExpandableListAdapter;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.consts.DistanceUnits;
import com.odigeo.app.android.lib.models.CityItemListDestination;
import com.odigeo.app.android.lib.models.SegmentGroup;
import com.odigeo.app.android.lib.models.SegmentWrapper;
import com.odigeo.app.android.lib.models.dto.BaggageIncludedDTO;
import com.odigeo.app.android.lib.models.dto.CabinClassDTO;
import com.odigeo.app.android.lib.models.dto.FareItineraryDTO;
import com.odigeo.app.android.lib.models.dto.SectionDTO;
import com.odigeo.app.android.lib.ui.widgets.ButtonSearchDate;
import com.odigeo.app.android.lib.ui.widgets.base.ButtonWithHint;
import com.odigeo.app.android.lib.utils.LocaleUtils;
import com.odigeo.app.android.view.adapters.BookingAdapter;
import com.odigeo.app.android.view.custom.SimpleHistorySearchRow;
import com.odigeo.calendar.CalendarPickerView;
import com.odigeo.calendar.MonthDescriptor;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.tools.DateUtils;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static org.hamcrest.CoreMatchers.instanceOf;

public class OdigeoMatchers {

  private static final String ADAPTED_DATA_DESCRIPTION = "with adapted data ";
  private static final String COUNTRY_CODE_DESCRIPTION = "with country code: '%s'";
  private static final String COUNTRY_NAME_DESCRIPTION = "with country name: '%s'";
  private static final String PHONE_CODE_DESCRIPTION = "with phone code: '%s'";
  private static final String MONTH_AND_YEAR_DESCRIPTION = "with month: '%d' and year: '%d'";
  private static final String DISTANCE_UNIT_DESCRIPTION = "with distance unit: ";
  private static final String NO_CONTINUE_BTN = "with no continue button shown";
  private static final String TRIP_ITEM_DESCRIPTION =
      "recyclerview has an item with departure: %s , arrival: %s , and date: %s";
  private static final String SEARCH_DATE_AND_PASSENGERS_DESCRIPTION =
      "with search date '%s' and %s passengers";
  private static final String DESTINATION_CITY_DESCRIPTION = "with destination city IATA code: ";
  private static final String TYPEFACE_DESCRIPTION = "with typeface: '%s'";
  private static final String IMAGE_DRAWABLE_DESCRIPTION = "has null drawable";

  private OdigeoMatchers() {
    // nothing
  }

  @NonNull public static Matcher<Object> hasCountryCode(final String countryCode) {
    return new BoundedMatcher<Object, Country>(Country.class) {
      @Override protected boolean matchesSafely(Country item) {
        return item.getCountryCode().equalsIgnoreCase(countryCode);
      }

      @Override public void describeTo(Description description) {
        String textDescription = String.format(COUNTRY_CODE_DESCRIPTION, countryCode);
        description.appendText(textDescription);
      }
    };
  }

  @NonNull public static Matcher<Object> hasCountryName(final String countryName) {
    return new BoundedMatcher<Object, Country>(Country.class) {
      @Override protected boolean matchesSafely(Country item) {
        return item.getName().equalsIgnoreCase(countryName);
      }

      @Override public void describeTo(Description description) {
        String textDescription = String.format(COUNTRY_NAME_DESCRIPTION, countryName);
        description.appendText(textDescription);
      }
    };
  }

  @NonNull public static Matcher<Object> hasPhoneCode(final String phoneCode) {
    return new BoundedMatcher<Object, Country>(Country.class) {
      @Override protected boolean matchesSafely(Country item) {
        return item.getPhonePrefix().equalsIgnoreCase(phoneCode);
      }

      @Override public void describeTo(Description description) {
        String textDescription = String.format(PHONE_CODE_DESCRIPTION, phoneCode);
        description.appendText(textDescription);
      }
    };
  }

  @NonNull public static Matcher<Object> withDistanceValue(final String value) {
    return new BoundedMatcher<Object, DistanceUnits>(DistanceUnits.class) {

      @Override public void describeTo(Description description) {
        description.appendText(DISTANCE_UNIT_DESCRIPTION + value);
      }

      @Override protected boolean matchesSafely(DistanceUnits item) {
        return item.value().equals(value);
      }
    };
  }

  @NonNull public static Matcher<Object> withNoContinueBtn() {
    return new BoundedMatcher<Object, FloatingGroupExpandableListView>(
        FloatingGroupExpandableListView.class) {

      @Override public void describeTo(Description description) {
        description.appendText(NO_CONTINUE_BTN);
      }

      @Override protected boolean matchesSafely(FloatingGroupExpandableListView listView) {
        ResultsActivityExpandableListAdapter adapter =
            ((ResultsActivityExpandableListAdapter) ((WrapperExpandableListAdapter) listView.getExpandableListAdapter())
                .getWrappedAdapter());

        return !adapter.isSegmentGroupWidgetSelected();
      }
    };
  }

  @NonNull public static Matcher<View> withAdaptedData() {
    return new TypeSafeMatcher<View>() {
      @Override protected boolean matchesSafely(View view) {
        if (!(view instanceof AdapterView)) {
          return false;
        }
        @SuppressWarnings("rawtypes") Adapter adapter = ((AdapterView) view).getAdapter();
        return (adapter.getCount() > 0);
      }

      @Override public void describeTo(Description description) {
        description.appendText(ADAPTED_DATA_DESCRIPTION);
      }
    };
  }

  @NonNull public static Matcher<Object> withMonthAndYear(final int month, final int year) {
    return new BoundedMatcher<Object, Object>(Object.class) {
      @Override protected boolean matchesSafely(Object monthDescriptor) {
        try {
          Field field = monthDescriptor.getClass().getDeclaredField("month");
          field.setAccessible(true);
          int itemMonth = ((Integer) field.get(monthDescriptor));

          field = monthDescriptor.getClass().getDeclaredField("year");
          field.setAccessible(true);
          int itemYear = ((Integer) field.get(monthDescriptor));

          return month == (itemMonth + 1) && year == itemYear;
        } catch (NoSuchFieldException e) {
          Log.e("withMonthAndYear", e.getMessage(), e);
        } catch (IllegalAccessException e) {
          Log.e("withMonthAndYear", e.getMessage(), e);
        }
        return false;
      }

      @Override public void describeTo(Description description) {
        String textDescription = String.format(MONTH_AND_YEAR_DESCRIPTION, month, year);
        description.appendText(textDescription);
      }
    };
  }

  @NonNull public static Matcher<View> withSearchDateAndPassengers(final String date,
      final String passengers) {
    return new BoundedMatcher<View, LinearLayout>(LinearLayout.class) {

      @Override public void describeTo(Description description) {
        String textDescription =
            String.format(SEARCH_DATE_AND_PASSENGERS_DESCRIPTION, date, passengers);
        description.appendText(textDescription);
      }

      @Override protected boolean matchesSafely(LinearLayout item) {
        boolean result = false;
        if (item.getChildCount() > 0) {
          for (int index = 0; index < item.getChildCount(); index++) {
            View child = item.getChildAt(index);
            if (child instanceof SimpleHistorySearchRow) {
              SimpleHistorySearchRow widget = ((SimpleHistorySearchRow) child);
              result |= (widget.getNoPassengers().getText().toString().equals(passengers)
                  && widget.getDatesTextView().getText().toString().equals(date));
            }
          }
        }
        return result;
      }
    };
  }

  @NonNull
  public static Matcher<View> hasTripItem(final String departureIata, final String arrivalIata,
      final long date) {
    return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {
      @Override protected boolean matchesSafely(RecyclerView item) {
        BookingAdapter adapter = (BookingAdapter) item.getAdapter();
        Booking booking = null;
        for (int index = 0; index < adapter.getItemCount(); index++) {
          if (BookingAdapter.TYPE_UPCOMING_TRIP == adapter.getItemViewType(index)) {
            booking = adapter.getItem(index);
            break;
          }
        }
        return booking != null
            && departureIata.equals(booking.getDepartureAirportCode())
            && arrivalIata.equals(booking.getArrivalAirportCode())
            && date == booking.getDepartureDate();
      }

      @Override public void describeTo(Description description) {
        String textDescription = String.format(TRIP_ITEM_DESCRIPTION, departureIata, arrivalIata,
            new Date(date).toString());
        description.appendText(textDescription);
      }
    };
  }

  @NonNull public static Matcher<Object> withDestinationCity(final String IataCode) {
    return new BoundedMatcher<Object, CityItemListDestination>(CityItemListDestination.class) {
      @Override protected boolean matchesSafely(CityItemListDestination item) {
        return item.getIataCode().equals(IataCode) && !item.isSubItem();
      }

      @Override public void describeTo(Description description) {
        description.appendText(DESTINATION_CITY_DESCRIPTION + IataCode);
      }
    };
  }

  @NonNull public static Matcher<Object> withAllResultsExpanded() {
    return new BoundedMatcher<Object, FloatingGroupExpandableListView>(
        FloatingGroupExpandableListView.class) {
      @Override protected boolean matchesSafely(FloatingGroupExpandableListView listView) {
        ExpandableListAdapter adapter = listView.getExpandableListAdapter();
        int groups = adapter.getGroupCount();
        for (int index = 0; index < groups; index++) {
          if (!listView.isGroupExpanded(index)) {
            return false;
          }
        }
        return true;
      }

      @Override public void describeTo(Description description) {
        description.appendText("Expandable ListView with all the items expanded");
      }
    };
  }

  @NonNull public static Matcher<Object> withResultsExpanded(final int resultsExpanded) {
    return new BoundedMatcher<Object, FloatingGroupExpandableListView>(
        FloatingGroupExpandableListView.class) {
      @Override protected boolean matchesSafely(FloatingGroupExpandableListView listView) {
        ExpandableListAdapter adapter = listView.getExpandableListAdapter();
        int groups = adapter.getGroupCount();
        for (int index = 0; index < groups; index++) {
          if (index < resultsExpanded) {
            if (!listView.isGroupExpanded(index)) {
              return false;
            }
          } else if (listView.isGroupExpanded(index)) {
            return false;
          }
        }
        return true;
      }

      @Override public void describeTo(Description description) {
        description.appendText(
            "Expandable ListView with " + resultsExpanded + " the items expanded");
      }
    };
  }

  @NonNull public static Matcher<View> withOdigeoHint(@NonNull final String hint) {
    return new BoundedMatcher<View, ButtonWithHint>(ButtonWithHint.class) {
      @Override protected boolean matchesSafely(ButtonWithHint buttonWithHint) {
        return hint.equals(buttonWithHint.getHintKey());
      }

      @Override public void describeTo(Description description) {
        description.appendText("with odigeo's hint " + hint);
      }
    };
  }

  @NonNull public static Matcher<View> isDepartureDateCorrect(@NonNull final Date date) {
    return new BoundedMatcher<View, ButtonSearchDate>(ButtonSearchDate.class) {
      @Override public void describeTo(Description description) {
        description.appendText("Date displayed is different than expected");
      }

      @Override protected boolean matchesSafely(ButtonSearchDate buttonSearchDate) {
        if (buttonSearchDate.getDate() == null && date == null) {
          return true;
        }
        return DateUtils.areSameDay(buttonSearchDate.getDate(), date) ? true : false;
      }
    };
  }

  @NonNull
  public static Matcher<View> isPreviousMonthVisible(@NonNull final String previousMonthTitle) {
    return new BoundedMatcher<View, CalendarPickerView>(CalendarPickerView.class) {
      @Override public void describeTo(Description description) {
        description.appendText("Previous month is visible");
      }

      @Override protected boolean matchesSafely(CalendarPickerView calendarPickerView) {
        int first = calendarPickerView.getFirstVisiblePosition();
        int last = calendarPickerView.getLastVisiblePosition();

        for (int i = first; i <= last; i++) {
          MonthDescriptor monthDescriptor =
              (MonthDescriptor) calendarPickerView.getAdapter().getItem(i);

          if (monthDescriptor.getLabel().equals(previousMonthTitle)) {
            return true;
          }
        }

        return false;
      }
    };
  }

  @NonNull public static Matcher<Object> withTypeface(@NonNull final Typeface typeface) {

    return new BoundedMatcher<Object, TextView>(TextView.class) {
      @Override public void describeTo(Description description) {
        description.appendText(String.format(TYPEFACE_DESCRIPTION,
            "" + (typeface != null ? typeface.getStyle() : "null")));
      }

      @Override protected boolean matchesSafely(TextView item) {
        if (typeface != null && item.getTypeface() != null) {
          return item.getTypeface().equals(typeface);
        } else if (typeface == null) {
          return item.getTypeface() == null || item.getTypeface()
              .equals(Typeface.defaultFromStyle(Typeface.NORMAL));
        }

        return false;
      }
    };
  }

  @NonNull public static Matcher<Object> hasNullImageDrawable() {
    return new BoundedMatcher<Object, ImageView>(ImageView.class) {
      @Override public void describeTo(Description description) {
        description.appendText(IMAGE_DRAWABLE_DESCRIPTION);
      }

      @Override protected boolean matchesSafely(ImageView item) {
        return item.getDrawable() == null;
      }
    };
  }

  public static Matcher<Object> withCabinClass(@NonNull final CabinClassDTO cabinClass) {
    return new BoundedMatcher<Object, CabinClassDTO>(CabinClassDTO.class) {
      @Override protected boolean matchesSafely(CabinClassDTO item) {
        return item == cabinClass;
      }

      @Override public void describeTo(Description description) {
        description.appendText("with cabin class " + cabinClass);
      }
    };
  }

  public static Matcher<View> hasBaggageIncludedResults() {
    return new BoundedMatcher<View, FloatingGroupExpandableListView>(
        FloatingGroupExpandableListView.class) {
      @Override public void describeTo(Description description) {
        description.appendText("Results have baggage included");
      }

      @Override protected boolean matchesSafely(FloatingGroupExpandableListView listView) {
        ResultsActivityExpandableListAdapter adapter =
            ((ResultsActivityExpandableListAdapter) ((WrapperExpandableListAdapter) listView.getExpandableListAdapter())
                .getWrappedAdapter());

        for (FareItineraryDTO fareItinerary : adapter.getFareItineraries()) {
          for (SegmentGroup segmentGroup : fareItinerary.getSegmentGroups()) {
            if (segmentGroup.getBaggageIncluded().equals(BaggageIncludedDTO.INCLUDED)) {
              return true;
            }
          }
        }
        return false;
      }
    };
  }

  public static Matcher<View> hasOnlyDirectFlights() {
    return new BoundedMatcher<View, FloatingGroupExpandableListView>(
        FloatingGroupExpandableListView.class) {
      @Override public void describeTo(Description description) {
        description.appendText("Results have only direct flights");
      }

      @Override protected boolean matchesSafely(FloatingGroupExpandableListView listView) {
        ResultsActivityExpandableListAdapter adapter =
            ((ResultsActivityExpandableListAdapter) ((WrapperExpandableListAdapter) listView.getExpandableListAdapter())
                .getWrappedAdapter());

        for (FareItineraryDTO fareItinerary : adapter.getFareItineraries()) {
          for (SegmentGroup segmentGroup : fareItinerary.getSegmentGroups()) {
            for (SegmentWrapper segmentWrapper : segmentGroup.getSegmentWrappers()) {
              if (segmentWrapper.getSegment().getSections().size() > 1) {
                return false;
              }
            }
          }
        }
        return true;
      }
    };
  }

  public static Matcher<View> hasCabinClass(@NonNull final CabinClassDTO classExpected) {
    return new BoundedMatcher<View, FloatingGroupExpandableListView>(
        FloatingGroupExpandableListView.class) {
      @Override protected boolean matchesSafely(FloatingGroupExpandableListView listView) {
        ResultsActivityExpandableListAdapter adapter =
            ((ResultsActivityExpandableListAdapter) ((WrapperExpandableListAdapter) listView.getExpandableListAdapter())
                .getWrappedAdapter());
        return checkCabinClassInAdapter(adapter);
      }

      private boolean checkCabinClassInAdapter(ResultsActivityExpandableListAdapter adapter) {
        boolean result = true;
        final Map<CabinClassDTO, Set<CabinClassDTO>> cabinClassesMap = getCabinClassesMap();
        Set<CabinClassDTO> classesInResults = new HashSet<>();
        for (FareItineraryDTO fareItinerary : adapter.getFareItineraries()) {
          for (SegmentGroup segmentGroup : fareItinerary.getSegmentGroups()) {
            for (SegmentWrapper segmentWrapper : segmentGroup.getSegmentWrappers()) {
              for (SectionDTO section : segmentWrapper.getSectionsObjects()) {
                CabinClassDTO cabinClass = section.getCabinClass();
                if (cabinClass != null) {
                  if (classExpected == CabinClassDTO.DEFAULT) {
                    classesInResults.add(cabinClass);
                  } else {
                    result &= cabinClassesMap.get(classExpected).contains(cabinClass);
                  }
                }
              }
            }
          }
        }
        if (classExpected == CabinClassDTO.DEFAULT) {
          return !classesInResults.isEmpty();
        } else {
          return result;
        }
      }

      @NonNull private Map<CabinClassDTO, Set<CabinClassDTO>> getCabinClassesMap() {
        final Map<CabinClassDTO, Set<CabinClassDTO>> cabinClassesMap = new HashMap<>();
        Set<CabinClassDTO> economicClasses = new HashSet<>();
        economicClasses.add(CabinClassDTO.ECONOMIC_DISCOUNTED);
        economicClasses.add(CabinClassDTO.TOURIST);
        economicClasses.add(CabinClassDTO.BUSINESS);
        cabinClassesMap.put(CabinClassDTO.TOURIST, economicClasses);
        Set<CabinClassDTO> premiumClasses = new HashSet<>();
        premiumClasses.add(CabinClassDTO.ECONOMIC_DISCOUNTED);
        premiumClasses.add(CabinClassDTO.PREMIUM_ECONOMY);
        cabinClassesMap.put(CabinClassDTO.PREMIUM_ECONOMY, premiumClasses);
        Set<CabinClassDTO> businessClasses = new HashSet<>();
        businessClasses.add(CabinClassDTO.ECONOMIC_DISCOUNTED);
        businessClasses.add(CabinClassDTO.BUSINESS);
        cabinClassesMap.put(CabinClassDTO.BUSINESS, businessClasses);
        Set<CabinClassDTO> firstClasses = new HashSet<>();
        firstClasses.add(CabinClassDTO.ECONOMIC_DISCOUNTED);
        firstClasses.add(CabinClassDTO.BUSINESS);
        firstClasses.add(CabinClassDTO.FIRST);
        cabinClassesMap.put(CabinClassDTO.FIRST, firstClasses);
        return cabinClassesMap;
      }

      @Override public void describeTo(Description description) {
        description.appendText("Results have " + classExpected + " cabin class");
      }
    };
  }

  public static Matcher<View> withToolbarTitle(final Matcher<String> textMatcher) {
    return new BoundedMatcher<View, Toolbar>(Toolbar.class) {
      @Override public boolean matchesSafely(Toolbar toolbar) {
        return textMatcher.matches(toolbar.getTitle());
      }

      @Override public void describeTo(Description description) {
        description.appendText("with toolbar title: ");
        textMatcher.describeTo(description);
      }
    };
  }

  @NonNull public static Matcher<View> withTextSizeLessThan(@NonNull final int size) {

    return new BoundedMatcher<View, TextView>(TextView.class) {
      @Override public void describeTo(Description description) {
        description.appendText("With text size: " + size);
      }

      @Override protected boolean matchesSafely(TextView item) {
        int textSize = item.getText().length();
        return textSize <= size;
      }
    };
  }

  @NonNull public static Matcher<View> isAllTextVisible() {

    return new BoundedMatcher<View, TextView>(TextView.class) {
      @Override public void describeTo(Description description) {
        description.appendText("All text should be visible");
      }

      @Override protected boolean matchesSafely(TextView item) {
        return !item.getText().toString().contains("...");
      }
    };
  }

  public static Matcher<View> withDestinationType(final int type) {
    return new BoundedMatcher<View, ListView>(ListView.class) {
      @Override protected boolean matchesSafely(ListView item) {
        DestinationAdapter adapter = ((DestinationAdapter) item.getAdapter());
        int size = adapter.getCount();
        boolean result = true;
        for (int index = 0; index < size; index++) {
          int itemType = adapter.getItemViewType(index);
          result &= (itemType == DestinationAdapter.HEADER) || (itemType == type);
        }
        return result;
      }

      @Override public void describeTo(Description description) {
        description.appendText("With destination's type: " + type);
      }
    };
  }

  public static Matcher<View> priceRemainsEqual(final double currentTotalPrice) {

    return new BoundedMatcher<View, TextView>(TextView.class) {
      @Override public void describeTo(Description description) {
        description.appendText(
            "New Total Price is different than Current Total Price: " + currentTotalPrice);
      }

      @Override protected boolean matchesSafely(TextView item) {
        String currentTotalPriceFormatted = LocaleUtils.getLocalizedCurrencyValue(currentTotalPrice,
            LocaleUtils.localeToString(Configuration.getCurrentLocale()));
        String newTotalPriceFormatted = item.getText().toString();
        return currentTotalPriceFormatted.equals(newTotalPriceFormatted);
      }
    };
  }

  public static Matcher<View> withErrorEnabled() {
    return new BoundedMatcher<View, TextInputLayout>(TextInputLayout.class) {

      @Override public void describeTo(Description description) {
        description.appendText("with error enabled ");
      }

      @Override protected boolean matchesSafely(TextInputLayout item) {
        return item.isErrorEnabled();
      }
    };
  }

  public static Matcher<View> withErrorMessageShown() {
    return new BoundedMatcher<View, TextInputLayout>(TextInputLayout.class) {
      @Override public void describeTo(Description description) {
        description.appendText("with error message shown ");
      }

      @Override protected boolean matchesSafely(TextInputLayout item) {
        return item.getError().length() > 0;
      }
    };
  }

  public static <T> void assertCurrentActivityIs(Class myclass) {
    assertThat("", getActivityInstance(), instanceOf(myclass));
  }

  public static Activity getActivityInstance() {
    final Activity[] currentActivity = new Activity[1];

    getInstrumentation().runOnMainSync(new Runnable() {
      public void run() {
        Collection<Activity> resumedActivity =
            ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED);
        for (Activity act : resumedActivity) {
          currentActivity[0] = act;
          break;
        }
      }
    });

    return currentActivity[0];
  }
}
