package com.odigeo.test.espresso;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Spinner;
import com.floatingexpandiblelistview.FloatingGroupExpandableListView;
import com.odigeo.app.android.lib.ui.widgets.SegmentGroupWidget;
import org.hamcrest.Matcher;

import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;

/**
 * Created by Javier Marsicano on 22/11/16.
 */
public class OdigeoViewActions {

  private static final String SET_SWITCH_CHECKED_DESCRIPTION = "Set switch checked ";
  private static final String SET_PASSENGER_COUNT_DESCRIPTION = "Set passenger count";
  private static final String OPEN_SPINNER = "Open spinner popup";
  private static final String SELECT_FLIGHT = "Select flight in results";
  private static final String CLICK_BTN = "click a button";

  private OdigeoViewActions() {
  }

  public static ViewAction setSwitchChecked(final boolean checked) {
    return new ViewAction() {
      @Override public Matcher<View> getConstraints() {
        return isAssignableFrom(SwitchCompat.class);
      }

      @Override public String getDescription() {
        return SET_SWITCH_CHECKED_DESCRIPTION + checked;
      }

      @Override public void perform(UiController uiController, View view) {
        if (view instanceof SwitchCompat) {
          SwitchCompat sw = (SwitchCompat) view;
          sw.setChecked(checked);
        } else {
          throw new IllegalArgumentException("view is not a SwitchCompat");
        }
      }
    };
  }

  public static ViewAction setPassengerCount(final int count) {
    return new ViewAction() {
      @Override public void perform(UiController uiController, View view) {
        if (view instanceof NumberPicker) {
          NumberPicker tp = (NumberPicker) view;
          tp.setValue(count);
        } else {
          throw new IllegalArgumentException("view is not a NumberPicker");
        }
      }

      @Override public String getDescription() {
        return SET_PASSENGER_COUNT_DESCRIPTION;
      }

      @Override public Matcher<View> getConstraints() {
        return isAssignableFrom(NumberPicker.class);
      }
    };
  }

  /**
   * this action is to perform click on a Button without need to be displayed at least 90 percent
   */
  public static ViewAction clickButton() {
    return new ViewAction() {
      @Override public Matcher<View> getConstraints() {
        return isAssignableFrom(Button.class);
      }

      @Override public String getDescription() {
        return CLICK_BTN;
      }

      @Override public void perform(UiController uiController, View view) {
        view.performClick();
      }
    };
  }

  /*This action is to replace click() due to random fails when opening Spinner popup*/
  public static ViewAction openSpinnerPopup() {
    return new ViewAction() {
      @Override public Matcher<View> getConstraints() {
        return isAssignableFrom(Spinner.class);
      }

      @Override public String getDescription() {
        return OPEN_SPINNER;
      }

      @Override public void perform(UiController uiController, View view) {
        Spinner spinner = (Spinner) view;
        spinner.performClick();
      }
    };
  }

  /**
   * Selects the first flight of the list
   */
  public static ViewAction selectFlight() {
    return new ViewAction() {
      @Override public Matcher<View> getConstraints() {
        return isAssignableFrom(FloatingGroupExpandableListView.class);
      }

      @Override public String getDescription() {
        return SELECT_FLIGHT;
      }

      @Override public void perform(UiController uiController, View view) {
        FloatingGroupExpandableListView listView = ((FloatingGroupExpandableListView) view);
        ((SegmentGroupWidget) listView.getChildAt(1)).getSegmentWidgets().get(0).performClick();
      }
    };
  }
}
