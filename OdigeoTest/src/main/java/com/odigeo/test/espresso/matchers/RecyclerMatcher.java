package com.odigeo.test.espresso.matchers;

import android.support.annotation.NonNull;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

import static android.support.test.espresso.intent.Checks.checkNotNull;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class RecyclerMatcher {

  public static Matcher<View> atPosition(final int position, @NonNull final int resourceId) {
    return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {
      @Override public void describeTo(Description description) {
        description.appendText("has item at position " + position + ": ");
      }

      @Override protected boolean matchesSafely(final RecyclerView view) {
        RecyclerView.ViewHolder viewHolder = view.findViewHolderForAdapterPosition(position);
        if (viewHolder == null) {
          return false;
        }
        View viewById = viewHolder.itemView.findViewById(resourceId);

        boolean matchesDescendant = hasDescendant(withId(resourceId)).matches(viewHolder.itemView);
        boolean matchesDisplay = isDisplayed().matches(viewById);

        return matchesDescendant && matchesDisplay;
      }
    };
  }

  public static Matcher<View> atPosition(final int position,
      @NonNull final Matcher<View> itemMatcher) {
    checkNotNull(itemMatcher);
    return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {
      @Override public void describeTo(Description description) {
        description.appendText("has item at position " + position + ": ");
        itemMatcher.describeTo(description);
      }

      @Override protected boolean matchesSafely(final RecyclerView view) {
        RecyclerView.ViewHolder viewHolder = view.findViewHolderForAdapterPosition(position);
        if (viewHolder == null) {
          return false;
        }

        return itemMatcher.matches(viewHolder.itemView);
      }
    };
  }
}
