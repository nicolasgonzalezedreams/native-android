package com.odigeo.test.espresso;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import static android.support.test.InstrumentationRegistry.getInstrumentation;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 1/11/16
 */
public class PermissionTestHelper {
  private static final int DENY_INDEX = 0;
  private static final int ALLOW_INDEX = 1;

  private PermissionTestHelper() {
    // Nothing
  }

  public static void allowPermission(@NonNull String permissionNeeded) {
    processPermission(permissionNeeded, ALLOW_INDEX);
  }

  public static void denyPermission(@NonNull String permissionNeeded) {
    processPermission(permissionNeeded, DENY_INDEX);
  }

  private static void processPermission(String permissionNeeded, int index) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !needsPermission(permissionNeeded)) {
      try {
        UiObject permissionOption = getPermissionOption(index);
        if (permissionOption.exists()) {
          permissionOption.click();
        }
      } catch (UiObjectNotFoundException e) {
        Log.e(PermissionTestHelper.class.getSimpleName(), e.getMessage(), e);
      }
    }
  }

  private static UiObject getPermissionOption(int index) {
    return UiDevice.getInstance(getInstrumentation())
        .findObject(new UiSelector().clickable(true).checkable(false).index(index));
  }

  private static boolean needsPermission(@NonNull String permissionNeeded) {
    Context context = InstrumentationRegistry.getTargetContext();
    int permissionStatus = ContextCompat.checkSelfPermission(context, permissionNeeded);
    return permissionStatus == PackageManager.PERMISSION_GRANTED;
  }
}
