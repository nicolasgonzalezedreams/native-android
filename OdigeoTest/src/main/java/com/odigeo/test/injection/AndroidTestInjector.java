package com.odigeo.test.injection;

import android.support.test.InstrumentationRegistry;
import com.odigeo.app.android.AndroidDependencyInjector;
import com.odigeo.test.robot.MockServerRobot;
import com.odigeo.test.robot.PreferencesRobot;
import com.odigeo.test.robot.TestRobot;
import com.odigeo.test.robot.mockserver.LocalMockServerNetController;
import com.odigeo.test.robot.mockserver.MockServerConfigurator;

public class AndroidTestInjector extends AndroidDependencyInjector {
  private static AndroidTestInjector sInstance;

  public static AndroidTestInjector getInstance() {
    if (sInstance == null) {
      sInstance = new AndroidTestInjector();
    }
    return sInstance;
  }

  public MockServerRobot provideMockServerRobot() {
    return new MockServerRobot(mContext, provideMockServerNetController(), provideDomainHelper());
  }

  public LocalMockServerNetController provideMockServerNetController() {
    return new LocalMockServerNetController(provideRequestHelper(),
        InstrumentationRegistry.getContext());
  }

  public PreferencesRobot providePreferencesRobot() {
    return new PreferencesRobot(mContext);
  }

  public TestRobot provideTestRobot() {
    return new TestRobot(mContext);
  }

  public MockServerConfigurator provideMockServerConfigurator() {
    return new MockServerConfigurator(provideMockServerRobot());
  }
}
