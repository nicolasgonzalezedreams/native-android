#!/bin/sh

export ANDROID_HOME=/Users/qauser/Library/Android/
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.7.0_79.jdk/Contents/Home/

echo "Run script"
PATH=$PATH:/usr/local/bin

cd ..

function buildNotes {
	local COMMENTARIES="$1"
	local BRANCH="$(hg branch)"
	local REVISION_NUMBER="$(hg id -n)"
	local DATE="$(date -u)"
	echo "Commentaries: "$COMMENTARIES"\nBranch: "$BRANCH"\nRevision number: "$REVISION_NUMBER"\nDate: "$DATE > fabric/releaseNotes.txt
}

buildNotes "$1"

gradle --daemon clean build -x test -x checkstyle -x checkstyleMain -x checkstyleTest distributeRelease
