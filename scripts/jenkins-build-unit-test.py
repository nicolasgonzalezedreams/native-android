#!/usr/bin/python
import os
import subprocess
import re

os.environ['ANDROID_HOME'] = "/Users/qauser/Library/Android/"
os.environ['JAVA_HOME'] = "/Library/Java/JavaVirtualMachines/jdk1.7.0_79.jdk/Contents/Home/"

os.environ['PATH'] = os.environ['PATH']+":/usr/local/bin"

print "Run script"

os.chdir(os.environ['HOME']+"/jenkins/workspace/android-native-app-unit-test/")
print os.getcwd()

# Pull all changes
hg_log_process = subprocess.Popen(["hg pull -u https://jrebollo_odigeo:Caloret14@bitbucket.org/odigeoteam/android-native-app"], stdout=subprocess.PIPE, shell=True)

(output, err) = hg_log_process.communicate()

print output

# Get last log
hg_log_process = subprocess.Popen(["hg log --limit 1"], stdout=subprocess.PIPE, shell=True)

(output, err) = hg_log_process.communicate()

print output

# Parse the response and get branch

patern = "[\n\r].*branch:\s*([^\n]*)"

new_branch = re.search(patern, output)

new_branch = re.sub('branch:[\s]*', '', new_branch.group(0))
new_branch = re.sub('\n', '', new_branch)

print new_branch

# Update branch
update_command = "hg update "+ new_branch

hg_update = subprocess.Popen([update_command], stdout=subprocess.PIPE, shell=True)

(output, err) = hg_update.communicate()

print output

# Test
gradle_test = subprocess.Popen(["gradle --daemon test"], stdout=subprocess.PIPE, shell=True)

(output, err) = gradle_test.communicate()

print output