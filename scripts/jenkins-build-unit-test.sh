#!/bin/sh

export ANDROID_HOME=/Users/qauser/Library/Android/
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.7.0_79.jdk/Contents/Home/

echo "Run script"
PATH=$PATH:/usr/local/bin

cd $HOME/jenkins/workspace/android-native-app-unit-test/

function changeBranch {
	Stext="$(hg log --limit 1)"
	local NEW_BRANCH;

	if [[ $Stext =~ [\n\r].*branch:\s*([^tag]*) ]]; then
    	NEW_BRANCH=${BASH_REMATCH[1]}
	else
    	exit 1
	fi

	echo $NEW_BRANCH;

	hg update "$NEW_BRANCH"

}

changeBranch

gradle --daemon test

