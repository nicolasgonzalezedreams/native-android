package com.odigeo.dataodigeo.session;

public class Credentials implements CredentialsInterface {
  private String mUser;
  private String mPassword;
  private CredentialsType mType;

  public Credentials(String user, String password, CredentialsType type) {
    this.mUser = user;
    this.mPassword = password;
    this.mType = type;
  }

  @Override public String getUser() {
    return mUser;
  }

  @Override public String getPassword() {
    return mPassword;
  }

  @Override public void setPassword(String newPassword) {
    mPassword = newPassword;
  }

  @Override public CredentialsType getType() {
    return mType;
  }

  @Override public String getCredentialTypeValue() {
    String value;
    switch (mType) {
      case FACEBOOK:
        value = "facebook";
        break;
      case GOOGLE:
        value = "google";
        break;
      case PASSWORD:
        value = "password";
        break;
      default:
        value = "";
    }
    return value;
  }
}
