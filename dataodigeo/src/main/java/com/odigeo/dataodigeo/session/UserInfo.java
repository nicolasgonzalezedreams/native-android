package com.odigeo.dataodigeo.session;

import com.odigeo.data.entity.userData.User.Status;

public class UserInfo implements UserInfoInterface {

  private long userId;
  private String mName;
  private String mProfilePicture;
  private Status mUserStatus;
  private String mUserEmail;

  public UserInfo(long userId, String name, String profilePicture, Status status,
      String userEmail) {
    this.userId = userId;
    this.mName = name;
    this.mProfilePicture = profilePicture;
    this.mUserStatus = status;
    this.mUserEmail = userEmail;
  }

  @Override public long getUserId() {
    return userId;
  }

  @Override public String getProfilePicture() {
    return mProfilePicture;
  }

  @Override public String getName() {
    return mName;
  }

  @Override public Status getUserStatus() {
    return mUserStatus;
  }

  @Override public String getEmail() {
    return mUserEmail;
  }
}
