package com.odigeo.dataodigeo.session;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Base64;
import com.odigeo.data.entity.userData.CreditCard;
import com.odigeo.data.entity.userData.User.Status;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.dataodigeo.db.dao.UserDBDAO;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import static com.odigeo.dataodigeo.net.controllers.AuthToken.EMPTY_TOKEN;

public class SessionControllerImpl implements SessionController {

  private static Credentials mCredentials;
  private Context mContext;
  private SharedPreferences mSharedPreferences;
  private SecretKey mKey;
  private UserDBDAO mUserDBDAO;
  private PreferencesControllerInterface mPreferencesController;
  private List<CreditCard> mCreditCards;

  public SessionControllerImpl(Context context, PreferencesControllerInterface preferencesController) {
    this.mContext = context;
    mUserDBDAO = new UserDBDAO(mContext);
    initKey();
    mPreferencesController = preferencesController;
  }

  private void initKey() {
    try {
      DESKeySpec keySpec = new DESKeySpec(SECRET_PHRASE.getBytes("UTF8"));
      SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
      mKey = keyFactory.generateSecret(keySpec);
    } catch (InvalidKeyException | UnsupportedEncodingException | InvalidKeySpecException | NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
  }

  @Override public Credentials getCredentials() {
    if (mCredentials == null) {
      mCredentials = decodeCredentials(getCredentialString());
    }
    return mCredentials;
  }

  private Credentials decodeCredentials(String token) {
    try {
      String[] credentialArray = token.split(SPLIT_TOKEN);

      return new Credentials(credentialArray[0], credentialArray[1],
          CredentialsInterface.CredentialsType.valueOf(credentialArray[2]));
    } catch (ArrayIndexOutOfBoundsException e) {
      return null;
    } catch (NullPointerException e) {
      return null;
    } catch (IllegalArgumentException e) {
      return null;
    }
  }

  private String getCredentialString() {
    String encriptedKey = encrypt(CREDENTIALS_KEY);
    mSharedPreferences = mContext.getSharedPreferences(encriptedKey, Context.MODE_PRIVATE);
    String credentialString = mSharedPreferences.getString(encriptedKey, null);
    return credentialString != null ? decrypt(credentialString) : null;
  }

  private String encrypt(String text) {
    String encryptedText = text;

    byte[] b = text.getBytes(Charset.forName("UTF-8"));

    try {
      Cipher cipher = Cipher.getInstance("DES");

      cipher.init(Cipher.ENCRYPT_MODE, mKey);
      encryptedText = Base64.encodeToString(cipher.doFinal(b), Base64.NO_WRAP);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException | NullPointerException e) {
      e.printStackTrace();
    }

    return encryptedText;
  }

  private String decrypt(String text) {
    String decryptedText = text;

    byte[] encrypedText = Base64.decode(text, Base64.NO_WRAP);

    try {
      Cipher cipher = Cipher.getInstance("DES");

      cipher.init(Cipher.DECRYPT_MODE, mKey);
      decryptedText = new String(cipher.doFinal(encrypedText));
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException | NullPointerException e) {
      e.printStackTrace();
    }

    return decryptedText;
  }

  @Override public void savePasswordCredentials(String user, String password,
      CredentialsInterface.CredentialsType type) {
    setPassword(password);
    Credentials credentials = new Credentials(user, password, type);
    String credentialString = credentials.getUser()
        + SPLIT_TOKEN
        + credentials.getPassword()
        + SPLIT_TOKEN
        + credentials.getType();
    saveCredentials(credentialString);
  }

  @Override public void saveSocialCredentials(String user, String socialToken,
      CredentialsInterface.CredentialsType type) {
    setPassword(socialToken);
    Credentials credentials = new Credentials(user, socialToken, type);
    String credentialString =
        credentials.getUser() + SPLIT_TOKEN + EMPTY_TOKEN + SPLIT_TOKEN + credentials.getType();
    saveCredentials(credentialString);
  }

  private void setPassword(String newPassword) {
    if (mCredentials != null) {
      mCredentials.setPassword(newPassword);
    }
  }

  private void saveCredentials(String credentialString) {
    String encryptedKey = encrypt(CREDENTIALS_KEY);
    String encryptedToken = encrypt(credentialString);
    mSharedPreferences = mContext.getSharedPreferences(encryptedKey, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.putString(encryptedKey, encryptedToken);
    editor.apply();
  }

  @Override public void saveUserInfo(long userId, String name, String pictureUrl, String email,
      Status status) {
    mSharedPreferences = mContext.getSharedPreferences(USER_INFO_KEY, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.putLong(USER_INFO_ID, userId);
    editor.putString(USER_INFO_NAME, name);
    editor.putString(USER_INFO_IMAGE, pictureUrl);
    editor.putString(USER_INFO_STATUS, status.toString());
    editor.putString(USER_INFO_EMAIL, email);
    editor.apply();
  }

  @Override public void saveUserName(String name) {
    mSharedPreferences = mContext.getSharedPreferences(USER_INFO_KEY, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.putString(USER_INFO_NAME, name);
    editor.apply();
  }

  @Override public void saveUserId(long userId) {
    mSharedPreferences = mContext.getSharedPreferences(USER_INFO_KEY, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.putLong(USER_INFO_ID, userId);
    editor.apply();
  }

  @Override public UserInfoInterface getUserInfo() {
    mSharedPreferences = mContext.getSharedPreferences(USER_INFO_KEY, Context.MODE_PRIVATE);
    long userId = mSharedPreferences.getLong(USER_INFO_ID, 0);
    String name = mSharedPreferences.getString(USER_INFO_NAME, "");
    String imageUrl = mSharedPreferences.getString(USER_INFO_IMAGE, "");
    String email = mSharedPreferences.getString(USER_INFO_EMAIL, "");
    Status userStatus = Status.valueOf(mSharedPreferences.getString(USER_INFO_STATUS, "ACTIVE"));
    return new UserInfo(userId, name, imageUrl, userStatus, email);
  }

  @Override public void removeAllData() {
    removeCredentials();
    removeUserInfo();
    removeSearchHistory();
  }

  private void removeCredentials() {
    mCredentials = null;
    String encryptedKey = encrypt(CREDENTIALS_KEY);
    mSharedPreferences = mContext.getSharedPreferences(encryptedKey, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.clear();
    editor.apply();
  }

  @Override public void removeUserInfo() {

    mUserDBDAO.deleteUsers();
    removeSharedPreferencesInfo();
  }

  @Override public void removeSharedPreferencesInfo() {
    mSharedPreferences = mContext.getSharedPreferences(USER_INFO_KEY, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.clear();
    editor.apply();
  }

  private void removeSearchHistory() {
    //TODO CHANGE THIS WHEN THE SEARCH HISTORY IS SAVED CORRECTLY
    //Clear search options
    SharedPreferences.Editor editor =
        mContext.getSharedPreferences("historyDepartCities", Context.MODE_PRIVATE).edit();
    editor.clear();
    editor.apply();

    //Clear History
    editor = mContext.getSharedPreferences("historyRetourCities", Context.MODE_PRIVATE).edit();
    editor.apply();
  }

  @Override public void saveGcmToken(String token) {
    mSharedPreferences = mContext.getSharedPreferences(GCM_TOKEN_KEY, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.putString(GCM_TOKEN, token);
    editor.apply();
  }

  @Override public String getGcmToken() {
    mSharedPreferences = mContext.getSharedPreferences(GCM_TOKEN_KEY, Context.MODE_PRIVATE);
    return mSharedPreferences.getString(GCM_TOKEN, "");
  }

  @Override public void saveLogoutWasMade(boolean logoutWasMade) {
    mPreferencesController.saveBooleanValue(LOGOUT_WAS_MADE, logoutWasMade);
  }

  @Override public boolean getLogoutWasMade() {
    return mPreferencesController.getBooleanValue(LOGOUT_WAS_MADE);
  }

  @Override public void invalidateGcmToken() {
    saveGcmToken("");
  }

  @VisibleForTesting UserDBDAO getUserDbDao() {
    return mUserDBDAO;
  }

  @Nullable @Override public List<CreditCard> getCreditCards() {
    return mCreditCards;
  }

  @Override public void attachCreditCardsToSession(List<CreditCard> creditCards) {
    mCreditCards = creditCards;
  }
}
