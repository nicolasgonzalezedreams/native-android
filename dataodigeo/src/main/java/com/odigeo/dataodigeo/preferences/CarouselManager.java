package com.odigeo.dataodigeo.preferences;

import android.content.Context;
import android.util.Log;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.data.preferences.CarouselManagerInterface;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.dataodigeo.net.mapper.CarrouselNetMapper;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import odigeo.dataodigeo.R;
import org.json.JSONException;
import org.json.JSONObject;

public class CarouselManager implements CarouselManagerInterface {

  private static final String TAG = "CarouselManager";
  private Context mContext;
  private PreferencesControllerInterface mPreferencesController;

  public CarouselManager(Context context, PreferencesControllerInterface preferencesController) {
    this.mContext = context;
    this.mPreferencesController = preferencesController;
  }

  @Override public List<Card> carouselManagerMapper(String carouselInfo) {
    if (!carouselInfo.isEmpty()) {
      try {
        return CarrouselNetMapper.jsonCarrouselMapper(carouselInfo);
      } catch (JSONException e) {
        Log.e(TAG, "carouselManagerMapper: Error parsing Json");
      }
    }
    return null;
  }

  @Override public long getCarouselLastUpdate() {
    return mPreferencesController.getLongValue(
        PreferencesControllerInterface.LAST_UPDATE_BOOKING_STATUS);
  }

  @Override public void deleteCarouselLastUpdate() {
    mPreferencesController.saveLongValue(PreferencesControllerInterface.LAST_UPDATE_BOOKING_STATUS,
        0);
  }

  @Override public String getCarouselImage(Segment segment) {
    String path = mContext.getString(R.string.booking_image_path);
    String imageUrl = null;
    if (segment != null) {
      Section lastSection = segment.getLastSection();
      imageUrl = path + String.format(mContext.getString(R.string.booking_image_filename),
          lastSection.getTo().getLocationCode());
    }
    return imageUrl;
  }

  @Override public void hideBookingCard(long bookingId, long date, Card.CardType cardType) {
    JSONObject jsonBookings = getAllHiddenCards(cardType);

    try {
      jsonBookings.put(String.valueOf(bookingId), date);
    } catch (JSONException e) {
      //Nothing
    }

    saveCardIdToHide(jsonBookings.toString(), cardType);
  }

  @Override public boolean mustShow(long bookingId, Card.CardType cardType) {
    JSONObject jsonBooking = getAllHiddenCards(cardType);

    Iterator<String> jsonBookingsIds = jsonBooking.keys();

    while (jsonBookingsIds.hasNext()) {
      try {
        long nextBookingId = Long.parseLong(jsonBookingsIds.next());

        if (nextBookingId == bookingId) {
          return false;
        }
      } catch (NumberFormatException e) {
        //Nothing
      }
    }

    return true;
  }

  @Override public void cleanPastHiddenCards() {
    cleanPastHiddenCards(Card.CardType.CANCELLED_SECTION_CARD);
    cleanPastHiddenCards(Card.CardType.DIVERTED_SECTION_CARD);
  }

  private void cleanPastHiddenCards(Card.CardType cardType) {
    Calendar calendar = Calendar.getInstance();
    long currentTime = calendar.getTimeInMillis();

    JSONObject jsonBookings = getAllHiddenCards(cardType);

    Iterator<String> bookingsIds = jsonBookings.keys();

    while (bookingsIds.hasNext()) {
      try {
        String bookingId = bookingsIds.next();
        long date = jsonBookings.getLong(bookingId);

        if (date < currentTime) {
          jsonBookings.remove(bookingId);
        }
      } catch (JSONException e) {
        //Nothing
      }
    }
    saveCardIdToHide(jsonBookings.toString(), cardType);
  }

  private JSONObject getAllHiddenCards(Card.CardType cardType) {
    String bookingJsonObject = null;
    JSONObject jsonBooking;

    if (cardType == Card.CardType.CANCELLED_SECTION_CARD) {
      bookingJsonObject =
          mPreferencesController.getStringValue(PreferencesControllerInterface.BOOKING_CANCELED);
    } else if (cardType == Card.CardType.DIVERTED_SECTION_CARD) {
      bookingJsonObject =
          mPreferencesController.getStringValue(PreferencesControllerInterface.BOOKING_DIVERTED);
    }

    if (bookingJsonObject != null) {
      try {
        jsonBooking = new JSONObject(bookingJsonObject);
      } catch (JSONException e) {
        jsonBooking = new JSONObject();
      }
    } else {
      jsonBooking = new JSONObject();
    }
    return jsonBooking;
  }

  public void saveCardIdToHide(String bookingIds, Card.CardType cardType) {
    if (cardType == Card.CardType.CANCELLED_SECTION_CARD) {
      mPreferencesController.saveStringValue(PreferencesControllerInterface.BOOKING_CANCELED,
          bookingIds);
    } else if (cardType == Card.CardType.DIVERTED_SECTION_CARD) {
      mPreferencesController.saveStringValue(PreferencesControllerInterface.BOOKING_CANCELED,
          bookingIds);
    }
  }
}
