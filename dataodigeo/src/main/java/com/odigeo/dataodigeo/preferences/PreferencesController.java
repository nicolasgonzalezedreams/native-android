package com.odigeo.dataodigeo.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.odigeo.configuration.ABAlias;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.preferences.PreferencesControllerInterface;
import com.odigeo.interactors.provider.MarketProviderInterface;
import com.odigeo.presenter.model.QAModeUrlModel;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PreferencesController implements PreferencesControllerInterface {

  private final DomainHelperInterface domainHelper;
  private final Context context;
  private final MarketProviderInterface marketProvider;

  public PreferencesController(Context context, DomainHelperInterface domainHelper,
      MarketProviderInterface marketProvider) {
    this.context = context;
    this.domainHelper = domainHelper;
    this.marketProvider = marketProvider;
  }

  private SharedPreferences getSharedPreferences() {
    return context.getSharedPreferences(SHARED_PREFERENCES_ODIGEO, Context.MODE_PRIVATE);
  }

  @Deprecated private SharedPreferences getHistorySearchPreferences() {
    return context.getSharedPreferences(SHARED_PREFERENCE_FILE_HISTORY_SEARCH,
        Context.MODE_PRIVATE);
  }

  private SharedPreferences getABTestPreferences() {
    return context.getSharedPreferences(SHARED_PREFERENCES_AB_TEST, Context.MODE_PRIVATE);
  }

  private SharedPreferences getABTestDimensionPreferences() {
    return context.getSharedPreferences(SHARED_PREFERENCES_AB_TEST_DIMENSIONS,
        Context.MODE_PRIVATE);
  }

  @Override public void saveStringValue(String constant, String value) {
    getSharedPreferences().edit().putString(constant, value).apply();
  }

  @Override public void saveLongValue(String constant, long value) {
    getSharedPreferences().edit().putLong(constant, value).apply();
  }

  @Override public void saveIntValue(String constant, int value) {
    getSharedPreferences().edit().putInt(constant, value).apply();
  }

  @Override public void saveFloatValue(String constant, float value) {
    getSharedPreferences().edit().putFloat(constant, value).apply();
  }

  @Override public void saveBooleanValue(String constant, boolean value) {
    getSharedPreferences().edit().putBoolean(constant, value).apply();
  }

  @Override public String getStringValue(String constant) {
    return getSharedPreferences().getString(constant, "");
  }

  @Override public Long getLongValue(String constant) {
    return getSharedPreferences().getLong(constant, 0);
  }

  @Override public int getIntValue(String constant) {
    try {
      return getSharedPreferences().getInt(constant, 0);
    } catch (ClassCastException e) {
      return 0;
    }
  }

  @Override public float getFloatValue(String constant) {
    return getSharedPreferences().getFloat(constant, 0);
  }

  @Override public boolean getBooleanValue(String constant) {
    return getSharedPreferences().getBoolean(constant, false);
  }

  @Override public boolean getBooleanValue(String constant, boolean defaultValue) {
    return getSharedPreferences().getBoolean(constant, defaultValue);
  }

  @Override public void deleteAllPreferences() {
    getSharedPreferences().edit().clear();
  }

  @Override public void deleteValue(String constant) {
    getSharedPreferences().edit().remove(constant).apply();
  }

  @Override public void clearHistorySearch() {
    getHistorySearchPreferences().edit().clear().apply();
  }

  @Override public boolean containsValue(String constant) {
    return getSharedPreferences().contains(constant);
  }

  @Override public void saveTestAssignment(String alias, int value) {
    getABTestPreferences().edit().putInt(alias, value).apply();
  }

  @Override public int getTestAssignment(ABAlias abAlias) {
    return getABTestPreferences().getInt(abAlias.value(), 0);
  }

  @Override public void clearTestAssignments() {
    getABTestPreferences().edit().clear().apply();
  }

  @Override public void clearDimensions() {
    getABTestDimensionPreferences().edit().clear().apply();
  }

  @Override public void saveDimensions(String key, int value) {
    getABTestDimensionPreferences().edit().putInt(key, value).apply();
  }

  @Override public Map<String, Integer> getDimensions() {
    return (Map<String, Integer>) getABTestDimensionPreferences().getAll();
  }

  @Override public List<QAModeUrlModel> getQAModeUrls() {
    List<QAModeUrlModel> urls = new ArrayList<>();

    String strUrls = getQAModesFromPreferences();
    if (strUrls.isEmpty()) {
      urls.add(new QAModeUrlModel(domainHelper.getUrl(), true));
      urls.add(new QAModeUrlModel(marketProvider.getProductionUrl(), false));
    } else {
      TypeToken t = new TypeToken<List<QAModeUrlModel>>() {
      };
      Type typeList = t.getType();
      urls = new Gson().fromJson(strUrls, typeList);
    }

    return urls;
  }

  @Override public List<QAModeUrlModel> saveQAModeUrl(QAModeUrlModel qaModeUrlModel) {
    List<QAModeUrlModel> existingQAUrls = getQAModeUrls();
    existingQAUrls.add(qaModeUrlModel);

    saveQAModeUrls(existingQAUrls);

    return existingQAUrls;
  }

  @Override public List<QAModeUrlModel> editQAModeUrl(QAModeUrlModel oldQaModeUrlModel,
      QAModeUrlModel newQaModeUrlModel) {
    List<QAModeUrlModel> existingQAUrls = getQAModeUrls();
    int index = existingQAUrls.indexOf(oldQaModeUrlModel);
    existingQAUrls.set(index, newQaModeUrlModel);

    List<QAModeUrlModel> newQAUrls = new ArrayList<>(existingQAUrls.size());
    for (QAModeUrlModel qaModeUrlModel : existingQAUrls) {
      newQAUrls.add(
          new QAModeUrlModel(qaModeUrlModel.getUrl(), qaModeUrlModel.equals(newQaModeUrlModel)));
    }
    saveQAModeUrls(newQAUrls);

    return newQAUrls;
  }

  @Override public List<QAModeUrlModel> deleteQAModeUrl(QAModeUrlModel qaModeUrlModel) {
    List<QAModeUrlModel> existingQAUrls = getQAModeUrls();
    int index = existingQAUrls.indexOf(qaModeUrlModel);
    existingQAUrls.remove(index);

    if (qaModeUrlModel.isSelected() && !existingQAUrls.isEmpty()) {
      QAModeUrlModel firstQaModeUrl = existingQAUrls.get(0);
      existingQAUrls.set(0, new QAModeUrlModel(firstQaModeUrl.getUrl(), true));
      List<QAModeUrlModel> updatedQaModeUrlModels = new ArrayList<>(existingQAUrls);
      for (QAModeUrlModel model : existingQAUrls) {
        updatedQaModeUrlModels.add(
            new QAModeUrlModel(model.getUrl(), model.equals(firstQaModeUrl)));
      }
    }

    saveQAModeUrls(existingQAUrls);

    return existingQAUrls;
  }

  @Override @Nullable public String getQAModeUrl() {
    final List<QAModeUrlModel> qaModeUrls = getQAModeUrls();
    for (QAModeUrlModel qaModeUrlModel : qaModeUrls) {
      if (qaModeUrlModel.isSelected()) {
        return qaModeUrlModel.getUrl();
      }
    }
    return null;
  }

  private void saveQAModeUrls(List<QAModeUrlModel> qaModeUrlModels) {
    TypeToken t = new TypeToken<List<QAModeUrlModel>>() {
    };
    Type typeList = t.getType();
    String strURLs = new Gson().toJson(qaModeUrlModels, typeList);

    saveQAModesToPreferences(strURLs);

    for (QAModeUrlModel modeUrlModel : qaModeUrlModels) {
      if (modeUrlModel.isSelected()) {
        domainHelper.putUrl(modeUrlModel.getUrl());
        break;
      }
    }
  }

  private SharedPreferences getQAModeUrlsPreferences() {
    return context.getSharedPreferences(SHARED_PREFERENCE_FILE_QA_MODE_URLS, Context.MODE_PRIVATE);
  }

  private void saveQAModesToPreferences(String strURLs) {
    getQAModeUrlsPreferences().edit().putString(PREFERENCE_QA_MODE_URLS, strURLs).apply();
  }

  private String getQAModesFromPreferences() {
    return getQAModeUrlsPreferences().getString(PREFERENCE_QA_MODE_URLS, "");
  }
}
