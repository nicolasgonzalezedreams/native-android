package com.odigeo.dataodigeo.preferences;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.odigeo.data.preferences.PackageController;

public class PackageControllerImpl implements PackageController {

  private Context context;

  public PackageControllerImpl(Context context) {
    this.context = context;
  }

  @Override public int getApplicationVersionCode() {
    PackageManager packageManager = context.getPackageManager();
    try {
      PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);

      return packageInfo.versionCode;
    } catch (Exception e) {
      return 0;
    }
  }
}