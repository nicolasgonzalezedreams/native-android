package com.odigeo.dataodigeo.alarms;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.odigeo.data.localizable.OneCMSAlarmsController;
import com.odigeo.data.tracker.CrashlyticsController;
import java.util.Calendar;

public class OneCMSAlarmsControllerImpl implements OneCMSAlarmsController {

  public static final String TAG = "OneCMSAlarmsController";
  public static final String EXTRA_ALARM_ID_KEY = "alarm_id";
  private static final String[] REPEATING_ALARM_HOURS = { "07:15", "23:00" };
  private static final int ALARM_REPEATING_INTERVAL = 1440;
  private static final int ALARM_SHORT_RETRY = 3600;
  private static final String PENDING_INTENT_ACTION = "com.edreams.travel.OneCMSAlarm";

  private final Context context;
  private Class<?> receiver;

  private CrashlyticsController crashlyticsController;

  public OneCMSAlarmsControllerImpl(Context context, Class<?> receiver,
      CrashlyticsController crashlyticsController) {
    this.context = context;
    this.receiver = receiver;
    this.crashlyticsController = crashlyticsController;
  }

  @Override public void setOneCMSUpdateAlarms() {
    Calendar today = Calendar.getInstance();
    for (int i = 0; i < REPEATING_ALARM_HOURS.length; i++) {
      try {
        Calendar alarmTime = Calendar.getInstance();

        String[] timeParts = REPEATING_ALARM_HOURS[i].split(":");
        alarmTime.set(Calendar.HOUR_OF_DAY, Integer.valueOf(timeParts[0]));
        alarmTime.set(Calendar.MINUTE, Integer.valueOf(timeParts[1]));
        alarmTime.set(Calendar.SECOND, 0);
        //if the alarm that's being setting is before the current hour, then
        //set it for the next day
        if (alarmTime.before(today)) {
          alarmTime.add(Calendar.MINUTE, ALARM_REPEATING_INTERVAL);
        }

        //I sum 1 to i as Id for this alarm since the alarm with id = 0 is the one
        //that should be set as a retry,
        programRepeatingAlarmManager(alarmTime, i + 1);
      } catch (Exception ex) {
        //couldn't parse the alarm...
        crashlyticsController.trackNonFatal(ex);
        Log.i(TAG, "error setting the alarm");
      }
    }
  }

  @Override public void retryOneCMSUpdate() {
    Calendar alarmTime = Calendar.getInstance();
    alarmTime.add(Calendar.SECOND, ALARM_SHORT_RETRY);

    Intent intent = new Intent(context, receiver);
    intent.setAction(PENDING_INTENT_ACTION);
    PendingIntent pIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    alarmManager.set(AlarmManager.RTC_WAKEUP, alarmTime.getTimeInMillis(), pIntent);
  }

  private void programRepeatingAlarmManager(Calendar alarmTime, int id) {
    Intent intent = new Intent(context, receiver);
    //just for test purpose, send the id of the alarm in the intent so it can be accesible through
    //the receiver
    intent.putExtra(EXTRA_ALARM_ID_KEY, id);
    PendingIntent pIntent = PendingIntent.getBroadcast(context, id, intent, 0);

    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime.getTimeInMillis(),
        AlarmManager.INTERVAL_DAY, pIntent);
  }
}
