package com.odigeo.dataodigeo.download;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import com.odigeo.data.download.DownloadController;
import com.odigeo.data.download.DownloadManagerController;
import java.util.Map;

public class DownloadControllerImpl implements DownloadController {

  private static long enqueue;
  private DownloadManager dwnManager;
  private Context context;
  private DownloadManagerController dwnActions;
  private String filename;

  public DownloadControllerImpl(Context context) {
    this.context = context;

    BroadcastReceiver receiver = new BroadcastReceiver() {
      @Override public void onReceive(Context context, Intent intent) {
        if (dwnActions != null) {
          String action = intent.getAction();
          if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(enqueue);
            if (dwnManager == null) {
              dwnManager = (DownloadManager) DownloadControllerImpl.this.context.getSystemService(
                  Context.DOWNLOAD_SERVICE);
            }
            Cursor c = dwnManager.query(query);
            if (c.moveToFirst()) {
              int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
              if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                Log.d("DOWNLOAD MANAGER",
                    "URI: " + c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI)));
                dwnActions.doAfterDownloadAction(filename);
              } else if (DownloadManager.STATUS_FAILED == c.getInt(columnIndex)) {
                Log.d("DOWNLOAD MANAGER",
                    "ERROR Reason: " + c.getInt(c.getColumnIndex(DownloadManager.COLUMN_REASON)));
                switch (c.getInt(c.getColumnIndex(DownloadManager.COLUMN_REASON))) {
                  case DownloadManager.ERROR_INSUFFICIENT_SPACE:
                    dwnActions.onInsufficientSpaceError();
                    break;
                  default:
                    dwnActions.onDownloadFailedError();
                    break;
                }
              }
            }
            c.close();
          }
        }
      }
    };

    this.context.registerReceiver(receiver,
        new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
  }

  @Override public void subscribeDownloader(DownloadManagerController downloader) {
    this.dwnActions = downloader;
    if (dwnManager == null) {
      dwnManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
    }
    Uri myDownloads = Uri.parse("content://downloads/my_downloads");
    DownloadManager.Query query = new DownloadManager.Query();
    query.setFilterById(enqueue);
    context.getContentResolver()
        .registerContentObserver(myDownloads, true,
            new DownloadObserver(new Handler(), enqueue, context, dwnActions));
  }

  @Override public void unsubscribeDownloader() {
    this.dwnActions = null;
  }

  @Override public void startDownload(String url, String filename) {
    startDownload(url, filename, null);
  }

  @Override public void startDownload(String url, String filename, Map<String, String> headers) {
    dwnManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
    this.filename = filename;
    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
    if (headers != null) {
      for (Map.Entry<String, String> entry : headers.entrySet()) {
        request.addRequestHeader(entry.getKey(), entry.getValue());
      }
    }
    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
    request.setDestinationInExternalFilesDir(context, Environment.DIRECTORY_DOWNLOADS,
        this.filename + ".pdf");
    request.setTitle(this.filename);
    enqueue = dwnManager.enqueue(request);
    Uri myDownloads = Uri.parse("content://downloads/my_downloads");
    DownloadManager.Query query = new DownloadManager.Query();
    query.setFilterById(enqueue);
    context.getContentResolver()
        .registerContentObserver(myDownloads, true,
            new DownloadObserver(new Handler(), enqueue, context, dwnActions));
  }

  @Override public void cancelDownload() {
    if (dwnManager != null) {
      dwnManager.remove(enqueue);
    }
  }

  public boolean isDownloadRunning() {
    if (dwnManager == null) {
      dwnManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
    }
    DownloadManager.Query query = new DownloadManager.Query();
    query.setFilterById(enqueue);
    Cursor c = dwnManager.query(query);
    if (c.moveToFirst()) {
      int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
      return c.getInt(columnIndex) == DownloadManager.STATUS_RUNNING;
    }
    c.close();
    return false;
  }
}

