package com.odigeo.dataodigeo.download;

import android.app.DownloadManager;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import com.odigeo.data.download.DownloadManagerController;

/**
 * Created by matia on 11/17/2015.
 */
class DownloadObserver extends ContentObserver {

  private static long mEnqueue;
  private DownloadManagerController mDwnActions;
  private Context mContext;

  /**
   * Creates a content observer.
   *
   * @param handler The handler to run {@link #onChange} on, or null if none.
   */
  public DownloadObserver(Handler handler, long enqueue, Context context,
      DownloadManagerController dwnActions) {
    super(handler);
    mDwnActions = dwnActions;
    mContext = context;
    mEnqueue = enqueue;
  }

  @Override public void onChange(boolean selfChange, Uri uri) {
    DownloadManager mDwnManager =
        (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
    DownloadManager.Query query = new DownloadManager.Query();
    query.setFilterById(mEnqueue);
    Cursor c = mDwnManager.query(query);
    if (c.moveToFirst()) {
      int downloadedIndex = c.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR);
      long downloaded = c.getLong(downloadedIndex);
      int sizeIndex = c.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES);
      long size = c.getLong(sizeIndex);
      if (size != -1) {
        if (mDwnActions != null) {
          mDwnActions.setTotalDownloadSize(size);
        }
      }
      if (mDwnActions != null) {
        mDwnActions.onProgress(downloaded);
      }
    }
    c.close();
  }
}
