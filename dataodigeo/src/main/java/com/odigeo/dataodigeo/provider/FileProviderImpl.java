package com.odigeo.dataodigeo.provider;

import android.content.Context;
import android.os.Environment;
import com.odigeo.provider.FileProvider;
import java.io.File;

public class FileProviderImpl implements FileProvider {

  private final Context context;

  public FileProviderImpl(Context context) {
    this.context = context;
  }

  @Override public File getExternalStorageDirectory() {
    return Environment.getExternalStorageDirectory();
  }

  @Override public File getExternalStorageDownloadDirectory() {
    return new File(Environment.getExternalStorageDirectory().getAbsolutePath()
        + File.separator + "Android" + File.separator
        + "data" + File.separator
        + context.getPackageName() + File.separator + "files" + File.separator
        + "Download" + File.separator);
  }
}
