package com.odigeo.dataodigeo.tracker;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 7/12/15
 */
public class RootUtil {

  private static final String TEST_KEYS_TAG = "test-keys";
  private static final String[] PATHS = new String[] {
      "/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su",
      "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su",
      "/data/local/su"
  };
  private static final String[] PROCESSES_ARRAY = new String[] { "/system/xbin/which", "su" };

  /**
   * Constructor to prevent the instantiation
   */
  private RootUtil() {
    // Nothing
  }

  /**
   * Retrieves if the current device has root permission.
   *
   * @return If the device is rooted.
   */
  public static boolean isDeviceRooted() {
    return checkByTag() || checkByPaths() || checkByCommand();
  }

  /**
   * Review if the root tag is activated in the device.
   *
   * @return If exists the root tag.
   */
  private static boolean checkByTag() {
    String buildTags = android.os.Build.TAGS;
    return buildTags != null && buildTags.contains(TEST_KEYS_TAG);
  }

  /**
   * Review if the root paths exist in the device.
   *
   * @return If some root path exists.
   */
  private static boolean checkByPaths() {
    for (String path : PATHS) {
      if (new File(path).exists()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Check if the root command works.
   *
   * @return If the command can be executed.
   */
  private static boolean checkByCommand() {
    Process process = null;
    try {
      process = Runtime.getRuntime().exec(PROCESSES_ARRAY);
      BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
      return in.readLine() != null;
    } catch (Throwable t) {
      return false;
    } finally {
      if (process != null) {
        process.destroy();
      }
    }
  }
}