package com.odigeo.dataodigeo.tracker;

import android.app.Application;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.format.DateFormat;
import android.util.Log;
import com.appsee.Appsee;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.localytics.android.Localytics;
import com.localytics.android.LocalyticsActivityLifecycleCallbacks;
import com.odigeo.data.entity.tracker.Product;
import com.odigeo.data.entity.tracker.Transaction;
import com.odigeo.data.tracker.CustomDimensionFormatter;
import com.odigeo.data.tracker.TrackerControllerInterface;
import com.odigeo.helper.ABTestHelper;
import com.tune.ma.application.TuneActivityLifecycleCallbacks;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.localytics.android.Localytics.setCustomDimension;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_ABOUT_US;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_BOOKING_REJECTED;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_CONFIRM_PURCHASE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_FULL_PRICE_INFORMATION;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_LAUNCH_EVENTS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_MTT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_MTT_NOTIFICATIONS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_NAVIGATION_ELEMENTS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_PAYMENT_DETAILS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_PWD_RECOVERY_EMAIL;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_RATE_APP;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_RATE_CAN_IMPROVE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_RATE_OK;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_REGISTER_FORM;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_SIGN_IN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ACTION_TRIP_DETAILS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ADULTS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ANALYTICS_BRAND_TRACKER;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ANALYTICS_MARKET_TRACKER;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.APPSEE_CATEGORY_KEY;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.APPSEE_LABEL_KEY;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.APP_LAUNCH_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CARS_HOME_PAGE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_ABOUT_US;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_CONFIGURATION_SCREEN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_CONFIRMATION_PAGE_CONFIRMED;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_CONFIRMATION_PAGE_PENDING;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_FLIGHTS_CONFIRMATION_PAGE_REJECTED;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_FLIGHTS_PAYMENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_FLIGHTS_RESULT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_HOME;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_LAUNCH_EVENTS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_MY_TRIPS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_MY_TRIPS_TRIP_INFO;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_RATE_APP;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_RATE_APP_LEAVE_FEEDBACK;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_SSO_LOGIN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_SSO_PWD_RECOVERY;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CATEGORY_SSO_REGISTER;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CHILDREN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CLASS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CLICKED_FORGOT_PASSWORD;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CLICKED_NEW_USER_SIGN_UP;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CLICKED_ON_LOGIN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CLICKED_PURCHASE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CLICKED_SIGN_IN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CLICKED_SIGN_IN_FACEBOOK;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CLICKED_SIGN_IN_GOOGLE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.CURRENT_USER_LOGIN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ENTERED_EMAIL_ADDRESS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ENTERED_PASSWORD;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.FLIGHT_BOOKING_SUMMARY_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.FLIGHT_PRICE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.FLIGHT_PRICE_001;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.FLIGHT_PRICE_1000;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.FLIGHT_PRICE_101;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.FLIGHT_PRICE_200;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.FLIGHT_PRICE_300;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.FLIGHT_PRICE_500;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.FLIGHT_PRICE_700;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.HOME_LOGIN_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.HOTELS_HOME_PAGE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.INFANTS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.INSURANCE_TYPE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_ADD_TO_CALENDAR_CLICKS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_ALERT_VIEW_CLOSED;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_ALERT_VIEW_SHOWN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_CALL_NOW_CLICKS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_GENERAL_CONDITIONS_CLICKS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_HELP_CENTER_CLICKS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_LOGIN_USER_WITHOUT_SAVED_METHODS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_LOGIN_USER_WITH_SAVED_METHODS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_MENU_OPEN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_MTT_CARD_BLOCKED;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_MTT_NOTIFICATIONS_OFF;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_MTT_NOTIFICATIONS_ON;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_OPEN_DESTINATION_GUIDE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_PURCHASE_CLICKS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_RATE_CAN_IMPROVE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_RATE_OK;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SEND_FEEDBACK;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SESSION_WITH_ACTIVE_BOOKINGS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SSO_EMAIL_AUTHENTICATION_FACEBOOK_ERROR;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SSO_EMAIL_AUTHENTICATION_GMAIL_ERROR;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_SSO_REGISTER;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_USER_NOT_STORED_PAYMENT_METHOD;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_USER_NOT_USED_SAVED_PAYMENT_METHOD_STORE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_USER_STORED_PAYMENT_METHOD;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LABEL_USER_USED_SAVED_PAYMENT_METHOD_STORE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LEG_1_AIRLINE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LEG_1_ARRIVAL_AIRPORT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LEG_1_DEPARTURE_AIRPORT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LEG_1_DEPARTURE_DATE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LEG_1_DEPARTURE_DAY;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LEG_2_AIRLINE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LEG_2_ARRIVAL_AIRPORT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LEG_2_DEPARTURE_AIRPORT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LEG_2_DEPARTURE_DATE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LEG_2_DEPARTURE_DAY;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LEG_3_AIRLINE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LEG_3_ARRIVAL_AIRPORT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LEG_3_DEPARTURE_AIRPORT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LEG_3_DEPARTURE_DATE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LEG_3_DEPARTURE_DAY;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LOCALYTICS_LOGIN_STATUS_CUSTOM_INDEX;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LOGGED_IN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.LOGIN_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.MY_TRIPS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.NA;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.NOT_LOGGED_IN;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.NUMBER_BAGS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.ONLY_DIRECT_FLIGHTS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.PURCHASE_INSURANCE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.PURCHASE_SUCCESSFULL;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.RETURN_DATE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.RETURN_DAY;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.SCREEN_PAYMENT_T_CS;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.SEARCH_FLIGHTS_SUMMARY_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.SIGN_UP_EVENT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.SOURCE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.SOURCE_DIRECT;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.SOURCE_LOCAL_NOTIFICATION;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.SOURCE_PUSH_NOTIFICATION;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.TAP_ON_TRAVEL_GUIDE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.TRIP_DURATION;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.TRIP_DURATION_0;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.TRIP_DURATION_1;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.TRIP_DURATION_2;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.TRIP_DURATION_3;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.TRIP_DURATION_4;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.TRIP_MULTI_STOP;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.TRIP_ONE_WAY;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.TRIP_ROUND_TRIP;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.TRIP_TYPE;
import static com.odigeo.dataodigeo.tracker.TrackerConstants.VALUE_PER_PASSENGER;

public class TrackerController implements TrackerControllerInterface {

  private static final String DATE_DAY_FORMAT = "EEEE";
  private static final String DATE_FORMAT = "dd-MM-yyyy";
  private static final String KEY_APPSEE = "APPSEE_APP_KEY";
  private static final String KEY_ANALYTICS = "ANALYTICS_APP_KEY";
  private static final int TRIP_INTERVAL_1 = 1;
  private static final int TRIP_INTERVAL_2 = 16;
  private static final int TRIP_INTERVAL_3 = 30;
  private static final int TRIP_INTERVAL_4 = 60;
  private static final int FLIGHT_INTERVAL_1 = 101;
  private static final int FLIGHT_INTERVAL_2 = 200;
  private static final int FLIGHT_INTERVAL_3 = 300;
  private static final int FLIGHT_INTERVAL_4 = 500;
  private static final int FLIGHT_INTERVAL_5 = 700;
  private static final int FLIGHT_INTERVAL_6 = 1000;
  private static final String GA_CUSTOM_DIMENSION_INDEX = "&cd1";
  private static final String GA_CUSTOM_DIMENSION_SSO_INDEX = "&cd2";
  private static final String PASSWORD = "password";

  private static final String PREFIX_DIMEN_FOR_TRACKER = "&cd";
  private static final int AB_TEST_DIMEN_MIN_VALUE = 16;
  private static final int AB_TEST_DIMEN_MAX_VALUE = 30;
  private static final int MARKET_DIMENSION = 1;

  private static TrackerController mInstance;
  private static Application mApplication;
  private final ABTestHelper abTestHelper;
  private final CustomDimensionFormatter customDimensionFormatter;
  private final String mMarket;
  private final boolean mSupportLocalytics;
  private String mAppseeKey;
  private AppEventsLogger mAppEventsLogger;
  private Map<String, Tracker> mAnalyticsTrackers;
  private GoogleAnalytics mGoogleAnalytics;
  private Tracker mAnalytics;

  private String analyticsBrandKey;

  private TrackerController(Application application, String market, ABTestHelper abTestHelper,
      CustomDimensionFormatter customDimensionFormatter) {
    mApplication = application;
    mMarket = market;
    this.abTestHelper = abTestHelper;
    this.customDimensionFormatter = customDimensionFormatter;
    mSupportLocalytics =
        !(RootUtil.isDeviceRooted() && Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT);
    mAppEventsLogger = AppEventsLogger.newLogger(application);
    mAnalyticsTrackers = new HashMap<>();
  }

  public static TrackerController newInstance(Application application, String market,
      ABTestHelper abTestHelper, CustomDimensionFormatter customDimensionFormatter) {
    if (mInstance == null) {
      mInstance =
          new TrackerController(application, market, abTestHelper, customDimensionFormatter);
    }
    return mInstance;
  }

  @Override public void initTracker() {
    try {
      ApplicationInfo applicationInfo = mApplication.getPackageManager()
          .getApplicationInfo(mApplication.getPackageName(), PackageManager.GET_META_DATA);
      Bundle bundle = applicationInfo.metaData;
      mAppseeKey = bundle.getString(KEY_APPSEE);
      analyticsBrandKey = bundle.getString(KEY_ANALYTICS);

      //Localytics
      mApplication.registerActivityLifecycleCallbacks(
          new LocalyticsActivityLifecycleCallbacks(mApplication));

      //Tune
      mApplication.registerActivityLifecycleCallbacks(new TuneActivityLifecycleCallbacks());

      //Google Analytics
      mGoogleAnalytics = GoogleAnalytics.getInstance(mApplication);
      mGoogleAnalytics.setLocalDispatchPeriod(1800);
      mAnalytics = mGoogleAnalytics.newTracker(analyticsBrandKey);
      mAnalytics.set(GA_CUSTOM_DIMENSION_INDEX, mMarket);

      if (isAnalyticsByBrand()) {
        mAnalyticsTrackers.put(ANALYTICS_BRAND_TRACKER,
            mGoogleAnalytics.newTracker(analyticsBrandKey));
      }

      updateDimensionForABTest();
    } catch (PackageManager.NameNotFoundException e) {
      Log.e(TrackerConstants.TAG, e.getMessage(), e);
    }
  }

  private boolean isAnalyticsByBrand() {
    return analyticsBrandKey != null && !analyticsBrandKey.isEmpty();
  }

  @Override public void setAnalyticsMarketKey(String marketKey) {
    mAnalyticsTrackers.put(ANALYTICS_MARKET_TRACKER, mGoogleAnalytics.newTracker(marketKey));
  }

  @Override public void clearAnalyticsMarketKey() {
    mAnalyticsTrackers.remove(ANALYTICS_MARKET_TRACKER);
  }

  @Override public void trackLocalyticsMyTripsClickedOnLogin() {
    Map<String, String> values = new HashMap<String, String>() {{
      put(CLICKED_ON_LOGIN, TrackerConstants.YES);
    }};

    trackLocalyticsEvent(MY_TRIPS, values);
  }

  @Override public void trackAddToCalendar() {
    trackAnalyticsEvent(CATEGORY_MY_TRIPS_TRIP_INFO, ACTION_TRIP_DETAILS, LABEL_ADD_TO_CALENDAR_CLICKS);
  }

  @Override public void startSession() {
    Appsee.start(mAppseeKey);
  }

  @Override public void trackDirectAppLaunch() {
    trackAppLaunch(SOURCE_DIRECT);
  }

  @Override public void trackPushNotificationAppLaunch() {
    trackAppLaunch(SOURCE_PUSH_NOTIFICATION);
  }

  @Override public void trackLocalNotificationAppLaunch() {
    trackAppLaunch(SOURCE_LOCAL_NOTIFICATION);
  }

  @Override public void trackOneWaySearchFlights(String departureAirport, String arrivalAirport,
      long departureDate, int adults, int children, int infants, boolean onlyDirectFlight,
      String classType, int tripDuration) {
    trackSearchFlights(departureAirport, arrivalAirport, getFormatDate(departureDate),
        getDayFromDate(departureDate), NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, adults, children,
        infants, onlyDirectFlight, classType, getTripDuration(tripDuration), TRIP_ONE_WAY);
  }

  @Override public void trackRoundTripSearchFlights(String departureAirport, String arrivalAirport,
      long departureDate, long returnDate, int adults, int children, int infants,
      boolean onlyDirectFlight, String classType, int tripDuration) {
    trackSearchFlights(departureAirport, arrivalAirport, getFormatDate(departureDate),
        getDayFromDate(departureDate), NA, NA, NA, NA, NA, NA, NA, NA, getFormatDate(returnDate),
        getDayFromDate(returnDate), adults, children, infants, onlyDirectFlight, classType,
        getTripDuration(tripDuration), TRIP_ROUND_TRIP);
  }

  @Override
  public void trackMultiStopSearchFlights(String departureAirport1, String arrivalAirport1,
      long departureDate1, String departureAirport2, String arrivalAirport2, long departureDate2,
      String departureAirport3, String arrivalAirport3, long departureDate3, long returnDate,
      int adults, int children, int infants, boolean onlyDirectFlight, String classType,
      int tripDuration) {
    //TODO Refactor with OdigeoSearchActivity refactor
    trackSearchFlights(departureAirport1, arrivalAirport1, getFormatDate(departureDate1),
        getDayFromDate(departureDate1), departureAirport2, arrivalAirport2,
        getFormatDate(departureDate2), getDayFromDate(departureDate2),
        departureAirport3 == null ? NA : departureAirport3,
        arrivalAirport3 == null ? NA : arrivalAirport3,
        departureDate3 == 0 ? NA : getFormatDate(departureDate3),
        departureDate3 == 0 ? NA : getDayFromDate(departureDate3), getFormatDate(returnDate),
        getDayFromDate(returnDate), adults, children, infants, onlyDirectFlight, classType,
        getTripDuration(tripDuration), TRIP_MULTI_STOP);
  }

  @Override
  public void trackFlightBookingSummary(int numAdults, int numChildren, String classFlight,
      BigDecimal flightPrice, int numInfants, String insuranceType, String airline1,
      String arrivalAirport1, String departureAirport1, long departureDate1, String airline2,
      String arrivalAirport2, String departureAirport2, long departureDate2, String airline3,
      String arrivalAirport3, String departureAirport3, long departureDate3, String purchaseSuccess,
      boolean purchaseInsurance, long returnDate, String tripType, int valuePerPassenger,
      int numberOfBags) {

    Map<String, String> values = new HashMap<>();
    values.put(ADULTS, String.valueOf(numAdults));
    values.put(CHILDREN, String.valueOf(numChildren));
    values.put(CLASS, classFlight);
    values.put(FLIGHT_PRICE, getFlighPriceInterval(flightPrice.intValue()));
    values.put(INFANTS, String.valueOf(numInfants));
    values.put(INSURANCE_TYPE, insuranceType);
    values.put(LEG_1_AIRLINE, airline1);
    values.put(LEG_1_ARRIVAL_AIRPORT, arrivalAirport1);
    values.put(LEG_1_DEPARTURE_AIRPORT, departureAirport1);
    values.put(LEG_1_DEPARTURE_DATE, getFormatDate(departureDate1));
    values.put(LEG_1_DEPARTURE_DAY, getDayFromDate(departureDate1));
    values.put(LEG_2_AIRLINE, airline2);
    values.put(LEG_2_ARRIVAL_AIRPORT, arrivalAirport2);
    values.put(LEG_2_DEPARTURE_AIRPORT, departureAirport2);
    values.put(LEG_2_DEPARTURE_DATE, getFormatDate(departureDate2));
    values.put(LEG_2_DEPARTURE_DAY, getDayFromDate(departureDate2));
    values.put(LEG_3_AIRLINE, airline3);
    values.put(LEG_3_ARRIVAL_AIRPORT, arrivalAirport3);
    values.put(LEG_3_DEPARTURE_AIRPORT, departureAirport3);
    values.put(LEG_3_DEPARTURE_DATE, getFormatDate(departureDate3));
    values.put(LEG_3_DEPARTURE_DAY, getDayFromDate(departureDate3));
    values.put(PURCHASE_SUCCESSFULL, purchaseSuccess);
    values.put(PURCHASE_INSURANCE,
        (purchaseInsurance) ? TrackerConstants.YES : TrackerConstants.NO);
    values.put(RETURN_DATE, getFormatDate(returnDate));
    values.put(RETURN_DAY, getDayFromDate(returnDate));
    values.put(TRIP_TYPE, tripType);
    values.put(VALUE_PER_PASSENGER, getFlighPriceInterval(valuePerPassenger));
    values.put(NUMBER_BAGS, String.valueOf(numberOfBags));
    trackLocalyticsEvent(FLIGHT_BOOKING_SUMMARY_EVENT, values);
  }

  @Override public void trackLocalyticsRegisterPage(boolean enteredEmail, boolean enteredPassword,
      boolean clickSignUp, boolean clickFacebook, boolean clickGooglePlus) {
    Map<String, String> values = new HashMap<>();
    values.put(ENTERED_EMAIL_ADDRESS, enteredEmail ? TrackerConstants.YES : TrackerConstants.NO);
    values.put(ENTERED_PASSWORD, enteredPassword ? TrackerConstants.YES : TrackerConstants.NO);
    values.put(CLICKED_SIGN_IN, clickSignUp ? TrackerConstants.YES : TrackerConstants.NO);
    values.put(CLICKED_SIGN_IN_FACEBOOK,
        clickFacebook ? TrackerConstants.YES : TrackerConstants.NO);
    values.put(CLICKED_SIGN_IN_GOOGLE,
        clickGooglePlus ? TrackerConstants.YES : TrackerConstants.NO);
    trackLocalyticsEvent(SIGN_UP_EVENT, values);
  }

  @Override
  public void trackRegisterPage(String category, String action, String label, String event,
      boolean enteredEmail, boolean enteredPassword, boolean clickSignUp, boolean clickFacebook,
      boolean clickGooglePlus) {
    trackAnalyticsEvent(category, action, label);
    trackAppseeEvent(event);
    trackLocalyticsRegisterPage(enteredEmail, enteredPassword, clickSignUp, clickFacebook,
        clickGooglePlus);
  }

  @Override public void trackLocalyticsHomeLoginPage(String event, boolean userLoginClick,
      boolean userSignUpClick) {
    Map<String, String> values = new HashMap<>();
    values.put(CURRENT_USER_LOGIN, userLoginClick ? TrackerConstants.YES : TrackerConstants.NO);
    values.put(CLICKED_NEW_USER_SIGN_UP,
        userLoginClick ? TrackerConstants.YES : TrackerConstants.NO);
    trackLocalyticsEvent(HOME_LOGIN_EVENT, values);
    if (event != null) {
      trackAppseeEvent(event);
    }
  }

  @Override
  public void trackJoinUsButtons(String event, boolean userLoginClick, boolean userSignUpClick,
      String category, String action, String label) {
    trackLocalyticsHomeLoginPage(event, userLoginClick, userSignUpClick);
    trackAnalyticsEvent(category, action, label);
  }

  @Override public void trackLocalyticsLoginPage(boolean enteredEmail, boolean enteredPassword,
      boolean clickSignUp, boolean clickFacebook, boolean clickGooglePlus, boolean forgotPassword) {
    Map<String, String> values = new HashMap<>();
    values.put(ENTERED_EMAIL_ADDRESS, enteredEmail ? TrackerConstants.YES : TrackerConstants.NO);
    values.put(ENTERED_PASSWORD, enteredPassword ? TrackerConstants.YES : TrackerConstants.NO);
    values.put(CLICKED_SIGN_IN, clickSignUp ? TrackerConstants.YES : TrackerConstants.NO);
    values.put(CLICKED_SIGN_IN_FACEBOOK,
        clickFacebook ? TrackerConstants.YES : TrackerConstants.NO);
    values.put(CLICKED_SIGN_IN_GOOGLE,
        clickGooglePlus ? TrackerConstants.YES : TrackerConstants.NO);
    values.put(CLICKED_FORGOT_PASSWORD,
        forgotPassword ? TrackerConstants.YES : TrackerConstants.NO);
    trackLocalyticsEvent(LOGIN_EVENT, values);
  }

  @Override public void trackLoginPage(String category, String action, String label, String event,
      boolean enteredEmail, boolean enteredPassword, boolean clickSignUp, boolean clickFacebook,
      boolean clickGooglePlus, boolean forgotPassword) {
    trackAppseeEvent(event);
    trackLocalyticsLoginPage(enteredEmail, enteredPassword, clickSignUp, clickFacebook,
        clickGooglePlus, forgotPassword);
    trackAnalyticsEvent(category, action, label);
  }

  @Override public void trackLoginRequestPassPage(String category, String action, String label,
      boolean enteredEmail, boolean enteredPassword, boolean clickSignUp, boolean clickFacebook,
      boolean clickGooglePlus, boolean forgotPassword) {
    trackAnalyticsEvent(category, action, label);
    trackLocalyticsLoginPage(enteredEmail, enteredPassword, clickSignUp, clickFacebook,
        clickGooglePlus, forgotPassword);
  }

  @Override
  public void trackPAXViewPage(String event, String category, String action1, String label1,
      String label2) {
    trackAppseeEvent(event);
    trackAnalyticsEvent(category, action1, label1);
  }

  @Override
  public void trackMyTripsManualImportViewPage(String event, String category, String action,
      String label) {
    trackAppseeEvent(event);
    trackAnalyticsEvent(category, action, label);
  }

  @Override public void trackAppseeEvent(String event) {
    Appsee.addEvent(event);
  }

  @Override public void trackAppseeEvent(String event, Map<String, Object> properties) {
    Appsee.addEvent(event, properties);
  }

  @Override public void trackLocalyticsEvent(String event) {
    Localytics.tagEvent(event);
  }

  @Override
  public void trackLocalyticsEventWithOneAtribute(String event, String atribute, String value) {
    Map<String, String> values = new HashMap<>();
    values.put(atribute, value);
    trackLocalyticsEvent(event, values);
  }

  @Override public void trackAnalyticsScreen(String screen) {
    for (Map.Entry<String, Tracker> entry : mAnalyticsTrackers.entrySet()) {
      entry.getValue().setScreenName(screen);

      entry.getValue().send(new HitBuilders.ScreenViewBuilder().build());
    }
  }

  @Override public void trackAnalyticsEvent(String category, String action, String label) {
    for (Map.Entry<String, Tracker> entry : mAnalyticsTrackers.entrySet()) {

      HitBuilders.EventBuilder eventBuilder =
          new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label);

      entry.getValue()
          .send(eventBuilder.build());
    }
  }

  @Override public void trackPaxTitleValidation(boolean isTitleValid) {
    if (!isTitleValid) {
      trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAX,
          TrackerConstants.ACTION_PAX_DETAILS_ERROR, TrackerConstants.LABEL_TITLE_MISSING_ERROR);
    }
  }

  @Override public void trackPaxNameValidation(boolean isNameValid) {
    if (!isNameValid) {
      trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAX,
          TrackerConstants.ACTION_PAX_DETAILS_ERROR, TrackerConstants.LABEL_NAME_INVALID_ERROR);
    }
  }

  @Override public void trackPaxSurnameValidation(boolean isLastnameValid) {
    if (!isLastnameValid) {
      trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAX,
          TrackerConstants.ACTION_PAX_DETAILS_ERROR, TrackerConstants.LABEL_SURNAME_INVALID_ERROR);
    }
  }

  @Override public void trackContactInformation(boolean isNameValid, boolean isSurnameValid,
      boolean isEmailValid, boolean isCityValid, boolean isAddressValid, boolean isPostalCodeValid,
      boolean isPhoneValid) {
    if (!isNameValid) {
      trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAX,
          TrackerConstants.ACTION_CUSTOMER_DETAILS_ERROR,
          TrackerConstants.LABEL_NAME_INVALID_ERROR);
    }

    if (!isSurnameValid) {
      trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAX,
          TrackerConstants.ACTION_CUSTOMER_DETAILS_ERROR,
          TrackerConstants.LABEL_CARD_HOLDER_INVALID);
    }

    if (!isEmailValid) {
      trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAX,
          TrackerConstants.ACTION_CUSTOMER_DETAILS_ERROR,
          TrackerConstants.LABEL_EMAIL_INVALID_ERROR);
    }

    if (!isCityValid) {
      trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAX,
          TrackerConstants.ACTION_CUSTOMER_DETAILS_ERROR,
          TrackerConstants.LABEL_CITY_MISSING_ERROR);
    }

    if (!isAddressValid) {
      trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAX,
          TrackerConstants.ACTION_CUSTOMER_DETAILS_ERROR,
          TrackerConstants.LABEL_ADDRESS_MISSING_ERROR);
    }

    if (!isPostalCodeValid) {
      trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAX,
          TrackerConstants.ACTION_CUSTOMER_DETAILS_ERROR,
          TrackerConstants.LABEL_POSTCODE_MISSING_ERROR);
    }

    if (!isPhoneValid) {
      trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAX,
          TrackerConstants.ACTION_CUSTOMER_DETAILS_ERROR,
          TrackerConstants.LABEL_PHONE_NUMBER_INVALID_ERROR);
    }
  }

  @Override public void updateDimensionOnUserLogin(boolean isLogged) {
    if (isLogged) {
      for (Map.Entry<String, Tracker> entry : mAnalyticsTrackers.entrySet()) {
        entry.getValue()
            .set(GA_CUSTOM_DIMENSION_SSO_INDEX, TrackerConstants.USER_LOGGED_DIMENSION_VALUE);
      }
      setCustomDimension(LOCALYTICS_LOGIN_STATUS_CUSTOM_INDEX, LOGGED_IN);
    } else {
      for (Map.Entry<String, Tracker> entry : mAnalyticsTrackers.entrySet()) {
        entry.getValue()
            .set(GA_CUSTOM_DIMENSION_SSO_INDEX, TrackerConstants.USER_NOT_LOGGED_DIMENSION_VALUE);
      }
      setCustomDimension(LOCALYTICS_LOGIN_STATUS_CUSTOM_INDEX, NOT_LOGGED_IN);
    }
  }

  @Override public void updateDimensionForABTest() {
    for (Map.Entry<String, Tracker> entry : mAnalyticsTrackers.entrySet()) {
      Tracker value = entry.getValue();
      if (isAnalyticsByBrand()) {
        value.set(PREFIX_DIMEN_FOR_TRACKER + MARKET_DIMENSION, mMarket);
      }

      List<String> customDimensions =
          customDimensionFormatter.formatCustomDimension(abTestHelper.getDimensions());
      int dimensionCounter = AB_TEST_DIMEN_MIN_VALUE;

      for (String customDimension : customDimensions) {
        if (dimensionCounter < AB_TEST_DIMEN_MAX_VALUE) {
          value.set(PREFIX_DIMEN_FOR_TRACKER + dimensionCounter, customDimension);
          dimensionCounter++;
        }
      }
    }
  }

  @Override public void trackPassengerBaggage(int adultPassengersCount, int childPassengersCount,
      int adultPassengerBaggagePieces, int childPassengerBaggagePieces) {

    int adultNumberToTrack = Math.min(adultPassengersCount, adultPassengerBaggagePieces);
    String label = TrackerConstants.LABEL_PARTIAL_NUMBER_BAGGAGES + adultNumberToTrack;

    if (childPassengerBaggagePieces != 0 || childPassengersCount != 0) {
      label += "-" + Math.min(childPassengerBaggagePieces, childPassengersCount);
    }

    trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAX, TrackerConstants.ACTION_PAX_BAGGAGE,
        label);
  }

  @Override public void trackOpenTripGuide() {
    String category = CATEGORY_MY_TRIPS;
    String action = ACTION_TRIP_DETAILS;
    String label = LABEL_OPEN_DESTINATION_GUIDE;
    trackAnalyticsEvent(category, action, label);

    boolean openedGuide = true;
    Map<String, String> values = new HashMap<>();
    values.put(TAP_ON_TRAVEL_GUIDE, openedGuide ? TrackerConstants.YES : TrackerConstants.NO);
    Localytics.tagEvent(MY_TRIPS, values);

    Map<String, Object> properties = new HashMap<>();
    properties.put(APPSEE_CATEGORY_KEY, CATEGORY_MY_TRIPS);
    properties.put(APPSEE_LABEL_KEY, LABEL_OPEN_DESTINATION_GUIDE);
    trackAppseeEvent(ACTION_TRIP_DETAILS, properties);
  }

  public void trackMTTCardEvent(String label) {
    trackAnalyticsEvent(CATEGORY_HOME, ACTION_MTT, label);

    Map<String, Object> properties = new HashMap<>();
    properties.put(APPSEE_CATEGORY_KEY, CATEGORY_HOME);
    properties.put(APPSEE_LABEL_KEY, label);
    trackAppseeEvent(ACTION_MTT, properties);
  }

  public void trackFlightNotificationsEnabled(boolean enabled) {
    String label = enabled ? LABEL_MTT_NOTIFICATIONS_ON : LABEL_MTT_NOTIFICATIONS_OFF;
    trackAnalyticsEvent(CATEGORY_MY_TRIPS_TRIP_INFO, ACTION_MTT_NOTIFICATIONS, label);

    Map<String, Object> properties = new HashMap<>();
    properties.put(APPSEE_CATEGORY_KEY, CATEGORY_MY_TRIPS_TRIP_INFO);
    properties.put(APPSEE_LABEL_KEY, label);
    trackAppseeEvent(ACTION_MTT_NOTIFICATIONS, properties);
  }

  public void trackSSOUserWithServiceOptions() {
    trackAnalyticsEvent(CATEGORY_HOME, ACTION_MTT, LABEL_MTT_CARD_BLOCKED);

    Map<String, Object> properties = new HashMap<>();
    properties.put(APPSEE_CATEGORY_KEY, CATEGORY_HOME);
    properties.put(APPSEE_LABEL_KEY, LABEL_MTT_CARD_BLOCKED);
    trackAppseeEvent(ACTION_MTT, properties);
  }

  @Override public void setUserLocalyticsData(String name, String lastName, String email) {
    Localytics.setCustomerFirstName(name);
    Localytics.setCustomerLastName(lastName);
    Localytics.setCustomerEmail(email);
  }

  @Override public void trackFullTransparencyDialogAppearance(boolean isShown) {
    if (isShown) {
      trackAnalyticsEvent(CATEGORY_FLIGHTS_RESULT, ACTION_FULL_PRICE_INFORMATION,
          LABEL_ALERT_VIEW_SHOWN);
    } else {
      trackAnalyticsEvent(CATEGORY_FLIGHTS_RESULT, ACTION_FULL_PRICE_INFORMATION,
          LABEL_ALERT_VIEW_CLOSED);
    }
  }

  @Override public void trackAppRateThanksView() {
    trackAnalyticsEvent(CATEGORY_RATE_APP_LEAVE_FEEDBACK, ACTION_RATE_OK, LABEL_SEND_FEEDBACK);
  }

  @Override public void trackYourAppExperienceLikeItButton() {
    trackAnalyticsEvent(CATEGORY_RATE_APP, ACTION_RATE_APP, LABEL_RATE_OK);
  }

  @Override public void trackYourAppExperienceNotLikeItButton() {
    trackAnalyticsEvent(CATEGORY_RATE_APP, ACTION_RATE_APP, LABEL_RATE_CAN_IMPROVE);
  }

  @Override public void trackMyTripsRateAppLikeItButton() {
    trackAnalyticsEvent(CATEGORY_MY_TRIPS, ACTION_RATE_APP, LABEL_RATE_OK);
  }

  @Override public void trackMyTripsRateAppNotLikeItButton() {
    trackAnalyticsEvent(CATEGORY_MY_TRIPS, ACTION_RATE_APP, LABEL_RATE_CAN_IMPROVE);
  }

  @Override public void trackAppRateHelpUsImproveView() {
    trackAnalyticsEvent(CATEGORY_RATE_APP_LEAVE_FEEDBACK, ACTION_RATE_CAN_IMPROVE,
        LABEL_SEND_FEEDBACK);
  }

  @Override public void trackHelpCenterButtonAbout() {
    trackAnalyticsEvent(CATEGORY_ABOUT_US, ACTION_ABOUT_US, LABEL_HELP_CENTER_CLICKS);
  }

  @Override public void trackAppLaunched() {
    mAppEventsLogger.logEvent(AppEventsConstants.EVENT_NAME_ACTIVATED_APP);
  }

  @Override public void trackSearchLaunched(String flightType, int adults, int kids, int infants,
      List<String> departures, List<String> arrivals, List<String> dates,
      List<String> departureCountries, List<String> arrivalCountries) {
    Bundle bundle = new Bundle();
    bundle.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, flightType);
    bundle.putInt(TrackerConstants.EVENT_PARAM_NUM_ADULTS, adults);
    bundle.putInt(TrackerConstants.EVENT_PARAM_NUM_KIDS, kids);
    bundle.putInt(TrackerConstants.EVENT_PARAM_NUM_INFANTS, infants);

    bundle =
        setSegmentInfo(bundle, departures, arrivals, dates, departureCountries, arrivalCountries);

    mAppEventsLogger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED, bundle);
  }

  @Override
  public void trackSummaryReached(String currency, String flightType, int adults, int kids,
      int infants, double price, List<String> departures, List<String> arrivals, List<String> dates,
      List<String> airlines, List<String> departureCountries, List<String> arrivalCountries) {
    Bundle bundle = getFacebookTrackBundle(currency, flightType, adults, kids, infants);
    bundle =
        setSegmentInfo(bundle, departures, arrivals, dates, departureCountries, arrivalCountries);
    bundle = setAirlinesInfo(bundle, airlines);
    mAppEventsLogger.logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, price, bundle);
  }

  @Override
  public void trackSummaryContinue(String currency, String flightType, int adults, int kids,
      int infants, double price, List<String> departures, List<String> arrivals, List<String> dates,
      List<String> airlines, List<String> departureCountries, List<String> arrivalCountries) {
    Bundle bundle = getFacebookTrackBundle(currency, flightType, adults, kids, infants);
    bundle =
        setSegmentInfo(bundle, departures, arrivals, dates, departureCountries, arrivalCountries);
    bundle = setAirlinesInfo(bundle, airlines);
    mAppEventsLogger.logEvent(AppEventsConstants.EVENT_NAME_ADDED_TO_CART, price, bundle);
  }

  @Override
  public void trackPaymentReached(String currency, String flightType, int adults, int kids,
      int infants, double price, List<String> departures, List<String> arrivals, List<String> dates,
      List<String> airlines, List<String> departureCountries, List<String> arrivalCountries) {
    Bundle bundle = getFacebookTrackBundle(currency, flightType, adults, kids, infants);
    bundle =
        setSegmentInfo(bundle, departures, arrivals, dates, departureCountries, arrivalCountries);
    bundle = setAirlinesInfo(bundle, airlines);
    mAppEventsLogger.logEvent(AppEventsConstants.EVENT_NAME_INITIATED_CHECKOUT, price, bundle);
  }

  @Override
  public void trackPaymentSuccessful(BigDecimal revenueMargin, Currency currency, String flightType,
      int adults, int kids, int infants, List<String> departures, List<String> arrivals,
      List<String> dates, List<String> airlines, List<String> departureCountries,
      List<String> arrivalCountries) {

    Bundle bundle =
        getFacebookTrackBundle(currency.getCurrencyCode(), flightType, adults, kids, infants);
    bundle =
        setSegmentInfo(bundle, departures, arrivals, dates, departureCountries, arrivalCountries);
    bundle = setAirlinesInfo(bundle, airlines);

    mAppEventsLogger.logPurchase(revenueMargin, currency, bundle);
  }

  @Override public void trackSignUp() {
    Bundle bundle = new Bundle();
    bundle.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, PASSWORD);
    mAppEventsLogger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, bundle);
  }

  @Override public void trackHotels() {
    mAppEventsLogger.logEvent(HOTELS_HOME_PAGE);
  }

  @Override public void trackCars() {
    mAppEventsLogger.logEvent(CARS_HOME_PAGE);
  }

  @Override public void trackSwitchNotificationsFlightStatus(boolean switchIsOn) {
    if (switchIsOn) {
      trackAnalyticsEvent(CATEGORY_CONFIGURATION_SCREEN, ACTION_MTT_NOTIFICATIONS,
          LABEL_MTT_NOTIFICATIONS_ON);
    } else {
      trackAnalyticsEvent(CATEGORY_CONFIGURATION_SCREEN, ACTION_MTT_NOTIFICATIONS,
          LABEL_MTT_NOTIFICATIONS_OFF);
    }
  }

  @Override public void trackConfirmationRejectedPhoneCall() {
    trackAnalyticsEvent(CATEGORY_FLIGHTS_CONFIRMATION_PAGE_REJECTED, ACTION_BOOKING_REJECTED,
        LABEL_CALL_NOW_CLICKS);
  }

  @NonNull
  private Bundle getFacebookTrackBundle(String currency, String flightType, int adults, int kids,
      int infants) {
    Bundle bundle = new Bundle();
    bundle.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
    bundle.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, flightType);
    bundle.putInt(TrackerConstants.EVENT_PARAM_NUM_ADULTS, adults);
    bundle.putInt(TrackerConstants.EVENT_PARAM_NUM_KIDS, kids);
    bundle.putInt(TrackerConstants.EVENT_PARAM_NUM_INFANTS, infants);
    return bundle;
  }

  private void trackLocalyticsEvent(String eventName, Map<String, String> values) {
    if (mSupportLocalytics) {
      Localytics.tagEvent(eventName, values);
    }
  }

  private void trackAppLaunch(String source) {
    Map<String, String> values = new HashMap<>();
    values.put(SOURCE, source);
    trackLocalyticsEvent(APP_LAUNCH_EVENT, values);
  }

  private void trackSearchFlights(String departureAirport1, String arrivalAirport1,
      String departureDate1, String departureDay1, String departureAirport2, String arrivalAirport2,
      String departureDate2, String departureDay2, String departureAirport3, String arrivalAirport3,
      String departureDate3, String departureDay3, String returnDate, String returnDay, int adults,
      int children, int infants, boolean onlyDirectFlight, String classType, String tripDuration,
      String tripType) {
    Map<String, String> values = new HashMap<>();
    values.put(LEG_1_DEPARTURE_AIRPORT, departureAirport1);
    values.put(LEG_1_ARRIVAL_AIRPORT, arrivalAirport1);
    values.put(LEG_1_DEPARTURE_DATE, departureDate1);
    values.put(LEG_1_DEPARTURE_DAY, departureDay1);
    values.put(LEG_2_DEPARTURE_AIRPORT, departureAirport2);
    values.put(LEG_2_ARRIVAL_AIRPORT, arrivalAirport2);
    values.put(LEG_2_DEPARTURE_DATE, departureDate2);
    values.put(LEG_2_DEPARTURE_DAY, departureDay2);
    values.put(LEG_3_DEPARTURE_AIRPORT, departureAirport3);
    values.put(LEG_3_ARRIVAL_AIRPORT, arrivalAirport3);
    values.put(LEG_3_DEPARTURE_DATE, departureDate3);
    values.put(LEG_3_DEPARTURE_DAY, departureDay3);
    values.put(RETURN_DATE, returnDate);
    values.put(RETURN_DAY, returnDay);
    values.put(ADULTS, String.valueOf(adults));
    values.put(CHILDREN, String.valueOf(children));
    values.put(INFANTS, String.valueOf(infants));
    values.put(ONLY_DIRECT_FLIGHTS,
        (onlyDirectFlight) ? TrackerConstants.YES : TrackerConstants.NO);
    values.put(CLASS, classType);
    values.put(TRIP_DURATION, tripDuration);
    values.put(TRIP_TYPE, tripType);
    trackLocalyticsEvent(SEARCH_FLIGHTS_SUMMARY_EVENT, values);
  }

  private String getDayFromDate(long date) {
    if (date == 0) {
      return TrackerConstants.NA;
    } else {
      Date d = new Date();
      d.setTime(date);
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_DAY_FORMAT, Locale.US);
      return simpleDateFormat.format(d);
    }
  }

  private String getFormatDate(long date) {
    if (date == 0) {
      return TrackerConstants.NA;
    } else {
      Date d = new Date();
      d.setTime(date);
      return (String) DateFormat.format(DATE_FORMAT, d);
    }
  }

  private String getTripDuration(int tripDuration) {
    String duration = TRIP_DURATION_0;
    if (tripDuration >= TRIP_INTERVAL_4) {
      duration = TRIP_DURATION_4;
    } else if (tripDuration >= TRIP_INTERVAL_3) {
      duration = TRIP_DURATION_3;
    } else if (tripDuration >= TRIP_INTERVAL_2) {
      duration = TRIP_DURATION_2;
    } else if (tripDuration >= TRIP_INTERVAL_1) {
      duration = TRIP_DURATION_1;
    }

    return duration;
  }

  private String getFlighPriceInterval(int flightPrice) {
    String price = FLIGHT_PRICE_001;
    if (flightPrice >= FLIGHT_INTERVAL_6) {
      price = FLIGHT_PRICE_1000;
    } else if (flightPrice >= FLIGHT_INTERVAL_5) {
      price = FLIGHT_PRICE_700;
    } else if (flightPrice >= FLIGHT_INTERVAL_4) {
      price = FLIGHT_PRICE_500;
    } else if (flightPrice >= FLIGHT_INTERVAL_3) {
      price = FLIGHT_PRICE_300;
    } else if (flightPrice >= FLIGHT_INTERVAL_2) {
      price = FLIGHT_PRICE_200;
    } else if (flightPrice >= FLIGHT_INTERVAL_1) {
      price = FLIGHT_PRICE_101;
    }

    return price;
  }

  public Bundle setSegmentInfo(Bundle bundle, List<String> departures, List<String> arrivals,
      List<String> dates, List<String> departureCountries, List<String> arrivalCountries) {
    if (departures.size() > 0) {
      bundle.putString(TrackerConstants.EVENT_PARAM_ORIGIN_1, departures.get(0));
      bundle.putString(TrackerConstants.EVENT_PARAM_DESTINATION_1, arrivals.get(0));
      bundle.putString(TrackerConstants.EVENT_PARAM_DATE_1, dates.get(0));
      bundle.putString(TrackerConstants.EVENT_PARAM_ARRIVAL_COUNTRY_1, arrivalCountries.get(0));
      bundle.putString(TrackerConstants.EVENT_PARAM_DEPARTURE_COUNTRY_1, departureCountries.get(0));
    }

    if (departures.size() > 1) {
      bundle.putString(TrackerConstants.EVENT_PARAM_ORIGIN_2, departures.get(1));
      bundle.putString(TrackerConstants.EVENT_PARAM_DESTINATION_2, arrivals.get(1));
      bundle.putString(TrackerConstants.EVENT_PARAM_DATE_2, dates.get(1));
      bundle.putString(TrackerConstants.EVENT_PARAM_ARRIVAL_COUNTRY_2, arrivalCountries.get(1));
      bundle.putString(TrackerConstants.EVENT_PARAM_DEPARTURE_COUNTRY_2, departureCountries.get(1));
    }

    if (departures.size() > 2) {
      bundle.putString(TrackerConstants.EVENT_PARAM_ORIGIN_3, departures.get(2));
      bundle.putString(TrackerConstants.EVENT_PARAM_DESTINATION_3, arrivals.get(2));
      bundle.putString(TrackerConstants.EVENT_PARAM_DATE_3, dates.get(2));
      bundle.putString(TrackerConstants.EVENT_PARAM_ARRIVAL_COUNTRY_3, arrivalCountries.get(2));
      bundle.putString(TrackerConstants.EVENT_PARAM_DEPARTURE_COUNTRY_3, departureCountries.get(2));
    }

    return bundle;
  }

  public Bundle setAirlinesInfo(Bundle bundle, List<String> airlines) {
    if (airlines.size() > 0) {
      bundle.putString(TrackerConstants.EVENT_PARAM_AIRLINE_1, airlines.get(0));
    }
    if (airlines.size() > 1) {
      bundle.putString(TrackerConstants.EVENT_PARAM_AIRLINE_2, airlines.get(1));
    }
    if (airlines.size() > 2) {
      bundle.putString(TrackerConstants.EVENT_PARAM_AIRLINE_3, airlines.get(2));
    }

    return bundle;
  }

  @VisibleForTesting void setAppEventsLogger(AppEventsLogger appEventsLogger) {
    mAppEventsLogger = appEventsLogger;
  }

  public void trackEcommerceTransaction(Transaction transaction, String gaCustomDim) {
    for (Map.Entry<String, Tracker> entry : mAnalyticsTrackers.entrySet()) {
      entry.getValue()
          .send(new HitBuilders.TransactionBuilder().setTransactionId(transaction.getId())
              .setAffiliation(transaction.getAffiliation())
              .setRevenue(transaction.getRevenue())
              .setTax(transaction.getTax())
              .setShipping(transaction.getShipping())
              .setCurrencyCode(transaction.getCurrencyCode())
              .setCustomDimension(1, gaCustomDim)
              .build());
    }
  }

  @Override public void trackEcommerceProduct(Product product, String gaCustomDim) {
    for (Map.Entry<String, Tracker> entry : mAnalyticsTrackers.entrySet()) {
      entry.getValue()
          .send(new HitBuilders.ItemBuilder().setTransactionId(product.getTransactionId())
              .setName(product.getItemName())
              .setSku(product.getItemSku())
              .setCategory(product.getItemCategory())
              .setPrice(product.getItemPrice())
              .setQuantity(product.getItemQuantity())
              .setCurrencyCode(product.getCurrencyCode())
              .setCustomDimension(1, gaCustomDim)
              .build());
    }
  }

  @Override public void trackApiRegisterError() {
    trackAnalyticsEvent(CATEGORY_SSO_REGISTER, ACTION_REGISTER_FORM, LABEL_SSO_REGISTER);
  }

  @Override public void trackApiLoginError() {
    trackAnalyticsEvent(CATEGORY_SSO_LOGIN, ACTION_SIGN_IN, LABEL_SSO_REGISTER);
  }

  @Override public void trackApiRecoverPasswordError() {
    trackAnalyticsEvent(CATEGORY_SSO_PWD_RECOVERY, ACTION_PWD_RECOVERY_EMAIL, LABEL_SSO_REGISTER);
  }

  @Override public void trackFacebookAuthenticationErrorRegister() {
    trackAnalyticsEvent(CATEGORY_SSO_REGISTER, ACTION_REGISTER_FORM,
        LABEL_SSO_EMAIL_AUTHENTICATION_FACEBOOK_ERROR);
  }

  @Override public void trackGoogleAuthenticationErrorRegister() {
    trackAnalyticsEvent(CATEGORY_SSO_REGISTER, ACTION_REGISTER_FORM,
        LABEL_SSO_EMAIL_AUTHENTICATION_GMAIL_ERROR);
  }

  @Override public void trackFacebookAuthenticationErrorLogin() {
    trackAnalyticsEvent(CATEGORY_SSO_LOGIN, ACTION_SIGN_IN,
        LABEL_SSO_EMAIL_AUTHENTICATION_FACEBOOK_ERROR);
  }

  @Override public void trackGoogleAuthenticationErrorLogin() {
    trackAnalyticsEvent(CATEGORY_SSO_LOGIN, ACTION_SIGN_IN,
        LABEL_SSO_EMAIL_AUTHENTICATION_GMAIL_ERROR);
  }

  @Override public void trackOpenMenu() {
    trackAnalyticsEvent(CATEGORY_HOME, ACTION_NAVIGATION_ELEMENTS, LABEL_MENU_OPEN);
  }

  @Override public void trackExpirationDateMissingInPayment() {
    trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
        TrackerConstants.ACTION_PAYMENT_DETAILS_ERROR,
        TrackerConstants.LABEL_EXPIRATION_DATE_MISSING);
  }

  @Override public void trackExpirationDateInvalidInPayment() {
    trackAnalyticsEvent(TrackerConstants.CATEGORY_FLIGHTS_PAYMENT,
        TrackerConstants.ACTION_PAYMENT_DETAILS_ERROR,
        TrackerConstants.LABEL_EXPIRATION_DATE_INVALID);
  }

  @Override public void trackPaymentTACClick() {
    trackAnalyticsEvent(CATEGORY_FLIGHTS_PAYMENT, ACTION_CONFIRM_PURCHASE,
        LABEL_GENERAL_CONDITIONS_CLICKS);
  }

  @Override public void trackPaymentTACScreen() {
    trackAnalyticsScreen(SCREEN_PAYMENT_T_CS);
  }

  @Override public void trackLocalyticsEventPurchase() {
    Localytics.tagEvent(CLICKED_PURCHASE);
  }

  @Override public void trackPaymentPurchaseClick() {
    trackAnalyticsEvent(CATEGORY_FLIGHTS_PAYMENT, ACTION_CONFIRM_PURCHASE, LABEL_PURCHASE_CLICKS);
  }

  @Override public void trackActiveBookingsOnLaunch() {
    trackAnalyticsEvent(CATEGORY_LAUNCH_EVENTS, ACTION_LAUNCH_EVENTS,
        LABEL_SESSION_WITH_ACTIVE_BOOKINGS);
  }

  @Override public void trackActiveBookingsOnImportTrip() {
    trackAnalyticsEvent(CATEGORY_MY_TRIPS, ACTION_LAUNCH_EVENTS,
        LABEL_SESSION_WITH_ACTIVE_BOOKINGS);
  }

  @Override public void trackActiveBookingsOnFlightConfirmed() {
    trackAnalyticsEvent(CATEGORY_CONFIRMATION_PAGE_CONFIRMED, ACTION_LAUNCH_EVENTS,
        LABEL_SESSION_WITH_ACTIVE_BOOKINGS);
  }

  @Override public void trackActiveBookingsOnFlightPending() {
    trackAnalyticsEvent(CATEGORY_CONFIRMATION_PAGE_PENDING, ACTION_LAUNCH_EVENTS,
        LABEL_SESSION_WITH_ACTIVE_BOOKINGS);
  }

  @Override public void trackUserLoggedWithPaymentMethods() {
    trackAnalyticsEvent(CATEGORY_FLIGHTS_PAYMENT, ACTION_PAYMENT_DETAILS,
        LABEL_LOGIN_USER_WITH_SAVED_METHODS);
  }

  @Override public void trackerUserLoggedWithoutPaymentMethods() {
    trackAnalyticsEvent(CATEGORY_FLIGHTS_PAYMENT, ACTION_PAYMENT_DETAILS,
        LABEL_LOGIN_USER_WITHOUT_SAVED_METHODS);
  }

  @Override public void trackStorePaymentMethodIsUsed() {
    trackAnalyticsEvent(CATEGORY_FLIGHTS_PAYMENT, ACTION_PAYMENT_DETAILS,
        LABEL_USER_USED_SAVED_PAYMENT_METHOD_STORE);
  }

  @Override public void trackStorePaymentMethodNotUsed() {
    trackAnalyticsEvent(CATEGORY_FLIGHTS_PAYMENT, ACTION_PAYMENT_DETAILS,
        LABEL_USER_NOT_USED_SAVED_PAYMENT_METHOD_STORE);
  }

  @Override public void trackPaymentMethodIsStore() {
    trackAnalyticsEvent(CATEGORY_FLIGHTS_PAYMENT, ACTION_PAYMENT_DETAILS,
        LABEL_USER_STORED_PAYMENT_METHOD);
  }

  @Override public void trackPaymentMethodIsNotStore() {
    trackAnalyticsEvent(CATEGORY_FLIGHTS_PAYMENT, ACTION_PAYMENT_DETAILS,
        LABEL_USER_NOT_STORED_PAYMENT_METHOD);
  }
}