package com.odigeo.dataodigeo.tracker.searchtracker;

import android.support.annotation.NonNull;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * This class was created to retain all the information that will be tracked by Facebook's SDK.
 * Whole the information will be converted to a SearchTrack instance, that object will give the
 * structure necessary to be converted to a JSON and sends it as "Searched string"/"Content Id".
 * It was built as static because firstly we need to give a minimum information about the flight,
 * after we will add more information in next stages of the booking flow.
 *
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 29/07/16
 */
public class SearchTrackHelper implements Serializable {
  private static final String ONE_WAY = "O";
  private static final String ROUND = "R";
  private static final String MULTI = "M";
  private Integer mAdults;
  private Integer mKids;
  private Integer mInfants;
  private Double mPrice;
  private List<String> mAirlines;
  private List<SegmentTracker> mSegments;
  private SimpleDateFormat mDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
  private String mFlightType;
  private ArrayList<String> mDepartures;
  private ArrayList<String> mArrivals;
  private ArrayList<String> mDates;
  private ArrayList<String> mArrivalCountries;
  private ArrayList<String> mDepartureCountries;

  public SearchTrackHelper() {
    // nothing
  }

  public void addAirline(String airline) {
    if (mAirlines == null) {
      mAirlines = new ArrayList<>();
    }
    if (!mAirlines.contains(airline)) {
      mAirlines.add(airline);
    }
  }

  public void addSegment(String departure, String arrival, long dateMillis, String departureCountry,
      String arrivalCountry) {
    if (mSegments == null) {
      mSegments = new ArrayList<>();
      mArrivals = new ArrayList<>();
      mDepartures = new ArrayList<>();
      mDates = new ArrayList<>();
      mDepartureCountries = new ArrayList<>();
      mArrivalCountries = new ArrayList<>();
    }

    SegmentTracker segmentTracker =
        new SegmentTracker(departure, arrival, mDateFormat.format(new Date(dateMillis)),
            departureCountry, arrivalCountry);

    if (!mSegments.contains(segmentTracker)) {
      mSegments.add(segmentTracker);
      mDepartures.add(segmentTracker.mDeparture);
      mArrivals.add(segmentTracker.mArrival);
      mDates.add(segmentTracker.mDate);
      mArrivalCountries.add(segmentTracker.mArrivalCountry);
      mDepartureCountries.add(segmentTracker.mDepartureCountry);
    }
  }

  protected SimpleDateFormat getDateFormat() {
    return mDateFormat;
  }

  public void clear() {
    mAdults = null;
    mKids = null;
    mInfants = null;
    mPrice = null;
    mAirlines = null;
    mSegments = null;
    mFlightType = null;
  }

  @NonNull private SearchTrack createSimpleSearchTrack() {
    SearchTrack searchTrack = new SearchTrack();
    searchTrack.mAdults = mAdults;
    searchTrack.mKids = mKids;
    searchTrack.mInfants = mInfants;
    searchTrack.mSegments = mSegments;
    return searchTrack;
  }

  public SearchTrack getSimpleSearchTrack() {
    return createSimpleSearchTrack();
  }

  public SearchTrack getSearchTrackWithPriceAndAirlines() {
    SearchTrack searchTrack = createSimpleSearchTrack();
    addPriceAndAirlines(searchTrack);
    return searchTrack;
  }

  private void addPriceAndAirlines(SearchTrack searchTrack) {
    searchTrack.mPrice = mPrice;
    searchTrack.mAirlines = mAirlines;
  }

  public String getFlightType() {
    return mFlightType;
  }

  public void setFlightType(int flightType) {
    switch (flightType) {
      case 0:
        mFlightType = ONE_WAY;
        break;
      case 1:
        mFlightType = ROUND;
        break;
      case 2:
        mFlightType = MULTI;
        break;
    }
  }

  public Integer getAdults() {
    return mAdults;
  }

  public void setAdults(int adults) {
    mAdults = adults;
  }

  public Integer getKids() {
    return mKids;
  }

  public void setKids(int kids) {
    mKids = kids;
  }

  public Integer getInfants() {
    return mInfants;
  }

  public void setInfants(int infants) {
    mInfants = infants;
  }

  public Double getPrice() {
    return mPrice;
  }

  public void setPrice(Double price) {
    mPrice = price;
  }

  public List<String> getAirlines() {
    return mAirlines;
  }

  public List<SegmentTracker> getSegments() {
    return mSegments;
  }

  public List<String> getOrigins() {
    return mDepartures;
  }

  public List<String> getDestinations() {
    return mArrivals;
  }

  public List<String> getDates() {
    return mDates;
  }

  public ArrayList<String> getArrivalCountries() {
    return mArrivalCountries;
  }

  public ArrayList<String> getDepartureCountries() {
    return mDepartureCountries;
  }
}