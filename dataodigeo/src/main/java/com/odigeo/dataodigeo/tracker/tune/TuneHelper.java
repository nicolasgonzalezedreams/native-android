package com.odigeo.dataodigeo.tracker.tune;

import java.util.ArrayList;
import java.util.List;

public class TuneHelper {
  private static List<TuneSegment> segments = new ArrayList<>();
  private static int passengers;
  private static double unitPrice;
  private static double revenueMarketing;
  private static String currencyCode;
  private static String bookingId;

  private TuneHelper() {
    // Nothing
  }

  public static void addSegment(TuneSegment segment) {
    segments.add(segment);
  }

  public static void setPassengers(int passengers) {
    TuneHelper.passengers = passengers;
  }

  public static void setRevenueMarketing(double revenueMarketing) {
    TuneHelper.revenueMarketing = revenueMarketing;
  }

  public static void setUnitPrice(double unitPrice) {
    TuneHelper.unitPrice = unitPrice;
  }

  public static void setCurrencyCode(String currencyCode) {
    TuneHelper.currencyCode = currencyCode;
  }

  public static void setBookingId(String bookingId) {
    TuneHelper.bookingId = bookingId;
  }

  public static void clear() {
    segments = new ArrayList<>();
    passengers = 0;
    unitPrice = 0.0;
    currencyCode = null;
    bookingId = null;
  }

  public static TuneWrapper getTuneWrapper() {
    return new TuneWrapper(segments, passengers, unitPrice, revenueMarketing, currencyCode,
        bookingId);
  }
}
