package com.odigeo.dataodigeo.tracker;

import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.helper.CrashlyticsUtil;

public class CrashlyticsControllerImpl implements CrashlyticsController {

  private CrashlyticsUtil crashlyticsUtil;

  public CrashlyticsControllerImpl(CrashlyticsUtil crashlyticsUtil) {
    this.crashlyticsUtil = crashlyticsUtil;
  }

  @Override public void trackNonFatal(Throwable throwable) {
    crashlyticsUtil.trackNonFatal(throwable);
  }

  @Override public void setString(String key, String value) {
    crashlyticsUtil.setString(key, value);
  }

  @Override public void setInt(String key, int value) {
    crashlyticsUtil.setInt(key, value);
  }

  @Override public void setLong(String key, long value) {
    crashlyticsUtil.setLong(key, value);
  }

  @Override public void setBool(String key, boolean value) {
    crashlyticsUtil.setBool(key, value);
  }

  @Override public void setDouble(String key, double value) {
    crashlyticsUtil.setDouble(key, value);
  }

  @Override public void setFloat(String key, float value) {
    crashlyticsUtil.setFloat(key, value);
  }
}
