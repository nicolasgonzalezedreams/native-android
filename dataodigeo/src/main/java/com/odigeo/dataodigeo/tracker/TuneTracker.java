package com.odigeo.dataodigeo.tracker;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.odigeo.data.tracker.TuneTrackerInterface;
import com.odigeo.dataodigeo.tracker.tune.TuneHelper;
import com.odigeo.dataodigeo.tracker.tune.TuneSegment;
import com.odigeo.dataodigeo.tracker.tune.TuneWrapper;
import com.tune.Tune;
import com.tune.TuneEvent;
import com.tune.TuneEventItem;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TuneTracker implements TuneTrackerInterface {

  private static TuneTracker sInstance;
  private Tune mTune;

  private TuneTracker() {
    mTune = Tune.getInstance();
  }

  public static TuneTracker getInstance() {
    if (sInstance == null) {
      sInstance = new TuneTracker();
    }
    return sInstance;
  }

  @Override public void setExistingUser(boolean isExistingUser) {
    if (isExistingUser) {
      mTune.setExistingUser(true);
    }
  }

  @Override public void trackSearch(boolean isSearch) {
    TuneEvent event;
    if (isSearch) {
      event = new TuneEvent(TuneEvent.SEARCH);
    } else {
      event = new TuneEvent(DEEPLINK_LAUNCH);
    }
    fillTuneEventWithTuneWrapper(event, false, false);
    mTune.measureEvent(event);
  }

  @Override public void trackViewProduct() {
    TuneEvent event = new TuneEvent(VIEW_PRODUCT);
    fillTuneEventWithTuneWrapper(event, true, false);
    mTune.measureEvent(event);
  }

  @Override public void trackViewBasket() {
    TuneEvent event = new TuneEvent(VIEW_BASKET);
    fillTuneEventWithTuneWrapper(event, true, false);
    mTune.measureEvent(event);
  }

  @Override public void trackTransaction() {
    TuneEvent event = new TuneEvent(PURCHASE);
    fillTuneEventWithTuneWrapper(event, true, true);
    mTune.measureEvent(event);
  }

  private void fillTuneEventWithTuneWrapper(TuneEvent event, boolean trackUnitPrice,
      boolean trackRevenue) {
    TuneWrapper tuneWrapper = TuneHelper.getTuneWrapper();
    List<TuneEventItem> tuneEventItems = new ArrayList<>();
    List<String> iatas = new ArrayList<>();
    Date[] dates = new Date[2];
    String departure = null;
    for (TuneSegment segment : tuneWrapper.getSegments()) {
      departure = extractDates(segment, departure, dates);
      extractIatas(iatas, segment);
    }

    TuneEventItem item = new TuneEventItem(getEventItemName(iatas));
    item.withQuantity(tuneWrapper.getPassengers());

    if (trackUnitPrice) {
      item.withUnitPrice(tuneWrapper.getUnitPrice());
    }

    if (trackRevenue) {
      item.withRevenue(tuneWrapper.getRevenueMarketing());
    }

    tuneEventItems.add(item);

    event.withEventItems(tuneEventItems);
    event.withDate1(dates[0]);
    event.withDate2(dates[1]);
    String bookingId = tuneWrapper.getBookingId();
    if (bookingId != null) {
      event.withAdvertiserRefId(bookingId);
    }

    mTune.setCurrencyCode(tuneWrapper.getCurrencyCode());
  }

  @NonNull private String getEventItemName(List<String> iatas) {
    StringBuilder stringBuilder = new StringBuilder();
    int size = iatas.size();
    for (int index = 0; index < size; index++) {
      if (index > 0) {
        stringBuilder.append(SEPARATOR);
      }
      stringBuilder.append(iatas.get(index));
    }
    return stringBuilder.toString();
  }

  private String extractDates(TuneSegment segment, String departure, Date[] dates) {
    if (departure == null) {
      departure = segment.getDeparture();
      dates[0] = new Date(segment.getDate());
    }
    if (!departure.equals(segment.getArrival())) {
      if (segment.getReturnDate() > 0) {
        dates[1] = new Date(segment.getReturnDate());
      } else if (segment.getDate() > dates[0].getTime()) {
        dates[1] = new Date(segment.getDate());
      } else if (segment.getReturnDate() == 0) {
        dates[1] = dates[0];
      }
    } else {
      dates[1] = new Date(segment.getDate());
    }
    return departure;
  }

  private void extractIatas(List<String> iatas, TuneSegment segment) {
    if (!iatas.contains(segment.getDeparture())) {
      iatas.add(segment.getDeparture());
    }
    if (!iatas.contains(segment.getArrival())) {
      iatas.add(segment.getArrival());
    }
  }

  @VisibleForTesting void setTune(Tune tune) {
    mTune = tune;
  }
}
