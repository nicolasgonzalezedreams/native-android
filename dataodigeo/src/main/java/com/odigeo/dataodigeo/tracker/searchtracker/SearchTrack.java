package com.odigeo.dataodigeo.tracker.searchtracker;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * This class models the information to be sent to the Facebook's SDK.
 *
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 29/07/16
 */

public class SearchTrack {
  private static final String ADULTS = "adults";
  private static final String KIDS = "kids";
  private static final String INFANTS = "infants";
  private static final String PRICE = "price";
  private static final String AIRLINES = "airlines";
  private static final String SEGMENTS = "segments";

  @SerializedName(ADULTS) Integer mAdults;
  @SerializedName(KIDS) Integer mKids;
  @SerializedName(INFANTS) Integer mInfants;
  @SerializedName(PRICE) Double mPrice;
  @SerializedName(AIRLINES) List<String> mAirlines;
  @SerializedName(SEGMENTS) List<SegmentTracker> mSegments;

  public double getPrice() {
    return mPrice == null ? 0.0 : mPrice;
  }
}
