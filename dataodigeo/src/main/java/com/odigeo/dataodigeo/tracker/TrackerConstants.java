package com.odigeo.dataodigeo.tracker;

/**
 * Created by josejuan.ramirez on 9/30/15.
 */
public class TrackerConstants {

  public static final String TAG = "TrackerController";

  /*
  Google Analytics types of tracker
   */
  public static final String ANALYTICS_BRAND_TRACKER = "analyticsBrandTracker";
  public static final String ANALYTICS_MARKET_TRACKER = "analyticsMarketTracker";

  /* Localytics dimensions:
   * Index used by the market custom dimen for localytics, must be the same that the one used in
   * the web dashboard
   */
  public static final int LOCALYTICS_LOGIN_STATUS_CUSTOM_INDEX = 2;

  //Localytics events
  public static final String APP_LAUNCH_EVENT = "App Launch";
  public static final String SEARCH_FLIGHTS_SUMMARY_EVENT = "Search Flights Summary";
  public static final String FLIGHTS_SEARCH_SUMMARY_EVENT = "Flights Search Summary";
  public static final String FLIGHT_BOOKING_SUMMARY_EVENT = "Flight Booking Summary";
  public static final String SIGN_UP_EVENT = "Sign up";
  public static final String HOME_LOGIN_EVENT = "Home Login";
  public static final String LOGIN_EVENT = "Login";
  public static final String MY_TRIPS = "My Trips";

  //Localytics attributes
  public static final String SOURCE = "Source";
  public static final String SOURCE_DIRECT = "Direct";
  public static final String SOURCE_PUSH_NOTIFICATION = "Push Notification";
  public static final String SOURCE_LOCAL_NOTIFICATION = "Local Notification";
  public static final String LEG_1_AIRLINE = "Leg 1 Airline";
  public static final String LEG_1_DEPARTURE_AIRPORT = "Leg 1 Departure Airport";
  public static final String LEG_1_ARRIVAL_AIRPORT = "Leg 1 Arrival Airport";
  public static final String LEG_1_DEPARTURE_DATE = "Leg 1 Departure Date";
  public static final String LEG_1_DEPARTURE_DAY = "Leg 1 Departure Day";
  public static final String LEG_2_AIRLINE = "Leg 2 Airline";
  public static final String LEG_2_DEPARTURE_AIRPORT = "Leg 2 Departure Airport";
  public static final String LEG_2_ARRIVAL_AIRPORT = "Leg 2 Arrival Airport";
  public static final String LEG_2_DEPARTURE_DATE = "Leg 2 Departure Date";
  public static final String LEG_2_DEPARTURE_DAY = "Leg 2 Departure Day";
  public static final String LEG_3_AIRLINE = "Leg 3 Airline";
  public static final String LEG_3_DEPARTURE_AIRPORT = "Leg 3 Departure Airport";
  public static final String LEG_3_ARRIVAL_AIRPORT = "Leg 3 Arrival Airport";
  public static final String LEG_3_DEPARTURE_DATE = "Leg 3 Departure Date";
  public static final String LEG_3_DEPARTURE_DAY = "Leg 3 Departure Day";
  public static final String RETURN_DATE = "Return Date";
  public static final String RETURN_DAY = "Return Day";
  public static final String ADULTS = "Adults";
  public static final String CHILDREN = "Children";
  public static final String INFANTS = "Infants";
  public static final String ONLY_DIRECT_FLIGHTS = "Only Direct Flight";
  public static final String CLASS = "Class";
  public static final String CLICKED_SEARCH_FLIGHTS = "Clicked Search Flights";
  public static final String CLEARED_SEARCH_HISTORY = "Cleared Search History";
  public static final String TRIP_DURATION = "Trip Duration";
  public static final String TRIP_DURATION_0 = "000";
  public static final String TRIP_DURATION_1 = "001-015";
  public static final String TRIP_DURATION_2 = "016-029";
  public static final String TRIP_DURATION_3 = "030-059";
  public static final String TRIP_DURATION_4 = "060+";
  public static final String TRIP_TYPE = "Trip type";
  public static final String TRIP_ONE_WAY = "One Way";
  public static final String TRIP_ROUND_TRIP = "Round Trip";
  public static final String TRIP_MULTI_STOP = "Multi Stops";
  public static final String ENTERED_EMAIL_ADDRESS = "Entered email address";
  public static final String ENTERED_PASSWORD = "Entered password";
  public static final String CLICKED_SIGN_IN = "Clicked Sign In";
  public static final String CLICKED_SIGN_IN_FACEBOOK = "Clicked Sign in with Facebook";
  public static final String CLICKED_SIGN_IN_GOOGLE = "Clicked Sign in with Google";
  public static final String CLICKED_FORGOT_PASSWORD = "Clicked Forgot Password";
  public static final String CLICKED_CONTINUE_PASSENGER = "Clicked Continue Passenger";
  public static final String CURRENT_USER_LOGIN = "Current user login";
  public static final String CLICKED_NEW_USER_SIGN_UP = "New user sign up clicked";
  public static final String CLICKED_CROSS_SELL_CARS = "Clicked Cross Sell Cars";
  public static final String CLICKED_CROSS_SELL_HOTELS = "Clicked Cross Sell Hotels";
  public static final String CLICKED_PURCHASE = "Clicked Purchase";
  public static final String CLICKED_CONTINUE_RESULTS = "Clicked Continue Results";
  public static final String FLIGHT_PRICE = "Flight Price";
  public static final String FLIGHT_PRICE_001 = "001-100";
  public static final String FLIGHT_PRICE_101 = "101-199";
  public static final String FLIGHT_PRICE_200 = "200-299";
  public static final String FLIGHT_PRICE_300 = "300-499";
  public static final String FLIGHT_PRICE_500 = "500-699";
  public static final String FLIGHT_PRICE_700 = "700-999";
  public static final String FLIGHT_PRICE_1000 = "1000+";
  public static final String INSURANCE_TYPE = "Insurance Type";
  public static final String NUMBER_BAGS = "Number of bags";
  public static final String PURCHASE_SUCCESSFULL = "Purchase Successful";
  public static final String PURCHASE_INSURANCE = "Purchase Insurance";
  public static final String VALUE_PER_PASSENGER = "Value per Passenger";
  public static final String TAP_ON_TRAVEL_GUIDE = "Tap On Travel Guide";
  public static final String CLICKED_ON_LOGIN = "Clicked on login";

  //Localytics notes
  public static final String NA = "N/A";
  public static final String STATUS_PENDING = "PENDING";
  public static final String STATUS_OK = "OK";
  public static final String STATUS_KO = "KO";
  public static final String TYPE_SIMPLE = "One Way";
  public static final String TYPE_ROUND = "Round Trip";
  public static final String TYPE_MULTIDESTINATION = "Multi Stop";
  public static final String YES = "Yes";
  public static final String NO = "No";
  public static final String LOGGED_IN = "Logged In";
  public static final String NOT_LOGGED_IN = "Not logged In";

  //Appsee events
  public static final String APPSEE_CATEGORY_KEY = "category";
  public static final String APPSEE_LABEL_KEY = "label";
  public static final String BACK_EVENT = "Back button";
  public static final String ORIGIN_TAP_EVENT = "Origin tap";
  public static final String DESTINATION_TAP_EVENT = "Destination tap";
  public static final String OUTBOUND_TAP_EVENT = "Outbound tap";
  public static final String INBOUND_TAP_EVENT = "Inbound tap";
  public static final String DIRECT_FLIGHTS_EVENT = "Direct flights";
  public static final String TAB_ROUND_TRIP_EVENT = "Tab Round trip";
  public static final String TAB_ONE_WAY_EVENT = "Tab One way";
  public static final String TAB_MULTI_STOPS_EVENT = "Tab Multi stops";
  public static final String CONTINUE_PASSENGERS_EVENT = "Continue in Passengers";
  public static final String MY_DATA_HOME_TAP_EVENT = "My data home tap";
  public static final String NEW_PAX_TRAVELLERS_TAP_EVENT = "Create new pax tap";
  public static final String LOGIN_TAP_EVENT = "Login tap";
  public static final String REGISTER_TAP_EVENT = "Register tap";
  public static final String FACEBOOK_LOGIN_TAP_EVENT = "Facebook login tap";
  public static final String GOOGLE_PLUS_LOGIN_TAP_EVENT = "Google+ login tap";
  public static final String LOGOUT_TAP_EVENT = "Log out tap";
  public static final String MY_TRIPS_HOME_EVENT = "My trips home tap";
  public static final String IMPORT_TRIP_EVENT = "Import trip tap";

  //Analytics screen names
  public static final String SCREEN_PAYMENT = "/BF/A_app/flights/payment/";
  public static final String SCREEN_PAYMENT_T_CS = "/BF/A_app/flights/payment/terms/";
  public static final String SCREEN_INSURANCES = "/BF/A_app/flights/insurances/";
  public static final String SCREEN_INSURANCES_T_CS = "/BF/A_app/flights/insurances/terms/";
  public static final String SCREEN_PASSENGERS = "/BF/A_app/flights/details-extras/";
  public static final String SCREEN_REGISTER = "/A_app/register/";
  public static final String SCREEN_REGISTER_EMAIL_ALREADY_USED =
      "/A_app/register/email_already_used/";
  public static final String SCREEN_REGISTER_SUCCESS = "/A_app/register/success/";
  public static final String SCREEN_LOGIN = "/A_app/login/";
  public static final String SCREEN_LOGIN_WRONG_TYPE = "/A_app/login/wrong_type/";
  public static final String SCREEN_LOGIN_ACCOUNT_BLOCKED = "/A_app/login/account_blocked/";
  public static final String SCREEN_LOGIN_ACCOUNT_BLOCKED_LEGACY =
      "/A_app/login/account_blocked_legacy/";
  public static final String SCREEN_LOGIN_PASSWORD_RECOVERY = "/A_app/login/password_recovery/";
  public static final String SCREEN_LOGIN_PASSWORD_RECOVERY_SENT =
      "/A_app/login/password_recovery/sent/";
  public static final String SCREEN_MYINFO_FREQUENT_TRAVELLERS_LIST =
      "/A_app/myinfo/frequent_travelers/";
  public static final String SCREEN_MYINFO_FREQUENT_TRAVELLERS_ACTION =
      "/A_app/myinfo/frequent_travelers/details/";
  public static final String SCREEN_MYINFO_ACCOUNT = "/A_app/myinfo/account/";
  public static final String SCREEN_MYINFO_ACCOUNT_PASSWORD = "/A_app/myinfo/account/password/";
  public static final String SCREEN_MYINFO = "/A_app/myinfo/";
  public static final String SCREEN_MYINFO_SSO = "/A_app/myinfo_sso/";
  public static final String SCREEN_ERROR_NO_CONNECTION = "/BF/A_app/flights/error/noconnection";
  public static final String SCREEN_ERROR_REPRICING = "/BF/A_app/flights/error/repricing";
  public static final String SCREEN_MY_TRIPS_HOME = "/A_app/mytrips/home/";
  public static final String SCREEN_MY_TRIPS_DETAILS = "/A_app/mytrips/details/";
  public static final String SCREEN_HOME = "/A_app/home/";
  public static final String SCREEN_CALENDAR = "/A_app/calendar/";

  //Analytics category
  public static final String CATEGORY_FLIGHTS_PAYMENT = "flights_pay_page";
  public static final String CATEGORY_FLIGHTS_INSURANCE = "flights_insurance";
  public static final String CATEGORY_FLIGHTS_PAX = "flights_pax_page";
  public static final String CATEGORY_FLIGHTS_RESULT = "flights_results";
  public static final String CATEGORY_MY_TRIPS = "my_trips";
  public static final String CATEGORY_MY_TRIPS_TRIP_INFO = "my_trips_trip_information";
  public static final String CATEGORY_SSO_REGISTER = "sso_register";
  public static final String CATEGORY_SSO_LOGIN = "sso_login";
  public static final String CATEGORY_SSO_LOGIN_FLIGHTS_PAX_PAGE = "sso_login_flights_pax_page";
  public static final String CATEGORY_SSO_BLOCKED = "sso_blocked";
  public static final String CATEGORY_SSO_BLOCKED_LEGACY = "sso_blocked_legacy";
  public static final String CATEGORY_SSO_PWD_RECOVERY = "sso_pwd_recovery";
  public static final String CATEGORY_SSO_PWD_RECOVERY_SENT = "sso_pwd_recovery_sent";
  public static final String CATEGORY_MY_AREA = "my_area";
  public static final String CATEGORY_MY_DATA_ACC = "my_data_acc";
  public static final String CATEGORY_HOME = "home";
  public static final String CATEGORY_MY_DATA_ACC_DETAILS = "my_data_acc_details";
  public static final String CATEGORY_MY_DATA_PAX = "my_data_pax";
  public static final String CATEGORY_APP_PERMISSIONS = "app_permissions";
  public static final String CATEGORY_RATE_APP_LEAVE_FEEDBACK = "rate_app_leave_feedback";
  public static final String CATEGORY_RATE_APP = "rate_app";
  public static final String CATEGORY_ABOUT_US = "about_us";
  public static final String CATEGORY_CONFIRMATION_PAGE_CONFIRMED =
      "flights_confirmation_page_confirmed";
  public static final String CATEGORY_CONFIRMATION_PAGE_PENDING =
      "flights_confirmation_page_pending";
  public static final String CATEGORY_CONFIGURATION_SCREEN = "configuration_screen";
  public static final String CATEGORY_FLIGHTS_CONFIRMATION_PAGE_REJECTED =
      "flights_confirmation_page_rejected";
  public static final String CATEGORY_ABOUT_US_FAQ = "about_us_faq";
  public static final String CATEGORY_ABOUT_US_CONTACT = "about_us_contact_us";
  public static final String CATEGORY_FLIGHTS_SUMMARY = "flights_summary";
  public static final String CATEGORY_FLIGHT_SEARCH = "flights_search";
  public static final String CATEGORY_PAYPAL_PAGE = "paypal_page";
  public static final String CATEGORY_TRUSTLY_PAGE = "trustly_page";
  public static final String CATEGORY_KLARNA_PAGE = "klarna_page";
  public static final String CATEGORY_LAUNCH_EVENTS = "launch_events";

  //Analytics action
  public static final String ACTION_REPRICING = "repricing";
  public static final String ACTION_CONFIRM_PURCHASE = "confirm_purchase";
  public static final String ACTION_PAYMENT_DETAILS = "payment_details";
  public static final String ACTION_PAYMENT_DETAILS_ERROR = "payment_details_error";
  public static final String ACTION_BIN_UPGRADE_DETECTION = "bin_upgrade_detection";
  public static final String ACTION_INSURANCE_INFORMATION = "insurance_information";
  public static final String ACTION_OPEN_HELP_CENTER = "help_center_widget";

  public static final String LABEL_PRODUCT_CARS_CLICKS = "product_cars_clicks";
  public static final String LABEL_PRODUCT_HOTELS_CLICKS = "product_hotels_clicks";
  public static final String LABEL_PRODUCT_FLIGHTS_CLICKS = "product_flights_clicks";
  public static final String ACTION_PRODUCT_SELECTOR = "product_selector";
  public static final String ACTION_NAVIGATION_ELEMENTS = "navigation_elements";
  public static final String ACTION_FULL_PRICE_SELECTOR = "full_price_selector";
  public static final String ACTION_CUSTOMER_DETAILS = "customer_details";
  public static final String ACTION_PAST_TRIPS = "past_trips";
  public static final String ACTION_EMPTY_TRIPS = "empty_trips";
  public static final String ACTION_UPCOMING_TRIPS = "upcoming_trips";
  public static final String ACTION_ADD_TRIP = "add_trip";
  public static final String ACTION_PAX_BAGGAGE = "pax_baggage";
  public static final String ACTION_TRIP_DETAILS = "trip_details";
  public static final String ACTION_PAX_DETAILS = "pax_details";
  public static final String ACTION_REGISTER_FORM = "register_form";
  public static final String ACTION_SIGN_IN = "sign_in";
  public static final String ACTION_SEARCHER_FLIGHTS = "searcher_flights";
  public static final String ACTION_FLIGHT_SUMMARY = "flight_summary";
  public static final String ACTION_PAX_DETAILS_ERROR = "pax_details_error";
  public static final String ACTION_CUSTOMER_DETAILS_ERROR = "customer_details_error";
  public static final String ACTION_SSO_BLOCKED = "sso_blocked";
  public static final String ACTION_PWD_RECOVERY = "pwd_recovery";
  public static final String ACTION_SSO_MODULE = "sso_module";
  public static final String ACTION_PWD_RECOVERY_EMAIL = "pwd_recovery_email";
  public static final String ACTION_SYNCHRONIZE_TRIPS = "synchronize_trips";
  public static final String ACTION_ACCOUNT_MENU = "account_menu";
  public static final String ACTION_NO_DATA = "no_data";
  public static final String ACTION_DATA_LOCAL = "data_local";
  public static final String ACTION_DATA_LOGGED = "data_logged";
  public static final String ACTION_DATA_PENDING_VALIDATION = "data_pending_validation";
  public static final String ACTION_ACCOUNT_SUMMARY = "account_summary";
  public static final String ACTION_PAX_SUMMARY = "pax_summary";
  public static final String ACTION_PERMISSIONS_ALERT = "permissions_alert";
  public static final String ACTION_MTT = "mtt_area";
  public static final String ACTION_MTT_NOTIFICATIONS = "mtt_notifications";
  public static final String ACTION_FULL_PRICE_INFORMATION = "full_price_information";
  public static final String ACTION_RATE_APP = "rate_app";
  public static final String ACTION_RATE_OK = "rate_ok";
  public static final String ACTION_RATE_CAN_IMPROVE = "rate_can_improve";
  public static final String ACTION_ABOUT_US = "about_us";
  public static final String ACTION_ON_LOAD_EVENTS = "on_load_events";
  public static final String ACTION_AUTO_CHECK_IN = "auto_check_in";
  public static final String ACTION_PAYMENT_RETRY = "payment_retry";
  public static final String ACTION_BOOKING_REJECTED = "booking_rejected";
  public static final String ACTION_ABOUT_US_CONTACT = "about_us_contact_us";
  public static final String ACTION_ERROR_SESSION_EXPIRED = "session_expiring";
  public static final String ACTION_COUPON_SECTION = "coupon_section";
  public static final String ACTION_SSO_PAX_PAGE = "sso_pax_page";
  public static final String ACTION_RECENT_SEARCH = "recent_search";
  public static final String ACTION_XSELL_WIDGET = "xsell_widget";
  public static final String ACTION_LAUNCH_EVENTS = "launch_events";
  public static final String ACTION_NAGS = "nags";

  //Analytics label
  public static final String LABEL_ADD_TO_CALENDAR_CLICKS = "add_to_calendar_clicks";
  public static final String LABEL_REPRICING_CONTINUE = "system_alert_buy_tickets";
  public static final String LABEL_REPRICING_GO_BACK = "system_alert_go_back";
  public static final String LABEL_PURCHASE_CLICKS = "purchase_clicks";
  public static final String LABEL_GENERAL_CONDITIONS_CLICKS = "general_conditions_clicks";
  public static final String LABEL_BIN_CARD_TYPE_OK = "bin_card_type_OK";
  public static final String LABEL_BIN_CARD_TYPE_NOT_DETECTED = "bin_card_type_not_detected";
  public static final String LABEL_INSURANCE_SELECT = "insurance_select_X";
  public static final String LABEL_INSURANCE_CONDITIONS = "insurance_conditions_X";
  public static final String LABEL_APP_MY_TRIPS_CLICKS = "app_my_trips_clicks";
  public static final String LABEL_GO_BACK = "go_back";
  public static final String LABEL_CLICK_CONTINUE = "continue_clicks";
  public static final String LABEL_CLICK_CONTINUE_FAIL = "continue_clicks_FE_error";
  public static final String LABEL_ADD_BOOKING = "add_booking";
  public static final String LABEL_MYTRIPS_SELECT_TRIP = "select_trip";
  public static final String LABEL_MYTRIPS_UPCOMING_TRIP_YES = "upcoming_trip_YES";
  public static final String LABEL_MYTRIPS_UPCOMING_TRIP_NO = "upcoming_trip_NO";

  public static final String LABEL_ADD_BOOKING_TO_MY_TRIPS_SUCCESS =
      "add_booking_to_my_trips_success";
  public static final String LABEL_ADD_BOOKING_TO_MY_TRIPS_FAILURE =
      "add_booking_to_my_trips_failure";
  public static final String LABEL_ADD_BOOKING_TO_MY_TRIPS_TAPS = "add_booking_to_my_trips_taps";
  public static final String LABEL_PARTIAL_NUMBER_BAGGAGES = "number_baggages_";
  public static final String LABEL_OPEN_FLIGHT_SUMMARY = "open_flight_summary";
  public static final String LABEL_OPEN_BOOKING_INFO = "booking_info_opened";
  public static final String LABEL_OPEN_PASSENGER_INFO = "passenger_info_opened";
  public static final String LABEL_OPEN_INSURANCE_INFO = "insurance_info_opened";
  public static final String LABEL_OPEN_BILLING_INFO = "billing_info_opened";
  public static final String LABEL_DELETE_PAX_CLICKS = "delete_pax_clicks";
  public static final String LABEL_OPEN_DESTINATION_GUIDE = "open_destination_guide";
  public static final String LABEL_CREATE_ACCOUNT_FACEBOOK_DATALOCAL =
      "create_account_facebook_data-local";
  public static final String LABEL_CREATE_ACCOUNT_FACEBOOK_DATANO =
      "create_account_facebook_data-no";
  public static final String LABEL_CREATE_ACCOUNT_GOOGLE_DATALOCAL =
      "create_account_google_data-local";
  public static final String LABEL_CREATE_ACCOUNT_GOOGLE_DATANO = "create_account_google_data-no";
  public static final String LABEL_CREATE_ACCOUNT_ODIGEO_DATALOCAL =
      "create_account_odigeo_data-local";
  public static final String LABEL_CREATE_ACCOUNT_ODIGEO_DATANO = "create_account_odigeo_data-no";
  public static final String LABEL_SSO_EMAIL_INCORRECT_FORMAT_ERROR =
      "sso_email_incorrect_format_error";
  public static final String LABEL_SSO_PWD_PCI_COMPLIANCE_ERROR = "sso_pwd_pci_compliance_error";
  public static final String LABEL_PASSWORD_UNMASK = "password_unmask";
  public static final String LABEL_SSO_EMAIL_ALREADY_USED_ODIGEO_ERROR =
      "sso_email_already_used_odigeo_error";
  public static final String LABEL_SSO_EMAIL_ALREADY_USED_FACEBOOK_ERROR =
      "sso_email_already_used_facebook_error";
  public static final String LABEL_SSO_EMAIL_ALREADY_USED_GOOGLE_ERROR =
      "sso_email_already_used_google_error";
  public static final String LABEL_SSO_EMAIL_AUTHENTICATION_FACEBOOK_ERROR =
      "sso_email_authentication_facebook_error";
  public static final String LABEL_SSO_EMAIL_AUTHENTICATION_GMAIL_ERROR =
      "sso_email_authentication_gmail_error";
  public static final String LABEL_LINKS_OPEN_CONDITIONS = "links_open_conditions";
  public static final String LABEL_SSO_EMAIL_ALREADY_USED_FACEBOOK_LOGIN =
      "sso_email_already_used_facebook_login";
  public static final String LABEL_SSO_EMAIL_ALREADY_USED_GOOGLE_LOGIN =
      "sso_email_already_used_google_login";
  public static final String LABEL_SIGN_IN_FACEBOOK_DATALOCAL = "sign_in_facebook_data-local";
  public static final String LABEL_SIGN_IN_FACEBOOK_DATANO = "sign_in_facebook_data-no";
  public static final String LABEL_SIGN_IN_GOOGLE_DATALOCAL = "sign_in_google_data-local";
  public static final String LABEL_SIGN_IN_GOOGLE_DATANO = "sign_in_google_data-no";
  public static final String LABEL_SIGN_IN_ODIGEO_DATALOCAL = "sign_in_odigeo_data-local";
  public static final String LABEL_SIGN_IN_ODIGEO_DATANO = "sign_in_odigeo_data-no";
  public static final String LABEL_FORGOT_PASSWORD_CLICKS = "forgot_password_password_clicks";
  public static final String LABEL_SSO_WRONG_SIGNIN_CREDENTIALS_ERROR =
      "sso_wrong_signin_credentials_error";
  public static final String LABEL_PARTIAL_PAX_PAYS = "pax_paying_";
  public static final String LABEL_PAX_SELECTED = "pax_selected_";
  public static final String LABEL_SSO_SUCCESSFUL_LOGIN_FACEBOOK = "sso_successful_login_facebook";
  public static final String LABEL_SSO_SUCCESSFUL_LOGIN_GOOGLE = "sso_successful_login_google";
  public static final String LABEL_SSO_SUCCESSFUL_LOGIN_ODIGEO = "sso_successful_login_odigeo";
  public static final String LABEL_SSO_SUCCESSFUL_REGISTER_ODIGEO =
      "sso_successful_register_odigeo";
  public static final String LABEL_SSO_REGISTER_NEWSLETTER = "register_subscribe_1";
  public static final String LABEL_SSO_UNREGISTER_NEWSLETTER = "register_subscribe_0";
  public static final String LABEL_GO_TO_MAIL = "go_to_mail";
  public static final String LABEL_GO_TO_LOGIN = "go_to_login";
  public static final String LABEL_SEND_INSTRUCTIONS = "send_instructions";
  public static final String LABEL_SSO_NOT_ODIGEO_ACCOUNT_ERROR = "sso_not_odigeo_account_error";
  public static final String LABEL_MTT_NOTIFICATIONS_ON = "mtt_notifications_on";
  public static final String LABEL_MTT_NOTIFICATIONS_OFF = "mtt_notifications_off";
  public static final String LABEL_MTT_CARD_BLOCKED = "card_blocked";
  public static final String LABEL_CC_SELECTOR_SHOWN = "cc_selector_shown";
  public static final String LABEL_PAYMENT_METHOD_SELECTED = "payment_method_selected_";
  public static final String LABEL_SSO_REGISTER = "sso_request_error";
  public static final String LABEL_UNKNOWN_SAME_SEARCH = "unknown_error_same_search";
  public static final String LABEL_UNKNOWN_NEW_SEARCH = "unknown_error_new_search";
  public static final String LABEL_PARTIAL_RECENT_SEARCH = "recent_search_select_";
  public static final String LABEL_RECENT_SEARCH_CLEAN = "recent_search_clean";
  public static final String LABEL_RECENT_SEARCH_VIEW_MORE = "recent_search_view_more";
  public static final String LABEL_SESSION_WITH_ACTIVE_BOOKINGS = "session_with_active_bookings";
  public static final String LABEL_CALENDAR_SELECT_ONE_WAY = "calendar_select_one_way";
  public static final String LABEL_CALENDAR_CONTINUE = "calendar_continue";
  public static final String LABEL_CALENDAR_BACK = "calendar_back";
  public static final String LABEL_GT_APPEARANCES = "ground_transportation_appearances";
  public static final String LABEL_GT_ON_CLICK = "ground_transportation_clicks";
  public static final String LABEL_LOGIN_USER_WITH_SAVED_METHODS = "saved_payment_displayed_1";
  public static final String LABEL_LOGIN_USER_WITHOUT_SAVED_METHODS = "saved_payment_displayed_0";
  public static final String LABEL_USER_USED_SAVED_PAYMENT_METHOD_STORE =
      "payment_method_selected_CC_saved_used";
  public static final String LABEL_USER_NOT_USED_SAVED_PAYMENT_METHOD_STORE =
      "payment_method_selected_CC_saved_used_not";
  public static final String LABEL_USER_STORED_PAYMENT_METHOD =
      "payment_method_selected_standard_stored";
  public static final String LABEL_USER_NOT_STORED_PAYMENT_METHOD =
      "payment_method_selected_standard_stored_not";
  public static final String LABEL_NAG_BAGGAGE_REMOVAL_APPEARANCES =
      "nag_baggage_removal_appearances";
  public static final String LABEL_NAG_BAGGAGE_REMOVAL_CANCEL = "nag_baggage_removal_cancel";
  public static final String LABEL_NAG_BAGGAGE_REMOVAL_REMOVE = "nag_baggage_removal_remove";

  //Payment Widget Validation Errors
  public static final String LABEL_CARD_NUMBER_INVALID = "card_number_invalid_error";
  public static final String LABEL_CARD_NUMBER_MISSING = "card_number_missing_error";
  public static final String LABEL_CARD_HOLDER_INVALID = "card_holder_name_invalid_error";
  public static final String LABEL_CARD_HOLDER_MISSING = "card_holder_name_missing_error";
  public static final String LABEL_EXPIRATION_DATE_INVALID = "expiration_date_invalid_error";
  public static final String LABEL_EXPIRATION_DATE_MISSING = "expiration_date_missing_error";
  public static final String LABEL_CVV_INVALID = "CVV_invalid_error";
  public static final String LABEL_CVV_MISSING = "CVV_missing_error";

  //Payment Promo Code Widget
  public static final String LABEL_COUPON_OPEN_CLICKS = "coupon_open_clicks";
  public static final String LABEL_COUPON_VALIDATE_CLICKS = "coupon_validate_clicks";
  public static final String LABEL_COUPON_OK = "coupon_ok";
  public static final String LABEL_COUPON_NOT_OK = "coupon_not_ok_error";

  public static final String LABEL_NAME_INVALID_ERROR = "name_invalid_error";
  public static final String LABEL_SURNAME_INVALID_ERROR = "surname_invalid_error";
  public static final String LABEL_TITLE_MISSING_ERROR = "title_missing_error";
  public static final String LABEL_EMAIL_INVALID_ERROR = "email_invalid_error";
  public static final String LABEL_CITY_MISSING_ERROR = "city_missing_error";
  public static final String LABEL_ADDRESS_MISSING_ERROR = "address_missing_error";
  public static final String LABEL_POSTCODE_MISSING_ERROR = "postcode_missing_error";
  public static final String LABEL_PHONE_NUMBER_INVALID_ERROR = "phone_number_invalid_error";
  public static final String LABEL_GO_TO_REGISTER = "go_to_register";
  public static final String LABEL_SYNCHRONIZE_TRIPS_EMPTY_ERROR = "synchronize_trips_empty_error";
  public static final String LABEL_SYNCHRONIZE_TRIPS_PULL_TRY = "synchronize_trips_pull_try";
  public static final String LABEL_SYNCHRONIZE_TRIPS_ERROR = "synchronize_trips_error";
  public static final String LABEL_JOIN_NOW_CLICKS = "join_now_clicks";
  public static final String LABEL_USUAL_TRAVELLERS_OPEN = "usual_travellers_open";
  public static final String LABEL_MY_TRIPS_OPEN = "my_trips_open";
  public static final String LABEL_APP_CONFIGURATION_CLICKS = "app_configuration_clicks";
  public static final String LABEL_INFORMATION_OPEN = "information_open";
  public static final String LABEL_ACCOUNT_PREFERENCES_OPEN = "account_preferences_open";
  public static final String LABEL_LOG_OUT_CLICKS = "log_out_clicks";
  public static final String LABEL_ACCOUNT_MENU_OPEN = "account_menu_open";
  public static final String LABEL_MENU_OPEN = "menu_open";
  public static final String LABEL_OPEN_PASSWORD = "open_password";
  public static final String LABEL_DELETE_ACCOUNT_CLICKS = "delete_account_clicks";
  public static final String LABEL_CHANGE_PASSWORD_CLICKS = "change_password_clicks";
  public static final String LABEL_CHANGE_PASSWORD_SUCCESFUL = "change_password_successful";
  public static final String LABEL_CHANGE_PASSWORD_FAILURE = "change_password_failure";
  public static final String LABEL_ADD_PASSENGER_CLICKS = "add_passenger_clicks";
  public static final String LABEL_OPEN_PAX_CLICKS = "open_pax_details";
  public static final String LABEL_CALENDAR_ACCESS_DENY = "calendar_access_deny";
  public static final String LABEL_CALENDAR_ACCESS_ALLOW = "calendar_access_allow";
  public static final String LABEL_LOCATION_ACCESS_ALLOW = "location_access_allow";
  public static final String LABEL_LOCATION_ACCESS_DENY = "location_access_deny";
  public static final String LABEL_PHONE_ACCESS_ALLOW = "phone_access_allow";
  public static final String LABEL_PHONE_ACCESS_DENY = "phone_access_deny";
  public static final String LABEL_ACCOUNT_ACCESS_ALLOW = "account_access_allow";
  public static final String LABEL_ACCOUNT_ACCESS_DENY = "account_access_deny";
  public static final String LABEL_STORAGE_ACCESS_ALLOW = "storage_access_allow";
  public static final String LABEL_STORAGE_ACCESS_DENY = "storage_access_deny";

  public static final String CARD_TYPE_IDENTIFIER = "{X}";
  public static final String CARD_POSITION_IDENTIFIER = "{Y}";
  public static final String LABEL_CARD_SHOWN_FORMAT =
      "card_shown_" + CARD_TYPE_IDENTIFIER + "_position_" + CARD_POSITION_IDENTIFIER;
  public static final String LABEL_CARD_SEE_INFO_FORMAT =
      "card_" + CARD_TYPE_IDENTIFIER + "_position_" + CARD_POSITION_IDENTIFIER + "_see_info";
  public static final String LABEL_CARD_SEARCH_CLICKED =
      "card_shwon_search_position_" + CARD_POSITION_IDENTIFIER + "_search_" + CARD_TYPE_IDENTIFIER;
  public static final String LABEL_CARD_DROPOFF_LAUNCH = "launch";
  public static final String LABEL_CARD_DROPOFF_EDIT = "edit";

  public static final String LABEL_CARD_NEXT_FLIGHT_OK = "next_flight_ok";
  public static final String LABEL_CARD_DELAYED = "next_flight_delayed";
  public static final String LABEL_CARD_CANCELLED = "next_flight_cancelled";
  public static final String LABEL_CARD_DIVERTED = "next_flight_diverted";
  public static final String LABEL_CARD_BOARDING_GATE = "boarding_gate";
  public static final String LABEL_CARD_BAGGAGE_BELT = "baggage_belt";
  public static final String LABEL_CARD_PROMO = "promo";
  public static final String LABEL_CARD_HOTEL = "hotel";
  public static final String LABEL_CARD_SSO = "sso";
  public static final String LABEL_CARD_CARS = "cars";
  public static final String LABEL_REFRESH_INFORMATION = "refresh_information";
  public static final String LABEL_CARD_SEARCH = "search";

  public static final String LABEL_ALERT_VIEW_SHOWN = "alertview_shown";
  public static final String LABEL_ALERT_VIEW_CLOSED = "alertview_closed";
  public static final String LABEL_BOTTOM_BAR_SHOWN = "bottombar_shown";

  public static final String LABEL_RATE_CAN_IMPROVE = "rate_can_improve";
  public static final String LABEL_RATE_OK = "rate_ok";
  public static final String LABEL_SEND_FEEDBACK = "send_feedback";

  public static final String LABEL_HELP_CENTER_CLICKS = "help_center_clicks";
  public static final String LABEL_CONFIRMATION_SHARE_TRIP = "share";

  public static final String LABEL_PAYMENT_RETRY_APPEARANCES = "payment_retry_appearances";
  public static final String LABEL_CHECK_PAYMENT_DETAILS = "check_payment_details";
  public static final String LABEL_CALL_NOW_CLICKS = "call_now_clicks";
  public static final String LABEL_ABOUT_SEND_EMAIL = "send_email_clicks";
  public static final String LABEL_ABOUT_TERMS_AND_CONDITIONS_OPEN =
      "terms_and_conditions_company_open";
  public static final String LABEL_ABOUT_TERMS_AND_CONDITIONS_AIRLINES_OPEN =
      "terms_and_conditions_airlines_opens";
  public static final String LABEL_ABOUT_PRIVACY_POLICY_OPEN = "privacy_policy_open";

  public static final String LABEL_SESSION_EXPIRED = "session_expiring_expired";
  public static final String LABEL_SESSION_EXPIRED_CALL = "call_now_clicks";
  public static final String LABEL_SESSION_EXPIRING_SAME_SEARCH = "session_expiring_relaunch";
  public static final String LABEL_SESSION_EXPIRING_NEW_SEARCH = "session_expiring_go_search";

  public static final String LABEL_AUTO_CHECKIN_SHOWN = "auto_checkin_shown";
  public static final String LABEL_AUTO_CHECKIN_REQUEST_BOARDING_PASS_CLICKS =
      "request_boarding_pass_clicks";
  public static final String LABEL_AUTO_CHECKIN_HOW_IT_WORKS_OPEN = "how_it_works_open";
  public static final String LABEL_SAME_BAGGAGE_CHECKED = "same_baggage_checked";
  public static final String LABEL_SAME_BAGGAGE_UNCHECKED = "same_baggage_unchecked";
  public static final String LABEL_SAME_BAGGAGE_AUTO_UNCHECKED = "same_baggage_auto_unchecked";
  public static final String LABEL_ADD_BAGGAGE = "add_baggage";
  public static final String LABEL_REMOVE_BAGGAGE = "remove_baggage";

  //Analytics screen
  public static final String SCREEN_ABOUT_APP = "/A_app/about/home/help/about-app/";
  public static final String SCREEN_FAQ = "/A_app/about/home/help/faq/";
  public static final String SCREEN_SECURE_SHOPPING = "/A_app/about/home/help/secure-shopping/";
  public static final String SCREEN_ABOUT_BRAND = "/A_app/about/home/help/about-brand/";
  public static final String SCREEN_ABOUT_TERMS_AND_CONDITIONS = "/A_app/about/home/terms/";
  public static final String SCREEN_CONTACT_US = "/A_app/about_contact/";
  public static final String SCREEN_ERROR_SESSION_EXPIRED =
      "/BF/A_app/flights/error/sessionexpired";
  public static final String SCREEN_ERROR_UNEXPECTED = "/BF/A_app/flights/error/unexpected";

  //values
  public static final String USER_LOGGED_DIMENSION_VALUE = "logged_1";
  public static final String USER_NOT_LOGGED_DIMENSION_VALUE = "logged_0";

  public static final String HOTELS_HOME_PAGE = "hotels_home_page";
  public static final String CARS_HOME_PAGE = "cars_home_page";

  //Facebook custom events parameters
  public static final String EVENT_PARAM_NUM_ADULTS = "number_of_adults";
  public static final String EVENT_PARAM_NUM_KIDS = "number_of_kids";
  public static final String EVENT_PARAM_NUM_INFANTS = "number_of_infants";
  public static final String EVENT_PARAM_ORIGIN_1 = "departure_city_1";
  public static final String EVENT_PARAM_DESTINATION_1 = "arrival_city_1";
  public static final String EVENT_PARAM_DATE_1 = "departure_date_dd-mm-yyyy_1";
  public static final String EVENT_PARAM_ORIGIN_2 = "departure_city_2";
  public static final String EVENT_PARAM_DESTINATION_2 = "arrival_city_2";
  public static final String EVENT_PARAM_DATE_2 = "departure_date_dd-mm-yyyy_2";
  public static final String EVENT_PARAM_ORIGIN_3 = "departure_city_3";
  public static final String EVENT_PARAM_DESTINATION_3 = "arrival_city_3";
  public static final String EVENT_PARAM_DATE_3 = "departure_date_dd-mm-yyyy_3";
  public static final String EVENT_PARAM_AIRLINE_1 = "airline_1";
  public static final String EVENT_PARAM_AIRLINE_2 = "airline_2";
  public static final String EVENT_PARAM_AIRLINE_3 = "airline_3";
  public static final String EVENT_PARAM_ARRIVAL_COUNTRY_1 = "arrival_country_1";
  public static final String EVENT_PARAM_ARRIVAL_COUNTRY_2 = "arrival_country_2";
  public static final String EVENT_PARAM_ARRIVAL_COUNTRY_3 = "arrival_country_3";
  public static final String EVENT_PARAM_DEPARTURE_COUNTRY_1 = "departure_country_1";
  public static final String EVENT_PARAM_DEPARTURE_COUNTRY_2 = "departure_country_2";
  public static final String EVENT_PARAM_DEPARTURE_COUNTRY_3 = "departure_country_3";
}
