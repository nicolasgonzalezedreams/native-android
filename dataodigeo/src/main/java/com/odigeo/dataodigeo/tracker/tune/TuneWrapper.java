package com.odigeo.dataodigeo.tracker.tune;

import java.util.List;

public class TuneWrapper {
  private List<TuneSegment> segments;
  private int passengers;
  private double unitPrice;
  private double revenueMarketing;
  private String currencyCode;
  private String bookingId;

  TuneWrapper(List<TuneSegment> segments, int passengers, double unitPrice, double revenueMarketing,
      String currencyCode, String bookingId) {
    this.segments = segments;
    this.passengers = passengers;
    this.unitPrice = unitPrice;
    this.revenueMarketing = revenueMarketing;
    this.currencyCode = currencyCode;
    this.bookingId = bookingId;
  }

  public List<TuneSegment> getSegments() {
    return segments;
  }

  public int getPassengers() {
    return passengers;
  }

  public double getUnitPrice() {
    return unitPrice;
  }

  public double getRevenueMarketing() {
    return revenueMarketing;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getBookingId() {
    return bookingId;
  }
}
