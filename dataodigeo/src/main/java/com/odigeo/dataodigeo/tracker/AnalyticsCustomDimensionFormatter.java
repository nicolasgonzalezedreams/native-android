package com.odigeo.dataodigeo.tracker;

import android.content.Context;
import com.odigeo.data.tracker.CustomDimensionFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import odigeo.dataodigeo.R;

public class AnalyticsCustomDimensionFormatter implements CustomDimensionFormatter {

  private static int MAX_DIMEN_SIZE = 15;
  private final Context context;

  public AnalyticsCustomDimensionFormatter(Context context) {
    this.context = context;
  }

  public List<String> formatCustomDimension(TreeMap<String, Integer> dimensions) {

    String dimensFormat = "";
    int counterDimenSize = 0;
    List<String> dimensToSend = new ArrayList<>();

    Set<String> keySet = dimensions.keySet();
    for (String key : keySet) {
      counterDimenSize++;
      dimensFormat = dimensFormat.concat(
          context.getString(R.string.format_google_analytics_dimension, key, dimensions.get(key)));
      if (counterDimenSize % MAX_DIMEN_SIZE == 0 || counterDimenSize == dimensions.size()) {
        dimensFormat = dimensFormat.substring(0, dimensFormat.length() - 1);
        dimensToSend.add(dimensFormat);
        counterDimenSize = 0;
        dimensFormat = "";
      }
    }

    return dimensToSend;
  }
}
