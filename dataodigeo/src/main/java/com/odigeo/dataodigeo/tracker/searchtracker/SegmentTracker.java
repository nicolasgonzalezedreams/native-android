package com.odigeo.dataodigeo.tracker.searchtracker;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * This class models the information about the flight segments to be sent to Facebook's SDK.
 *
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 29/07/16
 */

class SegmentTracker implements Serializable {
  private static final String DEPARTURE = "departure";
  private static final String ARRIVAL = "arrival";
  private static final String DATE = "date";
  private static final String DEPARTURE_COUNTRY = "departure_country";
  private static final String ARRIVAL_COUNTRY = "arrival_country";

  @SerializedName(DEPARTURE) String mDeparture;
  @SerializedName(ARRIVAL) String mArrival;
  @SerializedName(DATE) String mDate;
  @SerializedName(DEPARTURE_COUNTRY) String mDepartureCountry;
  @SerializedName(ARRIVAL_COUNTRY) String mArrivalCountry;

  SegmentTracker(String departure, String arrival, String date, String departureCountry,
      String arrivalCountry) {
    mDeparture = departure;
    mArrival = arrival;
    mDate = date;
    mDepartureCountry = departureCountry;
    mArrivalCountry = arrivalCountry;
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    SegmentTracker that = (SegmentTracker) o;

    if (!mDeparture.equals(that.mDeparture)) return false;
    if (!mArrival.equals(that.mArrival)) return false;
    return mDate.equals(that.mDate);
  }

  @Override public int hashCode() {
    int result = mDeparture.hashCode();
    result = 31 * result + mArrival.hashCode();
    result = 31 * result + mDate.hashCode();
    return result;
  }
}
