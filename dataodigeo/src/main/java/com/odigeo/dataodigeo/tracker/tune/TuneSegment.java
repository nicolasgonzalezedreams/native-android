package com.odigeo.dataodigeo.tracker.tune;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 17/08/16
 */

public class TuneSegment {
  private String mDeparture;
  private String mArrival;
  private long mDate;
  private long mReturnDate;

  public TuneSegment() {
  }

  public TuneSegment(String departure, String arrival, long date, long returnDate) {
    mDeparture = departure;
    mArrival = arrival;
    mDate = date;
    mReturnDate = returnDate;
  }

  public String getDeparture() {
    return mDeparture;
  }

  public void setDeparture(String departure) {
    mDeparture = departure;
  }

  public String getArrival() {
    return mArrival;
  }

  public void setArrival(String arrival) {
    mArrival = arrival;
  }

  public long getDate() {
    return mDate;
  }

  public void setDate(long date) {
    mDate = date;
  }

  public long getReturnDate() {
    return mReturnDate;
  }

  public void setReturnDate(long returnDate) {
    mReturnDate = returnDate;
  }
}
