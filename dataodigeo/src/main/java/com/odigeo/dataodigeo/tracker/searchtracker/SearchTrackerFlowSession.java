package com.odigeo.dataodigeo.tracker.searchtracker;

public class SearchTrackerFlowSession {

  private static SearchTrackerFlowSession mSearchTrackerFlowSession;
  private SearchTrackHelper mSearchTrackHelper;

  public static SearchTrackerFlowSession getInstance() {
    if (mSearchTrackerFlowSession == null) {
      mSearchTrackerFlowSession = new SearchTrackerFlowSession();
    }
    return mSearchTrackerFlowSession;
  }

  public SearchTrackHelper getSearchTrackHelper() {
    return mSearchTrackHelper;
  }

  public void setSearchTrackHelper(SearchTrackHelper searchTrackHelper) {
    mSearchTrackHelper = searchTrackHelper;
  }
}
