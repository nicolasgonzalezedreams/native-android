package com.odigeo.dataodigeo.repositories.xsell;

import com.odigeo.data.db.dao.GuideDBDAOInterface;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.xsell.CrossSelling;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.data.repositories.CrossSellingRepository;
import com.odigeo.presenter.model.CrossSellingType;
import java.util.ArrayList;
import java.util.List;

public class CrossSellingDataRepository implements CrossSellingRepository {

  public static final String TRIP_DETAILS_GROUND_TRANSPORTATION_ACTIVE =
      "mytrips_detail_ground_transports_active";

  public static final String MY_TRIPS_DETAILS_XSELLCARD_HOTEL_TITLE = "mytrips_detail_xsellcard_hotel_title";
  public static final String MY_TRIPS_DETAILS_XSELLCARD_HOTEL_LABEL = "mytrips_detail_xsellcard_hotel_label";
  public static final String MY_TRIPS_DETAILS_XSELLCARD_HOTEL_SUBTITLE = "mytrips_detail_xsellcard_hotel_subtitle";
  public static final String MY_TRIPS_DETAILS_XSELLCARD_CARS_TITLE = "mytrips_detail_xsellcard_car_title";
  public static final String MY_TRIPS_DETAILS_XSELLCARD_CARS_LABEL= "mytrips_detail_xsellcard_car_label";
  public static final String MY_TRIPS_DETAILS_XSELLCARD_CARS_SUBTITLE = "mytrips_detail_xsellcard_car_subtitle";
  public static final String MY_TRIPS_DETAILS_XSELLCARD_GT_TITLE = "mytrips_detail_xsellcard_ground_transports_title";
  public static final String MY_TRIPS_DETAILS_XSELLCARD_GT_LABEL = "mytrips_detail_xsellcard_ground_transports_label";
  public static final String MY_TRIPS_DETAILS_XSELLCARD_GT_SUBTITLE = "mytrips_detail_xsellcard_ground_transports_subtitle";
  public static final String MY_TRIPS_DETAILS_XSELLCARD_GUIDE_TITLE = "mytrips_detail_xsellcard_travelguide_title";
  public static final String MY_TRIPS_DETAILS_XSELLCARD_GUIDE_LABEL = "mytrips_detail_xsellcard_travelguide_label";
  public static final String MY_TRIPS_DETAILS_XSELLCARD_GUIDE_SUBTITLE = "mytrips_detail_xsellcard_travelguide_subtitle";

  private final LocalizableProvider localizableProvider;
  private final GuideDBDAOInterface guideDAO;

  public CrossSellingDataRepository(LocalizableProvider localizableProvider,
      GuideDBDAOInterface guideDao) {
    this.localizableProvider = localizableProvider;
    this.guideDAO = guideDao;
  }

  @Override public List<CrossSelling> getCrossSellingForBooking(Booking booking) {
    List<CrossSelling> crossSellings = new ArrayList<>();

    final String hotelTitle = localizableProvider.getString(MY_TRIPS_DETAILS_XSELLCARD_HOTEL_TITLE);
    final String hotelLabel = localizableProvider.getString(MY_TRIPS_DETAILS_XSELLCARD_HOTEL_LABEL);
    final String hotelSubtitle =
        localizableProvider.getString(MY_TRIPS_DETAILS_XSELLCARD_HOTEL_SUBTITLE);
    final String carsTitle = localizableProvider.getString(MY_TRIPS_DETAILS_XSELLCARD_CARS_TITLE);
    final String carsLabel = localizableProvider.getString(MY_TRIPS_DETAILS_XSELLCARD_CARS_LABEL);
    final String carsSubtitle =
        localizableProvider.getString(MY_TRIPS_DETAILS_XSELLCARD_CARS_SUBTITLE);
    final String groundTitle =
        localizableProvider.getString(MY_TRIPS_DETAILS_XSELLCARD_GT_TITLE);
    final String groundLabel =
        localizableProvider.getString(MY_TRIPS_DETAILS_XSELLCARD_GT_LABEL);
    final String groundSubtitle =
        localizableProvider.getString(MY_TRIPS_DETAILS_XSELLCARD_GT_SUBTITLE);
    final String guideTitle =
        localizableProvider.getString(MY_TRIPS_DETAILS_XSELLCARD_GUIDE_TITLE);
    final String guideLabel =
        localizableProvider.getString(MY_TRIPS_DETAILS_XSELLCARD_GUIDE_LABEL);
    final String guideSubtitle =
        localizableProvider.getString(MY_TRIPS_DETAILS_XSELLCARD_GUIDE_SUBTITLE);

    CrossSelling hotelCrossSelling =
        new CrossSelling(booking, false, hotelLabel, hotelTitle, CrossSellingType.TYPE_HOTEL,
            hotelSubtitle);
    CrossSelling carsCrossSelling =
        new CrossSelling(booking, false, carsLabel, carsTitle, CrossSellingType.TYPE_CARS,
            carsSubtitle);
    CrossSelling groundCrossSelling =
        new CrossSelling(booking, false, groundLabel, groundTitle, CrossSellingType.TYPE_GROUND,
            groundSubtitle);
    CrossSelling guideCrossSelling =
        new CrossSelling(booking, true, guideLabel, guideTitle, CrossSellingType.TYPE_GUIDE,
            guideSubtitle);

    crossSellings.add(hotelCrossSelling);
    crossSellings.add(carsCrossSelling);
    crossSellings.add(groundCrossSelling);
    crossSellings.add(guideCrossSelling);

    return crossSellings;
  }

  @Override public boolean getCrossSellingArrivalGuide(Booking booking) {
    return guideDAO.getGuideByGeoNodeId(booking.getArrivalGeoNodeId()) != null;
  }

  @Override public boolean shouldShowGroundTransportation() {
    String active = localizableProvider.getString(TRIP_DETAILS_GROUND_TRANSPORTATION_ACTIVE);
    return Boolean.parseBoolean(active);
  }
}
