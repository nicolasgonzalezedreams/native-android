package com.odigeo.dataodigeo.repositories.xsell;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.xsell.CrossSelling;
import java.util.List;

public interface CrossSellingDataStore {
  List<CrossSelling> getCrossSellingForBooking(Booking booking);
}
