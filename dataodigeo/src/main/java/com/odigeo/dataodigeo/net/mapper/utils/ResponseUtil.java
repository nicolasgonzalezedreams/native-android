package com.odigeo.dataodigeo.net.mapper.utils;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;
import org.json.JSONException;
import org.json.JSONObject;

public class ResponseUtil {

  public static String decompressResponseFromGzip(byte[] data) {
    String output = "";
    if (data != null && data.length != 0) {
      GZIPInputStream gStream;
      try {
        gStream = new GZIPInputStream(new ByteArrayInputStream(data));
        InputStreamReader reader = new InputStreamReader(gStream);
        BufferedReader in = new BufferedReader(reader);
        String read;
        while ((read = in.readLine()) != null) {
          output += read;
        }
        reader.close();
        in.close();
        gStream.close();
      } catch (IOException e) {
        output = new String(data);
      }
    }
    return output;
  }

  public static Response.ErrorListener createErrorListener(final OnRequestDataListener listener) {
    return new Response.ErrorListener() {
      @Override public void onErrorResponse(VolleyError error) {
        NetworkResponse response = error.networkResponse;
        if (response != null && response.data != null) {
          try {
            String errorJson = decompressResponseFromGzip(response.data);
            JSONObject errorJsonObject = new JSONObject(errorJson);
            processErrorResponse(errorJsonObject, listener);
          } catch (JSONException e) {
            listener.onError(MslError.UNK_001, "Unknown Error");
          }
        } else {
          if (error instanceof TimeoutError) {
            listener.onError(MslError.TO_001, "TimeOut Error");
          } else {
            listener.onError(MslError.UNK_001, "Unknown Error");
          }
        }
      }
    };
  }

  public static void processErrorResponse(JSONObject error, OnRequestDataListener listener)
      throws JSONException {
    MslError message = MslError.getEnum(error.getString("message"));
    String codeString = error.getString("codeString");
    if (message.equals(MslError.AUTH_000) || message.equals(MslError.AUTH_005) || message.equals(
        MslError.AUTH_006)) {
      if (listener instanceof OnAuthRequestDataListener) {
        ((OnAuthRequestDataListener) listener).onAuthError();
      } else {
        listener.onError(message, codeString);
      }
    } else {
      listener.onError(message, codeString);
    }
  }
}
