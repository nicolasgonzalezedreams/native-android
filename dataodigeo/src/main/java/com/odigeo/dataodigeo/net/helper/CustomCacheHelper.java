package com.odigeo.dataodigeo.net.helper;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.toolbox.HttpHeaderParser;
import java.util.Map;

public class CustomCacheHelper {

  public static Cache.Entry parseIgnoreCacheHeaders(NetworkResponse response) {
    long cacheTimeInMinutes = 3;
    long now = System.currentTimeMillis();

    Map<String, String> headers = response.headers;
    long serverDate = 0;
    String serverEtag;
    String headerValue;

    headerValue = headers.get("Date");
    if (headerValue != null) {
      serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
    }

    serverEtag = headers.get("ETag");

    final long cacheHitButRefreshed = cacheTimeInMinutes * 60 * 1000; // refresh
    final long cacheExpired = 24 * 60 * 60 * 1000; // 24 horas
    final long softExpire = now + cacheHitButRefreshed;
    final long ttl = now + cacheExpired;

    Cache.Entry entry = new Cache.Entry();
    entry.data = response.data;
    entry.etag = serverEtag;
    entry.softTtl = softExpire;
    entry.ttl = ttl;
    entry.serverDate = serverDate;
    entry.responseHeaders = headers;

    return entry;
  }
}
