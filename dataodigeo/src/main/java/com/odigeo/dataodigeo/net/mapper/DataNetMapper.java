package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.entity.carrousel.Carousel;
import com.odigeo.data.entity.userData.User;
import org.json.JSONException;
import org.json.JSONObject;

public class DataNetMapper<T> {

  private Class<T> mTypeClass;
  private DataType mDataType;
  public DataNetMapper(Class<T> typeClass, DataType dataType) {
    this.mDataType = dataType;
    this.mTypeClass = typeClass;
  }

  public T mapper(String dataToMapper) throws JSONException {
    if (mDataType.equals(DataType.JSON)) {
      return jsonMapper(dataToMapper);
    } else {
      //TODO: Handle it
      return null;
    }
  }

  private T jsonMapper(String dataToMapper) throws JSONException {
    if (mTypeClass.equals(User.class)) {
      return (T) UserNetMapper.jsonUserMapper(dataToMapper);
    } else if (mTypeClass.equals(StoredSearch.class)) {
      return (T) StoredSearchesNetMapper.mapperJsonObjectToStoredSearch(
          new JSONObject(dataToMapper), 0);
    } else if (mTypeClass.equals(Carousel.class)) {
      return (T) CarrouselNetMapper.jsonCarrouselMapper(dataToMapper);
    } else if (mTypeClass.equals(Boolean.class)) {
      return (T) EmptyNetMapper.emptyNetMapper(dataToMapper);
    } else {
      //TODO: Handle it
      return null;
    }
  }

  /* Use this DataType for specify the type of the data that you want to map */
  public enum DataType {
    JSON, XML
  }
}
