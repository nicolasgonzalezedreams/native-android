package com.odigeo.dataodigeo.net.mapper;

import android.support.annotation.NonNull;
import com.odigeo.data.entity.booking.Buyer;
import com.odigeo.data.entity.shoppingCart.BaggageSelectionResponse;
import com.odigeo.data.entity.shoppingCart.Traveller;
import com.odigeo.data.net.helper.AddPassengerNetJSONBuilderInterface;
import com.odigeo.tools.DateHelperInterface;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Deprecated public class AddPassengerNetJSONBuilder implements AddPassengerNetJSONBuilderInterface {

  private DateHelperInterface mDateHelper;

  public AddPassengerNetJSONBuilder(DateHelperInterface dateHelperInterface) {
    mDateHelper = dateHelperInterface;
  }

  public JSONObject getAddPassengerJSON(Buyer buyer, List<Traveller> travellerList, long bookingId,
      String sortCriteria) throws JSONException {
    JSONObject addPassengerJson = new JSONObject();

    addPassengerJson.put("step", "PASSENGER");
    addPassengerJson.put("bookingId", bookingId);
    addPassengerJson.put("sortCriteria", sortCriteria);
    addPassengerJson.put("buyer", parseBuyer(buyer));
    addPassengerJson.put("travellerRequests", parseTravellerRequest(travellerList));

    return addPassengerJson;
  }

  @NonNull private JSONArray parseTravellerRequest(List<Traveller> travellerList)
      throws JSONException {
    JSONArray travellerJsonArray = new JSONArray();

    for (Traveller traveller : travellerList) {
      JSONObject travellerJson = new JSONObject();

      travellerJson.put("travellerTypeName", traveller.getTravellerType().value());
      travellerJson.put("titleName", traveller.getTitle().value());
      travellerJson.put("name", traveller.getName());
      travellerJson.put("firstLastName", traveller.getFirstLastName());
      travellerJson.put("secondLastName", traveller.getSecondLastName());
      travellerJson.put("identification", traveller.getIdentification());
      travellerJson.put("identificationTypeName", traveller.getIdentificationType().value());
      travellerJson.put("identificationExpirationDay",
          mDateHelper.getDay(traveller.getIdentificationExpirationDate()));
      travellerJson.put("identificationExpirationMonth",
          mDateHelper.getMonth(traveller.getIdentificationExpirationDate()));
      travellerJson.put("identificationExpirationYear",
          mDateHelper.getYear(traveller.getIdentificationExpirationDate()));
      travellerJson.put("identificationIssueContryCode",
          traveller.getIdentificationIssueCountryCode());
      travellerJson.put("nationalityCountryCode", traveller.getNationalityCountryCode());
      travellerJson.put("localityCodeOfResidence", traveller.getLocalityCodeOfResidence());
      travellerJson.put("gender", traveller.getTravellerGender().value());
      travellerJson.put("dayOfBirth", mDateHelper.getDay(traveller.getDateOfBirth()));
      travellerJson.put("monthOfBirth", mDateHelper.getMonth(traveller.getDateOfBirth()));
      travellerJson.put("yearOfBirth", mDateHelper.getYear(traveller.getDateOfBirth()));
      travellerJson.put("mealName", "STANDARD");//this params don't use
      travellerJson.put("baggageSelections", parseBaggageSelections(traveller));

      travellerJsonArray.put(travellerJson);
    }
    return travellerJsonArray;
  }

  private JSONArray parseBaggageSelections(Traveller traveller) throws JSONException {
    JSONArray baggageSelectionJsonArray = new JSONArray();

    for (BaggageSelectionResponse baggageSelectionResponse : traveller.getBaggageSelections()) {

      JSONObject baggageSelectionsJson = new JSONObject();

      JSONObject baggageDescriptorJson = new JSONObject();
      baggageDescriptorJson.put("pieces",
          baggageSelectionResponse.getBaggageDescriptor().getPieces());
      baggageDescriptorJson.put("kilos",
          baggageSelectionResponse.getBaggageDescriptor().getKilos());

      baggageSelectionsJson.put("baggageDescriptor", baggageDescriptorJson);
      baggageSelectionsJson.put("segmentTypeIndex",
          baggageSelectionResponse.getSegmentTypeIndex().value());

      baggageSelectionJsonArray.put(baggageSelectionsJson);
    }

    return baggageSelectionJsonArray;
  }

  private JSONObject parseBuyer(Buyer buyer) throws JSONException {
    JSONObject buyerJson = new JSONObject();
    buyerJson.put("cityName", buyer.getCityName());
    buyerJson.put("phoneNumber1", buyer.getPhone());
    buyerJson.put("zipCode", buyer.getZipCode());
    buyerJson.put("cpf", buyer.getCpf());
    buyerJson.put("stateName", buyer.getStateName());
    buyerJson.put("address", buyer.getAddress());
    buyerJson.put("countryPhoneNumber1", buyer.getPhoneCountryCode());
    buyerJson.put("identification", buyer.getIdentification());
    buyerJson.put("name", buyer.getName());
    buyerJson.put("mail", buyer.getEmail());
    buyerJson.put("lastNames", buyer.getLastname());
    buyerJson.put("buyerIdentificationTypeName", buyer.getIdentificationType());
    buyerJson.put("countryCode", buyer.getCountry());
    return buyerJson;
  }
}
