package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.userData.UserLogin;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Irving
 * @since 15/09/2015
 */
public class UserLoginNetMapper {

  public static JSONObject mapperUserLoginToJsonObject(UserLogin login) throws JSONException {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(JsonKeys.ID, login.getUserLoginId());
    if (login.getPassword() != null) {
      jsonObject.put(JsonKeys.PASSWORD, login.getPassword());
    }
    if (login.getAccessToken() != null) {
      jsonObject.put(JsonKeys.ACCESS_TOKEN, login.getAccessToken());
    }
    if (login.getHashCode() != null) {
      jsonObject.put(JsonKeys.HASH_CODE, login.getHashCode());
    }
    if (login.getActivationCode() != null) {
      jsonObject.put(JsonKeys.ACTIVATION_CODE, login.getActivationCode());
    }
    //jsonObject.put(JsonKeys.ACTIVATION_CODE_DATE, login.getActivationCodeDate());
    jsonObject.put(JsonKeys.LOGIN_ATTEMPT_FAILED, login.getLoginAttemptFailed());
    jsonObject.put(JsonKeys.LAST_LOGIN_DATE, login.getLastLoginDate());
    return jsonObject;
  }

  public static UserLogin mapperJSONObjectToUserLogin(JSONObject userLogin, long userId) {
    if (userLogin != null) {
      return new UserLogin(0, userLogin.optLong(JsonKeys.ID),
          MapperUtil.optString(userLogin, JsonKeys.PASSWORD),
          MapperUtil.optString(userLogin, JsonKeys.ACCESS_TOKEN),
          userLogin.optLong(JsonKeys.EXPIRATION_TOKEN_DATE),
          MapperUtil.optString(userLogin, JsonKeys.HASH_CODE),
          MapperUtil.optString(userLogin, JsonKeys.ACTIVATION_CODE),
          userLogin.optLong(JsonKeys.ACTIVATION_CODE_DATE),
          userLogin.optLong(JsonKeys.LOGIN_ATTEMPT_FAILED),
          userLogin.optLong(JsonKeys.LAST_LOGIN_DATE),
          MapperUtil.optString(userLogin, JsonKeys.REFRESH_TOKEN), userId);
    }
    return null;
  }
}
