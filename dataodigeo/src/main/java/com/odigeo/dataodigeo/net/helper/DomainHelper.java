package com.odigeo.dataodigeo.net.helper;

import com.odigeo.data.net.helper.DomainHelperInterface;

public class DomainHelper implements DomainHelperInterface {

  private static DomainHelper mInstance;

  private String mUrl;

  private DomainHelper(String url) {
    mUrl = url;
  }

  public static DomainHelper newInstance(String url) {
    if (mInstance == null) {
      mInstance = new DomainHelper(url);
    }
    return mInstance;
  }

  @Override public void putUrl(String url) {
    mUrl = url;
  }

  @Override public String getUrl() {
    return mUrl;
  }
}
