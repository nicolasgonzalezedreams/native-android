package com.odigeo.dataodigeo.net.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserProfile.Title;
import com.odigeo.data.net.error.MslError;
import com.odigeo.presenter.SocialLoginPresenter;
import com.odigeo.presenter.contracts.views.LoginSocialInterface;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

import static com.odigeo.presenter.contracts.views.LoginSocialInterface.EMAIL;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.FACEBOOK_SOURCE;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.FEMALE_GENDER;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.FEMALE_GENDER_ABREVIATED;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.FIRST_NAME;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.GENDER;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.IMAGE_URL;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.LAST_NAME;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.NAME;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.TOKEN;

public class FacebookController {

  private Fragment fragment;
  private CallbackManager callbackManager;
  private SocialLoginPresenter loginPresenter;

  public FacebookController(Fragment fragment, SocialLoginPresenter loginPresenter) {
    this.fragment = fragment;
    this.loginPresenter = loginPresenter;
    if (!FacebookSdk.isInitialized()) {
      FacebookSdk.sdkInitialize(fragment.getActivity().getApplicationContext());
    }
    initializeFacebook();
  }

  private void initializeFacebook() {
    callbackManager = CallbackManager.Factory.create();
    LoginManager.getInstance().registerCallback(callbackManager, buildCallback());
  }

  public void login() {
    LoginManager.getInstance()
        .logInWithReadPermissions(fragment, Arrays.asList("public_profile", "email"));
  }

  public void logout() {
    LoginManager.getInstance().logOut();
  }

  private FacebookCallback<LoginResult> buildCallback() {
    return new FacebookCallback<LoginResult>() {
      @Override public void onSuccess(LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
            new GraphRequest.GraphJSONObjectCallback() {
              @Override public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                  Map<String, String> mapUserData = buildUserData(object);
                  UserProfile userProfile = buildUserProfile(mapUserData);
                  if (!TextUtils.isEmpty(mapUserData.get(EMAIL))) {
                    loginPresenter.login(mapUserData, userProfile, FACEBOOK_SOURCE);
                    //logout();
                  } else {
                    loginPresenter.buildSocialLoginErrorMessage(MslError.FB_001, FACEBOOK_SOURCE);
                  }
                } catch (Exception e) {
                  loginPresenter.buildSocialLoginErrorMessage(MslError.UNK_001, FACEBOOK_SOURCE);
                }
              }
            });

        Bundle parameters = new Bundle();
        parameters.putString("fields",
            "id, name, email, first_name, last_name, gender, cover, picture" + ".type(large)");
        request.setParameters(parameters);
        request.executeAsync();
      }

      @Override public void onCancel() {
        ((LoginSocialInterface) fragment).hideProgress();
        logout();
      }

      @Override public void onError(FacebookException e) {
        loginPresenter.buildSocialLoginErrorMessage(MslError.AUTH_005, FACEBOOK_SOURCE);
        logout();
      }
    };
  }

  private Map<String, String> buildUserData(JSONObject jsonUserData) throws Exception {
    Map<String, String> userData = new HashMap<>();
    userData.put(EMAIL, jsonUserData.optString("email"));
    userData.put(IMAGE_URL,
        jsonUserData.getJSONObject("picture").getJSONObject("data").getString("url"));
    userData.put(NAME, jsonUserData.optString("name"));
    userData.put(FIRST_NAME, jsonUserData.optString("first_name"));
    userData.put(LAST_NAME, jsonUserData.optString("last_name"));
    userData.put(GENDER, jsonUserData.optString("gender"));
    userData.put(TOKEN, AccessToken.getCurrentAccessToken().getToken());
    return userData;
  }

  private UserProfile buildUserProfile(Map<String, String> userData) {
    Title userTitle =
        userData.get(GENDER).equalsIgnoreCase(FEMALE_GENDER_ABREVIATED) || userData.get(GENDER)
            .equalsIgnoreCase(FEMALE_GENDER) ? Title.MS : Title.MR;
    return new UserProfile(-1, -1, userData.get(GENDER), userTitle, userData.get(FIRST_NAME), null,
        userData.get(LAST_NAME), null, UserProfile.UNKNOWN_BIRTHDATE, null, null, null, null, null,
        null, null, true, userData.get(IMAGE_URL), null, null, -1);
  }

  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    callbackManager.onActivityResult(requestCode, resultCode, data);
  }
}
