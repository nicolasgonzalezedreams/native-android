package com.odigeo.dataodigeo.net.mapper.utils;

import org.json.JSONObject;

/**
 * Class used by JsonMappers.
 *
 * @author matias.dirusso
 */
public class MapperUtil {

  /**
   * Solves the issue when the server returns null instead of NULL, so that this not shown as
   * "null".
   * http://code.google.com/p/android/issues/detail?id=13830
   *
   * @param json json
   * @param key key
   * @return value
   */
  public static String optString(JSONObject json, String key) {
    if (!json.isNull(key)) {
      return json.optString(key, null);
    } else {
      return null;
    }
  }
}
