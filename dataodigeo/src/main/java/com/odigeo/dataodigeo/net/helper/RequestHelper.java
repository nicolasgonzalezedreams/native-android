package com.odigeo.dataodigeo.net.helper;

import com.android.volley.Cache;
import com.android.volley.toolbox.ImageRequest;

public class RequestHelper {

  private CustomRequestQueue mRequestQueue;

  public RequestHelper(CustomRequestQueue requestQueue) {
    this.mRequestQueue = requestQueue;
  }

  public void addRequest(MslRequest mslRequest) {
    mRequestQueue.add(mslRequest);
  }

  public void addSyncRequest(MslSyncRequest mslSyncRequest) {
    mRequestQueue.add(mslSyncRequest);
  }

  public void addImageRequest(ImageRequest imageRequest) {
    mRequestQueue.add(imageRequest);
  }

  public void cleanCache(String key) {
    mRequestQueue.getCache().remove(key);
  }

  public void cleanCache() {
    mRequestQueue.getCache().clear();
  }

  public Cache.Entry isEntryInCache(String key) {
    return mRequestQueue.getCache().get("key");
  }

  public long getLastRequestDateInCache(String url) {
    Cache.Entry entry = mRequestQueue.getCache().get(url);
    if (entry != null) {
      return entry.serverDate;
    }
    return -1;
  }
}
