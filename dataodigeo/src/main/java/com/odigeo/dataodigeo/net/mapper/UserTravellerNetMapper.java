package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import com.odigeo.tools.CheckerTool;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Irving
 * @since 15/09/2015
 */
public class UserTravellerNetMapper {

  public static JSONObject mapperUserTravellerToJsonObject(UserTraveller userTraveller) {
    JSONObject jsonObject = new JSONObject();
    try {
      jsonObject.put(JsonKeys.ID, userTraveller.getUserTravellerId());
      jsonObject.put(JsonKeys.BUYER, userTraveller.getBuyer());
      if (!CheckerTool.isEmptyField(userTraveller.getEmail())) {
        jsonObject.put(JsonKeys.EMAIL, userTraveller.getEmail());
      }
      if (userTraveller.getTypeOfTraveller() != null
          && userTraveller.getTypeOfTraveller() != UserTraveller.TypeOfTraveller.UNKNOWN) {
        jsonObject.put(JsonKeys.TYPE_OF_TRAVELLER, userTraveller.getTypeOfTraveller().toString());
      }
      if (!CheckerTool.isEmptyField(userTraveller.getMealType())) {
        jsonObject.put(JsonKeys.MEAL_TYPE, userTraveller.getMealType());
      }
      if (userTraveller.getUserFrequentFlyers() != null) {
        jsonObject.put(JsonKeys.FREQUENT_FLYER_LIST,
            UserFrequentFlyerNetMapper.getFrequentFlyerListToJson(
                userTraveller.getUserFrequentFlyers()));
      }
      if (userTraveller.getUserProfile() != null) {
        jsonObject.put(JsonKeys.PROFILE,
            UserProfileNetMapper.mapperUserProfileToJsonObject(userTraveller.getUserProfile()));
      }
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return jsonObject;
  }

  public static UserTraveller mapperJsonObjectToUserTraveller(JSONObject traveller, long userId) {
    try {
      return new UserTraveller(0, traveller.optLong(JsonKeys.ID),
          traveller.optBoolean(JsonKeys.BUYER), MapperUtil.optString(traveller, JsonKeys.EMAIL),
          MapperUtil.optString(traveller, JsonKeys.TYPE_OF_TRAVELLER) == null
              ? UserTraveller.TypeOfTraveller.UNKNOWN : UserTraveller.TypeOfTraveller.valueOf(
              MapperUtil.optString(traveller, JsonKeys.TYPE_OF_TRAVELLER)),
          MapperUtil.optString(traveller, JsonKeys.MEAL_TYPE),
          UserFrequentFlyerNetMapper.mapperJSONArrayToFrequentFlyerList(
              traveller.optJSONArray(JsonKeys.FREQUENT_FLYER_LIST), traveller.optLong(JsonKeys.ID)),
          UserProfileNetMapper.mapperJsonObjectToUserProfile(
              traveller.optJSONObject(JsonKeys.PROFILE), traveller.optLong(JsonKeys.ID)), userId);
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static List<UserTraveller> mapperJsonArrayToUserTravellerList(
      JSONArray jsonArrayTravellers, long userId) throws JSONException {

    if (jsonArrayTravellers != null) {
      List<UserTraveller> travellerList = new ArrayList<>();
      for (int indexT = 0; indexT < jsonArrayTravellers.length(); indexT++) {
        JSONObject traveller = jsonArrayTravellers.optJSONObject(indexT);
        if (traveller != null) {
          travellerList.add(
              UserTravellerNetMapper.mapperJsonObjectToUserTraveller(traveller, userId));
        }
      }
      return travellerList;
    }
    return null;
  }

  public static JSONArray getUserTravellerListToJson(List<UserTraveller> userTravellerList) {
    JSONArray jsonArray = new JSONArray();
    if (userTravellerList != null) {
      for (int indexUT = 0; indexUT < userTravellerList.size(); indexUT++) {
        if (userTravellerList.get(indexUT) != null) {
          jsonArray.put(mapperUserTravellerToJsonObject(userTravellerList.get(indexUT)));
        }
      }
    }
    return jsonArray;
  }
}
