package com.odigeo.dataodigeo.net.controllers;

import android.support.annotation.VisibleForTesting;
import com.odigeo.data.net.controllers.VisitUserNetController;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.CustomRequestMethod;
import com.odigeo.dataodigeo.net.helper.MslAuthRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.HashMap;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.DELETE;
import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.PUT;

public class VisitUserNetControllerImpl implements VisitUserNetController {

  private RequestHelper requestHelper;
  private DomainHelperInterface domainHelper;
  private HeaderHelperInterface headerHelper;
  private HashMap<String, String> mapHeaders;
  private TokenController tokenController;

  public VisitUserNetControllerImpl(RequestHelper requestHelper, DomainHelperInterface domainHelper,
      HeaderHelperInterface headerHelper, TokenController tokenController) {
    this.requestHelper = requestHelper;
    this.domainHelper = domainHelper;
    this.headerHelper = headerHelper;
    this.tokenController = tokenController;
  }

  @Override public void loginVisitUser(final OnRequestDataListener<String> listener, long userId) {
    performRequest(listener, userId, PUT);
  }

  @Override public void logoutVisitUser(final OnRequestDataListener<String> listener, long userId) {
    performRequest(listener, userId, DELETE);
  }

  private void performRequest(final OnRequestDataListener<String> listener, long userId,
      CustomRequestMethod requestMethod) {
    String url = domainHelper.getUrl() + API_URL_VISIT_USER + "/" + userId;
    MslAuthRequest<String> registerVisitUserRequest =
        new MslAuthRequest<>(requestMethod, url, new OnRequestDataListener<String>() {
          @Override public void onResponse(String object) {
            listener.onResponse(object);
          }

          @Override public void onError(MslError error, String message) {
            listener.onError(error, message);
          }
        }, tokenController);

    mapHeaders = new HashMap<>();
    headerHelper.putDeviceId(mapHeaders);
    headerHelper.putAcceptEncoding(mapHeaders);
    headerHelper.putAcceptV4(mapHeaders);
    headerHelper.putContentType(mapHeaders);
    registerVisitUserRequest.setHeaders(mapHeaders);
    requestHelper.addRequest(registerVisitUserRequest);
  }

  @VisibleForTesting @Override public HashMap<String, String> getMapHeaders() {
    return mapHeaders;
  }
}