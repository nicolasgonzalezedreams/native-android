package com.odigeo.dataodigeo.net.helper;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.mapper.utils.ResponseUtil;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 11/02/16
 */
public class CountryRequest extends MslRequest<String> {

  public static final int RESPONSE_OK = 200;

  public CountryRequest(CustomRequestMethod method, String url,
      OnRequestDataListener<String> listener) {
    super(method, url, listener);
  }

  @Override protected Response<String> parseNetworkResponse(NetworkResponse response) {
    if (response.statusCode == RESPONSE_OK) {
      return Response.success(ResponseUtil.decompressResponseFromGzip(response.data),
          HttpHeaderParser.parseCacheHeaders(response));
    } else {
      return Response.success(null, HttpHeaderParser.parseCacheHeaders(response));
    }
  }
}
