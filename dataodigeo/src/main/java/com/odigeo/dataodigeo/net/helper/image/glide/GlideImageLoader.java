package com.odigeo.dataodigeo.net.helper.image.glide;

import android.content.ComponentName;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.RemoteViews;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.AppWidgetTarget;
import com.bumptech.glide.request.target.Target;
import com.odigeo.dataodigeo.net.helper.image.OdigeoImageLoader;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class GlideImageLoader implements OdigeoImageLoader<ImageView> {

  private static final int NO_IMAGE = 0;
  private static final int TRANSFORMATION_DURATION = 300;

  private final Context context;

  public GlideImageLoader(Context context) {
    this.context = context;
  }

  @Override public void load(ImageView view, String imageUrl) {
    load(view, imageUrl, NO_IMAGE, NO_IMAGE, true);
  }

  @Override
  public void load(ImageView view, @DrawableRes int drawableResId, boolean addDefaultAnimations) {
    RequestBuilder<Drawable> builder = Glide.with(context).load(drawableResId);
    if (addDefaultAnimations) {
      builder.transition(withCrossFade(android.R.anim.fade_in, TRANSFORMATION_DURATION));
    }
    builder.into(view);
  }

  @Override public void load(ImageView view, String imageUrl, @DrawableRes int placeholderId) {
    load(view, imageUrl, NO_IMAGE, placeholderId, true);
  }

  @Override public void load(final ImageView view, String imageUrl, @DrawableRes int loadingImage,
      @DrawableRes final int placeholderId, boolean addDefaultAnimations) {
    RequestBuilder<Drawable> builder = Glide.with(context).load(imageUrl);
    builder = addImageOptions(builder, loadingImage, placeholderId, null);
    builder = setPlaceholder(builder, view, placeholderId, null);

    if (addDefaultAnimations) {
      builder.transition(withCrossFade(android.R.anim.fade_in, TRANSFORMATION_DURATION));
    }
    builder.into(view);
  }

  private void load(ImageView view, @DrawableRes int drawable, Transformation transformation) {
    RequestBuilder<Drawable> builder = Glide.with(context)
        .load(drawable)
        .transition(withCrossFade(android.R.anim.fade_in, TRANSFORMATION_DURATION));

    if (transformation != null) {
      builder = builder.apply(RequestOptions.bitmapTransform(transformation));
    }

    builder.into(view);
  }

  @Override public void loadCircleTransformation(ImageView view, String imageUrl) {
    loadCircleTransformation(view, imageUrl, NO_IMAGE);
  }

  @Override public void loadCircleTransformation(ImageView view, String imageUrl,
      @DrawableRes int placeholderId) {
    RequestBuilder<Drawable> builder = Glide.with(context)
        .load(imageUrl)
        .transition(withCrossFade(android.R.anim.fade_in, TRANSFORMATION_DURATION));

    CircleTransform bitmapTransformation = new CircleTransform(context);
    builder = addImageOptions(builder, NO_IMAGE, placeholderId, bitmapTransformation);
    builder = setPlaceholder(builder, view, placeholderId, bitmapTransformation);

    builder.into(view);
  }

  @Override public void loadRoundedTransformation(ImageView view, String imageUrl,
      @DrawableRes int loadingImage, @DrawableRes int placeholderId, int radius) {
    RequestBuilder<Drawable> builder = Glide.with(context)
        .load(imageUrl)
        .transition(withCrossFade(android.R.anim.fade_in, TRANSFORMATION_DURATION));

    RoundedCornersTransformation bitmapTransformation =
        new RoundedCornersTransformation(context, radius, 0);
    builder = addImageOptions(builder, loadingImage, placeholderId, bitmapTransformation);
    builder = setPlaceholder(builder, view, placeholderId, bitmapTransformation);
    builder.into(view);
  }

  @Override
  public void loadRoundedTransformation(ImageView view, @DrawableRes int drawable, int radius) {
    load(view, drawable, new RoundedCornersTransformation(context, radius, 0));
  }

  @Override public void loadWidgetImage(final RemoteViews views, final String imageUrl,
      @IdRes final int imageResource, final int width, final int height, int appWidgetId,
      @DrawableRes final int placeholderId) {
    AppWidgetTarget target = new AppWidgetTarget(context, width, height, appWidgetId, views,
        new ComponentName(context.getPackageName(), context.getClass().getName()));

    Glide.with(context).asBitmap().load(imageUrl).into(target);
  }

  private RequestBuilder<Drawable> addImageOptions(RequestBuilder<Drawable> builder,
      @DrawableRes int loadingImage, @DrawableRes final int placeholderId,
      Transformation bitmapTransformation) {

    int finalLoadingImage = loadingImage == NO_IMAGE ? placeholderId : loadingImage;

    if (bitmapTransformation != null) {
      RequestBuilder<Drawable> thumbnail = Glide.with(context)
          .load(finalLoadingImage)
          .apply(RequestOptions.bitmapTransform(bitmapTransformation));
      builder =
          builder.thumbnail(thumbnail).apply(RequestOptions.bitmapTransform(bitmapTransformation));
    } else {
      RequestBuilder<Drawable> thumbnail = Glide.with(context).load(finalLoadingImage);
      builder = builder.thumbnail(thumbnail);
    }
    return builder;
  }

  private RequestBuilder<Drawable> setPlaceholder(RequestBuilder<Drawable> builder,
      final ImageView view, @DrawableRes final int placeholderId,
      final Transformation transformation) {
    builder = builder.listener(new RequestListener<Drawable>() {
      @Override
      public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target,
          boolean isFirstResource) {
        loadPlaceholder(view, placeholderId, transformation);
        return false;
      }

      @Override
      public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target,
          DataSource dataSource, boolean isFirstResource) {
        return false;
      }
    });
    return builder;
  }

  private void loadPlaceholder(ImageView view, int placeholderId, Transformation transformation) {
    RequestBuilder<Drawable> builder = Glide.with(context)
        .load(placeholderId)
        .transition(withCrossFade(android.R.anim.fade_in, TRANSFORMATION_DURATION));

    builder = addImageOptions(builder, NO_IMAGE, NO_IMAGE, transformation);

    builder.into(view);
  }
}
