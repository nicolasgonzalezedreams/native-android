package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.userData.UserAddress;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import com.odigeo.tools.CheckerTool;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Irving
 * @since 15/09/2015
 */
public class UserAddressNetMapper {

  public static String mapperUserAddresstoJsonString(UserAddress userAddress) throws JSONException {
    return mapperUserAddressToJsonObject(userAddress).toString();
  }

  public static JSONObject mapperUserAddressToJsonObject(UserAddress userAddress)
      throws JSONException {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(JsonKeys.ID, userAddress.getUserAddressId());
    if (!CheckerTool.isEmptyField(userAddress.getAddress())) {
      jsonObject.put(JsonKeys.ADDRESS, userAddress.getAddress());
    }
    if (!CheckerTool.isEmptyField(userAddress.getAddressType())) {
      jsonObject.put(JsonKeys.ADDRESS_TYPE, userAddress.getAddressType());
    }
    if (!CheckerTool.isEmptyField(userAddress.getCity())) {
      jsonObject.put(JsonKeys.CITY, userAddress.getCity());
    }
    if (!CheckerTool.isEmptyField(userAddress.getCountry())) {
      jsonObject.put(JsonKeys.COUNTRY, userAddress.getCountry());
    }
    if (!CheckerTool.isEmptyField(userAddress.getState())) {
      jsonObject.put(JsonKeys.STATE, userAddress.getState());
    }
    if (!CheckerTool.isEmptyField(userAddress.getPostalCode())) {
      jsonObject.put(JsonKeys.POSTAL_CODE, userAddress.getPostalCode());
    }
    if (!CheckerTool.isEmptyField(userAddress.getAlias())) {
      jsonObject.put(JsonKeys.ALIAS, userAddress.getAlias());
    }
    jsonObject.put(JsonKeys.IS_PRIMARY, userAddress.getIsPrimary());
    return jsonObject;
  }

  public static UserAddress mapperJSONObjectToUserAddress(JSONObject jsonObject, long profileId) {
    if (jsonObject != null) {
      return new UserAddress(0, jsonObject.optLong(JsonKeys.ID),
          MapperUtil.optString(jsonObject, JsonKeys.ADDRESS),
          MapperUtil.optString(jsonObject, JsonKeys.ADDRESS_TYPE),
          MapperUtil.optString(jsonObject, JsonKeys.CITY),
          MapperUtil.optString(jsonObject, JsonKeys.COUNTRY),
          MapperUtil.optString(jsonObject, JsonKeys.STATE),
          MapperUtil.optString(jsonObject, JsonKeys.POSTAL_CODE),
          jsonObject.optBoolean(JsonKeys.IS_PRIMARY),
          MapperUtil.optString(jsonObject, JsonKeys.ALIAS), profileId);
    }
    return null;
  }
}
