package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Irving
 * @since 15/09/2015
 */
public class UserFrequentFlyerNetMapper {

  public static JSONObject mapperUserFrequentFlyerToJSONObject(UserFrequentFlyer frequentFlyer)
      throws JSONException {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(JsonKeys.ID, frequentFlyer.getFrequentFlyerId());
    if (frequentFlyer.getAirlineCode() != null) {
      jsonObject.put(JsonKeys.AIRLINE_CODE, frequentFlyer.getAirlineCode());
    }
    if (frequentFlyer.getFrequentFlyerNumber() != null) {
      jsonObject.put(JsonKeys.FREQUENT_FLYER_NUMBER, frequentFlyer.getFrequentFlyerNumber());
    }
    return jsonObject;
  }

  public static JSONArray getFrequentFlyerListToJson(List<UserFrequentFlyer> userFrequentFlyerList)
      throws JSONException {
    JSONArray jsonArray = new JSONArray();
    if (userFrequentFlyerList != null) {
      for (int indexFF = 0; indexFF < userFrequentFlyerList.size(); indexFF++) {
        if (userFrequentFlyerList.get(indexFF) != null) {
          jsonArray.put(mapperUserFrequentFlyerToJSONObject(userFrequentFlyerList.get(indexFF)));
        }
      }
    }
    return jsonArray;
  }

  public static UserFrequentFlyer mapperJSONObjectToFrequentFlyer(JSONObject frequentFlyer,
      long travellerId) {
    if (frequentFlyer != null) {
      return new UserFrequentFlyer(0, frequentFlyer.optLong(JsonKeys.ID),
          MapperUtil.optString(frequentFlyer, JsonKeys.AIRLINE_CODE),
          MapperUtil.optString(frequentFlyer, JsonKeys.FREQUENT_FLYER_NUMBER), travellerId);
    }
    return null;
  }

  public static List<UserFrequentFlyer> mapperJSONArrayToFrequentFlyerList(
      JSONArray jsonArrayFrequentFlyers, long travellerId) throws JSONException {
    List<UserFrequentFlyer> frequentFlyerList = new ArrayList<>();
    if (jsonArrayFrequentFlyers != null) {
      for (int indexFFQ = 0; indexFFQ < jsonArrayFrequentFlyers.length(); indexFFQ++) {
        JSONObject frequentFlyer = jsonArrayFrequentFlyers.optJSONObject(indexFFQ);
        if (frequentFlyer != null) {
          frequentFlyerList.add(mapperJSONObjectToFrequentFlyer(frequentFlyer, travellerId));
        }
      }
    }
    return frequentFlyerList;
  }
}
