package com.odigeo.dataodigeo.net.helper;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class HeaderHelper implements HeaderHelperInterface {

  public static final String CONTENT_TYPE = "Content-Type";
  public static final String DEVICE_ID = "Device-ID";
  public static final String ACCEPT_ENCODING = "Accept-Encoding";
  public static final String ACCEPT = "Accept";
  public static final String HEADER_NONE_MATCH_PARAM = "If-None-Match";
  public static final String HEADER_AUTHORIZATION_PARAM = "ODGAuthorization";
  private static final double XXXHIGH = 4.0;
  private static final double XXHIGH = 3.0;
  private static final double XHIGH = 2.0;
  private static final double HIGH = 1.5;
  private static final String MSL_LOW = "S"; //not used
  private static final String MSL_MEDIUM = "M";
  private static final String MSL_HIGH = "L";
  private static final String MSL_XHIGH = "XL";
  private static final String MSL_XXHIGH = "XXL";
  private static final String MSL_XXXHIGH = "XXXL";
  private static final String ACCEPT_MSL_V3 = "v3";
  private static final String ACCEPT_MSL_V4 = "v4";
  private static final String ACCEPT_TYPE = "json";
  private static final String ACCEPT_ENCODE = "charset=utf-8";
  private static final String ACCEPT_BASE_MSL = "application/vnd.com.odigeo.msl.";
  private static final String CHANNEL = "ANDROID";
  private static HeaderHelper mInstance;
  private Context mContext;
  private String mWebSite;
  private String mLanguage;
  private String mLocale;
  private String mBrand;

  private HeaderHelper(Context context) {
    mContext = context;
  }

  public static HeaderHelper newInstance(Context context) {
    if (mInstance == null) {
      mInstance = new HeaderHelper(context);
    }
    return mInstance;
  }

  @Override public void putDeviceId(Map<String, String> hashMap) {
    hashMap.put(DEVICE_ID, getDeviceID(getApplicationVersionName(), getApplicationVersionCode(),
        getOdigeoScreenSize()));
  }

  @Override public void putContentType(Map<String, String> hashMap) {
    hashMap.put(CONTENT_TYPE, "application/json");
  }

  @Override public void putAcceptEncoding(Map<String, String> hashMap) {
    hashMap.put(ACCEPT_ENCODING, "gzip,deflate,sdch");
  }

  @Override public void putAccept(Map<String, String> hashMap) {
    hashMap.put(ACCEPT, getAccept());
  }

  @Override public void putAcceptV4(Map<String, String> hashMap) {
    hashMap.put(ACCEPT, getAcceptV4());
  }

  @Override public void putAcceptWithoutVersion(Map<String, String> hashMap) {
    hashMap.put(ACCEPT, "application/json;charset=utf-8");
  }

  @Override public void putHeaderNoneMatchParam(Map<String, String> hashMap) {
    hashMap.put(HEADER_NONE_MATCH_PARAM, "");
  }

  @Override public void createLoginAuthorizationHeader(Map<String, String> hashMap, String header) {
    hashMap.put(HEADER_AUTHORIZATION_PARAM, header);
  }

  /**
   * Gets the odigeo screen size id
   *
   * @return An string representing the odigeo id
   */
  private String getOdigeoScreenSize() {
    //TODO: DO NOT DELETE. Fix it with MSL because it's causing an error
    //        DisplayMetrics dm = mContext.getResources().getDisplayMetrics();
    //        float density = dm.density;
    //
    //        if (density >= XXXHIGH && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
    //            return MSL_XXXHIGH;
    //
    //        } else if (density >= XXHIGH && density < XXXHIGH && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
    //            return MSL_XXHIGH;
    //
    //        } else if (density >= XHIGH) {
    //            return MSL_XHIGH;
    //
    //        } else if (density >= HIGH && density < XHIGH) {
    //            return MSL_HIGH;
    //
    //        } else {
    //            return MSL_MEDIUM;
    //        }
    return MSL_XHIGH;
  }

  /**
   * This method gets the device's ID
   * <p/>
   * DeviceID Format:  CHANNEL;MODEL;OS-VERSION;BRAND;MARKET;WIFI-MAC-ADDRESS;LANGUAGE;LOCALE;APPVERSION;APPBUILD;SCALE
   * Examples:
   * ANDROID;GSI;4.0.1;O;FR;00:24:81:22:ec:78;it;es_ES;0.15;0;L
   *
   * @param appVersion Version number of the application
   * @param appBuild Build number of the application
   * @param screenSize The size of the device's screen
   * @return The device's ID
   */
  private String getDeviceID(String appVersion, String appBuild, String screenSize) {

    String macAddress = getMACAddress("wlan0");

    return String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", CHANNEL, Build.MODEL,
        Build.VERSION.RELEASE, mBrand, mWebSite, macAddress, mLanguage, mLocale, appVersion,
        appBuild, screenSize);
  }

  /**
   * Returns MAC address of the given interface name.
   *
   * @param interfaceName eth0, wlan0 or NULL=use last interface
   * @return mac address or empty string
   */
  private String getMACAddress(String interfaceName) {
    String macAddress = "";

    try {
      List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());

      for (NetworkInterface intf : interfaces) {

        byte[] mac = intf.getHardwareAddress();

        if (mac != null) {
          StringBuilder buf = new StringBuilder();

          for (byte aMac : mac) {
            buf.append(String.format("%02X:", aMac));
          }

          if (buf.length() > 0) {
            buf.deleteCharAt(buf.length() - 1);
          }

          macAddress = buf.toString();
        }

        if (intf.getName().equalsIgnoreCase(interfaceName)) {
          return macAddress;
        }
      }
    } catch (Exception ex) {
      return "00:00:00:00:00:00";
    }

    return macAddress;
  }

  /**
   * This method gets the application version name
   *
   * @return Application's version name
   */
  private String getApplicationVersionName() {
    PackageManager packageManager = mContext.getPackageManager();

    try {
      PackageInfo packageInfo = packageManager.getPackageInfo(mContext.getPackageName(), 0);

      return packageInfo.versionName;
    } catch (Exception e) {
      return "0";
    }
  }

  /**
   * This method gets the application version code
   *
   * @return Application's version code
   */
  private String getApplicationVersionCode() {
    PackageManager packageManager = mContext.getPackageManager();

    try {
      PackageInfo packageInfo = packageManager.getPackageInfo(mContext.getPackageName(), 0);

      return String.valueOf(packageInfo.versionCode);
    } catch (Exception e) {
      return "0";
    }
  }

  private String getAccept() {
    return ACCEPT_BASE_MSL + ACCEPT_MSL_V3 + "+" + ACCEPT_TYPE + ";" + ACCEPT_ENCODE;
  }

  private String getAcceptV4() {
    return ACCEPT_BASE_MSL + ACCEPT_MSL_V4 + "+" + ACCEPT_TYPE + ";" + ACCEPT_ENCODE;
  }

  public void setLocale(String locale) {
    mLocale = locale;
  }

  public void setmWebSite(String mWebSite) {
    this.mWebSite = mWebSite;
  }

  public void setmLanguage(String mLanguage) {
    this.mLanguage = mLanguage;
  }

  public void setmBrand(String mBrand) {
    this.mBrand = mBrand;
  }
}
