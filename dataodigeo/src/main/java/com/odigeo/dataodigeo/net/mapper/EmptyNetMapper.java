package com.odigeo.dataodigeo.net.mapper;

import org.json.JSONException;

/**
 * Created by matias.dirusso on 24/08/2015.
 */
public class EmptyNetMapper {

  public static Boolean emptyNetMapper(String emptyResponse) throws JSONException {
    if (emptyResponse.equals("")) {
      return true;
    }
    return false;
  }
}
