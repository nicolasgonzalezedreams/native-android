package com.odigeo.dataodigeo.net.helper;

import com.android.volley.Request;

/* *
*
* By using CustomRequestMethodImp we are making the application more independent of Volley.
* If we decide to change Volley by other in the future, we just have to change this class.
*
* */
public enum CustomRequestMethod {

  GET(Request.Method.GET), POST(Request.Method.POST), PUT(Request.Method.PUT), DELETE(
      Request.Method.DELETE), HEAD(Request.Method.HEAD), OPTIONS(Request.Method.OPTIONS), TRACE(
      Request.Method.TRACE), PATCH(Request.Method.PATCH);

  private final int type;

  CustomRequestMethod(int t) {
    type = t;
  }

  public int getValue() {
    return type;
  }

}
