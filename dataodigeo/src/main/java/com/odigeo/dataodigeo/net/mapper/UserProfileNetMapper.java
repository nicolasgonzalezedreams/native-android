package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import com.odigeo.tools.CheckerTool;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Irving
 * @since 15/09/2015
 */
public class UserProfileNetMapper {

  public static JSONObject mapperUserProfileToJsonObject(UserProfile userProfile)
      throws JSONException {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(JsonKeys.ID, userProfile.getUserProfileId());
    jsonObject.put(JsonKeys.IS_DEFAULT, userProfile.isDefaultTraveller());
    if (userProfile.getTitle() != null && userProfile.getTitle() != UserProfile.Title.UNKNOWN) {
      jsonObject.put(JsonKeys.TITLE, userProfile.getTitle().toString());
    }
    if (!CheckerTool.isEmptyField(userProfile.getName())) {
      jsonObject.put(JsonKeys.NAME, userProfile.getName());
    }
    if (!CheckerTool.isEmptyField(userProfile.getMiddleName())) {
      jsonObject.put(JsonKeys.MIDDLE_NAME, userProfile.getMiddleName());
    }
    if (!CheckerTool.isEmptyField(userProfile.getFirstLastName())) {
      jsonObject.put(JsonKeys.FIRST_LAST_NAME, userProfile.getFirstLastName());
    }
    if (!CheckerTool.isEmptyField(userProfile.getSecondLastName())) {
      jsonObject.put(JsonKeys.SECOND_LAST_NAME, userProfile.getSecondLastName());
    }
    if (!CheckerTool.isEmptyField(userProfile.getGender())) {
      jsonObject.put(JsonKeys.GENDER, userProfile.getGender());
    }
    jsonObject.put(JsonKeys.BIRTH_DATE, userProfile.getBirthDate());
    if (!CheckerTool.isEmptyField(userProfile.getPrefixPhoneNumber())) {
      jsonObject.put(JsonKeys.PREFIX_PHONE_NUMBER, userProfile.getPrefixPhoneNumber());
    }
    if (!CheckerTool.isEmptyField(userProfile.getPhoneNumber())) {
      jsonObject.put(JsonKeys.PHONE_NUMBER, userProfile.getPhoneNumber());
    }
    if (!CheckerTool.isEmptyField(userProfile.getPrefixAlternatePhoneNumber())) {
      jsonObject.put(JsonKeys.PREFIX_ALTERNATE_PHONE_NUMBER,
          userProfile.getPrefixAlternatePhoneNumber());
    }
    if (!CheckerTool.isEmptyField(userProfile.getAlternatePhoneNumber())) {
      jsonObject.put(JsonKeys.ALTERNATE_PHONE_NUMBER, userProfile.getAlternatePhoneNumber());
    }
    if (!CheckerTool.isEmptyField(userProfile.getMobilePhoneNumber())) {
      jsonObject.put(JsonKeys.MOBILE_PHONE_NUMBER, userProfile.getMobilePhoneNumber());
    }
    if (!CheckerTool.isEmptyField(userProfile.getNationalityCountryCode())) {
      jsonObject.put(JsonKeys.NATIONALITY_COUNTRY_CODE, userProfile.getNationalityCountryCode());
    }
    if (!CheckerTool.isEmptyField(userProfile.getCpf())) {
      jsonObject.put(JsonKeys.CPF, userProfile.getCpf());
    }
    if (userProfile.getUserAddress() != null) {
      jsonObject.put(JsonKeys.ADDRESS,
          UserAddressNetMapper.mapperUserAddressToJsonObject(userProfile.getUserAddress()));
    }
    if (userProfile.getUserIdentificationList() != null) {
      jsonObject.put(JsonKeys.IDENTIFICATION_LIST,
          getUserIdentificationListToJsonArray(userProfile.getUserIdentificationList()));
    }
    return jsonObject;
  }

  private static JSONArray getUserIdentificationListToJsonArray(
      List<UserIdentification> userIdentificationList) throws JSONException {
    JSONArray jsonArray = new JSONArray();
    if (userIdentificationList != null) {
      for (int indexUI = 0; indexUI < userIdentificationList.size(); indexUI++) {
        if (userIdentificationList.get(indexUI) != null) {
          jsonArray.put(UserIdentificationNetMapper.mapperUserIdentificationJsonObject(
              userIdentificationList.get(indexUI)));
        }
      }
    }
    return jsonArray;
  }

  public static UserProfile mapperJsonObjectToUserProfile(JSONObject profile, long travellerId)
      throws JSONException {
    if (profile != null) {
      return new UserProfile(0, profile.optLong(JsonKeys.ID),
          MapperUtil.optString(profile, JsonKeys.GENDER),
          MapperUtil.optString(profile, JsonKeys.TITLE) == null ? UserProfile.Title.UNKNOWN
              : UserProfile.Title.valueOf(MapperUtil.optString(profile, JsonKeys.TITLE)),
          MapperUtil.optString(profile, JsonKeys.NAME),
          MapperUtil.optString(profile, JsonKeys.MIDDLE_NAME),
          MapperUtil.optString(profile, JsonKeys.FIRST_LAST_NAME),
          MapperUtil.optString(profile, JsonKeys.SECOND_LAST_NAME),
          profile.optLong(JsonKeys.BIRTH_DATE),
          MapperUtil.optString(profile, JsonKeys.PREFIX_PHONE_NUMBER),
          MapperUtil.optString(profile, JsonKeys.PHONE_NUMBER),
          MapperUtil.optString(profile, JsonKeys.PREFIX_ALTERNATE_PHONE_NUMBER),
          MapperUtil.optString(profile, JsonKeys.ALTERNATE_PHONE_NUMBER),
          MapperUtil.optString(profile, JsonKeys.MOBILE_PHONE_NUMBER),
          MapperUtil.optString(profile, JsonKeys.NATIONALITY_COUNTRY_CODE),
          MapperUtil.optString(profile, JsonKeys.CPF), profile.optBoolean(JsonKeys.IS_DEFAULT), "",
          UserAddressNetMapper.mapperJSONObjectToUserAddress(
              profile.optJSONObject(JsonKeys.ADDRESS), profile.optLong(JsonKeys.ID)),
          UserIdentificationNetMapper.mapperJSONArrayToUserIdentificationList(
              profile.optJSONArray(JsonKeys.IDENTIFICATION_LIST), profile.optLong(JsonKeys.ID)),
          travellerId);
    }
    return null;
  }
}
