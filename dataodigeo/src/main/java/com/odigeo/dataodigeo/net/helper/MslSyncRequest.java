package com.odigeo.dataodigeo.net.helper;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.RequestFuture;
import com.odigeo.dataodigeo.net.mapper.utils.ResponseUtil;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;

public class MslSyncRequest<T> extends Request<T> {

  private static final int REQUEST_TIME_OUT = 20000;
  private static final float DEFAULT_BACKOFF_MULT = 1.0F;
  private static final int DEFAULT_MAX_RETRIES = 2;
  protected Map<String, String> headers;
  private Listener<T> listener;
  private String body;
  private String mBodyContentType;
  private boolean mShouldCacheHeaderResponse = false;

  public MslSyncRequest(CustomRequestMethod method, String url, RequestFuture<T> future) {
    super(method.getValue(), url, future);
    this.listener = future;
    this.headers = new HashMap<>();
    this.setRetryPolicy(
        new DefaultRetryPolicy(REQUEST_TIME_OUT, DEFAULT_MAX_RETRIES, DEFAULT_BACKOFF_MULT));
  }

  @Override protected Response<T> parseNetworkResponse(NetworkResponse response) {
    try {
      if (mShouldCacheHeaderResponse) {
        return Response.success(translateResponse(response),
            CustomCacheHelper.parseIgnoreCacheHeaders(response));
      }
      return Response.success(translateResponse(response),
          HttpHeaderParser.parseCacheHeaders(response));
    } catch (JSONException | IOException e) {
      return Response.error(new VolleyError());
    }
  }

  public T translateResponse(NetworkResponse response) throws JSONException, IOException {
    return (T) ResponseUtil.decompressResponseFromGzip(response.data);
  }

  @Override protected void deliverResponse(T response) {
    listener.onResponse(response);
  }

  @Override public void deliverError(VolleyError error) {
    super.deliverError(error);
  }

  @Override public byte[] getBody() throws AuthFailureError {
    return body == null ? null : body.getBytes();
  }

  public void setBody(String body) {
    this.body = body;
  }

  @Override public Map<String, String> getHeaders() throws AuthFailureError {
    return headers;
  }

  public void setHeaders(Map<String, String> headers) {
    this.headers = headers;
    if (headers.containsKey("Content-Type")) {
      this.mBodyContentType = headers.get("Content-Type");
      headers.remove("Content-Type");
    }
  }

  @Override public String getBodyContentType() {
    return mBodyContentType != null ? mBodyContentType : super.getBodyContentType();
  }

  public void forceCache() {
    this.mShouldCacheHeaderResponse = true;
  }
}
