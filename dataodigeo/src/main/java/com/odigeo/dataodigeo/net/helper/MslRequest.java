package com.odigeo.dataodigeo.net.helper;

import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.mapper.DataNetMapper;
import com.odigeo.dataodigeo.net.mapper.utils.ResponseUtil;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;

public class MslRequest<T> extends Request {

  private static final int REQUEST_TIME_OUT = 60000;
  private static final float DEFAULT_BACKOFF_MULT = 1.0F;
  private static final int DEFAULT_MAX_RETRIES = 0;
  private static final String LOCALWEBSERVER = "MOCKSERVER";
  protected Map<String, String> headers;
  private OnRequestDataListener<T> listener;
  private String body;
  private String mBodyContentType;
  private DataNetMapper mDataNetMapper;
  private boolean mShouldCacheHeaderResponse = false;

  public MslRequest(CustomRequestMethod method, String url, OnRequestDataListener<T> listener,
      DataNetMapper dataNetMapper) {
    super(method.getValue(), url, ResponseUtil.createErrorListener(listener));
    this.listener = listener;
    this.mDataNetMapper = dataNetMapper;
    headers = new HashMap<>();
    Log.d(LOCALWEBSERVER, "MSL:CONS1:" + url);
    this.setRetryPolicy(buildRetryPolicy());
  }

  /* Use this constructor when you want to parse the response directly on the Controller. Note that T in the OnRequestDataListener
   * should be String type */
  public MslRequest(CustomRequestMethod method, String url, OnRequestDataListener<T> listener) {
    super(method.getValue(), url, ResponseUtil.createErrorListener(listener));
    this.listener = listener;
    this.mDataNetMapper = null;
    headers = new HashMap<>();
    Log.d(LOCALWEBSERVER, "MSL:CONS2:" + url);
    this.setRetryPolicy(buildRetryPolicy());
  }

  private RetryPolicy buildRetryPolicy() {
    return new DefaultRetryPolicy(REQUEST_TIME_OUT, DEFAULT_MAX_RETRIES, DEFAULT_BACKOFF_MULT);
  }

  @Override protected Response<T> parseNetworkResponse(NetworkResponse response) {
    try {
      String str = new String(response.data, "UTF-8");
      Log.d(LOCALWEBSERVER, "MSL:DATA:" + str);
      if (mShouldCacheHeaderResponse) {
        return Response.success(translateResponse(response),
            CustomCacheHelper.parseIgnoreCacheHeaders(response));
      }
      return Response.success(translateResponse(response),
          HttpHeaderParser.parseCacheHeaders(response));
    } catch (JSONException | IOException e) {
      return Response.error(new VolleyError());
    }
  }

  public T translateResponse(NetworkResponse response) throws JSONException, IOException {
    String str = new String(response.data, "UTF-8");
    Log.d(LOCALWEBSERVER, "MSL:DATA:" + str);
    if (mDataNetMapper != null) {
      return (T) mDataNetMapper.mapper(ResponseUtil.decompressResponseFromGzip(response.data));
    } else {
      return (T) ResponseUtil.decompressResponseFromGzip(response.data);
    }
  }

  @Override protected void deliverResponse(Object response) {
    listener.onResponse((T) response);
  }

  @Override public void deliverError(VolleyError error) {
    super.deliverError(error);
  }

  @Override public byte[] getBody() throws AuthFailureError {
    Log.d(LOCALWEBSERVER, "MSL:BODY:" + body);
    return body == null ? null : body.getBytes();
  }

  public void setBody(String body) {
    this.body = body;
  }

  @Override public Map<String, String> getHeaders() throws AuthFailureError {
    return headers;
  }

  public void setHeaders(Map<String, String> headers) {
    this.headers = headers;
    if (headers.containsKey("Content-Type")) {
      this.mBodyContentType = headers.get("Content-Type");
      headers.remove("Content-Type");
    }
  }

  @Override public String getBodyContentType() {
    return mBodyContentType != null ? mBodyContentType : super.getBodyContentType();
  }

  public void forceCache() {
    this.mShouldCacheHeaderResponse = true;
  }
}
