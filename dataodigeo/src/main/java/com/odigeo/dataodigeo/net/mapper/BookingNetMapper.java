package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.booking.Baggage;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Buyer;
import com.odigeo.data.entity.booking.Carrier;
import com.odigeo.data.entity.booking.FlightStats;
import com.odigeo.data.entity.booking.Insurance;
import com.odigeo.data.entity.booking.ItineraryBooking;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.entity.booking.Traveller;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BookingNetMapper {

  private final HashMap<Long, Carrier> mCarrierMap = new HashMap<>();
  private final HashMap<Long, LocationBooking> mLocationBookingMap = new HashMap<>();
  private final HashMap<Long, ItineraryBooking> mItineraryBookingMap = new HashMap<>();

  public static ArrayList<Long> parseUserBookingIdFromJson(String userJson) throws JSONException {
    ArrayList<Long> bookingsId = new ArrayList<>();

    JSONObject jsonUser = new JSONObject(userJson);
    JSONArray bookings = jsonUser.optJSONArray("bookings");
    for (int i = 0; bookings != null && i < bookings.length(); i++) {
      bookingsId.add(bookings.optLong(i));
    }

    return bookingsId;
  }

  private ArrayList<Traveller> getTravellers(JSONObject bookingJson, Booking booking)
      throws JSONException {
    ArrayList<Traveller> travellerList = new ArrayList<>();

    JSONArray travellers = bookingJson.getJSONArray("travellers");

    for (int i = 0; i < travellers.length(); i++) {
      ArrayList<Baggage> baggageList = new ArrayList<>();

      JSONObject traveller = travellers.getJSONObject(i);

      JSONArray baggageSelections = traveller.getJSONArray("baggageSelections");

      String lastNames = MapperUtil.optString(traveller, "firstLastName");
      if (MapperUtil.optString(traveller, "secondLastName") != null) {
        lastNames += " " + MapperUtil.optString(traveller, "secondLastName");
      }

      travellerList.add(new Traveller(traveller.optLong("dateOfBirth"),
          MapperUtil.optString(traveller, "identification"),
          MapperUtil.optString(traveller, "identificationType"), lastNames,
          MapperUtil.optString(traveller, "name"), -1,
          MapperUtil.optString(traveller, "countryCodeOfResidence"),
          traveller.optLong("identificationExpirationDate"),
          MapperUtil.optString(traveller, "identificationIssueCountryCode"),
          MapperUtil.optString(traveller, "localityCodeOfResidence"),
          MapperUtil.optString(traveller, "meal"),
          MapperUtil.optString(traveller, "nationalityCountryCode"),
          MapperUtil.optString(traveller, "title"),
          MapperUtil.optString(traveller, "travellerGender"),
          MapperUtil.optString(traveller, "travellerType"), booking.getBookingId()));

      //Get baggageSelections and frequentFlayerCards
      for (int x = 0; x < baggageSelections.length(); x++) {
        JSONObject baggage = baggageSelections.getJSONObject(x);

        baggageList.add(new Baggage(-1, true, baggage.optLong("pieces"), baggage.optLong("kilos"),
            booking.getSegments().get(baggage.optInt("segmentIndex")), travellerList.get(i)));
      }

      travellerList.get(i).setBaggageList(baggageList);
    }

    return travellerList;
  }

  public Booking parseBooking(String bookingJsonString) throws JSONException {
    Booking booking = new Booking();

    JSONObject bookingJson = new JSONObject(bookingJsonString);

    //Parse locations
    parseLocations(bookingJson);

    //Parse itinerary bookings
    parseItineraryBookings(bookingJson);

    //Parse carriers
    parseCarriers(bookingJson);

    //Set booking id
    booking.setBookingId(bookingJson.optJSONObject("bookingBasicInfo").optLong("id"));

    //Set booking creation date
    booking.setTimestamp(bookingJson.optJSONObject("bookingBasicInfo").optLong("creationDate"));

    booking.setBookingStatus(MapperUtil.optString(bookingJson, "bookingStatus"));
    booking.setLocale(MapperUtil.optString(bookingJson, "locale"));

    booking.setCurrency(MapperUtil.optString(bookingJson.optJSONObject("price"), "currency"));
    booking.setPrice(bookingJson.optJSONObject("price").optDouble("amount"));

    //Set insurances
    booking.setInsurances(getInsurances(bookingJson, booking));

    //Set Buyer
    booking.setBuyer(getBuyer(bookingJson, booking));

    //Set Segments
    booking.setSegments(getSegmentWithSections(bookingJson, booking));

    //Set Travellers
    booking.setTravellers(getTravellers(bookingJson, booking));

    booking.setTravelCompanionNotification(bookingJson.optBoolean("travelCompanionNotifications"));

    booking.setSoundOfData(bookingJson.optBoolean("sod"));

    booking.setEnrichedBooking(bookingJson.optBoolean("enrichedBooking"));

    booking.setTripType(getTripType(booking));

    booking.setArrivalAirportCode(
        booking.getFirstSegment().getLastSection().getTo().getLocationCode());

    booking.setArrivalLastLeg(booking.getLastSegment().getLastSection().getArrivalDate());

    booking.setDepartureFirstLeg(booking.getFirstSegment().getFirstSection().getDepartureDate());

    return booking;
  }

  private Buyer getBuyer(JSONObject bookingJson, Booking booking) throws JSONException {
    JSONObject buyerJson = bookingJson.optJSONObject("buyer");

    Buyer buyer = new Buyer();

    buyer.setBookingId(booking.getBookingId());

    //Use opt because if no exist or if is null not throw jsonException

    buyer.setAddress(MapperUtil.optString(buyerJson, "address"));
    buyer.setName(MapperUtil.optString(buyerJson, "name"));
    buyer.setCountry(MapperUtil.optString(buyerJson, "country"));
    buyer.setEmail(MapperUtil.optString(buyerJson, "mail"));
    buyer.setCityName(MapperUtil.optString(buyerJson, "cityName"));
    buyer.setIdentification(MapperUtil.optString(buyerJson, "identification"));
    buyer.setIdentificationType(MapperUtil.optString(buyerJson, "buyerIdentificationType"));
    buyer.setCpf(MapperUtil.optString(buyerJson, "cpf"));
    buyer.setLastname(MapperUtil.optString(buyerJson, "lastNames"));
    buyer.setStateName(MapperUtil.optString(buyerJson, "stateName"));
    buyer.setZipCode(MapperUtil.optString(buyerJson, "zipCode"));

    if (buyerJson.optJSONObject("phoneNumber") != null) {
      buyer.setPhone(buyerJson.optJSONObject("phoneNumber").optString("number"));
      buyer.setPhoneCountryCode(buyerJson.optJSONObject("phoneNumber").optString("countryCode"));
    }

    if (buyerJson.optJSONObject("alternativePhoneNumber") != null) {
      buyer.setAlternativePhone(
          buyerJson.optJSONObject("alternativePhoneNumber").optString("number"));
    }

    return buyer;
  }

  private ArrayList<Segment> getSegmentWithSections(JSONObject bookingJson, Booking booking)
      throws JSONException {
    JSONArray itinerarySegmentsJson =
        bookingJson.optJSONObject("itineraryBookings").optJSONArray("itinerarySegments");

    ArrayList<Segment> segmentList = new ArrayList<>();

    for (int i = 0; i < itinerarySegmentsJson.length(); i++) {
      JSONObject segmentJson = itinerarySegmentsJson.optJSONObject(i);

      segmentList.add(
          new Segment(-1, segmentJson.optLong("duration"), 0, 0, booking.getBookingId()));

      //Get all sections
      ArrayList<Section> sectionList = new ArrayList<>();

      JSONArray sections = segmentJson.optJSONArray("sections");

      for (int x = 0; x < sections.length(); x++) {
        JSONObject sectionJsonParent = sections.optJSONObject(x);
        JSONObject sectionJson = sectionJsonParent.optJSONObject("section");

        Section section = new Section(-1, MapperUtil.optString(sectionJson, "id"),
            MapperUtil.optString(sectionJson, "aircraft"), sectionJson.optLong("arrivalDate"),
            MapperUtil.optString(sectionJson, "arrivalTerminal"),
            MapperUtil.optString(sectionJson, "baggageAllowanceQuantity"),
            MapperUtil.optString(sectionJson, "baggageAllowanceType"),
            MapperUtil.optString(sectionJson, "cabinClass"), sectionJson.optLong("departureDate"),
            MapperUtil.optString(sectionJson, "departureTerminal"), sectionJson.optLong("duration"),
            null, MapperUtil.optString(sectionJsonParent, "sectionType"),
            mCarrierMap.get(sectionJson.optLong("carrier")),
            mLocationBookingMap.get(sectionJson.optLong("from")),
            mItineraryBookingMap.get(sectionJsonParent.optLong("itineraryBookingId")),
            segmentList.get(i), mLocationBookingMap.get(sectionJson.optLong("to")));

        FlightStats flightStats = new FlightStats(MapperUtil.optString(sectionJson, "arrivalGate"),
            sectionJson.optLong("arrivalTime"),
            MapperUtil.optString(sectionJson, "arrivalTimeDelay"),
            MapperUtil.optString(sectionJson, "arrivalTimeType"),
            MapperUtil.optString(sectionJson, "baggageClaim"),
            MapperUtil.optString(sectionJson, "departureGate"),
            sectionJson.optLong("departureTime"),
            MapperUtil.optString(sectionJson, "departureTimeDelay"),
            MapperUtil.optString(sectionJson, "departureTimeType"),
            MapperUtil.optString(sectionJson, "destinationAirportName"),
            MapperUtil.optString(sectionJson, "destinationIataCode"),
            MapperUtil.optString(sectionJson, "eventReceived"),
            MapperUtil.optString(sectionJson, "flightStatus"),
            MapperUtil.optString(sectionJson, "gateChanged"),
            MapperUtil.optString(sectionJson, "operatingVendor"),
            MapperUtil.optString(sectionJson, "operatingVendorCode"),
            MapperUtil.optString(sectionJson, "operatingVendorLegNumber"),
            MapperUtil.optString(sectionJson, "updatedArrivalTerminal"),
            MapperUtil.optString(sectionJson, "updatedDepartureTerminal"), section.getSectionId());

        section.setFlightStats(flightStats);

        sectionList.add(section);
        mItineraryBookingMap.get(sectionJsonParent.optLong("itineraryBookingId"))
            .addSection(sectionList.get(x));
      }

      segmentList.get(i).setSectionsList(sectionList);
    }

    return segmentList;
  }

  private ArrayList<Insurance> getInsurances(JSONObject bookingJson, Booking booking)
      throws JSONException {
    ArrayList<Insurance> insuranceList = new ArrayList<>();

    JSONArray insuranceArray =
        bookingJson.optJSONObject("insuranceBookings").optJSONArray("bookings");

    for (int i = 0; i < insuranceArray.length(); i++) {
      JSONObject insuranceJson = insuranceArray.optJSONObject(i);

      JSONArray conditionsUrls =
          insuranceJson.optJSONObject("insurance").optJSONArray("conditionsUrls");
      String type;
      String conditionUrlPrimary;
      String conditionURLSecondary = null;

      if (conditionsUrls.length() > 1) {
        type = "EXTENDED";
        conditionUrlPrimary = MapperUtil.optString(conditionsUrls.optJSONObject(0), "url");
        conditionURLSecondary = MapperUtil.optString(conditionsUrls.optJSONObject(1), "url");
      } else {
        type = "BASIC";
        conditionUrlPrimary = MapperUtil.optString(conditionsUrls.optJSONObject(0), "url");
      }

      insuranceList.add(new Insurance(-1, -1, conditionUrlPrimary, conditionURLSecondary,
          MapperUtil.optString(insuranceJson.optJSONObject("insurance"), "description"), type,
          MapperUtil.optString(insuranceJson.optJSONObject("insurance"), "policy"),
          insuranceJson.optBoolean("selectable"),
          MapperUtil.optString(insuranceJson.optJSONObject("insurance"), "subTitle"),
          MapperUtil.optString(insuranceJson.optJSONObject("insurance"), "title"),
          insuranceJson.optDouble("totalPrice"), booking.getBookingId()));
    }

    return insuranceList;
  }

  //********************
  //   HASH PARSERS
  //********************

  private void parseCarriers(JSONObject bookingJson) throws JSONException {
    JSONArray carriersJson = bookingJson.optJSONObject("itineraryBookings")
        .optJSONObject("legend")
        .optJSONArray("carriers");

    for (int i = 0; i < carriersJson.length(); i++) {
      JSONObject carrierJson = carriersJson.optJSONObject(i);

      mCarrierMap.put(carrierJson.optLong("id"),
          new Carrier(carrierJson.optLong("id"), MapperUtil.optString(carrierJson, "code"),
              MapperUtil.optString(carrierJson, "name")));
    }
  }

  private void parseLocations(JSONObject bookingJson) throws JSONException {
    JSONArray locationsJson = bookingJson.optJSONObject("itineraryBookings")
        .optJSONObject("legend")
        .optJSONArray("locations");

    for (int i = 0; i < locationsJson.length(); i++) {
      JSONObject locationJson = locationsJson.optJSONObject(i);

      mLocationBookingMap.put(locationJson.optLong("geoNodeId"),
          new LocationBooking(locationJson.optLong("geoNodeId"),
              MapperUtil.optString(locationJson, "cityIataCode"),
              MapperUtil.optString(locationJson, "cityName"),
              MapperUtil.optString(locationJson, "countryName"),
              MapperUtil.optString(locationJson, "name"),
              MapperUtil.optString(locationJson, "countryCode"),
              MapperUtil.optString(locationJson, "timezone"),
              MapperUtil.optString(locationJson, "name"),
              MapperUtil.optString(locationJson, "type"),
              MapperUtil.optString(locationJson, "iataCode")));
    }
  }

  private void parseItineraryBookings(JSONObject bookingJson) throws JSONException {
    JSONArray itineraryBookingArray =
        bookingJson.optJSONObject("itineraryBookings").optJSONArray("bookings");

    for (int i = 0; i < itineraryBookingArray.length(); i++) {
      JSONObject itineraryBookingJson = itineraryBookingArray.optJSONObject(i);

      ItineraryBooking itineraryBooking = new ItineraryBooking(itineraryBookingJson.optLong("id"),
          MapperUtil.optString(itineraryBookingJson, "bookingStatus"),
          MapperUtil.optString(itineraryBookingJson, "pnr"), 0);

      mItineraryBookingMap.put(itineraryBookingJson.optLong("id"), itineraryBooking);
    }
  }

  private String getTripType(Booking booking) {

    String tripType = "";
    int segmentsSize = booking.getSegments().size();

    if (segmentsSize == 1) {
      tripType = Booking.TRIP_TYPE_ONE_WAY;
    } else if (segmentsSize == 2) {

      boolean isFirstSegmentDepartureEqualToLastSegmentArrival = booking.getFirstSegment()
          .getFirstSection()
          .getFrom()
          .getCityIATACode()
          .equals(booking.getLastSegment().getLastSection().getTo().getCityIATACode());

      boolean isFirstSegmentArrivalEqualToLastSegmentDeparture = booking.getFirstSegment()
          .getLastSection()
          .getTo()
          .getCityIATACode()
          .equals(booking.getLastSegment().getFirstSection().getFrom().getCityIATACode());

      if (isFirstSegmentDepartureEqualToLastSegmentArrival
          && isFirstSegmentArrivalEqualToLastSegmentDeparture) {
        tripType = Booking.TRIP_TYPE_ROUND_TRIP;
      } else {
        tripType = Booking.TRIP_TYPE_MULTI_SEGMENT;
      }
    } else {
      tripType = Booking.TRIP_TYPE_MULTI_SEGMENT;
    }
    return tripType;
  }
}
