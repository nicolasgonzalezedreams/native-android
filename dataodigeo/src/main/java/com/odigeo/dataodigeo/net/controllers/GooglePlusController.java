package com.odigeo.dataodigeo.net.controllers;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.net.error.MslError;
import com.odigeo.presenter.SocialLoginPresenter;
import com.odigeo.presenter.contracts.views.LoginSocialInterface;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.odigeo.presenter.contracts.views.LoginSocialInterface.EMAIL;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.FIRST_NAME;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.GOOGLE_SOURCE;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.IMAGE_URL;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.LAST_NAME;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.NAME;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.TOKEN;

public class GooglePlusController {

  public static final int REQUEST_RESOLVE_ERROR = 1001;
  public static final int REQUEST_SIGN_IN = 1000;
  public static final int REQUEST_EMAIL = 1002;
  public static final int USER_CANCELLED_ERROR_CODE = 4;
  public static final String PROFILE_URL =
      " https://www.googleapis.com/auth/plus.profile.emails.read";
  public static final String AUTHENTICATION_TYPE = "oauth2:";

  private Fragment fragment;
  private SocialLoginPresenter presenter;
  private GoogleApiClient mGoogleApiClient;
  private Intent mSignInIntent;
  private String mEmail = "";

  public GooglePlusController(Fragment fragment, SocialLoginPresenter presenter) {
    this.fragment = fragment;
    this.presenter = presenter;
  }

  public void loginGoogle() {
    fragment.startActivityForResult(AccountPicker.newChooseAccountIntent(null, null,
        new String[] { GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE }, true, null,
        AUTHENTICATION_TYPE + PROFILE_URL, null, null), REQUEST_EMAIL);
  }

  public void onActivityResult(int requestCode, Intent data) {
    if (requestCode == REQUEST_EMAIL) {
      if (data != null) {
        mEmail = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
        GoogleSignInOptions gso =
            new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).setAccountName(
                mEmail).requestScopes(new Scope(PROFILE_URL)).build();
        mGoogleApiClient =
            new GoogleApiClient.Builder(fragment.getActivity()).addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mSignInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        fragment.startActivityForResult(mSignInIntent, REQUEST_SIGN_IN);
      } else {
        LoginSocialInterface view = (LoginSocialInterface) fragment;
        view.hideProgress();
      }
    }
    if (requestCode == REQUEST_SIGN_IN) {
      GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
      if (result != null && result.isSuccess()) {
        Map<String, String> userData = buildUserData(result.getSignInAccount());
        onGoogleSignInSuccess(userData);
      } else {
        onGoogleSignInError(data.getIntExtra("errorCode", 0));
        disconnectOnSuccess();
      }
    }
  }

  private void onGoogleSignInSuccess(Map<String, String> userData) {
    UserProfile userProfile =
        new UserProfile(-1, -1, userData.get(FIRST_NAME), userData.get(LAST_NAME),
            UserProfile.UNKNOWN_BIRTHDATE, true, userData.get(IMAGE_URL), -1);

    buildToken(userData, userProfile);
  }

  private void onGoogleSignInError(int errorCode) {
    LoginSocialInterface view = (LoginSocialInterface) fragment;
    view.hideProgress();

    if (errorCode != USER_CANCELLED_ERROR_CODE) {
      presenter.buildSocialLoginErrorMessage(MslError.AUTH_006, GOOGLE_SOURCE);
    }
  }

  private Map<String, String> buildUserData(GoogleSignInAccount account) {
    Map<String, String> userData = new HashMap<>();
    if (account != null) {
      Uri photoUrl = account.getPhotoUrl();
      if (photoUrl != null) { //if the user hasn't profile photo
        userData.put(IMAGE_URL, photoUrl.toString());
      }
      userData.put(NAME, account.getDisplayName());
      if (account.getDisplayName() != null) {
        String[] names = account.getDisplayName().split(" ");
        if (names.length > 1) {
          userData.put(LAST_NAME, names[names.length - 1]);
        } else { //if the user hasn't last name
          userData.put(LAST_NAME, "");
        }
        userData.put(FIRST_NAME, names[0]);
      }
      userData.put(EMAIL, mEmail);
    }
    return userData;
  }

  private void buildToken(final Map<String, String> userData, final UserProfile userProfile) {
    AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
      @Override protected String doInBackground(Void... params) {
        String token = null;
        try {
          Account account = new Account(mEmail, GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
          token = GoogleAuthUtil.getToken(fragment.getActivity(), account,
              AUTHENTICATION_TYPE + PROFILE_URL);
        } catch (IOException transientEx) {
          // Network or server error, try later

        } catch (UserRecoverableAuthException e) {
          // Need Permissions
          fragment.startActivityForResult(e.getIntent(), REQUEST_SIGN_IN);
        } catch (GoogleAuthException authEx) {
          // The call is not ever expected to succeed
          // assuming you have already verified that
          // Google Play services is installed.
        }
        return token;
      }

      @Override protected void onPostExecute(String token) {
        if (token != null) {
          userData.put(TOKEN, token);
          //We have all information needed to login
          presenter.login(userData, userProfile, GOOGLE_SOURCE);
        }
      }
    };
    task.execute();
  }

  public void disconnectOnSuccess() {
    if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
      mGoogleApiClient.clearDefaultAccountAndReconnect();
      mGoogleApiClient.disconnect();
    }
  }
}
