package com.odigeo.dataodigeo.net.controllers;

import android.util.Log;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.net.controllers.UserNetControllerInterface;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.MslAuthRequest;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import com.odigeo.dataodigeo.net.mapper.DataNetMapper;
import com.odigeo.dataodigeo.net.mapper.UserNetMapper;
import com.odigeo.dataodigeo.session.SessionController;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.DELETE;
import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.GET;
import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.POST;
import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.PUT;
import static com.odigeo.dataodigeo.net.mapper.DataNetMapper.DataType.JSON;
import static com.odigeo.presenter.contracts.views.LoginSocialInterface.PASSWORD_SOURCE;

public class UserNetController implements UserNetControllerInterface {

  private RequestHelper mRequestHelper;
  private DomainHelperInterface mDomainHelper;
  private HeaderHelperInterface mHeaderHelper;
  private SessionController sessionController;
  private TokenController tokenController;

  public UserNetController(RequestHelper requestHelper, DomainHelperInterface domainHelper,
      HeaderHelperInterface headerHelper, SessionController sessionController,
      TokenController tokenController) {
    this.mRequestHelper = requestHelper;
    this.mDomainHelper = domainHelper;
    this.mHeaderHelper = headerHelper;
    this.sessionController = sessionController;
    this.tokenController = tokenController;
  }

  @Override public void login(OnRequestDataListener<User> listener, String email, String token,
      String source) {
    String header = "{\"type\":\"" + source + "\",\"login\":\"" + email + "\"," + (
        source.equals(PASSWORD_SOURCE) ? "\"password\"" : "\"token\"") + ":\"" + token + "\"}";
    loginUser(listener, header);
  }

  @Override
  public void resetPasswordUser(OnRequestDataListener<User> listener, String
      newPassword) {
    //TODO: AUTH REQUEST
    long userId = sessionController.getUserInfo().getUserId();
    String url = mDomainHelper.getUrl() + API_URL_USER + "/" + userId + "/password";

    MslAuthRequest<User> requestPasswordReset =
        new MslAuthRequest<>(PUT, url, listener, new DataNetMapper<>(User.class, JSON),
            tokenController);
    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAcceptV4(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    requestPasswordReset.setHeaders(mapHeaders);
    JSONObject resetPassJson = new JSONObject();
    try {
      resetPassJson.put("password", newPassword);
      requestPasswordReset.setBody(resetPassJson.toString());
    } catch (JSONException e) {
      System.err.println(e.getMessage());
    }
    mRequestHelper.addRequest(requestPasswordReset);
  }

  @Override public void updateUser(OnRequestDataListener<User> listener, User user) {
    //TODO: AUTH REQUEST
    String url = mDomainHelper.getUrl() + API_URL_USER + "/" + user.getUserId();
    MslAuthRequest<User> requestUpdateUser =
        new MslAuthRequest<>(PUT, url, listener, new DataNetMapper<>(User.class, JSON),
            tokenController);
    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAcceptV4(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    requestUpdateUser.setHeaders(mapHeaders);
    try {
      requestUpdateUser.setBody(UserNetMapper.mapperUserToJsonObject(user).toString());
    } catch (JSONException e) {
      System.err.println(e.getMessage());
    }
    mRequestHelper.addRequest(requestUpdateUser);
  }

  @Override public void deleteUser(OnRequestDataListener<Boolean> listener) {
    //TODO: AUTH REQUEST
    String url = mDomainHelper.getUrl()
        + API_URL_USER
        + "/0"
        + API_UR_DEVICE_TOKEN
        + sessionController.getGcmToken();
    MslAuthRequest<Boolean> requestDeleteUser =
        new MslAuthRequest<Boolean>(DELETE, url, listener, new DataNetMapper<>(Boolean.class, JSON),
            tokenController);
    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAcceptV4(mapHeaders);
    requestDeleteUser.setHeaders(mapHeaders);
    mRequestHelper.addRequest(requestDeleteUser);
  }

  @Override
  public void requestForgottenPassword(OnRequestDataListener<Boolean> listener, String email) {
    String url = mDomainHelper.getUrl() + API_URL_USER + "/recover";
    MslRequest<Boolean> requestPasswordRequest =
        new MslRequest<>(POST, url, listener, new DataNetMapper<>(Boolean.class, JSON));
    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAcceptV4(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    requestPasswordRequest.setHeaders(mapHeaders);
    JSONObject requestPassJson = new JSONObject();
    try {
      requestPassJson.put("email", email);
      requestPasswordRequest.setBody(requestPassJson.toString());
    } catch (JSONException e) {
      System.err.println(e.getMessage());
    }
    mRequestHelper.addRequest(requestPasswordRequest);
  }

  @Override public void getUser(OnAuthRequestDataListener<User> listener) {
    //TODO: AUTH REQUEST
    String url = mDomainHelper.getUrl() + API_URL_USER + "/0";
    MslAuthRequest<User> getUserRequest =
        new MslAuthRequest<>(GET, url, listener, new DataNetMapper<>(User.class, JSON),
            tokenController);
    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    mHeaderHelper.putHeaderNoneMatchParam(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAcceptV4(mapHeaders);
    getUserRequest.setHeaders(mapHeaders);
    mRequestHelper.addRequest(getUserRequest);
  }

  private void loginUser(OnRequestDataListener<User> listener, String header) {
    String url = mDomainHelper.getUrl() + API_URL_USER + "/0";
    MslRequest<User> getUserRequest =
        new MslRequest<>(GET, url, listener, new DataNetMapper<>(User.class, JSON));
    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    mHeaderHelper.putHeaderNoneMatchParam(mapHeaders);
    mHeaderHelper.createLoginAuthorizationHeader(mapHeaders, header);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAcceptV4(mapHeaders);
    getUserRequest.setHeaders(mapHeaders);
    mRequestHelper.addRequest(getUserRequest);
  }

  @Override public void registerUser(OnRequestDataListener<User> listener, final User user) {

    final MslRequest<User> registerUserRequest =
        new MslRequest<>(POST, mDomainHelper.getUrl() + API_URL_USER, listener,
            new DataNetMapper<>(User.class, JSON));

    JSONObject body = new JSONObject();
    try {
      JSONObject loginJson = new JSONObject();
      loginJson.put("password", user.getUserLogin().getPassword());
      body.put("email", user.getEmail());
      body.put("login", loginJson);
      body.put("marketingPortal", user.getMarketingPortal());
      body.put("acceptsNewsletter", user.getAcceptsNewsletter());
      body.put("acceptsPartnersInfo", user.getAcceptsPartnersInfo());
      body.put("profile", null);
      registerUserRequest.setBody(body.toString());
    } catch (JSONException e) {
      Log.e("UserNetController", e.getMessage());
    }

    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    mHeaderHelper.putAcceptV4(mapHeaders);
    registerUserRequest.setHeaders(mapHeaders);

    mRequestHelper.addRequest(registerUserRequest);
  }
}
