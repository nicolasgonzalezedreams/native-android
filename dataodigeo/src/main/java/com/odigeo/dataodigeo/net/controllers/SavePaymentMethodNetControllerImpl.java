package com.odigeo.dataodigeo.net.controllers;

import android.support.annotation.VisibleForTesting;
import com.google.gson.Gson;
import com.odigeo.data.entity.userData.InsertCreditCardRequest;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.net.controllers.SavePaymentMethodNetController;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.MslAuthRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import com.odigeo.dataodigeo.net.mapper.DataNetMapper;
import com.odigeo.dataodigeo.session.SessionController;
import java.util.HashMap;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.POST;
import static com.odigeo.dataodigeo.net.mapper.DataNetMapper.DataType.JSON;

public class SavePaymentMethodNetControllerImpl implements SavePaymentMethodNetController {

  private final static String URL_CREDITCARD = "creditcard";
  private final static String URL_USER = "user/";

  private RequestHelper requestHelper;
  private DomainHelperInterface domainHelper;
  private HeaderHelperInterface headerHelper;
  private TokenController tokenController;
  private SessionController sessionController;
  private HashMap<String, String> mapHeaders;
  private Gson gson;
  private MslAuthRequest<String> savePaymentMethodAuthRequest;

  public SavePaymentMethodNetControllerImpl(RequestHelper requestHelper,
      DomainHelperInterface domainHelper, HeaderHelperInterface headerHelper,
      TokenController tokenController, Gson gson, SessionController sessionController) {
    this.requestHelper = requestHelper;
    this.domainHelper = domainHelper;
    this.headerHelper = headerHelper;
    this.tokenController = tokenController;
    this.gson = gson;
    this.sessionController = sessionController;
  }

  @Override public void savePaymentMethod(final OnRequestDataListener<User> callback,
      InsertCreditCardRequest creditCard) {

    String userId = sessionController.getUserInfo().getUserId() + "/";
    final String url = domainHelper.getUrl() + URL_USER + userId + URL_CREDITCARD;

    savePaymentMethodAuthRequest =
        new MslAuthRequest<>(POST, url, new OnRequestDataListener<User>() {
          @Override public void onResponse(User user) {

          }

          @Override public void onError(MslError error, String message) {
            callback.onError(error, message);
          }
        }, new DataNetMapper<>(User.class, JSON), tokenController);

    mapHeaders = new HashMap<>();
    headerHelper.putDeviceId(mapHeaders);
    headerHelper.putAcceptEncoding(mapHeaders);
    headerHelper.putAcceptV4(mapHeaders);
    headerHelper.putContentType(mapHeaders);
    savePaymentMethodAuthRequest.setHeaders(mapHeaders);
    savePaymentMethodAuthRequest.setBody(gson.toJson(creditCard));
    requestHelper.addRequest(savePaymentMethodAuthRequest);
  }

  @VisibleForTesting @Override public HashMap<String, String> getMapHeaders() {
    return mapHeaders;
  }
}
