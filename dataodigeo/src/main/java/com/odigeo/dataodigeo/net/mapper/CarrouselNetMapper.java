package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.dataodigeo.constants.JsonKeys;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class CarrouselNetMapper {

  public static List<Card> jsonCarrouselMapper(String carrouselJson) throws JSONException {
    JSONObject jsonCarrousel = new JSONObject(carrouselJson);
    return CarouselCardNetMapper.mapperJsonArrayToCarouselCardList(
        jsonCarrousel.getJSONArray(JsonKeys.CARDS));
  }
}
