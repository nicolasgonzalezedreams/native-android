package com.odigeo.dataodigeo.net.controllers;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.data.entity.shoppingCart.ResumeBooking;
import com.odigeo.data.net.controllers.ResumeBookingNetController;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.HashMap;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.POST;

public class ResumeBookingNetControllerImpl implements ResumeBookingNetController {

  private static final String RESUME_BOOKING_URL = "resume";
  private static final int REQUEST_TIME_OUT = 5 * 1000 * 60;

  private RequestHelper mRequestHelper;
  private DomainHelperInterface mDomainHelper;
  private HeaderHelperInterface mHeaderHelper;
  private Gson mGson;

  public ResumeBookingNetControllerImpl(RequestHelper requestHelper,
      DomainHelperInterface domainHelper, HeaderHelperInterface headerHelper, Gson gson) {
    mRequestHelper = requestHelper;
    mDomainHelper = domainHelper;
    mHeaderHelper = headerHelper;
    mGson = gson;
  }

  @Override public void resumeBooking(final OnRequestDataListener<BookingResponse> listener,
      ResumeBooking resumeBooking) {
    String url = mDomainHelper.getUrl() + RESUME_BOOKING_URL;
    MslRequest<String> resumeBookingRequest =
        new MslRequest<>(POST, url, new OnRequestDataListener<String>() {
          @Override public void onResponse(String bookingResponseMSL) {
            BookingResponse bookingResponse =
                mGson.fromJson(bookingResponseMSL, BookingResponse.class);
            listener.onResponse(bookingResponse);
          }

          @Override public void onError(MslError error, String message) {
            listener.onError(error, message);
          }
        });

    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAccept(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    resumeBookingRequest.setHeaders(mapHeaders);
    resumeBookingRequest.setBody(mGson.toJson(resumeBooking));
    resumeBookingRequest.setRetryPolicy(
        new DefaultRetryPolicy(REQUEST_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    mRequestHelper.addRequest(resumeBookingRequest);
  }
}
