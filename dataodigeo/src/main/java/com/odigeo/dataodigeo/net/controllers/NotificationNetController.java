package com.odigeo.dataodigeo.net.controllers;

import com.odigeo.data.net.controllers.NotificationNetControllerInterface;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnAuthRequestDataListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.MslAuthRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import com.odigeo.dataodigeo.net.mapper.DataNetMapper;
import com.odigeo.dataodigeo.session.SessionController;
import java.util.HashMap;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.DELETE;
import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.PUT;
import static com.odigeo.dataodigeo.net.mapper.DataNetMapper.DataType.JSON;

/**
 * Created by matias on 1/16/2017.
 */

public class NotificationNetController implements NotificationNetControllerInterface {

  private RequestHelper mRequestHelper;
  private DomainHelperInterface mDomainHelper;
  private HeaderHelperInterface mHeaderHelper;
  private SessionController mSessionController;
  private TokenController mTokenController;

  public NotificationNetController(RequestHelper requestHelper, DomainHelperInterface domainHelper,
      HeaderHelperInterface headerHelper, SessionController sessionController,
      TokenController tokenController) {
    this.mRequestHelper = requestHelper;
    this.mDomainHelper = domainHelper;
    this.mHeaderHelper = headerHelper;
    this.mSessionController = sessionController;
    this.mTokenController = tokenController;
  }

  @Override
  public void disableNotifications(OnAuthRequestDataListener<Boolean> listener, String token) {
    //TODO: AUTH REQUEST
    String url = mDomainHelper.getUrl() + API_URL_NOTIFICATIONS + "/" + token;
    MslAuthRequest<Boolean> requestDisableNotifications =
        new MslAuthRequest<>(DELETE, url, listener, new DataNetMapper<>(Boolean.class, JSON),
            mTokenController);
    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    mHeaderHelper.putAcceptV4(mapHeaders);
    mHeaderHelper.putHeaderNoneMatchParam(mapHeaders);
    requestDisableNotifications.setHeaders(mapHeaders);
    mRequestHelper.addRequest(requestDisableNotifications);
  }

  @Override public void enableNotifications(OnRequestDataListener<Boolean> listener, String token) {
    //TODO: AUTH REQUEST
    if (mSessionController.getCredentials() != null) {
      String url = mDomainHelper.getUrl() + API_URL_NOTIFICATIONS + "/" + token;
      MslAuthRequest<Boolean> requestEnableNotifications =
          new MslAuthRequest<>(PUT, url, listener, new DataNetMapper<>(Boolean.class, JSON),
              mTokenController);
      HashMap<String, String> mapHeaders = new HashMap<>();
      mHeaderHelper.putDeviceId(mapHeaders);
      mHeaderHelper.putAcceptEncoding(mapHeaders);
      mHeaderHelper.putContentType(mapHeaders);
      mHeaderHelper.putAcceptV4(mapHeaders);
      mHeaderHelper.putHeaderNoneMatchParam(mapHeaders);
      requestEnableNotifications.setHeaders(mapHeaders);
      mRequestHelper.addRequest(requestEnableNotifications);
    }
  }
}
