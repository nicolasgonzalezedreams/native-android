package com.odigeo.dataodigeo.net.mapper;

import android.support.annotation.NonNull;
import android.util.Log;
import com.odigeo.data.entity.net.NetCountry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 12/02/16.
 */
public class NetCountryMapper {

  public static final String COUNTRIES = "countries";
  public static final String GEO_NODE_ID = "geoNodeId";
  public static final String COUNTRY_CODE = "countryCode";
  public static final String PHONE_PREFIX = "phonePrefix";
  public static final String NAME = "name";
  public static final String TEXTS = "texts";
  public static final String[] LANGUAGES = {
      "de", "it", "el", "tr", "pt", "fr", "en", "ru", "es", "nl", "ja", "zh", "no", "sv", "fi",
      "is", "pl", "da"
  };

  public List<NetCountry> parseNetCountries(String json) {
    List<NetCountry> netCountries = new ArrayList<>();
    try {
      JSONArray countries = new JSONObject(json).getJSONArray(COUNTRIES);
      netCountries = extractNetCountries(countries);
    } catch (JSONException e) {
      Log.e(COUNTRIES, e.getMessage(), e);
    }
    return netCountries;
  }

  private List<NetCountry> extractNetCountries(JSONArray countries) throws JSONException {
    int size = countries.length();
    int limit = size / 2;
    int index;
    int inverseIndex;
    boolean isSymmetric = size % 2 == 0;
    JSONObject country;
    JSONObject countryEnd;
    List<NetCountry> netCountries = new ArrayList<>(size);
    for (index = 0; index < limit; index++) {
      inverseIndex = size - index - 1;
      country = countries.getJSONObject(index);
      countryEnd = countries.getJSONObject(inverseIndex);
      NetCountry netCountry = extractNetCountry(country);
      NetCountry netCountryEnd = extractNetCountry(countryEnd);
      netCountries.add(index, netCountry);
      netCountries.add(netCountries.size() - index, netCountryEnd);
    }
    if (!isSymmetric) {
      country = countries.getJSONObject(index);
      NetCountry netCountry = extractNetCountry(country);
      netCountries.add(index, netCountry);
    }
    return netCountries;
  }

  @NonNull private NetCountry extractNetCountry(JSONObject country) throws JSONException {
    NetCountry netCountry = new NetCountry();
    netCountry.setGeoNodeId(country.getInt(GEO_NODE_ID));
    netCountry.setCountryCode(country.getString(COUNTRY_CODE));
    if (!country.isNull(PHONE_PREFIX)) {
      netCountry.setPhonePrefix(country.getString(PHONE_PREFIX));
    }
    JSONObject texts = country.getJSONObject(NAME).getJSONObject(TEXTS);
    Map<String, String> namesMap = new HashMap<>();
    for (String language : LANGUAGES) {
      if (!texts.isNull(language)) {
        namesMap.put(language, texts.getString(language));
      }
    }
    netCountry.setNames(namesMap);
    return netCountry;
  }
}
