package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.userData.Membership;
import com.odigeo.dataodigeo.constants.JsonKeys;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MembershipNetMapper {

  public static List<Membership> mapperJSONArrayToMembershipList(JSONArray jsonArray)
      throws JSONException {
    ArrayList<Membership> membershipArrayList = new ArrayList<>();
    for (int i = 0; i < jsonArray.length(); ++i) {
      membershipArrayList.add(mapperMembershipFromJson(jsonArray.getJSONObject(i)));
    }
    return membershipArrayList;
  }

  private static Membership mapperMembershipFromJson(JSONObject jsonMembership) {
    long memberId = jsonMembership.optLong(JsonKeys.MEMBER_ID);
    String memberName = jsonMembership.optString(JsonKeys.MEMBER_NAME);
    String memberLastNames = jsonMembership.optString(JsonKeys.MEMBER_LASTNAMES);
    String website = jsonMembership.optString(JsonKeys.MEMBER_WEBSITE);

    Membership membership = new Membership();
    membership.setMemberId(memberId);
    membership.setFirstName(memberName);
    membership.setLastNames(memberLastNames);
    membership.setWebsite(website);

    return membership;
  }

  public static JSONArray mapperMembershipListToJsonArray(List<Membership> memberships)
      throws JSONException {
    JSONArray membershipJsonArray = new JSONArray();

    for (Membership membership : memberships) {
      membershipJsonArray.put(mapperJsonFromMembership(membership));
    }

    return membershipJsonArray;
  }

  private static JSONObject mapperJsonFromMembership(Membership membership) throws JSONException {
    JSONObject membershipInfo = new JSONObject();
    membershipInfo.put(JsonKeys.MEMBER_WEBSITE, membership.getWebsite());
    membershipInfo.put(JsonKeys.MEMBER_ID, membership.getMemberId());
    membershipInfo.put(JsonKeys.MEMBER_NAME, membership.getFirstName());
    membershipInfo.put(JsonKeys.MEMBER_LASTNAMES, membership.getLastNames());
    return membershipInfo;
  }
}
