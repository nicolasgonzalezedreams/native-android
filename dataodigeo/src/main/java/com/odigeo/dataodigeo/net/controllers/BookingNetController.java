package com.odigeo.dataodigeo.net.controllers;

import android.util.Log;
import com.google.gson.Gson;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.BookingSummary;
import com.odigeo.data.entity.booking.BookingSummaryRequestItem;
import com.odigeo.data.net.controllers.BookingNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.MslAuthRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import com.odigeo.dataodigeo.net.mapper.BookingNetMapper;
import com.odigeo.dataodigeo.net.mapper.BookingSummaryNetMapper;
import com.odigeo.dataodigeo.session.SessionController;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.GET;
import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.PUT;

public class BookingNetController implements BookingNetControllerInterface {

  private static final String API_URL_BOOKING = "booking?";
  private static final String API_URL_BOOKING_NOTIFICATIONS = "booking";
  private static final String API_URL_BOOKING_STATUS = "bookingsStatus?";
  private static final String API_URL_BOOKING_EMAIL = "email=";
  private static final String API_URL_BOOKING_ID = "&bookingId=";
  private static final String API_URL_BOOKING_ITEMS = "items=";

  private RequestHelper mRequestHelper;
  private DomainHelperInterface mDomainHelper;
  private HeaderHelperInterface mHeaderHelper;
  private SessionController mSessionController;
  private TokenController mTokenController;

  private BookingNetMapper mBookingNetMapper;
  private BookingSummaryNetMapper mBookingSummaryNetMapper;

  public BookingNetController(RequestHelper requestHelper, DomainHelperInterface domainHelper,
      HeaderHelperInterface headerHelper, SessionController sessionController,
      TokenController tokenController) {
    mRequestHelper = requestHelper;
    mDomainHelper = domainHelper;
    mHeaderHelper = headerHelper;
    mSessionController = sessionController;
    mBookingSummaryNetMapper = new BookingSummaryNetMapper();
    mBookingNetMapper = new BookingNetMapper();
    mTokenController = tokenController;
  }

  @Override public void getBookingsId(final OnRequestDataListListener<Long> listener) {
    //TODO: AUTH REQUEST
    String url = mDomainHelper.getUrl() + API_URL_USER + "/0";
    MslAuthRequest<String> getUserRequest =
        new MslAuthRequest<>(GET, url, new OnRequestDataListener<String>() {
          @Override public void onResponse(String response) {
            try {
              listener.onResponse(BookingNetMapper.parseUserBookingIdFromJson(response));
            } catch (JSONException e) {
              onError(MslError.UNK_001, e.getMessage());
            }
          }

          @Override public void onError(MslError error, String message) {
            listener.onError(error, message);
          }
        }, mTokenController);

    Map<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    mHeaderHelper.putHeaderNoneMatchParam(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAcceptV4(mapHeaders);
    getUserRequest.setHeaders(mapHeaders);
    mRequestHelper.addRequest(getUserRequest);
  }

  @Override
  public void getBookingByEmail(final OnRequestDataListener<Booking> listener, String email,
      String id) {
    //TODO: AUTH REQUEST
    String url = mDomainHelper.getUrl()
        + API_URL_BOOKING
        + API_URL_BOOKING_EMAIL
        + email
        + API_URL_BOOKING_ID
        + id;

    final MslAuthRequest<String> getUserRequest =
        new MslAuthRequest<>(GET, url, new OnRequestDataListener<String>() {
          @Override public void onResponse(String response) {
            try {
              listener.onResponse(mBookingNetMapper.parseBooking(response));
            } catch (JSONException e) {
              e.printStackTrace();
              listener.onResponse(null);
            }
          }

          @Override public void onError(MslError error, String message) {
            listener.onResponse(null);
          }
        }, mTokenController);

    HashMap<String, String> mapHeaders = new HashMap<>();

    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    mHeaderHelper.putHeaderNoneMatchParam(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAccept(mapHeaders);
    getUserRequest.setHeaders(mapHeaders);
    getUserRequest.forceCache();
    mRequestHelper.addRequest(getUserRequest);
  }

  @Override public void getBookingById(final OnRequestDataListener<Booking> listener, String id) {
    if (mSessionController.getCredentials() != null) {
      getBookingByEmail(listener, mSessionController.getCredentials().getUser(), id);
    }
  }

  @Override public void getBookingStatus(final OnRequestDataListener<List<BookingSummary>> listener,
      List<BookingSummaryRequestItem> items) {
    //TODO: AUTH REQUEST
    Gson gson = new Gson();
    String bookingsJson = gson.toJson(items);
    try {
      bookingsJson = URLEncoder.encode(bookingsJson, "UTF-8");
      String url =
          mDomainHelper.getUrl() + API_URL_BOOKING_STATUS + API_URL_BOOKING_ITEMS + bookingsJson;

      MslAuthRequest<String> getUserRequest =
          new MslAuthRequest<>(GET, url, new OnRequestDataListener<String>() {
            @Override public void onResponse(String response) {
              Log.d("BookingStatus", "BookingNetController.getBookingStatus.onResponse");
              try {
                listener.onResponse(mBookingSummaryNetMapper.parseBookingStatus(response));
              } catch (JSONException e) {
                e.printStackTrace();
                listener.onError(MslError.UNK_001, e.getMessage());
              }
            }

            @Override public void onError(MslError error, String message) {
              Log.e("BookingStatus",
                  "BookingNetController.getBookingStatus.onPromotionCardsError: " + message);
              listener.onError(error, message);
            }
          }, mTokenController);

      HashMap<String, String> mapHeaders = new HashMap<>();

      mHeaderHelper.putDeviceId(mapHeaders);
      mHeaderHelper.putContentType(mapHeaders);
      mHeaderHelper.putHeaderNoneMatchParam(mapHeaders);
      mHeaderHelper.putAcceptEncoding(mapHeaders);
      mHeaderHelper.putAccept(mapHeaders);
      getUserRequest.setHeaders(mapHeaders);

      Log.d("BookingStatus", url);
      Log.d("BookingStatus", Arrays.toString(mapHeaders.entrySet().toArray()));

      mRequestHelper.addRequest(getUserRequest);
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      listener.onError(MslError.UNK_001, e.getMessage());
    }
  }

  @Override
  public void changeBookingNotificationStatus(final OnRequestDataListener<Boolean> listener,
      String id, boolean status) {
    //TODO: AUTH REQUEST
    String url = mDomainHelper.getUrl() + API_URL_BOOKING_NOTIFICATIONS + "/" + id;

    MslAuthRequest<String> bookingNotificationStatusRequest =
        new MslAuthRequest<>(PUT, url, new OnRequestDataListener<String>() {
          @Override public void onResponse(String response) {
            listener.onResponse(true);
          }

          @Override public void onError(MslError error, String message) {
            listener.onResponse(false);
          }
        }, mTokenController);

    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    bookingNotificationStatusRequest.setHeaders(mapHeaders);
    try {
      bookingNotificationStatusRequest.setBody(
          new JSONObject().put("travelCompanionNotifications", status).toString());
    } catch (JSONException e) {
      System.err.println(e.getMessage());
    }
    mRequestHelper.addRequest(bookingNotificationStatusRequest);
  }
}