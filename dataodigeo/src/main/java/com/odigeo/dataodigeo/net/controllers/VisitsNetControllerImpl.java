package com.odigeo.dataodigeo.net.controllers;

import android.support.annotation.VisibleForTesting;
import com.google.gson.Gson;
import com.odigeo.data.entity.session.Visit;
import com.odigeo.data.entity.session.request.VisitRequest;
import com.odigeo.data.net.controllers.VisitsNetController;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.HashMap;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.POST;

public class VisitsNetControllerImpl implements VisitsNetController {

  private RequestHelper requestHelper;
  private DomainHelperInterface domainHelper;
  private HeaderHelperInterface headerHelper;
  private HashMap<String, String> mapHeaders;
  private Gson gson;

  public VisitsNetControllerImpl(RequestHelper requestHelper, DomainHelperInterface domainHelper,
      HeaderHelperInterface headerHelper, Gson gson) {
    this.requestHelper = requestHelper;
    this.domainHelper = domainHelper;
    this.headerHelper = headerHelper;
    this.gson = gson;
  }

  @Override
  public void getVisits(final OnRequestDataListener<Visit> listener, VisitRequest visitRequest) {
    String url = domainHelper.getUrl() + API_URL_VISIT;
    MslRequest<String> mslVisitRequest =
        new MslRequest<>(POST, url, new OnRequestDataListener<String>() {
          @Override public void onResponse(String visitResponse) {
            Visit visit = gson.fromJson(visitResponse, Visit.class);
            listener.onResponse(visit);
          }

          @Override public void onError(MslError error, String message) {
            listener.onError(error, message);
          }
        });

    mapHeaders = new HashMap<>();
    headerHelper.putDeviceId(mapHeaders);
    headerHelper.putAcceptEncoding(mapHeaders);
    headerHelper.putAcceptV4(mapHeaders);
    headerHelper.putContentType(mapHeaders);
    mslVisitRequest.setHeaders(mapHeaders);
    mslVisitRequest.setBody(gson.toJson(visitRequest));
    requestHelper.addRequest(mslVisitRequest);
  }

  @VisibleForTesting @Override public HashMap<String, String> getMapHeaders() {
    return mapHeaders;
  }
}
