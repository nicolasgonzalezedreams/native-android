package com.odigeo.dataodigeo.net.controllers;

import android.support.annotation.VisibleForTesting;
import com.google.gson.Gson;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.request.PersonalInfoRequest;
import com.odigeo.data.net.controllers.AddPassengerNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.HashMap;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.POST;

public class AddPassengerNetController implements AddPassengerNetControllerInterface {

  private static final String URL_ADD_PASSENGER = "addPassenger";
  private HashMap<String, String> mMapHeaders;
  private final RequestHelper mRequestHelper;
  private final DomainHelperInterface mDomainHelper;
  private final HeaderHelperInterface mHeaderHelper;
  private final Gson mGson;

  public AddPassengerNetController(RequestHelper requestHelper, DomainHelperInterface domainHelper,
      HeaderHelperInterface headerHelper, Gson gson) {
    mRequestHelper = requestHelper;
    mDomainHelper = domainHelper;
    mHeaderHelper = headerHelper;
    mGson = gson;
  }

  @Override public void addPassengerToShoppingCart(
      final OnRequestDataListener<CreateShoppingCartResponse> listener,
      PersonalInfoRequest personalInfoRequest) {
    String url = mDomainHelper.getUrl() + URL_ADD_PASSENGER;
    MslRequest<String> addPassengerRequest =
        new MslRequest<>(POST, url, new OnRequestDataListener<String>() {
          @Override public void onResponse(String shoppingCartResponse) {
            CreateShoppingCartResponse createShoppingCartResponse =
                mGson.fromJson(shoppingCartResponse, CreateShoppingCartResponse.class);
            listener.onResponse(createShoppingCartResponse);
          }

          @Override public void onError(MslError error, String message) {
            listener.onError(error, message);
          }
        });

    mMapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mMapHeaders);
    mHeaderHelper.putAcceptEncoding(mMapHeaders);
    mHeaderHelper.putAccept(mMapHeaders);
    mHeaderHelper.putContentType(mMapHeaders);
    addPassengerRequest.setHeaders(mMapHeaders);
    addPassengerRequest.setBody(mGson.toJson(personalInfoRequest));
    mRequestHelper.addRequest(addPassengerRequest);
  }

  @VisibleForTesting @Override public HashMap<String, String> getMapHeaders() {
    return mMapHeaders;
  }
}
