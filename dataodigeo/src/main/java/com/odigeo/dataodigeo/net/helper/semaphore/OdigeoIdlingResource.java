package com.odigeo.dataodigeo.net.helper.semaphore;

import android.support.annotation.NonNull;
import android.support.test.espresso.IdlingResource;
import android.util.Log;
import com.android.volley.RequestQueue;
import java.lang.reflect.Field;
import java.util.Set;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 12/12/16
 */

class OdigeoIdlingResource implements IdlingResource {
  private static final String CURRENT_REQUESTS_FIELD = "mCurrentRequests";
  private static final String MESSAGE_VOLLEY = "Volley queue: ";
  private static final String DONE_MESSAGE = "Finishing idle";
  private static final String MESSAGE_RUNNING = "Is idling now with: ";
  private static final String MESSAGE_INCREMENT = "Manual increment: ";
  private static final String MESSAGE_DECREMENT = "Manual decrement: ";
  private static final String MESSAGE_RESET = "Reset: ";
  private volatile ResourceCallback resourceCallback;
  private Logger logger;
  private RequestQueue requestQueue;
  private Field currentRequest;
  private int count = 0;
  private boolean wasIdling = false;

  OdigeoIdlingResource(@NonNull RequestQueue requestQueue, boolean verbose) {
    init(requestQueue, verbose);
  }

  private void init(@NonNull RequestQueue requestQueue, boolean verbose) {
    this.requestQueue = requestQueue;
    this.logger = new Logger(verbose);
    try {
      currentRequest = RequestQueue.class.getDeclaredField(CURRENT_REQUESTS_FIELD);
      currentRequest.setAccessible(true);
    } catch (NoSuchFieldException e) {
      Log.e(getClass().getSimpleName(), e.getMessage(), e);
    }
  }

  @Override public String getName() {
    return getClass().getSimpleName();
  }

  @Override public boolean isIdleNow() {
    try {
      int requests = ((Set) currentRequest.get(requestQueue)).size();
      if (requests > 0) {
        logger.log(MESSAGE_VOLLEY + requests);
      }
      boolean isIdling = count + requests == 0;
      if (isIdling) {
        if (wasIdling) {
          wasIdling = false;
          resourceCallback.onTransitionToIdle();
          logger.log(DONE_MESSAGE);
        }
      } else {
        wasIdling = true;
        logger.log(MESSAGE_RUNNING + count);
      }
      return isIdling;
    } catch (IllegalAccessException e) {
      Log.e(getClass().getSimpleName(), e.getMessage(), e);
      return true;
    }
  }

  @Override public void registerIdleTransitionCallback(ResourceCallback callback) {
    resourceCallback = callback;
  }

  synchronized void increment() {
    count++;
    logger.log(MESSAGE_INCREMENT + count);
  }

  synchronized void decrement() {
    if (count > 0) {
      count--;
    }
    logger.log(MESSAGE_DECREMENT + count);
  }

  synchronized void reset() {
    count = 0;
    logger.log(MESSAGE_RESET + count);
  }

  private class Logger {
    private boolean verbose = false;

    Logger(boolean verbose) {
      this.verbose = verbose;
    }

    void log(@NonNull String message) {
      if (verbose) {
        Log.d(OdigeoIdlingResource.class.getSimpleName(), message);
      }
    }
  }
}
