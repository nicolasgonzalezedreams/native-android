package com.odigeo.dataodigeo.net.controllers;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartRequestModel;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.net.controllers.ShoppingCartNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.POST;

public class ShoppingCartNetController implements ShoppingCartNetControllerInterface {

  private static final int INITIAL_TIME_OUTS = 45000;
  private static final String API_URL_SHOPPING_CART = "createShoppingCart";
  private static final String API_URL_AVAILABLE_PRODUCTS = "getAvailableProducts";
  private final RequestHelper mRequestHelper;
  private final DomainHelperInterface mDomainHelper;
  private final HeaderHelperInterface mHeaderHelper;
  private final Gson mGson;

  public ShoppingCartNetController(RequestHelper requestHelper, DomainHelperInterface domainHelper,
      HeaderHelperInterface headerHelper, Gson gson) {
    mRequestHelper = requestHelper;
    mDomainHelper = domainHelper;
    mHeaderHelper = headerHelper;
    mGson = gson;
  }

  @Override
  public void getShoppingCart(CreateShoppingCartRequestModel createShoppingCartRequestModel,
      final OnRequestDataListener<CreateShoppingCartResponse> listener) {
    String url = mDomainHelper.getUrl() + API_URL_SHOPPING_CART;
    MslRequest<String> createShoppingCartRequest =
        new MslRequest<>(POST, url, new OnRequestDataListener<String>() {
          @Override public void onResponse(String shoppingCartResponse) {
            CreateShoppingCartResponse createShoppingCartResponse =
                mGson.fromJson(shoppingCartResponse, CreateShoppingCartResponse.class);
            listener.onResponse(createShoppingCartResponse);
          }

          @Override public void onError(MslError error, String message) {
            listener.onError(error, message);
          }
        });

    Map<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    mHeaderHelper.putAccept(mapHeaders);

    createShoppingCartRequest.setHeaders(mapHeaders);
    createShoppingCartRequest.setRetryPolicy(
        new DefaultRetryPolicy(INITIAL_TIME_OUTS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    createShoppingCartRequest.setBody(new Gson().toJson(createShoppingCartRequestModel));
    mRequestHelper.addRequest(createShoppingCartRequest);
  }

  @Override
  public void getAvailableProducts(long bookingId, OnRequestDataListener<String> listener) {
    String url = mDomainHelper.getUrl() + API_URL_AVAILABLE_PRODUCTS;
    MslRequest getShoppingCartRequest = new MslRequest<>(POST, url, listener);
    Map<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    mHeaderHelper.putAccept(mapHeaders);

    getShoppingCartRequest.setHeaders(mapHeaders);
    getShoppingCartRequest.setRetryPolicy(
        new DefaultRetryPolicy(INITIAL_TIME_OUTS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    try {
      getShoppingCartRequest.setBody(new JSONObject().put("bookingId", bookingId).toString());
    } catch (JSONException e) {
      e.printStackTrace();
    }
    mRequestHelper.addRequest(getShoppingCartRequest);
  }
}
