package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.userData.UserDevice;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by matias.dirusso on 9/21/2015.
 */
public class UserDeviceNetMapper {

  public static UserDevice mapperJSONObjectToUserDevice(JSONObject device, long userId) {
    if (device != null) {
      return new UserDevice(0, device.optLong(JsonKeys.ID),
          MapperUtil.optString(device, JsonKeys.DEVICE_NAME),
          MapperUtil.optString(device, JsonKeys.DEVICE_ID), device.optBoolean(JsonKeys.IS_PRIMARY),
          MapperUtil.optString(device, JsonKeys.ALIAS), userId);
    }
    return null;
  }

  public static List<UserDevice> mapperJSONArrayToUserDeviceList(JSONArray devices, long userId)
      throws JSONException {

    if (devices != null) {
      List<UserDevice> devicesList = new ArrayList<>();
      for (int indexD = 0; indexD < devices.length(); indexD++) {
        JSONObject device = null;
        device = devices.optJSONObject(indexD);
        if (device != null) {
          devicesList.add(mapperJSONObjectToUserDevice(device, userId));
        }
      }
      return devicesList;
    }
    return null;
  }

  public static JSONObject mapperUserDeviceToJSONObject(UserDevice userDevice)
      throws JSONException {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(JsonKeys.ID, userDevice.getUserDeviceId());
    if (userDevice.getDeviceName() != null) {
      jsonObject.put(JsonKeys.DEVICE_NAME, userDevice.getDeviceName());
    }
    if (userDevice.getDeviceId() != null) {
      jsonObject.put(JsonKeys.DEVICE_ID, userDevice.getDeviceId());
    }
    jsonObject.put(JsonKeys.IS_PRIMARY, userDevice.getIsPrimary());
    if (userDevice.getAlias() != null) {
      jsonObject.put(JsonKeys.ALIAS, userDevice.getAlias());
    }
    return jsonObject;
  }

  public static JSONArray getUserDevicesListToJson(List<UserDevice> userDevicesList)
      throws JSONException {
    JSONArray jsonArray = new JSONArray();
    if (userDevicesList != null) {
      for (int indexUT = 0; indexUT < userDevicesList.size(); indexUT++) {
        if (userDevicesList.get(indexUT) != null) {
          jsonArray.put(mapperUserDeviceToJSONObject(userDevicesList.get(indexUT)));
        }
      }
    }
    return jsonArray;
  }
}
