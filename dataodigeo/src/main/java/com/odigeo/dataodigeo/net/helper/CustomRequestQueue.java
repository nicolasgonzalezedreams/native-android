package com.odigeo.dataodigeo.net.helper;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import java.io.File;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

public class CustomRequestQueue extends RequestQueue {

  private static final String IMAGE_CACHE_DIR = "images";
  private static final double DISK_CACHE_PERCENT_USE = 0.025;
  private static final int DISK_CACHE_MIN_SIZE = 1024 * 1024 * 20;

  public CustomRequestQueue(Cache cache, Network network) {
    super(cache, network);
  }

  public static CustomRequestQueue createCustomRequestQueue(Context context) {
    CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
    return new CustomRequestQueue(generateCache(context), new BasicNetwork(new HurlStack()));
  }

  private static Cache generateCache(Context context) {
    String cachePath;

    try {
      cachePath = context.getExternalCacheDir().toString();
    } catch (NullPointerException e) {
      cachePath = context.getCacheDir().toString();
    }

    File cacheDir = new File(cachePath, IMAGE_CACHE_DIR);
    cacheDir.mkdirs();

    return new DiskBasedCache(cacheDir, calculateDiscCacheSize());
  }

  private static int calculateDiscCacheSize() {
    StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
    long blockSize;
    long availableBlocks;

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
      blockSize = stat.getBlockSizeLong();
      availableBlocks = stat.getAvailableBlocksLong();
    } else {
      blockSize = stat.getBlockSize();
      availableBlocks = stat.getAvailableBlocks();
    }

    long totalAvailableBytes = blockSize * availableBlocks;
    int total = (int) (totalAvailableBytes * DISK_CACHE_PERCENT_USE);
    if (total < DISK_CACHE_MIN_SIZE && DISK_CACHE_MIN_SIZE > totalAvailableBytes) {
      return DISK_CACHE_MIN_SIZE;
    } else {
      return total;
    }
  }
}
