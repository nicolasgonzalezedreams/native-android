package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.booking.SearchSegment;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by matias.dirusso on 25/7/2016.
 */
public class SearchSegmentNetMapper {

  public static JSONObject mapperSearchSegmentToJsonObject(SearchSegment searchSegment) {
    JSONObject jsonObject = new JSONObject();
    try {
      jsonObject.put(JsonKeys.ID, searchSegment.getSearchSegmentId());
      jsonObject.put(JsonKeys.ORIGIN_IATA_CODE, searchSegment.getOriginIATACode());
      jsonObject.put(JsonKeys.DEPARTURE_DATE, searchSegment.getDepartureDate());
      jsonObject.put(JsonKeys.DESTINATION_IATA_CODE, searchSegment.getDestinationIATACode());
      jsonObject.put(JsonKeys.SEGMENT_ORDER, searchSegment.getSegmentOrder());
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return jsonObject;
  }

  public static SearchSegment mapperJsonObjectToSearchSegment(JSONObject searchSegment,
      long storedSearchId) {
    return new SearchSegment(0, searchSegment.optLong(JsonKeys.ID),
        MapperUtil.optString(searchSegment, JsonKeys.ORIGIN_IATA_CODE),
        MapperUtil.optString(searchSegment, JsonKeys.DESTINATION_IATA_CODE),
        searchSegment.optLong(JsonKeys.DEPARTURE_DATE),
        searchSegment.optLong(JsonKeys.SEGMENT_ORDER), storedSearchId);
  }

  public static List<SearchSegment> mapperJsonArrayToSearchSegmentList(
      JSONArray jsonArraySearchSegments, long storedSearchId) throws JSONException {
    if (jsonArraySearchSegments != null) {
      List<SearchSegment> searchSegmentList = new ArrayList<>();
      for (int indexS = 0; indexS < jsonArraySearchSegments.length(); indexS++) {
        JSONObject searchSegment = jsonArraySearchSegments.optJSONObject(indexS);
        if (searchSegment != null) {
          searchSegmentList.add(
              SearchSegmentNetMapper.mapperJsonObjectToSearchSegment(searchSegment,
                  storedSearchId));
        }
      }
      return searchSegmentList;
    }
    return null;
  }

  public static JSONArray mapperSearchSegmentListToJson(List<SearchSegment> searchSegmentList) {
    JSONArray jsonArray = new JSONArray();
    if (searchSegmentList != null) {
      for (int indexS = 0; indexS < searchSegmentList.size(); indexS++) {
        if (searchSegmentList.get(indexS) != null) {
          jsonArray.put(mapperSearchSegmentToJsonObject(searchSegmentList.get(indexS)));
        }
      }
    }
    return jsonArray;
  }
}
