package com.odigeo.dataodigeo.net.mapper;

import android.util.Log;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by matias.dirusso on 22/7/2016.
 */
public class StoredSearchesNetMapper {

  public static JSONObject mapperStoredSearchToJsonObject(StoredSearch storedSearch) {
    JSONObject jsonObject = new JSONObject();
    try {
      jsonObject.put(JsonKeys.ID, storedSearch.getStoredSearchId());
      jsonObject.put(JsonKeys.NUM_ADULTS, storedSearch.getNumAdults());
      jsonObject.put(JsonKeys.NUM_CHILDREN, storedSearch.getNumChildren());
      jsonObject.put(JsonKeys.NUM_INFANTS, storedSearch.getNumInfants());
      jsonObject.put(JsonKeys.IS_DIRECT_FLIGHT, storedSearch.getIsDirectFlight());
      if (!storedSearch.getCabinClass().equals(StoredSearch.CabinClass.UNKNOWN)) {
        jsonObject.put(JsonKeys.CABIN_CLASS, storedSearch.getCabinClass());
      }
      if (!storedSearch.getTripType().equals(StoredSearch.TripType.UNKNOWN)) {
        jsonObject.put(JsonKeys.TRIP_TYPE, storedSearch.getTripType());
      }
      jsonObject.put(JsonKeys.SEARCH_SEGMENT_LIST,
          SearchSegmentNetMapper.mapperSearchSegmentListToJson(storedSearch.getSegmentList()));
    } catch (JSONException e) {
      Log.e(StoredSearchesNetMapper.class.getName(), e.getMessage());
    }
    return jsonObject;
  }

  public static StoredSearch mapperJsonObjectToStoredSearch(JSONObject storedSearch, long userId) {
    try {
      return new StoredSearch(0, storedSearch.optLong(JsonKeys.ID),
          storedSearch.optInt(JsonKeys.NUM_ADULTS), storedSearch.optInt(JsonKeys.NUM_CHILDREN),
          storedSearch.optInt(JsonKeys.NUM_INFANTS),
          MapperUtil.optString(storedSearch, JsonKeys.CABIN_CLASS) == null
              ? StoredSearch.CabinClass.UNKNOWN : StoredSearch.CabinClass.valueOf(
              MapperUtil.optString(storedSearch, JsonKeys.CABIN_CLASS)),
          storedSearch.optBoolean(JsonKeys.IS_DIRECT_FLIGHT),
          MapperUtil.optString(storedSearch, JsonKeys.TRIP_TYPE) == null
              ? StoredSearch.TripType.UNKNOWN : StoredSearch.TripType.valueOf(
              MapperUtil.optString(storedSearch, JsonKeys.TRIP_TYPE)), true,
          SearchSegmentNetMapper.mapperJsonArrayToSearchSegmentList(
              storedSearch.optJSONArray(JsonKeys.SEARCH_SEGMENT_LIST),
              storedSearch.optLong(JsonKeys.ID)), storedSearch.optLong(JsonKeys.CREATION_DATE),
          userId);
    } catch (JSONException e) {
      Log.e(StoredSearchesNetMapper.class.getName(), e.getMessage());
    }
    return null;
  }

  public static List<StoredSearch> mapperJsonArrayToStoredSearchesList(
      JSONArray jsonArrayStoredSearches, long userId) throws JSONException {
    if (jsonArrayStoredSearches != null) {
      List<StoredSearch> storedSearchesList = new ArrayList<>();
      for (int indexS = 0; indexS < jsonArrayStoredSearches.length(); indexS++) {
        JSONObject storedSearch = jsonArrayStoredSearches.optJSONObject(indexS);
        if (storedSearch != null) {
          storedSearchesList.add(
              StoredSearchesNetMapper.mapperJsonObjectToStoredSearch(storedSearch, userId));
        }
      }
      return storedSearchesList;
    }
    return null;
  }

  public static JSONArray mapperStoredSearchesListToJson(List<StoredSearch> storedSearchesList) {
    JSONArray jsonArray = new JSONArray();
    if (storedSearchesList != null) {
      for (int indexS = 0; indexS < storedSearchesList.size(); indexS++) {
        if (storedSearchesList.get(indexS) != null) {
          jsonArray.put(mapperStoredSearchToJsonObject(storedSearchesList.get(indexS)));
        }
      }
    }
    return jsonArray;
  }
}
