package com.odigeo.dataodigeo.net.controllers;

import com.odigeo.data.entity.booking.Guide;
import com.odigeo.data.net.controllers.ArrivalGuideNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.MslAuthRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import com.odigeo.dataodigeo.net.mapper.ArrivalGuideNetMapper;
import java.util.HashMap;
import org.json.JSONException;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.GET;

public class ArrivalGuideNetController implements ArrivalGuideNetControllerInterface {

  private static final String API_URL_ARRIVAL_GUIDE = "tripInformation?";

  private RequestHelper mRequestHelper;
  private DomainHelperInterface mDomainHelper;
  private HeaderHelperInterface mHeaderHelper;
  private TokenController mTokenController;

  private ArrivalGuideNetMapper mArrivalGuideNetMapper;

  public ArrivalGuideNetController(RequestHelper requestHelper, DomainHelperInterface domainHelper,
      HeaderHelperInterface headerHelper, TokenController tokenController) {
    mRequestHelper = requestHelper;
    mDomainHelper = domainHelper;
    mHeaderHelper = headerHelper;
    mArrivalGuideNetMapper = new ArrivalGuideNetMapper();
    mTokenController = tokenController;
  }

  @Override
  public void getGuideInformation(final OnRequestDataListener<Guide> listener, String cityIataCode,
      final long geoNodeId) {
    //TODO: AUTH REQUEST
    String url = mDomainHelper.getUrl() + API_URL_ARRIVAL_GUIDE + "to=" + cityIataCode;

    MslAuthRequest<String> getUserRequest =
        new MslAuthRequest<>(GET, url, new OnRequestDataListener<String>() {
          @Override public void onResponse(String response) {
            try {
              listener.onResponse(mArrivalGuideNetMapper.parseGuide(response, geoNodeId));
            } catch (JSONException e) {
              listener.onResponse(null);
            }
          }

          @Override public void onError(MslError error, String message) {
            listener.onResponse(null);
          }
        }, mTokenController);

    HashMap<String, String> mapHeaders = new HashMap<>();

    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    mHeaderHelper.putHeaderNoneMatchParam(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAccept(mapHeaders);
    getUserRequest.setHeaders(mapHeaders);
    mRequestHelper.addRequest(getUserRequest);
  }
}
