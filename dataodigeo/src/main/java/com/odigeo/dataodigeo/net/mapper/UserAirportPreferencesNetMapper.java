package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.userData.UserAirportPreferences;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by matias.dirusso on 9/21/2015.
 */
public class UserAirportPreferencesNetMapper {

  public static List<UserAirportPreferences> mapperJSONArrayToAirportPreferencesList(
      JSONArray jsonArrayToMapper, long userId) throws JSONException {

    if (jsonArrayToMapper != null) {
      List<UserAirportPreferences> arrayListResult = new ArrayList<>();
      for (int i = 0; i < jsonArrayToMapper.length(); i++) {
        JSONObject userAirportPreferences = jsonArrayToMapper.optJSONObject(i);
        if (userAirportPreferences != null) {
          arrayListResult.add(
              mapperJSONObjectToUserAiportPreference(userAirportPreferences, userId));
        }
      }
      return arrayListResult;
    }
    return null;
  }

  public static UserAirportPreferences mapperJSONObjectToUserAiportPreference(
      JSONObject jsonToMapper, long userId) {
    if (jsonToMapper != null) {
      return new UserAirportPreferences(0, jsonToMapper.optLong(JsonKeys.ID),
          MapperUtil.optString(jsonToMapper, JsonKeys.AIRPORT), userId);
    }
    return null;
  }

  public static JSONObject mapperAirportPreferencesToJSONObject(
      UserAirportPreferences userAirportPreferences) throws JSONException {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(JsonKeys.ID, userAirportPreferences.getUserAirportPreferencesId());
    if (userAirportPreferences.getAirport() != null) {
      jsonObject.put(JsonKeys.AIRPORT, userAirportPreferences.getAirport());
    }
    return jsonObject;
  }

  public static JSONArray getUserAirportPreferencesListToJson(
      List<UserAirportPreferences> userAirportPreferencesList) throws JSONException {
    JSONArray jsonArray = new JSONArray();
    if (userAirportPreferencesList != null) {
      for (int indexAP = 0; indexAP < userAirportPreferencesList.size(); indexAP++) {
        if (userAirportPreferencesList.get(indexAP) != null) {
          jsonArray.put(
              mapperAirportPreferencesToJSONObject(userAirportPreferencesList.get(indexAP)));
        }
      }
    }
    return jsonArray;
  }
}
