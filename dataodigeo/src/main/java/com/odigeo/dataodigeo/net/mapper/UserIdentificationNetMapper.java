package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Irving
 * @since 15/09/2015
 */
public class UserIdentificationNetMapper {

  public static String mapperUserIdentificationToJsonString(UserIdentification userIdentification)
      throws JSONException {
    return mapperUserIdentificationJsonObject(userIdentification).toString();
  }

  public static JSONObject mapperUserIdentificationJsonObject(UserIdentification userIdentification)
      throws JSONException {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(JsonKeys.ID, userIdentification.getId());
    if (userIdentification.getIdentificationId() != null) {
      jsonObject.put(JsonKeys.IDENTIFICATION_ID, userIdentification.getIdentificationId());
    }
    if (userIdentification.getIdentificationCountryCode() != null) {
      jsonObject.put(JsonKeys.IDENTIFICATION_COUNTRY_CODE,
          userIdentification.getIdentificationCountryCode());
    }
    jsonObject.put(JsonKeys.IDENTIFICATION_EXPIRATION_DATE,
        userIdentification.getIdentificationExpirationDate());
    if (userIdentification.getIdentificationType() != null
        && userIdentification.getIdentificationType()
        != UserIdentification.IdentificationType.UNKNOWN) {
      jsonObject.put(JsonKeys.IDENTIFICATION_TYPE,
          userIdentification.getIdentificationType().toString());
    }
    return jsonObject;
  }

  public static UserIdentification mapperJSONObjectToUserIdentification(JSONObject jsonObject,
      long profileId) {
    if (jsonObject != null) {
      return new UserIdentification(0, jsonObject.optLong(JsonKeys.ID),
          MapperUtil.optString(jsonObject, JsonKeys.IDENTIFICATION_ID),
          MapperUtil.optString(jsonObject, JsonKeys.IDENTIFICATION_COUNTRY_CODE),
          jsonObject.optLong(JsonKeys.IDENTIFICATION_EXPIRATION_DATE),
          MapperUtil.optString(jsonObject, JsonKeys.IDENTIFICATION_TYPE) == null
              ? UserIdentification.IdentificationType.UNKNOWN
              : UserIdentification.IdentificationType.valueOf(
                  MapperUtil.optString(jsonObject, JsonKeys.IDENTIFICATION_TYPE)), profileId);
    }
    return null;
  }

  public static List<UserIdentification> mapperJSONArrayToUserIdentificationList(
      JSONArray jsonArrayIdentifications, long profileId) throws JSONException {
    if (jsonArrayIdentifications != null) {
      List<UserIdentification> identifications = new ArrayList<>();
      for (int indexUI = 0; indexUI < jsonArrayIdentifications.length(); indexUI++) {
        JSONObject frequentFlyer = jsonArrayIdentifications.getJSONObject(indexUI);
        identifications.add(mapperJSONObjectToUserIdentification(frequentFlyer, profileId));
      }
      return identifications;
    }
    return null;
  }
}
