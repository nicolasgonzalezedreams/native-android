package com.odigeo.dataodigeo.net.controllers;

import com.android.volley.RequestQueue;
import com.odigeo.data.entity.net.NetCountry;
import com.odigeo.data.net.controllers.CountryNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.CountryRequest;
import com.odigeo.dataodigeo.net.helper.CustomRequestMethod;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.mapper.NetCountryMapper;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 11/02/16
 */
public class CountryNetController
    implements CountryNetControllerInterface, OnRequestDataListener<String> {

  private final RequestQueue mRequestQueue;
  private final DomainHelperInterface mDomainHelper;
  private final HeaderHelperInterface mHeaderHelper;
  private final NetCountryMapper mNetCountryMapper;
  private OnRequestDataListListener<NetCountry> mOnRequestDataListListener;

  public CountryNetController(RequestQueue mRequestQueue, DomainHelperInterface mDomainHelper,
      HeaderHelperInterface mHeaderHelper) {
    this.mRequestQueue = mRequestQueue;
    this.mDomainHelper = mDomainHelper;
    this.mHeaderHelper = mHeaderHelper;
    this.mNetCountryMapper = new NetCountryMapper();
  }

  private String getApiUrl() {
    return mDomainHelper.getUrl() + API_URL;
  }

  private Map<String, String> buildHeaders() {
    Map<String, String> headers = new HashMap<>();
    mHeaderHelper.putAcceptEncoding(headers);
    mHeaderHelper.putDeviceId(headers);
    mHeaderHelper.putContentType(headers);
    mHeaderHelper.putAccept(headers);
    return headers;
  }

  @Override
  public void getCountries(OnRequestDataListListener<NetCountry> onRequestDataListListener) {
    this.mOnRequestDataListListener = onRequestDataListListener;
    MslRequest<String> mslRequest = new CountryRequest(CustomRequestMethod.GET, getApiUrl(), this);
    mslRequest.setHeaders(buildHeaders());
    mslRequest.forceCache();
    mRequestQueue.add(mslRequest);
  }

  @Override public void onResponse(String countriesJson) {
    if (mOnRequestDataListListener != null) {
      mOnRequestDataListListener.onResponse(
          countriesJson != null ? mNetCountryMapper.parseNetCountries(countriesJson) : null);
    }
  }

  @Override public void onError(MslError error, String message) {
    if (mOnRequestDataListListener != null) {
      mOnRequestDataListListener.onError(error, message);
    }
  }
}
