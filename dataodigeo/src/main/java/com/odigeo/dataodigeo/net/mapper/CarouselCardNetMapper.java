package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.carrousel.CampaignCard;
import com.odigeo.data.entity.carrousel.Card;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class CarouselCardNetMapper {

  private static final String HOME_CARD = "HOME";
  private static final String PROMOTION_CARD = "PROMOTION";

  public static List<Card> mapperJsonArrayToCarouselCardList(JSONArray jsonArrayCards) {
    if (jsonArrayCards != null) {
      List<Card> cardList = new ArrayList<>();
      for (int indexC = 0; indexC < jsonArrayCards.length(); indexC++) {
        JSONObject card = jsonArrayCards.optJSONObject(indexC);
        if (card != null) {
          switch (MapperUtil.optString(card, JsonKeys.TYPE)) {
            case HOME_CARD:
              cardList.add(mapperJsonObjectToHomeCard(card));
              break;
            case PROMOTION_CARD:
              cardList.add(mapperJsonObjectToCampaignCard(card));
              break;
          }
        }
      }
      return cardList;
    }
    return null;
  }

  public static Card mapperJsonObjectToHomeCard(JSONObject homeCard) {
    return new Card(homeCard.optLong(JsonKeys.ID), Card.CardType.HOME_CARD,
        homeCard.optInt(JsonKeys.PRIORITY), MapperUtil.optString(homeCard, JsonKeys.IMAGE));
  }

  public static CampaignCard mapperJsonObjectToCampaignCard(JSONObject campaignCard) {
    return new CampaignCard(campaignCard.optLong(JsonKeys.ID),
        CampaignCard.CardSubType.fromString(campaignCard.optString(JsonKeys.SUBTYPE)),
        campaignCard.optInt(JsonKeys.PRIORITY), MapperUtil.optString(campaignCard, JsonKeys.IMAGE),
        MapperUtil.optString(campaignCard, JsonKeys.TITLE),
        MapperUtil.optString(campaignCard, JsonKeys.SUBTITLE),
        MapperUtil.optString(campaignCard, JsonKeys.URL),
        MapperUtil.optString(campaignCard, JsonKeys.URL_TEXT),
        MapperUtil.optString(campaignCard, JsonKeys.URL_2),
        MapperUtil.optString(campaignCard, JsonKeys.URL_TEXT_2),
        MapperUtil.optString(campaignCard, JsonKeys.CAMPAIGN_CARD_KEY));
  }
}
