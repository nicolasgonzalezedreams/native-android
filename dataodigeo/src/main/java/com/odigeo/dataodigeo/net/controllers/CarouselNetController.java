package com.odigeo.dataodigeo.net.controllers;

import com.odigeo.data.net.controllers.CarrouselNetControllerInterface;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.carousel.CarouselMockProvider;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.HashMap;
import java.util.Map;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.GET;

public class CarouselNetController implements CarrouselNetControllerInterface {

  private static final String API_URL_CARROUSEL = "carousel?";
  private RequestHelper mRequestHelper;
  private DomainHelperInterface mDomainHelper;
  private HeaderHelperInterface mHeaderHelper;

  public CarouselNetController(RequestHelper requestHelper, DomainHelperInterface domainHelper,
      HeaderHelperInterface headerHelper) {
    mRequestHelper = requestHelper;
    mDomainHelper = domainHelper;
    mHeaderHelper = headerHelper;
  }

  @Override public void getCarouselPromotion(OnRequestDataListener<String> listener,
      boolean hasToCleanCache) {
    String url = mDomainHelper.getUrl() + API_URL_CARROUSEL;
    MslRequest getCampaignRequest = new MslRequest<>(GET, url, listener);
    Map<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    mHeaderHelper.putAccept(mapHeaders);
    if (hasToCleanCache) {
      mRequestHelper.cleanCache(url);
    }
    getCampaignRequest.setHeaders(mapHeaders);
    getCampaignRequest.forceCache();
    mRequestHelper.addRequest(getCampaignRequest);
  }

  @Override public String getCarouselFromMock() {
    return CarouselMockProvider.provideMultiplePromosMockCarousel();
  }
}
