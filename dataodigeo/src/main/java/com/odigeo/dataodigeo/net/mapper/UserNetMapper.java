package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.userData.Membership;
import com.odigeo.data.entity.userData.User;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UserNetMapper {

  public static User jsonUserMapper(String userJson) throws JSONException {
    JSONObject jsonUser = new JSONObject(userJson);
    long userId = jsonUser.optLong(JsonKeys.ID);
    User newUser = new User(0, userId, MapperUtil.optString(jsonUser, JsonKeys.EMAIL),
        MapperUtil.optString(jsonUser, JsonKeys.WEB_SITE),
        jsonUser.optBoolean(JsonKeys.ACCEPTS_NEWSLETTER),
        jsonUser.optBoolean(JsonKeys.ACCEPTS_PARTNERS_INFO),
        jsonUser.optLong(JsonKeys.CREATION_DATE), jsonUser.optLong(JsonKeys.LAST_MODIFIED),
        MapperUtil.optString(jsonUser, JsonKeys.LOCALE),
        MapperUtil.optString(jsonUser, JsonKeys.MARKETING_PORTAL),
        MapperUtil.optString(jsonUser, JsonKeys.SOURCE) == null ? User.Source.UNKNOWN
            : User.Source.valueOf(MapperUtil.optString(jsonUser, JsonKeys.SOURCE)),
        MapperUtil.optString(jsonUser, JsonKeys.STATUS) == null ? User.Status.UNKNOWN
            : User.Status.valueOf(MapperUtil.optString(jsonUser, JsonKeys.STATUS)),
        UserAirLinePreferencesNetMapper.jsonArrayUserAirLinePreferencesMapper(
            jsonUser.optJSONArray(JsonKeys.AIRLINE_PREFERENCES_LIST), userId),
        UserAirportPreferencesNetMapper.mapperJSONArrayToAirportPreferencesList(
            jsonUser.optJSONArray(JsonKeys.AIRPORT_PREFERENCES_LIST), userId),
        UserDestinationPreferencesNetMapper.mapperJSONArrayToDestinationPreferencesList(
            jsonUser.optJSONArray(JsonKeys.DESTINATION_PREFERENCES_LIST), userId),
        UserDeviceNetMapper.mapperJSONArrayToUserDeviceList(jsonUser.optJSONArray(JsonKeys.DEVICES),
            userId),
        UserLoginNetMapper.mapperJSONObjectToUserLogin(jsonUser.optJSONObject(JsonKeys.LOGIN),
            userId), UserTravellerNetMapper.mapperJsonArrayToUserTravellerList(
        jsonUser.optJSONArray(JsonKeys.TRAVELLLER), userId),
        StoredSearchesNetMapper.mapperJsonArrayToStoredSearchesList(
            jsonUser.optJSONArray(JsonKeys.SEARCH_LIST), userId),
        UserCreditCardNetMapper.mapperJSONArrayToCreditCardUserList(
            jsonUser.optJSONArray(JsonKeys.CREDIT_CARDS)), hasMembership(jsonUser)
        ? MembershipNetMapper.mapperJSONArrayToMembershipList(
        jsonUser.optJSONArray(JsonKeys.MEMBERSHIP_INFO)) : new ArrayList<Membership>());

    ArrayList<Long> bookingsIdList = new ArrayList<>();
    JSONArray bookings = jsonUser.optJSONArray(JsonKeys.BOOKINGS);
    for (int i = 0; bookings != null && i < bookings.length(); i++) {
      bookingsIdList.add(bookings.optLong(i));
    }
    newUser.setBookingsId(bookingsIdList);

    return newUser;
  }

  private static boolean hasMembership(JSONObject jsonObject) {
    return jsonObject.has(JsonKeys.MEMBERSHIP_INFO) && !jsonObject.isNull(JsonKeys.MEMBERSHIP_INFO);
  }

  public static JSONObject mapperUserToJsonObject(User user) throws JSONException {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(JsonKeys.ID, user.getUserId());
    if (user.getEmail() != null) {
      jsonObject.put(JsonKeys.EMAIL, user.getEmail());
    }
    if (user.getWebSite() != null) {
      jsonObject.put(JsonKeys.WEB_SITE, user.getWebSite());
    }
    jsonObject.put(JsonKeys.ACCEPTS_NEWSLETTER, user.getAcceptsNewsletter());
    jsonObject.put(JsonKeys.ACCEPTS_PARTNERS_INFO, user.getAcceptsPartnersInfo());
    jsonObject.put(JsonKeys.CREATION_DATE, user.getCreationDate());
    jsonObject.put(JsonKeys.LAST_MODIFIED, user.getLastModified());
    if (user.getLocale() != null) {
      jsonObject.put(JsonKeys.LOCALE, user.getLocale());
    }
    if (user.getMarketingPortal() != null) {
      jsonObject.put(JsonKeys.MARKETING_PORTAL, user.getMarketingPortal());
    }
    if (user.getSource() != null && user.getSource() != User.Source.UNKNOWN) {
      jsonObject.put(JsonKeys.SOURCE, user.getSource().toString());
    }
    if (user.getStatus() != null && user.getStatus() != User.Status.UNKNOWN) {
      jsonObject.put(JsonKeys.STATUS, user.getStatus().toString());
    }
    if (user.getUserAirlinePreferencesList() != null) {
      jsonObject.put(JsonKeys.AIRLINE_PREFERENCES_LIST,
          UserAirLinePreferencesNetMapper.getUserAirlinePreferencesListToJson(
              user.getUserAirlinePreferencesList()));
    }
    if (user.getUserAirportPreferencesList() != null) {
      jsonObject.put(JsonKeys.AIRPORT_PREFERENCES_LIST,
          UserAirportPreferencesNetMapper.getUserAirportPreferencesListToJson(
              user.getUserAirportPreferencesList()));
    }
    if (user.getUserDestinationPreferencesList() != null) {
      jsonObject.put(JsonKeys.DESTINATION_PREFERENCES_LIST,
          UserDestinationPreferencesNetMapper.getUserDestinationPreferencesListToJson(
              user.getUserDestinationPreferencesList()));
    }
    if (user.getUserDevicesList() != null) {
      jsonObject.put(JsonKeys.DEVICES,
          (UserDeviceNetMapper.getUserDevicesListToJson(user.getUserDevicesList())));
    }
    if (user.getUserLogin() != null) {
      jsonObject.put(JsonKeys.LOGIN,
          UserLoginNetMapper.mapperUserLoginToJsonObject(user.getUserLogin()));
    }
    if (user.getUserTravellerList() != null) {
      jsonObject.put(JsonKeys.TRAVELLLER,
          UserTravellerNetMapper.getUserTravellerListToJson(user.getUserTravellerList()));
    }
    if (user.getUserSearchesList() != null) {
      jsonObject.put(JsonKeys.SEARCH_LIST,
          StoredSearchesNetMapper.mapperStoredSearchesListToJson(user.getUserSearchesList()));
    }
    if (user.getMemberships() != null && user.getMemberships().size() > 0) {
      jsonObject.put(JsonKeys.MEMBERSHIP_INFO,
          MembershipNetMapper.mapperMembershipListToJsonArray(user.getMemberships()));
    }
    return jsonObject;
  }
}
