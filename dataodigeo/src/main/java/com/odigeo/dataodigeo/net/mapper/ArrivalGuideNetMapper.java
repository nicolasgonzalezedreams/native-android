package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.booking.Guide;
import org.json.JSONException;
import org.json.JSONObject;

public class ArrivalGuideNetMapper {

  public Guide parseGuide(String guideJson, long geoNodeId) throws JSONException {

    JSONObject jsonResponse = new JSONObject(guideJson);
    JSONObject jsonGuide = jsonResponse.getJSONObject("to").getJSONObject("guide");

    String url = jsonGuide.getString("url");
    String language = jsonGuide.getString("language");

    return new Guide(-1, geoNodeId, url, language);
  }
}
