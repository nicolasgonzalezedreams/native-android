package com.odigeo.dataodigeo.net.controllers;

import com.google.gson.Gson;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.request.ModifyShoppingCartRequest;
import com.odigeo.data.net.controllers.AddProductsNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.CustomRequestMethod;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.HashMap;

public class AddProductsNetController implements AddProductsNetControllerInterface {

  private static final String URL_ADD_PRODUCT = "addProduct";

  private RequestHelper mRequestHelper;
  private DomainHelperInterface mDomainHelper;
  private HeaderHelperInterface mHeaderHelper;
  private Gson mGson;

  public AddProductsNetController(RequestHelper requestHelper, DomainHelperInterface domainHelper,
      HeaderHelperInterface headerHelper, Gson gson) {
    mRequestHelper = requestHelper;
    mDomainHelper = domainHelper;
    mHeaderHelper = headerHelper;
    mGson = gson;
  }

  @Override public void addProductsToShoppingCart(
      final OnRequestDataListener<CreateShoppingCartResponse> listener,
      ModifyShoppingCartRequest modifyShoppingCartRequest) {
    String url = mDomainHelper.getUrl() + URL_ADD_PRODUCT;
    MslRequest<String> addProductRequest =
        new MslRequest<>(CustomRequestMethod.POST, url, new OnRequestDataListener<String>() {
          @Override public void onResponse(String shoppingCartResponse) {
            CreateShoppingCartResponse createShoppingCartResponse =
                mGson.fromJson(shoppingCartResponse, CreateShoppingCartResponse.class);
            listener.onResponse(createShoppingCartResponse);
          }

          @Override public void onError(MslError error, String message) {
            listener.onError(error, message);
          }
        });

    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAccept(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    addProductRequest.setHeaders(mapHeaders);
    addProductRequest.setBody(mGson.toJson(modifyShoppingCartRequest));
    mRequestHelper.addRequest(addProductRequest);
  }
}