package com.odigeo.dataodigeo.net.controllers;

import com.android.volley.RequestQueue;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.net.controllers.AutoCompleteNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListListener;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.CustomRequestMethod;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.mapper.CityMapper;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 3/05/16
 */
public class AutoCompleteNetController
    implements AutoCompleteNetControllerInterface, OnRequestDataListener<String> {

  private static final String PARAMETERS =
      "?productType=FLIGHT&locale=%s&website=%s&departureOrArrival=DEPARTURE";
  private final RequestQueue mRequestQueue;
  private final DomainHelperInterface mDomainHelper;
  private final HeaderHelperInterface mHeaderHelper;
  private final CityMapper mCityMapper;
  private OnRequestDataListListener<City> mOnRequestDataListListener;

  public AutoCompleteNetController(RequestQueue requestQueue, DomainHelperInterface domainHelper,
      HeaderHelperInterface headerHelper) {
    this.mRequestQueue = requestQueue;
    this.mDomainHelper = domainHelper;
    this.mHeaderHelper = headerHelper;
    this.mCityMapper = new CityMapper();
  }

  @Override public void getDestination(String query, String locale, String website,
      OnRequestDataListListener<City> onRequestDataListListener) {
    mOnRequestDataListListener = onRequestDataListListener;
    String url =
        mDomainHelper.getUrl() + API_URL + query + String.format(PARAMETERS, locale, website);
    MslRequest<String> mslRequest = new MslRequest<>(CustomRequestMethod.GET, url, this);
    mslRequest.setHeaders(buildHeaders());
    mRequestQueue.add(mslRequest);
  }

  private Map<String, String> buildHeaders() {
    Map<String, String> headers = new HashMap<>();
    mHeaderHelper.putAcceptEncoding(headers);
    mHeaderHelper.putDeviceId(headers);
    mHeaderHelper.putContentType(headers);
    mHeaderHelper.putAccept(headers);
    return headers;
  }

  @Override public void onResponse(String json) {
    if (mOnRequestDataListListener != null) {
      mOnRequestDataListListener.onResponse(mCityMapper.parseCities(json));
    }
  }

  @Override public void onError(MslError error, String message) {
    if (mOnRequestDataListListener != null) {
      mOnRequestDataListListener.onError(error, message);
    }
  }
}
