package com.odigeo.dataodigeo.net.helper;

import com.android.volley.AuthFailureError;
import com.google.android.gms.auth.GoogleAuthException;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.controllers.TokenController;
import com.odigeo.dataodigeo.net.mapper.DataNetMapper;
import java.io.IOException;
import java.util.Map;

public class MslAuthRequest<T> extends MslRequest<T> {

  private TokenController mTokenController;

  public MslAuthRequest(CustomRequestMethod method, String url, OnRequestDataListener listener,
      DataNetMapper dataNetMapper, TokenController tokenController) {
    super(method, url, listener, dataNetMapper);
    this.mTokenController = tokenController;
  }

  public MslAuthRequest(CustomRequestMethod method, String url, OnRequestDataListener<T> listener,
      TokenController tokenController) {
    super(method, url, listener);
    this.mTokenController = tokenController;
  }

  @Override public Map<String, String> getHeaders() throws AuthFailureError {

    try {
      headers.put(HeaderHelper.HEADER_AUTHORIZATION_PARAM, mTokenController.buildAuthHeader());
    } catch (InterruptedException | GoogleAuthException | IOException e) {
    }

    return headers;
  }
}
