package com.odigeo.dataodigeo.net.helper.image;

import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.widget.ImageView;
import android.widget.RemoteViews;

public interface OdigeoImageLoader<T extends ImageView> {

  void load(T view, String imageUrl);

  void load(ImageView view, @DrawableRes int drawableResId, boolean addDefaultAnimations);

  void load(T view, String imageUrl, @DrawableRes int placeholderId);

  void load(ImageView view, String imageUrl, @DrawableRes int loadingImage,
      @DrawableRes int placeholderId, boolean addDefaultAnimations);

  void loadCircleTransformation(T view, String imageUrl);

  void loadCircleTransformation(T view, String imageUrl, @DrawableRes int placeholderId);

  void loadRoundedTransformation(T view, String imageUrl, @DrawableRes int loadingImage,
      @DrawableRes int placeholderId, int radius);

  void loadRoundedTransformation(ImageView view, @DrawableRes int drawable, int radius);

  void loadWidgetImage(RemoteViews view, String imageUrl, @IdRes int imageResource, int width,
      int height, int appWidgetId, @DrawableRes int placeholderId);
}
