package com.odigeo.dataodigeo.net.controllers;

public class AuthToken {

  public static final String EMPTY_TOKEN = "";
  public static final String INVALID_TOKEN = "invalid";

  private String token;
  private long expiresAt;

  public AuthToken() {
    token = EMPTY_TOKEN;
    expiresAt = 0;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String nToken) {
    this.token = nToken;
  }

  public long getExpiresAt() {
    return expiresAt;
  }

  public void setExpiresAt(long nExpiresAt) {
    this.expiresAt = nExpiresAt;
  }
}
