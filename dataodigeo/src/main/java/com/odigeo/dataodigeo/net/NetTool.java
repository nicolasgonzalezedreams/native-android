package com.odigeo.dataodigeo.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.odigeo.data.net.NetToolInterface;

/**
 * Created by Jaime Toca on 26/1/16.
 */
public class NetTool implements NetToolInterface {

  private Context mContext;

  public NetTool(Context context) {
    this.mContext = context;
  }

  public boolean isThereInternetConnection() {
    boolean isConnected;

    ConnectivityManager connectivityManager =
        (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
    isConnected = (networkInfo != null && networkInfo.isConnectedOrConnecting());

    return isConnected;
  }
}
