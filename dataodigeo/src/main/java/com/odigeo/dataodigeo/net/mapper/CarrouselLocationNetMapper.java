package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.carrousel.CarouselLocation;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import org.json.JSONObject;

/**
 * Created by matia on 1/25/2016.
 */
public class CarrouselLocationNetMapper {

  public static CarouselLocation mapperJsonObjectToCarrouselLocation(JSONObject carrouselLocation) {
    return new CarouselLocation(MapperUtil.optString(carrouselLocation, JsonKeys.IATA),
        carrouselLocation.optLong(JsonKeys.DATE),
        MapperUtil.optString(carrouselLocation, JsonKeys.TERMINAL),
        MapperUtil.optString(carrouselLocation, JsonKeys.DESCRIPTION));
  }
}
