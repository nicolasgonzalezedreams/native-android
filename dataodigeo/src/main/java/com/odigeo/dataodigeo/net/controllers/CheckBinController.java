package com.odigeo.dataodigeo.net.controllers;

import com.google.gson.Gson;
import com.odigeo.data.entity.CreditCardBinDetails;
import com.odigeo.data.net.controllers.BinCheckNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.HashMap;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.GET;

public class CheckBinController implements BinCheckNetControllerInterface {

  private static String URL_BINCHECK = "bin/";
  HashMap<String, String> mMapHeaders;
  MslRequest<String> mCheckBinRequest;
  private RequestHelper mRequestHelper;
  private DomainHelperInterface mDomainHelper;
  private HeaderHelperInterface mHeaderHelper;
  private Gson mGson;

  public CheckBinController(RequestHelper requestHelper, DomainHelperInterface domainHelper,
      HeaderHelperInterface headerHelper, Gson gson) {
    mRequestHelper = requestHelper;
    mDomainHelper = domainHelper;
    mHeaderHelper = headerHelper;
    mGson = gson;
  }

  @Override public void checkBin(final OnRequestDataListener<CreditCardBinDetails> listener,
      String creditCardNumber) {
    String url = mDomainHelper.getUrl() + URL_BINCHECK + creditCardNumber;

    mCheckBinRequest = new MslRequest<>(GET, url, new OnRequestDataListener<String>() {
      @Override public void onResponse(String checkBinResponse) {
        CreditCardBinDetails creditCardBinDetails =
            mGson.fromJson(checkBinResponse, CreditCardBinDetails.class);
        listener.onResponse(creditCardBinDetails);
      }

      @Override public void onError(MslError error, String message) {
        listener.onError(error, message);
      }
    });

    mMapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mMapHeaders);
    mHeaderHelper.putAcceptEncoding(mMapHeaders);
    mHeaderHelper.putAccept(mMapHeaders);
    mHeaderHelper.putContentType(mMapHeaders);
    mCheckBinRequest.setHeaders(mMapHeaders);
    mRequestHelper.addRequest(mCheckBinRequest);
  }
}
