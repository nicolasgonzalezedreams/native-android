package com.odigeo.dataodigeo.net.controllers;

import android.util.Log;
import com.odigeo.data.entity.AttachmentItem;
import com.odigeo.data.net.controllers.SendMailNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import com.odigeo.dataodigeo.net.mapper.DataNetMapper;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.POST;
import static com.odigeo.dataodigeo.net.mapper.DataNetMapper.DataType.JSON;

/**
 * Created by eleazarspak on 2/5/16.
 */
public class SendMailNetController implements SendMailNetControllerInterface {

  private static final String API_URL_SEND_MAIL = "mail";

  private static final String REQUEST_JSON_FROM_KEY = "from";
  private static final String REQUEST_JSON_TO_KEY = "to";
  private static final String REQUEST_JSON_SUBJECT_KEY = "subject";
  private static final String REQUEST_JSON_BODY_KEY = "body";
  private static final String REQUEST_JSON_ATTACHMENTS_KEY = "attachments";
  private static final String REQUEST_JSON_ATTACHMENTS_NAME_KEY = "name";
  private static final String REQUEST_JSON_ATTACHMENTS_CONTENT_KEY = "content";

  private RequestHelper mRequestHelper;
  private DomainHelperInterface mDomainHelper;
  private HeaderHelperInterface mHeaderHelper;

  public SendMailNetController(RequestHelper requestHelper, DomainHelperInterface domainHelper,
      HeaderHelperInterface headerHelper) {
    mRequestHelper = requestHelper;
    mDomainHelper = domainHelper;
    mHeaderHelper = headerHelper;
  }

  @Override public void sendMail(OnRequestDataListener<Boolean> listener, String from, String to,
      String subject, String body, List<AttachmentItem> attachments) {

    try {
      final MslRequest<Boolean> sendMailRequest =
          new MslRequest<>(POST, mDomainHelper.getUrl() + API_URL_SEND_MAIL, listener,
              new DataNetMapper(Boolean.class, JSON));

      JSONObject jsonBody = new JSONObject();
      try {
        jsonBody.put(REQUEST_JSON_FROM_KEY, from);
        jsonBody.put(REQUEST_JSON_TO_KEY, to);
        jsonBody.put(REQUEST_JSON_SUBJECT_KEY, subject);
        jsonBody.put(REQUEST_JSON_BODY_KEY, body);
        JSONArray jsonAttachments = new JSONArray();
        for (AttachmentItem attachmentItem : attachments) {
          JSONObject jsonAttachment = new JSONObject();
          jsonAttachment.put(REQUEST_JSON_ATTACHMENTS_NAME_KEY, attachmentItem.getName());
          jsonAttachment.put(REQUEST_JSON_ATTACHMENTS_CONTENT_KEY, attachmentItem.getContent());
          jsonAttachments.put(jsonAttachment);
        }
        jsonBody.put(REQUEST_JSON_ATTACHMENTS_KEY, jsonAttachments);
        sendMailRequest.setBody(jsonBody.toString());
      } catch (JSONException e) {
        Log.e("SendMailNetController", e.getMessage());
      }

      HashMap<String, String> mapHeaders = new HashMap<>();
      mHeaderHelper.putAcceptEncoding(mapHeaders);
      mHeaderHelper.putDeviceId(mapHeaders);
      mHeaderHelper.putContentType(mapHeaders);
      mHeaderHelper.putAcceptWithoutVersion(mapHeaders);
      sendMailRequest.setHeaders(mapHeaders);

      mRequestHelper.addRequest(sendMailRequest);
    } catch (Exception e) {
      listener.onError(MslError.UNK_001, e.toString());
    }
  }
}
