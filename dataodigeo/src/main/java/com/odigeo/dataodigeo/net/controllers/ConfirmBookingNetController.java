package com.odigeo.dataodigeo.net.controllers;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.odigeo.data.entity.shoppingCart.BookingResponse;
import com.odigeo.data.entity.shoppingCart.request.BookingRequest;
import com.odigeo.data.net.controllers.ConfirmBookingNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.CustomRequestMethod;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.HashMap;

public class ConfirmBookingNetController implements ConfirmBookingNetControllerInterface {

  private static final String URL_CONFIRM_BOOKING = "book";
  private static final int REQUEST_TIME_OUT = 5 * 1000 * 60;

  private RequestHelper mRequestHelper;
  private DomainHelperInterface mDomainHelper;
  private HeaderHelperInterface mHeaderHelper;
  private Gson mGson;

  public ConfirmBookingNetController(RequestHelper requestHelper,
      DomainHelperInterface domainHelper, HeaderHelperInterface headerHelper, Gson gson) {
    mRequestHelper = requestHelper;
    mDomainHelper = domainHelper;
    mHeaderHelper = headerHelper;
    mGson = gson;
  }

  @Override public void confirmBooking(final OnRequestDataListener<BookingResponse> listener,
      BookingRequest bookingRequest) {
    String url = mDomainHelper.getUrl() + URL_CONFIRM_BOOKING;
    MslRequest<String> confirmBookingRequest =
        new MslRequest<>(CustomRequestMethod.POST, url, new OnRequestDataListener<String>() {
          @Override public void onResponse(String object) {
            BookingResponse bookingResponse = mGson.fromJson(object, BookingResponse.class);
            listener.onResponse(bookingResponse);
          }

          @Override public void onError(MslError error, String message) {
            listener.onError(error, message);
          }
        });

    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAccept(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    confirmBookingRequest.setHeaders(mapHeaders);
    confirmBookingRequest.setBody(mGson.toJson(bookingRequest));
    confirmBookingRequest.setRetryPolicy(
        new DefaultRetryPolicy(REQUEST_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    mRequestHelper.addRequest(confirmBookingRequest);
  }
}