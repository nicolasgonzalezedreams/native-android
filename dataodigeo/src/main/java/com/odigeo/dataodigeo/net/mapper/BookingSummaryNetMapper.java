package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.booking.BookingSummary;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ismaelmachado on 10/15/15.
 */
public class BookingSummaryNetMapper {

  public static List<BookingSummary> parseBookingStatus(String userJson) throws JSONException {
    List<BookingSummary> bookingSummaryList = new ArrayList<>();

    JSONObject jsonResponse = new JSONObject(userJson);
    JSONArray bookingsArray = jsonResponse.getJSONArray("bookings");
    if (bookingsArray.length() > 0) {
      JSONObject bookings = ((JSONObject) bookingsArray.get(0));
      JSONArray bookingSummariesArray = bookings.getJSONArray("bookingSummaries");
      BookingSummary bookingSummary;

      for (int i = 0; i < bookingSummariesArray.length(); i++) {
        JSONObject jsonBookingSummary = (JSONObject) bookingSummariesArray.get(i);
        bookingSummary = new BookingSummary();
        bookingSummary.setId(jsonBookingSummary.getLong("id"));
        bookingSummary.setStatus(
            BookingSummary.Status.valueOf(jsonBookingSummary.getString("status")));
        bookingSummaryList.add(bookingSummary);
      }
    }

    return bookingSummaryList;
  }
}
