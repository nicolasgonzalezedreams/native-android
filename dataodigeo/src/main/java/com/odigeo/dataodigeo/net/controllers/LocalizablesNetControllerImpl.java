package com.odigeo.dataodigeo.net.controllers;

import com.odigeo.data.entity.localizables.LocalizablesResponse;
import com.odigeo.data.net.controllers.LocalizablesNetController;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import com.odigeo.dataodigeo.net.mapper.LocalizablesNetMapper;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.GET;

public class LocalizablesNetControllerImpl implements LocalizablesNetController {

  private static final String URL_LOCALIZABLES = "textFiles";
  HashMap<String, String> mapHeaders;
  MslRequest<String> localizablesRequest;
  private RequestHelper requestHelper;
  private DomainHelperInterface domainHelper;
  private HeaderHelperInterface headerHelper;
  private CrashlyticsController crashlyticsController;

  public LocalizablesNetControllerImpl(RequestHelper requestHelper,
      DomainHelperInterface domainHelper, HeaderHelperInterface headerHelper,
      CrashlyticsController crashlyticsController) {
    this.requestHelper = requestHelper;
    this.domainHelper = domainHelper;
    this.headerHelper = headerHelper;
    this.crashlyticsController = crashlyticsController;
  }

  @Override
  public void getLocalizables(final OnRequestDataListener<LocalizablesResponse> listener) {
    String url = domainHelper.getUrl() + URL_LOCALIZABLES;
    localizablesRequest = new MslRequest<>(GET, url, new OnRequestDataListener<String>() {
      @Override public void onResponse(String localizables) {
        try {
          JSONObject jsonObject = new JSONObject(localizables);
          LocalizablesResponse localizablesResponse =
              LocalizablesNetMapper.parseLocalizablesJSONToResponse(jsonObject);

          listener.onResponse(localizablesResponse);
        } catch (JSONException exception) {
          exception.printStackTrace();
          crashlyticsController.trackNonFatal(
              new Exception(CrashlyticsController.LOCALIZABLES_PARSING_EXCEPTION, exception));
          listener.onError(null, exception.getMessage());
        }
      }

      @Override public void onError(MslError error, String message) {
        listener.onError(error, message);
      }
    });

    mapHeaders = new HashMap<>();
    headerHelper.putDeviceId(mapHeaders);
    headerHelper.putAcceptEncoding(mapHeaders);
    headerHelper.putAccept(mapHeaders);
    headerHelper.putContentType(mapHeaders);
    localizablesRequest.setHeaders(mapHeaders);
    requestHelper.addRequest(localizablesRequest);
  }

  @Override public HashMap<String, String> getMapHeaders() {
    return mapHeaders;
  }
}
