package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.userData.UserAirlinePreferences;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UserAirLinePreferencesNetMapper {

  public static List<UserAirlinePreferences> jsonArrayUserAirLinePreferencesMapper(
      JSONArray jsonArrayToMapper, long userId) throws JSONException {
    if (jsonArrayToMapper != null) {
      List<UserAirlinePreferences> arrayListResult = new ArrayList<>();
      for (int i = 0; i < jsonArrayToMapper.length(); i++) {
        JSONObject userAirlinePreferences = jsonArrayToMapper.optJSONObject(i);
        if (userAirlinePreferences != null) {
          arrayListResult.add(jsonUserAirLinePreferencesMapper(userAirlinePreferences, userId));
        }
      }
      return arrayListResult;
    }
    return null;
  }

  public static UserAirlinePreferences jsonUserAirLinePreferencesMapper(JSONObject jsonToMapper,
      long userId) {
    if (jsonToMapper != null) {
      return new UserAirlinePreferences(0, jsonToMapper.optLong(JsonKeys.ID),
          MapperUtil.optString(jsonToMapper, JsonKeys.AIRLINE_CODE), userId);
    }
    return null;
  }

  public static JSONObject mapperAirlinePreferencesToJSONObject(
      UserAirlinePreferences airlinePreferences) throws JSONException {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(JsonKeys.ID, airlinePreferences.getUserAirlinePreferencesId());
    if (airlinePreferences.getAirlineCode() != null) {
      jsonObject.put(JsonKeys.AIRLINE_CODE, airlinePreferences.getAirlineCode());
    }
    return jsonObject;
  }

  public static JSONArray getUserAirlinePreferencesListToJson(
      List<UserAirlinePreferences> userAirlinePreferencesList) throws JSONException {
    JSONArray jsonArray = new JSONArray();
    if (userAirlinePreferencesList != null) {
      for (int indexUT = 0; indexUT < userAirlinePreferencesList.size(); indexUT++) {
        if (userAirlinePreferencesList.get(indexUT) != null) {
          jsonArray.put(
              mapperAirlinePreferencesToJSONObject(userAirlinePreferencesList.get(indexUT)));
        }
      }
    }
    return jsonArray;
  }
}
