package com.odigeo.dataodigeo.net.helper;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.odigeo.data.net.error.MslError;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.zip.GZIPInputStream;
import org.json.JSONException;
import org.json.JSONObject;

public class OdigeoRequestFuture<T>
    implements Future<T>, Response.Listener<T>, Response.ErrorListener {

  private Request<?> mRequest;
  private boolean mResultReceived = false;
  private T mResult;
  private VolleyError mException;

  private OdigeoRequestFuture() {
  }

  public static <E> OdigeoRequestFuture<E> newFuture() {
    return new OdigeoRequestFuture<>();
  }

  public void setRequest(Request<?> request) {
    mRequest = request;
  }

  public T getInfo() throws ExecutionException, InterruptedException {
    try {
      return get();
    } catch (InterruptedException | ExecutionException e) {
      throw e;
    }
  }

  @Override public synchronized boolean cancel(boolean mayInterruptIfRunning) {
    if (mRequest == null) {
      return false;
    }

    if (!isDone()) {
      mRequest.cancel();
      return true;
    } else {
      return false;
    }
  }

  @Override public T get() throws InterruptedException, ExecutionException {
    try {
      return doGet(null);
    } catch (TimeoutException e) {
      throw new AssertionError(e);
    }
  }

  @Override public T get(long timeout, TimeUnit unit)
      throws InterruptedException, ExecutionException, TimeoutException {
    return null;
  }

  private synchronized T doGet(Long timeoutMs)
      throws InterruptedException, ExecutionException, TimeoutException {
    if (mException != null) {
      throw new ExecutionException(mException);
    }

    if (mResultReceived) {
      return mResult;
    }

    if (timeoutMs == null) {
      wait(0);
    } else if (timeoutMs > 0) {
      wait(timeoutMs);
    }

    if (mException != null) {
      throw new ExecutionException(mException);
    }

    if (!mResultReceived) {
      throw new TimeoutException();
    }

    return mResult;
  }

  @Override public boolean isCancelled() {
    if (mRequest == null) {
      return false;
    }
    return mRequest.isCanceled();
  }

  @Override public synchronized boolean isDone() {
    return mResultReceived || mException != null || isCancelled();
  }

  @Override public synchronized void onResponse(T response) {
    mResultReceived = true;
    mResult = response;
    notifyAll();
  }

  @Override public synchronized void onErrorResponse(VolleyError error) {

    mException = error;

    NetworkResponse response = error.networkResponse;
    if (response != null && response.data != null) {
      try {
        String errorJson = decompressResponseFromGzip(response.data);
        JSONObject errorJsonObject = new JSONObject(errorJson);
        if (isAuthError(errorJsonObject)) {
          mException = new AuthFailureError(response);
        }
      } catch (JSONException e) {
      }
    }

    notifyAll();
  }

  private String decompressResponseFromGzip(byte[] data) {
    String output = "";
    if (data != null && data.length != 0) {
      GZIPInputStream gStream;
      try {
        gStream = new GZIPInputStream(new ByteArrayInputStream(data));
        InputStreamReader reader = new InputStreamReader(gStream);
        BufferedReader in = new BufferedReader(reader);
        String read;
        while ((read = in.readLine()) != null) {
          output += read;
        }
        reader.close();
        in.close();
        gStream.close();
      } catch (IOException e) {
        output = new String(data);
      }
    }
    return output;
  }

  private boolean isAuthError(JSONObject error) throws JSONException {
    MslError message = MslError.getEnum(error.getString("message"));
    if (message.equals(MslError.AUTH_005) || message.equals(MslError.AUTH_006)) {
      return true;
    } else {
      return false;
    }
  }
}
