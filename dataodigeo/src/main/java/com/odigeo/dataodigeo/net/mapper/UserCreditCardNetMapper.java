package com.odigeo.dataodigeo.net.mapper;

import android.support.annotation.Nullable;
import com.odigeo.data.entity.userData.CreditCard;
import com.odigeo.dataodigeo.constants.JsonKeys;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UserCreditCardNetMapper {

  @Nullable
  public static List<CreditCard> mapperJSONArrayToCreditCardUserList(JSONArray jsonArrayCreditCard)
      throws JSONException {
    if (jsonArrayCreditCard != null) {
      List<CreditCard> arrayListResult = new ArrayList<>();

      for (int i = 0; i < jsonArrayCreditCard.length(); i++) {
        JSONObject jsonCreditCard = jsonArrayCreditCard.optJSONObject(i);
        if (jsonCreditCard != null) {
          arrayListResult.add(mapperJSONObjectToCreditCard(jsonCreditCard));
        }
      }
      return arrayListResult;
    }
    return null;
  }

  private static CreditCard mapperJSONObjectToCreditCard(JSONObject jsonCreditCard)
      throws JSONException {
    long id = jsonCreditCard.optLong(JsonKeys.CARD_ID);
    String owner = jsonCreditCard.optString(JsonKeys.CARD_OWNER);
    long creationDate = jsonCreditCard.optLong(JsonKeys.CARD_CREATION);
    String creditCardNumber = jsonCreditCard.optString(JsonKeys.CARD_NUMBER);
    String expiryMonth = jsonCreditCard.optString(JsonKeys.CARD_EXPIRY_MONTH);
    String expiryYear = jsonCreditCard.optString(JsonKeys.CARD_EXPIRY_YEAR);
    long modifiedDate = jsonCreditCard.optLong(JsonKeys.CARD_MODIFIED_DATE);
    boolean isDefault = jsonCreditCard.optBoolean(JsonKeys.CARD_IS_DEFAULT);
    long lastUsage = jsonCreditCard.optLong(JsonKeys.CARD_LAST_USAGE);
    String creditCardType = jsonCreditCard.optString(JsonKeys.CARD_TYPE);

    return new CreditCard(creditCardNumber, expiryMonth, expiryYear, id, owner, creationDate,
        modifiedDate, lastUsage, isDefault, creditCardType);
  }
}
