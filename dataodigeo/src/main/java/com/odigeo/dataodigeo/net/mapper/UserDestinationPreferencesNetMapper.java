package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.userData.UserDestinationPreferences;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.utils.MapperUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by matias.dirusso on 9/21/2015.
 */
public class UserDestinationPreferencesNetMapper {

  public static List<UserDestinationPreferences> mapperJSONArrayToDestinationPreferencesList(
      JSONArray jsonArrayToMapper, long userId) throws JSONException {
    if (jsonArrayToMapper != null) {
      List<UserDestinationPreferences> arrayListResult = new ArrayList<>();
      for (int i = 0; i < jsonArrayToMapper.length(); i++) {
        JSONObject userDestinationPreferences = jsonArrayToMapper.optJSONObject(i);
        if (userDestinationPreferences != null) {
          arrayListResult.add(
              mapperJSONObjectToUserDestinationPreference(userDestinationPreferences, userId));
        }
      }
      return arrayListResult;
    }
    return null;
  }

  public static UserDestinationPreferences mapperJSONObjectToUserDestinationPreference(
      JSONObject jsonToMapper, long userId) {
    if (jsonToMapper != null) {
      return new UserDestinationPreferences(0, jsonToMapper.optLong(JsonKeys.ID),
          MapperUtil.optString(jsonToMapper, JsonKeys.DESTINATION), userId);
    }
    return null;
  }

  public static JSONObject mapperUserDestinationPreferencesToJSONObject(
      UserDestinationPreferences userDestinationPreferences) throws JSONException {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(JsonKeys.ID, userDestinationPreferences.getUserDestinationPreferencesId());
    if (userDestinationPreferences.getDestination() != null) {
      jsonObject.put(JsonKeys.DESTINATION, userDestinationPreferences.getDestination());
    }
    return jsonObject;
  }

  public static JSONArray getUserDestinationPreferencesListToJson(
      List<UserDestinationPreferences> userDestinationPreferencesList) throws JSONException {
    JSONArray jsonArray = new JSONArray();
    if (userDestinationPreferencesList != null) {
      for (int indexAP = 0; indexAP < userDestinationPreferencesList.size(); indexAP++) {
        if (userDestinationPreferencesList.get(indexAP) != null) {
          jsonArray.put(mapperUserDestinationPreferencesToJSONObject(
              userDestinationPreferencesList.get(indexAP)));
        }
      }
    }
    return jsonArray;
  }
}
