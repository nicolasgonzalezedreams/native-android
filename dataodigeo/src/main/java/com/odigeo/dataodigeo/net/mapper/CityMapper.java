package com.odigeo.dataodigeo.net.mapper;

import android.util.Log;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.entity.geo.Coordinates;
import com.odigeo.data.entity.geo.LocationDescriptionType;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 3/05/16
 */
public class CityMapper {

  private static final String TYPE_AIRPORT = "AIRPORT";
  private static final String TYPE_CITY = "CITY";
  private static final String TYPE_TRAIN_STATION = "TRAIN_STATION";
  private static final String TYPE_IATA_CODE = "IATA_CODE";
  private static final String TYPE_BUS_STATION = "BUS_STATION";

  private static final String CITIES = "cities";
  private static final String NAME = "name";
  private static final String TYPE = "type";
  private static final String GEO_NODE_ID = "geoNodeId";
  private static final String IATA_CODE = "iataCode";
  private static final String CITY_NAME = "cityName";
  private static final String COUNTRY_CODE = "countryCode";
  private static final String COUNTRY_NAME = "countryName";
  private static final String RELATED_LOCATIONS = "relatedLocations";
  private static final String LOCATION_NAMES = "locationNames";
  private static final String COORDINATES = "coordinates";
  private static final String LATITUDE = "latitude";
  private static final String LONGITUDE = "longitude";

  public List<City> parseCities(String json) {
    List<City> cities = null;
    if (json != null) {
      try {
        JSONArray root = new JSONObject(json).getJSONArray(CITIES);
        cities = extractCities(root);
      } catch (JSONException e) {
        Log.e(CITIES, e.getMessage(), e);
      }
    }
    return cities;
  }

  private List<City> extractCities(JSONArray jsonArray) throws JSONException {
    int size = jsonArray.length();
    int limit = size / 2;
    int index;
    int inverseIndex;
    boolean isSymmetric = size % 2 == 0;
    JSONObject jsonObject;
    JSONObject jsonObjectEnd;
    List<City> cities = new ArrayList<>(size);
    for (index = 0; index < limit; index++) {
      inverseIndex = size - index - 1;
      jsonObject = jsonArray.getJSONObject(index);
      jsonObjectEnd = jsonArray.getJSONObject(inverseIndex);
      City city = extractCity(jsonObject);
      City cityEnd = extractCity(jsonObjectEnd);
      cities.add(index, city);
      cities.add(cities.size() - index, cityEnd);
    }
    if (!isSymmetric) {
      jsonObject = jsonArray.getJSONObject(index);
      City city = extractCity(jsonObject);
      cities.add(index, city);
    }
    return cities;
  }

  private City extractCity(JSONObject jsonObject) throws JSONException {
    City city = new City();
    city.setName(jsonObject.getString(NAME));
    city.setGeoNodeId(jsonObject.getInt(GEO_NODE_ID));
    city.setIataCode(jsonObject.getString(IATA_CODE));
    city.setCityName(jsonObject.getString(CITY_NAME));
    city.setCountryCode(jsonObject.getString(COUNTRY_CODE));
    city.setCountryName(jsonObject.getString(COUNTRY_NAME));

    extractType(jsonObject, city);
    extractCoordinates(jsonObject, city);
    extractLocationNames(jsonObject, city);
    extractRelatedLocations(jsonObject, city);
    return city;
  }

  private void extractRelatedLocations(JSONObject jsonObject, City city) throws JSONException {
    List<City> relatedLocations = new ArrayList<>();
    if (!jsonObject.isNull(RELATED_LOCATIONS)) {
      JSONArray jsonArray = jsonObject.getJSONArray(RELATED_LOCATIONS);
      int size = jsonArray.length();
      for (int index = 0; index < size; index++) {
        JSONObject child = jsonArray.getJSONObject(index);
        relatedLocations.add(extractCity(child));
      }
      city.setRelatedLocations(relatedLocations);
    }
  }

  private void extractLocationNames(JSONObject jsonObject, City city) throws JSONException {
    List<String> locationNames = new ArrayList<>();
    JSONArray jsonArray = jsonObject.getJSONArray(LOCATION_NAMES);
    int size = jsonArray.length();
    for (int index = 0; index < size; index++) {
      locationNames.add(jsonArray.getString(index));
    }
    city.setLocationNames(locationNames);
  }

  private void extractCoordinates(JSONObject jsonObject, City city) throws JSONException {
    JSONObject jsonCoordinates = jsonObject.getJSONObject(COORDINATES);

    Coordinates coordinates =
        new Coordinates(jsonCoordinates.getDouble(LATITUDE), jsonCoordinates.getDouble(LONGITUDE));
    city.setCoordinates(coordinates);
  }

  private void extractType(JSONObject jsonObject, City city) throws JSONException {
    String type = jsonObject.getString(TYPE);
    switch (type) {
      case TYPE_AIRPORT:
        city.setType(LocationDescriptionType.AIRPORT);
        break;
      case TYPE_BUS_STATION:
        city.setType(LocationDescriptionType.BUS_STATION);
        break;
      case TYPE_CITY:
        city.setType(LocationDescriptionType.CITY);
        break;
      case TYPE_IATA_CODE:
        city.setType(LocationDescriptionType.IATA_CODE);
        break;
      case TYPE_TRAIN_STATION:
        city.setType(LocationDescriptionType.TRAIN_STATION);
    }
  }
}
