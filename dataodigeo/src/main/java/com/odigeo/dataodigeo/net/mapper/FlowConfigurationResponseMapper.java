package com.odigeo.dataodigeo.net.mapper;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.odigeo.data.entity.shoppingCart.FlowConfigurationResponse;

public class FlowConfigurationResponseMapper {

  private Gson mGson;

  public FlowConfigurationResponseMapper() {
    mGson = new Gson();
  }

  public FlowConfigurationResponse parseFlowConfigurationResponse(
      String flowConfigurationResponseJsonString) throws JsonSyntaxException {
    return mGson.fromJson(flowConfigurationResponseJsonString, FlowConfigurationResponse.class);
  }
}