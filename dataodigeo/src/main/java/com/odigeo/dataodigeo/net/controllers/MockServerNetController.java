package com.odigeo.dataodigeo.net.controllers;

import com.android.volley.toolbox.RequestFuture;
import com.odigeo.dataodigeo.net.helper.MslSyncRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import odigeo.dataodigeo.BuildConfig;
import org.json.JSONException;
import org.json.JSONObject;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.DELETE;
import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.GET;
import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.PUT;

public class MockServerNetController {

  private final static String DEFAULT_PORT = "8080";

  private RequestHelper mRequestHelper;

  public MockServerNetController(RequestHelper requestHelper) {
    mRequestHelper = requestHelper;
  }

  public void addStep(String step, String response)
      throws JSONException, ExecutionException, InterruptedException {
    String url = BuildConfig.MOCK_SERVER_HOST + DEFAULT_PORT + "/steps/" + step;
    RequestFuture future = RequestFuture.newFuture();
    MslSyncRequest<Void> addStepRequest = new MslSyncRequest<>(PUT, url, future);

    JSONObject body = new JSONObject();

    body.put("response", response);
    HashMap<String, String> headers = new HashMap<>();
    headers.put("Content-Type", "application/json");
    addStepRequest.setHeaders(headers);
    addStepRequest.setBody(body.toString());
    mRequestHelper.addSyncRequest(addStepRequest);
    future.get();
  }

  public String getPort() throws ExecutionException, InterruptedException, JSONException {
    String url = BuildConfig.MOCK_SERVER_HOST + DEFAULT_PORT + "/port";
    RequestFuture<String> future = RequestFuture.newFuture();
    MslSyncRequest<String> getPortRequest = new MslSyncRequest<>(GET, url, future);
    mRequestHelper.addSyncRequest(getPortRequest);
    JSONObject jsonResponse = new JSONObject(future.get());
    return jsonResponse.getString("port");
  }

  public void resetMockServer() throws ExecutionException, InterruptedException {
    String url = BuildConfig.MOCK_SERVER_HOST + DEFAULT_PORT + "/steps";
    RequestFuture future = RequestFuture.newFuture();
    MslSyncRequest<Void> resetMockServerRequest = new MslSyncRequest<>(DELETE, url, future);
    mRequestHelper.addSyncRequest(resetMockServerRequest);
    future.get();
  }

  public void invalidateCaches() {
    mRequestHelper.cleanCache();
  }
}
