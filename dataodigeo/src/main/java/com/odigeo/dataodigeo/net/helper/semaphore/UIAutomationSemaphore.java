package com.odigeo.dataodigeo.net.helper.semaphore;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import com.android.volley.RequestQueue;

/**
 * Created by Javier Marsicano on 26/10/16.
 */
public class UIAutomationSemaphore {
  private static UIAutomationSemaphore sInstance;
  private static RequestQueue sRequestQueue;
  private static boolean sVerbose = false;
  private OdigeoIdlingResource mIdlingResource;

  private UIAutomationSemaphore(@NonNull RequestQueue requestQueue) {
    mIdlingResource = new OdigeoIdlingResource(requestQueue, sVerbose);
  }

  public static UIAutomationSemaphore getInstance() {
    if (sInstance == null) {
      sInstance = new UIAutomationSemaphore(sRequestQueue);
    }
    return sInstance;
  }

  public static void init(@NonNull RequestQueue requestQueue) {
    sRequestQueue = requestQueue;
  }

  public static void init(@NonNull RequestQueue requestQueue, boolean verbose) {
    sRequestQueue = requestQueue;
    sVerbose = verbose;
  }

  public synchronized void idleUp() {
    mIdlingResource.increment();
  }

  public synchronized void idleDown() {
    mIdlingResource.decrement();
  }

  public void reset() {
    mIdlingResource.reset();
  }

  @VisibleForTesting public IdlingResource getIdlingResource() {
    return mIdlingResource;
  }
}
