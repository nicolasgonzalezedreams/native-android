package com.odigeo.dataodigeo.net.mapper;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.odigeo.data.entity.shoppingCart.AvailableProductsResponse;

public class AvailableProductsResponseMapper {

  private Gson mGson;

  public AvailableProductsResponseMapper() {
    mGson = new Gson();
  }

  public AvailableProductsResponse parseAvailableProductsResponse(
      String availableProductsResponseJsonString) throws JsonSyntaxException {
    AvailableProductsResponse availableProductsResponse;
    availableProductsResponse =
        mGson.fromJson(availableProductsResponseJsonString, AvailableProductsResponse.class);
    return availableProductsResponse;
  }
}
