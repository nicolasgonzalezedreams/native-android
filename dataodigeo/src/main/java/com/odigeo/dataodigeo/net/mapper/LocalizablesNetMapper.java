package com.odigeo.dataodigeo.net.mapper;

import com.odigeo.data.entity.localizables.LocalizablesResponse;
import com.odigeo.data.entity.localizables.OdigeoLocalizable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class LocalizablesNetMapper {

  public static LocalizablesResponse parseLocalizablesJSONToResponse(JSONObject localizables)
      throws JSONException {
    Iterator keys = localizables.keys();
    LocalizablesResponse localizablesResponse = new LocalizablesResponse();
    List<OdigeoLocalizable> localizableList = new ArrayList<>();
    String dynamicKey, value;
    OdigeoLocalizable odigeoLocalizable;

    while (keys.hasNext()) {
      dynamicKey = (String) keys.next();
      value = localizables.getString(dynamicKey);
      odigeoLocalizable = new OdigeoLocalizable();
      odigeoLocalizable.setLocalizableKey(dynamicKey);
      odigeoLocalizable.setLocalizableValue(value);
      localizableList.add(odigeoLocalizable);
    }

    localizablesResponse.setLocalizables(localizableList);

    return localizablesResponse;
  }
}
