package com.odigeo.dataodigeo.net.mapper;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;

public class CreateShoppingCartResponseMapper {

  private Gson mGson;

  public CreateShoppingCartResponseMapper() {
    mGson = new Gson();
  }

  public CreateShoppingCartResponse parseShoppingCart(String shoppingCartJsonString)
      throws JsonSyntaxException {
    CreateShoppingCartResponse shoppingCart;
    shoppingCart = mGson.fromJson(shoppingCartJsonString, CreateShoppingCartResponse.class);
    return shoppingCart;
  }
}
