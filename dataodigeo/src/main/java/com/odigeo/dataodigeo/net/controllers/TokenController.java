package com.odigeo.dataodigeo.net.controllers;

import android.content.Context;
import com.facebook.AccessToken;
import com.facebook.FacebookException;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.odigeo.dataodigeo.session.CredentialsInterface;
import com.odigeo.dataodigeo.session.SessionController;
import java.io.IOException;

import static com.odigeo.dataodigeo.net.controllers.AuthToken.EMPTY_TOKEN;
import static com.odigeo.dataodigeo.net.controllers.AuthToken.INVALID_TOKEN;
import static com.odigeo.dataodigeo.session.CredentialsInterface.CredentialsType.PASSWORD;

public class TokenController {

  private final AuthToken mAuthToken;
  private SessionController mSessionController;
  private CredentialsInterface credentials;
  private Context mContext;

  public TokenController(SessionController sessionController, Context context,
      AuthToken authToken) {
    mSessionController = sessionController;
    mContext = context;
    mAuthToken = authToken;
  }

  public String buildAuthHeader() throws InterruptedException, GoogleAuthException, IOException {
    credentials = mSessionController.getCredentials();
    return credentials == null ? buildHeaderWhenNoLogged() : buildHeaderWhenLogged();
  }

  private String buildHeaderWhenNoLogged() {
    return null;
  }

  private String buildHeaderWhenLogged()
      throws InterruptedException, GoogleAuthException, IOException {
    String tokenType = credentials.getType().equals(PASSWORD) ? "password" : "token";
    return "{\"type\":\""
        + credentials.getCredentialTypeValue()
        + "\",\"login\":\""
        + credentials.getUser()
        + "\",\""
        + tokenType
        + "\":\""
        + getToken()
        + "\"}";
  }

  private synchronized String getToken()
      throws InterruptedException, IOException, GoogleAuthException {

    switch (credentials.getType()) {
      case FACEBOOK:
        getFacebookToken();
        break;
      case GOOGLE:
        getGoogleToken();
        break;
      case PASSWORD:
        getPassword();
        break;
    }

    if (EMPTY_TOKEN.equals(mAuthToken.getToken())) {
      synchronized (mAuthToken) {
        mAuthToken.wait();
      }
    }

    return mAuthToken.getToken();
  }

  private void getFacebookToken() throws InterruptedException {
    try {
      mAuthToken.setToken(AccessToken.getCurrentAccessToken().getToken());
      mAuthToken.setExpiresAt(AccessToken.getCurrentAccessToken().getExpires().getTime());
      if (refreshTokenNeeded()) {
        mAuthToken.setToken(EMPTY_TOKEN);
        refreshFacebookToken();
      }
    } catch (NullPointerException e) {
      mAuthToken.setToken(INVALID_TOKEN);
      mAuthToken.setExpiresAt(0);
    }
  }

  private boolean refreshTokenNeeded() {
    return System.currentTimeMillis() > mAuthToken.getExpiresAt();
  }

  private synchronized void refreshFacebookToken() throws InterruptedException {
    AccessToken.refreshCurrentAccessTokenAsync(new AccessToken.AccessTokenRefreshCallback() {
      @Override public void OnTokenRefreshed(AccessToken accessToken) {
        mAuthToken.setToken(accessToken.getToken());
        mAuthToken.setExpiresAt(accessToken.getExpires().getTime());
        synchronized (mAuthToken) {
          mAuthToken.notify();
        }
      }

      @Override public void OnTokenRefreshFailed(FacebookException exception) {
        synchronized (mAuthToken) {
          mAuthToken.notify();
        }
      }
    });
  }

  private void getGoogleToken() throws GoogleAuthException, IOException {
    mAuthToken.setToken(GoogleAuthUtil.getToken(mContext, credentials.getUser(),
        GooglePlusController.AUTHENTICATION_TYPE + GooglePlusController.PROFILE_URL));
    mAuthToken.setExpiresAt(0);
  }

  private void getPassword() {
    mAuthToken.setToken(credentials.getPassword());
    mAuthToken.setExpiresAt(0);
  }
}
