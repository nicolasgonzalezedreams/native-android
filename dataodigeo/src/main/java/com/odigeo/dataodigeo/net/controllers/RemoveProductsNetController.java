package com.odigeo.dataodigeo.net.controllers;

import com.google.gson.Gson;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.request.ModifyShoppingCartRequest;
import com.odigeo.data.net.controllers.RemoveProductsNetControllerInterface;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.CustomRequestMethod;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.HashMap;

public class RemoveProductsNetController implements RemoveProductsNetControllerInterface {

  private static final String URL_REMOVE_PRODUCT = "removeProduct";

  private RequestHelper mRequestHelper;
  private DomainHelperInterface mDomainHelper;
  private HeaderHelperInterface mHeaderHelper;
  private Gson mGson;

  public RemoveProductsNetController(RequestHelper requestHelper,
      DomainHelperInterface domainHelper, HeaderHelperInterface headerHelper, Gson gson) {
    mRequestHelper = requestHelper;
    mDomainHelper = domainHelper;
    mHeaderHelper = headerHelper;
    mGson = gson;
  }

  @Override public void removeProductsFromShoppingCart(
      final OnRequestDataListener<CreateShoppingCartResponse> listener,
      ModifyShoppingCartRequest modifyShoppingCartRequest) {
    String url = mDomainHelper.getUrl() + URL_REMOVE_PRODUCT;
    MslRequest<String> removeInsurancesRequest =
        new MslRequest<>(CustomRequestMethod.POST, url, new OnRequestDataListener<String>() {
          @Override public void onResponse(String shoppingCartResponse) {
            CreateShoppingCartResponse createShoppingCartResponse =
                mGson.fromJson(shoppingCartResponse, CreateShoppingCartResponse.class);
            listener.onResponse(createShoppingCartResponse);
          }

          @Override public void onError(MslError error, String message) {
            listener.onError(error, message);
          }
        });

    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAccept(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    removeInsurancesRequest.setHeaders(mapHeaders);
    removeInsurancesRequest.setBody(mGson.toJson(modifyShoppingCartRequest));
    mRequestHelper.addRequest(removeInsurancesRequest);
  }
}