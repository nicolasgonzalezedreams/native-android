package com.odigeo.dataodigeo.net.controllers;

import android.util.Log;
import com.odigeo.data.entity.booking.SearchSegment;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.net.controllers.SearchesNetControllerInterface;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.helper.MslAuthRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import com.odigeo.dataodigeo.net.mapper.DataNetMapper;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.DELETE;
import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.POST;
import static com.odigeo.dataodigeo.net.mapper.DataNetMapper.DataType.JSON;

/**
 * Created by matias.dirusso on 25/7/2016.
 */
public class SearchesNetController implements SearchesNetControllerInterface {
  private RequestHelper mRequestHelper;
  private DomainHelperInterface mDomainHelper;
  private HeaderHelperInterface mHeaderHelper;
  private TokenController mTokenController;

  public SearchesNetController(RequestHelper requestHelper, DomainHelperInterface domainHelper,
      HeaderHelperInterface headerHelper, TokenController tokenController) {

    mRequestHelper = requestHelper;
    mDomainHelper = domainHelper;
    mHeaderHelper = headerHelper;
    mTokenController = tokenController;
  }

  @Override public void postUserSearch(OnRequestDataListener<StoredSearch> listener,
      final StoredSearch storedSearch) {
    String url = mDomainHelper.getUrl() + API_URL_USER + "/0/" + API_URL_SEARCH;
    final MslAuthRequest<StoredSearch> postUserSearchRequest =
        new MslAuthRequest<>(POST, url, listener, new DataNetMapper(StoredSearch.class, JSON),
            mTokenController);
    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAccept(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    postUserSearchRequest.setHeaders(mapHeaders);
    JSONObject body = new JSONObject();
    try {
      body.put(JsonKeys.NUM_ADULTS, storedSearch.getNumAdults());
      body.put(JsonKeys.NUM_CHILDREN, storedSearch.getNumChildren());
      body.put(JsonKeys.NUM_INFANTS, storedSearch.getNumInfants());
      body.put(JsonKeys.IS_DIRECT_FLIGHT, storedSearch.getIsDirectFlight());
      if (!storedSearch.getCabinClass().equals(StoredSearch.CabinClass.UNKNOWN)) {
        body.put(JsonKeys.CABIN_CLASS, storedSearch.getCabinClass().toString());
      }
      if (!storedSearch.getTripType().equals(StoredSearch.TripType.UNKNOWN)) {
        body.put(JsonKeys.TRIP_TYPE, storedSearch.getTripType().toString());
      }
      JSONArray searchSegmentsJson = new JSONArray();
      for (SearchSegment searchSegment : storedSearch.getSegmentList()) {
        JSONObject segmentJson = new JSONObject();
        segmentJson.put(JsonKeys.ORIGIN_IATA_CODE, searchSegment.getOriginIATACode());
        segmentJson.put(JsonKeys.DEPARTURE_DATE, searchSegment.getDepartureDate());
        segmentJson.put(JsonKeys.DESTINATION_IATA_CODE, searchSegment.getDestinationIATACode());
        segmentJson.put(JsonKeys.SEGMENT_ORDER, searchSegment.getSegmentOrder());
        searchSegmentsJson.put(segmentJson);
      }
      body.put(JsonKeys.SEARCH_SEGMENT_LIST, searchSegmentsJson);
      postUserSearchRequest.setBody(body.toString());
    } catch (JSONException e) {
      Log.e("UserNetController", e.getMessage());
    }
    mRequestHelper.addRequest(postUserSearchRequest);
  }

  @Override
  public void deleteUserSearch(OnRequestDataListener<Boolean> listener, StoredSearch storedSearch) {
    String url = mDomainHelper.getUrl()
        + API_URL_USER
        + "/0/"
        + API_URL_SEARCH
        + "/"
        + storedSearch.getStoredSearchId();
    final MslAuthRequest<Boolean> deleteUserSearchesRequest =
        new MslAuthRequest<>(DELETE, url, listener, new DataNetMapper(Boolean.class, JSON),
            mTokenController);
    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putDeviceId(mapHeaders);
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAccept(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    deleteUserSearchesRequest.setHeaders(mapHeaders);
    mRequestHelper.addRequest(deleteUserSearchesRequest);
  }
}
