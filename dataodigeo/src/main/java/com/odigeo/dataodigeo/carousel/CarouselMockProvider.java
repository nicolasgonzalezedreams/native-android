package com.odigeo.dataodigeo.carousel;

public class CarouselMockProvider {

  /* Provide a carousel with all the cards */
  public static String provideFullMockCarousel() {

    return
        "{ \"lastUpdate\":1453731268621, \"cards\":[ { \"type\":\"SECTION\", \"from\":{ \"date\":1464272700000, \"description\":\"PalmadeMaiorca\", "
            + "\"iata\":\"PMI\", \"terminal\":null }, \"carrier\":\"UX\", \"to\":{ \"date\":1464275700000, \"description\":\"Barcelona\", "
            + "\"iata\":\"BCN\", "
            + "\"terminal\":null }, \"flightId\":\"6106\", \"enrichedBooking\":\"true\" ,\"carrierName\":\"AirEuropa\", "
            + "\"id\":\"-1933816910\", \"priority\":0, \"image\":\"http://www.edreams"
            + ".com/images/mobile/govoyages/destination/600x440/MAD@2x.jpg\" }, "
            + "{ \"type\":\"DELAYED_SECTION\", \"departureDelay\":\"30\", \"arrivalDelay\":\"23\", \"from\":{ \"date\":1464272700000, "
            + "\"description\":\"PalmadeMaiorca\", "
            + "\"iata\":\"PMI\", \"terminal\":null }, \"carrier\":\"UX\", \"to\":{ \"date\":1464275700000, \"description\":\"Barcelona\", "
            + "\"iata\":\"BCN\", "
            + "\"terminal\":null }, \"flightId\":\"6106\", \"carrierName\":\"AirEuropa\", \"id\":\"1322840758\", \"priority\":0, "
            + "\"image\":\"http://www.edreams.com/images/mobile/govoyages/destination/600x440/BCN@2x.jpg\" }, "
            + "{ \"type\":\"DIVERTED_SECTION\", \"from\":{ \"date\":1464272700000, \"description\":\"PalmadeMaiorca\", \"iata\":\"PMI\", "
            + "\"terminal\":null }, "
            + "\"carrier\":\"UX\", \"to\":{ \"date\":1464275700000, \"description\":\"Barcelona\", \"iata\":\"BCN\", \"terminal\":null }, "
            + "\"flightId\":\"6106\", \"carrierName\":\"AirEuropa\", \"id\":\"-1933816910\", \"priority\":0, \"image\":\"http://www"
            + ".edreams.com/images/mobile/govoyages/destination/600x440/MAD@2x.jpg\" }, { \"type\":\"CANCELLED_SECTION\", "
            + "\"from\":{ \"date\":1464272700000, \"description\":\"PalmadeMaiorca\", \"iata\":\"PMI\", \"terminal\":null }, "
            + "\"carrier\":\"UX\", "
            + "\"to\":{ \"date\":1464275700000, \"description\":\"Barcelona\", \"iata\":\"BCN\", \"terminal\":null }, \"flightId\":\"6106\", "
            + "\"carrierName\":\"AirEuropa\", \"id\":\"-1933816910\", \"priority\":0, \"image\":\"http://www.edreams"
            + ".com/images/mobile/govoyages/destination/600x440/MAD@2x.jpg\" }, { \"type\":\"BAGGAGE_BELT\", "
            + "\"from\":{ \"date\":1464272700000, \"description\":\"PalmadeMaiorca\", \"iata\":\"PMI\", \"terminal\":null }, "
            + "\"carrier\":\"UX\", "
            + "\"to\":{ \"date\":1464275700000, \"description\":\"Barcelona\", \"iata\":\"BCN\", \"terminal\":null }, \"flightId\":\"6106\", "
            + "\"carrierName\":\"AirEuropa\", \"id\":\"-1933816910\", \"priority\":0, \"image\":\"http://www.edreams"
            + ".com/images/mobile/govoyages/destination/600x440/MAD@2x.jpg\", \"baggageBelt\":null }, { \"type\":\"BOARDING_GATE\", "
            + "\"from\":{ \"date\":1464272700000, \"description\":\"PalmadeMaiorca\", \"iata\":\"PMI\", \"terminal\":null }, "
            + "\"carrier\":\"UX\", "
            + "\"to\":{ \"date\":1464275700000, \"description\":\"Barcelona\", \"iata\":\"BCN\", \"terminal\":null }, \"flightId\":\"6106\", "
            + "\"carrierName\":\"AirEuropa\", \"id\":\"-1933816910\", \"priority\":0, \"image\":\"http://www.edreams"
            + ".com/images/mobile/govoyages/destination/600x440/JFK@2x.jpg\", \"gate\":null }, { \"type\":\"PROMOTION\", "
            + "\"priority\":0, \"image\":\"http://www.edreams.com/images/mobile/govoyages/destination/600x440/MAD@2x.jpg\", \"id\":\"1322840758\", \"title\":\"Mockpromotion\", \"subtitle\":\"Mocksubtitlepromotion\" } ] }";
  }

  /* Provide a carousel with a Canceled Flight */
  public static String provideCanceledMockCarousel() {

    return
        "{ \"lastUpdate\": 1453731268621, \"cards\": [ { \"type\": \"CANCELLED_SECTION\", \"from\": { \"date\": 1464272700000, \"description\": "
            + "\"Palma de Maiorca\", \"iata\": \"PMI\", \"terminal\": null }, \"carrier\": \"UX\", \"to\": { \"date\": 1464275700000, \"description\": "
            + "\"Barcelona\", \"iata\": \"BCN\", \"terminal\": null }, \"flightId\": \"6106\", \"carrierName\": \"Air Europa\", \"id\": \"-1933816910\", "
            + "\"priority\": 0, \"image\": null }, { \"type\": \"PROMOTION\", \"priority\": 0, \"image\": null, \"id\": \"1322840758\", \"title\": "
            + "\"Mock promotion\", \"subtitle\": \"Mock subtitle promotion\" } ] }";
  }

  /* Provide a carousel with a Diverted Flight */
  public static String provideDivertedFlightMockCarousel() {

    return
        "{ \"lastUpdate\": 1453731268621, \"cards\": [ { \"type\": \"DIVERTED_SECTION\", \"from\": { \"date\": 1464272700000, \"description\": "
            + "\"Palma de Maiorca\", \"iata\": \"PMI\", \"terminal\": null }, \"carrier\": \"UX\", \"to\": { \"date\": 1464275700000, \"description\": "
            + "\"Barcelona\", \"iata\": \"BCN\", \"terminal\": null }, \"flightId\": \"6106\", \"carrierName\": \"Air Europa\", \"id\": \"-1933816910\", "
            + "\"priority\": 0, \"image\": null }, { \"type\": \"BAGGAGE_BELT\", \"from\": { \"date\": 1464272700000, \"description\": \"Palma de Maiorca\","
            + " \"iata\": \"PMI\", \"terminal\": null }, \"carrier\": \"UX\", \"to\": { \"date\": 1464275700000, \"description\": \"Barcelona\", \"iata\": "
            + "\"BCN\", \"terminal\": null }, \"flightId\": \"6106\", \"carrierName\": \"Air Europa\", \"id\": \"-1933816910\", \"priority\": 0, "
            + "\"image\": null, \"baggageBelt\": null }, { \"type\": \"BOARDING_GATE\", \"from\": { \"date\": 1464272700000, \"description\": \"Palma de Maiorca\", "
            + "\"iata\": \"PMI\", \"terminal\": null }, \"carrier\": \"UX\", \"to\": { \"date\": 1464275700000, \"description\": \"Barcelona\", \"iata\": "
            + "\"BCN\", \"terminal\": null }, \"flightId\": \"6106\", \"carrierName\": \"Air Europa\", \"id\": \"-1933816910\", \"priority\": 0, "
            + "\"image\": null, \"gate\": null }, { \"type\": \"PROMOTION\", \"priority\": 0, \"image\": null, \"id\": \"1322840758\", \"title\": "
            + "\"Mock promotion\", \"subtitle\": \"Mock subtitle promotion\" } ] }";
  }

  /* Provide a carousel with a Delayed Flight */
  public static String provideDelayedFlightMockCarousel() {

    return
        "{ \"lastUpdate\": 1453731268621, \"cards\": [ { \"type\": \"DELAYED_SECTION\", \"departureDelay\": \"30\", \"arrivalDelay\": "
            + "\"23\", \"from\": { \"date\": 1464272700000, \"description\": \"Palma de Maiorca\", \"iata\": \"PMI\", \"terminal\": null },"
            + " \"carrier\": \"UX\", \"to\": { \"date\": 1464275700000, \"description\": \"Barcelona\", \"iata\": \"BCN\", \"terminal\": null }, "
            + "\"flightId\": \"6106\", \"carrierName\": \"Air Europa\", \"id\": \"1322840758\", \"priority\": 0, \"image\": null }, { \"type\": "
            + "\"BAGGAGE_BELT\", \"from\": { \"date\": 1464272700000, \"description\": \"Palma de Maiorca\", \"iata\": \"PMI\", \"terminal\": null }, "
            + "\"carrier\": \"UX\", \"to\": { \"date\": 1464275700000, \"description\": \"Barcelona\", \"iata\": \"BCN\", \"terminal\": null }, "
            + "\"flightId\": \"6106\", \"carrierName\": \"Air Europa\", \"id\": \"-1933816910\", \"priority\": 0, \"image\": null, \"baggageBelt\": null }, "
            + "{ \"type\": \"BOARDING_GATE\", \"from\": { \"date\": 1464272700000, \"description\": \"Palma de Maiorca\", \"iata\": \"PMI\", "
            + "\"terminal\": null }, \"carrier\": \"UX\", \"to\": { \"date\": 1464275700000, \"description\": \"Barcelona\", \"iata\": \"BCN\","
            + " \"terminal\": null }, \"flightId\": \"6106\", \"carrierName\": \"Air Europa\", \"id\": \"-1933816910\", \"priority\": 0, \"image\": null, "
            + "\"gate\": null }, { \"type\": \"PROMOTION\", \"priority\": 0, \"image\": null, \"id\": \"1322840758\", \"title\": \"Mock promotion\", "
            + "\"subtitle\": \"Mock subtitle promotion\" } ] }";
  }

  /* Provide a carousel with a Normal Flight */
  public static String provideNormalFlightMockCarousel() {

    return
        "{ \"lastUpdate\": 1453731268621, \"cards\": [ { \"type\": \"SECTION\", \"from\": { \"date\": 1464272700000, \"description\": "
            + "\"Palma de Maiorca\", \"iata\": \"PMI\", \"terminal\": null }, \"carrier\": \"UX\", \"to\": { \"date\": 1464275700000, \"description\": "
            + "\"Barcelona\", \"iata\": \"BCN\", \"terminal\": null }, \"flightId\": \"6106\", \"carrierName\": \"Air Europa\", \"id\": \"-1933816910\", "
            + "\"priority\": 0, \"image\": null }, { \"type\": \"BAGGAGE_BELT\", \"from\": { \"date\": 1464272700000, \"description\": \"Palma de Maiorca\", "
            + "\"iata\": \"PMI\", \"terminal\": null }, \"carrier\": \"UX\", \"to\": { \"date\": 1464275700000, \"description\": \"Barcelona\", "
            + "\"iata\": \"BCN\", \"terminal\": null }, \"flightId\": \"6106\", \"carrierName\": \"Air Europa\", \"id\": \"-1933816910\", \"priority\": 0, "
            + "\"image\": null, \"baggageBelt\": null }, { \"type\": \"BOARDING_GATE\", \"from\": { \"date\": 1464272700000, \"description\": \"Palma de Maiorca\", "
            + "\"iata\": \"PMI\", \"terminal\": null }, \"carrier\": \"UX\", \"to\": { \"date\": 1464275700000, \"description\": \"Barcelona\", "
            + "\"iata\": \"BCN\", \"terminal\": null }, \"flightId\": \"6106\", \"carrierName\": \"Air Europa\", \"id\": \"-1933816910\", \"priority\": 0, "
            + "\"image\": null, \"gate\": null }, { \"type\": \"PROMOTION\", \"priority\": 0, \"image\": null, \"id\": \"1322840758\", \"title\": "
            + "\"Mock promotion\", \"subtitle\": \"Mock subtitle promotion\" } ] }";
  }

  public static String provideMultiplePromosMockCarousel() {
    return "{ \"lastUpdate\":1453731268621, \"cards\":[{ \"type\":\"PROMOTION\", "
        + "\"priority\":0, \"image\":\"http://www.edreams.com/images/mobile/govoyages/destination/600x440/LAX@2x.jpg\", \"id\":\"1322840758\", \"title\":\"Mockpromotion\", \"subtitle\":\"Mocksubtitlepromotion\" },"
        + "{ \"type\":\"PROMOTION\", "
        + "\"priority\":0, \"image\":\"\", \"id\":\"1322840758\", \"title\":\"Mockpromotion\", \"subtitle\":\"Mocksubtitlepromotion\" },"
        + "{ \"type\":\"PROMOTION\", "
        + "\"priority\":0, \"image\":\"http://www.edreams.com/images/mobile/govoyages/destination/600x440/MAD@2x.jpg\", \"id\":\"1322840758\", \"title\":\"Mockpromotion\", \"subtitle\":\"Mocksubtitlepromotion\" },"
        + "{ \"type\":\"PROMOTION\", "
        + "\"priority\":0, \"image\":\"http://www.edreams.com/images/mobile/govoyages/destination/600x440/MAD@2x.jpg\", \"id\":\"1322840758\", \"title\":\"Mockpromotion\", \"subtitle\":\"Mocksubtitlepromotion\" },"
        + "{ \"type\":\"PROMOTION\", "
        + "\"priority\":0, \"image\":\"http://www.edreams.com/images/mobile/govoyages/destination/600x440/BER@2x.jpg\", \"id\":\"1322840758\", \"title\":\"Mockpromotion\", \"subtitle\":\"Mocksubtitlepromotion\" },"
        + "{ \"type\":\"PROMOTION\", "
        + "\"priority\":0, \"image\":\"http://www.edreams.com/images/mobile/govoyages/destination/600x440/JFK@2x.jpg\", \"id\":\"1322840758\", \"title\":\"Mockpromotion\", \"subtitle\":\"Mocksubtitlepromotion\" }, ] }";
  }
}
