package com.odigeo.dataodigeo.location;

public interface LocationControllerListener {

  void onLocationReady();
}
