package com.odigeo.dataodigeo.location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.entity.geo.LocationDescriptionType;
import com.odigeo.data.net.error.MslError;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.odigeo.dataodigeo.net.helper.CustomRequestMethod.GET;

public class LocationController
    implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
    LocationListener {

  private static final String API_URL_NEAREST_LOCATION = "nearestLocations?";
  private static final String API_URL_LONGITUDE = "longitude=";
  private static final String API_URL_LATITUDE = "latitude=";
  private static final String API_URL_RADIUS_IN_KM = "radiusInKm=";
  private static final String API_URL_LOCALE = "locale=";
  private static final String API_URL_PRODUCT_TYPE = "productType=";
  private static final String PRODUCT_TYPE_FLIGHT = "FLIGHT";
  private static LocationController instance;
  private final Context mContext;
  private Location mLocation;
  private GoogleApiClient mGoogleApiClient;
  private RequestHelper mRequestHelper;
  private City mNearestCity;
  private List<City> mNearestPlacesList = new ArrayList<>();
  private DomainHelperInterface mDomainHelper;
  private HeaderHelperInterface mHeaderHelper;
  private String mLocale;
  private LocationControllerListener mLocationControllerListener;
  private boolean isRequestingLocationUpdates = false;

  private LocationController(Context context, RequestHelper requestHelper,
      DomainHelperInterface domainHelper, HeaderHelperInterface headerHelper, String locale) {
    mContext = context.getApplicationContext();
    mGoogleApiClient =
        new GoogleApiClient.Builder(context.getApplicationContext()).addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build();
    mRequestHelper = requestHelper;
    mDomainHelper = domainHelper;
    mHeaderHelper = headerHelper;
    mLocale = locale;
  }

  public static LocationController getInstance(Context context, RequestHelper requestHelper,
      DomainHelperInterface domainHelper, HeaderHelperInterface headerHelper, String locale) {
    if (instance == null) {
      instance = new LocationController(context, requestHelper, domainHelper, headerHelper, locale);
    }
    return instance;
  }

  public void connect() {
    mGoogleApiClient.connect();
  }

  public void subscribe(LocationControllerListener locationControllerListener) {
    mLocationControllerListener = locationControllerListener;
  }

  public void unSubscribe() {
    this.mLocationControllerListener = null;
  }

  @SuppressWarnings("MissingPermission") @Override public void onConnected(Bundle bundle) {
    if (mLocation == null) {
      mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
      if (mLocation != null) {
        performNearestCitiesRequest();
      } else {
        startLocationUpdates();
      }
    }
  }

  @Override public void onConnectionSuspended(int i) {
  }

  @Override public void onConnectionFailed(ConnectionResult connectionResult) {
    mGoogleApiClient.connect();
  }

  @SuppressWarnings("MissingPermission") public void getNearestCitiesRequest() {
    mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

    if (mLocation == null) {
      if (!mGoogleApiClient.isConnected()) {
        mGoogleApiClient.connect();
      } else {
        startLocationUpdates();
      }
    } else {
      performNearestCitiesRequest();
    }
  }

  @SuppressWarnings("MissingPermission") public void startLocationUpdates() {

    if (!mGoogleApiClient.isConnected()) {
      mGoogleApiClient.connect();
      return;
    }

    if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
        == PackageManager.PERMISSION_GRANTED
        && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)
        == PackageManager.PERMISSION_GRANTED
        && !isRequestingLocationUpdates) {
      isRequestingLocationUpdates = true;
      LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
          new LocationRequest(), this);
    }
  }

  public void stopLocationUpdates() {
    if (mGoogleApiClient.isConnected() && isRequestingLocationUpdates) {
      LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
      isRequestingLocationUpdates = false;
    }
  }

  private void performNearestCitiesRequest() {
    String url = mDomainHelper.getUrl()
        + API_URL_NEAREST_LOCATION
        + API_URL_LONGITUDE
        + mLocation.getLongitude()
        + "&"
        + API_URL_LATITUDE
        + mLocation.getLatitude()
        + "&"
        + API_URL_RADIUS_IN_KM
        + 100
        + "&"
        + API_URL_LOCALE
        + mLocale
        + "&"
        + API_URL_PRODUCT_TYPE
        + PRODUCT_TYPE_FLIGHT;

    MslRequest<String> nearestCitiesRequest =
        new MslRequest<>(GET, url, new OnRequestDataListener<String>() {
          @Override public void onResponse(String object) {
            try {
              buildDestination(object);
            } catch (IOException e) {
              Log.e("LocationController", e.getMessage());
            }
          }

          @Override public void onError(MslError error, String message) {
            Log.e("LocationController", message);
          }
        });
    HashMap<String, String> mapHeaders = new HashMap<>();
    mHeaderHelper.putAcceptEncoding(mapHeaders);
    mHeaderHelper.putAccept(mapHeaders);
    mHeaderHelper.putContentType(mapHeaders);
    mHeaderHelper.putHeaderNoneMatchParam(mapHeaders);
    nearestCitiesRequest.setHeaders(mapHeaders);
    mRequestHelper.addRequest(nearestCitiesRequest);
  }

  private void buildDestination(String destinationString) throws IOException {
    TemporalDestination temporalDestination =
        new Gson().fromJson(destinationString, TemporalDestination.class);
    if (temporalDestination != null) {
      buildNearestCity(temporalDestination);
    } else {
      throw new IOException();
    }
  }

  private void buildNearestCity(TemporalDestination temporalDestination) {
    mNearestPlacesList = temporalDestination.getCities();
    City temporalNearesCity = null;
    for (City city : mNearestPlacesList) {
      Location locationCity = new Location("");
      locationCity.setLatitude(city.getCoordinates().getLatitude());
      locationCity.setLongitude(city.getCoordinates().getLongitude());
      city.setDistanceToCurrentLocation(calculateDistance(mLocation, locationCity));

      if (city.getType() == LocationDescriptionType.CITY && temporalNearesCity == null) {
        temporalNearesCity = city;
      } else if (city.getType() == LocationDescriptionType.CITY) {
        if (city.getDistanceToCurrentLocation()
            < temporalNearesCity.getDistanceToCurrentLocation()) {
          temporalNearesCity = city;
        }
      }
    }
    mNearestCity = temporalNearesCity;

    if (mLocationControllerListener != null) {
      mLocationControllerListener.onLocationReady();
    }
  }

  private float calculateDistance(Location locationFrom, Location locationTo) {
    float distanceInMeters = locationFrom.distanceTo(locationTo);
    return distanceInMeters / 1000f;
  }

  public City getNearestCity() {
    return mNearestCity;
  }

  public List<City> getNearestPlacesList() {
    return mNearestPlacesList;
  }

  public Location getCurrentLocation() {
    return mLocation;
  }

  @VisibleForTesting public void mockLocation(double latitude, double longitude) {
    mLocale = "en_US";
    mLocation = new Location("MockedLocation");
    long currentTime = System.currentTimeMillis();
    mLocation.setTime(currentTime);
    mLocation.setLatitude(latitude);
    mLocation.setLongitude(longitude);
    getNearestCitiesRequest();
  }

  @Override public void onLocationChanged(Location location) {
    stopLocationUpdates();

    mLocation = location;

    if (mNearestPlacesList.isEmpty()) {
      getNearestCitiesRequest();
    } else if (mLocationControllerListener != null) {
      mLocationControllerListener.onLocationReady();
    }

    if (mGoogleApiClient.isConnected()) {
      mGoogleApiClient.disconnect();
    }
  }
}
