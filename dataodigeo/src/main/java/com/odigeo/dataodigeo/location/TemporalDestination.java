package com.odigeo.dataodigeo.location;

import com.odigeo.data.entity.geo.City;
import java.util.List;

public class TemporalDestination {

  private List<City> cities;

  public final List<City> getCities() {
    return cities;
  }

  public final void setCities(List<City> cities) {
    this.cities = cities;
  }
}
