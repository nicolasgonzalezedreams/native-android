package com.odigeo.dataodigeo.welcome;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.odigeo.data.db.helper.TravellersHandlerInterface;
import com.odigeo.data.entity.userData.UserTraveller;
import java.util.List;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 19/09/16
 */

public class WelcomeProvider {
  private TravellersHandlerInterface mTravellers;

  public WelcomeProvider(TravellersHandlerInterface travellers) {
    mTravellers = travellers;
  }

  @Nullable public String getWelcomeMessage(String message) {
    List<UserTraveller> userTravellers = mTravellers.getFullUserTravellerList();
    String userName = null;
    if (userTravellers != null) {
      for (UserTraveller userTraveller : userTravellers) {
        if (userTraveller.getUserProfile().isDefaultTraveller()) {
          userName = userTraveller.getUserProfile().getName();
        }
      }
    }
    if (TextUtils.isEmpty(userName)) {
      return null;
    } else {
      return String.format(message, userName);
    }
  }
}
