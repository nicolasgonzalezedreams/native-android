package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.MSLLocationDBDAOInterface;
import com.odigeo.data.entity.booking.MSLLocation;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.MSLLocationDBMapper;

public class MSLLocationDBDAO extends OdigeoSQLiteOpenHelper implements MSLLocationDBDAOInterface {
  private MSLLocationDBMapper mMSLLocationDBMapper;

  public MSLLocationDBDAO(Context context) {
    super(context);
    mMSLLocationDBMapper = new MSLLocationDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + CITY_IATA_CODE
        + " NUMERIC, "
        + CITY_NAME
        + " TEXT, "
        + COUNTRY
        + " TEXT, "
        + GEO_NODE_ID
        + " NUMERIC PRIMARY KEY, "
        + LATITUDE
        + " NUMERIC, "
        + LOCATION_CODE
        + " TEXT, "
        + LOCATION_NAME
        + " TEXT, "
        + LOCATION_TYPE
        + " TEXT, "
        + LONGITUDE
        + " NUMERIC, "
        + MATCH_NAME
        + " TEXT "
        + ");";
  }

  @Override public MSLLocation getMSLLocation(long mslLocationId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + GEO_NODE_ID + " = " + mslLocationId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    MSLLocation mslLocation = mMSLLocationDBMapper.getMSLLocation(data);
    data.close();

    return mslLocation;
  }

  @Override public long addMSLLocation(MSLLocation mslLocation) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(CITY_IATA_CODE, mslLocation.getCityIATACode());
    values.put(CITY_NAME, mslLocation.getCityName());
    values.put(COUNTRY, mslLocation.getCountry());
    values.put(GEO_NODE_ID, mslLocation.getGeoNodeId());
    values.put(LATITUDE, mslLocation.getLatitude());
    values.put(LOCATION_CODE, mslLocation.getLocationCode());
    values.put(LOCATION_NAME, mslLocation.getLocationName());
    values.put(LOCATION_TYPE, mslLocation.getLocationType());
    values.put(LONGITUDE, mslLocation.getLongitude());
    values.put(MATCH_NAME, mslLocation.getMatchName());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }
}
