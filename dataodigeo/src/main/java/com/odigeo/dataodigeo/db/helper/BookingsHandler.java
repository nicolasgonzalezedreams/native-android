package com.odigeo.dataodigeo.db.helper;

import android.app.Application;
import com.odigeo.data.db.dao.BaggageDBDAOInterface;
import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.db.dao.BuyerDBDAOInterface;
import com.odigeo.data.db.dao.CarrierDBDAOInterface;
import com.odigeo.data.db.dao.FlightStatsDBDAOInterface;
import com.odigeo.data.db.dao.InsuranceDBDAOInterface;
import com.odigeo.data.db.dao.ItineraryBookingDBDAOInterface;
import com.odigeo.data.db.dao.LocationBookingDBDAOInterface;
import com.odigeo.data.db.dao.SectionDBDAOInterface;
import com.odigeo.data.db.dao.SegmentDBDAOInterface;
import com.odigeo.data.db.dao.TravellerDBDAOInterface;
import com.odigeo.data.db.helper.BookingsHandlerInterface;
import com.odigeo.data.entity.booking.Baggage;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Carrier;
import com.odigeo.data.entity.booking.ItineraryBooking;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.helper.CrashlyticsUtil;
import com.odigeo.interactors.DeleteBookingsInteractor;
import com.odigeo.interactors.provider.MyTripsProvider;
import com.odigeo.presenter.listeners.OnRemoveBookingsListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by javier.rebollo on 7/9/15.
 */
public class BookingsHandler implements BookingsHandlerInterface {

  private Application mApplication;
  private BookingDBDAOInterface mBookingDBDAO;
  private BuyerDBDAOInterface mBuyerDBDAO;
  private TravellerDBDAOInterface mTravellerDBDAO;
  private BaggageDBDAOInterface mBaggageDBDAO;
  private SegmentDBDAOInterface mSegmentDBDAO;
  private CarrierDBDAOInterface mCarrierDBDAO;
  private SectionDBDAOInterface mSectionDBDAO;
  private ItineraryBookingDBDAOInterface mItineraryDBDAO;
  private LocationBookingDBDAOInterface mLocationBookingDBDAO;
  private InsuranceDBDAOInterface mInsuranceDBDAO;
  private FlightStatsDBDAOInterface mFlightStatsDBDAO;
  private DeleteBookingsInteractor mDeleteBookingsInteractor;

  public BookingsHandler(Application application, BookingDBDAOInterface bookingDBDAO,
      BuyerDBDAOInterface buyerDBDAO, TravellerDBDAOInterface travellerDBDAO,
      BaggageDBDAOInterface baggageDBDAO, SegmentDBDAOInterface segmentDBDAO,
      CarrierDBDAOInterface carrierDBDAO, SectionDBDAOInterface sectionDBDAO,
      ItineraryBookingDBDAOInterface itineraryDBDAO,
      LocationBookingDBDAOInterface locationBookingDBDAO, InsuranceDBDAOInterface insuranceDBDAO,
      FlightStatsDBDAOInterface flightStatsDBDAO,
      DeleteBookingsInteractor deleteBookingsInteractor) {

    mApplication = application;
    mBookingDBDAO = bookingDBDAO;
    mBuyerDBDAO = buyerDBDAO;
    mTravellerDBDAO = travellerDBDAO;
    mBaggageDBDAO = baggageDBDAO;
    mSegmentDBDAO = segmentDBDAO;
    mCarrierDBDAO = carrierDBDAO;
    mSectionDBDAO = sectionDBDAO;
    mItineraryDBDAO = itineraryDBDAO;
    mLocationBookingDBDAO = locationBookingDBDAO;
    mInsuranceDBDAO = insuranceDBDAO;
    mFlightStatsDBDAO = flightStatsDBDAO;
    mDeleteBookingsInteractor = deleteBookingsInteractor;
  }

  @Override public void completeBookingData(MyTripsProvider myTripsProvider) {
    for (int i = 0; i < myTripsProvider.size(); i++) {
      //Check if not is null because the headers are null
      if (myTripsProvider.get(i) != null) {
        completeBooking(myTripsProvider.get(i));
      }
    }
  }

  @Override public void completeBookingData(List<Booking> bookings) {
    for (int i = 0; i < bookings.size(); i++) {
      //Check if not is null because the headers are null
      if (bookings.get(i) != null) {
        completeBooking(bookings.get(i));
      }
    }
  }

  @Override public void completeBooking(Booking booking) {
    long bookingId = booking.getBookingId();

    //Add insurances
    booking.setInsurances(mInsuranceDBDAO.getInsurances(bookingId));

    //Add buyer
    booking.setBuyer(mBuyerDBDAO.getBuyer(bookingId));

    //Add traveller
    booking.setTravellers(mTravellerDBDAO.getTravellers(bookingId));

    //Add segments
    booking.setSegments(mSegmentDBDAO.getSegments(bookingId));

    //Add baggage
    for (int y = 0; y < booking.getTravellers().size(); y++) {
      long travellerId = booking.getTravellers().get(y).getId();

      booking.getTravellers().get(y).setBaggageList(mBaggageDBDAO.getBaggages(travellerId));

      //Add segment and traveller to baggage
      for (int z = 0; z < booking.getTravellers().get(y).getBaggageList().size(); z++) {

        //Add traveller to baggage
        booking.getTravellers()
            .get(y)
            .getBaggageList()
            .get(z)
            .setTraveller(booking.getTravellers().get(y));

        long segmentId =
            booking.getTravellers().get(y).getBaggageList().get(z).getSegment().getId();

        //Search segment
        boolean found = false;
        int segmentIndex = 0;

        while (!found && segmentIndex < booking.getSegments().size()) {

          if (booking.getSegments().get(segmentIndex).getId() == segmentId) {
            //Add segment to baggage
            booking.getTravellers()
                .get(y)
                .getBaggageList()
                .get(z)
                .setSegment(booking.getSegments().get(segmentIndex));

            found = true;
          }

          segmentIndex++;
        }
      }
    }

    //Add sections
    for (int x = 0; x < booking.getSegments().size(); x++) {
      long segmentId = booking.getSegments().get(x).getId();

      booking.getSegments().get(x).setSectionsList(mSectionDBDAO.getSections(segmentId));

      //Add carrier, itinerary myTripsProvider and location booking to section
      for (int w = 0; w < booking.getSegments().get(x).getSectionsList().size(); w++) {
        //Add carrier
        booking.getSegments()
            .get(x)
            .getSectionsList()
            .get(w)
            .setCarrier(mCarrierDBDAO.getCarrier(
                booking.getSegments().get(x).getSectionsList().get(w).getCarrier().getId()));

        //Add itineraryBooking
        booking.getSegments()
            .get(x)
            .getSectionsList()
            .get(w)
            .setItineraryBooking(mItineraryDBDAO.getItineraryBooking(booking.getSegments()
                .get(x)
                .getSectionsList()
                .get(w)
                .getItineraryBooking()
                .getId()));

        //Add from
        booking.getSegments()
            .get(x)
            .getSectionsList()
            .get(w)
            .setFrom(mLocationBookingDBDAO.getLocationBooking(
                booking.getSegments().get(x).getSectionsList().get(w).getFrom().getGeoNodeId()));

        //Add to
        booking.getSegments()
            .get(x)
            .getSectionsList()
            .get(w)
            .setTo(mLocationBookingDBDAO.getLocationBooking(
                booking.getSegments().get(x).getSectionsList().get(w).getTo().getGeoNodeId()));

        //Set Flightstats
        booking.getSegments()
            .get(x)
            .getSectionsList()
            .get(w)
            .setFlightStats(mFlightStatsDBDAO.getFlightStats(
                booking.getSegments().get(x).getSectionsList().get(w).getSectionId(),
                Long.toString(booking.getBookingId())));
      }
    }
  }

  @Override public boolean saveBooking(Booking booking) {
    //Add booking
    boolean willBeSaved = true;
    List<Booking> bookings = mBookingDBDAO.getAllBookings();
    for (Booking item : bookings) {
      // If the booking existed on the database
      if (booking.getBookingId() == item.getBookingId()) {
        willBeSaved = false;
        break;
      }
    }

    if (willBeSaved) {
      long bookingId = mBookingDBDAO.addBooking(booking);
      if (bookingId != -1) {
        //Add buyer
        mBuyerDBDAO.addBuyer(booking.getBuyer());

        //Add travellers
        mTravellerDBDAO.addTravellers(booking.getTravellers());

        //Add segments
        mSegmentDBDAO.addSegments(booking.getSegments());

        //Add baggage
        ArrayList<Baggage> baggages = new ArrayList<>();

        for (int i = 0; i < booking.getTravellers().size(); i++) {

          for (int x = 0; x < booking.getTravellers().get(i).getBaggageList().size(); x++) {
            baggages.add(booking.getTravellers().get(i).getBaggageList().get(x));
          }
        }

        mBaggageDBDAO.addBaggages(baggages);

        //Add location booking
        mLocationBookingDBDAO.addLocationBookings(getLocationBookings(booking));

        //Add carrier
        ArrayList<Carrier> carriers = new ArrayList<>();

        for (int i = 0; i < booking.getSegments().size(); i++) {

          for (int x = 0; x < booking.getSegments().get(i).getSectionsList().size(); x++) {
            carriers.add(booking.getSegments().get(i).getSectionsList().get(x).getCarrier());
          }
        }

        mCarrierDBDAO.addCarriers(carriers);

        //Add itinerary bookings
        ArrayList<ItineraryBooking> itineraryBookings = new ArrayList<>();

        for (int i = 0; i < booking.getSegments().size(); i++) {

          for (int x = 0; x < booking.getSegments().get(i).getSectionsList().size(); x++) {
            itineraryBookings.add(
                booking.getSegments().get(i).getSectionsList().get(x).getItineraryBooking());
          }
        }

        mItineraryDBDAO.addItineraryBookings(itineraryBookings);

        //Add sections
        ArrayList<Section> sections = new ArrayList<>();

        for (int i = 0; i < booking.getSegments().size(); i++) {

          for (int x = 0; x < booking.getSegments().get(i).getSectionsList().size(); x++) {

            Section section = booking.getSegments().get(i).getSectionsList().get(x);
            mFlightStatsDBDAO.addOrUpdateFlightStats(section.getFlightStats(),
                Long.toString(booking.getBookingId()));
            sections.add(section);
          }
        }

        mSectionDBDAO.addSections(sections);

        //Add insurances
        mInsuranceDBDAO.addInsurances(booking.getInsurances());
      }
    }
    return willBeSaved;
  }

  @Override public void updateBookingFlightStats(Booking booking) {
    for (int i = 0; i < booking.getSegments().size(); i++) {
      for (int x = 0; x < booking.getSegments().get(i).getSectionsList().size(); x++) {
        Section section = booking.getSegments().get(i).getSectionsList().get(x);
        mFlightStatsDBDAO.addOrUpdateFlightStats(section.getFlightStats(),
            Long.toString(booking.getBookingId()));
      }
    }
  }

  @Override public void updateItineraryBooking(final Booking booking) {
    List<Segment> segments = mSegmentDBDAO.getSegments(booking.getBookingId());
    boolean hasToResetBookingInDb = false;
    for (int i = 0; i < segments.size(); i++) {
      List<Section> sections = mSectionDBDAO.getSections(segments.get(i).getId());
      for (int x = 0; x < sections.size(); x++) {
        Section section = sections.get(x);
        try {
          mItineraryDBDAO.updateItineraryBooking(
              Long.toString(section.getItineraryBooking().getId()),
              booking.getSegments().get(i).getSectionsList().get(x).getItineraryBooking());
        } catch (IndexOutOfBoundsException exception) {
                    /*
                    If IndexOutOfBoundsException, the booking from MSL has different segments and/or sections
                    compared with the booking stored on database. This case is not normal and doesn't happen a lot.
                    We mark the booking to reset, then outside the loop we track the booking id on Crashlytics for
                    future analysis and delete the booking from db and store it again to have an updated booking.
                     */
          hasToResetBookingInDb = true;
        }
      }
    }
    if (hasToResetBookingInDb) {
      if (mApplication instanceof CrashlyticsUtil) {
        String crashlyticsTag = "IndexOutOfBoundsException Booking ID: ";
        ((CrashlyticsUtil) mApplication).trackNonFatal(
            new Exception(crashlyticsTag + booking.getBookingId()));
      }
      mDeleteBookingsInteractor.removeBookings(
          Collections.singletonList(mBookingDBDAO.getBooking(booking.getBookingId())),
          new OnRemoveBookingsListener() {
            @Override public void onSuccess() {
              saveBooking(booking);
            }

            @Override public void onError(List<Long> removedBookings) {
            }
          });
    }
  }

  public List<LocationBooking> getLocationBookings(Booking booking) {
    List<LocationBooking> locationBookings = new ArrayList<>();

    for (int i = 0; i < booking.getSegments().size(); i++) {
      List<Section> sections = booking.getSegments().get(i).getSectionsList();

      for (int x = 0; x < sections.size(); x++) {
        locationBookings.add(sections.get(x).getFrom());
        locationBookings.add(sections.get(x).getTo());
      }
    }

    return locationBookings;
  }

  @Override public void deleteAllBookingData() {
    mBookingDBDAO.removeBookings();
    mBuyerDBDAO.deleteBuyers();
    mTravellerDBDAO.deleteTravellers();
    mSegmentDBDAO.deleteAllSegments();
    mBaggageDBDAO.deleteBagagges();
    mLocationBookingDBDAO.deleteLocations();
    mCarrierDBDAO.deleteCarrier();
    mItineraryDBDAO.deleteItineraries();
    mFlightStatsDBDAO.deleteFlightStats();
    mSectionDBDAO.deleteSections();
    mInsuranceDBDAO.removeAllInsurances();
  }
}
