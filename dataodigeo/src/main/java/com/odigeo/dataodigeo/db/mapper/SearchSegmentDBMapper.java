package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.SearchSegment;
import com.odigeo.dataodigeo.db.dao.SearchSegmentDBDAO;
import java.util.ArrayList;

public class SearchSegmentDBMapper {
  /**
   * Mapper search segment cursor list
   *
   * @param cursor Cursor with search segment data
   * @return {@link ArrayList<SearchSegment>}
   */
  public ArrayList<SearchSegment> getSearchSegmentList(Cursor cursor) {
    ArrayList<SearchSegment> searchSegmentList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(SearchSegmentDBDAO.ID));
        long searchSegmentId =
            cursor.getLong(cursor.getColumnIndex(SearchSegmentDBDAO.SEARCH_SEGMENT_ID));
        String originIATACode =
            cursor.getString(cursor.getColumnIndex(SearchSegmentDBDAO.ORIGIN_IATA_CODE));
        String destinationIATACode =
            cursor.getString(cursor.getColumnIndex(SearchSegmentDBDAO.DESTINATION_IATA_CODE));
        long departureDate =
            cursor.getLong(cursor.getColumnIndex(SearchSegmentDBDAO.DEPARTURE_DATE));
        long segmentOrder = cursor.getLong(cursor.getColumnIndex(SearchSegmentDBDAO.SEGMENT_ORDER));
        long parentSearchId =
            cursor.getLong(cursor.getColumnIndex(SearchSegmentDBDAO.PARENT_SEARCH_ID));

        searchSegmentList.add(
            new SearchSegment(id, searchSegmentId, originIATACode, destinationIATACode,
                departureDate, segmentOrder, parentSearchId));
      }
    }

    return searchSegmentList;
  }

  /**
   * Mapper search segment cursor
   *
   * @param cursor Cursor with search segment data
   * @return {@link SearchSegment}
   */
  public SearchSegment getSearchSegment(Cursor cursor) {
    ArrayList<SearchSegment> searchSegmentList = getSearchSegmentList(cursor);

    if (searchSegmentList.size() == 1) {
      return searchSegmentList.get(0);
    } else {
      return null;
    }
  }
}
