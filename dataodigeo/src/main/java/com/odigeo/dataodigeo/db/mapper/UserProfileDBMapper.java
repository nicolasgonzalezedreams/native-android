package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import android.util.Log;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.dataodigeo.db.dao.UserProfileDBDAO;
import java.util.ArrayList;

public class UserProfileDBMapper {

  /**
   * Mapper user profile cursor list
   *
   * @param cursor Cursor with user profile data
   * @return {@link ArrayList< UserProfile >}
   */
  public ArrayList<UserProfile> getUserProfileList(Cursor cursor) {
    ArrayList<UserProfile> userProfileList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(UserProfileDBDAO.ID));
        long userProfileId =
            cursor.getLong(cursor.getColumnIndex(UserProfileDBDAO.USER_PROFILE_ID));
        String gender = cursor.getString(cursor.getColumnIndex(UserProfileDBDAO.GENDER));
        String name = cursor.getString(cursor.getColumnIndex(UserProfileDBDAO.NAME));
        String middleName = cursor.getString(cursor.getColumnIndex(UserProfileDBDAO.MIDDLE_NAME));
        String firstLastName =
            cursor.getString(cursor.getColumnIndex(UserProfileDBDAO.FIRST_LAST_NAME));
        String secondLastName =
            cursor.getString(cursor.getColumnIndex(UserProfileDBDAO.SECOND_LAST_NAME));
        long birthDate = cursor.getLong(cursor.getColumnIndex(UserProfileDBDAO.BIRTHDATE));
        String prefixPhoneNumber =
            cursor.getString(cursor.getColumnIndex(UserProfileDBDAO.PREFIX_PHONE_NUMBER));
        String phoneNumber = cursor.getString(cursor.getColumnIndex(UserProfileDBDAO.PHONE_NUMBER));
        String prefixAlternatePhoneNumber =
            cursor.getString(cursor.getColumnIndex(UserProfileDBDAO.PREFIX_ALTERNATE_PHONE_NUMBER));
        String alternatePhoneNumber =
            cursor.getString(cursor.getColumnIndex(UserProfileDBDAO.ALTERNATE_PHONE_NUMBER));
        String mobilePhoneNumber =
            cursor.getString(cursor.getColumnIndex(UserProfileDBDAO.MOBILE_PHONE_NUMBER));
        String nationalityCountryCode =
            cursor.getString(cursor.getColumnIndex(UserProfileDBDAO.NATIONALITY_COUNTRY_CODE));
        String cpf = cursor.getString(cursor.getColumnIndex(UserProfileDBDAO.CPF));
        boolean isDefaultProfile =
            cursor.getInt(cursor.getColumnIndex(UserProfileDBDAO.IS_DEFAULT)) == 1;
        String photo = cursor.getString(cursor.getColumnIndex(UserProfileDBDAO.PHOTO));
        long userTravellerId =
            cursor.getLong(cursor.getColumnIndex(UserProfileDBDAO.USER_TRAVELLER_ID));

        try {

          UserProfile.Title title = null;
          if (cursor.getString(cursor.getColumnIndex(UserProfileDBDAO.TITLE)) != null) {
            title = UserProfile.Title.valueOf(
                cursor.getString(cursor.getColumnIndex(UserProfileDBDAO.TITLE)));
          } else {
            title = UserProfile.Title.UNKNOWN;
          }

          userProfileList.add(
              new UserProfile(id, userProfileId, gender, title, name, middleName, firstLastName,
                  secondLastName, birthDate, prefixPhoneNumber, phoneNumber,
                  prefixAlternatePhoneNumber, alternatePhoneNumber, mobilePhoneNumber,
                  nationalityCountryCode, cpf, isDefaultProfile, photo,
                  // TODO: Parse UserAddress and UserIdentification's
                  null, null, userTravellerId));
        } catch (IllegalArgumentException e) {
          Log.e("UserProfileDBMapper", e.getMessage());
        }
      }
    }

    return userProfileList;
  }

  /**
   * Mapper user profile cursor
   *
   * @param cursor Cursor with user profile data
   * @return {@link UserProfile}
   */
  public UserProfile getUserProfile(Cursor cursor) {
    ArrayList<UserProfile> userProfileList = getUserProfileList(cursor);

    if (userProfileList.size() == 1) {
      return userProfileList.get(0);
    } else {
      return null;
    }
  }
}
