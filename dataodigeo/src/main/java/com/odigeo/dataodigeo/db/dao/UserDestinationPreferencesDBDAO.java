package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.UserDestinationPreferencesDBDAOInterface;
import com.odigeo.data.entity.userData.UserDestinationPreferences;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.UserDestinationPreferencesDBMapper;

public class UserDestinationPreferencesDBDAO extends OdigeoSQLiteOpenHelper
    implements UserDestinationPreferencesDBDAOInterface {

  private UserDestinationPreferencesDBMapper mUserDestinationPreferencesDBMapper;

  public UserDestinationPreferencesDBDAO(Context context) {
    super(context);
    mUserDestinationPreferencesDBMapper = new UserDestinationPreferencesDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + USER_DESTINATION_PREFERENCES_ID
        + " NUMERIC, "
        + DESTINATION
        + " TEXT, "
        + USER_ID
        + " INTEGER "
        + ");";
  }

  @Override public UserDestinationPreferences getUserDestinationPreferences(
      long userDestinationPreferencesId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + userDestinationPreferencesId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    UserDestinationPreferences preferences =
        mUserDestinationPreferencesDBMapper.getUserDestinationPreferences(data);
    data.close();

    return preferences;
  }

  @Override
  public long addUserDestinationPreferences(UserDestinationPreferences userDestinationPreferences) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(USER_DESTINATION_PREFERENCES_ID,
        userDestinationPreferences.getUserDestinationPreferencesId());
    values.put(DESTINATION, userDestinationPreferences.getDestination());
    values.put(USER_ID, userDestinationPreferences.getUserId());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }
}
