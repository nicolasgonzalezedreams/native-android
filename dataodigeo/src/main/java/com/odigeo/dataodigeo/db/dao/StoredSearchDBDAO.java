package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.StoredSearchDBDAOInterface;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.StoredSearchDBMapper;
import java.util.List;

public class StoredSearchDBDAO extends OdigeoSQLiteOpenHelper
    implements StoredSearchDBDAOInterface {
  private StoredSearchDBMapper mStoredSearchDBMapper;

  public StoredSearchDBDAO(Context context) {
    super(context);
    mStoredSearchDBMapper = new StoredSearchDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + STORED_SEARCH_ID
        + " NUMERIC UNIQUE, "
        + NUM_ADULTS
        + " NUMERIC, "
        + NUM_CHILDREN
        + " NUMERIC, "
        + NUM_INFANTS
        + " NUMERIC, "
        + IS_DIRECT_FLIGHT
        + " NUMERIC, "
        + CABIN_CLASS
        + " TEXT, "
        + TRIP_TYPE
        + " TEXT, "
        + IS_SYNCHRONIZED
        + " NUMERIC, "
        + CREATION_DATE
        + " NUMERIC, "
        + USER_ID
        + " NUMERIC "
        + ");";
  }

  public static String getCreateTrigger() {
    return "CREATE TRIGGER generate_stored_search_id AFTER INSERT ON stored_search \n"
        + "WHEN (NEW.stored_search_id = 0 OR NEW.stored_search_id IS NULL) AND NEW.is_synchronized = 0\n"
        + "BEGIN \n"
        + "\tUPDATE stored_search SET stored_search_id = NEW.ID WHERE ID = NEW.ID; \n"
        + "END;";
  }

  @Override public long addStoredSearch(StoredSearch storedSearch) {

    SQLiteDatabase db = getOdigeoDatabase();
    return db.insert(TABLE_NAME, null, getContentValues(storedSearch));
  }

  @Override public boolean updateStoredSearch(StoredSearch storedSearch) {

    SQLiteDatabase db = getOdigeoDatabase();
    ContentValues contentValues = getContentValues(storedSearch);
    contentValues.remove(IS_SYNCHRONIZED);
    contentValues.remove(USER_ID);
    return db.update(TABLE_NAME, contentValues, STORED_SEARCH_ID + "=?",
        new String[] { String.valueOf(storedSearch.getStoredSearchId()) }) > 0;
  }

  @Override public List<StoredSearch> getAllStoredSearches() {
    SQLiteDatabase db = getOdigeoDatabase();
    Cursor data = db.query(TABLE_NAME, null, null, null, null, null, CREATION_DATE + " DESC");
    List<StoredSearch> storedSearchList = mStoredSearchDBMapper.getStoredSearchList(data);
    data.close();
    return storedSearchList;
  }

  @Override public List<StoredSearch> getLocalStoredSearches() {
    return queryStoredSearches("0");
  }

  @Override public List<StoredSearch> getSynchronizedStoredSearches() {
    return queryStoredSearches("1");
  }

  @Override public boolean removeAllStoredSearches() {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, null, null) > 0;
  }

  @Override public boolean removeMultiTripSearches() {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, TRIP_TYPE + "= 'M'", null) > 0;
  }

  @Override public boolean removeNonMultiTripSearches() {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, TRIP_TYPE + "= 'O' OR " + TRIP_TYPE + "= 'R'", null) > 0;
  }

  @Override public boolean removeStoredSearch(StoredSearch storedSearch) {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, STORED_SEARCH_ID + "=" + storedSearch.getStoredSearchId(), null)
        > 0;
  }

  @Override public StoredSearch getStoredSearch(long storedSearchId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + storedSearchId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    StoredSearch storedSearch = mStoredSearchDBMapper.getStoredSearch(data);
    data.close();

    return storedSearch;
  }

  private List<StoredSearch> queryStoredSearches(String type) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + IS_SYNCHRONIZED + " = ?";
    Cursor data = db.rawQuery(selectQuery, new String[] { type });
    List<StoredSearch> storedSearchList = mStoredSearchDBMapper.getStoredSearchList(data);
    data.close();
    return storedSearchList;
  }

  private ContentValues getContentValues(StoredSearch storedSearch) {

    ContentValues values = new ContentValues();

    values.put(STORED_SEARCH_ID, storedSearch.getStoredSearchId());
    values.put(NUM_ADULTS, storedSearch.getNumAdults());
    values.put(NUM_CHILDREN, storedSearch.getNumChildren());
    values.put(NUM_INFANTS, storedSearch.getNumInfants());
    values.put(IS_DIRECT_FLIGHT, storedSearch.getIsDirectFlight() ? 1 : 0);
    values.put(CABIN_CLASS, storedSearch.getCabinClass().toString());
    values.put(TRIP_TYPE, storedSearch.getTripType().toString());
    values.put(IS_SYNCHRONIZED, storedSearch.isSynchronized() ? 1 : 0);
    values.put(CREATION_DATE, storedSearch.getCreationDate());
    values.put(USER_ID, storedSearch.getUserId());

    return values;
  }
}
