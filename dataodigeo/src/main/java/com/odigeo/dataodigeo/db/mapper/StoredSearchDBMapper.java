package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.dataodigeo.db.dao.StoredSearchDBDAO;
import java.util.ArrayList;

/**
 * Created by matias.dirusso on 21/7/2016.
 */
public class StoredSearchDBMapper {
  /**
   * Mapper stored search cursor list
   *
   * @param cursor Cursor with stored search data
   * @return {@link ArrayList<StoredSearch>}
   */
  public ArrayList<StoredSearch> getStoredSearchList(Cursor cursor) {
    ArrayList<StoredSearch> storedSearchList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(StoredSearchDBDAO.ID));
        long storedSearchID =
            cursor.getLong(cursor.getColumnIndex(StoredSearchDBDAO.STORED_SEARCH_ID));
        int numAdults = cursor.getInt(cursor.getColumnIndex(StoredSearchDBDAO.NUM_ADULTS));
        int numChildren = cursor.getInt(cursor.getColumnIndex(StoredSearchDBDAO.NUM_CHILDREN));
        int numInfants = cursor.getInt(cursor.getColumnIndex(StoredSearchDBDAO.NUM_INFANTS));
        boolean isDirectFlight =
            (cursor.getInt(cursor.getColumnIndex(StoredSearchDBDAO.IS_DIRECT_FLIGHT)) == 1);
        StoredSearch.CabinClass cabinClass = null;
        if (cursor.getString(cursor.getColumnIndex(StoredSearchDBDAO.CABIN_CLASS)) != null) {
          cabinClass = StoredSearch.CabinClass.valueOf(
              cursor.getString(cursor.getColumnIndex(StoredSearchDBDAO.CABIN_CLASS)));
        } else {
          cabinClass = StoredSearch.CabinClass.UNKNOWN;
        }
        StoredSearch.TripType tripType = null;
        if (cursor.getString(cursor.getColumnIndex(StoredSearchDBDAO.TRIP_TYPE)) != null) {
          tripType = StoredSearch.TripType.valueOf(
              cursor.getString(cursor.getColumnIndex(StoredSearchDBDAO.TRIP_TYPE)));
        } else {
          tripType = StoredSearch.TripType.UNKNOWN;
        }
        boolean isSynchronized =
            cursor.getInt(cursor.getColumnIndex(StoredSearchDBDAO.IS_SYNCHRONIZED)) == 1;
        long creationDate = cursor.getLong(cursor.getColumnIndex(StoredSearchDBDAO.CREATION_DATE));
        long userId = cursor.getLong(cursor.getColumnIndex(StoredSearchDBDAO.USER_ID));
        storedSearchList.add(
            new StoredSearch(id, storedSearchID, numAdults, numChildren, numInfants, isDirectFlight,
                cabinClass, tripType, isSynchronized, creationDate, userId));
      }
    }

    return storedSearchList;
  }

  /**
   * Mapper stored search cursor
   *
   * @param cursor Cursor with stored search data
   * @return {@link StoredSearch}
   */
  public StoredSearch getStoredSearch(Cursor cursor) {
    ArrayList<StoredSearch> storedSearchList = getStoredSearchList(cursor);

    if (storedSearchList.size() == 1) {
      return storedSearchList.get(0);
    } else {
      return null;
    }
  }
}
