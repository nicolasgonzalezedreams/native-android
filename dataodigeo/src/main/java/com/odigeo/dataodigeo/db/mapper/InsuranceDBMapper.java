package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.Insurance;
import com.odigeo.dataodigeo.db.dao.InsuranceDBDAO;
import java.util.ArrayList;

public class InsuranceDBMapper {
  /**
   * Mapper insurance cursor list
   *
   * @param cursor Cursor with insurance data
   * @return {@link ArrayList<Insurance>}
   */
  public ArrayList<Insurance> getInsuranceList(Cursor cursor) {
    ArrayList<Insurance> insuranceList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(InsuranceDBDAO.ID));
        long insuranceId = cursor.getLong(cursor.getColumnIndex(InsuranceDBDAO.INSURANCE_ID));
        String conditionURLPrimary =
            cursor.getString(cursor.getColumnIndex(InsuranceDBDAO.CONDITION_URL_PRIMARY));
        String conditionURLSecundary =
            cursor.getString(cursor.getColumnIndex(InsuranceDBDAO.CONDITION_URL_SECUNDARY));
        String insuranceDescription =
            cursor.getString(cursor.getColumnIndex(InsuranceDBDAO.INSURANCE_DESCRIPTION));
        String insuranceType =
            cursor.getString(cursor.getColumnIndex(InsuranceDBDAO.INSURANCE_TYPE));
        String policy = cursor.getString(cursor.getColumnIndex(InsuranceDBDAO.POLICY));
        boolean selectable = (cursor.getInt(cursor.getColumnIndex(InsuranceDBDAO.SELECTABLE)) == 1);
        String subtitle = cursor.getString(cursor.getColumnIndex(InsuranceDBDAO.SUBTITLE));
        String title = cursor.getString(cursor.getColumnIndex(InsuranceDBDAO.TITLE));
        double total = cursor.getDouble(cursor.getColumnIndex(InsuranceDBDAO.TOTAL));
        long bookingId = cursor.getLong(cursor.getColumnIndex(InsuranceDBDAO.BOOKING_ID));

        insuranceList.add(new Insurance(id, insuranceId, conditionURLPrimary, conditionURLSecundary,
            insuranceDescription, insuranceType, policy, selectable, subtitle, title, total,
            bookingId));
      }
    }

    return insuranceList;
  }

  /**
   * Mapper insurance cursor
   *
   * @param cursor Cursor with insurance data
   * @return {@link Insurance}
   */
  public Insurance getInsurance(Cursor cursor) {
    ArrayList<Insurance> insuranceList = getInsuranceList(cursor);

    if (insuranceList.size() == 1) {
      return insuranceList.get(0);
    } else {
      return null;
    }
  }
}
