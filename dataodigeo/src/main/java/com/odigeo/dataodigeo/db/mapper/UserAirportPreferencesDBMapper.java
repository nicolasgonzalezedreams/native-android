package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.userData.UserAirportPreferences;
import com.odigeo.dataodigeo.db.dao.UserAirportPreferencesDBDAO;
import java.util.ArrayList;

public class UserAirportPreferencesDBMapper {

  /**
   * Mapper user airport preferences cursor list
   *
   * @param cursor Cursor with user airline preferences data
   * @return {@link ArrayList< UserAirportPreferences >}
   */
  public ArrayList<UserAirportPreferences> getUserAirportPreferencesList(Cursor cursor) {
    ArrayList<UserAirportPreferences> userAirportPreferencesList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(UserAirportPreferencesDBDAO.ID));
        long userAirportPreferences = cursor.getLong(
            cursor.getColumnIndex(UserAirportPreferencesDBDAO.USER_AIRPORT_PREFERENCES_ID));
        String airport =
            cursor.getString(cursor.getColumnIndex(UserAirportPreferencesDBDAO.AIRPORT));
        long userId = cursor.getLong(cursor.getColumnIndex(UserAirportPreferencesDBDAO.USER_ID));

        userAirportPreferencesList.add(
            new UserAirportPreferences(id, userAirportPreferences, airport, userId));
      }
    }

    return userAirportPreferencesList;
  }

  /**
   * Mapper user airport preferences cursor
   *
   * @param cursor Cursor with user airline preferences data
   * @return {@link UserAirportPreferences}
   */
  public UserAirportPreferences getUserAirportPreferences(Cursor cursor) {
    ArrayList<UserAirportPreferences> userAirportPreferencesList =
        getUserAirportPreferencesList(cursor);

    if (userAirportPreferencesList.size() == 1) {
      return userAirportPreferencesList.get(0);
    } else {
      return null;
    }
  }
}
