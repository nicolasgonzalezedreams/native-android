package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.BaggageDBDAOInterface;
import com.odigeo.data.entity.booking.Baggage;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.BaggageDBMapper;
import java.util.ArrayList;
import java.util.List;

public class BaggageDBDAO extends OdigeoSQLiteOpenHelper implements BaggageDBDAOInterface {
  private BaggageDBMapper mBaggageDBMapper;

  public BaggageDBDAO(Context context) {
    super(context);
    mBaggageDBMapper = new BaggageDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + INCLUDED_IN_PRICE
        + " NUMERIC, "
        + PIECES
        + " NUMERIC, "
        + WEIGHT
        + " NUMERIC, "
        + SEGMENT_ID
        + " INTEGER, "
        + TRAVELLER_ID
        + " INTEGER "
        + ");";
  }

  @Override public Baggage getBaggage(long baggageId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + baggageId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    Baggage baggage = mBaggageDBMapper.getBaggage(data);
    data.close();

    return baggage;
  }

  @Override public List<Baggage> getBaggages(long travellerId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + TRAVELLER_ID + " = " + travellerId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    List<Baggage> baggage = mBaggageDBMapper.getBaggageList(data);
    data.close();

    return baggage;
  }

  @Override public long addBaggage(Baggage baggage) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(INCLUDED_IN_PRICE, (baggage.getIncludedInPrice()) ? 1 : 0);
    values.put(PIECES, baggage.getPieces());
    values.put(WEIGHT, baggage.getWeight());
    values.put(SEGMENT_ID, baggage.getSegment().getId());
    values.put(TRAVELLER_ID, baggage.getTraveller().getId());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }

  @Override public ArrayList<Long> addBaggages(ArrayList<Baggage> baggages) {
    ArrayList<Long> baggageIds = new ArrayList<>();

    for (int i = 0; i < baggages.size(); i++) {
      baggageIds.add(addBaggage(baggages.get(i)));
      baggages.get(i).setId(baggageIds.get(i));
    }

    return baggageIds;
  }

  @Override public boolean deleteBaggage(long segmentId) {
    SQLiteDatabase db = getOdigeoDatabase();
    int rowsAffected = db.delete(TABLE_NAME, FILTER_BY_SEGMENT_ID, new String[] { "" + segmentId });
    return rowsAffected > 0;
  }

  @Override public long deleteBagagges() {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, null, null);
  }
}
