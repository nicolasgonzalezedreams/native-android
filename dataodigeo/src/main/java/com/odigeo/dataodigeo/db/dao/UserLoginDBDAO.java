package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.UserLoginDBDAOInterface;
import com.odigeo.data.entity.userData.UserLogin;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.UserLoginDBMapper;

public class UserLoginDBDAO extends OdigeoSQLiteOpenHelper implements UserLoginDBDAOInterface {

  private UserLoginDBMapper mUserLoginDBMapper;

  public UserLoginDBDAO(Context context) {
    super(context);
    mUserLoginDBMapper = new UserLoginDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + USER_LOGIN_ID
        + " NUMERIC, "
        + PASSWORD
        + " TEXT, "
        + ACCESS_TOKEN
        + " TEXT, "
        + EXPIRATION_TOKEN_DATE
        + " NUMERIC, "
        + HASH_CODE
        + " TEXT, "
        + ACTIVATION_CODE
        + " TEXT, "
        + ACTIVATION_CODE_DATE
        + " NUMERIC, "
        + LOGIN_ATTEMPT_FAILED
        + " NUMERIC, "
        + LAST_LOGIN_DATE
        + " NUMERIC, "
        + REFRESH_TOKEN
        + " TEXT, "
        + USER_ID
        + " INTEGER "
        + ");";
  }

  @Override public UserLogin getUserLogin(long userLoginId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + userLoginId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    UserLogin userLogin = mUserLoginDBMapper.getUserLogin(data);
    data.close();

    return userLogin;
  }

  @Override public long addUserLogin(UserLogin userLogin) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(USER_LOGIN_ID, userLogin.getUserLoginId());
    values.put(PASSWORD, userLogin.getPassword());
    values.put(ACCESS_TOKEN, userLogin.getAccessToken());
    values.put(EXPIRATION_TOKEN_DATE, userLogin.getExpirationTokenDate());
    values.put(HASH_CODE, userLogin.getHashCode());
    values.put(ACTIVATION_CODE, userLogin.getActivationCode());
    values.put(ACTIVATION_CODE_DATE, userLogin.getActivationCodeDate());
    values.put(LOGIN_ATTEMPT_FAILED, userLogin.getLoginAttemptFailed());
    values.put(LAST_LOGIN_DATE, userLogin.getLastLoginDate());
    values.put(REFRESH_TOKEN, userLogin.getRefreshToken());
    values.put(USER_ID, userLogin.getUserId());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }
}
