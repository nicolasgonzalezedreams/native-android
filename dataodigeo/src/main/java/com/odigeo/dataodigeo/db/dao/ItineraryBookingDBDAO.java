package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.ItineraryBookingDBDAOInterface;
import com.odigeo.data.entity.booking.ItineraryBooking;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.ItineraryBookingDBMapper;
import java.util.ArrayList;

public class ItineraryBookingDBDAO extends OdigeoSQLiteOpenHelper
    implements ItineraryBookingDBDAOInterface {

  private ItineraryBookingDBMapper mItineraryBookingDBMapper;

  public ItineraryBookingDBDAO(Context context) {
    super(context);
    mItineraryBookingDBMapper = new ItineraryBookingDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + BOOKING_STATUS
        + " TEXT, "
        + PNR
        + " TEXT UNIQUE, "
        + TIMESTAMP
        + " NUMERIC "
        + ");";
  }

  @Override public ItineraryBooking getItineraryBooking(long itineraryBookingId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + itineraryBookingId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    ItineraryBooking itineraryBooking = mItineraryBookingDBMapper.getItineraryBooking(data);
    data.close();

    return itineraryBooking;
  }

  @Override public ItineraryBooking getItineraryBookingWithPNR(String pnr) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + PNR + " = '" + pnr + "'";
    Cursor data = db.rawQuery(selectQuery, null);
    ItineraryBooking itineraryBooking = mItineraryBookingDBMapper.getItineraryBooking(data);
    data.close();

    return itineraryBooking;
  }

  @Override public long addItineraryBooking(ItineraryBooking itineraryBooking) {

    ItineraryBooking dbItineraryBooking = getItineraryBookingWithPNR(itineraryBooking.getPnr());

    if (dbItineraryBooking == null) {
      SQLiteDatabase db = getOdigeoDatabase();

      ContentValues values = new ContentValues();
      values.put(BOOKING_STATUS, itineraryBooking.getBookingStatus());
      values.put(PNR, itineraryBooking.getPnr());
      values.put(TIMESTAMP, itineraryBooking.getTimestamp());

      long newRowId;

      newRowId = db.insert(TABLE_NAME, null, values);

      return newRowId;
    } else {
      return dbItineraryBooking.getId();
    }
  }

  @Override
  public ArrayList<Long> addItineraryBookings(ArrayList<ItineraryBooking> itineraryBookings) {
    ArrayList<Long> itineraryBookingsIds = new ArrayList<>();

    for (int i = 0; i < itineraryBookings.size(); i++) {
      itineraryBookingsIds.add(addItineraryBooking(itineraryBookings.get(i)));

      itineraryBookings.get(i).setId(itineraryBookingsIds.get(i));
    }

    return itineraryBookingsIds;
  }

  @Override
  public long updateItineraryBooking(String itineraryBookingID, ItineraryBooking itineraryBooking) {
    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(BOOKING_STATUS, itineraryBooking.getBookingStatus());
    values.put(PNR, itineraryBooking.getPnr());
    values.put(TIMESTAMP, itineraryBooking.getTimestamp());

    long rowsAffected;

    rowsAffected = db.update(TABLE_NAME, values, ID + "=?", new String[] { itineraryBookingID });
    return rowsAffected;
  }

  @Override public long deleteItineraries() {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, null, null);
  }
}
