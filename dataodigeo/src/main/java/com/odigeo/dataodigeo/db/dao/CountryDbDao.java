package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.provider.BaseColumns;
import android.text.TextUtils;
import android.util.Log;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.net.NetCountry;
import com.odigeo.dataodigeo.db.mapper.CountryDbMapper;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 11/02/16
 */
public class CountryDbDao extends CountriesBaseDbDao<NetCountry> implements BaseColumns {

  public static final Map<String, String> PROJECTION_MAP;
  // Table
  public static final String TABLE_NAME = "Country";
  // Fields
  public static final String GEO_NODE_ID = "geoNodeId";
  public static final String COUNTRY_CODE = "countryCode";
  public static final String PHONE_PREFIX = "phonePrefix";
  // As Values
  public static final String COUNTRY_NAME_EN = "CountryNameEn";
  public static final String NAME_DEFAULT = "nameDefault";
  public static final String DEFAULT_LANGUAGE = "DefaultLanguage";
  // Sentences
  public static final String CREATE = "CREATE TABLE IF NOT EXISTS "
      + TABLE_NAME
      + "("
      + _ID
      + " INTEGER PRIMARY KEY AUTOINCREMENT, "
      + GEO_NODE_ID
      + " INTEGER NOT NULL, "
      + COUNTRY_CODE
      + " TEXT NOT NULL UNIQUE,"
      + PHONE_PREFIX
      + " TEXT);";
  public static final String DEFAULT_ISO = "en";
  private static final String SEPARATOR = ".";
  private static final String[] COLUMNS;
  // Join
  private static final String TABLE_JOIN = TABLE_NAME
      + " INNER JOIN "
      + CountryNameDbDao.TABLE_NAME
      + " ON "
      + CountryNameDbDao.TABLE_NAME
      + SEPARATOR
      + CountryNameDbDao.COUNTRY_ID
      + "="
      + TABLE_NAME
      + SEPARATOR
      + _ID
      + " INNER JOIN "
      + LanguageDbDao.TABLE_NAME
      + " ON "
      + CountryNameDbDao.TABLE_NAME
      + SEPARATOR
      + CountryNameDbDao.LANGUAGE_ID
      + "="
      + LanguageDbDao.TABLE_NAME
      + SEPARATOR
      + _ID
      + " INNER JOIN "
      + CountryNameDbDao.TABLE_NAME
      + " AS "
      + COUNTRY_NAME_EN
      + " ON "
      + COUNTRY_NAME_EN
      + SEPARATOR
      + CountryNameDbDao.COUNTRY_ID
      + "="
      + TABLE_NAME
      + SEPARATOR
      + _ID
      + " INNER JOIN "
      + LanguageDbDao.TABLE_NAME
      + " AS "
      + DEFAULT_LANGUAGE
      + " ON "
      + COUNTRY_NAME_EN
      + SEPARATOR
      + CountryNameDbDao.LANGUAGE_ID
      + "="
      + DEFAULT_LANGUAGE
      + SEPARATOR
      + LanguageDbDao._ID;
  private static final String SELECTION_ALL = LanguageDbDao.TABLE_NAME
      + SEPARATOR
      + LanguageDbDao.ISO_CODE
      + "=? AND "
      + DEFAULT_LANGUAGE
      + SEPARATOR
      + LanguageDbDao.ISO_CODE
      + "=?";
  private static final String SELECTION_PHONE_PREFIX = LanguageDbDao.TABLE_NAME
      + SEPARATOR
      + LanguageDbDao.ISO_CODE
      + "=? AND "
      + DEFAULT_LANGUAGE
      + SEPARATOR
      + LanguageDbDao.ISO_CODE
      + "=?"
      + " AND ("
      + TABLE_NAME
      + SEPARATOR
      + PHONE_PREFIX
      + " IS NOT NULL"
      + " OR "
      + TABLE_NAME
      + SEPARATOR
      + PHONE_PREFIX
      + "!='')";
  private static final String SELECTION_BY_CODE = TABLE_NAME
      + SEPARATOR
      + COUNTRY_CODE
      + "=? AND "
      + LanguageDbDao.TABLE_NAME
      + SEPARATOR
      + LanguageDbDao.ISO_CODE
      + "=? AND "
      + DEFAULT_LANGUAGE
      + SEPARATOR
      + LanguageDbDao.ISO_CODE
      + "=?";
  private static final String ORDER_JOIN =
      CountryNameDbDao.TABLE_NAME + SEPARATOR + CountryNameDbDao.NAME + " ASC";

  static {
    PROJECTION_MAP = new HashMap<>();
    PROJECTION_MAP.put(_ID, TABLE_NAME + SEPARATOR + _ID);
    PROJECTION_MAP.put(GEO_NODE_ID, TABLE_NAME + SEPARATOR + GEO_NODE_ID);
    PROJECTION_MAP.put(COUNTRY_CODE, TABLE_NAME + SEPARATOR + COUNTRY_CODE);
    PROJECTION_MAP.put(PHONE_PREFIX, TABLE_NAME + SEPARATOR + PHONE_PREFIX);
    PROJECTION_MAP.put(CountryNameDbDao.NAME,
        CountryNameDbDao.TABLE_NAME + SEPARATOR + CountryNameDbDao.NAME);
    PROJECTION_MAP.put(NAME_DEFAULT,
        COUNTRY_NAME_EN + SEPARATOR + CountryNameDbDao.NAME + " AS " + NAME_DEFAULT);
    COLUMNS = new String[] {
        PROJECTION_MAP.get(_ID), PROJECTION_MAP.get(GEO_NODE_ID), PROJECTION_MAP.get(COUNTRY_CODE),
        PROJECTION_MAP.get(PHONE_PREFIX), PROJECTION_MAP.get(CountryNameDbDao.NAME),
        PROJECTION_MAP.get(NAME_DEFAULT)
    };
  }

  private final CountryDbMapper countryDbMapper;

  public CountryDbDao() {
    countryDbMapper = new CountryDbMapper();
  }

  public List<Country> getAllCountriesLocalized(SQLiteDatabase database, String languageIsoCode,
      boolean filterNullPhonePrefix) {
    List<Country> countries = new LinkedList<>();
    Cursor cursor = null;
    try {
      String selection = filterNullPhonePrefix ? SELECTION_PHONE_PREFIX : SELECTION_ALL;
      cursor = database.query(TABLE_JOIN, COLUMNS, selection,
          new String[] { languageIsoCode, DEFAULT_ISO }, null, null, ORDER_JOIN);
      countries = countryDbMapper.getCountries(cursor);
    } catch (SQLiteException e) {
      Log.e(TABLE_NAME, e.getMessage(), e);
    } finally {
      if (cursor != null) {
        cursor.close();
      }
    }
    return countries;
  }

  public Country getCountryByCountryCode(SQLiteDatabase database, String languageIsoCode,
      String countryCode) {
    Country country = null;
    Cursor cursor = null;
    try {
      cursor = database.query(TABLE_JOIN, COLUMNS, SELECTION_BY_CODE,
          new String[] { countryCode, languageIsoCode, DEFAULT_ISO }, null, null, null);
      if (cursor != null && cursor.moveToFirst()) {
        country = countryDbMapper.getCountry(cursor);
      }
    } catch (SQLiteException e) {
      Log.e(TABLE_NAME, e.getMessage(), e);
    } finally {
      if (cursor != null) {
        cursor.close();
      }
    }
    return country;
  }

  @Override public String getTable() {
    return TABLE_NAME;
  }

  @Override public ContentValues createContentValues(NetCountry country) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(GEO_NODE_ID, country.getGeoNodeId());
    contentValues.put(COUNTRY_CODE, country.getCountryCode());
    contentValues.put(PHONE_PREFIX, country.getPhonePrefix());
    return contentValues;
  }

  @Override protected ContentValues createBulkContentValues(String[] csvEntry) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(_ID, csvEntry[0]);
    contentValues.put(GEO_NODE_ID, csvEntry[1]);
    contentValues.put(COUNTRY_CODE, csvEntry[2]);
    if (csvEntry.length == 4 && !TextUtils.isEmpty(csvEntry[3])) {
      contentValues.put(PHONE_PREFIX, csvEntry[3]);
    } else {
      contentValues.putNull(PHONE_PREFIX);
    }
    return contentValues;
  }
}
