package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.Traveller;
import com.odigeo.dataodigeo.db.dao.TravellerDBDAO;
import java.util.ArrayList;

public class TravellerDBMapper {
  /**
   * Mapper traveller cursor list
   *
   * @param cursor Cursor with traveller data
   * @return {@link ArrayList<Traveller>}
   */
  public ArrayList<Traveller> getTravellerList(Cursor cursor) {
    ArrayList<Traveller> travellerList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long dateOfBirth = cursor.getLong(cursor.getColumnIndex(TravellerDBDAO.DATE_OF_BIRTH));
        String identification =
            cursor.getString(cursor.getColumnIndex(TravellerDBDAO.IDENTIFICATION));
        String identificationType =
            cursor.getString(cursor.getColumnIndex(TravellerDBDAO.IDENTIFICATION_TYPE));
        String lastname = cursor.getString(cursor.getColumnIndex(TravellerDBDAO.LASTNAME));
        String name = cursor.getString(cursor.getColumnIndex(TravellerDBDAO.NAME));
        long id = cursor.getLong(cursor.getColumnIndex(TravellerDBDAO.ID));

        String countryCode = cursor.getString(cursor.getColumnIndex(TravellerDBDAO.COUNTRY_CODE));
        long identificationExpirationDate =
            cursor.getLong(cursor.getColumnIndex(TravellerDBDAO.IDENTIFICATION_EXPIRATION_DATE));
        String identificationIssueCountryCode = cursor.getString(
            cursor.getColumnIndex(TravellerDBDAO.IDENTIFICATION_ISSUE_COUNTRY_CODE));
        String localityCode = cursor.getString(cursor.getColumnIndex(TravellerDBDAO.LOCALITY_CODE));
        String meal = cursor.getString(cursor.getColumnIndex(TravellerDBDAO.MEAL));
        String nationality = cursor.getString(cursor.getColumnIndex(TravellerDBDAO.NATIONALITY));
        String title = cursor.getString(cursor.getColumnIndex(TravellerDBDAO.TITLE));
        String travellerGender =
            cursor.getString(cursor.getColumnIndex(TravellerDBDAO.TRAVELLER_GENDER));
        String travellerType =
            cursor.getString(cursor.getColumnIndex(TravellerDBDAO.TRAVELLER_TYPE));
        long bookingId = cursor.getLong(cursor.getColumnIndex(TravellerDBDAO.BOOKING_ID));

        travellerList.add(
            new Traveller(dateOfBirth, identification, identificationType, lastname, name, id,
                countryCode, identificationExpirationDate, identificationIssueCountryCode,
                localityCode, meal, nationality, title, travellerGender, travellerType, bookingId));
      }
    }

    return travellerList;
  }

  /**
   * Mapper traveller cursor
   *
   * @param cursor Cursor with traveller data
   * @return {@link Traveller}
   */
  public Traveller getTraveller(Cursor cursor) {
    ArrayList<Traveller> travellerList = getTravellerList(cursor);

    if (travellerList.size() == 1) {
      return travellerList.get(0);
    } else {
      return null;
    }
  }
}
