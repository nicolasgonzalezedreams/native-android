package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.Baggage;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.entity.booking.Traveller;
import com.odigeo.dataodigeo.db.dao.BaggageDBDAO;
import java.util.ArrayList;

public class BaggageDBMapper {
  /**
   * Mapper baggage cursor list
   *
   * @param cursor Cursor with baggage data
   * @return {@link ArrayList<Baggage>}
   */
  public ArrayList<Baggage> getBaggageList(Cursor cursor) {
    ArrayList<Baggage> baggageList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(BaggageDBDAO.ID));
        boolean includedInPrice =
            (cursor.getInt(cursor.getColumnIndex(BaggageDBDAO.INCLUDED_IN_PRICE)) == 1);
        long pieces = cursor.getLong(cursor.getColumnIndex(BaggageDBDAO.PIECES));
        long weight = cursor.getLong(cursor.getColumnIndex(BaggageDBDAO.WEIGHT));
        long segmentId = cursor.getLong(cursor.getColumnIndex(BaggageDBDAO.SEGMENT_ID));
        long travellerId = cursor.getLong(cursor.getColumnIndex(BaggageDBDAO.TRAVELLER_ID));

        baggageList.add(new Baggage(id, includedInPrice, pieces, weight, new Segment(segmentId),
            new Traveller(travellerId)));
      }
    }

    return baggageList;
  }

  /**
   * Mapper baggage cursor
   *
   * @param cursor Cursor with baggage data
   * @return {@link Baggage}
   */
  public Baggage getBaggage(Cursor cursor) {
    ArrayList<Baggage> baggageList = getBaggageList(cursor);

    if (baggageList.size() == 1) {
      return baggageList.get(0);
    } else {
      return null;
    }
  }
}
