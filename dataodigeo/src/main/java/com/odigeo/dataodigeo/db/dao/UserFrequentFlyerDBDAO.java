package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.UserFrequentFlyerDBDAOInterface;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.UserFrequentFlyerDBMapper;
import java.util.List;

public class UserFrequentFlyerDBDAO extends OdigeoSQLiteOpenHelper
    implements UserFrequentFlyerDBDAOInterface {

  private UserFrequentFlyerDBMapper mUserFrequentFlyerDBMapper;

  public UserFrequentFlyerDBDAO(Context context) {
    super(context);
    mUserFrequentFlyerDBMapper = new UserFrequentFlyerDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + USER_FREQUENT_FLYER_ID
        + " NUMERIC, "
        + AIRLINE_CODE
        + " TEXT, "
        + FREQUENT_FLYER_NUMBER
        + " TEXT, "
        + USER_TRAVELLER_ID
        + " INTEGER "
        + ");";
  }

  @Override public UserFrequentFlyer getUserFrequentFlyer(long userFrequentFlyerId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM "
        + TABLE_NAME
        + " WHERE "
        + USER_FREQUENT_FLYER_ID
        + " = "
        + userFrequentFlyerId
        + "";
    Cursor data = db.rawQuery(selectQuery, null);
    UserFrequentFlyer userFrequentFlyer = mUserFrequentFlyerDBMapper.getUserFrequentFlyer(data);
    data.close();

    return userFrequentFlyer;
  }

  @Override public List<UserFrequentFlyer> getUserFrequentFlyerList(long userTravellerId) {
    SQLiteDatabase db = getOdigeoDatabase();
    Cursor data = db.query(TABLE_NAME, null, USER_TRAVELLER_ID + "=?",
        new String[] { String.valueOf(userTravellerId) }, null, null, null);
    List<UserFrequentFlyer> frequentFlyerList =
        mUserFrequentFlyerDBMapper.getUserFrequentFlyerList(data);
    data.close();

    return frequentFlyerList;
  }

  @Override public long addUserFrequentFlyer(UserFrequentFlyer userFrequentFlyer) {

    SQLiteDatabase db = getOdigeoDatabase();
    long newRowId = db.insert(TABLE_NAME, null, getContentValues(userFrequentFlyer));

    /**
     * When userFrequentFlyerId is less to 1, is because in the app have not a user logged, and we need to set a valid id
     */
    if (userFrequentFlyer.getFrequentFlyerId() < 1) {
      UserFrequentFlyer newUserFrequentFlyer =
          new UserFrequentFlyer(newRowId, newRowId, userFrequentFlyer.getAirlineCode(),
              userFrequentFlyer.getFrequentFlyerNumber(), userFrequentFlyer.getUserTravellerId());
      updateUserFrequentFlyerByLocalId(newUserFrequentFlyer);
    }
    return newRowId;
  }

  @Override public boolean updateUserFrequentFlyer(long userFrequentFlyerId,
      UserFrequentFlyer userFrequentFlyer) {
    SQLiteDatabase db = getOdigeoDatabase();

    long rowsAffected = db.update(TABLE_NAME, getContentValues(userFrequentFlyer),
        USER_FREQUENT_FLYER_ID + "=" + userFrequentFlyerId, null);

    return (rowsAffected != 0);
  }

  @Override public long updateUserFrequentFlyerByLocalId(UserFrequentFlyer userFrequentFlyer) {
    SQLiteDatabase db = getOdigeoDatabase();
    long rowsAffected = db.update(TABLE_NAME, getContentValues(userFrequentFlyer),
        ID + "=" + userFrequentFlyer.getId(), null);

    return rowsAffected;
  }

  @Override public boolean updateOrCreateUserFrequentFlyer(UserFrequentFlyer userFrequentFlyer) {
    return updateUserFrequentFlyer(userFrequentFlyer.getFrequentFlyerId(), userFrequentFlyer) || (
        addUserFrequentFlyer(userFrequentFlyer)
            != -1);
  }

  @Override public long deleteUserFrequentFlyer(long userFrequentFlyerId) {
    SQLiteDatabase database = getOdigeoDatabase();
    long rowsAffected = database.delete(TABLE_NAME, USER_FREQUENT_FLYER_ID + "=?",
        new String[] { String.valueOf(userFrequentFlyerId) });
    return rowsAffected;
  }

  private ContentValues getContentValues(UserFrequentFlyer userFrequentFlyer) {
    ContentValues values = new ContentValues();
    values.put(USER_FREQUENT_FLYER_ID, userFrequentFlyer.getFrequentFlyerId());
    values.put(AIRLINE_CODE, userFrequentFlyer.getAirlineCode());
    values.put(FREQUENT_FLYER_NUMBER, userFrequentFlyer.getFrequentFlyerNumber());
    values.put(USER_TRAVELLER_ID, userFrequentFlyer.getUserTravellerId());
    return values;
  }
}
