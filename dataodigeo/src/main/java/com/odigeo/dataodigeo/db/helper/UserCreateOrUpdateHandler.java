package com.odigeo.dataodigeo.db.helper;

import com.odigeo.data.db.dao.MembershipDBDAOInterface;
import com.odigeo.data.db.dao.SearchSegmentDBDAOInterface;
import com.odigeo.data.db.dao.StoredSearchDBDAOInterface;
import com.odigeo.data.db.dao.UserAddressDBDAOInterface;
import com.odigeo.data.db.dao.UserDBDAOInterface;
import com.odigeo.data.db.dao.UserFrequentFlyerDBDAOInterface;
import com.odigeo.data.db.dao.UserIdentificationDBDAOInterface;
import com.odigeo.data.db.dao.UserProfileDBDAOInterface;
import com.odigeo.data.db.dao.UserTravellerDBDAOInterface;
import com.odigeo.data.db.helper.TravellersHandlerInterface;
import com.odigeo.data.db.helper.UserCreateOrUpdateHandlerInterface;
import com.odigeo.data.entity.booking.SearchSegment;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.entity.userData.Membership;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.entity.userData.UserAddress;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.data.tracker.CrashlyticsController;
import java.util.List;

public class UserCreateOrUpdateHandler implements UserCreateOrUpdateHandlerInterface {

  private UserDBDAOInterface mUserDBDao;
  private UserTravellerDBDAOInterface mUserTravellerDBDao;
  private UserProfileDBDAOInterface mUserProfileDBDao;
  private UserAddressDBDAOInterface mUserAddressDBDao;
  private UserFrequentFlyerDBDAOInterface mUserFrequentFlyerDBDao;
  private UserIdentificationDBDAOInterface mUserIdentificationDBDao;
  private TravellersHandlerInterface mReadEntitiesInDB;
  private StoredSearchDBDAOInterface mStoredSearchDBDao;
  private SearchSegmentDBDAOInterface mSearchSegmentDBDao;
  private MembershipDBDAOInterface membershipDBDAOInterface;
  private CrashlyticsController crashlyticsController;

  public UserCreateOrUpdateHandler(UserDBDAOInterface userDBDAO,
      UserTravellerDBDAOInterface userTravellerDBDao, UserProfileDBDAOInterface userProfileDBDao,
      UserAddressDBDAOInterface userAddressDBDao,
      UserFrequentFlyerDBDAOInterface userFrequentFlyerDBDao,
      UserIdentificationDBDAOInterface userIdentificationDBDao,
      TravellersHandlerInterface readEntitiesInDB, StoredSearchDBDAOInterface storedSearchDBDao,
      SearchSegmentDBDAOInterface searchSegmentDBDao,
      MembershipDBDAOInterface membershipDBDAOInterface,
      CrashlyticsController crashlyticsController) {

    mUserDBDao = userDBDAO;
    mUserTravellerDBDao = userTravellerDBDao;
    mUserProfileDBDao = userProfileDBDao;
    mUserAddressDBDao = userAddressDBDao;
    mUserFrequentFlyerDBDao = userFrequentFlyerDBDao;
    mUserIdentificationDBDao = userIdentificationDBDao;
    mReadEntitiesInDB = readEntitiesInDB;
    mStoredSearchDBDao = storedSearchDBDao;
    mSearchSegmentDBDao = searchSegmentDBDao;
    this.membershipDBDAOInterface = membershipDBDAOInterface;
    this.crashlyticsController = crashlyticsController;
  }

  @Override public boolean addOrCreateUserAndAllComponents(User user) {
    boolean isUserUpdated = true;
    boolean areUserTravellersUpdated = true;
    boolean areStoredSearchesUpdated = true;
    boolean isMembershipUpdated = true;

    if (user != null) {

      isUserUpdated = mUserDBDao.updateOrCreateUser(user);
      List userTravellerList = user.getUserTravellerList();
      if (userTravellerList != null) {
        if (hasDefaultTraveller(userTravellerList)) {
          setAllUserToNotDefault();
        }
        for (int i = 0; i < user.getUserTravellerList().size(); i++) {
          UserTraveller userTraveller = user.getUserTravellerList().get(i);
          if (!updateOrCreateAUserTravellerAndAllComponentsInDB(userTraveller)) {
            areUserTravellersUpdated = false;
          }
        }
      }
      List<StoredSearch> storedSearchList = user.getUserSearchesList();
      if (storedSearchList != null) {
        for (int i = 0; i < storedSearchList.size(); i++) {
          StoredSearch storedSearch = storedSearchList.get(i);
          if (!updateOrCreateStoredSearchAndAllComponentsInDB(storedSearch)) {
            areStoredSearchesUpdated = false;
          }
        }
      }

      try {
        isMembershipUpdated = updateMembership(user.getMemberships());
      } catch(Exception e) {
        crashlyticsController.trackNonFatal(e);
        membershipDBDAOInterface.createTableIfNotExists();
        isMembershipUpdated = updateMembership(user.getMemberships());
      }
    }
    return isUserUpdated
        && areUserTravellersUpdated
        && areStoredSearchesUpdated
        && isMembershipUpdated;
  }

  private boolean updateMembership(List<Membership> memberships) {
    boolean isMembershipUpdated = true;
    for (Membership membership : memberships) {
      isMembershipUpdated =
          isMembershipUpdated && membershipDBDAOInterface.addMembership(membership);
    }

    return isMembershipUpdated;
  }

  public void setAllUserToNotDefault() {
    List<UserTraveller> userTravellersInDevice = mReadEntitiesInDB.getFullUserTravellerList();
    if (hasDefaultTraveller(userTravellersInDevice)) {
      for (UserTraveller userTraveller : userTravellersInDevice) {
        if (userTraveller != null
            && userTraveller.getUserProfile() != null
            && userTraveller.getUserProfile().isDefaultTraveller()) {
          UserProfile userProfile = userTraveller.getUserProfile();
          UserProfile noDefaultUserProfile =
              new UserProfile(userProfile.getId(), userProfile.getUserProfileId(),
                  userProfile.getGender(), userProfile.getTitle(), userProfile.getName(),
                  userProfile.getMiddleName(), userProfile.getFirstLastName(),
                  userProfile.getSecondLastName(), userProfile.getBirthDate(),
                  userProfile.getPrefixPhoneNumber(), userProfile.getPhoneNumber(),
                  userProfile.getPrefixAlternatePhoneNumber(),
                  userProfile.getAlternatePhoneNumber(), userProfile.getMobilePhoneNumber(),
                  userProfile.getNationalityCountryCode(), userProfile.getCpf(), false,
                  userProfile.getPhoto(), userProfile.getUserAddress(),
                  userProfile.getUserIdentificationList(), userProfile.getUserTravellerId());

          UserTraveller noDefaultUserTraveller =
              new UserTraveller(userTraveller.getId(), userTraveller.getUserTravellerId(),
                  userTraveller.getBuyer(), userTraveller.getEmail(),
                  userTraveller.getTypeOfTraveller(), userTraveller.getMealType(),
                  userTraveller.getUserFrequentFlyers(), noDefaultUserProfile,
                  userTraveller.getUserId());
          updateOrCreateAUserTravellerAndAllComponentsInDB(noDefaultUserTraveller);
        }
      }
    }
  }

  private boolean hasDefaultTraveller(List<UserTraveller> userTravellerList) {
    if (userTravellerList != null) {
      for (UserTraveller isDefaultUserTraveller : userTravellerList) {
        if (isDefaultUserTraveller != null
            && isDefaultUserTraveller.getUserProfile() != null
            && isDefaultUserTraveller.getUserProfile().isDefaultTraveller()) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public boolean updateOrCreateStoredSearchAndAllComponentsInDB(StoredSearch storedSearch) {
    boolean areSearchSegmentsUpdated = true;
    if (storedSearch != null && (mStoredSearchDBDao.updateStoredSearch(storedSearch)
        || mStoredSearchDBDao.addStoredSearch(storedSearch) != -1)) {
      areSearchSegmentsUpdated = updateSearchSegments(storedSearch);
      return areSearchSegmentsUpdated;
    }
    return false;
  }

  @Override
  public boolean updateOrCreateAUserTravellerAndAllComponentsInDB(UserTraveller userTraveller) {
    boolean isUserProfilesUpdated = true;
    boolean isUserAddressesUpdated = true;
    boolean areUserIdentificationsUpdated = true;
    boolean areUserFrequentFlyersUpdated;
    if (userTraveller != null && mUserTravellerDBDao.updateOrCreateUserTraveller(userTraveller)) {

      UserProfile userProfile = userTraveller.getUserProfile();
      if (userProfile != null) {
        if (mUserProfileDBDao.updateOrCreateUserProfile(userProfile)) {
          if (userProfile.getUserAddress() != null) {
            isUserAddressesUpdated =
                mUserAddressDBDao.updateOrCreateUserAddress(userProfile.getUserAddress());
          }
          areUserIdentificationsUpdated =
              updateOrCreateUserIdentifications(userProfile.getUserIdentificationList(),
                  userProfile.getUserProfileId());
        } else {
          isUserProfilesUpdated = false;
        }
      }
      areUserFrequentFlyersUpdated = updateFrequentFlyers(userTraveller);
      return isUserAddressesUpdated
          && areUserFrequentFlyersUpdated
          && isUserProfilesUpdated
          && areUserIdentificationsUpdated;
    }
    return false;
  }

  /**
   * Perform whole the process to update, add and delete the frequent flyer code for a {@link
   * UserTraveller}.
   *
   * @param userTraveller Owner of the frequent flyer codes.
   * @return If the process went OK.
   */
  private boolean updateFrequentFlyers(UserTraveller userTraveller) {
    boolean areUserFrequentFlyersUpdated = true;
    // Get both lists, from view and database.
    List<UserFrequentFlyer> flyerListFromView = userTraveller.getUserFrequentFlyers();
    List<UserFrequentFlyer> flyerListFromDBToDelete =
        mUserFrequentFlyerDBDao.getUserFrequentFlyerList(userTraveller.getUserTravellerId());
    if (flyerListFromView != null) {
      for (UserFrequentFlyer itemFrequent : flyerListFromView) {
        if (itemFrequent != null) {
          if (mUserFrequentFlyerDBDao.updateOrCreateUserFrequentFlyer(itemFrequent)) {
            flyerListFromDBToDelete.remove(itemFrequent);
          } else {
            areUserFrequentFlyersUpdated = false;
          }
        }
      }
    }
    // Delete the left frequent flyers from the database.
    for (UserFrequentFlyer item : flyerListFromDBToDelete) {
      mUserFrequentFlyerDBDao.deleteUserFrequentFlyer(item.getFrequentFlyerId());
    }
    return areUserFrequentFlyersUpdated;
  }

  /**
   * Perform whole the process to update, add and delete the search segment code for a {@link
   * StoredSearch}.
   *
   * @param storedSearch Search where the segment belongs.
   * @return If the process went OK.
   */
  private boolean updateSearchSegments(StoredSearch storedSearch) {
    boolean areSearchSegmentsUpdated = true;
    // Get both lists, from view and database.
    List<SearchSegment> searchSegmentListFromView = storedSearch.getSegmentList();
    List<SearchSegment> searchSegmentListFromDBToDelete =
        mSearchSegmentDBDao.getSearchSegmentList(storedSearch.getStoredSearchId());
    if (searchSegmentListFromView != null) {
      for (SearchSegment itemSegment : searchSegmentListFromView) {
        if (itemSegment != null) {
          if (mSearchSegmentDBDao.updateSearchSegment(itemSegment.getSearchSegmentId(), itemSegment)
              || mSearchSegmentDBDao.addSearchSegment(itemSegment)) {
            searchSegmentListFromDBToDelete.remove(itemSegment);
          } else {
            areSearchSegmentsUpdated = false;
          }
        }
      }
    }

    for (SearchSegment item : searchSegmentListFromDBToDelete) {
      mSearchSegmentDBDao.deleteSearchSegment(item.getSearchSegmentId());
    }
    return areSearchSegmentsUpdated;
  }

  private boolean updateOrCreateUserIdentifications(List<UserIdentification> userIdentificationList,
      long userProfileId) {
    boolean areUserIdentificationsUpdated = true;
    List<UserIdentification> userIdentificationListToDelete =
        mUserIdentificationDBDao.getUserIdentifications(userProfileId);
    if (userIdentificationList != null) {
      for (UserIdentification userIdentification : userIdentificationList) {
        if (userIdentification != null) {
          if (mUserIdentificationDBDao.updateOrCreateUserIdentification(userIdentification)) {
            userIdentificationListToDelete.remove(userIdentification);
          } else {
            areUserIdentificationsUpdated = false;
          }
        }
      }
    }
    for (UserIdentification userIdentificationToDelete : userIdentificationListToDelete) {
      mUserIdentificationDBDao.deleteUserIdentification(
          userIdentificationToDelete.getUserIdentificationId());
    }
    return areUserIdentificationsUpdated;
  }

  @Override public long saveAUserTravellerAndAllComponentsInDBWithoutUserLogged(
      UserTraveller fullUserTraveller) {
    if (fullUserTraveller != null) {
      long userTravellerId = mUserTravellerDBDao.addUserTraveller(fullUserTraveller);
      if (userTravellerId != -1) {
        UserProfile userProfile = fullUserTraveller.getUserProfile();
        if (userProfile != null) {
          UserProfile newUserProfileProfile =
              new UserProfile(-1, -1, userProfile.getGender(), userProfile.getTitle(),
                  userProfile.getName(), userProfile.getMiddleName(),
                  userProfile.getFirstLastName(), userProfile.getSecondLastName(),
                  userProfile.getBirthDate(), userProfile.getPrefixPhoneNumber(),
                  userProfile.getPhoneNumber(), userProfile.getPrefixAlternatePhoneNumber(),
                  userProfile.getAlternatePhoneNumber(), userProfile.getMobilePhoneNumber(),
                  userProfile.getNationalityCountryCode(), userProfile.getCpf(),
                  userProfile.isDefaultTraveller(), userProfile.getPhoto(),
                  userProfile.getUserAddress(), userProfile.getUserIdentificationList(),
                  userTravellerId);
          long userProfileId = mUserProfileDBDao.addUserProfile(newUserProfileProfile);
          UserAddress userAddress = userProfile.getUserAddress();
          if (userAddress != null) {
            UserAddress newUserAddress =
                new UserAddress(-1, -1, userAddress.getAddress(), userAddress.getAddressType(),
                    userAddress.getCity(), userAddress.getCountry(), userAddress.getState(),
                    userAddress.getPostalCode(), userAddress.getIsPrimary(), userAddress.getAlias(),
                    userProfileId);
            mUserAddressDBDao.addUserAddress(newUserAddress);
          }
          List<UserIdentification> userIdentificationList = userProfile.getUserIdentificationList();
          if (userIdentificationList != null) {
            for (UserIdentification userIdentification : userIdentificationList) {
              if (userIdentification != null) {
                UserIdentification newUserIdentification =
                    new UserIdentification(-1, -1, userIdentification.getIdentificationId(),
                        userIdentification.getIdentificationCountryCode(),
                        userIdentification.getIdentificationExpirationDate(),
                        userIdentification.getIdentificationType(), userProfileId);
                mUserIdentificationDBDao.addUserIdentification(newUserIdentification);
              }
            }
          }
        }
        List<UserFrequentFlyer> frequentFlyerList = fullUserTraveller.getUserFrequentFlyers();
        if (frequentFlyerList != null) {
          for (UserFrequentFlyer frequentFlyer : frequentFlyerList) {
            if (frequentFlyer != null) {
              UserFrequentFlyer newFrequentFlyer =
                  new UserFrequentFlyer(-1, -1, frequentFlyer.getAirlineCode(),
                      frequentFlyer.getFrequentFlyerNumber(), userTravellerId);
              mUserFrequentFlyerDBDao.addUserFrequentFlyer(newFrequentFlyer);
            }
          }
        }
      }
      return userTravellerId;
    }
    return -1;
  }

  @Override public boolean deleteUserTravellerCompletely(long userTravellerId) {
    return deleteUserTravellerCompletely(mReadEntitiesInDB.getFullUserTraveller(userTravellerId));
  }

  @Override public boolean deleteUserTravellerCompletely(UserTraveller fullUserTraveller) {
    if (fullUserTraveller.getUserFrequentFlyers() != null) {
      for (UserFrequentFlyer userFrequentFlyer : fullUserTraveller.getUserFrequentFlyers()) {
        if (mUserFrequentFlyerDBDao.deleteUserFrequentFlyer(userFrequentFlyer.getFrequentFlyerId())
            < 1) {
          return false;
        }
      }
    }
    UserProfile userProfile = fullUserTraveller.getUserProfile();
    if (userProfile.getUserIdentificationList() != null) {
      for (UserIdentification userIdentification : userProfile.getUserIdentificationList()) {
        if (!mUserIdentificationDBDao.deleteUserIdentification(
            userIdentification.getUserIdentificationId())) {
          return false;
        }
      }
    }
    if (userProfile.getUserAddress() != null) {
      if (!mUserAddressDBDao.deleteUserAddress(userProfile.getUserAddress().getUserAddressId())) {
        return false;
      }
    }
    return mUserProfileDBDao.deleteUserProfile(userProfile.getUserProfileId())
        && mUserTravellerDBDao.deleteUserTraveller(fullUserTraveller.getUserTravellerId());
  }
}
