package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import android.util.Log;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.dataodigeo.db.dao.UserTravellerDBDAO;
import java.util.ArrayList;

public class UserTravellerDBMapper {

  /**
   * Mapper user traveller cursor list
   *
   * @param cursor Cursor with user traveller data
   * @return {@link ArrayList< UserTraveller >}
   */
  public ArrayList<UserTraveller> getUserTravellerList(Cursor cursor) {
    ArrayList<UserTraveller> userTravellerList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(UserTravellerDBDAO.ID));
        long userTravellerId =
            cursor.getLong(cursor.getColumnIndex(UserTravellerDBDAO.USER_TRAVELLER_ID));
        boolean buyer = (cursor.getInt(cursor.getColumnIndex(UserTravellerDBDAO.BUYER)) == 1);
        String email = cursor.getString(cursor.getColumnIndex(UserTravellerDBDAO.EMAIL));
        String mealType = cursor.getString(cursor.getColumnIndex(UserTravellerDBDAO.MEAL_TYPE));
        long userId = cursor.getLong(cursor.getColumnIndex(UserTravellerDBDAO.USER_ID));

        try {

          UserTraveller.TypeOfTraveller typeOfTraveller = null;
          if (cursor.getString(cursor.getColumnIndex(UserTravellerDBDAO.TYPE_OF_TRAVELLER))
              != null) {
            typeOfTraveller = UserTraveller.TypeOfTraveller.valueOf(
                cursor.getString(cursor.getColumnIndex(UserTravellerDBDAO.TYPE_OF_TRAVELLER)));
          } else {
            typeOfTraveller = UserTraveller.TypeOfTraveller.UNKNOWN;
          }
          userTravellerList.add(
              new UserTraveller(id, userTravellerId, buyer, email, typeOfTraveller, mealType,
                  userId));
        } catch (IllegalArgumentException e) {
          Log.e("UserTravellerDBMapper", e.getMessage());
        }
      }
    }
    return userTravellerList;
  }

  public ArrayList<UserTraveller> getUserTravellerListSimplified(Cursor cursor) {
    ArrayList<UserTraveller> userTravellerList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long userTravellerId =
            cursor.getLong(cursor.getColumnIndex(UserTravellerDBDAO.USER_TRAVELLER_ID));

        try {
          UserTraveller.TypeOfTraveller typeOfTraveller = UserTraveller.TypeOfTraveller.valueOf(
              cursor.getString(cursor.getColumnIndex(UserTravellerDBDAO.TYPE_OF_TRAVELLER)));

          userTravellerList.add(
              new UserTraveller(0, userTravellerId, false, null, typeOfTraveller, null, 0));
        } catch (IllegalArgumentException e) {
          Log.e("UserTravellerDBMapper", e.getMessage());
        }
      }
    }
    cursor.close();
    return userTravellerList;
  }

  /**
   * Mapper user traveller cursor
   *
   * @param cursor Cursor with user traveller data
   * @return {@link UserTraveller}
   */
  public UserTraveller getUserTraveller(Cursor cursor) {
    ArrayList<UserTraveller> userTravellerList = getUserTravellerList(cursor);

    if (userTravellerList.size() == 1) {
      return userTravellerList.get(0);
    } else {
      return null;
    }
  }
}
