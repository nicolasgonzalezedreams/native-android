package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.LocationBookingDBDAOInterface;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.LocationBookingDBMapper;
import java.util.ArrayList;
import java.util.List;

public class LocationBookingDBDAO extends OdigeoSQLiteOpenHelper
    implements LocationBookingDBDAOInterface {
  private LocationBookingDBMapper mLocationBookingDBMapper;

  public LocationBookingDBDAO(Context context) {
    super(context);
    mLocationBookingDBMapper = new LocationBookingDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + CITY_IATA_CODE
        + " NUMERIC, "
        + CITY_NAME
        + " TEXT, "
        + COUNTRY
        + " TEXT, "
        + GEO_NODE_ID
        + " NUMERIC PRIMARY KEY, "
        + LATITUDE
        + " NUMERIC, "
        + LOCATION_CODE
        + " TEXT, "
        + LOCATION_NAME
        + " TEXT, "
        + LOCATION_TYPE
        + " TEXT, "
        + LONGITUDE
        + " NUMERIC, "
        + MATCH_NAME
        + " TEXT, "
        + COUNTRY_CODE
        + " TEXT, "
        + CURRENCY_CODE
        + " TEXT, "
        + CURRENCY_RATE
        + " NUMERIC, "
        + CURRENCY_UPDATED_DATE
        + " NUMERIC, "
        + TIMEZONE
        + " TEXT "
        + ");";
  }

  @Override public LocationBooking getLocationBooking(long locationGeoNodeId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + GEO_NODE_ID + " = " + locationGeoNodeId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    LocationBooking locationBooking = mLocationBookingDBMapper.getLocationBooking(data);
    data.close();

    return locationBooking;
  }

  @Override public long addLocationBooking(LocationBooking locationBooking) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(CITY_IATA_CODE, locationBooking.getCityIATACode());
    values.put(CITY_NAME, locationBooking.getCityName());
    values.put(COUNTRY, locationBooking.getCountry());
    values.put(GEO_NODE_ID, locationBooking.getGeoNodeId());
    values.put(LATITUDE, locationBooking.getLatitude());
    values.put(LOCATION_CODE, locationBooking.getLocationCode());
    values.put(LOCATION_NAME, locationBooking.getLocationName());
    values.put(LOCATION_TYPE, locationBooking.getLocationType());
    values.put(LONGITUDE, locationBooking.getLongitude());
    values.put(MATCH_NAME, locationBooking.getMatchName());

    values.put(COUNTRY_CODE, locationBooking.getCountryCode());
    values.put(CURRENCY_CODE, locationBooking.getCurrencyCode());
    values.put(CURRENCY_RATE, locationBooking.getCurrencyRate());
    values.put(CURRENCY_UPDATED_DATE, locationBooking.getCurrencyUpdatedDate());
    values.put(TIMEZONE, locationBooking.getTimezone());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }

  @Override public List<Long> addLocationBookings(List<LocationBooking> locationBookings) {
    List<Long> locationBookingsIds = new ArrayList<>();
    LocationBooking location;
    LocationBooking existingLocation;

    for (int i = 0; i < locationBookings.size(); i++) {
      location = locationBookings.get(i);
      existingLocation = getLocationBooking(location.getGeoNodeId());
      if (existingLocation == null) {
        locationBookingsIds.add(addLocationBooking(location));
      }
    }

    return locationBookingsIds;
  }

  /**
   * Delete all Locations
   *
   * @return rows affected
   */
  @Override public long deleteLocations() {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, null, null);
  }
}
