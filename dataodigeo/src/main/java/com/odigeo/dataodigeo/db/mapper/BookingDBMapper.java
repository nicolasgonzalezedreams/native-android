package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.dataodigeo.db.dao.BookingDBDAO;
import java.util.ArrayList;
import java.util.List;

public class BookingDBMapper {
  /**
   * Mapper booking cursor list
   *
   * @param cursor Cursor with booking data
   * @return {@link List<Booking>}
   */
  public List<Booking> getBookingList(Cursor cursor) {
    List<Booking> bookingList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long bookingId = cursor.getLong(cursor.getColumnIndex(BookingDBDAO.BOOKING_ID));
        String arrivalAirportCode =
            cursor.getString(cursor.getColumnIndex(BookingDBDAO.ARRIVAL_AIRPORT_CODE));
        long arrivalLastLeg = cursor.getLong(cursor.getColumnIndex(BookingDBDAO.ARRIVAL_LAST_LEG));
        String bookingStatus = cursor.getString(cursor.getColumnIndex(BookingDBDAO.BOOKING_STATUS));
        String currency = cursor.getString(cursor.getColumnIndex(BookingDBDAO.CURRENCY));
        long departureFirstLeg =
            cursor.getLong(cursor.getColumnIndex(BookingDBDAO.DEPARTURE_FIRST_LEG));
        String locale = cursor.getString(cursor.getColumnIndex(BookingDBDAO.LOCALE));
        String market = cursor.getString(cursor.getColumnIndex(BookingDBDAO.MARKET));
        double price = cursor.getDouble(cursor.getColumnIndex(BookingDBDAO.PRICE));
        String sortCriteria = cursor.getString(cursor.getColumnIndex(BookingDBDAO.SORT_CRITERIA));
        long timestamp = cursor.getLong(cursor.getColumnIndex(BookingDBDAO.TIMESTAMP));
        long total = cursor.getLong(cursor.getColumnIndex(BookingDBDAO.TOTAL));
        String tripType = cursor.getString(cursor.getColumnIndex(BookingDBDAO.TRIP_TYPE));
        int multiDestinyIndex =
            cursor.getInt(cursor.getColumnIndex(BookingDBDAO.MULTI_DESTINY_INDEX));
        boolean travelCompanionNotification =
            (cursor.getInt(cursor.getColumnIndex(BookingDBDAO.TRAVEL_COMPANION_NOTIFICATION)) == 1);
        boolean soundOfData =
            (cursor.getInt(cursor.getColumnIndex(BookingDBDAO.SOUND_OF_DATA)) == 1);
        boolean enrichedBooking =
            (cursor.getInt(cursor.getColumnIndex(BookingDBDAO.ENRICHED_BOOKING)) == 1);

        bookingList.add(
            new Booking(bookingId, arrivalAirportCode, arrivalLastLeg, bookingStatus, currency,
                departureFirstLeg, locale, market, price, sortCriteria, timestamp, total, tripType,
                multiDestinyIndex, travelCompanionNotification, soundOfData, enrichedBooking));
      }
    }

    return bookingList;
  }

  /**
   * Mapper booking cursor
   *
   * @param cursor Cursor with booking data
   * @return {@link Booking}
   */
  public Booking getBooking(Cursor cursor) {
    List<Booking> bookingList = getBookingList(cursor);

    if (bookingList.size() == 1) {
      return bookingList.get(0);
    } else {
      return null;
    }
  }

  public List<Long> getBookingIdList(Cursor cursor) {
    List<Long> bookingIdList = new ArrayList<>();
    while (cursor.moveToNext()) {
      bookingIdList.add(Long.parseLong(cursor.getString(0)));
    }
    return bookingIdList;
  }
}
