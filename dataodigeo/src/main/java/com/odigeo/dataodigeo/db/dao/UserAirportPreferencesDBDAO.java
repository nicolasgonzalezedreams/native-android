package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.UserAirportPreferencesDBDAOInterface;
import com.odigeo.data.entity.userData.UserAirportPreferences;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.UserAirportPreferencesDBMapper;

public class UserAirportPreferencesDBDAO extends OdigeoSQLiteOpenHelper
    implements UserAirportPreferencesDBDAOInterface {

  private UserAirportPreferencesDBMapper mUserAirportPreferencesDBMapper;

  public UserAirportPreferencesDBDAO(Context context) {
    super(context);
    mUserAirportPreferencesDBMapper = new UserAirportPreferencesDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + USER_AIRPORT_PREFERENCES_ID
        + " NUMERIC, "
        + AIRPORT
        + " TEXT, "
        + USER_ID
        + " INTEGER "
        + ");";
  }

  @Override public UserAirportPreferences getUserAirportPreferences(long userAirportPreferencesId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + userAirportPreferencesId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    UserAirportPreferences preferences =
        mUserAirportPreferencesDBMapper.getUserAirportPreferences(data);
    data.close();

    return preferences;
  }

  @Override public long addUserAirportPreferences(UserAirportPreferences userAirportPreferences) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(USER_AIRPORT_PREFERENCES_ID, userAirportPreferences.getUserAirportPreferencesId());
    values.put(AIRPORT, userAirportPreferences.getAirport());
    values.put(USER_ID, userAirportPreferences.getUserId());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }
}
