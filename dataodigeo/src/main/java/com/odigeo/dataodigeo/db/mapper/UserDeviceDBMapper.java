package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.userData.UserDevice;
import com.odigeo.dataodigeo.db.dao.UserDeviceDBDAO;
import java.util.ArrayList;

public class UserDeviceDBMapper {

  /**
   * Mapper user device cursor list
   *
   * @param cursor Cursor with user device data
   * @return {@link ArrayList< UserDevice >}
   */
  public ArrayList<UserDevice> getUserDeviceList(Cursor cursor) {
    ArrayList<UserDevice> userDeviceList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(UserDeviceDBDAO.ID));
        long userDeviceId = cursor.getLong(cursor.getColumnIndex(UserDeviceDBDAO.USER_DEVICE_ID));
        String deviceName = cursor.getString(cursor.getColumnIndex(UserDeviceDBDAO.DEVICE_NAME));
        String deviceId = cursor.getString(cursor.getColumnIndex(UserDeviceDBDAO.DEVICE_ID));
        boolean isPrimary = (cursor.getInt(cursor.getColumnIndex(UserDeviceDBDAO.IS_PRIMARY)) == 1);
        String alias = cursor.getString(cursor.getColumnIndex(UserDeviceDBDAO.ALIAS));
        long userId = cursor.getLong(cursor.getColumnIndex(UserDeviceDBDAO.USER_ID));

        userDeviceList.add(
            new UserDevice(id, userDeviceId, deviceName, deviceId, isPrimary, alias, userId));
      }
    }

    return userDeviceList;
  }

  /**
   * Mapper user device cursor
   *
   * @param cursor Cursor with user device data
   * @return {@link UserDevice}
   */
  public UserDevice getUserDevice(Cursor cursor) {
    ArrayList<UserDevice> userDeviceList = getUserDeviceList(cursor);

    if (userDeviceList.size() == 1) {
      return userDeviceList.get(0);
    } else {
      return null;
    }
  }
}
