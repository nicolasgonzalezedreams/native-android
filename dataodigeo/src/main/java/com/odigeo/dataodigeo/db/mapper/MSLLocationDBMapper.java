package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.MSLLocation;
import com.odigeo.dataodigeo.db.dao.MSLLocationDBDAO;
import java.util.ArrayList;

public class MSLLocationDBMapper {

  /**
   * Mapper mslLocation cursor list
   *
   * @param cursor Cursor with mslLocation data
   * @return {@link ArrayList< MSLLocation >}
   */
  public ArrayList<MSLLocation> getMSLLocationList(Cursor cursor) {
    ArrayList<MSLLocation> mslLocationList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        String cityIATACode =
            cursor.getString(cursor.getColumnIndex(MSLLocationDBDAO.CITY_IATA_CODE));
        String cityName = cursor.getString(cursor.getColumnIndex(MSLLocationDBDAO.CITY_NAME));
        String country = cursor.getString(cursor.getColumnIndex(MSLLocationDBDAO.COUNTRY));
        long geoNodeId = cursor.getLong(cursor.getColumnIndex(MSLLocationDBDAO.GEO_NODE_ID));
        long latitude = cursor.getLong(cursor.getColumnIndex(MSLLocationDBDAO.LATITUDE));
        String locationCode =
            cursor.getString(cursor.getColumnIndex(MSLLocationDBDAO.LOCATION_CODE));
        String locationName =
            cursor.getString(cursor.getColumnIndex(MSLLocationDBDAO.LOCATION_NAME));
        String locationType =
            cursor.getString(cursor.getColumnIndex(MSLLocationDBDAO.LOCATION_TYPE));
        long longitude = cursor.getLong(cursor.getColumnIndex(MSLLocationDBDAO.LONGITUDE));
        String matchName = cursor.getString(cursor.getColumnIndex(MSLLocationDBDAO.MATCH_NAME));

        mslLocationList.add(
            new MSLLocation(cityIATACode, cityName, country, geoNodeId, latitude, locationCode,
                locationName, locationType, longitude, matchName));
      }
    }

    return mslLocationList;
  }

  /**
   * Mapper mslLocation cursor
   *
   * @param cursor Cursor with mslLocation data
   * @return {@link MSLLocation}
   */
  public MSLLocation getMSLLocation(Cursor cursor) {
    ArrayList<MSLLocation> mslLocationList = getMSLLocationList(cursor);

    if (mslLocationList.size() == 1) {
      return mslLocationList.get(0);
    } else {
      return null;
    }
  }
}
