package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.odigeo.data.db.dao.MembershipDBDAOInterface;
import com.odigeo.data.entity.userData.Membership;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.MembershipDBMapper;

import java.util.List;

public class MembershipDBDAO extends OdigeoSQLiteOpenHelper implements MembershipDBDAOInterface {

  private MembershipDBMapper membershipDBMapper;

  public MembershipDBDAO(Context context) {
    super(context);
    membershipDBMapper = new MembershipDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE IF NOT EXISTS "
        + TABLE_NAME
        + " ( "
        + WEBSITE
        + " TEXT  PRIMARY KEY,"
        + FIRST_NAME
        + " TEXT, "
        + LAST_NAMES
        + " TEXT, "
        + MEMBER_ID
        + " NUMERIC "
        + ");";
  }

  @Override public boolean addMembership(Membership membership) {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.insertWithOnConflict(TABLE_NAME, null, getContentValues(membership),
        SQLiteDatabase.CONFLICT_IGNORE) != -1;
  }

  @Override public Membership getMembershipForMarket(String market) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + WEBSITE + " = '" + market + "'";
    Cursor data = db.rawQuery(selectQuery, null);
    Membership membership = membershipDBMapper.getMembership(data);
    data.close();

    return membership;
  }

  @Override public boolean clearMembership() {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, null, null) != -1;
  }

  @Override public List<Membership> getAllMembershipsOfUser() {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME;
    Cursor data = db.rawQuery(selectQuery, null);
    List<Membership> memberships = membershipDBMapper.getMembershipList(data);
    data.close();

    return memberships;
  }

  @Override public boolean deleteMembership(Membership membership) {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, "WEBSITE=?", new String[] { membership.getWebsite() }) != -1;
  }

  @Override public void createTableIfNotExists() {
    SQLiteDatabase db = getOdigeoDatabase();
    db.execSQL(getCreateTableSentence());
  }

  private ContentValues getContentValues(Membership membership) {
    ContentValues values = new ContentValues();

    values.put(MEMBER_ID, membership.getMemberId());
    values.put(FIRST_NAME, membership.getFirstName());
    values.put(LAST_NAMES, membership.getLastNames());
    values.put(WEBSITE, membership.getWebsite());

    return values;
  }
}
