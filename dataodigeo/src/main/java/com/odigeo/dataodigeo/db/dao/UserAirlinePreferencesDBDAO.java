package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.UserAirlinePreferencesDBDAOInterface;
import com.odigeo.data.entity.userData.UserAirlinePreferences;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.UserAirlinePreferencesDBMapper;

public class UserAirlinePreferencesDBDAO extends OdigeoSQLiteOpenHelper
    implements UserAirlinePreferencesDBDAOInterface {

  private UserAirlinePreferencesDBMapper mUserAirlinePreferencesDBMapper;

  public UserAirlinePreferencesDBDAO(Context context) {
    super(context);
    mUserAirlinePreferencesDBMapper = new UserAirlinePreferencesDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + USER_AIRLINE_PREFERENCES_ID
        + " NUMERIC, "
        + AIRLINE_CODE
        + " TEXT, "
        + USER_ID
        + " INTEGER "
        + ");";
  }

  @Override public UserAirlinePreferences getUserAirlinePreferences(long userAirlinePreferencesId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + userAirlinePreferencesId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    UserAirlinePreferences preferences =
        mUserAirlinePreferencesDBMapper.getUserAirlinePreferences(data);
    data.close();

    return preferences;
  }

  @Override public long addUserAirlinePreferences(UserAirlinePreferences userAirlinePreferences) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(USER_AIRLINE_PREFERENCES_ID, userAirlinePreferences.getUserAirlinePreferencesId());
    values.put(AIRLINE_CODE, userAirlinePreferences.getAirlineCode());
    values.put(USER_ID, userAirlinePreferences.getUserId());

    long newRowId;
    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }
}
