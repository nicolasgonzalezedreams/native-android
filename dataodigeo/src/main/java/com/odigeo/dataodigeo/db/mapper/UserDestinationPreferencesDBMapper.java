package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.userData.UserDestinationPreferences;
import com.odigeo.dataodigeo.db.dao.UserDestinationPreferencesDBDAO;
import java.util.ArrayList;

public class UserDestinationPreferencesDBMapper {

  /**
   * Mapper user destination preferences cursor list
   *
   * @param cursor Cursor with user airline preferences data
   * @return {@link ArrayList< UserDestinationPreferences >}
   */
  public ArrayList<UserDestinationPreferences> getUserDestinationPreferencesList(Cursor cursor) {
    ArrayList<UserDestinationPreferences> userDestinationPreferencesList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(UserDestinationPreferencesDBDAO.ID));
        long userDestinationPreferences = cursor.getLong(
            cursor.getColumnIndex(UserDestinationPreferencesDBDAO.USER_DESTINATION_PREFERENCES_ID));
        String destination =
            cursor.getString(cursor.getColumnIndex(UserDestinationPreferencesDBDAO.DESTINATION));
        long userId =
            cursor.getLong(cursor.getColumnIndex(UserDestinationPreferencesDBDAO.USER_ID));

        userDestinationPreferencesList.add(
            new UserDestinationPreferences(id, userDestinationPreferences, destination, userId));
      }
    }

    return userDestinationPreferencesList;
  }

  /**
   * Mapper user destination preferences cursor
   *
   * @param cursor Cursor with user airline preferences data
   * @return {@link UserDestinationPreferences}
   */
  public UserDestinationPreferences getUserDestinationPreferences(Cursor cursor) {
    ArrayList<UserDestinationPreferences> userDestinationPreferencesList =
        getUserDestinationPreferencesList(cursor);

    if (userDestinationPreferencesList.size() == 1) {
      return userDestinationPreferencesList.get(0);
    } else {
      return null;
    }
  }
}
