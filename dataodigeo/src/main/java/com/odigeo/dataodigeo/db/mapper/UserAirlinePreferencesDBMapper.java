package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.userData.UserAirlinePreferences;
import com.odigeo.dataodigeo.db.dao.UserAirlinePreferencesDBDAO;
import java.util.ArrayList;

public class UserAirlinePreferencesDBMapper {

  /**
   * Mapper user airline preferences cursor list
   *
   * @param cursor Cursor with user airline preferences data
   * @return {@link ArrayList< UserAirlinePreferences >}
   */
  public ArrayList<UserAirlinePreferences> getUserAirlinePreferencesList(Cursor cursor) {
    ArrayList<UserAirlinePreferences> userAirlinePreferencesList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(UserAirlinePreferencesDBDAO.ID));
        long userAirlinePreferencesId = cursor.getLong(
            cursor.getColumnIndex(UserAirlinePreferencesDBDAO.USER_AIRLINE_PREFERENCES_ID));
        String airlineCode =
            cursor.getString(cursor.getColumnIndex(UserAirlinePreferencesDBDAO.AIRLINE_CODE));
        long userId = cursor.getLong(cursor.getColumnIndex(UserAirlinePreferencesDBDAO.USER_ID));

        userAirlinePreferencesList.add(
            new UserAirlinePreferences(id, userAirlinePreferencesId, airlineCode, userId));
      }
    }

    return userAirlinePreferencesList;
  }

  /**
   * Mapper user airline preferences cursor
   *
   * @param cursor Cursor with user airline preferences data
   * @return {@link UserAirlinePreferences}
   */
  public UserAirlinePreferences getUserAirlinePreferences(Cursor cursor) {
    ArrayList<UserAirlinePreferences> userAirlinePreferencesList =
        getUserAirlinePreferencesList(cursor);

    if (userAirlinePreferencesList.size() == 1) {
      return userAirlinePreferencesList.get(0);
    } else {
      return null;
    }
  }
}
