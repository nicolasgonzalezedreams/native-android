package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.SectionDBDAOInterface;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.SectionDBMapper;
import java.util.ArrayList;
import java.util.List;

public class SectionDBDAO extends OdigeoSQLiteOpenHelper implements SectionDBDAOInterface {
  private SectionDBMapper mSectionDBMapper;

  public SectionDBDAO(Context context) {
    super(context);
    mSectionDBMapper = new SectionDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + SECTION_ID
        + " TEXT, "
        + AIRCRAFT
        + " TEXT, "
        + ARRIVAL_DATE
        + " NUMERIC, "
        + ARRIVAL_TERMINAL
        + " TEXT, "
        + BAGGAGE_ALLOWANCE_QUANTITY
        + " TEXT, "
        + BAGGAGE_ALLOWANCE_TYPE
        + " TEXT, "
        + CABIN_CLASS
        + " TEXT, "
        + DEPARTURE_DATE
        + " NUMERIC, "
        + DEPARTURE_TERMINAL
        + " TEXT, "
        + DURATION
        + " NUMERIC, "
        + FLIGHT_ID
        + " TEXT, "
        + SECTION_TYPE
        + " TEXT, "
        + CARRIER_ID
        + " NUMERIC, "
        + FROM
        + " NUMERIC, "
        + ITINERARY_BOOKING_ID
        + " NUMERIC, "
        + SEGMENT_ID
        + " NUMERIC, "
        + TO
        + " NUMERIC "
        + ");";
  }

  @Override public Section getSection(long sectionId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + sectionId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    Section section = mSectionDBMapper.getSection(data);
    data.close();

    return section;
  }

  @Override public long getIdSegmentFormEarlierSection(long date) {
    SQLiteDatabase db = getOdigeoDatabase();
    final String parameterDate = String.valueOf(date);
    final String gapDate = "GAP_DATE";

    String selectQuery = "SELECT "
        + SEGMENT_ID
        + ","
        + " MIN ("
        + ARRIVAL_DATE
        + " )"
        + " FROM "
        + TABLE_NAME
        + " WHERE "
        + ARRIVAL_DATE
        + " + 1800000 >= "
        + parameterDate;
    Cursor data = db.rawQuery(selectQuery, null);
    long idSegment = mSectionDBMapper.getIdSegmentFormEarlierSection(data);
    data.close();
    return idSegment;
  }

  @Override public List<Section> getSections(long segmentId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + SEGMENT_ID + " = " + segmentId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    List<Section> sections = mSectionDBMapper.getSectionList(data);
    data.close();

    return sections;
  }

  @Override public long addSection(Section section) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(SECTION_ID, section.getSectionId());
    values.put(AIRCRAFT, section.getAircraft());
    values.put(ARRIVAL_DATE, section.getArrivalDate());
    values.put(ARRIVAL_TERMINAL, section.getArrivalTerminal());
    values.put(BAGGAGE_ALLOWANCE_QUANTITY, section.getBaggageAllowanceQuantity());
    values.put(BAGGAGE_ALLOWANCE_TYPE, section.getBaggageAllowanceType());
    values.put(CABIN_CLASS, section.getCabinClass());
    values.put(DEPARTURE_DATE, section.getDepartureDate());
    values.put(DEPARTURE_TERMINAL, section.getDepartureTerminal());
    values.put(DURATION, section.getDuration());
    values.put(FLIGHT_ID, section.getFlightID());
    values.put(SECTION_TYPE, section.getSectionType());
    values.put(CARRIER_ID, section.getCarrier().getId());
    values.put(FROM, section.getFrom().getGeoNodeId());
    values.put(ITINERARY_BOOKING_ID, section.getItineraryBooking().getId());
    values.put(SEGMENT_ID, section.getSegment().getId());
    values.put(TO, section.getTo().getGeoNodeId());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }

  @Override public ArrayList<Long> addSections(ArrayList<Section> sections) {
    ArrayList<Long> sectionsId = new ArrayList<>();

    for (int i = 0; i < sections.size(); i++) {
      sectionsId.add(addSection(sections.get(i)));

      sections.get(i).setId(sectionsId.get(i));
    }

    return sectionsId;
  }

  @Override public boolean deleteSection(long segmentId) {
    SQLiteDatabase db = getOdigeoDatabase();
    int rowsAffected = db.delete(TABLE_NAME, FILTER_BY_SEGMENT_ID, new String[] { "" + segmentId });
    return rowsAffected > 0;
  }

  @Override public long deleteSections() {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, null, null);
  }
}
