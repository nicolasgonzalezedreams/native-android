package com.odigeo.dataodigeo.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.VisibleForTesting;
import com.odigeo.dataodigeo.db.dao.BaggageDBDAO;
import com.odigeo.dataodigeo.db.dao.BookingDBDAO;
import com.odigeo.dataodigeo.db.dao.BuyerDBDAO;
import com.odigeo.dataodigeo.db.dao.CarrierDBDAO;
import com.odigeo.dataodigeo.db.dao.CityDBDAO;
import com.odigeo.dataodigeo.db.dao.FlightStatsDBDAO;
import com.odigeo.dataodigeo.db.dao.ForecastDBDAO;
import com.odigeo.dataodigeo.db.dao.GuideDBDAO;
import com.odigeo.dataodigeo.db.dao.InsuranceDBDAO;
import com.odigeo.dataodigeo.db.dao.ItineraryBookingDBDAO;
import com.odigeo.dataodigeo.db.dao.LastOriginDestinationDBDAO;
import com.odigeo.dataodigeo.db.dao.LocationBookingDBDAO;
import com.odigeo.dataodigeo.db.dao.MSLLocationDBDAO;
import com.odigeo.dataodigeo.db.dao.MembershipDBDAO;
import com.odigeo.dataodigeo.db.dao.SearchSegmentDBDAO;
import com.odigeo.dataodigeo.db.dao.SectionDBDAO;
import com.odigeo.dataodigeo.db.dao.SegmentDBDAO;
import com.odigeo.dataodigeo.db.dao.StoredSearchDBDAO;
import com.odigeo.dataodigeo.db.dao.TravellerDBDAO;
import com.odigeo.dataodigeo.db.dao.UserAddressDBDAO;
import com.odigeo.dataodigeo.db.dao.UserAirlinePreferencesDBDAO;
import com.odigeo.dataodigeo.db.dao.UserAirportPreferencesDBDAO;
import com.odigeo.dataodigeo.db.dao.UserDBDAO;
import com.odigeo.dataodigeo.db.dao.UserDestinationPreferencesDBDAO;
import com.odigeo.dataodigeo.db.dao.UserDeviceDBDAO;
import com.odigeo.dataodigeo.db.dao.UserFrequentFlyerDBDAO;
import com.odigeo.dataodigeo.db.dao.UserIdentificationDBDAO;
import com.odigeo.dataodigeo.db.dao.UserLoginDBDAO;
import com.odigeo.dataodigeo.db.dao.UserProfileDBDAO;
import com.odigeo.dataodigeo.db.dao.UserTravellerDBDAO;
import com.odigeo.dataodigeo.db.helper.CountriesDbHelper;

public abstract class OdigeoSQLiteOpenHelper extends SQLiteOpenHelper {

  public static final String DATABASE_NAME = "Odigeo.db";
  public static final String ID = "id";
  /* App Version 3.0 */
  protected static final int DATABASE_VERSION_SSO = 1;
  /* App Version 3.1 */
  protected static final int DATABASE_VERSION_ARRIVAL_GUIDES = 2;
  /* App Version 3.2 */
  protected static final int DATABASE_VERSION_FLIGHT_STATS = 3;
  /* App Version 3.3 */
  protected static final int DATABASE_VERSION_FLIGHT_STATS_CHANGES_AND_COUNTRIES = 4;
  /* App Version 3.4.8 */
  protected static final int DATABASE_VERSION_SEARCH_FLIGHT_HISTORY_SYNC = 5;
  /* App Version 4.7.0 */
  protected static final int DATABASE_VERSION_LOCAL_CITIES = 6;
  /* App Version 4.11.0 */
  protected static final int DATABASE_VERSION_MEMBERSHIP = 7;
  /* App Version 4.15.0 */
  protected static final int DATABASE_VERSION_MEMBERSHIP_FIX = 8;
  /* Always update this with the latest database version */
  public static final int DATABASE_VERSION = DATABASE_VERSION_MEMBERSHIP_FIX;
  private static SQLiteDatabase db;
  private final Context mContext;
  private boolean isTest = false;

  public OdigeoSQLiteOpenHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
    this.mContext = context;
  }

  @Override public void onCreate(SQLiteDatabase db) {
    db.execSQL(BaggageDBDAO.getCreateTableSentence());
    db.execSQL(BookingDBDAO.getCreateTableSentence());
    db.execSQL(BuyerDBDAO.getCreateTableSentence());
    db.execSQL(CarrierDBDAO.getCreateTableSentence());
    db.execSQL(ForecastDBDAO.getCreateTableSentence());
    db.execSQL(GuideDBDAO.getCreateTableSentence());
    db.execSQL(InsuranceDBDAO.getCreateTableSentence());
    db.execSQL(ItineraryBookingDBDAO.getCreateTableSentence());
    db.execSQL(LastOriginDestinationDBDAO.getCreateTableSentence());
    db.execSQL(LocationBookingDBDAO.getCreateTableSentence());
    db.execSQL(MSLLocationDBDAO.getCreateTableSentence());
    db.execSQL(SearchSegmentDBDAO.getCreateTableSentence());
    db.execSQL(SectionDBDAO.getCreateTableSentence());
    db.execSQL(SegmentDBDAO.getCreateTableSentence());
    db.execSQL(StoredSearchDBDAO.getCreateTableSentence());
    db.execSQL(StoredSearchDBDAO.getCreateTrigger());
    db.execSQL(TravellerDBDAO.getCreateTableSentence());
    db.execSQL(UserAddressDBDAO.getCreateTableSentence());
    db.execSQL(UserAirlinePreferencesDBDAO.getCreateTableSentence());
    db.execSQL(UserAirportPreferencesDBDAO.getCreateTableSentence());
    db.execSQL(UserDBDAO.getCreateTableSentence());
    db.execSQL(UserDestinationPreferencesDBDAO.getCreateTableSentence());
    db.execSQL(UserDeviceDBDAO.getCreateTableSentence());
    db.execSQL(UserFrequentFlyerDBDAO.getCreateTableSentence());
    db.execSQL(UserIdentificationDBDAO.getCreateTableSentence());
    db.execSQL(UserLoginDBDAO.getCreateTableSentence());
    db.execSQL(UserProfileDBDAO.getCreateTableSentence());
    db.execSQL(UserTravellerDBDAO.getCreateTableSentence());
    db.execSQL(FlightStatsDBDAO.getCreateTableSentence());
    db.execSQL(CityDBDAO.getCreateTableSentence());
    db.execSQL(MembershipDBDAO.getCreateTableSentence());
    updateCountries(db);
  }

  /**
   * During the update of the database we need to propagate the new changes in the tables, from
   * the newest version to the oldest.
   * @param db
   * @param oldVersion
   * @param newVersion
   */
  @Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    if (newVersion > oldVersion) {
      switch (oldVersion) {

        /*
          DATABASE_VERSION_SSO (3.0)
          Users that are updating the App from this version will have to update the database structure for:
            - Arrival Guides (Guide table created)
            - Flights tables changes (FlightStats table created, Booking table changes)
            - Countries (Country, Language, CountryName tables created)
            - There is no need to call updateFlightStats(db) because that method alters FlightStats table
            and that change is already made on the creation sentence of FlightStats
        */
        case DATABASE_VERSION_SSO:
          updateGuides(db);
          updateFlights(db);
          updateCountries(db);
          updateStoredSearches(db);
          updateSearchSegments(db);
          updateCities(db);
          updateMembership(db);
          break;

        /*
          DATABASE_VERSION_ARRIVAL_GUIDES (3.1)
          This structure of the database already has Guide table created.
          Users that are updating the App from this version will have to update the database structure for:
            - Flights tables changes (FlightStats table added, Booking table changes)
            - Countries (Country, Language, CountryName tables added)
            - There is no need to call updateFlightStats(db) because that method alters FlightStats table
            and that change is already made on the creation sentence of FlightStats
        */
        case DATABASE_VERSION_ARRIVAL_GUIDES:
          updateFlights(db);
          updateCountries(db);
          updateStoredSearches(db);
          updateSearchSegments(db);
          updateCities(db);
          updateMembership(db);
          break;

        /*
          DATABASE_VERSION_FLIGHT_STATS (3.2)
          This structure of the database already has Guide and FlightStats table created, it also has the changes on Booking table.
          Users that are updating the App from this version will have to update the database structure for:
            - FlightStats table changes
        */
        case DATABASE_VERSION_FLIGHT_STATS:
          updateCountries(db);
          updateFlightStats(db);
          updateStoredSearches(db);
          updateSearchSegments(db);
          updateCities(db);
          updateMembership(db);
          break;

        /*
          DATABASE_VERSION_FLIGHT_STATS_CHANGES_AND_COUNTRIES (3.4.8)
          This structure of the database already has StoredSearch and SearchSegment tables created, but with no usage
          Users that are updating the App from this version will have to update the database structure for:
            - StoredSearch
            - SearchSegment
        */
        case DATABASE_VERSION_FLIGHT_STATS_CHANGES_AND_COUNTRIES:
          updateStoredSearches(db);
          updateSearchSegments(db);
          updateCities(db);
          updateMembership(db);
          break;

        /*
          DATABASE_VERSION_SEARCH_FLIGHTS_HISTORY_SYNC (4.7.0)
          Users that are updating the App from this version will have to update the database
          structure for:
            - Cities
         */
        case DATABASE_VERSION_SEARCH_FLIGHT_HISTORY_SYNC:
          updateCities(db);
          updateMembership(db);
          break;

        /*
          DATABASE_VERSION_LOCAL_CITIES (4.11.0)
          Users that are updating the from this version will have to update the database
          structure for:
            - Membership
         */
        case DATABASE_VERSION_LOCAL_CITIES:
        case DATABASE_VERSION_MEMBERSHIP:
          updateMembership(db);
      }
    }
  }

  private void updateGuides(SQLiteDatabase db) {
    db.execSQL(GuideDBDAO.getCreateTableSentence());
  }

  private void updateCountries(SQLiteDatabase db) {
    CountriesDbHelper countriesDbHelper = new CountriesDbHelper(mContext);
    countriesDbHelper.createCountriesTables(db);
    if (!isTest) {
      countriesDbHelper.populateCountriesTables(db);
    }
  }

  @VisibleForTesting public void disablePopulate() {
    isTest = true;
  }

  private void updateMembership(SQLiteDatabase db) {
    db.execSQL(MembershipDBDAO.getCreateTableSentence());
  }

  private void updateCities(SQLiteDatabase db) {
    db.execSQL(CityDBDAO.getCreateTableSentence());
  }

  private void updateFlights(SQLiteDatabase db) {
    db.execSQL(FlightStatsDBDAO.getCreateTableSentence());
    db.execSQL(BookingDBDAO.getAlterTableSentenceForTravelCompanionNotification());
    db.execSQL(BookingDBDAO.getAlterTableSentenceForSoundOfData());
    db.execSQL(BookingDBDAO.getAlterTableSentenceForEnrichedBooking());
  }

  private void updateFlightStats(SQLiteDatabase db) {
    db.execSQL(FlightStatsDBDAO.getAlterTableSentenceForBookingId());
  }

  private void updateStoredSearches(SQLiteDatabase db) {
    db.execSQL(StoredSearchDBDAO.TABLE_DROP);
    db.execSQL(StoredSearchDBDAO.getCreateTableSentence());
    db.execSQL(StoredSearchDBDAO.getCreateTrigger());
  }

  private void updateSearchSegments(SQLiteDatabase db) {
    db.execSQL(SearchSegmentDBDAO.TABLE_DROP);
    db.execSQL(SearchSegmentDBDAO.getCreateTableSentence());
  }

  @Override public String getDatabaseName() {
    return DATABASE_NAME;
  }

  public SQLiteDatabase getOdigeoDatabase() {
    if (db == null || !db.isOpen()) {
      db = getWritableDatabase();
    }
    return db;
  }

  public void closeOdigeoDatabase() {
    if (db != null && db.isOpen()) {
      db.close();
      db = null;
    }
  }

  public void odigeoBeginTransaction() {
    db.beginTransaction();
  }

  public void odigeoEndTransaction() {
    if (db != null) {
      db.endTransaction();
    }
  }

  public void odigeoSetTransactionSuccessful() {
    if (db != null) {
      db.setTransactionSuccessful();
    }
  }
}
