package com.odigeo.dataodigeo.db.helper;

import com.odigeo.data.db.dao.MembershipDBDAOInterface;
import com.odigeo.data.db.helper.MembershipHandlerInterface;
import com.odigeo.data.entity.userData.Membership;

import java.util.List;

public class MembershipHandler implements MembershipHandlerInterface {

  private MembershipDBDAOInterface membershipDBDAOInterface;

  public MembershipHandler(MembershipDBDAOInterface membershipDBDAOInterface) {
    this.membershipDBDAOInterface = membershipDBDAOInterface;
  }

  @Override public boolean addMembership(Membership membership) {
    return membershipDBDAOInterface.addMembership(membership);
  }

  @Override public Membership getMembershipFromMarket(String market) {
    return membershipDBDAOInterface.getMembershipForMarket(market);
  }

  @Override public boolean clearMembership() {
    return membershipDBDAOInterface.clearMembership();
  }

  @Override public List<Membership> getAllMembershipsOfUser() {
    return membershipDBDAOInterface.getAllMembershipsOfUser();
  }

  @Override public boolean deleteMembership(Membership membership) {
    return membershipDBDAOInterface.deleteMembership(membership);
  }

  @Override public void createTableIfNotExists() {
    membershipDBDAOInterface.createTableIfNotExists();
  }
}
