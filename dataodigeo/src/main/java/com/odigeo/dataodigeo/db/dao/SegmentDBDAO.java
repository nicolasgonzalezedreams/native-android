package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.SegmentDBDAOInterface;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.SegmentDBMapper;
import java.util.ArrayList;
import java.util.List;

public class SegmentDBDAO extends OdigeoSQLiteOpenHelper implements SegmentDBDAOInterface {
  private SegmentDBMapper mSegmentDBMapper;

  public SegmentDBDAO(Context context) {
    super(context);
    mSegmentDBMapper = new SegmentDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + DURATION
        + " NUMERIC, "
        + SEATS
        + " NUMERIC, "
        + MAIN_CARRIER_ID
        + " NUMERIC, "
        + BOOKING_ID
        + " NUMERIC "
        + ");";
  }

  @Override public Segment getSegment(long segmentId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + segmentId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    Segment segment = mSegmentDBMapper.getSegment(data);
    data.close();

    return segment;
  }

  @Override public List<Segment> getSegments(long bookingId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + BOOKING_ID + " = " + bookingId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    List<Segment> segments = mSegmentDBMapper.getSegmentList(data);
    data.close();

    return segments;
  }

  @Override public long addSegment(Segment segment) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();

    values.put(DURATION, segment.getDuration());
    values.put(SEATS, segment.getSeats());
    values.put(MAIN_CARRIER_ID, segment.getMainCarrierId());
    values.put(BOOKING_ID, segment.getBookingId());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }

  @Override public List<Long> addSegments(List<Segment> segments) {
    List<Long> segmentsId = new ArrayList<>();

    for (int i = 0; i < segments.size(); i++) {
      segmentsId.add(addSegment(segments.get(i)));
      segments.get(i).setId(segmentsId.get(i));
    }

    return segmentsId;
  }

  @Override public boolean deleteSegments(long bookingId) {
    SQLiteDatabase db = getOdigeoDatabase();
    int rowsAffected = db.delete(TABLE_NAME, FILTER_BY_BOOKING_ID, new String[] { "" + bookingId });
    return rowsAffected > 0;
  }

  @Override public long deleteAllSegments() {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, null, null);
  }
}
