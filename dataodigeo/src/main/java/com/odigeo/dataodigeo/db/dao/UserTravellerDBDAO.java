package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.UserDBDAOInterface;
import com.odigeo.data.db.dao.UserTravellerDBDAOInterface;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.UserTravellerDBMapper;
import java.util.List;

public class UserTravellerDBDAO extends OdigeoSQLiteOpenHelper
    implements UserTravellerDBDAOInterface {

  private UserTravellerDBMapper mUserTravellerDBMapper;
  private UserDBDAOInterface mUserDBDAO;

  public UserTravellerDBDAO(Context context, UserDBDAOInterface userDBDAO) {
    super(context);
    mUserDBDAO = userDBDAO;
    mUserTravellerDBMapper = new UserTravellerDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + USER_TRAVELLER_ID
        + " NUMERIC, "
        + BUYER
        + " NUMERIC, "
        + EMAIL
        + " TEXT, "
        + TYPE_OF_TRAVELLER
        + " TEXT, "
        + MEAL_TYPE
        + " TEXT, "
        + USER_ID
        + " INTEGER "
        + ");";
  }

  @Override public UserTraveller getUserTraveller(long userTravellerId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + USER_TRAVELLER_ID + " = " + userTravellerId;
    Cursor data = db.rawQuery(selectQuery, null);
    UserTraveller userTraveller = mUserTravellerDBMapper.getUserTraveller(data);
    data.close();
    return userTraveller;
  }

  @Override
  public UserTraveller getUserTravellerByUserTravellerId(long userTravellerId, String column) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + column + " = " + userTravellerId;
    Cursor data = db.rawQuery(selectQuery, null);
    UserTraveller userTraveller = mUserTravellerDBMapper.getUserTraveller(data);
    data.close();
    return userTraveller;
  }

  @Override public long addUserTraveller(UserTraveller userTraveller) {

    updateCurrentUser();

    SQLiteDatabase db = getOdigeoDatabase();

    long newRowId = db.insert(TABLE_NAME, null, getContentValues(userTraveller));

    /**
     * When userTravellerId is less to 1, is because in the app have not a user logged, and we need to set a valid id
     */
    if (userTraveller.getUserTravellerId() < 1) {
      UserTraveller userTravellerWithIds =
          new UserTraveller(newRowId, newRowId, userTraveller.getBuyer(), userTraveller.getEmail(),
              userTraveller.getTypeOfTraveller(), userTraveller.getMealType(),
              userTraveller.getUserId());

      userTraveller.getUserProfile().setUserTravellerId(newRowId);
      if (userTraveller.getUserProfile().getUserAddress() != null) {
        userTraveller.getUserProfile().getUserAddress().setUserProfileId(newRowId);
      }
      updateUserTravellerByLocalId(userTravellerWithIds);
    }

    return newRowId;
  }

  private void updateCurrentUser() {
    User currentUser = mUserDBDAO.getCurrentUser();
    if (currentUser != null) {
      currentUser.setLasModified(System.currentTimeMillis());
      mUserDBDAO.updateUser(currentUser.getEmail(), currentUser);
    }
  }

  @Override public long updateUserTraveller(UserTraveller userTraveller) {

    SQLiteDatabase db = getOdigeoDatabase();
    long rowAffected = db.update(TABLE_NAME, getContentValues(userTraveller),
        USER_TRAVELLER_ID + "=" + userTraveller.getUserTravellerId(), null);

    return rowAffected;
  }

  @Override public long updateUserTravellerByLocalId(UserTraveller userTraveller) {

    SQLiteDatabase db = getOdigeoDatabase();
    long rowAffected =
        db.update(TABLE_NAME, getContentValues(userTraveller), ID + "=" + userTraveller.getId(),
            null);

    return rowAffected;
  }

  @Override public boolean updateOrCreateUserTraveller(UserTraveller userTraveller) {
    return (updateUserTraveller(userTraveller) > 0) || (addUserTraveller(userTraveller) != -1);
  }

  @Override public List<UserTraveller> getAllUserTravellers() {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME;
    Cursor data = db.rawQuery(selectQuery, null);
    List<UserTraveller> userTravellerList = mUserTravellerDBMapper.getUserTravellerList(data);
    data.close();
    return userTravellerList;
  }

  @Override public List<UserTraveller> getAllUserTravellerByType(
      UserTraveller.TypeOfTraveller typeOfTraveller) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + TYPE_OF_TRAVELLER + " = ?";
    Cursor data = db.rawQuery(selectQuery, new String[] { typeOfTraveller.toString() });
    List<UserTraveller> userTravellerList = mUserTravellerDBMapper.getUserTravellerList(data);
    data.close();
    return userTravellerList;
  }

  @Override public List<UserTraveller> getUserTravellerForListing() {
    SQLiteDatabase db = getOdigeoDatabase();
    Cursor data = db.query(TABLE_NAME, null, null, null, null, null, TYPE_OF_TRAVELLER + " ASC");
    List<UserTraveller> userTravellerList = mUserTravellerDBMapper.getUserTravellerList(data);
    data.close();
    return userTravellerList;
  }

  @Override public boolean deleteUserTraveller(long userTravellerId) {
    SQLiteDatabase db = getOdigeoDatabase();
    int rowsAffected = db.delete(TABLE_NAME, USER_TRAVELLER_ID + "=" + userTravellerId, null);
    return rowsAffected > 0;
  }

  private ContentValues getContentValues(UserTraveller userTraveller) {
    ContentValues values = new ContentValues();
    values.put(USER_TRAVELLER_ID, userTraveller.getUserTravellerId());
    values.put(BUYER, (userTraveller.getBuyer()) ? 1 : 0);
    values.put(EMAIL, userTraveller.getEmail());
    if (userTraveller.getTypeOfTraveller() != null) {
      values.put(TYPE_OF_TRAVELLER, userTraveller.getTypeOfTraveller().toString());
    }
    values.put(MEAL_TYPE, userTraveller.getMealType());
    values.put(USER_ID, userTraveller.getUserId());
    return values;
  }
}