package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import android.text.TextUtils;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.dataodigeo.db.dao.CountryNameDbDao;
import java.util.LinkedList;
import java.util.List;

import static com.odigeo.dataodigeo.db.dao.CountryDbDao.COUNTRY_CODE;
import static com.odigeo.dataodigeo.db.dao.CountryDbDao.GEO_NODE_ID;
import static com.odigeo.dataodigeo.db.dao.CountryDbDao.NAME_DEFAULT;
import static com.odigeo.dataodigeo.db.dao.CountryDbDao.PHONE_PREFIX;
import static com.odigeo.dataodigeo.db.dao.CountryDbDao._ID;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 11/02/16
 */
public class CountryDbMapper {

  public Country getCountry(Cursor cursor) {
    Country country = new Country();
    country.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
    country.setGeoNodeId(cursor.getInt(cursor.getColumnIndexOrThrow(GEO_NODE_ID)));
    country.setCountryCode(cursor.getString(cursor.getColumnIndexOrThrow(COUNTRY_CODE)));
    country.setPhonePrefix(cursor.getString(cursor.getColumnIndexOrThrow(PHONE_PREFIX)));
    extractName(cursor, country);
    return country;
  }

  private void extractName(Cursor cursor, Country country) {
    country.setName(cursor.getString(cursor.getColumnIndexOrThrow(CountryNameDbDao.NAME)));
    if (TextUtils.isEmpty(country.getName())) {
      country.setName(cursor.getString(cursor.getColumnIndexOrThrow(NAME_DEFAULT)));
    }
  }

  public List<Country> getCountries(Cursor cursor) {
    List<Country> countries = new LinkedList<>();
    while (cursor.moveToNext()) {
      countries.add(getCountry(cursor));
    }
    return countries;
  }
}
