package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.ForecastDBDAOInterface;
import com.odigeo.data.entity.booking.Forecast;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.ForecastDBMapper;

public class ForecastDBDAO extends OdigeoSQLiteOpenHelper implements ForecastDBDAOInterface {
  private ForecastDBMapper mForecastDBMapper;

  public ForecastDBDAO(Context context) {
    super(context);
    mForecastDBMapper = new ForecastDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + FORECAST_ID
        + " NUMERIC, "
        + FORECAST_DATE
        + " NUMERIC, "
        + FORECAST_DESCRIPTION
        + " TEXT, "
        + FORECAST_ICON
        + " TEXT, "
        + FORECAST_UPDATED_DATE
        + " NUMERIC, "
        + TEMPERATURE
        + " NUMERIC, "
        + LOCATION_ID
        + " INTEGER "
        + ");";
  }

  @Override public Forecast getForecast(long forecastId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + forecastId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    Forecast forecast = mForecastDBMapper.getForecast(data);
    data.close();

    return forecast;
  }

  @Override public long addForecast(Forecast forecast) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(FORECAST_ID, forecast.getForecastId());
    values.put(FORECAST_DATE, forecast.getForecastDate());
    values.put(FORECAST_DESCRIPTION, forecast.getForecastDescription());
    values.put(FORECAST_ICON, forecast.getForecastIcon());
    values.put(FORECAST_UPDATED_DATE, forecast.getForecastUpdatedDate());
    values.put(TEMPERATURE, forecast.getTemperature());
    values.put(LOCATION_ID, forecast.getLocationId());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }
}
