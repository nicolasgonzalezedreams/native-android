package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.UserProfileDBDAOInterface;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.UserProfileDBMapper;

public class UserProfileDBDAO extends OdigeoSQLiteOpenHelper implements UserProfileDBDAOInterface {

  private UserProfileDBMapper mUserProfileDBMapper;

  public UserProfileDBDAO(Context context) {
    super(context);
    mUserProfileDBMapper = new UserProfileDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + USER_PROFILE_ID
        + " NUMERIC, "
        + GENDER
        + " TEXT, "
        + TITLE
        + " TEXT, "
        + NAME
        + " TEXT, "
        + MIDDLE_NAME
        + " TEXT, "
        + FIRST_LAST_NAME
        + " TEXT, "
        + SECOND_LAST_NAME
        + " TEXT, "
        + BIRTHDATE
        + " NUMERIC, "
        + PREFIX_PHONE_NUMBER
        + " TEXT, "
        + PHONE_NUMBER
        + " TEXT, "
        + PREFIX_ALTERNATE_PHONE_NUMBER
        + " TEXT, "
        + ALTERNATE_PHONE_NUMBER
        + " TEXT, "
        + MOBILE_PHONE_NUMBER
        + " TEXT, "
        + NATIONALITY_COUNTRY_CODE
        + " TEXT, "
        + CPF
        + " TEXT, "
        + IS_DEFAULT
        + " INTEGER, "
        + PHOTO
        + " TEXT, "
        + USER_TRAVELLER_ID
        + " INTEGER "
        + ");";
  }

  @Override public UserProfile getUserProfile(long userProfileId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + USER_PROFILE_ID + " = " + userProfileId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    UserProfile userProfile = mUserProfileDBMapper.getUserProfile(data);
    data.close();
    return userProfile;
  }

  @Override public UserProfile getUserProfile(long id, String column) {
    SQLiteDatabase db = getOdigeoDatabase();
    Cursor data =
        db.query(TABLE_NAME, null, column + "=?", new String[] { String.valueOf(id) }, null, null,
            null);
    UserProfile userProfile = mUserProfileDBMapper.getUserProfile(data);
    data.close();
    return userProfile;
  }

  @Override public long addUserProfile(UserProfile userProfile) {

    SQLiteDatabase db = getOdigeoDatabase();
    long newRowId = db.insert(TABLE_NAME, null, createContentValues(userProfile));

    /**
     * When userProfileId is less to 1, is because in the app have not a user logged, and we need to set a valid id
     */
    if (userProfile.getUserProfileId() < 1) {
      UserProfile newUserProfile =
          new UserProfile(newRowId, newRowId, userProfile.getGender(), userProfile.getTitle(),
              userProfile.getName(), userProfile.getMiddleName(), userProfile.getFirstLastName(),
              userProfile.getSecondLastName(), userProfile.getBirthDate(),
              userProfile.getPrefixPhoneNumber(), userProfile.getPhoneNumber(),
              userProfile.getPrefixAlternatePhoneNumber(), userProfile.getAlternatePhoneNumber(),
              userProfile.getMobilePhoneNumber(), userProfile.getNationalityCountryCode(),
              userProfile.getCpf(), userProfile.isDefaultTraveller(), userProfile.getPhoto(),
              userProfile.getUserAddress(), userProfile.getUserIdentificationList(),
              userProfile.getUserTravellerId());
      updateUserProfileByLocalId(newUserProfile);
    }
    return newRowId;
  }

  @Override public long updateUserProfileByLocalId(UserProfile userProfile) {
    SQLiteDatabase db = getOdigeoDatabase();
    long rowsAffected =
        db.update(TABLE_NAME, createContentValues(userProfile), ID + "=" + userProfile.getId(),
            null);

    return rowsAffected;
  }

  @Override public long updateUserProfile(UserProfile userProfile) {

    SQLiteDatabase db = getOdigeoDatabase();
    long rowsAffected = db.update(TABLE_NAME, createContentValues(userProfile),
        USER_PROFILE_ID + "='" + userProfile.getUserProfileId() + "'", null);

    return rowsAffected;
  }

  @Override public boolean updateOrCreateUserProfile(UserProfile userProfile) {
    return (updateUserProfile(userProfile) > 0 || (addUserProfile(userProfile) != -1));
  }

  @Override public boolean deleteUserProfile(long userProfileId) {
    SQLiteDatabase database = getOdigeoDatabase();
    long rowsAffected = database.delete(TABLE_NAME, USER_PROFILE_ID + "=?",
        new String[] { String.valueOf(userProfileId) });

    return rowsAffected > 0;
  }

  private ContentValues createContentValues(UserProfile userProfile) {

    ContentValues values = new ContentValues();
    values.put(USER_PROFILE_ID, userProfile.getUserProfileId());
    if (userProfile.getGender() != null) {
      values.put(GENDER, userProfile.getGender());
    }
    if (userProfile.getTitle() != null) {
      values.put(TITLE, userProfile.getTitle().toString());
    }
    values.put(NAME, userProfile.getName());
    values.put(MIDDLE_NAME, userProfile.getMiddleName());
    values.put(FIRST_LAST_NAME, userProfile.getFirstLastName());
    values.put(SECOND_LAST_NAME, userProfile.getSecondLastName());
    values.put(BIRTHDATE, userProfile.getBirthDate());
    values.put(PREFIX_PHONE_NUMBER, userProfile.getPrefixPhoneNumber());
    values.put(PHONE_NUMBER, userProfile.getPhoneNumber());
    values.put(PREFIX_ALTERNATE_PHONE_NUMBER, userProfile.getPrefixAlternatePhoneNumber());
    values.put(ALTERNATE_PHONE_NUMBER, userProfile.getAlternatePhoneNumber());
    values.put(MOBILE_PHONE_NUMBER, userProfile.getMobilePhoneNumber());
    values.put(NATIONALITY_COUNTRY_CODE, userProfile.getNationalityCountryCode());
    values.put(CPF, userProfile.getCpf());
    values.put(IS_DEFAULT, (userProfile.isDefaultTraveller()) ? 1 : 0);
    values.put(PHOTO, userProfile.getPhoto());
    values.put(USER_TRAVELLER_ID, userProfile.getUserTravellerId());
    return values;
  }
}
