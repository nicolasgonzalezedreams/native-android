package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.LastOriginDestinationDBDAOInterface;
import com.odigeo.data.entity.booking.LastOriginDestination;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.LastOriginDestinationDBMapper;

public class LastOriginDestinationDBDAO extends OdigeoSQLiteOpenHelper
    implements LastOriginDestinationDBDAOInterface {

  private LastOriginDestinationDBMapper mLastOriginDestinationDBMapper;

  public LastOriginDestinationDBDAO(Context context) {
    super(context);
    mLastOriginDestinationDBMapper = new LastOriginDestinationDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + CITY_IATA_CODE
        + " NUMERIC, "
        + CITY_NAME
        + " TEXT, "
        + COUNTRY
        + " TEXT, "
        + GEO_NODE_ID
        + " NUMERIC PRIMARY KEY, "
        + LATITUDE
        + " NUMERIC, "
        + LOCATION_CODE
        + " TEXT, "
        + LOCATION_NAME
        + " TEXT, "
        + LOCATION_TYPE
        + " TEXT, "
        + LONGITUDE
        + " NUMERIC, "
        + MATCH_NAME
        + " TEXT, "
        + IS_ORIGIN
        + " NUMERIC, "
        + TIMESTAMP
        + " NUMERIC "
        + ");";
  }

  @Override public LastOriginDestination getLastOriginDestination(long lastOriginDestinationId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM "
        + TABLE_NAME
        + " WHERE "
        + GEO_NODE_ID
        + " = "
        + lastOriginDestinationId
        + "";
    Cursor data = db.rawQuery(selectQuery, null);
    LastOriginDestination destination =
        mLastOriginDestinationDBMapper.getLastOriginDestination(data);
    data.close();

    return destination;
  }

  @Override public long addLastOriginDestination(LastOriginDestination lastOriginDestination) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(CITY_IATA_CODE, lastOriginDestination.getCityIATACode());
    values.put(CITY_NAME, lastOriginDestination.getCityName());
    values.put(COUNTRY, lastOriginDestination.getCountry());
    values.put(GEO_NODE_ID, lastOriginDestination.getGeoNodeId());
    values.put(LATITUDE, lastOriginDestination.getLatitude());
    values.put(LOCATION_CODE, lastOriginDestination.getLocationCode());
    values.put(LOCATION_NAME, lastOriginDestination.getLocationName());
    values.put(LOCATION_TYPE, lastOriginDestination.getLocationType());
    values.put(LONGITUDE, lastOriginDestination.getLongitude());
    values.put(MATCH_NAME, lastOriginDestination.getMatchName());
    values.put(IS_ORIGIN, lastOriginDestination.getIsOrigin());
    values.put(TIMESTAMP, lastOriginDestination.getTimestamp());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }
}
