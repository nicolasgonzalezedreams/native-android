package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.Forecast;
import com.odigeo.dataodigeo.db.dao.ForecastDBDAO;
import java.util.ArrayList;

public class ForecastDBMapper {
  /**
   * Mapper forecast cursor list
   *
   * @param cursor Cursor with forecast data
   * @return {@link ArrayList<Forecast>}
   */
  public ArrayList<Forecast> getForecastList(Cursor cursor) {
    ArrayList<Forecast> forecastList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(ForecastDBDAO.ID));
        long forecastId = cursor.getLong(cursor.getColumnIndex(ForecastDBDAO.FORECAST_ID));
        long forecastDate = cursor.getLong(cursor.getColumnIndex(ForecastDBDAO.FORECAST_DATE));
        String forecastDescription =
            cursor.getString(cursor.getColumnIndex(ForecastDBDAO.FORECAST_DESCRIPTION));
        String forecastIcon = cursor.getString(cursor.getColumnIndex(ForecastDBDAO.FORECAST_ICON));
        long forecastUpdatedDate =
            cursor.getLong(cursor.getColumnIndex(ForecastDBDAO.FORECAST_UPDATED_DATE));
        long temperature = cursor.getLong(cursor.getColumnIndex(ForecastDBDAO.TEMPERATURE));
        long locationId = cursor.getLong(cursor.getColumnIndex(ForecastDBDAO.LOCATION_ID));

        forecastList.add(
            new Forecast(id, forecastId, forecastDate, forecastDescription, forecastIcon,
                forecastUpdatedDate, temperature, locationId));
      }
    }

    return forecastList;
  }

  /**
   * Mapper forecast cursor
   *
   * @param cursor Cursor with forecast data
   * @return {@link Forecast}
   */
  public Forecast getForecast(Cursor cursor) {
    ArrayList<Forecast> forecastList = getForecastList(cursor);

    if (forecastList.size() == 1) {
      return forecastList.get(0);
    } else {
      return null;
    }
  }
}
