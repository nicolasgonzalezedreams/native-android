package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.userData.UserLogin;
import com.odigeo.dataodigeo.db.dao.UserLoginDBDAO;
import java.util.ArrayList;

public class UserLoginDBMapper {

  /**
   * Mapper user login cursor list
   *
   * @param cursor Cursor with user login data
   * @return {@link ArrayList< UserLogin >}
   */
  public ArrayList<UserLogin> getUserLoginList(Cursor cursor) {
    ArrayList<UserLogin> userLoginList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(UserLoginDBDAO.ID));
        long userLoginId = cursor.getLong(cursor.getColumnIndex(UserLoginDBDAO.USER_LOGIN_ID));
        String password = cursor.getString(cursor.getColumnIndex(UserLoginDBDAO.PASSWORD));
        String accessToken = cursor.getString(cursor.getColumnIndex(UserLoginDBDAO.ACCESS_TOKEN));
        long expirationTokenDate =
            cursor.getLong(cursor.getColumnIndex(UserLoginDBDAO.EXPIRATION_TOKEN_DATE));
        String hashCode = cursor.getString(cursor.getColumnIndex(UserLoginDBDAO.HASH_CODE));
        String activationCode =
            cursor.getString(cursor.getColumnIndex(UserLoginDBDAO.ACTIVATION_CODE));
        long activationCodeDate =
            cursor.getLong(cursor.getColumnIndex(UserLoginDBDAO.ACTIVATION_CODE_DATE));
        long loginAttemptFaild =
            cursor.getLong(cursor.getColumnIndex(UserLoginDBDAO.LOGIN_ATTEMPT_FAILED));
        long lastLoginDate = cursor.getLong(cursor.getColumnIndex(UserLoginDBDAO.LAST_LOGIN_DATE));
        String refreshToken = cursor.getString(cursor.getColumnIndex(UserLoginDBDAO.REFRESH_TOKEN));
        long userId = cursor.getLong(cursor.getColumnIndex(UserLoginDBDAO.USER_ID));

        userLoginList.add(
            new UserLogin(id, userLoginId, password, accessToken, expirationTokenDate, hashCode,
                activationCode, activationCodeDate, loginAttemptFaild, lastLoginDate, refreshToken,
                userId));
      }
    }

    return userLoginList;
  }

  /**
   * Mapper user login cursor
   *
   * @param cursor Cursor with user login data
   * @return {@link UserLogin}
   */
  public UserLogin getUserLogin(Cursor cursor) {
    ArrayList<UserLogin> userLoginList = getUserLoginList(cursor);

    if (userLoginList.size() == 1) {
      return userLoginList.get(0);
    } else {
      return null;
    }
  }
}
