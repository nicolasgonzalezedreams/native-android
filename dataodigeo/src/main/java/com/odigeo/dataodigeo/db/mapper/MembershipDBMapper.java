package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;

import com.odigeo.data.entity.userData.Membership;
import com.odigeo.dataodigeo.db.dao.MembershipDBDAO;

import java.util.ArrayList;

public class MembershipDBMapper {

  public Membership getMembership(Cursor cursor) {
    ArrayList<Membership> membershipList = getMembershipList(cursor);

    if (membershipList.size() > 0) {
      return membershipList.get(0);
    }

    return null;
  }

  public ArrayList<Membership> getMembershipList(Cursor cursor) {
    ArrayList<Membership> membershipList = new ArrayList<>();
    Membership membership;
    while (cursor.moveToNext()) {
      membership = getMembershipRow(cursor);
      membershipList.add(membership);
    }

    return membershipList;
  }

  private Membership getMembershipRow(Cursor row) {
    long memberId = row.getLong(row.getColumnIndex(MembershipDBDAO.MEMBER_ID));
    String firstName = row.getString(row.getColumnIndex(MembershipDBDAO.FIRST_NAME));
    String lastNames = row.getString(row.getColumnIndex(MembershipDBDAO.LAST_NAMES));
    String market = row.getString(row.getColumnIndex(MembershipDBDAO.WEBSITE));

    Membership membership = new Membership();
    membership.setMemberId(memberId);
    membership.setFirstName(firstName);
    membership.setLastNames(lastNames);
    membership.setWebsite(market);

    return membership;
  }
}
