package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.Carrier;
import com.odigeo.data.entity.booking.ItineraryBooking;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.dataodigeo.db.dao.SectionDBDAO;
import java.util.ArrayList;
import java.util.List;

public class SectionDBMapper {
  /**
   * Mapper section cursor list
   *
   * @param cursor Cursor with section data
   * @return {@link ArrayList<Section>}
   */
  public List<Section> getSectionList(Cursor cursor) {
    List<Section> sectionList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(SectionDBDAO.ID));
        String sectionId = cursor.getString(cursor.getColumnIndex(SectionDBDAO.SECTION_ID));
        String aircraft = cursor.getString(cursor.getColumnIndex(SectionDBDAO.AIRCRAFT));
        long arrivalDate = cursor.getLong(cursor.getColumnIndex(SectionDBDAO.ARRIVAL_DATE));
        String arrivalTerminal =
            cursor.getString(cursor.getColumnIndex(SectionDBDAO.ARRIVAL_TERMINAL));
        String baggageAllowanceQuantity =
            cursor.getString(cursor.getColumnIndex(SectionDBDAO.BAGGAGE_ALLOWANCE_QUANTITY));
        String baggageAllowanceType =
            cursor.getString(cursor.getColumnIndex(SectionDBDAO.BAGGAGE_ALLOWANCE_TYPE));
        String cabinClass = cursor.getString(cursor.getColumnIndex(SectionDBDAO.CABIN_CLASS));
        long departureDate = cursor.getLong(cursor.getColumnIndex(SectionDBDAO.DEPARTURE_DATE));
        String departureTerminal =
            cursor.getString(cursor.getColumnIndex(SectionDBDAO.DEPARTURE_TERMINAL));
        long duration = cursor.getLong(cursor.getColumnIndex(SectionDBDAO.DURATION));
        String flightID = cursor.getString(cursor.getColumnIndex(SectionDBDAO.FLIGHT_ID));
        String sectionType = cursor.getString(cursor.getColumnIndex(SectionDBDAO.SECTION_TYPE));

        long carrierId = cursor.getLong(cursor.getColumnIndex(SectionDBDAO.CARRIER_ID));
        long fromId = cursor.getLong(cursor.getColumnIndex(SectionDBDAO.FROM));
        long toId = cursor.getLong(cursor.getColumnIndex(SectionDBDAO.TO));
        long itineraryBookingId =
            cursor.getLong(cursor.getColumnIndex(SectionDBDAO.ITINERARY_BOOKING_ID));
        long segmentId = cursor.getLong(cursor.getColumnIndex(SectionDBDAO.SEGMENT_ID));

        sectionList.add(new Section(id, sectionId, aircraft, arrivalDate, arrivalTerminal,
            baggageAllowanceQuantity, baggageAllowanceType, cabinClass, departureDate,
            departureTerminal, duration, flightID, sectionType, new Carrier(carrierId),
            new LocationBooking(fromId), new ItineraryBooking(itineraryBookingId),
            new Segment(segmentId), new LocationBooking(toId)));
      }
    }

    return sectionList;
  }

  /**
   * Mapper section cursor
   *
   * @param cursor Cursor with section data
   * @return {@link Section}
   */
  public Section getSection(Cursor cursor) {
    List<Section> sectionList = getSectionList(cursor);

    if (sectionList.size() == 1) {
      return sectionList.get(0);
    } else {
      return null;
    }
  }

  public long getIdSegmentFormEarlierSection(Cursor data) {
    if (data.getCount() > 0) {
      data.moveToNext();
      return data.getLong(data.getColumnIndex(SectionDBDAO.SEGMENT_ID));
    }
    return -1;
  }
}
