package com.odigeo.dataodigeo.db.helper;

import com.odigeo.data.db.dao.MembershipDBDAOInterface;
import com.odigeo.data.db.dao.UserAddressDBDAOInterface;
import com.odigeo.data.db.dao.UserDBDAOInterface;
import com.odigeo.data.db.dao.UserFrequentFlyerDBDAOInterface;
import com.odigeo.data.db.dao.UserIdentificationDBDAOInterface;
import com.odigeo.data.db.dao.UserProfileDBDAOInterface;
import com.odigeo.data.db.dao.UserTravellerDBDAOInterface;
import com.odigeo.data.db.helper.TravellersHandlerInterface;
import com.odigeo.data.entity.userData.Membership;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.entity.userData.UserAddress;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.dataodigeo.db.dao.UserTravellerDBDAO;
import java.util.ArrayList;
import java.util.List;

public class TravellersHandler implements TravellersHandlerInterface {

  private UserDBDAOInterface mUserDBDao;
  private UserTravellerDBDAOInterface mUserTravellerDBDao;
  private UserProfileDBDAOInterface mUserProfileDBDao;
  private UserAddressDBDAOInterface mUserAddressDBDao;
  private UserIdentificationDBDAOInterface mUserIdentificationDBDao;
  private UserFrequentFlyerDBDAOInterface mUserFrequentFlyerDBDao;
  private MembershipDBDAOInterface membershipDBDAOInterface;

  public TravellersHandler(UserDBDAOInterface userDBDao,
      UserTravellerDBDAOInterface mUserTravellerDBDao, UserProfileDBDAOInterface mUserProfileDBDao,
      UserAddressDBDAOInterface mUserAddressDBDao,
      UserIdentificationDBDAOInterface userIdentificationDBDao,
      UserFrequentFlyerDBDAOInterface userFrequentFlyer,
      MembershipDBDAOInterface membershipDBDAOInterface) {
    this.mUserDBDao = userDBDao;
    this.mUserTravellerDBDao = mUserTravellerDBDao;
    this.mUserProfileDBDao = mUserProfileDBDao;
    this.mUserAddressDBDao = mUserAddressDBDao;
    this.mUserIdentificationDBDao = userIdentificationDBDao;
    this.mUserFrequentFlyerDBDao = userFrequentFlyer;
    this.membershipDBDAOInterface = membershipDBDAOInterface;
  }

  @Override public User getCurrentUser() {
    User user = mUserDBDao.getCurrentUser();
    if (user != null) {
      List<Membership> memberships = membershipDBDAOInterface.getAllMembershipsOfUser();
      List<UserTraveller> userTravellerList = mUserTravellerDBDao.getAllUserTravellers();
      List<UserTraveller> completeUserTravellerList = new ArrayList<>();
      if (userTravellerList != null && !userTravellerList.isEmpty()) {
        for (UserTraveller uTraveller : userTravellerList) {
          completeUserTravellerList.add(getFullUserTraveller(uTraveller.getUserTravellerId()));
        }
      }
      // TODO: Recover all relations: UserAirlinePreferences, UserDestinationPreferences ........
      return new User(user.getId(), user.getUserId(), user.getEmail(), user.getWebSite(),
          user.getAcceptsNewsletter(), user.getAcceptsPartnersInfo(), user.getCreationDate(),
          user.getLastModified(), user.getLocale(), user.getMarketingPortal(), user.getSource(),
          user.getStatus(), null, null, null, null, null, completeUserTravellerList, null, null,
          memberships);
    }
    return null;
  }

  @Override public User getCurrentUser(boolean areDefault) {
    User user = mUserDBDao.getCurrentUser();
    if (user != null) {
      List<Membership> memberships = membershipDBDAOInterface.getAllMembershipsOfUser();
      List<UserTraveller> userTravellerList = mUserTravellerDBDao.getAllUserTravellers();
      List<UserTraveller> completeUserTravellerList = new ArrayList<>();
      if (userTravellerList != null && !userTravellerList.isEmpty()) {
        for (UserTraveller uTraveller : userTravellerList) {
          completeUserTravellerList.add(
              getFullUserTraveller(uTraveller.getUserTravellerId(), areDefault));
        }
      }
      // TODO: Recover all relations: UserAirlinePreferences, UserDestinationPreferences ........
      return new User(user.getId(), user.getUserId(), user.getEmail(), user.getWebSite(),
          user.getAcceptsNewsletter(), user.getAcceptsPartnersInfo(), user.getCreationDate(),
          user.getLastModified(), user.getLocale(), user.getMarketingPortal(), user.getSource(),
          user.getStatus(), null, null, null, null, null, completeUserTravellerList, null, null,
          memberships);
    }
    return null;
  }

  @Override
  public List<UserTraveller> getUserTravellersByType(UserTraveller.TypeOfTraveller travellerType) {
    return mUserTravellerDBDao.getAllUserTravellerByType(travellerType);
  }

  @Override public List<UserTraveller> getSimpleUserTravellerList() {
    return mUserTravellerDBDao.getUserTravellerForListing();
  }

  @Override public UserTraveller getFullUserTraveller(long userTravellerId) {
    UserTraveller userTraveller =
        mUserTravellerDBDao.getUserTravellerByUserTravellerId(userTravellerId,
            UserTravellerDBDAO.USER_TRAVELLER_ID);
    if (userTraveller != null) {
      List<UserFrequentFlyer> frequentFlyers =
          mUserFrequentFlyerDBDao.getUserFrequentFlyerList(userTravellerId);
      UserProfile profile = mUserProfileDBDao.getUserProfile(userTraveller.getUserTravellerId(),
          UserTravellerDBDAO.USER_TRAVELLER_ID);
      if (profile != null) {
        profile = getFullUserProfile(profile);
      }
      return new UserTraveller(userTraveller.getId(), userTraveller.getUserTravellerId(),
          userTraveller.getBuyer(), userTraveller.getEmail(), userTraveller.getTypeOfTraveller(),
          userTraveller.getMealType(), frequentFlyers, profile, userTraveller.getUserId());
    }
    return null;
  }

  @Override public UserTraveller getFullUserTraveller(long userTravellerId, boolean areDefault) {
    UserTraveller userTraveller =
        mUserTravellerDBDao.getUserTravellerByUserTravellerId(userTravellerId,
            UserTravellerDBDAO.USER_TRAVELLER_ID);
    if (userTraveller != null) {
      List<UserFrequentFlyer> frequentFlyers =
          mUserFrequentFlyerDBDao.getUserFrequentFlyerList(userTravellerId);
      UserProfile profile = mUserProfileDBDao.getUserProfile(userTraveller.getUserTravellerId(),
          UserTravellerDBDAO.USER_TRAVELLER_ID);
      if (profile != null) {
        profile = getFullUserProfile(profile, areDefault);
      }
      return new UserTraveller(userTraveller.getId(), userTraveller.getUserTravellerId(),
          userTraveller.getBuyer(), userTraveller.getEmail(), userTraveller.getTypeOfTraveller(),
          userTraveller.getMealType(), frequentFlyers, profile, userTraveller.getUserId());
    }
    return null;
  }

  @Override public List<UserTraveller> getFullUserTravellerList() {
    List<UserTraveller> fullUserTravellers = new ArrayList<>();
    List<UserTraveller> userTravellerList = mUserTravellerDBDao.getAllUserTravellers();
    for (UserTraveller userTraveller : userTravellerList) {
      fullUserTravellers.add(getFullUserTraveller(userTraveller.getUserTravellerId()));
    }
    return fullUserTravellers;
  }

  @Override
  public List<UserTraveller> getFullUserTravellerList(List<UserTraveller> userTravellerList) {
    List<UserTraveller> fullUserTravellers = new ArrayList<>();
    for (UserTraveller userTraveller : userTravellerList) {
      fullUserTravellers.add(getFullUserTraveller(userTraveller.getUserTravellerId()));
    }
    return fullUserTravellers;
  }

  @Override public UserAddress getUserAddressByUserProfileId(long userProfileId) {
    return mUserAddressDBDao.getUserAddressByUserProfileId(userProfileId);
  }

  @Override public List<UserIdentification> getUserIdentifications(long userProfileId) {
    return mUserIdentificationDBDao.getUserIdentifications(userProfileId);
  }

  @Override public UserProfile getFullUserProfile(UserProfile userProfile) {
    long userProfileId = userProfile.getUserProfileId();
    UserAddress address = getUserAddressByUserProfileId(userProfileId);
    List<UserIdentification> userIdentificationList = getUserIdentifications(userProfileId);

    return new UserProfile(userProfile.getId(), userProfile.getUserProfileId(),
        userProfile.getGender(), userProfile.getTitle(), userProfile.getName(),
        userProfile.getMiddleName(), userProfile.getFirstLastName(),
        userProfile.getSecondLastName(), userProfile.getBirthDate(),
        userProfile.getPrefixPhoneNumber(), userProfile.getPhoneNumber(),
        userProfile.getPrefixAlternatePhoneNumber(), userProfile.getAlternatePhoneNumber(),
        userProfile.getMobilePhoneNumber(), userProfile.getNationalityCountryCode(),
        userProfile.getCpf(), userProfile.isDefaultTraveller(), userProfile.getPhoto(), address,
        userIdentificationList, userProfile.getUserTravellerId());
  }

  @Override public UserProfile getFullUserProfile(UserProfile userProfile, boolean isDefault) {
    long userProfileId = userProfile.getUserProfileId();
    UserAddress address = getUserAddressByUserProfileId(userProfileId);
    List<UserIdentification> userIdentificationList = getUserIdentifications(userProfileId);

    return new UserProfile(userProfile.getId(), userProfile.getUserProfileId(),
        userProfile.getGender(), userProfile.getTitle(), userProfile.getName(),
        userProfile.getMiddleName(), userProfile.getFirstLastName(),
        userProfile.getSecondLastName(), userProfile.getBirthDate(),
        userProfile.getPrefixPhoneNumber(), userProfile.getPhoneNumber(),
        userProfile.getPrefixAlternatePhoneNumber(), userProfile.getAlternatePhoneNumber(),
        userProfile.getMobilePhoneNumber(), userProfile.getNationalityCountryCode(),
        userProfile.getCpf(), isDefault, userProfile.getPhoto(), address, userIdentificationList,
        userProfile.getUserTravellerId());
  }
}
