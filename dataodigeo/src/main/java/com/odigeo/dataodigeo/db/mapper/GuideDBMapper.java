package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.Guide;
import com.odigeo.dataodigeo.db.dao.GuideDBDAO;
import java.util.ArrayList;

public class GuideDBMapper {

  /**
   * Mapper guide cursor list
   *
   * @param cursor Cursor with guide data
   * @return {@link ArrayList<Guide>}
   */
  public ArrayList<Guide> getGuideList(Cursor cursor) {
    ArrayList<Guide> guideList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(GuideDBDAO.ID));
        Long geoNodeId = cursor.getLong(cursor.getColumnIndex(GuideDBDAO.GEO_NODE_ID));
        String url = cursor.getString(cursor.getColumnIndex(GuideDBDAO.URL));
        String language = cursor.getString(cursor.getColumnIndex(GuideDBDAO.LANGUAGE));

        guideList.add(new Guide(id, geoNodeId, url, language));
      }
    }

    return guideList;
  }

  /**
   * Mapper guide cursor
   *
   * @param cursor Cursor with guide data
   * @return {@link Guide}
   */
  public Guide getGuide(Cursor cursor) {
    ArrayList<Guide> guideList = getGuideList(cursor);

    if (guideList.size() == 1) {
      return guideList.get(0);
    } else {
      return null;
    }
  }
}
