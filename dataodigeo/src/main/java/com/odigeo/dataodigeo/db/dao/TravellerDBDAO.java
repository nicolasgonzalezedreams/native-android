package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.TravellerDBDAOInterface;
import com.odigeo.data.entity.booking.Traveller;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.TravellerDBMapper;
import java.util.ArrayList;
import java.util.List;

public class TravellerDBDAO extends OdigeoSQLiteOpenHelper implements TravellerDBDAOInterface {
  private TravellerDBMapper mTravellerDBMapper;

  public TravellerDBDAO(Context context) {
    super(context);
    mTravellerDBMapper = new TravellerDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + DATE_OF_BIRTH
        + " NUMERIC, "
        + IDENTIFICATION
        + " TEXT, "
        + IDENTIFICATION_TYPE
        + " TEXT, "
        + LASTNAME
        + " TEXT, "
        + NAME
        + " TEXT, "
        + COUNTRY_CODE
        + " TEXT, "
        + IDENTIFICATION_EXPIRATION_DATE
        + " NUMERIC, "
        + IDENTIFICATION_ISSUE_COUNTRY_CODE
        + " TEXT, "
        + LOCALITY_CODE
        + " TEXT, "
        + MEAL
        + " TEXT, "
        + NATIONALITY
        + " TEXT, "
        + TITLE
        + " TEXT, "
        + TRAVELLER_GENDER
        + " TEXT, "
        + TRAVELLER_TYPE
        + " TEXT, "
        + BOOKING_ID
        + " NUMERIC "
        + ");";
  }

  @Override public Traveller getTraveller(long travellerId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + travellerId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    Traveller traveller = mTravellerDBMapper.getTraveller(data);
    data.close();

    return traveller;
  }

  @Override public List<Traveller> getTravellers(long bookingId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + BOOKING_ID + " = " + bookingId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    List<Traveller> travellers = mTravellerDBMapper.getTravellerList(data);
    data.close();

    return travellers;
  }

  @Override public long addTraveller(Traveller traveller) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(DATE_OF_BIRTH, traveller.getDateOfBirth());
    values.put(IDENTIFICATION, traveller.getIdentification());
    values.put(IDENTIFICATION_TYPE, traveller.getIdentificationType());
    values.put(LASTNAME, traveller.getLastname());
    values.put(NAME, traveller.getName());
    values.put(COUNTRY_CODE, traveller.getCountryCode());
    values.put(IDENTIFICATION_EXPIRATION_DATE, traveller.getIdentificationExpirationDate());
    values.put(IDENTIFICATION_ISSUE_COUNTRY_CODE, traveller.getIdentificationIssueCountryCode());
    values.put(LOCALITY_CODE, traveller.getLocalityCode());
    values.put(MEAL, traveller.getMeal());
    values.put(NATIONALITY, traveller.getNationality());
    values.put(TITLE, traveller.getTitle());
    values.put(TRAVELLER_GENDER, traveller.getTravellerGender());
    values.put(TRAVELLER_TYPE, traveller.getTravellerType());
    values.put(BOOKING_ID, traveller.getBookingId());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }

  @Override public List<Long> addTravellers(List<Traveller> travellers) {
    List<Long> travellersIds = new ArrayList<>();

    for (int i = 0; i < travellers.size(); i++) {
      travellersIds.add(addTraveller(travellers.get(i)));
      travellers.get(i).setId(travellersIds.get(i));
    }

    return travellersIds;
  }

  @Override public boolean deleteTraveller(long bookingId) {
    SQLiteDatabase db = getOdigeoDatabase();
    int rowsAffected = db.delete(TABLE_NAME, FILTER_BY_BOOKING_ID, new String[] { "" + bookingId });
    return rowsAffected > 0;
  }

  @Override public long deleteTravellers() {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, null, null);
  }
}
