package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.UserIdentificationDBDAOInterface;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.UserIdentificationDBMapper;
import java.util.List;

public class UserIdentificationDBDAO extends OdigeoSQLiteOpenHelper
    implements UserIdentificationDBDAOInterface {

  private UserIdentificationDBMapper mUserIdentificationDBMapper;

  public UserIdentificationDBDAO(Context context) {
    super(context);
    mUserIdentificationDBMapper = new UserIdentificationDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + USER_IDENTIFICATION_ID
        + " NUMERIC, "
        + IDENTIFICATION_ID
        + " TEXT, "
        + IDENTIFICATION_COUNTRY_CODE
        + " TEXT, "
        + IDENTIFICATION_EXPIRATION_DATE
        + " NUMERIC, "
        + IDENTIFICATION_TYPE
        + " TEXT, "
        + USER_PROFILE_ID
        + " INTEGER "
        + ");";
  }

  @Override public UserIdentification getUserIdentification(long userIdentificationId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM "
        + TABLE_NAME
        + " WHERE "
        + USER_IDENTIFICATION_ID
        + " = "
        + userIdentificationId
        + "";
    Cursor data = db.rawQuery(selectQuery, null);
    UserIdentification userIdentification = mUserIdentificationDBMapper.getUserIdentification(data);
    data.close();

    return userIdentification;
  }

  @Override public long addUserIdentification(UserIdentification userIdentification) {

    SQLiteDatabase db = getOdigeoDatabase();

    long newRowId = db.insert(TABLE_NAME, null, getContentValues(userIdentification));

    /**
     * When userIdentificationId is less to 1, is because in the app have not a user logged, and we need to set a valid id
     */
    if (userIdentification.getUserIdentificationId() < 1) {
      UserIdentification newUserIdentification =
          new UserIdentification(newRowId, newRowId, userIdentification.getIdentificationId(),
              userIdentification.getIdentificationCountryCode(),
              userIdentification.getIdentificationExpirationDate(),
              userIdentification.getIdentificationType(), userIdentification.getUserProfileId());
      updateUserIdentificationByLocalId(newUserIdentification);
    }
    return newRowId;
  }

  @Override public boolean updateUserIdentification(long userIdentificationId,
      UserIdentification userIdentification) {
    SQLiteDatabase db = getOdigeoDatabase();

    long rowsAffected = db.update(TABLE_NAME, getContentValues(userIdentification),
        USER_IDENTIFICATION_ID + "=" + userIdentificationId, null);

    return (rowsAffected != 0);
  }

  @Override public long updateUserIdentificationByLocalId(UserIdentification userIdentification) {

    SQLiteDatabase db = getOdigeoDatabase();
    long rowsAffected = db.update(TABLE_NAME, getContentValues(userIdentification),
        ID + "=" + userIdentification.getId(), null);

    return rowsAffected;
  }

  @Override public boolean updateOrCreateUserIdentification(UserIdentification userIdentification) {
    return updateUserIdentification(userIdentification.getUserIdentificationId(),
        userIdentification) || (addUserIdentification(userIdentification) != -1);
  }

  @Override public List<UserIdentification> getUserIdentifications(long userProfileId) {
    SQLiteDatabase db = getOdigeoDatabase();
    Cursor cursor = db.query(TABLE_NAME, null, USER_PROFILE_ID + "=?",
        new String[] { String.valueOf(userProfileId) }, null, null, null);
    List<UserIdentification> userIdentificationList =
        mUserIdentificationDBMapper.getUserIdentificationList(cursor);
    cursor.close();

    return userIdentificationList;
  }

  @Override public boolean deleteUserIdentification(long userIdentificationId) {
    SQLiteDatabase db = getOdigeoDatabase();
    int rowsAffected = db.delete(TABLE_NAME, USER_IDENTIFICATION_ID + "=?",
        new String[] { "" + userIdentificationId });

    return rowsAffected > 0;
  }

  private ContentValues getContentValues(UserIdentification userIdentification) {
    ContentValues values = new ContentValues();
    values.put(USER_IDENTIFICATION_ID, userIdentification.getUserIdentificationId());
    values.put(IDENTIFICATION_ID, userIdentification.getIdentificationId());
    values.put(IDENTIFICATION_COUNTRY_CODE, userIdentification.getIdentificationCountryCode());
    values.put(IDENTIFICATION_EXPIRATION_DATE,
        userIdentification.getIdentificationExpirationDate());
    if (userIdentification.getIdentificationType() != null) {
      values.put(IDENTIFICATION_TYPE, userIdentification.getIdentificationType().toString());
    }
    values.put(USER_PROFILE_ID, userIdentification.getUserProfileId());
    return values;
  }
}
