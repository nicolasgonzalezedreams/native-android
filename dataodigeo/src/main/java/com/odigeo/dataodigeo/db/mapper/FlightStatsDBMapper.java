package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.FlightStats;
import com.odigeo.dataodigeo.db.dao.FlightStatsDBDAO;
import java.util.ArrayList;

/**
 * Created by ximenaperez1 on 1/13/16.
 */
public class FlightStatsDBMapper {

  /**
   * Mapper flight stats cursor list
   *
   * @param cursor Cursor with flight stats data
   * @return {@link ArrayList<FlightStats>}
   */
  public ArrayList<FlightStats> getFlightStatsList(Cursor cursor) {
    ArrayList<FlightStats> flightStatsList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(FlightStatsDBDAO.ID));
        String sectionId = cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.SECTION_ID));
        String arrivalGate = cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.ARRIVAL_GATE));
        String arrivalTimeDelay =
            cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.ARRIVAL_TIME_DELAY));
        long arrivalTime = cursor.getLong(cursor.getColumnIndex(FlightStatsDBDAO.ARRIVAL_TIME));
        String arrivalTimeType =
            cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.ARRIVAL_TIME_TYPE));
        String baggageClaim =
            cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.BAGGAGE_CLAIM));
        String departureGate =
            cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.DEPARTURE_GATE));
        long departureTime = cursor.getLong(cursor.getColumnIndex(FlightStatsDBDAO.DEPARTURE_TIME));
        String departureTimeDelay =
            cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.DEPARTURE_TIME_DELAY));
        String departureTimeType =
            cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.DEPARTURE_TIME_TYPE));
        String destinationAirportName =
            cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.DESTINATION_AIRPORT_NAME));
        String destinationIataCode =
            cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.DESTINATION_IATA_CODE));
        String eventReceived =
            cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.EVENT_RECEIVED));
        String flightStatus =
            cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.FLIGHT_STATUS));
        String gateChanged = cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.GATE_CHANGED));
        String operatingVendor =
            cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.OPERATING_VENDOR));
        String operatingVendorCode =
            cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.OPERATING_VENDOR_CODE));
        String operatingVendorLegNumber =
            cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.OPERATING_VENDOR_LEG_NUMBER));
        String updatedArrivalTerminal =
            cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.UPDATED_ARRIVAL_TERMINAL));
        String updatedDepartureTerminal =
            cursor.getString(cursor.getColumnIndex(FlightStatsDBDAO.UPDATED_DEPARTURE_TERMINAL));

        flightStatsList.add(
            new FlightStats(id, arrivalGate, arrivalTime, arrivalTimeDelay, arrivalTimeType,
                baggageClaim, departureGate, departureTime, departureTimeDelay, departureTimeType,
                destinationAirportName, destinationIataCode, eventReceived, flightStatus,
                gateChanged, operatingVendor, operatingVendorCode, operatingVendorLegNumber,
                updatedArrivalTerminal, updatedDepartureTerminal, sectionId));
      }
    }

    return flightStatsList;
  }

  /**
   * Mapper flight stats cursor
   *
   * @param cursor Cursor with flight stats data
   * @return {@link FlightStats}
   */
  public FlightStats getFlightStats(Cursor cursor) {
    ArrayList<FlightStats> guideList = getFlightStatsList(cursor);

    if (guideList.size() == 1) {
      return guideList.get(0);
    } else {
      return null;
    }
  }

  public boolean existFlightStatsInDB(Cursor cursor) {
    ArrayList<FlightStats> flightStatsList = getFlightStatsList(cursor);
    return (!flightStatsList.isEmpty());
  }
}
