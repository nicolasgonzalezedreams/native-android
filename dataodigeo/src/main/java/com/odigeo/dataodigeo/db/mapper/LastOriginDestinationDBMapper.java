package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.LastOriginDestination;
import com.odigeo.dataodigeo.db.dao.LastOriginDestinationDBDAO;
import java.util.ArrayList;

public class LastOriginDestinationDBMapper {

  /**
   * Mapper last origin destination cursor list
   *
   * @param cursor Cursor with last origin destination data
   * @return {@link ArrayList< LastOriginDestination >}
   */
  public ArrayList<LastOriginDestination> getLastOriginDestinationList(Cursor cursor) {
    ArrayList<LastOriginDestination> lastOriginDestinationList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        String cityIATACode =
            cursor.getString(cursor.getColumnIndex(LastOriginDestinationDBDAO.CITY_IATA_CODE));
        String cityName =
            cursor.getString(cursor.getColumnIndex(LastOriginDestinationDBDAO.CITY_NAME));
        String country =
            cursor.getString(cursor.getColumnIndex(LastOriginDestinationDBDAO.COUNTRY));
        long geoNodeId =
            cursor.getLong(cursor.getColumnIndex(LastOriginDestinationDBDAO.GEO_NODE_ID));
        long latitude = cursor.getLong(cursor.getColumnIndex(LastOriginDestinationDBDAO.LATITUDE));
        String locationCode =
            cursor.getString(cursor.getColumnIndex(LastOriginDestinationDBDAO.LOCATION_CODE));
        String locationName =
            cursor.getString(cursor.getColumnIndex(LastOriginDestinationDBDAO.LOCATION_NAME));
        String locationType =
            cursor.getString(cursor.getColumnIndex(LastOriginDestinationDBDAO.LOCATION_TYPE));
        long longitude =
            cursor.getLong(cursor.getColumnIndex(LastOriginDestinationDBDAO.LONGITUDE));
        String matchName =
            cursor.getString(cursor.getColumnIndex(LastOriginDestinationDBDAO.MATCH_NAME));
        boolean isOrigin =
            (cursor.getInt(cursor.getColumnIndex(LastOriginDestinationDBDAO.IS_ORIGIN)) == 1);
        long timestamp =
            cursor.getLong(cursor.getColumnIndex(LastOriginDestinationDBDAO.TIMESTAMP));

        lastOriginDestinationList.add(
            new LastOriginDestination(cityIATACode, cityName, country, geoNodeId, latitude,
                locationCode, locationName, locationType, longitude, matchName, isOrigin,
                timestamp));
      }
    }

    return lastOriginDestinationList;
  }

  /**
   * Mapper last origin destination cursor
   *
   * @param cursor Cursor with last origin destination data
   * @return {@link LastOriginDestination}
   */
  public LastOriginDestination getLastOriginDestination(Cursor cursor) {
    ArrayList<LastOriginDestination> lastOriginDestinationList =
        getLastOriginDestinationList(cursor);

    if (lastOriginDestinationList.size() == 1) {
      return lastOriginDestinationList.get(0);
    } else {
      return null;
    }
  }
}
