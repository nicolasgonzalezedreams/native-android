package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import android.util.Log;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.dataodigeo.db.dao.UserIdentificationDBDAO;
import java.util.ArrayList;

public class UserIdentificationDBMapper {

  /**
   * Mapper user identification cursor list
   *
   * @param cursor Cursor with user identification data
   * @return {@link ArrayList< UserIdentification >}
   */
  public ArrayList<UserIdentification> getUserIdentificationList(Cursor cursor) {
    ArrayList<UserIdentification> userIdentificationList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(UserIdentificationDBDAO.ID));
        long userIdentificationId =
            cursor.getLong(cursor.getColumnIndex(UserIdentificationDBDAO.USER_IDENTIFICATION_ID));
        String identification =
            cursor.getString(cursor.getColumnIndex(UserIdentificationDBDAO.IDENTIFICATION_ID));
        String identificationCountryCode = cursor.getString(
            cursor.getColumnIndex(UserIdentificationDBDAO.IDENTIFICATION_COUNTRY_CODE));
        long identificationExpirationDate = cursor.getLong(
            cursor.getColumnIndex(UserIdentificationDBDAO.IDENTIFICATION_EXPIRATION_DATE));
        long userProfileId =
            cursor.getLong(cursor.getColumnIndex(UserIdentificationDBDAO.USER_PROFILE_ID));

        try {
          String idetificationTypeString =
              cursor.getString(cursor.getColumnIndex(UserIdentificationDBDAO.IDENTIFICATION_TYPE));
          UserIdentification.IdentificationType identificationType = null;

          if (idetificationTypeString != null) {
            identificationType =
                UserIdentification.IdentificationType.valueOf(idetificationTypeString);
          }

          userIdentificationList.add(
              new UserIdentification(id, userIdentificationId, identification,
                  identificationCountryCode, identificationExpirationDate, identificationType,
                  userProfileId));
        } catch (IllegalArgumentException e) {
          Log.e("UserIdentificationDB", e.getMessage());
        }
      }
    }

    return userIdentificationList;
  }

  /**
   * Mapper user identification cursor
   *
   * @param cursor Cursor with user identification data
   * @return {@link UserIdentification}
   */
  public UserIdentification getUserIdentification(Cursor cursor) {
    ArrayList<UserIdentification> userIdentificationList = getUserIdentificationList(cursor);

    if (userIdentificationList.size() == 1) {
      return userIdentificationList.get(0);
    } else {
      return null;
    }
  }
}
