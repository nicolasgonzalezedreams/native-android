package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.SearchSegmentDBDAOInterface;
import com.odigeo.data.entity.booking.SearchSegment;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.SearchSegmentDBMapper;
import java.util.List;

public class SearchSegmentDBDAO extends OdigeoSQLiteOpenHelper
    implements SearchSegmentDBDAOInterface {
  private SearchSegmentDBMapper mSearchSegmentDBMapper;

  public SearchSegmentDBDAO(Context context) {
    super(context);
    mSearchSegmentDBMapper = new SearchSegmentDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + SEARCH_SEGMENT_ID
        + " NUMERIC, "
        + ORIGIN_IATA_CODE
        + " TEXT, "
        + DESTINATION_IATA_CODE
        + " TEXT, "
        + DEPARTURE_DATE
        + " NUMERIC, "
        + SEGMENT_ORDER
        + " NUMERIC, "
        + PARENT_SEARCH_ID
        + " INTEGER "
        + ");";
  }

  @Override public SearchSegment getSearchSegment(long searchSegmentId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + searchSegmentId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    SearchSegment searchSegment = mSearchSegmentDBMapper.getSearchSegment(data);
    data.close();

    return searchSegment;
  }

  @Override public boolean addSearchSegment(SearchSegment searchSegment) {

    SQLiteDatabase db = getOdigeoDatabase();
    return db.insert(TABLE_NAME, null, getContentValues(searchSegment)) != -1;
  }

  @Override public boolean updateSearchSegment(long searchSegmentId, SearchSegment searchSegment) {
    SQLiteDatabase db = getOdigeoDatabase();

    return db.update(TABLE_NAME, getContentValues(searchSegment),
        SEARCH_SEGMENT_ID + "=" + searchSegmentId, null) != 0;
  }

  @Override public long deleteSearchSegment(long searchSegmentId) {
    SQLiteDatabase database = getOdigeoDatabase();
    return database.delete(TABLE_NAME, SEARCH_SEGMENT_ID + "=?",
        new String[] { String.valueOf(searchSegmentId) });
  }

  @Override public long deleteSearchSegmentFromParent(long parentId) {
    SQLiteDatabase database = getOdigeoDatabase();
    return database.delete(TABLE_NAME, PARENT_SEARCH_ID + "=" + parentId, null);
  }

  @Override public List<SearchSegment> getSearchSegmentList(long storedSearchId) {
    SQLiteDatabase db = getOdigeoDatabase();
    Cursor data = db.query(TABLE_NAME, null, PARENT_SEARCH_ID + "=?",
        new String[] { String.valueOf(storedSearchId) }, null, null, null);
    List<SearchSegment> searchSegmentList = mSearchSegmentDBMapper.getSearchSegmentList(data);
    data.close();

    return searchSegmentList;
  }

  @Override public boolean removeAllSearchSegments() {
    SQLiteDatabase db = getOdigeoDatabase();
    int rowsAffected = db.delete(TABLE_NAME, null, null);
    return rowsAffected > 0;
  }

  private ContentValues getContentValues(SearchSegment searchSegment) {

    ContentValues values = new ContentValues();

    values.put(SEARCH_SEGMENT_ID, searchSegment.getSearchSegmentId());
    values.put(ORIGIN_IATA_CODE, searchSegment.getOriginIATACode());
    values.put(DESTINATION_IATA_CODE, searchSegment.getDestinationIATACode());
    values.put(DEPARTURE_DATE, searchSegment.getDepartureDate());
    values.put(SEGMENT_ORDER, searchSegment.getSegmentOrder());
    values.put(PARENT_SEARCH_ID, searchSegment.getParentSearchId());

    return values;
  }
}
