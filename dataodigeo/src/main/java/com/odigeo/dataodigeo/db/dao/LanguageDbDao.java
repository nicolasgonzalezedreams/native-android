package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.provider.BaseColumns;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 12/02/16.
 */
public class LanguageDbDao extends CountriesBaseDbDao<String> implements BaseColumns {

  // Table
  public static final String TABLE_NAME = "Language";
  // Fields
  public static final String ISO_CODE = "iso";
  // Sentences
  public static final String CREATE = "CREATE TABLE IF NOT EXISTS "
      + TABLE_NAME
      + "("
      + _ID
      + " INTEGER PRIMARY KEY AUTOINCREMENT, "
      + ISO_CODE
      + " TEXT NOT NULL UNIQUE);";

  private void extractDataAndFillMap(Map<String, Long> idsMap, SQLiteDatabase database) {
    try {
      Cursor cursor = database.query(TABLE_NAME, null, null, null, null, null, null);
      if (cursor != null) {
        while (cursor.moveToNext()) {
          addEntryToMap(idsMap, cursor);
        }
        cursor.close();
      }
    } catch (SQLiteException e) {
      Log.e(TABLE_NAME, e.getMessage(), e);
    }
  }

  private void addEntryToMap(Map<String, Long> idsMap, Cursor cursor) {
    String iso = cursor.getString(cursor.getColumnIndexOrThrow(ISO_CODE));
    long id = cursor.getInt(cursor.getColumnIndexOrThrow(_ID));
    idsMap.put(iso, id);
  }

  public Map<String, Long> getIdsMap(SQLiteDatabase database) {
    Map<String, Long> idsMap = new HashMap<>();
    extractDataAndFillMap(idsMap, database);
    return idsMap;
  }

  @Override public String getTable() {
    return TABLE_NAME;
  }

  @Override public ContentValues createContentValues(String languageIsoCode) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(ISO_CODE, languageIsoCode);
    return contentValues;
  }

  @Override protected ContentValues createBulkContentValues(String[] csvEntry) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(_ID, csvEntry[0]);
    contentValues.put(ISO_CODE, csvEntry[1]);
    return contentValues;
  }
}
