package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.userData.UserAddress;
import com.odigeo.dataodigeo.db.dao.UserAddressDBDAO;
import java.util.ArrayList;

public class UserAddressDBMapper {

  /**
   * Mapper user address cursor list
   *
   * @param cursor Cursor with user address data
   * @return {@link ArrayList< UserAddress >}
   */
  public ArrayList<UserAddress> getUserAddressList(Cursor cursor) {
    ArrayList<UserAddress> userAddressList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(UserAddressDBDAO.ID));
        long userAddressId =
            cursor.getLong(cursor.getColumnIndex(UserAddressDBDAO.USER_ADDRESS_ID));
        String address = cursor.getString(cursor.getColumnIndex(UserAddressDBDAO.ADDRESS));
        String addressType = cursor.getString(cursor.getColumnIndex(UserAddressDBDAO.ADDRESS_TYPE));
        String city = cursor.getString(cursor.getColumnIndex(UserAddressDBDAO.CITY));
        String country = cursor.getString(cursor.getColumnIndex(UserAddressDBDAO.COUNTRY));
        String state = cursor.getString(cursor.getColumnIndex(UserAddressDBDAO.STATE));
        String postalCode = cursor.getString(cursor.getColumnIndex(UserAddressDBDAO.POSTAL_CODE));
        boolean isPrimary =
            (cursor.getInt(cursor.getColumnIndex(UserAddressDBDAO.IS_PRIMARY)) == 1);
        String alias = cursor.getString(cursor.getColumnIndex(UserAddressDBDAO.ALIAS));
        long userProfileId =
            cursor.getLong(cursor.getColumnIndex(UserAddressDBDAO.USER_PROFILE_ID));

        userAddressList.add(
            new UserAddress(id, userAddressId, address, addressType, city, country, state,
                postalCode, isPrimary, alias, userProfileId));
      }
    }

    cursor.close();

    return userAddressList;
  }

  /**
   * Mapper user address cursor
   *
   * @param cursor Cursor with user address data
   * @return {@link UserAddress}
   */
  public UserAddress getUserAddress(Cursor cursor) {
    ArrayList<UserAddress> userAddressList = getUserAddressList(cursor);

    if (userAddressList.size() == 1) {
      return userAddressList.get(0);
    } else {
      return null;
    }
  }
}
