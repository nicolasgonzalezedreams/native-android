package com.odigeo.dataodigeo.db.helper;

import com.odigeo.data.db.dao.CityDBDAOInterface;
import com.odigeo.data.db.helper.CitiesHandlerInterface;
import com.odigeo.data.entity.geo.City;

public class CitiesHandler implements CitiesHandlerInterface {

  private CityDBDAOInterface cityDBDAOInterface;

  public CitiesHandler(CityDBDAOInterface cityDBDAOInterface) {
    this.cityDBDAOInterface = cityDBDAOInterface;
  }

  @Override public City getCity(String iata) {
    return cityDBDAOInterface.getCityByIATA(iata);
  }

  @Override public void storeCity(City city) {
    cityDBDAOInterface.addCity(city);
  }
}
