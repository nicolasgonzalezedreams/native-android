package com.odigeo.dataodigeo.db.helper;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.helper.CountriesDbHelperInterface;
import com.odigeo.data.db.listener.DatabaseUpdateListener;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.net.NetCountry;
import com.odigeo.data.net.listener.OnRequestDataListListener;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.CountryDbDao;
import com.odigeo.dataodigeo.db.dao.CountryNameDbDao;
import com.odigeo.dataodigeo.db.dao.LanguageDbDao;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/02/16.
 */
public class CountriesDbHelper extends OdigeoSQLiteOpenHelper
    implements CountriesDbHelperInterface {

  private final LanguageDbDao mLanguageDbDao;
  private final CountryDbDao mCountryDbDao;
  private final CountryNameDbDao mCountryNameDbDao;
  private final AssetManager mAssetManager;

  public CountriesDbHelper(Context context) {
    super(context);
    mLanguageDbDao = new LanguageDbDao();
    mCountryDbDao = new CountryDbDao();
    mCountryNameDbDao = new CountryNameDbDao();
    mAssetManager = context.getAssets();
  }

  @Override public void updateCountries(final List<NetCountry> netCountries,
      final DatabaseUpdateListener databaseUpdateListener) {
    Thread thread = new Thread(new Runnable() {
      @Override public void run() {
        if (!netCountries.isEmpty()) {
          SQLiteDatabase database = getOdigeoDatabase();
          odigeoBeginTransaction();
          cleanAndInsertNetCountries(netCountries, database);
          odigeoEndTransaction();
        }
        if (databaseUpdateListener != null) {
          databaseUpdateListener.onUpdateFinish();
        }
      }
    });
    thread.start();
  }

  @Override
  public void getCountries(final String languageIsoCode, final boolean filterNullPhonePrefix,
      final OnRequestDataListListener<Country> requestDataListListener) {
    Thread thread = new Thread(new Runnable() {
      @Override public void run() {
        List<Country> countries =
            mCountryDbDao.getAllCountriesLocalized(getOdigeoDatabase(), languageIsoCode,
                filterNullPhonePrefix);
        Collections.sort(countries);
        if (requestDataListListener != null) {
          requestDataListListener.onResponse(countries);
        }
      }
    });
    thread.start();
  }

  @Override public Country getCountryByCountryCode(String languageIsoCode, String countryCode) {
    Country country =
        mCountryDbDao.getCountryByCountryCode(getOdigeoDatabase(), languageIsoCode, countryCode);
    return country;
  }

  private void cleanAndInsertNetCountries(List<NetCountry> netCountries, SQLiteDatabase database) {
    mCountryDbDao.clean(database);
    mCountryNameDbDao.clean(database);
    if (insertNetCountries(netCountries, database)) {
      odigeoSetTransactionSuccessful();
    }
  }

  private boolean insertNetCountries(List<NetCountry> netCountries, SQLiteDatabase database) {
    boolean result = true;
    long countryId;
    for (NetCountry netCountry : netCountries) {
      countryId = mCountryDbDao.insert(database, netCountry);
      result = insertLanguagesAndNames(database, countryId, netCountry);
      if (!result) {
        break;
      }
    }
    return result;
  }

  private boolean insertLanguagesAndNames(SQLiteDatabase database, long countryId,
      NetCountry netCountry) {
    Map<Long, String> languageIdAndNameMap;
    if (countryId > 0) {
      languageIdAndNameMap = updateAndGetLanguagesMap(database, netCountry.getNames());
      return mCountryNameDbDao.insertNames(database, countryId, languageIdAndNameMap);
    } else {
      return false;
    }
  }

  private Map<Long, String> updateAndGetLanguagesMap(SQLiteDatabase database,
      Map<String, String> languageAndNameMap) {
    Map<Long, String> resultMap = new HashMap<>();
    Map<String, Long> languageAndIdsMap = mLanguageDbDao.getIdsMap(database);
    for (String isoCode : languageAndIdsMap.keySet()) {
      resultMap.put(languageAndIdsMap.get(isoCode), languageAndNameMap.get(isoCode));
      languageAndNameMap.remove(isoCode);
    }
    insertRemainingElements(database, languageAndNameMap, resultMap);
    return resultMap;
  }

  private void insertRemainingElements(SQLiteDatabase database,
      Map<String, String> languageAndNameMap, Map<Long, String> resultMap) {
    long languageId;
    for (String isoCode : languageAndNameMap.keySet()) {
      languageId = mLanguageDbDao.insert(database, isoCode);
      resultMap.put(languageId, languageAndNameMap.get(isoCode));
    }
  }

  public void createCountriesTables(SQLiteDatabase database) {
    database.execSQL(CountryDbDao.CREATE);
    database.execSQL(LanguageDbDao.CREATE);
    database.execSQL(CountryNameDbDao.CREATE);
  }

  public void populateCountriesTables(SQLiteDatabase database) {
    database.beginTransaction();
    boolean result = mLanguageDbDao.bulkInsert(mAssetManager, database);
    result &= mCountryDbDao.bulkInsert(mAssetManager, database);
    result &= mCountryNameDbDao.bulkInsert(mAssetManager, database);
    if (result) {
      database.setTransactionSuccessful();
    }
    database.endTransaction();
  }
}
