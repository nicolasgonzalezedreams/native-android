package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.GuideDBDAOInterface;
import com.odigeo.data.entity.booking.Guide;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.GuideDBMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GuideDBDAO extends OdigeoSQLiteOpenHelper implements GuideDBDAOInterface {

  private GuideDBMapper mGuideDBMapper;

  public GuideDBDAO(Context context) {
    super(context);
    mGuideDBMapper = new GuideDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE IF NOT EXISTS "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + GEO_NODE_ID
        + " NUMERIC, "
        + URL
        + " TEXT, "
        + LANGUAGE
        + " TEXT "
        + ");";
  }

  @Override public Guide getGuide(long guideId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + guideId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    Guide guide = mGuideDBMapper.getGuide(data);
    data.close();
    return guide;
  }

  @Override public Guide getGuideByGeoNodeId(long geoNodeId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + GEO_NODE_ID + " = " + geoNodeId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    Guide guide = mGuideDBMapper.getGuide(data);
    data.close();
    return guide;
  }

  @Override public long addGuide(Guide guide) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(GEO_NODE_ID, guide.getGeoNodeId());
    values.put(URL, guide.getUrl());
    values.put(LANGUAGE, guide.getLanguage());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }

  @Override public Map<Long, Guide> getAllGuides() {

    Map<Long, Guide> guides = new HashMap<>();

    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME;
    Cursor data = db.rawQuery(selectQuery, null);

    if (data.getCount() > 0) {

      while (data.moveToNext()) {

        long id = data.getLong(data.getColumnIndex(GuideDBDAO.ID));
        long geoNodeId = data.getLong(data.getColumnIndex(GuideDBDAO.GEO_NODE_ID));
        String url = data.getString(data.getColumnIndex(GuideDBDAO.URL));
        String language = data.getString(data.getColumnIndex(GuideDBDAO.LANGUAGE));
        guides.put(geoNodeId, new Guide(id, geoNodeId, url, language));
      }
    }

    data.close();

    return guides;
  }

  @Override public boolean deleteGuide(long geoNodeId) {
    SQLiteDatabase db = getOdigeoDatabase();
    int rowsAffected = db.delete(TABLE_NAME, GEO_NODE_ID + "=?", new String[] { "" + geoNodeId });
    return rowsAffected > 0;
  }

  @Override public Map<Long, Guide> deleteGuides(Map<Long, Guide> guidesToDelete) {
    List<Long> geoNodesGuidesToDelete = new ArrayList<>();
    for (Map.Entry<Long, Guide> guide : guidesToDelete.entrySet()) {
      if (deleteGuide(guide.getKey())) {
        geoNodesGuidesToDelete.add(guide.getKey());
      }
    }
    for (long geoNodeId : geoNodesGuidesToDelete) {
      guidesToDelete.remove(geoNodeId);
    }
    return guidesToDelete;
  }
}