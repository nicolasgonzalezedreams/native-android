package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.BuyerDBDAOInterface;
import com.odigeo.data.entity.booking.Buyer;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.BuyerDBMapper;

public class BuyerDBDAO extends OdigeoSQLiteOpenHelper implements BuyerDBDAOInterface {
  private BuyerDBMapper mBuyerDBMapper;

  public BuyerDBDAO(Context context) {
    super(context);
    mBuyerDBMapper = new BuyerDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + DATE_OF_BIRTH
        + " NUMERIC, "
        + IDENTIFICATION
        + " TEXT, "
        + IDENTIFICATION_TYPE
        + " TEXT, "
        + LASTNAME
        + " TEXT, "
        + NAME
        + " TEXT, "
        + ADDRESS
        + " TEXT, "
        + ALTERNATIVE_PHONE
        + " TEXT, "
        + CITY_NAME
        + " TEXT, "
        + COUNTRY
        + " TEXT, "
        + CPF
        + " TEXT, "
        + EMAIL
        + " TEXT, "
        + PHONE_NUMBER
        + " TEXT, "
        + PHONE_COUNTRY_CODE
        + " TEXT, "
        + STATE_NAME
        + " TEXT, "
        + ZIP_CODE
        + " TEXT, "
        + BOOKING_ID
        + " NUMERIC "
        + ");";
  }

  @Override public Buyer getBuyer(long bookingId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + BOOKING_ID + " = " + bookingId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    Buyer buyer = mBuyerDBMapper.getBuyer(data);
    data.close();

    return buyer;
  }

  @Override public long addBuyer(Buyer buyer) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(DATE_OF_BIRTH, buyer.getDateOfBirth());
    values.put(IDENTIFICATION, buyer.getIdentification());
    values.put(IDENTIFICATION_TYPE, buyer.getIdentificationType());
    values.put(LASTNAME, buyer.getLastname());
    values.put(NAME, buyer.getName());
    values.put(ADDRESS, buyer.getAddress());
    values.put(ALTERNATIVE_PHONE, buyer.getAlternativePhone());
    values.put(CITY_NAME, buyer.getCityName());
    values.put(COUNTRY, buyer.getCountry());
    values.put(CPF, buyer.getCpf());
    values.put(EMAIL, buyer.getEmail());
    values.put(PHONE_NUMBER, buyer.getPhone());
    values.put(PHONE_COUNTRY_CODE, buyer.getPhoneCountryCode());
    values.put(STATE_NAME, buyer.getStateName());
    values.put(ZIP_CODE, buyer.getZipCode());
    values.put(BOOKING_ID, buyer.getBookingId());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    buyer.setId(newRowId);

    return newRowId;
  }

  @Override public boolean deleteBuyer(long bookingId) {
    SQLiteDatabase db = getOdigeoDatabase();
    int rowsAffected = db.delete(TABLE_NAME, FILTER_BY_BOOKING_ID, new String[] { "" + bookingId });
    return rowsAffected > 0;
  }

  @Override public long deleteBuyers() {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, null, null);
  }
}
