package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.Carrier;
import com.odigeo.dataodigeo.db.dao.CarrierDBDAO;
import java.util.ArrayList;

public class CarrierDBMapper {
  /**
   * Mapper carrier cursor list
   *
   * @param cursor Cursor with carrier data
   * @return {@link ArrayList<Carrier>}
   */
  public ArrayList<Carrier> getCarrierList(Cursor cursor) {
    ArrayList<Carrier> carrierList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(CarrierDBDAO.ID));
        String code = cursor.getString(cursor.getColumnIndex(CarrierDBDAO.CODE));
        String name = cursor.getString(cursor.getColumnIndex(CarrierDBDAO.NAME));

        carrierList.add(new Carrier(id, code, name));
      }
    }

    return carrierList;
  }

  /**
   * Mapper carrier cursor
   *
   * @param cursor Cursor with carrier data
   * @return {@link Carrier}
   */
  public Carrier getCarrier(Cursor cursor) {
    ArrayList<Carrier> carrierList = getCarrierList(cursor);

    if (carrierList.size() == 1) {
      return carrierList.get(0);
    } else {
      return null;
    }
  }
}
