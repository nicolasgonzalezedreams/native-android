package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.provider.BaseColumns;
import android.text.TextUtils;
import android.util.Log;
import java.util.Map;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 12/02/16.
 */
public class CountryNameDbDao extends CountriesBaseDbDao implements BaseColumns {

  // Table
  public static final String TABLE_NAME = "CountryName";
  // Fields
  public static final String LANGUAGE_ID = "languageId";
  public static final String COUNTRY_ID = "countryId";
  public static final String NAME = "name";
  // Sentences
  public static final String CREATE = "CREATE TABLE IF NOT EXISTS "
      + TABLE_NAME
      + "("
      + _ID
      + " INTEGER PRIMARY KEY AUTOINCREMENT, "
      + LANGUAGE_ID
      + " INTEGER NOT NULL REFERENCES "
      + LanguageDbDao.TABLE_NAME
      + " ON DELETE CASCADE, "
      + COUNTRY_ID
      + " INTEGER NOT NULL REFERENCES "
      + CountryDbDao.TABLE_NAME
      + " ON DELETE CASCADE, "
      + NAME
      + " TEXT);";

  public boolean insertNames(SQLiteDatabase database, long countryId,
      Map<Long, String> languageIdAndNameMap) {
    boolean result = true;
    long insertedId;
    try {
      for (Long languageId : languageIdAndNameMap.keySet()) {
        insertedId = database.insert(getTable(), null,
            createContentValues(countryId, languageId, languageIdAndNameMap.get(languageId)));
        result &= (insertedId > 0);
      }
    } catch (SQLiteException e) {
      result = false;
      Log.e(getTable(), e.getMessage(), e);
    }
    return result;
  }

  private ContentValues createContentValues(long countryId, long languageId, String name) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(COUNTRY_ID, countryId);
    contentValues.put(LANGUAGE_ID, languageId);
    contentValues.put(NAME, name);
    return contentValues;
  }

  @Override public String getTable() {
    return TABLE_NAME;
  }

  @Override public long insert(SQLiteDatabase database, Object object) {
    throw new UnsupportedOperationException();
  }

  @Override protected ContentValues createContentValues(Object object) {
    throw new UnsupportedOperationException();
  }

  @Override protected ContentValues createBulkContentValues(String[] csvEntry) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(_ID, csvEntry[0]);
    contentValues.put(LANGUAGE_ID, csvEntry[1]);
    contentValues.put(COUNTRY_ID, csvEntry[2]);
    if (csvEntry.length == 4 && !TextUtils.isEmpty(csvEntry[3])) {
      contentValues.put(NAME, csvEntry[3]);
    } else {
      contentValues.putNull(NAME);
    }
    return contentValues;
  }
}
