package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.InsuranceDBDAOInterface;
import com.odigeo.data.entity.booking.Insurance;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.InsuranceDBMapper;
import java.util.ArrayList;
import java.util.List;

public class InsuranceDBDAO extends OdigeoSQLiteOpenHelper implements InsuranceDBDAOInterface {

  public static final String FILTER_BY_BOOKING_ID = BOOKING_ID + "=?";
  private InsuranceDBMapper mInsuranceDBMapper;

  public InsuranceDBDAO(Context context) {
    super(context);
    mInsuranceDBMapper = new InsuranceDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + INSURANCE_ID
        + " NUMERIC, "
        + CONDITION_URL_PRIMARY
        + " TEXT, "
        + CONDITION_URL_SECUNDARY
        + " TEXT, "
        + INSURANCE_DESCRIPTION
        + " TEXT, "
        + INSURANCE_TYPE
        + " TEXT, "
        + POLICY
        + " TEXT, "
        + SELECTABLE
        + " NUMERIC, "
        + SUBTITLE
        + " TEXT, "
        + TITLE
        + " TEXT, "
        + TOTAL
        + " NUMERIC, "
        + BOOKING_ID
        + " NUMERIC "
        + ");";
  }

  @Override public Insurance getInsurance(long insuranceId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + insuranceId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    Insurance insurance = mInsuranceDBMapper.getInsurance(data);
    data.close();

    return insurance;
  }

  @Override public List<Insurance> getInsurances(long bookingId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + BOOKING_ID + " = " + bookingId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    List<Insurance> insurances = mInsuranceDBMapper.getInsuranceList(data);
    data.close();

    return insurances;
  }

  @Override public long addInsurance(Insurance insurance) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(INSURANCE_ID, insurance.getInsuranceId());
    values.put(CONDITION_URL_PRIMARY, insurance.getConditionURLPrimary());
    values.put(CONDITION_URL_SECUNDARY, insurance.getConditionURLSecundary());
    values.put(INSURANCE_DESCRIPTION, insurance.getInsuranceDescription());
    values.put(INSURANCE_TYPE, insurance.getInsuranceType());
    values.put(POLICY, insurance.getPolicy());
    values.put(SELECTABLE, insurance.getSelectable() ? 1 : 0);
    values.put(SUBTITLE, insurance.getSubtitle());
    values.put(TITLE, insurance.getTitle());
    values.put(TOTAL, insurance.getTotal());
    values.put(BOOKING_ID, insurance.getBookingId());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }

  @Override public List<Long> addInsurances(List<Insurance> insurances) {
    List<Long> insurancesId = new ArrayList<>();

    for (int i = 0; i < insurances.size(); i++) {
      insurancesId.add(addInsurance(insurances.get(i)));

      insurances.get(i).setId(insurancesId.get(i));
    }

    return insurancesId;
  }

  @Override public boolean removeInsurance(long bookingId) {
    SQLiteDatabase db = getOdigeoDatabase();
    int rowsAffected = db.delete(TABLE_NAME, FILTER_BY_BOOKING_ID, new String[] { "" + bookingId });
    return rowsAffected > 0;
  }

  @Override public long removeAllInsurances() {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, null, null);
  }
}
