package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.UserAddressDBDAOInterface;
import com.odigeo.data.entity.userData.UserAddress;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.UserAddressDBMapper;

public class UserAddressDBDAO extends OdigeoSQLiteOpenHelper implements UserAddressDBDAOInterface {

  private UserAddressDBMapper mUserAddressDBMapper;

  public UserAddressDBDAO(Context context) {
    super(context);
    mUserAddressDBMapper = new UserAddressDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + USER_ADDRESS_ID
        + " NUMERIC, "
        + ADDRESS
        + " TEXT, "
        + ADDRESS_TYPE
        + " TEXT, "
        + CITY
        + " TEXT, "
        + COUNTRY
        + " TEXT, "
        + STATE
        + " TEXT, "
        + POSTAL_CODE
        + " TEXT, "
        + IS_PRIMARY
        + " NUMERIC, "
        + ALIAS
        + " TEXT, "
        + USER_PROFILE_ID
        + " INTEGER "
        + ");";
  }

  @Override public UserAddress getUserAddress(long userAddressId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + USER_ADDRESS_ID + " = " + userAddressId;
    Cursor data = db.rawQuery(selectQuery, null);
    UserAddress userAddress = mUserAddressDBMapper.getUserAddress(data);
    data.close();
    return userAddress;
  }

  @Override public UserAddress getUserAddressByUserProfileId(long userProfileId) {
    SQLiteDatabase db = getOdigeoDatabase();
    Cursor data =
        db.query(TABLE_NAME, null, USER_PROFILE_ID + "=" + userProfileId, null, null, null, null);
    UserAddress userAddress = mUserAddressDBMapper.getUserAddress(data);
    data.close();
    return userAddress;
  }

  @Override public long addUserAddress(UserAddress userAddress) {

    SQLiteDatabase db = getOdigeoDatabase();
    long newRowId = db.insert(TABLE_NAME, null, getContentValues(userAddress));

    /**
     * When userAddressId is less to 1, is because in the app have not a user logged, and we need to set a valid id
     */
    if (userAddress.getUserAddressId() < 1) {
      UserAddress newUserAddress = new UserAddress(newRowId, newRowId, userAddress.getAddress(),
          userAddress.getAddressType(), userAddress.getCity(), userAddress.getCountry(),
          userAddress.getState(), userAddress.getPostalCode(), userAddress.getIsPrimary(),
          userAddress.getAlias(), userAddress.getUserProfileId());
      updateUserAddressByLocalId(newUserAddress);
    }
    return newRowId;
  }

  @Override public boolean updateUserAddress(long userAddressId, UserAddress userAddress) {

    SQLiteDatabase db = getOdigeoDatabase();
    long rowsAffected =
        db.update(TABLE_NAME, getContentValues(userAddress), USER_ADDRESS_ID + "=" + userAddressId,
            null);

    return (rowsAffected != 0);
  }

  @Override public long updateUserAddressByLocalId(UserAddress userAddress) {
    SQLiteDatabase db = getOdigeoDatabase();
    long rowsAffected =
        db.update(TABLE_NAME, getContentValues(userAddress), ID + "=" + userAddress.getId(), null);

    return rowsAffected;
  }

  @Override public boolean updateOrCreateUserAddress(UserAddress userAddress) {
    return updateUserAddress(userAddress.getUserAddressId(), userAddress) || (addUserAddress(
        userAddress) != -1);
  }

  @Override public boolean deleteUserAddress(long userAddressId) {
    SQLiteDatabase database = getOdigeoDatabase();
    long rowsAffected = database.delete(TABLE_NAME, USER_ADDRESS_ID + "=?",
        new String[] { String.valueOf(userAddressId) });
    return rowsAffected > 0;
  }

  private ContentValues getContentValues(UserAddress userAddress) {

    ContentValues values = new ContentValues();
    values.put(USER_ADDRESS_ID, userAddress.getUserAddressId());
    values.put(ADDRESS, userAddress.getAddress());
    values.put(ADDRESS_TYPE, userAddress.getAddressType());
    values.put(CITY, userAddress.getCity());
    values.put(COUNTRY, userAddress.getCountry());
    values.put(STATE, userAddress.getState());
    values.put(POSTAL_CODE, userAddress.getPostalCode());
    values.put(IS_PRIMARY, (userAddress.getIsPrimary()) ? 1 : 0);
    values.put(ALIAS, userAddress.getAlias());
    values.put(USER_PROFILE_ID, userAddress.getUserProfileId());

    return values;
  }
}
