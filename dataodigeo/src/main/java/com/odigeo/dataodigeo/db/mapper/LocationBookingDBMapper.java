package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.dataodigeo.db.dao.LocationBookingDBDAO;
import java.util.ArrayList;

public class LocationBookingDBMapper {

  /**
   * Mapper location booking cursor list
   *
   * @param cursor Cursor with location booking data
   * @return {@link ArrayList< LocationBooking >}
   */
  public ArrayList<LocationBooking> getLocationBookingList(Cursor cursor) {
    ArrayList<LocationBooking> locationBookingList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        String cityIATACode =
            cursor.getString(cursor.getColumnIndex(LocationBookingDBDAO.CITY_IATA_CODE));
        String cityName = cursor.getString(cursor.getColumnIndex(LocationBookingDBDAO.CITY_NAME));
        String country = cursor.getString(cursor.getColumnIndex(LocationBookingDBDAO.COUNTRY));
        long geoNodeId = cursor.getLong(cursor.getColumnIndex(LocationBookingDBDAO.GEO_NODE_ID));
        long latitude = cursor.getLong(cursor.getColumnIndex(LocationBookingDBDAO.LATITUDE));
        String locationCode =
            cursor.getString(cursor.getColumnIndex(LocationBookingDBDAO.LOCATION_CODE));
        String locationName =
            cursor.getString(cursor.getColumnIndex(LocationBookingDBDAO.LOCATION_NAME));
        String locationType =
            cursor.getString(cursor.getColumnIndex(LocationBookingDBDAO.LOCATION_TYPE));
        long longitude = cursor.getLong(cursor.getColumnIndex(LocationBookingDBDAO.LONGITUDE));
        String matchName = cursor.getString(cursor.getColumnIndex(LocationBookingDBDAO.MATCH_NAME));

        String countryCode =
            cursor.getString(cursor.getColumnIndex(LocationBookingDBDAO.COUNTRY_CODE));
        String currencyCode =
            cursor.getString(cursor.getColumnIndex(LocationBookingDBDAO.CURRENCY_CODE));
        long currencyRate =
            cursor.getLong(cursor.getColumnIndex(LocationBookingDBDAO.CURRENCY_RATE));
        long currencyUpdatedDate =
            cursor.getLong(cursor.getColumnIndex(LocationBookingDBDAO.CURRENCY_UPDATED_DATE));
        String timezone = cursor.getString(cursor.getColumnIndex(LocationBookingDBDAO.TIMEZONE));

        locationBookingList.add(
            new LocationBooking(cityIATACode, cityName, country, geoNodeId, latitude, locationCode,
                locationName, locationType, longitude, matchName, countryCode, currencyCode,
                currencyRate, currencyUpdatedDate, timezone));
      }
    }

    return locationBookingList;
  }

  /**
   * Mapper location booking cursor
   *
   * @param cursor Cursor with location booking data
   * @return {@link LocationBooking}
   */
  public LocationBooking getLocationBooking(Cursor cursor) {
    ArrayList<LocationBooking> locationBookingList = getLocationBookingList(cursor);

    if (locationBookingList.size() == 1) {
      return locationBookingList.get(0);
    } else {
      return null;
    }
  }
}
