package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.ItineraryBooking;
import com.odigeo.dataodigeo.db.dao.ItineraryBookingDBDAO;
import java.util.ArrayList;

public class ItineraryBookingDBMapper {
  /**
   * Mapper itinerary booking cursor list
   *
   * @param cursor Cursor with itinerary booking data
   * @return {@link ArrayList<ItineraryBooking>}
   */
  public ArrayList<ItineraryBooking> getItineraryBookingList(Cursor cursor) {
    ArrayList<ItineraryBooking> itineraryBookingList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(ItineraryBookingDBDAO.ID));
        String bookingStatus =
            cursor.getString(cursor.getColumnIndex(ItineraryBookingDBDAO.BOOKING_STATUS));
        String pnr = cursor.getString(cursor.getColumnIndex(ItineraryBookingDBDAO.PNR));
        long timestamp = cursor.getLong(cursor.getColumnIndex(ItineraryBookingDBDAO.TIMESTAMP));

        itineraryBookingList.add(new ItineraryBooking(id, bookingStatus, pnr, timestamp));
      }
    }

    return itineraryBookingList;
  }

  /**
   * Mapper itinerary booking cursor
   *
   * @param cursor Cursor with itinerary booking data
   * @return {@link ItineraryBooking}
   */
  public ItineraryBooking getItineraryBooking(Cursor cursor) {
    ArrayList<ItineraryBooking> itineraryBookingList = getItineraryBookingList(cursor);

    if (itineraryBookingList.size() == 1) {
      return itineraryBookingList.get(0);
    } else {
      return null;
    }
  }
}
