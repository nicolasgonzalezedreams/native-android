package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.CarrierDBDAOInterface;
import com.odigeo.data.entity.booking.Carrier;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.CarrierDBMapper;
import java.util.ArrayList;

public class CarrierDBDAO extends OdigeoSQLiteOpenHelper implements CarrierDBDAOInterface {

  private CarrierDBMapper mCarrierDBMapper;

  public CarrierDBDAO(Context context) {
    super(context);
    mCarrierDBMapper = new CarrierDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + CODE
        + " TEXT UNIQUE, "
        + NAME
        + " TEXT "
        + ");";
  }

  @Override public Carrier getCarrier(long carrierId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + carrierId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    Carrier carrier = mCarrierDBMapper.getCarrier(data);
    data.close();

    return carrier;
  }

  @Override public Carrier getCarrierWithCode(String code) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + CODE + " = '" + code + "'";
    Cursor data = db.rawQuery(selectQuery, null);
    Carrier carrier = mCarrierDBMapper.getCarrier(data);
    data.close();

    return carrier;
  }

  @Override public long addCarrier(Carrier carrier) {

    Carrier dbCarrier = getCarrierWithCode(carrier.getCode());

    if (dbCarrier == null) {

      SQLiteDatabase db = getOdigeoDatabase();

      ContentValues values = new ContentValues();
      values.put(CODE, carrier.getCode());
      values.put(NAME, carrier.getName());

      long newRowId;

      newRowId = db.insert(TABLE_NAME, null, values);

      return newRowId;
    } else {
      return dbCarrier.getId();
    }
  }

  @Override public ArrayList<Long> addCarriers(ArrayList<Carrier> carriers) {
    ArrayList<Long> carriersIds = new ArrayList<>();

    for (int i = 0; i < carriers.size(); i++) {
      carriersIds.add(addCarrier(carriers.get(i)));

      carriers.get(i).setId(carriersIds.get(i));
    }

    return carriersIds;
  }

  @Override public long deleteCarrier() {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, null, null);
  }
}
