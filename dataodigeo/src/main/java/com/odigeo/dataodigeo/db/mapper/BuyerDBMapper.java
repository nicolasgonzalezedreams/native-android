package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.Buyer;
import com.odigeo.dataodigeo.db.dao.BuyerDBDAO;
import java.util.ArrayList;

public class BuyerDBMapper {
  /**
   * Mapper buyer cursor list
   *
   * @param cursor Cursor with buyer data
   * @return {@link ArrayList<Buyer>}
   */
  public ArrayList<Buyer> getBuyerList(Cursor cursor) {
    ArrayList<Buyer> buyerList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long dateOfBirth = cursor.getLong(cursor.getColumnIndex(BuyerDBDAO.DATE_OF_BIRTH));
        String identification = cursor.getString(cursor.getColumnIndex(BuyerDBDAO.IDENTIFICATION));
        String identificationType =
            cursor.getString(cursor.getColumnIndex(BuyerDBDAO.IDENTIFICATION_TYPE));
        String lastname = cursor.getString(cursor.getColumnIndex(BuyerDBDAO.LASTNAME));
        String name = cursor.getString(cursor.getColumnIndex(BuyerDBDAO.NAME));
        long id = cursor.getLong(cursor.getColumnIndex(BuyerDBDAO.ID));
        String address = cursor.getString(cursor.getColumnIndex(BuyerDBDAO.ADDRESS));
        String alternativePhone =
            cursor.getString(cursor.getColumnIndex(BuyerDBDAO.ALTERNATIVE_PHONE));
        String cityName = cursor.getString(cursor.getColumnIndex(BuyerDBDAO.CITY_NAME));
        String country = cursor.getString(cursor.getColumnIndex(BuyerDBDAO.COUNTRY));
        String cpf = cursor.getString(cursor.getColumnIndex(BuyerDBDAO.CPF));
        String email = cursor.getString(cursor.getColumnIndex(BuyerDBDAO.EMAIL));
        String phone = cursor.getString(cursor.getColumnIndex(BuyerDBDAO.PHONE_NUMBER));
        String phoneCountryCode =
            cursor.getString(cursor.getColumnIndex(BuyerDBDAO.PHONE_COUNTRY_CODE));
        String stateName = cursor.getString(cursor.getColumnIndex(BuyerDBDAO.STATE_NAME));
        String zipCode = cursor.getString(cursor.getColumnIndex(BuyerDBDAO.ZIP_CODE));
        long bookingId = cursor.getLong(cursor.getColumnIndex(BuyerDBDAO.BOOKING_ID));

        buyerList.add(
            new Buyer(dateOfBirth, identification, identificationType, lastname, name, id, address,
                alternativePhone, cityName, country, cpf, email, phone, phoneCountryCode, stateName,
                zipCode, bookingId));
      }
    }

    return buyerList;
  }

  /**
   * Mapper buyer cursor
   *
   * @param cursor Cursor with buyer data
   * @return {@link Buyer}
   */
  public Buyer getBuyer(Cursor cursor) {
    ArrayList<Buyer> buyerList = getBuyerList(cursor);

    if (buyerList.size() == 1) {
      return buyerList.get(0);
    } else {
      return null;
    }
  }
}
