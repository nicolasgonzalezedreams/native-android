package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.BookingDBDAOInterface;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.BookingDBMapper;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BookingDBDAO extends OdigeoSQLiteOpenHelper implements BookingDBDAOInterface {

  private static final String DEFAULT_UPCOMING_BOOKING_ORDER_BY = DEPARTURE_FIRST_LEG + " ASC";
  private static final String DEFAULT_PAST_BOOKING_ORDER_BY = ARRIVAL_LAST_LEG + " ASC";

  private BookingDBMapper mBookingDBMapper;

  public BookingDBDAO(Context context) {
    super(context);
    mBookingDBMapper = new BookingDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + BOOKING_ID
        + " NUMERIC PRIMARY KEY, "
        + ARRIVAL_AIRPORT_CODE
        + " TEXT, "
        + ARRIVAL_LAST_LEG
        + " NUMERIC, "
        + BOOKING_STATUS
        + " TEXT, "
        + CURRENCY
        + " TEXT, "
        + DEPARTURE_FIRST_LEG
        + " NUMERIC, "
        + LOCALE
        + " TEXT, "
        + MARKET
        + " TEXT, "
        + PRICE
        + " NUMERIC, "
        + SORT_CRITERIA
        + " TEXT, "
        + TIMESTAMP
        + " NUMERIC, "
        + TOTAL
        + " NUMERIC, "
        + TRIP_TYPE
        + " TEXT, "
        + MULTI_DESTINY_INDEX
        + " NUMERIC, "
        + TRAVEL_COMPANION_NOTIFICATION
        + " NUMERIC, "
        + SOUND_OF_DATA
        + " NUMERIC, "
        + ENRICHED_BOOKING
        + " NUMERIC "
        + ");";
  }

  public static String getAlterTableSentenceForTravelCompanionNotification() {
    return "ALTER TABLE "
        + TABLE_NAME
        + " ADD COLUMN "
        + TRAVEL_COMPANION_NOTIFICATION
        + " NUMERIC";
  }

  public static String getAlterTableSentenceForSoundOfData() {
    return "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + SOUND_OF_DATA + " NUMERIC";
  }

  public static String getAlterTableSentenceForEnrichedBooking() {
    return "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + ENRICHED_BOOKING + " NUMERIC";
  }

  @Override public Booking getBooking(long bookingId) {
    SQLiteDatabase db = getOdigeoDatabase();
    Cursor data =
        db.query(TABLE_NAME, null, FILTER_BY_BOOKING_ID, new String[] { String.valueOf(bookingId) },
            null, null, null);
    Booking booking = mBookingDBMapper.getBooking(data);
    data.close();
    return booking;
  }

  @Override public List<Booking> getNextBookingsByDate(long dateInMiliseconds) {
    SQLiteDatabase db = getOdigeoDatabase();
    Cursor data = db.query(TABLE_NAME, null, TIMESTAMP + "<?",
        new String[] { String.valueOf(dateInMiliseconds) }, null, null, null);
    List<Booking> bookingList = mBookingDBMapper.getBookingList(data);
    data.close();
    return bookingList;
  }

  @Override public int getNextMultiDestinyIndex(String tripType) {
    int lastIndex = 0;
    // TODO: 13/10/15 Refactor to use query secure method
    if (tripType.equals(Booking.TRIP_TYPE_MULTI_SEGMENT)) {
      SQLiteDatabase db = getOdigeoDatabase();
      String query = "SELECT MAX("
          + BOOKING_ID
          + "), "
          + MULTI_DESTINY_INDEX
          + " FROM "
          + TABLE_NAME
          + " WHERE "
          + TRIP_TYPE
          + " = '"
          + Booking.TRIP_TYPE_MULTI_SEGMENT
          + "'";
      Cursor data = db.rawQuery(query, null);
      if (data.getCount() > 0) {
        data.moveToNext();
        lastIndex = data.getInt(data.getColumnIndex(MULTI_DESTINY_INDEX));
      }
      if (lastIndex == 6) {
        lastIndex = 0;
      }
      data.close();

      lastIndex++;
    }
    return lastIndex;
  }

  @Override public long addBooking(Booking booking) {
    int multiDestinyIndex = getNextMultiDestinyIndex(booking.getTripType());
    SQLiteDatabase db = getOdigeoDatabase();
    ContentValues values = new ContentValues();
    values.put(BOOKING_ID, booking.getBookingId());
    values.put(ARRIVAL_AIRPORT_CODE, booking.getArrivalAirportCode());
    values.put(ARRIVAL_LAST_LEG, booking.getArrivalLastLeg());
    values.put(BOOKING_STATUS, booking.getBookingStatus());
    values.put(CURRENCY, booking.getCurrency());
    values.put(DEPARTURE_FIRST_LEG, booking.getDepartureFirstLeg());
    values.put(LOCALE, booking.getLocale());
    values.put(MARKET, booking.getMarket());
    values.put(PRICE, booking.getPrice());
    values.put(SORT_CRITERIA, booking.getSortCriteria());
    values.put(TIMESTAMP, booking.getTimestamp());
    values.put(TOTAL, booking.getTotal());
    values.put(TRIP_TYPE, booking.getTripType());
    values.put(MULTI_DESTINY_INDEX, multiDestinyIndex);
    values.put(TRAVEL_COMPANION_NOTIFICATION, booking.getIsTravelCompanionNotification());
    values.put(SOUND_OF_DATA, booking.getIsSoundOfData());
    values.put(ENRICHED_BOOKING, booking.getIsEnrichedBooking());
    long newRowId;
    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }

  @Override public boolean deleteBooking(long bookingId) {
    SQLiteDatabase db = getOdigeoDatabase();
    int rowsAffected = db.delete(TABLE_NAME, FILTER_BY_BOOKING_ID, new String[] { "" + bookingId });
    return rowsAffected > 0;
  }

  @Override public List<Booking> getAllBookings() {
    List<Booking> bookings = new ArrayList<>();
    bookings.addAll(getUpcomingBookings());
    bookings.addAll(getPastBookings());
    return bookings;
  }

  @Override public List<Long> getAllBookingIds() {
    SQLiteDatabase db = getOdigeoDatabase();
    Cursor data = db.query(TABLE_NAME, new String[] { BOOKING_ID }, null, null, null, null, null);
    return mBookingDBMapper.getBookingIdList(data);
  }

  @Override public List<Booking> getUpcomingBookings() {
    final String currentTime = String.valueOf(Calendar.getInstance().getTimeInMillis());
    SQLiteDatabase db = getOdigeoDatabase();
    Cursor data =
        db.query(TABLE_NAME, null, ARRIVAL_LAST_LEG + " >= ?", new String[] { currentTime }, null,
            null, DEFAULT_UPCOMING_BOOKING_ORDER_BY);
    List<Booking> bookingList = mBookingDBMapper.getBookingList(data);
    data.close();

    return bookingList;
  }

  @Override public List<Booking> getPastBookings() {
    final String currentTime = String.valueOf(Calendar.getInstance().getTimeInMillis());
    SQLiteDatabase db = getOdigeoDatabase();
    Cursor data =
        db.query(TABLE_NAME, null, ARRIVAL_LAST_LEG + " < ?", new String[] { currentTime }, null,
            null, DEFAULT_PAST_BOOKING_ORDER_BY);
    List<Booking> bookingList = mBookingDBMapper.getBookingList(data);
    data.close();

    return bookingList;
  }

  @Override public int removeBookings() {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, null, null);
  }

  @Override public long updateStatus(long bookingId, String status) {
    long rowAffected;
    SQLiteDatabase db = getOdigeoDatabase();
    ContentValues values = new ContentValues();
    values.put(BOOKING_STATUS, status);
    rowAffected = db.update(TABLE_NAME, values, FILTER_BY_BOOKING_ID,
        new String[] { String.valueOf(bookingId) });

    return rowAffected;
  }

  @Override public boolean changeBookingNotifications(long bookingId, boolean status) {
    long rowAffected;
    SQLiteDatabase db = getOdigeoDatabase();
    ContentValues values = new ContentValues();
    values.put(TRAVEL_COMPANION_NOTIFICATION, status);
    rowAffected = db.update(TABLE_NAME, values, FILTER_BY_BOOKING_ID,
        new String[] { String.valueOf(bookingId) });

    return rowAffected > 0;
  }

  @Override public boolean changeEnrichedBooking(long bookingId, boolean enriched) {
    long rowAffected;
    SQLiteDatabase db = getOdigeoDatabase();
    ContentValues values = new ContentValues();
    values.put(ENRICHED_BOOKING, enriched);
    rowAffected = db.update(TABLE_NAME, values, FILTER_BY_BOOKING_ID,
        new String[] { String.valueOf(bookingId) });

    return rowAffected > 0;
  }

  public long getLatestPendingBookingAddedTime() {
    long result = -1;
    SQLiteDatabase db = getOdigeoDatabase();
    Cursor data = db.query(TABLE_NAME, new String[] { TIMESTAMP }, FILTER_BY_STATUS,
        new String[] { Booking.BOOKING_STATUS_PENDING, Booking.BOOKING_STATUS_REQUEST }, null, null,
        TIMESTAMP + " DESC", "1");
    if (data.moveToNext()) {
      result = data.getLong(data.getColumnIndex(BookingDBDAO.TIMESTAMP));
    }
    data.close();
    return result;
  }

  @Override public List<Booking> getPendingBookings() {
    SQLiteDatabase db = getOdigeoDatabase();

    Cursor data = db.query(TABLE_NAME, null, FILTER_BY_STATUS,
        new String[] { Booking.BOOKING_STATUS_PENDING, Booking.BOOKING_STATUS_REQUEST }, null, null,
        DEFAULT_UPCOMING_BOOKING_ORDER_BY);
    List<Booking> bookingList = mBookingDBMapper.getBookingList(data);
    data.close();

    return bookingList;
  }

  @Override public void initTransaction() {
    odigeoBeginTransaction();
  }

  @Override public void endTransaction() {
    odigeoEndTransaction();
  }

  @Override public void setTransactionSuccessful() {
    odigeoSetTransactionSuccessful();
  }

  @Override public void closeDatabase() {
    closeOdigeoDatabase();
  }
}
