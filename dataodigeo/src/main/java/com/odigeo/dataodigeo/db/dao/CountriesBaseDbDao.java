package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import au.com.bytecode.opencsv.CSVReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/02/16.
 */
public abstract class CountriesBaseDbDao<T> {

  protected static final String CSV_EXTENSION = ".csv";

  protected abstract String getTable();

  protected abstract ContentValues createContentValues(T object);

  protected abstract ContentValues createBulkContentValues(String[] csvEntry);

  public long insert(SQLiteDatabase database, T object) {
    long insertedId = -1;
    try {
      insertedId = database.insert(getTable(), null, createContentValues(object));
    } catch (SQLiteException e) {
      Log.e(getTable(), e.getMessage(), e);
    }
    return insertedId;
  }

  public void clean(SQLiteDatabase database) {
    try {
      database.delete(getTable(), null, null);
    } catch (SQLiteException e) {
      Log.e(getTable(), e.getMessage(), e);
    }
  }

  protected String getCsvFileName() {
    return getTable() + CSV_EXTENSION;
  }

  public boolean bulkInsert(AssetManager assetManager, SQLiteDatabase database) {
    try {
      InputStream inputStream = assetManager.open(getCsvFileName());
      CSVReader csvReader = new CSVReader(new InputStreamReader(inputStream));
      return insertCsvValues(database, csvReader.readAll());
    } catch (IOException | SQLiteException e) {
      Log.e(getTable(), e.getMessage(), e);
      return false;
    }
  }

  private boolean insertCsvValues(SQLiteDatabase database, List<String[]> entries)
      throws IOException {
    boolean result = true;
    long insertedId;
    for (String[] entry : entries) {
      insertedId = database.insert(getTable(), null, createBulkContentValues(entry));
      result &= insertedId > 0;
    }
    return result;
  }
}
