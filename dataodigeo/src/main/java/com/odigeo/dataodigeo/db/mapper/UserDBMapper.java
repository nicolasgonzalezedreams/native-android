package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import android.util.Log;
import com.odigeo.data.entity.userData.User;
import com.odigeo.dataodigeo.db.dao.UserDBDAO;
import java.util.ArrayList;

public class UserDBMapper {

  /**
   * Mapper user cursor list
   *
   * @param cursor Cursor with user data
   * @return {@link ArrayList< User > userList}
   */
  public ArrayList<User> getUserList(Cursor cursor) {
    ArrayList<User> userList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(UserDBDAO.ID));
        long userId = cursor.getLong(cursor.getColumnIndex(UserDBDAO.USER_ID));
        String email = cursor.getString(cursor.getColumnIndex(UserDBDAO.EMAIL));
        String webSite = cursor.getString(cursor.getColumnIndex(UserDBDAO.WEBSITE));
        boolean acceptsNewsletter =
            (cursor.getInt(cursor.getColumnIndex(UserDBDAO.ACCEPTS_NEWSLETTER)) == 1);
        boolean acceptsPartnersInfo =
            (cursor.getInt(cursor.getColumnIndex(UserDBDAO.ACCEPTS_PARTNERS_INFO)) == 1);
        long creationDate = cursor.getLong(cursor.getColumnIndex(UserDBDAO.CREATION_DATE));
        long lastModified = cursor.getLong(cursor.getColumnIndex(UserDBDAO.LAST_MODIFIED));
        String locale = cursor.getString(cursor.getColumnIndex(UserDBDAO.LOCALE));
        String marketingPortal =
            cursor.getString(cursor.getColumnIndex(UserDBDAO.MARKETING_PORTAL));

        try {

          User.Source source = null;
          if (cursor.getString(cursor.getColumnIndex(UserDBDAO.SOURCE)) != null) {
            source = User.Source.valueOf(cursor.getString(cursor.getColumnIndex(UserDBDAO.SOURCE)));
          } else {
            source = User.Source.UNKNOWN;
          }

          User.Status status = null;
          if (cursor.getString(cursor.getColumnIndex(UserDBDAO.STATUS)) != null) {
            status = User.Status.valueOf(cursor.getString(cursor.getColumnIndex(UserDBDAO.STATUS)));
          } else {
            source = User.Source.UNKNOWN;
          }

          userList.add(new User(id, userId, email, webSite, acceptsNewsletter, acceptsPartnersInfo,
              creationDate, lastModified, locale, marketingPortal, source, status));
        } catch (IllegalArgumentException e) {
          Log.e("UserDBMapper", e.getMessage());
        }
      }
    }

    return userList;
  }

  /**
   * Mapper user cursor
   *
   * @param cursor Cursor with user data
   * @return User
   */
  public User getUser(Cursor cursor) {
    ArrayList<User> userList = getUserList(cursor);

    if (userList.size() == 1) {
      return userList.get(0);
    } else {
      return null;
    }
  }
}
