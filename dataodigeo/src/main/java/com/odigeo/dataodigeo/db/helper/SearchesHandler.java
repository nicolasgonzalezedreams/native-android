package com.odigeo.dataodigeo.db.helper;

import android.support.annotation.NonNull;
import com.odigeo.data.db.dao.SearchSegmentDBDAOInterface;
import com.odigeo.data.db.dao.StoredSearchDBDAOInterface;
import com.odigeo.data.db.helper.SearchesHandlerInterface;
import com.odigeo.data.entity.booking.SearchSegment;
import com.odigeo.data.entity.booking.StoredSearch;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by matias.dirusso on 27/7/2016.
 */
public class SearchesHandler implements SearchesHandlerInterface {
  private SearchSegmentDBDAOInterface mSearchSegmentDBDao;
  private StoredSearchDBDAOInterface mStoredSearchDBDao;

  public SearchesHandler(StoredSearchDBDAOInterface storedSearchDBDao,
      SearchSegmentDBDAOInterface searchSegmentDBDao) {
    mStoredSearchDBDao = storedSearchDBDao;
    mSearchSegmentDBDao = searchSegmentDBDao;
  }

  @Override public List<StoredSearch> getCompleteSearchesFromDB() {
    return completeStoredSearchesData(mStoredSearchDBDao.getAllStoredSearches());
  }

  @Override public void removeSynchronizedStoredSearches() {
    List<StoredSearch> syncStoredSearches = mStoredSearchDBDao.getSynchronizedStoredSearches();

    for (StoredSearch storedSearch : syncStoredSearches) {
      if (mSearchSegmentDBDao.deleteSearchSegmentFromParent(storedSearch.getStoredSearchId())
          >= 0) {
        mStoredSearchDBDao.removeStoredSearch(storedSearch);
      }
    }
  }

  @Override public void updateStoredSearch(StoredSearch storedSearch) {
    mStoredSearchDBDao.updateStoredSearch(storedSearch);
  }

  @Override public void removeAllSearchesLocally(StoredSearch.TripType tripType) {
    List<StoredSearch> storedSearches = getCompleteSearchesFromDB();
    if (tripType.equals(StoredSearch.TripType.M)) {
      mStoredSearchDBDao.removeMultiTripSearches();
    } else {
      mStoredSearchDBDao.removeNonMultiTripSearches();
    }
    for (StoredSearch search : storedSearches) {
      if (search.getTripType().equals(tripType)) {
        mSearchSegmentDBDao.deleteSearchSegmentFromParent(search.getStoredSearchId());
      }
    }
  }

  @Override public void saveStoredSearch(@NonNull StoredSearch storedSearch) {
    long rowId = mStoredSearchDBDao.addStoredSearch(storedSearch);

    long storedSearchId =
        (storedSearch.getStoredSearchId() != 0) ? storedSearch.getStoredSearchId() : rowId;

    if (rowId != -1) {
      for (SearchSegment searchSegment : storedSearch.getSegmentList()) {
        searchSegment.setParentSearchId(storedSearchId);
        mSearchSegmentDBDao.addSearchSegment(searchSegment);
      }
    }
  }

  public List<StoredSearch> completeStoredSearchesData(List<StoredSearch> storedSearches) {
    List<StoredSearch> completeList = new ArrayList<>();
    int multiStopCount = 0;
    int totalCount = 0;
    for (StoredSearch storedSearch : storedSearches) {
      if (storedSearch != null) {
        //Multistop history search has an independent limit
        if (storedSearch.getTripType().equals(StoredSearch.TripType.M)) {
          if (multiStopCount < MAX_SEARCHES) {
            completeList.add(completeSearch(storedSearch));
            totalCount++;
            multiStopCount++;
          }
        } else if (totalCount - multiStopCount < MAX_SEARCHES) {
          completeList.add(completeSearch(storedSearch));
          totalCount++;
        }
      }
    }
    return completeList;
  }

  @Override public StoredSearch completeSearch(StoredSearch storedSearch) {
    return new StoredSearch(storedSearch.getId(), storedSearch.getStoredSearchId(),
        storedSearch.getNumAdults(), storedSearch.getNumChildren(), storedSearch.getNumInfants(),
        storedSearch.getCabinClass(), storedSearch.getIsDirectFlight(), storedSearch.getTripType(),
        storedSearch.isSynchronized(),
        mSearchSegmentDBDao.getSearchSegmentList(storedSearch.getStoredSearchId()),
        storedSearch.getCreationDate(), storedSearch.getUserId());
  }
}
