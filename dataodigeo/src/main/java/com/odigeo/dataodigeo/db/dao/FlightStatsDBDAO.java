package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.FlightStatsDBDAOInterface;
import com.odigeo.data.entity.booking.FlightStats;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.FlightStatsDBMapper;

/**
 * Created by ximenaperez1 on 1/13/16.
 */
public class FlightStatsDBDAO extends OdigeoSQLiteOpenHelper implements FlightStatsDBDAOInterface {

  private FlightStatsDBMapper mFlightStatsDBMapper;

  public FlightStatsDBDAO(Context context) {
    super(context);
    mFlightStatsDBMapper = new FlightStatsDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE IF NOT EXISTS "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + ARRIVAL_GATE
        + " TEXT, "
        + ARRIVAL_TIME
        + " NUMERIC, "
        + ARRIVAL_TIME_DELAY
        + " TEXT, "
        + ARRIVAL_TIME_TYPE
        + " TEXT, "
        + BAGGAGE_CLAIM
        + " TEXT, "
        + DEPARTURE_GATE
        + " TEXT, "
        + DEPARTURE_TIME
        + " NUMERIC, "
        + DEPARTURE_TIME_DELAY
        + " TEXT, "
        + DEPARTURE_TIME_TYPE
        + " TEXT, "
        + DESTINATION_AIRPORT_NAME
        + " TEXT, "
        + DESTINATION_IATA_CODE
        + " TEXT, "
        + EVENT_RECEIVED
        + " TEXT, "
        + FLIGHT_STATUS
        + " TEXT, "
        + GATE_CHANGED
        + " TEXT, "
        + OPERATING_VENDOR
        + " TEXT, "
        + OPERATING_VENDOR_CODE
        + " TEXT, "
        + OPERATING_VENDOR_LEG_NUMBER
        + " TEXT, "
        + UPDATED_ARRIVAL_TERMINAL
        + " TEXT, "
        + UPDATED_DEPARTURE_TERMINAL
        + " TEXT, "
        + SECTION_ID
        + " TEXT, "
        + BOOKING_ID
        + " TEXT "
        + ");";
  }

  public static String getAlterTableSentenceForBookingId() {
    return "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + BOOKING_ID + " TEXT";
  }

  @Override public FlightStats getFlightStats(String sectionId, String bookingId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM "
        + TABLE_NAME
        + " WHERE "
        + SECTION_ID
        + " = "
        + sectionId
        + " AND "
        + BOOKING_ID
        + " = "
        + bookingId
        + "";
    Cursor data = db.rawQuery(selectQuery, null);
    FlightStats flightStats = mFlightStatsDBMapper.getFlightStats(data);
    data.close();
    return flightStats;
  }

  @Override public long addOrUpdateFlightStats(FlightStats flightStats, String bookingId) {
    if (existFlightStatsInDB(flightStats.getSectionId(), bookingId)) {
      return updateFlightStats(flightStats, bookingId);
    }
    return addFlightStats(flightStats, bookingId);
  }

  private boolean existFlightStatsInDB(String flightStatsId, String bookingId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM "
        + TABLE_NAME
        + " WHERE "
        + SECTION_ID
        + " = "
        + flightStatsId
        + " AND "
        + BOOKING_ID
        + " = "
        + bookingId
        + "";
    Cursor flightStatsCursor = db.rawQuery(selectQuery, null);
    boolean exists = flightStatsCursor.moveToNext();
    flightStatsCursor.close();
    return exists;
  }

  private long addFlightStats(FlightStats flightStats, String bookingId) {
    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(SECTION_ID, flightStats.getSectionId());
    values.put(ARRIVAL_GATE, flightStats.getArrivalGate());
    values.put(ARRIVAL_TIME, flightStats.getArrivalTime());
    values.put(ARRIVAL_TIME_DELAY, flightStats.getArrivalTimeDelay());
    values.put(ARRIVAL_TIME_TYPE, flightStats.getArrivalTimeType());
    values.put(BAGGAGE_CLAIM, flightStats.getBaggageClaim());
    values.put(DEPARTURE_GATE, flightStats.getDepartureGate());
    values.put(DEPARTURE_TIME, flightStats.getDepartureTime());
    values.put(DEPARTURE_TIME_DELAY, flightStats.getDepartureTimeDelay());
    values.put(DEPARTURE_TIME_TYPE, flightStats.getDepartureTimeType());
    values.put(EVENT_RECEIVED, flightStats.getEventReceived());
    values.put(FLIGHT_STATUS, flightStats.getFlightStatus());
    values.put(GATE_CHANGED, flightStats.getGateChanged());
    values.put(OPERATING_VENDOR, flightStats.getOperatingVendor());
    values.put(OPERATING_VENDOR_CODE, flightStats.getOperatingVendorCode());
    values.put(OPERATING_VENDOR_LEG_NUMBER, flightStats.getOperatingVendorLegNumber());
    values.put(UPDATED_ARRIVAL_TERMINAL, flightStats.getUpdatedArrivalTerminal());
    values.put(UPDATED_DEPARTURE_TERMINAL, flightStats.getUpdatedDepartureTerminal());
    values.put(BOOKING_ID, bookingId);

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);
    return newRowId;
  }

  private long updateFlightStats(FlightStats flightStats, String bookingId) {
    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(ARRIVAL_GATE, flightStats.getArrivalGate());
    values.put(ARRIVAL_TIME, flightStats.getArrivalTime());
    values.put(ARRIVAL_TIME_DELAY, flightStats.getArrivalTimeDelay());
    values.put(ARRIVAL_TIME_TYPE, flightStats.getArrivalTimeType());
    values.put(BAGGAGE_CLAIM, flightStats.getBaggageClaim());
    values.put(DEPARTURE_GATE, flightStats.getDepartureGate());
    values.put(DEPARTURE_TIME, flightStats.getDepartureTime());
    values.put(DEPARTURE_TIME_DELAY, flightStats.getDepartureTimeDelay());
    values.put(DEPARTURE_TIME_TYPE, flightStats.getDepartureTimeType());
    values.put(EVENT_RECEIVED, flightStats.getEventReceived());
    values.put(FLIGHT_STATUS, flightStats.getFlightStatus());
    values.put(GATE_CHANGED, flightStats.getGateChanged());
    values.put(OPERATING_VENDOR, flightStats.getOperatingVendor());
    values.put(OPERATING_VENDOR_CODE, flightStats.getOperatingVendorCode());
    values.put(OPERATING_VENDOR_LEG_NUMBER, flightStats.getOperatingVendorLegNumber());
    values.put(UPDATED_ARRIVAL_TERMINAL, flightStats.getUpdatedArrivalTerminal());
    values.put(UPDATED_DEPARTURE_TERMINAL, flightStats.getUpdatedDepartureTerminal());

    long rowsAffected;

    rowsAffected = db.update(TABLE_NAME, values, SECTION_ID + "=? AND " + BOOKING_ID + "=?",
        new String[] { String.valueOf(flightStats.getSectionId()), bookingId });
    return rowsAffected;
  }

  @Override public long deleteFlightStats() {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.delete(TABLE_NAME, null, null);
  }
}
