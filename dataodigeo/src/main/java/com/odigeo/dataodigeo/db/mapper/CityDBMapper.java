package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.entity.geo.Coordinates;
import com.odigeo.data.entity.geo.LocationDescriptionType;
import com.odigeo.dataodigeo.db.dao.CityDBDAO;
import java.util.ArrayList;

public class CityDBMapper {

  public City getCity(Cursor cursor) {
    ArrayList<City> cityList = getCityList(cursor);

    if (cityList.size() > 0) {
      return cityList.get(0);
    }

    return null;
  }

  public ArrayList<City> getCityList(Cursor cursor) {
    ArrayList<City> cityList = new ArrayList<>();
    City city;
    Coordinates coordinates;
    String iataCode, cityName, countryCode, countryName, type;
    int geoNodeId;
    double latitude, longitude;

    while (cursor.moveToNext()) {
      geoNodeId = cursor.getInt(cursor.getColumnIndex(CityDBDAO.GEONODE_ID));
      iataCode = cursor.getString(cursor.getColumnIndex(CityDBDAO.IATA_CODE));
      cityName = cursor.getString(cursor.getColumnIndex(CityDBDAO.CITY_NAME));
      countryCode = cursor.getString(cursor.getColumnIndex(CityDBDAO.COUNTRY_CODE));
      countryName = cursor.getString(cursor.getColumnIndex(CityDBDAO.COUNTRY_NAME));
      latitude = cursor.getDouble(cursor.getColumnIndex(CityDBDAO.LATITUDE));
      longitude = cursor.getDouble(cursor.getColumnIndex(CityDBDAO.LONGITUDE));
      type = cursor.getString(cursor.getColumnIndex(CityDBDAO.TYPE));

      city = new City();
      city.setIataCode(iataCode);
      city.setCityName(cityName);
      city.setCountryCode(countryCode);
      city.setCountryName(countryName);
      city.setGeoNodeId(geoNodeId);
      coordinates = new Coordinates();
      coordinates.setLatitude(latitude);
      coordinates.setLongitude(longitude);
      city.setCoordinates(coordinates);
      city.setType(getLocationType(type));
      cityList.add(city);
    }

    return cityList;
  }

  private LocationDescriptionType getLocationType(String type) {
    if (type.equals(LocationDescriptionType.AIRPORT.name())) {
      return LocationDescriptionType.AIRPORT;
    } else if (type.equals(LocationDescriptionType.BUS_STATION.name())) {
      return LocationDescriptionType.BUS_STATION;
    } else if (type.equals(LocationDescriptionType.CITY.name())) {
      return LocationDescriptionType.CITY;
    } else if (type.equals(LocationDescriptionType.IATA_CODE.name())) {
      return LocationDescriptionType.IATA_CODE;
    } else {
      return LocationDescriptionType.TRAIN_STATION;
    }
  }
}
