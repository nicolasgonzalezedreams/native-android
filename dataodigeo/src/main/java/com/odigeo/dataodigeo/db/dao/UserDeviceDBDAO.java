package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.UserDeviceDBDAOInterface;
import com.odigeo.data.entity.userData.UserDevice;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.UserDeviceDBMapper;

public class UserDeviceDBDAO extends OdigeoSQLiteOpenHelper implements UserDeviceDBDAOInterface {

  private UserDeviceDBMapper mUserDeviceDBMapper;

  public UserDeviceDBDAO(Context context) {
    super(context);
    mUserDeviceDBMapper = new UserDeviceDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + USER_DEVICE_ID
        + " NUMERIC, "
        + DEVICE_NAME
        + " TEXT, "
        + DEVICE_ID
        + " TEXT, "
        + IS_PRIMARY
        + " NUMERIC, "
        + ALIAS
        + " TEXT, "
        + USER_ID
        + " INTEGER "
        + ");";
  }

  @Override public UserDevice getUserDevice(long userDeviceId) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + userDeviceId + "";
    Cursor data = db.rawQuery(selectQuery, null);
    UserDevice userDevice = mUserDeviceDBMapper.getUserDevice(data);
    data.close();

    return userDevice;
  }

  @Override public long addUserDevice(UserDevice userDevice) {

    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(USER_DEVICE_ID, userDevice.getUserDeviceId());
    values.put(DEVICE_NAME, userDevice.getDeviceName());
    values.put(DEVICE_ID, userDevice.getDeviceId());
    values.put(IS_PRIMARY, (userDevice.getIsPrimary()) ? 1 : 0);
    values.put(ALIAS, userDevice.getAlias());
    values.put(USER_ID, userDevice.getUserId());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }
}
