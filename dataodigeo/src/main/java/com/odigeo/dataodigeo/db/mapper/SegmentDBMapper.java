package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.dataodigeo.db.dao.SegmentDBDAO;
import java.util.ArrayList;

public class SegmentDBMapper {
  /**
   * Mapper segment cursor list
   *
   * @param cursor Cursor with segment data
   * @return {@link ArrayList<Segment>}
   */
  public ArrayList<Segment> getSegmentList(Cursor cursor) {
    ArrayList<Segment> segmentList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(SegmentDBDAO.ID));
        long duration = cursor.getLong(cursor.getColumnIndex(SegmentDBDAO.DURATION));
        long seats = cursor.getLong(cursor.getColumnIndex(SegmentDBDAO.SEATS));
        long mainCarrier = cursor.getLong(cursor.getColumnIndex(SegmentDBDAO.MAIN_CARRIER_ID));
        long bookingId = cursor.getLong(cursor.getColumnIndex(SegmentDBDAO.BOOKING_ID));

        segmentList.add(new Segment(id, duration, seats, mainCarrier, bookingId));
      }
    }

    return segmentList;
  }

  /**
   * Mapper segment cursor
   *
   * @param cursor Cursor with segment data
   * @return {@link Segment}
   */
  public Segment getSegment(Cursor cursor) {
    ArrayList<Segment> segmentList = getSegmentList(cursor);

    if (segmentList.size() == 1) {
      return segmentList.get(0);
    } else {
      return null;
    }
  }
}
