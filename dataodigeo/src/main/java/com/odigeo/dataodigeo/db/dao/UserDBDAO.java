package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.UserDBDAOInterface;
import com.odigeo.data.entity.userData.User;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.UserDBMapper;

public class UserDBDAO extends OdigeoSQLiteOpenHelper implements UserDBDAOInterface {

  private UserDBMapper mUserDBMapper;

  public UserDBDAO(Context context) {
    super(context);
    mUserDBMapper = new UserDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + USER_ID
        + " NUMERIC, "
        + EMAIL
        + " TEXT, "
        + WEBSITE
        + " TEXT, "
        + ACCEPTS_NEWSLETTER
        + " NUMERIC, "
        + ACCEPTS_PARTNERS_INFO
        + " NUMERIC, "
        + CREATION_DATE
        + " NUMERIC, "
        + LAST_MODIFIED
        + " NUMERIC, "
        + LOCALE
        + " TEXT, "
        + MARKETING_PORTAL
        + " TEXT, "
        + SOURCE
        + " TEXT, "
        + STATUS
        + " TEXT "
        + ");";
  }

  @Override public User getCurrentUser() {
    SQLiteDatabase db = getOdigeoDatabase();
    Cursor data = db.query(TABLE_NAME, null, null, null, null, null, null);
    User user = mUserDBMapper.getUser(data);
    data.close();

    return user;
  }

  @Override public User getUser(String email) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + EMAIL + " = '" + email + "'";
    Cursor data = db.rawQuery(selectQuery, null);
    User user = mUserDBMapper.getUser(data);
    data.close();

    return user;
  }

  @Override public long addUser(User user) {
    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(USER_ID, user.getUserId());
    values.put(EMAIL, user.getEmail());
    values.put(WEBSITE, user.getWebSite());
    values.put(ACCEPTS_NEWSLETTER, (user.getAcceptsNewsletter()) ? 1 : 0);
    values.put(ACCEPTS_PARTNERS_INFO, (user.getAcceptsPartnersInfo()) ? 1 : 0);
    values.put(CREATION_DATE, user.getCreationDate());
    values.put(LAST_MODIFIED, user.getLastModified());
    values.put(LOCALE, user.getLocale());
    values.put(MARKETING_PORTAL, user.getMarketingPortal());
    values.put(SOURCE, user.getSource().toString());
    values.put(STATUS, user.getStatus().toString());

    long newRowId;

    newRowId = db.insert(TABLE_NAME, null, values);

    return newRowId;
  }

  @Override public boolean updateUser(String email, User user) {
    SQLiteDatabase db = getOdigeoDatabase();

    ContentValues values = new ContentValues();
    values.put(USER_ID, user.getUserId());
    values.put(EMAIL, user.getEmail());
    values.put(WEBSITE, user.getWebSite());
    values.put(ACCEPTS_NEWSLETTER, (user.getAcceptsNewsletter()) ? 1 : 0);
    values.put(ACCEPTS_PARTNERS_INFO, (user.getAcceptsPartnersInfo()) ? 1 : 0);
    values.put(CREATION_DATE, user.getCreationDate());
    values.put(LAST_MODIFIED, user.getLastModified());
    values.put(LOCALE, user.getLocale());
    values.put(MARKETING_PORTAL, user.getMarketingPortal());
    if (user.getSource() != null) {
      values.put(SOURCE, user.getSource().toString());
    }
    if (user.getStatus() != null) {
      values.put(STATUS, user.getStatus().toString());
    }

    long rowsAffected;

    rowsAffected = db.update(TABLE_NAME, values, EMAIL + "=" + "'" + email + "'", null);

    return (rowsAffected != 0);
  }

  @Override public boolean updateOrCreateUser(User user) {
    return updateUser(user.getEmail(), user) || (addUser(user) != -1);
  }

  @Override public boolean deleteUsers() {
    SQLiteDatabase database = getOdigeoDatabase();
    database.beginTransaction();
    try {
      database.delete(TABLE_NAME, null, null);
      database.delete(UserAddressDBDAO.TABLE_NAME, null, null);
      database.delete(UserAirlinePreferencesDBDAO.TABLE_NAME, null, null);
      database.delete(UserAirportPreferencesDBDAO.TABLE_NAME, null, null);
      database.delete(UserDestinationPreferencesDBDAO.TABLE_NAME, null, null);
      database.delete(UserDeviceDBDAO.TABLE_NAME, null, null);
      database.delete(UserFrequentFlyerDBDAO.TABLE_NAME, null, null);
      database.delete(UserIdentificationDBDAO.TABLE_NAME, null, null);
      database.delete(UserLoginDBDAO.TABLE_NAME, null, null);
      database.delete(UserProfileDBDAO.TABLE_NAME, null, null);
      database.delete(UserTravellerDBDAO.TABLE_NAME, null, null);
      database.setTransactionSuccessful();
    } catch (SQLException e) {
      database.endTransaction();
      return false;
    }
    database.endTransaction();
    return true;
  }
}
