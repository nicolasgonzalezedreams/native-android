package com.odigeo.dataodigeo.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.dao.CityDBDAOInterface;
import com.odigeo.data.entity.geo.City;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.mapper.CityDBMapper;

public class CityDBDAO extends OdigeoSQLiteOpenHelper implements CityDBDAOInterface {

  private CityDBMapper cityDBMapper;

  public CityDBDAO(Context context) {
    super(context);
    cityDBMapper = new CityDBMapper();
  }

  public static String getCreateTableSentence() {
    return "CREATE TABLE "
        + TABLE_NAME
        + " ("
        + GEONODE_ID
        + " NUMERIC PRIMARY KEY, "
        + IATA_CODE
        + " TEXT, "
        + CITY_NAME
        + " TEXT, "
        + NAME
        + " TEXT, "
        + COUNTRY_CODE
        + " TEXT, "
        + COUNTRY_NAME
        + " TEXT, "
        + LATITUDE
        + " REAL, "
        + LONGITUDE
        + " REAL, "
        + TYPE
        + " TEXT "
        + ");";
  }

  @Override public boolean addCity(City city) {
    SQLiteDatabase db = getOdigeoDatabase();
    return db.insertWithOnConflict(TABLE_NAME, null, getContentValues(city),
        SQLiteDatabase.CONFLICT_IGNORE) != -1;
  }

  @Override public City getCityByIATA(String iataCode) {
    SQLiteDatabase db = getOdigeoDatabase();
    String selectQuery =
        "SELECT * FROM " + TABLE_NAME + " WHERE " + IATA_CODE + " = '" + iataCode + "'";
    Cursor data = db.rawQuery(selectQuery, null);
    City city = cityDBMapper.getCity(data);
    data.close();

    return city;
  }

  private ContentValues getContentValues(City city) {
    ContentValues values = new ContentValues();

    values.put(GEONODE_ID, city.getGeoNodeId());
    values.put(IATA_CODE, city.getIataCode());
    values.put(CITY_NAME, city.getCityName());
    values.put(NAME, city.getName());
    values.put(COUNTRY_CODE, city.getCountryCode());
    values.put(COUNTRY_NAME, city.getCountryName());
    values.put(LATITUDE, city.getCoordinates().getLatitude());
    values.put(LONGITUDE, city.getCoordinates().getLongitude());
    values.put(TYPE, city.getType().toString());

    return values;
  }
}
