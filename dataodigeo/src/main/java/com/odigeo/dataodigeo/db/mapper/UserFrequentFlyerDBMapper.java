package com.odigeo.dataodigeo.db.mapper;

import android.database.Cursor;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.dataodigeo.db.dao.UserFrequentFlyerDBDAO;
import java.util.ArrayList;

public class UserFrequentFlyerDBMapper {

  /**
   * Mapper user frequent Flyer cursor list
   *
   * @param cursor Cursor with user frequent Flyer data
   * @return {@link ArrayList< UserFrequentFlyer >}
   */
  public ArrayList<UserFrequentFlyer> getUserFrequentFlyerList(Cursor cursor) {
    ArrayList<UserFrequentFlyer> userFrequentFlyerList = new ArrayList<>();

    if (cursor.getCount() > 0) {

      while (cursor.moveToNext()) {

        long id = cursor.getLong(cursor.getColumnIndex(UserFrequentFlyerDBDAO.ID));
        long userFrequentFlyerId =
            cursor.getLong(cursor.getColumnIndex(UserFrequentFlyerDBDAO.USER_FREQUENT_FLYER_ID));
        String airlineCode =
            cursor.getString(cursor.getColumnIndex(UserFrequentFlyerDBDAO.AIRLINE_CODE));
        String frequentFlyerNumber =
            cursor.getString(cursor.getColumnIndex(UserFrequentFlyerDBDAO.FREQUENT_FLYER_NUMBER));
        long userTravellerId =
            cursor.getLong(cursor.getColumnIndex(UserFrequentFlyerDBDAO.USER_TRAVELLER_ID));

        userFrequentFlyerList.add(
            new UserFrequentFlyer(id, userFrequentFlyerId, airlineCode, frequentFlyerNumber,
                userTravellerId));
      }
    }

    cursor.close();

    return userFrequentFlyerList;
  }

  /**
   * Mapper user frequent Flyer cursor
   *
   * @param cursor Cursor with user frequent Flyer data
   * @return {@link UserFrequentFlyer}
   */
  public UserFrequentFlyer getUserFrequentFlyer(Cursor cursor) {
    ArrayList<UserFrequentFlyer> userFrequentFlyerList = getUserFrequentFlyerList(cursor);

    if (userFrequentFlyerList.size() == 1) {
      return userFrequentFlyerList.get(0);
    } else {
      return null;
    }
  }
}
