package com.odigeo.dataodigeo.firebase.remoteconfig;

public interface FirebaseAnalyticsInterface {
  void setUserProperty(String experiment, String variant);
}
