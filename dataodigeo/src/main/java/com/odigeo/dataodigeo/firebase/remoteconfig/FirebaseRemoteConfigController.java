package com.odigeo.dataodigeo.firebase.remoteconfig;

import android.support.annotation.NonNull;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.odigeo.data.ab.RemoteConfigControllerInterface;
import odigeo.dataodigeo.BuildConfig;
import odigeo.dataodigeo.R;

public class FirebaseRemoteConfigController implements RemoteConfigControllerInterface {

  private static FirebaseRemoteConfigController instance;
  private long CACHE_EXPIRATION_TIME = 3600;
  private FirebaseRemoteConfig firebaseRemoteConfig;
  private FirebaseAnalyticsController firebaseAnalytics;

  public FirebaseRemoteConfigController(FirebaseRemoteConfig firebaseRemoteConfig,
      FirebaseAnalyticsController firebaseAnalytics) {
    this.firebaseRemoteConfig = firebaseRemoteConfig;
    FirebaseRemoteConfigSettings firebaseRemoteConfigSettings =
        new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(BuildConfig.DEBUG)
            .build();
    this.firebaseRemoteConfig.setConfigSettings(firebaseRemoteConfigSettings);
    this.firebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);

    this.firebaseAnalytics = firebaseAnalytics;

    if (this.firebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
      CACHE_EXPIRATION_TIME = 0;
    }
  }

  public static FirebaseRemoteConfigController newInstance(
      FirebaseRemoteConfig firebaseRemoteConfig,
      FirebaseAnalyticsController firebaseAnalyticsController) {
    if (instance == null) {
      instance =
          new FirebaseRemoteConfigController(firebaseRemoteConfig, firebaseAnalyticsController);
    }
    return instance;
  }

  @Override public void fetchConfig() {
    firebaseRemoteConfig.fetch(CACHE_EXPIRATION_TIME)
        .addOnCompleteListener(new OnCompleteListener<Void>() {
          @Override public void onComplete(@NonNull Task<Void> task) {
            if (task.isSuccessful()) {
              firebaseRemoteConfig.activateFetched();
            }
          }
        });
  }

  @Override public void initExperiment(final String experimentName, final String parameter) {
    String experimentVariant = firebaseRemoteConfig.getString(parameter);
    firebaseAnalytics.setUserProperty(experimentName, experimentVariant);
  }

  @Override public String getStringVariant(String parameter) {
    return firebaseRemoteConfig.getValue(parameter).asString();
  }

  @Override public boolean getBoolVariant(String parameter) {
    return firebaseRemoteConfig.getValue(parameter).asBoolean();
  }

  @Override public double getDoubleVariant(String parameter) {
    return firebaseRemoteConfig.getValue(parameter).asDouble();
  }

  @Override public long getLongVariant(String parameter) {
    return firebaseRemoteConfig.getValue(parameter).asLong();
  }

  @Override public boolean isDropOffActive() {
    return firebaseRemoteConfig.getBoolean(FirebaseRemoteConfigConstants.DROPOFF_ACTIVE);
  }

  @Override public boolean isWalkthroughActive() {
    return firebaseRemoteConfig.getBoolean(FirebaseRemoteConfigConstants.WALKTHROUGH_ACTIVE);
  }

  @Override public boolean shouldShowDefaultWalkthrough() {
    return firebaseRemoteConfig.getBoolean(FirebaseRemoteConfigConstants.WALKTHROUGH_DEFAULT);
  }
}