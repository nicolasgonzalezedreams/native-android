package com.odigeo.dataodigeo.firebase.remoteconfig;

import android.content.Context;
import com.google.firebase.analytics.FirebaseAnalytics;

public class FirebaseAnalyticsController implements FirebaseAnalyticsInterface {

  private static FirebaseAnalyticsController instance;
  private FirebaseAnalytics firebaseAnalytics;

  public FirebaseAnalyticsController(Context context) {
    firebaseAnalytics = FirebaseAnalytics.getInstance(context);
  }

  public static FirebaseAnalyticsController getInstance(Context context) {
    if (instance == null) {
      instance = new FirebaseAnalyticsController(context);
    }
    return instance;
  }

  @Override public void setUserProperty(String experiment, String variant) {
    firebaseAnalytics.setUserProperty(experiment, variant);
  }
}
