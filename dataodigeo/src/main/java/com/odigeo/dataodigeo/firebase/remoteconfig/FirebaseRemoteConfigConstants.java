package com.odigeo.dataodigeo.firebase.remoteconfig;

class FirebaseRemoteConfigConstants {
  static String DROPOFF_ACTIVE = "dropoff_active";
  static String WALKTHROUGH_ACTIVE = "walkthrough_active";
  static String WALKTHROUGH_DEFAULT = "walkthrough_default";
}
