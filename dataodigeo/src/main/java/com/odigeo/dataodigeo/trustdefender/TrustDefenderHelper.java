package com.odigeo.dataodigeo.trustdefender;

import com.threatmetrix.TrustDefender.Config;
import com.threatmetrix.TrustDefender.ProfilingOptions;
import com.threatmetrix.TrustDefender.THMStatusCode;
import com.threatmetrix.TrustDefender.TrustDefender;

public class TrustDefenderHelper {

  private final TrustDefender trustDefender;

  public TrustDefenderHelper(TrustDefender trustDefender) {
    this.trustDefender = trustDefender;
  }

  public void init(final Config config, final TrustDefenderListener listener) {
    Thread thread = new Thread(new Runnable() {
      @Override public void run() {
        listener.onInit(trustDefender.init(config));
      }
    });
    thread.start();
  }

  THMStatusCode doProfileRequest(ProfilingOptions profilingOptions) {
    return trustDefender.doProfileRequest(profilingOptions);
  }

  String getSessionID() {
    return trustDefender.getResult().getSessionID();
  }
}
