package com.odigeo.dataodigeo.trustdefender;

import android.content.Context;
import android.util.Log;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.data.trustdefender.TrustDefenderController;
import com.threatmetrix.TrustDefender.Config;
import com.threatmetrix.TrustDefender.EndNotifier;
import com.threatmetrix.TrustDefender.ProfilingOptions;
import com.threatmetrix.TrustDefender.ProfilingResult;
import com.threatmetrix.TrustDefender.THMStatusCode;
import com.threatmetrix.TrustDefender.TrustDefender;
import odigeo.dataodigeo.R;

import static com.odigeo.data.tracker.CrashlyticsController.CYBERSOURCE_MERCHANT_ID;
import static com.odigeo.data.tracker.CrashlyticsController.DAPI_JSESSION;
import static com.odigeo.data.tracker.CrashlyticsController.TRUST_DEFENDER_CONFIGURATION;
import static com.odigeo.data.tracker.CrashlyticsController.TRUST_DEFENDER_ID;
import static com.odigeo.data.tracker.CrashlyticsController.TRUST_DEFENDER_STATUS_DESCRIPTION;

public class TrustDefenderControllerImpl implements TrustDefenderController {

  private static final  String FINGERPRINT_SERVER_URL = "h-sdk.online-metrix.net";
  private static final String TAG = "TRUST_DEFENDER";
  private static final String TAG_ORG_ID = "OrgId: ";
  private static final String TAG_SUCCESSFUL = "Init successful: ";
  private static final String TAG_UNSUCCESSFUL = "Init unsuccessful: ";
  private static final String TAG_VERSION = "Version: ";
  private static final String TAG_PROFILING_RESULT = "ProfilingResult: ";
  private static final String TAG_SESSION_ID = "SessionId: ";

  private final Context context;
  private final CrashlyticsController crashlyticsController;
  private final TrustDefenderHelper trustDefenderHelper;
  private final boolean isTestEnvironment;

  public TrustDefenderControllerImpl(Context context, CrashlyticsController crashlyticsController,
      TrustDefenderHelper trustDefenderHelper, boolean isTestEnvironment) {
    this.context = context;
    this.crashlyticsController = crashlyticsController;
    this.trustDefenderHelper = trustDefenderHelper;
    this.isTestEnvironment = isTestEnvironment;
  }

  @Override public void sendFingerPrint(String cyberSourceMerchantId, String jSessionId) {
    if (cyberSourceMerchantId != null && jSessionId != null) {
      final String trustDefenderId = cyberSourceMerchantId + jSessionId.split("\\.")[0];
      final String orgId;
      if (isTestEnvironment) {
        orgId = context.getResources().getString(R.string.trust_defender_debug_org_id);
      } else {
        orgId = context.getResources().getString(R.string.trust_defender_release_org_id);
      }
      createFingerPrintProfile(trustDefenderId, orgId);
    } else {
      sendTrustDefenderIdNotValidToCrashlytics(cyberSourceMerchantId, jSessionId);
    }
  }

  private void createFingerPrintProfile(final String sessionId, String orgId) {
    Log.i(TAG, TAG_ORG_ID + orgId);

    Config config =
        new Config().setOrgId(orgId).setContext(context).setFPServer(FINGERPRINT_SERVER_URL);

    trustDefenderHelper.init(config, new TrustDefenderListener() {
      @Override public void onInit(THMStatusCode thmStatusCode) {
        if (thmStatusCode == THMStatusCode.THM_OK
            || thmStatusCode == THMStatusCode.THM_Already_Initialised) {
          Log.i(TAG, TAG_SUCCESSFUL + thmStatusCode.getDesc());
          doProfile(sessionId);
        } else {
          Log.i(TAG, TAG_UNSUCCESSFUL + thmStatusCode.getDesc());
          sendTHMSInitUnsuccessfulToCrashlytics(thmStatusCode.getDesc());
        }
      }
    });
  }

  private void doProfile(String sessionId) {
    Log.i(TAG, TAG_VERSION + TrustDefender.version);

    ProfilingOptions profilingOptions =
        new ProfilingOptions().setSessionID(sessionId).setEndNotifier(new EndNotifier() {
          @Override public void complete(ProfilingResult profilingResult) {
            Log.i(TAG, TAG_PROFILING_RESULT + profilingResult.getSessionID());
          }
        });

    THMStatusCode statusCode = trustDefenderHelper.doProfileRequest(profilingOptions);
    if (statusCode == THMStatusCode.THM_OK) {
      Log.i(TAG, TAG_SESSION_ID + trustDefenderHelper.getSessionID());
    } else {
      sendTHMSInitUnsuccessfulToCrashlytics(statusCode.getDesc());
    }
  }

  private void sendTrustDefenderIdNotValidToCrashlytics(String cyberSourceMerchantId,
      String jSessionId) {
    crashlyticsController.trackNonFatal(
        new Exception(TRUST_DEFENDER_ID, new Throwable(TRUST_DEFENDER_ID)));
    crashlyticsController.setString(CYBERSOURCE_MERCHANT_ID, cyberSourceMerchantId);
    crashlyticsController.setString(DAPI_JSESSION, jSessionId);
  }

  private void sendTHMSInitUnsuccessfulToCrashlytics(String initStatusDesc) {
    crashlyticsController.trackNonFatal(
        new Exception(TRUST_DEFENDER_CONFIGURATION, new Throwable(TRUST_DEFENDER_CONFIGURATION)));
    crashlyticsController.setString(TRUST_DEFENDER_STATUS_DESCRIPTION, initStatusDesc);
  }
}
