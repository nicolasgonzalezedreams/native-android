package com.odigeo.dataodigeo.trustdefender;

import com.threatmetrix.TrustDefender.THMStatusCode;

interface TrustDefenderListener {

  void onInit(THMStatusCode thmStatusCode);
}
