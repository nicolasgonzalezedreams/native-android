package com.odigeo.dataodigeo.constants;

/**
 * The rule is: If key is in lowerCamelCase (e.g. <b>myJsonKey</b> ), the name of the constant is
 * the same but in constant name format (e.g.
 * <b>MY_JSON_KEY</b>
 *
 * @author Irving
 * @since 11/09/2015
 */
public class JsonKeys {

  public static final String ID = "id";

  public static final String SOURCE = "source";
  public static final String LAST_MODIFIED = "lastModified";
  public static final String LOCALE = "locale";
  public static final String STATUS = "status";
  public static final String CREATION_DATE = "creationDate";
  public static final String BRAND = "brand";
  public static final String WEB_SITE = "webSite";
  public static final String MARKETING_PORTAL = "marketingPortal";
  public static final String TRAFFIC_INTERFACE = "trafficInterface";
  public static final String ACCEPTS_NEWSLETTER = "acceptsNewsletter";
  public static final String ACCEPTS_PARTNERS_INFO = "acceptsPartnersInfo";
  public static final String PROFILE = "profile";

  public static final String MEMBERSHIP_INFO = "memberships";
  public static final String MEMBER_ID = "memberId";
  public static final String MEMBER_NAME = "name";
  public static final String MEMBER_LASTNAMES = "lastNames";
  public static final String MEMBER_WEBSITE = "website";

  public static final String GENDER = "gender";
  public static final String TITLE = "title";
  public static final String NAME = "name";
  public static final String FIRST_LAST_NAME = "firstLastName";
  public static final String MIDDLE_NAME = "middleName";
  public static final String SECOND_LAST_NAME = "secondLastName";
  public static final String BIRTH_DATE = "birthDate";
  public static final String PREFIX_PHONE_NUMBER = "prefixPhoneNumber";
  public static final String PHONE_NUMBER = "phoneNumber";
  public static final String MOBILE_PHONE_NUMBER = "mobilePhoneNumber";
  public static final String PREFIX_ALTERNATE_PHONE_NUMBER = "prefixAlternatePhoneNumber";
  public static final String ALTERNATE_PHONE_NUMBER = "alternatePhoneNumber";
  public static final String IDENTIFICATION_LIST = "identificationList";

  public static final String IDENTIFICATION_TYPE = "identificationType";
  public static final String IDENTIFICATION_ID = "identificationId";
  public static final String IDENTIFICATION_EXPIRATION_DATE = "identificationExpirationDate";
  public static final String IDENTIFICATION_COUNTRY_CODE = "identificationCountryCode";

  public static final String TRAVELLLER = "traveller";
  public static final String BUYER = "buyer";
  public static final String TYPE_OF_TRAVELLER = "typeOfTraveller";
  public static final String FREQUENT_FLYER_LIST = "frequentFlyerList";

  public static final String IS_DEFAULT = "isDefault";
  public static final String CPF = "cpf";
  public static final String NATIONALITY_COUNTRY_CODE = "nationalityCountryCode";

  public static final String ADDRESS = "address";
  public static final String ADDRESS_TYPE = "addressType";
  public static final String CITY = "city";
  public static final String COUNTRY = "country";
  public static final String POSTAL_CODE = "postalCode";
  public static final String IS_PRIMARY = "isPrimary";
  public static final String ALIAS = "alias";
  public static final String STATE = "state";

  public static final String MEAL_TYPE = "mealType";
  public static final String EMAIL = "email";
  public static final String DEVICES = "devices";
  public static final String AIRPORT_PREFERENCES_LIST = "airportPreferencesList";
  public static final String AIRLINE_PREFERENCES_LIST = "airlinePreferencesList";
  public static final String DESTINATION_PREFERENCES_LIST = "destinationPreferencesList";

  public static final String BOOKINGS = "bookings";
  public static final String LOGIN = "login";
  public static final String INVOICE_LIST = "invoiceList";
  public static final String SEARCH_LIST = "searchList";
  public static final String CREDIT_CARDS = "creditCards";
  public static final String CARD_ID = "id";
  public static final String CARD_OWNER = "owner";
  public static final String CARD_CREATION = "creationDate";
  public static final String CARD_NUMBER = "creditCardNumber";
  public static final String CARD_EXPIRY_MONTH = "expirationDateMonth";
  public static final String CARD_EXPIRY_YEAR = "expirationDateYear";
  public static final String CARD_MODIFIED_DATE = "modifiedDate";
  public static final String CARD_IS_DEFAULT = "isDefault";
  public static final String CARD_LAST_USAGE = "lastUsageDate";
  public static final String CARD_TYPE = "creditCardTypeId";

  public static final String LOGIN_ATTEMPT_FAILED = "loginAttemptFailed";
  public static final String LAST_LOGIN_DATE = "lastLoginDate";
  public static final String AIRLINE_CODE = "airlineCode";
  public static final String FREQUENT_FLYER_NUMBER = "frequentFlyerNumber";

  public static final String DEVICE_NAME = "deviceName";
  public static final String DEVICE_ID = "deviceId";
  public static final String PASSWORD = "password";
  public static final String ACCESS_TOKEN = "accessToken";
  public static final String EXPIRATION_TOKEN_DATE = "expirationTokenDate";
  public static final String HASH_CODE = "hashCode";
  public static final String ACTIVATION_CODE = "activationCode";
  public static final String ACTIVATION_CODE_DATE = "activationCodeDate";
  public static final String REFRESH_TOKEN = "refreshToken";
  public static final String AIRPORT = "airport";
  public static final String DESTINATION = "destination";

  public static final String IMAGE = "image";
  public static final String PRIORITY = "priority";
  public static final String TYPE = "type";
  public static final String CARDS = "cards";
  public static final String LAST_UPDATE = "lastUpdate";
  public static final String SUBTITLE = "subtitle";
  public static final String CARRIER = "carrier";
  public static final String CARRIER_NAME = "carrierName";
  public static final String FLIGHT_ID = "flightId";
  public static final String IATA = "iata";
  public static final String DATE = "date";
  public static final String TERMINAL = "terminal";
  public static final String DESCRIPTION = "description";
  public static final String FROM = "from";
  public static final String TO = "to";
  public static final String DEPARTURE_DELAY = "departureDelay";
  public static final String ARRIVAL_DELAY = "arrivalDelay";
  public static final String BAGGAGE_BELT = "baggageBelt";
  public static final String GATE = "gate";
  public static final String URL = "url";
  public static final String URL_TEXT = "urlText";
  public static final String URL_2 = "url2";
  public static final String URL_TEXT_2 = "urlText2";
  public static final String SUBTYPE = "subType";

  // STORED SEARCHES
  public static final String NUM_ADULTS = "numAdults";
  public static final String NUM_CHILDREN = "numChildren";
  public static final String NUM_INFANTS = "numInfants";
  public static final String IS_DIRECT_FLIGHT = "directFlight";
  public static final String CABIN_CLASS = "cabinClass";
  public static final String CREDIT_CARD_TYPE = "creditCardType";
  public static final String TRIP_TYPE = "tripType";
  public static final String SEARCH_SEGMENT_LIST = "searchSegmentList";

  //SEARCH SEGMENT
  public static final String ORIGIN_IATA_CODE = "origin";
  public static final String DESTINATION_IATA_CODE = "destination";
  public static final String DEPARTURE_DATE = "departureDate";
  public static final String RETURN_DATE = "returnDate";
  public static final String SEGMENT_ORDER = "segmentOrder";

  public static final String CAMPAIGN_CARD_KEY = "campaignCardKey";
  public static final String SORT_CRITERIA = "sortCriteria";
  public static final String ITINERARY_SELECTION_REQUEST = "itinerarySelectionRequest";
}
