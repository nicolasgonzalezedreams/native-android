package com.odigeo.dataodigeo.tracker;

import android.os.Bundle;
import android.support.annotation.NonNull;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.odigeo.data.tracker.CustomDimensionFormatter;
import com.odigeo.dataodigeo.tracker.searchtracker.SearchTrackHelper;
import com.odigeo.helper.ABTestHelper;
import java.math.BigDecimal;
import java.util.Currency;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(RobolectricTestRunner.class) @Config(manifest = Config.NONE)
public class TrackerControllerTest {
  private static final String FLIGHT_TYPE = "OW";
  private static final String CURRENCY_CODE = "EUR";
  private static final String DEPARTURE_COUNTRY = "ES";
  private static final String ARRIVAL_COUNTRY = "ES";
  private static final double PRICE = 123.53;
  private static final String AIRLINE = "KLM";
  private static final String DEPARTURE = "MAD";
  private static final String ARRIVAL = "BCN";
  private static final String SIGN_UP_METHOD = "password";
  private static final long DATE_MILLIS = 1488002400000L;
  private TrackerController mTrackerController;
  private SearchTrackHelper mSearchTrackHelper;

  @Mock private AppEventsLogger mAppEventsLogger;
  @Mock private ABTestHelper abTestHelper;
  @Mock private CustomDimensionFormatter customDimensionFormatter;

  @Before public void setUp() throws Exception {
    FacebookSdk.sdkInitialize(RuntimeEnvironment.application);
    MockitoAnnotations.initMocks(this);
    mTrackerController =
        TrackerController.newInstance(RuntimeEnvironment.application, null, abTestHelper,
            customDimensionFormatter);
    mTrackerController.setAppEventsLogger(mAppEventsLogger);
    mSearchTrackHelper = new SearchTrackHelper();
  }

  @Test public void trackSearchLaunched_callLogEventOnce() {
    SearchTrackHelper helper = buildSimpleSearchTrack();
    mTrackerController.trackSearchLaunched(FLIGHT_TYPE, helper.getAdults(), helper.getKids(),
        helper.getInfants(), helper.getOrigins(), helper.getDestinations(), helper.getDates(),
        helper.getDepartureCountries(), helper.getArrivalCountries());
    Bundle bundle = new Bundle();
    bundle.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, FLIGHT_TYPE);
    bundle.putInt(TrackerConstants.EVENT_PARAM_NUM_ADULTS, helper.getAdults());
    bundle.putInt(TrackerConstants.EVENT_PARAM_NUM_KIDS, helper.getKids());
    bundle.putInt(TrackerConstants.EVENT_PARAM_NUM_INFANTS, helper.getInfants());
    bundle =
        mTrackerController.setSegmentInfo(bundle, helper.getOrigins(), helper.getDestinations(),
            helper.getDates(), helper.getDepartureCountries(), helper.getArrivalCountries());

    verify(mAppEventsLogger, times(1)).logEvent(eq(AppEventsConstants.EVENT_NAME_SEARCHED),
        refEq(bundle));
    verifyNoMoreInteractions(mAppEventsLogger);
  }

  @Test public void trackSummaryReached_callLogEventOnce() {
    SearchTrackHelper helper = buildSearchTrackWithPriceAndAirlines();
    mTrackerController.trackSummaryReached(CURRENCY_CODE, FLIGHT_TYPE, helper.getAdults(),
        helper.getKids(), helper.getInfants(), PRICE, helper.getOrigins(), helper.getDestinations(),
        helper.getDates(), helper.getAirlines(), helper.getDepartureCountries(),
        helper.getArrivalCountries());
    verify(mAppEventsLogger, times(1)).logEvent(eq(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT),
        eq(PRICE), refEq(getBundle(helper)));
    verifyNoMoreInteractions(mAppEventsLogger);
  }

  @Test public void trackSummaryContinue_callLogEventOnce() {
    SearchTrackHelper helper = buildSearchTrackWithPriceAndAirlines();
    mTrackerController.trackSummaryContinue(CURRENCY_CODE, FLIGHT_TYPE, helper.getAdults(),
        helper.getKids(), helper.getInfants(), PRICE, helper.getOrigins(), helper.getDestinations(),
        helper.getDates(), helper.getAirlines(), helper.getDepartureCountries(),
        helper.getArrivalCountries());
    verify(mAppEventsLogger, times(1)).logEvent(eq(AppEventsConstants.EVENT_NAME_ADDED_TO_CART),
        eq(PRICE), refEq(getBundle(helper)));
    verifyNoMoreInteractions(mAppEventsLogger);
  }

  @Test public void trackPaymentReached_callLogEventOnce() {
    SearchTrackHelper helper = buildSearchTrackWithPriceAndAirlines();
    mTrackerController.trackPaymentReached(CURRENCY_CODE, FLIGHT_TYPE, helper.getAdults(),
        helper.getKids(), helper.getInfants(), PRICE, helper.getOrigins(), helper.getDestinations(),
        helper.getDates(), helper.getAirlines(), helper.getDepartureCountries(),
        helper.getArrivalCountries());
    verify(mAppEventsLogger, times(1)).logEvent(
        eq(AppEventsConstants.EVENT_NAME_INITIATED_CHECKOUT), eq(PRICE), refEq(getBundle(helper)));
    verifyNoMoreInteractions(mAppEventsLogger);
  }

  @Test public void trackPaymentSuccessful_callLogPurchaseOnce() throws Exception {
    BigDecimal flightPrice = new BigDecimal(PRICE);
    Currency currency = Currency.getInstance(CURRENCY_CODE);
    SearchTrackHelper helper = buildSearchTrackWithPriceAndAirlines();

    mTrackerController.trackPaymentSuccessful(flightPrice, currency, FLIGHT_TYPE,
        helper.getAdults(), helper.getKids(), helper.getInfants(), helper.getOrigins(),
        helper.getDestinations(), helper.getDates(), helper.getAirlines(),
        helper.getDepartureCountries(), helper.getArrivalCountries());

    verify(mAppEventsLogger, times(1)).logPurchase(refEq(flightPrice), refEq(currency),
        refEq(getBundle(helper)));
    verifyNoMoreInteractions(mAppEventsLogger);
  }

  @Test public void trackSignUp_callLogEventOnce() throws Exception {
    mTrackerController.trackSignUp();
    Bundle bundle = new Bundle();
    bundle.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, SIGN_UP_METHOD);
    verify(mAppEventsLogger, times(1)).logEvent(
        eq(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION), refEq(bundle));
    verifyNoMoreInteractions(mAppEventsLogger);
  }

  @Test public void trackHotels_callLogEventOnce() throws Exception {
    mTrackerController.trackHotels();
    verify(mAppEventsLogger, times(1)).logEvent(TrackerConstants.HOTELS_HOME_PAGE);
    verifyNoMoreInteractions(mAppEventsLogger);
  }

  @Test public void trackCars_callLogEventOnce() throws Exception {
    mTrackerController.trackCars();
    verify(mAppEventsLogger, times(1)).logEvent(TrackerConstants.CARS_HOME_PAGE);
    verifyNoMoreInteractions(mAppEventsLogger);
  }

  @NonNull private Bundle getBundle(SearchTrackHelper helper) {
    Bundle bundle = new Bundle();
    bundle.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, CURRENCY_CODE);
    bundle.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, FLIGHT_TYPE);
    bundle.putInt(TrackerConstants.EVENT_PARAM_NUM_ADULTS, helper.getAdults());
    bundle.putInt(TrackerConstants.EVENT_PARAM_NUM_KIDS, helper.getKids());
    bundle.putInt(TrackerConstants.EVENT_PARAM_NUM_INFANTS, helper.getInfants());
    bundle =
        mTrackerController.setSegmentInfo(bundle, helper.getOrigins(), helper.getDestinations(),
            helper.getDates(), helper.getDepartureCountries(), helper.getArrivalCountries());
    bundle = mTrackerController.setAirlinesInfo(bundle, helper.getAirlines());
    return bundle;
  }

  private SearchTrackHelper buildSimpleSearchTrack() {
    mSearchTrackHelper.setAdults(1);
    mSearchTrackHelper.setKids(0);
    mSearchTrackHelper.setInfants(1);
    mSearchTrackHelper.addSegment(DEPARTURE, ARRIVAL, DATE_MILLIS, DEPARTURE_COUNTRY,
        ARRIVAL_COUNTRY);
    return mSearchTrackHelper;
  }

  private SearchTrackHelper buildSearchTrackWithPriceAndAirlines() {
    mSearchTrackHelper.setAdults(1);
    mSearchTrackHelper.setKids(0);
    mSearchTrackHelper.setInfants(1);
    mSearchTrackHelper.setPrice(PRICE);
    mSearchTrackHelper.addAirline(AIRLINE);
    mSearchTrackHelper.addSegment(DEPARTURE, ARRIVAL, DATE_MILLIS, DEPARTURE_COUNTRY,
        ARRIVAL_COUNTRY);
    return mSearchTrackHelper;
  }
}