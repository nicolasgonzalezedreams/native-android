package com.odigeo.dataodigeo.tracker;

import com.odigeo.data.tracker.TuneTrackerInterface;
import com.odigeo.dataodigeo.tracker.tune.TuneHelper;
import com.odigeo.dataodigeo.tracker.tune.TuneSegment;
import com.tune.Tune;
import com.tune.TuneEvent;
import com.tune.TuneEventItem;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class TuneTrackerTest {

  private static final String CURRENCY_CODE = "USD";
  private static final int PASSENGERS = 1;
  private static final long DATE = 1456380000000L;
  private static final long RETURN_DATE = 1456552800000L;
  private static final String DEPARTURE = "MAD";
  private static final String ARRIVAL = "BCN";
  private static final double UNIT_PRICE = 123.54;
  private static final double REVENUE_MARKETING = 67.890;
  private static final String BOOKING_ID = "430000001";
  private TuneTracker mTuneTracker;
  @Mock private Tune mTune;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    mTuneTracker = TuneTracker.getInstance();
    mTuneTracker.setTune(mTune);
    TuneHelper.clear();
  }

  @Test public void getInstance_returnTheSameInstance() throws Exception {
    assertEquals(mTuneTracker, TuneTracker.getInstance());
  }

  @Test public void setExistingUser_withTrue_callsSetExistingUserOnce() throws Exception {
    mTuneTracker.setExistingUser(true);
    verify(mTune, times(1)).setExistingUser(true);
  }

  @Test public void setExistingUser_withFalse_doNotCallsSetExistingUser() throws Exception {
    mTuneTracker.setExistingUser(false);
    verify(mTune, never()).setExistingUser(anyBoolean());
  }

  @Test public void trackViewListing_withOneWayAndIsSearchTrue_callsMeasureEventOnce()
      throws Exception {
    loadOneWayData();
    mTuneTracker.trackSearch(true);
    ArgumentCaptor<TuneEvent> argumentCaptor = ArgumentCaptor.forClass(TuneEvent.class);
    verify(mTune, times(1)).measureEvent(argumentCaptor.capture());
    TuneEvent event = argumentCaptor.getValue();
    assertEquals(TuneEvent.SEARCH, event.getEventName());
    assertEquals(PASSENGERS, event.getEventItems().get(0).quantity);
    assertEquals(DATE, event.getDate1().getTime());
    assertEquals(DATE, event.getDate2().getTime());
    validateEventItem(event, false);
  }

  @Test public void trackViewListing_withRoundAndIsSearchTrue_callsMeasureEventOnce()
      throws Exception {

    loadRoundData();
    mTuneTracker.trackSearch(true);
    ArgumentCaptor<TuneEvent> argumentCaptor = ArgumentCaptor.forClass(TuneEvent.class);
    verify(mTune, times(1)).measureEvent(argumentCaptor.capture());
    TuneEvent event = argumentCaptor.getValue();
    assertEquals(TuneEvent.SEARCH, event.getEventName());
    assertEquals(PASSENGERS, event.getEventItems().get(0).quantity);
    assertEquals(DATE, event.getDate1().getTime());
    assertEquals(RETURN_DATE, event.getDate2().getTime());
    validateEventItem(event, false);
  }

  @Test public void trackViewListing_withOneWayAndIsSearchFalse_callsMeasureEventOnce()
      throws Exception {
    loadOneWayData();
    mTuneTracker.trackSearch(false);
    ArgumentCaptor<TuneEvent> argumentCaptor = ArgumentCaptor.forClass(TuneEvent.class);
    verify(mTune, times(1)).measureEvent(argumentCaptor.capture());
    TuneEvent event = argumentCaptor.getValue();
    assertEquals(TuneTracker.DEEPLINK_LAUNCH, event.getEventName());
    assertEquals(PASSENGERS, event.getEventItems().get(0).quantity);
    assertEquals(DATE, event.getDate1().getTime());
    assertEquals(DATE, event.getDate2().getTime());
    validateEventItem(event, false);
  }

  @Test public void trackViewListing_withRoundAndIsSearchFalse_callsMeasureEventOnce()
      throws Exception {

    loadRoundData();
    mTuneTracker.trackSearch(false);
    ArgumentCaptor<TuneEvent> argumentCaptor = ArgumentCaptor.forClass(TuneEvent.class);
    verify(mTune, times(1)).measureEvent(argumentCaptor.capture());
    TuneEvent event = argumentCaptor.getValue();
    assertEquals(TuneTracker.DEEPLINK_LAUNCH, event.getEventName());
    assertEquals(PASSENGERS, event.getEventItems().get(0).quantity);
    assertEquals(DATE, event.getDate1().getTime());
    assertEquals(RETURN_DATE, event.getDate2().getTime());
    validateEventItem(event, false);
  }

  @Test public void trackTransaction_withOneWayAndRevenueTrue_callsMeasureEventTwice()
      throws Exception {

    loadOneWayWithPriceAndBookingId();
    mTuneTracker.trackTransaction();
    ArgumentCaptor<TuneEvent> argumentCaptor = ArgumentCaptor.forClass(TuneEvent.class);
    verify(mTune, times(1)).measureEvent(argumentCaptor.capture());
    List<TuneEvent> events = argumentCaptor.getAllValues();
    validateRevenue(events, true, false);
  }

  @Test public void trackTransaction_withRoundAndRevenueTrue_callsMeasureEventTwice()
      throws Exception {

    loadRoundWithPriceAndBookingId();
    mTuneTracker.trackTransaction();
    ArgumentCaptor<TuneEvent> argumentCaptor = ArgumentCaptor.forClass(TuneEvent.class);
    verify(mTune, times(1)).measureEvent(argumentCaptor.capture());
    List<TuneEvent> events = argumentCaptor.getAllValues();
    validateRevenue(events, true, true);
  }

  @Test public void trackTransaction_withOneWayAndRevenueFalse_callsMeasureEventTwice()
      throws Exception {

    loadOneWayWithPriceAndBookingId();
    mTuneTracker.trackTransaction();
    ArgumentCaptor<TuneEvent> argumentCaptor = ArgumentCaptor.forClass(TuneEvent.class);
    verify(mTune, times(1)).measureEvent(argumentCaptor.capture());
    List<TuneEvent> events = argumentCaptor.getAllValues();
    validateRevenue(events, false, false);
  }

  @Test public void trackTransaction_withRoundAndRevenueFalse_callsMeasureEventTwice()
      throws Exception {

    loadRoundWithPriceAndBookingId();
    mTuneTracker.trackTransaction();
    ArgumentCaptor<TuneEvent> argumentCaptor = ArgumentCaptor.forClass(TuneEvent.class);
    verify(mTune, times(1)).measureEvent(argumentCaptor.capture());
    List<TuneEvent> events = argumentCaptor.getAllValues();
    validateRevenue(events, false, true);
  }

  @Test public void trackViewProduct_withOneWay_callsMeasureEventOnce() throws Exception {

    loadOneWayWithPriceData();
    mTuneTracker.trackViewProduct();
    ArgumentCaptor<TuneEvent> argumentCaptor = ArgumentCaptor.forClass(TuneEvent.class);
    verify(mTune, times(1)).measureEvent(argumentCaptor.capture());
    TuneEvent event = argumentCaptor.getValue();
    assertEquals(TuneTracker.VIEW_PRODUCT, event.getEventName());
    assertEquals(PASSENGERS, event.getEventItems().get(0).quantity);
    assertEquals(DATE, event.getDate1().getTime());
    assertEquals(DATE, event.getDate2().getTime());
    validateEventItem(event, true);
  }

  @Test public void trackViewProduct_withRound_callsMeasureEventOnce() throws Exception {

    loadRoundWithPriceData();
    mTuneTracker.trackViewProduct();
    ArgumentCaptor<TuneEvent> argumentCaptor = ArgumentCaptor.forClass(TuneEvent.class);
    verify(mTune, times(1)).measureEvent(argumentCaptor.capture());
    TuneEvent event = argumentCaptor.getValue();
    assertEquals(TuneTracker.VIEW_PRODUCT, event.getEventName());
    assertEquals(PASSENGERS, event.getEventItems().get(0).quantity);
    assertEquals(DATE, event.getDate1().getTime());
    assertEquals(RETURN_DATE, event.getDate2().getTime());
    validateEventItem(event, true);
  }

  @Test public void trackViewBasket_withOneWay_callsMeasureEventOnce() throws Exception {

    loadOneWayWithPriceData();
    mTuneTracker.trackViewBasket();
    ArgumentCaptor<TuneEvent> argumentCaptor = ArgumentCaptor.forClass(TuneEvent.class);
    verify(mTune, times(1)).measureEvent(argumentCaptor.capture());
    TuneEvent event = argumentCaptor.getValue();
    assertEquals(TuneTracker.VIEW_BASKET, event.getEventName());
    assertEquals(PASSENGERS, event.getEventItems().get(0).quantity);
    assertEquals(DATE, event.getDate1().getTime());
    assertEquals(DATE, event.getDate2().getTime());
    validateEventItem(event, true);
  }

  @Test public void trackViewBasket_withRound_callsMeasureEventOnce() throws Exception {

    loadRoundWithPriceData();
    mTuneTracker.trackViewBasket();
    ArgumentCaptor<TuneEvent> argumentCaptor = ArgumentCaptor.forClass(TuneEvent.class);
    verify(mTune, times(1)).measureEvent(argumentCaptor.capture());
    TuneEvent event = argumentCaptor.getValue();
    assertEquals(TuneTracker.VIEW_BASKET, event.getEventName());
    assertEquals(PASSENGERS, event.getEventItems().get(0).quantity);
    assertEquals(DATE, event.getDate1().getTime());
    assertEquals(RETURN_DATE, event.getDate2().getTime());
    validateEventItem(event, true);
  }

  private void loadOneWayWithPriceAndBookingId() {

    loadOneWayWithPriceData();
    TuneHelper.setBookingId(BOOKING_ID);
  }

  private void loadRoundWithPriceAndBookingId() {

    loadRoundWithPriceData();
    TuneHelper.setBookingId(BOOKING_ID);
    TuneHelper.setRevenueMarketing(REVENUE_MARKETING);
  }

  private void loadOneWayWithPriceData() {

    loadOneWayData();
    TuneHelper.setUnitPrice(UNIT_PRICE);
    TuneHelper.setCurrencyCode(CURRENCY_CODE);
  }

  private void loadRoundWithPriceData() {

    loadRoundData();
    TuneHelper.setUnitPrice(UNIT_PRICE);
    TuneHelper.setCurrencyCode(CURRENCY_CODE);
  }

  private void loadOneWayData() {

    TuneSegment segment = new TuneSegment();
    segment.setDeparture(DEPARTURE);
    segment.setArrival(ARRIVAL);
    segment.setDate(DATE);

    TuneHelper.addSegment(segment);
    TuneHelper.setPassengers(PASSENGERS);
  }

  private void loadRoundData() {

    TuneSegment segment = new TuneSegment();
    segment.setDeparture(DEPARTURE);
    segment.setArrival(ARRIVAL);
    segment.setDate(DATE);
    segment.setReturnDate(RETURN_DATE);

    TuneHelper.addSegment(segment);
    TuneHelper.setPassengers(PASSENGERS);
  }

  private void validateRevenue(List<TuneEvent> events, boolean hasRevenue, boolean isRound) {

    for (int index = 0; index < events.size(); index++) {
      TuneEvent event = events.get(index);
      if (index == 0) {
        assertEquals(TuneTrackerInterface.PURCHASE, event.getEventName());
        assertEquals(PASSENGERS, event.getEventItems().get(0).quantity);
        assertEquals(DATE, event.getDate1().getTime());
        if (isRound) {
          assertEquals(RETURN_DATE, event.getDate2().getTime());
        } else {
          assertEquals(DATE, event.getDate2().getTime());
        }
        validateEventItem(event, true);
      }
      if (hasRevenue) {
        assertEquals(REVENUE_MARKETING, event.getEventItems().get(0).revenue, 0.1);
      }
    }
  }

  private void validateEventItem(TuneEvent event, boolean havePrice) {

    assertNotNull(event.getEventItems());
    assertFalse(event.getEventItems().isEmpty());
    assertEquals(1, event.getEventItems().size());
    TuneEventItem item = event.getEventItems().get(0);
    assertEquals(DEPARTURE + TuneTracker.SEPARATOR + ARRIVAL, item.itemname);
    if (havePrice) {
      assertEquals(UNIT_PRICE, item.unitPrice, 0.1);
    } else {
      assertEquals(0.0, item.unitPrice, 0.1);
    }
  }
}