package com.odigeo.dataodigeo.tracker.searchtracker;

import android.support.annotation.NonNull;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 29/07/16
 */
public class SearchTrackHelperTest {
  private static final String DEPARTURE = "BCN";
  private static final String ARRIVAL = "MAD";
  private static final String DEPARTURE_COUNTRY = "ES";
  private static final String ARRIVAL_COUNTRY = "ES";
  private static final long DATE_MILLIS = 1488002400000L;
  private static final Integer ADULTS = 1;
  private static final Integer KIDS = 0;
  private static final Integer INFANTS = 1;
  private static final long RETURN_DATE_MILLIS = 1488175200000L;
  private static final double PRICE = 123.53;
  private static final String FIRST_AIRLINE = "Japan Airlines";
  private static final String SECOND_AIRLINE = "United";
  private SearchTrackHelper mSearchTrackHelper;

  @Before public void setUp() throws Exception {
    mSearchTrackHelper = new SearchTrackHelper();
  }

  @Test public void clear_searchTrackIsClean() throws Exception {
    getOneWaySimpleSearchTrack();
    mSearchTrackHelper.clear();
    SearchTrack searchTrack = mSearchTrackHelper.getSimpleSearchTrack();
    assertNull(searchTrack.mAdults);
    assertNull(searchTrack.mKids);
    assertNull(searchTrack.mInfants);
    assertNull(searchTrack.mPrice);
    assertNull(searchTrack.mAirlines);
    assertNull(searchTrack.mSegments);
    assertNull(mSearchTrackHelper.getFlightType());
  }

  @Test public void getSimpleSearchTrack_oneWaySearchTrackOk() throws Exception {
    SearchTrack searchTrack = getOneWaySimpleSearchTrack();
    validatePassengers(searchTrack);
    validateSimpleSearchTrack(searchTrack);
    assertEquals(1, searchTrack.mSegments.size());
    validateOneWaySegment(searchTrack);
  }

  @Test public void getSimpleSearchTrack_roundSearchTrackOk() throws Exception {
    SearchTrack searchTrack = getRoundSimpleSearchTrack();
    validatePassengers(searchTrack);
    validateSimpleSearchTrack(searchTrack);
    assertEquals(2, searchTrack.mSegments.size());
    validateRoundSegment(searchTrack);
  }

  @Test public void getSimpleSearchTrack_multiSearchTrackOk() throws Exception {
    SearchTrack searchTrack = getMultiSimpleSearchTrack();
    validatePassengers(searchTrack);
    validateSimpleSearchTrack(searchTrack);
    assertEquals(2, searchTrack.mSegments.size());
    validateMultiSegment(searchTrack);
  }

  @Test public void getSearchTrackWithPriceAndAirlines_oneWaySearchTrackOk() throws Exception {
    SearchTrack searchTrack = getOneWaySearchTrackWithPriceAndAirlines();
    validatePassengers(searchTrack);
    validatePriceAndAirlines(searchTrack);
    assertEquals(1, searchTrack.mSegments.size());
    validateOneWaySegment(searchTrack);
  }

  @Test public void getSearchTrackWithPriceAndAirlines_RoundSearchTrackOk() throws Exception {
    SearchTrack searchTrack = getRoundSearchTrackWithPriceAndAirlines();
    validatePassengers(searchTrack);
    validatePriceAndAirlines(searchTrack);
    assertEquals(2, searchTrack.mSegments.size());
    validateRoundSegment(searchTrack);
  }

  @Test public void getSearchTrackWithPriceAndAirlines_MultiSearchTrackOk() throws Exception {
    SearchTrack searchTrack = getMultiSearchTrackWithPriceAndAirlines();
    validatePassengers(searchTrack);
    validatePriceAndAirlines(searchTrack);
    assertEquals(2, searchTrack.mSegments.size());
    validateMultiSegment(searchTrack);
  }

  private void validateMultiSegment(SearchTrack searchTrack) {
    SegmentTracker firstSegment = searchTrack.mSegments.get(0);
    SegmentTracker secondSegment = searchTrack.mSegments.get(1);
    validateSegmentData(firstSegment, DEPARTURE, ARRIVAL, DATE_MILLIS);
    validateSegmentData(secondSegment, ARRIVAL, DEPARTURE, RETURN_DATE_MILLIS);
  }

  private void validateRoundSegment(SearchTrack searchTrack) {
    SegmentTracker segmentTracker = searchTrack.mSegments.get(0);
    SegmentTracker segmentTrackerReturn = searchTrack.mSegments.get(1);
    validateSegmentData(segmentTracker, DEPARTURE, ARRIVAL, DATE_MILLIS);
    validateSegmentData(segmentTrackerReturn, ARRIVAL, DEPARTURE, RETURN_DATE_MILLIS);
  }

  private void validateOneWaySegment(SearchTrack searchTrack) {
    SegmentTracker segmentTracker = searchTrack.mSegments.get(0);
    validateSegmentData(segmentTracker, DEPARTURE, ARRIVAL, DATE_MILLIS);
  }

  private void validateSegmentData(SegmentTracker segmentTracker, String departure, String arrival,
      long date) {
    assertEquals(departure, segmentTracker.mDeparture);
    assertEquals(arrival, segmentTracker.mArrival);
    assertEquals(mSearchTrackHelper.getDateFormat().format(new Date(date)), segmentTracker.mDate);
  }

  private void validatePriceAndAirlines(SearchTrack searchTrack) {
    assertEquals(PRICE, searchTrack.mPrice);
    assertNotNull(searchTrack.mAirlines);
    assertEquals(2, searchTrack.mAirlines.size());
    assertEquals(FIRST_AIRLINE, searchTrack.mAirlines.get(0));
    assertEquals(SECOND_AIRLINE, searchTrack.mAirlines.get(1));
    assertNotNull(searchTrack.mSegments);
  }

  private void validateSimpleSearchTrack(SearchTrack searchTrack) {
    assertNull(searchTrack.mPrice);
    assertNull(searchTrack.mAirlines);
    assertNotNull(searchTrack.mSegments);
  }

  private void validatePassengers(SearchTrack searchTrack) {
    assertEquals(ADULTS, searchTrack.mAdults);
    assertEquals(KIDS, searchTrack.mKids);
    assertEquals(INFANTS, searchTrack.mInfants);
  }

  @NonNull private SearchTrack getMultiSimpleSearchTrack() {
    setMultiSimpleData();
    return mSearchTrackHelper.getSimpleSearchTrack();
  }

  @NonNull private SearchTrack getRoundSimpleSearchTrack() {
    setRoundSimpleData();
    return mSearchTrackHelper.getSimpleSearchTrack();
  }

  @NonNull private SearchTrack getOneWaySimpleSearchTrack() {
    setOneWaySimpleData();
    return mSearchTrackHelper.getSimpleSearchTrack();
  }

  @NonNull private SearchTrack getMultiSearchTrackWithPriceAndAirlines() {
    setMultiSimpleData();
    setPriceAndAirlines();
    return mSearchTrackHelper.getSearchTrackWithPriceAndAirlines();
  }

  @NonNull private SearchTrack getRoundSearchTrackWithPriceAndAirlines() {
    setRoundSimpleData();
    setPriceAndAirlines();
    return mSearchTrackHelper.getSearchTrackWithPriceAndAirlines();
  }

  @NonNull private SearchTrack getOneWaySearchTrackWithPriceAndAirlines() {
    setOneWaySimpleData();
    setPriceAndAirlines();
    return mSearchTrackHelper.getSearchTrackWithPriceAndAirlines();
  }

  private void setMultiSimpleData() {
    setPassengerData();
    mSearchTrackHelper.addSegment(DEPARTURE, ARRIVAL, DATE_MILLIS, DEPARTURE_COUNTRY,
        ARRIVAL_COUNTRY);
    mSearchTrackHelper.addSegment(ARRIVAL, DEPARTURE, RETURN_DATE_MILLIS, DEPARTURE_COUNTRY,
        ARRIVAL_COUNTRY);
  }

  private void setRoundSimpleData() {
    setPassengerData();
    mSearchTrackHelper.addSegment(DEPARTURE, ARRIVAL, DATE_MILLIS, DEPARTURE_COUNTRY,
        ARRIVAL_COUNTRY);
    mSearchTrackHelper.addSegment(ARRIVAL, DEPARTURE, RETURN_DATE_MILLIS, DEPARTURE_COUNTRY,
        ARRIVAL_COUNTRY);
  }

  private void setOneWaySimpleData() {
    setPassengerData();
    mSearchTrackHelper.addSegment(DEPARTURE, ARRIVAL, DATE_MILLIS, DEPARTURE_COUNTRY,
        ARRIVAL_COUNTRY);
  }

  private void setPriceAndAirlines() {
    mSearchTrackHelper.setPrice(PRICE);
    mSearchTrackHelper.addAirline(FIRST_AIRLINE);
    mSearchTrackHelper.addAirline(SECOND_AIRLINE);
  }

  private void setPassengerData() {
    mSearchTrackHelper.setAdults(ADULTS);
    mSearchTrackHelper.setKids(KIDS);
    mSearchTrackHelper.setInfants(INFANTS);
  }
}