package com.odigeo.dataodigeo.tracker.tune;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;

public class TuneHelperTest {
  private static final int PASSENGERS = 1;
  private static final long DATE = 1456380000000L;
  private static final long RETURN_DATE = 1456552800000L;
  private static final String DEPARTURE = "MAD";
  private static final String ARRIVAL = "ARRIVAL";
  private static final double UNIT_PRICE = 123.45;
  private static final double REVENUE_MARKETING = 67.890;
  private static final String CURRENCY_CODE = "USD";
  private static final String BOOKING_ID = "430000001";

  @Before public void setUp() throws Exception {
    TuneHelper.clear();
    loadData();
  }

  @Test public void clear_tuneWrapperAttributesShouldBeNull() throws Exception {
    TuneHelper.clear();
    TuneWrapper wrapper = TuneHelper.getTuneWrapper();
    assertNotNull(wrapper);
    assertNotNull(wrapper.getSegments());
    assertTrue(wrapper.getSegments().isEmpty());
    assertEquals(0, wrapper.getPassengers());
    assertEquals(0.0, wrapper.getUnitPrice());
    assertNull(wrapper.getCurrencyCode());
    assertNull(wrapper.getBookingId());
  }

  @Test public void getTuneWrapper_tuneWrapperAttributesShouldBeOk() throws Exception {
    TuneWrapper wrapper = TuneHelper.getTuneWrapper();
    assertNotNull(wrapper);
    assertNotNull(wrapper.getSegments());
    assertFalse(wrapper.getSegments().isEmpty());
    assertEquals(1, wrapper.getSegments().size());
    TuneSegment segment = wrapper.getSegments().get(0);
    assertEquals(DEPARTURE, segment.getDeparture());
    assertEquals(ARRIVAL, segment.getArrival());
    assertEquals(DATE, segment.getDate());
    assertEquals(RETURN_DATE, segment.getReturnDate());
    assertEquals(PASSENGERS, wrapper.getPassengers());
    assertEquals(UNIT_PRICE, wrapper.getUnitPrice());
    assertEquals(REVENUE_MARKETING, wrapper.getRevenueMarketing());
    assertEquals(CURRENCY_CODE, wrapper.getCurrencyCode());
    assertEquals(BOOKING_ID, wrapper.getBookingId());
  }

  private void loadData() {
    TuneSegment segment = new TuneSegment();
    segment.setDeparture(DEPARTURE);
    segment.setArrival(ARRIVAL);
    segment.setDate(DATE);
    segment.setReturnDate(RETURN_DATE);

    TuneHelper.addSegment(segment);
    TuneHelper.setPassengers(PASSENGERS);
    TuneHelper.setUnitPrice(UNIT_PRICE);
    TuneHelper.setRevenueMarketing(REVENUE_MARKETING);
    TuneHelper.setCurrencyCode(CURRENCY_CODE);
    TuneHelper.setBookingId(BOOKING_ID);
  }
}