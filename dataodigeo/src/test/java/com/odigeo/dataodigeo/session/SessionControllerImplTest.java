package com.odigeo.dataodigeo.session;

import android.content.Context;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.preferences.PreferencesController;
import com.odigeo.interactors.provider.MarketProviderInterface;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(RobolectricTestRunner.class) public class SessionControllerImplTest {

  private static final String USER = "javier@odigeo.com";
  private static final String PASS = "pass123";
  private SessionControllerImpl mSessionController;
  private Credentials mCredentials;

  @Mock private DomainHelperInterface domainHelper;
  @Mock private MarketProviderInterface marketProvider;

  @Before public void setUp() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    mSessionController = new SessionControllerImpl(context,
        new PreferencesController(context, domainHelper, marketProvider));
    mSessionController.getUserDbDao().disablePopulate();
    mCredentials = new Credentials(USER, PASS, CredentialsInterface.CredentialsType.PASSWORD);
  }

  @Test public void getNonExistentCredentials() {
    mSessionController.removeAllData();
    Credentials actualCredentials = mSessionController.getCredentials();
    assertNull(actualCredentials);
  }

  @Test public void getExistentCredentials() {
    mSessionController.savePasswordCredentials(mCredentials.getUser(), mCredentials.getPassword(),
        mCredentials.getType());

    Credentials actualCredentials = mSessionController.getCredentials();

    assertEquals(mCredentials.getUser(), actualCredentials.getUser());
    assertEquals(mCredentials.getPassword(), actualCredentials.getPassword());
    assertEquals(mCredentials.getType(), actualCredentials.getType());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
