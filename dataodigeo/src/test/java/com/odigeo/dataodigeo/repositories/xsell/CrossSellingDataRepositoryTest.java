package com.odigeo.dataodigeo.repositories.xsell;

import com.odigeo.data.db.dao.GuideDBDAOInterface;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.data.entity.booking.Guide;
import com.odigeo.data.entity.xsell.CrossSelling;
import com.odigeo.data.localizable.LocalizableProvider;
import com.odigeo.data.repositories.CrossSellingRepository;
import java.util.List;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.odigeo.dataodigeo.repositories.xsell.CrossSellingDataRepository.MY_TRIPS_DETAILS_XSELLCARD_CARS_LABEL;
import static com.odigeo.dataodigeo.repositories.xsell.CrossSellingDataRepository.MY_TRIPS_DETAILS_XSELLCARD_CARS_SUBTITLE;
import static com.odigeo.dataodigeo.repositories.xsell.CrossSellingDataRepository.MY_TRIPS_DETAILS_XSELLCARD_CARS_TITLE;
import static com.odigeo.dataodigeo.repositories.xsell.CrossSellingDataRepository.MY_TRIPS_DETAILS_XSELLCARD_GT_LABEL;
import static com.odigeo.dataodigeo.repositories.xsell.CrossSellingDataRepository.MY_TRIPS_DETAILS_XSELLCARD_GT_SUBTITLE;
import static com.odigeo.dataodigeo.repositories.xsell.CrossSellingDataRepository.MY_TRIPS_DETAILS_XSELLCARD_GT_TITLE;
import static com.odigeo.dataodigeo.repositories.xsell.CrossSellingDataRepository.MY_TRIPS_DETAILS_XSELLCARD_GUIDE_LABEL;
import static com.odigeo.dataodigeo.repositories.xsell.CrossSellingDataRepository.MY_TRIPS_DETAILS_XSELLCARD_GUIDE_SUBTITLE;
import static com.odigeo.dataodigeo.repositories.xsell.CrossSellingDataRepository.MY_TRIPS_DETAILS_XSELLCARD_GUIDE_TITLE;
import static com.odigeo.dataodigeo.repositories.xsell.CrossSellingDataRepository.MY_TRIPS_DETAILS_XSELLCARD_HOTEL_LABEL;
import static com.odigeo.dataodigeo.repositories.xsell.CrossSellingDataRepository.MY_TRIPS_DETAILS_XSELLCARD_HOTEL_SUBTITLE;
import static com.odigeo.dataodigeo.repositories.xsell.CrossSellingDataRepository.MY_TRIPS_DETAILS_XSELLCARD_HOTEL_TITLE;
import static com.odigeo.dataodigeo.repositories.xsell.CrossSellingDataRepository.TRIP_DETAILS_GROUND_TRANSPORTATION_ACTIVE;
import static com.odigeo.presenter.model.CrossSellingType.TYPE_CARS;
import static com.odigeo.presenter.model.CrossSellingType.TYPE_GROUND;
import static com.odigeo.presenter.model.CrossSellingType.TYPE_GUIDE;
import static com.odigeo.presenter.model.CrossSellingType.TYPE_HOTEL;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class CrossSellingDataRepositoryTest {

  @Mock private LocalizableProvider localizableProvider;
  @Mock private GuideDBDAOInterface guideDao;
  @Mock private Booking booking;

  private CrossSellingRepository crossSellingRepository;

  @Before public void setup() {
    crossSellingRepository = new CrossSellingDataRepository(localizableProvider, guideDao);
  }

  @Test public void testGetCrossSellingForBookingUsesRightCMSKeys() {
    crossSellingRepository.getCrossSellingForBooking(booking);

    verify(localizableProvider).getString(MY_TRIPS_DETAILS_XSELLCARD_HOTEL_TITLE);
    verify(localizableProvider).getString(MY_TRIPS_DETAILS_XSELLCARD_HOTEL_LABEL);
    verify(localizableProvider).getString(MY_TRIPS_DETAILS_XSELLCARD_HOTEL_SUBTITLE);
    verify(localizableProvider).getString(MY_TRIPS_DETAILS_XSELLCARD_CARS_TITLE);
    verify(localizableProvider).getString(MY_TRIPS_DETAILS_XSELLCARD_CARS_LABEL);
    verify(localizableProvider).getString(MY_TRIPS_DETAILS_XSELLCARD_CARS_SUBTITLE);
    verify(localizableProvider).getString(MY_TRIPS_DETAILS_XSELLCARD_GT_TITLE);
    verify(localizableProvider).getString(MY_TRIPS_DETAILS_XSELLCARD_GT_LABEL);
    verify(localizableProvider).getString(MY_TRIPS_DETAILS_XSELLCARD_GT_SUBTITLE);
    verify(localizableProvider).getString(MY_TRIPS_DETAILS_XSELLCARD_GUIDE_TITLE);
    verify(localizableProvider).getString(MY_TRIPS_DETAILS_XSELLCARD_GUIDE_LABEL);
    verify(localizableProvider).getString(MY_TRIPS_DETAILS_XSELLCARD_GUIDE_SUBTITLE);

    verifyNoMoreInteractions(localizableProvider);
  }

  @Test public void testGetCrossSellingArrivalGuideCallsDao() {
    when(guideDao.getGuideByGeoNodeId(booking.getArrivalGeoNodeId())).thenReturn(new Guide());

    final boolean hasArrivalGuide = crossSellingRepository.getCrossSellingArrivalGuide(booking);

    assertThat(hasArrivalGuide, is(true));
  }

  @Test public void testShouldShowGroundTransportationUsesCMSKey() {
    when(localizableProvider.getString(TRIP_DETAILS_GROUND_TRANSPORTATION_ACTIVE)).thenReturn(
        "true");

    boolean shouldShowGroundTransportation =
        crossSellingRepository.shouldShowGroundTransportation();
    assertThat(shouldShowGroundTransportation, is(true));

    when(localizableProvider.getString(TRIP_DETAILS_GROUND_TRANSPORTATION_ACTIVE)).thenReturn(
        "false");

    shouldShowGroundTransportation = crossSellingRepository.shouldShowGroundTransportation();
    assertThat(shouldShowGroundTransportation, is(false));
  }

  @Test public void testGetCrossSellingForBookingRightOrder() {

    List<CrossSelling> crossSellings = crossSellingRepository.getCrossSellingForBooking(booking);

    assertThat(crossSellings,
        IsIterableContainingInOrder.contains(new CrossSellingItemTypeMatcher(TYPE_HOTEL),
            new CrossSellingItemTypeMatcher(TYPE_CARS),
            new CrossSellingItemTypeMatcher(TYPE_GROUND),
            new CrossSellingItemTypeMatcher(TYPE_GUIDE)));
  }

  private static class CrossSellingItemTypeMatcher extends BaseMatcher<CrossSelling> {

    private final int type;

    CrossSellingItemTypeMatcher(int crossSellingType) {
      this.type = crossSellingType;
    }

    @Override public boolean matches(Object item) {
      return item instanceof CrossSelling && ((CrossSelling) item).getType() == type;
    }

    @Override public void describeTo(Description description) {
      description.appendText("CrossSelling type should match " + type);
    }
  }
}
