package com.odigeo.dataodigeo.firebase.remoteconfig;

import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class) @Config(manifest = Config.NONE)
public class FirebaseRemoteConfigControllerTest {

  private static final String experimentName = "Experiment";
  private static final String variantName = "variantName";
  private static final String variantPercentile = "variantPercentile";

  private FirebaseRemoteConfigController firebaseRemoteConfigController;

  @Mock private FirebaseRemoteConfig firebaseRemoteConfig;
  @Mock private FirebaseAnalyticsController firebaseAnalyticsController;

  private Task<Void> successfulTask = FirebaseMockProvider.provideSuccessfulTask();
  private Task<Void> failingTask = FirebaseMockProvider.priovideFailingTask();
  private FirebaseRemoteConfigInfo firebaseRemoteConfigInfo =
      FirebaseMockProvider.provideRemoteConfigInfo();

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    when(firebaseRemoteConfig.getInfo()).thenReturn(firebaseRemoteConfigInfo);
    firebaseRemoteConfigController =
        new FirebaseRemoteConfigController(firebaseRemoteConfig, firebaseAnalyticsController);
  }

  @Test public void initExperimentSuccessful() {
    when(firebaseRemoteConfig.fetch(anyLong())).thenReturn(successfulTask);
    when(firebaseRemoteConfig.getString(variantName)).thenReturn(variantPercentile);
    firebaseRemoteConfigController.fetchConfig();
    firebaseRemoteConfigController.initExperiment(experimentName, variantName);

    verify(firebaseRemoteConfig).activateFetched();
    verify(firebaseRemoteConfig).getString(variantName);
    verify(firebaseAnalyticsController).setUserProperty(experimentName, variantPercentile);
  }

  @Test public void initExperimentFailing() {
    when(firebaseRemoteConfig.fetch(anyLong())).thenReturn(failingTask);
    firebaseRemoteConfigController.fetchConfig();
    firebaseRemoteConfigController.initExperiment(experimentName, variantName);

    verify(firebaseRemoteConfig, times(0)).activateFetched();
    verify(firebaseAnalyticsController, times(0)).setUserProperty(experimentName,
        variantPercentile);
  }
}
