package com.odigeo.dataodigeo.firebase.remoteconfig;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigInfo;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import java.util.concurrent.Executor;

public class FirebaseMockProvider {

  public static Task<Void> provideSuccessfulTask() {
    return new Task<Void>() {
      @Override public boolean isComplete() {
        return true;
      }

      @Override public boolean isSuccessful() {
        return true;
      }

      @Override public Void getResult() {
        return null;
      }

      @Override public <X extends Throwable> Void getResult(@NonNull Class<X> aClass) throws X {
        return null;
      }

      @Nullable @Override public Exception getException() {
        return null;
      }

      @NonNull @Override public Task<Void> addOnSuccessListener(
          @NonNull OnSuccessListener<? super Void> onSuccessListener) {
        return null;
      }

      @NonNull @Override public Task<Void> addOnSuccessListener(@NonNull Executor executor,
          @NonNull OnSuccessListener<? super Void> onSuccessListener) {
        return null;
      }

      @NonNull @Override public Task<Void> addOnSuccessListener(@NonNull Activity activity,
          @NonNull OnSuccessListener<? super Void> onSuccessListener) {
        return null;
      }

      @NonNull @Override
      public Task<Void> addOnFailureListener(@NonNull OnFailureListener onFailureListener) {
        return null;
      }

      @NonNull @Override public Task<Void> addOnFailureListener(@NonNull Executor executor,
          @NonNull OnFailureListener onFailureListener) {
        return null;
      }

      @NonNull @Override public Task<Void> addOnFailureListener(@NonNull Activity activity,
          @NonNull OnFailureListener onFailureListener) {
        return null;
      }

      @NonNull @Override public Task<Void> addOnCompleteListener(
          @NonNull OnCompleteListener<Void> onCompleteListener) {
        if (isComplete() && onCompleteListener != null) {
          onCompleteListener.onComplete(this);
        }

        return this;
      }
    };
  }

  public static FirebaseRemoteConfigInfo provideRemoteConfigInfo() {
    return new FirebaseRemoteConfigInfo() {
      @Override public long getFetchTimeMillis() {
        return 0;
      }

      @Override public int getLastFetchStatus() {
        return 0;
      }

      @Override public FirebaseRemoteConfigSettings getConfigSettings() {
        return new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(false).build();
      }
    };
  }

  public static Task<Void> priovideFailingTask() {
    return new Task<Void>() {
      @Override public boolean isComplete() {
        return true;
      }

      @Override public boolean isSuccessful() {
        return false;
      }

      @Override public Void getResult() {
        return null;
      }

      @Override public <X extends Throwable> Void getResult(@NonNull Class<X> aClass) throws X {
        return null;
      }

      @Nullable @Override public Exception getException() {
        return null;
      }

      @NonNull @Override public Task<Void> addOnSuccessListener(
          @NonNull OnSuccessListener<? super Void> onSuccessListener) {
        return null;
      }

      @NonNull @Override public Task<Void> addOnSuccessListener(@NonNull Executor executor,
          @NonNull OnSuccessListener<? super Void> onSuccessListener) {
        return null;
      }

      @NonNull @Override public Task<Void> addOnSuccessListener(@NonNull Activity activity,
          @NonNull OnSuccessListener<? super Void> onSuccessListener) {
        return null;
      }

      @NonNull @Override
      public Task<Void> addOnFailureListener(@NonNull OnFailureListener onFailureListener) {
        return null;
      }

      @NonNull @Override public Task<Void> addOnFailureListener(@NonNull Executor executor,
          @NonNull OnFailureListener onFailureListener) {
        return null;
      }

      @NonNull @Override public Task<Void> addOnFailureListener(@NonNull Activity activity,
          @NonNull OnFailureListener onFailureListener) {
        return null;
      }

      @NonNull @Override public Task<Void> addOnCompleteListener(
          @NonNull OnCompleteListener<Void> onCompleteListener) {
        onCompleteListener.onComplete(this);
        return this;
      }
    };
  }
}
