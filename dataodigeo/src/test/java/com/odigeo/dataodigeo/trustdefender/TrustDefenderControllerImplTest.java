package com.odigeo.dataodigeo.trustdefender;

import android.content.Context;
import android.content.res.Resources;
import com.odigeo.data.tracker.CrashlyticsController;
import com.threatmetrix.TrustDefender.ProfilingOptions;
import com.threatmetrix.TrustDefender.ProfilingResult;
import com.threatmetrix.TrustDefender.THMStatusCode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class) @Config(manifest = Config.NONE)
public class TrustDefenderControllerImplTest {

  private static final String ORG_ID = "k6dvnkdk";
  private static final String CYBER_SOURCE_MERCHANT_ID = "eDreams";
  private static final String JSESSION_ID = "123456ABCD.bcn-141";

  @Mock Context context;
  @Mock Resources resources;
  @Mock private CrashlyticsController crashlyticsController;
  @Mock private TrustDefenderHelper trustDefenderHelper;
  @Mock ProfilingResult profilingResult;
  @Captor private ArgumentCaptor<TrustDefenderListener> trustDefenderListener;

  private TrustDefenderControllerImpl trustDefenderController;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    when(context.getResources()).thenReturn(resources);
    when(resources.getString(anyInt())).thenReturn(ORG_ID);

    trustDefenderController =
        new TrustDefenderControllerImpl(context, crashlyticsController, trustDefenderHelper, true);
  }

  @Test public void shouldCreateFingerprint() {
    when(trustDefenderHelper.doProfileRequest(any(ProfilingOptions.class))).thenReturn(
        THMStatusCode.THM_OK);
    trustDefenderController.sendFingerPrint(CYBER_SOURCE_MERCHANT_ID, JSESSION_ID);
    verify(trustDefenderHelper).init(any(com.threatmetrix.TrustDefender.Config.class),
        trustDefenderListener.capture());
    trustDefenderListener.getValue().onInit(THMStatusCode.THM_OK);
    verify(trustDefenderHelper).getSessionID();
  }

  @Test public void shouldNotCreateFingerprint() {
    when(trustDefenderHelper.doProfileRequest(any(ProfilingOptions.class))).thenReturn(
        THMStatusCode.THM_OK);
    trustDefenderController.sendFingerPrint(CYBER_SOURCE_MERCHANT_ID, JSESSION_ID);
    verify(trustDefenderHelper).init(any(com.threatmetrix.TrustDefender.Config.class),
        trustDefenderListener.capture());
    trustDefenderListener.getValue().onInit(THMStatusCode.THM_Connection_Error);
    verify(trustDefenderHelper, never()).getSessionID();
  }
}
