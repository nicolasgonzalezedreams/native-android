package com.odigeo.data.net.volley;

import android.content.Context;
import com.odigeo.dataodigeo.net.helper.CustomRequestQueue;
import com.odigeo.dataodigeo.net.helper.DomainHelper;
import com.odigeo.dataodigeo.net.helper.HeaderHelper;
import com.odigeo.dataodigeo.net.helper.RequestHelper;

public class VolleyDependenciesProvider {

  public static final String CONTENT_TYPE = "Content-Type";
  public static final String DEVICE_ID = "Device-ID";
  public static final String ACCEPT_ENCODING = "Accept-Encoding";
  public static final String ACCEPT = "Accept";
  private static final String URL = "https://msl.odigeo.com/mobile-api/msl/";

  public static RequestHelper provideRequestHelper(Context context) {
    return new RequestHelper(provideRequestQueue(context));
  }

  private static CustomRequestQueue provideRequestQueue(Context context) {
    CustomRequestQueue mRequestQueue = CustomRequestQueue.createCustomRequestQueue(context);
    mRequestQueue.start();
    return mRequestQueue;
  }

  public static HeaderHelper provideHeaderHelper(Context context) {
    return HeaderHelper.newInstance(context);
  }

  public static DomainHelper provideDomainHelper() {
    return DomainHelper.newInstance(URL);
  }
}
