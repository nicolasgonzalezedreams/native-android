package com.odigeo.data.net.controllers;

import com.android.volley.AuthFailureError;
import com.google.gson.Gson;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartRequestModel;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.ItinerarySortCriteria;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.net.volley.VolleyDependenciesProvider;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.controllers.ShoppingCartNetController;
import com.odigeo.dataodigeo.net.helper.DomainHelper;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import com.odigeo.test.mock.MocksProvider;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class) public class ShoppingCartNetControllerTest {

  @Mock private RequestHelper requestHelper;
  @Mock private HeaderHelperInterface headerHelperInterface;
  @Mock private OnRequestDataListener<CreateShoppingCartResponse> listener;

  private ShoppingCartNetController shoppingCartNetController;
  private DomainHelper domainHelper;
  private CreateShoppingCartRequestModel createShoppingCartRequestModel;
  private Gson gson;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    domainHelper = VolleyDependenciesProvider.provideDomainHelper();
    gson = new Gson();

    shoppingCartNetController =
        new ShoppingCartNetController(requestHelper, domainHelper, headerHelperInterface, gson);
  }

  @Test public void getShoppingCartRequestCheckSortCriteriaNotMemberTest() {
    createShoppingCartRequestModel = MocksProvider.provideCreateShoppingCartRequestModelMocks()
        .provideCreateShoppingCartRequestModelMock(false);

    shoppingCartNetController.getShoppingCart(createShoppingCartRequestModel, listener);

    ArgumentCaptor<MslRequest> captor = ArgumentCaptor.forClass(MslRequest.class);
    Mockito.verify(requestHelper).addRequest(captor.capture());
    MslRequest request = captor.getValue();

    try {
      JSONObject body = new JSONObject(new String(request.getBody()));
      assertNotNull(body);
      JSONObject itinerarySelectionRequest =
          body.getJSONObject(JsonKeys.ITINERARY_SELECTION_REQUEST);
      assertEquals(ItinerarySortCriteria.MINIMUM_PURCHASABLE_PRICE.value(),
          itinerarySelectionRequest.getString(JsonKeys.SORT_CRITERIA));
    } catch (AuthFailureError authFailureError) {
      authFailureError.printStackTrace();
    } catch (JSONException jsonException) {
      jsonException.printStackTrace();
    }
  }

  @Test public void getShoppingCartRequestCheckSortCriteriaMemberUserTest() {
    createShoppingCartRequestModel = MocksProvider.provideCreateShoppingCartRequestModelMocks()
        .provideCreateShoppingCartRequestModelMock(true);

    shoppingCartNetController.getShoppingCart(createShoppingCartRequestModel, listener);

    ArgumentCaptor<MslRequest> captor = ArgumentCaptor.forClass(MslRequest.class);
    Mockito.verify(requestHelper).addRequest(captor.capture());
    MslRequest request = captor.getValue();

    try {
      JSONObject body = new JSONObject(new String(request.getBody()));
      assertNotNull(body);
      JSONObject itinerarySelectionRequest =
          body.getJSONObject(JsonKeys.ITINERARY_SELECTION_REQUEST);
      assertEquals(ItinerarySortCriteria.MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE.value(),
          itinerarySelectionRequest.getString(JsonKeys.SORT_CRITERIA));
    } catch (AuthFailureError authFailureError) {
      authFailureError.printStackTrace();
    } catch (JSONException jsonException) {
      jsonException.printStackTrace();
    }
  }
}
