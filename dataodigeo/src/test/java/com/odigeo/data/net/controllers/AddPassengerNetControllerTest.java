package com.odigeo.data.net.controllers;

import android.content.Context;
import com.google.gson.GsonBuilder;
import com.odigeo.data.entity.shoppingCart.CreateShoppingCartResponse;
import com.odigeo.data.entity.shoppingCart.request.PersonalInfoRequest;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.net.volley.VolleyDependenciesProvider;
import com.odigeo.dataodigeo.net.controllers.AddPassengerNetController;
import com.odigeo.dataodigeo.net.helper.DomainHelper;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class) public class AddPassengerNetControllerTest {

  @Mock RequestHelper mRequestHelper;
  DomainHelper mDomainHelper;
  @Mock private OnRequestDataListener<CreateShoppingCartResponse> mOnRequestDataListenerInteractor;
  @Captor private ArgumentCaptor<OnRequestDataListener<String>> mOnRequestDataListenerData;
  private PersonalInfoRequest mPersonalInfoRequest;
  private AddPassengerNetControllerInterface mAddPassengerNetControllerInterface;
  private Context mApplicationContext;
  private HeaderHelperInterface mHeaderHelperInterface;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    mApplicationContext = RuntimeEnvironment.application.getApplicationContext();
    mHeaderHelperInterface = VolleyDependenciesProvider.provideHeaderHelper(mApplicationContext);
    mDomainHelper = VolleyDependenciesProvider.provideDomainHelper();

    mAddPassengerNetControllerInterface =
        new AddPassengerNetController(mRequestHelper, mDomainHelper, mHeaderHelperInterface,
            new GsonBuilder().serializeNulls().create());

    mAddPassengerNetControllerInterface.addPassengerToShoppingCart(mOnRequestDataListenerInteractor,
        mPersonalInfoRequest);
  }

  @Test public void shouldAddHeaders() {
    HashMap<String, String> mMapHeaders;
    String headerElement;

    mMapHeaders = mAddPassengerNetControllerInterface.getMapHeaders();

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.ACCEPT_ENCODING);
    assertEquals(headerElement, "gzip,deflate,sdch");

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.ACCEPT);
    assertEquals(headerElement, "application/vnd.com.odigeo.msl.v3+json;charset=utf-8");

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.DEVICE_ID);
    assertNotNull(headerElement);

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.CONTENT_TYPE);
    assertNull(headerElement);
  }

  @Test public void shouldCallAddRequest() {
    verify(mRequestHelper).addRequest(any(MslRequest.class));
  }
}