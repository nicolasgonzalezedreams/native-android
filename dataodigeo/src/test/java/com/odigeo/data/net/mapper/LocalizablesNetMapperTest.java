package com.odigeo.data.net.mapper;

import com.odigeo.data.entity.localizables.LocalizablesResponse;
import com.odigeo.dataodigeo.net.mapper.LocalizablesNetMapper;
import junit.framework.Assert;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class) public class LocalizablesNetMapperTest {

  @Rule public final ExpectedException exception = ExpectedException.none();
  public static final String mockJson = "{\"key1\":\"value1\", \"key2\":\"value2\"}";

  @Test public void testParseValidJson() throws JSONException {
    JSONObject jsonObject = new JSONObject(mockJson);
    LocalizablesResponse localizablesResponse =
        LocalizablesNetMapper.parseLocalizablesJSONToResponse(jsonObject);

    Assert.assertEquals(localizablesResponse.getLocalizables().size(), 2);
    Assert.assertEquals(localizablesResponse.getLocalizables().get(0).getLocalizableKey(), "key1");
    Assert.assertEquals(localizablesResponse.getLocalizables().get(0).getLocalizableValue(),
        "value1");
    Assert.assertEquals(localizablesResponse.getLocalizables().get(1).getLocalizableKey(), "key2");
    Assert.assertEquals(localizablesResponse.getLocalizables().get(1).getLocalizableValue(),
        "value2");
  }
}
