package com.odigeo.data.net.mapper;

import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.StoredSearchesNetMapper;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static com.odigeo.test.mock.MocksProvider.providesStoredSearchMocks;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Javier Marsicano on 16/08/16.
 */
@RunWith(RobolectricTestRunner.class) @Config(manifest = Config.NONE)
public class StoredSearchesNetMapperTest {

  private static final int STORED_SEARCH_ID1 = 5721;
  private static final int STORED_SEARCH_ID2 = 5642;

  private static final String SEARCHES_JSON_OK = "{\"searchList\": [\n"
      + "    {\n"
      + "      \"id\":"
      + STORED_SEARCH_ID1
      + ",\n"
      + "      \"numAdults\": 1,\n"
      + "      \"numChildren\": 0,\n"
      + "      \"numInfants\": 0,\n"
      + "      \"directFlight\": true,\n"
      + "      \"cabinClass\": null,\n"
      + "      \"tripType\": \"M\",\n"
      + "      \"searchSegmentList\": [\n"
      + "        {\n"
      + "          \"id\": 8815,\n"
      + "          \"origin\": \"TYO\",\n"
      + "          \"departureDate\": 1471219200000,\n"
      + "          \"destination\": \"SEA\",\n"
      + "          \"returnDate\": 0,\n"
      + "          \"segmentOrder\": 0\n"
      + "        },\n"
      + "        {\n"
      + "          \"id\": 8816,\n"
      + "          \"origin\": \"SEA\",\n"
      + "          \"departureDate\": 1471910400000,\n"
      + "          \"destination\": \"AMS\",\n"
      + "          \"returnDate\": 0,\n"
      + "          \"segmentOrder\": 1\n"
      + "        }\n"
      + "      ],\n"
      + "      \"creationDate\": 1470857400000\n"
      + "    },\n"
      + "    {\n"
      + "      \"id\":"
      + STORED_SEARCH_ID2
      + ",\n"
      + "      \"numAdults\": 1,\n"
      + "      \"numChildren\": 0,\n"
      + "      \"numInfants\": 0,\n"
      + "      \"directFlight\": false,\n"
      + "      \"cabinClass\": \"TOURIST\",\n"
      + "      \"tripType\": \"O\",\n"
      + "      \"searchSegmentList\": [\n"
      + "        {\n"
      + "          \"id\": 8736,\n"
      + "          \"origin\": \"BUE\",\n"
      + "          \"departureDate\": 1495907200000,\n"
      + "          \"destination\": \"PAR\",\n"
      + "          \"returnDate\": 1496252800000,\n"
      + "          \"segmentOrder\": 0\n"
      + "        }\n"
      + "      ]\n"
      + "    }]}";

  private StoredSearchesNetMapper mStoredSearchesNetMapper;
  private JSONObject mSearchesJsonObject;
  private StoredSearch mStoredSearch;

  @Before public void setUp() throws JSONException {
    mStoredSearchesNetMapper = new StoredSearchesNetMapper();

    mStoredSearch = providesStoredSearchMocks().provideStoredSearchWithSegment();
    mSearchesJsonObject = new JSONObject(SEARCHES_JSON_OK);
  }

  @Test public void jsonArrayToStoredSearchesListTest() throws JSONException {
    List<StoredSearch> storedSearchList =
        mStoredSearchesNetMapper.mapperJsonArrayToStoredSearchesList(
            mSearchesJsonObject.getJSONArray("searchList"), 0);
    assertNotNull(storedSearchList);
    assertEquals(2, storedSearchList.size());
    assertEquals(STORED_SEARCH_ID1, storedSearchList.get(0).getStoredSearchId());
    assertEquals(STORED_SEARCH_ID2, storedSearchList.get(1).getStoredSearchId());
    assertEquals(StoredSearch.TripType.M, storedSearchList.get(0).getTripType());
    assertEquals(StoredSearch.TripType.O, storedSearchList.get(1).getTripType());
    assertEquals(2, storedSearchList.get(0).getSegmentList().size());
    assertEquals(1, storedSearchList.get(1).getSegmentList().size());
  }

  @Test public void jsonObjectToStoredSearchTest() throws JSONException {
    StoredSearch storedSearch = mStoredSearchesNetMapper.mapperJsonObjectToStoredSearch(
        mSearchesJsonObject.getJSONArray("searchList").getJSONObject(0), 0);
    assertNotNull(storedSearch);
    assertEquals(STORED_SEARCH_ID1, storedSearch.getStoredSearchId());
    assertEquals(StoredSearch.CabinClass.UNKNOWN, storedSearch.getCabinClass());
    assertEquals(StoredSearch.TripType.M, storedSearch.getTripType());
    assertEquals(2, storedSearch.getSegmentList().size());
  }

  @Test public void storedSearchToJsonObjectTest() throws JSONException {
    JSONObject mappedStoredSearchJsonObject =
        mStoredSearchesNetMapper.mapperStoredSearchToJsonObject(mStoredSearch);
    assertNotNull(mappedStoredSearchJsonObject);
    validateJsonObject(mappedStoredSearchJsonObject);
  }

  @Test public void storedSearchesListToJsonTest() throws JSONException {
    List<StoredSearch> storedSearchList = new ArrayList<>();
    storedSearchList.add(mStoredSearch);
    JSONArray storedSearchListJson =
        mStoredSearchesNetMapper.mapperStoredSearchesListToJson(storedSearchList);
    assertNotNull(storedSearchListJson);
    assertEquals(storedSearchListJson.length(), 1);
    JSONObject jsonObject = storedSearchListJson.getJSONObject(0);
    validateJsonObject(jsonObject);
  }

  private void validateJsonObject(JSONObject jsonObject) throws JSONException {
    assertNotNull(jsonObject);
    assertEquals(mStoredSearch.getStoredSearchId(), jsonObject.getInt("id"));
    assertEquals(mStoredSearch.getCabinClass().name(), jsonObject.getString(JsonKeys.CABIN_CLASS));
    assertEquals(mStoredSearch.getTripType().name(), jsonObject.getString(JsonKeys.TRIP_TYPE));
    assertEquals(mStoredSearch.getSegmentList().size(),
        jsonObject.getJSONArray(JsonKeys.SEARCH_SEGMENT_LIST).length());
  }
}
