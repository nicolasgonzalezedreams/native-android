package com.odigeo.data.net.mapper;

import com.odigeo.data.entity.net.NetCountry;
import com.odigeo.dataodigeo.net.mapper.NetCountryMapper;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 12/02/16.
 */
@RunWith(RobolectricTestRunner.class) public class NetCountryMapperTest {

  private static final String JSON_INVALID = "{";
  private static final String JSON_OK = "{"
      + "\"countries\": ["
      + "{"
      + "\"name\": {"
      + "\"texts\": {"
      + "\"de\": \"Afghanistan\","
      + "\"it\": \"Afganistan\","
      + "\"el\": \"Αφγανιστάν\","
      + "\"tr\": \"Afganistan\","
      + "\"pt\": \"Afganistán, Estado Islámico de\","
      + "\"fr\": \"Afghanistan\","
      + "\"en\": \"Afghanistan\","
      + "\"ru\": \"Афганистан\","
      + "\"es\": \"Afganistán\","
      + "\"nl\": \"Afghanistan\","
      + "\"ja\": \"アフガニスタン\""
      + "}"
      + "},"
      + "\"geoNodeId\": 11007,"
      + "\"countryCode\": \"AF\","
      + "\"phonePrefix\": \"+93\""
      + "},"
      + "{"
      + "\"name\": {"
      + "\"texts\": {"
      + "\"de\": \"Åland Inseln\","
      + "\"it\": \"Isole Aland\","
      + "\"el\": \"Νησιά Ώλαντ\","
      + "\"tr\": \"Aland Adaları\","
      + "\"pt\": \"Ilhas Åland\","
      + "\"fr\": \"Îles d'Åland\","
      + "\"en\": \"Åland Islands\","
      + "\"ru\": \"Аландские острова\","
      + "\"es\": \"Islas Åland\","
      + "\"nl\": \"Ålandeilanden\","
      + "\"ja\": \"アーランド諸島\""
      + "}"
      + "},"
      + "\"geoNodeId\": 11248,"
      + "\"countryCode\": \"AX\","
      + "\"phonePrefix\": null"
      + "},"
      + "{"
      + "\"name\": {"
      + "\"texts\": {"
      + "\"de\": \"Albanien\","
      + "\"it\": \"Albania\","
      + "\"el\": \"Αλβανία\","
      + "\"tr\": \"Arnavutluk\","
      + "\"pt\": \"Albânia\","
      + "\"fr\": \"Albanie\","
      + "\"en\": \"Albania\","
      + "\"ru\": \"Албания\","
      + "\"es\": \"Albania\","
      + "\"nl\": \"Albanië\","
      + "\"ja\": \"アルバニア\""
      + "}"
      + "},"
      + "\"geoNodeId\": 11008,"
      + "\"countryCode\": \"AL\","
      + "\"phonePrefix\": \"+355\""
      + "}"
      + "]}";

  private NetCountryMapper mNetCountryMapper;

  @Before public void setUp() {
    mNetCountryMapper = new NetCountryMapper();
  }

  @Test public void testParseInvalidJson() {
    List<NetCountry> netCountries = mNetCountryMapper.parseNetCountries(JSON_INVALID);
    Assert.assertNotNull(netCountries);
    Assert.assertEquals(0, netCountries.size());
  }

  @Test public void testParseCountries() {
    List<NetCountry> netCountries = mNetCountryMapper.parseNetCountries(JSON_OK);
    Assert.assertNotNull(netCountries);
    Assert.assertEquals(3, netCountries.size());
    Assert.assertEquals("AF", netCountries.get(0).getCountryCode());
    Assert.assertEquals("AX", netCountries.get(1).getCountryCode());
    Assert.assertEquals("AL", netCountries.get(2).getCountryCode());
  }
}
