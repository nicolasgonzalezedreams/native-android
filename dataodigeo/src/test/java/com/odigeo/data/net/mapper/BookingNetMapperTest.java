package com.odigeo.data.net.mapper;

import com.odigeo.data.entity.booking.Booking;
import com.odigeo.dataodigeo.net.mapper.BookingNetMapper;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by javier.rebollo on 3/9/15.
 */
@RunWith(RobolectricTestRunner.class) public class BookingNetMapperTest {
  private BookingNetMapper mBookingNetMapper;
  private String mJsonFake;
  private String mOneWayBooking;
  private String mRoundTripBooking;
  private String mMultiTripBooking;
  private String mTwoSegmentsMultiTripBooking;

  @Before public void setUp() {
    mBookingNetMapper = new BookingNetMapper();
    mJsonFake =
        "{\"insuranceBookings\":{\"bookings\":[]},\"locale\":\"es_ES\",\"price\":{\"currency\":\"EUR\",\"amount\":1446.62},\"buyer\":{\"address\":\"C/PeriodistaJuanOsorioBueno,2\",\"name\":\"MIRIANA\",\"country\":\"ES\",\"mail\":\"myriammolinal@gmail.com\",\"phoneNumber\":{\"number\":\"619029304\",\"countryCode\":\"ES\"},\"cityName\":\"Granada\",\"identification\":null,\"alternativePhoneNumber\":{\"number\":\"958151450\",\"countryCode\":\"ES\"},\"buyerIdentificationType\":null,\"cpf\":null,\"lastNames\":\"MOLINALEON\",\"stateName\":\"Granada\",\"zipCode\":\"18014\"},\"itineraryBookings\":{\"bookings\":[{\"id\":1372378145,\"numAdults\":2,\"resident\":false,\"segments\":[0,1],\"numChildren\":0,\"numInfants\":0,\"additionalParameters\":[],\"bookingStatus\":\"CONTRACT\",\"pnr\":\"2F6UOK\",\"itineraryProviders\":[0],\"merchant\":true,\"paymentMethodType\":\"CREDITCARD\",\"corporateCreditCard\":\"5215450192783519\",\"providerBookingItinerary\":[{\"providerItineraryId\":824141752,\"ticketNumbers\":[{\"number\":\"9352453290\",\"numPassenger\":1},{\"number\":\"9352453289\",\"numPassenger\":1},{\"number\":\"9352453287\",\"numPassenger\":2},{\"number\":\"9352453288\",\"numPassenger\":2}]}],\"residentValidations\":{},\"baggageProviderPrice\":0,\"totalProviderPrice\":1432.64,\"officeId\":\"BCNED3351\",\"creditCardDetails\":{\"owner\":\"MIRIANAMOLINALEON\",\"creditCardType\":\"CA\",\"creditCardNumber\":\"5215450192783519\",\"cvv\":null,\"creditCardExpirationDate\":\"0818\"},\"corporateCreditCardExDate\":\"0818\"}],\"bookingItinerary\":{\"type\":\"SIMPLE\",\"departure\":{\"name\":\"AdolfoSuárezMadrid-Barajas\",\"type\":\"Airport\",\"timeZone\":\"Europe/Madrid\",\"geoNodeId\":1147,\"iataCode\":\"MAD\",\"cityName\":\"Madrid\",\"countryCode\":\"ES\",\"cityIataCode\":\"MAD\",\"countryName\":\"Spain\"},\"departureDate\":1468503000000,\"arrivalDate\":1470224400000,\"tripType\":\"MULTI_SEGMENT\",\"arrival\":{\"name\":\"CapeTownInternationalAirport\",\"type\":\"Airport\",\"timeZone\":\"Africa/Johannesburg\",\"geoNodeId\":418,\"iataCode\":\"CPT\",\"cityName\":\"CapeTown\",\"countryCode\":\"ZA\",\"cityIataCode\":\"CPT\",\"countryName\":\"SouthAfrica\"}},\"legend\":{\"carriers\":[{\"name\":\"Emirates\",\"id\":0,\"code\":\"EK\"}],\"locations\":[{\"name\":\"AdolfoSuárezMadrid-Barajas\",\"type\":\"Airport\",\"timeZone\":\"Europe/Madrid\",\"geoNodeId\":1147,\"iataCode\":\"MAD\",\"cityName\":\"Madrid\",\"countryCode\":\"ES\",\"cityIataCode\":\"MAD\",\"countryName\":\"Spain\"},{\"name\":\"ORTamboInternationalAirport\",\"type\":\"Airport\",\"timeZone\":\"Africa/Johannesburg\",\"geoNodeId\":917,\"iataCode\":\"JNB\",\"cityName\":\"Johannesburg\",\"countryCode\":\"ZA\",\"cityIataCode\":\"JNB\",\"countryName\":\"SouthAfrica\"},{\"name\":\"CapeTownInternationalAirport\",\"type\":\"Airport\",\"timeZone\":\"Africa/Johannesburg\",\"geoNodeId\":418,\"iataCode\":\"CPT\",\"cityName\":\"CapeTown\",\"countryCode\":\"ZA\",\"cityIataCode\":\"CPT\",\"countryName\":\"SouthAfrica\"},{\"name\":\"DubaiInternationalAirport\",\"type\":\"Airport\",\"timeZone\":\"Asia/Dubai\",\"geoNodeId\":536,\"iataCode\":\"DXB\",\"cityName\":\"Dubai\",\"countryCode\":\"AE\",\"cityIataCode\":\"DXB\",\"countryName\":\"UnitedArabEmirates\"}]},\"itinerarySegments\":[{\"duration\":1200,\"sections\":[{\"sectionType\":\"SCALE\",\"itineraryBookingId\":1372378145,\"id\":0,\"section\":{\"id\":\"142\",\"duration\":435,\"from\":1147,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":null,\"arrivalTerminal\":null,\"baggageAllowanceQuantity\":null,\"baggageAllowanceType\":null,\"carrier\":0,\"departureDate\":1468510200770,\"arrivalDate\":1468543500770,\"departureTerminal\":null,\"fareInfoPassengers\":[],\"operatingCarrier\":null,\"technicalStops\":[],\"to\":536}},{\"sectionType\":\"ARRIVAL\",\"itineraryBookingId\":1372378145,\"id\":1,\"section\":{\"id\":\"772\",\"duration\":580,\"from\":536,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":null,\"arrivalTerminal\":null,\"baggageAllowanceQuantity\":null,\"baggageAllowanceType\":null,\"carrier\":0,\"departureDate\":1468554600775,\"arrivalDate\":1468582200775,\"departureTerminal\":null,\"fareInfoPassengers\":[],\"operatingCarrier\":null,\"technicalStops\":[],\"to\":418}}]},{\"duration\":1105,\"sections\":[{\"sectionType\":\"SCALE\",\"itineraryBookingId\":1372378145,\"id\":2,\"section\":{\"id\":\"764\",\"duration\":480,\"from\":917,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":null,\"arrivalTerminal\":null,\"baggageAllowanceQuantity\":null,\"baggageAllowanceType\":null,\"carrier\":0,\"departureDate\":1470165300776,\"arrivalDate\":1470201300776,\"departureTerminal\":null,\"fareInfoPassengers\":[],\"operatingCarrier\":null,\"technicalStops\":[],\"to\":536}},{\"sectionType\":\"ARRIVAL\",\"itineraryBookingId\":1372378145,\"id\":3,\"section\":{\"id\":\"141\",\"duration\":480,\"from\":536,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":null,\"arrivalTerminal\":null,\"baggageAllowanceQuantity\":null,\"baggageAllowanceType\":null,\"carrier\":0,\"departureDate\":1470210000777,\"arrivalDate\":1470231600777,\"departureTerminal\":null,\"fareInfoPassengers\":[],\"operatingCarrier\":null,\"technicalStops\":[],\"to\":1147}}]}]},\"travellers\":[{\"name\":\"MIRIANA\",\"title\":\"MRS\",\"countryCodeOfResidence\":null,\"dateOfBirth\":null,\"firstLastName\":\"MOLINA\",\"identification\":null,\"identificationExpirationDate\":null,\"identificationIssueCountryCode\":null,\"identificationType\":null,\"localityCodeOfResidence\":null,\"meal\":null,\"middleName\":\"\",\"nationalityCountryCode\":null,\"secondLastName\":\"LEON\",\"travellerGender\":null,\"travellerType\":\"ADULT\",\"baggageSelections\":[{\"kilos\":0,\"pieces\":1,\"segmentIndex\":0},{\"kilos\":0,\"pieces\":1,\"segmentIndex\":1}],\"numPassenger\":1,\"frequentFlyerCards\":[{\"number\":\"281782642\",\"carrierCode\":\"EK\"}]},{\"name\":\"LAURA\",\"title\":\"MS\",\"countryCodeOfResidence\":null,\"dateOfBirth\":null,\"firstLastName\":\"BLANCO\",\"identification\":null,\"identificationExpirationDate\":null,\"identificationIssueCountryCode\":null,\"identificationType\":null,\"localityCodeOfResidence\":null,\"meal\":null,\"middleName\":\"\",\"nationalityCountryCode\":null,\"secondLastName\":\"MOLINA\",\"travellerGender\":null,\"travellerType\":\"ADULT\",\"baggageSelections\":[{\"kilos\":0,\"pieces\":1,\"segmentIndex\":0},{\"kilos\":0,\"pieces\":1,\"segmentIndex\":1}],\"numPassenger\":2,\"frequentFlyerCards\":[{\"number\":\"281782642\",\"carrierCode\":\"EK\"}]}],\"deposit\":null,\"bookingStatus\":\"CONTRACT\",\"collectionState\":\"COLLECTED\",\"currencyCode\":\"EUR\",\"message\":\"\",\"bookingBasicInfo\":{\"id\":1372377658,\"agent\":{\"firstName\":\"SiteSpain\",\"lastName\":\"SiteSpain\"},\"creationDate\":1441128934000,\"lastUpdate\":1441132478000,\"clientBookingReferenceId\":null,\"partner\":null,\"brandCode\":\"ED\",\"marketingPortalCode\":\"skyscanner\",\"websiteCode\":\"ES\"}}";

    // mail: gonzalo.molina@globant.com - booking id: 430569978 - GoVoyages QA14
    mOneWayBooking =
        "{\"locale\":\"fr_FR\",\"price\":{\"currency\":\"EUR\",\"amount\":393.03},\"buyer\":{\"address\":\"ghggh\",\"name\":\"GONZALO "
            + "JAVIER\",\"country\":\"AO\",\"mail\":\"gonzalo.molina@globant.com\",\"phoneNumber\":{\"number\":\"55555255\","
            + "\"countryCode\":\"AI\"},\"cityName\":\"fhggg\",\"identification\":null,\"alternativePhoneNumber\":null,"
            + "\"buyerIdentificationType\":null,\"cpf\":null,\"lastNames\":\"MOLINA\",\"stateName\":\"Golaso\",\"zipCode\":\"ghggg\"},"
            + "\"travellers\":[{\"name\":\"Gonzalo Javier\",\"title\":\"MR\",\"countryCodeOfResidence\":null,\"dateOfBirth\":-90000000,"
            + "\"firstLastName\":\"Molina\",\"identification\":null,\"identificationExpirationDate\":null,"
            + "\"identificationIssueCountryCode\":null,\"identificationType\":null,\"localityCodeOfResidence\":null,\"meal\":null,"
            + "\"middleName\":\"\",\"nationalityCountryCode\":null,\"secondLastName\":null,\"travellerGender\":null,"
            + "\"travellerType\":\"ADULT\",\"baggageSelections\":[{\"kilos\":0,\"pieces\":0,\"segmentIndex\":0}],\"numPassenger\":1,"
            + "\"frequentFlyerCards\":[]}],\"itineraryBookings\":{\"legend\":{\"carriers\":[{\"name\":\"British Airways\",\"id\":0,"
            + "\"code\":\"BA\"},{\"name\":\"Aer Lingus\",\"id\":2,\"code\":\"EI\"},{\"name\":\"Iberia\",\"id\":1,\"code\":\"IB\"}],"
            + "\"locations\":[{\"name\":\"Heathrow\",\"type\":\"Airport\",\"geoNodeId\":1070,\"iataCode\":\"LHR\","
            + "\"cityName\":\"Londres\",\"countryCode\":\"GB\",\"cityIataCode\":\"LON\",\"countryName\":\"Royaume-Uni\","
            + "\"timezone\":\"Europe/London\"},{\"name\":\"Adolfo Suárez Madrid - Barajas\",\"type\":\"Airport\",\"geoNodeId\":1147,"
            + "\"iataCode\":\"MAD\",\"cityName\":\"Madrid\",\"countryCode\":\"ES\",\"cityIataCode\":\"MAD\",\"countryName\":\"Espagne\","
            + "\"timezone\":\"Europe/Madrid\"},{\"name\":\"Gatwick\",\"type\":\"Airport\",\"geoNodeId\":1068,\"iataCode\":\"LGW\","
            + "\"cityName\":\"Londres\",\"countryCode\":\"GB\",\"cityIataCode\":\"LON\",\"countryName\":\"Royaume-Uni\","
            + "\"timezone\":\"Europe/London\"},{\"name\":\"Belfast City Apt\",\"type\":\"Airport\",\"geoNodeId\":195,"
            + "\"iataCode\":\"BHD\",\"cityName\":\"Belfast\",\"countryCode\":\"GB\",\"cityIataCode\":\"BFS\","
            + "\"countryName\":\"Royaume-Uni\",\"timezone\":\"Europe/London\"}]},\"bookings\":[{\"numAdults\":1,\"resident\":false,"
            + "\"segments\":[0],\"numChildren\":0,\"numInfants\":0,\"pnr\":\"PNR TEST\",\"residentValidations\":[],"
            + "\"baggageProviderPrice\":0,\"id\":430569980,\"bookingStatus\":\"CONTRACT\"}],\"itinerarySegments\":[{\"duration\":1080,"
            + "\"sections\":[{\"id\":0,\"section\":{\"id\":\"7050\",\"duration\":140,\"from\":1147,"
            + "\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":\"321\",\"arrivalTerminal\":\"5\",\"baggageAllowanceQuantity\":1,"
            + "\"baggageAllowanceType\":\"NP\",\"carrier\":0,\"departureDate\":1450113000000,\"arrivalDate\":1450117800000,"
            + "\"departureTerminal\":\"4\",\"fareInfoPassengers\":[],\"operatingCarrier\":1,\"technicalStops\":[],\"to\":1070,"
            + "\"departureTime\":1450113000000,\"arrivalTime\":1450117800000,\"cabinClassCode\":\"M\",\"eventReceived\":\"false\","
            + "\"operatingVendor\":\"British Airways\",\"operatingVendorCode\":\"BA\",\"operatingVendorLegNumber\":\"7050\","
            + "\"baggageClaim\":null,\"departureGate\":null,\"updatedDepartureTerminal\":null,\"destinationIataCode\":\"LHR\","
            + "\"destinationAirportName\":\"Heathrow\",\"arrivalGate\":null,\"updatedArrivalTerminal\":null,\"flightStatus\":null,"
            + "\"departureTimeType\":null,\"departureTimeDelay\":null,\"arrivalTimeType\":null,\"arrivalTimeDelay\":null},"
            + "\"sectionType\":\"SCALE\",\"itineraryBookingId\":430569980},{\"id\":1,\"section\":{\"id\":\"5933\",\"duration\":85,"
            + "\"from\":1068,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":\"320\",\"arrivalTerminal\":null,"
            + "\"baggageAllowanceQuantity\":1,\"baggageAllowanceType\":\"NP\",\"carrier\":0,\"departureDate\":1450169100000,"
            + "\"arrivalDate\":1450174200000,\"departureTerminal\":\"S\",\"fareInfoPassengers\":[],\"operatingCarrier\":2,"
            + "\"technicalStops\":[],\"to\":195,\"departureTime\":1450169100000,\"arrivalTime\":1450174200000,\"cabinClassCode\":\"M\","
            + "\"eventReceived\":\"false\",\"operatingVendor\":\"British Airways\",\"operatingVendorCode\":\"BA\","
            + "\"operatingVendorLegNumber\":\"5933\",\"baggageClaim\":null,\"departureGate\":null,\"updatedDepartureTerminal\":null,"
            + "\"destinationIataCode\":\"BHD\",\"destinationAirportName\":\"Belfast City Apt\",\"arrivalGate\":null,"
            + "\"updatedArrivalTerminal\":null,\"flightStatus\":null,\"departureTimeType\":null,\"departureTimeDelay\":null,"
            + "\"arrivalTimeType\":null,\"arrivalTimeDelay\":null},\"sectionType\":\"ARRIVAL\",\"itineraryBookingId\":430569980}]}]},"
            + "\"insuranceBookings\":{\"bookings\":[{\"startDate\":1450109400000,\"totalPrice\":5.35,\"insurance\":{\"id\":2284,"
            + "\"type\":null,\"policy\":\"CNXFLT151\",\"provider\":\"AON\",\"description\":\"{b Nous vous remboursons le billet en cas "
            + "d'annulation pour tout motif justifié.}\\n Indemnisation en cas de retard du vol.\\n Couverture en cas d'incident avec la "
            + "compagnie aérienne.\\n Si vous ratez votre vol, nous vous en trouvons un autre en moins de 48 h.\",\"title\":\"Assurance "
            + "annulation de vol\",\"totalPrice\":\"Prix de l'assurance<\\/b>\\n\\nIl varie en fonction d'un certain nombre de critères, "
            + "notamment le prix de votre billet et le nombre de personnes qui voyagent avec vous. Pour plus d'informations sur le calcul"
            + " du prix, cliquez ici.<\\/a>\\n\",\"conditionsUrls\":[{\"type\":\"BASIC\",\"url\":\"http://www.taeds"
            + ".com/aonseguros/extractoPDF.asp?POLICY_NUMBER=CNXFLT151&LANGUAGE=FR&BRAND=GV&COUNTRY=FR&ID_CM=1613\"},"
            + "{\"type\":\"EXTENDED\",\"url\":\"http://www.taeds.com/aonseguros/condicionPDF"
            + ".asp?POLICY_NUMBER=CNXFLT151&LANGUAGE=FR&BRAND=GV&COUNTRY=FR&ID_CM=1613\"}],\"subtitle\":\"\",\"conditionAcceptance\":\"En"
            + " sélectionnant, j'accepte les conditions<\\/b>\",\"documentLink\":\"Voyagez en toute sérénité avec l'assurance de GO "
            + "Voyages<\\/b>\\n\\nPour plus d'informations sur la Garantie annulation, consultez le détail de la police d'assurance"
            + ".<\\/a>\\n\",\"textConditions\":\"Nous vous recommandons vivement d'en prendre connaissance.\\n\\nQue couvre la Garantie "
            + "annulation ?<\\/b>\\n\\nEn choisissant cette option, vous êtes :\\n\\n- Indemnisé en cas d'annulation de votre part, pour "
            + "tout motif d'annulation couvert par la police\\n- Dédommagé en cas de retard de vol\\n- Couvert dans le cas où vous ratez "
            + "votre vol, sous réserve des conditions applicables\\n\\nNotre gamme de produits d'assurance<\\/b>Pour profiter pleinement "
            + "de votre voyage, vous avez le choix entre la Garantie annulation et le Pack multirisques, qui regroupe les Garanties "
            + "annulation et assistance rapatriement. Grâce à ces deux produits, vous restez serein même en cas d'annulation de votre "
            + "voyage. En effet, ils couvrent de nombreuses causes d'annulation. Le Pack multirisques offre d'autres avantages, tels que "
            + "l'assistance médicale et la couverture en cas de perte de vos bagages, selon les conditions applicables.\\n\\nQui est "
            + "couvert par votre assurance ?<\\/b>\\n\\nTous les passagers. Lorsque vous ajoutez une assurance à votre panier, vous êtes "
            + "assuré que toutes les personnes participant au voyage seront couvertes. Ceci s'appliquant uniquement aux résidents "
            + "européens. Les non membres de l'Espace économique européen, comme les citoyens suisses, ne bénéficient pas de cette "
            + "couverture.\\n\\nQui fournit ces garanties ?<\\/b>\\n\\nEurop Assistance, une des plus importantes compagnies d'assurance "
            + "spécialisé dans le voyage. Nous sommes votre intermédiaire lors de la vente de ce produit mais Europ Assistance sera votre"
            + " interlocuteur privilégié pour toute demande.\\n\\nSinistre & réclamation<\\/b>\\n\\nLorsque vous souscrivez une "
            + "assurance, nous souhaitons vous rendre les démarches aussi simple que possibles tout en nous assurant que vous nous "
            + "fournissez toutes les informations nécessaires à la vérification de votre demande :\\n\\n- Si vous avez souscrit le Pack "
            + "multirisques, un numéro de téléphone est à votre disposition 24/24 pour tout besoin urgent d'assistance à l'étranger\\n- "
            + "Pour toute autre demande, rendez-vous sur le site Internet. Si vous préférez un contact direct, un numéro de téléphone est"
            + " à votre disposition\\n\\nQue dois-je emporter en voyage si j'ai souscrit une assurance ?<\\/b>\\n\\nNous vous "
            + "recommandons d'emporter une copie de votre police d'assurance. Cette dernière détaille les modalités pour nous contacter "
            + "et vous informe des justificatifs à conserver pour toute réclamation ultérieure.\\n\\n\",\"totalPriceDetail\":\"Le prix "
            + "total comprend la prime d'assurance pour la couverture assurance, fournie par Europ Assistance, la taxe sur la prime "
            + "d'assurance et les frais d'intermédiation fournis par le service d'intermédiation de GO Voyages. Il est calculé comme suit"
            + " :\\n\\nSi le prix de votre billet est inférieur à 150 € par personne (adultes et enfants) taxes d'aéroport, surcharge "
            + "carburant et toute autre taxe applicable incluses, hors frais de carte de crédit, la partie assurance du prix total "
            + "s'élève à :\\n\\n- 1,95 € de prime de Garantie annulation\\n- Majoration de 9,00 % de cette somme, correspondant à la taxe"
            + " sur la prime d'assurance.\\n\\nPar ailleurs, vous devrez vous acquitter des Frais d'intermédiation pour l'achat de toute "
            + "assurance. Le montant de ces frais est égal au prix total que vous payez pour votre assurance moins la prime d'assurance "
            + "et la taxe sur la prime d'assurance.\\n\\nSi le prix de votre billet est compris entre 150 €et 299 € par personne (adultes"
            + " et enfants) taxes d'aéroport, surcharge carburant et toute autre taxe applicable incluses, hors frais de carte de crédit,"
            + " la partie assurance du prix total s'élève à :\\n\\n- 2,96 € de prime de Garantie annulation\\n.- Majoration de 9,00 % de "
            + "cette somme, correspondant à la taxe sur la prime d'assurance.\\n\\nPar ailleurs, vous devrez vous acquitter des Frais "
            + "d'intermédiation pour l'achat de toute assurance. Le montant de ces frais est égal au prix total que vous payez pour votre"
            + " assurance moins la prime d'assurance et la taxe sur la prime d'assurance.\\n\\nSi le prix de votre billet est compris "
            + "entre 300 €et 449 € par personne (adultes et enfants) taxes d'aéroport, surcharge carburant et toute autre taxe applicable"
            + " incluses, hors frais de carte de crédit, la partie assurance du prix total s'élève à :\\n\\n- 4,91 € de prime de Garantie"
            + " annulation\\n.- Majoration de 9,00 % de cette somme, correspondant à la taxe sur la prime d'assurance.\\n\\nPar ailleurs,"
            + " vous devrez vous acquitter des Frais d'intermédiation pour l'achat de toute assurance. Le montant de ces frais est égal "
            + "au prix total que vous payez pour votre assurance moins la prime d'assurance et la taxe sur la prime d'assurance.\\n\\nSi "
            + "le prix de votre billet est compris entre 450 €et 599 € par personne (adultes et enfants) taxes d'aéroport, surcharge "
            + "carburant et toute autre taxe applicable incluses, hors frais de carte de crédit, la partie assurance du prix total "
            + "s'élève à :\\n\\n- 6,86 € de prime de Garantie annulation\\n.- Majoration de 9,00 % de cette somme, correspondant à la "
            + "taxe sur la prime d'assurance.\\n\\nPar ailleurs, vous devrez vous acquitter des Frais d'intermédiation pour l'achat de "
            + "toute assurance. Le montant de ces frais est égal au prix total que vous payez pour votre assurance moins la prime "
            + "d'assurance et la taxe sur la prime d'assurance.\\n\\nSi le prix de votre billet est compris entre 600 € et 799 € par "
            + "personne (adultes et enfants) taxes d'aéroport, surcharge carburant et toute autre taxe applicable incluses, hors frais de"
            + " carte de crédit, la partie assurance du prix total s'élève à :\\n\\n- 9,10 € de prime de Garantie annulation\\n.- "
            + "Majoration de 9,00 % de cette somme, correspondant à la taxe sur la prime d'assurance.\\n\\nPar ailleurs, vous devrez vous"
            + " acquitter des Frais d'intermédiation pour l'achat de toute assurance. Le montant de ces frais est égal au prix total que "
            + "vous payez pour votre assurance moins la prime d'assurance et la taxe sur la prime d'assurance.\\n\\nSi le prix de votre "
            + "billet est compris entre 800 €et 999 € par personne (adultes et enfants) taxes d'aéroport, surcharge carburant et toute "
            + "autre taxe applicable incluses, hors frais de carte de crédit, la partie assurance du prix total s'élève à :\\n\\n- 11,56 "
            + "€ de prime de Garantie annulation\\n.- Majoration de 9,00 % de cette somme, correspondant à la taxe sur la prime "
            + "d'assurance.\\n\\nPar ailleurs, vous devrez vous acquitter des Frais d'intermédiation pour l'achat de toute assurance. Le "
            + "montant de ces frais est égal au prix total que vous payez pour votre assurance moins la prime d'assurance et la taxe sur "
            + "la prime d'assurance.\\n\\nSi le prix de votre billet est compris entre 1000 €et 1199 € par personne (adultes et enfants) "
            + "taxes d'aéroport, surcharge carburant et toute autre taxe applicable incluses, hors frais de carte de crédit, la partie "
            + "assurance du prix total s'élève à :\\n\\n- 14,30 € de prime de Garantie annulation\\n.- Majoration de 9,00 % de cette "
            + "somme, correspondant à la taxe sur la prime d'assurance.\\n\\nPar ailleurs, vous devrez vous acquitter des Frais "
            + "d'intermédiation pour l'achat de toute assurance. Le montant de ces frais est égal au prix total que vous payez pour votre"
            + " assurance moins la prime d'assurance et la taxe sur la prime d'assurance.\\n\\nSi le prix de votre billet est compris "
            + "entre 1200 €et 1399 € par personne (adultes et enfants) taxes d'aéroport, surcharge carburant et toute autre taxe "
            + "applicable incluses, hors frais de carte de crédit, la partie assurance du prix total s'élève à :\\n\\n- 16,90 € de prime "
            + "de Garantie annulation\\n.- Majoration de 9,00 % de cette somme, correspondant à la taxe sur la prime d'assurance"
            + ".\\n\\nPar ailleurs, vous devrez vous acquitter des Frais d'intermédiation pour l'achat de toute assurance. Le montant de "
            + "ces frais est égal au prix total que vous payez pour votre assurance moins la prime d'assurance et la taxe sur la prime "
            + "d'assurance.\\n\\nSi le prix de votre billet est compris entre 1400 €et 2999 € par personne (adultes et enfants) taxes "
            + "d'aéroport, surcharge carburant et toute autre taxe applicable incluses, hors frais de carte de crédit, la partie "
            + "assurance du prix total s'élève à :\\n\\n- 28,60 € de prime de Garantie annulation\\n.- Majoration de 9,00 % de cette "
            + "somme, correspondant à la taxe sur la prime d'assurance.\\n\\nPar ailleurs, vous devrez vous acquitter des Frais "
            + "d'intermédiation pour l'achat de toute assurance. Le montant de ces frais est égal au prix total que vous payez pour votre"
            + " assurance moins la prime d'assurance et la taxe sur la prime d'assurance.\\n\\nSi le prix de votre billet est compris "
            + "entre 3000 €et 5999 € par personne (adultes et enfants) taxes d'aéroport, surcharge carburant et toute autre taxe "
            + "applicable incluses, hors frais de carte de crédit, la partie assurance du prix total s'élève à :\\n\\n- 58,50 € de prime "
            + "de Garantie annulation\\n.- Majoration de 9,00 % de cette somme, correspondant à la taxe sur la prime d'assurance"
            + ".\\n\\nPar ailleurs, vous devrez vous acquitter des Frais d'intermédiation pour l'achat de toute assurance. Le montant de "
            + "ces frais est égal au prix total que vous payez pour votre assurance moins la prime d'assurance et la taxe sur la prime "
            + "d'assurance.\\n\\nSi le prix de votre billet est compris entre 6000 €et 9999 € par personne (adultes et enfants) taxes "
            + "d'aéroport, surcharge carburant et toute autre taxe applicable incluses, hors frais de carte de crédit, la partie "
            + "assurance du prix total s'élève à :\\n\\n- 104 € de prime de Garantie annulation\\n.- Majoration de 9,00 % de cette somme,"
            + " correspondant à la taxe sur la prime d'assurance.\\n\\nPar ailleurs, vous devrez vous acquitter des Frais "
            + "d'intermédiation pour l'achat de toute assurance. Le montant de ces frais est égal au prix total que vous payez pour votre"
            + " assurance moins la prime d'assurance et la taxe sur la prime d'assurance.\",\"insuranceDescription\":\"Cancellation "
            + "flight\"},\"selectable\":true,\"otherExpenses\":0,\"endDate\":1450174200000,\"id\":430569985,"
            + "\"bookingStatus\":\"CONTRACT\"}]},\"bookingStatus\":\"CONTRACT\",\"collectionState\":\"COLLECTED\","
            + "\"bookingBasicInfo\":{\"id\":430569978,\"creationDate\":1450097595000,\"lastUpdate\":1450097655000,"
            + "\"clientBookingReferenceId\":\"947777\",\"website\":{\"code\":\"GOFR\",\"brand\":\"GV\"},\"requestDate\":1450097595000},"
            + "\"sod\":false,\"travelCompanionNotifications\":true,\"enrichedBooking\":true}";

    // mail: gonzalo.molina@globant.com - booking id: 430732181 - eDreams QA14
    mRoundTripBooking =
        "{\"locale\":\"es_ES\",\"price\":{\"currency\":\"EUR\",\"amount\":1004.49},\"buyer\":{\"address\":\"olayon\","
            + "\"name\":\"GONZALO\",\"country\":\"DE\",\"mail\":\"gonzalo.molina@globant.com\",\"phoneNumber\":{\"number\":\"25582582\","
            + "\"countryCode\":\"DE\"},\"cityName\":\"cordoba\",\"identification\":null,\"alternativePhoneNumber\":null,"
            + "\"buyerIdentificationType\":null,\"cpf\":null,\"lastNames\":\"MOLINA\",\"stateName\":null,\"zipCode\":\"foca\"},"
            + "\"travellers\":[{\"name\":\"gonzalo\",\"title\":\"MR\",\"countryCodeOfResidence\":null,\"dateOfBirth\":1048978800000,"
            + "\"firstLastName\":\"molina\",\"identification\":null,\"identificationExpirationDate\":null,"
            + "\"identificationIssueCountryCode\":null,\"identificationType\":null,\"localityCodeOfResidence\":null,\"meal\":null,"
            + "\"middleName\":\"\",\"nationalityCountryCode\":null,\"secondLastName\":null,\"travellerGender\":null,"
            + "\"travellerType\":\"ADULT\",\"baggageSelections\":[{\"kilos\":0,\"pieces\":0,\"segmentIndex\":0},{\"kilos\":0,"
            + "\"pieces\":0,\"segmentIndex\":1}],\"numPassenger\":1,\"frequentFlyerCards\":[]}],"
            + "\"itineraryBookings\":{\"legend\":{\"carriers\":[{\"name\":\"Alitalia\",\"id\":0,\"code\":\"AZ\"},{\"name\":\"British "
            + "Airways\",\"id\":3,\"code\":\"BA\"},{\"name\":\"Iberia\",\"id\":2,\"code\":\"IB\"},{\"name\":\"Klm Royal Dutch Airlines\","
            + "\"id\":1,\"code\":\"KL\"}],\"locations\":[{\"name\":\"Heathrow\",\"type\":\"Airport\",\"geoNodeId\":1070,"
            + "\"iataCode\":\"LHR\",\"cityName\":\"Londres\",\"countryCode\":\"GB\",\"cityIataCode\":\"LON\","
            + "\"countryName\":\"Royaume-Uni\",\"timezone\":\"Europe/London\"},{\"name\":\"Adolfo Suárez Madrid - Barajas\","
            + "\"type\":\"Airport\",\"geoNodeId\":1147,\"iataCode\":\"MAD\",\"cityName\":\"Madrid\",\"countryCode\":\"ES\","
            + "\"cityIataCode\":\"MAD\",\"countryName\":\"Espagne\",\"timezone\":\"Europe/Madrid\"},{\"name\":\"Cairo International "
            + "Airport\",\"type\":\"Airport\",\"geoNodeId\":311,\"iataCode\":\"CAI\",\"cityName\":\"Le Caire\",\"countryCode\":\"EG\","
            + "\"cityIataCode\":\"CAI\",\"countryName\":\"Égypte\",\"timezone\":\"Africa/Cairo\"},{\"name\":\"John F Kennedy Intl "
            + "Airport\",\"type\":\"Airport\",\"geoNodeId\":902,\"iataCode\":\"JFK\",\"cityName\":\"New York\",\"countryCode\":\"US\","
            + "\"cityIataCode\":\"NYC\",\"countryName\":\"États-unis\",\"timezone\":\"America/New_York\"},{\"name\":\"Schiphol\","
            + "\"type\":\"Airport\",\"geoNodeId\":79,\"iataCode\":\"AMS\",\"cityName\":\"Amsterdam\",\"countryCode\":\"NL\","
            + "\"cityIataCode\":\"AMS\",\"countryName\":\"Pays-bas\",\"timezone\":\"Europe/Amsterdam\"},{\"name\":\"Fiumicino\","
            + "\"type\":\"Airport\",\"geoNodeId\":607,\"iataCode\":\"FCO\",\"cityName\":\"Rome\",\"countryCode\":\"IT\","
            + "\"cityIataCode\":\"ROM\",\"countryName\":\"Italie\",\"timezone\":\"Europe/Rome\"}]},\"bookings\":[{\"numAdults\":1,"
            + "\"resident\":false,\"segments\":[0,1],\"numChildren\":0,\"numInfants\":0,\"pnr\":\"PNR TEST\",\"residentValidations\":[],"
            + "\"baggageProviderPrice\":0,\"id\":430732187,\"bookingStatus\":\"CONTRACT\"},{\"numAdults\":1,\"resident\":false,"
            + "\"segments\":[2,3],\"numChildren\":0,\"numInfants\":0,\"pnr\":\"PNR TEST\",\"residentValidations\":[],"
            + "\"baggageProviderPrice\":0,\"id\":430732183,\"bookingStatus\":\"CONTRACT\"}],\"itinerarySegments\":[{\"duration\":1950,"
            + "\"sections\":[{\"id\":0,\"section\":{\"id\":\"897\",\"duration\":210,\"from\":311,\"cabinClass\":\"ECONOMIC_DISCOUNTED\","
            + "\"aircraft\":\"32S\",\"arrivalTerminal\":\"3\",\"baggageAllowanceQuantity\":1,\"baggageAllowanceType\":\"NP\","
            + "\"carrier\":0,\"departureDate\":1461515400000,\"arrivalDate\":1461528000000,\"departureTerminal\":\"1\","
            + "\"fareInfoPassengers\":[],\"operatingCarrier\":null,\"technicalStops\":[],\"to\":607,\"departureTime\":1461515400000,"
            + "\"arrivalTime\":1461528000000,\"cabinClassCode\":\"M\",\"eventReceived\":\"false\",\"operatingVendor\":\"Alitalia\","
            + "\"operatingVendorCode\":\"AZ\",\"operatingVendorLegNumber\":\"897\",\"baggageClaim\":null,\"departureGate\":null,"
            + "\"updatedDepartureTerminal\":null,\"destinationIataCode\":\"FCO\",\"destinationAirportName\":\"Leonardo da Vinci–Fiumicino"
            + " Airport\",\"arrivalGate\":null,\"updatedArrivalTerminal\":null,\"flightStatus\":null,\"departureTimeType\":null,"
            + "\"departureTimeDelay\":null,\"arrivalTimeType\":null,\"arrivalTimeDelay\":null},\"sectionType\":\"SCALE\","
            + "\"itineraryBookingId\":430732187},{\"id\":1,\"section\":{\"id\":\"58\",\"duration\":155,\"from\":607,"
            + "\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":\"32S\",\"arrivalTerminal\":\"2\",\"baggageAllowanceQuantity\":1,"
            + "\"baggageAllowanceType\":\"NP\",\"carrier\":0,\"departureDate\":1461571200000,\"arrivalDate\":1461580500000,"
            + "\"departureTerminal\":\"1\",\"fareInfoPassengers\":[],\"operatingCarrier\":null,\"technicalStops\":[],\"to\":1147,"
            + "\"departureTime\":1461571200000,\"arrivalTime\":1461580500000,\"cabinClassCode\":\"M\",\"eventReceived\":\"false\","
            + "\"operatingVendor\":\"Alitalia\",\"operatingVendorCode\":\"AZ\",\"operatingVendorLegNumber\":\"58\",\"baggageClaim\":null,"
            + "\"departureGate\":null,\"updatedDepartureTerminal\":null,\"destinationIataCode\":\"MAD\","
            + "\"destinationAirportName\":\"Adolfo Suárez Madrid–Barajas Airport\",\"arrivalGate\":null,\"updatedArrivalTerminal\":null,"
            + "\"flightStatus\":null,\"departureTimeType\":null,\"departureTimeDelay\":null,\"arrivalTimeType\":null,"
            + "\"arrivalTimeDelay\":null},\"sectionType\":\"HUB\",\"itineraryBookingId\":430732187},{\"id\":4,"
            + "\"section\":{\"id\":\"6253\",\"duration\":495,\"from\":1147,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":\"342\","
            + "\"arrivalTerminal\":\"7\",\"baggageAllowanceQuantity\":1,\"baggageAllowanceType\":\"NP\",\"carrier\":2,"
            + "\"departureDate\":1461602700000,\"arrivalDate\":1461610800000,\"departureTerminal\":\"4S\",\"fareInfoPassengers\":[],"
            + "\"operatingCarrier\":null,\"technicalStops\":[],\"to\":902,\"departureTime\":1461602700000,\"arrivalTime\":1461610800000,"
            + "\"cabinClassCode\":\"M\",\"eventReceived\":\"false\",\"operatingVendor\":\"Iberia\",\"operatingVendorCode\":\"IB\","
            + "\"operatingVendorLegNumber\":\"6253\",\"baggageClaim\":null,\"departureGate\":null,\"updatedDepartureTerminal\":null,"
            + "\"destinationIataCode\":\"JFK\",\"destinationAirportName\":\"John F Kennedy International Airport\",\"arrivalGate\":null,"
            + "\"updatedArrivalTerminal\":null,\"flightStatus\":null,\"departureTimeType\":null,\"departureTimeDelay\":null,"
            + "\"arrivalTimeType\":null,\"arrivalTimeDelay\":null},\"sectionType\":\"ARRIVAL\",\"itineraryBookingId\":430732183}]},"
            + "{\"duration\":1395,\"sections\":[{\"id\":5,\"section\":{\"id\":\"4686\",\"duration\":420,\"from\":902,"
            + "\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":\"744\",\"arrivalTerminal\":\"5\",\"baggageAllowanceQuantity\":1,"
            + "\"baggageAllowanceType\":\"NP\",\"carrier\":2,\"departureDate\":1462047300000,\"arrivalDate\":1462090500000,"
            + "\"departureTerminal\":\"7\",\"fareInfoPassengers\":[],\"operatingCarrier\":3,\"technicalStops\":[],\"to\":1070,"
            + "\"departureTime\":1462047300000,\"arrivalTime\":1462090500000,\"cabinClassCode\":\"M\",\"eventReceived\":\"false\","
            + "\"operatingVendor\":\"Iberia\",\"operatingVendorCode\":\"IB\",\"operatingVendorLegNumber\":\"4686\",\"baggageClaim\":null,"
            + "\"departureGate\":null,\"updatedDepartureTerminal\":null,\"destinationIataCode\":\"LHR\","
            + "\"destinationAirportName\":\"London Heathrow Airport\",\"arrivalGate\":null,\"updatedArrivalTerminal\":null,"
            + "\"flightStatus\":null,\"departureTimeType\":null,\"departureTimeDelay\":null,\"arrivalTimeType\":null,"
            + "\"arrivalTimeDelay\":null},\"sectionType\":\"SCALE\",\"itineraryBookingId\":430732183},{\"id\":6,"
            + "\"section\":{\"id\":\"3163\",\"duration\":145,\"from\":1070,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":\"320\","
            + "\"arrivalTerminal\":\"4\",\"baggageAllowanceQuantity\":1,\"baggageAllowanceType\":\"NP\",\"carrier\":2,"
            + "\"departureDate\":1462094100000,\"arrivalDate\":1462106400000,\"departureTerminal\":\"5\",\"fareInfoPassengers\":[],"
            + "\"operatingCarrier\":3,\"technicalStops\":[],\"to\":1147,\"departureTime\":1462094100000,\"arrivalTime\":1462106400000,"
            + "\"cabinClassCode\":\"M\",\"eventReceived\":\"false\",\"operatingVendor\":\"Iberia\",\"operatingVendorCode\":\"IB\","
            + "\"operatingVendorLegNumber\":\"3163\",\"baggageClaim\":null,\"departureGate\":null,\"updatedDepartureTerminal\":null,"
            + "\"destinationIataCode\":\"MAD\",\"destinationAirportName\":\"Adolfo Suárez Madrid–Barajas Airport\",\"arrivalGate\":null,"
            + "\"updatedArrivalTerminal\":null,\"flightStatus\":null,\"departureTimeType\":null,\"departureTimeDelay\":null,"
            + "\"arrivalTimeType\":null,\"arrivalTimeDelay\":null},\"sectionType\":\"HUB\",\"itineraryBookingId\":430732183},{\"id\":2,"
            + "\"section\":{\"id\":\"1704\",\"duration\":160,\"from\":1147,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":\"73H\","
            + "\"arrivalTerminal\":null,\"baggageAllowanceQuantity\":1,\"baggageAllowanceType\":\"NP\",\"carrier\":1,"
            + "\"departureDate\":1462122600000,\"arrivalDate\":1462132200000,\"departureTerminal\":\"2\",\"fareInfoPassengers\":[],"
            + "\"operatingCarrier\":null,\"technicalStops\":[],\"to\":79,\"departureTime\":1462122600000,\"arrivalTime\":1462132200000,"
            + "\"cabinClassCode\":\"M\",\"eventReceived\":\"false\",\"operatingVendor\":\"Klm Royal Dutch Airlines\","
            + "\"operatingVendorCode\":\"KL\",\"operatingVendorLegNumber\":\"1704\",\"baggageClaim\":null,\"departureGate\":null,"
            + "\"updatedDepartureTerminal\":null,\"destinationIataCode\":\"AMS\",\"destinationAirportName\":\"Amsterdam Airport "
            + "Schiphol\",\"arrivalGate\":null,\"updatedArrivalTerminal\":null,\"flightStatus\":null,\"departureTimeType\":null,"
            + "\"departureTimeDelay\":null,\"arrivalTimeType\":null,\"arrivalTimeDelay\":null},\"sectionType\":\"SCALE\","
            + "\"itineraryBookingId\":430732187},{\"id\":3,\"section\":{\"id\":\"553\",\"duration\":260,\"from\":79,"
            + "\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":\"772\",\"arrivalTerminal\":\"1\",\"baggageAllowanceQuantity\":1,"
            + "\"baggageAllowanceType\":\"NP\",\"carrier\":1,\"departureDate\":1462137000000,\"arrivalDate\":1462156200000,"
            + "\"departureTerminal\":null,\"fareInfoPassengers\":[],\"operatingCarrier\":null,\"technicalStops\":[],\"to\":311,"
            + "\"departureTime\":1462137000000,\"arrivalTime\":1462159800000,\"cabinClassCode\":\"M\",\"eventReceived\":\"false\","
            + "\"operatingVendor\":\"Klm Royal Dutch Airlines\",\"operatingVendorCode\":\"KL\",\"operatingVendorLegNumber\":\"553\","
            + "\"baggageClaim\":null,\"departureGate\":null,\"updatedDepartureTerminal\":null,\"destinationIataCode\":\"CAI\","
            + "\"destinationAirportName\":\"Cairo International Airport\",\"arrivalGate\":null,\"updatedArrivalTerminal\":null,"
            + "\"flightStatus\":null,\"departureTimeType\":null,\"departureTimeDelay\":null,\"arrivalTimeType\":null,"
            + "\"arrivalTimeDelay\":null},\"sectionType\":\"ARRIVAL\",\"itineraryBookingId\":430732187}]}]},"
            + "\"insuranceBookings\":{\"bookings\":[{\"startDate\":1461508200000,\"totalPrice\":0.87,\"insurance\":{\"id\":2283,"
            + "\"type\":null,\"policy\":\"CNXHUB153\",\"provider\":\"AON\",\"description\":\"Sólo<\\/b> cubre la pérdida del segundo "
            + "vuelo / tren cuando las compañías son diferentes.\",\"title\":\"Seguro de pérdida de conexión\",\"totalPrice\":null,"
            + "\"conditionsUrls\":[{\"type\":\"BASIC\",\"url\":\"http://www.taeds.com/aonseguros/extractoPDF"
            + ".asp?POLICY_NUMBER=CNXHUB153&LANGUAGE=ES&BRAND=ED&COUNTRY=ES&ID_CM=1620\"},{\"type\":\"EXTENDED\",\"url\":\"http://www"
            + ".taeds.com/aonseguros/condicionPDF.asp?POLICY_NUMBER=CNXHUB153&LANGUAGE=ES&BRAND=ED&COUNTRY=ES&ID_CM=1620\"}],"
            + "\"subtitle\":\"Incluido en el precio\",\"conditionAcceptance\":null,\"documentLink\":null,\"textConditions\":null,"
            + "\"totalPriceDetail\":null,\"insuranceDescription\":\"Missed Connection\"},\"selectable\":false,\"otherExpenses\":0,"
            + "\"endDate\":1462099200000,\"id\":430732189,\"bookingStatus\":\"CONTRACT\"}]},\"bookingStatus\":\"CONTRACT\","
            + "\"collectionState\":\"COLLECTED\",\"bookingBasicInfo\":{\"id\":430732181,\"creationDate\":1459343369000,"
            + "\"lastUpdate\":1459343415000,\"clientBookingReferenceId\":\"430732181\",\"website\":{\"code\":\"ES\",\"brand\":\"ED\"},"
            + "\"requestDate\":1459343369000},\"sod\":false,\"travelCompanionNotifications\":true,\"enrichedBooking\":true}";

    // mail: gonzalo.molina@globant.com - booking id: 430808640 - eDreams QA14
    mMultiTripBooking =
        "{\"locale\":\"en_GB\",\"price\":{\"currency\":\"GBP\",\"amount\":756.65},\"buyer\":{\"address\":\"olayon\","
            + "\"name\":\"FHGHV\",\"country\":\"AO\",\"mail\":\"gonzalo.molina@globant.com\",\"phoneNumber\":{\"number\":\"25552552\","
            + "\"countryCode\":\"AO\"},\"cityName\":\"cordoba\",\"identification\":null,\"alternativePhoneNumber\":null,"
            + "\"buyerIdentificationType\":null,\"cpf\":null,\"lastNames\":\"CFBBBHH\",\"stateName\":null,\"zipCode\":\"gol\"},"
            + "\"travellers\":[{\"name\":\"fhghv\",\"title\":\"MR\",\"countryCodeOfResidence\":null,\"dateOfBirth\":-90000000,"
            + "\"firstLastName\":\"cfbbbhh\",\"identification\":null,\"identificationExpirationDate\":null,"
            + "\"identificationIssueCountryCode\":null,\"identificationType\":null,\"localityCodeOfResidence\":null,\"meal\":null,"
            + "\"middleName\":\"\",\"nationalityCountryCode\":null,\"secondLastName\":null,\"travellerGender\":null,"
            + "\"travellerType\":\"ADULT\",\"baggageSelections\":[{\"kilos\":0,\"pieces\":0,\"segmentIndex\":0},{\"kilos\":0,"
            + "\"pieces\":0,\"segmentIndex\":1},{\"kilos\":0,\"pieces\":0,\"segmentIndex\":2}],\"numPassenger\":1,"
            + "\"frequentFlyerCards\":[]}],\"itineraryBookings\":{\"legend\":{\"carriers\":[{\"name\":\"Ryanair\",\"id\":2,"
            + "\"code\":\"FR\"},{\"name\":\"Lan Airlines\",\"id\":0,\"code\":\"LA\"},{\"name\":\"Boliviana De Aviacion\",\"id\":1,"
            + "\"code\":\"OB\"}],\"locations\":[{\"name\":\"Paris Beauvais\",\"type\":\"Airport\",\"geoNodeId\":290,\"iataCode\":\"BVA\","
            + "\"cityName\":\"Paris\",\"countryCode\":\"FR\",\"cityIataCode\":\"PAR\",\"countryName\":\"France\","
            + "\"timezone\":\"Europe/Paris\"},{\"name\":\"Pajas Blancas\",\"type\":\"Airport\",\"geoNodeId\":408,\"iataCode\":\"COR\","
            + "\"cityName\":\"Córdoba\",\"countryCode\":\"AR\",\"cityIataCode\":\"COR\",\"countryName\":\"Argentine\","
            + "\"timezone\":\"America/Argentina/Cordoba\"},{\"name\":\"Viru Viru International\",\"type\":\"Airport\",\"geoNodeId\":2035,"
            + "\"iataCode\":\"VVI\",\"cityName\":\"Santa Cruz\",\"countryCode\":\"BO\",\"cityIataCode\":\"VVI\","
            + "\"countryName\":\"Bolivie\",\"timezone\":\"America/La_Paz\"},{\"name\":\"Adolfo Suárez Madrid - Barajas\","
            + "\"type\":\"Airport\",\"geoNodeId\":1147,\"iataCode\":\"MAD\",\"cityName\":\"Madrid\",\"countryCode\":\"ES\","
            + "\"cityIataCode\":\"MAD\",\"countryName\":\"Espagne\",\"timezone\":\"Europe/Madrid\"},{\"name\":\"Pistarini\","
            + "\"type\":\"Airport\",\"geoNodeId\":599,\"iataCode\":\"EZE\",\"cityName\":\"Buenos Aires\",\"countryCode\":\"AR\","
            + "\"cityIataCode\":\"BUE\",\"countryName\":\"Argentine\",\"timezone\":\"America/Argentina/Buenos_Aires\"},{\"name\":\"Jorge "
            + "Newberry\",\"type\":\"Airport\",\"geoNodeId\":37,\"iataCode\":\"AEP\",\"cityName\":\"Buenos Aires\","
            + "\"countryCode\":\"AR\",\"cityIataCode\":\"BUE\",\"countryName\":\"Argentine\","
            + "\"timezone\":\"America/Argentina/Buenos_Aires\"}]},\"bookings\":[{\"numAdults\":1,\"resident\":false,\"segments\":[0],"
            + "\"numChildren\":0,\"numInfants\":0,\"pnr\":\"PNR TEST\",\"residentValidations\":[],\"baggageProviderPrice\":0,"
            + "\"id\":430808642,\"bookingStatus\":\"CONTRACT\"},{\"numAdults\":1,\"resident\":false,\"segments\":[1],\"numChildren\":0,"
            + "\"numInfants\":0,\"pnr\":\"PNR TEST\",\"residentValidations\":[],\"baggageProviderPrice\":0,\"id\":430808644,"
            + "\"bookingStatus\":\"CONTRACT\"},{\"numAdults\":1,\"resident\":false,\"segments\":[2],\"numChildren\":0,\"numInfants\":0,"
            + "\"pnr\":\"PNR TEST\",\"residentValidations\":[],\"baggageProviderPrice\":0,\"id\":430808645,"
            + "\"bookingStatus\":\"CONTRACT\"}],\"itinerarySegments\":[{\"duration\":75,\"sections\":[{\"id\":0,"
            + "\"section\":{\"id\":\"4201\",\"duration\":75,\"from\":408,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":\"320\","
            + "\"arrivalTerminal\":null,\"baggageAllowanceQuantity\":23,\"baggageAllowanceType\":\"WE\",\"carrier\":0,"
            + "\"departureDate\":1461315900000,\"arrivalDate\":1461320400000,\"departureTerminal\":null,\"fareInfoPassengers\":[],"
            + "\"operatingCarrier\":null,\"technicalStops\":[],\"to\":37,\"departureTime\":1461315900000,\"arrivalTime\":1461320520000,"
            + "\"cabinClassCode\":\"M\",\"eventReceived\":\"true\",\"operatingVendor\":\"Lan Airlines\",\"operatingVendorCode\":\"LA\","
            + "\"operatingVendorLegNumber\":\"4201\",\"baggageClaim\":null,\"departureGate\":\"7\",\"updatedDepartureTerminal\":\"1\","
            + "\"destinationIataCode\":\"AEP\",\"destinationAirportName\":\"Jorge Newbery Airport\",\"arrivalGate\":null,"
            + "\"updatedArrivalTerminal\":\"A\",\"flightStatus\":\"ARRIVED\",\"departureTimeType\":\"ACTUAL\","
            + "\"departureTimeDelay\":\"0\",\"arrivalTimeType\":\"ACTUAL\",\"arrivalTimeDelay\":\"2\"},\"sectionType\":\"ARRIVAL\","
            + "\"itineraryBookingId\":430808642}]},{\"duration\":1045,\"sections\":[{\"id\":1,\"section\":{\"id\":\"709\","
            + "\"duration\":170,\"from\":599,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":\"737\",\"arrivalTerminal\":null,"
            + "\"baggageAllowanceQuantity\":2,\"baggageAllowanceType\":\"NP\",\"carrier\":1,\"departureDate\":1461685800000,"
            + "\"arrivalDate\":1461692400000,\"departureTerminal\":\"A\",\"fareInfoPassengers\":[],\"operatingCarrier\":null,"
            + "\"technicalStops\":[],\"to\":2035,\"departureTime\":1461685800000,\"arrivalTime\":1461692400000,\"cabinClassCode\":\"M\","
            + "\"eventReceived\":\"false\",\"operatingVendor\":\"Boliviana De Aviacion\",\"operatingVendorCode\":\"OB\","
            + "\"operatingVendorLegNumber\":\"709\",\"baggageClaim\":null,\"departureGate\":null,\"updatedDepartureTerminal\":null,"
            + "\"destinationIataCode\":\"VVI\",\"destinationAirportName\":\"Viru Viru International Airport\",\"arrivalGate\":null,"
            + "\"updatedArrivalTerminal\":null,\"flightStatus\":null,\"departureTimeType\":null,\"departureTimeDelay\":null,"
            + "\"arrivalTimeType\":null,\"arrivalTimeDelay\":null},\"sectionType\":\"SCALE\",\"itineraryBookingId\":430808644},{\"id\":2,"
            + "\"section\":{\"id\":\"776\",\"duration\":660,\"from\":2035,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":\"767\","
            + "\"arrivalTerminal\":\"4S\",\"baggageAllowanceQuantity\":2,\"baggageAllowanceType\":\"NP\",\"carrier\":1,"
            + "\"departureDate\":1461705300000,\"arrivalDate\":1461766500000,\"departureTerminal\":null,\"fareInfoPassengers\":[],"
            + "\"operatingCarrier\":null,\"technicalStops\":[],\"to\":1147,\"departureTime\":1461705300000,\"arrivalTime\":1461766500000,"
            + "\"cabinClassCode\":\"M\",\"eventReceived\":\"false\",\"operatingVendor\":\"Boliviana De Aviacion\","
            + "\"operatingVendorCode\":\"OB\",\"operatingVendorLegNumber\":\"776\",\"baggageClaim\":null,\"departureGate\":null,"
            + "\"updatedDepartureTerminal\":null,\"destinationIataCode\":\"MAD\",\"destinationAirportName\":\"Adolfo Suárez "
            + "Madrid–Barajas Airport\",\"arrivalGate\":null,\"updatedArrivalTerminal\":null,\"flightStatus\":null,"
            + "\"departureTimeType\":null,\"departureTimeDelay\":null,\"arrivalTimeType\":null,\"arrivalTimeDelay\":null},"
            + "\"sectionType\":\"ARRIVAL\",\"itineraryBookingId\":430808644}]},{\"duration\":135,\"sections\":[{\"id\":3,"
            + "\"section\":{\"id\":\"5444\",\"duration\":135,\"from\":1147,\"cabinClass\":\"TOURIST\",\"aircraft\":null,"
            + "\"arrivalTerminal\":null,\"baggageAllowanceQuantity\":null,\"baggageAllowanceType\":null,\"carrier\":2,"
            + "\"departureDate\":1461825900000,\"arrivalDate\":1461834000000,\"departureTerminal\":null,\"fareInfoPassengers\":[],"
            + "\"operatingCarrier\":null,\"technicalStops\":[],\"to\":290,\"departureTime\":1461825900000,\"arrivalTime\":1461834000000,"
            + "\"cabinClassCode\":\"I\",\"eventReceived\":\"false\",\"operatingVendor\":\"Ryanair\",\"operatingVendorCode\":\"FR\","
            + "\"operatingVendorLegNumber\":\"5444\",\"baggageClaim\":null,\"departureGate\":null,\"updatedDepartureTerminal\":null,"
            + "\"destinationIataCode\":\"BVA\",\"destinationAirportName\":\"Beauvais–Tillé Airport\",\"arrivalGate\":null,"
            + "\"updatedArrivalTerminal\":null,\"flightStatus\":null,\"departureTimeType\":null,\"departureTimeDelay\":null,"
            + "\"arrivalTimeType\":null,\"arrivalTimeDelay\":null},\"sectionType\":\"ARRIVAL\",\"itineraryBookingId\":430808645}]}]},"
            + "\"insuranceBookings\":{\"bookings\":[]},\"bookingStatus\":\"CONTRACT\",\"collectionState\":\"COLLECTED\","
            + "\"bookingBasicInfo\":{\"id\":430808640,\"creationDate\":1461259301000,\"lastUpdate\":1461259335000,"
            + "\"clientBookingReferenceId\":\"430808640\",\"website\":{\"code\":\"UK\",\"brand\":\"ED\"},\"requestDate\":1461259301000},"
            + "\"sod\":false,\"travelCompanionNotifications\":true,\"enrichedBooking\":true}";

    // mail: gonzalo.molina@globant.com - booking id: 430826475 - eDreams QA14
    mTwoSegmentsMultiTripBooking =
        "{\"locale\":\"es_ES\",\"price\":{\"currency\":\"EUR\",\"amount\":1019.75},"
            + "\"buyer\":{\"address\":\"olayon\",\"name\":\"GONZA\",\"country\":\"DZ\",\"mail\":\"gonzalo.molina@globant.com\","
            + "\"phoneNumber\":{\"number\":\"255588525\",\"countryCode\":\"DZ\"},\"cityName\":\"cordoba\",\"identification\":null,"
            + "\"alternativePhoneNumber\":null,\"buyerIdentificationType\":null,\"cpf\":null,\"lastNames\":\"MOLINA\",\"stateName\":null,"
            + "\"zipCode\":\"dolor\"},\"travellers\":[{\"name\":\"gonza\",\"title\":\"MR\",\"countryCodeOfResidence\":null,"
            + "\"dateOfBirth\":-90000000,\"firstLastName\":\"molina\",\"identification\":null,\"identificationExpirationDate\":null,"
            + "\"identificationIssueCountryCode\":null,\"identificationType\":null,\"localityCodeOfResidence\":null,\"meal\":null,"
            + "\"middleName\":\"\",\"nationalityCountryCode\":null,\"secondLastName\":null,\"travellerGender\":null,"
            + "\"travellerType\":\"ADULT\",\"baggageSelections\":[{\"kilos\":0,\"pieces\":0,\"segmentIndex\":0},{\"kilos\":0,"
            + "\"pieces\":0,\"segmentIndex\":1}],\"numPassenger\":1,\"frequentFlyerCards\":[]}],"
            + "\"itineraryBookings\":{\"legend\":{\"carriers\":[{\"name\":\"Emirates\",\"id\":2,\"code\":\"EK\"},{\"name\":\"Renfe\","
            + "\"id\":1,\"code\":\"RNF\"},{\"name\":\"Renfe - AVE\",\"id\":0,\"code\":\"RNF\"}],\"locations\":[{\"name\":\"Barcelona "
            + "Sants\",\"type\":\"Train Station\",\"geoNodeId\":1064945,\"iataCode\":\"YJB\",\"cityName\":\"Barcelone\","
            + "\"countryCode\":\"ES\",\"cityIataCode\":\"BCN\",\"countryName\":\"Espagne\",\"timezone\":\"Europe/Madrid\"},"
            + "{\"name\":\"Madrid Atocha\",\"type\":\"Train Station\",\"geoNodeId\":1064946,\"iataCode\":\"XOC\",\"cityName\":\"Madrid\","
            + "\"countryCode\":\"ES\",\"cityIataCode\":\"MAD\",\"countryName\":\"Espagne\",\"timezone\":\"Europe/Madrid\"},"
            + "{\"name\":\"Adolfo Suárez Madrid - Barajas\",\"type\":\"Airport\",\"geoNodeId\":1147,\"iataCode\":\"MAD\","
            + "\"cityName\":\"Madrid\",\"countryCode\":\"ES\",\"cityIataCode\":\"MAD\",\"countryName\":\"Espagne\","
            + "\"timezone\":\"Europe/Madrid\"},{\"name\":\"Pistarini\",\"type\":\"Airport\",\"geoNodeId\":599,\"iataCode\":\"EZE\","
            + "\"cityName\":\"Buenos Aires\",\"countryCode\":\"AR\",\"cityIataCode\":\"BUE\",\"countryName\":\"Argentine\","
            + "\"timezone\":\"America/Argentina/Buenos_Aires\"},{\"name\":\"Dubai International Airport\",\"type\":\"Airport\","
            + "\"geoNodeId\":536,\"iataCode\":\"DXB\",\"cityName\":\"Dubaï\",\"countryCode\":\"AE\",\"cityIataCode\":\"DXB\","
            + "\"countryName\":\"Émirats Arabes Unis\",\"timezone\":\"Asia/Dubai\"}]},\"bookings\":[{\"numAdults\":1,\"resident\":false,"
            + "\"segments\":[0],\"numChildren\":0,\"numInfants\":0,\"pnr\":\"PNR TEST\",\"residentValidations\":[],"
            + "\"baggageProviderPrice\":0,\"id\":430826478,\"bookingStatus\":\"CONTRACT\"},{\"numAdults\":1,\"resident\":false,"
            + "\"segments\":[1],\"numChildren\":0,\"numInfants\":0,\"pnr\":\"PNR TEST\",\"residentValidations\":[],"
            + "\"baggageProviderPrice\":0,\"id\":430826477,\"bookingStatus\":\"CONTRACT\"}],\"itinerarySegments\":[{\"duration\":2095,"
            + "\"sections\":[{\"id\":1,\"section\":{\"id\":\"248\",\"duration\":1115,\"from\":599,\"cabinClass\":\"ECONOMIC_DISCOUNTED\","
            + "\"aircraft\":\"77W\",\"arrivalTerminal\":\"3\",\"baggageAllowanceQuantity\":2,\"baggageAllowanceType\":\"NP\","
            + "\"carrier\":2,\"departureDate\":1461879000000,\"arrivalDate\":1461971100000,\"departureTerminal\":\"A\","
            + "\"fareInfoPassengers\":[],\"operatingCarrier\":null,\"technicalStops\":[],\"to\":536,\"departureTime\":1461879000000,"
            + "\"arrivalTime\":1461971100000,\"cabinClassCode\":\"M\",\"eventReceived\":\"false\",\"operatingVendor\":\"Emirates\","
            + "\"operatingVendorCode\":\"EK\",\"operatingVendorLegNumber\":\"248\",\"baggageClaim\":null,\"departureGate\":null,"
            + "\"updatedDepartureTerminal\":null,\"destinationIataCode\":\"DXB\",\"destinationAirportName\":\"Dubai International "
            + "Airport\",\"arrivalGate\":null,\"updatedArrivalTerminal\":null,\"flightStatus\":null,\"departureTimeType\":null,"
            + "\"departureTimeDelay\":null,\"arrivalTimeType\":null,\"arrivalTimeDelay\":null},\"sectionType\":\"SCALE\","
            + "\"itineraryBookingId\":430826477},{\"id\":2,\"section\":{\"id\":\"141\",\"duration\":470,\"from\":536,"
            + "\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":\"388\",\"arrivalTerminal\":\"4S\",\"baggageAllowanceQuantity\":2,"
            + "\"baggageAllowanceType\":\"NP\",\"carrier\":2,\"departureDate\":1462001700000,\"arrivalDate\":1462022700000,"
            + "\"departureTerminal\":\"3\",\"fareInfoPassengers\":[],\"operatingCarrier\":null,\"technicalStops\":[],\"to\":1147,"
            + "\"departureTime\":1462001700000,\"arrivalTime\":1462022700000,\"cabinClassCode\":\"M\",\"eventReceived\":\"false\","
            + "\"operatingVendor\":\"Emirates\",\"operatingVendorCode\":\"EK\",\"operatingVendorLegNumber\":\"141\","
            + "\"baggageClaim\":null,\"departureGate\":null,\"updatedDepartureTerminal\":null,\"destinationIataCode\":\"MAD\","
            + "\"destinationAirportName\":\"Adolfo Suárez Madrid–Barajas Airport\",\"arrivalGate\":null,\"updatedArrivalTerminal\":null,"
            + "\"flightStatus\":null,\"departureTimeType\":null,\"departureTimeDelay\":null,\"arrivalTimeType\":null,"
            + "\"arrivalTimeDelay\":null},\"sectionType\":\"ARRIVAL\",\"itineraryBookingId\":430826477}]},{\"duration\":190,"
            + "\"sections\":[{\"id\":0,\"section\":{\"id\":\"3153\",\"duration\":190,\"from\":1064946,\"cabinClass\":\"TOURIST\","
            + "\"aircraft\":null,\"arrivalTerminal\":null,\"baggageAllowanceQuantity\":null,\"baggageAllowanceType\":null,\"carrier\":1,"
            + "\"departureDate\":1462030200000,\"arrivalDate\":1462041600000,\"departureTerminal\":null,\"fareInfoPassengers\":[],"
            + "\"operatingCarrier\":null,\"technicalStops\":[],\"to\":1064945,\"departureTime\":1462030200000,"
            + "\"arrivalTime\":1462041600000,\"cabinClassCode\":\"I\",\"eventReceived\":\"false\",\"operatingVendor\":\"Renfe\","
            + "\"operatingVendorCode\":\"RNF\",\"operatingVendorLegNumber\":\"3153\",\"baggageClaim\":null,\"departureGate\":null,"
            + "\"updatedDepartureTerminal\":null,\"destinationIataCode\":\"YJB\",\"destinationAirportName\":\"Barcelona Sants\","
            + "\"arrivalGate\":null,\"updatedArrivalTerminal\":null,\"flightStatus\":null,\"departureTimeType\":null,"
            + "\"departureTimeDelay\":null,\"arrivalTimeType\":null,\"arrivalTimeDelay\":null},\"sectionType\":\"ARRIVAL\","
            + "\"itineraryBookingId\":430826478}]}]},\"insuranceBookings\":{\"bookings\":[]},\"bookingStatus\":\"CONTRACT\","
            + "\"collectionState\":\"COLLECTED\",\"bookingBasicInfo\":{\"id\":430826475,\"creationDate\":1461762583000,"
            + "\"lastUpdate\":1461762616000,\"clientBookingReferenceId\":\"430826475\",\"website\":{\"code\":\"ES\",\"brand\":\"ED\"},"
            + "\"requestDate\":1461762583000},\"sod\":false,\"travelCompanionNotifications\":true,\"enrichedBooking\":true}";
  }

  @Test public void getBooking() {
    try {
      Booking booking = mBookingNetMapper.parseBooking(mJsonFake);
      System.out.println(booking.toString());
    } catch (JSONException e) {
      e.printStackTrace();
      assertNotNull(null);
    }
  }

  @Test public void testOneWayType() {
    try {
      Booking booking = mBookingNetMapper.parseBooking(mOneWayBooking);
      assertEquals(Booking.TRIP_TYPE_ONE_WAY, booking.getTripType());
    } catch (JSONException e) {
      e.printStackTrace();
      assertNotNull(null);
    }
  }

  @Test public void testRoundTripType() {
    try {
      Booking booking = mBookingNetMapper.parseBooking(mRoundTripBooking);
      assertEquals(Booking.TRIP_TYPE_ROUND_TRIP, booking.getTripType());
    } catch (JSONException e) {
      e.printStackTrace();
      assertNotNull(null);
    }
  }

  @Test public void testMultiTripType() {
    try {
      Booking booking = mBookingNetMapper.parseBooking(mMultiTripBooking);
      assertEquals(Booking.TRIP_TYPE_MULTI_SEGMENT, booking.getTripType());
    } catch (JSONException e) {
      e.printStackTrace();
      assertNotNull(null);
    }
  }

  @Test public void testTwoSegmentsMultiTripType() {
    try {
      Booking booking = mBookingNetMapper.parseBooking(mTwoSegmentsMultiTripBooking);
      assertEquals(Booking.TRIP_TYPE_MULTI_SEGMENT, booking.getTripType());
    } catch (JSONException e) {
      e.printStackTrace();
      assertNotNull(null);
    }
  }

  @Test public void testArrivalAirportCode() {
    try {
      Booking booking = mBookingNetMapper.parseBooking(mOneWayBooking);
      String expectedValue = "BHD";
      assertEquals(expectedValue, booking.getArrivalAirportCode());
    } catch (JSONException e) {
      e.printStackTrace();
      assertNotNull(null);
    }
  }

  @Test public void testArrivalLastLeg() {
    try {
      Booking booking = mBookingNetMapper.parseBooking(mOneWayBooking);
      long expectedValue = 1450174200000L;
      assertEquals(expectedValue, booking.getArrivalLastLeg());
    } catch (JSONException e) {
      e.printStackTrace();
      assertNotNull(null);
    }
  }

  @Test public void testDepartureFirstLeg() {
    try {
      Booking booking = mBookingNetMapper.parseBooking(mTwoSegmentsMultiTripBooking);
      long expectedValue = 1461879000000L;
      assertEquals(expectedValue, booking.getDepartureFirstLeg());
    } catch (JSONException e) {
      e.printStackTrace();
      assertNotNull(null);
    }
  }
}
