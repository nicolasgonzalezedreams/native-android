package com.odigeo.data.net.mapper;

import com.odigeo.data.entity.booking.SearchSegment;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.SearchSegmentNetMapper;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by Javier Marsicano on 16/08/16.
 */
@RunWith(RobolectricTestRunner.class) @Config(manifest = Config.NONE)
public class SearchSegmentNetMapperTest {

  private static final int SEARCH_SEGMENT_ID = 8736;
  private static final long DEPARTURE_DATE = 1495907200000L;
  private static final long RETURN_DATE = 1496252800000L;
  private static final String IATA_BUENOS_AIRES = "BUE";
  private static final String IATA_PARIS = "PAR";
  private static final int SEGMENT_ORDER = 0;

  private static final String SEARCH_SEGMENT_JSON = "{\"searchSegmentList\": [\n"
      + "        {\n"
      + "          \"id\":"
      + SEARCH_SEGMENT_ID
      + ",\n"
      + "          \"origin\": \"BUE\",\n"
      + "          \"departureDate\": 1495907200000,\n"
      + "          \"destination\": \"PAR\",\n"
      + "          \"returnDate\": 1496252800000L,\n"
      + "          \"segmentOrder\": 0\n"
      + "        }\n"
      + "      ]}";

  private SearchSegmentNetMapper mSearchSegmentNetMapper;

  @Before public void setUp() {
    mSearchSegmentNetMapper = new SearchSegmentNetMapper();
  }

  private SearchSegment getSearchSegment() {
    return new SearchSegment(0, SEARCH_SEGMENT_ID, IATA_BUENOS_AIRES, IATA_PARIS, DEPARTURE_DATE,
        RETURN_DATE, SEGMENT_ORDER);
  }

  @Test public void jsonObjectToSearchSegmentTest() throws JSONException {
    JSONObject jsonObject = new JSONObject();
    JSONObject searchSegmentJsonArray = new JSONObject(SEARCH_SEGMENT_JSON);
    jsonObject = searchSegmentJsonArray.getJSONArray(JsonKeys.SEARCH_SEGMENT_LIST).getJSONObject(0);

    SearchSegment mappedSearchSegment =
        mSearchSegmentNetMapper.mapperJsonObjectToSearchSegment(jsonObject, 0);
    validateSearchSegment(mappedSearchSegment);
  }

  @Test public void jsonArrayToSearchSegmentListTest() throws JSONException {
    JSONArray jsonArray = new JSONArray();
    JSONObject searchSegmentJsonArray = new JSONObject(SEARCH_SEGMENT_JSON);
    jsonArray = searchSegmentJsonArray.getJSONArray(JsonKeys.SEARCH_SEGMENT_LIST);
    List<SearchSegment> searchSegmentList =
        mSearchSegmentNetMapper.mapperJsonArrayToSearchSegmentList(jsonArray, 0);
    assertNotNull(searchSegmentList);
    validateSearchSegment(searchSegmentList.get(0));
  }

  private void validateSearchSegment(SearchSegment searchSegment) {
    assertNotNull(searchSegment);
    assertEquals(SEARCH_SEGMENT_ID, searchSegment.getSearchSegmentId());
    assertEquals(DEPARTURE_DATE, searchSegment.getDepartureDate());
    assertEquals(IATA_BUENOS_AIRES, searchSegment.getOriginIATACode());
  }

  @Test public void searchSegmentToJsonObjectTest() throws JSONException {
    JSONObject mappedSearchSegment =
        mSearchSegmentNetMapper.mapperSearchSegmentToJsonObject(getSearchSegment());
    assertNotNull(mappedSearchSegment);
    validateJsonObject(mappedSearchSegment);
  }

  @Test public void searchSegmentListToJsonTest() throws JSONException {
    List<SearchSegment> searchSegmentList = new ArrayList<>();
    searchSegmentList.add(getSearchSegment());
    JSONArray jsonArray = mSearchSegmentNetMapper.mapperSearchSegmentListToJson(searchSegmentList);
    validateJsonObject(jsonArray.getJSONObject(0));
  }

  private void validateJsonObject(JSONObject jsonObject) throws JSONException {
    assertEquals(SEARCH_SEGMENT_ID, jsonObject.getInt(JsonKeys.ID));
    assertEquals(DEPARTURE_DATE, jsonObject.getLong(JsonKeys.DEPARTURE_DATE));
    assertEquals(IATA_BUENOS_AIRES, jsonObject.getString(JsonKeys.ORIGIN_IATA_CODE));
  }
}
