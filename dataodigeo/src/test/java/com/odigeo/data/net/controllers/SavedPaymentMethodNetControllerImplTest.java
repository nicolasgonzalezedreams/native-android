package com.odigeo.data.net.controllers;

import android.content.Context;
import com.google.gson.GsonBuilder;
import com.odigeo.data.entity.userData.InsertCreditCardRequest;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.net.volley.VolleyDependenciesProvider;
import com.odigeo.dataodigeo.net.controllers.SavePaymentMethodNetControllerImpl;
import com.odigeo.dataodigeo.net.controllers.TokenController;
import com.odigeo.dataodigeo.net.helper.DomainHelper;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import com.odigeo.dataodigeo.session.SessionController;
import com.odigeo.dataodigeo.session.UserInfoInterface;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class) public class SavedPaymentMethodNetControllerImplTest {

  @Mock OnRequestDataListener<User> callback;
  @Mock private RequestHelper requestHelper;
  @Mock private SessionController sessionController;
  @Mock private UserInfoInterface userInfoInterface;
  @Mock private TokenController tokenController;

  private Context applicationContext;
  private HeaderHelperInterface headerHelperInterface;
  private DomainHelper domainHelper;
  private SavePaymentMethodNetController savedPaymentMethodNetController;
  private InsertCreditCardRequest randomCreditCard;
  private HashMap<String, String> mMapHeaders;
  private String headerElement;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    applicationContext = RuntimeEnvironment.application.getApplicationContext();
    headerHelperInterface = VolleyDependenciesProvider.provideHeaderHelper(applicationContext);
    domainHelper = VolleyDependenciesProvider.provideDomainHelper();

    savedPaymentMethodNetController =
        new SavePaymentMethodNetControllerImpl(requestHelper, domainHelper, headerHelperInterface,
            tokenController, new GsonBuilder().serializeNulls().create(), sessionController);
  }

  @Test public void shouldAddHeaders() {
    givenRandomCreditCardAndUserInfo();

    savedPaymentMethodNetController.savePaymentMethod(callback, randomCreditCard);

    mMapHeaders = savedPaymentMethodNetController.getMapHeaders();

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.ACCEPT_ENCODING);
    assertEquals(headerElement, "gzip,deflate,sdch");

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.ACCEPT);
    assertEquals(headerElement, "application/vnd.com.odigeo.msl.v4+json;charset=utf-8");

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.DEVICE_ID);
    assertNotNull(headerElement);

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.CONTENT_TYPE);
    assertNull(headerElement);
  }

  @Test public void shouldInsertCreditCard() {
    givenRandomCreditCardAndUserInfo();

    savedPaymentMethodNetController.savePaymentMethod(callback, randomCreditCard);

    verify(requestHelper).addRequest(any(MslRequest.class));
  }

  private void givenRandomCreditCardAndUserInfo() {
    randomCreditCard = givenRandomCreditCard();
    when(sessionController.getUserInfo()).thenReturn(userInfoInterface);
    when(userInfoInterface.getUserId()).thenReturn(1234L);
  }

  private InsertCreditCardRequest givenRandomCreditCard() {
    return new InsertCreditCardRequest("123456789", "12", "21", "jimy", 0, 0, 0, false, "VI");
  }
}
