package com.odigeo.data.net.controllers;

import android.content.Context;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.net.volley.VolleyDependenciesProvider;
import com.odigeo.dataodigeo.net.controllers.TokenController;
import com.odigeo.dataodigeo.net.controllers.VisitUserNetControllerImpl;
import com.odigeo.dataodigeo.net.helper.DomainHelper;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.HashMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class) public class VisitUserNetControllerImplTest {

  private final long userId = 1;
  @Mock private OnRequestDataListener<String> onRequestDataListenerInteractor;
  @Mock private RequestHelper requestHelper;
  @Mock private TokenController tokenController;
  @Captor private ArgumentCaptor<OnRequestDataListener<String>> onRequestDataListenerData;
  private VisitUserNetController visitUserNetController;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    Context applicationContext = RuntimeEnvironment.application.getApplicationContext();
    HeaderHelperInterface headerHelperInterface =
        VolleyDependenciesProvider.provideHeaderHelper(applicationContext);
    DomainHelper domainHelper = VolleyDependenciesProvider.provideDomainHelper();

    visitUserNetController =
        new VisitUserNetControllerImpl(requestHelper, domainHelper, headerHelperInterface,
            tokenController);
  }

  @Test public void shouldLogVisitUser() {
    visitUserNetController.loginVisitUser(onRequestDataListenerInteractor, userId);
  }

  @Test public void shouldLogoutUser() {
    visitUserNetController.logoutVisitUser(onRequestDataListenerInteractor, userId);
  }

  @After public void shouldAddHeaders() {
    HashMap<String, String> mMapHeaders;
    String headerElement;

    mMapHeaders = visitUserNetController.getMapHeaders();

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.ACCEPT_ENCODING);
    assertEquals(headerElement, "gzip,deflate,sdch");

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.ACCEPT);
    assertEquals(headerElement, "application/vnd.com.odigeo.msl.v4+json;charset=utf-8");

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.DEVICE_ID);
    assertNotNull(headerElement);

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.CONTENT_TYPE);
    assertNull(headerElement);

    verify(requestHelper).addRequest(any(MslRequest.class));
  }
}
