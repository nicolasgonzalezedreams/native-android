package com.odigeo.data.net.mapper;

import com.odigeo.data.entity.booking.Guide;
import com.odigeo.dataodigeo.net.mapper.ArrivalGuideNetMapper;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class) public class ArrivalGuideNetMapperTest {
  private ArrivalGuideNetMapper mArrivalGuideNetMapper;
  private String jsonFake;

  @Before public void setUp() {
    mArrivalGuideNetMapper = new ArrivalGuideNetMapper();
    jsonFake =
        "{\"to\":{\"location\":{\"latitude\":40.416741,\"longitude\":-3.70325,\"cityName\":\"Madrid\",\"countryCode\":\"ES\"},\"currency\":{\"currency\":\"EUR\",\"exchangeRate\":1},\"guide\":{\"language\":\"en\",\"url\":\"https://api.arrivalguides.com/en/Dynamic/Download?dest=MADRID&partner=GOVOYAGES\"}}}";
  }

  @Test public void testGetArrivalGuide() {

    try {
      Guide guide = mArrivalGuideNetMapper.parseGuide(jsonFake, 1);
      assertEquals(guide.getUrl(),
          "https://api.arrivalguides.com/en/Dynamic/Download?dest=MADRID&partner=GOVOYAGES");
      assertEquals(guide.getLanguage(), "en");
      System.out.println(guide.toString());
    } catch (JSONException e) {
      e.printStackTrace();
      assertNotNull(null);
    }
  }
}
