package com.odigeo.data.net.controllers;

import com.android.volley.AuthFailureError;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.controllers.SearchesNetController;
import com.odigeo.dataodigeo.net.controllers.TokenController;
import com.odigeo.dataodigeo.net.helper.DomainHelper;
import com.odigeo.dataodigeo.net.helper.MslAuthRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static com.odigeo.test.mock.MocksProvider.providesStoredSearchMocks;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by Javier Marsicano on 12/08/16.
 */
@RunWith(RobolectricTestRunner.class) @Config(manifest = Config.NONE)
public class SearchesNetControllerTest {
  @Mock private RequestHelper mRequestHelper;
  @Mock private HeaderHelperInterface mHeaderHelper;
  @Mock private TokenController mTokenController;
  @Mock private OnRequestDataListener<StoredSearch> mOnRequestDataListener;

  private SearchesNetController searchesNetController;
  private StoredSearch mStoredSearch;
  private DomainHelperInterface mDomainHelper;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);

    mStoredSearch = providesStoredSearchMocks().provideStoredSearchWithSegment();
    mDomainHelper = DomainHelper.newInstance("http://test.odigeo.com/");
    searchesNetController =
        new SearchesNetController(mRequestHelper, mDomainHelper, mHeaderHelper, mTokenController);
  }

  @Test public void postUserSearchTestVerifyCorrectBody() {
    searchesNetController.postUserSearch(mOnRequestDataListener, mStoredSearch);

    ArgumentCaptor<MslAuthRequest> captor = ArgumentCaptor.forClass(MslAuthRequest.class);
    Mockito.verify(mRequestHelper).addRequest(captor.capture());
    MslAuthRequest request = captor.getValue();
    try {
      JSONObject body = new JSONObject(new String(request.getBody()));
      assertNotNull(body);
      assertEquals(body.getString(JsonKeys.TRIP_TYPE), StoredSearch.TripType.O.name());

      JSONArray jsonArray = body.getJSONArray(JsonKeys.SEARCH_SEGMENT_LIST);
      assertNotNull(jsonArray);
      assertEquals(mStoredSearch.getSegmentList().size(), jsonArray.length());
      assertEquals(mStoredSearch.getSegmentList().get(0).getOriginIATACode(),
          jsonArray.getJSONObject(0).getString(JsonKeys.ORIGIN_IATA_CODE));
      assertEquals(mStoredSearch.getSegmentList().get(0).getDepartureDate(),
          jsonArray.getJSONObject(0).getLong(JsonKeys.DEPARTURE_DATE));
    } catch (AuthFailureError authFailureError) {
      authFailureError.printStackTrace();
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }

  @Test public void postUserSearchTestVerifyHeaderAssembling() {
    searchesNetController.postUserSearch(mOnRequestDataListener, mStoredSearch);
    Mockito.verify(mHeaderHelper).putDeviceId(Matchers.any(Map.class));
    Mockito.verify(mHeaderHelper).putAcceptEncoding(Matchers.any(Map.class));
    Mockito.verify(mHeaderHelper).putAccept(Matchers.any(Map.class));
    Mockito.verify(mHeaderHelper).putContentType(Matchers.any(Map.class));
  }
}
