package com.odigeo.data.net.mapper;

import com.odigeo.data.entity.userData.Membership;
import com.odigeo.dataodigeo.constants.JsonKeys;
import com.odigeo.dataodigeo.net.mapper.MembershipNetMapper;
import com.odigeo.test.mock.mocks.UserMocks;
import java.util.List;
import junit.framework.Assert;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class) public class MembershipNetMapperTest {

  public static final String correctJson = "[{\"memberId\":"
      + UserMocks.MEMBER_ID
      + ", \"name\":\""
      + UserMocks.NAME
      + "\", "
      + "\"lastNames\":\""
      + UserMocks.SURNAME
      + "\", \"website\":\""
      + UserMocks.WEBSITE_MEMBERSHIP
      + "\"}, "
      + "{\"memberId\":123, "
      + "\"name\":\"Paco\", "
      + "\"lastNames\":\"Copa\", \"website\":\"en_UK\"}]";

  public static final String incorrectJson = "[1, 2, 3]";

  @Rule public ExpectedException expectedException = ExpectedException.none();

  @Test public void testParseValidJson() throws JSONException {
    List<Membership> memberships =
        MembershipNetMapper.mapperJSONArrayToMembershipList(new JSONArray(correctJson));

    Assert.assertEquals(memberships.size(), 2);
    Assert.assertEquals(memberships.get(0).getMemberId(), UserMocks.MEMBER_ID);
    Assert.assertEquals(memberships.get(0).getFirstName(), UserMocks.NAME);
    Assert.assertEquals(memberships.get(0).getLastNames(), UserMocks.SURNAME);
    Assert.assertEquals(memberships.get(0).getWebsite(), UserMocks.WEBSITE_MEMBERSHIP);
  }

  @Test public void testParseIncorrectJson() throws JSONException {
    expectedException.expect(JSONException.class);

    MembershipNetMapper.mapperJSONArrayToMembershipList(new JSONArray(incorrectJson));
  }

  @Test public void testParseValidMembershipList() throws JSONException {
    List<Membership> memberships = UserMocks.provideDefaultMemberships();
    JSONArray membershipJson = MembershipNetMapper.mapperMembershipListToJsonArray(memberships);

    Assert.assertEquals(membershipJson.length(), 1);
    Assert.assertEquals(membershipJson.getJSONObject(0).getLong(JsonKeys.MEMBER_ID),
        UserMocks.MEMBER_ID);
    Assert.assertEquals(membershipJson.getJSONObject(0).getString(JsonKeys.MEMBER_NAME),
        UserMocks.NAME);
    Assert.assertEquals(membershipJson.getJSONObject(0).getString(JsonKeys.MEMBER_LASTNAMES),
        UserMocks.SURNAME);
    Assert.assertEquals(membershipJson.getJSONObject(0).getString(JsonKeys.MEMBER_WEBSITE),
        UserMocks.WEBSITE_MEMBERSHIP);
  }
}
