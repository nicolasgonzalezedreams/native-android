package com.odigeo.data.net.mapper;

import com.odigeo.data.entity.geo.City;
import com.odigeo.dataodigeo.net.mapper.CityMapper;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 3/05/16
 */
@RunWith(RobolectricTestRunner.class) public class CityMapperTest {

  public static final String mockJson =
      "{\"cities\":[{\"name\":\"Mexico City\",\"type\":\"CITY\",\"geoNodeId\":3838,\"iataCode\":\"MEX\","
          + "\"cityName\":\"Mexico City\",\"coordinates\":{\"latitude\":19.43,\"longitude\":-99.139999},\"countryCode\":\"MX\","
          + "\"countryName\":\"Mexico\",\"locationNames\":[\"México D.F - Ciudad de México\",\"Mexiko-Stadt\",\"Mexico City\"],"
          + "\"geoNodeType\":\"CITY\",\"relatedLocations\":[{\"name\":\"Juarez Intl Airport\",\"type\":\"AIRPORT\",\"geoNodeId\":1187,\"iataCode\":"
          + "\"MEX\",\"cityName\":\"Mexico City\",\"coordinates\":{\"latitude\":19.433333,\"longitude\":-99.083333},\"countryCode\":\"MX\",\""
          + "countryName\":\"Mexico\",\"locationNames\":[\"Juarez Intl Airport\"],\"geoNodeType\":\"AIRPORT\",\"relatedLocations\":null}]},"
          + "{\"name\":\"Mexicali\",\"type\":\"CITY\",\"geoNodeId\":2914,\"iataCode\":\"MXL\",\"cityName\":\"Mexicali\","
          + "\"coordinates\":{\"latitude\":32.650001,\"longitude\":-115.470001},\"countryCode\":\"MX\",\"countryName\":\"Mexico\",\"locationNames\""
          + ":[\"Mexicali\"],\"geoNodeType\":\"CITY\",\"relatedLocations\":[{\"name\":\"Mexicali\",\"type\":\"AIRPORT\",\"geoNodeId\":1283,\""
          + "iataCode\":\"MXL\",\"cityName\":\"Mexicali\",\"coordinates\":{\"latitude\":32.65,\"longitude\":-115.45},\"countryCode\":\"MX\",\""
          + "countryName\":\"Mexico\",\"locationNames\":[\"Mexicali\"],\"geoNodeType\":\"AIRPORT\",\"relatedLocations\":null}]}]}";

  private CityMapper mCityMapper;

  @Before public void setUp() {
    mCityMapper = new CityMapper();
  }

  @Test public void testParseValidJson() {
    List<City> cities = mCityMapper.parseCities(mockJson);
    Assert.assertNotNull(cities);
    Assert.assertEquals(cities.size(), 2);
    Assert.assertEquals(cities.get(0).getRelatedLocations().size(), 1);
    Assert.assertNull(cities.get(0).getRelatedLocations().get(0).getRelatedLocations());
  }

  @Test public void testParseInvalidJson() {
    List<City> cities = mCityMapper.parseCities(mockJson.substring(1));
    Assert.assertNull(cities);
  }
}
