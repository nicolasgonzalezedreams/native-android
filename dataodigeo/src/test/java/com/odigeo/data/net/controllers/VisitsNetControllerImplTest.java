package com.odigeo.data.net.controllers;

import android.content.Context;
import com.google.gson.GsonBuilder;
import com.odigeo.data.entity.session.Visit;
import com.odigeo.data.entity.session.request.VisitRequest;
import com.odigeo.data.net.helper.HeaderHelperInterface;
import com.odigeo.data.net.listener.OnRequestDataListener;
import com.odigeo.data.net.volley.VolleyDependenciesProvider;
import com.odigeo.dataodigeo.net.controllers.VisitsNetControllerImpl;
import com.odigeo.dataodigeo.net.helper.DomainHelper;
import com.odigeo.dataodigeo.net.helper.MslRequest;
import com.odigeo.dataodigeo.net.helper.RequestHelper;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class) public class VisitsNetControllerImplTest {

  @Mock private OnRequestDataListener<Visit> onRequestDataListenerInteractor;
  @Mock private RequestHelper requestHelper;
  @Captor private ArgumentCaptor<OnRequestDataListener<String>> mOnRequestDataListenerData;
  private VisitRequest visitRequest;
  private VisitsNetController visitsNetController;
  private Context applicationContext;
  private HeaderHelperInterface headerHelperInterface;
  private DomainHelper domainHelper;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    applicationContext = RuntimeEnvironment.application.getApplicationContext();
    headerHelperInterface = VolleyDependenciesProvider.provideHeaderHelper(applicationContext);
    domainHelper = VolleyDependenciesProvider.provideDomainHelper();

    visitsNetController =
        new VisitsNetControllerImpl(requestHelper, domainHelper, headerHelperInterface,
            new GsonBuilder().serializeNulls().create());

    visitsNetController.getVisits(onRequestDataListenerInteractor, visitRequest);
  }

  @Test public void shouldAddHeaders() {
    HashMap<String, String> mMapHeaders;
    String headerElement;

    mMapHeaders = visitsNetController.getMapHeaders();

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.ACCEPT_ENCODING);
    assertEquals(headerElement, "gzip,deflate,sdch");

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.ACCEPT);
    assertEquals(headerElement, "application/vnd.com.odigeo.msl.v4+json;charset=utf-8");

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.DEVICE_ID);
    assertNotNull(headerElement);

    headerElement = mMapHeaders.get(VolleyDependenciesProvider.CONTENT_TYPE);
    assertNull(headerElement);
  }

  @Test public void shouldCallGetVisitsRequest() {
    verify(requestHelper).addRequest(any(MslRequest.class));
  }
}
