package com.odigeo.data.net.mapper;

import com.odigeo.dataodigeo.carousel.CarouselMockProvider;
import com.odigeo.dataodigeo.net.mapper.CarrouselNetMapper;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by matia on 1/26/2016.
 */
//@RunWith(RobolectricTestRunner.class)
public class CarrouselNetMapperTest {

  private CarrouselNetMapper mCarrouselNetMapper;
  private CarouselMockProvider mCarouselMockProvider;
  private String jsonFake;

  @Before public void setUp() {
    mCarrouselNetMapper = new CarrouselNetMapper();
    mCarouselMockProvider = new CarouselMockProvider();
  }

  @Test public void testGetCarousel() {

  }

/*    @Test
    public void testGetCarrousel() {
        try {
            Carousel carousel = mCarrouselNetMapper.jsonCarrouselMapper(mCarouselMockProvider.provideFullMockCarousel());
            assertEquals(carousel.getLastUpdate(), 1453731268621L);
            assertEquals(carousel.getCarrouselCards().size(), 7);
            SectionCard sectionCard = (SectionCard) carousel.getCarrouselCards().get(0);
            assertEquals(sectionCard.getType(), Card.CardType.SECTION_CARD);
            assertEquals(sectionCard.getCarrier(), "UX");
            assertEquals(sectionCard.getFlightId(), "6106");
            assertEquals(sectionCard.getCarrierName(), "AirEuropa");
            assertEquals(sectionCard.getId(), -1933816910);
            assertEquals(sectionCard.getPriority(), 0);
            assertNull(sectionCard.getImage());
            CarouselLocation from = sectionCard.getFrom();
            assertEquals(from.getDate(), 1464272700000L);
            assertEquals(from.getDescription(), "PalmadeMaiorca");
            assertEquals(from.getIata(), "PMI");
            assertNull(from.getTerminal());
            CarouselLocation to = sectionCard.getTo();
            assertEquals(to.getDate(), 1464275700000L);
            assertEquals(to.getDescription(), "Barcelona");
            assertEquals(to.getIata(), "BCN");
            assertNull(to.getTerminal());
            System.out.println(carousel.toString());
        }
        catch (JSONException e) {
            e.printStackTrace();
            assertNotNull(null);
        }
    } */
}
