package com.odigeo.data.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.data.net.helper.DomainHelperInterface;
import com.odigeo.dataodigeo.net.mapper.CarrouselNetMapper;
import com.odigeo.dataodigeo.preferences.CarouselManager;
import com.odigeo.dataodigeo.preferences.PreferencesController;
import com.odigeo.interactors.provider.MarketProviderInterface;
import org.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class) public class CarouselManagerTest {

  private static final String SHARED_PREFERENCES_LAST_UPDATE_BOOKING_STATUS = "lastupdatebooking";
  private long randomLastUpdate = 1234567;
  private CarouselManager mCarouselManager;
  private String urlImageDestination =
      "http://www.edreams.com/images/mobile/govoyages/destination/600x440/MAD@2x.jpg";
  private LocationBooking locationBooking = new LocationBooking();
  private PreferencesControllerInterface mPreferencesController;

  @Mock private DomainHelperInterface domainHelper;
  @Mock private MarketProviderInterface marketProvider;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);
    mPreferencesController =
        new PreferencesController(RuntimeEnvironment.application.getApplicationContext(),
            domainHelper, marketProvider);
    mCarouselManager = new CarouselManager(RuntimeEnvironment.application, mPreferencesController);
    initLocationCodeForGetImageTest();
    clearSharedPrefs();
  }

  @After public void tearDown() {
    clearSharedPrefs();
  }

  private void initLocationCodeForGetImageTest() {
    locationBooking.setLocationCode("MAD");
  }

  private void clearSharedPrefs() {
    SharedPreferences sharedPreferences = RuntimeEnvironment.application.getSharedPreferences(
        SHARED_PREFERENCES_LAST_UPDATE_BOOKING_STATUS, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPreferences.edit();
    editor.clear();
    editor.apply();
  }

  @Test public void testCarouselManagerMapper() {
    String json = "";
    mCarouselManager.carouselManagerMapper(json);
    try {
      Mockito.verify(CarrouselNetMapper.jsonCarrouselMapper(json));
    } catch (JSONException e) {
      Log.e("CarouselManagerTest", "CarouselManagerTest: Error parsing Json");
    }
  }

  @Test public void testGetCarouselLastUpdate() {
    SharedPreferences sharedPreferences = RuntimeEnvironment.application.getSharedPreferences(
        SHARED_PREFERENCES_LAST_UPDATE_BOOKING_STATUS, Context.MODE_PRIVATE);
    sharedPreferences.edit()
        .putLong(PreferencesControllerInterface.LAST_UPDATE_BOOKING_STATUS, randomLastUpdate)
        .apply();
    long savedLastUpdate =
        sharedPreferences.getLong(SHARED_PREFERENCES_LAST_UPDATE_BOOKING_STATUS, 0);
    assertEquals(savedLastUpdate, randomLastUpdate);
  }

  @Test public void testDeleteCarouselLastUpdate() {
    SharedPreferences sharedPreferences = RuntimeEnvironment.application.getSharedPreferences(
        SHARED_PREFERENCES_LAST_UPDATE_BOOKING_STATUS, Context.MODE_PRIVATE);
    sharedPreferences.edit()
        .putLong(PreferencesControllerInterface.LAST_UPDATE_BOOKING_STATUS, 0)
        .apply();
    long savedLastUpdate =
        sharedPreferences.getLong(SHARED_PREFERENCES_LAST_UPDATE_BOOKING_STATUS, 0);
    assertEquals(savedLastUpdate, 0);
  }

  @Test public void testGetCarouselImage() {
    String path = "http://www.edreams.com/images/mobile/govoyages/destination/600x440/";
    String image = path + locationBooking.getLocationCode() + "@2x.jpg";
    assertEquals(image, urlImageDestination);
  }
}