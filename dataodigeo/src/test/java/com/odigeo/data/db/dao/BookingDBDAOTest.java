package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.BookingDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class BookingDBDAOTest extends DbDaoTest<BookingDBDAO> {

  private Booking mBooking;

  @Override protected BookingDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new BookingDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mBooking =
        new Booking(2, "arrivalAirportCode", 0, "bookingStatus", "currency", 0, "locale", "market",
            3, "sortCriteria", 4, 5, "tripType", 1, false, false, false);
  }

  @Test public void addBookingAndGetBookingTest() {
    long rowId;

    rowId = mDbDao.addBooking(mBooking);

    assertNotEquals(rowId, -1);

    Booking booking = mDbDao.getBooking(mBooking.getBookingId());

    assertEquals(mBooking.getBookingId(), booking.getBookingId());
    assertEquals(mBooking.getArrivalAirportCode(), booking.getArrivalAirportCode());
    assertEquals(mBooking.getArrivalLastLeg(), booking.getArrivalLastLeg());
    assertEquals(mBooking.getBookingStatus(), booking.getBookingStatus());
    assertEquals(mBooking.getCurrency(), booking.getCurrency());
    assertEquals(mBooking.getDepartureFirstLeg(), booking.getDepartureFirstLeg());
    assertEquals(mBooking.getLocale(), booking.getLocale());
    assertEquals(mBooking.getMarket(), booking.getMarket());
    assertEquals(mBooking.getPrice(), booking.getPrice(), 1e-15);
    assertEquals(mBooking.getSortCriteria(), booking.getSortCriteria());
    assertEquals(mBooking.getTimestamp(), booking.getTimestamp());
    assertEquals(mBooking.getTotal(), booking.getTotal());
    assertEquals(mBooking.getTripType(), booking.getTripType());
    assertEquals(mBooking.getIsSoundOfData(), booking.getIsSoundOfData());
    assertEquals(mBooking.getIsTravelCompanionNotification(),
        booking.getIsTravelCompanionNotification());
    assertEquals(mBooking.getIsEnrichedBooking(), booking.getIsEnrichedBooking());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}