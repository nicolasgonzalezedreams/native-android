package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.userData.User;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.UserDBDAO;
import java.lang.reflect.Field;
import java.util.Calendar;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class UserDBDAOTest extends DbDaoTest<UserDBDAO> {

  private User mUser;

  @Override protected UserDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new UserDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mUser = new User(1, 2, "javier@rebollo.com", "javierwebsite.com", false, true,
        Calendar.getInstance().getTimeInMillis(), Calendar.getInstance().getTimeInMillis(), "es_ES",
        null, User.Source.REGISTERED, User.Status.ACTIVE);
  }

  @Test public void addUserAndGetUserTest() {
    long rowId = mDbDao.addUser(mUser);
    assertNotEquals(rowId, -1);
    User user = mDbDao.getUser(mUser.getEmail());
    assertEquals(user.getUserId(), mUser.getUserId());
    assertEquals(user.getEmail(), mUser.getEmail());
    assertEquals(user.getWebSite(), mUser.getWebSite());
    assertEquals(user.getAcceptsNewsletter(), mUser.getAcceptsNewsletter());
    assertEquals(user.getAcceptsPartnersInfo(), mUser.getAcceptsPartnersInfo());
    assertEquals(user.getCreationDate(), mUser.getCreationDate());
    assertEquals(user.getLastModified(), mUser.getLastModified());
    assertEquals(user.getLocale(), mUser.getLocale());
    assertEquals(user.getMarketingPortal(), mUser.getMarketingPortal());
    assertEquals(user.getSource(), mUser.getSource());
    assertEquals(user.getStatus(), mUser.getStatus());
  }

  @Test public void updateSuccessTest() {
    long rowId = mDbDao.addUser(mUser);
    assertNotEquals(rowId, -1);
    User newUserData = new User(rowId, 3, "javier2@rebollo.com", "javierwebsite2.com", true, false,
        Calendar.getInstance().getTimeInMillis(), Calendar.getInstance().getTimeInMillis(), "es_MX",
        null, User.Source.FACEBOOK, User.Status.LEGACY);
    assertTrue(mDbDao.updateUser(mUser.getEmail(), newUserData));
    User user = mDbDao.getUser(newUserData.getEmail());
    assertEquals(user.getId(), newUserData.getId());
    assertEquals(user.getUserId(), newUserData.getUserId());
    assertEquals(user.getEmail(), newUserData.getEmail());
    assertEquals(user.getWebSite(), newUserData.getWebSite());
    assertEquals(user.getAcceptsNewsletter(), newUserData.getAcceptsNewsletter());
    assertEquals(user.getAcceptsPartnersInfo(), newUserData.getAcceptsPartnersInfo());
    assertEquals(user.getCreationDate(), newUserData.getCreationDate());
    assertEquals(user.getLastModified(), newUserData.getLastModified());
    assertEquals(user.getLocale(), newUserData.getLocale());
    assertEquals(user.getMarketingPortal(), newUserData.getMarketingPortal());
    assertEquals(user.getSource(), newUserData.getSource());
    assertEquals(user.getStatus(), newUserData.getStatus());
  }

  @Test public void updateFailTest() {
    assertFalse(mDbDao.updateUser(mUser.getEmail(), mUser));
  }

  @Test public void updateOrCreateCreateTest() {
    assertTrue(mDbDao.updateOrCreateUser(mUser));
  }

  @Test public void updateOrCreateUpdateTest() {
    long rowId = mDbDao.addUser(mUser);
    assertNotEquals(rowId, -1);
    assertTrue(mDbDao.updateOrCreateUser(mUser));
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
