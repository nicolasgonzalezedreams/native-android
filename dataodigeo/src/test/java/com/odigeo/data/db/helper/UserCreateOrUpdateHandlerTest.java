package com.odigeo.data.db.helper;

import com.odigeo.data.db.dao.MembershipDBDAOInterface;
import com.odigeo.data.db.dao.SearchSegmentDBDAOInterface;
import com.odigeo.data.db.dao.StoredSearchDBDAOInterface;
import com.odigeo.data.db.dao.UserAddressDBDAOInterface;
import com.odigeo.data.db.dao.UserDBDAOInterface;
import com.odigeo.data.db.dao.UserFrequentFlyerDBDAOInterface;
import com.odigeo.data.db.dao.UserIdentificationDBDAOInterface;
import com.odigeo.data.db.dao.UserProfileDBDAOInterface;
import com.odigeo.data.db.dao.UserTravellerDBDAOInterface;
import com.odigeo.data.entity.booking.SearchSegment;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.data.entity.userData.Membership;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.entity.userData.UserAddress;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.data.tracker.CrashlyticsController;
import com.odigeo.dataodigeo.db.helper.UserCreateOrUpdateHandler;
import com.odigeo.test.mock.mocks.UserMocks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class) public class UserCreateOrUpdateHandlerTest {

  @Mock private UserDBDAOInterface userDBDAOInterface;
  @Mock private UserTravellerDBDAOInterface userTravellerDBDAOInterface;
  @Mock private UserProfileDBDAOInterface userProfileDBDAOInterface;
  @Mock private UserAddressDBDAOInterface userAddressDBDAOInterface;
  @Mock private UserFrequentFlyerDBDAOInterface userFrequentFlyerDBDAOInterface;
  @Mock private UserIdentificationDBDAOInterface userIdentificationDBDAOInterface;
  @Mock private TravellersHandlerInterface travellersHandlerInterface;
  @Mock private StoredSearchDBDAOInterface storedSearchDBDAOInterface;
  @Mock private SearchSegmentDBDAOInterface searchSegmentDBDAOInterface;
  @Mock private MembershipDBDAOInterface membershipDBDAOInterface;
  @Mock private CrashlyticsController crashlyticsController;

  private UserCreateOrUpdateHandler userCreateOrUpdateHandler;
  private User user;

  @Before public void setUp() {
    MockitoAnnotations.initMocks(this);

    user = new UserMocks.Builder().build();

    userCreateOrUpdateHandler =
        new UserCreateOrUpdateHandler(userDBDAOInterface, userTravellerDBDAOInterface,
            userProfileDBDAOInterface, userAddressDBDAOInterface, userFrequentFlyerDBDAOInterface,
            userIdentificationDBDAOInterface, travellersHandlerInterface,
            storedSearchDBDAOInterface, searchSegmentDBDAOInterface, membershipDBDAOInterface,
            crashlyticsController);
  }

  @Test public void userHasTravellersAndInfoIsStored() {
    when(userDBDAOInterface.updateOrCreateUser(any(User.class))).thenReturn(true);
    when(userTravellerDBDAOInterface.updateOrCreateUserTraveller(
        any(UserTraveller.class))).thenReturn(true);
    when(userProfileDBDAOInterface.updateOrCreateUserProfile(any(UserProfile.class))).thenReturn(
        true);
    when(userAddressDBDAOInterface.updateOrCreateUserAddress(any(UserAddress.class))).thenReturn(
        true);
    when(userIdentificationDBDAOInterface.updateOrCreateUserIdentification(
        any(UserIdentification.class))).thenReturn(true);
    when(storedSearchDBDAOInterface.updateStoredSearch(any(StoredSearch.class))).thenReturn(true);
    when(searchSegmentDBDAOInterface.updateSearchSegment(anyLong(),
        any(SearchSegment.class))).thenReturn(true);
    when(membershipDBDAOInterface.addMembership(any(Membership.class))).thenReturn(true);

    assertTrue(userCreateOrUpdateHandler.addOrCreateUserAndAllComponents(user));

    verify(userTravellerDBDAOInterface, times(1)).updateOrCreateUserTraveller(
        user.getUserTravellerList().get(0));
    verify(userProfileDBDAOInterface, times(1)).updateOrCreateUserProfile(
        user.getUserTravellerList().get(0).getUserProfile());
    verify(userAddressDBDAOInterface, times(1)).updateOrCreateUserAddress(
        user.getUserTravellerList().get(0).getUserProfile().getUserAddress());
    verify(userIdentificationDBDAOInterface, times(1)).updateOrCreateUserIdentification(
        user.getUserTravellerList().get(0).getUserProfile().getUserIdentificationList().get(0));
    verify(userIdentificationDBDAOInterface, times(0)).deleteUserIdentification(
        user.getUserTravellerList()
            .get(0)
            .getUserProfile()
            .getUserIdentificationList()
            .get(0)
            .getUserIdentificationId());
  }

  @Test public void userHasTravellersAndFailToStoreUserTravellerInfo() {
    when(userDBDAOInterface.updateOrCreateUser(any(User.class))).thenReturn(true);
    when(userTravellerDBDAOInterface.updateOrCreateUserTraveller(
        any(UserTraveller.class))).thenReturn(false);
    when(userProfileDBDAOInterface.updateOrCreateUserProfile(any(UserProfile.class))).thenReturn(
        true);
    when(userAddressDBDAOInterface.updateOrCreateUserAddress(any(UserAddress.class))).thenReturn(
        true);
    when(userIdentificationDBDAOInterface.updateOrCreateUserIdentification(
        any(UserIdentification.class))).thenReturn(true);
    when(storedSearchDBDAOInterface.updateStoredSearch(any(StoredSearch.class))).thenReturn(true);
    when(searchSegmentDBDAOInterface.updateSearchSegment(anyLong(),
        any(SearchSegment.class))).thenReturn(true);
    when(membershipDBDAOInterface.addMembership(any(Membership.class))).thenReturn(true);

    assertFalse(userCreateOrUpdateHandler.addOrCreateUserAndAllComponents(user));
  }

  @Test public void userHasTravellersAndFailToStoreUserProfileInfo() {
    when(userDBDAOInterface.updateOrCreateUser(any(User.class))).thenReturn(true);
    when(userTravellerDBDAOInterface.updateOrCreateUserTraveller(
        any(UserTraveller.class))).thenReturn(true);
    when(userProfileDBDAOInterface.updateOrCreateUserProfile(any(UserProfile.class))).thenReturn(
        false);
    when(userAddressDBDAOInterface.updateOrCreateUserAddress(any(UserAddress.class))).thenReturn(
        true);
    when(userIdentificationDBDAOInterface.updateOrCreateUserIdentification(
        any(UserIdentification.class))).thenReturn(true);
    when(storedSearchDBDAOInterface.updateStoredSearch(any(StoredSearch.class))).thenReturn(true);
    when(searchSegmentDBDAOInterface.updateSearchSegment(anyLong(),
        any(SearchSegment.class))).thenReturn(true);
    when(membershipDBDAOInterface.addMembership(any(Membership.class))).thenReturn(true);

    assertFalse(userCreateOrUpdateHandler.addOrCreateUserAndAllComponents(user));
  }

  @Test public void userHasTravellersAndFailToStoreUserAddressInfo() {
    when(userDBDAOInterface.updateOrCreateUser(any(User.class))).thenReturn(true);
    when(userTravellerDBDAOInterface.updateOrCreateUserTraveller(
        any(UserTraveller.class))).thenReturn(true);
    when(userProfileDBDAOInterface.updateOrCreateUserProfile(any(UserProfile.class))).thenReturn(
        true);
    when(userAddressDBDAOInterface.updateOrCreateUserAddress(any(UserAddress.class))).thenReturn(
        false);
    when(userIdentificationDBDAOInterface.updateOrCreateUserIdentification(
        any(UserIdentification.class))).thenReturn(true);
    when(storedSearchDBDAOInterface.updateStoredSearch(any(StoredSearch.class))).thenReturn(true);
    when(searchSegmentDBDAOInterface.updateSearchSegment(anyLong(),
        any(SearchSegment.class))).thenReturn(true);
    when(membershipDBDAOInterface.addMembership(any(Membership.class))).thenReturn(true);

    assertFalse(userCreateOrUpdateHandler.addOrCreateUserAndAllComponents(user));
  }

  @Test public void userHasTravellersAndFailToStoreUserIdentificationInfo() {
    when(userDBDAOInterface.updateOrCreateUser(any(User.class))).thenReturn(true);
    when(userTravellerDBDAOInterface.updateOrCreateUserTraveller(
        any(UserTraveller.class))).thenReturn(true);
    when(userProfileDBDAOInterface.updateOrCreateUserProfile(any(UserProfile.class))).thenReturn(
        true);
    when(userAddressDBDAOInterface.updateOrCreateUserAddress(any(UserAddress.class))).thenReturn(
        true);
    when(userIdentificationDBDAOInterface.updateOrCreateUserIdentification(
        any(UserIdentification.class))).thenReturn(false);
    when(storedSearchDBDAOInterface.updateStoredSearch(any(StoredSearch.class))).thenReturn(true);
    when(searchSegmentDBDAOInterface.updateSearchSegment(anyLong(),
        any(SearchSegment.class))).thenReturn(true);
    when(membershipDBDAOInterface.addMembership(any(Membership.class))).thenReturn(true);

    assertFalse(userCreateOrUpdateHandler.addOrCreateUserAndAllComponents(user));
  }

  @Test public void userHasStoredSearchesAndInfoIsStored() {
    when(userDBDAOInterface.updateOrCreateUser(any(User.class))).thenReturn(true);
    when(userTravellerDBDAOInterface.updateOrCreateUserTraveller(
        any(UserTraveller.class))).thenReturn(true);
    when(userProfileDBDAOInterface.updateOrCreateUserProfile(any(UserProfile.class))).thenReturn(
        true);
    when(userAddressDBDAOInterface.updateOrCreateUserAddress(any(UserAddress.class))).thenReturn(
        true);
    when(userIdentificationDBDAOInterface.updateOrCreateUserIdentification(
        any(UserIdentification.class))).thenReturn(true);
    when(storedSearchDBDAOInterface.updateStoredSearch(any(StoredSearch.class))).thenReturn(true);
    when(searchSegmentDBDAOInterface.updateSearchSegment(anyLong(),
        any(SearchSegment.class))).thenReturn(true);
    when(membershipDBDAOInterface.addMembership(any(Membership.class))).thenReturn(true);

    assertTrue(userCreateOrUpdateHandler.addOrCreateUserAndAllComponents(user));

    verify(storedSearchDBDAOInterface, times(1)).updateStoredSearch(
        user.getUserSearchesList().get(0));

    verify(searchSegmentDBDAOInterface, times(1)).updateSearchSegment(
        user.getUserSearchesList().get(0).getSegmentList().get(0).getSearchSegmentId(),
        user.getUserSearchesList().get(0).getSegmentList().get(0));
    verify(searchSegmentDBDAOInterface, times(1)).updateSearchSegment(
        user.getUserSearchesList().get(0).getSegmentList().get(1).getSearchSegmentId(),
        user.getUserSearchesList().get(0).getSegmentList().get(1));
  }

  @Test public void userHasStoredSearchsAndFailToStoreStoredSearchInfo() {
    when(userDBDAOInterface.updateOrCreateUser(any(User.class))).thenReturn(true);
    when(userTravellerDBDAOInterface.updateOrCreateUserTraveller(
        any(UserTraveller.class))).thenReturn(true);
    when(userProfileDBDAOInterface.updateOrCreateUserProfile(any(UserProfile.class))).thenReturn(
        true);
    when(userAddressDBDAOInterface.updateOrCreateUserAddress(any(UserAddress.class))).thenReturn(
        true);
    when(userIdentificationDBDAOInterface.updateOrCreateUserIdentification(
        any(UserIdentification.class))).thenReturn(true);
    when(storedSearchDBDAOInterface.updateStoredSearch(any(StoredSearch.class))).thenReturn(false);
    when(storedSearchDBDAOInterface.addStoredSearch(any(StoredSearch.class))).thenReturn(-1l);
    when(searchSegmentDBDAOInterface.updateSearchSegment(anyLong(),
        any(SearchSegment.class))).thenReturn(true);
    when(membershipDBDAOInterface.addMembership(any(Membership.class))).thenReturn(true);

    assertFalse(userCreateOrUpdateHandler.addOrCreateUserAndAllComponents(user));
  }

  @Test public void userHasStoredSearchsAndFailToStoreSearchSegmentInfo() {
    when(userDBDAOInterface.updateOrCreateUser(any(User.class))).thenReturn(true);
    when(userTravellerDBDAOInterface.updateOrCreateUserTraveller(
        any(UserTraveller.class))).thenReturn(true);
    when(userProfileDBDAOInterface.updateOrCreateUserProfile(any(UserProfile.class))).thenReturn(
        true);
    when(userAddressDBDAOInterface.updateOrCreateUserAddress(any(UserAddress.class))).thenReturn(
        true);
    when(userIdentificationDBDAOInterface.updateOrCreateUserIdentification(
        any(UserIdentification.class))).thenReturn(true);
    when(storedSearchDBDAOInterface.updateStoredSearch(any(StoredSearch.class))).thenReturn(true);
    when(searchSegmentDBDAOInterface.updateSearchSegment(anyLong(),
        any(SearchSegment.class))).thenReturn(false);
    when(membershipDBDAOInterface.addMembership(any(Membership.class))).thenReturn(true);

    assertFalse(userCreateOrUpdateHandler.addOrCreateUserAndAllComponents(user));
  }

  @Test public void userIsAMemberAndMembershipInfoIsStored() {
    when(userDBDAOInterface.updateOrCreateUser(any(User.class))).thenReturn(true);
    when(userTravellerDBDAOInterface.updateOrCreateUserTraveller(
        any(UserTraveller.class))).thenReturn(true);
    when(userProfileDBDAOInterface.updateOrCreateUserProfile(any(UserProfile.class))).thenReturn(
        true);
    when(userAddressDBDAOInterface.updateOrCreateUserAddress(any(UserAddress.class))).thenReturn(
        true);
    when(userIdentificationDBDAOInterface.updateOrCreateUserIdentification(
        any(UserIdentification.class))).thenReturn(true);
    when(storedSearchDBDAOInterface.updateStoredSearch(any(StoredSearch.class))).thenReturn(true);
    when(searchSegmentDBDAOInterface.updateSearchSegment(anyLong(),
        any(SearchSegment.class))).thenReturn(true);
    when(membershipDBDAOInterface.addMembership(any(Membership.class))).thenReturn(true);

    assertTrue(userCreateOrUpdateHandler.addOrCreateUserAndAllComponents(user));

    verify(membershipDBDAOInterface, times(1)).addMembership(user.getMemberships().get(0));
  }

  @Test public void userIsNotAMemberAndNoMembershipInfoIsStored() {
    user = new UserMocks.Builder().setMembershipInfo(new ArrayList<Membership>()).build();

    when(userDBDAOInterface.updateOrCreateUser(any(User.class))).thenReturn(true);
    when(userTravellerDBDAOInterface.updateOrCreateUserTraveller(
        any(UserTraveller.class))).thenReturn(true);
    when(userProfileDBDAOInterface.updateOrCreateUserProfile(any(UserProfile.class))).thenReturn(
        true);
    when(userAddressDBDAOInterface.updateOrCreateUserAddress(any(UserAddress.class))).thenReturn(
        true);
    when(userIdentificationDBDAOInterface.updateOrCreateUserIdentification(
        any(UserIdentification.class))).thenReturn(true);
    when(storedSearchDBDAOInterface.updateStoredSearch(any(StoredSearch.class))).thenReturn(true);
    when(searchSegmentDBDAOInterface.updateSearchSegment(anyLong(),
        any(SearchSegment.class))).thenReturn(true);

    assertTrue(userCreateOrUpdateHandler.addOrCreateUserAndAllComponents(user));

    verifyZeroInteractions(membershipDBDAOInterface);
  }

  @Test public void deleteUserTravellerSuccess() {

    when(userFrequentFlyerDBDAOInterface.deleteUserFrequentFlyer(anyLong())).thenReturn(1l);
    when(userIdentificationDBDAOInterface.deleteUserIdentification(anyLong())).thenReturn(true);
    when(userAddressDBDAOInterface.deleteUserAddress(anyLong())).thenReturn(true);
    when(userProfileDBDAOInterface.deleteUserProfile(anyLong())).thenReturn(true);
    when(userTravellerDBDAOInterface.deleteUserTraveller(anyLong())).thenReturn(true);

    assertTrue(userCreateOrUpdateHandler.deleteUserTravellerCompletely(
        user.getUserTravellerList().get(0)));

    verify(userIdentificationDBDAOInterface).deleteUserIdentification(user.getUserTravellerList()
        .get(0)
        .getUserProfile()
        .getUserIdentificationList()
        .get(0)
        .getUserIdentificationId());
    verify(userAddressDBDAOInterface).deleteUserAddress(
        user.getUserTravellerList().get(0).getUserProfile().getUserAddress().getUserAddressId());
    verify(userProfileDBDAOInterface).deleteUserProfile(
        user.getUserTravellerList().get(0).getUserProfile().getUserProfileId());
    verify(userTravellerDBDAOInterface).deleteUserTraveller(
        user.getUserTravellerList().get(0).getUserTravellerId());
  }

  @Test public void deleteUserTravellerFailDeletingIdentification() {

    when(userFrequentFlyerDBDAOInterface.deleteUserFrequentFlyer(anyLong())).thenReturn(1l);
    when(userIdentificationDBDAOInterface.deleteUserIdentification(anyLong())).thenReturn(false);
    when(userAddressDBDAOInterface.deleteUserAddress(anyLong())).thenReturn(true);
    when(userProfileDBDAOInterface.deleteUserProfile(anyLong())).thenReturn(true);
    when(userTravellerDBDAOInterface.deleteUserTraveller(anyLong())).thenReturn(true);

    assertFalse(userCreateOrUpdateHandler.deleteUserTravellerCompletely(
        user.getUserTravellerList().get(0)));
  }

  @Test public void deleteUserTravellerFailDeletingAddress() {

    when(userFrequentFlyerDBDAOInterface.deleteUserFrequentFlyer(anyLong())).thenReturn(1l);
    when(userIdentificationDBDAOInterface.deleteUserIdentification(anyLong())).thenReturn(true);
    when(userAddressDBDAOInterface.deleteUserAddress(anyLong())).thenReturn(false);
    when(userProfileDBDAOInterface.deleteUserProfile(anyLong())).thenReturn(true);
    when(userTravellerDBDAOInterface.deleteUserTraveller(anyLong())).thenReturn(true);

    assertFalse(userCreateOrUpdateHandler.deleteUserTravellerCompletely(
        user.getUserTravellerList().get(0)));
  }

  @Test public void deleteUserTravellerFailDeletingProfile() {

    when(userFrequentFlyerDBDAOInterface.deleteUserFrequentFlyer(anyLong())).thenReturn(1l);
    when(userIdentificationDBDAOInterface.deleteUserIdentification(anyLong())).thenReturn(true);
    when(userAddressDBDAOInterface.deleteUserAddress(anyLong())).thenReturn(true);
    when(userProfileDBDAOInterface.deleteUserProfile(anyLong())).thenReturn(false);
    when(userTravellerDBDAOInterface.deleteUserTraveller(anyLong())).thenReturn(true);

    assertFalse(userCreateOrUpdateHandler.deleteUserTravellerCompletely(
        user.getUserTravellerList().get(0)));
  }

  @Test public void deleteUserTravellerFailDeletingUserTraveller() {

    when(userFrequentFlyerDBDAOInterface.deleteUserFrequentFlyer(anyLong())).thenReturn(1l);
    when(userIdentificationDBDAOInterface.deleteUserIdentification(anyLong())).thenReturn(true);
    when(userAddressDBDAOInterface.deleteUserAddress(anyLong())).thenReturn(true);
    when(userProfileDBDAOInterface.deleteUserProfile(anyLong())).thenReturn(true);
    when(userTravellerDBDAOInterface.deleteUserTraveller(anyLong())).thenReturn(false);

    assertFalse(userCreateOrUpdateHandler.deleteUserTravellerCompletely(
        user.getUserTravellerList().get(0)));
  }
}
