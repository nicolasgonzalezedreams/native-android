package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.userData.UserLogin;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.UserLoginDBDAO;
import java.lang.reflect.Field;
import java.util.Calendar;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class UserLoginDBDAOTest extends DbDaoTest<UserLoginDBDAO> {

  private UserLogin mUserLogin;

  @Override protected UserLoginDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new UserLoginDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mUserLogin = new UserLogin((long) 1, (long) 12, "Falso123", "adgadskjahdojhadgaopga",
        Calendar.getInstance().getTimeInMillis(), "HashCode", "ActivationCode",
        Calendar.getInstance().getTimeInMillis(), (long) 1234567890,
        Calendar.getInstance().getTimeInMillis(), "nose", (long) 13);
  }

  @Test public void addUserLoginAndGetUserLoginTest() {
    long rowId = mDbDao.addUserLogin(mUserLogin);
    assertNotEquals(rowId, -1);
    UserLogin userLogin = mDbDao.getUserLogin(mUserLogin.getId());
    assertEquals(userLogin.getId(), mUserLogin.getId());
    assertEquals(userLogin.getUserLoginId(), mUserLogin.getUserLoginId());
    assertEquals(userLogin.getPassword(), mUserLogin.getPassword());
    assertEquals(userLogin.getAccessToken(), mUserLogin.getAccessToken());
    assertEquals(userLogin.getExpirationTokenDate(), mUserLogin.getExpirationTokenDate());
    assertEquals(userLogin.getHashCode(), mUserLogin.getHashCode());
    assertEquals(userLogin.getActivationCode(), mUserLogin.getActivationCode());
    assertEquals(userLogin.getActivationCodeDate(), mUserLogin.getActivationCodeDate());
    assertEquals(userLogin.getLoginAttemptFailed(), mUserLogin.getLoginAttemptFailed());
    assertEquals(userLogin.getLastLoginDate(), mUserLogin.getLastLoginDate());
    assertEquals(userLogin.getRefreshToken(), mUserLogin.getRefreshToken());
    assertEquals(userLogin.getUserId(), mUserLogin.getUserId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
