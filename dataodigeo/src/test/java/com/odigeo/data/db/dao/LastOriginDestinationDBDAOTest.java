package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.LastOriginDestination;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.LastOriginDestinationDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class LastOriginDestinationDBDAOTest extends DbDaoTest<LastOriginDestinationDBDAO> {

  private LastOriginDestination mLastOriginDestination;

  @Override protected LastOriginDestinationDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new LastOriginDestinationDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mLastOriginDestination =
        new LastOriginDestination("cityIATACode", "cityName", "country", 0, 2, "locationCode",
            "locationName", "locationType", 3, "matchName", true, 4);
  }

  @Test public void addLastOriginDestinationAndGetLastOriginDestinationTest() {
    long rowId = mDbDao.addLastOriginDestination(mLastOriginDestination);
    assertNotEquals(rowId, -1);
    LastOriginDestination lastOriginDestination =
        mDbDao.getLastOriginDestination(mLastOriginDestination.getGeoNodeId());
    assertEquals(lastOriginDestination.getCityIATACode(), mLastOriginDestination.getCityIATACode());
    assertEquals(lastOriginDestination.getCityName(), mLastOriginDestination.getCityName());
    assertEquals(lastOriginDestination.getCountry(), mLastOriginDestination.getCountry());
    assertEquals(lastOriginDestination.getGeoNodeId(), mLastOriginDestination.getGeoNodeId());
    assertEquals(lastOriginDestination.getLatitude(), mLastOriginDestination.getLatitude());
    assertEquals(lastOriginDestination.getLocationCode(), mLastOriginDestination.getLocationCode());
    assertEquals(lastOriginDestination.getLocationName(), mLastOriginDestination.getLocationName());
    assertEquals(lastOriginDestination.getLocationType(), mLastOriginDestination.getLocationType());
    assertEquals(lastOriginDestination.getLongitude(), mLastOriginDestination.getLongitude());
    assertEquals(lastOriginDestination.getMatchName(), mLastOriginDestination.getMatchName());
    assertEquals(lastOriginDestination.getIsOrigin(), mLastOriginDestination.getIsOrigin());
    assertEquals(lastOriginDestination.getTimestamp(), mLastOriginDestination.getTimestamp());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
