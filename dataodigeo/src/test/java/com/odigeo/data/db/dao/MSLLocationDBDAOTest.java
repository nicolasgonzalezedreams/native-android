package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.MSLLocation;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.MSLLocationDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MSLLocationDBDAOTest extends DbDaoTest<MSLLocationDBDAO> {

  private MSLLocation mMSLLocation;

  @Override protected MSLLocationDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new MSLLocationDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mMSLLocation =
        new MSLLocation("cityIATACode", "cityName", "country", 0, 2, "locationCode", "locationName",
            "locationType", 3, "matchName");
  }

  @Test public void addMSLLocationAndGetMSLLocationTest() {
    long rowId = mDbDao.addMSLLocation(mMSLLocation);
    assertNotEquals(rowId, -1);
    MSLLocation locationBooking = mDbDao.getMSLLocation(mMSLLocation.getGeoNodeId());
    assertEquals(locationBooking.getCityIATACode(), mMSLLocation.getCityIATACode());
    assertEquals(locationBooking.getCityName(), mMSLLocation.getCityName());
    assertEquals(locationBooking.getCountry(), mMSLLocation.getCountry());
    assertEquals(locationBooking.getGeoNodeId(), mMSLLocation.getGeoNodeId());
    assertEquals(locationBooking.getLatitude(), mMSLLocation.getLatitude());
    assertEquals(locationBooking.getLocationCode(), mMSLLocation.getLocationCode());
    assertEquals(locationBooking.getLocationName(), mMSLLocation.getLocationName());
    assertEquals(locationBooking.getLocationType(), mMSLLocation.getLocationType());
    assertEquals(locationBooking.getLongitude(), mMSLLocation.getLongitude());
    assertEquals(locationBooking.getMatchName(), mMSLLocation.getMatchName());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
