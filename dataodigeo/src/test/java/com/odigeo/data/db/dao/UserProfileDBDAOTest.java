package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.userData.UserProfile;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.UserProfileDBDAO;
import java.lang.reflect.Field;
import java.util.Calendar;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class UserProfileDBDAOTest extends DbDaoTest<UserProfileDBDAO> {

  private UserProfile mUserProfile;

  @Override protected UserProfileDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new UserProfileDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mUserProfile =
        new UserProfile((long) 1, (long) 12, "Masculino", UserProfile.Title.MR, "Javier", null,
            "Rebollo", "Cinta", Calendar.getInstance().getTimeInMillis(), "+34", "999999999", "+34",
            "666666666", "333333333", "ES", null, false, "ruta de la foto to wapa", null, null,
            (long) 13);
  }

  @Test public void addUserProfileAndGetUserProfileTest() {
    long rowId = mDbDao.addUserProfile(mUserProfile);
    assertNotEquals(rowId, -1);
    UserProfile userProfile = mDbDao.getUserProfile(mUserProfile.getUserProfileId());
    assertEquals(userProfile.getUserProfileId(), mUserProfile.getUserProfileId());
    assertEquals(userProfile.getGender(), mUserProfile.getGender());
    assertEquals(userProfile.getTitle(), mUserProfile.getTitle());
    assertEquals(userProfile.getName(), mUserProfile.getName());
    assertEquals(userProfile.getMiddleName(), mUserProfile.getMiddleName());
    assertEquals(userProfile.getFirstLastName(), mUserProfile.getFirstLastName());
    assertEquals(userProfile.getSecondLastName(), mUserProfile.getSecondLastName());
    assertEquals(userProfile.getBirthDate(), mUserProfile.getBirthDate());
    assertEquals(userProfile.getPrefixPhoneNumber(), mUserProfile.getPrefixPhoneNumber());
    assertEquals(userProfile.getPhoneNumber(), mUserProfile.getPhoneNumber());
    assertEquals(userProfile.getPrefixAlternatePhoneNumber(),
        mUserProfile.getPrefixAlternatePhoneNumber());
    assertEquals(userProfile.getAlternatePhoneNumber(), mUserProfile.getAlternatePhoneNumber());
    assertEquals(userProfile.getMobilePhoneNumber(), mUserProfile.getMobilePhoneNumber());
    assertEquals(userProfile.getNationalityCountryCode(), mUserProfile.getNationalityCountryCode());
    assertEquals(userProfile.getCpf(), mUserProfile.getCpf());
    assertEquals(userProfile.getPhoto(), mUserProfile.getPhoto());
    assertEquals(userProfile.getUserTravellerId(), mUserProfile.getUserTravellerId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
