package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.userData.UserAirportPreferences;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.UserAirportPreferencesDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class UserAirportPreferencesDBDAOTest extends DbDaoTest<UserAirportPreferencesDBDAO> {

  private UserAirportPreferences mUserAirportPreferences;

  @Override protected UserAirportPreferencesDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new UserAirportPreferencesDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mUserAirportPreferences =
        new UserAirportPreferences((long) 1, (long) 12, "XLE-1025", (long) 12);
  }

  @Test public void addUserAirportPreferencesAndGetUserAirportPreferencesTest() {
    long rowId = mDbDao.addUserAirportPreferences(mUserAirportPreferences);
    assertNotEquals(rowId, -1);
    UserAirportPreferences userAirportPreferences =
        mDbDao.getUserAirportPreferences(mUserAirportPreferences.getId());
    assertEquals(userAirportPreferences.getId(), mUserAirportPreferences.getId());
    assertEquals(userAirportPreferences.getUserAirportPreferencesId(),
        mUserAirportPreferences.getUserAirportPreferencesId());
    assertEquals(userAirportPreferences.getAirport(), mUserAirportPreferences.getAirport());
    assertEquals(userAirportPreferences.getUserId(), mUserAirportPreferences.getUserId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
