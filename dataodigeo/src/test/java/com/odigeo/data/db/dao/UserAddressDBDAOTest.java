package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.userData.UserAddress;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.UserAddressDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class UserAddressDBDAOTest extends DbDaoTest<UserAddressDBDAO> {

  private UserAddress mUserAddress;

  @Override protected UserAddressDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new UserAddressDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mUserAddress =
        new UserAddress(1, 2, "Falsa, 123", "Calle", "Sevilla", "Sevilla", "Sevilla", "41500", true,
            "Casa", 3);
  }

  @Test public void addUserAddressAndGetUserAddressTest() {
    long rowId = mDbDao.addUserAddress(mUserAddress);
    assertNotEquals(rowId, -1);
    UserAddress userAddress = mDbDao.getUserAddress(mUserAddress.getUserAddressId());
    assertEquals(userAddress.getUserAddressId(), mUserAddress.getUserAddressId());
    assertEquals(userAddress.getAddress(), mUserAddress.getAddress());
    assertEquals(userAddress.getAddressType(), mUserAddress.getAddressType());
    assertEquals(userAddress.getCity(), mUserAddress.getCity());
    assertEquals(userAddress.getCountry(), mUserAddress.getCountry());
    assertEquals(userAddress.getState(), mUserAddress.getState());
    assertEquals(userAddress.getPostalCode(), mUserAddress.getPostalCode());
    assertEquals(userAddress.getIsPrimary(), mUserAddress.getIsPrimary());
    assertEquals(userAddress.getAlias(), mUserAddress.getAlias());
    assertEquals(userAddress.getUserProfileId(), mUserAddress.getUserProfileId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
