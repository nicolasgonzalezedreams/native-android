package com.odigeo.data.db.dao;

import android.content.Context;

import com.odigeo.data.entity.userData.Membership;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.MembershipDBDAO;

import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class MembershipDBDAOTest extends DbDaoTest<MembershipDBDAO> {

  private Membership membership;

  @Override protected MembershipDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new MembershipDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    membership = new Membership();
    membership.setMemberId(123324l);
    membership.setFirstName("FirstName");
    membership.setLastNames("Surname Surname");
    membership.setWebsite("ES_es");
  }

  @Test public void addMembershipAngGetMembershipTest() {
    boolean added = mDbDao.addMembership(membership);
    assertTrue(added);

    Membership membershipRetrieved = mDbDao.getMembershipForMarket(membership.getWebsite());

    assertEquals(membershipRetrieved.getMemberId(), membership.getMemberId());
    assertEquals(membershipRetrieved.getFirstName(), membership.getFirstName());
    assertEquals(membershipRetrieved.getLastNames(), membership.getLastNames());
    assertEquals(membershipRetrieved.getWebsite(), membership.getWebsite());
  }

  @Test public void addMembershipAndClearMembershipTest() {
    boolean added = mDbDao.addMembership(membership);
    assertTrue(added);

    boolean deleted = mDbDao.clearMembership();
    assertTrue(deleted);

    Membership membershipRetrieved = mDbDao.getMembershipForMarket(membership.getWebsite());
    assertNull(membershipRetrieved);
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
