package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.SearchSegment;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.SearchSegmentDBDAO;
import java.lang.reflect.Field;
import java.util.List;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@Config(manifest = Config.NONE) public class SearchSegmentDBDAOTest
    extends DbDaoTest<SearchSegmentDBDAO> {

  private static final String DEPARTURE_CITY = "BCN";
  private static final String ARRIVAL_CITY = "MAD";
  private static final int ID = 1;
  private static final int SEGMENT_ID = 1;
  private static final int SEGMENT_ORDER = 0;
  private static final long DEPARTURE_DATE = 143000000L;
  private static final int PARENT_ID = 5770;
  private SearchSegment mSearchSegment;

  @Override protected SearchSegmentDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new SearchSegmentDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mSearchSegment = new SearchSegment(ID, SEGMENT_ID, DEPARTURE_CITY, ARRIVAL_CITY, DEPARTURE_DATE,
        SEGMENT_ORDER, PARENT_ID);
  }

  @Test public void addSearchSegmentAndGetSearchSegmentTest() {
    boolean isAdded = mDbDao.addSearchSegment(mSearchSegment);
    assertTrue(isAdded);
    SearchSegment searchSegment = mDbDao.getSearchSegment(mSearchSegment.getId());
    verifySearchSegments(searchSegment, mSearchSegment);
  }

  private void verifySearchSegments(SearchSegment actual, SearchSegment expected) {
    assertNotNull(actual);
    assertEquals(actual.getId(), expected.getId());
    assertEquals(actual.getSearchSegmentId(), expected.getSearchSegmentId());
    assertEquals(actual.getOriginIATACode(), expected.getOriginIATACode());
    assertEquals(actual.getDestinationIATACode(), expected.getDestinationIATACode());
    assertEquals(actual.getDepartureDate(), expected.getDepartureDate());
    assertEquals(actual.getSegmentOrder(), expected.getSegmentOrder());
    assertEquals(actual.getParentSearchId(), expected.getParentSearchId());
  }

  @Test public void updateSearchSegmentTest_succes() {
    boolean isAdded = mDbDao.addSearchSegment(mSearchSegment);
    assertTrue(isAdded);
    SearchSegment otherSearchSegment =
        new SearchSegment(ID, SEGMENT_ID, ARRIVAL_CITY, DEPARTURE_CITY, DEPARTURE_DATE,
            SEGMENT_ORDER, PARENT_ID);
    isAdded = mDbDao.updateSearchSegment(SEGMENT_ID, otherSearchSegment);
    assertTrue(isAdded);
    SearchSegment actual = mDbDao.getSearchSegment(SEGMENT_ID);
    verifySearchSegments(actual, otherSearchSegment);
  }

  @Test public void updateSearchSegmentTest_fail() {
    boolean isAdded = mDbDao.addSearchSegment(mSearchSegment);
    assertTrue(isAdded);
    SearchSegment otherSearchSegment =
        new SearchSegment(ID, SEGMENT_ID, ARRIVAL_CITY, DEPARTURE_CITY, DEPARTURE_DATE,
            SEGMENT_ORDER, PARENT_ID);
    isAdded = mDbDao.updateSearchSegment(0, otherSearchSegment);
    assertTrue(!isAdded);
  }

  @Test public void getSearchSegmentListTest_success() {
    boolean isAdded = mDbDao.addSearchSegment(mSearchSegment);
    assertTrue(isAdded);
    List<SearchSegment> result = mDbDao.getSearchSegmentList(mSearchSegment.getParentSearchId());
    assertNotNull(result);
    assertEquals(1, result.size());
    verifySearchSegments(mSearchSegment, result.get(0));
  }

  @Test public void getSearchSegmentListTest_fail() {
    boolean isAdded = mDbDao.addSearchSegment(mSearchSegment);
    assertTrue(isAdded);
    List<SearchSegment> result = mDbDao.getSearchSegmentList(0);
    assertNotNull(result);
    assertEquals(0, result.size());
  }

  @Test public void deleteSearchSegmentTest() {
    boolean isAdded = mDbDao.addSearchSegment(mSearchSegment);
    assertTrue(isAdded);
    long result = mDbDao.deleteSearchSegment(mSearchSegment.getSearchSegmentId());
    assertTrue(result == 1);
  }

  @Test public void deleteSearchSegmentFromParentTest() {
    boolean isAdded = mDbDao.addSearchSegment(mSearchSegment);
    assertTrue(isAdded);
    long result = mDbDao.deleteSearchSegmentFromParent(mSearchSegment.getParentSearchId());
    assertTrue(result == 1);
  }

  @Test public void removeAllSearchSegmentsTest() {
    mDbDao.addSearchSegment(mSearchSegment);
    boolean isRemoved = mDbDao.removeAllSearchSegments();
    assertTrue(isRemoved);
    assertNull(mDbDao.getSearchSegment(mSearchSegment.getSearchSegmentId()));
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}