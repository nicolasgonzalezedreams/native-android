package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.ItineraryBooking;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.ItineraryBookingDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ItineraryBookingDBDAOTest extends DbDaoTest<ItineraryBookingDBDAO> {

  private ItineraryBooking mItineraryBooking;

  @Override protected ItineraryBookingDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new ItineraryBookingDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mItineraryBooking = new ItineraryBooking(1, "bookingStatus", "pnr", 2);
  }

  @Test public void addItineraryBookingAndGetItineraryBookingTest() {
    long rowId = mDbDao.addItineraryBooking(mItineraryBooking);
    assertNotEquals(rowId, -1);
    ItineraryBooking itineraryBooking = mDbDao.getItineraryBooking(mItineraryBooking.getId());
    assertEquals(itineraryBooking.getId(), itineraryBooking.getId());
    assertEquals(itineraryBooking.getBookingStatus(), itineraryBooking.getBookingStatus());
    assertEquals(itineraryBooking.getPnr(), itineraryBooking.getPnr());
    assertEquals(itineraryBooking.getTimestamp(), itineraryBooking.getTimestamp());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}