package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.userData.UserAirlinePreferences;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.UserAirlinePreferencesDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class UserAirlinePreferencesDBDAOTest extends DbDaoTest<UserAirlinePreferencesDBDAO> {

  private UserAirlinePreferences mUserAirlinePreferences;

  @Override protected UserAirlinePreferencesDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new UserAirlinePreferencesDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mUserAirlinePreferences =
        new UserAirlinePreferences((long) 1, (long) 12, "XLE-1025", (long) 12);
  }

  @Test public void addUserAirlinePreferencesAndGetUserAirlinePreferencesTest() {
    long rowId = mDbDao.addUserAirlinePreferences(mUserAirlinePreferences);
    assertNotEquals(rowId, -1);
    UserAirlinePreferences userAirlinePreferences =
        mDbDao.getUserAirlinePreferences(mUserAirlinePreferences.getId());
    assertEquals(userAirlinePreferences.getId(), mUserAirlinePreferences.getId());
    assertEquals(userAirlinePreferences.getUserAirlinePreferencesId(),
        mUserAirlinePreferences.getUserAirlinePreferencesId());
    assertEquals(userAirlinePreferences.getAirlineCode(), mUserAirlinePreferences.getAirlineCode());
    assertEquals(userAirlinePreferences.getUserId(), mUserAirlinePreferences.getUserId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
