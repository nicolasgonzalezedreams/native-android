package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.Baggage;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.data.entity.booking.Traveller;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.BaggageDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class BaggageDBDAOTest extends DbDaoTest<BaggageDBDAO> {

  private Baggage mBaggage;

  @Override protected BaggageDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new BaggageDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mBaggage = new Baggage(1, true, 2, 12, new Segment(13), new Traveller(14));
  }

  @Test public void addBaggageAndGetBaggageTest() {
    long rowId;
    rowId = mDbDao.addBaggage(mBaggage);
    assertNotEquals(rowId, -1);
    Baggage baggage = mDbDao.getBaggage(mBaggage.getId());
    assertEquals(baggage.getId(), mBaggage.getId());
    assertEquals(baggage.getIncludedInPrice(), mBaggage.getIncludedInPrice());
    assertEquals(baggage.getPieces(), mBaggage.getPieces());
    assertEquals(baggage.getWeight(), mBaggage.getWeight());
    assertEquals(baggage.getSegment().getId(), mBaggage.getSegment().getId());
    assertEquals(baggage.getTraveller().getId(), mBaggage.getTraveller().getId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
