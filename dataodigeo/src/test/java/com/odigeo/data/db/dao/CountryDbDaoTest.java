package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.db.listener.DatabaseUpdateListener;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.net.NetCountry;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.CountryDbDao;
import com.odigeo.dataodigeo.db.helper.CountriesDbHelper;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 12/02/16.
 */
public class CountryDbDaoTest extends DbDaoTest<CountriesDbHelper> {

  private CountryDbDao mCountryDbDao;
  private NetCountry mNetCountry;
  private NetCountry mNetCountryPhonePrefixNull;
  private NetCountry mNetCountryInvalid;

  @Override protected CountriesDbHelper getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new CountriesDbHelper(context);
  }

  @Override public void setUp() {
    super.setUp();
    mCountryDbDao = new CountryDbDao();
    mNetCountry = new NetCountry();
    fillNetCountries();
    fillNetCountryInvalid();
  }

  private void fillNetCountryInvalid() {
    mNetCountryInvalid = new NetCountry();
    mNetCountryInvalid.setGeoNodeId(1);
    mNetCountryInvalid.setPhonePrefix("+52");
    mNetCountryInvalid.setNames(getNamesMap());
  }

  private Map<String, String> getNamesMap() {
    Map<String, String> namesMap = new HashMap<>();
    namesMap.put("es", "México");
    namesMap.put("en", "Mexico");
    return namesMap;
  }

  private void fillNetCountries() {
    mNetCountry = new NetCountry();
    mNetCountry.setGeoNodeId(1);
    mNetCountry.setCountryCode("MX");
    mNetCountry.setPhonePrefix("+52");
    mNetCountry.setNames(getNamesMap());

    mNetCountryPhonePrefixNull = new NetCountry();
    mNetCountryPhonePrefixNull.setGeoNodeId(2);
    mNetCountryPhonePrefixNull.setCountryCode("XX");
    mNetCountryPhonePrefixNull.setNames(getNamesMap());
  }

  @Test public void testInsertCountry() {
    long insertedId = mCountryDbDao.insert(mDbDao.getOdigeoDatabase(), mNetCountry);
    Assert.assertEquals(1, insertedId);
  }

  @Test public void testInsertInvalidCountry() {
    long insertedId = mCountryDbDao.insert(mDbDao.getOdigeoDatabase(), mNetCountryInvalid);
    Assert.assertEquals(-1, insertedId);
  }

  @Test public void testInsertDuplicated() {
    long insertedId = mCountryDbDao.insert(mDbDao.getOdigeoDatabase(), mNetCountry);
    Assert.assertEquals(1, insertedId);
    insertedId = mCountryDbDao.insert(mDbDao.getOdigeoDatabase(), mNetCountry);
    Assert.assertEquals(-1, insertedId);
  }

  @Test public void testGetCountry() {

  }

  @Test public void testGetAllCountriesLocalized() {
    updateCountriesWithCountDownLatch();
    List<Country> countries =
        mCountryDbDao.getAllCountriesLocalized(mDbDao.getOdigeoDatabase(), "es", false);
    Assert.assertNotNull(countries);
    Assert.assertFalse(countries.isEmpty());
    Assert.assertEquals(2, countries.size());
    Assert.assertEquals("México", countries.get(0).getName());
  }

  @Test public void testGetAllCountriesLocalizedFiltered() {
    updateCountriesWithCountDownLatch();
    List<Country> countries =
        mCountryDbDao.getAllCountriesLocalized(mDbDao.getOdigeoDatabase(), "es", true);
    Assert.assertNotNull(countries);
    Assert.assertFalse(countries.isEmpty());
    Assert.assertEquals(1, countries.size());
    Assert.assertEquals("México", countries.get(0).getName());
  }

  private void updateCountriesWithCountDownLatch() {
    List<NetCountry> netCountries = new ArrayList<>();
    netCountries.add(mNetCountry);
    netCountries.add(mNetCountryPhonePrefixNull);
    final CountDownLatch countDownLatch = new CountDownLatch(1);
    try {
      mDbDao.updateCountries(netCountries, new DatabaseUpdateListener() {
        @Override public void onUpdateFinish() {
          countDownLatch.countDown();
        }
      });
      countDownLatch.await();
    } catch (InterruptedException e) {
      Assert.assertTrue(false);
    }
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}