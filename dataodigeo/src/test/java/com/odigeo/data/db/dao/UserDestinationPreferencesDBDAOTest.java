package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.userData.UserDestinationPreferences;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.UserDestinationPreferencesDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class UserDestinationPreferencesDBDAOTest
    extends DbDaoTest<UserDestinationPreferencesDBDAO> {

  private UserDestinationPreferences mUserDestinationPreferences;

  @Override protected UserDestinationPreferencesDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new UserDestinationPreferencesDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mUserDestinationPreferences =
        new UserDestinationPreferences((long) 1, (long) 12, "Madrid", (long) 12);
  }

  @Test public void addUserDestinationPreferencesAndGetUserDestinationPreferencesTest() {
    long rowId = mDbDao.addUserDestinationPreferences(mUserDestinationPreferences);
    assertNotEquals(rowId, -1);
    UserDestinationPreferences userDestinationPreferences =
        mDbDao.getUserDestinationPreferences(mUserDestinationPreferences.getId());
    assertEquals(userDestinationPreferences.getId(), mUserDestinationPreferences.getId());
    assertEquals(userDestinationPreferences.getUserDestinationPreferencesId(),
        mUserDestinationPreferences.getUserDestinationPreferencesId());
    assertEquals(userDestinationPreferences.getDestination(),
        mUserDestinationPreferences.getDestination());
    assertEquals(userDestinationPreferences.getUserId(), mUserDestinationPreferences.getUserId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
