package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.Forecast;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.ForecastDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ForecastDBDAOTest extends DbDaoTest<ForecastDBDAO> {

  private Forecast mForecast;

  @Override protected ForecastDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new ForecastDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mForecast = new Forecast(1, 2, 3, "forecastDescription", "forecastIcon", 4, 5);
  }

  @Test public void addForecastAndGetForecastTest() {
    long rowId;
    rowId = mDbDao.addForecast(mForecast);
    assertNotEquals(rowId, -1);
    Forecast forecast = mDbDao.getForecast(mForecast.getId());
    assertEquals(forecast.getId(), forecast.getId());
    assertEquals(forecast.getForecastId(), forecast.getForecastId());
    assertEquals(forecast.getForecastDate(), forecast.getForecastDate());
    assertEquals(forecast.getForecastDescription(), forecast.getForecastDescription());
    assertEquals(forecast.getForecastIcon(), forecast.getForecastIcon());
    assertEquals(forecast.getForecastUpdatedDate(), forecast.getForecastUpdatedDate());
    assertEquals(forecast.getTemperature(), forecast.getTemperature());
    assertEquals(forecast.getLocationId(), forecast.getLocationId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}