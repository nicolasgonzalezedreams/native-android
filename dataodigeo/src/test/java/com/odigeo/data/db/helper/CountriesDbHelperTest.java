package com.odigeo.data.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.odigeo.data.db.listener.DatabaseUpdateListener;
import com.odigeo.data.entity.booking.Country;
import com.odigeo.data.entity.net.NetCountry;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.CountryDbDao;
import com.odigeo.dataodigeo.db.dao.LanguageDbDao;
import com.odigeo.dataodigeo.db.helper.CountriesDbHelper;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/02/16.
 */
@RunWith(RobolectricTestRunner.class) public class CountriesDbHelperTest {

  public static final int GEO_NODE_ID_MODIFIED = 7;
  public static final int GEO_NODE_ID_DEFAULT = 1;
  private CountriesDbHelper mCountriesDbHelper;
  private LanguageDbDao mLanguageDbDao;
  private CountryDbDao mCountryDbDao;
  private List<NetCountry> mNetCountries;
  private List<NetCountry> mNetCountriesSecond;
  private List<NetCountry> mNetCountriesInvalid;
  private CountDownLatch mCountDownLatch;

  private List<NetCountry> createMocks(boolean valid, int geoNodeId) {
    List<NetCountry> netCountries = new LinkedList<>();
    NetCountry netCountry = new NetCountry();
    netCountry.setGeoNodeId(geoNodeId);
    if (valid) {
      netCountry.setCountryCode("MX");
    }
    netCountry.setPhonePrefix("+52");
    netCountry.setNames(getNamesMap(geoNodeId == GEO_NODE_ID_MODIFIED));
    netCountries.add(netCountry);
    return netCountries;
  }

  private Map<String, String> getNamesMap(boolean needAdditional) {
    Map<String, String> namesMap = new HashMap<>();
    namesMap.put("es", "México");
    namesMap.put("en", "Mexico");
    if (needAdditional) {
      namesMap.put("fr", "Mexique");
    }
    return namesMap;
  }

  private void updateCountriesWithCountDownLatch(List<NetCountry> netCountries) {
    try {
      mCountDownLatch = new CountDownLatch(1);
      mCountriesDbHelper.updateCountries(netCountries, new DatabaseUpdateListener() {
        @Override public void onUpdateFinish() {
          mCountDownLatch.countDown();
        }
      });
      mCountDownLatch.await();
    } catch (InterruptedException e) {
      Assert.assertTrue(false);
    }
  }

  @Before public void setUp() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    mCountriesDbHelper = new CountriesDbHelper(context);
    mCountriesDbHelper.disablePopulate();
    mLanguageDbDao = new LanguageDbDao();
    mCountryDbDao = new CountryDbDao();
    mNetCountries = createMocks(true, GEO_NODE_ID_DEFAULT);
    mNetCountriesSecond = createMocks(true, GEO_NODE_ID_MODIFIED);
    mNetCountriesInvalid = createMocks(false, GEO_NODE_ID_DEFAULT);
  }

  @Test public void testInsertCountries() {
    updateCountriesWithCountDownLatch(mNetCountries);
    SQLiteDatabase database = mCountriesDbHelper.getOdigeoDatabase();
    Map<String, Long> idsMap = mLanguageDbDao.getIdsMap(database);
    List<Country> countriesEs = mCountryDbDao.getAllCountriesLocalized(database, "es", false);
    List<Country> countriesEn = mCountryDbDao.getAllCountriesLocalized(database, "en", false);
    Assert.assertNotNull(idsMap);
    Assert.assertFalse(idsMap.isEmpty());
    Assert.assertEquals(2, idsMap.size());
    Assert.assertFalse(countriesEn.isEmpty());
    Assert.assertFalse(countriesEs.isEmpty());
    Assert.assertEquals(GEO_NODE_ID_DEFAULT, countriesEs.size());
    Assert.assertEquals(GEO_NODE_ID_DEFAULT, countriesEn.size());
    Assert.assertEquals("México", countriesEs.get(0).getName());
    Assert.assertEquals("Mexico", countriesEn.get(0).getName());
  }

  @Test public void testInsertInvalidCountry() {
    updateCountriesWithCountDownLatch(mNetCountriesInvalid);
    SQLiteDatabase database = mCountriesDbHelper.getOdigeoDatabase();
    Map<String, Long> idsMap = mLanguageDbDao.getIdsMap(database);
    List<Country> countriesEs = mCountryDbDao.getAllCountriesLocalized(database, "es", false);
    Assert.assertNotNull(idsMap);
    Assert.assertTrue(idsMap.isEmpty());
    Assert.assertTrue(countriesEs.isEmpty());
  }

  @Test public void testClean() {
    updateCountriesWithCountDownLatch(mNetCountries);
    updateCountriesWithCountDownLatch(mNetCountriesSecond);
    SQLiteDatabase database = mCountriesDbHelper.getOdigeoDatabase();
    List<Country> countriesEs = mCountryDbDao.getAllCountriesLocalized(database, "es", false);
    Assert.assertFalse(countriesEs.isEmpty());
    Assert.assertEquals(GEO_NODE_ID_DEFAULT, countriesEs.size());
    Assert.assertEquals(GEO_NODE_ID_MODIFIED, countriesEs.get(0).getGeoNodeId());
  }

  @Test public void testAddNewLanguage() {
    updateCountriesWithCountDownLatch(mNetCountries);
    updateCountriesWithCountDownLatch(mNetCountriesSecond);
    SQLiteDatabase database = mCountriesDbHelper.getOdigeoDatabase();
    Map<String, Long> idsMap = mLanguageDbDao.getIdsMap(database);
    List<Country> countriesFr = mCountryDbDao.getAllCountriesLocalized(database, "fr", false);
    Assert.assertFalse(idsMap.isEmpty());
    Assert.assertEquals(3, idsMap.size());
    Assert.assertFalse(countriesFr.isEmpty());
    Assert.assertEquals(1, countriesFr.size());
    Assert.assertEquals("Mexique", countriesFr.get(0).getName());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}