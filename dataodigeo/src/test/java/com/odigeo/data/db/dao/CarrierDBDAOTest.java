package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.Carrier;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.CarrierDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CarrierDBDAOTest extends DbDaoTest<CarrierDBDAO> {

  private Carrier mCarrier;

  @Override protected CarrierDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new CarrierDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mCarrier = new Carrier(1, "code", "name");
  }

  @Test public void addCarrierAndGetCarrierTest() {
    long rowId;

    rowId = mDbDao.addCarrier(mCarrier);

    assertNotEquals(rowId, -1);

    Carrier carrier = mDbDao.getCarrier(mCarrier.getId());

    assertEquals(carrier.getId(), carrier.getId());
    assertEquals(carrier.getName(), carrier.getName());
    assertEquals(carrier.getCode(), carrier.getCode());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}