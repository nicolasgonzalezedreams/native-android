package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.Carrier;
import com.odigeo.data.entity.booking.ItineraryBooking;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.data.entity.booking.Section;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.SectionDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SectionDBDAOTest extends DbDaoTest<SectionDBDAO> {

  private Section mSection;

  @Override protected SectionDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new SectionDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mSection = new Section(1, "0", "aircraft", 2, "arrivalTerminal", "baggageAllowanceQuantity",
        "baggageAllowanceType", "cabinClass", 3, "departureTerminal", 4, "flightID", "sectionType",
        new Carrier(5), new LocationBooking(6), new ItineraryBooking(7), new Segment(8),
        new LocationBooking(9));
  }

  @Test public void addSectionAndGetSectionTest() {
    long rowId = mDbDao.addSection(mSection);
    assertNotEquals(rowId, -1);
    Section section = mDbDao.getSection(mSection.getId());
    assertEquals(mSection.getId(), section.getId());
    assertEquals(mSection.getSectionId(), section.getSectionId());
    assertEquals(mSection.getAircraft(), section.getAircraft());
    assertEquals(mSection.getArrivalDate(), section.getArrivalDate());
    assertEquals(mSection.getArrivalTerminal(), section.getArrivalTerminal());
    assertEquals(mSection.getBaggageAllowanceQuantity(), section.getBaggageAllowanceQuantity());
    assertEquals(mSection.getBaggageAllowanceType(), section.getBaggageAllowanceType());
    assertEquals(mSection.getCabinClass(), section.getCabinClass());
    assertEquals(mSection.getDepartureDate(), section.getDepartureDate());
    assertEquals(mSection.getDepartureTerminal(), section.getDepartureTerminal());
    assertEquals(mSection.getDuration(), section.getDuration());
    assertEquals(mSection.getFlightID(), section.getFlightID());
    assertEquals(mSection.getSectionType(), section.getSectionType());

    //assert carrier, assert only id because in this test the others fields are null
    assertEquals(mSection.getCarrier().getId(), section.getCarrier().getId());

    //assert carrier, assert only id because in this test the others fields are null
    assertEquals(mSection.getFrom().getGeoNodeId(), section.getFrom().getGeoNodeId());

    //assert carrier, assert only id because in this test the others fields are null
    assertEquals(mSection.getItineraryBooking().getId(), section.getItineraryBooking().getId());

    //assert carrier, assert only id because in this test the others fields are null
    assertEquals(mSection.getSegment().getId(), section.getSegment().getId());

    //assert carrier, assert only id because in this test the others fields are null
    assertEquals(mSection.getTo().getGeoNodeId(), section.getTo().getGeoNodeId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}