package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.FlightStats;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.FlightStatsDBDAO;
import java.lang.reflect.Field;
import java.util.Calendar;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by ximenaperez1 on 1/13/16.
 */

public class FlightStatsDBDAOTest extends DbDaoTest<FlightStatsDBDAO> {

  FlightStats mFlightStats, mFlightStats2, mFlightStats3;
  String bookingId1, bookingId2;
  String sectionId1, sectionId2;

  @Override protected FlightStatsDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new FlightStatsDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    sectionId1 = "1";
    sectionId2 = "2";
    bookingId1 = "123456789";
    bookingId2 = "987654321";
    mFlightStats =
        new FlightStats("arrivalGate", Calendar.getInstance().getTimeInMillis(), "arrivalTimeDelay",
            "arrivalTimeType", "baggageClaim", "departureGate",
            Calendar.getInstance().getTimeInMillis(), "departureTimeDelay", "departureTimeType",
            "destinationAirportName", "destinationAirportIataCode", "eventReceived", "flightStatus",
            "gateChanged", "operatingVendor", "operatingVendorCode", "operatingVendorLegNumber",
            "updatedArrivalTerminal", "updatedDepartureTerminal", sectionId1);

    mFlightStats2 = new FlightStats("arrivalGate2", Calendar.getInstance().getTimeInMillis(),
        "arrivalTimeDelay2", "arrivalTimeType2", "baggageClaim2", "departureGate2",
        Calendar.getInstance().getTimeInMillis(), "departureTimeDelay2", "departureTimeType2",
        "destinationAirportName2", "destinationAirportIataCode2", "eventReceived2", "flightStatus2",
        "gateChanged2", "operatingVendor2", "operatingVendorCode2", "operatingVendorLegNumber2",
        "updatedArrivalTerminal2", "updatedDepartureTerminal2", sectionId2);

    mFlightStats3 = new FlightStats("arrivalGate3", Calendar.getInstance().getTimeInMillis(),
        "arrivalTimeDelay3", "arrivalTimeType3", "baggageClaim3", "departureGate3",
        Calendar.getInstance().getTimeInMillis(), "departureTimeDelay3", "departureTimeType3",
        "destinationAirportName3", "destinationAirportIataCode3", "eventReceived3", "flightStatus3",
        "gateChanged3", "operatingVendor3", "operatingVendorCode3", "operatingVendorLegNumber3",
        "updatedArrivalTerminal3", "updatedDepartureTerminal3", sectionId2);
  }

  @Test public void addOrUpdateFlightStatsWhenNoExistTest() {
    long rowId;
    rowId = mDbDao.addOrUpdateFlightStats(mFlightStats, bookingId1);
    assertNotEquals(rowId, -1);

    FlightStats flightStats = mDbDao.getFlightStats(mFlightStats.getSectionId(), bookingId1);
    assertEquals(mFlightStats.getArrivalGate(), flightStats.getArrivalGate());
    assertEquals(mFlightStats.getArrivalTime(), flightStats.getArrivalTime());
    assertEquals(mFlightStats.getArrivalTimeDelay(), flightStats.getArrivalTimeDelay());
    assertEquals(mFlightStats.getArrivalTimeType(), flightStats.getArrivalTimeType());
    assertEquals(mFlightStats.getBaggageClaim(), flightStats.getBaggageClaim());
    assertEquals(mFlightStats.getDepartureGate(), flightStats.getDepartureGate());
    assertEquals(mFlightStats.getDepartureTime(), flightStats.getDepartureTime());
    assertEquals(mFlightStats.getDepartureTimeDelay(), flightStats.getDepartureTimeDelay());
    assertEquals(mFlightStats.getDepartureTimeType(), flightStats.getDepartureTimeType());
    assertEquals(mFlightStats.getEventReceived(), flightStats.getEventReceived());
    assertEquals(mFlightStats.getFlightStatus(), flightStats.getFlightStatus());
    assertEquals(mFlightStats.getGateChanged(), flightStats.getGateChanged());
    assertEquals(mFlightStats.getOperatingVendor(), flightStats.getOperatingVendor());
    assertEquals(mFlightStats.getOperatingVendorCode(), flightStats.getOperatingVendorCode());
    assertEquals(mFlightStats.getOperatingVendorLegNumber(),
        flightStats.getOperatingVendorLegNumber());
    assertEquals(mFlightStats.getUpdatedArrivalTerminal(), flightStats.getUpdatedArrivalTerminal());
    assertEquals(mFlightStats.getUpdatedDepartureTerminal(),
        flightStats.getUpdatedDepartureTerminal());
    assertEquals(mFlightStats.getSectionId(), flightStats.getSectionId());
  }

  @Test public void updateFlightStatsTest() {
    long rowId;
    rowId = mDbDao.addOrUpdateFlightStats(mFlightStats2, bookingId1);
    assertNotEquals(rowId, -1);

    FlightStats flightStats = mDbDao.getFlightStats(sectionId2, bookingId1);
    assertEquals(mFlightStats2.getArrivalGate(), flightStats.getArrivalGate());
    assertEquals(mFlightStats2.getSectionId(), flightStats.getSectionId());

    rowId = mDbDao.addOrUpdateFlightStats(mFlightStats3, bookingId1);
    assertNotEquals(rowId, -1);

    flightStats = mDbDao.getFlightStats(sectionId2, bookingId1);
    assertEquals(mFlightStats3.getArrivalGate(), flightStats.getArrivalGate());
    assertEquals(mFlightStats3.getSectionId(), flightStats.getSectionId());
  }

  @Test public void noDuplicateFlightStatsTest() {
    long rowId;
    rowId = mDbDao.addOrUpdateFlightStats(mFlightStats2, bookingId1);
    assertNotEquals(rowId, -1);

    rowId = mDbDao.addOrUpdateFlightStats(mFlightStats3, bookingId2);
    assertNotEquals(rowId, -1);

    FlightStats flightStats = mDbDao.getFlightStats(sectionId2, bookingId2);
    assertNotEquals(mFlightStats2.getArrivalGate(), flightStats.getArrivalGate());
    assertEquals(mFlightStats3.getArrivalGate(), flightStats.getArrivalGate());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
