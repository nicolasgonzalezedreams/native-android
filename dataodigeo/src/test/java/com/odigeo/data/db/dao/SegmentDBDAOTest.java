package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.Segment;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.SegmentDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SegmentDBDAOTest extends DbDaoTest<SegmentDBDAO> {

  private Segment mSegment;

  @Override protected SegmentDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new SegmentDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mSegment = new Segment(1, 2, 3, 4, 5);
  }

  @Test public void addSegmentAndGetSegmentTest() {
    long rowId = mDbDao.addSegment(mSegment);
    assertNotEquals(rowId, -1);
    Segment segment = mDbDao.getSegment(mSegment.getId());
    assertEquals(segment.getId(), segment.getId());
    assertEquals(segment.getDuration(), segment.getDuration());
    assertEquals(segment.getSeats(), segment.getSeats());
    assertEquals(segment.getMainCarrierId(), segment.getMainCarrierId());
    assertEquals(segment.getBookingId(), segment.getBookingId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}