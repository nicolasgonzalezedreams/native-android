package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.Buyer;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.BuyerDBDAO;
import java.lang.reflect.Field;
import java.util.Calendar;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class BuyerDBDAOTest extends DbDaoTest<BuyerDBDAO> {

  private Buyer mBuyer;

  @Override protected BuyerDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new BuyerDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mBuyer =
        new Buyer(Calendar.getInstance().getTimeInMillis(), "identification", "identificationType",
            "lastname", "name", 1, "address", "alternativePhone", "cityName", "country", "cpf",
            "email", "phone", "phoneCountryCode", "stateName", "zipCode", 2);
  }

  @Test public void addBuyerAndGetBuyerTest() {
    long rowId;

    rowId = mDbDao.addBuyer(mBuyer);

    assertNotEquals(rowId, -1);

    Buyer buyer = mDbDao.getBuyer(mBuyer.getBookingId());

    assertEquals(mBuyer.getDateOfBirth(), buyer.getDateOfBirth(), 1e-15);
    assertEquals(mBuyer.getIdentification(), buyer.getIdentification());
    assertEquals(mBuyer.getIdentificationType(), buyer.getIdentificationType());
    assertEquals(mBuyer.getLastname(), buyer.getLastname());
    assertEquals(mBuyer.getName(), buyer.getName());
    assertEquals(mBuyer.getId(), buyer.getId());
    assertEquals(mBuyer.getAddress(), buyer.getAddress());
    assertEquals(mBuyer.getAlternativePhone(), buyer.getAlternativePhone());
    assertEquals(mBuyer.getCityName(), buyer.getCityName());
    assertEquals(mBuyer.getCountry(), buyer.getCountry());
    assertEquals(mBuyer.getCpf(), buyer.getCpf());
    assertEquals(mBuyer.getEmail(), buyer.getEmail());
    assertEquals(mBuyer.getPhone(), buyer.getPhone());
    assertEquals(mBuyer.getPhoneCountryCode(), buyer.getPhoneCountryCode());
    assertEquals(mBuyer.getStateName(), buyer.getStateName());
    assertEquals(mBuyer.getZipCode(), buyer.getZipCode());
    assertEquals(mBuyer.getBookingId(), buyer.getBookingId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}