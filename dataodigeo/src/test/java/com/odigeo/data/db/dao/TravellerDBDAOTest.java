package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.Traveller;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.TravellerDBDAO;
import java.lang.reflect.Field;
import java.util.Calendar;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TravellerDBDAOTest extends DbDaoTest<TravellerDBDAO> {

  private Traveller mTraveller;

  @Override protected TravellerDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new TravellerDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mTraveller = new Traveller(Calendar.getInstance().getTimeInMillis(), "identification",
        "identificationType", "lastname", "name", 1, "countryCode",
        Calendar.getInstance().getTimeInMillis(), "identificationIssueCountryCode", "localityCode",
        "meal", "nationality", "title", "travellerGender", "travellerType", 2);
  }

  @Test public void addTravellerAndGetTravellerTest() {
    long rowId = mDbDao.addTraveller(mTraveller);
    assertNotEquals(rowId, -1);
    Traveller traveller = mDbDao.getTraveller(mTraveller.getId());
    assertEquals(traveller.getDateOfBirth(), traveller.getDateOfBirth());
    assertEquals(traveller.getIdentification(), traveller.getIdentification());
    assertEquals(traveller.getIdentificationType(), traveller.getIdentificationType());
    assertEquals(traveller.getLastname(), traveller.getLastname());
    assertEquals(traveller.getName(), traveller.getName());
    assertEquals(traveller.getId(), traveller.getId());
    assertEquals(traveller.getCountryCode(), traveller.getCountryCode());
    assertEquals(traveller.getIdentificationExpirationDate(),
        traveller.getIdentificationExpirationDate());
    assertEquals(traveller.getIdentificationIssueCountryCode(),
        traveller.getIdentificationIssueCountryCode());
    assertEquals(traveller.getLocalityCode(), traveller.getLocalityCode());
    assertEquals(traveller.getMeal(), traveller.getMeal());
    assertEquals(traveller.getNationality(), traveller.getNationality());
    assertEquals(traveller.getTitle(), traveller.getTitle());
    assertEquals(traveller.getTravellerGender(), traveller.getTravellerGender());
    assertEquals(traveller.getTravellerType(), traveller.getTravellerType());
    assertEquals(traveller.getBookingId(), traveller.getBookingId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}