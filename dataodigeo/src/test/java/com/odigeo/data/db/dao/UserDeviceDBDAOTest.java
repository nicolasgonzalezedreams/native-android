package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.userData.UserDevice;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.UserDeviceDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class UserDeviceDBDAOTest extends DbDaoTest<UserDeviceDBDAO> {

  private UserDevice mUserDevice;

  @Override protected UserDeviceDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new UserDeviceDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mUserDevice =
        new UserDevice((long) 1, (long) 12, "Dispositivo chachi", "jasgljk13013kadgkn90424tk", true,
            "Nexus 5", (long) 12);
  }

  @Test public void addUserDeviceAndGetUserDeviceTest() {
    long rowId = mDbDao.addUserDevice(mUserDevice);
    assertNotEquals(rowId, -1);
    UserDevice userDevice = mDbDao.getUserDevice(mUserDevice.getId());
    assertEquals(userDevice.getId(), mUserDevice.getId());
    assertEquals(userDevice.getUserDeviceId(), mUserDevice.getUserDeviceId());
    assertEquals(userDevice.getDeviceName(), mUserDevice.getDeviceName());
    assertEquals(userDevice.getDeviceId(), mUserDevice.getDeviceId());
    assertEquals(userDevice.getIsPrimary(), mUserDevice.getIsPrimary());
    assertEquals(userDevice.getAlias(), mUserDevice.getAlias());
    assertEquals(userDevice.getUserId(), mUserDevice.getUserId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
