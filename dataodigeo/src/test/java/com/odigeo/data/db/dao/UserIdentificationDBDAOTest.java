package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.userData.UserIdentification;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.UserIdentificationDBDAO;
import java.lang.reflect.Field;
import java.util.Calendar;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class UserIdentificationDBDAOTest extends DbDaoTest<UserIdentificationDBDAO> {

  private UserIdentification mUserIdentification;

  @Override protected UserIdentificationDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new UserIdentificationDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mUserIdentification = new UserIdentification((long) 1, (long) 12, "11111111A", null,
        Calendar.getInstance().getTimeInMillis(), UserIdentification.IdentificationType.NIF,
        (long) 13);
  }

  @Test public void addUserIdentificationAndGetUserIdentificationTest() {
    long rowId = mDbDao.addUserIdentification(mUserIdentification);
    assertNotEquals(rowId, -1);
    UserIdentification userIdentification =
        mDbDao.getUserIdentification(mUserIdentification.getUserIdentificationId());
    assertEquals(userIdentification.getIdentificationId(),
        mUserIdentification.getIdentificationId());
    assertEquals(userIdentification.getUserIdentificationId(),
        mUserIdentification.getUserIdentificationId());
    assertEquals(userIdentification.getIdentificationCountryCode(),
        mUserIdentification.getIdentificationCountryCode());
    assertEquals(userIdentification.getIdentificationExpirationDate(),
        mUserIdentification.getIdentificationExpirationDate());
    assertEquals(userIdentification.getIdentificationType(),
        mUserIdentification.getIdentificationType());
    assertEquals(userIdentification.getUserProfileId(), mUserIdentification.getUserProfileId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
