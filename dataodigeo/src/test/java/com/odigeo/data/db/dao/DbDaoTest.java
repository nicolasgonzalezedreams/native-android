package com.odigeo.data.db.dao;

import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 19/02/16
 */
@RunWith(RobolectricTestRunner.class)
public abstract class DbDaoTest<T extends OdigeoSQLiteOpenHelper> {

  protected T mDbDao;

  protected abstract T getDbDao();

  @Before public void setUp() {
    mDbDao = getDbDao();
    mDbDao.disablePopulate();
  }
}
