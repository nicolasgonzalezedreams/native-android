package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.userData.User;
import com.odigeo.data.entity.userData.UserTraveller;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.UserDBDAO;
import com.odigeo.dataodigeo.db.dao.UserTravellerDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.mockito.Mockito;
import org.robolectric.RuntimeEnvironment;

import static com.odigeo.test.mock.MocksProvider.providesPassengerMocks;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class UserTravellerDBDAOTest extends DbDaoTest<UserTravellerDBDAO> {

  private UserTraveller mUserTraveller;
  private UserDBDAOInterface userDBDAO;

  @Override protected UserTravellerDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    userDBDAO = Mockito.mock(UserDBDAO.class);
    return new UserTravellerDBDAO(context, userDBDAO);
  }

  @Override public void setUp() {
    super.setUp();
    mUserTraveller = providesPassengerMocks().provideUserTravellerWithDataNoProfile();
  }

  @Test public void addUserTravellerAndGetUserTravellerTest() {

    User mockUser = new User();
    Mockito.when(userDBDAO.getCurrentUser()).thenReturn(mockUser);

    long rowId = mDbDao.addUserTraveller(mUserTraveller);
    assertNotEquals(rowId, -1);
    UserTraveller userTraveller = mDbDao.getUserTraveller(mUserTraveller.getUserTravellerId());
    assertEquals(userTraveller.getUserTravellerId(), mUserTraveller.getUserTravellerId());
    assertEquals(userTraveller.getBuyer(), mUserTraveller.getBuyer());
    assertEquals(userTraveller.getEmail(), mUserTraveller.getEmail());
    assertEquals(userTraveller.getTypeOfTraveller(), mUserTraveller.getTypeOfTraveller());
    assertEquals(userTraveller.getMealType(), mUserTraveller.getMealType());
    assertEquals(userTraveller.getUserId(), mUserTraveller.getUserId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
