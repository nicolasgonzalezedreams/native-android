package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.StoredSearch;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.StoredSearchDBDAO;
import java.lang.reflect.Field;
import java.util.List;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static com.odigeo.test.mock.MocksProvider.providesStoredSearchMocks;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@Config(manifest = Config.NONE) public class StoredSearchDBDAOTest
    extends DbDaoTest<StoredSearchDBDAO> {

  private StoredSearch mStoredSearch;
  private StoredSearch mSyncStoredSearch;

  @Override protected StoredSearchDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new StoredSearchDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mStoredSearch = providesStoredSearchMocks().provideStoredSearchNotSync();
    mSyncStoredSearch = providesStoredSearchMocks().provideSyncStoredSearch();
  }

  @Test public void getStoredSearchTest() {
    boolean isAdded = (mDbDao.addStoredSearch(mStoredSearch) != -1);
    assertNotEquals(isAdded, false);
    StoredSearch result = mDbDao.getStoredSearch(mStoredSearch.getId());
    assertNotNull(result);
  }

  @Test public void addStoredSearchAndGetStoredSearchTest() {

    boolean isAdded = (mDbDao.addStoredSearch(mStoredSearch) != -1);
    assertNotEquals(isAdded, false);
    StoredSearch storedSearch = mDbDao.getStoredSearch(mStoredSearch.getId());
    verifyStoredSearch(mStoredSearch, storedSearch);
  }

  @Test public void getAllStoredSearchesTest() {
    boolean isAdded = (mDbDao.addStoredSearch(mStoredSearch) != -1);
    assertNotEquals(isAdded, false);
    isAdded = (mDbDao.addStoredSearch(mSyncStoredSearch) != -1);
    assertNotEquals(isAdded, false);
    List<StoredSearch> storedSearches = mDbDao.getAllStoredSearches();
    verifyStoredSearch(storedSearches.get(0), mStoredSearch);
    verifyStoredSearch(storedSearches.get(1), mSyncStoredSearch);
  }

  @Test public void getLocalStoredSearchesTest() {
    boolean isAdded = (mDbDao.addStoredSearch(mStoredSearch) != -1);
    assertNotEquals(isAdded, false);
    isAdded = (mDbDao.addStoredSearch(mSyncStoredSearch) != -1);
    assertNotEquals(isAdded, false);
    List<StoredSearch> storedSearches = mDbDao.getLocalStoredSearches();
    verifyStoredSearch(storedSearches.get(0), mStoredSearch);
  }

  @Test public void getSynchronizedStoredSearchesTest() {
    boolean isAdded = (mDbDao.addStoredSearch(mStoredSearch) != -1);
    assertNotEquals(isAdded, false);
    isAdded = (mDbDao.addStoredSearch(mSyncStoredSearch) != -1);
    assertNotEquals(isAdded, false);
    List<StoredSearch> storedSearches = mDbDao.getSynchronizedStoredSearches();
    verifyStoredSearch(storedSearches.get(0), mSyncStoredSearch);
  }

  @Test public void removeAllStoredSearchesTest() {
    boolean isAdded = (mDbDao.addStoredSearch(mStoredSearch) != -1);
    assertNotEquals(isAdded, false);
    isAdded = (mDbDao.addStoredSearch(mSyncStoredSearch) != -1);
    assertNotEquals(isAdded, false);
    boolean removeSuccess = mDbDao.removeAllStoredSearches();
    assertNotEquals(removeSuccess, false);
    List<StoredSearch> storedSearches = mDbDao.getAllStoredSearches();
    assertTrue(storedSearches.isEmpty());
  }

  @Test public void removeStoredSearchTest() {
    boolean isAdded = (mDbDao.addStoredSearch(mStoredSearch) != -1);
    assertNotEquals(isAdded, false);
    isAdded = (mDbDao.addStoredSearch(mSyncStoredSearch) != -1);
    assertNotEquals(isAdded, false);
    boolean removeSuccess = mDbDao.removeStoredSearch(mStoredSearch);
    assertNotEquals(removeSuccess, false);
    StoredSearch result = mDbDao.getStoredSearch(mStoredSearch.getId());
    assertNull(result);
  }

  @Test public void updateAndGetStoredSearchTest() {
    boolean isAdded = (mDbDao.addStoredSearch(mStoredSearch) != -1);
    assertNotEquals(isAdded, false);
    StoredSearch otherStoredSearch =
        new StoredSearch(mStoredSearch.getId(), mStoredSearch.getStoredSearchId(), 1, 1, 0, false,
            StoredSearch.CabinClass.BUSINESS, StoredSearch.TripType.M, false, 700000, 0);
    boolean isUpdated = mDbDao.updateStoredSearch(otherStoredSearch);
    assertNotEquals(isUpdated, false);
    StoredSearch actual = mDbDao.getStoredSearch(mStoredSearch.getId());
    verifyStoredSearch(otherStoredSearch, actual);
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }

  public void verifyStoredSearch(StoredSearch expected, StoredSearch actual) {
    assertEquals(expected.getId(), actual.getId());
    assertEquals(expected.getStoredSearchId(), actual.getStoredSearchId());
    assertEquals(expected.getNumAdults(), actual.getNumAdults());
    assertEquals(expected.getNumChildren(), actual.getNumChildren());
    assertEquals(expected.getNumInfants(), actual.getNumInfants());
    assertEquals(expected.getIsDirectFlight(), actual.getIsDirectFlight());
    assertEquals(expected.getCabinClass(), actual.getCabinClass());
    assertEquals(expected.getTripType(), actual.getTripType());
    assertEquals(expected.isSynchronized(), actual.isSynchronized());
    assertEquals(expected.getUserId(), actual.getUserId());
  }
}