package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.LocationBooking;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.LocationBookingDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class LocationBookingDBDAOTest extends DbDaoTest<LocationBookingDBDAO> {

  private LocationBooking mLocationBooking;

  @Override protected LocationBookingDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new LocationBookingDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mLocationBooking =
        new LocationBooking("cityIATACode", "cityName", "country", 0, 2, "locationCode",
            "locationName", "locationType", 3, "matchName", "countryCode", "currencyCode", 4, 5,
            "timezone");
  }

  @Test public void addLocationBookingAndGetLocationBookingTest() {
    long rowId = mDbDao.addLocationBooking(mLocationBooking);
    assertNotEquals(rowId, -1);
    LocationBooking locationBooking = mDbDao.getLocationBooking(mLocationBooking.getGeoNodeId());
    assertEquals(locationBooking.getCityIATACode(), mLocationBooking.getCityIATACode());
    assertEquals(locationBooking.getCityName(), mLocationBooking.getCityName());
    assertEquals(locationBooking.getCountry(), mLocationBooking.getCountry());
    assertEquals(locationBooking.getGeoNodeId(), mLocationBooking.getGeoNodeId());
    assertEquals(locationBooking.getLatitude(), mLocationBooking.getLatitude());
    assertEquals(locationBooking.getLocationCode(), mLocationBooking.getLocationCode());
    assertEquals(locationBooking.getLocationName(), mLocationBooking.getLocationName());
    assertEquals(locationBooking.getLocationType(), mLocationBooking.getLocationType());
    assertEquals(locationBooking.getLongitude(), mLocationBooking.getLongitude());
    assertEquals(locationBooking.getMatchName(), mLocationBooking.getMatchName());
    assertEquals(locationBooking.getCountryCode(), mLocationBooking.getCountryCode());
    assertEquals(locationBooking.getCurrencyCode(), mLocationBooking.getCurrencyCode());
    assertEquals(locationBooking.getCurrencyRate(), mLocationBooking.getCurrencyRate());
    assertEquals(locationBooking.getCurrencyUpdatedDate(),
        mLocationBooking.getCurrencyUpdatedDate());
    assertEquals(locationBooking.getTimezone(), mLocationBooking.getTimezone());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
