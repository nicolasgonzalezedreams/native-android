package com.odigeo.data.db.helper;

import android.app.Application;
import android.content.Context;
import com.odigeo.data.entity.booking.Booking;
import com.odigeo.dataodigeo.db.dao.BaggageDBDAO;
import com.odigeo.dataodigeo.db.dao.BookingDBDAO;
import com.odigeo.dataodigeo.db.dao.BuyerDBDAO;
import com.odigeo.dataodigeo.db.dao.CarrierDBDAO;
import com.odigeo.dataodigeo.db.dao.FlightStatsDBDAO;
import com.odigeo.dataodigeo.db.dao.InsuranceDBDAO;
import com.odigeo.dataodigeo.db.dao.ItineraryBookingDBDAO;
import com.odigeo.dataodigeo.db.dao.LocationBookingDBDAO;
import com.odigeo.dataodigeo.db.dao.SectionDBDAO;
import com.odigeo.dataodigeo.db.dao.SegmentDBDAO;
import com.odigeo.dataodigeo.db.dao.TravellerDBDAO;
import com.odigeo.dataodigeo.db.helper.BookingsHandler;
import com.odigeo.dataodigeo.net.mapper.BookingNetMapper;
import com.odigeo.interactors.DeleteBookingsInteractor;
import org.json.JSONException;
import org.junit.Before;
import org.robolectric.RuntimeEnvironment;

/**
 * Created by javier.rebollo on 7/9/15.
 */
public class ImportMyTripsHandlerTest {

  private Application mApplication;
  private BookingsHandlerInterface mBookingsHandlerInterface;
  private BookingNetMapper mBookingNetMapper;
  private String jsonFake;
  private Booking mBooking;
  private DeleteBookingsInteractor mDeleteBookingsInteractor;

  @Before public void setUp() {
    Context context = RuntimeEnvironment.application.getApplicationContext();

    mBookingsHandlerInterface =
        new BookingsHandler(mApplication, new BookingDBDAO(context), new BuyerDBDAO(context),
            new TravellerDBDAO(context), new BaggageDBDAO(context), new SegmentDBDAO(context),
            new CarrierDBDAO(context), new SectionDBDAO(context),
            new ItineraryBookingDBDAO(context), new LocationBookingDBDAO(context),
            new InsuranceDBDAO(context), new FlightStatsDBDAO(context), mDeleteBookingsInteractor);

    mBookingNetMapper = new BookingNetMapper();
    jsonFake =
        "{\"insuranceBookings\":{\"bookings\":[]},\"locale\":\"es_ES\",\"price\":{\"currency\":\"EUR\",\"amount\":1446.62},"
            + "\"buyer\":{\"address\":\"C/PeriodistaJuanOsorioBueno,2\",\"name\":\"MIRIANA\",\"country\":\"ES\","
            + "\"mail\":\"myriammolinal@gmail.com\",\"phoneNumber\":{\"number\":\"619029304\",\"countryCode\":\"ES\"},"
            + "\"cityName\":\"Granada\",\"identification\":null,\"alternativePhoneNumber\":{\"number\":\"958151450\","
            + "\"countryCode\":\"ES\"},\"buyerIdentificationType\":null,\"cpf\":null,\"lastNames\":\"MOLINALEON\","
            + "\"stateName\":\"Granada\",\"zipCode\":\"18014\"},\"itineraryBookings\":{\"bookings\":[{\"id\":1372378145,"
            + "\"numAdults\":2,\"resident\":false,\"segments\":[0,1],\"numChildren\":0,\"numInfants\":0,"
            + "\"additionalParameters\":[],\"bookingStatus\":\"CONTRACT\",\"pnr\":\"2F6UOK\",\"itineraryProviders\":[0],"
            + "\"merchant\":true,\"paymentMethodType\":\"CREDITCARD\",\"corporateCreditCard\":\"5215450192783519\","
            + "\"providerBookingItinerary\":[{\"providerItineraryId\":824141752,\"ticketNumbers\":[{\"number\":\"9352453290\","
            + "\"numPassenger\":1},{\"number\":\"9352453289\",\"numPassenger\":1},{\"number\":\"9352453287\",\"numPassenger\":2},"
            + "{\"number\":\"9352453288\",\"numPassenger\":2}]}],\"residentValidations\":{},\"baggageProviderPrice\":0,"
            + "\"totalProviderPrice\":1432.64,\"officeId\":\"BCNED3351\",\"creditCardDetails\":{\"owner\":\"MIRIANAMOLINALEON\","
            + "\"creditCardType\":\"CA\",\"creditCardNumber\":\"5215450192783519\",\"cvv\":null,"
            + "\"creditCardExpirationDate\":\"0818\"},\"corporateCreditCardExDate\":\"0818\"}],"
            + "\"bookingItinerary\":{\"type\":\"SIMPLE\",\"departure\":{\"name\":\"AdolfoSuárezMadrid-Barajas\","
            + "\"type\":\"Airport\",\"timeZone\":\"Europe/Madrid\",\"geoNodeId\":1147,\"iataCode\":\"MAD\","
            + "\"cityName\":\"Madrid\",\"countryCode\":\"ES\",\"cityIataCode\":\"MAD\",\"countryName\":\"Spain\"},"
            + "\"departureDate\":1468503000000,\"arrivalDate\":1470224400000,\"tripType\":\"MULTI_SEGMENT\","
            + "\"arrival\":{\"name\":\"CapeTownInternationalAirport\",\"type\":\"Airport\",\"timeZone\":\"Africa/Johannesburg\","
            + "\"geoNodeId\":418,\"iataCode\":\"CPT\",\"cityName\":\"CapeTown\",\"countryCode\":\"ZA\",\"cityIataCode\":\"CPT\","
            + "\"countryName\":\"SouthAfrica\"}},\"legend\":{\"carriers\":[{\"name\":\"Emirates\",\"id\":0,\"code\":\"EK\"}],"
            + "\"locations\":[{\"name\":\"AdolfoSuárezMadrid-Barajas\",\"type\":\"Airport\",\"timeZone\":\"Europe/Madrid\","
            + "\"geoNodeId\":1147,\"iataCode\":\"MAD\",\"cityName\":\"Madrid\",\"countryCode\":\"ES\",\"cityIataCode\":\"MAD\","
            + "\"countryName\":\"Spain\"},{\"name\":\"ORTamboInternationalAirport\",\"type\":\"Airport\","
            + "\"timeZone\":\"Africa/Johannesburg\",\"geoNodeId\":917,\"iataCode\":\"JNB\",\"cityName\":\"Johannesburg\","
            + "\"countryCode\":\"ZA\",\"cityIataCode\":\"JNB\",\"countryName\":\"SouthAfrica\"},"
            + "{\"name\":\"CapeTownInternationalAirport\",\"type\":\"Airport\",\"timeZone\":\"Africa/Johannesburg\","
            + "\"geoNodeId\":418,\"iataCode\":\"CPT\",\"cityName\":\"CapeTown\",\"countryCode\":\"ZA\",\"cityIataCode\":\"CPT\","
            + "\"countryName\":\"SouthAfrica\"},{\"name\":\"DubaiInternationalAirport\",\"type\":\"Airport\","
            + "\"timeZone\":\"Asia/Dubai\",\"geoNodeId\":536,\"iataCode\":\"DXB\",\"cityName\":\"Dubai\",\"countryCode\":\"AE\","
            + "\"cityIataCode\":\"DXB\",\"countryName\":\"UnitedArabEmirates\"}]},\"itinerarySegments\":[{\"duration\":1200,"
            + "\"sections\":[{\"sectionType\":\"SCALE\",\"itineraryBookingId\":1372378145,\"id\":0,\"section\":{\"id\":\"142\","
            + "\"duration\":435,\"from\":1147,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":null,\"arrivalTerminal\":null,"
            + "\"baggageAllowanceQuantity\":null,\"baggageAllowanceType\":null,\"carrier\":0,\"departureDate\":1468510200770,"
            + "\"arrivalDate\":1468543500770,\"departureTerminal\":null,\"fareInfoPassengers\":[],\"operatingCarrier\":null,"
            + "\"technicalStops\":[],\"to\":536}},{\"sectionType\":\"ARRIVAL\",\"itineraryBookingId\":1372378145,\"id\":1,"
            + "\"section\":{\"id\":\"772\",\"duration\":580,\"from\":536,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":null,"
            + "\"arrivalTerminal\":null,\"baggageAllowanceQuantity\":null,\"baggageAllowanceType\":null,\"carrier\":0,"
            + "\"departureDate\":1468554600775,\"arrivalDate\":1468582200775,\"departureTerminal\":null,\"fareInfoPassengers\":[],"
            + "\"operatingCarrier\":null,\"technicalStops\":[],\"to\":418}}]},{\"duration\":1105,"
            + "\"sections\":[{\"sectionType\":\"SCALE\",\"itineraryBookingId\":1372378145,\"id\":2,\"section\":{\"id\":\"764\","
            + "\"duration\":480,\"from\":917,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":null,\"arrivalTerminal\":null,"
            + "\"baggageAllowanceQuantity\":null,\"baggageAllowanceType\":null,\"carrier\":0,\"departureDate\":1470165300776,"
            + "\"arrivalDate\":1470201300776,\"departureTerminal\":null,\"fareInfoPassengers\":[],\"operatingCarrier\":null,"
            + "\"technicalStops\":[],\"to\":536}},{\"sectionType\":\"ARRIVAL\",\"itineraryBookingId\":1372378145,\"id\":3,"
            + "\"section\":{\"id\":\"141\",\"duration\":480,\"from\":536,\"cabinClass\":\"ECONOMIC_DISCOUNTED\",\"aircraft\":null,"
            + "\"arrivalTerminal\":null,\"baggageAllowanceQuantity\":null,\"baggageAllowanceType\":null,\"carrier\":0,"
            + "\"departureDate\":1470210000777,\"arrivalDate\":1470231600777,\"departureTerminal\":null,\"fareInfoPassengers\":[],"
            + "\"operatingCarrier\":null,\"technicalStops\":[],\"to\":1147}}]}]},\"travellers\":[{\"name\":\"MIRIANA\","
            + "\"title\":\"MRS\",\"countryCodeOfResidence\":null,\"dateOfBirth\":null,\"firstLastName\":\"MOLINA\","
            + "\"identification\":null,\"identificationExpirationDate\":null,\"identificationIssueCountryCode\":null,"
            + "\"identificationType\":null,\"localityCodeOfResidence\":null,\"meal\":null,\"middleName\":\"\","
            + "\"nationalityCountryCode\":null,\"secondLastName\":\"LEON\",\"travellerGender\":null,\"travellerType\":\"ADULT\","
            + "\"baggageSelections\":[{\"kilos\":0,\"pieces\":1,\"segmentIndex\":0},{\"kilos\":0,\"pieces\":1,"
            + "\"segmentIndex\":1}],\"numPassenger\":1,\"frequentFlyerCards\":[{\"number\":\"281782642\","
            + "\"carrierCode\":\"EK\"}]},{\"name\":\"LAURA\",\"title\":\"MS\",\"countryCodeOfResidence\":null,"
            + "\"dateOfBirth\":null,\"firstLastName\":\"BLANCO\",\"identification\":null,\"identificationExpirationDate\":null,"
            + "\"identificationIssueCountryCode\":null,\"identificationType\":null,\"localityCodeOfResidence\":null,\"meal\":null,"
            + "\"middleName\":\"\",\"nationalityCountryCode\":null,\"secondLastName\":\"MOLINA\",\"travellerGender\":null,"
            + "\"travellerType\":\"ADULT\",\"baggageSelections\":[{\"kilos\":0,\"pieces\":1,\"segmentIndex\":0},{\"kilos\":0,"
            + "\"pieces\":1,\"segmentIndex\":1}],\"numPassenger\":2,\"frequentFlyerCards\":[{\"number\":\"281782642\","
            + "\"carrierCode\":\"EK\"}]}],\"deposit\":null,\"bookingStatus\":\"CONTRACT\",\"collectionState\":\"COLLECTED\","
            + "\"currencyCode\":\"EUR\",\"message\":\"\",\"bookingBasicInfo\":{\"id\":1372377658,"
            + "\"agent\":{\"firstName\":\"SiteSpain\",\"lastName\":\"SiteSpain\"},\"creationDate\":1441128934000,"
            + "\"lastUpdate\":1441132478000,\"clientBookingReferenceId\":null,\"partner\":null,\"brandCode\":\"ED\","
            + "\"marketingPortalCode\":\"skyscanner\",\"websiteCode\":\"ES\"}}";

    try {
      mBooking = mBookingNetMapper.parseBooking(jsonFake);
    } catch (JSONException e) {
      mBooking = null;
    }
  }
}
