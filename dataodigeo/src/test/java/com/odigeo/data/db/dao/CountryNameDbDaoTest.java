package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.CountryNameDbDao;
import com.odigeo.dataodigeo.db.helper.CountriesDbHelper;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/02/16.
 */
public class CountryNameDbDaoTest extends DbDaoTest<CountriesDbHelper> {

  private CountryNameDbDao mCountryNameDbDao;

  private Map<Long, String> getIdsAndNamesMap() {
    Map<Long, String> idsAndNamesMap = new HashMap<>();
    idsAndNamesMap.put(1L, "México");
    idsAndNamesMap.put(2L, "Mexico");
    idsAndNamesMap.put(3L, "Mexique");
    return idsAndNamesMap;
  }

  @Override protected CountriesDbHelper getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new CountriesDbHelper(context);
  }

  @Override public void setUp() {
    super.setUp();
    mCountryNameDbDao = new CountryNameDbDao();
  }

  @Test public void testInsertNames() {
    boolean result =
        mCountryNameDbDao.insertNames(mDbDao.getOdigeoDatabase(), 1L, getIdsAndNamesMap());
    Assert.assertTrue(result);
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
