package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.Insurance;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.InsuranceDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class InsuranceDBDAOTest extends DbDaoTest<InsuranceDBDAO> {

  private Insurance mInsurance;

  @Override protected InsuranceDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new InsuranceDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mInsurance =
        new Insurance(1, 2, "conditionURLPrimary", "conditionURLSecundary", "insuranceDescription",
            "insuranceType", "policy", true, "subtitle", "title", 3.0, 4);
  }

  @Test public void addInsuranceAndGetInsuranceTest() {
    long rowId;

    rowId = mDbDao.addInsurance(mInsurance);

    assertNotEquals(rowId, -1);

    Insurance insurance = mDbDao.getInsurance(mInsurance.getId());

    assertEquals(mInsurance.getId(), insurance.getId());
    assertEquals(mInsurance.getInsuranceId(), insurance.getInsuranceId());
    assertEquals(mInsurance.getConditionURLPrimary(), insurance.getConditionURLPrimary());
    assertEquals(mInsurance.getConditionURLSecundary(), insurance.getConditionURLSecundary());
    assertEquals(mInsurance.getInsuranceDescription(), insurance.getInsuranceDescription());
    assertEquals(mInsurance.getInsuranceType(), insurance.getInsuranceType());
    assertEquals(mInsurance.getPolicy(), insurance.getPolicy());
    assertEquals(mInsurance.getSelectable(), insurance.getSelectable());
    assertEquals(mInsurance.getSubtitle(), insurance.getSubtitle());
    assertEquals(mInsurance.getTitle(), insurance.getTitle());
    assertEquals(mInsurance.getTotal(), insurance.getTotal(), 0);
    assertEquals(mInsurance.getBookingId(), insurance.getBookingId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}