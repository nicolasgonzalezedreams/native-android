package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.geo.City;
import com.odigeo.data.entity.geo.Coordinates;
import com.odigeo.data.entity.geo.LocationDescriptionType;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.CityDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CityDBDAOTest extends DbDaoTest<CityDBDAO> {

  private City city;

  @Override protected CityDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new CityDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    city = new City();
    city.setIataCode("BCN");
    city.setCityName("Barcelona");
    city.setType(LocationDescriptionType.CITY);
    city.setCoordinates(new Coordinates(1.223, 34.4));
    city.setCountryCode("ES");
    city.setCountryName("España");
    city.setGeoNodeId(1342432);
    city.setGroupParent(false);
    city.setDistanceToCurrentLocation(23432);
  }

  @Test public void addCityAndGetCityTest() {
    boolean added = mDbDao.addCity(city);
    assertTrue(added);

    City cityRetrieved = mDbDao.getCityByIATA(city.getIataCode());

    assertEquals(cityRetrieved.getIataCode(), city.getIataCode());
    assertEquals(cityRetrieved.getCityName(), city.getCityName());
    assertEquals(cityRetrieved.getName(), city.getName());

    Coordinates coordinatesRetrieved = cityRetrieved.getCoordinates();
    Coordinates coordinates = city.getCoordinates();

    assertEquals(coordinatesRetrieved.getLatitude(), coordinates.getLatitude(), 0);
    assertEquals(coordinatesRetrieved.getLongitude(), coordinates.getLongitude(), 0);

    assertEquals(cityRetrieved.getCountryCode(), city.getCountryCode());
    assertEquals(cityRetrieved.getCountryName(), city.getCountryName());
    assertEquals(cityRetrieved.getGeoNodeId(), city.getGeoNodeId());
    assertEquals(cityRetrieved.isGroupParent(), city.isGroupParent());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
