package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.LanguageDbDao;
import com.odigeo.dataodigeo.db.helper.CountriesDbHelper;
import java.lang.reflect.Field;
import java.util.Map;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 12/02/16.
 */
public class LanguageDbDaoTest extends DbDaoTest<CountriesDbHelper> {

  private final String[] languages = { "es", "en", "fr" };
  private LanguageDbDao mLanguageDbDao;

  @Override protected CountriesDbHelper getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new CountriesDbHelper(context);
  }

  @Override public void setUp() {
    super.setUp();
    mLanguageDbDao = new LanguageDbDao();
  }

  @Test public void testInsertLanguage() {
    long insertedId = mLanguageDbDao.insert(mDbDao.getOdigeoDatabase(), languages[0]);
    Assert.assertEquals(1, insertedId);
    Map<String, Long> map = mLanguageDbDao.getIdsMap(mDbDao.getOdigeoDatabase());
    Assert.assertEquals(1, map.keySet().size());
    Assert.assertEquals(1L, (long) map.get(languages[0]));
  }

  @Test public void testInsertDuplicated() {
    long insertedId = mLanguageDbDao.insert(mDbDao.getOdigeoDatabase(), languages[0]);
    Assert.assertEquals(1, insertedId);
    insertedId = mLanguageDbDao.insert(mDbDao.getOdigeoDatabase(), languages[0]);
    Assert.assertEquals(-1, insertedId);
    Map<String, Long> idsMap = mLanguageDbDao.getIdsMap(mDbDao.getOdigeoDatabase());
    Assert.assertEquals(1, idsMap.keySet().size());
    Assert.assertEquals(1L, (long) idsMap.get(languages[0]));
  }

  @Test public void testGetIdsMap() {
    long insertedId = -1;
    for (String language : languages) {
      insertedId = mLanguageDbDao.insert(mDbDao.getOdigeoDatabase(), language);
    }
    Assert.assertEquals(languages.length, (int) insertedId);
    Map<String, Long> map = mLanguageDbDao.getIdsMap(mDbDao.getOdigeoDatabase());
    Assert.assertEquals(languages.length, map.keySet().size());
    Assert.assertEquals(2L, (long) map.get(languages[1]));
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
