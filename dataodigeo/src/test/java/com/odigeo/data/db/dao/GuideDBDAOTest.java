package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.booking.Guide;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.GuideDBDAO;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class GuideDBDAOTest extends DbDaoTest<GuideDBDAO> {

  private Guide mGuide;
  private Guide mGuideSecond;
  private Guide mGuideThird;

  @Override protected GuideDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new GuideDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mDbDao.disablePopulate();
    mGuide = new Guide(1, 777,
        "https://api.arrivalguides.com/en/Dynamic/Download?dest=LONDON&partner=EDREAMS", "en");

    mGuideSecond = new Guide(2, 555,
        "https://api.arrivalguides.com/en/Dynamic/Download?dest=MADRID&partner=EDREAMS", "sp");

    mGuideThird = new Guide(3, 333,
        "https://api.arrivalguides.com/en/Dynamic/Download?dest=BERLIN&partner=EDREAMS", "de");
  }

  @Test public void addGuide() {
    long rowId;
    rowId = mDbDao.addGuide(mGuide);
    assertNotEquals(rowId, -1);
  }

  @Test public void getGuideTest() {
    mDbDao.addGuide(mGuide);
    Guide guide = mDbDao.getGuide(mGuide.getId());
    assertEquals(guide.getId(), mGuide.getId());
    assertEquals(guide.getGeoNodeId(), mGuide.getGeoNodeId());
    assertEquals(guide.getUrl(), mGuide.getUrl());
    assertEquals(guide.getLanguage(), mGuide.getLanguage());
  }

  @Test public void getGuideByGeoNodeTest() {
    mDbDao.addGuide(mGuide);
    Guide guide = mDbDao.getGuideByGeoNodeId(mGuide.getGeoNodeId());
    assertEquals(guide.getId(), mGuide.getId());
    assertEquals(guide.getGeoNodeId(), mGuide.getGeoNodeId());
    assertEquals(guide.getUrl(), mGuide.getUrl());
    assertEquals(guide.getLanguage(), mGuide.getLanguage());
  }

  @Test public void getAllGuidesTest() {

    long rowId;
    rowId = mDbDao.addGuide(mGuide);
    assertNotEquals(rowId, -1);
    rowId = mDbDao.addGuide(mGuideSecond);
    assertNotEquals(rowId, -1);
    rowId = mDbDao.addGuide(mGuideThird);
    assertNotEquals(rowId, -1);

    Map<Long, Guide> guides = mDbDao.getAllGuides();

    assertEquals(guides.size(), 3);

    Guide firstGuide = guides.get(mGuide.getGeoNodeId());
    assertEquals(firstGuide.getId(), mGuide.getId());
    assertEquals(firstGuide.getGeoNodeId(), mGuide.getGeoNodeId());
    assertEquals(firstGuide.getUrl(), mGuide.getUrl());
    assertEquals(firstGuide.getLanguage(), mGuide.getLanguage());

    Guide secondGuide = guides.get(mGuideSecond.getGeoNodeId());
    assertEquals(secondGuide.getId(), mGuideSecond.getId());
    assertEquals(secondGuide.getGeoNodeId(), mGuideSecond.getGeoNodeId());
    assertEquals(secondGuide.getUrl(), mGuideSecond.getUrl());
    assertEquals(secondGuide.getLanguage(), mGuideSecond.getLanguage());

    Guide thirdGuide = guides.get(mGuideThird.getGeoNodeId());
    assertEquals(thirdGuide.getId(), mGuideThird.getId());
    assertEquals(thirdGuide.getGeoNodeId(), mGuideThird.getGeoNodeId());
    assertEquals(thirdGuide.getUrl(), mGuideThird.getUrl());
    assertEquals(thirdGuide.getLanguage(), mGuideThird.getLanguage());
  }

  @Test public void deleteGuideTest() {
    mDbDao.addGuide(mGuide);
    assertTrue(mDbDao.deleteGuide(mGuide.getGeoNodeId()));
  }

  @Test public void deleteGuidesTest() {
    long rowId;
    rowId = mDbDao.addGuide(mGuide);
    assertNotEquals(rowId, -1);
    rowId = mDbDao.addGuide(mGuideSecond);
    assertNotEquals(rowId, -1);
    rowId = mDbDao.addGuide(mGuideThird);
    assertNotEquals(rowId, -1);

    Map<Long, Guide> guidesToDelete = new HashMap<>();
    guidesToDelete.put(mGuide.getGeoNodeId(), mGuide);
    guidesToDelete.put(mGuideSecond.getGeoNodeId(), mGuideSecond);

        /*
        If all guides were deleted successfully, deleteGuides should return an empty Map
         */
    assertEquals(mDbDao.deleteGuides(guidesToDelete).size(), 0);

        /*
        Since we added three guides to database and deleted two, getAllGuides should return 1 guide
         */
    assertEquals(mDbDao.getAllGuides().size(), 1);
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
