package com.odigeo.data.db.dao;

import android.content.Context;
import com.odigeo.data.entity.userData.UserFrequentFlyer;
import com.odigeo.dataodigeo.db.OdigeoSQLiteOpenHelper;
import com.odigeo.dataodigeo.db.dao.UserFrequentFlyerDBDAO;
import java.lang.reflect.Field;
import org.junit.After;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class UserFrequetFlyerDBDAOTest extends DbDaoTest<UserFrequentFlyerDBDAO> {

  private UserFrequentFlyer mUserFrequentFlyer;

  @Override protected UserFrequentFlyerDBDAO getDbDao() {
    Context context = RuntimeEnvironment.application.getApplicationContext();
    return new UserFrequentFlyerDBDAO(context);
  }

  @Override public void setUp() {
    super.setUp();
    mUserFrequentFlyer = new UserFrequentFlyer(1, 2, "XGD-1651", "jasgljk13013kadgkn90424tk", 3);
  }

  @Test public void addUserFrequentFlyerAndGetUserFrequentFlyerTest() {
    long rowId = mDbDao.addUserFrequentFlyer(mUserFrequentFlyer);
    assertNotEquals(rowId, -1);
    UserFrequentFlyer userFrequentFlyer =
        mDbDao.getUserFrequentFlyer(mUserFrequentFlyer.getFrequentFlyerId());
    assertEquals(userFrequentFlyer.getFrequentFlyerId(), mUserFrequentFlyer.getFrequentFlyerId());
    assertEquals(userFrequentFlyer.getAirlineCode(), mUserFrequentFlyer.getAirlineCode());
    assertEquals(userFrequentFlyer.getFrequentFlyerNumber(),
        mUserFrequentFlyer.getFrequentFlyerNumber());
    assertEquals(userFrequentFlyer.getUserTravellerId(), mUserFrequentFlyer.getUserTravellerId());
  }

  @After public void tearDown() throws Exception {
    Field field = OdigeoSQLiteOpenHelper.class.getDeclaredField("db");
    field.setAccessible(true);
    field.set(null, null);
  }
}
