package go.voyage;

import go.voyage.tests.importTrip.ImportTripTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    ImportTripTest.class
}) public class NonDeterministicTestSuite {
}
