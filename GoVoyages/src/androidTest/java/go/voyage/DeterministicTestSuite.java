package go.voyage;

import go.voyage.tests.calendar.CalendarHolidaysTest;
import go.voyage.tests.calendar.CalendarSelectionTest;
import go.voyage.tests.carousel.CarouselCTATest;
import go.voyage.tests.carousel.CarouselDropOffTest;
import go.voyage.tests.confirmation.ConfirmationTest;
import go.voyage.tests.insurances.InsurancesTest;
import go.voyage.tests.logout.LogoutTest;
import go.voyage.tests.passenger.BaggageTest;
import go.voyage.tests.passenger.PassengerTest;
import go.voyage.tests.payment.PaymentFormTest;
import go.voyage.tests.payment.PaymentPurchaseTest;
import go.voyage.tests.payment.PromoCodeTest;
import go.voyage.tests.register.RegisterTest;
import go.voyage.tests.results.ResultsTest;
import go.voyage.tests.search.SearchTest;
import go.voyage.tests.settings.SettingsTest;
import go.voyage.tests.walkthrough.WalkthroughTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
    CarouselCTATest.class, CarouselDropOffTest.class, ConfirmationTest.class, InsurancesTest.class,
    LogoutTest.class, BaggageTest.class, PassengerTest.class, PaymentFormTest.class,
    PaymentPurchaseTest.class, PromoCodeTest.class, RegisterTest.class, ResultsTest.class,
    SearchTest.class, SettingsTest.class, WalkthroughTest.class, CalendarHolidaysTest.class,
    CalendarSelectionTest.class
}) public class DeterministicTestSuite {
}
