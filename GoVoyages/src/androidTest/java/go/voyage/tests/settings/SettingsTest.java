package go.voyage.tests.settings;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.settings.OdigeoSettingsTest;
import com.pepino.annotations.FeatureOptions;
import go.voyage.activities.SettingsActivity;

/**
 * Created by Javier Marsicano on 18/01/17.
 */

@FeatureOptions(feature = "settings.feature") public class SettingsTest extends OdigeoSettingsTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(SettingsActivity.class);
  }
}

