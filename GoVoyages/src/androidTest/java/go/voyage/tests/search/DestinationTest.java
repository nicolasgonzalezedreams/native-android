package go.voyage.tests.search;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.search.OdigeoDestinationTest;
import com.pepino.annotations.FeatureOptions;
import go.voyage.activities.DestinationActivity;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 23/01/17
 */
@FeatureOptions(feature = "destination.feature") public class DestinationTest
    extends OdigeoDestinationTest {

  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(DestinationActivity.class, true, false);
  }
}
