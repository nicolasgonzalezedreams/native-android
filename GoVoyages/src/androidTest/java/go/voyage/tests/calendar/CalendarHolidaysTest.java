package go.voyage.tests.calendar;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.search.OdigeoCalendarHolidaysTest;
import com.pepino.annotations.FeatureOptions;
import go.voyage.activities.SearchActivity;

/**
 * Created by daniel.morales on 22/12/16.
 */
@FeatureOptions(feature = "calendar_holidays.feature") public class CalendarHolidaysTest
    extends OdigeoCalendarHolidaysTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(SearchActivity.class);
  }
}
