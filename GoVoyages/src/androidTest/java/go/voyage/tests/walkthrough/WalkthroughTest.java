package go.voyage.tests.walkthrough;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.walkthrough.OdigeoWalkthroughTest;
import com.pepino.annotations.FeatureOptions;
import go.voyage.activities.WalkthroughActivity;

/**
 * Created by daniel.morales on 3/2/17.
 */
@FeatureOptions(feature = "walkthrough.feature") public class WalkthroughTest
    extends OdigeoWalkthroughTest {

  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(WalkthroughActivity.class, false, false);
  }
}
