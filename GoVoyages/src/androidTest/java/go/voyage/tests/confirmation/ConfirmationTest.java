package go.voyage.tests.confirmation;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.confirmation.OdigeoConfirmationTest;
import com.pepino.annotations.FeatureOptions;
import go.voyage.activities.ConfirmationActivity;

@FeatureOptions(feature = "confirmation.feature") public class ConfirmationTest
    extends OdigeoConfirmationTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(ConfirmationActivity.class, false, false);
  }
}