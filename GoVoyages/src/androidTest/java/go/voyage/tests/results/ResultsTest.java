package go.voyage.tests.results;

import android.support.test.rule.ActivityTestRule;
import com.odigeo.test.tests.results.OdigeoResultsTest;
import com.pepino.annotations.FeatureOptions;
import go.voyage.activities.SearchResultsActivity;

/**
 * Created by Javier Marsicano on 08/03/17.
 */

@FeatureOptions(feature = "results.feature") public class ResultsTest extends OdigeoResultsTest {
  @Override public ActivityTestRule getActivityTestRule() {
    return new ActivityTestRule<>(SearchResultsActivity.class, false, false);
  }
}
