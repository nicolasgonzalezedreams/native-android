Feature: As a user I want to change baggage amount in passengers page

  Scenario Outline: Select baggage
    Given user is in Passenger page
    When user select <numberOfBaggages> baggages
    Then user has <numberOfBaggages> baggages selected

    Examples:
      | numberOfBaggages |
      | 0                |
      | 1                |

  Scenario Outline: Deselect baggage
    Given user is in Passenger page
    When user select <numberOfBaggages> baggages
    And user unselect baggage
    Then user has no baggage selected
    Examples:
      | numberOfBaggages |
      | 1                |

  Scenario: Baggage included in both legs
    Given user is in Passenger page With baggage included
    When user has baggage included
    Then baggage info appears in one row