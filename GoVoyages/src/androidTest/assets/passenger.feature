Feature: As user, I want to get filled the passengers page if I have an user added in MyData page.

  Scenario: Passenger information is filled if user has saved a passenger in MyData
    Given user is in Passenger page
    When User have saved a complete passenger in MyData
    Then Passenger information is filled

  Scenario: Show details travel page from passengers page
    Given user is in Passenger page
    When user select flight details button
    Then user is in Flight details page

  Scenario Outline: Can continue the booking flow with passengers information filled
    Given user is in Passenger page
    When fields are filled with the following data: <titleKey> title, <name> name, <surname> surname, <mainPassenger> main passenger, <address> address, <city> city, <postcode> postcode, <countryOfResidence> country of residence, <countryCode> country code, <phoneNumber> phone number, <email> email
    And user select the Continue button
    Then we reached the Assurance page

    Examples:
      | titleKey  | name     | surname     | mainPassenger | address           | city     | postcode | countryOfResidence | countryCode | phoneNumber | email              |
      | common_mr | TestName | TestSurname | Enabled       | Calle Test 45 1 2 | TestCity | 12345    | ES                 | ES +34      | 654987123   | testemail@test.com |

  Scenario Outline: Can not continue the booking flow if passengers information is not filled
    Given user is in Passenger page
    When fields are filled with the following data: <titleKey> title, <name> name, <surname> surname, <mainPassenger> main passenger, <address> address, <city> city, <postcode> postcode, <countryOfResidence> country of residence, <countryCode> country code, <phoneNumber> phone number, <email> email
    And user select the Continue button
    Then alert message appears

    Examples:
      | titleKey  | name     | surname     | mainPassenger | address           | city     | postcode | countryOfResidence | countryCode | phoneNumber | email              |
      | common_mr |          | TestSurname | Enabled       | Calle Test 45 1 2 | TestCity | 12345    | ES                 | ES +34      | 654987123   | testemail@test.com |
      | common_mr | TestName |             | Enabled       | Calle Test 45 1 2 | TestCity | 12345    | ES                 | ES +34      | 654987123   | testemail@test.com |
      | common_mr | TestName | TestSurname | Enabled       |                   | TestCity | 12345    | ES                 | ES +34      | 654987123   | testemail@test.com |
      | common_mr | TestName | TestSurname | Enabled       | Calle Test 45 1 2 |          | 12345    | ES                 | ES +34      | 654987123   | testemail@test.com |
      | common_mr | TestName | TestSurname | Enabled       | Calle Test 45 1 2 | TestCity |          | ES                 | ES +34      | 654987123   | testemail@test.com |
      | common_mr | TestName | TestSurname | Enabled       | Calle Test 45 1 2 | TestCity | 12345    |                    | ES +34      | 654987123   | testemail@test.com |
      | common_mr | TestName | TestSurname | Enabled       | Calle Test 45 1 2 | TestCity | 12345    | ES                 |             | 654987123   | testemail@test.com |
      | common_mr | TestName | TestSurname | Enabled       | Calle Test 45 1 2 | TestCity | 12345    | ES                 | ES +34      |             | testemail@test.com |
      | common_mr | TestName | TestSurname | Enabled       | Calle Test 45 1 2 | TestCity | 12345    | ES                 | ES +34      | 654987123   |                    |

  Scenario: Popup of repeat search appears after timeout
    Given user is in Passenger page
    When Timeout has expired
    Then TimeCounter popup appears

  Scenario: Frequent flier information can be added
    Given user is in Passenger page
    When User select frequent flier information
    Then User is in frequent flier information page

  Scenario: Duplicated booking message when user perform the same search as an already booked flight
    Given user is in Passenger page
    When fields are filled with the same data as a booking stored in the device
    And user continues with the booking flow
    Then duplicated booking message appears
