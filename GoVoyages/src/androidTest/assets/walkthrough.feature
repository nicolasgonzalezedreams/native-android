Feature: Specific walkthrough is only showed on specific countries, default one is showed instead.

  Scenario Outline: Paypal walkthrough
    Given The <market> market is selected
    When Walkthrough page appears
    Then Specific walkthrough <isShowed>

    Examples:
      | market | isShowed |
      | FR     | false    |

