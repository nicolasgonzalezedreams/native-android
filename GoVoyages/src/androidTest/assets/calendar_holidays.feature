Feature: Weekends are marked as holidays and regional holidays in case we are in ES, US markets

  Scenario Outline: Weekends are marked in bold
    Given I have selected <country> market
    When I am in Calendar screen
    Then Weekends are marked as holidays

    Examples:
      | country |
      | FR      |

  Scenario Outline: Regional holidays are NOT marked as holidays
    Given I have selected <country> market
    When I am in Calendar screen
    Then Regional holidays are NOT marked as holiday

    Examples:
      | country |
      | FR      |