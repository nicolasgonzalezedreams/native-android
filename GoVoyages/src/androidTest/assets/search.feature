Feature: As a user I want to perform a flight search, so that I can select a flight.

  Scenario Outline: Search Return direct flight
    Given user is in search page
    When user selects only direct flights
    And User searches <type> flight
    Then ReSearch Page should appear

    Examples:
      | type              |
      | Return No Results |

  Scenario Outline: Search flights
    Given user is in search page
    When User searches <type> flight
    Then flight results should appear

    Examples:
      | type      |
      | Return    |
      | Multistop |


  Scenario Outline: Search flights error
    Given user is in search page
    When User searches <type> flight
    Then error page should appear

    Examples:
      | type         |
      | Search_error |


#  Scenario Outline: Search flight with different cabin classes
#    Given user is in search page
#    When User select <class> class
#    And User searches <type> flight
#    Then flight results should appear
#    And flight class should be <class>
#
#    Examples:
#      | class    | type        |
#      | all      | OW          |
#      | economy  | OW economy  |
#      | premium  | OW premium  |
#      | business | OW business |
#      | first    | OW first    |

  Scenario Outline: Search only direct flight
    Given user is in search page
    When user selects only direct flights
    And User searches <type> flight
    Then only direct flights should appear

    Examples:
      | type          |
      | Return Direct |

  Scenario Outline: Select a recent origin
    Given user is in search page
    And the user have searched origin <place>
    When user select recent origin <place>
    Then origin selected should be <place>

    Examples:
      | place |
      | PAR   |

  Scenario Outline: Select a recent destination
    Given user is in search page
    And the user have searched destination <place>
    When user select recent destination <place>
    Then destination selected should be <place>

    Examples:
      | place |
      | MAD   |

  Scenario: Use latest search to get a flight
    Given user have stored latest searches
    And user is in search page
    When User select a latest search
    Then flight results should appear

  Scenario: Delete all latest searches
    Given user have stored latest searches
    And user is in search page
    When User delete all latest searches
    Then Latest search list should not appear

  Scenario Outline: Latest searches syncro added searches
    Given user is logged in
    And user is in search page
    When User searches <type> flight
    Then searches should be syncronized in user account

    Examples:
      | type           |
      | OW - Logged In |

  Scenario Outline: Number of passengers cannot be higher than 9, number of infants per adult is 2
  and number of babies for adult is 1
    Given user is in search page
    When user select <Adults> adults, <Infants> infants and <Babies> babies
    Then combination <IsPossible> possible
    Examples:
      | Adults | Infants | Babies | IsPossible |
      | 9      | 0       | 0      | yes        |
      | 8      | 2       | 0      | no         |
      | 8      | 0       | 2      | no         |
      | 1      | 2       | 1      | yes        |
      | 1      | 3       | 0      | no         |
      | 1      | 0       | 2      | no         |
