package go.voyage.activities;

import android.os.Bundle;
import com.odigeo.app.android.lib.activities.OdigeoConfirmationActivity;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.squareup.otto.Subscribe;

/**
 * Created by emiliano.desantis on 09/10/2014.
 */
public class ConfirmationActivity extends OdigeoConfirmationActivity {

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    performGATracking();
  }

  @Subscribe public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getApplication());
  }

  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getApplication());
  }
}
