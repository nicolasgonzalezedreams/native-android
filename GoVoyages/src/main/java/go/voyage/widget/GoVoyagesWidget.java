package go.voyage.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.DimenRes;
import android.util.TypedValue;
import android.widget.RemoteViews;
import com.odigeo.app.android.lib.utils.Util;
import com.odigeo.app.android.lib.widget.OdigeoWidget;
import com.odigeo.data.entity.booking.Segment;
import go.voyage.navigator.NavigationDrawerNavigator;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 05/03/15
 */
public class GoVoyagesWidget extends OdigeoWidget {

  @Override public Class getHomeActivity() {
    return NavigationDrawerNavigator.class;
  }

  @Override public Class getWidgetServiceClass() {
    return GoVoyagesWidgetService.class;
  }

  @Override protected void adjustSmallBookingWidget(Context context, RemoteViews remoteViews) {
    updateViewParams(context, remoteViews, com.odigeo.app.android.lib.R.dimen.size_font_L,
        com.odigeo.app.android.lib.R.dimen.size_font_XXS,
        com.odigeo.app.android.lib.R.dimen.size_font_XS,
        com.odigeo.app.android.lib.R.dimen.odigeo_widget_go_padding_vertical_small);
  }

  @Override protected void adjustNormalBookingWidget(Context context, RemoteViews remoteViews) {
    updateViewParams(context, remoteViews, com.odigeo.app.android.lib.R.dimen.size_font_XL,
        com.odigeo.app.android.lib.R.dimen.size_font_XS,
        com.odigeo.app.android.lib.R.dimen.size_font_S,
        com.odigeo.app.android.lib.R.dimen.odigeo_widget_go_padding_vertical);
  }

  /**
   * Update view params for GoVoyages booking layout
   *
   * @param context Context to get the resources
   * @param remoteViews RemoteViews to be updated
   * @param cityNameFontSize Font size resource for city name fields
   * @param labelFontSize Font size resource for label fields
   * @param dateFontSize Font size resource for date fields
   */
  private void updateViewParams(Context context, RemoteViews remoteViews,
      @DimenRes int cityNameFontSize, @DimenRes int labelFontSize, @DimenRes int dateFontSize,
      @DimenRes int sectionMarginSmall) {
    float cityNameSize = Util.getDimensionInDP(context, cityNameFontSize);
    float labelSize = Util.getDimensionInDP(context, labelFontSize);
    float dateSize = Util.getDimensionInDP(context, dateFontSize);
    updateFontSize(remoteViews, cityNameSize, labelSize, dateSize);

    int sectionPadding = (int) Util.getDimensionInDP(context, sectionMarginSmall);
    updateSectionPadding(remoteViews, sectionPadding);
  }

  /**
   * Update all the necessary remote views font size using the specified values
   *
   * @param remoteViews Remote view object to update
   * @param cityNameSize Size to be used for city name fields
   * @param labelSize Size to be used for label fields
   * @param dateSize Size to be used for date fields
   */
  @TargetApi(Build.VERSION_CODES.JELLY_BEAN) private void updateFontSize(RemoteViews remoteViews,
      float cityNameSize, float labelSize, float dateSize) {
    remoteViews.setTextViewTextSize(com.odigeo.app.android.lib.R.id.lblDeparture,
        TypedValue.COMPLEX_UNIT_SP, cityNameSize);
    remoteViews.setTextViewTextSize(com.odigeo.app.android.lib.R.id.lblDepartureLabel,
        TypedValue.COMPLEX_UNIT_SP, labelSize);
    remoteViews.setTextViewTextSize(com.odigeo.app.android.lib.R.id.lblDepartureDate,
        TypedValue.COMPLEX_UNIT_SP, dateSize);
    remoteViews.setTextViewTextSize(com.odigeo.app.android.lib.R.id.lblDepartureTime,
        TypedValue.COMPLEX_UNIT_SP, dateSize);

    remoteViews.setTextViewTextSize(com.odigeo.app.android.lib.R.id.lblArrival,
        TypedValue.COMPLEX_UNIT_SP, cityNameSize);
    remoteViews.setTextViewTextSize(com.odigeo.app.android.lib.R.id.lblArrivalLabel,
        TypedValue.COMPLEX_UNIT_SP, labelSize);
    remoteViews.setTextViewTextSize(com.odigeo.app.android.lib.R.id.lblArrivalDate,
        TypedValue.COMPLEX_UNIT_SP, dateSize);
    remoteViews.setTextViewTextSize(com.odigeo.app.android.lib.R.id.lblArrivalTime,
        TypedValue.COMPLEX_UNIT_SP, dateSize);
  }

  /**
   * Update the paddings of the sections for GoVoyages
   *
   * @param remoteViews Remote views to update
   * @param sectionPadding Section padding to set between each content section
   */
  @TargetApi(Build.VERSION_CODES.JELLY_BEAN) private void updateSectionPadding(
      RemoteViews remoteViews, int sectionPadding) {
    remoteViews.setViewPadding(com.odigeo.app.android.lib.R.id.lnlDeparture, 0, sectionPadding, 0,
        sectionPadding);
    remoteViews.setViewPadding(com.odigeo.app.android.lib.R.id.lnlArrival, 0, sectionPadding, 0,
        sectionPadding);
  }

  @Override protected void setBookingScales(Segment segment, RemoteViews remoteViews) {
    //nothing
  }
}
