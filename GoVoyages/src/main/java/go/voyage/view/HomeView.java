package go.voyage.view;

import android.view.View;
import com.odigeo.app.android.lib.utils.GATracker;
import com.odigeo.app.android.lib.utils.events.GAScreenTrackingEvent;
import com.odigeo.app.android.lib.utils.events.GATrackingEvent;
import com.squareup.otto.Subscribe;

/**
 * Created by daniel.morales on 22/12/16.
 */

public class HomeView extends com.odigeo.app.android.view.HomeView {

  public static HomeView newInstance() {
    return new HomeView();
  }

  @Override public void onResume() {
    super.onResume();
  }

  @Override protected void initComponent(View view) {
    super.initComponent(view);
  }

  //TODO: Use the generic tracker
  @Subscribe public void onGAEventTriggered(GATrackingEvent event) {
    GATracker.trackEvent(event, getActivity().getApplication());
  }

  //TODO: Use the generic tracker
  @Subscribe public void onGAScreenTrackingTriggered(GAScreenTrackingEvent screenTrackingEvent) {
    GATracker.trackScreen(screenTrackingEvent.getLabel(), getActivity().getApplication());
  }

  @Override public void setCampaignBackgroundImage() {
  }

  @Override public void setCityBackgroundImage(String imageUrl) {
  }

  @Override public void setDefaultBackgroundImage() {
  }

  @Override public void translateBackgroundPosition(float position) {
  }

  @Override protected boolean shouldShowPromotionText() {
    return false;
  }
}