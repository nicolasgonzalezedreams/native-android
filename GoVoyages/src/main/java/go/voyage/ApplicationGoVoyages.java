package go.voyage;

import com.crashlytics.android.Crashlytics;
import com.odigeo.app.android.lib.OdigeoApp;
import com.odigeo.app.android.lib.config.Configuration;
import com.odigeo.app.android.lib.config.Fonts;
import com.odigeo.app.android.navigator.InsurancesNavigator;
import com.odigeo.app.android.navigator.MyTripDetailsNavigator;
import com.odigeo.app.android.navigator.MyTripsNavigator;
import com.odigeo.app.android.navigator.PassengerNavigator;
import com.odigeo.app.android.navigator.PaymentNavigator;
import com.odigeo.app.android.navigator.TravellersNavigator;
import go.voyage.activities.CalendarActivity;
import go.voyage.activities.ConditionsInsurances;
import go.voyage.activities.ConfirmationActivity;
import go.voyage.activities.DestinationActivity;
import go.voyage.activities.DuplicatedBookingActivity;
import go.voyage.activities.FiltersActivity;
import go.voyage.activities.FrequentFlyerActivity;
import go.voyage.activities.NoConnectionActivity;
import go.voyage.activities.PixelWebViewActivity;
import go.voyage.activities.QAModeActivity;
import go.voyage.activities.SearchActivity;
import go.voyage.activities.SearchResultsActivity;
import go.voyage.activities.SettingsActivity;
import go.voyage.activities.SummaryActivity;
import go.voyage.activities.WalkthroughActivity;
import go.voyage.activities.WebViewActivity;
import go.voyage.navigator.NavigationDrawerNavigator;
import go.voyage.widget.GoVoyagesWidget;
import io.fabric.sdk.android.Fabric;

/**
 * General application class for GoVoyage
 *
 * @author Manuel Ortiz
 * @author Javier Silva
 * @since 28/08/2014
 */
public class ApplicationGoVoyages extends OdigeoApp {

  @Override public void onCreate() {
    configureBuildTypes(BuildConfig.MSL_URL, BuildConfig.DEBUG);
    super.onCreate();
  }

  protected void setFonts() {

    Fonts fonts = Configuration.getInstance().getFonts();
    fonts.setBlack(getFontAsset(com.odigeo.app.android.lib.R.string.fontBlack));
    fonts.setBlackItalic(getFontAsset(com.odigeo.app.android.lib.R.string.fontBlackItalic));
    fonts.setBold(getFontAsset(com.odigeo.app.android.lib.R.string.fontBold));
    fonts.setBoldItalic(getFontAsset(com.odigeo.app.android.lib.R.string.fontBoldItalic));
    fonts.setExtraLight(getFontAsset(com.odigeo.app.android.lib.R.string.fontExtraLight));
    fonts.setExtraLightItalic(
        getFontAsset(com.odigeo.app.android.lib.R.string.fontExtraLightItalic));
    fonts.setItalic(getFontAsset(com.odigeo.app.android.lib.R.string.fontItalic));
    fonts.setLight(getFontAsset(com.odigeo.app.android.lib.R.string.fontLight));
    fonts.setLightItalic(getFontAsset(com.odigeo.app.android.lib.R.string.fontLightItalic));
    fonts.setRegular(getFontAsset(com.odigeo.app.android.lib.R.string.fontRegular));
    fonts.setSemiBold(getFontAsset(com.odigeo.app.android.lib.R.string.fontSemiBold));
    fonts.setSemiBoldItalic(getFontAsset(com.odigeo.app.android.lib.R.string.fontSemiBoldItalic));
    fonts.pack();
  }

  @Override public Class getHomeActivity() {

    return NavigationDrawerNavigator.class;
  }

  @Override public Class getCalendarActivityClass() {

    return CalendarActivity.class;
  }

  @Override public Class getDestinationActivityClass() {

    return DestinationActivity.class;
  }

  @Override public Class getSearchResultsActivityClass() {

    return SearchResultsActivity.class;
  }

  @Override public Class getFiltersActivityClass() {

    return FiltersActivity.class;
  }

  @Override public Class getInsurancesActivityClass() {

    return InsurancesNavigator.class;
  }

  @Override public Class getInsurancesConditionsActivityClass() {
    return ConditionsInsurances.class;
  }

  @Override public Class getSummaryActivityClass() {

    return SummaryActivity.class;
  }

  @Override public Class getSearchFlightsActivityClass() {

    return SearchActivity.class;
  }

  @Override public Class getMyTripDetailsActivityClass() {
    return MyTripDetailsNavigator.class;
  }

  @Override public Class getMyTripsActivityClass() {
    return MyTripsNavigator.class;
  }

  @Override public Class getMyInfoActivityClass() {
    return TravellersNavigator.class;
  }

  @Override public Class getSettingsActivityClass() {

    return SettingsActivity.class;
  }

  @Override public Class getPassengersActivityClass() {
    return PassengerNavigator.class;
  }

  @Override public Class getPaymentActivityClass() {

    return PaymentNavigator.class;
  }

  @Override public Class getPixelWebViewActivityClass() {

    return PixelWebViewActivity.class;
  }

  @Override public Class getConfirmationActivityClass() {

    return ConfirmationActivity.class;
  }

  @Override public Class getNoConnectionClass() {

    return NoConnectionActivity.class;
  }

  @Override public Class getWebViewActivityClass() {

    return WebViewActivity.class;
  }

  @Override public Class getBookingDuplicatedActivityClass() {
    return DuplicatedBookingActivity.class;
  }

  @Override public Class getFrequentFlyer() {
    return FrequentFlyerActivity.class;
  }

  @Override public Class getWalkthroughActivity() {
    return WalkthroughActivity.class;
  }

  @Override public Class getQAModeActivityClass() {

    return QAModeActivity.class;
  }

  @Override public Class getWidgetClass() {
    return GoVoyagesWidget.class;
  }

  @Override public void setCrashlyticValue(String key, String value) {
    Crashlytics.setString(key, value);
  }

  public void initializeCrashlytics() {
    if (!Fabric.isInitialized()) {
      Fabric.with(this, new Crashlytics());
    }
  }

  @Override public boolean hasAnimatedBackground() {
    return true;
  }

  @Override public Class getNavigationDrawerNavigator() {
    return NavigationDrawerNavigator.class;
  }

  @Override public void trackNonFatal(Throwable throwable) {
    Crashlytics.logException(throwable);
  }

  @Override public void setBool(String key, boolean value) {
    Crashlytics.setBool(key, value);
  }

  @Override public void setDouble(String key, double value) {
    Crashlytics.setDouble(key, value);
  }

  @Override public void setFloat(String key, float value) {
    Crashlytics.setFloat(key, value);
  }

  @Override public void setInt(String key, int value) {
    Crashlytics.setInt(key, value);
  }

  @Override public void setString(String key, String value) {
    Crashlytics.setString(key, value);
  }

  @Override public void setLong(String key, long value) {
    Crashlytics.setLong(key, value);
  }
}
