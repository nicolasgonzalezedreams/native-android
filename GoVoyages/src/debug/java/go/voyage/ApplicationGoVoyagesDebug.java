package go.voyage;

import com.facebook.stetho.Stetho;

/**
 * Created by Javier Marsicano on 05/04/16.
 */
public class ApplicationGoVoyagesDebug extends ApplicationGoVoyages {
    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }

    @Override
    public boolean hasAnimatedBackground() {
        return false;
    }
}
